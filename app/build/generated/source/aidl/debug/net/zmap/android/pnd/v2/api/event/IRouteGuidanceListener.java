/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /Users/Yamamoto/Documents/AndroidProjects/SakuraVanDemo.gles.nojni.Android50/app/src/main/aidl/net/zmap/android/pnd/v2/api/event/IRouteGuidanceListener.aidl
 */
package net.zmap.android.pnd.v2.api.event;
/**
 * ルート案内リスナインターフェース (aidl)
 */
public interface IRouteGuidanceListener extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener
{
private static final java.lang.String DESCRIPTOR = "net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener interface,
 * generating a proxy if needed.
 */
public static net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener))) {
return ((net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener)iin);
}
return new net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_onStarted:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.RouteGuidance _arg0;
if ((0!=data.readInt())) {
_arg0 = net.zmap.android.pnd.v2.api.event.RouteGuidance.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.onStarted(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_onStopped:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.RouteGuidance _arg0;
if ((0!=data.readInt())) {
_arg0 = net.zmap.android.pnd.v2.api.event.RouteGuidance.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.onStopped(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_onPaused:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.RouteGuidance _arg0;
if ((0!=data.readInt())) {
_arg0 = net.zmap.android.pnd.v2.api.event.RouteGuidance.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.onPaused(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_onResumed:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.RouteGuidance _arg0;
if ((0!=data.readInt())) {
_arg0 = net.zmap.android.pnd.v2.api.event.RouteGuidance.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.onResumed(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_onNearToCrossroad:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.RouteGuidance _arg0;
if ((0!=data.readInt())) {
_arg0 = net.zmap.android.pnd.v2.api.event.RouteGuidance.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.onNearToCrossroad(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_onNearToWaypoint:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.RouteGuidance _arg0;
if ((0!=data.readInt())) {
_arg0 = net.zmap.android.pnd.v2.api.event.RouteGuidance.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.onNearToWaypoint(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_onNearToDestination:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.RouteGuidance _arg0;
if ((0!=data.readInt())) {
_arg0 = net.zmap.android.pnd.v2.api.event.RouteGuidance.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.onNearToDestination(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_onRerouted:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.RouteGuidance _arg0;
if ((0!=data.readInt())) {
_arg0 = net.zmap.android.pnd.v2.api.event.RouteGuidance.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.onRerouted(_arg0);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
/**
     * 案内を開始したときに呼び出されるリスナ関数
     */
@Override public void onStarted(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((routeGuidance!=null)) {
_data.writeInt(1);
routeGuidance.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_onStarted, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 案内を終了したときに呼び出されるリスナ関数
     */
@Override public void onStopped(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((routeGuidance!=null)) {
_data.writeInt(1);
routeGuidance.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_onStopped, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 案内を一時停止したときに呼び出されるリスナ関数
     */
@Override public void onPaused(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((routeGuidance!=null)) {
_data.writeInt(1);
routeGuidance.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_onPaused, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 一時停止した再開したときに呼び出されるリスナ関数
     */
@Override public void onResumed(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((routeGuidance!=null)) {
_data.writeInt(1);
routeGuidance.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_onResumed, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * ルート案内中、目標案内 (交差点) に接近して、音声案内したときに呼び出されるリスナ関数
     */
@Override public void onNearToCrossroad(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((routeGuidance!=null)) {
_data.writeInt(1);
routeGuidance.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_onNearToCrossroad, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * ルート案内中、経由地に接近して、音声案内したときに呼び出されるリスナ関数
     */
@Override public void onNearToWaypoint(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((routeGuidance!=null)) {
_data.writeInt(1);
routeGuidance.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_onNearToWaypoint, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * ルート案内中、目的地に接近して、音声案内したときに呼び出されるリスナ関数
     */
@Override public void onNearToDestination(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((routeGuidance!=null)) {
_data.writeInt(1);
routeGuidance.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_onNearToDestination, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * ルート案内中、リルート後に案内を開始したときに呼び出されるリスナ関数
     */
@Override public void onRerouted(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((routeGuidance!=null)) {
_data.writeInt(1);
routeGuidance.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_onRerouted, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_onStarted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_onStopped = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_onPaused = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_onResumed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_onNearToCrossroad = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_onNearToWaypoint = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
static final int TRANSACTION_onNearToDestination = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
static final int TRANSACTION_onRerouted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
}
/**
     * 案内を開始したときに呼び出されるリスナ関数
     */
public void onStarted(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException;
/**
     * 案内を終了したときに呼び出されるリスナ関数
     */
public void onStopped(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException;
/**
     * 案内を一時停止したときに呼び出されるリスナ関数
     */
public void onPaused(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException;
/**
     * 一時停止した再開したときに呼び出されるリスナ関数
     */
public void onResumed(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException;
/**
     * ルート案内中、目標案内 (交差点) に接近して、音声案内したときに呼び出されるリスナ関数
     */
public void onNearToCrossroad(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException;
/**
     * ルート案内中、経由地に接近して、音声案内したときに呼び出されるリスナ関数
     */
public void onNearToWaypoint(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException;
/**
     * ルート案内中、目的地に接近して、音声案内したときに呼び出されるリスナ関数
     */
public void onNearToDestination(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException;
/**
     * ルート案内中、リルート後に案内を開始したときに呼び出されるリスナ関数
     */
public void onRerouted(net.zmap.android.pnd.v2.api.event.RouteGuidance routeGuidance) throws android.os.RemoteException;
}
