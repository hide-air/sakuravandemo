/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /Users/Yamamoto/Documents/AndroidProjects/SakuraVanDemo.gles.nojni.Android50/app/src/main/aidl/net/zmap/android/pnd/v2/api/INaviService.aidl
 */
package net.zmap.android.pnd.v2.api;
public interface INaviService extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements net.zmap.android.pnd.v2.api.INaviService
{
private static final java.lang.String DESCRIPTOR = "net.zmap.android.pnd.v2.api.INaviService";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an net.zmap.android.pnd.v2.api.INaviService interface,
 * generating a proxy if needed.
 */
public static net.zmap.android.pnd.v2.api.INaviService asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof net.zmap.android.pnd.v2.api.INaviService))) {
return ((net.zmap.android.pnd.v2.api.INaviService)iin);
}
return new net.zmap.android.pnd.v2.api.INaviService.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION__openMap:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
boolean _arg1;
_arg1 = (0!=data.readInt());
this._openMap(_arg0, _arg1);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_openMap:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
net.zmap.android.pnd.v2.api.GeoPoint _arg1;
if ((0!=data.readInt())) {
_arg1 = net.zmap.android.pnd.v2.api.GeoPoint.CREATOR.createFromParcel(data);
}
else {
_arg1 = null;
}
int _arg2;
_arg2 = data.readInt();
boolean _arg3;
_arg3 = (0!=data.readInt());
this.openMap(_arg0, _arg1, _arg2, _arg3);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_getCenter:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
net.zmap.android.pnd.v2.api.GeoPoint _result = this.getCenter(_arg0);
reply.writeNoException();
if ((_result!=null)) {
reply.writeInt(1);
_result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_setCenter:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
net.zmap.android.pnd.v2.api.GeoPoint _arg1;
if ((0!=data.readInt())) {
_arg1 = net.zmap.android.pnd.v2.api.GeoPoint.CREATOR.createFromParcel(data);
}
else {
_arg1 = null;
}
this.setCenter(_arg0, _arg1);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_getZoom:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
int _result = this.getZoom(_arg0);
reply.writeNoException();
reply.writeInt(_result);
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_setZoom:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
int _arg1;
_arg1 = data.readInt();
this.setZoom(_arg0, _arg1);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_setDayNightMode:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
int _arg1;
_arg1 = data.readInt();
this.setDayNightMode(_arg0, _arg1);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_drawIcon:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
java.lang.String _arg1;
_arg1 = data.readString();
net.zmap.android.pnd.v2.api.overlay.Icon _arg2;
if ((0!=data.readInt())) {
_arg2 = net.zmap.android.pnd.v2.api.overlay.Icon.CREATOR.createFromParcel(data);
}
else {
_arg2 = null;
}
this.drawIcon(_arg0, _arg1, _arg2);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_drawLine:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
java.lang.String _arg1;
_arg1 = data.readString();
net.zmap.android.pnd.v2.api.overlay.Line _arg2;
if ((0!=data.readInt())) {
_arg2 = net.zmap.android.pnd.v2.api.overlay.Line.CREATOR.createFromParcel(data);
}
else {
_arg2 = null;
}
this.drawLine(_arg0, _arg1, _arg2);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_removeLayer:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
java.lang.String _arg1;
_arg1 = data.readString();
this.removeLayer(_arg0, _arg1);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_removeAllLayers:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
this.removeAllLayers(_arg0);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_getRemainingDistance:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
net.zmap.android.pnd.v2.api.navigation.RemainingDistance _result = this.getRemainingDistance(_arg0);
reply.writeNoException();
if ((_result!=null)) {
reply.writeInt(1);
_result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_calculateRoute:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
net.zmap.android.pnd.v2.api.navigation.RouteRequest _arg1;
if ((0!=data.readInt())) {
_arg1 = net.zmap.android.pnd.v2.api.navigation.RouteRequest.CREATOR.createFromParcel(data);
}
else {
_arg1 = null;
}
boolean _arg2;
_arg2 = (0!=data.readInt());
this.calculateRoute(_arg0, _arg1, _arg2);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_startRouteGuidance:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
this.startRouteGuidance(_arg0);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_stopRouteGuidance:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
this.stopRouteGuidance(_arg0);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_addFavorites:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.NaviResult _arg0;
_arg0 = new net.zmap.android.pnd.v2.api.NaviResult();
net.zmap.android.pnd.v2.api.navigation.RoutePoint _arg1;
if ((0!=data.readInt())) {
_arg1 = net.zmap.android.pnd.v2.api.navigation.RoutePoint.CREATOR.createFromParcel(data);
}
else {
_arg1 = null;
}
int _arg2;
_arg2 = data.readInt();
this.addFavorites(_arg0, _arg1, _arg2);
reply.writeNoException();
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_isNaviAppRunning:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.isNaviAppRunning();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_registerVehiclePositionListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IVehiclePositionListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IVehiclePositionListener.Stub.asInterface(data.readStrongBinder());
this.registerVehiclePositionListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_unregisterVehiclePositionListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IVehiclePositionListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IVehiclePositionListener.Stub.asInterface(data.readStrongBinder());
this.unregisterVehiclePositionListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_registerRoadListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IRoadListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IRoadListener.Stub.asInterface(data.readStrongBinder());
this.registerRoadListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_unregisterRoadListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IRoadListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IRoadListener.Stub.asInterface(data.readStrongBinder());
this.unregisterRoadListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_registerAreaListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IAreaListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IAreaListener.Stub.asInterface(data.readStrongBinder());
this.registerAreaListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_unregisterAreaListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IAreaListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IAreaListener.Stub.asInterface(data.readStrongBinder());
this.unregisterAreaListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_registerRouteGuidanceListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener.Stub.asInterface(data.readStrongBinder());
this.registerRouteGuidanceListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_unregisterRouteGuidanceListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener.Stub.asInterface(data.readStrongBinder());
this.unregisterRouteGuidanceListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_registerRouteCalcListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IRouteCalcListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IRouteCalcListener.Stub.asInterface(data.readStrongBinder());
this.registerRouteCalcListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_unregisterRouteCalcListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IRouteCalcListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IRouteCalcListener.Stub.asInterface(data.readStrongBinder());
this.unregisterRouteCalcListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_isStarted:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.isStarted();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_registerApplicationStatusListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IApplicationStatusListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IApplicationStatusListener.Stub.asInterface(data.readStrongBinder());
this.registerApplicationStatusListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_unregisterApplicationStatusListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IApplicationStatusListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IApplicationStatusListener.Stub.asInterface(data.readStrongBinder());
this.unregisterApplicationStatusListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_registerVehiclePositionExListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener.Stub.asInterface(data.readStrongBinder());
this.registerVehiclePositionExListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_unregisterVehiclePositionExListener:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener _arg0;
_arg0 = net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener.Stub.asInterface(data.readStrongBinder());
this.unregisterVehiclePositionExListener(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_requestDrivingControlLock:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
boolean _result = this.requestDrivingControlLock(_arg0);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_requestDrivingControlUnLock:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
boolean _result = this.requestDrivingControlUnLock(_arg0);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_requestDrivingControl:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
boolean _arg1;
_arg1 = (0!=data.readInt());
boolean _result = this.requestDrivingControl(_arg0, _arg1);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements net.zmap.android.pnd.v2.api.INaviService
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
/**
     * 地図を表示
     */
@Override public void _openMap(net.zmap.android.pnd.v2.api.NaviResult res, boolean forced) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(((forced)?(1):(0)));
mRemote.transact(Stub.TRANSACTION__openMap, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 経緯度・縮尺を指定して地図を表示
     */
@Override public void openMap(net.zmap.android.pnd.v2.api.NaviResult res, net.zmap.android.pnd.v2.api.GeoPoint point, int zoomLevel, boolean forced) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((point!=null)) {
_data.writeInt(1);
point.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
_data.writeInt(zoomLevel);
_data.writeInt(((forced)?(1):(0)));
mRemote.transact(Stub.TRANSACTION_openMap, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 地図中心座標の取得
     */
@Override public net.zmap.android.pnd.v2.api.GeoPoint getCenter(net.zmap.android.pnd.v2.api.NaviResult res) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
net.zmap.android.pnd.v2.api.GeoPoint _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getCenter, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
_result = net.zmap.android.pnd.v2.api.GeoPoint.CREATOR.createFromParcel(_reply);
}
else {
_result = null;
}
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * 地図中心座標の設定
     */
@Override public void setCenter(net.zmap.android.pnd.v2.api.NaviResult res, net.zmap.android.pnd.v2.api.GeoPoint point) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((point!=null)) {
_data.writeInt(1);
point.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_setCenter, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 地図縮尺の取得
     */
@Override public int getZoom(net.zmap.android.pnd.v2.api.NaviResult res) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getZoom, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * 地図縮尺の設定
     */
@Override public void setZoom(net.zmap.android.pnd.v2.api.NaviResult res, int zoomLevel) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(zoomLevel);
mRemote.transact(Stub.TRANSACTION_setZoom, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 昼・夜モードの設定
     */
@Override public void setDayNightMode(net.zmap.android.pnd.v2.api.NaviResult res, int mode) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(mode);
mRemote.transact(Stub.TRANSACTION_setDayNightMode, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 指定した地点にユーザーアイコンを描画する.
     * 形状は指定したユーザレイヤに登録される.
     */
@Override public void drawIcon(net.zmap.android.pnd.v2.api.NaviResult res, java.lang.String layerId, net.zmap.android.pnd.v2.api.overlay.Icon icon) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(layerId);
if ((icon!=null)) {
_data.writeInt(1);
icon.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_drawIcon, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 指定した地点にライン形状を描画する.
     * 形状は指定したユーザレイヤに登録される.
     */
@Override public void drawLine(net.zmap.android.pnd.v2.api.NaviResult res, java.lang.String layerId, net.zmap.android.pnd.v2.api.overlay.Line line) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(layerId);
if ((line!=null)) {
_data.writeInt(1);
line.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_drawLine, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 指定したユーザレイヤを消去する.
     */
@Override public void removeLayer(net.zmap.android.pnd.v2.api.NaviResult res, java.lang.String layerId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(layerId);
mRemote.transact(Stub.TRANSACTION_removeLayer, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 全てのユーザレイヤを消去する.
     */
@Override public void removeAllLayers(net.zmap.android.pnd.v2.api.NaviResult res) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_removeAllLayers, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 目的地までの案内情報(残距離、残時間..)を取得する
     */
@Override public net.zmap.android.pnd.v2.api.navigation.RemainingDistance getRemainingDistance(net.zmap.android.pnd.v2.api.NaviResult res) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
net.zmap.android.pnd.v2.api.navigation.RemainingDistance _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getRemainingDistance, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
_result = net.zmap.android.pnd.v2.api.navigation.RemainingDistance.CREATOR.createFromParcel(_reply);
}
else {
_result = null;
}
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * ルート ID を指定して案内を開始する
     */
@Override public void calculateRoute(net.zmap.android.pnd.v2.api.NaviResult res, net.zmap.android.pnd.v2.api.navigation.RouteRequest routeRequest, boolean routeGuidanceStarting) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((routeRequest!=null)) {
_data.writeInt(1);
routeRequest.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
_data.writeInt(((routeGuidanceStarting)?(1):(0)));
mRemote.transact(Stub.TRANSACTION_calculateRoute, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 案内を開始する
     */
@Override public void startRouteGuidance(net.zmap.android.pnd.v2.api.NaviResult res) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_startRouteGuidance, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 案内を終了する
     */
@Override public void stopRouteGuidance(net.zmap.android.pnd.v2.api.NaviResult res) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_stopRouteGuidance, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * お気に入り登録
     */
@Override public void addFavorites(net.zmap.android.pnd.v2.api.NaviResult res, net.zmap.android.pnd.v2.api.navigation.RoutePoint point, int categoryId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((point!=null)) {
_data.writeInt(1);
point.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
_data.writeInt(categoryId);
mRemote.transact(Stub.TRANSACTION_addFavorites, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
res.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 起動確認
     */
@Override public boolean isNaviAppRunning() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_isNaviAppRunning, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * 自車位置更新イベントリスナの登録
     */
@Override public void registerVehiclePositionListener(net.zmap.android.pnd.v2.api.event.IVehiclePositionListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_registerVehiclePositionListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 自車位置更新イベントリスナの解除
     */
@Override public void unregisterVehiclePositionListener(net.zmap.android.pnd.v2.api.event.IVehiclePositionListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_unregisterVehiclePositionListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 道路種別変更イベントリスナの登録
     */
@Override public void registerRoadListener(net.zmap.android.pnd.v2.api.event.IRoadListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_registerRoadListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 道路種別変更イベントリスナの解除
     */
@Override public void unregisterRoadListener(net.zmap.android.pnd.v2.api.event.IRoadListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_unregisterRoadListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 行政界移動イベントリスナの登録
     */
@Override public void registerAreaListener(net.zmap.android.pnd.v2.api.event.IAreaListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_registerAreaListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 行政界移動イベントリスナの解除
     */
@Override public void unregisterAreaListener(net.zmap.android.pnd.v2.api.event.IAreaListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_unregisterAreaListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * ルート案内イベントリスナの登録
     */
@Override public void registerRouteGuidanceListener(net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_registerRouteGuidanceListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * ルート案内イベントリスナの解除
     */
@Override public void unregisterRouteGuidanceListener(net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_unregisterRouteGuidanceListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * ルート探索イベントリスナの登録
     */
@Override public void registerRouteCalcListener(net.zmap.android.pnd.v2.api.event.IRouteCalcListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_registerRouteCalcListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * ルート探索イベントリスナの解除
     */
@Override public void unregisterRouteCalcListener(net.zmap.android.pnd.v2.api.event.IRouteCalcListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_unregisterRouteCalcListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     *
     */
@Override public boolean isStarted() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_isStarted, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * アプリケーションステータスリスナの登録
     */
@Override public void registerApplicationStatusListener(net.zmap.android.pnd.v2.api.event.IApplicationStatusListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_registerApplicationStatusListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * アプリケーションステータスリスナの解除
     */
@Override public void unregisterApplicationStatusListener(net.zmap.android.pnd.v2.api.event.IApplicationStatusListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_unregisterApplicationStatusListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 自車位置更新イベントリスナの登録(拡張版)
     */
@Override public void registerVehiclePositionExListener(net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_registerVehiclePositionExListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * 自車位置更新イベントリスナの解除(拡張版)
     */
@Override public void unregisterVehiclePositionExListener(net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener listener) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_unregisterVehiclePositionExListener, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
/**
     * 走行規制機能の制御許可申請
     */
@Override public boolean requestDrivingControlLock(int nKey) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(nKey);
mRemote.transact(Stub.TRANSACTION_requestDrivingControlLock, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * 走行規制機能の制御解放
     */
@Override public boolean requestDrivingControlUnLock(int nKey) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(nKey);
mRemote.transact(Stub.TRANSACTION_requestDrivingControlUnLock, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * 走行規制指示
     */
@Override public boolean requestDrivingControl(int nKey, boolean bControl) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(nKey);
_data.writeInt(((bControl)?(1):(0)));
mRemote.transact(Stub.TRANSACTION_requestDrivingControl, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
}
static final int TRANSACTION__openMap = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_openMap = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_getCenter = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_setCenter = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_getZoom = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_setZoom = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
static final int TRANSACTION_setDayNightMode = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
static final int TRANSACTION_drawIcon = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
static final int TRANSACTION_drawLine = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
static final int TRANSACTION_removeLayer = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
static final int TRANSACTION_removeAllLayers = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
static final int TRANSACTION_getRemainingDistance = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
static final int TRANSACTION_calculateRoute = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
static final int TRANSACTION_startRouteGuidance = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
static final int TRANSACTION_stopRouteGuidance = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
static final int TRANSACTION_addFavorites = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
static final int TRANSACTION_isNaviAppRunning = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
static final int TRANSACTION_registerVehiclePositionListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
static final int TRANSACTION_unregisterVehiclePositionListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 18);
static final int TRANSACTION_registerRoadListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 19);
static final int TRANSACTION_unregisterRoadListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 20);
static final int TRANSACTION_registerAreaListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 21);
static final int TRANSACTION_unregisterAreaListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 22);
static final int TRANSACTION_registerRouteGuidanceListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 23);
static final int TRANSACTION_unregisterRouteGuidanceListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 24);
static final int TRANSACTION_registerRouteCalcListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 25);
static final int TRANSACTION_unregisterRouteCalcListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 26);
static final int TRANSACTION_isStarted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 27);
static final int TRANSACTION_registerApplicationStatusListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 28);
static final int TRANSACTION_unregisterApplicationStatusListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 29);
static final int TRANSACTION_registerVehiclePositionExListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 30);
static final int TRANSACTION_unregisterVehiclePositionExListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 31);
static final int TRANSACTION_requestDrivingControlLock = (android.os.IBinder.FIRST_CALL_TRANSACTION + 32);
static final int TRANSACTION_requestDrivingControlUnLock = (android.os.IBinder.FIRST_CALL_TRANSACTION + 33);
static final int TRANSACTION_requestDrivingControl = (android.os.IBinder.FIRST_CALL_TRANSACTION + 34);
}
/**
     * 地図を表示
     */
public void _openMap(net.zmap.android.pnd.v2.api.NaviResult res, boolean forced) throws android.os.RemoteException;
/**
     * 経緯度・縮尺を指定して地図を表示
     */
public void openMap(net.zmap.android.pnd.v2.api.NaviResult res, net.zmap.android.pnd.v2.api.GeoPoint point, int zoomLevel, boolean forced) throws android.os.RemoteException;
/**
     * 地図中心座標の取得
     */
public net.zmap.android.pnd.v2.api.GeoPoint getCenter(net.zmap.android.pnd.v2.api.NaviResult res) throws android.os.RemoteException;
/**
     * 地図中心座標の設定
     */
public void setCenter(net.zmap.android.pnd.v2.api.NaviResult res, net.zmap.android.pnd.v2.api.GeoPoint point) throws android.os.RemoteException;
/**
     * 地図縮尺の取得
     */
public int getZoom(net.zmap.android.pnd.v2.api.NaviResult res) throws android.os.RemoteException;
/**
     * 地図縮尺の設定
     */
public void setZoom(net.zmap.android.pnd.v2.api.NaviResult res, int zoomLevel) throws android.os.RemoteException;
/**
     * 昼・夜モードの設定
     */
public void setDayNightMode(net.zmap.android.pnd.v2.api.NaviResult res, int mode) throws android.os.RemoteException;
/**
     * 指定した地点にユーザーアイコンを描画する.
     * 形状は指定したユーザレイヤに登録される.
     */
public void drawIcon(net.zmap.android.pnd.v2.api.NaviResult res, java.lang.String layerId, net.zmap.android.pnd.v2.api.overlay.Icon icon) throws android.os.RemoteException;
/**
     * 指定した地点にライン形状を描画する.
     * 形状は指定したユーザレイヤに登録される.
     */
public void drawLine(net.zmap.android.pnd.v2.api.NaviResult res, java.lang.String layerId, net.zmap.android.pnd.v2.api.overlay.Line line) throws android.os.RemoteException;
/**
     * 指定したユーザレイヤを消去する.
     */
public void removeLayer(net.zmap.android.pnd.v2.api.NaviResult res, java.lang.String layerId) throws android.os.RemoteException;
/**
     * 全てのユーザレイヤを消去する.
     */
public void removeAllLayers(net.zmap.android.pnd.v2.api.NaviResult res) throws android.os.RemoteException;
/**
     * 目的地までの案内情報(残距離、残時間..)を取得する
     */
public net.zmap.android.pnd.v2.api.navigation.RemainingDistance getRemainingDistance(net.zmap.android.pnd.v2.api.NaviResult res) throws android.os.RemoteException;
/**
     * ルート ID を指定して案内を開始する
     */
public void calculateRoute(net.zmap.android.pnd.v2.api.NaviResult res, net.zmap.android.pnd.v2.api.navigation.RouteRequest routeRequest, boolean routeGuidanceStarting) throws android.os.RemoteException;
/**
     * 案内を開始する
     */
public void startRouteGuidance(net.zmap.android.pnd.v2.api.NaviResult res) throws android.os.RemoteException;
/**
     * 案内を終了する
     */
public void stopRouteGuidance(net.zmap.android.pnd.v2.api.NaviResult res) throws android.os.RemoteException;
/**
     * お気に入り登録
     */
public void addFavorites(net.zmap.android.pnd.v2.api.NaviResult res, net.zmap.android.pnd.v2.api.navigation.RoutePoint point, int categoryId) throws android.os.RemoteException;
/**
     * 起動確認
     */
public boolean isNaviAppRunning() throws android.os.RemoteException;
/**
     * 自車位置更新イベントリスナの登録
     */
public void registerVehiclePositionListener(net.zmap.android.pnd.v2.api.event.IVehiclePositionListener listener) throws android.os.RemoteException;
/**
     * 自車位置更新イベントリスナの解除
     */
public void unregisterVehiclePositionListener(net.zmap.android.pnd.v2.api.event.IVehiclePositionListener listener) throws android.os.RemoteException;
/**
     * 道路種別変更イベントリスナの登録
     */
public void registerRoadListener(net.zmap.android.pnd.v2.api.event.IRoadListener listener) throws android.os.RemoteException;
/**
     * 道路種別変更イベントリスナの解除
     */
public void unregisterRoadListener(net.zmap.android.pnd.v2.api.event.IRoadListener listener) throws android.os.RemoteException;
/**
     * 行政界移動イベントリスナの登録
     */
public void registerAreaListener(net.zmap.android.pnd.v2.api.event.IAreaListener listener) throws android.os.RemoteException;
/**
     * 行政界移動イベントリスナの解除
     */
public void unregisterAreaListener(net.zmap.android.pnd.v2.api.event.IAreaListener listener) throws android.os.RemoteException;
/**
     * ルート案内イベントリスナの登録
     */
public void registerRouteGuidanceListener(net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener listener) throws android.os.RemoteException;
/**
     * ルート案内イベントリスナの解除
     */
public void unregisterRouteGuidanceListener(net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener listener) throws android.os.RemoteException;
/**
     * ルート探索イベントリスナの登録
     */
public void registerRouteCalcListener(net.zmap.android.pnd.v2.api.event.IRouteCalcListener listener) throws android.os.RemoteException;
/**
     * ルート探索イベントリスナの解除
     */
public void unregisterRouteCalcListener(net.zmap.android.pnd.v2.api.event.IRouteCalcListener listener) throws android.os.RemoteException;
/**
     *
     */
public boolean isStarted() throws android.os.RemoteException;
/**
     * アプリケーションステータスリスナの登録
     */
public void registerApplicationStatusListener(net.zmap.android.pnd.v2.api.event.IApplicationStatusListener listener) throws android.os.RemoteException;
/**
     * アプリケーションステータスリスナの解除
     */
public void unregisterApplicationStatusListener(net.zmap.android.pnd.v2.api.event.IApplicationStatusListener listener) throws android.os.RemoteException;
/**
     * 自車位置更新イベントリスナの登録(拡張版)
     */
public void registerVehiclePositionExListener(net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener listener) throws android.os.RemoteException;
/**
     * 自車位置更新イベントリスナの解除(拡張版)
     */
public void unregisterVehiclePositionExListener(net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener listener) throws android.os.RemoteException;
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
/**
     * 走行規制機能の制御許可申請
     */
public boolean requestDrivingControlLock(int nKey) throws android.os.RemoteException;
/**
     * 走行規制機能の制御解放
     */
public boolean requestDrivingControlUnLock(int nKey) throws android.os.RemoteException;
/**
     * 走行規制指示
     */
public boolean requestDrivingControl(int nKey, boolean bControl) throws android.os.RemoteException;
}
