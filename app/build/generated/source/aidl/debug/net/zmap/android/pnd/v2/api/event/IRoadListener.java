/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /Users/Yamamoto/Documents/AndroidProjects/SakuraVanDemo.gles.nojni.Android50/app/src/main/aidl/net/zmap/android/pnd/v2/api/event/IRoadListener.aidl
 */
package net.zmap.android.pnd.v2.api.event;
/**
 * 道路種別変更リスナインタフェース (aidl)
 */
public interface IRoadListener extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements net.zmap.android.pnd.v2.api.event.IRoadListener
{
private static final java.lang.String DESCRIPTOR = "net.zmap.android.pnd.v2.api.event.IRoadListener";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an net.zmap.android.pnd.v2.api.event.IRoadListener interface,
 * generating a proxy if needed.
 */
public static net.zmap.android.pnd.v2.api.event.IRoadListener asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof net.zmap.android.pnd.v2.api.event.IRoadListener))) {
return ((net.zmap.android.pnd.v2.api.event.IRoadListener)iin);
}
return new net.zmap.android.pnd.v2.api.event.IRoadListener.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_onChanged:
{
data.enforceInterface(DESCRIPTOR);
android.location.Location _arg0;
if ((0!=data.readInt())) {
_arg0 = android.location.Location.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
net.zmap.android.pnd.v2.api.event.Road _arg1;
if ((0!=data.readInt())) {
_arg1 = net.zmap.android.pnd.v2.api.event.Road.CREATOR.createFromParcel(data);
}
else {
_arg1 = null;
}
net.zmap.android.pnd.v2.api.event.Road _arg2;
if ((0!=data.readInt())) {
_arg2 = net.zmap.android.pnd.v2.api.event.Road.CREATOR.createFromParcel(data);
}
else {
_arg2 = null;
}
this.onChanged(_arg0, _arg1, _arg2);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements net.zmap.android.pnd.v2.api.event.IRoadListener
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
/**
     * 道路種別が変化したときに呼ばれるリスナ関数 (車モードのみ)
     */
@Override public void onChanged(android.location.Location location, net.zmap.android.pnd.v2.api.event.Road from, net.zmap.android.pnd.v2.api.event.Road to) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((location!=null)) {
_data.writeInt(1);
location.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
if ((from!=null)) {
_data.writeInt(1);
from.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
if ((to!=null)) {
_data.writeInt(1);
to.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_onChanged, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_onChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
}
/**
     * 道路種別が変化したときに呼ばれるリスナ関数 (車モードのみ)
     */
public void onChanged(android.location.Location location, net.zmap.android.pnd.v2.api.event.Road from, net.zmap.android.pnd.v2.api.event.Road to) throws android.os.RemoteException;
}
