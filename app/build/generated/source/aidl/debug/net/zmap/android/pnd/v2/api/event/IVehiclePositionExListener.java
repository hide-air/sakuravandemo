/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /Users/Yamamoto/Documents/AndroidProjects/SakuraVanDemo.gles.nojni.Android50/app/src/main/aidl/net/zmap/android/pnd/v2/api/event/IVehiclePositionExListener.aidl
 */
package net.zmap.android.pnd.v2.api.event;
/**
 * 自車位置更新リスナインタフェース (aidl)
 */
public interface IVehiclePositionExListener extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener
{
private static final java.lang.String DESCRIPTOR = "net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener interface,
 * generating a proxy if needed.
 */
public static net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener))) {
return ((net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener)iin);
}
return new net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_onChanged:
{
data.enforceInterface(DESCRIPTOR);
net.zmap.android.pnd.v2.api.event.VehiclePositionEx _arg0;
if ((0!=data.readInt())) {
_arg0 = net.zmap.android.pnd.v2.api.event.VehiclePositionEx.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.onChanged(_arg0);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
/**
     * 自車位置が更新されたときに呼び出されるリスナ (自車位置情報拡張版)
     */
@Override public void onChanged(net.zmap.android.pnd.v2.api.event.VehiclePositionEx vehiclePositionEx) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((vehiclePositionEx!=null)) {
_data.writeInt(1);
vehiclePositionEx.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_onChanged, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_onChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
}
/**
     * 自車位置が更新されたときに呼び出されるリスナ (自車位置情報拡張版)
     */
public void onChanged(net.zmap.android.pnd.v2.api.event.VehiclePositionEx vehiclePositionEx) throws android.os.RemoteException;
}
