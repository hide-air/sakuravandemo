/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /Users/Yamamoto/Documents/AndroidProjects/SakuraVanDemo.gles.nojni.Android50/app/src/main/aidl/net/zmap/android/pnd/v2/api/event/IApplicationStatusListener.aidl
 */
package net.zmap.android.pnd.v2.api.event;
public interface IApplicationStatusListener extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements net.zmap.android.pnd.v2.api.event.IApplicationStatusListener
{
private static final java.lang.String DESCRIPTOR = "net.zmap.android.pnd.v2.api.event.IApplicationStatusListener";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an net.zmap.android.pnd.v2.api.event.IApplicationStatusListener interface,
 * generating a proxy if needed.
 */
public static net.zmap.android.pnd.v2.api.event.IApplicationStatusListener asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof net.zmap.android.pnd.v2.api.event.IApplicationStatusListener))) {
return ((net.zmap.android.pnd.v2.api.event.IApplicationStatusListener)iin);
}
return new net.zmap.android.pnd.v2.api.event.IApplicationStatusListener.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_onStarted:
{
data.enforceInterface(DESCRIPTOR);
this.onStarted();
reply.writeNoException();
return true;
}
case TRANSACTION_onStopped:
{
data.enforceInterface(DESCRIPTOR);
this.onStopped();
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements net.zmap.android.pnd.v2.api.event.IApplicationStatusListener
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void onStarted() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_onStarted, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void onStopped() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_onStopped, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_onStarted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_onStopped = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
}
public void onStarted() throws android.os.RemoteException;
public void onStopped() throws android.os.RemoteException;
}
