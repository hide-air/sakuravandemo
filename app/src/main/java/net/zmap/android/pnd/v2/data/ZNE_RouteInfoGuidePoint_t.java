/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNE_RouteInfoGuidePoint_t.java
 * Description    ルート案内情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNE_RouteInfoGuidePoint_t
{
    private String          szPointName;
    private JNITwoLong      stPos ;
    /**
    * Created on 2010/08/06
    * Title:       getPointName
    * Description:   目標名称を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getPointName()
    {
        return szPointName;
    }
    /**
    * Created on 2010/08/06
    * Title:       getstPos
    * Description:   目標座標を取得する
    * @param1  無し
    * @return       JNITwoLong

    * @version        1.0
    */
    public JNITwoLong getstPos()
    {
        return stPos;
    }
}
