package net.zmap.android.pnd.v2.common.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.addition.activity.AdditionalMenu;
import net.zmap.android.pnd.v2.app.NaviAppStatus;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.utils.DbHelper;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.inquiry.activity.AroundSearchMainMenuActivity;
import net.zmap.android.pnd.v2.inquiry.activity.FavoritesGenreMenuInquiry;
import net.zmap.android.pnd.v2.inquiry.activity.HistoryInquiry;
import net.zmap.android.pnd.v2.inquiry.activity.SearchMainMenuActivity;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.route.activity.RouteEditMain;
import net.zmap.android.pnd.v2.sakuracust.CommonInquiryOps;
import net.zmap.android.pnd.v2.sakuracust.DataCtrlException;
import net.zmap.android.pnd.v2.sakuracust.DataCtrlFacade;
import net.zmap.android.pnd.v2.sakuracust.MatchingModeMenu;
import net.zmap.android.pnd.v2.sakuracust.DrawTrackActivity;

import java.text.MessageFormat;

/**
 * MainMenu
 *
 * @author wangdong
 *
 */
public class MainMenu extends InquiryBaseLoading {

	public DrawTrackActivity DTActivity;
	private int menu_show_type = Constants.MENU_NORMAL;

//Add 2012/03/19 Z01t_nakajima Start(No.320) -->ref 2499
    private int menu_naviMode = CommonLib.getNaviMode();
//Add 2012/03/19 Z01t_nakajima End(No.320) <-- ref 2499
//Chg 2011/09/15 Z01_h_yamada Start -->
//    //案内中ﾒﾆｭｰ
//    private int[] menu_navi_res;
//    //総合ﾒﾆｭｰ
//    private int[] menu_res;
//
//    private int[] menu_id;
//--------------------------------------------

    //案内中ﾒﾆｭｰ
    private final int[] menu_navi_res = new int[] {
            //Chg 2011/10/05 Z01_h_yamada Start -->
//    		R.string.mainmenu_navi_walk,           //ここから歩く/ここから車
//            R.string.mainmenu_navi_route,          //ルート
//            R.string.mainmenu_navi_surround_route, //ルート沿い周辺
//            R.string.mainmenu_navi_dest,           //目的地
//            R.string.mainmenu_navi_end,            //案内を終了
//            R.string.mainmenu_navi_surround,       //周辺
//            R.string.mainmenu_navi_other           //その他
//--------------------------------------------
            R.string.mainmenu_navi_dest, //目的地を再設定
            0,
            R.string.mainmenu_navi_route, //ルート編集
            R.string.mainmenu_navi_surround, //周辺から再設定
            R.string.mainmenu_navi_surround_route, //ルート沿いの施設を検索
            0,
            R.string.mainmenu_navi_end, //案内を終了
            R.string.mainmenu_navi_walk, //ここから歩く/ここから車
            R.string.mainmenu_navi_other //その他
//Chg 2011/10/05 Z01_h_yamada End <--
    };

    //総合ﾒﾆｭｰ
    private final int[] menu_res = new int[] {
            R.string.mainmenu_def_fav_edit, //お気に入り編集
            R.string.mainmenu_def_matchingmode,
            R.string.mainmenu_def_route, //ルート
            R.string.mainmenu_def_history, //履歴編集
            R.string.mainmenu_def_setting, //設定/情報
            R.string.mainmenu_def_additional,
            R.string.mainmenu_def_exit, //ItsmoNaviを終了
            R.string.mainmenu_def_koko_mail, //いまココメール
            R.string.mainmenu_def_mode, //モード選択
    };

    private final int[] menu_id = new int[] {
            R.id.mainmenu_1,
            R.id.mainmenu_mat,
            R.id.mainmenu_2,
            R.id.mainmenu_3,
            R.id.mainmenu_5,
            R.id.mainmenu_add,
            R.id.mainmenu_6,
            R.id.mainmenu_8,
            R.id.mainmenu_9,
    };
//Chg 2011/09/15 Z01_h_yamada End <--

    private Button[] menu_button;

    private int MENU_SIZE;

//Chg 2011/09/13 Z01_h_yamada Start -->
//    private TableLayout layout;
//--------------------------------------------
    private LinearLayout layout;

//Chg 2011/09/13 Z01_h_yamada End <--

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//Del 2011/09/15 Z01_h_yamada Start -->
//        initRes();
//Del 2011/09/15 Z01_h_yamada End <--

        MENU_SIZE = menu_id.length;
        menu_button = new Button[MENU_SIZE];

        Intent oIntent = getIntent();
        if (oIntent != null) {
            menu_show_type = oIntent.getIntExtra(Constants.FLAG_MENU_TYPE, Constants.MENU_NORMAL);
        }

//Del 2011/09/17 Z01_h_yamada Start -->
//        setMenuTitle(R.drawable.mainmenu);
//Del 2011/09/17 Z01_h_yamada End <--

        LayoutInflater inflater = LayoutInflater.from(this);

//Chg 2011/09/13 Z01_h_yamada Start -->
//      layout = (TableLayout) inflater.inflate(R.layout.main_menu, null);
//--------------------------------------------
        layout = (LinearLayout)inflater.inflate(R.layout.main_menu, null);
//Chg 2011/09/13 Z01_h_yamada End <--

        setViewInWorkArea(layout);

        updateView();
    }

//Del 2011/09/15 Z01_h_yamada Start -->
//
//    private void initRes(){
//        menu_navi_res = new int[]{
//                R.drawable.menu_btn_navi_walk,         //ここから歩く/ここから車
//                R.drawable.btn_route,                  //ルート
//                R.drawable.menu_btn_surround_route,    //ルート沿い周辺
//                -1,                     //Reserved
//                R.drawable.menu_btn_dest,              //目的地
//                R.drawable.menu_btn_navi_over,         //案内を終了
//                -1,                                    //Reserved
//                R.drawable.menu_btn_surround,          //周辺
//                R.drawable.menu_btn_other              //その他
//            };
//
//        menu_res = new int[]{
//                R.drawable.btn_fav_edit,                //お気に入り編集
//                R.drawable.btn_route,                   //ルート
//                R.drawable.btn_history,                 //履歴編集
//                -1,                                     //Reserved
//                R.drawable.btn_setting,                 //設定/情報
//                R.drawable.btn_exit,                    //Eclipseを終了
//                -1,                                     //Reserved
//                R.drawable.btn_koko_mail,               //いまココメール
//                R.drawable.btn_mode                     //モード選択
//            };
//
//        menu_id = new int[]{
//                R.id.mainmenu_1,
//                R.id.mainmenu_2,
//                R.id.mainmenu_3,
//                R.id.mainmenu_4,
//                R.id.mainmenu_5,
//                R.id.mainmenu_6,
//                R.id.mainmenu_7,
//                R.id.mainmenu_8,
//                R.id.mainmenu_9,
//        };
//
//    }
//Del 2011/09/15 Z01_h_yamada End <--

    private void updateView() {

        final int INVALID_RES = -1;

        int naviMode = CommonLib.getNaviMode();

        for (int i = 0; i < MENU_SIZE; i++) {
            Button oButton = menu_button[i];
            if (oButton == null) {
                oButton = (Button)layout.findViewById(menu_id[i]);
                menu_button[i] = oButton;
            }

            if (oButton != null) {
                int resId = menu_res[i];
                if (menu_show_type == Constants.MENU_NAVI) {
//Chg 2011/09/26 Z01yoneya Start -->
//                    resId = menu_navi_res[i];
//------------------------------------------------------------------------------------------------------------------------------------
                    try {
                        resId = menu_navi_res[i];
                        if (resId == 0) resId = INVALID_RES;
                    } catch (ArrayIndexOutOfBoundsException e) {
                        resId = INVALID_RES;
                    }
//Chg 2011/09/26 Z01yoneya End <--

                    if (menu_id[i] == R.id.mainmenu_8) {
                        if (naviMode == Constants.NE_NAVIMODE_CAR) {
//Chg 2011/09/13 Z01_h_yamada Start -->
//							resId = R.drawable.menu_btn_navi_walk;
//--------------------------------------------
//Chg 2011/09/26 Z01yoneya Start -->
//                            if (CommonLib.isBIsExistsWalkerData()) {
//------------------------------------------------------------------------------------------------------------------------------------
                            boolean isExistWalkData;
                            try {
                                isExistWalkData = ((NaviApplication)getApplication()).getNaviAppDataPath().isExistWalkData();
                            } catch (NullPointerException e) {
                                isExistWalkData = false;
                            }
                            if (isExistWalkData) {
//Chg 2011/09/26 Z01yoneya End <--
                                resId = R.string.mainmenu_navi_walk;
                            } else {
                                //歩行者データが無い時は「ここから歩く」非表示
                                resId = INVALID_RES;
                            }

//Chg 2011/09/13 Z01_h_yamada End <--
                        }
                        if (naviMode == Constants.NE_NAVIMODE_MAN) {
//Chg 2011/09/13 Z01_h_yamada Start -->
//							resId = R.drawable.menu_btn_navi_car;
//--------------------------------------------
                            resId = R.string.mainmenu_navi_car;
//Chg 2011/09/13 Z01_h_yamada End <--
                        }
                    }
                }
//Chg 2011/09/13 Z01_h_yamada Start -->
//                if(resId != -1){
//                    oButton.setBackgroundResource(resId);
//                }
////Add 2011/09/02 Z01yoneya Start -->
//                //歩行者データが無い時は「ここから歩く」非表示
//                if(resId == R.drawable.menu_btn_navi_walk && !CommonLib.isBIsExistsWalkerData()){
//                    oButton.setVisibility(View.INVISIBLE);
//                }
////Add 2011/09/02 Z01yoneya End <--
//                final int index = i;
//                oButton.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        clickItem(index);
//                    }
//                });
//--------------------------------------------
                if (resId == INVALID_RES) {
//Chg 2011/09/26 Z01yoneya Start -->
//                    oButton.setVisibility(View.INVISIBLE);
//------------------------------------------------------------------------------------------------------------------------------------
                    oButton.setVisibility(View.GONE);
//Chg 2011/09/26 Z01yoneya End <--
                } else {
                    oButton.setText(getResources().getString(resId));

                    final int resBtnId = menu_id[i];
                    oButton.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            clickItem(resBtnId);
                        }
                    });
                }
//Chg 2011/09/13 Z01_h_yamada End <--

            }

        }
//Add 2012/03/19 Z01t_nakajima Start(No.320) --> ref 2499
        menu_naviMode = naviMode;
//Add 2012/03/19 Z01t_nakajima End(No.320) <-- ref 2499
        onNaviModeChanged();
        onUserModeChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();

        int menu_type = menu_show_type;
        if (!CommonLib.isNaviStart()) {
            menu_show_type = Constants.MENU_NORMAL;
        }
//Chg 2012/03/19 Z01t_nakajima Start(No.320) --> ref 2499
       // if (menu_type != menu_show_type) {
        if ((menu_type != menu_show_type) ||
            ((menu_show_type == Constants.MENU_NAVI) && (menu_naviMode != CommonLib.getNaviMode()))
            ) {
//Chg 2012/03/19 Z01t_nakajima End(No.320) <-- ref 2499
            updateView();
        }

        onNaviModeChanged();
        onUserModeChanged();
    }

    private void onNaviModeChanged() {
        if (menu_button[1] != null) {
            if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_BICYCLE) {
                menu_button[1].setEnabled(false);
            } else {
                menu_button[1].setEnabled(true);
            }
        }
    }

    private void onUserModeChanged() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (menu_button != null) {
            for (int i = 0, cnt = menu_button.length; i < cnt; i++) {
                menu_button[i] = null;
            }
        }
        layout = null;
//Del 2011/09/15 Z01_h_yamada Start -->
//        menu_id = null;
//        menu_res = null;
//        menu_navi_res = null;
//Del 2011/09/15 Z01_h_yamada End <--
    }

    /**
     * Menu click event.
     *
     * @param index
     *            from 0 to 8.(index 6 is empty)
     */
    private void clickItem(int resId) {
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"menu click index=" + index);
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 メニュー画面 Start -->
    	// 走行規制中の場合「終了する」以外は許可しない
    	if(resId != R.id.mainmenu_6)
    	{
    		if(DrivingRegulation.CheckDrivingRegulation()){
    			return;
    		}
    	}
// ADD 2013.08.08 M.Honma 走行規制 メニュー画面 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

    	switch (resId) {
            case R.id.mainmenu_1:
                // T-T1_総合ﾒﾆｭｰ
                if (menu_show_type == Constants.MENU_NORMAL) {
                    //yangyang add start 20101214
                    Intent oIntent = new Intent();
                    oIntent.setClass(this, FavoritesGenreMenuInquiry.class);
                    NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
//Chg 2011/09/17 Z01_h_yamada Start -->
//        		oIntent.putExtra(Constants.PARAMS_TITLE, R.drawable.menu_history_mod);
//--------------------------------------------
                    oIntent.putExtra(Constants.PARAMS_FAVORITES_EDIT, true);
//Chg 2011/09/17 Z01_h_yamada End <--
//Chg 2011/09/22 Z01yoneya Start -->
//            	this.startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_FAVORITESGENREMENUINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
                    NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_FAVORITESGENREMENUINQUIRY);
//Chg 2011/09/22 Z01yoneya End <--
                    //yangyang add end 20101214
                }
                // T-T2_案内中ﾒﾆｭｰ
//Chg 2011/10/05 Z01_h_yamada Start -->
//            if (menu_show_type == Constants.MENU_NAVI) {
//                //TODO
////                if(CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN){
////                    CommonLib.setNaviMode(Constants.NE_NAVIMODE_BICYCLE);
////                }
//                //for reroute
////                if(CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN){
////                  CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
////                }else{
////                    CommonLib.setNaviMode(Constants.NE_NAVIMODE_MAN);
////                }
//            	CommonLib.setIsStartCalculateRoute(true);
//                setResult(Constants.RESULT_REROUTE);
//                finish();
//            }
//--------------------------------------------
                if (menu_show_type == Constants.MENU_NAVI) {
                    Intent oIntent = new Intent();
                    oIntent.putExtra(Constants.FROM_FLAG_KEY, AppInfo.ID_ACTIVITY_MAINMENU);
                    oIntent.setClass(this, SearchMainMenuActivity.class);
                    NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_MAINMENU);
                }
//Chg 2011/10/05 Z01_h_yamada End <--
                break;
            case R.id.mainmenu_2:
                // ルート編集画面を遷移する
                Intent intentRoute = new Intent();
                // #875 XuYang add start
//    		intentRoute.putExtra(Constants.AddressString, (String) getIntent().getExtras().get(Constants.AddressString));
                // #875 XuYang add end
                intentRoute.setClass(this, RouteEditMain.class);
//Chg 2011/09/22 Z01yoneya Start -->
//    		this.startActivityForResult(intentRoute, AppInfo.ID_ACTIVITY_ROUTE);
//------------------------------------------------------------------------------------------------------------------------------------
                NaviActivityStarter.startActivityForResult(this, intentRoute, AppInfo.ID_ACTIVITY_ROUTE);
//Chg 2011/09/22 Z01yoneya End <--
                break;
            case R.id.mainmenu_3:
                if (menu_show_type == Constants.MENU_NORMAL) {
                    Intent hisIntent = new Intent(getIntent());

//Chg 2011/09/17 Z01_h_yamada Start -->
//    		hisIntent.putExtra(Constants.PARAMS_TITLE, R.drawable.menu_history_mod);
//--------------------------------------------
                    hisIntent.putExtra(Constants.PARAMS_FAVORITES_EDIT, true);
//Chg 2011/09/17 Z01_h_yamada End <--
                    hisIntent.setClass(this, HistoryInquiry.class);
                    hisIntent.putExtra(Constants.PARAMS_HISTORY_FROM_MAINMENU, Constants.PARAMS_HISTORY_FROM_MAINMENU);
//Chg 2011/09/22 Z01yoneya Start -->
//    	    startActivityForResult(hisIntent, AppInfo.ID_ACITIVITY_HISTORYINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
                    NaviActivityStarter.startActivityForResult(this, hisIntent, AppInfo.ID_ACITIVITY_HISTORYINQUIRY);
//Chg 2011/09/22 Z01yoneya End <--
                }

//Chg 2011/10/05 Z01_h_yamada Start -->
//        	if (menu_show_type == Constants.MENU_NAVI) {
//        		// 沿いルート周辺
//				// XuYang add start
//		        Intent oIntent = new Intent();
//		        CommonLib.setAroundFlg(true);
//		        oIntent.setClass(this, AroundSearchMainMenuActivity.class);
//		        oIntent.putExtra(Constants.FROM_FLAG_KEY, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
////Chg 2011/09/22 Z01yoneya Start -->
////		        startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
////------------------------------------------------------------------------------------------------------------------------------------
//		        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
////Chg 2011/09/22 Z01yoneya End <--
//		        // XuYang add end
//        	}
//--------------------------------------------
                if (menu_show_type == Constants.MENU_NAVI) {
                    Intent oIntent = new Intent();
                    CommonLib.setAroundFlg(false);
                    oIntent.setClass(this, AroundSearchMainMenuActivity.class);
                    oIntent.putExtra(Constants.FROM_FLAG_KEY, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
                    JNITwoLong myPosi = new JNITwoLong();
                    NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(myPosi);
                    CommonLib.setMapCenterInfo(myPosi);
                    NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
                }
//Chg 2011/10/05 Z01_h_yamada End <--
                break;
            case R.id.mainmenu_5:
                if (menu_show_type == Constants.MENU_NORMAL) {

                    // 設定/情報画面を遷移する
                    Intent intent = new Intent();
                    intent.setClass(this, SettingMain.class);
//Chg 2011/09/22 Z01yoneya Start -->
//                this.startActivityForResult(intent, AppInfo.ID_ACTIVITY_SETTING);
//------------------------------------------------------------------------------------------------------------------------------------
                    NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_SETTING);
//Chg 2011/09/22 Z01yoneya End <--
                }
//Chg 2011/10/05 Z01_h_yamada Start -->
//            if (menu_show_type == Constants.MENU_NAVI) {
//                Intent oIntent = new Intent();
//            //Add 2011/08/15 Z01kubo Start-->
//			//Redmine #1331, #1340, #1342
//                oIntent.putExtra(Constants.FROM_FLAG_KEY, AppInfo.ID_ACTIVITY_MAINMENU);
//            //Add 2011/08/15 Z01kubo End-->
//                oIntent.setClass(this, SearchMainMenuActivity.class);
////Chg 2011/09/22 Z01yoneya Start -->
////                startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_MAINMENU);
////------------------------------------------------------------------------------------------------------------------------------------
//                NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_MAINMENU);
////Chg 2011/09/22 Z01yoneya End <--
//            }
//--------------------------------------------
                if (menu_show_type == Constants.MENU_NAVI) {
                    // 沿いルート周辺
                    Intent oIntent = new Intent();
                    CommonLib.setAroundFlg(true);
                    oIntent.setClass(this, AroundSearchMainMenuActivity.class);
                    oIntent.putExtra(Constants.FROM_FLAG_KEY, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
                    NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
                }
//Chg 2011/10/05 Z01_h_yamada End <--
                break;
            case R.id.mainmenu_6:
                // Eclipse 終了
                if (menu_show_type == Constants.MENU_NORMAL) {
                    showDialog(Constants.DIALOG_EXIT_APP);
                }
                // 案内終了
                if (menu_show_type == Constants.MENU_NAVI) {
                    showDialog(Constants.DIALOG_EXIT_NAVIGATION);
                }
                break;
            case R.id.mainmenu_8:
                if (menu_show_type == Constants.MENU_NORMAL) {

                    // いまココメール
                    this.startShowPage(NOTSHOWCANCELBTN);

                }
//Chg 2011/10/05 Z01_h_yamada Start -->
//			if (menu_show_type == Constants.MENU_NAVI) {
//				// XuYang add start
//		        Intent oIntent = new Intent();
//		        CommonLib.setAroundFlg(false);
//		        oIntent.setClass(this, AroundSearchMainMenuActivity.class);
//		        oIntent.putExtra(Constants.FROM_FLAG_KEY, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
////Chg 2011/09/22 Z01yoneya Start -->
////		        startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
////------------------------------------------------------------------------------------------------------------------------------------
//// Add 2011/09/27 katsuta Start --> 課題No.177
//		        JNITwoLong myPosi = new JNITwoLong();
//		        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(myPosi);
//		        CommonLib.setMapCenterInfo(myPosi);
//// Add 2011/09/27 katsuta End <--
//		        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
////Chg 2011/09/22 Z01yoneya End <--
//		        // XuYang add endsyuu
//        	}
//--------------------------------------------
                if (menu_show_type == Constants.MENU_NAVI) {
                    CommonLib.setIsStartCalculateRoute(true);
                    setResult(Constants.RESULT_REROUTE);
                    finish();
                }
//Chg 2011/10/05 Z01_h_yamada End <--
                break;

            case R.id.mainmenu_9:
                // モード選択
                if (menu_show_type == Constants.MENU_NORMAL) {
                    Intent intent = new Intent();
                    intent.setClass(this, NaviMode.class);
//Chg 2011/09/22 Z01yoneya Start -->
//                startActivityForResult(intent, AppInfo.ID_ACTIVITY_NAVI_MODE_CHANGE);
//------------------------------------------------------------------------------------------------------------------------------------
                    NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_NAVI_MODE_CHANGE);
//Chg 2011/09/22 Z01yoneya End <--
                }
                // その他
                if (menu_show_type == Constants.MENU_NAVI) {
                    Intent intent = new Intent();
                    intent.setClass(this, MainMenu.class);
                    // #875 XuYang add start
//                intent.putExtra(Constants.AddressString, (String) getIntent().getExtras().get(Constants.AddressString));
                    // #875 XuYang add end
                    intent.putExtra(Constants.FLAG_MENU_TYPE, Constants.MENU_NORMAL);
//Chg 2011/09/22 Z01yoneya Start -->
//                startActivityForResult(intent, AppInfo.ID_ACTIVITY_MENU_NORMAL);
//------------------------------------------------------------------------------------------------------------------------------------
                    NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_MENU_NORMAL);
//Chg 2011/09/22 Z01yoneya End <--
                }
                break;
            case R.id.mainmenu_add:
            {
            	// 追加画面へ遷移する
                Intent intentAdd = new Intent();
                intentAdd.setClass(this, AdditionalMenu.class);
                NaviActivityStarter.startActivityForResult(this, intentAdd, AppInfo.ID_ACTIVITY_DRICON_IMPORTCSV);
            }
                break;
            case R.id.mainmenu_mat:
            {
            	// マッチングモード画面に遷移する
                Intent intentAdd = new Intent();
                intentAdd.setClass(this, MatchingModeMenu.class);
                NaviActivityStarter.startActivityForResult(this, intentAdd, AppInfo.ID_ACTIVITY_DRICON_MATCHCONF);
            }
            	break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Toast toast = Toast.makeText(this, null, Toast.LENGTH_SHORT);
        switch (requestCode) {
            case AppInfo.ID_ACTIVITY_MAIL:
                // いまここメール
                if (resultCode == android.app.Activity.RESULT_OK) {
                    // いまここメール起動成功
                } else if (resultCode == RESULT_CANCELED) {
                    // いまここメール起動失敗
                    this.showDialog(Constants.DIALOG_NOW_MAIL_FAIL);
                }
                break;
            case AppInfo.ID_ACTIVITY_ROUTE:
                if (resultCode == Constants.RESULTCODE_ROUTE_DETERMINATION) {
                    this.setResult(resultCode);
                    this.finish();
                }
                break;
            //yangyang add start 20101215
            case AppInfo.ID_ACTIVITY_FAVORITESGENREMENUINQUIRY:
                if (resultCode == Activity.RESULT_OK) {
                    this.setResult(resultCode);
                    this.finish();
                }
                break;
            //yangyang add start 20101215
            case AppInfo.ID_ACTIVITY_MENU_NORMAL:
                if (!CommonLib.isNaviStart()) {
                    this.setResult(resultCode);
                    finish();
                }
            case AppInfo.ID_ACTIVITY_DRICON_IMPORTCSV:
            	// CSVのインポートを開始する。
            	//try
            	{
            		Dialog wd;
            		switch (resultCode)
            		{
            		case AppInfo.ID_ACTIVITY_DRICON_IMPORTCSV:
            			wd = this.createDialog(Constants.DIALOG_WAIT, -1);
	            		//DataCtrlFacade.ExecuteImportCSV(DbHelper.INPORTCSV_NAME, DbHelper.DATABASE_NAME);
            			DataCtrlFacade.ExecuteImportCSVOnAnotherThread(wd);
	            		break;
            		case AppInfo.ID_ACTIVITY_DRICON_EXPORTCSV:
            			wd = this.createDialog(Constants.DIALOG_WAIT, -1);
            			//DataCtrlFacade.ExecuteExportCSV(DbHelper.EXPORTCSV_NAME, DbHelper.DATABASE_NAME);
            			DataCtrlFacade.ExecuteExportCSVOnAnotherThread(wd);
            			break;
            		}
            	}
/*            	
            	catch (DataCtrlException e)
            	{
            		e.printStackTrace();
            	}
*/            	
            	break;
            case AppInfo.ID_ACTIVITY_DRICON_MATCHCONF:
            	// resultCodeをそのままマッチングモードで使用する。
            	// AppInfo.ID_ACTIVITY_DRICON_MATCHFROMMAP → 常に地図から
            	// AppInfo.ID_ACTIVITY_DRICON_MATCHFROMADR → 常に住所から
            	// AppInfo.ID_ACTIVITY_DRICON_MATCHCONF    → 都度確認する
            	
            	// resultCodeに関わらず兎に角結果をセットする。
            	if (resultCode != RESULT_CANCELED)
            	{
	                this.setResult(resultCode);
	                finish();
            	}
            	break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        final CustomDialog oDialog = new CustomDialog(this);
        Resources oRes = getResources();
        boolean hasDialog = true;
        switch (id) {

            // アプリ終了確認（ダイアログ表示）
            case Constants.DIALOG_EXIT_APP:

                oDialog.setTitle(oRes.getString(R.string.title_dialog_exit_app));
                oDialog.setMessage(oRes.getString(R.string.txt_exit_app_confirm));

                oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {
                    @Override
                    public void onClick(View v) {

//Chg 2011/10/11 Z01yoneya Start -->
////Add 2011/09/26 Z01yoneya Start -->
//                        NaviAppStatus.setAppFinishingFlag(true);
//                        NaviApplication app = ((NaviApplication)getApplication());
//                        app.stopNaviService();
////Add 2011/09/26 Z01yoneya End <--
//------------------------------------------------------------------------------------------------------------------------------------
                        try {
                            NaviAppStatus.setAppStatus(NaviAppStatus.APP_STATE_STOPPING);
                        } catch (IllegalStateException e) {
                            //例外が発生しないように状態遷移させること
                        }

                        CommonLib.setStopServiceFlag(true);
//Chg 2011/10/11 Z01yoneya End <--
                        setResult(Constants.RESULTCODE_FINISH_ALL);
// Chg 2011/10/20 katsuta Start -->
//                        finish();
//                        oDialog.dismiss();
                        oDialog.dismiss();
                        finish();
// Chg 2011/10/20 katsuta End <--
                    }
                });
                oDialog.addButton(oRes.getString(R.string.btn_cancel), new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        oDialog.dismiss();
                    }
                });

                break;
            // ルート案内終了確認（ダイアログ表示）
            case Constants.DIALOG_EXIT_NAVIGATION:

                oDialog.setTitle(oRes.getString(R.string.txt_navi_exit_title));
                oDialog.setMessage(oRes.getString(R.string.txt_navi_exit));

                oDialog.addButton(oRes.getString(R.string.btn_ok),
                        new OnClickListener() {
                            public void onClick(View v) {
// ADD -- 2012/03/09 -- H.Fukushima(No.406) --- ↓ ----- ref #3929
								CommonLib.setChangePos(false);
								CommonLib.setChangePosFromSearch(false);
// ADD -- 2012/03/09 -- H.Fukushima(No.406) --- ↑ ----- ref #3929

// Add by CPJsunagawa '2015-07-08 Start
				                CommonLib.setRegIcon(false);
				                CommonLib.setInputText(false);
// Add by CPJsunagawa '2015-07-08 End

//Chg 2011/09/26 Z01yoneya Start -->
//                                CommonLib.guideEnd(true);
//------------------------------------------------------------------------------------------------------------------------------------
                                CommonLib.guideEnd(true, ((NaviApplication)getApplication()).getNaviAppDataPath());
//Chg 2011/09/26 Z01yoneya End <--
                                oDialog.dismiss();
                                setResult(RESULT_OK);
                                finish();
                            }
                        });
                oDialog.addButton(oRes.getString(
                        R.string.btn_cancel),
                        new OnClickListener() {
                            public void onClick(View v) {
                                oDialog.dismiss();
                            }
                        });
                break;

            default:
                hasDialog = false;
                break;
        }

        return hasDialog ? oDialog : super.onCreateDialog(id);
    }

    @Override
    protected boolean onStartShowPage() throws Exception {
        // TODO 自動生成されたメソッド・スタブ
        return true;
    }

    @Override
    protected void onFinishShowPage(boolean bGetData) throws Exception {
        JNITwoLong CenterCoordinate = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(
                CenterCoordinate);
        String ae = String.valueOf(CenterCoordinate.getM_lLong());
        String an = String.valueOf(CenterCoordinate.getM_lLat());

//Chg 2011/11/15 Z01_h_yamada Start -->
//        Long limit = CenterCoordinate.getM_lLong() / 3600000;
//        Long minute = CenterCoordinate.getM_lLong() % 3600000 / 60000;
//        double second = CenterCoordinate.getM_lLong() / 1000 % 60;
//
//        Long limitLat = CenterCoordinate.getM_lLat() / 3600000;
//        Long minuteLat = CenterCoordinate.getM_lLat() % 3600000 / 60000;
//        double secondLat = CenterCoordinate.getM_lLat() / 1000 % 60;
//
//        String lon = limit + ":" + minute + ":" + second;
//        String lat = limitLat + ":" + minuteLat + ":" + secondLat;
//        Intent it = new Intent(Intent.ACTION_SEND);
//        it.setType("text/plain");
////         it.putExtra(Intent.EXTRA_EMAIL, "me@abc.com");
//        it.putExtra(Intent.EXTRA_TEXT, MessageFormat.format(getString(R.string.email_template), ae, an));
//        it.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject));
//        startActivity(it.createChooser(it, getString(R.string.email_client_chooser_title)));
//--------------------------------------------

		Uri uri = Uri.parse("mailto:");
		Intent intent=new Intent(Intent.ACTION_DEFAULT, uri);

		String body = MessageFormat.format(getString(R.string.email_template), ae, an);

		intent.putExtra("sms_body", body);
		intent.putExtra(Intent.EXTRA_TEXT, body);
		intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject));

		Intent chooser = Intent.createChooser(intent, null);
		chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(chooser);

//Chg 2011/11/15 Z01_h_yamada End <--
    }
}
