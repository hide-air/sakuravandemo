/**
 *  @file		DriConMainMenuActivity
 *  @brief		ドライブコンテンツのメインメニューを表示する
 *
 *  @attention
 *  @note		リスト表示
 *
 *  @author		Manabu Watanabe [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2011-09-01)
 *  @version	$Revision: $ by $Author: $
 *
 */
package net.zmap.android.pnd.v2.dricon.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
//ADD 2013.08.08 M.Honma 走行規制 メッセージ表示とフラグ取得 Start -->
import net.zmap.android.pnd.v2.common.DrivingRegulation;
//ADD 2013.08.08 M.Honma 走行規制 メッセージ表示とフラグ取得 End <--
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.dricon.controller.DriverContentsCtrl;


public class DriConMainMenuActivity extends DriconMenuListBaseActivity{

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.view.DriconMenuListBaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(m_oContentsDataList == null){
			m_nCount = 0;
		}else{
			m_nCount = m_oContentsDataList.size();
		}
		setTitleValue("ドライブコンテンツ");
		initDriConMenuLayout();
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.common.activity.MenuBaseActivity#onResume()
	 */
	public void onResume(){
		super.onResume();
		notifyUpdataList();
	}

	/**
	 * アイテムをクリックする
	 * @param pos クリックされたアイテム位置
	 */
	private void clickItem(int pos){
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ オン禁止 Start -->
		if(DrivingRegulation.CheckDrivingRegulation()){
			return;
		}
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ オン禁止 End <--
		Intent oIntent = new Intent(getIntent());
		oIntent.setClass(this,DriConDetailMenuActivity.class);
		oIntent.putExtra(Constants.DC_INTENT_DATA_SETTING_INDEX_EXTRA, pos);
		NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_DRICON_SETTING_DETAIL);
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.view.DriconMenuListBaseActivity#getViewList(int, android.view.View, android.view.ViewGroup)
	 */
	public View getViewList(int postion, View oView, ViewGroup parent) {
		if(oView == null)
		{
			LayoutInflater oInflater = LayoutInflater.from(DriConMainMenuActivity.this);
			m_oLayout = (LinearLayout)oInflater.inflate(R.layout.dricon_menu_item, null);
			oView = m_oLayout;
		} else {
			m_oLayout = (LinearLayout)oView;
		}

		final int wPos = postion;
		//コンテンツ名
		TextView txtView = (TextView)m_oLayout.findViewById(R.id.dc_menu_item_text);
		//コンテンツのIcon
		ImageView imgIcon = (ImageView)m_oLayout.findViewById(R.id.dc_menu_item_imgIcon);
		int key = m_oContentsDataList.get(wPos).m_iContentsID;
		Bitmap bitmap = DriverContentsCtrl.getController().getIconImgData(key);
		if(bitmap != null){
			imgIcon.setImageBitmap(bitmap);
		}

		//詳細メニュー画面へのボタン
		Button btnDetailMenu = (Button)m_oLayout.findViewById(R.id.dc_menu_item_btn);
		btnDetailMenu.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v){
				clickItem(wPos);
			}
		});

		if(m_oContentsDataList != null){
			if (wPos < m_oContentsDataList.size()) {
				if(m_oContentsDataList.get(wPos).m_bDisplayItem){
					btnDetailMenu.setText("オン");
				}else{
					btnDetailMenu.setText("オフ");
				}
				txtView.setText(m_oContentsDataList.get(wPos).m_sContentsName);
				if (AppInfo.isEmpty(m_oContentsDataList.get(wPos).m_sContentsName)) {
					txtView.setEnabled(false);
				} else {
					txtView.setEnabled(true);
				}

			} else {
				txtView.setText("");
				txtView.setEnabled(false);
			}
		}
		return oView;
	}

}
