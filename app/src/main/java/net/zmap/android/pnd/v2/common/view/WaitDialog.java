package net.zmap.android.pnd.v2.common.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.activity.LocationInfo;

public class WaitDialog extends Dialog
{
	private Context m_oContext;
//	private LinearLayout m_oButtonArea = null;
//	private List <Button>oButtons = new ArrayList<Button>();
//	private int count = 0;
	private boolean isShowProGressBar = false;
	private ProgressBar progressBar;

	public WaitDialog(Context oContext)
	{
		super(oContext);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		m_oContext = oContext;


		setContentView(R.layout.loading_dialog);
//Add 2011/07/25 Z01thedoanh Start -->
        if(CommonLib.getIsFullScreeen() == 0){
			DisplayMetrics display = new DisplayMetrics();
			((Activity) oContext).getWindowManager().getDefaultDisplay().getMetrics(display);

			WindowManager.LayoutParams params = getWindow().getAttributes();
			params.x = ( (int) CommonLib.screenWidth/2 - (int) display.widthPixels/2);
			params.y = ( (int) CommonLib.screenHeight/2 - (int) display.heightPixels/2);
			this.getWindow().setAttributes(params);
        }
//Add 2011/07/25 Z01thedoanh End <--

//		m_oButtonArea = (LinearLayout)findViewById(R.id.btnArea);

		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
	}

	public WaitDialog(Context oContext,int styleId)
	{
		super(oContext,styleId);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		m_oContext = oContext;


		setContentView(R.layout.loading_dialog);
//Add 2011/07/25 Z01thedoanh Start -->
		if(CommonLib.getIsFullScreeen() == 0){
			DisplayMetrics display = new DisplayMetrics();
			((Activity) oContext).getWindowManager().getDefaultDisplay().getMetrics(display);

			WindowManager.LayoutParams params = getWindow().getAttributes();
			params.x = ( (int) CommonLib.screenWidth/2 - (int) display.widthPixels/2);
			params.y = ( (int) CommonLib.screenHeight/2 - (int) display.heightPixels/2);
			this.getWindow().setAttributes(params);
        }
//Add 2011/07/25 Z01thedoanh End <--

//		m_oButtonArea = (LinearLayout)findViewById(R.id.btnArea);

		progressBar = (ProgressBar) findViewById(R.id.progress_bar);

	}


//	private Button createButton()
//	{
//	    Button oButton = oButtons.get(count);
//	    if(oButton == null){
//	        oButton = new Button(getContext());
//		}
//	    count ++;
////	    if((count >=2) && (!ScreenAdapter.isLandMode())){
////            count ++;
////        }
//	    oButton.setVisibility(View.VISIBLE);
//
//		return oButton;
//	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if(keyCode == KeyEvent.KEYCODE_SEARCH){
	        return true;
	    }
	    // XuYang add start 走行中
	    // 走行／停止
		if (keyCode == KeyEvent.KEYCODE_R && event.getAction() == KeyEvent.ACTION_DOWN) {
			Constants.RunStopflag = true;
		}
		if (keyCode == KeyEvent.KEYCODE_S && event.getAction() == KeyEvent.ACTION_DOWN) {
			Constants.RunStopflag = false;
		}

		// クレードル（車載ホルダ）
		if (keyCode == KeyEvent.KEYCODE_O && event.getAction() == KeyEvent.ACTION_DOWN) {
			Constants.NaviOnOffflag = true;
		}
		if (keyCode == KeyEvent.KEYCODE_F && event.getAction() == KeyEvent.ACTION_DOWN) {
			Constants.NaviOnOffflag = false;
		}
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			systemLimit();
		}
		// XuYang add end 走行中
	    return super.onKeyDown(keyCode, event);
	}
	// XuYang add start 走行中
	public void systemLimit() {
		if (CommonLib.isLimit()) {
        	goLocationInfo();
		}
	}
	public void goLocationInfo() {
		Intent intent = new Intent();
		intent.setClass(m_oContext, LocationInfo.class);
		m_oContext.startActivity(intent);
	}
	// XuYang add end 走行中

	private Runnable updateThread = new Runnable(){
        @Override
        public void run() {

            if (progressBar != null) {
                if (isShowProGressBar) {
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }
            }
        }

	};

	public void setShowProGressBar(final boolean isShowProGressBar) {
        this.isShowProGressBar = isShowProGressBar;
        new Handler().post(updateThread);
    }

// Add 2011/10/13 katsuta Start -->
    public void setText(int wId) {
		TextView txtView = (TextView)findViewById(R.id.TextView_wait);
		txtView.setText(wId);
    }
// Add 2011/10/13 katsuta End <--
}
