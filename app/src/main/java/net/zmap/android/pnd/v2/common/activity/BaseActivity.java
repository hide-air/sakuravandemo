package net.zmap.android.pnd.v2.common.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.StatFs;
import android.view.KeyEvent;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.app.NaviAppStatus;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.services.SystemService;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.StorageUtil;
import net.zmap.android.pnd.v2.common.utils.WakeLockUtils;
import net.zmap.android.pnd.v2.common.view.CustomDialog;

import java.io.File;

public class BaseActivity extends Activity {

    protected boolean hasRegistUpdate = false;
    protected BaseActivityStack mActivityStack = null;

    private AudioManager mAudio;

//Add 2011/11/16 Z01_h_yamada Start -->
    private boolean		misForeground = false;
//Add 2011/11/16 Z01_h_yamada End <--

    public NaviApplication getNaviApplication()
    {
        return ((NaviApplication)getApplication());
    }

    /*
     * アクティビティ作成可能なアプリ状態か判定する
     */
    protected boolean isCreateActivityStatus() {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "BaseActivity::isCreateActivityStatus() isAppFinishing()=" + NaviAppStatus.isAppFinishing() + " isAppRunning()=" + NaviAppStatus.isAppRunning() + " isAppPrepareNaviEngine=" + NaviAppStatus.isAppPrepareNaviEngine());

        if (NaviAppStatus.isAppRunning()) {
            return true;
        }
        if (NaviAppStatus.isAppPrepareNaviEngine()) {
            return true;
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "BaseActivity::onCreate isAppFinishing()=" + NaviAppStatus.isAppFinishing() + " class=" + this.toString());
//Chg 2011/10/11 Z01yoneya Start -->
////Add 2011/09/22 Z01yoneya Start -->
////アプリ終了中はアクティビティを作成しない。
////アクティビティスタックにも追加しない
//if (NaviAppStatus.isAppFinishing()) {
//    //NaviLog.d("", "BaseActivity::onCreate isAppFinishing() finish()!!!! this=" + this.toString());
//    finish();
//    return;
//}
////Add 2011/09/22 Z01yoneya End <--
////Add 2011/09/29 Z01yoneya Start -->
////ダウンロード認証が完了してない場合に、
////ナビアクティビティを起動しようとした場合は
////メインアクティビティを起動する。
//if (!NaviAppStatus.isCompleteDownloadAuthentication()) {
//    finish();
//    return;
//}
////Add 2011/09/29 Z01yoneya End <--
//------------------------------------------------------------------------------------------------------------------------------------
        if (!isCreateActivityStatus()) {
            finish();
            return;
        }
//Chg 2011/10/11 Z01yoneya End <--

        mActivityStack = ((NaviApplication)getApplication()).getBaseActivityStack();

        //full screen
        SystemService.setFullScreen(this);

        mActivityStack.addActivity(this);

        mAudio = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
    }

//Add 2011/09/29 Z01yoneya Start -->
    @Override
    protected void onStart() {
        super.onStart();

        NaviLog.d(NaviLog.PRINT_LOG_TAG, "BaseActivity::onStart isAppFinishing()=" + NaviAppStatus.isAppFinishing() + " class=" + this.toString());

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        NaviLog.d(NaviLog.PRINT_LOG_TAG, "BaseActivity::onRestart isAppFinishing()=" + NaviAppStatus.isAppFinishing() + " class=" + this.toString());

    }
//Add 2011/09/29 Z01yoneya End <--

    public boolean popAllActivityExceptMap() {
        return mActivityStack.popAllActivityExceptMap();
    }

//Add 2011/12/13 Z01_h_yamada Start -->
    public void removeSubAllActivity() {
        mActivityStack.removeSubAllActivity();
    }
    public BaseActivity getRootActivity() {
        return mActivityStack.getRootActivity();
    }
//Add 2011/12/13 Z01_h_yamada End <--
    public void finishMapUpdate() {
        Intent intent = getIntent();
        if (intent != null) {
            /*boolean updated = intent.getBooleanExtra("jp.co.cslab.pnd.UPDATER_DONE", false);
            if (updated) {
                showDialog(DIALOG_AREA_MAP_UPDATE_OVER);
            }*/
            int updateType = intent.getIntExtra("UpdaterType", -1);
            int updateResult = intent.getIntExtra("UpdaterResult", -1);
            switch (updateType) {
                case -1://no update
                    break;
                case 0://差分更新
                    if (updateResult == 0) {
                        sendUpdateOverBroadcast();
                    }
                    break;

                case 1://エリア更新（＝都道府県更新・全国更新）
                    if (updateResult == 0) {//
                        sendUpdateOverBroadcast();
                    }
                    break;
            }
        }
    }

    //自身の起動が完了したことを周知
    private void sendUpdateOverBroadcast() {
        Intent intentUPD = new Intent("android.intent.action.MAIN");
        intentUPD.putExtra("PND_CREATED", true);
        sendBroadcast(intentUPD);
    }

    @Override
    protected void onResume() {
        super.onResume();
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "BaseActivity::onResume isAppFinishing()=" + NaviAppStatus.isAppFinishing() + " class=" + this.toString());
//Add 2011/11/16 Z01_h_yamada Start -->
    	misForeground = true;
//Add 2011/11/16 true End <--

        WakeLockUtils.closeScreenLock(this);
    }

    @Override
    protected void onPause() {
//Add 2011/11/16 Z01_h_yamada Start -->
    	misForeground = false;
//Add 2011/11/16 true End <--
        super.onPause();
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "BaseActivity::onPause isAppFinishing()=" + NaviAppStatus.isAppFinishing() + " class=" + this.toString());
        WakeLockUtils.showScreenLock();
    }

    @Override
    protected void onDestroy() {
        if (mActivityStack != null) {
            mActivityStack.removeActivity(this);
        }

        NaviLog.d(NaviLog.PRINT_LOG_TAG, "BaseActivity::onDestroy isAppFinishing()=" + NaviAppStatus.isAppFinishing() + " class=" + this.toString());

        super.onDestroy();
    }

    public boolean isForeground() {
    	return misForeground;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_SEARCH) {
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            mAudio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            mAudio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
        }
        return super.onKeyUp(keyCode, event);
    }

    protected void showLiteVersionInfo() {
        SystemService.showMessage(this, R.string.temp);
    }

    /**
     * 全部スレッドをクリアする
     */
//Chg 2011/04/22 Z01doanh Start -->
//protected void killMainThread() {
//--------------------------------------------
    public void killMainThread() {
//Chg 2011/04/22 Z01doanh End <--

        NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity::killMainThread() start");

        ActivityManager manager = (ActivityManager)getSystemService("activity");
        if (manager != null) {
//            manager.restartPackage(getPackageName());
            manager.killBackgroundProcesses(getPackageName());
//Del 2012/02/23 Z01_h_yamada Start --> #3790
//          System.exit(0);
//Del 2012/02/23 Z01_h_yamada End   <--
        }

        NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity::killMainThread() end");

    }

    //バッテリ残量が既定値
    private static final int MIN_BATTERY_LEVEL = 10;

    public boolean isNeedUpd(int updateType) {
        if (!checkBattery()) {
            return false;
        }
        return true;
    }

    //バッテリ残量確認
    public boolean checkBattery() {
        //電源接続中 バッテリ残量が既定値以上あることを確認
        return (CommonLib.BatteryStatusCharging || CommonLib.BatteryLevel > MIN_BATTERY_LEVEL);
    }

    public static final int NEED_UPDATE_OK = 0;
    public static final int NO_NEED_UPDATE = 1;
    public static final int NEED_MORE_UPDATE_SIZE = 2;

    public int isUpdUpdate() {
        //specially modification
//        updCommon.setWorkDirPath();

        return NEED_UPDATE_OK;
    }

    //更新再開要否判定関数
    public boolean isNeedReUPD() {

        return false;
    }

    protected boolean checkSDAccess() {
//@@MOD-START BB-0003 2012/10/19 Y.Hayashida
    	File path = new File(StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME));
    	return path.exists();
//        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) ? true : false;
//@@MOD-END BB-0003 2012/10/19 Y.Hayashida
    }

    protected boolean checkMemory(long size) {
        if (checkSDAccess()) {
//@@MOD-START BB-0003 2012/10/19 Y.Hayashida
        	File path = new File(StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME));
//            File path = Environment.getExternalStorageDirectory();
//@@MOD-END BB-0003 2012/10/19 Y.Hayashida
            StatFs statfs = new StatFs(path.getPath());
            long blocSize = statfs.getBlockSize();
//          long totalBlocks = statfs.getBlockCount();
            long availaBlock = statfs.getAvailableBlocks();
            long available = availaBlock * blocSize;

            if (available > size) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    protected Dialog onCreateDialog(int id) {

        final CustomDialog oDialog = new CustomDialog(this);
        getResources();
        boolean hasDialog = true;

        switch (id) {

            default:
                hasDialog = false;
                break;
        }

        return hasDialog ? oDialog : super.onCreateDialog(id);
    }

//Add 2011/11/22 Z01_h_yamada Start -->
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "BaseActivity::onActivityResult resultCode=" + resultCode + " class=" + this.toString());

        ComponentName componentName = getCallingActivity();
        if (componentName == null) {
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "BaseActivity::onActivityResult callingAcitivity is none");
        }
        else {
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "BaseActivity::onActivityResult callingAcitivity=" + getCallingActivity().toShortString());
            
        }

        if (resultCode == Constants.RESULTCODE_FINISH_ALL) {
            setResult(resultCode, data);
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
//Add 2011/11/22 Z01_h_yamada End <--

//Add 2011/11/16 Z01_h_yamada Start -->
	/** ダイアログの表示要求
	*
	* @param id  表示するダイアログID
	* @return    成功(true) / 失敗(false)
	**/
    public boolean RequestShowDialog(int id) {

    	if ( this.isForeground() ) {

	    	switch (id) {
	    	case Constants.DIALOG_MEDIA_SIZE_INSUFFICIENT:
	    		// SDカードの容量不足通知
	    		try {
		        	final CustomDialog oDialog = new CustomDialog(this);
		        	if ( oDialog != null ) {
		    		   oDialog.setTitle(R.string.media_size_insufficient_title);
		    		   oDialog.setMessage(R.string.media_size_insufficient_text);
		    		   oDialog.addCancelButton(R.string.btn_ok);
		    		   oDialog.show();
		    		   return true;
		        	}
	    		} catch (Exception e) {
	    			NaviLog.w(NaviLog.PRINT_LOG_TAG, e);
	    			return false;
	    		}
	    	}
    	}
    	return false;
    }

//Add 2011/11/16 Z01_h_yamada End <--

    protected boolean dialogShowing = false;
    OnDismissListener mDismissListener = null;

    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        if (mDismissListener == null) {
            mDismissListener = new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    onDismissDialog(dialog);
                }
            };
        }
        dialog.setOnDismissListener(mDismissListener);
        dialogShowing = true;

        super.onPrepareDialog(id, dialog);
    }

    /**
     * ダイアログがDismissされる際にコールされる
     *
     * @param dialog
     *            ダイアログ
     */
    protected void onDismissDialog(DialogInterface dialog) {
        dialogShowing = false;
    }

    /**
     * ダイアログ表示状態判定
     *
     * @return true : ダイアログ表示中 false ダイアログ非表示
     */
    public boolean isShowingDialog() {
        return dialogShowing;
    }

    /**
     * アクティビティを前面に出す
     */
    public void wakeupActivity() {

        //即メインアクティビティが前面に来るように設定する
        Context context = getBaseContext();
        Intent intent = new Intent(context, this.getClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendInt = PendingIntent.getActivity(context, -1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager)context.getSystemService(ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), 1, pendInt);
        alarm.cancel(pendInt);
    }
}
