//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.content.Intent;
import android.os.Bundle;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_UserFile_RecordIF;

/**
 * K-F1_お気に入り一覧
 *
 * */
public class FavoritesInquiry extends FavoritesGenreInquiry {



	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		Intent oIntent = getIntent();
		//yangyang del start Bug1035
//		oTool = new ScrollTool(this);
////		oTool.bindView(oBox);
//		oTool.bindView(oList);
//		setViewInOperArea(oTool);
		//yangyang del end Bug1035
		recordCount = oIntent.getIntExtra(Constants.PARAMS_RECORD_COUNT, 0);
	}
	@Override
	protected POI_UserFile_RecordIF[] reShowData(long start, int count,
			long genreIndex, POI_UserFile_RecordIF[] mPoiUserFileListitem) {

		for (int i = 0; i < count; i++) {
			mPoiUserFileListitem[i] = new POI_UserFile_RecordIF();
		}

//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"FavoritesInquiry reShowData===genreIndex==" + genreIndex);
		NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecList(genreIndex, start, count,
				mPoiUserFileListitem, Rec);
		//自宅の場合
		if (genreIndex == super.KIND_OF_HOUSE) {
			POI_UserFile_RecordIF[] xyTest = null;
			xyTest = new POI_UserFile_RecordIF[1];
			for (int i = 0; i < 1; i++) {
				xyTest[i] = new POI_UserFile_RecordIF();
			}
			int flagIndex = 1;
			if (Rec.lcount == 0) {
//				flagIndex = 0;
				mPoiUserFileListitem[0].m_DispName = this.getResources().getString(R.string.dialog_myhome_save_button1);
				mPoiUserFileListitem[0].m_lLat = 0;
				mPoiUserFileListitem[0].m_lLon = 0;
			}
			real_house_count = (int)Rec.lcount;
			//自宅２
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecList(8, start, count,
					xyTest, Rec);
			if (Rec.lcount != 0 ) {
				mPoiUserFileListitem[flagIndex] = xyTest[0];
			} else {
				mPoiUserFileListitem[1].m_DispName = this.getResources().getString(R.string.dialog_myhome_save_button2);
				mPoiUserFileListitem[1].m_lLat = 0;
				mPoiUserFileListitem[1].m_lLon = 0;
			}
		}

		return mPoiUserFileListitem;
	}


}
