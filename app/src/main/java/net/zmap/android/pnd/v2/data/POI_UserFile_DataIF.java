/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           POI_UserFile_DataIF.java
 * Description    周辺検索用
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class POI_UserFile_DataIF {
    public short m_sDataIndex;
    public short m_sDataSize;
    public String m_Data;
    /**
    * Created on 2010/08/06
    * Title:       getM_sDataIndex
    * Description:  登録用　データの種類を取得する
    * @param1  無し
    * @return       short

    * @version        1.0
    */
    public short getM_sDataIndex() {
        return m_sDataIndex;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_sDataIndex
    * Description:  登録用　データサイズを取得する
    * @param1  無し
    * @return       short

    * @version        1.0
    */
    public short getM_sDataSize() {
        return m_sDataSize;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_sDataIndex
    * Description:    登録用　データポインタを取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getM_Data() {
        return m_Data;
    }
}
