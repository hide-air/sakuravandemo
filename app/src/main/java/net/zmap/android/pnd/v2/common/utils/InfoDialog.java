
package net.zmap.android.pnd.v2.common.utils;

import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.LinearLayout;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.NaviService;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.ItsmoNaviDriveExternalApi;
import net.zmap.android.pnd.v2.api.exception.NaviException;
import net.zmap.android.pnd.v2.api.map.ZoomLevel;
import net.zmap.android.pnd.v2.api.util.GeoUtils;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.DeliveryInfo;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.ProductInfo;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.maps.OpenMap;
import net.zmap.android.pnd.v2.sakuracust.CommonInquiryOps;
import net.zmap.android.pnd.v2.sakuracust.SakuraCustPOIData;
import net.zmap.android.pnd.v2.inquiry.activity.AddressProvinceInquiry;

/**
 * メッセージとOK、キャンセル(デフォルトは非表示)ボタンを表示するダイアログ
 */
public class InfoDialog extends Dialog {

    private Button mbtnOk; // 更新ボタン
    private Context mCtx;
    private LinearLayout mDispLL;
    
    // マニュアルマッチングで使用する情報
    //private int mCustID;
    //private long mLon, mLat;
    private EditText mName, mAdr;
    
    // 商品情報取得に使用する情報
    private EditText mCustCode;
    
    // 編集により値を取得する項目
    private EditText mCorrectComment;
    private EditText mDeliveryComment;
    private EditText mBoxPlace;
    private EditText mBirthday;
    private EditText mBirthdayMonth;
    private EditText mBirthdayDay;
    private EditText mMapNo;
    
    private DeliveryInfo mDelivInfo;
    
    private Button mReturn;
    private Button mManualMatch;
    private Button mDrawRoute;
    //private Button mManualMatchFromAdr;
    private Button mProductInfo;
    private Button mDeliveryStatus;
// Add by CPJsunagawa '2015-07-08 Start
    private Button mRegIcon;
    private Button mRegText;
// Add by CPJsunagawa '2015-07-08 End

    //private static ItsmoNaviDriveExternalApi oNaviApi;

/*    
    public static void InitializeInfoDialog(Context context)
    {
    	if (oNaviApi == null)
    	{
	    	oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
	    	oNaviApi.connect(context);
    	}
    }
*/    

    //public InfoDialog(Context context, String address, String product, String date, int claim, int idx) {
    public InfoDialog(Context context, DeliveryInfo delivInfo/*, int idx*/, LinearLayout dispLL) {
        super(context);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        // Add by M.Suna '13-09-03 start
//        NaviService oNavi = new NaviService();
        //ItsmoNaviDriveExternalApi oNaviApi = new ItsmoNaviDriveExternalApi();
        //ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
        //GeoPoint point = new GeoPoint();
        //point.

/*        
		int zoomLevel;
		zoomLevel = 1;
		try
		{
// Mod by M.Suna '13-09-13 Start
	    	ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
			//oNaviApi.openMap(delivInfo.mPoint, zoomLevel, true);
			//oNaviApi.connect(context);
			// 拡大して表示
			oNaviApi.setZoom(ZoomLevel.M_20);
			// 世界測地系BLを日本測地系に変換して中心に表示
			oNaviApi.setCenter(GeoUtils.toWGS84(delivInfo.mPoint.getLatitudeMs(), delivInfo.mPoint.getLongitudeMs()));
// Mod by M.Suna '13-09-13 End
		}
		catch (NaviException e)
		{
			e.printStackTrace();
			//return;
		}
*/		
        // Add by M.Suna '13-09-03 End

// 以下を顧客詳細情報画面に変えたい　by M.Suna '13-09-20
		// ダイアログレイアウト
//        LinearLayout dlgLayout = (LinearLayout) LayoutInflater.from(context).inflate(
//                R.layout.dlg_info, null);

        // ボタン周りの初期化
//        InitComponent(dlgLayout);
//        mCtx = context;

//        TextView tv = (TextView) dlgLayout.findViewById(R.id.tvAddress);
        //tv.setText(address);
//        tv.setText(delivInfo.mAddress);
//        tv = (TextView) dlgLayout.findViewById(R.id.tvCode);
        //tv.setText("宅配商品：" + product );
//        tv.setText("宅配商品：" + delivInfo.mProduct );
//        tv = (TextView) dlgLayout.findViewById(R.id.tvDate);
        //tv.setText("契約日：" + date);
//        tv.setText("契約日：" + delivInfo.mLastDate);
//        tv = (TextView) dlgLayout.findViewById(R.id.tvClaim);
        //if( claim > 0) {
//        if( delivInfo.mClaim > 0) {
//            tv.setText("クレーム　あり");
//        } else {
//            tv.setText("クレーム　なし");
//        }
        // 以下を顧客詳細情報画面に変えたい　by M.Suna '13-09-20

        LinearLayout dlgLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.layout_kokyakusyosai, null);

        // ボタン周りの初期化
        InitComponent(dlgLayout);
        mCtx = context;

/* ここがポイント！だと思う　by M.Suna '13-09-27 */
/*
        if( context instanceof MapActivity ) {
            Button btn = (Button)dlgLayout.findViewById(R.id.);
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ((MapActivity)mCtx).showCaution();
                }});
        }
*/
        // タイトル非表示
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        // TODO ここに、コントロールにデータを入れる処理を入れる。

        // ポイント
        //EditText dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText1);
        //dispCtrl.setText("" + delivInfo.mCustPoint);
        
        // 顧客ID（マニュアルマッチングで使う）
        //mCustID = delivInfo.mCustID;
        //mLon    = delivInfo.mPoint.getLongitudeMs();
        //mLat    = delivInfo.mPoint.getLatitudeMs();
        // キーボードを引っ込めるリスナ
/*        
		View.OnFocusChangeListener dropKeyboardOFCL = new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus && mCtx instanceof Activity)
				{
					Activity a = (Activity)mCtx;
					InputMethodManager inputMethodManager = (InputMethodManager) a.getSystemService(a.getBaseContext().INPUT_METHOD_SERVICE);
					inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				}
			}
		};
*/		

        // 顧客名
        //EditText dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText1);
        //EditText dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText3);
        mName = (EditText)dlgLayout.findViewById(R.id.editText3);
        mName.setText(delivInfo.mCustmer);
        mName.setFocusableInTouchMode(false);
        //mName.setOnFocusChangeListener(dropKeyboardOFCL);
        
        // 顧客コード
        mCustCode = (EditText)dlgLayout.findViewById(R.id.editText12);
        mCustCode.setText(delivInfo.mCustCode);
        mCustCode.setFocusableInTouchMode(false);
        //mCustCode.setOnFocusChangeListener(dropKeyboardOFCL);
        
        // この時点でプロダクト情報を取得しておく
        CommonInquiryOps.setProductInfoFromDB((Activity)mCtx, delivInfo.mCustCode);
        
        // 住所
        //dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText4);
        //dispCtrl.setText(delivInfo.mAddress);
        mAdr = (EditText)dlgLayout.findViewById(R.id.editText4);
        mAdr.setText(delivInfo.mAddress);
        mAdr.setFocusableInTouchMode(false);
        
        // 電話番号
        EditText dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText5);
        dispCtrl.setText(delivInfo.mTelNo);
        dispCtrl.setFocusableInTouchMode(false);
        
        // 誕生日
        mBirthday = (EditText)dlgLayout.findViewById(R.id.editText13);
        mBirthdayMonth = (EditText)dlgLayout.findViewById(R.id.editTextBirthdayMonth);
        mBirthdayDay = (EditText)dlgLayout.findViewById(R.id.editTextBirthdayDay);
        
        //if (!"".equals(delivInfo.mBirthDay))
        if (!"".equals(delivInfo.mBirthDay) && delivInfo.mBirthDay.length() >= 8)
        {
	        String byear = delivInfo.mBirthDay.substring(0, 4);
	        String bmonth = delivInfo.mBirthDay.substring(4, 6);
	        String bday = delivInfo.mBirthDay.substring(6, 8);
	        
	        mBirthday.setText(byear);
	        mBirthdayMonth.setText(bmonth);
	        mBirthdayDay.setText(bday);
        }
        
        // 成約年月日
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText6);
        dispCtrl.setText(delivInfo.mSeiyakuDate);
        dispCtrl.setFocusableInTouchMode(false);

        // 解約日
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText14);
        dispCtrl.setText(delivInfo.mKaiyakuDate);
        dispCtrl.setFocusableInTouchMode(false);
        
        // 取引開始年月日
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText7);
        dispCtrl.setText(delivInfo.mTorihikiDate);
        dispCtrl.setFocusableInTouchMode(false);
        
        // 解約理由
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText15);
        dispCtrl.setText(delivInfo.mKaiyakuReason);
        dispCtrl.setFocusableInTouchMode(false);
        
        // 回収種別
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText8);
        dispCtrl.setText(delivInfo.mKaishuKind);
        dispCtrl.setFocusableInTouchMode(false);
        
        // 集金コメント
        mCorrectComment = (EditText)dlgLayout.findViewById(R.id.editText16);
        mCorrectComment.setText(delivInfo.mCollectingMemo);
        
        // 配達コース
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText9);
        dispCtrl.setText(delivInfo.mDeliCourse);
        dispCtrl.setFocusableInTouchMode(false);
        
        // 設定配達順
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText17);
        dispCtrl.setText("" + delivInfo.mDeliOrder);
        dispCtrl.setFocusableInTouchMode(false);
        
        // 配達コメント
        mDeliveryComment = (EditText)dlgLayout.findViewById(R.id.editText10);
        mDeliveryComment.setText(delivInfo.mDeliComment);
        
        // 図番
        mMapNo = (EditText)dlgLayout.findViewById(R.id.editText18);
        mMapNo.setText(delivInfo.mMapNo);
        
        // BOX位置
        mBoxPlace = (EditText)dlgLayout.findViewById(R.id.editText11);
        mBoxPlace.setText(delivInfo.mBoxPlace);
        
        // 連絡メモ
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editContactingMemo);
        dispCtrl.setText("" + delivInfo.mContactingMemo);
        dispCtrl.setFocusableInTouchMode(false);
        
        // 取り急ぎ任意の商品を取得する。
        //NaviApplication app = (NaviApplication)((Activity)mCtx).getApplication();
        //ProductInfo pi = app.getProduct(0);
        
        // ステータスの設定
        //mNowStatus = pi.mDeliveryStatus;
        mNowStatus = delivInfo.mDeliveryStatus;
        SetStatusCaption();
        
        mDelivInfo = delivInfo;
        mDispLL = dispLL;
        
        // モーダル状態セット
        this.setCanceledOnTouchOutside(false);

        // レイアウトセット
        this.setContentView(dlgLayout);
        
        getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
    }


    /**
     * ボタン周りの初期化
     *
     * @param dlgLayout ダイアログレイアウト
     */
    private void InitComponent(LinearLayout dlgLayout) {

        // OKボタンイベントセット
        mbtnOk = (Button) dlgLayout.findViewById(R.id.button1);
        mbtnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
/*            	
            	String tbyear = mBirthday.getText().toString();
            	String byear = !"".equals(tbyear) ? tbyear : "0000";
            	if (byear.length() != 4)
            	{
            		AlertDialog.Builder adb = new AlertDialog.Builder(mCtx);
            		adb.setMessage("年は4桁で入力してください。");
            		adb.setPositiveButton("OK", null);
            		adb.show();
            		return;
            	}
*/            	
            	
            	// 内部メモリに反映
            	int oldStatus = mDelivInfo.mDeliveryStatus; 
            	mDelivInfo.mDeliveryStatus = mNowStatus;
            	mDelivInfo.mCollectingMemo = mCorrectComment.getText().toString();
            	mDelivInfo.mDeliComment = mDeliveryComment.getText().toString();
            	mDelivInfo.mMapNo = mMapNo.getText().toString();
            	mDelivInfo.mBoxPlace = mBoxPlace.getText().toString();
//*            	
            	String tbyear = mBirthday.getText().toString();
            	String byear = !"".equals(tbyear) ? tbyear : "0000";
            	while (byear.length() <= 3)
            	{
            		byear = "0" + byear;
            	}
//*/            	
            	
            	String tbmonth = mBirthdayMonth.getText().toString();
            	String bmonth = !"".equals(tbmonth) ? tbmonth : "00";
            	if (bmonth.length() == 1) bmonth = "0" + bmonth;
            	
            	String tbday = mBirthdayDay.getText().toString();
            	String bday = !"".equals(tbday) ? tbday : "00";
            	if (bday.length() == 1) bday = "0" + bday;
            	
            	//mDelivInfo.mBirthDay = mBirthday.getText().toString();
            	mDelivInfo.mBirthDay = byear + bmonth + bday;
            	
            	// DBへ反映
            	DbHelper helper = new DbHelper(mCtx);
            	SQLiteDatabase db = helper.getWritableDatabase();
            	db.execSQL("UPDATE ProductInfo Set DeliveryStatus = " + mDelivInfo.mDeliveryStatus + " WHERE CustCode = '" + mCustCode.getText().toString() + "'");
            	
            	StringBuffer sb = new StringBuffer();
            	sb.append("UPDATE DeliveryInfo SET CollectingMemo = '");
            	sb.append(mDelivInfo.mCollectingMemo); // 集金コメント
            	sb.append("',DeliComment = '");
            	sb.append(mDelivInfo.mDeliComment); // 配達コメント
            	sb.append("',MapNo = '");
            	sb.append(mDelivInfo.mMapNo); // 図番
            	sb.append("',BoxPlace = '");
            	sb.append(mDelivInfo.mBoxPlace); // BOX位置 
            	sb.append("',BirthDay = '");
            	sb.append(mDelivInfo.mBirthDay); // 誕生日
            	sb.append("' WHERE CustID = ");
            	//sb.append(mCustID);
            	sb.append(mDelivInfo.mCustID);
            	db.execSQL(sb.toString());
            	
            	db.close();
            	helper.close();
            	
            	// ボタンのキャプションにも反映
            	TextView tv = (TextView)mDispLL.findViewById(R.id.infoMemo);
            	tv.setText(mDelivInfo.mContactingMemo);
            	tv = (TextView)mDispLL.findViewById(R.id.infoComment);
            	tv.setText(mDelivInfo.mDeliComment);
                tv = (TextView)mDispLL.findViewById(R.id.infoWorkCode);
                tv.setText(mDelivInfo.mMapNo);
                tv = (TextView)mDispLL.findViewById(R.id.infoBoxPosition);
                tv.setText(mDelivInfo.mBoxPlace);
                
                Button btn = (Button)mDispLL.findViewById(R.id.btnDeliveryStatus);
                //String[] cap = mCtx.getResources().getStringArray(R.array.deliverystate);
                //btn.setText("【" + cap[mDelivInfo.mDeliveryStatus] + "】");
                
                // そして数を反映
                if (mCtx instanceof MapActivity)
                {
                	MapActivity ma = (MapActivity)mCtx;
                	ma.setStatusButton(btn, mDelivInfo.mDeliveryStatus);
                	ma.reflectDeliveredCount();
                }
                else
                {
                    String[] cap = mCtx.getResources().getStringArray(R.array.deliverystate);
                    btn.setText("【" + cap[mDelivInfo.mDeliveryStatus] + "】");
                }
// Add by CPJsunagawa '2015-06-04 Start
//            	Button btn2 = (Button)mDispLL.findViewById(R.id.btnDispCap);
// Add by CPJsunagawa '2015-06-04 End
                
                // ステータスが変わった場合はルートも引き直す
                if (oldStatus != mDelivInfo.mDeliveryStatus)
                {
                	MapActivity ma = (MapActivity)mCtx;
                	NaviApplication na = ma.getNaviApplication();
                	CommonInquiryOps.updateRouteData(ma);
                	//na.setTapRouteStartPos(0);
                	na.setRoutePageFirstOnly();
                	//ma.determineVehicleRoute(0);
                	ma.determineVehicleRouteFirst();
                	//ma.showListPageZero();
                	ma.showList(true, false, false);
                }
            	
                InfoDialog.this.dismiss();
            }
        });
        
        // 戻るボタンイベントセット
        mReturn = (Button)dlgLayout.findViewById(R.id.button4);
        mReturn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				InfoDialog.this.dismiss();
			}
		});

        // マニュアルマッチング
        mManualMatch = (Button)dlgLayout.findViewById(R.id.manualMatching);
        mManualMatch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
/*				
				String capOfFromMap = mCtx.getString(R.string.infoMatchingFromMap);
				String capOfFromAdr = mCtx.getString(R.string.infoMatchingFromAdr);
				String[] items = new String[]{capOfFromMap, capOfFromAdr};
				AlertDialog.Builder adb = new AlertDialog.Builder(mCtx);
				adb.setTitle(R.string.infoMatching);
				adb.setItems(items, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface i, int which)
					{
						switch (which)
						{
						case 0: // 地図から
						{
					        CommonLib.setChangePos(true);
					        CommonLib.setChangePosFromSearch(false);
					        CommonLib.routeSearchFlag = false;

					        SakuraCustPOIData oData = new SakuraCustPOIData();
					        oData.m_MyIndex = mDelivInfo.mMyIndex;
					        oData.m_CustID = mDelivInfo.mCustID;
					        oData.m_sName = mName.getText().toString();
					        oData.m_sAddress = mAdr.getText().toString();
					        oData.m_wLong = mDelivInfo.mPoint.getLongitudeMs();
					        oData.m_wLat = mDelivInfo.mPoint.getLatitudeMs();
					        Intent intent = new Intent();
					        intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_EDIT);
					        
					        OpenMap.moveMapTo((Activity)mCtx, oData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
						}
							break;
						case 1: // 住所から
						{
							if (mCtx instanceof Activity)
							{
								Activity act = (Activity)mCtx;
						        Intent oIntent = new Intent(act.getIntent());
						        oIntent.setClass(mCtx, AddressProvinceInquiry.class);
						        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
						            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
						        }
						        NaviRun.GetNaviRunObj().setSearchKind(AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY);
						        oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, act.getResources().getString(R.string.address_title));
						        oIntent.putExtra(Constants.ADDRESS_SEARCH_FOR_MATCH, mDelivInfo.mMyIndex); // 送る値は現状のインデックス
						        NaviActivityStarter.startActivityForResult(act, oIntent, AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY);
						        
							}
						}
							break;
						}
						
						// このダイアログを閉じる。
						InfoDialog.this.dismiss();
					}
				}
				);
				adb.setPositiveButton(R.string.btn_cancel, null);
				adb.show();
*/
				if (mCtx instanceof MapActivity)
				{
					MapActivity act = (MapActivity)mCtx;
					act.executeManualMatching(mDelivInfo, null);
					
					// このダイアログを閉じる。
					InfoDialog.this.dismiss();
				}
/*				
				// マッチング処理を開始する。
				//createDialog(Constants.DIALOG_WAIT, -1);
		        CommonLib.setChangePos(true);
		        CommonLib.setChangePosFromSearch(false);
		        CommonLib.routeSearchFlag = false;

		        SakuraCustPOIData oData = new SakuraCustPOIData();
		        //oData.m_CustID = mCustID;
		        oData.m_MyIndex = mDelivInfo.mMyIndex;
		        oData.m_CustID = mDelivInfo.mCustID;
		        oData.m_sName = mName.getText().toString();
		        oData.m_sAddress = mAdr.getText().toString();
		        //oData.m_wLong = mLon;
		        //oData.m_wLat = mLat;
		        oData.m_wLong = mDelivInfo.mPoint.getLongitudeMs();
		        oData.m_wLat = mDelivInfo.mPoint.getLatitudeMs();
		        Intent intent = new Intent();
		        intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_EDIT);
		        //intent.putExtra(AppInfo.FLAG_POI_TYPE, AppInfo.POI_LOCAL);
		        
		        OpenMap.moveMapTo((Activity)mCtx, oData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
		        
				// このダイアログを閉じる。
				InfoDialog.this.dismiss();
*/				
			}
		});
        
        // ルート引き
        mDrawRoute = (Button)dlgLayout.findViewById(R.id.infoDrawRoute);
        mDrawRoute.setOnClickListener(new View.OnClickListener() {
        	@Override
			public void onClick(View v) {
        		if (mCtx instanceof MapActivity)
                {
        			// 即ルートを引く
                	MapActivity ma = (MapActivity)mCtx;
                	
                	// ページを頭に持って行く
                	//NaviApplication app = (NaviApplication)ma.getApplication();
                	//app.setRoutePageFirstOnly();
                	
                	NaviApplication app = (NaviApplication)ma.getApplication();
                	app.setNowRoutingCustomerIndex(mDelivInfo);
                	//app.setCloseUpPoint(mDelivInfo.mPoint.getLatitudeMs(), mDelivInfo.mPoint.getLongitudeMs());
                	//app.clearCloseUpPoint();
                	// ルートを引く前に中心を調整する。
                	try
                	{
	        	    	ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
	        			oNaviApi.setCenter(GeoUtils.toWGS84(mDelivInfo.mPoint.getLatitudeMs(), mDelivInfo.mPoint.getLongitudeMs()));
                	}
                	catch (NaviException e)
                	{
                		e.printStackTrace();
                	}
                	
                	// ルートを引く
                	ma.calcRoute();
                	// このダイアログを閉じる。
					InfoDialog.this.dismiss();
				}        	
        	}
        });
        
        // 住所から
/*        
        mManualMatchFromAdr = (Button)dlgLayout.findViewById(R.id.manualMatchingFromAdr);
        mManualMatchFromAdr.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mCtx instanceof Activity)
				{
					Activity act = (Activity)mCtx;
			        Intent oIntent = new Intent(act.getIntent());
			        oIntent.setClass(mCtx, AddressProvinceInquiry.class);
			        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
			            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
			        }
			        NaviRun.GetNaviRunObj().setSearchKind(AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY);
			        oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, act.getResources().getString(R.string.address_title));
			        oIntent.putExtra(Constants.ADDRESS_SEARCH_FOR_MATCH, mDelivInfo.mMyIndex); // 送る値は現状のインデックス
			        NaviActivityStarter.startActivityForResult(act, oIntent, AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY);
			        
					// このダイアログを閉じる。
					InfoDialog.this.dismiss();
				}
			}
		});
*/		
        
        // 商品情報
        mProductInfo = (Button)dlgLayout.findViewById(R.id.productInfo);
        mProductInfo.setOnClickListener(new View.OnClickListener() {
        	@Override
			public void onClick(View v) {
        		//Activity palAct = (Activity)mCtx;
        		
        		// プロダクト情報を取得する
        		//CommonInquiryOps.setProductInfoFromDB(palAct, mCustCode.getText().toString());
        		
        		// 商品情報のダイアログを表示する。
        		//NaviApplication app = (NaviApplication)palAct.getApplication();
        		//ProductInfo pi = app.getProduct(0);
        		ProductDialog pd = new ProductDialog(mCtx);
        		pd.show();
        	}
        });
        
        // 配達ステータス
        mDeliveryStatus = (Button)dlgLayout.findViewById(R.id.delivst);
        mDeliveryStatus.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//IncrementStatus();
				showSelectStatus();
			}
		});
/* Cut 20160118 Start */
// Add by CPJsunagawa '2015-07-08 Start
    	// アイコン登録
        mRegIcon = (Button)dlgLayout.findViewById(R.id.infoRegIcon);
        mRegIcon.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//IncrementStatus();
//				showSelectStatus();

            	System.out.println("*** InfoDialog InitComponent onClick ***");
// Add by CPJsunagawa '2015-07-08 Start
                CommonLib.setRegIcon(true);
                CommonLib.setInputText(false);
// Add by CPJsunagawa '2015-07-08 End

				if (mCtx instanceof MapActivity)
				{
					MapActivity act = (MapActivity)mCtx;
	            	System.out.println("*** InfoDialog InitComponent Before executeSelectIcon ***");
//					act.executeSelectIcon(mDelivInfo, null);
//					act.executeSelectIconMain(mDelivInfo, null);
					act.executeSelectIconMain(null);
					
					// このダイアログを閉じる。
					InfoDialog.this.dismiss();
					
					// ここでリストを閉じてみる
					act.hideList();
				}
				
			}
		});

    	// テキスト入力
        mRegText = (Button)dlgLayout.findViewById(R.id.infoRegText);
        mRegText.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//IncrementStatus();
//				showSelectStatus();

// Add by CPJsunagawa '2015-07-08 Start
                CommonLib.setRegIcon(false);
                CommonLib.setInputText(true);
// Add by CPJsunagawa '2015-07-08 End

				if (mCtx instanceof MapActivity)
				{
					MapActivity act = (MapActivity)mCtx;
					act.executeInputText(mDelivInfo, null);
					
					// このダイアログを閉じる。
					InfoDialog.this.dismiss();
				}
			}
		});
// Add by CPJsunagawa '2015-07-08 End
/* Cut 20160118 End */

    }

    private int mNowStatus = 0;
/*    
    private void IncrementStatus()
    {
    	String[] cap = mCtx.getResources().getStringArray(R.array.deliverystate);
    	mNowStatus++;
    	if (mNowStatus >= cap.length) mNowStatus = 0;
    	SetStatusCaption();
    }
*/    
    
    private void SetStatusCaption()
    {
    	if (mCtx instanceof MapActivity)
    	{
    		MapActivity ma = (MapActivity)mCtx;
    		ma.setStatusButton(mDeliveryStatus, mNowStatus, false);
    	}
    	else
    	{
	    	String[] cap = mCtx.getResources().getStringArray(R.array.deliverystate);
	    	mDeliveryStatus.setText(cap[mNowStatus]);
    	}
    }
    
    private void showSelectStatus()
    {
    	// ラジオボタン押下時の挙動
    	DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener(){
    		public void onClick(DialogInterface di, int which)
    		{
    			mNowStatus = which;
    			SetStatusCaption();
    			di.dismiss();
    		}
    	};
    	
    	// アラートダイアログの構築
    	AlertDialog.Builder ab = new AlertDialog.Builder(mCtx);
    	ab.setTitle(R.string.dialog_cap_status);
    	String[] cap = mCtx.getResources().getStringArray(R.array.deliverystate);
    	ab.setSingleChoiceItems(cap, mNowStatus, ocl);
    	ab.setPositiveButton(R.string.btn_cancel, null);
    	
    	// 表示する。
    	ab.show();
    }

/*
// Add by CPJsunagawa '2015-07-08 Start
    private void showRegIcon()
    {
    	// ラジオボタン押下時の挙動
    	DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener(){
    		public void onClick(DialogInterface di, int which)
    		{
    			mNowStatus = which;
    			SetStatusCaption();
    			di.dismiss();
    		}
    	};
    	
    	// アラートダイアログの構築
    	AlertDialog.Builder ab = new AlertDialog.Builder(mCtx);
    	ab.setTitle(R.string.dialog_cap_status);
    	String[] cap = mCtx.getResources().getStringArray(R.array.deliverystate);
    	ab.setSingleChoiceItems(cap, mNowStatus, ocl);
    	ab.setPositiveButton(R.string.btn_cancel, null);
    	
    	// 表示する。
    	ab.show();
    }
// Add by CPJsunagawa '2015-07-08 End
*/
}
