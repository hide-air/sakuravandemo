package net.zmap.android.pnd.v2.vics;

public interface ProbeEventListener {

	/**
	 * データを成功に返した後、当該関数を呼出す。
	 *
	 */
	public void onFinish(String strData);

	/**
	 * 途中でキャンセルした時、当該関数を呼出す。
	 *
	 */
	public void onCancel();

	/**
	 * エラーが発生した時、当該関数を呼出す。
	 *
	 */
	public void onError();

	/**
	 * 要求がタイムアウトした時、当該関数を呼出す。
	 *
	 */
	public void onTimeOut();
}
