/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           POI_UserFile_RecordIF.java
 * Description    取得・登録用レコード部（IF受け渡し用）
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class POI_UserFile_RecordIF {
    public long m_lIconCode;
    public String m_DispName;
    public long m_lDate;
    public long m_lLon;
    public long m_lLat;
    public long m_lUserFlag;
    public long m_lImport;

    /**
    * Created on 2010/08/06
    * Title:       setM_DispName
    * Description:  表示用名称を設定する
    * @param1  無し
    * @return       void

    * @version        1.0
    */
    public void setM_DispName(String dispName) {
        m_DispName = dispName;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lIconCode
    * Description:  アイコンコードを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lIconCode() {
        return m_lIconCode;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_DispName
    * Description:  表示用名称を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getM_DispName() {
        return m_DispName;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_DispName
    * Description:  日付を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lDate() {
        return m_lDate;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lLon
    * Description:  経度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLon() {
        return m_lLon;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lLat
    * Description:  緯度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLat() {
        return m_lLat;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lUserFlag
    * Description:  ユーザ定義フラグを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lUserFlag() {
        return m_lUserFlag;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_lImport
    * Description:  インポートフラグ（インポートから読み出しの時TRUE)を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lImport() {
        return m_lImport;
    }
}
