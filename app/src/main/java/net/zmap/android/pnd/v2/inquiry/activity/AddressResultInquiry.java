//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.ScrollBoxAdapter;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Address_ListItem;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.maps.OpenMap;

/**
 * K-A3_住所第４階層
 * K-A4_住所第５階層
 * K-A5_住所第６階層
 *
 *
 * */
public class AddressResultInquiry extends InquiryBaseLoading implements ScrollBoxAdapter {

	/**
	 * 次レベルのIndex
	 * */
	private JNILong Treeindex = new JNILong();
	/** JNIから、取得した表示の内容 */
	private POI_Address_ListItem[] m_Poi_Add_Listitem = null;
	/** 表示した内容 */
	private POI_Address_ListItem[] m_Poi_Show_Listitem = null;
	ScrollList oList = null;
	/** スクロールできる総件数 */
	private int llRecordCount = 0;
	private int getRecordIndex = 0;
	private static AddressResultInquiry instance;
	/** JumpKey５０音の内容 */
//	private JNIJumpKey[] JumpKey = null;
	/**
	 * JNIからでーたを取得するとき、スタートIndex
	 * */
	private long RecIndex;
	private boolean OKBtnFlag = false;
//	/** スクロール対象 */
//	private ScrollBox oBox =null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		instance = this;
		startShowPage(NOTSHOWCANCELBTN);

		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"AddressResultInquiry onCreate");

//		initDialog();

	}

	public static AddressResultInquiry getInstance() {
		return instance;
	}


//	@Override
//	protected void searchJUMP() {
//		JNIShort JumpRecCount = new JNIShort();
////		super.searchJUMP();
//		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetJumpList(JumpKey,//[out]
//				JumpRecCount);//JumpRecCountは50音の個数,JumpKey５０音の内容
//		initDialog();
//	}
	/**
	 * 画面を初期化する
	 *
	 *
	 * */
	private void initDialog() {
		Intent oIntent = getIntent();
//Del 2011/09/17 Z01_h_yamada Start -->
//		int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE,0);
//		if (0!= titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--
		LayoutInflater oInflater = LayoutInflater.from(this);
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base_list_freescroll, null);
//		タイトルを設定する
		TextView oText = (TextView)oLayout.findViewById(R.id.inquiry_title);
		String sTitle = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
		if(sTitle == null)
		{
			oText.setText("");
		}
		else
		{
// Add by CPJsunagawa '13-12-25 Start
        	if (oIntent.hasExtra(Constants.ADDRESS_SEARCH_DB_ADR))
        	{
        		sTitle += " （" + oIntent.getStringExtra(Constants.ADDRESS_SEARCH_DB_ADR) + "）";
        	}
// Add by CPJsunagawa '13-12-25 End
			oText.setText(sTitle);
		}
//

		Button oBtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
		oBtn.setBackgroundResource(R.drawable.btn_default);
		oBtn.setText(R.string.address_ok);
//		oBtn.setText(R.string.btn_all_country);
		oBtn.setOnClickListener(OnOKListener);
//		oBox = (ScrollBox)oLayout.findViewById(R.id.scrollBox);
//		oBox.setAdapter(this);
		oList = (ScrollList)oLayout.findViewById(R.id.scrollList);
//Del 2011/07/21 Z01thedoanh Start -->
		//oList.setPadding(0, 5, 0, 5);
//Del 2011/07/21 Z01thedoanh End <--
//Del 2011/10/06 Z01_h_yamada Start -->
//		oList.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
		oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
		oList.setAdapter(listAdapter);
//
		ScrollTool oTool = new ScrollTool(this);
//		oTool.bindView(oBox);
		oTool.bindView(oList);
		setViewInOperArea(oTool);

		this.setViewInWorkArea(oLayout);

	}
	private ListAdapter listAdapter = new BaseAdapter (){

		@Override
		public int getCount() {
			return AddressResultInquiry.this.getCount();
		}

		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int wPos, View oView, ViewGroup parent) {

//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"getView wPos--------------->" + wPos);
			return AddressResultInquiry.this.getView(wPos, oView);
		}

	};
	@Override
	protected void search() {
		JNILong RecCount = new JNILong();
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"search do-------------");
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecCount(RecCount);
//		llRecordCount =initPageIndexAndCnt(RecCount);
		llRecordCount = (int) RecCount.lcount;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"search llRecordCount===" + llRecordCount);
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"start ====128======" + RecIndex);
		getList(RecIndex);
//		if(bIsRefresh) {
//			oBox.refresh();
//		}

	}

	/**
	 * 総件数を取得する
	 * */
	private void initTreeIndex() {
		Intent oIntent = getIntent();

//		int pos = oIntent.getIntExtra(Constants.PARAMS_PROVINCE_ID, 0);
		long lTreeRecIndex = oIntent.getLongExtra(Constants.PARAMS_TREERECINDEX, (long)0);
//		short WideCode = oIntent.getShortExtra(Constants.PARAMS_WIDECODE, (short) 0);
//		short MiddleCode = oIntent.getShortExtra(Constants.PARAMS_MIDDLECODE, (short) 0);
//		short NarrowCode = oIntent.getShortExtra(Constants.PARAMS_NARROWCODE, (short) 0);
//		lTreeRecIndex = pos;
//		oIntent.putExtra(Constants.PARAMS_TREERECINDEX, lTreeRecIndex);
//		lTreeRecIndex = 25;
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"30 ====lTreeRecIndex==" + lTreeRecIndex);
		// 住所レベル（Treeindex）を取得する
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POIAddr_GetNextTreeIndex start");
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetNextTreeIndex(lTreeRecIndex, Treeindex);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POIAddr_GetNextTreeIndex end");
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"1466 ====Treeindex==" + Treeindex.lcount);
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchNextList(lTreeRecIndex);
		//RDB切替してから、該当内容は実行しない
//		searchJUMP();

		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"1467 ====lTreeRecIndex==" + lTreeRecIndex);



	}

	private void initDataObj() {

		initOnceGetList();

//		if(JumpKey  == null){
//			JumpKey = new JNIJumpKey[50];
//			for (int i = 0; i < 50; i++) {
//				JumpKey[i] = new JNIJumpKey();
//			}
//		}
		m_Poi_Show_Listitem = new POI_Address_ListItem[ONCE_GET_COUNT*2];
		for (int i = 0 ; i < ONCE_GET_COUNT*2 ; i++) {
			m_Poi_Show_Listitem[i] = new POI_Address_ListItem();
		}

	}

	private void initOnceGetList() {
		m_Poi_Add_Listitem = new POI_Address_ListItem[ONCE_GET_COUNT];
		for (int i = 0 ; i < ONCE_GET_COUNT ; i++) {
			m_Poi_Add_Listitem[i] = new POI_Address_ListItem();
		}

	}

	@Override
	public int getCount() {

		if (llRecordCount> 5) {
			return llRecordCount;
		} else {
			return 5;
		}
	}

	@Override
	public int getCountInBox() {
		return 5;
	}
//	private boolean bIsRefresh = true;
//	private int getRecordIndex = 0;
	int wOldPos = 0;
	@Override
	public View getView(final int wPos, View oView) {
//		int pageIndex = 0;
		LinearLayout oLayout = null;
		Button obtn = null;
		if (oView == null || !(oView instanceof LinearLayout)) {
//			obtn = new Button(this);
//			oView = obtn;
			oLayout = new LinearLayout(this);
			oLayout.setFocusable(false);
			oLayout.setGravity(Gravity.CENTER_VERTICAL);
//			oLayout.setPadding(0, 5, 0, 5);
			obtn = new Button(this);
//Chg 2011/11/01 Z01_h_yamada Start -->
//			obtn.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
//--------------------------------------------
			obtn.setGravity(Gravity.CENTER);
//Chg 2011/11/01 Z01_h_yamada End <--
//Add 2011/07/21 Z01thedoanh Start -->
			//ScrollListのLayout設定

//Chg 2011/08/11 Z01_h_yamada Start -->
//			ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
//			obtn.setLayoutParams(params);
////Add 2011/07/21 Z01thedoanh End <--
//			obtn.setId(R.id.btnOk);
//			obtn.setTextSize(26);
//--------------------------------------------
			Resources res = getResources();
			ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, (int)res.getDimension(R.dimen.Button_628_70_Lheight));
			obtn.setLayoutParams(params);
			obtn.setId(R.id.btnOk);
//Chg 2011/11/01 Z01_h_yamada Start -->
//			obtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.Common_Small_textSize));
//--------------------------------------------
			obtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.Common_Large_textSize));
//Chg 2011/11/01 Z01_h_yamada End <--
//Chg 2011/08/11 Z01_h_yamada End <--

			obtn.setBackgroundResource(R.drawable.btn_default);
			//前から「...」とする
			obtn.setSingleLine(true);
			obtn.setEllipsize(TruncateAt.START);
			oLayout.addView(obtn);
			oView = oLayout;
		} else {
//			if (wPos%getCountInBox() == 0) {
//				//ページIndexを取得する
//				pageIndex  = wPos/getCountInBox();
//				RecIndex = getCountInBox() * pageIndex;
//			}
//			obtn = (Button)oView;
			oLayout = (LinearLayout)oView;
			obtn = (Button)oLayout.findViewById(R.id.btnOk);
		}

		//下にスクロールの場合
		if (wOldPos <= wPos) {
			if (wPos < getCount() && ONCE_GET_COUNT != 0
					&& wPos >= ONCE_GET_COUNT && (wPos % ONCE_GET_COUNT == 0 ||
							(getRecordIndex != wPos / ONCE_GET_COUNT))) {
				//上にスクロール場合
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview  1==== wPos===="
//						+ wPos + ",==getRecordIndex==" + getRecordIndex
//						+ ",==(wPos / ONCE_GET_COUNT)=="
//						+ (wPos / ONCE_GET_COUNT));
				RecIndex = wPos;
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"44 wOldPos <= wPos  ==== wPos====" + wPos);
				getList(RecIndex);
			}
		}else {
			/*総件数76件、最後ページであるし、上にスクロール時、
			 * 70あるいは６９のデータを表示するとき、グレーボタンを表示しないように
			 */
			if (getRecordIndex != wPos / ONCE_GET_COUNT) {
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview  2==== wPos===="
//						+ wPos + ",==getRecordIndex==" + getRecordIndex
//						+ ",==(wPos / ONCE_GET_COUNT)=="
//						+ (wPos / ONCE_GET_COUNT));
				RecIndex = wPos;
				getList(RecIndex);
			}
		}

//		if (wPos> == 0) {
//			search();
//		}
//		obtn.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
//		obtn.setTextSize(26);
		if (wPos < llRecordCount) {
			obtn.setText(getButtonValue(wPos));
			obtn.setEnabled(true);
			obtn.setTextColor(getResources().getColor(R.color.text));
		} else {
			obtn.setText("");
			obtn.setEnabled(false);
		}
		obtn.setPadding(10, 10, 10, 10);

		obtn.setOnClickListener(new OnClickListener(){

	        @Override
	        public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 住所 市町村 第4階層以降選択禁止 Start -->
				if(DrivingRegulation.CheckDrivingRegulation()){
					return;
				}
// ADD 2013.08.08 M.Honma 走行規制 住所 市町村 第4階層以降選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
	        	doClick(oView,wPos);
	        }


	    });
		//上にスクロールの場合
		if (wOldPos > wPos) {
			if (wPos < getCount() && ONCE_GET_COUNT != 0
					&& wPos >= ONCE_GET_COUNT && (wPos % ONCE_GET_COUNT == 0 ||
							(getRecordIndex != wPos / ONCE_GET_COUNT))) {
	//			getRecordIndex = wPos/ONCE_GET_COUNT;
				//上にスクロール場合
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview  3==== wPos===="
//						+ wPos + ",==getRecordIndex==" + getRecordIndex
//						+ ",==(wPos / ONCE_GET_COUNT)=="
//						+ (wPos / ONCE_GET_COUNT));
				RecIndex = wOldPos-ONCE_GET_COUNT;
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"44 wOldPos > wPos  ==== RecIndex====" + RecIndex);
				getList(RecIndex);
			}
		}
		wOldPos = wPos;

		return oView;
	}
	private String getButtonValue(int wPos) {

		int showIndex = getShowIndex(wPos);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"44 showIndex=========" + showIndex + "===========" + m_Poi_Show_Listitem[showIndex].getM_Name());
		return m_Poi_Show_Listitem[showIndex].getM_Name();
	}

	private int getShowIndex(int wPos) {
		int pageIndex  = wPos/ONCE_GET_COUNT;

		if (wPos >= ONCE_GET_COUNT*2) {
			wPos = wPos-ONCE_GET_COUNT*pageIndex+ONCE_GET_COUNT;
		}
		return wPos;
	}
	Button clickBtn = null;
	POI_Address_ListItem poi = new POI_Address_ListItem();
	private void doClick(View oView,int wPos) {
		clickBtn = (Button)oView;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"clickBtn.isFocusable()===" + clickBtn.isFocusable() +
//				",clickBtn.isFocused()===" + clickBtn.isFocused());
		OKBtnFlag = false;
//		initListSelectedFalse();
//		setButtonSelected(clickBtn,true);
//		POI_Address_ListItem poi = new POI_Address_ListItem();
		wOldPos = 0;
		int showIndex = getShowIndex(wPos);
		RecIndex = wPos;
		getList(RecIndex);
		poi = m_Poi_Show_Listitem[showIndex];
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"Treeindex.lcount==" + Treeindex.lcount);
//		if (Treeindex.lcount == 5 || Treeindex.lcount == 4) {
		if (poi.getM_iNextCategoryFlag() == 1) {


			Intent oIntent = new Intent(getIntent());
			oIntent.setClass(this,AddressResultInquiry.class);
			String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY) +
			Constants.FLAG_TITLE +
			clickBtn.getText().toString();
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"title========" + title);

			String[] list = new String [STRINGLEN];
			list[INDEX_0] = String.valueOf(poi.getM_lLongitude());
			list[INDEX_1] = String.valueOf(poi.getM_lLatitude());
//			list[INDEX_2] = m_Poi_Add_Listitem[wPos%this.getCountInBox()].getM_Name();
//			TextView oText = (TextView) this.findViewById(R.id.inquiry_title);

//			list[INDEX_2] = oText.getText().toString();
			list[INDEX_2] = title.replace(Constants.FLAG_TITLE, " ");
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"list====long==" + list[INDEX_0]+ "=====lat==" + list[INDEX_1]);
			hashSelectedPoi.add(list);
			oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
			oIntent.putExtra(Constants.PARAMS_CLICK_LAT, poi.getM_lLatitude());
			oIntent.putExtra(Constants.PARAMS_CLICK_LON, poi.getM_lLongitude());
			oIntent.putExtra(Constants.PARAMS_TREERECINDEX, (long)wPos);
			//NaviLog.d(NaviLog.PRINT_LOG_TAG,"tttt onclick wPos====" + wPos);
			oIntent.putExtra(Constants.PARAMS_CLICK_NAME, poi.getM_Name());
//Chg 2011/09/23 Z01yoneya Start -->
//			this.startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_ADDRESSRESULTINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
			NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_ADDRESSRESULTINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
		} else {

//			startShowPage(SHOWCANCELBTN);
//Add 2012/02/07 katsuta Start --> #2698
//			NaviLog.d(NaviLog.PRINT_LOG_TAG, "AddressResultInquiry doClick ================== bIsListClicked: " + bIsListClicked);
   			if (bIsListClicked) {
   				return;
   			}
   			else {
   				bIsListClicked = true;
   			}
//Add 2012/02/07 katsuta End <--#2698
			MoveMapto();

		}
//		} else {
//			POI_Address_ListItem poi = m_Poi_Add_Listitem[wPos%getCountInBox()];
//            long lon = poi.getM_lLongitude();
//            long lat = poi.getM_lLatitude();
//            String address = poi.getM_Name();
//
//            openMap(null, lon, lat, address);
//		}

	}



//	private void setButtonSelected(Button clickBtn, boolean b) {
//		clickBtn.setSelected(b);
//		if (b){
//			clickBtn.setTextColor(0xffffffff);
//		} else {
//			clickBtn.setTextColor(0xff000000);
//		}
//
//	}

	private void getList(Long lRecIndex) {
		JNILong Rec = new JNILong();
		if (lRecIndex <=0 ){
			lRecIndex = (long)0;
		}
		initOnceGetList();
		getRecordIndex = (int) (lRecIndex / ONCE_GET_COUNT);
		if (ONCE_GET_COUNT != 0 && lRecIndex % ONCE_GET_COUNT != 0) {
			lRecIndex = lRecIndex/ONCE_GET_COUNT*ONCE_GET_COUNT;
		}
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecList(lRecIndex,
				ONCE_GET_COUNT, m_Poi_Add_Listitem, Rec);// Rec 有効なリスト数、例えば：50中、10だけあるはRec=10;
		resetShowList(lRecIndex,Rec);
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"11 lRecIndex===" + lRecIndex);
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"11 Rec===" + Rec.lcount);
//		search();
	}

	private void resetShowList(Long lRecIndex,JNILong Rec) {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"44 lRecIndex==============" + lRecIndex);
		if (lRecIndex < ONCE_GET_COUNT) {
			int len = m_Poi_Add_Listitem.length;
			for (int i = 0 ; i < len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_Add_Listitem[i];
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"lRecIndex < ONCE_GET_COUNT i==============" + i);
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"lRecIndex < ONCE_GET_COUNT m_Poi_Show_Listitem[0]====" + m_Poi_Show_Listitem[0].getM_Name());
//				if (m_Poi_Show_Listitem[15].getM_Name() != null) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"m_Poi_Show_Listitem[15]====" + m_Poi_Show_Listitem[15].getM_Name());
//				}
			}
		} else {
			if (lRecIndex >= ONCE_GET_COUNT*2) {
				for (int i = m_Poi_Add_Listitem.length-ONCE_GET_COUNT; i < ONCE_GET_COUNT*2 ; i++) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"lRecIndex >= ONCE_GET_COUNT*2 resetShowList i===" + i);
					if (i <ONCE_GET_COUNT) {
						m_Poi_Show_Listitem[i] = m_Poi_Show_Listitem[i+ONCE_GET_COUNT];
					} else {
						m_Poi_Show_Listitem[i] = new POI_Address_ListItem();
					}
				}
			}

			int len = (int)Rec.lcount;
			int j = 0;
			for (int m = ONCE_GET_COUNT ; m < ONCE_GET_COUNT + len ; m++) {
				m_Poi_Show_Listitem[m] = m_Poi_Add_Listitem[j];
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"44 i====" + m + "===========" + m_Poi_Show_Listitem[m].getM_Name());
				j++;
			}
//			if (lRecIndex == ONCE_GET_COUNT) {
//				getList((long)0);
//			}

		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"m_Poi_Show_Listitem[0]====" + m_Poi_Show_Listitem[0].getM_Name());
	}
	OnClickListener OnOKListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 住所 確定禁止 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
//ADD 2013.08.08 M.Honma 走行規制 住所 確定禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
			OKBtnFlag = true;
//Add 2012/02/07 katsuta Start --> #2698
//			NaviLog.d(NaviLog.PRINT_LOG_TAG, "AddressResultInquiry onClick ================== bIsListClicked: " + bIsListClicked);
   			if (bIsListClicked) {
   				return;
   			}
   			else {
   				bIsListClicked = true;
   			}
//Add 2012/02/07 katsuta End <--#2698
			MoveMapto();
		}};
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if (keyCode == KeyEvent.KEYCODE_BACK) {
//			hashSelectedPoi.remove(hashSelectedPoi.size()-1);
//			JNIGoBack();
//
////			oBox.refresh();
//		}
//		return super.onKeyDown(keyCode, event);
//	}

	@Override
	protected boolean onClickGoMap() {

//		JNIGoBack();
//		JNIGoBack();
//		JNIGoBack();
//		JNIGoBack();
//		if (Treeindex.lcount > 2) {
//			JNIGoBack();
//		}
//		if (Treeindex.lcount > 1) {
//			JNIGoBack();
//		}
//		if (Treeindex.lcount > 0){
//			JNIGoBack();
//		}
		hashSelectedPoi.clear();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddrKey_Clear();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_Clear();
		return super.onClickGoMap();
	}

	@Override
	protected boolean onClickGoBack() {
//		JNIGoBack();
		return super.onClickGoBack();
	}

	private void JNIGoBack() {
//		bIsRefresh = false;
		hashSelectedPoi.remove(hashSelectedPoi.size()-1);
		if (Treeindex == null) {
			Treeindex = new JNILong();
		}
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetPrevTreeIndex(
				Treeindex);
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchPrevList();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent oIntent) {
		if (requestCode == AppInfo.ID_ACTIVITY_ADDRESSRESULTINQUIRY) {
			this.setResult(resultCode, oIntent);
// Add by CPJsunagawa '13-12-25 Start
			// RESULTCODE_CHECK_PLACEが結果であれば即落とす
			//if (resultCode == Constants.	RESULTCODE_VEHICLE_ROUTE_DETERMINATION)
			if (resultCode == Constants.RESULTCODE_CHECK_PLACE)
			{
				finish();
			}
			else
			{
				JNIGoBack();
			}
			//JNIGoBack();
// Add by CPJsunagawa '13-12-25 End
		}
		super.onActivityResult(requestCode, resultCode, oIntent);
	}

	@Override
	protected boolean onStartShowPage() throws Exception {
		//メモリを申請する
		initDataObj();
		//総件数を取得する
		initTreeIndex();
		RecIndex = 0;
		search();
		return true;
	}

	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {

		if(bGetData)
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					initDialog();
				}


			});
		}

	}

	private void MoveMapto() {
		if (!OKBtnFlag) {
		Intent oIntent = new Intent(getIntent());

// Add by CPJsunagawa '13-12-25 Start
		// ここで、住所検索によるマッチングであるフラグがあった場合、
		// extraにINDEXを入れて返してゆく
		if (oIntent.hasExtra(Constants.ADDRESS_SEARCH_FOR_MATCH))
		{
			int index = oIntent.getIntExtra(Constants.ADDRESS_SEARCH_FOR_MATCH, -1);
			if (index >= 0)
			{
				
				oIntent.putExtra(Constants.CHANGE_COORD_LIST_INDEX, index);
				oIntent.putExtra(Constants.INTENT_LONGITUDE, poi.getM_lLongitude());
				oIntent.putExtra(Constants.INTENT_LATITUDE, poi.getM_lLatitude());
				//setResult(Constants.RESULTCODE_VEHICLE_ROUTE_DETERMINATION, oIntent);
				setResult(Constants.RESULTCODE_CHECK_PLACE, oIntent);
				finish();
				return;
			}
		}
// Add by CPJsunagawa '13-12-25 End 
			
// Add by CPJsunagawa '2015-07-08 Start
		// アイコン登録であるフラグがあった場合、extraにINDEX、選択された位置情報を返す
		if (oIntent.hasExtra(Constants.REGIST_ICON))
		{
			int index = oIntent.getIntExtra(Constants.REGIST_ICON, -1);
			if (index >= 0)
			{
				oIntent.putExtra(Constants.SELECT_ICON_INDEX, index);
				oIntent.putExtra(Constants.INTENT_LONGITUDE, poi.getM_lLongitude());
				oIntent.putExtra(Constants.INTENT_LATITUDE, poi.getM_lLatitude());
				setResult(Constants.RESULTCODE_REGIST_ICON, oIntent);
				finish();
				return;
			}
		}

		// テキスト入力であるフラグがあった場合、extraにINDEX、選択された位置情報を返す
		if (oIntent.hasExtra(Constants.INPUT_TEXT))
		{
			int index = oIntent.getIntExtra(Constants.INPUT_TEXT, -1);
			if (index >= 0)
			{
				oIntent.putExtra(Constants.SELECT_ICON_INDEX, index);
				oIntent.putExtra(Constants.INTENT_LONGITUDE, poi.getM_lLongitude());
				oIntent.putExtra(Constants.INTENT_LATITUDE, poi.getM_lLatitude());
				setResult(Constants.RESULTCODE_INPUT_TEXT, oIntent);
				finish();
				return;
			}
		}
// Add by CPJsunagawa '2015-07-08 End
			
		String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY) +
		Constants.FLAG_TITLE +
		clickBtn.getText().toString();

//		POI_Address_ListItem poi = m_Poi_Add_Listitem[wPos%getCountInBox()];
//        long lon = poi.getM_lLongitude();
//        long lat = poi.getM_lLatitude();
//        String address = poi.getM_Name();
//        openMap(null, lon, lat, address);

        POIData oData = new POIData();
//        oData.m_sName = poi.getM_Name();
// Mod -- 2011/09/20 -- H.Fukushima --- ↓ ----- MarketV2 No.131
        if (Treeindex.lcount == 7) {
//      if (Treeindex.lcount == 6) {
// Mod -- 2011/09/20 -- H.Fukushima --- ↑ ----- MarketV2 No.131

        	int lastIndex = title.lastIndexOf(Constants.FLAG_TITLE);//.replace(Constants.FLAG_TITLE, " ");
        	title = title.substring(0, lastIndex) + "-"  + title.substring(lastIndex+1, title.length());
        	lastIndex = title.lastIndexOf(Constants.FLAG_TITLE);//.replace(Constants.FLAG_TITLE, " ");
        	if (AppInfo.isMatches(title.substring(lastIndex-1,lastIndex))) {
        		title = title.substring(0, lastIndex) + "-"  + title.substring(lastIndex+1, title.length());
        	}

// Mod -- 2011/09/20 -- H.Fukushima --- ↓ ----- MarketV2 No.131
        } else if (Treeindex.lcount == 6) {
//      } else if (Treeindex.lcount == 5) {
// Mod -- 2011/09/20 -- H.Fukushima --- ↑ ----- MarketV2 No.131
        	int lastIndex = title.lastIndexOf(Constants.FLAG_TITLE);//.replace(Constants.FLAG_TITLE, " ");
        	title = title.substring(0, lastIndex) + "-"  + title.substring(lastIndex+1, title.length());
        }
        oData.m_sName = title.replace(Constants.FLAG_TITLE, " ");

//        Toast otoast = Toast.makeText(this, null, Toast.LENGTH_SHORT);
//        otoast.setText(title);
//        otoast.show();
        oData.m_sAddress =  oData.m_sName ;
        oData.m_wLong = poi.getM_lLongitude();
        oData.m_wLat = poi.getM_lLatitude();
        // XuYang add start #1056
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            CommonLib.setChangePosFromSearch(true);
            CommonLib.setChangePos(false);
        } else {
            CommonLib.setChangePosFromSearch(false);
            CommonLib.setChangePos(false);
        }
        // XuYang add end #1056
        OpenMap.moveMapTo(AddressResultInquiry.this, oData, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
		} else {
			//NaviLog.d(NaviLog.PRINT_LOG_TAG,"result 238 Treeindex.lcount==" + Treeindex.lcount);
//			Intent oIntent = getIntent();
//            long lon = oIntent.getLongExtra(Constants.PARAMS_CLICK_LON, 0);
//            long lat = oIntent.getLongExtra(Constants.PARAMS_CLICK_LAT, 0);
//            String address = oIntent.getStringExtra(ChashSelectedPoi.size()-1M;);
            String[] list = hashSelectedPoi.get(hashSelectedPoi.size()-1);

// Add by CPJsunagawa '13-12-25 Start
			// ここで、住所検索によるマッチングであるフラグがあった場合、
			// extraにINDEXを入れて返してゆく
			Intent oIntent = getIntent();
			if (oIntent.hasExtra(Constants.ADDRESS_SEARCH_FOR_MATCH))
			{
				int index = oIntent.getIntExtra(Constants.ADDRESS_SEARCH_FOR_MATCH, -1);
				if (index >= 0)
				{
					
					oIntent.putExtra(Constants.CHANGE_COORD_LIST_INDEX, index);
					oIntent.putExtra(Constants.INTENT_LONGITUDE, Long.parseLong(list[INDEX_0]));
					oIntent.putExtra(Constants.INTENT_LATITUDE, Long.parseLong(list[INDEX_1]));
					//setResult(Constants.RESULTCODE_VEHICLE_ROUTE_DETERMINATION, oIntent);
					setResult(Constants.RESULTCODE_CHECK_PLACE, oIntent);
					finish();
					return;
				}
			}
// Add by CPJsunagawa '13-12-25 Start

// Add by CPJsunagawa '2015-07-08 Start
			// アイコン登録であるフラグがあった場合、extraにINDEX、選択された位置情報を返す
			if (oIntent.hasExtra(Constants.REGIST_ICON))
			{
				int index = oIntent.getIntExtra(Constants.REGIST_ICON, -1);
				if (index >= 0)
				{
					oIntent.putExtra(Constants.SELECT_ICON_INDEX, index);
					oIntent.putExtra(Constants.INTENT_LONGITUDE, poi.getM_lLongitude());
					oIntent.putExtra(Constants.INTENT_LATITUDE, poi.getM_lLatitude());
					setResult(Constants.RESULTCODE_REGIST_ICON, oIntent);
					finish();
					return;
				}
			}

			// テキスト入力であるフラグがあった場合、extraにINDEX、選択された位置情報を返す
			if (oIntent.hasExtra(Constants.INPUT_TEXT))
			{
				int index = oIntent.getIntExtra(Constants.INPUT_TEXT, -1);
				if (index >= 0)
				{
					oIntent.putExtra(Constants.SELECT_ICON_INDEX, index);
					oIntent.putExtra(Constants.INTENT_LONGITUDE, poi.getM_lLongitude());
					oIntent.putExtra(Constants.INTENT_LATITUDE, poi.getM_lLatitude());
					setResult(Constants.RESULTCODE_INPUT_TEXT, oIntent);
					finish();
					return;
				}
			}
// Add by CPJsunagawa '2015-07-08 End
			
//            long lon = Long.parseLong(list[INDEX_0]);
//            long lat = Long.parseLong(list[INDEX_1]);
//            String address = list[INDEX_2];
//            NaviLog.d(NaviLog.PRINT_LOG_TAG,"list====long==" + list[INDEX_0]+ "=====lat==" + list[INDEX_1]);
//            openMap(null, lon, lat, address);

            POIData oData = new POIData();
            oData.m_sName = list[INDEX_2];
            oData.m_sAddress = list[INDEX_2];
            oData.m_wLong = Long.parseLong(list[INDEX_0]);
            oData.m_wLat = Long.parseLong(list[INDEX_1]);
         // XuYang add start #1056
            Intent intent = getIntent();
            if (null != intent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
                CommonLib.setChangePosFromSearch(true);
                CommonLib.setChangePos(false);
            } else {
                CommonLib.setChangePosFromSearch(false);
                CommonLib.setChangePos(false);
            }
            // XuYang add end #1056
            OpenMap.moveMapTo(AddressResultInquiry.this, oData, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
		}
		this.createDialog(Constants.DIALOG_WAIT, -1);
	}
//	private void initListSelectedFalse() {
//		int iCount = oList.getChildCount();
//		for (int i = 0 ; i < iCount; i++) {
//			LinearLayout oLayout = (LinearLayout)oList.getChildAt(i);
//			Button obtn = (Button) oLayout.findViewById(R.id.btnOk);
//			obtn.setSelected(false);
//			obtn.setTextColor(0xff000000);
//		}
//	}
}
