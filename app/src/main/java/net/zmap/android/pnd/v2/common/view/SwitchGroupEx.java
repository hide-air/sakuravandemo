// ADD 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 Start -->
package net.zmap.android.pnd.v2.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ToggleButton;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.DrivingRegulation;
import net.zmap.android.pnd.v2.common.utils.ScreenAdapter;

/**
 *  @file		SwitchGroupEx
 *  @brief		走行規制用 スイッチグループ
 *
 *  @attention
 *  @note
 *
 *  @author		M.Honma
 *  @date		$Date:: 2013-09-06 00:00:00 +0900 #$ (Create at 2013-09-06)
 *  @version	$Revision: $ by $Author: $
 *
 */
public class SwitchGroupEx extends SwitchGroup
{
	public SwitchGroupEx(Context oContext)
	{
		super(oContext);
	}
	
	public SwitchGroupEx(Context oContext, AttributeSet oSet)
	{
		super(oContext, oSet);
	}
	
	@Override
	public void setSize(int wSize)
	{
		if(wSize > 0)
		{
			removeAllViews();

			ToggleButton oButton;
//Chg 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//			DisplayMetrics dm = getResources().getDisplayMetrics();
//			int intMargin = (int) dm.density * 5;
//------------------------------------------------
			int intMargin = (int) ScreenAdapter.getScale() * 5;
//Chg 2011/08/11 Z01thedoanh (自由解像度対応) End <--

			LinearLayout.LayoutParams oParams = new LinearLayout.LayoutParams(
					intMargin, LinearLayout.LayoutParams.FILL_PARENT);
			LinearLayout.LayoutParams oParams1 = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT, intMargin);

			for(int i = 0;i < wSize;++i)
			{
				final int wIndex = i;
// CHG 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 Start -->
				//oButton = new ToggleButton(getContext());
				oButton = new ToggleButtonEx(getContext());
// CHG 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 End <--
				if(m_wResId != null && m_wResId[i] != -1)
				{
					oButton.setBackgroundResource(m_wResId[i]);
				}
				oButton.setOnCheckedChangeListener(new OnCheckedChangeListener(){

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked)
					{
						if(isChecked)
						{
							setSelected(wIndex);
							buttonView.setTextColor(getResources().getColor(R.color.switch_on));
						}
						else
						{
							buttonView.setTextColor(getResources().getColor(R.color.switch_off));
						}
					}

				});
				oButton.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View oView)
					{
// ADD 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 Start -->
						if(DrivingRegulation.CheckDrivingRegulation()){
							return;
						}
// ADD 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 End <--
						ToggleButton oButton = (ToggleButton)oView;
						if(!oButton.isChecked())
						{
							oButton.setChecked(true);
						}
					}

				});
				if(m_wText != null && m_wText[i] != -1)
				{
					oButton.setText(getResources().getString(m_wText[i]));
					oButton.setTextOff(getResources().getString(m_wText[i]));
					oButton.setTextOn(getResources().getString(m_wText[i]));
				}
				else
				{
					oButton.setText("");
					oButton.setTextOff("");
					oButton.setTextOn("");
				}
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
				oButton.setPadding(0, 0, 0, 0);
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
				oButton.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimension(R.dimen.Common_Huge_textSize));
				oButton.postInvalidate();
//Chg 2011/08/15 Z01_h_yamada Start -->
//				addView(oButton);
//--------------------------------------------
				addView(oButton, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT, 1));
//Chg 2011/08/15 Z01_h_yamada End <--
				m_ovList.addElement(oButton);
				if (flgBtnWidth == 0) {
					// ボタン横表示の時、
					if(i != wSize - 1)
					{
						LinearLayout oLayout = new LinearLayout(getContext());
						addView(oLayout,oParams);
					}
				} else if (flgBtnWidth == 1) {
					// ボタン縦表示
					if(i != wSize - 1)
					{
						LinearLayout oLayout = new LinearLayout(getContext());
						addView(oLayout,oParams1);
					}
				}
			}
		}
	}
	
	/**
	 *  @file		ToggleButtonEx
	 *  @brief		走行規制用 トグルボタン
	 *
	 *  @attention
	 *  @note
	 *
	 *  @author		M.Honma
	 *  @date		$Date:: 2013-09-06 00:00:00 +0900 #$ (Create at 2013-09-06)
	 *  @version	$Revision: $ by $Author: $
	 *
	 */
	public class ToggleButtonEx extends ToggleButton
	{
		  public ToggleButtonEx(Context context, AttributeSet attrs)
		  {
			  super(context, attrs);
		  }
		  public ToggleButtonEx(Context context)
		  {
			  super(context);
		  }
		  
		  @Override
		  public void toggle()
		  {
			  // 走行規制する場合は終了する
			  if(DrivingRegulation.GetDrivingRegulationFlg()){
				  return;
			  }
			  super.toggle();
		  }
	}
}

//ADD 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 End <--