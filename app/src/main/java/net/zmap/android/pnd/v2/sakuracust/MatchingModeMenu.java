package net.zmap.android.pnd.v2.sakuracust;

import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.activity.MenuBaseActivity;
import net.zmap.android.pnd.v2.common.utils.DbHelper;
import net.zmap.android.pnd.v2.sakuracust.CommonInquiryOps;

public class MatchingModeMenu extends MenuBaseActivity
	implements OnClickListener{
	private Button mBtnMatchFromMap = null;
	private Button mBtnMatchFromAdr = null;
	private Button mBtnMatchConf = null;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		LayoutInflater oInflater = LayoutInflater.from(this);
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.matching_mode, null);	
		
		// コントロールのインスタンス
		mBtnMatchFromMap = (Button)oLayout.findViewById(R.id.matching_frommap);
		mBtnMatchFromAdr = (Button)oLayout.findViewById(R.id.matching_fromadr);
		mBtnMatchConf  = (Button)oLayout.findViewById(R.id.matching_confirmation);
		
		// イベント定義
		mBtnMatchFromMap.setOnClickListener(this);
		mBtnMatchFromAdr.setOnClickListener(this);
		mBtnMatchConf.setOnClickListener(this);
		
		setViewInWorkArea(oLayout);
	}
	
	protected Dialog onCreateDialog(int id) {
		return super.onCreateDialog(id);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.equals(mBtnMatchFromMap))
		{
			// 常に地図から
			setResult(AppInfo.ID_ACTIVITY_DRICON_MATCHFROMMAP);
		}
		else if (v.equals(mBtnMatchFromAdr))
		{
			// 常に住所から
			setResult(AppInfo.ID_ACTIVITY_DRICON_MATCHFROMADR);
		}
		else if (v.equals(mBtnMatchConf))
		{
			// 都度確認する
			setResult(AppInfo.ID_ACTIVITY_DRICON_MATCHCONF);
		}
		finish();
	}
}
