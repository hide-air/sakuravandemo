package net.zmap.android.pnd.v2.api;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 座標クラス
 */
public final class GeoPoint implements Parcelable {
    private static final double MAX_LATITUDE     = 90.0d;
    private static final double MIN_LATITUDE     = -90.0d;
    private static final double MAX_LONGITUDE    = 180.0d;
    private static final double MIN_LONGITUDE    = -180.0d;

    private static final long   MAX_LATITUDE_MS  = 324000000L;
    private static final long   MIN_LATITUDE_MS  = -324000000L;
    private static final long   MAX_LONGITUDE_MS = 648000000L;
    private static final long   MIN_LONGITUDE_MS = -648000000L;

    private double              mLatitude;
    private double              mLongitude;
    private long                mLatitudeMs;
    private long                mLongitudeMs;

// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
    private boolean[]           mWorldPoint = new boolean[1];
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END
    /**
     * コンストラクタ
     */
    public GeoPoint() {

    }

    /**
     * コンストラクタ
     *
     * @param point
     *            座標
     */
    public GeoPoint(GeoPoint point) {
        if (point != null) {
            mLatitude = point.getLatitude();
            mLongitude = point.getLongitude();
            mLatitudeMs = point.getLatitudeMs();
            mLongitudeMs = point.getLongitudeMs();
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
        	mWorldPoint[0] = point.getWorldPoint();
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END
        }
    }

    /**
     * コンストラクタ
     *
     * @param latitude
     *            緯度 (度)
     * @param longitude
     *            経度 (度)
     */
    public GeoPoint(double latitude, double longitude) {
        setLatitude(latitude);
        setLongitude(longitude);
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
        // デフォルトは世界測地系
        setWorldPoint(true);
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END
    }

    /**
     * コンストラクタ
     *
     * @param latitude
     *            緯度 (ミリ秒)
     * @param longitude
     *            経度 (ミリ秒)
     */
    public GeoPoint(long latitude, long longitude) {
        setLatitudeMs(latitude);
        setLongitudeMs(longitude);
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
        // デフォルトは世界測地系
        setWorldPoint(true);
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END
    }
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
    /**
     * コンストラクタ
     *
     * @param latitude
     *            緯度 (度)
     * @param longitude
     *            経度 (度)
     * @param worldPoint
     *            測地系 (true: 世界測地系, false: 日本測地系)
     */
    public GeoPoint(double latitude, double longitude, boolean worldPoint) {
        setLatitude(latitude);
        setLongitude(longitude);
    	setWorldPoint(worldPoint);
    }

    /**
     * コンストラクタ
     *
     * @param latitude
     *            緯度 (ミリ秒)
     * @param longitude
     *            経度 (ミリ秒)
     * @param worldPoint
     *            測地系 (true: 世界測地系, false: 日本測地系)
     */
    public GeoPoint(long latitude, long longitude, boolean worldPoint) {
        setLatitudeMs(latitude);
        setLongitudeMs(longitude);
    	setWorldPoint(worldPoint);
    }
    /**
     * 測地系取得
     *
     * @return 測地系 (true: 世界測地系, false: 日本測地系)
     */
    public boolean getWorldPoint() {
        return mWorldPoint[0];
    }

    /**
     * 測地系設定
     *
     * @param worldPoint
     *         測地系 (true: 世界測地系, false: 日本測地系)
     */
    public void setWorldPoint(boolean worldPoint) {
        mWorldPoint[0] = worldPoint;
    }
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END

    /**
     * 緯度 (度)取得
     *
     * @return 緯度 (度)
     */
    public double getLatitude() {
        return mLatitude;
    }

    /**
     * 緯度 (度)設定
     *
     * @param latitude
     *            緯度 (度)
     */
    public void setLatitude(double latitude) {
        if (!isValidLatitude(latitude)) {
            throw new IllegalArgumentException();
        }
        mLatitude = latitude;
        mLatitudeMs = toMilliseconds(latitude);
    }

    /**
     * 経度 (度)取得
     *
     * @return 経度 (度)
     */
    public double getLongitude() {
        return mLongitude;
    }

    /**
     * 経度 (度)設定
     *
     * @param longitude
     *            経度 (度)
     */
    public void setLongitude(double longitude) {
        if (!isValidLongitude(longitude)) {
            throw new IllegalArgumentException();
        }
        mLongitude = longitude;
        mLongitudeMs = toMilliseconds(longitude);
    }

    /**
     * 緯度(ミリ秒)取得
     *
     * @return 緯度 (ミリ秒)
     */
    public long getLatitudeMs() {
        return mLatitudeMs;
    }

    /**
     * 緯度(ミリ秒)設定
     *
     * @param latitude
     *            緯度 (ミリ秒)
     */
    public void setLatitudeMs(long latitude) {
        if (!isValidLatitudeMs(latitude)) {
            throw new IllegalArgumentException();
        }
        mLatitudeMs = latitude;
        mLatitude = toDegrees(latitude);
    }

    /**
     * 経度(ミリ秒)取得
     *
     * @return 経度 (ミリ秒)
     */
    public long getLongitudeMs() {
        return mLongitudeMs;
    }

    /**
     * 経度(ミリ秒)設定
     *
     * @param longitude
     *            経度 (ミリ秒)
     */
    public void setLongitudeMs(long longitude) {
        if (!isValidLongitudeMs(longitude)) {
            throw new IllegalArgumentException();
        }
        mLongitudeMs = longitude;
        mLongitude = toDegrees(longitude);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(mLatitudeMs ^ (mLatitudeMs >>> 32));
        result = prime * result + (int)(mLongitudeMs ^ (mLongitudeMs >>> 32));
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
        result = prime * result + (mWorldPoint[0] ? 0 : 1);
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        GeoPoint other = (GeoPoint)obj;
        if (mLatitudeMs != other.mLatitudeMs) return false;
        if (mLongitudeMs != other.mLongitudeMs) return false;
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
        if (mWorldPoint[0] != other.mWorldPoint[0]) return false;
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END
        return true;
    }

    private GeoPoint(Parcel in) {
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
        mLatitudeMs = in.readLong();
        mLongitudeMs = in.readLong();
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
        in.readBooleanArray(mWorldPoint);
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END
    }

    public static final Parcelable.Creator<GeoPoint> CREATOR = new Parcelable.Creator<GeoPoint>() {
        @Override
        public GeoPoint createFromParcel(Parcel in) {
            return new GeoPoint(in);
        }

        @Override
        public GeoPoint[] newArray(int size) {
            return new GeoPoint[size];
        }
    };

    /* (非 Javadoc)
     * @see android.os.Parcelable#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /* (非 Javadoc)
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeDouble(mLatitude);
        out.writeDouble(mLongitude);
        out.writeLong(mLatitudeMs);
        out.writeLong(mLongitudeMs);
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
        out.writeBooleanArray(mWorldPoint);
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END
    }

    private boolean isValidLatitude(double latitude) {
        return latitude <= MAX_LATITUDE && latitude >= MIN_LATITUDE;
    }

    private boolean isValidLongitude(double longitude) {
        return longitude <= MAX_LONGITUDE && longitude >= MIN_LONGITUDE;
    }

    private boolean isValidLatitudeMs(long latitude) {
        return latitude <= MAX_LATITUDE_MS && latitude >= MIN_LATITUDE_MS;
    }

    private boolean isValidLongitudeMs(long longitude) {
        return longitude <= MAX_LONGITUDE_MS && longitude >= MIN_LONGITUDE_MS;
    }

    /**
     * ミリ秒→度変換
     *
     * @param milliseconds　ミリ秒
     * @return 度
     */
    public static double toDegrees(long milliseconds) {
        return (double)milliseconds / 3600000.0d;
    }

    /**
     * 度→ミリ秒変換
     *
     * @param degrees
     *            度
     * @return ミリ秒
     */
    public static long toMilliseconds(double degrees) {
        return (long)(degrees * 3600000);
    }
}