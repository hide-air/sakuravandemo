/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNE_LaneGuideInfo.java
 * Description    レーン案内情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNE_LaneGuideInfo {
    private int iValid;
    private int cLeftAddLaneNum;
    private int cRightAddLaneNum;
    private int cLeftReduceLaneNum;
    private int cRightReduceLaneNum;
    private int cEnterLaneNum;
    private int cExitGuideDir;
    private int sExitLaneFlag;
    private int[] aucArrowFlag = null;
    /**
    * Created on 2010/08/06
    * Title:       initUnSAPAServiceCode
    * Description:  関数を初期化します
    * @param1   int len レーン数
    * @return       void

    * @version        1.0
    */
    public void initAucArrowFlag(int len){
        aucArrowFlag = new int[len];
    }
    /**
    * Created on 2010/08/06
    * Title:       getIValid
    * Description:  有効フラグを取得する
    * @param1   無し
    * @return       int

    * @version        1.0
    */
    public int getIValid() {
        return iValid;
    }
    /**
    * Created on 2010/08/06
    * Title:       getCLeftAddLaneNum
    * Description:  左レーン増加数を取得する
    * @param1   無し
    * @return       int

    * @version        1.0
    */
    public int getCLeftAddLaneNum() {
        return cLeftAddLaneNum;
    }
    /**
    * Created on 2010/08/06
    * Title:       getCRightAddLaneNum
    * Description:  右レーン増加数を取得する
    * @param1   無し
    * @return       int

    * @version        1.0
    */
    public int getCRightAddLaneNum() {
        return cRightAddLaneNum;
    }
    /**
    * Created on 2010/08/06
    * Title:       getCLeftReduceLaneNum
    * Description:  左レーン現象数を取得する
    * @param1   無し
    * @return       int

    * @version        1.0
    */
    public int getCLeftReduceLaneNum() {
        return cLeftReduceLaneNum;
    }
    /**
    * Created on 2010/08/06
    * Title:       getCRightReduceLaneNum
    * Description:  右レーン現象数を取得する
    * @param1   無し
    * @return       int

    * @version        1.0
    */
    public int getCRightReduceLaneNum() {
        return cRightReduceLaneNum;
    }
    /**
    * Created on 2010/08/06
    * Title:       getCEnterLaneNum
    * Description:  交差点進入レーン数(0-16)を取得する
    * @param1   無し
    * @return       int

    * @version        1.0
    */
    public int getCEnterLaneNum() {
        return cEnterLaneNum;
    }
    /**
    * Created on 2010/08/06
    * Title:       getCExitGuideDir
    * Description:  脱出方向矢印値(ucharビット値の意味は下のdefineの通り)を取得する
    * @param1   無し
    * @return       int

    * @version        1.0
    */
    public int getCExitGuideDir() {
        return cExitGuideDir;
    }
    /**
    * Created on 2010/08/06
    * Title:       getSExitLaneFlag
    * Description:  自車が脱出するレーン(bit値が1)を取得する
    * @param1   無し
    * @return       int

    * @version        1.0
    */
    public int getSExitLaneFlag() {
        return sExitLaneFlag;
    }
    /**
    * Created on 2010/08/06
    * Title:       getAucArrowFlag
    * Description:  レーンごとの脱出矢印方向(ucharビット値の意味は下のdefineの通り、配列は左レーンから順番)を取得する
    * @param1   無し
    * @return       int[]

    * @version        1.0
    */
    public int[] getAucArrowFlag() {
        return aucArrowFlag;
    }
}
