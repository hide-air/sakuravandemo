package net.zmap.android.pnd.v2.api;

/**
 * ナビサービスインテント アクション・付加情報
 */
public final class NaviIntent {

    /**
     * ナビサービスのアクション名
     */
    public static final String ACTION_NAVI_SERVICE     = "net.zmap.android.pnd.v2.action.NAVI_SERVICE";
    /**
     * 外部からのナビアプリ起動のアクション名
     */
    public static final String ACTION_NAVI_START       = "net.zmap.android.pnd.v2.api.action.NAVI_START";
    /** 外部起動フラグ */
    public static final String EXTRA_INVOKED_BY_AGENT  = "net.zmap.android.pnd.v2.api.extra.invoked_by_agent";

    /**
     * 自車位置更新イベント
     *
     * @see #EXTRA_LOCATION
     */
    public static final String ACTION_VEHICLE_POSITION = "net.zmap.android.navi.intent.action.MY_POSITION";
    /** ロケーション (緯度・経度、世界測地) */
//    public static final String EXTRA_LOCATION             = "net.zmap.android.pnd.v2.api.extra.location";
    public static final String EXTRA_LOCATION          = "net.zmap.android.navi.intent.extra.LOCATION";

    /**
     * 道路種別変更イベント
     *
     * @see #EXTRA_LOCATION
     * @see #EXTRA_ROAD_FROM
     * @see #EXTRA_ROAD_TO
     */
    public static final String ACTION_ROAD_MOVED       = "net.zmap.android.pnd.v2.api.action.ROAD_MOVED";
    /** 旧道路種別 */
    public static final String EXTRA_ROAD_FROM         = "net.zmap.android.pnd.v2.api.extra.road_from";
    /** 新道路種別 */
    public static final String EXTRA_ROAD_TO           = "net.zmap.android.pnd.v2.api.extra.road_to";

    /**
     * 行政界移動イベント
     *
     * @see #EXTRA_LOCATION
     * @see #EXTRA_ADMIN_FROM
     * @see #EXTRA_ADMIN_TO
     */
    public static final String ACTION_AREA_MOVED       = "net.zmap.android.pnd.v2.api.action.AREA_MOVED";
    /** 旧行政界名 */
    public static final String EXTRA_ADMIN_FROM        = "net.zmap.android.pnd.v2.api.extra.admin_from";
    /** 新行政界名 */
    public static final String EXTRA_ADMIN_TO          = "net.zmap.android.pnd.v2.api.extra.admin_to";

    /**
     * 表示地図操作イベント
     *
     * @see #EXTRA_LOCATION
     */
    public static final String ACTION_MAP_CONTROL      = "net.zmap.android.pnd.v2.api.action.MAP_CONTROL";
    public static final String EXTRA_POINT             = "net.zmap.android.pnd.v2.api.extra.POINT";

    /**
     * 指定位置地図表示
     *
     * @see #EXTRA_LOCATION
     */
    public static final String ACTION_VIEW_MAP_CENTER  = "net.zmap.android.pnd.v2.api.action.VIEW_MAP_CENTER";

    /**
     * ルート案内イベント
     *
     * @see #EXTRA_LOCATION
     * @see #EXTRA_GUIDE_TYPE
     * @see #EXTRA_TARGET_TYPE
     * @see #EXTRA_TARGET_NAME
     * @see #EXTRA_TARGET_LOCATION
     * @see #EXTRA_TARGET_DISTANCE
     * @see #EXTRA_TARGET_DIRECTION
     */
    public static final String ACTION_GUIDE            = "net.zmap.android.pnd.v2.api.action.GUIDE";
    /** ルート案内イベント種別 */
    public static final String EXTRA_GUIDE_TYPE        = "net.zmap.android.pnd.v2.api.extra.guide_type";
    /** 目標種別 */
    public static final String EXTRA_TARGET_TYPE       = "net.zmap.android.pnd.v2.api.extra.target_type";
    /** 目標名称 */
    public static final String EXTRA_TARGET_NAME       = "net.zmap.android.pnd.v2.api.extra.target_name";
    /** 目標座標 */
    public static final String EXTRA_TARGET_LOCATION   = "net.zmap.android.pnd.v2.api.extra.target_location";
    /** 目標距離 */
    public static final String EXTRA_TARGET_DISTANCE   = "net.zmap.android.pnd.v2.api.extra.target_distance";
    /** 進行方向種別 */
    public static final String EXTRA_TARGET_DIRECTION  = "net.zmap.android.pnd.v2.api.extra.target_direction";

    /**
     * ルート案内イベント
     *
     */
    public static final String ACTION_ROUTE_CALCULATED = "net.zmap.android.pnd.v2.api.action.ROUTE_CALCULATED";
    /** ルート案内イベント種別 */
    public static final String EXTRA_ROUTE_CALCULATED  = "net.zmap.android.pnd.v2.api.extra.route_calculated";

    /**
     * 検索結果リストイベント
     *
     */
    public static final String ACTION_SEARCH_LIST      = "net.zmap.android.pnd.v2.api.action.SEARCH_LIST";

    public static final String ACTION_STARTED          = "net.zmap.android.pnd.v2.api.action.STARTED";
    public static final String ACTION_STOPPED          = "net.zmap.android.pnd.v2.api.action.STOPPED";

// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応 START
    /**
     * 自車情報通知イベント(拡張版)
     *
     * @see #EXTRA_LOCATION_EX
     */
    public static final String ACTION_VEHICLE_EX_POSITION = "net.zmap.android.navi.intent.action.MY_POSITION_EX";
    /** ロケーション (高速道路種別) */
    public static final String EXTRA_LOCATION_EX     = "net.zmap.android.navi.intent.extra.LOCATION_EX";
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応  END

}