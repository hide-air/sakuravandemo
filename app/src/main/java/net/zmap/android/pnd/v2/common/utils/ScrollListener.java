package net.zmap.android.pnd.v2.common.utils;

public interface ScrollListener
{
	public void moveNextPage();
	public void movePreviousPage();
	public boolean isHasNextPage();
	public boolean isHasPreviousPage();
	public void setControlListener(ScrollControlListener oListener);
}
