package net.zmap.android.pnd.v2.api;

/**
 * サービス接続リスナー
 */
public interface ApiServiceConnection {

    /**
     * サービスの接続通知
     *
     * @param interfaceName
     *            サービスにバインドしているインタフェース名称 (class.getName())
     */
    void onServiceDisconnected(String interfaceName);

    /**
     * サービスの切断通知
     *
     * @param interfaceName
     *            サービスにバインドしているインタフェース名称 (class.getName())
     */
    void onServiceConnected(String interfaceName);
}
