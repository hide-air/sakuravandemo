package net.zmap.android.pnd.v2.api.navigation;

import java.util.ArrayList;
import java.util.List;

import net.zmap.android.pnd.v2.api.exception.NaviInvalidValueException;
import net.zmap.android.pnd.v2.common.utils.NaviLog;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * ルート探索条件
 */
public final class RouteRequest implements Parcelable {

    /** ナビモード: クルマ */
    public static final int  NAVI_MODE_CAR           = 1;
    /** ナビモード: 徒歩 */
    public static final int  NAVI_MODE_MAN           = 2;
    /** ナビモード: アロー */
    public static final int  NAVI_MODE_ARROW         = 3;

    /** 探索種別: なし */
    public static final int  SEARCH_TYPE_NONE        = 0;
    /** 探索種別: 推奨 */
    public static final int  SEARCH_TYPE_RECOMMENDED = 1;
    /** 探索種別: 一般 */
    public static final int  SEARCH_TYPE_GENERAL     = 2;
    /** 探索種別: 距離 */
    public static final int  SEARCH_TYPE_DISTANCE    = 3;
    /** 探索種別: 道幅 */
    public static final int  SEARCH_TYPE_WIDTH       = 4;
    /** 探索種別: 別ルート */
    public static final int  SEARCH_TYPE_ANOTHER     = 5;

    /** 規制種別: なし */
    public static final int  REGULATION_TYPE_NONE    = 0;
    /** 規制種別: フェリー */
    public static final int  REGULATION_TYPE_FERRY   = (1 << 0);
    /** 規制種別: 時間 */
    public static final int  REGULATION_TYPE_TIME    = (1 << 1);
    /** 規制種別: 季節 */
    public static final int  REGULATION_TYPE_SEASON  = (1 << 2);
    /** 規制種別: VICS */
    public static final int  REGULATION_TYPE_VICS    = (1 << 3);

    private RoutePoint       mOrigin;

    private RoutePoint       mDestination;

    private List<RoutePoint> mWaypoints              = new ArrayList<RoutePoint>();

    /** ナビモード */
    private int              mNaviMode               = NAVI_MODE_CAR;

    /** 探索種別 */
    private int              mSearchType             = SEARCH_TYPE_NONE;

    /** 規制種別 */
    private int              mRegulations            = REGULATION_TYPE_NONE;

    /**
     * 開始位置取得
     * @return 開始位置
     */
    public RoutePoint getOrigin() {
        return mOrigin;
    }

    /**
     * 開始位置設定
     * @param origin　開始位置
     */
    public void setOrigin(RoutePoint origin) {
        this.mOrigin = origin;
    }

    /**
     * 目的地取得
     * @return 目的地
     */
    public RoutePoint getDestination() {
        return mDestination;
    }

    /**
     * 目的地設定
     * @param destination 目的地
     */
    public void setDestination(RoutePoint destination) {
        this.mDestination = destination;
    }

    /**
     * 経由地取得
     * @return 経由地リスト
     */
    public List<RoutePoint> getWaypoints() {
        return mWaypoints;
    }

    /**
     * 経由地設定
     * @param waypoints 経由地リスト
     */
    public void setWaypoints(List<RoutePoint> waypoints) {
        this.mWaypoints = waypoints;
    }

    /**
     * 経由地リストの最後に経由地を追加します。
     * @param waypoint 経由地
     */
    public void addWaypoint(RoutePoint waypoint) {
        this.mWaypoints.add(waypoint);
    }

    /**
     * ナビモード取得
     *
     * @return ナビモード
     */
    public int getNaviMode() {
        return mNaviMode;
    }

    /**
     * ナビモード設定
     *
     * @param naviMode
     *            ナビモード
     * @throws NaviInvalidValueException 不正な値、無効値
     */
    public void setNaviMode(int naviMode) throws NaviInvalidValueException {
        if (!validateNaviMode(naviMode)) {
            throw new NaviInvalidValueException();
        }
        mNaviMode = naviMode;
    }

    /**
     * 探索種別取得
     *
     * @return 探索種別
     */
    public int getSearchType() {
        return mSearchType;
    }

    /**
     * 探索種別設定
     *
     * @param searchType
     *            探索種別
     * @throws NaviInvalidValueException 不正な値、無効値
     */
    public void setSearchType(int searchType) throws NaviInvalidValueException {
        if (!validateSearchType(searchType)) {
            throw new NaviInvalidValueException();
        }
        mSearchType = searchType;
    }

    /**
     * 規制種別取得
     *
     * @return 規制種別
     */
    public int getRegulations() {
        return mRegulations;
    }

    /**
     * 規制種別設定
     *
     * @param regulationType
     *            規制種別
     * @throws NaviInvalidValueException 不正な値、無効値
     */
    public void setRegulations(int regulationType) throws NaviInvalidValueException {
        if (!validateRegulationType(regulationType)) {
            throw new NaviInvalidValueException();
        }
        mRegulations = regulationType;
    }

    /**
     * 規制種別ビット追加
     *
     * @param regulationType
     *            規制種別フラグ
     * @throws NaviInvalidValueException 不正な値、無効値
     */
    public void addRegulation(int regulationType) throws NaviInvalidValueException {
        if (!validateRegulationType(regulationType)) {
            throw new NaviInvalidValueException();
        }
        mRegulations |= regulationType;
    }

    /**
     * 規制種別ビット削除
     *
     * @param regulationType
     *            規制種別フラグ
     * @throws NaviInvalidValueException 不正な値、無効値
     */
    public void removeRegulation(int regulationType) throws NaviInvalidValueException {
        if (!validateRegulationType(regulationType)) {
            throw new NaviInvalidValueException();
        }
        mRegulations &= ~regulationType;
    }

    /**
     * ナビモード妥当性チェック
     *
     * @return true:妥当 false:不正
     */
    private boolean validateNaviMode(int naviMode) {
        if (naviMode != NAVI_MODE_CAR &&
                naviMode != NAVI_MODE_MAN &&
                naviMode != NAVI_MODE_ARROW) {
            return false;
        }
        return true;
    }

    /**
     * 探索種別妥当性チェック
     *
     * @param searchType
     *            探索種別
     * @return true:妥当 false:不正
     */
    private static boolean validateSearchType(int searchType) {
        if (searchType != SEARCH_TYPE_NONE &&
                searchType != SEARCH_TYPE_RECOMMENDED &&
                searchType != SEARCH_TYPE_GENERAL &&
                searchType != SEARCH_TYPE_DISTANCE &&
                searchType != SEARCH_TYPE_WIDTH &&
                searchType != SEARCH_TYPE_ANOTHER) {
            return false;
        }
        return true;
    }

    /**
     * 規制種別妥当性チェック
     *
     * @return true:妥当 false:不正
     */
    private boolean validateRegulationType(int regulations) {
        regulations &= ~REGULATION_TYPE_FERRY;
        regulations &= ~REGULATION_TYPE_TIME;
        regulations &= ~REGULATION_TYPE_SEASON;
        regulations &= ~REGULATION_TYPE_VICS;
        return (regulations == 0) ? true : false;
    }

    /*
     * (非 Javadoc)
     */
    private RouteRequest(Parcel in) {
        mOrigin = in.readParcelable(RoutePoint.class.getClassLoader());
        mDestination = in.readParcelable(RoutePoint.class.getClassLoader());
        mWaypoints = in.createTypedArrayList(RoutePoint.CREATOR);
        mNaviMode = in.readInt();
        mSearchType = in.readInt();
        mRegulations = in.readInt();
    }

    /**
     * コンストラクタ
     */
    public RouteRequest() {
    }

    /**
     * コンストラクタ
     * @param original ルート探索条件
     */
    public RouteRequest(RouteRequest original) {
        mOrigin = new RoutePoint(original.getOrigin());
        mDestination = new RoutePoint(original.getDestination());
        for (RoutePoint waypoint : original.getWaypoints()) {
            mWaypoints.add(waypoint);
        }
        mNaviMode = original.getNaviMode();
        mSearchType = original.getSearchType();
        mRegulations = original.getRegulations();
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(mOrigin, flags);
        out.writeParcelable(mDestination, flags);
        out.writeTypedList(mWaypoints);
        out.writeInt(mNaviMode);
        out.writeInt(mSearchType);
        out.writeInt(mRegulations);
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /* (非 Javadoc)
     *
     */
    public static final Parcelable.Creator<RouteRequest> CREATOR = new Parcelable.Creator<RouteRequest>() {
        public RouteRequest createFromParcel(Parcel in) {
            return new RouteRequest(in);
        }

        public RouteRequest[] newArray(int size) {
            return new RouteRequest[size];
        }
    };
}