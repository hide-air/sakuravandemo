/**
 *  @file       GyroSensorData.java
 *  @brief      ジャイロセンサー通知データ
 *
 *  @attention
 *  @note
 *
 *  @author     Yuki Sawada [Z01]
 *  @date       $Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2010-10-13)
 *  @version    $Revision: $ by $Author: $
 *
 */

package net.zmap.android.pnd.v2.data;

public class GyroSensorData {
    public int      accuracy;       ///< 精度
    public long     timestamp;      ///< タイムスタンプ
    public float    gyroAccelX;     ///< X軸周りの角速度
    public float    gyroAccelY;     ///< Y軸周りの角速度
    public float    gyroAccelZ;     ///< Z軸周りの角速度
// Add 2011/02/28 sawada Start -->
    public long		status;			///< センサ状態 (SENSOR_STATUS_*)
// Add 2011/02/28 sawada End   <--

    public int      getAccuracy() {
        return accuracy;
    }
    public long getTimestamp() {
        return timestamp;
    }
    public float getGyroAccelX() {
        return gyroAccelX;
    }
    public float getGyroAccelY() {
        return gyroAccelY;
    }
    public float getGyroAccelZ() {
        return gyroAccelZ;
    }
// Add 2011/02/28 sawada Start -->
	public long getStatus() {
		return status;
	}
// Add 2011/02/28 sawada End   <--
}