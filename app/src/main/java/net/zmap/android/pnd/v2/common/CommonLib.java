/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           CommonLib.java
 * Description    共通class
 * Created on     2009/12/30
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.GeoUri;
import net.zmap.android.pnd.v2.api.NaviResult;
import net.zmap.android.pnd.v2.api.navigation.RoutePoint;
import net.zmap.android.pnd.v2.api.navigation.RouteRequest;
import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.common.activity.BaseActivity;
import net.zmap.android.pnd.v2.common.services.FileService;
import net.zmap.android.pnd.v2.common.utils.GeoUtils;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.ROUTE_DISTANCE;
import net.zmap.android.pnd.v2.data.ZNE_RoutePoint;
import net.zmap.android.pnd.v2.data.ZVP_SatInfoList_t;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.maps.MapView;
import net.zmap.android.pnd.v2.maps.OpenMap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

public class CommonLib {
    private static final String TAG = "CommonLib";
// ADD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応 START
    private static final String DEF_SJIS_COVER = "$";
// ADD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応  END
    // xuyang add start #813
    public static boolean routeSearchFlag = false;
    // xuyang add end #813
    private static boolean bIsOpenMap = false;
//Del 2012/02/23 Z01_h_yamada Start --> #3790
//    // Whether Navigation startup or not
//    private static boolean bIsNaviStart = false;
//Del 2012/02/23 Z01_h_yamada End <--
    // Whether Navigation startup from Intent or not
    private static boolean bIsIntentNaviOpenMapStart = false;
    // For startNaviFromIntent Start
    private static boolean bIsIntentNoNaviOpenMapStart = false;
    private static boolean bIsNaviForeground = false;
    private static boolean bIsBackground = false;
    // For startNaviFromIntent End
    private static boolean bIsIntentNaviRouteDiplay = false;
    private static boolean bIsIntentNavi = false;
    private static boolean bIsSearching = false;
//    private  static boolean bIsRouteCalculating = false;
    private static boolean bIsDemo = false;
//Del 2011/09/26 Z01yoneya Start -->
//    private static boolean bIsFromIntentStart = false;
//Del 2011/09/26 Z01yoneya End <--
    private static boolean bIsRouteDisplay = false;
    private static boolean bIsRouteCancel = false;
//    private  static boolean bIsCarMode = false;
    //Whether the Map's object is mapView or not
    private static boolean bIsMapView = false;
    private static boolean bIsManModeRouteCalcStas = false;
    private static boolean bIsShowGuide = false;
    private static boolean bIsDirectionBoard = false;
    private static boolean bIsShowJct = false;
    private static boolean bIsShowMagMap = false;
    private static boolean bIsAppoachIntersection = false;
    //Whether displaying the magMap's name or not
    private static boolean bIsShowMagMapName = false;
    //Whether the vehicle on highway or not
    private static boolean bIsOnHighWay = false;
    // For GuideInfo isn't synchronization Start
    private static boolean bIsDisplayGuideInfo = false;
    //When navigation stop,Whether Hide guide's information or not
    private static boolean bIsGuideEnd = false;
    //Whether Hide diplayBoard or not
    private static boolean bIsHideGuide = false;
    private static boolean bIsRouteEditCarMode = false;
    private static boolean bIsRouteEditModeChange = false;
    private static boolean bIsNaviEnd = false;
    //For GuideInfo isn't synchronization End
    /***/
    private static boolean isNaviStart;
    private static boolean bIsIntentPoiLogStart = false;
    private static boolean bIsIntentFirstPoiLogStart = false;
    private static boolean bIsApproachHighway = false;

    private static boolean bIsAutoMap = false;
//Del 2011/10/04 Z01yoneya Start -->
//    private static boolean bIsExistsDetailMapData = false;
//    private static boolean bIsExistsWalkerData = false;
////Add 2011/09/16 Z01_h_yamada Start -->
//    private static boolean bIsExistsVICS = false;
////Add 2011/09/16 Z01_h_yamada End <--
//Del 2011/10/04 Z01yoneya End <--

    private static boolean bIsStartNaviFromMapIntent = false;
    private static boolean bIsStartNaviFromINavintent = false;
    private static JNITwoLong latLon = new JNITwoLong();
    //Modify start For Redmine 1660
    private static JNITwoLong MaplatLon = new JNITwoLong();
    //Modify end For Redmine 1660

    // xuyang add start #1782
    private static byte ScaleVlaue;
    public static boolean backMapFlag = false;
    // xuyang add end #1782

//Del 2012/03/13 Z01_h_fukushima(No.405) Start --> ref 2510 案内中画面での地点追加
//  // xuyang add start #1914
//  private  static JNITwoLong mapOldPoint = new JNITwoLong();
//  public static boolean RouteEndNaviFlag = false;
//  // xuyang add end #1914
//Del 2012/03/13 Z01_h_fukushima(No.405) End <-- ref 2510
    private static boolean bIsStartCalculateRoute = false;
	private  static boolean bIsShowDoubleMap = false;

    public	 static boolean bIsModeChanging = false;
    public static String vpLogFileName = null;
    public static String vpLogFileFormatName = null;
//    public static String routeFilePath = new String();
//Del 2011/09/26 Z01yoneya Start -->
//    public static String[] naviResFilePath = new String[10];
//Del 2011/09/26 Z01yoneya End <--
    public static String date = null;

    public static FileWriter fw = null;
    public static FileReader fr = null;
    public static BufferedReader bf = null;
//    private static Calendar c = Calendar.getInstance();

    public static List<String> lstLayerId = new ArrayList<String>();
    //public static List<List<MapIcon>> lstMapIconAll = new ArrayList<List<MapIcon>>();
    public static List<String> lstLayer = new ArrayList<String>();
    //public static MapIcon mapIconInfo = null;

    private static ZVP_SatInfoList_t SatInfoList_t = new ZVP_SatInfoList_t();

    //For locusMap Start
    public static int MAP_VIEW_FLAG = 0;

    public static ArrayList<String> ADD_DATE = new ArrayList<String>();
    public static int angle = 0;
    public static int VOICE_PROGRESS = 0;
    public static boolean bMapColorAuto = false;
    public static String SEARCHP_ROPERTY = null;
    public static int SEARCHP_MODE = 0;

    public static int Start_Demo_Guide_FLAG = 0;
    // 経由地があるかどうかフラグ
    public static boolean isExistThroughPlaceflag = false;
    //for the back function of POI search result
    public static boolean flag = true;
    // XuYang add start
    public static boolean isAround = true;
    // XuYang add end
    public static String strShortcutName = null;

    // XuYang add start
    public static String strShortcutCode = null;

    public static boolean bIsFromRouteEdit = false;

//Add 2011/09/29 Z01yoneya Start -->
    protected static NaviGuideRestoreFile naviGuideRestoreFile = new NaviGuideRestoreFile();

// ADD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる) START
    private static boolean mIsSigleMapActivity = true;
// ADD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる)  END
//Add 2011/09/29 Z01yoneya End <--

    /**
     * @return the strShortcutCode
     */
    public static String getStrShortcutCode() {
        return strShortcutCode;
    }

    /**
     * @param strShortcutCode
     *            the strShortcutCode to set
     */
    public static void setStrShortcutCode(String strShortcutCode) {
        CommonLib.strShortcutCode = strShortcutCode;
    }

    // XuYang add end
    //yangyang add start QA49
    public static String strShortPointCode = null;

    public static String getStrShortPointCode() {
        return strShortPointCode;
    }

    public static void setStrShortPointCode(String strShortPointCode) {
        CommonLib.strShortPointCode = strShortPointCode;
    }

    //yangyang add end QA49

    public static String getShortcutName() {
        return strShortcutName;
    }

    public static void setShortcutName(String ShortcutName) {
        CommonLib.strShortcutName = ShortcutName;
    }

    /** 施設周辺と沿いルート周辺のフラグ */
    public static boolean sbAroundFlg = false;

    /**
     * 施設周辺と沿いルート周辺のフラグ
     *
     * @return
     */
    public static boolean isAroundFlg() {
        return sbAroundFlg;
    }

    public static int BatteryLevel = 0;
    public static boolean BatteryStatusCharging = false;

    // XuYang add start 電波フライトモード フラグ
    public static int SignalFlag = 0;

    // XuYang add end 電波フライトモード フラグ

    /**
     * Created on 2009/12/25
     * Title：setAroundFlg
     * Description: 施設周辺と沿いルート周辺のフラグ
     *
     * @param:無し　
     * @return：boolean
     *
     * @version 1.0
     */
    public static void setAroundFlg(boolean bAroundFlg) {
        sbAroundFlg = bAroundFlg;
    }

    public static boolean sbMiddleKindFlg = false;

    /**
     * Created on 2009/12/25
     * Title：isAroundFlg
     * Description: 施設周辺と沿いルート周辺のフラグ
     *
     * @param:無し　
     * @return：boolean
     *
     * @version 1.0
     */
    public static boolean isMiddleKindFlg() {
        return sbMiddleKindFlg;
    }

    /**
     * Created on 2009/12/25
     * Title：setAroundFlg
     * Description: 施設周辺と沿いルート周辺のフラグ
     *
     * @param:無し　
     * @return：boolean
     *
     * @version 1.0
     */
    public static void setMiddleKindFlg(boolean bMiddleKindFlg) {
        sbMiddleKindFlg = bMiddleKindFlg;
    }

    /**
     * 「現在地を変更」のフラグ
     */
    public static int LOCATION_CHANGE_FLAG = 0;

    /**
     * Created on 2009/12/25
     * Title：isBMapColorAuto
     * Description: 自動色を判断する
     *
     * @param:無し　
     * @return：boolean
     *
     * @version 1.0
     */
    public static boolean isBMapColorAuto() {
        return bMapColorAuto;
    }

    /**
     * Created on 2009/12/25
     * Title：setBMapColorAuto
     * Description: 自動色を設定する
     *
     * @param:無し　
     * @return：boolean
     *
     * @version 1.0
     */
    public static void setBMapColorAuto(boolean mapColorAuto) {
        bMapColorAuto = mapColorAuto;
    }

    /**
     * 密度の値
     * */
    public static float IDENSITY = 0;
    public static double Altitude = 0;

    /**
     * Created on 2009/12/25
     * Title：getIDENSITY
     * Description: IDENSITYを取得する
     *
     * @param:無し　
     * @return：float　
     *
     * @version 1.0
     */
    public static float getIDENSITY() {
        return IDENSITY;
    }

    /**
     * Created on 2009/12/25
     * Title：setIDENSITY
     * Description: IDENSITYを設定する
     *
     * @param:float　
     * @return：無し
     *
     * @version 1.0
     */
    public static void setIDENSITY(float idensity) {
        IDENSITY = idensity;
    }

//Del 2011/09/26 Z01yoneya Start -->
//    public static boolean bLimitUser = false;
//    /**
//     * Created on 2009/12/25
//     * Title：isBLimitUser
//     * Description: 制限の状態を判断する
//     * @param:無し　
//     * @return：boolean　「true:制限なしの場合 false:制限ありの場合」
//
//     * @version 1.0
//     */
//    public static boolean isBLimitUser() {
//        return bLimitUser;
//    }
//    /**
//     * Created on 2009/12/25
//     * Title：setBLimitUser
//     * Description: 制限を設定する
//     * @param:　boolean　「true:制限なしの場合 false:制限ありの場合」
//     * @return　無し　
//
//     * @version 1.0
//     */
//    public static void setBLimitUser(boolean limitUser) {
//        bLimitUser = limitUser;
//    }
//Del 2011/09/26 Z01yoneya End <--

    /**
     * 度、分、秒の座標値をミリ秒に変換する
     *
     * @param wDrgree
     *            度
     * @param wMinute
     *            分
     * @param wSecond
     *            秒
     * @return ミリ秒
     */
    public static int drgreeToMS(int wDrgree, int wMinute, int wSecond) {
        return (wDrgree * 60 * 60 * 1000) + (wMinute * 60 * 1000) + wSecond * 1000;
    }

    /**
     * ミリ秒をMM:DD:SSのフォーマットに変換する
     *
     * @param second_ms
     *            ミリ秒
     * @return MM:DD:SSのフォーマット
     */
    public static int[] msReverse(int second_ms) {
        int[] ret = new int[3];
        int degree, minute, second;
        degree = second_ms / 1000 / 60 / 60;
        minute = (second_ms - degree * 1000 * 60 * 60) / 1000 / 60;
        second = (second_ms - degree * 1000 * 60 * 60 - minute * 1000 * 60) / 1000;
        ret[0] = degree;
        ret[1] = minute;
        ret[2] = second;
        return ret;
    }

    /**
     * ミリ秒をDD.MM.SS.sssのフォーマットに変換する
     *
     * @param second_ms
     *            ミリ秒
     * @return DD.MM.SS.sssのフォーマット(123.45.7.03)
     */
    public static String formatDegree(long second_ms) {
        StringBuilder sb = new StringBuilder();
        long degree = second_ms / 1000 / 60 / 60;
        long minute = (second_ms - degree * 1000 * 60 * 60) / 1000 / 60;
        long second = (second_ms - degree * 1000 * 60 * 60 - minute * 1000 * 60) / 1000;
        long ms = second_ms - (degree * 3600 + minute * 60 + second) * 1000;

        sb.append(degree + ".");
        sb.append(minute + ".");
        sb.append(second + ".");
        if (ms < 100) {
            sb.append("0");
        }
        sb.append(ms);

        return sb.toString();
    }

    /**
     * DD.MM.SS.sssのフォーマットをミリ秒に変換する
     *
     * @param str
     *            DD.MM.SS.sssのフォーマット(123.45.7.03)
     * @return ミリ秒
     */
    public static long reverseDegree(String str) {
        if (str == null) {
            return 0;
        }
        String[] s = str.split("\\.");
        if (s == null || s.length == 0) {
            return 0;
        }
        long degree = 0;
        long minute = 0;
        long second = 0;
        long ms = 0;
        try {
            degree = Long.parseLong(s[0]);
            minute = Long.parseLong(s[1]);
            second = Long.parseLong(s[2]);
            ms = Long.parseLong(s[3]);
        } catch (NumberFormatException e) {
            NaviLog.e("CommonLib", e);
            return 0L;
        }

        return (degree * 3600 + minute * 60 + second) * 1000 + ms;
    }

    /**
     * 度単位の座標値をミリ秒に変換する
     *
     * @param deg
     *            度単位の座標値
     * @return ミリ秒
     */
    public static long degreeToMS(double deg) {
        return (long)(deg * 3600000);
    }

    /**
     * Created on 2009/12/25
     * Title：MSToDegree
     * Description: ミリ秒で表示する座標値を度単位の座標値に変換する
     *
     * @param ms
     *            1/1000単位で表示する座標値
     *
     * @version 1.0
     */

    /**
     * ミリ秒で表示する座標値を度単位の座標値に変換する
     *
     * @param ms
     *            1ミリ秒で表示する座標値
     * @return 度単位の座標値
     */
    public static double MSToDegree(long ms) {
        return ms / 3600000.0;
    }

    /**
     * Created on 2009/12/25
     * Title：GetIntentZoomLevel
     * Description: GoogleMapのスケールをNavigationでのスケールに合わせる
     *
     * @param String
     *            zoom GoogleMapのスケール
     *
     * @version 1.0
     */
    public static int GetIntentZoomLevel(String zoom) {
        String[] strZoom = zoom.split("=");
        int zoomLevel = Integer.valueOf(strZoom[1]);

        switch (zoomLevel) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                zoomLevel = 12;
                break;
            case 6:
            case 7:
                zoomLevel = 11;
                break;
            case 8:
                zoomLevel = 10;
                break;
            case 9:
                zoomLevel = 9;
                break;
            case 10:
                zoomLevel = 8;
                break;
            case 11:
                zoomLevel = 7;
                break;
            case 12:
                zoomLevel = 6;
                break;
            case 13:
                zoomLevel = 5;
                break;
            case 14:
            case 15:
                zoomLevel = 4;
                break;
            case 16:
                zoomLevel = 3;
                break;
            case 17:
                zoomLevel = 2;
                break;
            case 18:
                zoomLevel = 1;
                break;
            default:
                zoomLevel = 12;
                break;
        }

        return zoomLevel;
    }

    /**
     * 比例尺の階段数はマッチングする
     *
     * @param scale
     *            比例尺
     * @return 比例尺の階段数
     */
    public static byte matchingZoomLevel(String scale) {
        byte zoomLevel = 0;
        if (scale.equals("10m")) {
            zoomLevel = 0;
        } else if (scale.equals("20m")) {
            zoomLevel = 1;
        } else if (scale.equals("40m")) {
            zoomLevel = 2;
        } else if (scale.equals("50m")) {
            zoomLevel = 3;
        } else if (scale.equals("100m")) {
            zoomLevel = 4;
        } else if (scale.equals("200m")) {
            zoomLevel = 5;
        } else if (scale.equals("500m")) {
            zoomLevel = 6;
        } else if (scale.equals("1km")) {
            zoomLevel = 7;
        } else if (scale.equals("2km")) {
            zoomLevel = 8;
        } else if (scale.equals("5km")) {
            zoomLevel = 9;
        } else if (scale.equals("10km")) {
            zoomLevel = 10;
        } else if (scale.equals("20km")) {
            zoomLevel = 11;
        } else if (scale.equals("50km")) {
            zoomLevel = 12;
        } else if (scale.equals("100km")) {
            zoomLevel = 13;
        }

        return zoomLevel;
    }

    /**
     * Created on 2009/12/25
     * Title：GetIntentLongitude
     * Description: 受け取った経緯度を解析する
     *
     * @param String
     *            location 受け取った経緯度
     * @return JNITwoLong
     *
     * @version 1.0
     */
    public static JNITwoLong GetIntentLongitude(String location) {
        String strCoordinate = null;
        String[] strLonLat = null;
        JNITwoLong lonLat = new JNITwoLong();

        int num = location.indexOf("?");
        if (-1 != num) {
            strCoordinate = location.substring(0, num);
            strLonLat = strCoordinate.split(",");
        } else {
            strLonLat = location.split(",");
        }

        long wgslongitude = CommonLib.degreeToMS(Double.valueOf(strLonLat[0]));
        long wgslatitude = CommonLib.degreeToMS(Double.valueOf(strLonLat[1]));

        NaviRun.JNI_LIB_dtmConvByDegree(Constants.DTAUM_INDEX_TOKYO,
                        Constants.CONVERT_FROM_WGS84,
                        wgslatitude,
                        wgslongitude,
                        lonLat);

        return lonLat;
    }

    /**
     * WGS84座標系で表示する経緯度を東京座標系で表示する経緯度に変換する
     *
     * @param location
     *            受け取った経緯度
     * @return 東京座標系で表示する経緯度
     */
    public static JNITwoLong GetIntentLongitudeForRoute(Location location) {
        JNITwoLong lonLat = new JNITwoLong();

        if (null != location) {
            long wgslongitude = CommonLib.degreeToMS(location.getLongitude());
            long wgslatitude = CommonLib.degreeToMS(location.getLatitude());

            NaviRun.JNI_LIB_dtmConvByDegree(Constants.DTAUM_INDEX_TOKYO, Constants.CONVERT_FROM_WGS84, wgslongitude, wgslatitude, lonLat);
        }

        return lonLat;
    }

    /**
     * 東京座標系で表示する経緯度をにWGS84座標系で表示する経緯度変換する
     *
     * @param DBLatLon
     *            東京座標系で表示する経緯度
     * @return WGS84座標系で表示する経緯度
     */
    public static Location ConvertDBCoordinateSystemToWGS(JNITwoLong DBLatLon) {
        Location locationWgs = new Location(LocationManager.GPS_PROVIDER);
        JNITwoLong LonLat = new JNITwoLong();

        if (null != DBLatLon) {
            NaviRun.JNI_LIB_dtmConvByDegree(Constants.CONVERT_FROM_WGS84, Constants.DTAUM_INDEX_TOKYO, DBLatLon.getM_lLong(), DBLatLon.getM_lLat(), LonLat);
            locationWgs.setLongitude(CommonLib.MSToDegree(LonLat.getM_lLong()));
            locationWgs.setLatitude(CommonLib.MSToDegree(LonLat.getM_lLat()));
        } else {
            return null;
        }

        return locationWgs;
    }

    /**
     * 案内開始時に、案内情報を保存.
     *
     * @param isViaPassed
     *            経由地通過情報
     */
    public static void createNaviDataFile(boolean isViaPassed, NaviAppDataPath naviAppDataPath) {
        naviGuideRestoreFile.createNaviDataFile(isViaPassed, naviAppDataPath);
    }

    /*
     * 案内再開ファイル読み込み
     */
    public static boolean readNaviDataFile(NaviAppDataPath naviAppDataPath) {
        return naviGuideRestoreFile.readNaviDataFile(naviAppDataPath);
    }

    /*
     * 案内再開ファイル削除
     */
    public static void deleteNaviDataFile(NaviAppDataPath naviAppDataPath) {
        naviGuideRestoreFile.deleteNaviDataFile(naviAppDataPath);
    }

    /*
     * 案内再開ファイルクラス取得
     */
    public static NaviGuideRestoreFile getNaviGuideRestoreFile() {
        return naviGuideRestoreFile;
    }

    /**
     * Created on 2010/08/02 Title: createVplogFile Description: Vp Logファイルを創建する
     *
     * @param1 無し
     * @return 無し
     * @author xiayx
     * @version 1.0
     */
//Chg 2011/09/26 Z01yoneya Start -->
//    public static void createVplogFile() {
//        if (Constants.VPLOG_OUTPUT == Constants.ON) {
//            String[] strPath = naviResFilePath[6].split(" ");
//
//            StringBuffer strBuffer = new StringBuffer(strPath[2]);
//------------------------------------------------------------------------------------------------------------------------------------
    private static NaviAppDataPath naviAppDataPath = null; //本当はNaviApplicationクラスから参照させたいが、LocationListnerの方が解決できないのでメンバーで持つ。
                                                           //createVplogFile()はMapActivityのinitAppConfig()から呼ばれるから、naviAppDataPathは必ず初期化される。

    public static void createVplogFile(NaviAppDataPath naviAppDataPathClass) {
        if (Constants.VPLOG_OUTPUT == Constants.ON) {
            naviAppDataPath = naviAppDataPathClass;
            StringBuffer strBuffer = new StringBuffer(naviAppDataPath.getLogDataPath());
//Chg 2011/09/26 Z01yoneya End <--

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss");

            Date d = new Date();
            strBuffer.append("LocationManagerLog");
            strBuffer.append(sdf.format(d));
            strBuffer.append(".txt");

            String vpLogFile = strBuffer.toString();

            setVpLogFileName(vpLogFile);

            if (fw == null) {
                vpLogFile = getVpLogFileName();
                File f = new File(vpLogFile);

                try {
                    fw = new FileWriter(f);
                    fw.write("Ver 1");
                    fw.write("\r\n");
                } catch (FileNotFoundException e) {
//Chg 2011/11/10 Z01thedoanh Start -->
                	//CommonLib.log("CommonLib", "createVplogFile FileNotFoundException ", e);
//----------------------------------------------------------------------------
                	NaviLog.e("CommonLib", "createVplogFile FileNotFoundException " + e);
//Chg 2011/11/10 Z01thedoanh End <--
                } catch (IOException e) {
//Chg 2011/11/10 Z01thedoanh Start -->
                    //CommonLib.log("CommonLib", "createVplogFile IOException ", e);
//----------------------------------------------------------------------------
                	NaviLog.e("CommonLib", "createVplogFile IOException " + e);
//Chg 2011/11/10 Z01thedoanh End <--
                }
            }
        }
    }

    /**
     * Created on 2010/08/02 Title: writeVplogInmation Description: GPS
     * をデータは書いてファイルまで(へ)入ります
     *
     * @param1 Location oLoc GPSの情報
     * @return 無し
     * @author xiayx
     * @version 1.0
     */
    private static SimpleDateFormat myFmt;
    private static Date Logdate = new Date();

    public static void writeVplogInmation(Location oLoc) {
        String strLatitude = null;
        String strLongitude = null;
        String strAccuracy = null;
        String strSpeed = null;
        String strDirection = null;

        NumberFormat ddf1 = NumberFormat.getNumberInstance();
        StringBuffer strb = new StringBuffer();
        try {

            if (oLoc.getTime() > 0) {
                /*
                c.setTimeInMillis( oLoc.getTime() );
                int iYear = c.get( Calendar.YEAR );
                int iMonth = ( c.get( Calendar.MONTH ) ) + 1;
                int iDay = c.get( Calendar.DAY_OF_MONTH );
                int iHour = c.get( Calendar.HOUR_OF_DAY );
                int iMinute = c.get( Calendar.MINUTE );
                int iSecond = c.get( Calendar.SECOND );

                ddf1.setMaximumFractionDigits( 0 );
                strb.append( ddf1.format( iYear ) );
                strb.append( "-" );
                strb.append( ddf1.format( iMonth ) );
                strb.append( "-" );
                strb.append( ddf1.format( iDay ) );
                strb.append( "-" );
                strb.append( " " );
                strb.append( ddf1.format( iHour ) );
                strb.append( ":" );
                strb.append( ddf1.format( iMinute ) );
                strb.append( ":" );
                strb.append( ddf1.format( iSecond ) );
                strb.append( "," ); */

                if (myFmt == null) {
                    myFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                }
                if (Logdate == null) {
                    Logdate = new Date();
                }
                Logdate.setTime(oLoc.getTime());
                String dateStr = myFmt.format(Logdate);
                strb.append(dateStr);
                strb.append(",");
            }

            ddf1.setMaximumFractionDigits(5);
            strLatitude = ddf1.format(oLoc.getLatitude());

            strb.append(strLatitude);
            strb.append(",");
            strLongitude = ddf1.format(oLoc.getLongitude());
            strb.append(strLongitude);
            strb.append(",");

            strAccuracy = ddf1.format(oLoc.getAccuracy());
            strb.append(strAccuracy);
            strb.append(",");

            strAccuracy = ddf1.format(oLoc.getAltitude());
            strb.append(strAccuracy);
            strb.append(",");

            ddf1.setMaximumFractionDigits(2);
            strSpeed = ddf1.format(oLoc.getSpeed());
            strb.append(strSpeed);
            strb.append(",");
            strDirection = ddf1.format(oLoc.getBearing());
            strb.append(strDirection);
            //add liutch 1216
            if (fw == null) {
//Chg 2011/09/26 Z01yoneya Start -->
//                createVplogFile();
//------------------------------------------------------------------------------------------------------------------------------------
                if (naviAppDataPath != null) {
                    createVplogFile(naviAppDataPath);
                }
//Chg 2011/09/26 Z01yoneya End <--
            }
            //end
            if (fw != null) {
                fw.write(strb.toString());
                fw.write("\r\n");
                fw.flush();
            }
        } catch (IOException e) {
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException e1) {
                //  System.out.println( e1.getMessage() );
//Chg 2011/11/10 Z01thedoanh Start -->
                //CommonLib.log("CommonLib", "writeVplogInmation IOException", e1);
//----------------------------------------------------------------------------
                NaviLog.e("CommonLib", "writeVplogInmation IOException" + e1);
//Chg 2011/11/10 Z01thedoanh End <--
            }
            //  System.out.println( e.getMessage() );
//Chg 2011/11/10 Z01thedoanh Start -->
            //CommonLib.log("CommonLib", "writeVplogInmation IOException", e);
//----------------------------------------------------------------------------
            NaviLog.e("CommonLib", "writeVplogInmation IOException" + e);
//Chg 2011/11/10 Z01thedoanh End <--
        }
    }

    /**
     * 閉鎖して書いて機能のVP Logファイル
     */
    public static void closeVplogFileForwrite() {
        try {
            if (fw != null) {
                fw.close();
            }
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }
    }

    /**
     * 閉鎖して機能のVP Logファイルを読み取ります
     */
    public static void closeVplogFileForread() {
        try {
            if (fr != null) {
                fr.close();
            }
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }
    }

    /**
     * VP Logファイルを開けます
     */
//Chg 2011/09/26 Z01yoneya Start -->
//    public static void openVplogFile() {
//
//        String[] strPath = naviResFilePath[6].split(" ");
////      String gpsLog = strPath[2] + "LocationManagerLog.txt";
//
//      try {
//
////          File file = new File(gpsLog);
////          if (!file.exists()) {
////              return;
////          }
//          File file = CommonLib.openFile(strPath[2], "LocationManagerLog.txt");
//------------------------------------------------------------------------------------------------------------------------------------
    public static void openVplogFile(NaviAppDataPath naviAppDataPath) {
        try {
            File file = CommonLib.openFile(naviAppDataPath.getLogDataPath(), "LocationManagerLog.txt");
//Chg 2011/09/26 Z01yoneya End <--

            fr = new FileReader(file);
            bf = new BufferedReader(fr);
            if (bf != null) {
                String str = bf.readLine();
                setVpLogFileFormatName(str);

                setBf(bf);
            }

        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);

            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException e1) {
    				NaviLog.e(NaviLog.PRINT_LOG_TAG, e1);
                }
            }
        } finally {

        }
    }

//Del 2012/02/23 Z01_h_yamada Start --> #3790
//    /**
//     * Created on 2009/12/25
//     * Title：isBIsNaviStart
//     * Description: カレントNaviの状態（起動/起動していない）を取得する
//     *
//     * @param 無し
//     * @return boolean
//     *
//     * @version 1.0
//     */
//    public static boolean isBIsNaviStart() {
//        return bIsNaviStart;
//    }
//
//    /**
//     * Created on 2009/12/25
//     * Title：isBIsNaviStart
//     * Description: カレントNaviの状態（起動/起動していない）を設定する
//     *
//     * @param boolean isNaviStart カレントNaviの状態（起動/起動していない）
//     * @return void
//     *
//     * @version 1.0
//     */
//    public static void setBIsNaviStart(boolean isNaviStart) {
//        bIsNaviStart = isNaviStart;
//    }
//Del 2012/02/23 Z01_h_yamada End <--

    /**
     * Created on 2009/12/25
     * Title：isBIsIntentNaviOpenMapStart
     * Description: 地図Appli起動かを判断する
     *
     * @param 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsIntentNaviOpenMapStart() {
        return bIsIntentNaviOpenMapStart;
    }

    /**
     * Created on 2009/12/25
     * Title：setBIsIntentNaviOpenMapStart
     * Description: 地図Appli起動の状態を設定する
     *
     * @param boolean isIntentNaviOpenMapStart 地図Appli起動の状態
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsIntentNaviOpenMapStart(boolean isIntentNaviOpenMapStart) {
        bIsIntentNaviOpenMapStart = isIntentNaviOpenMapStart;
    }

    /**
     * Created on 2009/12/25
     * Title：isBIsIntentNaviRouteDiplay
     * Description: oute Appli起動かを判断する
     *
     * @param 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsIntentNaviRouteDiplay() {
        return bIsIntentNaviRouteDiplay;
    }

    /**
     * Created on 2009/12/25
     * Title：setBIsIntentNaviRouteDiplay
     * Description: Route Appli起動の状態を設定する
     *
     * @param boolean isIntentNaviRouteDiplay Route Appli起動の状態
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsIntentNaviRouteDiplay(boolean isIntentNaviRouteDiplay) {
        bIsIntentNaviRouteDiplay = isIntentNaviRouteDiplay;
    }

    /**
     * Created on 2009/12/25
     * Title：isBIsIntentNavi
     * Description: Intent起動かを判断する
     *
     * @param 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsIntentNavi() {
        return bIsIntentNavi;
    }

    /**
     * Created on 2009/12/25
     * Description: Intent起動の状態を設定する
     *
     * @param: Intent起動の状態
     */

    /**
     * Created on 2009/12/25
     * Title：setBIsIntentNavi
     * Description: Intent起動の状態を設定する
     *
     * @param boolean isIntentNavi Route Intent起動の状態
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsIntentNavi(boolean isIntentNavi) {
        bIsIntentNavi = isIntentNavi;
    }

    /**
     * Created on 2009/12/25
     * Title：isBIsSearching
     * Description: 検索中かを判断する
     *
     * @param 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsSearching() {
        return bIsSearching;
    }

    /**
     * Created on 2009/12/25
     * Title：setBIsIntentNavi
     * Description: 検索中かを設定する
     *
     * @param boolean isSearching Route 検索状態
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsSearching(boolean isSearching) {
        bIsSearching = isSearching;
    }

    /**
     * Created on 2009/12/25
     * Title：isBIsRouteCalculating
     * Description: 経路計算中かを判断する
     *
     * @param 無し
     * @return boolean
     *
     * @version 1.0
     */
/*    public static boolean isBIsRouteCalculating() {
        return bIsRouteCalculating;
    }*/

    /**
     * Created on 2009/12/25
     * Title：setBIsRouteCalculating
     * Description: カレントの経路計算状態を設定する
     *
     * @param boolean isRouteCalculating カレントの経路計算状態
     * @return void
     *
     * @version 1.0
     */
    /*public static void setBIsRouteCalculating(boolean isRouteCalculating) {
        bIsRouteCalculating = isRouteCalculating;
    }*/

    /**
     * Created on 2009/12/25
     * Title：isBIsDemo
     * Description: Demo中かを判断する
     *
     * @param 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsDemo() {
        return bIsDemo;
    }

    /**
     * Created on 2009/12/25
     * Title：setBIsDemo
     * Description: Demo中かを設定する
     *
     * @param boolean isDemo Demo状態
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsDemo(boolean isDemo) {
        bIsDemo = isDemo;
    }

//Del 2011/09/26 Z01yoneya Start -->
//    /**
//     * Created on 2009/12/25
//     * Title：isBIsFromIntentStart
//     * Description: カレントのIntent起動状態を判断する
//     *
//     * @param 無し
//     * @return boolean
//     *
//     * @version 1.0
//     */
//    public static boolean isBIsFromIntentStart() {
//        return bIsFromIntentStart;
//    }
//
//    /**
//     * Created on 2009/12/25
//     * Title：setBIsFromIntentStart
//     * Description: カレントのIntent起動状態を設定する
//     *
//     * @param boolean isFromIntentStart Intent起動状態
//     * @return void
//     *
//     * @version 1.0
//     */
//    public static void setBIsFromIntentStart(boolean isFromIntentStart) {
//        bIsFromIntentStart = isFromIntentStart;
//    }
//Del 2011/09/26 Z01yoneya End <--

    /**
     * Created on 2009/12/25
     * Title：isBIsRouteDisplay
     * Description: Route Displayを判断する
     *
     * @param 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsRouteDisplay() {
        return bIsRouteDisplay;
    }

    /**
     * Created on 2009/12/25
     * Title：setBIsRouteDisplay
     * Description: Route Displayを設定する
     *
     * @param boolean isRouteDisplay Route Display状態
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsRouteDisplay(boolean isRouteDisplay) {
        bIsRouteDisplay = isRouteDisplay;
    }

    /**
     * Created on 2009/12/25
     * Title：isBIsRouteCancel
     * Description: Route Cancel状態を判断する
     *
     * @param 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsRouteCancel() {
        return bIsRouteCancel;
    }

    /**
     * Created on 2009/12/25
     * Title：setBIsRouteCancel
     * Description: Route Displayを設定する
     *
     * @param boolean isRouteCancel Route Cancel状態
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsRouteCancel(boolean isRouteCancel) {
        bIsRouteCancel = isRouteCancel;
    }

    /**
     * Created on 2009/12/25
     * Title：isBIsCarMode
     * Description: Car Mode状態を判断する
     *
     * @param 無し
     * @return boolean
     *
     * @version 1.0
     */
    /*public static boolean isBIsCarMode() {
        return bIsCarMode;
    }*/

    /**
     * Created on 2009/12/25
     * Title：setBIsCarMode
     * Description: Car Mode状態を設定する
     *
     * @param boolean isCarMode Car Mode状態
     * @return void
     *
     * @version 1.0
     */
    /*public static void setBIsCarMode(boolean isCarMode) {
        bIsCarMode = isCarMode;
    }*/

    /**
     * Created on 2009/12/25
     * Title：isBIsManModeRouteCalcStas
     * Description: Man Mode Route calc状態を判断する
     *
     * @param 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsManModeRouteCalcStas() {
        return bIsManModeRouteCalcStas;
    }

    /**
     * Created on 2009/12/25
     * Title：setBIsManModeRouteCalcStas
     * Description: Man Mode Route calc状態を設定する
     *
     * @param boolean isManModeRouteCalcStas Man Mode Route calc状態
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsManModeRouteCalcStas(boolean isManModeRouteCalcStas) {
        bIsManModeRouteCalcStas = isManModeRouteCalcStas;
    }

    /**
     * Created on 2009/12/25
     * Description: MapView状態を設定する
     *
     * @param: MapView状態
     */
    /**
     * Created on 2009/12/25
     * Title：setBIsMapView
     * Description: MapView状態を設定する
     *
     * @param boolean isMapView MapView状態
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsMapView(boolean isMapView) {
        bIsMapView = isMapView;
    }

    /**
     * Created on 2009/12/25
     * Title：isBIsMapView
     * Description: MapView状態を判断する
     *
     * @param 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsMapView() {
        return bIsMapView;
    }

    /**
     * Created on 2010/08/02
     * Title: setSensorData
     * Description: 経度と緯度を設定する
     *
     * @param1 JNITwoLong Coordinate 経度と緯度の数値
     * @return 無し
     *
     * @version 1.0
     */
    public static void setSensorData(JNITwoLong Coordinate) {
        latLon = Coordinate;

        return;
    }

    /**
     * Created on 2010/08/02
     * Title: getSensorData
     * Description: 経度と緯度を取得する
     *
     * @param1 無し
     * @return JNITwoLong
     *
     * @version 1.0
     */
    public static JNITwoLong getSensorData() {
        return latLon;
    }

//Modify start For Redmine 1660
    /**
     * Created on 2010/08/02
     * Title: setMapCenterInfo
     * Description: 地図中心経度と緯度を設定する
     *
     * @param1 JNITwoLong Coordinate 経度と緯度の数値
     * @return 無し
     *
     * @version 1.0
     */
    public static void setMapCenterInfo(JNITwoLong Coordinate) {
        MaplatLon = Coordinate;
        return;
    }

    /**
     * Created on 2010/08/02
     * Title: getMapCenterInfo
     * Description: 地図中心経度と緯度を取得する
     *
     * @param1 無し
     * @return JNITwoLong
     *
     * @version 1.0
     */
    public static JNITwoLong getMapCenterInfo() {
        return MaplatLon;
    }

//Modify start For Redmine 1660

//Del 2012/03/13 Z01_h_fukushima(No.405) Start --> ref 2510 案内中画面での地点追加
//  // xuyang add start #1914
//   public static void setMapOldPoint(JNITwoLong Coordinate) {
//       mapOldPoint = Coordinate;
//       return;
//   }
//   public static JNITwoLong getMapOldPoint() {
//       return mapOldPoint;
//
//   }
//   // xuyang add end #1914
//Del 2012/03/13 Z01_h_fukushima(No.405) End <--
     // xuyang add start #1782
     /**
      * 保存地図拡大縮小の値
      */
     public static void setScaleValue(byte scaleVale) {
         ScaleVlaue = scaleVale;
     }
     /**
      * 取得地図拡大縮小の値
      * @return
      */
     public static byte getScaleValue(){
         return ScaleVlaue;
     }
     // xuyang add end #1782
    /**
     * Created on 2010/08/02
     * Title: isBIsShowGuide
     * Description: Guideを表示すらなければならないかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsShowGuide() {
        return bIsShowGuide;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsShowGuide
     * Description: Guideを表示すらなければならないかどうかを設定する
     *
     * @param1 boolean isShowGuide
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsShowGuide(boolean isShowGuide) {
        bIsShowGuide = isShowGuide;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsDirectionBoard
     * Description: DirectionBoard を表示すらなければならないかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsDirectionBoard() {
        return bIsDirectionBoard;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsDirectionBoard
     * Description: DirectionBoard を表示すらなければならないかどうかを設定する
     *
     * @param1 boolean isDirectionBoard
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsDirectionBoard(boolean isDirectionBoard) {
        bIsDirectionBoard = isDirectionBoard;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsShowJct
     * Description: JCT を表示すらなければならないかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsShowJct() {
        return bIsShowJct;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsShowJct
     * Description: JCT を表示すらなければならないかどうかを設定する
     *
     * @param1 boolean isShowJct
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsShowJct(boolean isShowJct) {
        bIsShowJct = isShowJct;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsShowMagMap
     * Description: MagMap を表示すらなければならないかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsShowMagMap() {
        return bIsShowMagMap;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsShowMagMap
     * Description: MagMap を表示すらなければならないかどうかを設定する
     *
     * @param1 boolean isShowMagMap
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsShowMagMap(boolean isShowMagMap) {
        bIsShowMagMap = isShowMagMap;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsAppoachIntersection
     * Description: 交差点 を表示すらなければならないかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsAppoachIntersection() {
        return bIsAppoachIntersection;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsAppoachIntersection
     * Description: 交差点 を表示すらなければならないかどうかを設定する
     *
     * @param1 boolean isAppoachIntersection
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsAppoachIntersection(boolean isAppoachIntersection) {
        bIsAppoachIntersection = isAppoachIntersection;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsShowMagMapName
     * Description: MagMap名称 を表示すらなければならないかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsShowMagMapName() {
        return bIsShowMagMapName;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsShowMagMapName
     * Description: MagMap名称 を表示すらなければならないかどうかを設定する
     *
     * @param1 boolean isShowMagMapName
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsShowMagMapName(boolean isShowMagMapName) {
        bIsShowMagMapName = isShowMagMapName;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsOnHighWay
     * Description: 高速の道の上にあるかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsOnHighWay() {
        return bIsOnHighWay;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsOnHighWay
     * Description: 高速の道の上にあるかどうかを設定する
     *
     * @param1 boolean isOnHighWay
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsOnHighWay(boolean isOnHighWay) {
        bIsOnHighWay = isOnHighWay;
    }

    // For GuideInfo isn't synchronisation Start
    /**
     * Created on 2010/08/02
     * Title: isBIsDisplayGuideInfo
     * Description: 地図に転がる時、隠れる案内の情報かどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsDisplayGuideInfo() {
        return bIsDisplayGuideInfo;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsDisplayGuideInfo
     * Description: 地図に転がる時、隠れる案内の情報かどうかを設定する
     *
     * @param1 boolean isDisplayGuideInfo
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsDisplayGuideInfo(boolean isDisplayGuideInfo) {
        bIsDisplayGuideInfo = isDisplayGuideInfo;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsGuideEnd
     * Description: 案内終わるかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsGuideEnd() {
        return bIsGuideEnd;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsGuideEnd
     * Description: 案内終わるかどうかを設定する
     *
     * @param1 boolean isGuideEnd
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsGuideEnd(boolean isGuideEnd) {
        bIsGuideEnd = isGuideEnd;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsGuideEnd
     * Description: 案内の情報隠れるかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsHideGuide() {
        return bIsHideGuide;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsHideGuide
     * Description: 案内の情報隠れるかどうかを設定する
     *
     * @param1 boolean isHideGuide
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsHideGuide(boolean isHideGuide) {
        bIsHideGuide = isHideGuide;
    }

    //For GuideInfo isn't synchronisation End

    // For startNaviFromIntent Start
    /**
     * Created on 2010/08/02
     * Title: isBIsIntentNoNaviOpenMapStart
     * Description: Naviがスタートしていない時、OpenMapを開けるかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsIntentNoNaviOpenMapStart() {
        return bIsIntentNoNaviOpenMapStart;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsIntentNoNaviOpenMapStart
     * Description: Naviがスタートしていない時、OpenMapを開けるかどうかを設定する
     *
     * @param1 boolean isHideGuide
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsIntentNoNaviOpenMapStart(
            boolean isIntentNoNaviOpenMapStart) {
        bIsIntentNoNaviOpenMapStart = isIntentNoNaviOpenMapStart;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsNaviForeground
     * Description: Naviは最もフロントエンドは表示するかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsNaviForeground() {
        return bIsNaviForeground;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsNaviForeground
     * Description: Naviは最もフロントエンドは表示するかどうかを設定する
     *
     * @param1 boolean isNaviForeground
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsNaviForeground(boolean isNaviForeground) {
        bIsNaviForeground = isNaviForeground;
    }

    //For startNaviFromIntent End

    /**
     * Created on 2010/08/02
     * Title: isBIsRouteEditCarMode
     * Description: Route編集の画面、車のモードだかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsRouteEditCarMode() {
        return bIsRouteEditCarMode;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsRouteEditCarMode
     * Description: Route編集の画面、車のモードだかどうかを設定する
     *
     * @param1 boolean isRouteEditCarMode
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsRouteEditCarMode(boolean isRouteEditCarMode) {
        bIsRouteEditCarMode = isRouteEditCarMode;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsRouteEditModeChange
     * Description: Route編集の画面、人/車のモードの切替を行うかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsRouteEditModeChange() {
        return bIsRouteEditModeChange;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsRouteEditModeChange
     * Description: Route編集の画面、人/車のモードの切替を行うかどうかを設定する
     *
     * @param1 boolean isRouteEditModeChange
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsRouteEditModeChange(boolean isRouteEditModeChange) {
        bIsRouteEditModeChange = isRouteEditModeChange;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsNaviEnd
     * Description: Navi終わるかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsNaviEnd() {
        return bIsNaviEnd;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsNaviEnd
     * Description: Navi終わるかどうかを設定する
     *
     * @param1 boolean isNaviEnd
     * @return 無し
     *
     * @version 1.0
     */
    public static void setBIsNaviEnd(boolean isNaviEnd) {
        bIsNaviEnd = isNaviEnd;
    }

    //For Bug 111420 End
    /**
     * Created on 2010/08/02
     * Title: getVpLogFileName
     * Description: VP Logファイルの名称を取得する（書いて入ります）
     *
     * @param1 無し
     * @return String
     *
     * @version 1.0
     */
    public static String getVpLogFileName() {
        return vpLogFileName;
    }

    /**
     * Created on 2010/08/02
     * Title: setVpLogFileName
     * Description: VP Logファイルの名称を設定する（書いて入ります）
     *
     * @param1 String vpLogFileName
     * @return 無し
     *
     * @version 1.0
     */
    public static void setVpLogFileName(String vpLogFileName) {
        CommonLib.vpLogFileName = vpLogFileName;
    }

    /**
     * Created on 2010/08/02
     * Title: getVpLogFileFormatName
     * Description: VP Logファイルの名称を取得する（読み取ります）
     *
     * @param1 無し
     * @return String
     *
     * @version 1.0
     */
    public static String getVpLogFileFormatName() {
        return vpLogFileFormatName;
    }

    /**
     * Created on 2010/08/02
     * Title: setVpLogFileFormatName
     * Description: VP Logファイルの名称を設定する（読み取ります）
     *
     * @param1 String vpLogFileFormatName
     * @return 無し
     *
     * @version 1.0
     */
    public static void setVpLogFileFormatName(String vpLogFileFormatName) {
        CommonLib.vpLogFileFormatName = vpLogFileFormatName;
    }

    /**
     * Created on 2010/08/02
     * Title: getBf
     * Description: ファイルの種類の実例を読みを取得する
     *
     * @param1 無し
     * @return BufferedReader
     *
     * @version 1.0
     */
    public static BufferedReader getBf() {
        return bf;
    }

    /**
     * Created on 2010/08/02
     * Title: setBf
     * Description: ファイルの種類の実例を読みを設定する
     *
     * @param1 BufferedReader bf
     * @return void
     *
     * @version 1.0
     */
    public static void setBf(BufferedReader bf) {
        CommonLib.bf = bf;
    }

    /*
     * ルート情報ファイル削除
     */
    public static void deleteRouteDataXml(NaviAppDataPath naviAppDataPath) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "CommonLib::deleteRouteDataXml()");

        deleteFile(naviAppDataPath.getUserDataPath(), NaviAppDataPath.ROUTE_DATA_XML_FILENAME);
    }

    public static void deleteFile(String path, String name) {
        try {
            File file = openFile(path, name);
            if (file != null && file.isFile() && file.exists()) {
                boolean result = file.delete();
                if (!result) {
                    NaviLog.e(NaviLog.PRINT_LOG_TAG, "file delete error [" + path + " " + name + "]");
                }
            }
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }
    }

    /**
     * 「現在地」のメソッド
     *
     * @param context
     *            コンテキスト
     * @return
     */
    public static void locusMap(Activity context) {
//        CommonLib.MAP_VIEW_FLAG = MapView.getInstance().getMap_view_flag();
//        if (CommonLib.MAP_VIEW_FLAG == Constants.OPEN_MAP_VIEW){
//            CommonLib.MAP_VIEW_FLAG = Constants.COMMON_MAP_VIEW;
//        }
//        MapView.getInstance().setMap_view_flag(CommonLib.MAP_VIEW_FLAG);
//
//        Intent intent = new Intent();
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.putExtra(Constants.FLAG_BACK_CAR_POS, true);
//        intent.setClass(context, MapActivity.class);
//        context.startActivityForResult(intent, AppInfo.ID_ACTIVITY_MAP);
//        NaviRun.GetNaviRunObj().setSearchKind(Constants.SCROLL_MAP_KIND);
//        NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
//        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();

//        JNITwoLong carPos = new JNITwoLong();
//        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(carPos);
//
//        POIData poi = new POIData();
//        poi.m_wLong = carPos.getM_lLong();
//        poi.m_wLat = carPos.getM_lLat();
//
//        OpenMap.moveMapTo(context, poi, null, AppInfo.POI_NONE, Constants.LOCATION_MAP_REQUEST);

        OpenMap.locusMap(context);

    }
//Add 2011/12/14 Z01_h_yamada Start -->
    public static void goCarLocation(BaseActivity context) {

    	// メインのMapActivity以外は終了
    	context.removeSubAllActivity();

        try {
        	BaseActivity act = context.getRootActivity();
        	if (act.getClass().equals(MapActivity.class)) {
            	OpenMap.locationBack((MapActivity)act);
        	}
        } catch (IllegalStateException e) {
        }
    }
//Add 2011/12/14 Z01_h_yamada End <--

    /*
     * ルート情報ファイル作成
     */
    public static void creatRouteDataXml(NaviAppDataPath naviAppDataPath) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "CommonLib::creatRouteDataXml()");

        List<String> strInput = new ArrayList<String>();
        strInput.add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\r\n");
        strInput.add("<ROUTE_DATA>" + "\r\n");
        strInput.add("</ROUTE_DATA>");

        RouteDataFile routeDataFile = new RouteDataFile();
        routeDataFile.writeRouteDataFile(NaviAppDataPath.ROUTE_DATA_XML_FILENAME, strInput);
    }

    /**
     * ファイル存在を判断する
     *
     * @param filePath
     *            ファイル パス
     * @return true:存在 false:存在しない
     */
    public static boolean fileExists(String filePath) {
        File file = new File(filePath);
        return (file.isFile() && file.exists());
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsIntentPoiLogStart
     * Description: NaviをPoiLogスタートを通じて(通って)かどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsIntentPoiLogStart() {
        return bIsIntentPoiLogStart;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsIntentPoiLogStart
     * Description: NaviをPoiLogスタートを通じて(通って)かどうかを設定する
     *
     * @param1 boolean isIntentPoiLogStart
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsIntentPoiLogStart(boolean isIntentPoiLogStart) {
        bIsIntentPoiLogStart = isIntentPoiLogStart;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsIntentFirstPoiLogStart
     * Description: NaviをPoiLogスタートを通じて(通って)第1回かどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsIntentFirstPoiLogStart() {
        return bIsIntentFirstPoiLogStart;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsIntentFirstPoiLogStart
     * Description: NaviをPoiLogスタートを通じて(通って)かどうかを設定する
     *
     * @param1 boolean isIntentFirstPoiLogStart
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsIntentFirstPoiLogStart(boolean isIntentFirstPoiLogStart) {
        bIsIntentFirstPoiLogStart = isIntentFirstPoiLogStart;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsApproachHighway
     * Description: 高速の道に接近するかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsApproachHighway() {
        return bIsApproachHighway;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsApproachHighway
     * Description: 高速の道に接近するかどうかを設定する
     *
     * @param1 boolean isApproachHighway
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsApproachHighway(boolean isApproachHighway) {
        bIsApproachHighway = isApproachHighway;
    }

    /**
     * Created on 2010/01/15
     * Description:「地点追加」レストを取得する
     *
     * @param 無し
     * @return ArrayList<String> 　「地点追加」レスト
     */
    public static ArrayList<String> getADD_DATE() {
        return ADD_DATE;
    }

    /**
     * Created on 2010/01/15
     * Description:「地点追加」レストを設定する
     *
     * @param ArrayList
     *            <String> 　「地点追加」レスト
     * @return 無し
     */
    public static void setADD_DATE(ArrayList<String> add_date) {
        ADD_DATE = add_date;
    }

    // For MENU_PLACE_ADD End
    /**
     * Created on 2010/01/15
     * Description:「地図色状態」を判断する
     *
     * @param 無し
     * @return boolean　「true：自動　false:その他」
     */
    public static boolean isBIsAutoMap() {
        return bIsAutoMap;
    }

    /**
     * Created on 2010/01/15
     * Description:「地図色状態」を設定する
     *
     * @param boolean　「true：自動　false:その他」
     * @return 無し
     */
    public static void setBIsAutoMap(boolean isAutoMap) {
        bIsAutoMap = isAutoMap;
    }

    /**
     * Created on 2010/08/02
     * Title: isbIsStartNaviFromMapIntent
     * Description: 地図IntentスタートNaviかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isbIsStartNaviFromMapIntent() {
        return bIsStartNaviFromMapIntent;
    }

    /**
     * Created on 2010/08/02
     * Title: setbIsStartNaviFromMapIntent
     * Description: 地図IntentスタートNaviかどうかを設定する
     *
     * @param1 boolean bIsStartNaviFromMapIntent
     * @return void
     *
     * @version 1.0
     */
    public static void setbIsStartNaviFromMapIntent(
            boolean bIsStartNaviFromMapIntent) {
        CommonLib.bIsStartNaviFromMapIntent = bIsStartNaviFromMapIntent;
    }

    /**
     * Created on 2010/08/02
     * Title: isbIsStartNaviFromINavintent
     * Description: ナビIntentスタートNaviかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isbIsStartNaviFromINavintent() {
        return bIsStartNaviFromINavintent;
    }

    /**
     * Created on 2010/08/02
     * Title: setbIsStartNaviFromINavintent
     * Description: ナビIntentスタートNaviかどうかを設定する
     *
     * @param1 boolean bIsStartNaviFromINavintent
     * @return void
     *
     * @version 1.0
     */
    public static void setbIsStartNaviFromINavintent(
            boolean bIsStartNaviFromINavintent) {
        CommonLib.bIsStartNaviFromINavintent = bIsStartNaviFromINavintent;
    }

    /**
     * Created on 2010/08/02
     * Title: isBIsBackground
     * Description: Naviはバックグランドがあるかどうかを判断する
     *
     * @param1 無し
     * @return boolean
     *
     * @version 1.0
     */
    public static boolean isBIsBackground() {
        return bIsBackground;
    }

    /**
     * Created on 2010/08/02
     * Title: setBIsBackground
     * Description: Naviはバックグランドがあるかどうかを設定する
     *
     * @param1 boolean isBackground
     * @return void
     *
     * @version 1.0
     */
    public static void setBIsBackground(boolean isBackground) {
        bIsBackground = isBackground;
    }

    /**
     * Created on 2010/08/02
     * Title: getSatInfoList_t
     * Description: GPS情報を取得する
     *
     * @param1 無し
     * @return ZVP_SatInfoList_t
     *
     * @version 1.0
     */
    public static ZVP_SatInfoList_t getSatInfoList_t() {
        return SatInfoList_t;
    }

    /**
     * Created on 2010/08/02
     * Title: setSatInfoList_t
     * Description: GPS情報を設定する
     *
     * @param1 ZVP_SatInfoList_t satInfoList_t GPS情報
     * @return void
     *
     * @version 1.0
     */
    public static void setSatInfoList_t(ZVP_SatInfoList_t satInfoList_t) {
        SatInfoList_t = satInfoList_t;
    }

    /**
     * Created on 2010/08/02
     * Title: getSatInfoList_t
     * Description: 高度を取得する
     *
     * @param1 無し
     * @return double
     *
     * @version 1.0
     */
    public static double getAltitude() {
        return Altitude;
    }

    /**
     * Created on 2010/08/02
     * Title: setSatInfoList_t
     * Description: 高度を設定する
     *
     * @param1 double altitude 高度
     * @return void
     *
     * @version 1.0
     */
    public static void setAltitude(double altitude) {
        Altitude = altitude;
    }

    /**
     * Created on 2010/08/02
     * Title: getSatInfoList_t
     * Description: Dateを取得する
     *
     * @param1 無し
     * @return String
     *
     * @version 1.0
     */
    public static String getDate() {
        return date;
    }

    /**
     * Created on 2010/08/02
     * Title: setSatInfoList_t
     * Description: Dateを設定する
     *
     * @param1 String date
     * @return void
     *
     * @version 1.0
     */
    public static void setDate(String date) {
        CommonLib.date = date;
    }

    private static JNIInt naviMode = new JNIInt();

    /**
     * ナビモードを戻る
     *
     * @return　ナビモード
     * @see net.zmap.android.pnd.v2.common.Constants#NE_NAVIMODE_CAR
     * @see net.zmap.android.pnd.v2.common.Constants#NE_NAVIMODE_MAN
     * @see net.zmap.android.pnd.v2.common.Constants#NE_NAVIMODE_BICYCLE
     */
    public static int getNaviMode() {
        NaviRun.GetNaviRunObj().JNI_NE_GetNaviMode(naviMode);
        return naviMode.getM_iCommon();
    }
    public static int getNaviCarMode() {
        NaviRun.GetNaviRunObj().JNI_NE_GetNaviCarMode(naviMode);
        return naviMode.getM_iCommon();
    }
    /**
     * ナビモードを設定
     *
     * @param naviMode
     *            ナビモード
     * @see net.zmap.android.pnd.v2.common.Constants#NE_NAVIMODE_CAR
     * @see net.zmap.android.pnd.v2.common.Constants#NE_NAVIMODE_MAN
     * @see net.zmap.android.pnd.v2.common.Constants#NE_NAVIMODE_BICYCLE
     */
    public static void setNaviMode(int naviMode) {
        NaviRun.GetNaviRunObj().JNI_NE_SetNaviMode(naviMode);
        NaviLog.v(NaviLog.PRINT_LOG_TAG, "change navigation mode. (" + naviMode + ")");
    }

    public static void clearViewInLayout(View child) {
        if (child != null && child.getParent() != null) {
            ViewGroup vg = (ViewGroup)(child.getParent());
            vg.removeView(child);
        }
    }

    private static boolean route5Mode = false;;

    public static boolean is5RouteMode() {
        return route5Mode;
    }

    public static void set5RouteMode(boolean is5Route) {
        route5Mode = is5Route;
    }

    private static boolean isChangePosFromSearch;
    private static boolean isChangePos;
// Add by CPJsunagawa '2015-07-08 Start
    private static boolean isRegIcon;
    private static boolean isInputText;
// Add by CPJsunagawa '2015-07-08 End
// Add by CPJsunagawa '2015-08-15 Start
    private static boolean isMoveIcon;
    private static boolean isDeleteIcon;
    private static boolean isDrawLine;
// Add by CPJsunagawa '2015-08-15 End

    public static boolean isChangePosFromSearch() {
        return isChangePosFromSearch;
    }

    public static void setChangePosFromSearch(boolean isChangePosFromSearch) {
        CommonLib.isChangePosFromSearch = isChangePosFromSearch;
    }

    public static boolean isChangePos() {
        return isChangePos;
    }

    public static void setChangePos(boolean isChangePos) {
        CommonLib.isChangePos = isChangePos;
    }
// Add by CPJsunagawa '2015-07-08 Start
    public static boolean isRegIcon() {
        return isRegIcon;
    }

    public static void setRegIcon(boolean isRegIcon) {
    	if(isRegIcon == false) {
    		int a = 1;
        	System.out.println("***　setRegIcon is false　***");
    	} else {
    		int b = 1;
        	System.out.println("***　setRegIcon is true　***");
    	}
        CommonLib.isRegIcon = isRegIcon;
    }

    public static boolean isInputText() {
        return isInputText;
    }

    public static void setInputText(boolean isInputText) {
        CommonLib.isInputText = isInputText;
    }
// Add by CPJsunagawa '2015-07-08 End
	
// Add by CPJsunagawa '2015-08-15 Start
    public static boolean isMoveIcon() {
        return isMoveIcon;
    }

    public static void setMoveIcon(boolean isMoveIcon) {
    	if(isMoveIcon == false) {
    		int a = 1;
        	System.out.println("***　isMoveIcon is false　***");
    	} else {
    		int b = 1;
        	System.out.println("***　isMoveIcon is true　***");
    	}
        CommonLib.isMoveIcon = isMoveIcon;
    }

    public static boolean isDeleteIcon() {
        return isDeleteIcon;
    }

    public static void setDeleteIcon(boolean isDeleteIcon) {
    	if(isDeleteIcon == false) {
    		int a = 1;
        	System.out.println("***　isDeleteIcon is false　***");
    	} else {
    		int b = 1;
        	System.out.println("***　isDeleteIcon is true　***");
    	}
        CommonLib.isDeleteIcon = isDeleteIcon;
    }

    public static boolean isDrawLine() {
        return isDrawLine;
    }

    public static void setDrawLine(boolean isDrawLine) {
    	if(isDrawLine == false) {
    		int a = 1;
        	System.out.println("***　isDrawLine is false　***");
    	} else {
    		int b = 1;
        	System.out.println("***　isDrawLine is true　***");
    	}
        CommonLib.isDrawLine = isDrawLine;
    }
// Add by CPJsunagawa '2015-08-15 End

    /**
     * 距離をフォーマットする。
     *
     * @param lDistance
     *            距離
     * @return 距離 距離 >= 1000の場合 *.* km, 距離 < 1000の場合 * m
     */
    public static String getFormatDistance(long lDistance) {
        String strDistance;
        double dCalcDistance;
        long lnCalcDistance;

// Add 2011/09/21 Z01kkubo Start --> 課題No.4,No.5対応
//        // ナビモード取得
//        int naviMode = CommonLib.getNaviMode();
//
//        // 車モードの場合は10m単位に切り捨てる
//        if (naviMode == Constants.NE_NAVIMODE_CAR && lDistance < 1000) {
//        	lDistance /= 10;
//        	lDistance *= 10;
//        }
// Add 2011/09/21 Z01kkubo End <--

        //NaviLog.d("CommonLib -> getFormatDistance", "getFormatDistance" + lDistance);
        if (Constants.LONG_MAX == lDistance) {
            strDistance = "5km以上";
            return strDistance;
        }

        if (lDistance < 0) {
            return "0m";
        }

        if (lDistance < 1000) {
            strDistance = (lDistance) + "m";
        } else {
            if (lDistance % 1000 == 0) {
                strDistance = (lDistance / 1000) + "km";
            } else {
                dCalcDistance = (double)lDistance / (double)1000;
                if (dCalcDistance < (double)10.0) {
                    lnCalcDistance = (long)(dCalcDistance * 10);
                    dCalcDistance = (double)lnCalcDistance / (double)10;
                    //10km以下
                    NumberFormat ddf1 = NumberFormat.getNumberInstance();
                    ddf1.setMaximumFractionDigits(1);
                    strDistance = ddf1.format(dCalcDistance) + "km";
                } else {
                    //10km以上
                    lnCalcDistance = (long)(dCalcDistance + 0.5);
                    NumberFormat ddf1 = NumberFormat.getNumberInstance();
                    ddf1.setMaximumFractionDigits(0);
                    strDistance = ddf1.format(lnCalcDistance) + "km";
                }
            }
        }

        return strDistance;
    }

    public static void setDistenceText(TextView view, String text, int textSize) {

        if (null == text) {
            return;
        }
        int start = text.indexOf("km");
        if (start < 0) {
            start = text.indexOf("m");
        }
        if (start >= 0) {
            SpannableStringBuilder style = new SpannableStringBuilder(text);
            style.setSpan(new AbsoluteSizeSpan(textSize), start, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(style);
        }
    }

    /**
     * 到着予想時刻をフォーマットする。
     *
     * @param hour
     *            時
     * @param minute
     *            分
     * @return 予測時刻（h:mm）
     */
    public static String getFormatTime(int hour, int minute) {
        return hour + ":" + (minute < 10 ? "0" + minute : minute);
    }

    /**
     *
     * Created on 2010/01/15
     * Title：getFormatGuideDistance
     * Description:距離をフォーマットする
     *
     * @param lDistance
     *            距離
     * @return 距離 距離 >= 1000の場合、*.* km 距離 < 1000の場合、* m
     *
     * @version 1.0
     */
    public static String getFormatGuideDistance(long lDistance) {
        String strDistance;

// Add 2011/09/21 Z01kkubo Start --> 課題No.4,No.5対応
//        // ナビモード取得
//        int naviMode = CommonLib.getNaviMode();
//
//        // 車モードの場合は10m単位に切り捨てる
//        if (naviMode == Constants.NE_NAVIMODE_CAR && lDistance < 1000) {
//        	lDistance /= 10;
//        	lDistance *= 10;
//        }
// Add 2011/09/21 Z01kkubo End <--

        if (0 == lDistance) {
            return null;
        } else {
            if (lDistance >= 10000) {
                strDistance = (lDistance / 1000) + "km";
            } else if (lDistance >= 1000) {
                NumberFormat ddf1 = NumberFormat.getNumberInstance();
                ddf1.setMaximumFractionDigits(1);
                strDistance = ddf1.format((double)lDistance / (double)1000) + "km";
            } else {
                strDistance = (lDistance) + "m";
            }
        }

        return strDistance;
    }

    /**
     *
     * Created on 2010/01/15
     * Title：getFormatGuideMinute
     * Description:時間をフォーマットする
     *
     * @param lMinute
     * @return String
     *
     * @version 1.0
     */
    public static String getFormatGuideMinute(long lMinute) {
        String strMinute;

        if (0 == lMinute) {
            strMinute = (lMinute) + "分";
        } else {
            if (lMinute >= 60) {
                if (0 == lMinute % 60) {
                    strMinute = (lMinute / 60) + "時間";
                } else {
                    strMinute = (lMinute / 60) + "時間" + (lMinute % 60) + "分";
                }
            } else {
                strMinute = (lMinute % 60) + "分";
            }
        }

        return strMinute;
    }

    public static boolean isNaviStart() {
        return isNaviStart;
    }

    public static void setNaviStart(boolean isNaviStart) {
        CommonLib.isNaviStart = isNaviStart;
        NaviLog.v(NaviLog.PRINT_LOG_TAG, "change navigation status. (" + isNaviStart + ")");
    }

    private static boolean hasRoute;

    public static boolean hasRoute() {
        return hasRoute;
    }

    public static void setHasRoute(boolean b_HasRoute) {
        hasRoute = b_HasRoute;
    }

    public static boolean isNaviMode() {
        int mapMode = MapView.getInstance().getMap_view_flag();
        return mapMode == Constants.NAVI_MAP_VIEW
                || mapMode == Constants.SIMULATION_MAP_VIEW
                || mapMode == Constants.ROUTE_MAP_VIEW_BIKE;
    }

//Chg 2011/09/26 Z01yoneya Start -->
//    public static void guideEnd(boolean isForceEnd) {
//------------------------------------------------------------------------------------------------------------------------------------
    public static void guideEnd(boolean isForceEnd, NaviAppDataPath naviAppDataPath) {
//Chg 2011/09/26 Z01yoneya End <--

        CommonLib.setNaviStart(false);
        CommonLib.setHasRoute(false);

        // Start_Demo_Guide_FLAGが1の場合、デモ開始ボタン。2の場合案内開始ボタン
        if (getNaviMode() == Constants.NE_NAVIMODE_CAR) {
            if (CommonLib.Start_Demo_Guide_FLAG == 1) {
                NaviRun.GetNaviRunObj().JNI_DG_endDemoDrive(1);
            } else {
                NaviRun.GetNaviRunObj().JNI_DG_endDemoDrive(0);
            }
        }

        MapView mapView = MapView.getInstance();
        if (isForceEnd) {
            CommonLib.setBIsOnHighWay(false);
            mapView.updateScale();
//Chg 2011/09/26 Z01yoneya Start -->
//            CommonLib.deleteNaviDataFile();
//------------------------------------------------------------------------------------------------------------------------------------
            CommonLib.deleteNaviDataFile(naviAppDataPath);
//Chg 2011/09/26 Z01yoneya End <--
            CommonLib.setBIsNaviEnd(true);
            mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
        } else {
            //
            if (mapView.getMap_view_flag() == Constants.SIMULATION_MAP_VIEW) {
                CommonLib.setBIsOnHighWay(false);
                mapView.setMap_view_flag(Constants.ROUTE_MAP_VIEW);
                mapView.updateScale();
            } else if (mapView.getMap_view_flag() == Constants.NAVI_MAP_VIEW) {
//Chg 2011/09/26 Z01yoneya Start -->
//                CommonLib.deleteNaviDataFile();
//------------------------------------------------------------------------------------------------------------------------------------
                CommonLib.deleteNaviDataFile(naviAppDataPath);
//Chg 2011/09/26 Z01yoneya End <--
                CommonLib.setBIsNaviEnd(true);
                mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
            }
        }

        if (CommonLib.isBIsOnHighWay()) {
            CommonLib.setBIsOnHighWay(false);
        }

        CommonLib.setBIsGuideEnd(true);
        NaviRun.GetNaviRunObj().JNI_ct_uic_StopGuide();
        if (CommonLib.Start_Demo_Guide_FLAG == 1) {
            NaviRun.GetNaviRunObj().JNI_NE_StopGuidance();
            CommonLib.Start_Demo_Guide_FLAG = 0;
        }
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();

        CommonLib.setBIsShowGuide(false);
        CommonLib.setBIsDirectionBoard(false);
        CommonLib.setBIsShowJct(false);
        CommonLib.setBIsShowMagMap(false);
        CommonLib.setBIsShowMagMapName(false);
        CommonLib.setBIsAppoachIntersection(false);

        CommonLib.setBIsRouteDisplay(false);
    }

    public static String[] tokenize(String sStr, char[] czSign) {
        if (sStr != null && czSign != null && czSign.length > 0) {
            int wBegin;
            Vector<String> ovStr = new Vector<String>();
            char[] czStr = sStr.toCharArray();

            wBegin = 0;
            for (int i = 0; i < czStr.length; ++i) {
                if (czStr[i] == '\"' || czStr[i] == '\'') {
                    char cMid = czStr[i];
                    while (i < czStr.length - 1) {
                        ++i;
                        if (czStr[i] == '\\') {
                            if (i < czStr.length - 1) ++i;
                        } else if (cMid == czStr[i]) break;
                    }
                } else {
                    for (int j = 0; j < czSign.length; ++j) {
                        if (czStr[i] == czSign[j]) {
                            ovStr.addElement(new String(czStr, wBegin, i - wBegin));
                            wBegin = i + 1;
                            break;
                        }
                    }
                }
            }

            if (wBegin == 0) {
                String[] szOutput = new String[1];
                szOutput[0] = sStr;
                return szOutput;
            } else {
                ovStr.addElement(new String(czStr, wBegin, czStr.length - wBegin));
                return vectorToArray(ovStr);
            }
        }
        return null;
    }

    /**
     *
     * @param
     * @param
     * @return
     */
    static public String[] tokenize(String sStr, String sSign) {
        if (sStr != null && sSign != null && sSign.length() > 0) {
            Vector<String> ovStr = new Vector<String>();

            int wBegin = 0;
            int wEnd = 0;
            while ((wEnd = sStr.indexOf(sSign, wBegin)) != -1) {
                ovStr.addElement(sStr.substring(wBegin, wEnd));
                wBegin = wEnd + sSign.length();
            }
            if (wBegin == 0) {
                String[] szOutput = new String[1];
                szOutput[0] = sStr;
                return szOutput;
            } else {
                ovStr.addElement(sStr.substring(wBegin));
                return vectorToArray(ovStr);
            }
        }
        return null;
    }

    /**
     *
     * @param
     * @return
     */
    static public String[] vectorToArray(Vector<String> ovInput) {
        if (ovInput != null) {
            String[] szOutput = new String[ovInput.size()];
//			for(int i = 0;i< ovInput.size();++i)
//			{
//				szOutput[i] = (String)ovInput.elementAt(i);
//			}
            ovInput.copyInto(szOutput);
            return szOutput;
        }
        return null;
    }

    /**
     * 探索距離設定
     *
     * @return
     */
    public static long calcRouteDistance() {
        ROUTE_DISTANCE routeDistance = new ROUTE_DISTANCE();
        NaviRun.GetNaviRunObj().JNI_java_GetRouteCalcDistance(routeDistance);
//      long num = routeDistance.getRouteCount();
//      JNILong[] alnDistance = new JNILong[(int) num];
        JNILong[] alnDistance;
        alnDistance = routeDistance.getalnDistance();
        long lAllDistance = 0;
        for (int i = 0; i < routeDistance.getRouteCount(); i++) {
            lAllDistance += alnDistance[i].lcount;
        }
        if (lAllDistance > 0) {
            return lAllDistance;
        } else {
            return 0;
        }

    }

    /**
     * 走行中の操作制限
     *
     * @return true: 走行規制あり、false:走行規制なし
     */
    public static boolean isLimit() {

        JNIInt moveStatus = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_VP_GetMovingStatus(moveStatus);
        int iMoveStatus = moveStatus.getM_iCommon();
        //        NaviRun.GetNaviRunObj().JNI_NE_Navi_SetMovingSpeed(iSpeed);
        /*// NE_MovingStat_StopNow = 1, ///< 停止中  NE_MovingStat_MovingNow =2, ///< 走行中
         * Constants.RunStopflag
        */
        if (iMoveStatus == 2) {
            // 走行中
            Constants.RunStopflag = true;
        } else {
            Constants.RunStopflag = false;
        }
        JNIInt DriveMode = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_Java_GetDriveMode(DriveMode);
        if (DriveMode.getM_iCommon() == 1) {
            // 助手席
            Constants.UserModeflag = false;
        } else {
            // 運転手モード
            Constants.UserModeflag = true;
        }
        boolean flgCar = false;
        JNIInt mode = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetNaviMode(mode);
        if (mode.getM_iCommon() == Constants.NE_NAVIMODE_CAR) {
            flgCar = true;
        }
        // 制限かどうかを判断する
        if (Constants.RunStopflag == true
                && Constants.NaviOnOffflag == true
                && Constants.UserModeflag == true
                && flgCar == true) {
            return true;
        } else {
            return false;
        }
    }

    public static long calcDist(long lon1, long lat1, long lon2, long lat2) {
        lon1 = lon1 * 1000 / 256;
        lat1 = lat1 * 1000 / 256;

        lon2 = lon2 * 1000 / 256;
        lat2 = lat2 * 1000 / 256;
        JNILong lDistance = new JNILong();
        NaviRun.GetNaviRunObj().JNI_Lib_calcDistancePointToPoint(lon1, lat1, lon2, lat2, lDistance);//両点間距離を計算する(1/256で用)
        return lDistance.getLcount();
    }

    public static Bitmap rotate(Bitmap b, int degrees) {

        Bitmap b2 = null;
        if (degrees != 0 && b != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float)b.getWidth() / 2, (float)b.getHeight() / 2);
            try {
                b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
            } catch (OutOfMemoryError ex) {
                return b;
            }
        } else {
            //add liutch 0426 bug1036 -->
            return b;
            //<--
        }

        return b2;

    }
//Del 2011/11/10 Z01thedoanh Start -->
//    public static void log(String tag, String msg, Exception e) {
//        Log.e(tag, msg, e);
//    }
//Del 2011/11/10 Z01thedoanh End <--

    public static File openFile(String path, String name) throws IOException {
        FileService.createDir(path);
        File file = new File(path + name);
        if (!file.exists()) {
            boolean result = file.createNewFile();
            if (!result) {
                NaviLog.e(NaviLog.PRINT_LOG_TAG, "createNewFile error [" + path + " " + name + "]");
            }
        }
        return file;
    }

//Del 2012/02/09 Z01_h_yamada Start -->
//    public static Bitmap getScaledBitmap(Activity activity, int iconId, float scaleNumber) {
//        Bitmap temp = BitmapFactory.decodeResource(activity.getResources(), iconId);
//        return getScaledBitmap(activity, temp, scaleNumber);
//    }
//
//    public static Bitmap getScaledBitmap(Activity activity, Bitmap bitmap, float scaleNumber) {
//        if (bitmap == null) {
//            return null;
//        }
//        int dstWidth = (int)(bitmap.getWidth() * scaleNumber);
//        int dstHeight = (int)(bitmap.getHeight() * scaleNumber);
//        return Bitmap.createScaledBitmap(bitmap, dstWidth, dstHeight, false);
//    }

    public static Bitmap getTransparentBmp(Bitmap bmp) {
        if (bmp == null) {
            return null;
        }
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        if (width <= 0 || height <= 0) {
            return null;
        }
        int pixels[] = new int[width * height];
        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int index = i + j * width;
                int r = (pixels[index] >> 16) & 0xff;
                int g = (pixels[index] >> 8) & 0xff;
                int b = pixels[index] & 0xff;
                if (r == 0 && g == 0 && b == 0) {
                    pixels[index] = pixels[index] & 0x00ffffff;
                }
            }
        }
        Bitmap tempBmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        tempBmp.setPixels(pixels, 0, width, 0, 0, width, height);
        pixels = null;
        return tempBmp;
    }
  //Del 2012/02/09 Z01_h_yamada End <--

    public static boolean routeHasVia() {
        JNITwoLong JNIpositon = new JNITwoLong();
        JNIString JNIpositonName = new JNIString();
        JNIInt JNIpass = new JNIInt();
        for (int i = Constants.NAVI_VIA_INDEX; i < Constants.NAVI_DEST_INDEX; i++) {
            NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(i, JNIpositon, JNIpositonName, JNIpass);
            if (JNIpositon.getM_lLong() != 0 || JNIpositon.getM_lLat() != 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean isNaviState() {
        return isNaviMode() || isLoggerMode();
    }

    public static boolean isLoggerMode() {
        return (Constants.LoadMode_LOG == Constants.ON && Constants.VP_LoadMode_LoggerManager == Constants.ON)
                && LoggerManager.isLoggerLoad();
    }

    public static boolean isMapScrolled() {

        JNITwoLong MyPosi = new JNITwoLong();
        JNITwoLong MapCenter = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(MyPosi);
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(MapCenter);
        return ((MyPosi.getM_lLat() != MapCenter.getM_lLat()) && (MyPosi.getM_lLong() != MapCenter.getM_lLong()));
    }

    public static void getMemoryInfo(String key) {
        Runtime.getRuntime().gc();
        //NaviLog.d(key, "external alloc:" + VMRuntime.getRuntime().getExternalBytesAllocated());
        Runtime.getRuntime().gc();
    }

    public static void recycleBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }
            bitmap = null;
        }
    }

    ///
    public static String getFileCRCCode(File file) throws Exception {

        FileInputStream fileinputstream = new FileInputStream(file);

        CRC32 crc32 = new CRC32();

        CheckedInputStream checkedinputstream = new CheckedInputStream(fileinputstream, crc32);
        for (; checkedinputstream.read() != -1;) {

        }

        checkedinputstream.close();
        fileinputstream.close();

        return Long.toHexString(crc32.getValue());

    }

    public static void setIsOpenMap(boolean bFlag) {
        bIsOpenMap = bFlag;
    }

    public static boolean IsOpenMap() {
        return bIsOpenMap;
    }

    public static void setIsStartCalculateRoute(boolean bFlag) {
        bIsStartCalculateRoute = bFlag;
    }

    public static boolean IsStartCalculateRoute() {
        return bIsStartCalculateRoute;
    }

    public static void setIsCalcFromRouteEdit(boolean bFlag) {
        bIsFromRouteEdit = bFlag;
    }

    public static boolean getIsCalcFromRouteEdit() {
        return bIsFromRouteEdit;
    }

	public static void setIsShowDoubleMap(boolean bFlag){
    	bIsShowDoubleMap= bFlag;
	}
    public static boolean IsShowDoubleMap(){
    	return bIsShowDoubleMap;
	}

//Add 2011/07/27 Z01thedoanh (自由解像度対応) Start -->
    private static int bIsFullScreen = 1;
    public static int screenWidth = 800;
    public static int screenHeight = 480;
    private static float fGuideAreaWidth = 387;
    private static float fGuideAreaMarginLeft = 0;
    private static float fGuideAreaMarginRight = 0;
    private static float fGuideBackGroundScale = (float)1.0;
    private static int iOrientation = 2;

    public static int getIsFullScreeen() {
        return bIsFullScreen;
    }

    public static void setIsFullScreeen(int bFlag) {
        bIsFullScreen = bFlag;
    }

    public static Bitmap ReSizeBmpImage(float newHeight, float newWidth, Bitmap baseBitmap) {

        int baseWidth = baseBitmap.getWidth();
        int baseHeight = baseBitmap.getHeight();

//Add 2012/02/23 Z01_h_yamada Start --> #3839
        if (newHeight == 0 || newWidth == 0 || baseWidth == 0 || baseHeight == 0) {
            NaviLog.e(NaviLog.PRINT_LOG_TAG, "resize image size error. BW=" + baseWidth + ", BH=" + baseHeight + ", NW=" + newWidth + ", NH=" + newHeight);
        	return baseBitmap;
        }
//Add 2012/02/23 Z01_h_yamada End <--

        Matrix matrix = new Matrix();
        matrix.postScale(newWidth / baseWidth, newHeight / baseHeight);
//Chg 2012/02/07 Z01_h_yamada Start -->
//		return Bitmap.createBitmap(baseBitmap, 0, 0, baseWidth, baseHeight, matrix, true);
//--------------------------------------------
		Bitmap out;
		try {
			out = Bitmap.createBitmap(baseBitmap, 0, 0, baseWidth, baseHeight, matrix, true);
		} catch( OutOfMemoryError e ) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
			java.lang.System.gc();
			// 2回目は try-catch せず、2回目も失敗した時は諦める
			try {
				out = Bitmap.createBitmap(baseBitmap, 0, 0, baseWidth, baseHeight, matrix, true);
			} catch( OutOfMemoryError ex ) {
				return baseBitmap;
			}
		}
		return out;
//Chg 2012/02/07 Z01_h_yamada End <--
    }

    public static void setGuideAreaWidth(float areaWidth) {
        fGuideAreaWidth = areaWidth;
    }

    public static float getGuideAreaWidth() {
        return fGuideAreaWidth;
    }

    public static void setGuideAreaMarginLeft(float areaMarginLeft) {
        fGuideAreaMarginLeft = areaMarginLeft;
    }

    public static float getGuideAreaMarginLeft() {
        return fGuideAreaMarginLeft;
    }

    public static void setGuideAreaMarginRight(float areaMarginRight) {
        fGuideAreaMarginRight = areaMarginRight;
    }

    public static float getGuideAreaMarginRight() {
        return fGuideAreaMarginRight;
    }

    public static void setGuideBackGroundScale(float scale) {
        fGuideBackGroundScale = scale;
    }

    public static float getGuideBackGroundScale() {
        return fGuideBackGroundScale;
    }

    public static void setOrientation(int ori) {
        iOrientation = ori;
    }

    public static int getOrientation() {
        return iOrientation;
    }

//Add 2011/07/27 Z01thedoanh (自由解像度対応) End <--

//Add 2011/11/23 Z01_h_yamada Start -->
    private static boolean bStopLaunchServiceOnDestroy = false; //マップアクティビティ終了時に起動サービスを終了するフラグ

    public static void setStopServiceFlag(boolean bStopFlag) {
    	NaviLog.d(NaviLog.PRINT_LOG_TAG,  "CommonLib::setStopServiceFlag " + bStopLaunchServiceOnDestroy + " -> " + bStopFlag);
        bStopLaunchServiceOnDestroy = bStopFlag;
    }

    public static boolean getStopServiceFlag() {
        return bStopLaunchServiceOnDestroy;
    }
//Add 2011/11/23 Z01_h_yamada End <--

//Add 2011/12/14 Z01_h_yamada Start -->
    public static Bitmap rotateCommpas(Bitmap srcBmp, int newWidth, int newHeight, int angle ) {

	    Bitmap bitmap = CommonLib.ReSizeBmpImage(newHeight, newWidth, srcBmp);
	    if (angle % 360 == 0) {
	        //回転なし
	        return bitmap;
	    }

        //回転あり
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);

//Chg 2012/02/07 Z01_h_yamada Start -->
//      Bitmap b1 = Bitmap.createBitmap(bitmap, 0, 0, newWidth, newHeight, matrix, true);
//--------------------------------------------
        Bitmap b1;
        try {
        	b1 = Bitmap.createBitmap(bitmap, 0, 0, newWidth, newHeight, matrix, true);
        } catch( OutOfMemoryError e ) {
        	NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        	java.lang.System.gc();
        	// 2回目は try-catch せず、2回目も失敗した時は諦める
            try {
            	b1 = Bitmap.createBitmap(bitmap, 0, 0, newWidth, newHeight, matrix, true);
            } catch( OutOfMemoryError ex ) {
            	return bitmap;
            }
        }
        if (bitmap != null) {
        	bitmap.recycle();
        	bitmap = null;
        }
//Chg 2012/02/07 Z01_h_yamada End <--

        int x = (b1.getWidth() - newWidth) / 2;
        int y = (b1.getHeight() - newHeight) / 2;
        int w = newWidth+1;
        int h = newHeight+1;

        if (w >= b1.getWidth()) {
        	w = b1.getWidth();
        	x = 0;
        }
        if (h >= b1.getHeight()) {
        	h = b1.getHeight();
        	y = 0;
        }
        if (x + w > b1.getWidth()) {
        	x = b1.getWidth() -w;
        }
        if (x < 0) {
        	x = 0;
        }
        if (y + h > b1.getHeight()) {
        	y = b1.getHeight() -h;
        }
        if (y < 0) {
        	y = 0;
        }
//Chg 2012/02/07 Z01_h_yamada Start -->
//      return Bitmap.createBitmap(b1, x, y, w, h);
//--------------------------------------------
        Bitmap out;
        try {
        	out = Bitmap.createBitmap(b1, x, y, w, h);
        } catch( OutOfMemoryError e ) {
        	NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        	java.lang.System.gc();
        	// 2回目は try-catch せず、2回目も失敗した時は諦める
            try {
            	out = Bitmap.createBitmap(b1, x, y, w, h);
            } catch( OutOfMemoryError ex ) {
            	return b1;
            }
        }
        if (b1 != null) {
        	b1.recycle();
        	b1 = null;
        }
        return out;
//Chg 2012/02/07 Z01_h_yamada End <--
    }
//Add 2011/12/14 Z01_h_yamada End <--


    // 地点情報の取得
    public static ZNE_RoutePoint[] getRoutePoints(RoutePoint startPoint, RoutePoint destination, List<RoutePoint> waypoints) {
        final int NUM_POINTS = 9;

        ZNE_RoutePoint[] routePoints = new ZNE_RoutePoint[NUM_POINTS];
        for (int i = 0; i < routePoints.length; i++) {
            routePoints[i] = null;
        }

        if (startPoint != null) {
        	
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
        	GeoPoint coords;
	    	if( startPoint.getPoint().getWorldPoint() ){
	    		coords = GeoUtils.toTokyoDatum(startPoint.getPoint());
	    	}
	    	else{
	    		coords = startPoint.getPoint();
	    	}
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END
        	
            JNITwoLong lonLat = new JNITwoLong();
            lonLat.setM_lLat(coords.getLatitudeMs());
            lonLat.setM_lLong(coords.getLongitudeMs());

            ZNE_RoutePoint point = new ZNE_RoutePoint();
            point.setSzPointName(startPoint.getName());
            point.setStPos(lonLat);
            point.setIPassed(0);
            routePoints[Constants.NAVI_START_INDEX] = point;
            NaviLog.d(TAG, "routePoints[" + Constants.NAVI_START_INDEX + "]---->name[" +  point.getSzPointName() + "], latitude[" + startPoint.getPoint().getLatitudeMs() + "], longitude[" + startPoint.getPoint().getLongitudeMs() + "]");
        }

        for (int i = 0; i < waypoints.size(); i++) {
            RoutePoint node = waypoints.get(i);

// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
        	GeoPoint coords;
	    	if( node.getPoint().getWorldPoint() ){
	    		coords = GeoUtils.toTokyoDatum(node.getPoint());
	    	}
	    	else{
	    		coords = node.getPoint();
	    	}
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END
        	
            JNITwoLong lonLat = new JNITwoLong();
            lonLat.setM_lLat(coords.getLatitudeMs());
            lonLat.setM_lLong(coords.getLongitudeMs());

            ZNE_RoutePoint point = new ZNE_RoutePoint();
            point.setSzPointName(node.getName());
            point.setStPos(lonLat);
            point.setIPassed(0);

            routePoints[Constants.NAVI_VIA_INDEX + i] = point;
            int j = Constants.NAVI_VIA_INDEX + i;
            NaviLog.d(TAG, "routePoints[" + j + "]---->name[" +  point.getSzPointName() + "], latitude[" + coords.getLatitudeMs() + "], longitude[" + coords.getLongitudeMs() + "]");
        }

// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
    	GeoPoint coords;
    	if( destination.getPoint().getWorldPoint() ){
    		coords = GeoUtils.toTokyoDatum(destination.getPoint());
    	}
    	else{
    		coords = destination.getPoint();
    	}
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END
    	
        JNITwoLong lonLat = new JNITwoLong();
        lonLat.setM_lLat(coords.getLatitudeMs());
        lonLat.setM_lLong(coords.getLongitudeMs());

        ZNE_RoutePoint point = new ZNE_RoutePoint();
        point.setSzPointName(destination.getName());
        point.setStPos(lonLat);
        point.setIPassed(0);
        routePoints[Constants.NAVI_DEST_INDEX] = point;
        NaviLog.d(TAG, "routePoints[" + Constants.NAVI_DEST_INDEX + "]---->name[" +  point.getSzPointName() + "], latitude[" + coords.getLatitudeMs() + "], longitude[" + coords.getLongitudeMs() + "]");

        return routePoints;
    }

    public static ZNE_RoutePoint[] getRoutePoints(String goalName, GeoUri geo, List<String> viaNames, List<Location> viaLocations) {
        final int NUM_POINTS = 9;

        ZNE_RoutePoint[] routePoints = new ZNE_RoutePoint[NUM_POINTS];
        for (int i = 0; i < routePoints.length; i++) {
            routePoints[i] = null;
        }

        if (viaLocations != null) {
            for (int i = 0; i < viaLocations.size(); i++) {
                JNITwoLong lonLat = new JNITwoLong();
                lonLat.setM_lLat(GeoUtils.toTokyoDatum(viaLocations.get(i)).getLatitudeMs());
                lonLat.setM_lLong(GeoUtils.toTokyoDatum(viaLocations.get(i)).getLongitudeMs());

                ZNE_RoutePoint point = new ZNE_RoutePoint();
                if (viaNames != null && i < viaNames.size()) {
                    point.setSzPointName(viaNames.get(i));
                }
                point.setStPos(lonLat);
                point.setIPassed(0);
                routePoints[Constants.NAVI_VIA_INDEX + i] = point;
                int j = Constants.NAVI_VIA_INDEX + i;
                NaviLog.d(TAG, "routePoints[" + j + "]---->name[" +  point.getSzPointName() + "], latitude[" + lonLat.getM_lLat() + "], longitude[" + lonLat.getM_lLong() + "]");
            }
        }

        GeoPoint goalcoordinate = GeoUtils.toTokyoDatum(geo.getLatitude(), geo.getLongitude());
        JNITwoLong lonLat = new JNITwoLong();
        lonLat.setM_lLat(goalcoordinate.getLatitudeMs());
        lonLat.setM_lLong(goalcoordinate.getLongitudeMs());

        ZNE_RoutePoint point = new ZNE_RoutePoint();
        point.setSzPointName(goalName);
        point.setStPos(lonLat);
        point.setIPassed(0);
        routePoints[Constants.NAVI_DEST_INDEX] = point;
        NaviLog.d(TAG, "routePoints[" + Constants.NAVI_DEST_INDEX + "]---->name[" +  point.getSzPointName() + "], latitude[" + goalcoordinate.getLatitudeMs() + "], longitude[" + goalcoordinate.getLongitudeMs() + "]");

        return routePoints;
    }

    public static boolean isExternalAction(Intent intent) {
        String action = intent.getAction();
        if (Constants.INTENT_VIEW_ACTION.equals(action)) {
            return true;
        }
        if (Constants.INTENT_NAVI_ACTION.equals(action)) {
            return true;
        }
        return false;
    }

    private static final int    ROUTE_CALCULATE_POINT_NAME_LENGTH = 40;
    public static boolean checkRouteRequest(NaviResult result, RouteRequest routeRequest) {
        RoutePoint point = routeRequest.getOrigin();
        if (point != null) {
            int checkResult = checkRoutePoint(point);
            if (checkResult != NaviResult.RESULT_OK) {
                result.setResult(checkResult, "origin name[" + point.getName() + "]");
                return false;
            }
        }

        point = routeRequest.getDestination();
        if (point != null) {
            int checkResult = checkRoutePoint(point);
            if (checkResult != NaviResult.RESULT_OK) {
                result.setResult(checkResult, "destination name[" + point.getName() + "]");
                return false;
            }
        }

        List<RoutePoint> list = routeRequest.getWaypoints();
        for (int i = 0; i < list.size(); i++) {
            RoutePoint wayPoint = list.get(i);
            int checkResult = checkRoutePoint(wayPoint);
            if (checkResult != NaviResult.RESULT_OK) {
                result.setResult(checkResult, "wayPoint[" + i + "] name[" + wayPoint.getName() + "]");
                return false;
            }
        }

        return true;
    }

    private static int checkRoutePoint(RoutePoint point) {
        if (point == null) {
            return NaviResult.RESULT_OK;
        }
        String name = point.getName();
        if (name != null && name.length() > 0 && name.getBytes().length > ROUTE_CALCULATE_POINT_NAME_LENGTH) {
            return NaviResult.RESULT_INVALID_VALUE;
        }
        return NaviResult.RESULT_OK;
    }

    public static boolean isDebuggable(Context context) {
        PackageManager manager = context.getPackageManager();
        ApplicationInfo appInfo = null;
        try {
            appInfo = manager.getApplicationInfo(context.getPackageName(), 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        if ((appInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) == ApplicationInfo.FLAG_DEBUGGABLE) {
            return true;
        }
        return false;
    }

    private static final String     GPS_INI_FILE = "gps.ini";
    private static final String     LABEL_GPS_REQUEST_MIN_TIME = "GPSRequestMinTime";
    private static int              mGPSRequestMinTime = 200;
    public static void readGPSIniFile(NaviAppDataPath naviAppDataPath) {
        String fileName = naviAppDataPath.getUserDataPath() + GPS_INI_FILE;

        FileReader fr = null;
        BufferedReader br = null;
        try {
            fr = new FileReader(fileName);
            br = new BufferedReader(fr);

            String line;
            while((line = br.readLine()) != null) {
                if (line.length() == 0 || line.startsWith("#")) {
                    continue;
                }

                String[] data = line.split("#")[0].split("=");
                if(data.length == 2) {
                    if (LABEL_GPS_REQUEST_MIN_TIME.equals(data[0].trim())) {
                        mGPSRequestMinTime = (new Integer(data[1].trim())).intValue();
                    }
                }
            }
        }
        catch (FileNotFoundException e) {
// DEL.2013.08.09 N.Sasao デフォルトで動作するスタックトレース表示を削除 START
//            e.printStackTrace();
// DEL.2013.08.09 N.Sasao デフォルトで動作するスタックトレース表示を削除  END
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (br != null) {
                br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return;
    }
    public static int getGPSRequestMinTime() {
        return mGPSRequestMinTime;
    }

    private static String      mMediaFileName;
    private static MediaPlayer mPlayer;

    public static String getMediaFileName() {
        return mMediaFileName;
    }

    public static void startMediaPlayer(String fileName) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "[DEBUG] startMediaPlayer fileName[" + fileName + "]");

        if (fileName == null || fileName.length() == 0) {
            return;
        }
        if (mMediaFileName != null && mMediaFileName.length() > 0 && !fileName.equals(mMediaFileName)) {
            releaseMediaPlayer();
            File file = new File(mMediaFileName);
            boolean result = file.delete();
            if (!result) {
                NaviLog.e(NaviLog.PRINT_LOG_TAG, "file delete error [" +mMediaFileName + "]");
            }
        }
        mMediaFileName = fileName;

        if (mPlayer == null) {
            mPlayer = new MediaPlayer();
        }
        else {
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
            }
            mPlayer.reset();
        }

        try {
            mPlayer.setDataSource(mMediaFileName);
            mPlayer.prepare();

            mPlayer.setOnCompletionListener(new OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    NaviLog.d(NaviLog.PRINT_LOG_TAG, "[DEBUG] mediaPlayer onCompletion");
                }
            });

            mPlayer.start();


        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void releaseMediaPlayer() {
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
    }

    public static boolean deleteVoiceFile(String pathName) {
        File path = new File(pathName);
        if (!path.exists()) {
            return false;
        }

        File[] work = path.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isFile() && !file.isHidden() && file.getName().startsWith(Constants.PREFIX_VOICE_FILE);
            }
        });

        for (int i = 0; i < work.length; i++) {
            work[i].delete();
        }

        return true;
    }
 // ADD.2013.10.21 N.Sasao ファイル削除(再帰処理)移植 START
 	//ファイルやフォルダを削除
 	//フォルダの場合、中にあるすべてのファイルやサブフォルダも削除されます
 	public static boolean deleteFileAll(File dirOrFile) {
 	    if (dirOrFile.isDirectory()) {//ディレクトリの場合
 	        String[] children = dirOrFile.list();//ディレクトリにあるすべてのファイルを処理する
 	        for (int i=0; i<children.length; i++) {
 	            boolean success = deleteFileAll(new File(dirOrFile, children[i]));
 	            if (!success) {
 	                return false;
 	            }
 	        }
 	    }
 		return dirOrFile.delete();
 	}
 // ADD.2013.10.21 N.Sasao ファイル削除(再帰処理)移植  END

// ADD.2013.11.14 N.Sasao NaviService起動時のアイコン制御対応 START
 	/**
     * 動作OSが指定バージョン以上か確認する
     * 桁数が同一、桁数が違う同一のバージョンもtrueを返す
     */
 	public static boolean isUpperValueVersion( String value ){
    	String 	OSVersion = Build.VERSION.RELEASE;

    	if( OSVersion.length() <= 0 ){
    		return false;
    	}

    	String[] valueSplit = value.split("\\.");
    	String[] OSVersionSplit = OSVersion.split("\\.");

    	boolean bResult = true;

    	// 検索データの方が桁数が多い場合は、こちらで検索する
    	if( valueSplit.length >= OSVersionSplit.length ){
	    	for( int nIndex = 0; nIndex < valueSplit.length; nIndex++ ){
	    		if( nIndex < OSVersionSplit.length ){
	    			if( Integer.parseInt( valueSplit[nIndex] ) < Integer.parseInt( OSVersionSplit[nIndex] ) ){
	    				bResult = false;
	    				break;
	    			}
	    		}
	    		else{
	    			// こちらは検索データを全て確認出来るため、OSバージョンの桁が少ない時点で
	    			// 検索データがOSバージョン以上と確定なる
	    			break;
	    		}
	    	}
    	}
    	// OSバージョンが桁が多い場合はこちらで検索する
    	else{
			for( int nIndex = 0; nIndex < OSVersionSplit.length; nIndex++ ){
	    		if( nIndex < valueSplit.length ){
	    			if( Integer.parseInt( valueSplit[nIndex] ) < Integer.parseInt( OSVersionSplit[nIndex] ) ){
	    				bResult = false;
	    				break;
	    			}
	    		}
	    		else{
	    			// 検索データが4, OSバージョンが4.0の場合は一致とみなすため、除外する。
	    			// それ以外は検索データが大きくないと判断
	    			if( 0 != Integer.parseInt( OSVersionSplit[nIndex] ) ){
	    				bResult = false;
	    			}
	    			break;
	    		}
	    	}
    	}

    	return bResult;
    }
// ADD.2013.11.14 N.Sasao NaviService起動時のアイコン制御対応  END
// ADD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応 START
 	/**
     * 文字列をShiftJIS変換する。
     * OS4.4のバグで、2桁以下の文字列をgetBytesでShiftJIS変換すると文字化けするため
     * 変換文字前後に1Byteの文字を付加し、変換後に付加したデータ取り除いた物を返す
     */
 	public static byte[] getToSJIS(String str){

 		if( str.length() <= 0){
 			return null;
 		}
 		String strTmp = String.format("%s%s%s", DEF_SJIS_COVER, str, DEF_SJIS_COVER);
 		byte[] byteTmp = null;

		try {
			byteTmp = strTmp.getBytes("ShiftJIS");
		} catch (UnsupportedEncodingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		if( byteTmp == null ){
			return null;
		}

 		int nByteCnt = byteTmp.length;

 		if( nByteCnt <= 0 ){
			return null;
		}

 		byte[] rtn = new byte[nByteCnt];

 		for( int i = 0; i < nByteCnt; i++ ){
 			// 前後DEF_SJIS_COVERの値を除き、前詰めコピーする
 			if( i != 0 && i != nByteCnt-1 ){
 				rtn[i-1] = byteTmp[i];
 			}
 		}

  		return rtn;
 	}
// ADD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応  END
// ADD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる) START
 	public static void setSingleMapActivityStatus( boolean bSingle ){
 		mIsSigleMapActivity = bSingle;
 	}
 	public static boolean isSingleMapActivity(){
 		return mIsSigleMapActivity;
	}
// ADD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる)  END
}
