package net.zmap.android.pnd.v2.common.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class WalkingStatusService
{
//	private WalkingStatusEventReceiver mWalkingEventReceiver = null;
//	private Context mContext = null;
	private WalkingStatusEventListener mWalkingListener = null;

	public WalkingStatusService(Context context, WalkingStatusEventListener listener) {
//		mContext = context;
		mWalkingListener = listener;

	}

//	public void release() {
//		if (mContext != null && mWalkingEventReceiver != null) {
//			mContext.unregisterReceiver(mWalkingEventReceiver);
//		}
//	}

	public class WalkingStatusEventReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context oContext, Intent oIntent) {
			if (mWalkingListener != null) {
				mWalkingListener.onWalkingEventChange(oIntent);
			}
		}
	}
}