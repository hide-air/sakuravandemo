package net.zmap.android.pnd.v2.overlay;

import android.util.Log;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.overlay.Icon;
import net.zmap.android.pnd.v2.api.overlay.Line;
import net.zmap.android.pnd.v2.api.overlay.OverlayItem;
import net.zmap.android.pnd.v2.common.utils.GeoUtils;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.engine.NaviEngineUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DrawUserLayer {

    private static final String TAG = "DrawUserLayer";

    private String mLayerId;
    private long mNELayerId;
    private Map<Long, OverlayItem> mNEFigureIdMap = new HashMap<Long, OverlayItem>();
//MOD.2013.09.19 N.Sasao 外部連携固定パス変更・後処理対応実装 START
	private DrawIconPathManager mDrawIconPathManager = null;
	private String mExternalDataPath = null;

    public DrawUserLayer( String layerId ) {
        mLayerId = layerId;
    }

    public DrawUserLayer( String layerId, String datapath ) {
        mLayerId = layerId;
        mExternalDataPath = datapath;
    }
//MOD.2013.09.19 N.Sasao 外部連携固定パス変更・後処理対応実装  END

    public String getLayerId() {
        return mLayerId;
    }

    public void setNELayerId(long naviEngineLayerId) {
        mNELayerId = naviEngineLayerId;
    }

    public long getNELayerId() {
        return mNELayerId;
    }

    /**
     * 描画ユーザ形状情報取得
     *
     * @param drawFigureId
     * @return
     */
    public OverlayItem getUserFigure(long drawFigureId) {
        return mNEFigureIdMap.get(Long.valueOf(drawFigureId));
    }

    /**
     * 描画ユーザ形状登録
     * @param userPoi ユーザ形状情報
     */
    public boolean registDrawUserPoi(Icon userPoi) {
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
    	JNITwoLong locateTKY = new JNITwoLong();
    	if( userPoi.getPoint().getWorldPoint() ){
    		locateTKY = GeoUtils.toTokyoJniTwoLong(userPoi.getPoint());
    	}
    	else{
            locateTKY.setM_lLong(userPoi.getPoint().getLongitudeMs());
            locateTKY.setM_lLat(userPoi.getPoint().getLatitudeMs());
    	}
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END
        JNILong figureId = new JNILong();
//MOD.2013.09.19 N.Sasao 外部連携固定パス変更・後処理対応実装 START
    	if( mDrawIconPathManager == null ){
    		mDrawIconPathManager = new DrawIconPathManager( mExternalDataPath );
    	}

        String pngFilePath = mDrawIconPathManager.getPngFilePath( userPoi.getUrl() );
//MOD.2013.09.19 N.Sasao 外部連携固定パス変更・後処理対応実装  END
        if (pngFilePath == null) {
            Log.e(TAG, "DrawIconPathManager.getPngFilePath error.");
            return false;
        }

        long ret = NaviRun.GetNaviRunObj().JNI_NE_AddUserFigure_Point(
                mNELayerId,
                locateTKY.getM_lLong(), locateTKY.getM_lLat(),
//Chg 2011/11/15 seto Start -->
//                userPoi.getContentTitle(), userPoi.getIconFilePath(), figureId);
//--------------------------------------------
                userPoi.getDescription(), pngFilePath, figureId);
//Chg 2011/11/15 seto End <--
        if (ret != 0) {
            return false;
        }

        userPoi.setNEID(figureId.getLcount());
        userPoi.setParentLayer(this);

        mNEFigureIdMap.put(Long.valueOf(figureId.getLcount()), userPoi);
// Add 2011/11/04 r.itoh Start -->
        NaviEngineUtils.redrawSurface(NaviRun.CT_ICU_CtrlType_Map1);
// Add 2011/11/04 r.itoh End <--

        return true;
    }

    public boolean registDrawUserLine(Line userLine) {

        List<GeoPoint> path = userLine.getPath();

        int locationNum = path.size();
        JNITwoLong[] locateTKYs = new JNITwoLong[locationNum];
        int i = 0;
        for (i = 0; i < locationNum; i++) {
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
        	if( path.get(i).getWorldPoint() ){
        		locateTKYs[i] = GeoUtils.toTokyoJniTwoLong(path.get(i));
        	}
        	else{
        		locateTKYs[i].setM_lLong(path.get(i).getLongitudeMs());
        		locateTKYs[i].setM_lLat(path.get(i).getLatitudeMs());
        	}
// ADD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END
        }
        JNILong figureId = new JNILong();




//        long ret = NaviRun.GetNaviRunObj().JNI_NE_AddUserFigure_Line(
//                mNELayerId,
//                locateTKYs,
//                userLine.getContentTitle(), userLine.getDrawParam(), figureId);
        long ret = NaviRun.GetNaviRunObj().JNI_NE_AddUserFigure_Line(
                mNELayerId,
                locateTKYs,
                "", userLine.getDrawParam(), figureId);
        if (ret != 0) {
            return false;
        }

        userLine.setNEID(figureId.getLcount());
        userLine.setParentLayer(this);

        mNEFigureIdMap.put(Long.valueOf(figureId.getLcount()), userLine);
// Add 2011/11/04 r.itoh Start -->
        NaviEngineUtils.redrawSurface(NaviRun.CT_ICU_CtrlType_Map1);
// Add 2011/11/04 r.itoh End <--

        return true;
    }

}

