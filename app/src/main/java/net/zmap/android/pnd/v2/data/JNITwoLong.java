/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           JNITwoLong.java
 * Description    2��long�^�̃f�[�^��class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class JNITwoLong {

    private long m_lLong;
    private long m_lLat;
    /**
    * Created on 2010/08/06
    * Title:       getM_lLong
    * Description:  第一個のデータを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLong() {
        return m_lLong;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_lLat
    * Description:  第2のデータを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLat() {
        return m_lLat;
    }

    /**
    * Created on 2010/08/06
    * Title:       setM_lLong
    * Description:  第一個のデータを設定する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public void setM_lLong(long long1) {
        m_lLong = long1;
    }
    /**
    * Created on 2010/08/06
    * Title:       setM_lLat
    * Description:  第2のデータを設定する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public void setM_lLat(long lat) {
        m_lLat = lat;
    }
}
