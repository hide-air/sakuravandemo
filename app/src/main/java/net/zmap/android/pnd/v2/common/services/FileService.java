package net.zmap.android.pnd.v2.common.services;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import net.zmap.android.pnd.v2.common.utils.NaviLog;

public class FileService
{
	private static final int SIZE = 4 * 1024;

	public static byte[] readFile(String sPath)
	{
		byte[] ozBuf = null;
		File oFile = new File(sPath);
		if(oFile.exists() && oFile.isFile())
		{
			FileInputStream oStream = null;
			try
			{
				oStream = new FileInputStream(oFile);
				ozBuf = readBtyeStream(oStream,SIZE);
			}
			catch(Exception e)
			{
				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
			}
			finally
			{
				try
				{
					if(oStream != null)
					{
						oStream.close();
					}
				}
				catch(Exception e1)
				{
					NaviLog.e(NaviLog.PRINT_LOG_TAG, e1);
				}
			}
		}
		return ozBuf;
	}


	public static boolean saveFile(String sDir, String sFileName, byte[] ozBuf)
	{
		boolean bRe = false;
		if(sDir != null && sFileName != null && ozBuf != null)
		{
			createDir(sDir);
			File oFile = new File(sDir, sFileName);
			FileOutputStream oStream = null;
			try
			{
				if(!oFile.exists())
				{
					if(!oFile.createNewFile())
					{
						return false;
					}
				}

				oStream = new FileOutputStream(oFile);
				oStream.write(ozBuf);
				bRe = true;
			}
			catch(Exception e)
			{
				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
			}
			finally
			{
				if(oStream != null)
				{
					try
					{
						oStream.close();
					}
					catch(Exception e1){
						NaviLog.e(NaviLog.PRINT_LOG_TAG, e1);
					}
				}
			}
		}
		return bRe;
	}

	public static void createDir(String sPath)
	{
		File oDir;
		String sMid;
		if(sPath.startsWith("/"))
		{
			sPath = sPath.substring(1);
		}
		int wIndex = sPath.indexOf('/');
		while(wIndex != -1)
		{
			sMid = sPath.substring(0,wIndex);
			oDir = new File(sMid);
			if(!oDir.exists())
			{
				boolean result = oDir.mkdir();
				if (!result) {
	                NaviLog.e(NaviLog.PRINT_LOG_TAG, "mkdir error [" + sMid + "]");
	                return;
				}
			}
			wIndex = sPath.indexOf('/', wIndex + 1);
		}
		oDir = new File(sPath);
		if(!oDir.exists())
		{
			boolean result = oDir.mkdir();
            if (!result) {
                NaviLog.e(NaviLog.PRINT_LOG_TAG, "mkdir error [" + sPath + "]");
                return;
            }
		}
	}

	public static void delete(String sPath,String sName)
	{
		if(sPath != null)
		{
			File oFile = new File(sPath + sName);
			if(oFile.isDirectory())
			{
				String[] szList = oFile.list();
				for(int i = 0;i < szList.length;++i)
				{
					delete(sPath,szList[i]);
				}
				oFile.delete();
			}
			else if(oFile.isFile())
			{
				oFile.delete();
			}
		}
	}

	public static boolean isExist(String sPath)
	{
		File oFile = new File(sPath);
		return oFile.exists();
	}

	private static byte[] readBtyeStream(InputStream oInputStream, int length)throws Exception
	{
		if (oInputStream != null)
		{
		    int bufferSize = 1024;
		    if (length > 0)
		    {
		        bufferSize = length;
		    }
		    ByteArrayOutputStream oBos = new ByteArrayOutputStream();
		    int wMid = 0;
		    byte[] bzBuf = new byte[bufferSize];
		    while (true)
		    {
		        wMid = oInputStream.read(bzBuf, 0, bufferSize);
		        if (wMid == -1)
		            break;
		        oBos.write(bzBuf, 0, wMid);
		    }
		    oBos.flush();
		    return oBos.toByteArray();
		}
		return null;
	}
}
