package net.zmap.android.pnd.v2.engine;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.exception.NaviInvalidValueException;
import net.zmap.android.pnd.v2.api.navigation.RoutePoint;
import net.zmap.android.pnd.v2.api.navigation.RouteRequest;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.utils.GeoUtils;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIShort;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.maps.MapView;
import android.location.Location;

public class NaviEngineUtils {

    public static final int NE_ROUTE_SEARCH_TYPE_MAN_COMMEND    = 1; ///< 探索種別(徒歩): 推奨
    public static final int NE_ROUTE_SEARCH_TYPE_MAN_ROOFED     = 1; ///< 探索種別(徒歩): 屋根
    public static final int NE_ROUTE_SEARCH_TYPE_MAN_EASINESS   = 2; ///< 探索種別(徒歩): 楽

    public static final int NE_ROUTE_SEARCH_TYPE_CAR_NONE     = 0; ///< 探索種別 : なし
    public static final int NE_ROUTE_SEARCH_TYPE_CAR_COMMEND  = 1; ///< 探索種別: 推奨
    public static final int NE_ROUTE_SEARCH_TYPE_CAR_GENERAL  = 2; ///< 探索種別: 一般
    public static final int NE_ROUTE_SEARCH_TYPE_CAR_DISTANCE = 3; ///< 探索種別: 距離
    public static final int NE_ROUTE_SEARCH_TYPE_CAR_WIDTH    = 4; ///< 探索種別: 道幅
    public static final int NE_ROUTE_SEARCH_TYPE_CAR_ANOTHER  = 5; ///< 探索種別: 別ルート

    public static final int NE_REGULTION_TYPE_USE_VICS              = (0x1<<4);     ///< 規制 : VICS利用
    public static final int NE_REGULTION_TYPE_USE_FERRY             = (0x1<<5);     ///< 規制 : フェリー使用
    public static final int NE_REGULTION_TYPE_THINK_WINTER_CLOSE    = (0x1<<6);     ///< 規制 : 冬季閉鎖道路使用
    public static final int NE_REGULTION_TYPE_THINK_TIME_RULE       = (0x1<<10);    ///< 規制 : 時間帯規制

    /**
     * デフォルト探索 規制情報取得
     * @return
     */
    static public int GetDefaultmRegulationType()
    {
        JNIInt regulationType = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_Drv_GetComplexProperty(regulationType);
        return regulationType.getM_iCommon();
    }

    /**
     * デフォルト探索種別取得
     * @return
     */
    static public int GetDefaultRouteSearchType( int naviMode )
    {
        JNIInt routeSearchType = new JNIInt();

        if (naviMode == Constants.NE_NAVIMODE_CAR) {
            NaviRun.GetNaviRunObj().JNI_NE_Drv_GetDefaultSearchProperty(routeSearchType);
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_Man_GetDefaultSearchProperty(routeSearchType);
        }

        return routeSearchType.getM_iCommon();
    }

    /**
     * デフォルトの探索条件を取得する
     * @return
     */
    static public RouteRequest getDefaulltRouteRequest( int naviMode )
    {
        RouteRequest routeCondition = new RouteRequest();

        try {
            routeCondition.setNaviMode( naviMode );
        } catch (NaviInvalidValueException e) {
            e.printStackTrace();
        }
        try {
            routeCondition.setSearchType( GetDefaultRouteSearchType(naviMode) );
        } catch (NaviInvalidValueException e) {
            e.printStackTrace();
        }

        long regulationType = GetDefaultmRegulationType();
        if ((regulationType & NE_REGULTION_TYPE_USE_FERRY) != 0) {
            try {
                routeCondition.addRegulation(RouteRequest.REGULATION_TYPE_FERRY);
            } catch (NaviInvalidValueException e) {
                e.printStackTrace();
            }
        }
        if ((regulationType & NE_REGULTION_TYPE_THINK_TIME_RULE) != 0) {
            try {
                routeCondition.addRegulation(RouteRequest.REGULATION_TYPE_TIME);
            } catch (NaviInvalidValueException e) {
                e.printStackTrace();
            }
        }
        if ((regulationType & NE_REGULTION_TYPE_THINK_WINTER_CLOSE) != 0) {
            try {
                routeCondition.addRegulation(RouteRequest.REGULATION_TYPE_SEASON);
            } catch (NaviInvalidValueException e) {
                e.printStackTrace();
            }
        }
        if ((regulationType & NE_REGULTION_TYPE_USE_VICS) != 0) {
            try {
                routeCondition.addRegulation(RouteRequest.REGULATION_TYPE_VICS);
            } catch (NaviInvalidValueException e) {
                e.printStackTrace();
            }
        }

        return routeCondition;

    }


    /**
     * 現在のルートの探索条件を取得する
     * @return
     */
    static public RouteRequest getCurrentRouteRequest()
    {
        int naviMode = CommonLib.getNaviMode();
        RouteRequest routeCondition = getDefaulltRouteRequest( naviMode );

        JNIInt routeType = new JNIInt();
        if ( naviMode == Constants.NE_NAVIMODE_CAR) {
            NaviRun.GetNaviRunObj().JNI_NE_Drv_GetSectionSearchProperty(
                    Constants.NAVI_START_INDEX, Constants.NAVI_DEST_INDEX, routeType);
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_Man_GetPointSearchProperty(Constants.NAVI_DEST_INDEX, routeType);
        }
        try {
            routeCondition.setSearchType( routeType.getM_iCommon());
        } catch (NaviInvalidValueException e) {
            e.printStackTrace();
        }

        JNITwoLong positon = new JNITwoLong();
        JNIString name = new JNIString();
        JNIInt passed = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(Constants.NAVI_DEST_INDEX, positon, name, passed);
        RoutePoint routePoint = new RoutePoint();
        routePoint.setName(name.getM_StrAddressString());
        routePoint.setPoint(new GeoPoint(positon.getM_lLat(), positon.getM_lLong()));
        routeCondition.setDestination(routePoint);

        for (int i = Constants.NAVI_VIA_INDEX; i < Constants.NAVI_DEST_INDEX; i++) {
            positon = new JNITwoLong();
            name = new JNIString();
            passed = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(i, positon, name, passed);

            if (!GeoUtils.inBoundMapTKY(positon.getM_lLat(), positon.getM_lLong())) {
                break;
            }

            routePoint = new RoutePoint();
            routePoint.setName(name.getM_StrAddressString());
            routePoint.setPoint(new GeoPoint(positon.getM_lLat(), positon.getM_lLong()));

            routeCondition.getWaypoints().add(routePoint);
        }

        return routeCondition;
    }

    /**
     * VP 方位から GPS 方位への変換
     */
    static private int vpDirToGpsDir(int vpDir) {
        // GPS 方位は北から時計周りに 0-359 度
        // VP 方位は北から半時計周りに 0-359 度
        return 360 - vpDir;
    }


    /**
     * @return
     */
    static public Location getVehicleLocation()
    {
        JNITwoLong lonLat = new JNITwoLong();
        JNILong speed = new JNILong();
        JNIShort direction = new JNIShort();
// MOD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応① START
        if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN || CommonLib.getNaviMode() == Constants.NE_NAVIMODE_BICYCLE ) {
        	NaviRun.GetNaviRunObj().JNI_CT_BG_GetVehiclePosition(lonLat, speed, direction);
        }
        else{
        NaviRun.GetNaviRunObj().JNI_CT_Drv_GetVehiclePosition2(lonLat, speed, direction);
        }
// MOD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応①  END
        // VP 方位は北から半時計周りに 0-359 度
        int vpDir = direction.getM_sJumpRecCount();

        Location location = GeoUtils.toWGS84Location(lonLat.getM_lLat(), lonLat.getM_lLong());
        // android.location.Location は GPS 方位 (direction of travel in degrees East of true North)
        location.setBearing(vpDirToGpsDir(vpDir));
        location.setSpeed(speed.getLcount() / 3.6f);
        location.setTime(System.currentTimeMillis());

        return location;
    }

    /**
     * ルートが表示されているか判定
     *
     * @return true 表示されている
     * @return false 表示されてない
     */
    public static boolean isShowRoute()
    {
        //NaviEngineからRouteの有無を習得したいが現状ないCommonLibで代用
        return CommonLib.hasRoute();
    }

 // Add 2011/11/04 r.itoh Start [ExternalAPI] -->
    /**
     * サーフェース再描画
     * @param surfaceType
     */
    public static void redrawSurface(int surfaceType) {
        NaviRun.GetNaviRunObj().JNI_CT_ICU_ReqRefleshMapUI(surfaceType);
        if (MapView.getInstance().getMap_view_flag() != Constants.SIMULATION_MAP_VIEW) {
            NaviRun.GetNaviRunObj().JNI_NE_ExposeMap();
        }
    }
// Add 2011/11/04 r.itoh End [ExternalAPI] <--

}
