package net.zmap.android.pnd.v2.inquiry.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.BackgroundColorSpan;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.NaviRun.NaviEngine;

public class KeyboardKanaView extends ItemView {

	public static String KEY = "key";
	public static String TEL = "tel";
	public String FAVORITES = "favorites";
	private String RECORD_ZERO_COUNT = "0件";

	private JNILong ListCount = new JNILong();
	private int minLimited = 0;
	private static int maxLimited = 0;
// Del 2011/09/19 r.itoh Start --> refactoring : delete falg that is not used.
//	private boolean btnEnable = false;
// Del 2011/09/19 r.itoh End <-- refactoring : delete falg that is not used.
	private static  EditText oText = null;
	private TextView oCountView = null;
	private Button btn_a = null;
	private Button btn_ka = null;
	private Button btn_sa = null;
	private Button btn_ta = null;
	private Button btn_na = null;
	private Button btn_ha = null;
	private Button btn_ma = null;
	private Button btn_ya = null;
	private Button btn_ra = null;
	private Button btn_wa = null;
	private Button btn_left = null;
	private Button btn_right = null;
	private static String changeValue = "";
	private static String oldchangeValue = "";
// Del 2011/09/19 r.itoh Start --> refactoring : delete falg that is not used.
//	private static boolean doSearchFlag = false;
// Del 2011/09/19 r.itoh End <-- refactoring : delete falg that is not used.


	private static  int clickFlag = 0;
	private static int leftClickFlag = 0;
// Del 2011/10/12 Z01kkubo Start --> 課題No.219 文字入力で長音を2つ以上連続投入不可とする対応
	//private static boolean rightClickFlag = false;
// Del 2011/10/12 Z01kkubo End <--
	private static Activity m_oContext = null;
	private Button o_SearchButton = null;
	private Button m_oEditBtn = null;
	private String keyType = "";
	private boolean doEnterFlag = false;
	/**false:Add  true : delete*/
	private boolean flag_AddAndDel = false;
	private boolean textChangeFlag = false;

	private int    mButtonId = 0;

	public TextView getoCountView() {
		return oCountView;
	}


	public void setoCountView(TextView oCountView) {
		this.oCountView = oCountView;
	}

	public Button getM_oEditBtn() {
		return m_oEditBtn;
	}


	public void setM_oEditBtn(Button m_oEditBtn) {
		this.m_oEditBtn = m_oEditBtn;
	}

//Del 2011/11/01 Z01_h_yamada Start -->
//	public Button getO_SearchButton() {
//		return o_SearchButton;
//	}
//	public void setO_oSearchButton(Button oButton) {
//		o_SearchButton = oButton;
//	}
//Del 2011/11/01 Z01_h_yamada End <--


	public static int getClickFlag() {
		return clickFlag;
	}
// Del 2011/09/19 r.itoh Start --> delete falg that is not used.
//	public static boolean isDoSearchFlag() {
//		return doSearchFlag;
//	}
//
//	public static void setDoSearchFlag(boolean doSearchFlag) {
//		KeyboardKanaView.doSearchFlag = doSearchFlag;
//	}
// Del 2011/09/19 r.itoh End <-- delete falg that is not used.


	/**
	 * コンストラクタ
	 * */
	public static void setClickFlag(int clickFlag) {
		KeyboardKanaView.clickFlag = clickFlag;
	}

	/**
	 * コンストラクタ
	 * */
	public KeyboardKanaView(Activity oContext, int wId) {
		super(oContext, wId, R.layout.keyboard_kana);
		m_oContext = oContext;
		init(oContext);

	}

//Del 2011/11/01 Z01_h_yamada Start -->
//	/**
//	 * コンストラクタ
//	 * */
//	public KeyboardKanaView(Activity oContext, int wId,Button obtn) {
//		super(oContext, wId, R.layout.keyboard_kana);
//		m_oContext = oContext;
//		if (obtn == null) {
//			o_SearchButton = (Button)super.findViewById(oContext, R.id.Button_search);
//		} else {
//			o_SearchButton = obtn;
//		}
//		init(oContext);
//
//	}
//
//	/**
//	 * コンストラクタ
//	 * */
//	public KeyboardKanaView(Activity oContext, int wId,Button oSearchbtn,Button oEditbtn) {
//		super(oContext, wId, R.layout.keyboard_kana);
//		m_oContext = oContext;
//		if (oSearchbtn == null) {
//			o_SearchButton = (Button)super.findViewById(oContext, R.id.Button_search);
//		} else {
//			o_SearchButton = oSearchbtn;
//		}
//		if (oEditbtn != null) {
//			m_oEditBtn = oEditbtn;
//		} else {
//			m_oEditBtn = (Button) findViewById(this, R.id.Button_edit);
//		}
//		init(oContext);
//
//	}
//
//	/**
//	 * コンストラクタ
//	 * */
//	public KeyboardKanaView(final Activity oContext,Button obtn) {
//		super(oContext, 0, R.layout.keyboard_kana);
//		m_oContext = oContext;
//		if (obtn == null) {
//			o_SearchButton = (Button)super.findViewById(oContext, R.id.Button_search);
//		} else {
//			o_SearchButton = obtn;
//		}
//
//		init(oContext);
//	}
//Del 2011/11/01 Z01_h_yamada End <--

	/**
	 * 最小の限定
	 * */
	public int getMinLimited() {
		return minLimited;
	}
	/**
	 * 最大の限定
	 * */
	public static int getMaxLimited() {
		return maxLimited;
	}

//Chg 2011/11/01 Z01_h_yamada Start -->
//	/**
//	 * 最小の限定
//	 * */
//	public void setMinLimited(int min,Button btn) {
//		this.minLimited = min;
//		o_SearchButton = btn;
//	}
//
//	/**
//	 * 最大の限定
//	 * */
//	public void setMaxLimited(int max,Button btn) {
//		KeyboardKanaView.maxLimited = max;
//		o_SearchButton = btn;
//	}
//--------------------------------------------
	/**
	 * 最小の限定
	 * */
	public void setMinLimited(int min) {
		this.minLimited = min;
	}

	/**
	 * 最大の限定
	 * */
	public void setMaxLimited(int max) {
		KeyboardKanaView.maxLimited = max;
	}
//Chg 2011/11/01 Z01_h_yamada End <--


//	/**
//	 *
//	 * @param str
//	 * */
//	public static boolean isChangeTitle(String str) {
//		if (str.equals(m_oContext.getResources().getString(R.string.text_key_input))) {
//			return false;
//		} else {
//			return true;
//		}
//	}

	/**
	 * 毎回、キーボードの「Del」は2回実行したが、フラグで2回目の操作を取得する
	 * */
	private int delFlag = 0;
	private int oTextLen = 0;
	private String beforeStr = "";
	private void init(final Context oContext) {

//		getCountThread = new Thread(this);
		oText = (EditText)this.findViewById(oContext, R.id.txtTitle_freeword);
		oCountView = (TextView) this.findViewById(oContext, R.id.text_record_count);
//Add 2011/11/01 Z01_h_yamada Start -->
		o_SearchButton = (Button) this.findViewById(oContext, R.id.Button_search);
		o_SearchButton.setEnabled(false);
//Add 2011/11/01 Z01_h_yamada End <--
		KEYBOARD_RIGHT = m_oContext.getResources().getStringArray(R.array.keyboard_right);
		//yangyang add start Bug1044
		oText.setOnKeyListener(new OnKeyListener (){

			@Override
			public boolean onKey(View arg0, int KeyId, KeyEvent event) {

				if (KeyId == KeyEvent.KEYCODE_DPAD_RIGHT) {
					if (oText.getSelectionEnd() >= oText.getText().length()) {
						return true;
					}

				}
				if (KeyId == KeyEvent.KEYCODE_DPAD_CENTER) {
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//					hiddenSoftInputMethod(m_oContext);
//Del 2011/09/08 Z01_h_yamada End <--
//Add 2011/09/21 Z01_h_yamada Start -->
					AppInfo.hiddenSoftInputMethod(m_oContext);
//Add 2011/09/21 Z01_h_yamada End <--
					return true;
				}
				if (KeyId == KeyEvent.KEYCODE_DPAD_RIGHT || KeyId == KeyEvent.KEYCODE_DPAD_LEFT) {
					delLineForText(oText);

				} else if (KeyId == KeyEvent.KEYCODE_DEL) {
					//yangyang add start Bug1076
					delFlag++;

					if (delFlag %2 == 1) {
						oTextLen = oText.getText().toString().length();
					}
					boolean bRightBtnFlag = true;
					if (AppInfo.isEmpty(oText.getText().toString().replaceAll(KEYBOARD_RIGHT[clickFlag % KEYBOARD_RIGHT.length], ""))) {
						bRightBtnFlag = false;
					}
					if (delFlag % 2 == 0
							&& (AppInfo.isEmpty(oText.getText().toString()) || !bRightBtnFlag)) {
						oCountView.setText(RECORD_ZERO_COUNT);
						TwoBtnAndScrollView.initBtnEnable(oText.getSelectionEnd(), oText);
					} else if (delFlag % 2 == 1 && oText.getText().toString().length() == 1) {
					} else if (oText.getSelectionEnd() == 0 && oTextLen == oText.getText().toString().length() && delFlag % 2 == 0) {
						doDelTitle();
					} else {
						setFlag_AddAndDel(false);
// Del 2011/09/19 r.itoh Start --> delete falg that is not used.
//                      doSearchFlag = false;
// Del 2011/09/19 r.itoh End <-- delete falg that is not used.
						//件数を取得する
						getListCount(oText.getText().toString());
					}
					//yangyang add end Bug1076
					return false;
				}

				return false;
			}


			});

//Add 2011/09/21 Z01_h_yamada Start -->
		oText.setOnTouchListener(new OnTouchListener() {
		public boolean onTouch(View v, android.view.MotionEvent event) {
			AppInfo.hiddenSoftInputMethod(m_oContext);
			return true;
		}
	});
//Add 2011/09/21 Z01_h_yamada End <--

//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//		oText.setOnTouchListener(new OnTouchListener() {
//			public boolean onTouch(View v, android.view.MotionEvent event) {
//				hiddenSoftInputMethod(m_oContext);
//				return true;
//			}
//		});
////Del 2011/09/08 Z01_h_yamada End <--
		//yangyang add end Bug1044

		oText.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable oEdit) {

				//yangyang add start Bug1044
				if (m_oEditBtn != null) {
					if (oEdit.length() > 0) {
						m_oEditBtn.setEnabled(true);
					} else {
						m_oEditBtn.setEnabled(false);
					}
				}
				//yangyang add end Bug1044
				checkMinLimited(oEdit.toString().length());
				if (getMaxLimited() != 0) {
					if (oEdit.toString().length() <= getMaxLimited() && oEdit.toString().length() >= getMinLimited()) {
						o_SearchButton.setEnabled(true);
					} else {
						o_SearchButton.setEnabled(false);
					}
				}

				if (keyType.equals(KEY)) {
					//yangyang add start Bug1044
					boolean bRightBtnFlag = true;
					if (AppInfo.isEmpty(oText.getText().toString().replaceAll(KEYBOARD_RIGHT[clickFlag % KEYBOARD_RIGHT.length], ""))) {
						bRightBtnFlag = false;
						o_SearchButton.setEnabled(false);
					}

                    String strKana = AppInfo.getKanaList(oEdit.toString());
                    if (strKana.length() != oEdit.toString().length()) {
                        setFlag_AddAndDel(false);

                        int len = oText.getSelectionEnd();
                        oText.setText(strKana);
                        if (len - 1 != 0) {
                            oText.setSelection(len - 1);
                        }

                    }

                    if (AppInfo.isEmpty(strKana) || !bRightBtnFlag) {
                        getListCount("");
                        oCountView.setText(RECORD_ZERO_COUNT);
                        TwoBtnAndScrollView.initBtnEnable(oText.getSelectionEnd(), oText);
                    } else {
                        setFlag_AddAndDel(false);
                        //件数を取得する
                        getListCount(oEdit.toString());
                    }

				} else if (keyType.equals(FAVORITES)) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"textChangeFlag==" + textChangeFlag);
					if (textChangeFlag) {
						textChangeFlag = true;

						o_SearchButton.setEnabled(true);
						checkMinLimited(oEdit.toString().length());
					} else {
						o_SearchButton.setEnabled(false);
					}
				}

			}

            private void checkMinLimited(int recordLen) {
				if (recordLen >= getMinLimited()) {
					o_SearchButton.setEnabled(true);
				} else {
					if (o_SearchButton.isEnabled()) {
						oText.setHint("");
					}
					o_SearchButton.setEnabled(false);
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				//お気に入り登録名称編集画面で未編集時に[登録]ボタンがグレーダウンしない

				if (keyType.equals(FAVORITES)) {
					beforeStr = s.toString();
				}

			}


			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
//				setFlag_AddAndDel(false);
				if (count > 1 ||!s.toString().equals(beforeStr)) {
					textChangeFlag = true;
				}
			}});


        oText.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                    KeyEvent event) {
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"onEditorAction======================" + event.getKeyCode());
                if (!doEnterFlag && keyType.equals(KEY)) {

                    getListCount(getText());
                }
                return true;
            }
        });

		btn_a = (Button) findViewById(oContext,R.id.btn_freeword_a);
		btn_a.setOnClickListener(OnClickLisFreeword);
		btn_ka = (Button) findViewById(oContext,R.id.btn_freeword_ka);
		btn_ka.setOnClickListener(OnClickLisFreeword);
		btn_sa = (Button) findViewById(oContext,R.id.btn_freeword_sa);
		btn_sa.setOnClickListener(OnClickLisFreeword);
		btn_ta = (Button) findViewById(oContext,R.id.btn_freeword_ta);
		btn_ta.setOnClickListener(OnClickLisFreeword);
		btn_na = (Button) findViewById(oContext,R.id.btn_freeword_na);
		btn_na.setOnClickListener(OnClickLisFreeword);
		btn_ha = (Button) findViewById(oContext,R.id.btn_freeword_ha);
		btn_ha.setOnClickListener(OnClickLisFreeword);
		btn_ma = (Button) findViewById(oContext,R.id.btn_freeword_ma);
		btn_ma.setOnClickListener(OnClickLisFreeword);
		btn_ya = (Button) findViewById(oContext,R.id.btn_freeword_ya);
		btn_ya.setOnClickListener(OnClickLisFreeword);
		btn_ra = (Button) findViewById(oContext,R.id.btn_freeword_ra);
		btn_ra.setOnClickListener(OnClickLisFreeword);
		btn_wa = (Button) findViewById(oContext,R.id.btn_freeword_wa);
		btn_wa.setOnClickListener(OnClickLisFreeword);
		btn_left = (Button) findViewById(oContext, R.id.btn_freeword_left);
		btn_left.setOnClickListener(OnClickLisFreeword);
		btn_left.setEnabled(false);
		btn_right = (Button) findViewById(oContext, R.id.btn_freeword_right);
		btn_right.setOnClickListener(OnClickLisFreeword);
	}
	private static String[] KEYBOARD_A = null;
	private static String[] KEYBOARD_KA = null;
	private static String[] KEYBOARD_SA = null;
	private static String[] KEYBOARD_TA = null;
	private static String[] KEYBOARD_NA = null;
	private static String[] KEYBOARD_HA = null;
	private static String[] KEYBOARD_MA = null;
	private static String[] KEYBOARD_YA = null;
	private static String[] KEYBOARD_RA = null;
	private static String[] KEYBOARD_WA = null;

	private static String[] KEYBOARD_LEFT_01 = null;
	private static String[] KEYBOARD_LEFT_02 = null;
	private static String[] KEYBOARD_LEFT_03 = null;
	private static String[] KEYBOARD_RIGHT = null;

	private OnClickListener OnClickLisFreeword = new OnClickListener(){

		@Override
		public void onClick(View v) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 かな入力 入力禁止 Start -->
			// 走行規制中の場合、ボタン操作を許可しない
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
//ADD 2013.08.08 M.Honma 走行規制 かな入力 入力禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
			int wId = v.getId();
// Del 2011/09/19 r.itoh Start --> delete falg that is not used.
//			doSearchFlag = false;
// Del 2011/09/19 r.itoh End <-- delete falg that is not used.
// Del 2011/10/12 Z01kkubo Start --> 課題No.219 文字入力で長音を2つ以上連続投入不可とする対応
			//rightClickFlag = false;
// Del 2011/10/12 Z01kkubo End <--
			switch(wId) {
			case R.id.btn_freeword_a:

				if (null == KEYBOARD_A) {
					KEYBOARD_A = m_oContext.getResources().getStringArray(R.array.keyboard_a);
				}
				checkClickFlag(KEYBOARD_A);
				reSetChangeValue(KEYBOARD_A);
				leftClickFlag=-1;
	            mButtonId = wId;
				break;
			case R.id.btn_freeword_ka:
				if (null == KEYBOARD_KA) {
					KEYBOARD_KA = m_oContext.getResources().getStringArray(R.array.keyboard_ka);
				}
				checkClickFlag(KEYBOARD_KA);
				reSetChangeValue(KEYBOARD_KA);
				leftClickFlag=-1;
                mButtonId = wId;
				break;
			case R.id.btn_freeword_sa:
				if (null == KEYBOARD_SA) {
					KEYBOARD_SA = m_oContext.getResources().getStringArray(R.array.keyboard_sa);
				}
				checkClickFlag(KEYBOARD_SA);
				reSetChangeValue(KEYBOARD_SA);
				leftClickFlag=-1;
                mButtonId = wId;
				break;
			case R.id.btn_freeword_ta:
				if (null == KEYBOARD_TA) {
					KEYBOARD_TA = m_oContext.getResources().getStringArray(R.array.keyboard_ta);
				}
				checkClickFlag(KEYBOARD_TA);
				reSetChangeValue(KEYBOARD_TA);
				leftClickFlag=-1;
                mButtonId = wId;
				break;
			case R.id.btn_freeword_na:
				if (null == KEYBOARD_NA) {
					KEYBOARD_NA = m_oContext.getResources().getStringArray(R.array.keyboard_na);
				}
				checkClickFlag(KEYBOARD_NA);
				reSetChangeValue(KEYBOARD_NA);
				leftClickFlag=-1;
                mButtonId = wId;
				break;
			case R.id.btn_freeword_ha:
				if (null == KEYBOARD_HA) {
					KEYBOARD_HA = m_oContext.getResources().getStringArray(R.array.keyboard_ha);
				}
				checkClickFlag(KEYBOARD_HA);
				reSetChangeValue(KEYBOARD_HA);
				leftClickFlag=-1;
                mButtonId = wId;
				break;
			case R.id.btn_freeword_ma:
				if (null == KEYBOARD_MA) {
					KEYBOARD_MA = m_oContext.getResources().getStringArray(R.array.keyboard_ma);
				}
				checkClickFlag(KEYBOARD_MA);
				reSetChangeValue(KEYBOARD_MA);
				leftClickFlag=-1;
                mButtonId = wId;
				break;
			case R.id.btn_freeword_ya:
				if (null == KEYBOARD_YA) {
					KEYBOARD_YA = m_oContext.getResources().getStringArray(R.array.keyboard_ya);
				}
				checkClickFlag(KEYBOARD_YA);
				reSetChangeValue(KEYBOARD_YA);
				leftClickFlag=-1;
                mButtonId = wId;
				break;
			case R.id.btn_freeword_ra:
				if (null == KEYBOARD_RA) {
					KEYBOARD_RA = m_oContext.getResources().getStringArray(R.array.keyboard_ra);
				}
				checkClickFlag(KEYBOARD_RA);
				reSetChangeValue(KEYBOARD_RA);
				leftClickFlag=-1;
                mButtonId = wId;
				break;
			case R.id.btn_freeword_wa:
				if (null == KEYBOARD_WA) {
					KEYBOARD_WA = m_oContext.getResources().getStringArray(R.array.keyboard_wa);
				}
				checkClickFlag(KEYBOARD_WA);
				reSetChangeValue(KEYBOARD_WA);
				leftClickFlag=-1;
                mButtonId = wId;
				break;
			case R.id.btn_freeword_left:
				//yangyang add start Bug1044
				oldchangeValue = changeValue;
				//yangyang add end Bug1044
				if (leftClickFlag == -1) {
					leftClickFlag++;
				}
				//yangyang mod start Bug1044
				if (oText.getText().toString().length() == 0 || !Html.toHtml(oText.getText()).contains("<u>")) {
					//yangyang mod end Bug1044
					clickFlag = 0;
					changeValue = "";
					leftClickFlag = -1;
					break;
				} else {
					String oValue = oText.getText().toString();
					oValue = oValue.substring(oText.getSelectionEnd()-1,oText.getSelectionEnd());
					if (InquiryBaseLoading.check(oValue, m_oContext.getResources().getStringArray(R.array.keyboard_ka)) ||
							InquiryBaseLoading.check(oValue, m_oContext.getResources().getStringArray(R.array.keyboard_left_ga))) {
						KEYBOARD_LEFT_01 = m_oContext.getResources().getStringArray(R.array.keyboard_left_ga);
						KEYBOARD_LEFT_02 = m_oContext.getResources().getStringArray(R.array.keyboard_ka);
						KEYBOARD_LEFT_03 = null;
					} else if (InquiryBaseLoading.check(oValue, m_oContext.getResources().getStringArray(R.array.keyboard_sa)) ||
							InquiryBaseLoading.check(oValue, m_oContext.getResources().getStringArray(R.array.keyboard_left_za))) {
						KEYBOARD_LEFT_01 = m_oContext.getResources().getStringArray(R.array.keyboard_left_za);
						KEYBOARD_LEFT_02 = m_oContext.getResources().getStringArray(R.array.keyboard_sa);
						KEYBOARD_LEFT_03 = null;
					} else if (InquiryBaseLoading.check(oValue, m_oContext.getResources().getStringArray(R.array.keyboard_ta)) ||
							InquiryBaseLoading.check(oValue, m_oContext.getResources().getStringArray(R.array.keyboard_left_da))) {
						KEYBOARD_LEFT_01 = m_oContext.getResources().getStringArray(R.array.keyboard_left_da);
						KEYBOARD_LEFT_02 = m_oContext.getResources().getStringArray(R.array.keyboard_ta);
						KEYBOARD_LEFT_03 = null;
					} else if (InquiryBaseLoading.check(oValue, m_oContext.getResources().getStringArray(R.array.keyboard_ha)) ||
							InquiryBaseLoading.check(oValue, m_oContext.getResources().getStringArray(R.array.keyboard_left_ba)) ||
							InquiryBaseLoading.check(oValue, m_oContext.getResources().getStringArray(R.array.keyboard_left_pa))) {
						KEYBOARD_LEFT_01 = m_oContext.getResources().getStringArray(R.array.keyboard_left_ba);
						KEYBOARD_LEFT_02 = m_oContext.getResources().getStringArray(R.array.keyboard_left_pa);
						KEYBOARD_LEFT_03 = m_oContext.getResources().getStringArray(R.array.keyboard_ha);
					} else {
						changeValue = "";
						KEYBOARD_LEFT_01 = null;
						KEYBOARD_LEFT_02 = null;
						KEYBOARD_LEFT_02 = null;
						break;
					}
				}

				if (Html.toHtml(oText.getText()).contains("<u>") && (KEYBOARD_LEFT_01 != null || KEYBOARD_LEFT_02 != null || KEYBOARD_LEFT_03 != null)) {

					if (leftClickFlag == 0) {
						changeValue = KEYBOARD_LEFT_01[clickFlag % KEYBOARD_LEFT_01.length];
					} else if (leftClickFlag == 1){
						changeValue = KEYBOARD_LEFT_02[clickFlag % KEYBOARD_LEFT_02.length];
					} else if (leftClickFlag == 2) {
						changeValue = KEYBOARD_LEFT_03[clickFlag % KEYBOARD_LEFT_03.length];
					}

					leftClickFlag++;
					if (KEYBOARD_LEFT_03 == null || KEYBOARD_LEFT_03.length == 0) {
						leftClickFlag = leftClickFlag % 2;
					} else {
						leftClickFlag = leftClickFlag % 3;
					}
				} else {
//					//yangyang mod start Bug1044
//					clickFlag = 0;
					changeValue = "";
//					leftClickFlag = -1;
					//yangyang mod end Bug1044
				}

				break;
			case R.id.btn_freeword_right:
				if (null == KEYBOARD_RIGHT) {
					KEYBOARD_RIGHT = m_oContext.getResources().getStringArray(R.array.keyboard_right);
				}
				checkClickFlag(KEYBOARD_RIGHT);
				reSetChangeValue(KEYBOARD_RIGHT);
				leftClickFlag=-1;
// Del 2011/10/12 Z01kkubo Start --> 課題No.219 文字入力で長音を2つ以上連続投入不可とする対応
				//rightClickFlag = true;
// Del 2011/10/12 Z01kkubo End <--
                mButtonId = wId;
				break;
			default:
				doChangeTitle(changeValue);
				leftClickFlag=-1;
				break;
			}

			//タイトル部分の内容をリセットする
			if (!AppInfo.isEmpty(changeValue)) {

				doChangeTitle(changeValue);

			} else {
				//yangyang add start Bug1044
				if (leftClickFlag != -1) {
					changeValue = oldchangeValue;
					leftClickFlag=-1;
				}
				//yangyang add end Bug1044
			}
		}

		private void reSetChangeValue(String[] keyboard) {
			if (oText.getText().toString().length() < getMaxLimited()) {
				changeValue = keyboard[clickFlag % keyboard.length];
			} else if (oText.getText().toString().length() == getMaxLimited()) {
				if (!AppInfo.isEmpty(changeValue) && InquiryBaseLoading.check(changeValue, keyboard)) {
					changeValue = keyboard[clickFlag % keyboard.length];
				} else {
					changeValue = "";
					delLineForText(oText);
				}
			}

		}

		private void checkClickFlag(String[] checkList) {
//			if (AppInfo.isEmpty(changeValue) || !InquiryBaseLoading.check(changeValue, checkList)) {
			if (AppInfo.isEmpty(changeValue) || !Html.toHtml(oText.getText()).contains("<u>") ||
					!InquiryBaseLoading.check(changeValue, checkList)) {
				clickFlag = 0;

			} else {
				clickFlag++;
//				clickFlag = clickFlag % checkList.length;
			}

		}

	};

	//yangyang add start Bug1076
	/**
	 * 修正処理
	 *
	 * */
	public void doDelTitle() {
		StringBuffer sb  = new StringBuffer();
		setFlag_AddAndDel(true);
// Del 2011/09/19 r.itoh Start --> delete falg that is not used.
////		if (KeyboardKanaView.isDoSearchFlag()) {
//		KeyboardKanaView.setDoSearchFlag(false);
////	}
// Del 2011/09/19 r.itoh End <-- delete falg that is not used.

		int index = oText.getSelectionEnd();
		sb.replace(0, sb.length(), oText.getText().toString());
		if (index == 0) {
			sb.deleteCharAt(index);
		} else {
			sb.deleteCharAt(index-1);
		}
		String oValue = sb.toString();
		oText.setText(oValue);
//		delLineForText(oText);
		if (index == 0) {
			oText.setSelection(index);
		} else {
			oText.setSelection(index - 1);
		}

		if (AppInfo.isEmpty(getText())) {
			oText.setHint("");
		}
		//カーソルの初期化
		TwoBtnAndScrollView.initBtnEnable(oText.getSelectionEnd(), oText);

	}
	//yangyang add end Bug1076
	/**
	 * タイトル部分の内容をリセットする
	 * @param string リセットの内容
	 *
	 * */
	public static void doChangeTitle(String string) {

		StringBuffer sb  = new StringBuffer();
		int index = 0;
//		if (!isChangeTitle()) {
//
//			oTitle.setText( string );
//			sb.append(oTitle.getText().toString());
//		} else {

		index = oText.getSelectionEnd();
		sb.replace(0, sb.length(), oText.getText().toString());
		if (sb.length() < getMaxLimited()) {
			sb.insert(index, string);
		} else  if (Html.toHtml(oText.getText()).contains("<u>") && (clickFlag != 0 && index != 0) || leftClickFlag >= 0) {
			sb.insert(index, string);
		} else {
			return;
		}
		if ((clickFlag != 0 && index != 0) || leftClickFlag >= 0) {
			sb.deleteCharAt(index-1);
		}
		String oValue = sb.toString();
		int start = 0;
		int end = 0;
// Chg 2011/10/12 Z01kkubo Start --> 課題No.219 文字入力で長音を2つ以上連続投入不可とする対応
//		//「ー」以外の場合
//		if ( !rightClickFlag) {
//			//チェンジできる文字が下にラインを追加する
//			if (oValue.length() > 1 ) {
//				//各ボタンがはじめに押下した場合、濁点，半濁点ボタン以外の場合
//				if (clickFlag == 0 && leftClickFlag == -1) {
//					start = index;
//					end = index + 1;
////					oValue = oValue.substring(0,index) +
////					"<u><font color='#ffffff'>" + oValue.substring(index,index+1)  + "</font></u>"
////					 + oValue.substring(index+1,oValue.length());
//				} else {
//					start = index-1;
//					end = index ;
////					oValue = oValue.substring(0,index-1) +
////					"<u><font color='#ffffff'>" + oValue.substring(index-1,index)  + "</font></u>"
////					 + oValue.substring(index,oValue.length());
//				}
//			} else {
////				oValue = "<u><B style='color:white;background-color:#000000'>" + oValue + "</b></u>";
//				start = 0;
//				end = oValue.length();
//			}
//		}
//--------------------------------------------------------------------------------------------
		//チェンジできる文字が下にラインを追加する
		if (oValue.length() > 1 ) {
			//各ボタンがはじめに押下した場合、濁点，半濁点ボタン以外の場合
			if (clickFlag == 0 && leftClickFlag == -1) {
				start = index;
				end = index + 1;
			} else {
				start = index-1;
				end = index ;
			}
		} else {
			start = 0;
			end = oValue.length();
		}
// Chg 2011/10/12 Z01kkubo End <--

		oText.setText(Html.fromHtml(oValue));
//		String aaa = Html.toHtml(oText.getText());
		if (start ==0 && end == 0) {
			delLineForText(oText);
		} else {
			highlight(start,end,oValue);
		}
//		if ( !rightClickFlag) {
//			//チェンジできる文字が下にラインを追加する
//			if (oValue.length() > 1 ) {
//				if (clickFlag == 0 && leftClickFlag == -1) {
//					oText.getText().setSpan(new BackgroundColorSpan(0xFFFFFFFF), index,index+1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//				} else {
//					oText.getText().setSpan(new BackgroundColorSpan(0xFFFFFFFF), index-1,index, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//				}
//			} else {
//				oText.getText().setSpan(new BackgroundColorSpan(0xFFFFFFFF), 0,oText.getText().toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//			}
//		}


//		}
		//フォーカスの初期化
		if (sb.length() < getMaxLimited()) {
			if (clickFlag != 0 || leftClickFlag >= 0) {
				oText.setSelection(index);
			} else {
				oText.setSelection(index + 1);
			}
		} else if (sb.length() == getMaxLimited()) {
// Chg 2011/10/12 Z01kkubo Start --> 課題No.219 文字入力で長音を2つ以上連続投入不可とする対応
//			if ( !rightClickFlag) {
//				oText.setSelection(end);
//			} else {
//				oText.setSelection(oText.getSelectionEnd());
//			}
//-------------------------------------------------------------------------------------------
			// 最大文字分を入力した後は最も右にフォーカス置く
			oText.setSelection(end);
// Chg 2011/10/12 Z01kkubo End <--
		}
		//set isSearchEnable
//		setSearchEnable(oTitle.getText().toString());
		//カーソルの初期化
		TwoBtnAndScrollView.initBtnEnable(oText.getSelectionEnd(), oText);
	}


	public boolean isFlag_AddAndDel() {
		return flag_AddAndDel;
	}


	public void setFlag_AddAndDel(boolean flagAddAndDel) {
		flag_AddAndDel = flagAddAndDel;
	}

// Del 2011/09/19 r.itoh Start --> refactoring : delete falg that is not used.
//	public boolean isBtnEnable() {
//		return btnEnable;
//	}
//
//	public void setBtnEnable(boolean btnEnable) {
//		this.btnEnable = btnEnable;
//	}
// Del 2011/09/19 r.itoh End <-- refactoring : delete falg that is not used.

	public String getText(){
		return oText.getText().toString();
	}
	public EditText getTextObj(){
		return oText;
	}


	public void setType(String string) {
		keyType = string;

	}
	public int getRecordCount () {
// Add 2011/09/19 r.itoh Start --> bug_fix[No.70] response is improved, when "KANA" button is pushed
	    return (int)ListCount.getLcount();

//		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_SearchList(getText());
//		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetMatchCount(ListCount);
//		//次画面から戻る場合、入力状態は要らない
//		delLineForText(oText);
////		TwoBtnAndScrollView.initBtnEnable(oText.getSelectionEnd(), oText);
//		setListCount(ListCount);
//		return (int)ListCount.lcount;

// Add 2011/09/19 r.itoh End <-- bug_fix[No.70] response is improved, when "KANA" button is pushed
	}


// Add 2011/09/19 r.itoh Start --> bug_fix[No.70] response is improved, when "KANA" button is pushed

	// CALL_SKIP_TIME 時間の間に呼ばれた リスト件数取得の要求をひとつにまとめる
	// SearchEngineが検索中(Busy)の場合 CALL_RETRY_TIME 時間待った後再度検索を行う。
	//
    static class POIListCountGetHandler extends Handler {
        protected final int CALL_SKIP_TIME = 500;
        protected final int CALL_RETRY_TIME = 100;

        Handler mRunOnUIHandler = new Handler();
        String mSearchString = null;

        boolean postGetListCoount(String searchString) {
            removeMessages(0);
            mSearchString = searchString;

            return this.sendEmptyMessageDelayed(0, CALL_SKIP_TIME);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            long lRet = NaviRun.GetNaviRunObj().JNI_NE_POIFacility_Cancel();
            if (mSearchString.length() == 0) {
                return;
            }
            lRet = NaviRun.GetNaviRunObj().JNI_NE_POIFacility_SearchList(mSearchString);
            if( lRet == NaviEngine.NE_WRN_BUSY )
            {
                this.sendEmptyMessageDelayed(0, CALL_SKIP_TIME + CALL_RETRY_TIME);
            }
        }

    }
    POIListCountGetHandler mPoiListCountGetter = new POIListCountGetHandler();

    // POI件数表示更新
    public void UpdateUiPoiListNum() {
        NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetMatchCount(ListCount);
        setListCount(ListCount);
        updateKanaButton();
    }
// Add 2011/09/19 r.itoh End <-- bug_fix[No.70] response is improved, when "KANA" button is pushed


// Chg 2011/09/19 r.itoh Start --> bug_fix[No.70] response is improved, when "KANA" button is pushed
    private void getListCount(final String pStr) {

        doEnterFlag = true;
        o_SearchButton.setEnabled(false);
        mPoiListCountGetter.postGetListCoount( pStr );

    }

//    private void getListCount_old(final String pStr) {
//
//		doEnterFlag = true;
//		if (!doSearchFlag) {
//			o_SearchButton.setEnabled(false);
//// Del 2011/05/31 sawada Start -->
////			m_oContext.showDialog(Constants.DIALOG_WAIT);
//// Del 2011/05/31 sawada End   <--
//
//			android.os.Handler hander = new android.os.Handler();
//			hander.postDelayed(new Runnable() {
//
//				@Override
//				public void run() {
//
//						doSearchFlag = true;
//						NaviRun.GetNaviRunObj().JNI_NE_POIFacility_SearchList(pStr);
//						NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetMatchCount(ListCount);
//						setListCount(ListCount);
//						doSearchFlag = false;
//
//// Del 2011/05/31 sawada Start -->
////						if (m_oWarningDialog != null) {
////							m_oWarningDialog.cancel();
////						}
//// Del 2011/05/31 sawada End   <--
//
//				}},2000);
////			android.os.Handler handerBoard = new android.os.Handler();
////			handerBoard.postDelayed(new Runnable() {
////
////				@Override
////				public void run() {
////					InputMethodManager imm = (InputMethodManager) m_oContext.getSystemService(INPUT_METHOD_SERVICE);
////					if(m_oContext.getWindow().getCurrentFocus()!=null) {
//////						imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
////						imm.showSoftInput(m_oContext.getWindow().getCurrentFocus(), 0);
////					}
////
////				}
////
////			}, 3000);
//		}
//	}
// Chg 2011/09/19 r.itoh End <-- bug_fix[No.70] response is improved, when "KANA" button is pushed

	/**
	 * 件数を取得して、表示する
	 * @param pListCount 取得した件数の対象
	 * */
	public void setListCount(JNILong pListCount) {
		ListCount = pListCount;
		int recodeCount = (int)ListCount.lcount;
		oCountView.setText("" + recodeCount + "件");
		doEnterFlag = false;
		//3000件以上の場合、検索ボタンは使用不可とする
		if (recodeCount > 3000 || recodeCount == 0) {
			o_SearchButton.setEnabled(false);
		} else {
			o_SearchButton.setEnabled(true);
		}
		TwoBtnAndScrollView.initBtnEnable(oText.getSelectionEnd(), oText);
	}

	private Dialog m_oWarningDialog = null;

	public void putWaitDialog(Dialog oWarningDialog) {
		m_oWarningDialog = oWarningDialog;

	}

	/**
	 * タイトルの内容をリセットする（入力状態を削除する）
	 * @param oText
	 * */
	public static void delLineForText(EditText oText) {
		if (null != oText) {
			String editString = Html.toHtml(oText.getText());
			int selectionIndex = oText.getSelectionEnd();
			if (editString.contains("<u>")) {
				editString = editString.replace("<u><font color =\"#ffffff\">", "");
				editString = editString.replace("</font></u>", "");
// Del 2011/09/19 r.itoh Start --> delete falg that is not used.
//				//この場合は検索処理は要らないので、[doSearchFlag]がtrueに変更する
//				doSearchFlag = true;
// Del 2011/09/19 r.itoh End <-- delete falg that is not used.
				oText.setText(Html.fromHtml(editString).toString().trim());
				oText.setSelection(selectionIndex);
			}
		}

	}
//	/**
//	 * ソフトキーボードが不表示する
//	 *
//	 * */
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//	private void hiddenSoftInputMethod(final Activity m_oContext) {
//		android.os.Handler handerBoard = new android.os.Handler();
//		handerBoard.postDelayed(new Runnable() {
//
//			@Override
//			public void run() {
//
//				AppInfo.hiddenSoftInputMethod(m_oContext);
//
//			}
//		}, 500);
//	}
////Del 2011/09/08 Z01_h_yamada End <--

//Add 2011/11/01 Z01_h_yamada Start -->
	public void setSearchListener(OnClickListener oListener) {
		if (o_SearchButton != null) {
			o_SearchButton.setOnClickListener(oListener);
		}
	}


	public void setSearchBtnText(String str) {
		if (o_SearchButton != null) {
			o_SearchButton.setText(str);
		}
	}

//Add 2011/11/01 Z01_h_yamada End <--

	private static void highlight(int start,int end,String str){
        SpannableStringBuilder spannable=new SpannableStringBuilder(str);//変更した文字列
        CharacterStyle spanLine = new UnderlineSpan();
		spannable.setSpan(spanLine, start, end,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		ForegroundColorSpan span = new ForegroundColorSpan(Color.WHITE);
		spannable.setSpan(span, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		BackgroundColorSpan spanBgColor =  new BackgroundColorSpan(Color.BLACK);
		spannable.setSpan(spanBgColor, start, end,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        oText.setText(spannable);
//        String aaa = Html.toHtml(oText.getText());
    }

    public void updateKanaButton() {
        if (Html.toHtml(oText.getText()).contains("<u>") &&
                (mButtonId == R.id.btn_freeword_ka ||
                 mButtonId == R.id.btn_freeword_sa ||
                 mButtonId == R.id.btn_freeword_ta ||
                 mButtonId == R.id.btn_freeword_ha)) {
            btn_left.setEnabled(true);
        }
        else {
            btn_left.setEnabled(false);
        }

        if (ListCount.lcount > 1) {
            btn_a.setEnabled(true);
            btn_ka.setEnabled(true);
            btn_sa.setEnabled(true);
            btn_ta.setEnabled(true);
            btn_na.setEnabled(true);
            btn_ha.setEnabled(true);
            btn_ma.setEnabled(true);
            btn_ya.setEnabled(true);
            btn_ra.setEnabled(true);
            btn_wa.setEnabled(true);
            btn_right.setEnabled(true);
            return;
        }

        btn_a.setEnabled(false);
        btn_ka.setEnabled(false);
        btn_sa.setEnabled(false);
        btn_ta.setEnabled(false);
        btn_na.setEnabled(false);
        btn_ha.setEnabled(false);
        btn_ma.setEnabled(false);
        btn_ya.setEnabled(false);
        btn_ra.setEnabled(false);
        btn_wa.setEnabled(false);
        btn_right.setEnabled(false);

        if (!Html.toHtml(oText.getText()).contains("<u>")) {
            return;
        }

        switch(mButtonId) {
        case R.id.btn_freeword_a:
            btn_a.setEnabled(true);
            break;
        case R.id.btn_freeword_ka:
            btn_ka.setEnabled(true);
            break;
        case R.id.btn_freeword_sa:
            btn_sa.setEnabled(true);
            break;
        case R.id.btn_freeword_ta:
            btn_ta.setEnabled(true);
            break;
        case R.id.btn_freeword_na:
            btn_na.setEnabled(true);
            break;
        case R.id.btn_freeword_ha:
            btn_ha.setEnabled(true);
            break;
        case R.id.btn_freeword_ma:
            btn_ma.setEnabled(true);
            break;
        case R.id.btn_freeword_ya:
            btn_ya.setEnabled(true);
            break;
        case R.id.btn_freeword_ra:
            btn_ra.setEnabled(true);
            break;
        case R.id.btn_freeword_wa:
            btn_wa.setEnabled(true);
            break;
        case R.id.btn_freeword_right:
            btn_right.setEnabled(true);
            break;
        }
    }
}

