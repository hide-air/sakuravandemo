package net.zmap.android.pnd.v2.broadcast;

import java.util.ArrayList;
import java.util.Calendar;

import net.zmap.android.pnd.v2.api.NaviIntent;
import net.zmap.android.pnd.v2.api.event.RouteCalculated;
import net.zmap.android.pnd.v2.api.navigation.RemainingDistance;
import net.zmap.android.pnd.v2.api.navigation.RouteRequest;
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応 START
import net.zmap.android.pnd.v2.api.event.VehiclePositionEx;
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応  END
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.utils.GeoUtils;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNITwoInt;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.ZCT_AreaName;
import net.zmap.android.pnd.v2.data.ZNE_VehicleInfo_t;
import net.zmap.android.pnd.v2.data.ZNUI_DESTINATION_DATA;
import net.zmap.android.pnd.v2.data.ZNUI_TIME;
import net.zmap.android.pnd.v2.engine.NaviEngineUtils;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
// ADD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応① START
import android.location.LocationManager;
// ADD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応①  END
import android.os.Bundle;

public class NaviApiBroadcastSender {

    Context mBroadCaster = null;
    RouteGuideBroadcaster mRouteGuideBroadcast = null; //ルート案内ブロードキャスト
// ADD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応① START
    Location oldLocation = new Location( LocationManager.GPS_PROVIDER );
    Location oldLocationEx = new Location( LocationManager.GPS_PROVIDER );
// ADD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応①  END

    public NaviApiBroadcastSender(Context broadcaster) {
        mBroadCaster = broadcaster;
    }

    /**
     * broadcastRouteCalculated
     * ブロードキャスト送信
     */
    public void broadcastRouteCalculated() {

        RouteCalculated routeCalculated = new RouteCalculated();

        RouteRequest routeRequest = NaviEngineUtils.getCurrentRouteRequest();
        routeCalculated.setRouteRequest(routeRequest);

        ZNUI_DESTINATION_DATA destinationData = new ZNUI_DESTINATION_DATA();
        int naviMode = CommonLib.getNaviMode();
        if (naviMode == Constants.NE_NAVIMODE_CAR) {
            NaviRun.GetNaviRunObj().JNI_Java_GetDestination(destinationData);
        } else if (naviMode == Constants.NE_NAVIMODE_MAN) {
            NaviRun.GetNaviRunObj().JNI_WG_GetDestination(destinationData);
        }
        ZNUI_TIME time = destinationData.getStTime();
        Calendar mCalendar = Calendar.getInstance();
        mCalendar.setTimeInMillis(System.currentTimeMillis());
        int systemTime = mCalendar.get(Calendar.HOUR_OF_DAY) * 60 + mCalendar.get(Calendar.MINUTE);
        int arrivalTime = time.getNHouer() * 60 + time.getNMinute();
        long remainingTime;
        if (arrivalTime >= systemTime) {
            remainingTime = (arrivalTime - systemTime) * 60 * 1000L;
        } else {
            remainingTime = (arrivalTime + 1440 - systemTime) * 60 * 1000L;
        }
        RemainingDistance remainingDistance = new RemainingDistance();
        remainingDistance.setDistance(destinationData.getLnDistance());
        remainingDistance.setTime(remainingTime);
        routeCalculated.setRemainingDistance(remainingDistance);

        Intent intent = new Intent();
        intent.setAction(NaviIntent.ACTION_ROUTE_CALCULATED);

        Bundle bundle = new Bundle();
        bundle.putParcelable(NaviIntent.EXTRA_ROUTE_CALCULATED, routeCalculated);

        intent.putExtras(bundle);
        mBroadCaster.sendBroadcast(intent);
    }

    /**
     * マップマッチング情報 (座標・速度・方位) ブロードキャスト
     */
    public void broadcastVehiclePosition() {

        Location location = NaviEngineUtils.getVehicleLocation();
// ADD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応① START
        if( (oldLocation.getLatitude() != location.getLatitude()) 	||
           	(oldLocation.getLongitude() != location.getLongitude()) ||
           	(oldLocation.getBearing() != location.getBearing()) 	||
       		(oldLocation.getSpeed() != location.getSpeed()) ){
        	oldLocation.set( location );
        }
        else{
        	// 前回の位置情報と変化ないため、送信しない。
        	// 通常車モードはここには入らず、歩行者 or アローモード時のみここに入る
        	return;
        }
// ADD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応①  END
        Intent intent = new Intent();
        intent.setAction(NaviIntent.ACTION_VEHICLE_POSITION);

        Bundle bundle = new Bundle();
        bundle.putParcelable(NaviIntent.EXTRA_LOCATION, location);

        intent.putExtras(bundle);
        mBroadCaster.sendBroadcast(intent);
    }

    /**
     * マップマッチング情報 (道路種別) ブロードキャスト
     */
    public void broadcastRoadMoved() {

        ZNE_VehicleInfo_t vehicleInfo = new ZNE_VehicleInfo_t();

        NaviRun.GetNaviRunObj().JNI_DG_getVehiclePosInfo(vehicleInfo);

        JNITwoLong lonLat = vehicleInfo.getstPos();
        JNITwoInt roadType = vehicleInfo.getRoadType();

        Location location = GeoUtils.toWGS84Location(lonLat.getM_lLat(), lonLat.getM_lLong());
        location.setTime(System.currentTimeMillis());

        Intent intent = new Intent();
        intent.setAction(NaviIntent.ACTION_ROAD_MOVED);

        Bundle bundle = new Bundle();
        bundle.putParcelable(NaviIntent.EXTRA_LOCATION, location);
        bundle.putInt(NaviIntent.EXTRA_ROAD_FROM, roadType.getM_iSizeX());
        bundle.putInt(NaviIntent.EXTRA_ROAD_TO, roadType.getM_iSizeY());

        intent.putExtras(bundle);
        mBroadCaster.sendBroadcast(intent);
    }

    private ZCT_AreaName mAreaName = new ZCT_AreaName();

    /**
     * 行政界変更ブロードキャスト
     */
    public void broadcastAreaMoved() {
        final String EMPTY_NAME = "";

        JNITwoLong coordinate = new JNITwoLong();
        JNIString addressString = new JNIString();

        // 現在地を取得する
        NaviRun.GetNaviRunObj().JNI_CT_Drv_GetVehiclePosition(coordinate);
        Location location = GeoUtils.toWGS84Location(coordinate.getM_lLat(), coordinate.getM_lLong());
        location.setTime(System.currentTimeMillis());

        // 旧名称と新名称を取得する
        String preAreaName = mAreaName.getPreAreaName();
        if (preAreaName == null) {
            preAreaName = EMPTY_NAME;
        }
        String curAreaName = mAreaName.getCurAreaName();
        if (curAreaName == null) {
            curAreaName = EMPTY_NAME;
        }

        // 現在地の名称を取得する
        NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(coordinate.getM_lLong(), coordinate.getM_lLat(), addressString);
        String vpAreaName = addressString.getM_StrAddressString();
        if (vpAreaName == null) {
            vpAreaName = EMPTY_NAME;
        }

        if (vpAreaName.equals(curAreaName)) {
            // 前回の名称が同じ → 通知の必要なし
            return;
        } else {
            // 現在地の名称が変わった → 通知の必要あり
            mAreaName.setPreAreaName(curAreaName);
            mAreaName.setCurAreaName(vpAreaName);

            ArrayList<String> listPreAreaName = new ArrayList<String>();
            String[] preSplittedNames = preAreaName.split(" ");
            for (int i = 0; i < preSplittedNames.length; i++) {
                if (preSplittedNames[i] != null) {
                    listPreAreaName.add(preSplittedNames[i]);
                }
            }

            ArrayList<String> listCurAreaName = new ArrayList<String>();
            String[] curSplittedNames = curAreaName.split(" ");
            for (int i = 0; i < curSplittedNames.length; i++) {
                if (curSplittedNames[i] != null) {
                    listCurAreaName.add(curSplittedNames[i]);
                }
            }

            Intent intent = new Intent();
            intent.setAction(NaviIntent.ACTION_AREA_MOVED);

            Bundle bundle = new Bundle();
            bundle.putParcelable(NaviIntent.EXTRA_LOCATION, location);
            bundle.putStringArrayList(NaviIntent.EXTRA_ADMIN_FROM, listPreAreaName);
            bundle.putStringArrayList(NaviIntent.EXTRA_ADMIN_TO, listCurAreaName);

            intent.putExtras(bundle);
            mBroadCaster.sendBroadcast(intent);
        }
    }

    /**
     * 案内中ルート情報(目標案内、経由地、目的地) ブロードキャスト
     */
    public void broadcastRouteGuide() {
        if (mRouteGuideBroadcast == null) {
            mRouteGuideBroadcast = new RouteGuideBroadcaster(mBroadCaster);
        }
        if (mRouteGuideBroadcast == null) {
            return;
        }
        mRouteGuideBroadcast.intentRouteGuide();
    }

    /**
     * ルート案内(開始、終了) ブロードキャスト
     */
    public void broadcastNavigation(int eventType) {
        if (mRouteGuideBroadcast == null) {
            mRouteGuideBroadcast = new RouteGuideBroadcaster(mBroadCaster);
        }
        if (mRouteGuideBroadcast == null) {
            return;
        }
        mRouteGuideBroadcast.intentNavigation(eventType);
    }

// Add 2011/10/19 sawada Start -->
    /**
     * ナビ起動ブロードキャスト
     */
    public void broadcastNaviStarted() {
        Intent intent = new Intent();
        intent.setAction(NaviIntent.ACTION_NAVI_START);
        mBroadCaster.sendBroadcast(intent);
    }
// Add 2011/10/19 sawada End <--
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応 START
    /**
     * マップマッチング情報 (座標・速度・方位) + 高速走行中 ブロードキャスト
     */
    public void broadcastVehiclePositionEx() {

        Location location = NaviEngineUtils.getVehicleLocation();
// ADD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応① START
        if( (oldLocationEx.getLatitude() != location.getLatitude()) 	||
            (oldLocationEx.getLongitude() != location.getLongitude()) ||
            (oldLocationEx.getBearing() != location.getBearing()) 	||
           	(oldLocationEx.getSpeed() != location.getSpeed()) ){
        	oldLocationEx.set( location );
        }
        else{
        	// 前回の位置情報と変化ないため、送信しない。
        	// 通常車モードはここには入らず、歩行者 or アローモード時のみここに入る
        	return;
        }
// ADD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応①  END
        VehiclePositionEx vehiclePositionEx = new VehiclePositionEx(location, CommonLib.isBIsOnHighWay());

        Intent intent = new Intent();
        intent.setAction(NaviIntent.ACTION_VEHICLE_EX_POSITION);

        Bundle bundle = new Bundle();
        bundle.putParcelable(NaviIntent.EXTRA_LOCATION_EX, vehiclePositionEx);

        intent.putExtras(bundle);
        mBroadCaster.sendBroadcast(intent);

    }
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応  END
}
