package net.zmap.android.pnd.v2.maps;

import android.app.Activity;
import android.content.Intent;

import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.data.PoiBaseData;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.MapInfo;
import net.zmap.android.pnd.v2.data.NaviRun;

public class OpenMap {

    private static Activity oActivity;
    private static PoiBaseData oData;
    private static int poiType = AppInfo.POI_NONE;
    private static int request_id;
    private static String strFrom = Constants.FROM_SEARCH;
    private static Intent oIntent;

    /**
     * Open Map
     *
     * @param activity
     *            Activity
     * @param poi
     *            PoiBaseDatas, used for save map move destination info.
     * @param intent
     *            Additional data,if not necessary set null，Recommended use getIntent()
     */
    public static void moveMapTo(Activity activity, PoiBaseData poi, Intent intent, int poi_type, int searchType) {
        oActivity = activity;
        oData = poi;
        poiType = poi_type;
        oIntent = intent;

    	System.out.println("*** OpenMap Into moveMapTo ***");

    	if (intent != null) {
            String s_from = intent.getStringExtra(Constants.ROUTE_FLAG_KEY);
            if (s_from != null) {
                strFrom = s_from;
            }
            //1128 start
            else {
                strFrom = Constants.FROM_SEARCH;
            }
            //1128 end
        }

    	System.out.println("*** OpenMap moveMapTo strFrom => " + strFrom);

    	int activity_id = AppInfo.ID_ACTIVITY_OPEN_MAP;
//        if(Constants.FROM_ROUTE_SEARCH.equals(strFrom)){
//            activity_id = AppInfo.ID_ACTIVITY_ROUTE_MAP;
//        }
//        if(Constants.FROM_NAVI_SEARCH.equals(strFrom)){
//            activity_id = AppInfo.ID_ACTIVITY_NAVI_MAP;
//        }
        if (Constants.FROM_ROUTE_EDIT.equals(strFrom)) {
            activity_id = AppInfo.ID_ACTIVITY_ROUTE_MODIFY;
        }
        if (Constants.FROM_ROUTE_ADD.equals(strFrom)) {
           activity_id = AppInfo.ID_ACTIVITY_ROUTE_ADD;
        }
// Add by CPJsunagawa '2015-07-08 Start
    	if (Constants.FROM_REGIST_ICON.equals(strFrom)) {
        	System.out.println("*** OpenMap moveMapTo Constants.FROM_REGIST_ICON !!");
            activity_id = AppInfo.ID_ACTIVITY_REGIST_ICON;
        }
// Add by CPJsunagawa '2015-08-15 Start
    	if (Constants.FROM_MOVE_ICON.equals(strFrom)) {
        	System.out.println("*** OpenMap moveMapTo Constants.FROM_MOVE_ICON !!");
            activity_id = AppInfo.ID_ACTIVITY_MOVE_ICON;
        }
    	if (Constants.FROM_DELETE_ICON.equals(strFrom)) {
        	System.out.println("*** OpenMap moveMapTo Constants.FROM_DELETE_ICON !!");
            activity_id = AppInfo.ID_ACTIVITY_DELETE_ICON;
        }
    	if (Constants.FROM_DRAW_LINE.equals(strFrom)) {
        	System.out.println("*** OpenMap moveMapTo Constants.FROM_DRAW_LINE !!");
            activity_id = AppInfo.ID_ACTIVITY_DRAW_LINE;
        }
// Add by CPJsunagawa '2015-08-15 End
        if (Constants.FROM_INPUT_TEXT.equals(strFrom)) {
           activity_id = AppInfo.ID_ACTIVITY_INPUT_TEXT;
        }
// Add by CPJsunagawa '2015-07-08 End
        request_id = activity_id;

        JNITwoLong mapCentre = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(mapCentre);

        long Latitude = 0;
        long Longitude = 0;

        if (poi != null) {
            Latitude = poi.m_wLat;
            Longitude = poi.m_wLong;
        }

// Chg 2011/10/07 Z01kkubo Start --> 課題No.190 高速案内中の経由地追加「地図から探す」でマップ表示不正対応
        //if (Longitude == 0
        //        || Latitude == 0
        //        || (Longitude == mapCentre.getM_lLong() && Latitude == mapCentre.getM_lLat())) {
        //------------------------------------------------------------------------------------------------
        // 目的地検索画面から地点地図に遷移する場合は、常にelseルートを通すようにする
        // (自車位置と同位置であっても、常に検索地点を指した上で地図中心アイコンを表示するようにする)
        if ((Constants.LOCAL_INQUIRY_REQUEST  != searchType) &&
        		(Longitude == 0
                || Latitude == 0
                || (Longitude == mapCentre.getM_lLong() && Latitude == mapCentre.getM_lLat()))) {
// Chg 2011/10/07 Z01kkubo End <--
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"[DEBUG] : OpenMap  Longitude=" + Longitude + ", Latitude=" + Latitude + ", searchType=" + searchType + ", same Place!");

        	System.out.println("*** OpenMap moveMapTo Path(1)");

        	if (Constants.FAVORITE_ADD_REQUEST == searchType) {
            	System.out.println("*** OpenMap moveMapTo Path(2)");

                NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);
//Chg 2011/11/02 Z01_h_yamada Start --> ﾃﾞﾓ走行後のお気に入り登録で、登録後にクルクルが終了しない問題の解決(描画更新されないため)
//               NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();
//--------------------------------------------
                int ActiveType = 2;
                int TransType = 2;
                MapInfo map = new MapInfo();
                NaviRun.GetNaviRunObj().JNI_CT_GetReqMapPosition(map);
                NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(ActiveType, TransType, map.getAppLevel(),map.getScale(),map.getMapPoint().getLongitude(), map.getMapPoint().getLatitude(),map.getMapAngle());
//Chg 2011/11/02 Z01_h_yamada End <--

                NaviRun.GetNaviRunObj().setSearchKind(searchType);
            } else {
            	System.out.println("*** OpenMap moveMapTo Path(3)");

                openMap();

                NaviRun.GetNaviRunObj().setSearchKind(Constants.SCROLL_MAP_KIND);
            }
        } else {

        	System.out.println("*** OpenMap moveMapTo Path(4)");

        	MapActivity mapActivity = NaviRun.GetNaviRunObj().getMapActivity();
        	if (mapActivity != null) {
        		mapActivity.SavePosInfo();
        	}

            int lineFlag = 1;
            // Move Map
            if (Constants.FAVORITE_ADD_REQUEST == searchType) {
                lineFlag = 0;
            }
            NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(lineFlag, 0);

            int ActiveType = 2;
            int TransType = 2;

            JNIInt MapScale = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
            byte bScale = (byte)MapScale.getM_iCommon();

// Add by CPJsunagawa '13-12-25 Start
            if (bScale >= 1) bScale--;
// Add by CPJsunagawa '13-12-25 End

            MapInfo map = new MapInfo();
            NaviRun.GetNaviRunObj().JNI_CT_GetReqMapPosition(map);
            NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(ActiveType, TransType, bScale, 1.0, Longitude, Latitude,map.getMapAngle());

            NaviRun.GetNaviRunObj().setSearchKind(searchType);
        }
   }

// 2013/10/17 M.Suna Start --> Add 2012/4/16 CP furusawa Start -->
    /**
     * Open Map
     * moveMapTo(Activity, PoiBaseData, Intent, int, int)をベースに外部連携用に別途用意
     *
     * @param poi
     *            PoiBaseDatas, used for save map move destination info.
     */
    public static void moveMapTo(PoiBaseData poi, int poi_type, int searchType) {
        long Latitude = 0;
        long Longitude = 0;

    	System.out.println("*** Into moveMapTo(PoiBaseData poi, int poi_type, int searchType)");

        if (poi != null) {
            Latitude = poi.m_wLat;
            Longitude = poi.m_wLong;
        }

        int lineFlag = 1;
        // Move Map
        if (Constants.FAVORITE_ADD_REQUEST == searchType) {
            lineFlag = 0;
        }
        NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(lineFlag, 0);

        int ActiveType = 2;
        int TransType = 2;
        JNIInt MapScale = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
        byte bScale = (byte)MapScale.getM_iCommon();
        
        MapInfo map = new MapInfo();
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"[DEBUG] : OpenMap  Longitude=" + Longitude + ", Latitude=" + Latitude + ", searchType=" + searchType);
// MOD S 2013/09/18 y_matsumoto
//        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(ActiveType, TransType, bScale, Longitude, Latitude);
      NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(ActiveType, TransType, bScale, 1.0, Longitude, Latitude, map.getMapAngle());
// MOD E 2013/09/18 y_matsumoto
        NaviRun.GetNaviRunObj().setSearchKind(searchType);
   }
// 2013/10/17 M.Suna End --> Add 2012/4/16 CP furusawa End <--

    /**
     * open MapActivity
     */
    public static void openMap() {
    	System.out.println("*** Into openMap ***");

        if (oActivity != null) {
            Intent intent = new Intent();
            if (oIntent != null) {
                intent.putExtras(oIntent);
            }
            CommonLib.setIsOpenMap(true);
            intent.setClass(oActivity, MapActivity.class);
            intent.putExtra(AppInfo.POI_DATA, oData);
            intent.putExtra(AppInfo.FLAG_POI_TYPE, poiType);
            intent.putExtra(Constants.ROUTE_FLAG_KEY, strFrom);

//Chg 2011/09/23 Z01yoneya Start -->
//            oActivity.startActivityForResult(intent, request_id);
//------------------------------------------------------------------------------------------------------------------------------------
        	System.out.println("*** openMap => intent.putExtra(Constants.ROUTE_FLAG_KEY, strFrom)" + strFrom);
        	System.out.println("*** openMap startActivityForResult(oActivity, intent, request_id) request_id=> " + request_id);

            NaviActivityStarter.startActivityForResult(oActivity, intent, request_id);
//Chg 2011/09/23 Z01yoneya End <--
        }

        oIntent = null;
    }

    /**
     * back to car position.
     *
     * @param activity
     */
    public static void locusMap(Activity activity) {
        oActivity = activity;
        oIntent = activity.getIntent();

        if (oIntent == null) {
            oIntent = new Intent();
        }

        oIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        oIntent.putExtra(Constants.FLAG_BACK_CAR_POS, true);
//      oIntent.putExtra(Constants.ROUTE_FLAG_KEY, strFrom);

        if (CommonLib.isMapScrolled()) {
            locusMap(Constants.LOCATION_MAP_REQUEST);
        } else {
            locusMap();
        }
    }

    /**
     * back to car position.
     */
    public static void locusMap() {

        NaviRun.GetNaviRunObj().setSearchKind(Constants.SCROLL_MAP_KIND);

        oIntent.setClass(oActivity, MapActivity.class);
//Chg 2011/09/23 Z01yoneya Start -->
//        oActivity.startActivity(oIntent);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivity(oActivity, oIntent);
//Chg 2011/09/23 Z01yoneya End <--

        oIntent = null;
        oActivity = null;
    }

    private static void locusMap(int type) {
        NaviRun.GetNaviRunObj().setSearchKind(type);

        NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);

        NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
    }

    public static void locationBack(MapActivity activity) {
        oActivity = activity;

        locusMap(Constants.LOCATION_MAP_2_REQUEST);
    }

    public static void onLocationBack() {
        try {
            ((MapActivity)oActivity).showLocusMap();
        } catch (Exception e) {
        }
    }

}
