package net.zmap.android.pnd.v2.sakuracust;

//import java.io.UnsupportedEncodingException;

//import android.provider.Contacts.SettingsColumns;
import android.util.Log;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Address_ListItem;

/**
 * NDK内で直接soをコールするのは諦め、Java側で<br>
 * 住所検索を走らせる。このクラスのメソッドは、<br>
 * JNIの内部からコールされる。
 * @author Norimasa
 *
 */
public class GetAddressFromItsmo {
	private static final int POI_SUCCESS = 0x10091001;
	//private static boolean mNowAddressMatch; // アドレスマッチング中
	private static boolean mNowSearching; // 住所サーチ中
	//private static GoSearchNextList mGoSearchNextList = new GoSearchNextList(); 
	
	/** 全クリア */
	public static void resetAllAdr()
	{
		NaviRun.GetNaviRunObj().JNI_NE_POIAddrKey_Clear();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_Clear();
	}
	
	/** アドレスマッチングの開始（イベント横取り開始） */
	public static void startAddressMatch()
	{
		//mNowAddressMatch = true;
	}
	
	/** アドレスマッチングの終了（イベント横取り終了） */
	public static void endAddressMatch()
	{
		//mNowAddressMatch = false;
	}
	
	/**
	 * NaviRun.initEventHandlerのPOI_ADDRESS_GETJUMPKEYに於ける横取りを行う。
	 * @param succeededMsg msg.arg2の値
	 * @param searchKind iSearchKindの値を入れる。
	 * @return true 横取りした、false デフォルト処理を行う。
	 */
	public static boolean interceptEvent2(int succeededMsg, int searchKind)
	{
		// 結局POI_ADDRESS_GETJUMPKEYは、住所検索関連では何もやってない。
		return mNowSearching;
	}
	
	/**
	 * NaviRun.initEventHandlerのPOI_ADDRESS_SEARCHENDに於ける横取りを行う。
	 * @param succeededMsg msg.arg2の値
	 * @param searchKind iSearchKindの値を入れる。
	 * @return true 横取りした、false デフォルト処理を行う。
	 */
	public static boolean interceptEvent(int succeededMsg, int searchKind)
	{
		//if (!mNowAddressMatch) return false; // アドレスマッチング中でなければ勿論終了
		if (!mNowSearching) return false;
		mNowSearching = false;
		//if (succeededMsg != POI_SUCCESS) return false;
		
		//mNowSearching = false;
		return true;
	}
	
	/**
	 * 都道府県の検索を開始する。
	 * @param ForDeliveringToNDK C++へ引き渡すためのオブジェクト
	 */
	public static void startSearchPref(ForDeliveringToNDK arg)
	{
		resetAllAdr();
		startSearchNextAdr(DataCtrlFacade.CHIHO_LEV_AM, 0, arg);
	}
	
	/**
	 * 次の検索を開始する。
	 * @param prevHir 前のヒエラルキー
	 * @param setIdx 
	 * @param arg
	 */
	public static void startSearchNextAdr(int prevHir, int setIdx, ForDeliveringToNDK arg)
	{
		Log.d("startSearchNextAdr", "prevHir : " + prevHir + " - setIdx : " + setIdx);
		int trueSetIdx = (prevHir >= DataCtrlFacade.AREA_LEV_AM) ? setIdx - 1 : 0; // コード設定時、インデックスに1を足しているので、此処で1引く
		
		// 次の住所レベル
		JNILong Treeindex = new JNILong();
		long ndkRes = NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetNextTreeIndex(trueSetIdx, Treeindex);
		if (ndkRes >= 1)
		{
			if (prevHir <= DataCtrlFacade.AREA_LEV_AM)
			{
				// 上位が都道府県であれば一旦クリアしてリトライする。
				resetAllAdr();
				ndkRes = NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetNextTreeIndex(trueSetIdx, Treeindex);
			}
			
			if (ndkRes >= 1)
			{
				arg.count = 0;
				Log.e("JNI_NE_POIAddr_GetNextTreeIndex error(1)", "JNI Error code : " + ndkRes);
				return;
			}
		}
		
		if (prevHir == DataCtrlFacade.AREA_LEV_AM && Treeindex.lcount <= 1)
		{
			// もし、上位が都道府県で、取得した次位も都道府県であれば、
			// ダミーで設定しておく
			resetAllAdr();
			ndkRes = NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchNextList(0);
			if (ndkRes >= 1)
			{
				arg.count = 0;
				Log.e("JNI_NE_POIAddr_SearchNextList error", "JNI Error code : " + ndkRes);
				return;
			}
			
			try
			{
				// 0.1秒眠らせてみる
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
				arg.count = 0;
				return;
			}
			
			// そして改めて取得する。
			ndkRes = NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetNextTreeIndex(trueSetIdx, Treeindex);
			if (ndkRes >= 1)
			{
				arg.count = 0;
				Log.e("JNI_NE_POIAddr_GetNextTreeIndex error(2)", "JNI Error code : " + ndkRes);
				return;
			}
		}
		
		if (prevHir >= DataCtrlFacade.AREA_LEV_AM)
		{
			// 選択したレコードを1レコードだけ読み込む
			// ※POIに選択した都道府県名を保持させるため
			POI_Address_ListItem[] Listitem =	 new POI_Address_ListItem[1];
			JNILong Rec = new JNILong();
			NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecList(trueSetIdx, 1, Listitem, Rec);		
		}
		
		// 次の住所レベルによってNaviRun.GetNaviRunObj().setSearchKind()に引き渡す値を振り分ける
		
		int setSrchKind = AppInfo.ID_ACTIVITY_ADDRESSRESULTINQUIRY;
		switch ((int)Treeindex.lcount)
		{
		case 1: // 都道府県
			setSrchKind = AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY;
			break;
		case 2: // 市区町村
		case 3: // 大字
			setSrchKind = AppInfo.ID_ACTIVITY_ADDRESSCITYINQUIRY;
			break;
		default:
			break;
		}
		
		NaviRun.GetNaviRunObj().setSearchKind(setSrchKind);
		
		//mNowSearching = true;
		//NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchNextList(setIdx);
		GoSearchNextList goSearchNextList = new GoSearchNextList(trueSetIdx);
		goSearchNextList.start();
		
		try
		{
			//while (mNowSearching) Thread.sleep(100); // mNowSearchingがfalseになるまで待つ
			goSearchNextList.join();
/*			
			int retrycnt = 0;
			while (mNowSearching)
			{
				if (!mNowSearching)
				{
					break;
				}
				Thread.sleep(100); // mNowSearchingがfalseになるまで待つ
				if (retrycnt++ >= 300) break;
			}
*/			
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
			arg.count = 0;
			mNowSearching = false;
			return;
		}
		//mNowSearching = false;
		
		ndkRes = goSearchNextList.getNDKResult();
		if (ndkRes >= 1)
		{
			arg.count = 0;
			Log.e("JNI_NE_POIAddr_SearchNextList error", "JNI Error code : " + ndkRes);
			mNowSearching = false;
			return;
		}
		
		if (mNowSearching)
		{
			Log.e("Get Address Error", "(JNI_NE_POIAddr_SearchNextList) Time Out");
		}
		
		// 住所リストの取得
		arg.mNowAdrLevel = (int)Treeindex.lcount;
		JNILong ListCount = new JNILong();
		long result = NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecCount(ListCount);

		if (result != 0)
		{
			Log.e("Get Address Error", "(JNI_NE_POIAddr_GetRecCount)code : " + result);
			arg.count = 0;
			return;
		}
		
		int prefCnt = (int)ListCount.lcount;
		POI_Address_ListItem[] listItem = createListItem(prefCnt);
		if (prefCnt >= 1)
		{
			result = NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecList(0, prefCnt, listItem, ListCount);
			
			if (result != 0)
			{
				Log.e("Get Address Error", "(JNI_NE_POIAddr_GetRecList)code : " + result);
				arg.count = 0;
				return;
			}
			
			// 取得した住所リストをC++へ引き渡す
			POI_Address_ListItemtoForDeliveringToNDK(listItem, arg);
		}
		else
		{
			Log.e("Get Address Error", "(JNI_NE_POIAddr_GetRecCount)count : 0");
			arg.count = 0;
		}
	}
	
	private static void POI_Address_ListItemtoForDeliveringToNDK(
			POI_Address_ListItem[] listItem,
			ForDeliveringToNDK arg)
	{
		arg.count = listItem.length;
		arg.mNextCategoly = new int[arg.count];
		arg.mAdrCode = new int[arg.count];
		arg.mAdrNameLen = new int[arg.count];
		arg.mAdrName = new byte[arg.count][];
		arg.mLongitude = new double[arg.count];
		arg.mLatitude = new double[arg.count];
		
		String setStr;
		byte[] strByte;
		
		for (int i = 0; i <= arg.count - 1; i++)
		{
			arg.mAdrCode[i] = i + 1;
			//arg.mAdrCode[i] = i;
			arg.mNextCategoly[i] = listItem[i].getM_iNextCategoryFlag();
/*			
			try
			{
				arg.mAdrName[i] = listItem[i].getM_Name().getBytes("SJIS");
			}
			catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
*/
			setStr = (arg.mNowAdrLevel >= 4) ? changeHalfNumericStrToFull(listItem[i].getM_Name()) : listItem[i].getM_Name();
			//strByte = listItem[i].getM_Name().getBytes();
			strByte = setStr.getBytes();
			
			//String allStr = HtoZ_Kkana(setStr);
			//strByte = allStr.getBytes();
			
/*			
			try
			{
				strByte = setStr.getBytes("SJIS");
			}
			catch (Exception e)
			{
				e.printStackTrace();
				strByte = setStr.getBytes();
			}
*/			
			arg.mAdrNameLen[i] = strByte.length;
			arg.mAdrName[i] = strByte;
			arg.mLongitude[i] = listItem[i].getM_lLongitude();
			arg.mLatitude[i]  = listItem[i].getM_lLatitude();
		}
	}
	
	private static final char[] halfs = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	private static final char[] fulls = {'０', '１', '２', '３', '４', '５', '６', '７', '８', '９'};
	private static String changeHalfNumericStrToFull(String src)
	{
		char one;
		boolean isChanged;
		StringBuffer sb = new StringBuffer();
		int j;
		for (int i = 0; i <= src.length() - 1; i++)
		{
			isChanged = false;
			one = src.charAt(i);
			for (j = 0; j <= halfs.length - 1; j++)
			{
				if (one == halfs[j])
				{
					sb.append(fulls[j]);
					isChanged = true;
					break;
				}
			}
			
			if (!isChanged)
			{
				sb.append(one);
			}
		}
		
		return sb.toString();
	}
	
	// 半角カタカナ<-->全角カタカナ変換テーブル
    private static final String kanaTbl[][] = {
      // 2文字構成の濁点付き半角カナ
      // 必ずテーブルに先頭に置いてサーチ順を優先すること
      { "ｶﾞ", "ガ" }, { "ｷﾞ", "ギ" }, { "ｸﾞ", "グ" }, { "ｹﾞ", "ゲ" }, { "ｺﾞ", "ゴ" }, 
      { "ｻﾞ", "ザ" }, { "ｼﾞ", "ジ" }, { "ｽﾞ", "ズ" }, { "ｾﾞ", "ゼ" }, { "ｿﾞ", "ゾ" },
      { "ﾀﾞ", "ダ" }, { "ﾁﾞ", "ヂ" }, { "ﾂﾞ", "ヅ" }, { "ﾃﾞ", "デ" }, { "ﾄﾞ", "ド" },
      { "ﾊﾞ", "バ" }, { "ﾋﾞ", "ビ" }, { "ﾌﾞ", "ブ" }, { "ﾍﾞ", "ベ" }, { "ﾎﾞ", "ボ" }, 
      { "ﾊﾟ", "パ" }, { "ﾋﾟ", "ピ" }, { "ﾌﾟ", "プ" }, { "ﾍﾟ", "ペ" }, { "ﾎﾟ", "ポ" }, 
      { "ｳﾞ", "ヴ" },
      // 1文字構成の半角カナ
      { "ｱ", "ア" }, { "ｲ", "イ" }, { "ｳ", "ウ" }, { "ｴ", "エ" }, { "ｵ", "オ" }, 
      { "ｶ", "カ" }, { "ｷ", "キ" }, { "ｸ", "ク" }, { "ｹ", "ケ" }, { "ｺ", "コ" }, 
      { "ｻ", "サ" }, { "ｼ", "シ" }, { "ｽ", "ス" }, { "ｾ", "セ" }, { "ｿ", "ソ" }, 
      { "ﾀ", "タ" }, { "ﾁ", "チ" }, { "ﾂ", "ツ" }, { "ﾃ", "テ" }, { "ﾄ", "ト" }, 
      { "ﾅ", "ナ" }, { "ﾆ", "ニ" }, { "ﾇ", "ヌ" }, { "ﾈ", "ネ" }, { "ﾉ", "ノ" }, 
      { "ﾊ", "ハ" }, { "ﾋ", "ヒ" }, { "ﾌ", "フ" }, { "ﾍ", "ヘ" }, { "ﾎ", "ホ" }, 
      { "ﾏ", "マ" }, { "ﾐ", "ミ" }, { "ﾑ", "ム" }, { "ﾒ", "メ" }, { "ﾓ", "モ" }, 
      { "ﾔ", "ヤ" }, { "ﾕ", "ユ" }, { "ﾖ", "ヨ" }, 
      { "ﾗ", "ラ" }, { "ﾘ", "リ" }, { "ﾙ", "ル" }, { "ﾚ", "レ" }, { "ﾛ", "ロ" }, 
      { "ﾜ", "ワ" }, { "ｦ", "ヲ" }, { "ﾝ", "ン" }, 
      { "ｧ", "ァ" }, { "ｨ", "ィ" }, { "ｩ", "ゥ" }, { "ｪ", "ェ" }, { "ｫ", "ォ" }, 
      { "ｬ", "ャ" }, { "ｭ", "ュ" }, { "ｮ", "ョ" }, { "ｯ", "ッ" }, 
      { "｡", "。" }, { "｢", "「" }, { "｣", "」" }, { "､", "、" }, { "･", "・" }, 
      { "ｰ", "ー" }, { "", "" }
  };
    
    public static String HtoZ_Kkana(String p) {
        StringBuffer sb = new StringBuffer();

        for (int i = 0, j = 0; i < p.length(); i++) {
            Character c = new Character(p.charAt(i));       
            // Unicode半角カタカナのコード範囲か？            
            if (c.compareTo(new Character((char)0xff61)) >= 0
                && c.compareTo(new Character((char)0xff9f)) <= 0) {
                // 半角全角変換テーブルを検索する
                for (j = 0; j < kanaTbl.length; j++) {
                    if (p.substring(i).startsWith(kanaTbl[j][0])) {
                        sb.append(kanaTbl[j][1]);
                        i += kanaTbl[j][0].length() - 1;
                        break;
                    }
                }
                
                // 検索できなければ、変換しない
                if (j >= kanaTbl.length) {
                  sb.append(c);
                }
            } else { // Unicode半角カタカナ以外なら変換しない
              sb.append(c);
            }
        }
        return sb.toString();
    }
	
	private static POI_Address_ListItem[] createListItem(int count)
	{
		if (count == 0) return null;
		
		POI_Address_ListItem[] listItem = new POI_Address_ListItem[count];
		for (int i = 0; i <= count - 1; i++)
		{
			listItem[i] = new POI_Address_ListItem();
		}
		return listItem;
	}
	
	// 別スレッドで住所検索を走らせる。
	private static class GoSearchNextList extends Thread
	{
		private int m_setIdx;
		private long m_ndkRes;
		private GoSearchNextList(int setIdx)
		{
			m_setIdx = setIdx;
		}
		
		public final long getNDKResult(){return m_ndkRes;}
		
		public void run()
		{
			m_ndkRes = NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchNextList(m_setIdx);
			if (m_ndkRes >= 1) return;
/*			
			int retrycnt = 0;
			while (mNowSearching)
			{
				if (!mNowSearching)
				{
					break;
				}
				
				try
				{
					Thread.sleep(50); // mNowSearchingがfalseになるまで待つ
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				if (retrycnt++ >= 30) break;
			}
*/
		}
	}
}
