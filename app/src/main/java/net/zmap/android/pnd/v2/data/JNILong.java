/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           JNIJumpKey.java
 * Description    long Class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class JNILong {
	public long lcount;


	/**
	 *  longの値を取得する
	 * @return  longの値を取得する
	 */
	public long getLcount() {
		return lcount;
	}

}
