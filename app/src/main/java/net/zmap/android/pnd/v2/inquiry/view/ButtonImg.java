package net.zmap.android.pnd.v2.inquiry.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.widget.Button;

public class ButtonImg extends Button
{
	private static final Bitmap Bitmap = null;
	public static String LEFT= "left";
	public static String RIGHT = "right";
	private Bitmap m_oIcon = null;
//Del 2011/09/13 Z01_h_yamada Start -->
//	private Bitmap m_oIcon1 = null;
//Del 2011/09/13 Z01_h_yamada End <--

	private int m_wPaddingLeft = 0;
	private int m_wPaddingRight = 0;
	private int m_wPaddingTop = 0;
	private int m_wPaddingBottom = 0;

//Add 2011/09/13 Z01_h_yamada Start -->
	private int m_wDrawIconSize = 0;
//Add 2011/09/13 Z01_h_yamada End <--

	private int m_wIconW = 0;
	private int m_wIconH = 0;
	private int m_wIconSpace = 0;

	private String iconFlag = LEFT;

	public ButtonImg(Context context)
	{
		super(context);
	}

	public ButtonImg(Context context,AttributeSet oSet)
	{
		super(context,oSet);
	}

	public void setIcon(Bitmap oBitmap)
	{
		m_oIcon = oBitmap;
	}

	public void setIcon(int wId)
	{
		m_oIcon = BitmapFactory.decodeResource(getResources(), wId);
	}
//Del 2011/09/13 Z01_h_yamada Start -->
//	public void setIcon1(Bitmap oBitmap)
//	{
//		m_oIcon1 = oBitmap;
//	}
//
//	public void setIcon1(int wId)
//	{
//		m_oIcon1 = BitmapFactory.decodeResource(getResources(), wId);
//	}
//Del 2011/09/13 Z01_h_yamada End <--

	public void setPadding(int wL,int wT,int wR,int wB)
	{
		m_wPaddingLeft = wL;
		m_wPaddingTop = wT;
		m_wPaddingRight = wR;
		m_wPaddingBottom = wB;
		resetIconSize(getWidth(), getHeight());
	}

	public void setIconSpace(int wSpace)
	{
		m_wIconSpace = wSpace;
		resetIconSize(getWidth(), getHeight());
	}

	public void setOneLine (ButtonImg btn) {
		btn.setSingleLine(true);
		btn.setEllipsize(TruncateAt.START);
	}

	/**
	 * ボタンにイメージがあるかどうかをチェックする
	 *
	 * */
	public ButtonImg initBtn(ButtonImg btn,int imgId,String iconPosFlag) {
		if (-1 != imgId) {
			btn.setIcon(imgId);
		} else {
			btn.setIcon(null);
		}
		iconFlag = iconPosFlag;
		m_wDrawIconSize = 0;

		btn.setIconSpace(5);
		return btn;
	}

//Add 2011/09/13 Z01_h_yamada Start -->
	public ButtonImg initBtn(ButtonImg btn,int imgId,String iconPosFlag, int iconDrawSize) {
		if (-1 != imgId) {
			btn.setIcon(imgId);
		} else {
			btn.setIcon(null);
		}
		iconFlag = iconPosFlag;
		m_wDrawIconSize = iconDrawSize;

		btn.setIconSpace(5);
		return btn;
	}
//Add 2011/09/13 Z01_h_yamada End <--

//Del 2011/09/13 Z01_h_yamada Start -->
//	/**
//	 * ボタンにイメージがあるかどうかをチェックする
//	 *
//	 * */
//	public ButtonImg initBtn(ButtonImg btn,int imgId1,int imgId2,String iconPosFlag) {
//		if (-1 != imgId1) {
//			btn.setIcon(imgId1);
//		} else {
//			btn.setIcon(null);
//		}
//		if (-1 != imgId2) {
//			btn.setIcon1(imgId2);
//		} else {
//			btn.setIcon1(null);
//		}
//		iconFlag = iconPosFlag;
//
//		btn.setIconSpace(5);
//		return btn;
//	}
//Del 2011/09/13 Z01_h_yamada End <--

	@Override
	protected void onDraw(Canvas oCanvas)
	{
		if(m_oIcon != null)
		{
//Chg 2011/09/13 Z01_h_yamada Start -->
//				int wY = (getHeight() - m_wIconH)/2;
//				Rect oOldRect = new Rect(0, 0, m_oIcon.getWidth(), m_oIcon.getHeight());
//				Rect oNewRect = null;
//				if (iconFlag.equals(LEFT)) {
//				oNewRect = new Rect(m_wPaddingLeft, wY - m_wPaddingTop + m_wIconSpace,
//				m_oIcon.getWidth() + m_wPaddingLeft,wY + m_oIcon.getHeight() - m_wPaddingTop + m_wIconSpace);
//			} else {
//				oNewRect = new Rect(getWidth()-m_wIconW-m_wIconSpace, wY,getWidth()-m_wIconSpace,wY + m_wIconH);
//			}
//--------------------------------------------
			Rect oOldRect = new Rect(0, 0, m_oIcon.getWidth(), m_oIcon.getHeight());
			Rect oNewRect = null;
			if (iconFlag.equals(LEFT)) {
				// 左上寄せ
				oNewRect = new Rect(m_wPaddingLeft, m_wPaddingTop + m_wIconSpace,
									m_wIconW + m_wPaddingLeft,m_wPaddingTop + m_wIconSpace + m_wIconH);
			} else {
				// 右寄せ(全体)
				int wY = (getHeight() - m_wIconH)/2;
				oNewRect = new Rect(getWidth()-m_wIconW-m_wIconSpace, wY,
									getWidth()-m_wIconSpace, wY + m_wIconH);
			}

//Chg 2011/09/13 Z01_h_yamada End <--

// Chg 2011/06/07 sawada Start -->
//			oCanvas.drawBitmap(m_oIcon, oOldRect, oNewRect, null);
			Matrix matrix = new Matrix();
			matrix.postScale((float)oNewRect.width() / oOldRect.width(), (float)oNewRect.height() / oOldRect.height());
			Bitmap scaled = Bitmap.createBitmap(m_oIcon, 0, 0, oOldRect.width(), oOldRect.height(), matrix, true);
			oCanvas.drawBitmap(scaled, oNewRect.left, oNewRect.top, null);
// Chg 2011/06/07 sawada End   <--
		}

//Del 2011/09/13 Z01_h_yamada Start -->
//		if(m_oIcon1 != null)
//		{
//			int wY = (getHeight() - m_wIconH)/2;
//			Rect oOldRect = new Rect(0, 0, m_oIcon1.getWidth(), m_oIcon1.getHeight());
//			Rect oNewRect = null;
//			if (iconFlag.equals(LEFT)) {
//				oNewRect = new Rect(m_wPaddingLeft, wY - m_wPaddingTop+m_oIcon.getHeight()+2,
//						m_oIcon.getWidth() + m_wPaddingLeft,wY + m_oIcon1.getHeight() - m_wPaddingTop+m_oIcon.getHeight()+2);
//			} else {
//				oNewRect = new Rect(getWidth()-m_wIconW-m_wIconSpace, wY+m_oIcon.getHeight()+2,getWidth()-m_wIconSpace,wY + m_wIconH+m_oIcon.getHeight()+2);
//			}
//// Chg 2011/06/07 sawada Start -->
////			oCanvas.drawBitmap(m_oIcon1, oOldRect, oNewRect, null);
//			Matrix matrix = new Matrix();
//			matrix.postScale((float)oNewRect.width() / oOldRect.width(), (float)oNewRect.height() / oOldRect.height());
//			Bitmap scaled = Bitmap.createBitmap(m_oIcon1, 0, 0, oOldRect.width(), oOldRect.height(), matrix, true);
//			oCanvas.drawBitmap(scaled, oNewRect.left, oNewRect.top, null);
//// Chg 2011/06/07 sawada End   <--
//		}
//Del 2011/09/13 Z01_h_yamada End <--
//yangyang del start Bug1032
//		if (iconFlag.equals(RIGHT) || m_oIcon == null) {
//			super.setPadding(5, 8, m_wIconW, 12);
//			super.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
//		}
//		if (iconFlag.equals(LEFT) &&  m_oIcon == null) {
//			super.setPadding(m_wPaddingLeft + 60 + m_wIconSpace, m_wPaddingTop+20, m_wPaddingRight, m_wPaddingBottom);
//		}
		//yangyang del end Bug1032
		super.onDraw(oCanvas);
	}

	public void superPadding(int wL,int wT,int wR,int wB) {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"superPadding=="+ wL);
		super.setPadding(wL, wT, wR, wB);
	}
	@Override
	protected void onSizeChanged(int wW, int wH, int oldw, int oldh)
	{
        m_wPaddingLeft = 10;
		super.onSizeChanged(wW, wH, oldw, oldh);
		resetIconSize(wW, wH);
	}

	private void resetIconSize(int wW,int wH)
	{
		if(m_oIcon != null)
		{
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"wH======" + wH);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"Left====" + (m_wPaddingLeft + m_wIconW + m_wIconSpace));
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"m_wPaddingTop()==" + m_wPaddingTop +"===m_wPaddingRight===" + m_wPaddingRight+ "=======m_wPaddingBottom===" + m_wPaddingBottom);
////		super.setPadding(5, 8, m_wIconW, 12);

//Chg 2011/09/13 Z01_h_yamada Start -->
//			m_wIconH = wH - getPaddingTop() - getPaddingBottom();
//			m_wIconW = m_wIconH * m_oIcon.getWidth() / m_oIcon.getHeight();
//--------------------------------------------
			if (iconFlag.equals(RIGHT)) {
				m_wIconH = wH - getPaddingTop() - getPaddingBottom();
				m_wIconW = m_wIconH * m_oIcon.getWidth() / m_oIcon.getHeight();
			} else {
				if (m_wDrawIconSize > 0) {
					m_wIconW = m_wDrawIconSize;
					m_wIconH = m_wDrawIconSize * m_oIcon.getWidth() / m_oIcon.getHeight();
				} else {
					m_wIconH = m_oIcon.getHeight();
					m_wIconW = m_wIconH * m_oIcon.getWidth() / m_oIcon.getHeight();
				}
			}
//Chg 2011/09/13 Z01_h_yamada End <--

			//アイコンが左側の場合、onDraw()の前で設定する、右の場合、onDraw()の後で設定する
			if (iconFlag.equals(LEFT)) {
				super.setPadding(m_wPaddingLeft + m_wIconW + m_wIconSpace,
						m_wPaddingTop, m_wPaddingRight, m_wPaddingBottom);
			}
		}
	}



}
