package net.zmap.android.pnd.v2.common.services;

import android.content.Intent;

public interface WalkingStatusEventListener
{
	public void onWalkingEventChange(Intent intent);
}
