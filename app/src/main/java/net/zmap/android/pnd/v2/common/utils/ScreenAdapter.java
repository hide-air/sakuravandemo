package net.zmap.android.pnd.v2.common.utils;

import android.content.res.Configuration;

import net.zmap.android.pnd.v2.common.Constants;


public class ScreenAdapter {

// ADD.2013.02.13 N.Sasao Bug #9269 地点地図画面でボタン位置ズレ対応 START
	private static int old_width;
	private static int old_height;
// ADD.2013.02.13 N.Sasao Bug #9269 地点地図画面でボタン位置ズレ対応  END

	private static int width;
	private static int height;
	private static float scale = 1.0f;
	private static int orientation;
//Add 2011/07/27 Z01thedoanh (自由解像度対応) Start -->
	private static int statusBarHeight = 0;
//Add 2011/07/27 Z01thedoanh (自由解像度対応) End <--

	public static void init(int w, int h, float s_scale){
		scale = s_scale;
		initScreenSize(w, h);
	}
	public static int getWidth() {
		return width;
	}
	public static int getHeight() {
		return height;
	}

	public static void initScreenSize(int w, int h){
// ADD.2013.02.13 N.Sasao Bug #9269 地点地図画面でボタン位置ズレ対応 START
		old_width = width;
		old_height = height;
// ADD.2013.02.13 N.Sasao Bug #9269 地点地図画面でボタン位置ズレ対応  END
		width =  w; //(int) (w * scale);
		height = h; //(int) (h * scale);

        Constants.SCREEN_CENTER_X = width / 2;
        Constants.SCREEN_CENTER_Y = height / 2;

		orientation = width > height ? Configuration.ORIENTATION_LANDSCAPE : Configuration.ORIENTATION_PORTRAIT;
	}

//@@MOD-START BB-0006 2012/12/07 Y.Hayashida
    /**
     * init
     * 初期化（ウィジェット表示専用）
     * @param int   w               スクリーン幅
     * @param int   h               スクリーン高さ
     * @param float s_scale         スケール
     * @param int   widgetWidth     右側ウィジェットの幅（ピクセル)
     * @param int   widgetHeight    上側ウィジェットの高さ（ピクセル）
     */
    public static void init(int w, int h, float s_scale, int widgetWidth, int widgetHeight){
        scale = s_scale;
        initScreenSize(w, h, widgetWidth, widgetHeight);
    }

    /**
     * initScreenSize
     * スクリーンサイズの初期化（ウィジェット表示専用）
     * @param int   w               スクリーン幅
     * @param int   h               スクリーン高さ
     * @param float s_scale         スケール
     * @param int   widgetWidth     右側ウィジェットの幅（ピクセル)
     * @param int   widgetHeight    上側ウィジェットの高さ（ピクセル）
     */
    public static void initScreenSize(int w, int h, int widgetWidth, int widgetHeight){
// ADD.2013.02.13 N.Sasao Bug #9269 地点地図画面でボタン位置ズレ対応 START
        old_width = width;
        old_height = height;
// ADD.2013.02.13 N.Sasao Bug #9269 地点地図画面でボタン位置ズレ対応  END
        // ウィジェット
        width =  w - widgetWidth;
        height = h - widgetHeight;
        Constants.SCREEN_CENTER_X = (w - widgetWidth) / 2;
        Constants.SCREEN_CENTER_Y = (h - widgetHeight) / 2;
        orientation = width > height ? Configuration.ORIENTATION_LANDSCAPE : Configuration.ORIENTATION_PORTRAIT;
    }
//@@MOD-END BB-0006 2012/12/07 Y.Hayashida

	public static boolean isVga(){
		return Math.min(width, height) > 400;
	}
	public static boolean isLandMode() {
		return (width > height);
	}
	public static float getScale() {
		return scale;
	}
    public static int getOrientation() {
        return orientation;
    }
//Add 2011/07/27 Z01thedoanh (自由解像度対応) Start -->
    /**
     * ステータスバーの高さを取得する
     *
     * @param1 無し
     * @return int
     */
    public static int getStatusBarHeight()
    {
        return statusBarHeight;
    }

    /**
     * ステータスバーの高さを設定する
     *
     * @param1 String statusBarHeight
     * @return 無し
     */
    public static void setStatusBarHeight(int a_statusBarHeight )
    {
    	statusBarHeight = a_statusBarHeight;
    }

//Add 2011/07/27 Z01thedoanh (自由解像度対応) End <--

// ADD.2013.02.13 N.Sasao Bug #9269 地点地図画面でボタン位置ズレ対応 START
    /**
     * 以前と現在の高さ、幅の情報が変わったか判定する
     *
     * @param1 none
     * @return true: 変更あり false: 変更なし
     */
    public static boolean isCurrentSizeChange()
    {
    	if( (old_width != width) || (old_height != height) ){
    		return true;
    	}
    	return false;
    }
// ADD.2013.02.13 N.Sasao Bug #9269 地点地図画面でボタン位置ズレ対応  END
}
