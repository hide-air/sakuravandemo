package net.zmap.android.pnd.v2.api.overlay;

import net.zmap.android.pnd.v2.overlay.DrawUserLayer;
import android.os.Parcel;
import android.os.Parcelable;

public abstract class OverlayItem implements Parcelable {
	private long mNEID;
	private DrawUserLayer mMyParent;

    /**
     * コンストラクタ
     *
     */
    public OverlayItem() {
        super();
    }

    protected OverlayItem(Parcel in) {
    }

    
    /**
     * NEから受け取ったIDを取得する。
     * @return
     */
    public long getNEID(){return mNEID;}
    
    public void setNEID(long value){mNEID = value;}
    
    /**
     * 自分の所属する親レイヤを取得する。
     * @return
     */
    public DrawUserLayer getParentLayer(){return mMyParent;}
    
    public void setParentLayer(DrawUserLayer value){mMyParent = value;}

	/* (非 Javadoc)
     * @see net.zmap.android.pnd.v2.agenthmi.api.data.NaviPoint#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
    }

    /* (非 Javadoc)
     * @see net.zmap.android.pnd.v2.agenthmi.api.common.data.ContentData#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }
}
