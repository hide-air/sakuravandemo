package net.zmap.android.pnd.v2.common.services;

import android.content.Context;
import android.content.Intent;

public interface FlightModeEventListener
{
	public void onFlightModeChange(Context con, Intent oIntent);
}
