package net.zmap.android.pnd.v2.common.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.app.NaviAppStatus;

public class ScrollTool extends LinearLayout implements ScrollControlListener
{
//	private OnClickListener m_oUpListener = null;
//	private OnClickListener m_oDownListener = null;
	private ScrollListener m_oListener = null;

	private Button m_oUpButton = null;
	private Button m_oDownButton = null;
	private ScrollBar m_oBar = null;

	public ScrollTool(Context oContext)
	{
		super(oContext);

		setGravity(Gravity.CENTER_HORIZONTAL);

		LayoutInflater oInflater = LayoutInflater.from(oContext);
		LinearLayout oLayout = (LinearLayout) oInflater.inflate(
				R.layout.scroll_toolex, null);

		LinearLayout.LayoutParams oParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

		m_oUpButton = (Button)oLayout.findViewById(R.id.btn_up);
		m_oUpButton.setEnabled(false);
		m_oUpButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View oView)
			{
				clickUp();
			}

		});


		m_oDownButton = (Button)oLayout.findViewById(R.id.btn_down);
		m_oDownButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View oView)
			{
				clickDown();
			}

		});

		m_oBar = (ScrollBar)oLayout.findViewById(R.id.scrollBar);

		addView(oLayout,oParams);
	}

	public void bindView(ScrollListener oListener)
	{
		m_oListener = oListener;
		if(m_oListener != null)
		{
			m_oListener.setControlListener(this);
		}
	}

	public void clickUp()
	{
		if(m_oListener != null)
		{
			m_oListener.movePreviousPage();
			//yangyang add start Bug1109
			update();
			//yangyang add end Bug1109
		}
	}

	public void clickDown()
	{
		if(m_oListener != null)
		{
			m_oListener.moveNextPage();
			//yangyang add start Bug1109
			update();
			//yangyang add end Bug1109
		}
	}

	//yangyang add start Bug1109
	public void update()
	{
		if(m_oListener != null)
		{
			m_oDownButton.setEnabled(m_oListener.isHasNextPage());
			m_oDownButton.setPressed(false);
			m_oUpButton.setEnabled(m_oListener.isHasPreviousPage());
			m_oUpButton.setPressed(false);
			m_oBar.setEnabled(m_oListener.isHasNextPage() || m_oListener.isHasPreviousPage());
		}
	}
	//yangyang add end Bug1109

	@Override
	public void update(int wPos, float wAreaHeight, int wHeight)
	{
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"===>" + wPos + ":" + wAreaHeight + ":"+ wHeight);
		if(m_oListener != null)
		{
			m_oBar.update(wPos,wAreaHeight,wHeight,!m_oListener.isHasNextPage());
			m_oDownButton.setEnabled(m_oListener.isHasNextPage());
			m_oUpButton.setEnabled(m_oListener.isHasPreviousPage());
			//add by wangdong 20110222
			m_oBar.setEnabled(m_oListener.isHasNextPage() || m_oListener.isHasPreviousPage());
		}
	}
}
