/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZVP_SatInfoList_t.java
 * Description    GPS衛星情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZVP_SatInfoList_t {
    public long         wTotalSatsInView;
    public long         lnLongitude1000;
    public long         lnLatitude1000;
    public double           dAltitude;
    public ZVP_SatInfo_t    []stVPSatInfo = null;
    /**
    * Created on 2010/08/06
    * Title:       getWTotalSatsInView
    * Description:   上空に見える衛星数を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getWTotalSatsInView() {
        return wTotalSatsInView;
    }

    /**
    * Created on 2010/08/06
    * Title:       getLnLongitude1000
    * Description:   経度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLnLongitude1000() {
        return lnLongitude1000;
    }

    /**
    * Created on 2010/08/06
    * Title:       getLnLatitude1000
    * Description:   緯度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLnLatitude1000() {
        return lnLatitude1000;
    }
    /**
    * Created on 2010/08/06
    * Title:       getDAltitude
    * Description:   標高を取得する
    * @param1  無し
    * @return       double

    * @version        1.0
    */
    public double getDAltitude() {
        return dAltitude;
    }
    /**
    * Created on 2010/08/06
    * Title:       getStVPSatInfo
    * Description:   各衛星の情報を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public ZVP_SatInfo_t[] getStVPSatInfo() {
        return stVPSatInfo;
    }
}
