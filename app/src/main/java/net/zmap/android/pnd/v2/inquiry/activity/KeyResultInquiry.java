//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;


import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.ScrollBoxAdapter;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Facility_ListItem;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.maps.OpenMap;

/**
 * K-N1_名称検索結果
 *
 * */
public class KeyResultInquiry extends InquiryBaseLoading implements ScrollBoxAdapter {

	/** 表示した都道府県の内容 */
	private POI_Facility_ListItem[] m_Poi_Facility_Listitem = null;
	/** 表示した内容 */
	private POI_Facility_ListItem[] m_Poi_Show_Listitem = null;
	private long DISTANCE = 1000;
	/**
	 * JNIからでーたを取得するとき、スタートIndex
	 * */
	private long RecIndex = 0;
	/** スクロールできる総件数 */
	private int allRecordCount = 0;
	public ScrollList oList = null;

	/** JNIから取得した総件数 */
	private JNILong RecCount = new JNILong();
	/** スクロール対象 */
//	private ScrollBox oBox;
	/** 距離ボタンはクリックするかのフラグ */
//	private boolean isDistanceBtnClick = false;
	Button oBtnKey = null;
	Button oBtnDistance = null;
	private String KEYBY_ALL_COUNTRY = "AllCountry";
	private String KEYBY_ALL_CITY = "AllCity";
	private String KEYBY_KEY = "Key";
	private int getRecordIndex = 0;
	/** 0:初期化　1：50音順　2：距離順 */
	private int waitDialogType = 0;
	private int DISTANCE_SORT_BUTTON = 2;
	private int FIFTY_SORT_BUTTON = 1;
	private int DEFAULT_SORT_BUTTON = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		waitDialogType = DEFAULT_SORT_BUTTON;
		startShowPage(NOTSHOWCANCELBTN);

	}
	private void getMapCenter() {
		JNITwoLong Coordinate = new JNITwoLong();
		NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(Coordinate);
		AppInfo.m_lLat = Coordinate.getM_lLat();
		AppInfo.m_lLon = Coordinate.getM_lLong();
	}
	private ListAdapter listAdapter = new BaseAdapter (){

		@Override
		public int getCount() {
			int iReturn = KeyResultInquiry.this.getCount();
			if (iReturn > 5) {
				return iReturn;
			} else {
				return 5;
			}

		}

		//yangyang add start
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}
        //yangyang add end

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int wPos, View oView, ViewGroup parent) {

//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"getView wPos--------------->" + wPos);
			return KeyResultInquiry.this.getView(wPos, oView);
		}

	};
	//距離順ボタンのクリック処理
	private OnClickListener OnSortChangeBydistanceListener = new OnClickListener() {

		@Override
		public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 50音検索結果 距離順禁止 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 50音検索結果 距離順禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

			waitDialogType = DISTANCE_SORT_BUTTON;
			oView.setSelected(true);
			if (oView instanceof Button) {
	            oBtnDistance = (Button) oView;
	            oBtnDistance.setTextColor(0xffffffff);
			}
			oBtnKey.setSelected(false);
			oBtnKey.setTextColor(0xff000000);

			startShowPage(NOTSHOWCANCELBTN);


//			oBox..setM_wIndex(0);
//			oBox.reset();
//			oBox.refresh();
		}
	};

	//50音順ボタンのクリック処理
	private OnClickListener OnSortChangeByKeyListener = new OnClickListener() {

		@Override
		public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 50音検索結果 50音順禁止 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 50音検索結果 50音順禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

			waitDialogType = FIFTY_SORT_BUTTON;
			oView.setSelected(true);
			oBtnKey.setTextColor(0xffffffff);
			oBtnDistance.setSelected(false);
			oBtnDistance.setTextColor(0xff000000);

			startShowPage(NOTSHOWCANCELBTN);

//			oList.setSelection(0);
//			oList.reset();
//			oList.scroll(0);
//			oBox.setM_wIndex(0);
//			oBox.reset();
//			oBox.refresh();
		}
	};

	/**
	 * JNIの初期化する
	 * @param oIntent Intent対象
	 *
	 * */
	private void initJNIData(Intent oIntent) {

		int iProvinceId = 0;
		int iCityId = 0;
		String strFlag = oIntent.getStringExtra(Constants.FROM_WHICH_SEARCH_KEY);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"strFlag ============" + strFlag);
		if (strFlag.equals(KEYBY_ALL_COUNTRY)) {
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"AllCountry");
			// 全国
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POI_Facility_Refine start");
			NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_Refine((long) 0,(long) 0);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POI_Facility_Refine end");
//		} else if () {
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POI_Facility_Refine start");
//			NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_Refine((long) 0,(long) 0);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POI_Facility_Refine end");
		} else if (strFlag.equals(KEYBY_ALL_CITY)) {
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"AllCity");
			//市区町村
			iProvinceId = oIntent.getIntExtra(Constants.PARAMS_PROVINCE_ID, 0);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"cityitem=======iProvinceId==" + iProvinceId);
			NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_Refine((long) iProvinceId, (long) 0);
		} else if (strFlag.equals(KEYBY_KEY)) {
// Add 2011/05/31 sawada Start -->
//			NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_SearchRefine(Constants.MAX_RECORD_COUNT);
			NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_SyllabaricSearch();
// Add 2011/05/31 sawada End   <--
		} else {
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"cityitem");
			iProvinceId = oIntent.getIntExtra(Constants.PARAMS_PROVINCE_ID, 0);
			iCityId = oIntent.getIntExtra(Constants.PARAMS_TREERECINDEX, 0);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"cityitem=======iProvinceId==" + iProvinceId);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"cityitem=======iCityId==" + iCityId);
			NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_Refine((long) iProvinceId, (long) iCityId);
		}

	}

	@Override
	public int getCount() {
		return allRecordCount;
	}

	@Override
	public int getCountInBox() {
		return 5;
	}

	int wOldPos = 0;
	@Override
	public View getView(final int wPos, View oView) {
		LinearLayout dataLayout = null;
		if (oView == null || !(oView instanceof LinearLayout)) {
			LayoutInflater oInflater = LayoutInflater.from(this);
			dataLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_distance_button_freescroll, null);

			oView  = dataLayout;
//			if (wPos == 0) {
//				initDataList();
//				initCount();
//				RecIndex=0;
//			}


		} else {
//			if (wPos%getCountInBox() == 0) {
//				//ページIndexを取得する
//				pageIndex = wPos/getCountInBox();
//				RecIndex = getCountInBox() * pageIndex;
////				SearchCacheNum = (int) (RecIndex + getCountInBox());
//			}
			dataLayout = (LinearLayout)oView;
		}
//		initListSelectedFalse();
		//表示したいデータを取得する
//		if (wPos%getCountInBox() == 0) {
//			getData();
//		}

		//下にスクロールの場合
		if (wOldPos <= wPos) {
			if (wPos < getCount() && ONCE_GET_COUNT != 0
					&& wPos >= ONCE_GET_COUNT && (wPos % ONCE_GET_COUNT == 0 ||
							(getRecordIndex != wPos / ONCE_GET_COUNT))) {
				//上にスクロール場合
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview  1==== wPos===="
//						+ wPos + ",==getRecordIndex==" + getRecordIndex
//						+ ",==(wPos / ONCE_GET_COUNT)=="
//						+ (wPos / ONCE_GET_COUNT));
				RecIndex = wPos;
				getCurrentPageShowData();
			}
		}else {
			/*総件数76件、最後ページであるし、上にスクロール時、
			 * 70あるいは６９のデータを表示するとき、グレーボタンを表示しないように
			 */
			if (getRecordIndex != wPos / ONCE_GET_COUNT) {
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview  2==== wPos===="
//				+ wPos + ",==getRecordIndex==" + getRecordIndex
//				+ ",==(wPos / ONCE_GET_COUNT)=="
//				+ (wPos / ONCE_GET_COUNT));
				RecIndex = wPos;
				getCurrentPageShowData();
			}
		}

		LinearLayout oBtn = (LinearLayout) dataLayout.findViewById(R.id.LinearLayout_distance);
		TextView oValue = (TextView) dataLayout.findViewById(R.id.TextView_value);
		if (wPos < allRecordCount) {
			oValue.setText(getButtonValue(wPos));
			oBtn.setEnabled(true);
		} else {
			oValue.setText("");
			oBtn.setEnabled(false);
		}


		oValue = (TextView)dataLayout.findViewById(R.id.TextView_distance);
		//距離順ボタンを押下した場合、距離の内容を表示する
		if (waitDialogType == 2) {
			long distance = getButtonDistance(wPos);
			if (wPos < allRecordCount) {
				//yangyang mod start 20110415 Bug995
				if (distance >= DISTANCE) {
					long dis_m = distance%DISTANCE;
					String str = String.valueOf(dis_m/(DISTANCE/10));
					str = str.substring(0,1);
					distance = distance/DISTANCE;
					//yangyang mod start 20110415 Bug995


					oValue.setText("(" + distance + "." + str + "km)");
				} else {
					oValue.setText("(" + getButtonDistance(wPos) + "m)");
				}
				oValue.setVisibility(TextView.VISIBLE);
			} else {
				oValue.setVisibility(TextView.GONE);
			}

		} else {
			oValue.setVisibility(TextView.GONE);
		}
		oBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 50音検索結果 結果禁止 Start -->
				if(DrivingRegulation.CheckDrivingRegulation()){
					return;
				}
// ADD 2013.08.08 M.Honma 走行規制 50音検索結果 結果禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

//Add 2012/02/07 katsuta Start --> #2698
//				NaviLog.d(NaviLog.PRINT_LOG_TAG, "KeyResultInquiry onClick(s) ================== bIsListClicked: " + bIsListClicked);

				if (bIsListClicked) {
					return;
				}
				else {
					bIsListClicked = true;
				}
				NaviLog.d(NaviLog.PRINT_LOG_TAG, "KeyResultInquiry onClick(e) ================== bIsListClicked: " + bIsListClicked);
//Add 2012/02/07 katsuta End <-- #2698
//				initListSelectedFalse();
				//yangyang del start 20110415 Bug1001
//				LinearLayout obtn = (LinearLayout)oView;
				//yangyang add start bug830
//				obtn.setSelected(true);
				//yangyang add end bug830

//				TextView oValue = (TextView) obtn.findViewById(R.id.TextView_value);
				//yangyang del end 20110415 Bug1001
//				//yangyang add start Bug830
//				oValue.setTextColor(0xffffffff);
//				TextView oDis = (TextView) obtn.findViewById(R.id.TextView_distance);
//				oDis.setTextColor(0xffffffff);
				//yangyang add end Bug830
//				POI_Facility_ListItem poi = new POI_Facility_ListItem();
				int showIndex = getShowIndex(wPos);

				RecIndex = wPos;
				getCurrentPageShowData();

				POI_Facility_ListItem poi = m_Poi_Show_Listitem[showIndex];
				Intent oIntent = getIntent();
				//yangyang add start Bug830
//				AppInfo.listIndex = wPos;
				//yangyang add end Bug830
				//yangyang del start 20110415 Bug1001
//				String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY) +
//				Constants.FLAG_TITLE +
//				oValue.getText().toString();
//				String[] titleList = title.split(Constants.FLAG_TITLE);
//				title = "";
//				for (int i = 1 ; i < titleList.length;i++) {
//					title += titleList[i] + " ";
//				}
				//yangyang del end 20110415 Bug1001
//	            int ActiveType = 2;
//	            int TransType = 2;
//	            byte bScale = 0;
//	            long Longitude = 0;
//	            long Latitude = 0;
//	            JNIInt MapScale = new JNIInt();
//	            NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
//	            bScale = (byte) MapScale.getM_iCommon();
//
//	            Latitude = poi.getM_lLatitude();
//	            Longitude = poi.getM_lLongitude();
//	            NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(ActiveType,
//	                    TransType, bScale, Longitude, Latitude);
//
//	            NaviRun.GetNaviRunObj().setSearchKind(Constants.KEY_INQUIRY_RESULT);
				POIData oData = new POIData();
//                oData.m_sName = poi.getM_Name();
				//yangyang mod start 20110415 Bug1001
//				oData.m_sName = title;
				oData.m_sName =poi.getM_Name();
				//yangyang mod end 20110415 Bug1001
                oData.m_sAddress = poi.getM_Name();
                oData.m_wLong = poi.getM_lLongitude();
                oData.m_wLat = poi.getM_lLatitude();
             // XuYang add start #1056
                if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
                    CommonLib.setChangePosFromSearch(true);
                    CommonLib.setChangePos(false);
                } else {
                    CommonLib.setChangePosFromSearch(false);
                    CommonLib.setChangePos(false);
                }
                // XuYang add end #1056
                OpenMap.moveMapTo(KeyResultInquiry.this,oData, oIntent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
                KeyResultInquiry.this.createDialog(Constants.DIALOG_WAIT, -1);

//				bIsClicked = false;


			}


		});

		//上にスクロールの場合
		if (wOldPos > wPos) {
			if (wPos < getCount() && ONCE_GET_COUNT != 0
					//wPos >= ONCE_GET_COUNT 条件の原因：上にスクロールのとき、Index：１５の場合、
					//0～14のデータを取得したが、Index<１５の状況はデータの取得必要がない
					&& wPos >= ONCE_GET_COUNT && (wPos % ONCE_GET_COUNT == 0 ||
							(getRecordIndex != wPos / ONCE_GET_COUNT))) {
	//			getRecordIndex = wPos/ONCE_GET_COUNT;
				//上にスクロール場合
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview  3==== wPos===="
//						+ wPos + ",==getRecordIndex==" + getRecordIndex
//						+ ",==(wPos / ONCE_GET_COUNT)=="
//						+ (wPos / ONCE_GET_COUNT));
				RecIndex = wOldPos - ONCE_GET_COUNT;

				getCurrentPageShowData();
			}
		}
		wOldPos = wPos;
		return oView;
	}
	/**
	 * JNIから、総件数を取得する
	 * */
	private void initCount() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POIFacility_GetRecCount start");
		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetRecCount(RecCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POIFacility_GetRecCount end============RecCount=========" + RecCount.lcount);
		showToast(this,RecCount);
		allRecordCount = initPageIndexAndCnt(RecCount);

	}


	private void resetShowList(Long lRecIndex,JNILong Rec) {
		if (lRecIndex < ONCE_GET_COUNT) {
			int len = m_Poi_Facility_Listitem.length;
			for (int i = 0 ; i < len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_Facility_Listitem[i];
			}
		} else {
			if (lRecIndex >= ONCE_GET_COUNT*2) {
				for (int i = m_Poi_Facility_Listitem.length-ONCE_GET_COUNT; i < ONCE_GET_COUNT*2 ; i++) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"resetShowList i===" + i);
					if (i <ONCE_GET_COUNT) {
						m_Poi_Show_Listitem[i] = m_Poi_Show_Listitem[i+ONCE_GET_COUNT];
					} else {
						m_Poi_Show_Listitem[i] = new POI_Facility_ListItem();
					}
				}
			}

			int len = (int)Rec.lcount;
			int j = 0;
			for (int i = ONCE_GET_COUNT ; i < ONCE_GET_COUNT + len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_Facility_Listitem[j];
//				System.out.println(m_Poi_Show_Listitem[i].getM_Name());
				j++;
			}
		}

	}
	/**
	 * データオブジェクトのメモリを申請する
	 * */
	private void initDataList() {


		initOnceGetList();

		m_Poi_Show_Listitem = new POI_Facility_ListItem[ONCE_GET_COUNT*2];
		for (int i = 0 ; i < ONCE_GET_COUNT*2 ; i++) {
			m_Poi_Show_Listitem[i] = new POI_Facility_ListItem();
		}

	}
	private void initOnceGetList() {
		m_Poi_Facility_Listitem = new POI_Facility_ListItem[ONCE_GET_COUNT];
		for (int i = 0 ; i < ONCE_GET_COUNT ; i++) {
			m_Poi_Facility_Listitem[i] = new POI_Facility_ListItem();
		}

	}

	private String getButtonValue(int wPos) {
		int showIndex = getShowIndex(wPos);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"wPos=====showIndex=========" + wPos + "," + showIndex);
		return m_Poi_Show_Listitem[showIndex].getM_Name();
	}
	private long getButtonDistance(int wPos) {
		int showIndex = getShowIndex(wPos);
		return m_Poi_Show_Listitem[showIndex].getM_lDistance();
	}

	private int getShowIndex(int wPos) {
		if (wPos >= ONCE_GET_COUNT*2) {
			int pageIndex  = wPos/ONCE_GET_COUNT;
			wPos = wPos - ONCE_GET_COUNT * pageIndex + ONCE_GET_COUNT;
		}
		return wPos;
	}
	/**
	 * カレント表示した情報を初期化する
	 * */
	private void getCurrentPageShowData() {
		JNILong ListCount  = new JNILong();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"RecIndex===" + RecIndex);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POIFacility_GetRecList start");
		if (RecIndex < 0) {
			RecIndex = 0;
		}
		getRecordIndex = (int)RecIndex / ONCE_GET_COUNT;
		initOnceGetList();
		if (ONCE_GET_COUNT != 0 && RecIndex % ONCE_GET_COUNT != 0) {
			RecIndex = RecIndex/ONCE_GET_COUNT*ONCE_GET_COUNT;
		}
		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetRecList(RecIndex, ONCE_GET_COUNT,
				m_Poi_Facility_Listitem, ListCount);


		resetShowList(RecIndex,ListCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POIFacility_GetRecList end");

	}

	@Override
	protected boolean onClickGoMap() {

		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_Clear();
		return super.onClickGoMap();
	}

	@Override
	protected boolean onClickGoBack() {
		doBack();

		return super.onClickGoBack();
	}

	private void doBack() {
		Intent oIntent = getIntent();
		String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
		if (-1 != title.lastIndexOf(Constants.FLAG_TITLE)) {
			title = title.substring(0, title.lastIndexOf(Constants.FLAG_TITLE));
		}

		oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
		this.setResult(Activity.RESULT_CANCELED,getIntent());
		finish();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent oIntent) {
		//yangyang add start bug830
		if (resultCode == RESULT_CANCELED) {
			//ある条件で戻るとき、選択状態は戻らないが、全部選択状態を削除した
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"RecIndex-=========listIndex====" + RecIndex + "====" + AppInfo.listIndex);

//			initListSelectedFalse();
//			boolean isSelected = false;
//			int iCount = oList.getChildCount();
//			if (iCount > 0) {
//				for (int i = 0 ; i < iCount; i++) {
//					LinearLayout obtn = (LinearLayout)oList.getChildAt(i);
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"===========isSelected=>>>" + obtn.isSelected() + ",isFocused ====" + obtn.isFocused());
//					if (obtn.isSelected()) {
//						isSelected = true;
////						LinearLayout obtn = (LinearLayout)oList.getChildAt(selectedIndex);
//						obtn.setSelected(true);
//						break;
//					} else {
//						obtn.setSelected(false);
//						TextView oValue = (TextView) obtn.findViewById(R.id.TextView_value);
//						oValue.setTextColor(0xff000000);
//						TextView oDis = (TextView) obtn.findViewById(R.id.TextView_distance);
//						oDis.setTextColor(0xff000000);
//					}
//				}
//				if (!isSelected) {
//					int selectedIndex = AppInfo.listIndex;
//					LinearLayout obtn = (LinearLayout)oList.getChildAt(selectedIndex);
//					obtn.setSelected(true);
//					TextView oValue = (TextView) obtn.findViewById(R.id.TextView_value);
//					oValue.setTextColor(0xffffffff);
//					TextView oDis = (TextView) obtn.findViewById(R.id.TextView_distance);
//					oDis.setTextColor(0xffffffff);
//				}
//			}

		}
		//yangyang add end bug830
		super.onActivityResult(requestCode, resultCode, oIntent);
	}

//	private void initListSelectedFalse() {
//		int iCount = oList.getChildCount();
//		for (int i = 0 ; i < iCount; i++) {
//			LinearLayout obtn = (LinearLayout)oList.getChildAt(i);
//			obtn.setSelected(false);
//			TextView oValue = (TextView) obtn.findViewById(R.id.TextView_value);
//			oValue.setTextColor(0xff000000);
//			TextView oDis = (TextView) obtn.findViewById(R.id.TextView_distance);
//			oDis.setTextColor(0xff000000);
//		}
//	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			doBack();
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected boolean onStartShowPage() throws Exception {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStartShowPage waitDialogType=======" + waitDialogType);
		if (waitDialogType == FIFTY_SORT_BUTTON){

			//50音順
			NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_SyllabaricSearch();
//			isDistanceBtnClick = false;
			initCount();
			RecIndex = 0;
			getCurrentPageShowData();

		} else if (waitDialogType == DISTANCE_SORT_BUTTON) {

//			oBtnKey.setSelected(false);
//			JNITwoLong Coordinate = new JNITwoLong();
//			NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(Coordinate);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"Coordinate.getM_lLong()===" + Coordinate.getM_lLong());
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"Coordinate.getM_lLat()===" + Coordinate.getM_lLat());
			//地図中心の緯度
			long lnLongitude = AppInfo.m_lLon;//Coordinate.getM_lLong();//*8/DISTANCE;
			//地図中心の経度
			long lnLatitude = AppInfo.m_lLat;//Coordinate.getM_lLat();//*8/DISTANCE;
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"lnLongitude===" + lnLongitude);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"lnLatitude===" + lnLatitude);
			//検索の範囲（ｍ）
// Chg 2011/05/31 sawada Start -->
////		long lnDistance = 5000;
//			long lnDistance = 0;
			long lnDistance = 5000;
// Chg 2011/05/31 sawada End   <--
			NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_AroundSearch(lnLatitude, lnLongitude, lnDistance);
//			isDistanceBtnClick = true;
			initCount();
			RecIndex= 0;
			getCurrentPageShowData();
		} else {
			//初期化
			//JNIの初期化する
			oIntent = getIntent();
			initJNIData(oIntent);
			initDataList();
			initCount();
			RecIndex=0;
			getCurrentPageShowData();
			getMapCenter();
		}
		return true;
	}
	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {
		if(bGetData)
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					if (waitDialogType == FIFTY_SORT_BUTTON || waitDialogType == DISTANCE_SORT_BUTTON) {
						oList.reset();
						oList.scroll(0);
					} else {
						initDialog();
					}
				}




			});
		}

	}
	private void initDialog() {
		Intent oIntent = getIntent();

//Del 2011/09/17 Z01_h_yamada Start -->
//		//タイトルを設定する
//		int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE,0);
//		if (0!= titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--


		//画面を初期化する
		LayoutInflater oInflater = LayoutInflater.from(this);
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base_list_freescroll, null);
//		検索内容を初期化する
		TextView oText = (TextView)oLayout.findViewById(R.id.inquiry_title);
		String sTitle = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
		if(sTitle == null)
		{
			oText.setText("");
		}
		else
		{
			oText.setText(sTitle);
		}
//		50音順、距離順ボタンの初期化

		oBtnKey = (Button) oLayout.findViewById(R.id.inquiry_btn);
		oBtnKey.setBackgroundResource(R.drawable.btn_default);
		oBtnKey.setText(R.string.btn_sort_key);
		oBtnKey.setOnClickListener(OnSortChangeByKeyListener);
		//yangyang add start Bug1050
		oBtnKey.setSelected(true);
		oBtnKey.setTextColor(0xffffffff);
		//yangyang add end Bug1050

		oBtnDistance = (Button)oLayout.findViewById(R.id.inquiry_btn2);
		oBtnDistance.setVisibility(Button.VISIBLE);
		oBtnDistance.setBackgroundResource(R.drawable.btn_default);
		oBtnDistance.setText(R.string.btn_sort_distance);
		oBtnDistance.setOnClickListener(OnSortChangeBydistanceListener);

//		oBox = (ScrollBox)oLayout.findViewById(R.id.scrollBox);
//		oBox.setAdapter(this);
		oList = (ScrollList)oLayout.findViewById(R.id.scrollList);
		oList.setPadding(0, 5, 0, 5);
//Del 2011/10/06 Z01_h_yamada Start -->
//		oList.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
		oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
		oList.setAdapter(listAdapter);
//
		ScrollTool oTool = new ScrollTool(this);
//		oTool.bindView(oBox);
		oTool.bindView(oList);

		setViewInOperArea(oTool);

		this.setViewInWorkArea(oLayout);

	}
// Add 2011/09/27 Z01nakajima Start -->
    public static void showToast(final Activity oActivity, JNILong RecCount) {
// Chg 2011/10/08 katsuta Start -->
//        if (RecCount.lcount > Constants.MAX_RECORD_COUNT) {
    	if (RecCount.lcount >= Constants.MAX_RECORD_COUNT) {
// Chg 2011/10/08 katsuta End <--
            oActivity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast toast = Toast.makeText(oActivity, null, Toast.LENGTH_SHORT);
                    toast.setText(R.string.max_limited_msg);
                    toast.show();
                }
            });

        }
        else if (RecCount.lcount == Constants.NOT_RECORD_COUNT) {
            oActivity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast toast = Toast.makeText(oActivity, null, Toast.LENGTH_SHORT);
                    toast.setText(R.string.not_around_msg);
                    toast.show();
                }
            });

        }
    }
// Add 2011/09/27 Z01nakajima End <--
}
