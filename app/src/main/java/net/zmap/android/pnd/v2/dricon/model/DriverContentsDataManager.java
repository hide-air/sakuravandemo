/**
 *  @file		DriverContentsDataManager
 *  @brief		ドライブコンテンツのデータ取得を実行する
 *
 *  @attention
 *  @note
 *
 *  @author		Manabu Watanabe [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2011-09-01)
 *  @version	$Revision: $ by $Author: $
 *
 */

package net.zmap.android.pnd.v2.dricon.model;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;

// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
import java.io.File;
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
// ADD.2013.04.05 N.Sasao AyncTask二重実行エラー対応 START
import java.util.Timer;
import java.util.TimerTask;
// ADD.2013.04.05 N.Sasao AyncTask二重実行エラー対応  END
import java.util.Map.Entry;
//ADD.2013.03.01 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(3) START
import java.util.Iterator;

// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
import net.zmap.android.pnd.v2.common.utils.CipherUtils;
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END

import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.DC_ContentsData;
import net.zmap.android.pnd.v2.data.DC_ContentsDetailData;
import net.zmap.android.pnd.v2.data.DC_POIInfoData;
import net.zmap.android.pnd.v2.data.DC_POIInfoDetailData;
//ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない START
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
//ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  END

public class DriverContentsDataManager implements DriveContentsAsyncDataGetter.onEndGetListener  {

	private Handler mHandler = null;
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
	private Handler mDataManagerHandler = null;
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
    private List<DC_ContentsData> mlstContents = null;
    public Map<Integer , DC_ContentsDetailData> mMapDetail = null;
// MOD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2) START
    public Map<Long,DC_POIInfoDetailData>	mMapPOIDetail = new LinkedHashMap<Long , DC_POIInfoDetailData>();
// MOD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2)  END
    public List<DC_POIInfoDetailData> 			mOrbisList = null;
    public DriveContentsAsyncDataGetter mAsyncGetter = null;
    public List<Integer> mMeshList = null;
    public Activity		mParentActivity = null;
    public int			mMenuDataFlag;
    public List<DC_POIInfoDetailData> mOrbisPOIDetail = null;
    private boolean mOrbisAlert = true;
    private Object mObjLock = new Object();

// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
    private CipherUtils oCipher = null;
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
    private String mDataPath = null;
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

    	//優先順位は
// MOD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1) START
    public static final int STATE_TYPE_NONE = 0;
    public static final int STATE_TYPE_CONTENTS = 1;
    public static final int STATE_TYPE_MESH = 2;
    public static final int STATE_TYPE_ORBIS = 3;
    public static final int STATE_TYPE_DELETE_FOLDER = 4;
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
    public static final int STATE_TYPE_MESH_SEND = 5;
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
    public static final int STATE_TYPE_POI_DETAIL = 6;

    public static final int EVENT_TYPE_REQUEST_MAP_UPDATE = 0;
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END

    public static final int REQUEST_TYPE_NONE = 0;
    public static final int REQUEST_TYPE_REAROUND = 1;
// ADD.2013.03.28 N.Sasao ドライブコンテンツ切り替え時に再描画処理を行う START
    public static final int REQUEST_TYPE_REDRAW_ONLY = 2;
// ADD.2013.03.28 N.Sasao ドライブコンテンツ切り替え時に再描画処理を行う  END
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END

// MOD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1)  END
    private int mStateType = STATE_TYPE_NONE;

// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
	private int mlstDataOffset = 0;
	private boolean mIsRequestRecreate = false;
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない START
    private final static int POIDATA_CULL_CHECK	= 20;
// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  END
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
    private static final String CIPHER_FILE_NAME = "Content";
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
    /**
     * コンストラクタ
     * @param handler イベントハンドラ
     * @param activity 親となるアクティビティ
     */
    public DriverContentsDataManager(Handler handler, Activity activity) {
		mHandler = handler;
		mParentActivity = activity;
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
		dataManagerEventHandler();
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
		oCipher = new CipherUtils( activity );
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
        mDataPath = ((NaviApplication)mParentActivity.getApplication()).getNaviAppDataPath().getAssetFilePath();;
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
	}

	/**
	 * ハンドラにメッセージを送信する
	 * @param msg 送信すべきメッセージ
	 */
	public void notifyHandler(Message msg){
		mHandler.sendMessage(msg);
	}

// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
	/**
	 * ハンドラにメッセージを送信する
	 * @param msg 送信すべきメッセージ
	 */
	public void notifyDataManagerHandler(Message msg){
		mDataManagerHandler.sendMessage(msg);
	}
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END

	/**
	 * コンテンツ取得可能かどうかを判定する
	 * @return コンテンをすでに保持する場合、true
	 */
	public boolean isGetContents(){
		return (mlstContents!= null && mMapDetail != null);
	}

	/**
	 * メニューデータ取得開始
	 * @param flag データ取得成功時にイベントで渡されるパラメータ
	 * @note 非同期クラスの呼び出しを行い、終了する
	 * @return 非同期クラスの呼び出し完了できるとtrue。
	 */

	public boolean getMenuData(int flag) {
// ADD.2013.04.05 N.Sasao コベリティ指摘対応対応 START
		boolean bResult = false;
// ADD.2013.04.05 N.Sasao コベリティ指摘対応対応  END

		if(	mStateType == STATE_TYPE_CONTENTS){
			return false;
		}

		String strVersion = "";
		try {
		    String packegeName = mParentActivity.getPackageName();
		    PackageInfo packageInfo = mParentActivity.getPackageManager().getPackageInfo(packegeName, PackageManager.GET_META_DATA);
		    strVersion =  packageInfo.versionName;
		    if(strVersion != null && strVersion.length() > 2){
		    	strVersion = strVersion.substring(0, strVersion.length() - 2);
		    }
		} catch (NameNotFoundException e) {
		}
// MOD.2013.04.05 N.Sasao コベリティ指摘対応対応 START
	    if( (strVersion == null) || (strVersion.length() == 0) ){
	    	return false;
	    }

		mMenuDataFlag = flag;
		if(mAsyncGetter != null){
			mAsyncGetter.cancel(true);
			mAsyncGetter = null;
		}
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
        String userPath = ((NaviApplication)mParentActivity.getApplication()).getNaviAppDataPath().getUserDataPath();
		mAsyncGetter = new DriveContentsAsyncDataGetter(this , mObjLock, userPath, mDataPath);
		if( mAsyncGetter == null ){
			return false;
		}
// MOD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START

		String beforeDriconPath = mDataPath +
				DriveContentsAsyncDataGetter.GetContentsOpposingPath() +
				CIPHER_FILE_NAME;
		String afterDriconPath = mDataPath +
				DriveContentsAsyncDataGetter.GetContentsOpposingPath() +
				DriveContentsAsyncDataGetter.GetContentsName();

		if( oCipher == null ){
			return false;
		}
		bResult = oCipher.fileToDecrypt(beforeDriconPath, afterDriconPath, false);

		if( bResult ){
	    	mAsyncGetter.SetTask(DriveContentsAsyncDataGetter.READ_CONTENTS_LIST);
		}
		else{
			DriveContentsAsyncDataGetter.DeleteFolder(null , DriveContentsAsyncDataGetter.FILETYPE_ALL, mDataPath);
			mAsyncGetter.SetTask(DriveContentsAsyncDataGetter.DOWNLOAD_MAIN);
		}
// MOD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

		mStateType = STATE_TYPE_CONTENTS;

		bResult = true;
// MOD.2013.04.05 N.Sasao AyncTask二重実行エラー対応 START
		if(mlstContents == null){
			try{
				//ドライブコンテンツ運用サーバ
// MOD.2013.08.21 N.Sasao v27本番サーバー切り替え対応 START
				mAsyncGetter.execute("http://mdp.its-mo.net/android/zdcpnd_v27/cgi/drive/get_contents.cgi?ver=pnd.c2," + strVersion); // v2.7 本番サーバー
//				mAsyncGetter.execute("http://mdts.its-mo.net/android/zdcpnd_v27/cgi/drive/get_contents.cgi?ver=pnd.c2," + strVersion); // v2.7 試験サーバー
// MOD.2013.08.21 N.Sasao v27本番サーバー切り替え対応  END
//				mAsyncGetter.execute("http://mdp.its-mo.net/android/zdcpnd_v25/cgi/drive/get_contents.cgi?ver=pnd.c2," + strVersion); // v2.5
			}catch(IllegalStateException e){
				// フェールセーフ
				NaviLog.e(NaviLog.PRINT_LOG_TAG, "getMenuData execute1 error = " + e.toString() );
				bResult = false;
			}
		}else{
			try{
				mAsyncGetter.execute((String)null);
			}catch(IllegalStateException e){
				// フェールセーフ
				NaviLog.e(NaviLog.PRINT_LOG_TAG, "getMenuData execute2 error = " + e.toString() );
				bResult = false;
			}
		}
// MOD.2013.04.05 N.Sasao AyncTask二重実行エラー対応  END
		return bResult;
// MOD.2013.04.05 N.Sasao コベリティ指摘対応対応  END
	}

	/**
	 * @param nID
	 * @return
	 */
	public DC_ContentsDetailData getDetailMenuData(int nID){
		if(mMapDetail != null){
			return mMapDetail.get(nID);
		}
		return null;
	}

// ADD/MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
	/**
	 * POIの詳細情報を取得する
	 * @param id POIのID
	 * @return 成功した場合、IDにあったPOIの詳細情報。失敗した場合、NULL。
	 */
	public boolean requestDetailPOI(long id) {
		boolean bRes = false;
// MOD.2013.04.05 N.Sasao AyncTask二重実行エラー対応 START
		DC_POIInfoDetailData oPOIDetailData = new DC_POIInfoDetailData();
// MOD.2013.04.05 N.Sasao AyncTask二重実行エラー対応  END
		synchronized(this){
			if( (mMapPOIDetail != null) && !mMapPOIDetail.isEmpty() && (mMapPOIDetail.containsKey(id)) ){
				try{
					oPOIDetailData = mMapPOIDetail.get(id);
// ADD.2013.06.17 N.Sasao キャッシュ値を返す時にエラーとして結果を返す START
					bRes = true;
// ADD.2013.06.17 N.Sasao キャッシュ値を返す時にエラーとして結果を返す  END
				}
		  		catch(IllegalArgumentException e){
		  			NaviLog.e(NaviLog.PRINT_LOG_TAG, "DRIVE_CONTENTS requestDetailPOI id is not found = " + id);
		  			return false;
		  		}

				if( (oPOIDetailData != null) && oPOIDetailData.m_bDetailUpdate ){
					Message msg = new Message();
					msg.what = Constants.DC_POI_DETAIL_DATA;
					msg.arg1 = 0;
					msg.arg2 = 0;
					msg.obj = oPOIDetailData;
					notifyHandler(msg);
				}
				else{
					bRes = requestDetailPOIData(id);
				}
			}
		}
		return bRes;
	}
	/**
	 * 周辺検索開始
	 * @param meshList メッシュ一覧
	 * @note 非同期クラスの呼び出しを行い、終了する
	 * @return 非同期クラスの呼び出し完了できるとtrue。
	 */
	public boolean requestDetailPOIData( long id ) {
		if(	mStateType == STATE_TYPE_CONTENTS ||
			mStateType == STATE_TYPE_DELETE_FOLDER ||
			mStateType == STATE_TYPE_ORBIS){
			return false;
		}

		if(mlstContents == null || mMapDetail == null){
			return false;
		}

		if(mAsyncGetter != null){
			mAsyncGetter.cancel(true);
			mAsyncGetter = null;
		}

        String userPath = ((NaviApplication)mParentActivity.getApplication()).getNaviAppDataPath().getUserDataPath();
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
		mAsyncGetter = new DriveContentsAsyncDataGetter(this , mObjLock, userPath, mDataPath);
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

		mAsyncGetter.SetTask(DriveContentsAsyncDataGetter.DOWNLOAD_POI_DETAIL);
		mAsyncGetter.SetContentsList(mlstContents , mMapDetail, mMapPOIDetail );

		mAsyncGetter.SetRequestPoiId(id);
		mStateType = STATE_TYPE_POI_DETAIL;
// MOD.2013.04.05 N.Sasao AyncTask二重実行エラー対応 START
		try{
			mAsyncGetter.execute();
		}catch(IllegalStateException e){
			// フェールセーフ
			NaviLog.e(NaviLog.PRINT_LOG_TAG, "requestDetailPOIData execute error = " + e.toString() );
			return false;
		}
// MOD.2013.04.05 N.Sasao AyncTask二重実行エラー対応  END
		return true;
	}
// ADD/MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
	/**
	 * ユーザが変更した情報を設定し、ファイルに保存する
	 * @param list コンテンツ情報
	 * @return 保存に成功した場合、trueとなる
	 */
	public boolean setSettingMenu(List<DC_ContentsData> list) {
		if(list == null){
			return false;
		}

		mStateType = STATE_TYPE_DELETE_FOLDER;
		for(int j = 0; j < list.size() ; j++){
			if(list.get(j).m_bDisplayItem == true){
				if(mMapDetail != null){
					if(!mMapDetail.get(list.get(j).m_iContentsID).m_sContentsID.equals("zdcorbis")){
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
						DriveContentsAsyncDataGetter.DeleteFolder(mMapDetail.get(list.get(j).m_iContentsID).m_sURL , DriveContentsAsyncDataGetter.FILETYPE_MESH, mDataPath);
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
					}else{
						mOrbisAlert = list.get(j).m_bCheckItem || list.get(j).m_bLayering;
					}
				}
			}
		}

		mStateType = STATE_TYPE_NONE;

		if(mlstContents != null){
			mlstContents = null;
		}

		mlstContents = new ArrayList<DC_ContentsData>(list);

        String userPath = ((NaviApplication)mParentActivity.getApplication()).getNaviAppDataPath().getUserDataPath();
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
		DriveContentsAsyncDataGetter Getter = new DriveContentsAsyncDataGetter(this , mObjLock, userPath, mDataPath);
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
		Getter.writeContentsOutputFile( mlstContents, mMapDetail);

// ADD.2013.03.28 N.Sasao ドライブコンテンツ切り替え時に再描画処理を行う START
		/* 現状の最新データで再描画を要求 */
		cacheRedrawPOIInfo();
// ADD.2013.03.28 N.Sasao ドライブコンテンツ切り替え時に再描画処理を行う  END

		return true;
	}

// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2) START
	/**
	 * 周辺検索開始前判断処理
	 * @param meshList メッシュ一覧
	 * @note 周辺検索開始前にキャッシュを返すか非同期クラスの呼び出しを行うか判断する
	 * @note 周辺検索が必要な場合は、そのまま周辺検索を開始する。
	 * @return キャッシュデータを返す or 呼び出し完了できるとtrue。
	 */
	public boolean preAroundSearch(List<Integer> meshList) {
		boolean bSearchRes = false;
// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
		if(	mStateType == STATE_TYPE_CONTENTS ||
			mStateType == STATE_TYPE_DELETE_FOLDER ||
			mStateType == STATE_TYPE_ORBIS ||
			mStateType == STATE_TYPE_POI_DETAIL){
// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
//			NaviLog.e(NaviLog.PRINT_LOG_TAG, "preAroundSearch return mStateType = " + mStateType);
			return false;
		}

		if(mlstContents == null || mMapDetail == null){
			return false;
		}
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
		/* 最初のコンテンツから再生成する要求があった場合 */
		if( mIsRequestRecreate == true ){
			mlstDataOffset = 0;
			mIsRequestRecreate = false;
		}

		long start = System.currentTimeMillis();
		NaviLog.v(NaviLog.PRINT_LOG_TAG, "DRIVE_CONTENTS AroundSearch start time = " + start);
		bSearchRes = aroundSearch(meshList);

// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
		return bSearchRes;
	}
// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2)  END
	/**
	 * 周辺検索開始
	 * @param meshList メッシュ一覧
	 * @note 非同期クラスの呼び出しを行い、終了する
	 * @return 非同期クラスの呼び出し完了できるとtrue。
	 */
	public boolean aroundSearch(List<Integer> meshList) {
		if(	mStateType == STATE_TYPE_CONTENTS ||
			mStateType == STATE_TYPE_DELETE_FOLDER ||
// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
			mStateType == STATE_TYPE_ORBIS ||
			mStateType == STATE_TYPE_POI_DETAIL){
// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
			return false;
		}

		if(mlstContents == null || mMapDetail == null){
			return false;
		}

		if(mAsyncGetter != null){
			mAsyncGetter.cancel(true);
			mAsyncGetter = null;
		}

// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2) START
		if(mMeshList != null){
			mMeshList = null;
		}
// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2) START
		mMeshList = new ArrayList<Integer>(meshList);

        String userPath = ((NaviApplication)mParentActivity.getApplication()).getNaviAppDataPath().getUserDataPath();
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
		mAsyncGetter = new DriveContentsAsyncDataGetter(this , mObjLock, userPath, mDataPath);
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2) START
		/* キャンセル割り込み対策(基本この中には来ない) */
		if( mlstDataOffset >= mlstContents.size() ){
			mlstDataOffset = 0;
			NaviLog.i(NaviLog.PRINT_LOG_TAG, "aroundSearch error clear");
		}

		mAsyncGetter.SetlstDataOffset( mlstDataOffset );
// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2)  END
		mAsyncGetter.SetTask(DriveContentsAsyncDataGetter.DOWNLOAD_MESH);
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
		synchronized(this){
			mAsyncGetter.SetContentsList(mlstContents , mMapDetail, mMapPOIDetail );
		}
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
		mAsyncGetter.SetMeshList(mMeshList );
		mStateType = STATE_TYPE_MESH;
// MOD.2013.04.05 N.Sasao AyncTask二重実行エラー対応 START
		try{
			mAsyncGetter.execute();
		}catch(IllegalStateException e){
			// フェールセーフ
			NaviLog.e(NaviLog.PRINT_LOG_TAG, "aroundSearch execute error = " + e.toString() );
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
                public void run() {
                	aroundSearch(mMeshList);
                }
            }, 100);
		}
// MOD.2013.04.05 N.Sasao AyncTask二重実行エラー対応  END
		return true;
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.model.DriveContentsAsyncDataGetter.onEndGetListener#onEndData(java.util.List, java.util.Map)
	 * コンテンツ情報取得終了イベント
	 */
	@Override
	public void onEndData(List<DC_ContentsData> list , Map<Integer , DC_ContentsDetailData> mapData) {

		if(list != null && mapData != null){
			mlstContents = new ArrayList<DC_ContentsData>(list);
			mMapDetail = new HashMap<Integer , DC_ContentsDetailData>(mapData);
	        String userPath = ((NaviApplication)mParentActivity.getApplication()).getNaviAppDataPath().getUserDataPath();
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
			DriveContentsAsyncDataGetter Getter = new DriveContentsAsyncDataGetter(this , mObjLock, userPath, mDataPath);
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
			Getter.writeContentsOutputFile( mlstContents , mMapDetail);
			Getter = null;
			for(int j = 0; j < mlstContents.size() ; j++){
				if(mlstContents.get(j).m_bDisplayItem == true){
					if(mMapDetail != null){
						if(!mMapDetail.get(mlstContents.get(j).m_iContentsID).m_sContentsID.equals("zdcorbis")){
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
							DriveContentsAsyncDataGetter.DeleteFolder(mMapDetail.get(mlstContents.get(j).m_iContentsID).m_sURL , DriveContentsAsyncDataGetter.FILETYPE_MESH, mDataPath);
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
						}else{
							mOrbisAlert = mlstContents.get(j).m_bCheckItem || mlstContents.get(j).m_bLayering;
						}
					}
				}
			}
		}
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
		String beforeDriconPath = mDataPath +
				DriveContentsAsyncDataGetter.GetContentsOpposingPath() +
				DriveContentsAsyncDataGetter.GetContentsName();

		String afterDriconPath = mDataPath +
				DriveContentsAsyncDataGetter.GetContentsOpposingPath() +
				CIPHER_FILE_NAME;
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

		if( oCipher != null ){
			oCipher.fileToEncrypt(beforeDriconPath, afterDriconPath, true);
		}
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END

		Message msg = new Message();
		msg.what = mMenuDataFlag;
		msg.arg1 = 0;
		msg.arg2 = 1;
		msg.obj = mlstContents;
		notifyHandler(msg);

		if( mStateType == STATE_TYPE_CONTENTS ){
// ADD.2013.04.05 N.Sasao AyncTask二重実行エラー対応 START
			mAsyncGetter = null;
// ADD.2013.04.05 N.Sasao AyncTask二重実行エラー対応  END
			mStateType = STATE_TYPE_NONE;
		}
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.model.DriveContentsAsyncDataGetter.onEndGetListener#onEndAroundSearch(java.util.Map)
	 * 周辺検索終了イベント
	 */
	@Override
	public void onEndAroundSearch(Map<Long , DC_POIInfoDetailData> mapData) {

// MOD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2) START
		if(mMapPOIDetail == null ||  mMeshList == null){
			return;
		}

        if( mlstContents == null ){
        	return;
        }
// ADD.2013.04.05 N.Sasao POI詳細情報取得失敗対応 START
        if(mapData == null){
        	return;
        }
// ADD.2013.04.05 N.Sasao POI詳細情報取得失敗対応  END
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
        if( mAsyncGetter == null ){
			return;
		}

		/* メッシュデータ生成正常とし、データ生成オフセットを更新する */
		mlstDataOffset = mAsyncGetter.GetlstDataOffset();
// ADD.2013.04.05 N.Sasao POI詳細情報取得失敗対応 START
		if( mlstDataOffset <= 0){
			// ここに来る場合はキャンセル処理が実行されているので、データは無視する。
			return;
		}
// ADD.2013.04.05 N.Sasao POI詳細情報取得失敗対応  END
// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
		NaviLog.v(NaviLog.PRINT_LOG_TAG, "DRIVE_CONTENTS onEndAroundSearch DataOffset = " + (mlstDataOffset-1));

		Map<Long , DC_POIInfoDetailData> wkmapData = new LinkedHashMap<Long , DC_POIInfoDetailData>(mapData);

		int nRequestType = 0;

		if( mStateType == STATE_TYPE_MESH ){
		// ADD.2013.04.05 N.Sasao AyncTask二重実行エラー対応 START
			mAsyncGetter = null;
		// ADD.2013.04.05 N.Sasao AyncTask二重実行エラー対応  END
			mStateType = STATE_TYPE_NONE;
			nRequestType = REQUEST_TYPE_REAROUND;
		}
		else{
			nRequestType = REQUEST_TYPE_REDRAW_ONLY;
		}

		Message msg = new Message();
		msg.what = EVENT_TYPE_REQUEST_MAP_UPDATE;
		msg.arg1 = nRequestType;
		msg.arg2 = 0;
		msg.obj = wkmapData;
		notifyDataManagerHandler(msg);
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END

	}
/// ADD/MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.model.DriveContentsAsyncDataGetter.onEndGetListener#onResultDetailPOIData(java.util.Map)
	 * POI詳細情報取得結果イベント
	 */
	@Override
	public void onResultDetailPOIData( DC_POIInfoDetailData poiDetailData ) {
		if( poiDetailData == null ){
			// 上位でToastを表示させる
		}
		else{
			poiDetailData.m_bDetailUpdate = true;

			synchronized(this){
				mMapPOIDetail.put(poiDetailData.m_POIInfo.m_POIId, poiDetailData);
			}
		}
		Message msg = new Message();
		msg.what = Constants.DC_POI_DETAIL_DATA;
		msg.arg1 = 0;
		msg.arg2 = 0;
		msg.obj = poiDetailData;
		notifyHandler(msg);

		if( mStateType == STATE_TYPE_POI_DETAIL ){
// ADD.2013.04.05 N.Sasao AyncTask二重実行エラー対応 START
			mStateType = STATE_TYPE_NONE;
			mAsyncGetter = null;
// ADD.2013.04.05 N.Sasao AyncTask二重実行エラー対応  END
		}
	}
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.model.DriveContentsAsyncDataGetter.onEndGetListener#onCancel(int)
	 * 非同期クラスのキャンセル時のイベント
	 */
	@Override
	public void onCancel(int nTask, Object obj) {
		switch(nTask){
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
		case DriveContentsAsyncDataGetter.READ_CONTENTS_LIST:
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
		case DriveContentsAsyncDataGetter.DOWNLOAD_MAIN:
			onEndData(null,null);    // イベントの通知
			return;
		case DriveContentsAsyncDataGetter.DOWNLOAD_MESH:
			/* キャンセル時は、メッシュ単位で生成出来たところまでのデータを反映させる */
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
			if( obj != null && mlstContents != null && !mlstContents.isEmpty() ){
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
				NaviLog.v(NaviLog.PRINT_LOG_TAG, "DRIVE_CONTENTS onCancel sendAroundMapData start" );

				Message msg = new Message();
				msg.what = EVENT_TYPE_REQUEST_MAP_UPDATE;
				msg.arg1 = REQUEST_TYPE_NONE;
				msg.arg2 = 0;
				msg.obj = obj;
			}
			break;
// MOD.2013.07.26 N.Sasao ドライブコンテンツキャンセル機能実装 START
		case DriveContentsAsyncDataGetter.DOWNLOAD_ORBIS_MESH:
			onEndOrbisData(null);
			break;
		case DriveContentsAsyncDataGetter.DOWNLOAD_POI_DETAIL:
			onResultDetailPOIData(null);
			break;
// MOD.2013.07.26 N.Sasao ドライブコンテンツキャンセル機能実装  END
		}
		// キャンセル後にGetterを再設定している場合があるので、ここは初期化しない
//		mAsyncGetter = null;
	}
// ADD/MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END

	/**
	 * 内部で動作する非同期オブジェクトの削除
	 */
	public void stopService() {
		if(mAsyncGetter != null){
			mAsyncGetter.cancel(true);
			mAsyncGetter = null;
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1) START
			mStateType = STATE_TYPE_NONE;
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1)  END
		}
	}

	/**
	 * 全サービスの終了
	 * キャッシュの削除
	 */
	public void release() {
		stopService();

		mStateType = STATE_TYPE_DELETE_FOLDER;
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
// MOD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
		DriveContentsAsyncDataGetter.DeleteFolder(null , DriveContentsAsyncDataGetter.FILETYPE_CONTENTS, mDataPath);
		DriveContentsAsyncDataGetter.DeleteFolder(null , DriveContentsAsyncDataGetter.FILETYPE_MESH_ALL, mDataPath);
//		DriveContentsAsyncDataGetter.DeleteFolder(null , DriveContentsAsyncDataGetter.FILETYPE_ALL);
// MOD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
		mStateType = STATE_TYPE_NONE;
	}

	/**
	 * @param strList
	 * @return
	 */
	public boolean setOrbisData(List<String> strList)	{
// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
		if(	mStateType == STATE_TYPE_CONTENTS ||
			mStateType == STATE_TYPE_DELETE_FOLDER ||
			mStateType == STATE_TYPE_POI_DETAIL ){
			return false;
		}
// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END

		if(mlstContents == null || mMapDetail == null){
			return false;
		}

		if(mAsyncGetter != null){
			mAsyncGetter.cancel(true);
			mAsyncGetter = null;
		}

        String userPath = ((NaviApplication)mParentActivity.getApplication()).getNaviAppDataPath().getUserDataPath();
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
		mAsyncGetter = new DriveContentsAsyncDataGetter(this , mObjLock, userPath, mDataPath);
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

		mAsyncGetter.SetTask(DriveContentsAsyncDataGetter.DOWNLOAD_ORBIS_MESH);
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
		synchronized(this){
			mAsyncGetter.SetContentsList( mlstContents , mMapDetail, mMapPOIDetail );
		}
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
		mAsyncGetter.SetOrbisMeshList(strList );
		mStateType = STATE_TYPE_ORBIS;
// ADD.2013.04.05 N.Sasao AyncTask二重実行エラー対応 START
		try{
			mAsyncGetter.execute();
		}catch(IllegalStateException e){
			// フェールセーフ
			NaviLog.e(NaviLog.PRINT_LOG_TAG, "setOrbisData execute error = " + e.toString() );
			return false;
		}
// ADD.2013.04.05 N.Sasao AyncTask二重実行エラー対応  END

		return true;
	}

	/**
	 * @return
	 */
	public List<DC_POIInfoDetailData> getOrbisList()	{
		if(mOrbisAlert){
			return mOrbisList;
		}
		return null;
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.model.DriveContentsAsyncDataGetter.onEndGetListener#onEndOrbisData(java.util.List)
	 */
	@Override
	public void onEndOrbisData(List<DC_POIInfoDetailData> list) {
		if(mOrbisList != null){
			mOrbisList = null;
		}

		if(list != null) {
			mOrbisList = new ArrayList<DC_POIInfoDetailData>(list);
		}
		Message msg = new Message();
		msg.what = Constants.DC_ORBIS_DATA_CHANGED;
		msg.arg1 = 0;
		msg.arg2 = 1;
		msg.obj = mOrbisList;
		notifyHandler(msg);

		if( mStateType == STATE_TYPE_ORBIS ){
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
			mStateType = STATE_TYPE_NONE;
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
// ADD.2013.04.05 N.Sasao AyncTask二重実行エラー対応 START
			mAsyncGetter = null;
// ADD.2013.04.05 N.Sasao AyncTask二重実行エラー対応  END
		}
	}
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1) START
	/** データマネージャの状態を返す
	 * @param なし
	 * @return DataManagerの状態
	 */
	public int GetDataManagerState(){
		return mStateType;
	}
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1) START

// ADD/MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
	/**
	 * 該当ドライブコンテンツデータ削除
	 * @param nDelOffset 不必要なドライブコンテンツデータのオフセット
	 * @return なし
	 */
	public void AppropriateDriveContetnsClear(){

		/* 異常系フェールセーフ1 */
		if( (null == mMapPOIDetail) || (null == mlstContents) ){
			NaviLog.e(NaviLog.PRINT_LOG_TAG, "AppropriateDriveContetnsClear failsafe1");
			return;
		}

		/* 空の場合、行う必要がないので即終了 */
		if( mMapPOIDetail.isEmpty() || mlstContents.isEmpty() ){
			NaviLog.i(NaviLog.PRINT_LOG_TAG, "AppropriateDriveContetnsClear none");
			return;
		}
// ADD.2013.04.02 N.Sasao 同一緯度経度にあったPOI情報は最初のデータを有効とする START
		long lLat = 0;
		long lLon = 0;
// ADD.2013.04.02 N.Sasao 同一緯度経度にあったPOI情報は最初のデータを有効とする  END

		/* 描画用のバッファから、今回更新対象となるコンテンツを検索し、削除する */
		Iterator<Entry<Long, DC_POIInfoDetailData>> it = mMapPOIDetail.entrySet().iterator();
		for ( ;; ) {
			if( !it.hasNext() ){
				break;
			}
			Entry<Long, DC_POIInfoDetailData> ent = it.next();

			int nId = ent.getValue().m_nContentsID;

			if( (nId >= 0) && (nId < mlstContents.size()) ){
				/* ユーザー選択可能なドライブコンテンツでユーザー選択されていないコンテンツデータを削除する */
				if( (mlstContents.get(nId).m_bLayering == false) &&
						(mlstContents.get(nId).m_bCheckItem == false) ){
					NaviLog.d(NaviLog.PRINT_LOG_TAG, "it removed case1 = " + ent.getValue().m_POIInfo.m_sPOIName );
					it.remove();
				}
// DEL.2013.06.14 N.Sasao POI情報リスト表示対応に伴い、同一緯度経度情報削除機能は無効とする START
//// ADD.2013.04.02 N.Sasao 同一緯度経度にあったPOI情報は最初のデータを有効とする START
//				else if( (mlstContents.get(nId).m_bLayering == false) &&
//						 (lLat == ent.getValue().m_POIInfo.m_lnLatitude1000) &&
//						 (lLon == ent.getValue().m_POIInfo.m_lnLongitude1000) ){
//					NaviLog.d(NaviLog.PRINT_LOG_TAG, "it removed case2 = " + ent.getValue().m_POIInfo.m_sPOIName );
//					it.remove();
//				}
//				else{
//					if( (lLat != ent.getValue().m_POIInfo.m_lnLatitude1000) &&
//						(lLon != ent.getValue().m_POIInfo.m_lnLongitude1000) ){
//						lLat = ent.getValue().m_POIInfo.m_lnLatitude1000;
//						lLon = ent.getValue().m_POIInfo.m_lnLongitude1000;
//					}
//				}
//// ADD.2013.04.02 N.Sasao 同一緯度経度にあったPOI情報は最初のデータを有効とする  END
// DEL.2013.06.14 N.Sasao POI情報リスト表示対応に伴い、同一緯度経度情報削除機能は無効とする  END
			}
			else{
				NaviLog.e(NaviLog.PRINT_LOG_TAG, "AllreadyDrawDriveContetnsClear failsafe2");
			}
		}
	}
	/**
	 * ドライブコンテンツデータ再生成要求
	 * @param なし
	 * @return なし
	 */
	public void requestRecreate(){
		mIsRequestRecreate = true;
	}
	/**
	 * ドライブコンテンツデータ再生成要求
	 * @param なし
	 * @return なし
	 */
	public boolean isRequestRecreate(){
		return mIsRequestRecreate;
	}

	/**
	 * ドライブコンテンツデータ作成結果(バッファリング＆結果応答)
	 * @param mapData 描画するドライブコンテンツ
	 * @return なし
	 * @note このメソッドはマルチで呼ばれる可能性あり。mMapPOIDetailの使用箇所は排他制御すること
	 */
	private void sendAroundMapData( Map<Long , DC_POIInfoDetailData> mapData, int nRequestCode ){
        if( mapData == null ){
        	return;
        }
// MOD.2013.03.28 N.Sasao ドライブコンテンツ切り替え時に再描画処理を行う START
		if( !mMapPOIDetail.equals(mapData) || (nRequestCode == REQUEST_TYPE_REDRAW_ONLY) ){
// MOD.2013.03.28 N.Sasao ドライブコンテンツ切り替え時に再描画処理を行う  END
        	mMapPOIDetail.clear();
        	mMapPOIDetail = new LinkedHashMap<Long , DC_POIInfoDetailData>(mapData);
        	/* 作成要求を出していないメッシュ番号(描画不要)のデータを削除する */
        	RemoveNoRequestMeshData();
        	/* 表示から非表示選択に変更された(ユーザー選択解除した)コンテンツを削除する */
// MOD.2013.04.02 N.Sasao 同一緯度経度にあったPOI情報は最初のデータを有効とする START
        	AppropriateDriveContetnsClear();
// MOD.2013.04.02 N.Sasao 同一緯度経度にあったPOI情報は最初のデータを有効とする  END

			List<DC_POIInfoData> poiList = new ArrayList<DC_POIInfoData>();

// MOD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない START
	    	// ユーザー選択コンテンツの表示制限
	        boolean contentsVaildInfo = false;

			// 表示している縮尺を取得
			JNILong lnDistance = new JNILong();
	        NaviRun.GetNaviRunObj().JNI_Java_GetScaleDistance(lnDistance);

	        // 詳細地図の時
		    if( lnDistance.getLcount() < 50 ){
		    	contentsVaildInfo = true;
		    }

		    List<DC_POIInfoDetailData> sortMapPOIDetail = new ArrayList<DC_POIInfoDetailData>();

	    	int nMeshCnt = 0;

		    if( (mMeshList != null) && !mMeshList.isEmpty() ){
			    nMeshCnt = mMeshList.size();

			    // メッシュリストと同じソート順番にする
			    for( int nCnt = 0; nCnt < mMeshList.size(); nCnt++ ){
		    		Iterator<Entry<Long , DC_POIInfoDetailData>> it = mMapPOIDetail.entrySet().iterator();
					for ( ;; ) {
						if( !it.hasNext() ){
							break;
						}

						Entry<Long, DC_POIInfoDetailData> ent = it.next();

						/* 保持しているデータのメッシュID取得 */
						long lMeshID = ent.getValue().m_POIInfo.m_lMeshNum;

						if( lMeshID == mMeshList.get(nCnt) ){
							sortMapPOIDetail.add(ent.getValue());
						}
					}
				}
		    }
		    else{
		    	contentsVaildInfo = true;
		    }
		    int intArray[] = new int[nMeshCnt];

		    for( int nCnt = 0; nCnt < sortMapPOIDetail.size(); nCnt++ ){
	            int nId = sortMapPOIDetail.get(nCnt).m_nContentsID;

	            if( (nId >= 0) && (nId < mlstContents.size()) ){
	            	// 表示必須データは無条件格納
					if( (mlstContents.get(nId).m_bLayering == true) || (contentsVaildInfo == true) ){
		            	poiList.add( sortMapPOIDetail.get(nCnt).m_POIInfo );
					}
					// ユーザー選択したコンテンツは、既定数以下だけ格納する(縮尺50mより下は無条件格納)
					else if( (mlstContents.get(nId).m_bLayering == false) &&
							(mlstContents.get(nId).m_bCheckItem == true) ){

						int nIndex = 0;

						int nMesuNum = (int)sortMapPOIDetail.get(nCnt).m_POIInfo.m_lMeshNum;

						if( contentsVaildInfo == true ){
							poiList.add( sortMapPOIDetail.get(nCnt).m_POIInfo );
						}
						else if( mMeshList.contains( nMesuNum ) ){
							nIndex = mMeshList.indexOf( nMesuNum );

							if( intArray[nIndex] < POIDATA_CULL_CHECK ){
								poiList.add( sortMapPOIDetail.get(nCnt).m_POIInfo );
								intArray[nIndex]++;
							}
						}
						else{
							NaviLog.e(NaviLog.PRINT_LOG_TAG, "DRIVE_CONTENTS mMeshList index errordata = " + sortMapPOIDetail.get(nCnt).m_POIInfo.m_sPOIName );
						}
					}
					else{
						NaviLog.e(NaviLog.PRINT_LOG_TAG, "DRIVE_CONTENTS poiList errordata = " + sortMapPOIDetail.get(nCnt).m_POIInfo.m_sPOIName );
					}
	            }
		    }
// MOD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  END
// MOD.2013.07.26 N.Sasao Bug #14497 ドライブコンテンツの表示を全てオフにしてもアイコンが表示されている START
	        if(poiList.size() >= 0){
// MOD.2013.07.26 N.Sasao Bug #14497 ドライブコンテンツの表示を全てオフにしてもアイコンが表示されている  END
				Message msg = new Message();
				msg.what = Constants.DC_AROUND_SEARCH;
				msg.arg1 = 0;
				msg.arg2 = 1;
				msg.obj = poiList;
				notifyHandler(msg);

				long end = System.currentTimeMillis();
				NaviLog.v(NaviLog.PRINT_LOG_TAG, "DRIVE_CONTENTS sendAroundMapData send datacnt = " + poiList.size() + " endtime = " + end );
	        }
		}

		switch(nRequestCode){
			case REQUEST_TYPE_REAROUND:
				/* POIデータが最終データに到達していない場合、再度実行 */
		        if( mlstDataOffset < mlstContents.size() ){
		        	aroundSearch(mMeshList);
		        }
		        else{
// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
		        	if( mStateType == STATE_TYPE_MESH ){
		        		mlstDataOffset = 0;
		        		mStateType = STATE_TYPE_NONE;
		        	}
// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
		        }
		        break;
			case REQUEST_TYPE_NONE:
// ADD.2013.03.28 N.Sasao ドライブコンテンツ切り替え時に再描画処理を行う START
			case REQUEST_TYPE_REDRAW_ONLY:
// ADD.2013.03.28 N.Sasao ドライブコンテンツ切り替え時に再描画処理を行う  END
		    default:
		    	break;
		}
	}

	/**
	 * ドライブコンテンツイベントハンドラ
	 * @param なし
	 * @return なし
	 */
	private void dataManagerEventHandler(){
		mDataManagerHandler = new Handler() {
            @SuppressWarnings("unchecked")
			public void handleMessage(final Message msg) {
            	if( msg == null ){
            		return;
            	}
            	synchronized(this){
	                switch (msg.what) {
		                // ドライブコンテンツデータの描画データ送信
	// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
		                case DriverContentsDataManager.EVENT_TYPE_REQUEST_MAP_UPDATE:
	// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
		                	sendAroundMapData( (Map<Long , DC_POIInfoDetailData>)msg.obj, msg.arg1 );
		        			break;
		               	default:
		               		break;
	                }
            	}
            }
    	};
	}

    /**
     * リクエストされなかったメッシュデータのコンテンツを削除する
     * @return なし
     */
    private void RemoveNoRequestMeshData(){
    	long lMeshID;
    	Integer nInt;

		/* 異常系フェールセーフ1 */
		if( (null == mMapPOIDetail) || (null == mMeshList) ){
			NaviLog.e(NaviLog.PRINT_LOG_TAG, "RemoveNoRequestMeshData failsafe1");
			return;
		}

		/* 空の場合、行う必要がないので即終了 */
		if( mMapPOIDetail.isEmpty() || mMeshList.isEmpty() ){
			NaviLog.i(NaviLog.PRINT_LOG_TAG, "RemoveNoRequestMeshData none");
			return;
		}

    	/* 描画用のバッファから、今回リクエストしていないコンテンツを検索し、削除する */
		Iterator<Entry<Long, DC_POIInfoDetailData>> it = mMapPOIDetail.entrySet().iterator();
		for ( ;; ) {
			if( !it.hasNext() ){
				break;
			}

			Entry<Long, DC_POIInfoDetailData> ent = it.next();

			/* 保持しているデータのメッシュID取得 */
			lMeshID = ent.getValue().m_POIInfo.m_lMeshNum;
			nInt = (int) lMeshID;
			/* リクエストしていない情報を保持しているときは、保持情報から持ってる情報を削除する */
			if( mMeshList.contains(nInt) != true ){
				NaviLog.d(NaviLog.PRINT_LOG_TAG, "RemoveNoRequestMeshData it removed Mesh ID is not request = " + lMeshID);
				it.remove();
			}
		}
    }
// ADD/MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
// ADD.2013.04.03 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない START
    /**
     * 現状の最新データで再描画を要求する
     * @return なし
     */
    public void cacheRedrawPOIInfo(){
    	Map<Long,DC_POIInfoDetailData>	wkMapPOIDetail;
		/* 現状の最新データで再描画を要求 */
    	synchronized(this){
    		wkMapPOIDetail = new LinkedHashMap<Long , DC_POIInfoDetailData>(mMapPOIDetail);
    	}
		Message msg = new Message();
		msg.what = EVENT_TYPE_REQUEST_MAP_UPDATE;
		msg.arg1 = REQUEST_TYPE_REDRAW_ONLY;
		msg.arg2 = 0;
		msg.obj = wkMapPOIDetail;
		notifyDataManagerHandler(msg);
    }
// ADD.2013.04.03 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  END
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
    /**
     * ドライブコンテンツの初期化を行う。
     * アイコンやメッシュデータの削除は、初回取得のドリコンシーケンスに任せる
     * @return なし
     */
    public void cacheDriconDataClear(){
    	stopService();

		synchronized(this){
    		if( mlstContents != null ){
    	    	mlstContents.clear();
    		}
    		if( mMapDetail != null ){
    	    	mMapDetail.clear();
    		}
    		if( mMapPOIDetail != null ){
    	    	mMapPOIDetail.clear();
    		}
    		if( mOrbisList != null ){
    	    	mOrbisList.clear();
    		}
    		if( mMeshList != null ){
    	    	mMeshList.clear();
    		}

    		if( mOrbisPOIDetail != null ){
    	    	mOrbisPOIDetail.clear();
    		}

	    	mlstContents = null;
	    	mMapDetail = null;
	    	mMapPOIDetail = new LinkedHashMap<Long , DC_POIInfoDetailData>();
	    	mOrbisList = null;
	    	mMeshList = null;
	    	mOrbisPOIDetail = null;

	    	// ドライブコンテンツ描画初期化要求
	    	List<DC_POIInfoData> poiList = new ArrayList<DC_POIInfoData>();

			Message msg = new Message();
			msg.what = Constants.DC_AROUND_SEARCH;
			msg.arg1 = 0;
			msg.arg2 = 1;
			msg.obj = poiList;
			notifyHandler(msg);

            // 暗号保存しているドライブコンテンツリストを削除
			String driconPath = mDataPath +
					DriveContentsAsyncDataGetter.GetContentsOpposingPath() +
					CIPHER_FILE_NAME;

			File driconFile = new File(driconPath);
			if( driconFile != null ){
				if( driconFile.exists() ){
// MOD.2013.07.22 N.Sasao お知らせ機能実装 START
					deleteFile( driconFile );
// MOD.2013.07.22 N.Sasao お知らせ機能実装  END
				}
			}
			// ドライブコンテンツのユーザーデータを削除
	        String userPath = ((NaviApplication)mParentActivity.getApplication()).getNaviAppDataPath().getUserDataPath();
			DriveContentsAsyncDataGetter.DeleteFolder(null , DriveContentsAsyncDataGetter.FILETYPE_USER_SETTING, userPath);

            // 暗号・複合キーも作り直す
			if( oCipher != null ){
				oCipher.prefKeyUpdate();
			}
    	}
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
    }
// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
    /**
     * ドライブコンテンツ暗号化ファイル名を返す。
     * @return ファイル名
     */
    public static String GetCipherContentsName(){
    	return CIPHER_FILE_NAME;
    }

    /**
     * ファイルやフォルダを削除する(再帰処理)
     * @param 削除するディレクトリ名かファイル名
     * @return 削除結果を返す
     */
	public static boolean deleteFile(File dirOrFile) {
	    if (dirOrFile.isDirectory()) {//ディレクトリの場合
	        String[] children = dirOrFile.list();//ディレクトリにあるすべてのファイルを処理する
	        for (int i=0; i<children.length; i++) {
	            boolean success = deleteFile(new File(dirOrFile, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }
		return dirOrFile.delete();
	}
// ADD.2013.07.22 N.Sasao お知らせ機能実装  END
}