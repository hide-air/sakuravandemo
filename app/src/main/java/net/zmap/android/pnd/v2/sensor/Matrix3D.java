package net.zmap.android.pnd.v2.sensor;

public class Matrix3D {
	final public static int AXIS_X = 0;
	final public static int AXIS_Y = 1;
	final public static int AXIS_Z = 2;

	private double[][] mRotationMatrixX = new double[4][4];
	private double[][] mRotationMatrixY = new double[4][4];
	private double[][] mRotationMatrixZ = new double[4][4];
	private double[][] mMultMatrix = new double[4][4];

	public Matrix3D() {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				mRotationMatrixX[i][j] = 0;
				mRotationMatrixY[i][j] = 0;
				mRotationMatrixZ[i][j] = 0;
				mMultMatrix[i][j] = 0;
			}
		}
	}

	public void makeRotationMatrix(double[][] matrix, double rad, int axisType) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				matrix[i][j] = 0;
			}
		}
		switch (axisType) {
		case AXIS_X:
			matrix[0][0] = 1;
			matrix[1][1] = Math.cos(rad);
			matrix[1][2] = -Math.sin(rad);
			matrix[2][1] = Math.sin(rad);
			matrix[2][2] = Math.cos(rad);
			matrix[3][3] = 1;
			break;
		case AXIS_Y:
			matrix[0][0] = Math.cos(rad);
			matrix[0][2] = Math.sin(rad);
			matrix[1][1] = 1;
			matrix[2][0] = -Math.sin(rad);
			matrix[2][2] = Math.cos(rad);
			matrix[3][3] = 1;
			break;
		case AXIS_Z:
			matrix[0][0] = Math.cos(rad);
			matrix[0][1] = -Math.sin(rad);
			matrix[1][0] = Math.sin(rad);
			matrix[1][1] = Math.cos(rad);
			matrix[2][2] = 1;
			matrix[3][3] = 1;
			break;
		}
	}

    public void product(double a[][], double b[][], double c[][], int n) {
    	for (int i = 0; i < n; i++) {
    		for (int j = 0; j < n; j++) {
    			for (int k = 0; k < n; k++) {
    				c[i][j] += a[i][k] * b[k][j];
    			}
    		}
    	}
    }

    public void invert(float baseX, float baseY, float baseZ) {
    	makeRotationMatrix(mRotationMatrixX, baseX, AXIS_X);	// yaw
    	makeRotationMatrix(mRotationMatrixY, baseY, AXIS_Y);	// roll
    	makeRotationMatrix(mRotationMatrixZ, baseZ, AXIS_Z);	// pitch
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				mMultMatrix[i][j] = 0;
			}
		}
		product(mRotationMatrixX, mRotationMatrixY, mMultMatrix, 4);
    }

    public float[] convert(float x, float y, float z) {
    	float values[] = new float[3];
    	double[][] rotationMatrixX = new double[4][4];
    	double[][] multMatrix = new double[4][4];

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				rotationMatrixX[i][j] = mMultMatrix[i][j];
				multMatrix[i][j] = 0;
			}
		}
		product(rotationMatrixX, mRotationMatrixZ, multMatrix, 4);

		for (int i = 0; i < 3; i++) {
			values[i] = (float)(x * multMatrix[i][0] + y * multMatrix[i][1] + z * multMatrix[i][2] + multMatrix[i][3]);
		}

		return values;
    }
};
