/**
 ********************************************************************
 * Copyright (c) 2012.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project        MarketV2
 * File           SelectDrivingRegulation.java
 * Description    車載器連携切換画面
 * Created on     2013/01/07
 *
 ********************************************************************
 */

package net.zmap.android.pnd.v2.common.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.DrivingRegulation;
import net.zmap.android.pnd.v2.common.utils.ScrollBox;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.SwitchGroupEx;
import net.zmap.android.pnd.v2.common.view.SwitchGroupListener;


/**
 * Created on 2012/12/25
 * <p>
 * Title: 車載器連携切換画面
 * <p>
 * Description
 * <p>
 * author　N.Sasao
 * version 1.0
 */
public class SelectDrivingRegulation extends MenuBaseActivity {
    private LinearLayout oLayout;

    private SwitchGroupEx switchDrivingRegulation;
	/**
	 * 初期化処理
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        LayoutInflater oInflater = LayoutInflater.from(this);
        oLayout = (LinearLayout)oInflater.inflate(R.layout.setting_select_driving_regulation, null);
        ScrollBox oBox = (ScrollBox)oLayout.findViewById(R.id.setting_contents_area);

        switchDrivingRegulation = (SwitchGroupEx)oLayout.findViewById(R.id.switchDrivingRegulation);
        switchDrivingRegulation.setListener(new SwitchGroupListener() {
            @Override
            public void onSelectChange(int wSelect)
            {
                switch (wSelect) {
                	// 連携なし
                	case DrivingRegulation.DEF_VALUE_GUIDE_NONE:
                    	// 走行規制中
                    	if(DrivingRegulation.CheckDrivingRegulation()){
                    		return;
                    	}
                    	// 外部API走行規制権限譲渡中
                    	if( DrivingRegulation.IsDrivingControlLock() ){
                    		showDialog( Constants.DIALOG_DURING_EXTERNAL_CONTROL );
                    		return;
                    	}

        	            DrivingRegulation.SetDrivingRegulationGuide( DrivingRegulation.DEF_VALUE_GUIDE_NONE );
        	            // サービスのバインド状態を更新
                    	DrivingRegulation.updateScreenActivity( SelectDrivingRegulation.this );
        	            DrivingRegulation.UnBind();

                		break;
               		// SmartAccess連携
                	case DrivingRegulation.DEF_VALUE_GUIDE_CLARION:
                    	// 走行規制中
                    	if( DrivingRegulation.CheckDrivingRegulation() ){
                    		return;
                    	}
                    	// 外部API走行規制権限譲渡中
                    	if( DrivingRegulation.IsDrivingControlLock() ){
                    		showDialog( Constants.DIALOG_DURING_EXTERNAL_CONTROL );
                    		return;
                    	}
                    	// 該当サービス環境が整っていない
                    	if( !DrivingRegulation.IsReadyEnv( DrivingRegulation.DEF_VALUE_GUIDE_CLARION ) ){
                    		showDialog( Constants.DIALOG_NOT_ALREADY_ENVIRONMENT );
                    		// 連携なしを設定
                    		switchDrivingRegulation.setSelected( DrivingRegulation.DEF_VALUE_GUIDE_NONE );
                    		switchDrivingRegulation.getView( DrivingRegulation.DEF_VALUE_GUIDE_CLARION ).setVisibility(View.GONE);
                    		return;
                    	}

                    	DrivingRegulation.SetDrivingRegulationGuide( DrivingRegulation.DEF_VALUE_GUIDE_CLARION );
                    	DrivingRegulation.updateScreenActivity( SelectDrivingRegulation.this );
                		// サービスのバインド状態を更新
                        DrivingRegulation.Bind();
                		break;
/******************************************************/
/* 新規車載器連携サービス追加時は、ボタン処理の追加を */
/******************************************************/
                	default:
                		break;
                }
            }
        });

/****************************************************/
/* 新規車載器連携サービス追加時は、環境確認の追加を */
/****************************************************/
        // 画面表示時点で、該当サービス環境が整ってない場合は、非表示情報を設定する
        if( !DrivingRegulation.IsReadyEnv( DrivingRegulation.DEF_VALUE_GUIDE_CLARION ) ){
        	switchDrivingRegulation.getView( DrivingRegulation.DEF_VALUE_GUIDE_CLARION ).setVisibility(View.GONE);
        }

        // 設定値反映
        int nSelect = DrivingRegulation.GetDrivingRegulationGuide();
        // 設定を反映させる機能が動作可能の環境にあるとき
        if( switchDrivingRegulation.getView( nSelect ).getVisibility() != View.GONE ){
        	switchDrivingRegulation.setSelected( nSelect );
        }
        else{
        	// 環境が整っていないため、連携なしとする
        	// 外部API連携中は前の画面で遷移制御しているので、ここではしない
        	DrivingRegulation.SetDrivingRegulationGuide( DrivingRegulation.DEF_VALUE_GUIDE_NONE );
            // サービスのバインド状態を更新
        	DrivingRegulation.updateScreenActivity( SelectDrivingRegulation.this );
            DrivingRegulation.UnBind();
            switchDrivingRegulation.setSelected( DrivingRegulation.DEF_VALUE_GUIDE_NONE );
        }

		ScrollTool oTool = new ScrollTool(this);
        oTool.bindView(oBox);

        setViewInOperArea(oTool);
        setViewInWorkArea(oLayout);
	}

	/**
	* Created on 2013/12/25
	* Title:       onKeyDown
	* Description:  onKeyDownの処理
	* @param1  keyCode キーコードを押す
	* @param2  event イベント
	* @return 		boolean
	* @author         N.Sasao
	* @version        1.0
	*/
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}
