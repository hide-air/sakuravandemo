package net.zmap.android.pnd.v2.vics;

import android.os.AsyncTask;

import net.zmap.android.pnd.v2.common.http.HttpUtils;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;


public class VicsAsyncDataGetter extends AsyncTask<Void, Void, Boolean> {

    private VicsListener m_listener;

    // 接続試行回数
    static private final int MAX_CONN_TRIAL = 3;

    // タイムアウトまでの時間 (10000ミリ秒)
    static private final int GET_DATA_NETWORK_TIMEOUT_INTERVAL = 10000;

    // 出力ファイル
    static private final String PATH_GETVICS = "/Tmp/getvics";

    HttpUtils httpUtils = null;

    /**
     * コンストラクタ
     *
     * @param context
     * @param listener
     */
    public VicsAsyncDataGetter(VicsListener listener) {
        m_listener = listener;
    }


    /** 非同期処理開始
     *
     */
    @Override
    protected Boolean doInBackground(Void... params) {
        return GetTrafficDataFromNetwork();
    }


    /** Vics データ取得中断
     *
     */
    @Override
    protected void onCancelled() {

        if (httpUtils != null ) {
            httpUtils.shutdown();
        }

        m_listener.onCancel(null);

        return;
    }


    /** Vics データ取得完了
     *
     */
    @Override
    protected void onPostExecute(Boolean result) {

        if(result == null) {
            result = false;
        }
        if (result) {
            if (httpUtils != null ) {

                // getvics ファイル書き出し
                httpUtils.deleteFile(PATH_GETVICS);
                httpUtils.saveFile2SD(PATH_GETVICS);

                // 取得成功
                if (m_listener != null) {
                    m_listener.onFinish(null);
                }
            }
        } else {
            if (m_listener != null) {
                // 取得失敗
                m_listener.onError(null);
            }
        }

        if (httpUtils != null ) {
            httpUtils.shutdown();
        }

        return;
    }


    /** GETVICS ファイル削除
     *
     */
    static public void DeleteGetVicsFile()
    {
        HttpUtils httpUtils = new HttpUtils("");
        httpUtils.deleteFile(PATH_GETVICS);
    }


    /** VICSデータ取得
     *
     * vsevtdrv.c より移植
     *
     * @return 取得に成功した場合、true
     */
    private boolean GetTrafficDataFromNetwork()
    {
        boolean bRet = false;
        int i = 2;  // 隣接する2次メッシュまで取得

        int iCenMeshNo, iMeshNoMin, iMeshNoMax;

        // 自車位置取得
        JNITwoLong carPos = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(carPos);

        iCenMeshNo = vs_ulGetMesh2Code(carPos);

        iMeshNoMin = GetMeshNoMin(i, iCenMeshNo);
        iMeshNoMax = GetMeshNoMax(i, iCenMeshNo);

        bRet = GetTrafficData(iMeshNoMin, iMeshNoMax);
        if (!bRet)
        {
            return false;
        }
        else
        {
            NaviLog.i("VicsAsyncDataGetter", "RegistTrafficData");
        }

        return true; /*vics0007*/
    }

    /** 1次メッシュコード取得
     *
     * vsevtdrv.c より移植
     *
     * @return 作成成功の場合、true。
     */
    private int vs_ulGetMesh2Code (JNITwoLong carPos)
    {
        long lLongitude;                                            /* 経度                       */
        long lLatitude;                                             /* 緯度                       */
        int iMeshX;                                                 /* １次メッシュコードＸ       */
        int iMeshY;                                                 /* １次メッシュコードＹ       */
        int iMesh2X;                                                /* ２次メッシュコードＸ       */
        int iMesh2Y;                                                /* ２次メッシュコードＹ       */
        int i2ndMeshCode;                                           /* ２次メッシュコード        */

        final long LOCATION_TO_MESHY_ZDC = 2400000;
        final long LOCATION_TO_MESHX_ZDC = 3600000;

        /* 絶対経緯度の取得     */
        lLongitude = carPos.getM_lLong();
        lLatitude = carPos.getM_lLat();

        /* １次メッシュコード算出  */

        iMeshY = (int)(lLatitude  / LOCATION_TO_MESHY_ZDC );
        iMeshX = (int)(lLongitude / LOCATION_TO_MESHX_ZDC )-100;

        iMesh2Y = (int)((lLatitude  - (iMeshY)      *LOCATION_TO_MESHY_ZDC)*8/LOCATION_TO_MESHY_ZDC);
        iMesh2X = (int)((lLongitude - (iMeshX + 100)*LOCATION_TO_MESHX_ZDC)*8/LOCATION_TO_MESHX_ZDC);

        /* ２次メッシュコード生成  */
        i2ndMeshCode =iMeshY*10000 +iMeshX*100+ iMesh2Y*10+iMesh2X;

        return  i2ndMeshCode;
    }


    /** 自車位置周辺のメッシュ番号最小値を取得
     *
     * @param i メッシュタイプ
     * @param aucMeshNo 自車位置メッシュ番号
     */
    int GetMeshNoMin(int i, int iMeshNo)
    {
        int iMeshX;                                                 /* １次メッシュコードＸ       */
        int iMeshY;                                                 /* １次メッシュコードＹ       */
        int iMesh2X;                                                /* ２次メッシュコードＸ       */
        int iMesh2Y;                                                /* ２次メッシュコードＹ       */

        int itempMeshX;                                                 /* １次メッシュコードＸ       */
        int itempMeshY;                                                 /* １次メッシュコードＹ       */
        int itempMesh2X;                                                /* ２次メッシュコードＸ       */
        int itempMesh2Y;                                                /* ２次メッシュコードＹ       */

        iMesh2X = (int)(iMeshNo%10);
        iMesh2Y = (int)((iMeshNo%100)/10);
        iMeshX  = (int)((iMeshNo%10000)/100);
        iMeshY  = (int)((iMeshNo%1000000)/10000);

        itempMesh2X = iMesh2X -i+2*8;
        itempMesh2Y = iMesh2Y -i+2*8;

        itempMeshX = iMeshX + (int)(itempMesh2X/8)-2;
        itempMeshY = iMeshY + (int)(itempMesh2Y/8)-2;

        itempMesh2X = itempMesh2X%8;
        itempMesh2Y = itempMesh2Y%8;

        return (itempMeshY*10000 +itempMeshX*100+ itempMesh2Y*10+itempMesh2X);
    }


    /** 自車位置周辺のメッシュ番号最大値を取得
     *
     * @param i メッシュタイプ
     * @param aucMeshNo 自車位置メッシュ番号
     */
    int GetMeshNoMax(int i, int iMeshNo)
    {
        int iMeshX;                                                 /* １次メッシュコードＸ       */
        int iMeshY;                                                 /* １次メッシュコードＹ       */
        int iMesh2X;                                                /* ２次メッシュコードＸ       */
        int iMesh2Y;                                                /* ２次メッシュコードＹ       */

        int itempMeshX;                                                 /* １次メッシュコードＸ       */
        int itempMeshY;                                                 /* １次メッシュコードＹ       */
        int itempMesh2X;                                                /* ２次メッシュコードＸ       */
        int itempMesh2Y;                                                /* ２次メッシュコードＹ       */

        iMesh2X = (int)(iMeshNo%10);
        iMesh2Y = (int)((iMeshNo%100)/10);
        iMeshX  = (int)((iMeshNo%10000)/100);
        iMeshY  = (int)((iMeshNo%1000000)/10000);

        itempMesh2X = iMesh2X +i;
        itempMesh2Y = iMesh2Y +i;

        itempMeshX = iMeshX + (int)(itempMesh2X/8);
        itempMeshY = iMeshY + (int)(itempMesh2Y/8);

        itempMesh2X = itempMesh2X%8;
        itempMesh2Y = itempMesh2Y%8;

        return (itempMeshY*10000 +itempMeshX*100+ itempMesh2Y*10+itempMesh2X);
    }


    /** getvics.cgi アクセス
     *
     */
    private boolean GetTrafficData(int aucMeshNoMin, int aucMeshNoMax)
    {
//        int j = 0;
//
//        // getvics.cgi アクセス用 URL を作成
//        httpUtils = MakeHttpUtils(aucMeshNoMin, aucMeshNoMax);
//
//        for (j = 0; j < MAX_CONN_TRIAL; j++)
//        {
//            if (isCancelled()) {
//                break;
//            }
//
//            if (httpUtils != null && httpUtils.httpGet()) {
//                // VICS データ取得
//                return true;
//            }
//        }

        return  false;
    }


    /** getvics.cgi アクセス用 URL 作成
     *
     */
    private HttpUtils MakeHttpUtils(int iMeshNoMin , int iMeshNoMax)
    {
        String strVicsRequestUrlHeader = "http://210.133.109.190:80/cgi-bin/msrs/getvics.cgi";

        String strVicsRequestCid = "PND";
        String strVicsRequestSid = "PND";
        String strVicsRequestAid = "AKfdAFIUVOrGOMbzxkk54VIExuIKBrra";

        int iVicsRequestLD2 = iMeshNoMin;
        int iVicsRequestRU2 = iMeshNoMax;
        String strVicsRequestOdt = "0xFFFF3C";
        String strVicsRequestBo  = "L";
        String strVicsRequestEdt = "7";

        // URL 設定
        httpUtils = new HttpUtils(strVicsRequestUrlHeader);

        httpUtils.addParam("cid", strVicsRequestCid);
        httpUtils.addParam("sid", strVicsRequestSid);
        httpUtils.addParam("aid", strVicsRequestAid);
        httpUtils.addParam("bo" , strVicsRequestBo);
        httpUtils.addParam("ld2", "" + iVicsRequestLD2);
        httpUtils.addParam("ru2", "" + iVicsRequestRU2);
        httpUtils.addParam("odt", strVicsRequestOdt);
        httpUtils.addParam("edt", strVicsRequestEdt);

        // タイムアウトまでの時間
        httpUtils.setConnectionTimeout(GET_DATA_NETWORK_TIMEOUT_INTERVAL);
        httpUtils.setReadTimeout(GET_DATA_NETWORK_TIMEOUT_INTERVAL);

        return	httpUtils;
    }
};