package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Mybox_ListGenreCount;
import net.zmap.android.pnd.v2.data.POI_UserFile_AroundRec;
import net.zmap.android.pnd.v2.data.POI_UserFile_RouteRec;
import net.zmap.android.pnd.v2.data.ZPOI_Route_Param;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.inquiry.view.ItemView;
import net.zmap.android.pnd.v2.maps.OpenMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 周辺検索のお気に入り（K-F0_お気に入りジャンル一覧）
 *
 * @author XuYang
 *
 */
public class AroundFavoritesGenreInquiry extends InquiryBaseLoading {
    private JNITwoLong Coordinate = new JNITwoLong();
    protected Intent oMyIntent = null;
    protected ScrollList oList = null;
    private ScrollTool oTool = null;
    private POI_UserFile_AroundRec[] m_Poi_UserFile_Listitem = null;
    private POI_UserFile_RouteRec[] m_Poi_UserFile_RouteListitem = null;
    private List<POI_UserFile_AroundRec> aroundList = new ArrayList<POI_UserFile_AroundRec>();
    private List<POI_UserFile_RouteRec> aroundRouteList = new ArrayList<POI_UserFile_RouteRec>();
    /** グループタイプの件数 */
    private JNILong RecCount = new JNILong();
    private POI_Mybox_ListGenreCount[] CountArray;
    private POI_Mybox_ListGenreCount[] CountRouteArray;
//    private final long MAX_COUNT = 30;
    private TextView oText = null;
    private int KIND_OF_FRIEND = 0;
    private int KIND_OF_EAT = 1;
    private int KIND_OF_SHOPPING = 2;
    private int KIND_OF_INTEREST = 3;
    private int KIND_OF_SIGHTSEEING = 4;
    private int KIND_OF_WORK = 5;
    private int KIND_OF_OTHERS = 6;
    private int KIND_OF_HOUSE = 7;
// Add 2011/11/18 Z01_h_yamada Start -->
    private int KIND_OF_HOUSE2 = 8;
// Add 2011/11/18 Z01_h_yamada Start -->

    private String sTitle = "";
    private List<Button> menu_button = null;
    private Button btnItem = null;
    private int selectFavoritesId = 0;
    private final int[] favoritesName = new int[] { R.id.Btn_Group_friend,// 友達親戚
            R.id.Btn_Group_eat, // 食事
            R.id.Btn_Group_shopping, // 買物
            R.id.Btn_Group_interest,// 趣味
            R.id.Btn_Group_sightseeing,// 観光
            R.id.Btn_Group_work,// 仕事
            R.id.Btn_Group_other,// その他
            R.id.Btn_Group_myhome,// 自宅
    };
    private final int[] favoritesStringName = new int[] {
            R.string.genre_title_friend, R.string.genre_title_eat,
            R.string.genre_title_shopping, R.string.genre_title_interest,
            R.string.genre_title_sightseeing, R.string.genre_title_work,
            R.string.genre_title_other, R.string.genre_title_myhome };
    private final int[] favoritesId = new int[] { KIND_OF_FRIEND, KIND_OF_EAT,
            KIND_OF_SHOPPING, KIND_OF_INTEREST, KIND_OF_SIGHTSEEING,
            KIND_OF_WORK, KIND_OF_OTHERS, KIND_OF_HOUSE };
    private boolean mbAroundFlg = false; // 沿いルート周辺と施設周辺のフラグ true:沿いルート周辺
    private static AroundFavoritesGenreInquiry instance;
    ItemView oview=null;
    private int recordCount = 0;
 // Add 2011/10/03 katsuta Start -->
	private  int  SearchCacheNum = 0;
// Add 2011/10/03 katsuta End <--

    /** 0:初期化　1：ジャンルタイプ */
	private int waitDialogType = 0;
	private int GENRE_BUTTON = 1;
	private int DEFAULT_SORT_BUTTON = 0;

    /** 現在の接続リクエストのid */
    protected String m_sReqId = null;
	//<--
//Add 2011/06/09 Z01thedoanh Start -->
    private final long INIT_LAYOUT_BIGSHOW = 0;		//大分類結果なしの時はグレーボタン数を表示
//Add 2011/06/09 Z01thedoanh End <--

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 沿いルート周辺と施設周辺のフラグを取得
        mbAroundFlg = CommonLib.isAroundFlg();
        instance = this;

        oMyIntent = getIntent();


        sTitle = oMyIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
        //RecCountの内容は削除されず、画面の遷移のとき、該当内容をリセットする

        if (AppInfo.isEmpty(sTitle)) {
            NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
            initRecordCount();


            initDialog();
        } else {
        	initRecordCount();
            recordCount = oMyIntent.getIntExtra(Constants.PARAMS_RECORD_COUNT, 0);
            waitDialogType = DEFAULT_SORT_BUTTON;
			this.startShowPage(NOTSHOWCANCELBTN);
        }

    }


    private void initRecordCount() {
    	NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetGenreCount(RecCount);
    	recordCount = (int)RecCount.lcount;
	}


	private void initDialog() {
//Del 2011/09/17 Z01_h_yamada Start -->
//		int titleId = oMyIntent.getIntExtra(Constants.PARAMS_TITLE, 0);
//        if (0 != titleId) {
//            this.setMenuTitle(titleId);
//        }
//Del 2011/09/17 Z01_h_yamada End <--

         LayoutInflater oInflater = LayoutInflater.from(this);
         LinearLayout oLayout = (LinearLayout) oInflater.inflate(
                 R.layout.inquiry_base, null);
         Button obtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
         obtn.setVisibility(Button.GONE);

         oText = (TextView) oLayout.findViewById(R.id.inquiry_title);
         if (sTitle == null) {
        	 oText.setText(R.string.genre_title);
         } else {
        	 oText.setText(sTitle);
         }


         oview = new ItemView(this, 0, R.layout.inquiry_favorites_genre_freescroll);
         // お気に入りの9つボタンを初期化する
         initGroupButton(oview);
         oList = (ScrollList) oview.findViewById(this, R.id.scrollList);
//Del 2011/07/21 Z01thedoanh Start -->
         //oList.setPadding(5, 4, 2, 4);
//Del 2011/07/21 Z01thedoanh End <--
         oList.setAdapter(new BaseAdapter(){

             @Override
             public int getCount() {
             	if (sTitle == null) {
//             		return 5;
             		return 0;
             	} else {
// Chg 2011/10/19 Z01kkubo Start --> 課題No.275 周辺検索からお気に入りを選択すると検索処理時に強制終了 修正
//             		if (recordCount < 5) {
//             			return 5;
//             		} else {
//             			return recordCount;
//             		}
//------------------------------------------------------------------------------------------------------------------------------------
             		// 5件未満でも5件以上でも、登録レコード件数をそのまま返却する
             		// そうしないと AroundFavGetView() の周辺検索ルートにて
             		// 4件以下の時にも5レコード目までリストを参照しに行き、アプリが落ちてしまうため。。
             		return recordCount;
// Chg 2011/10/19 Z01kkubo End <--

             	}
//                 if (mbAroundFlg) {
//                 	//沿いルート周辺
//                     if (aroundRouteList.size() < 5) {
//                         return 5;
//                     }
//                     return aroundRouteList.size();
//                 } else {
//                     if (aroundList.size()<5) {
//                         return 5;
//                     }
//                     return aroundList.size();
//                 }
             }

             //yangyang add start
 			@Override
 			public boolean isEnabled(int position) {
 				return false;
 			}

 			@Override
 			public boolean areAllItemsEnabled() {
 				return false;
 			}
             //yangyang add end

             @Override
             public Object getItem(int position) {
                 return null;
             }

             @Override
             public long getItemId(int position) {
                 return 0;
             }

             @Override
             public View getView(int position, View convertView, ViewGroup parent) {
                 return AroundFavGetView(position, convertView);
             }

         });
//Del 2011/10/06 Z01_h_yamada Start -->
//         oList.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
         oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));

         LinearLayout oGroupList = (LinearLayout) oLayout
                 .findViewById(R.id.LinearLayout_list);
         oGroupList.addView(oview.getView(), new LinearLayout.LayoutParams(
                 LinearLayout.LayoutParams.FILL_PARENT,
                 LinearLayout.LayoutParams.FILL_PARENT));

         if (sTitle != null) {
        	 int genreIndex = oMyIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX,0);
 			getSearchList(0, genreIndex);
         } else {
        	 initDataObj();
         }
         oTool = new ScrollTool(this);
         oTool.bindView(oList);
         setViewInOperArea(oTool);
         setViewInWorkArea(oLayout);
    }

    private void initDataObj() {
// Add 2011/10/03 katsuta Start -->
    	SearchCacheNum = ONCE_GET_COUNT;
// Add 2011/10/03 katsuta End <--

    	if (mbAroundFlg) {// 沿いルート周辺検索
            if (aroundRouteList == null) {
                aroundRouteList = new ArrayList<POI_UserFile_RouteRec>();
            } else {
                if (!aroundRouteList.isEmpty())
                    aroundRouteList.clear();
            }

            POI_UserFile_RouteRec aroundBigListitem1 = new POI_UserFile_RouteRec();
//Chg 2011/06/09 Z01thedoanh Start -->
            //for (int i = 0; i < 5; i++){
            for (int i = 0; i < INIT_LAYOUT_BIGSHOW; i++){
//Chg 2011/06/09 Z01thedoanh End <--
                aroundBigListitem1.setM_DispName("");
                aroundRouteList.add(aroundBigListitem1);
            }
        } else {
            if (aroundList == null) {
                aroundList = new ArrayList<POI_UserFile_AroundRec>();
            } else {
                if (!aroundList.isEmpty())
                    aroundList.clear();
            }

            POI_UserFile_AroundRec aroundBigListitem = new POI_UserFile_AroundRec();
//Chg 2011/06/09 Z01thedoanh Start -->
            //for (int i = 0; i < 5; i++) {
            for (int i = 0; i < INIT_LAYOUT_BIGSHOW; i++){
//Chg 2011/06/09 Z01thedoanh End <--
                aroundBigListitem.setM_DispName("");
                aroundList.add(aroundBigListitem);
            }
        }

	}


	/**
     * リストに指定データの位置を取得
     *
     * @param data
     *指定データ
     * @return 位置のindex
     */
    private int isExistDataInArray(int data) {
        int ret = -1;
        for (int i = 0; i < favoritesName.length; i++) {
            if (favoritesName[i] == data) {
                return i;
            }
        }
        return ret;
    }

    public static AroundFavoritesGenreInquiry getInstance() {
        return instance;
    }

    /**
     * 表示するデータを取得
     */
    public View AroundFavGetView(final int wPos, View oView) {
        LinearLayout oListLayout = null;
        if (oView == null || !(oView instanceof LinearLayout)) {
            LayoutInflater oInflater = LayoutInflater.from(this);
            oListLayout = (LinearLayout) oInflater.inflate(
                    R.layout.inquiry_distance_button_freescroll, null);
            oView = oListLayout;
        } else {
            oListLayout = (LinearLayout) oView;
        }

        LinearLayout oBtn = (LinearLayout) oListLayout
                .findViewById(R.id.LinearLayout_distance);
        oBtn.setBackgroundResource(R.drawable.btn_default);
        TextView oDistanceView = (TextView) oBtn
                .findViewById(R.id.TextView_distance);
        TextView oValue = (TextView) oBtn.findViewById(R.id.TextView_value);
//Del 2011/11/21 Z01_h_yamada Start -->
//        Resources oRes = getResources();
//        String home1 = oRes.getString(R.string.dialog_myhome_save_button1);
//        String home2 = oRes.getString(R.string.dialog_myhome_save_button2);
//Del 2011/11/21 Z01_h_yamada End <--

        //yangyang add start
//    	NaviNaviLog.d("AroundFavoritesGenreInquiry", "aroundRouteList.size : "+aroundRouteList.size());

//        if (wPos < recordCount) {
//    	if (wPos < aroundRouteList.size()) {
//	        if (wPos % SearchCacheNum == 0) {
//	        	if ((mbAroundFlg && wPos >= aroundRouteList.size()) ||
//	        			(!mbAroundFlg && wPos >= aroundList.size())) {
//	        		if (wPos == 0) {
//	        			initGroupButton(oview);
//	        		}
//		        	int genreIndex = getIntent().getIntExtra(Constants.PARAMS_GENRE_INDEX, 0);
//		        	getSearchList(wPos,genreIndex);
//	        	}
//	        }
//        }
        //yangyang add end
        // 沿いルート周辺であるかどうかを判断する
        if (mbAroundFlg) {
            // 沿いルート周辺の場合
        	NaviLog.d(NaviLog.PRINT_LOG_TAG, "AroundFavoritesGenreInquiry::AroundFavGetView  aroundRouteList size : "+aroundRouteList.size()+" wPos : "+wPos);

//            if (wPos < aroundRouteList.size()) {
// Add 2011/09/30 katsuta Start -->
              if (wPos >= aroundRouteList.size()) {
                  if (wPos % SearchCacheNum == 0) {
          			int genreIndex = getIntent().getIntExtra(Constants.PARAMS_GENRE_INDEX, 0);
                      reGetListRouteData(wPos, genreIndex);
                  }
              }

// Add 2011/09/30 katsuta End <--
              if (!"".equals(aroundRouteList.get(wPos).getM_DispName())) {
// Del 2011/11/18 Z01_h_yamada Start -->
//                  if (home1.equals(aroundRouteList.get(wPos).getM_DispName())
//                          || home2.equals(aroundRouteList.get(wPos).getM_DispName())) {
//                      oValue.setText(aroundRouteList.get(wPos).getM_DispName());
//                      oDistanceView.setText("");
//                      oBtn.setEnabled(false);
//                  } else {
// Del 2011/11/18 Z01_h_yamada End <--
                      // 地点の名称を取得
                      oValue.setText(aroundRouteList.get(wPos).getM_DispName());
                      // 地点の距離を取得
                      long dis = aroundRouteList.get(wPos).getM_lDistance();
                      String s = String.valueOf(dis);
                      oDistanceView.setText("(" + s + "m" + ")");
                      if (dis >= 1000) {
                          double distance = (double) dis / (double) 1000;
                          int t = (int) (distance * 10);
                          distance = (double) t / (double) 10;
                          s = String.valueOf(distance);
                          oDistanceView.setText("(" + s + "km" + ")");
                      }
                      // Click処理
                      oBtn.setEnabled(true);
                      oBtn.setOnClickListener(new OnClickListener() {
                          @Override
                          public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・お気に入り 沿いルート時の選択禁止 Start -->
                  			if(DrivingRegulation.CheckDrivingRegulation()){
                  				return;
                  			}
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・お気に入り 沿いルート時の選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
//Add 2012/02/07 katsuta Start --> #2698
//      						NaviLog.d(NaviLog.PRINT_LOG_TAG, "AroundFavoritesGenreInquiry onClick ================== bIsListClicked: " + bIsListClicked);
      			   			if (bIsListClicked) {
      			   				return;
      			   			}
      			   			else {
      			   				bIsListClicked = true;
      			   			}
//Add 2012/02/07 katsuta End <--#2698

                              POI_UserFile_RouteRec poi = aroundRouteList.get(wPos);
                              POIData oData = new POIData();
                              oData.m_sAddress = poi.getM_DispName();
                              oData.m_sName = poi.getM_DispName();
                              oData.m_wLong = poi.getM_lLon();
                              oData.m_wLat = poi.getM_lLat();

                            //yangyang mod start Bug813
//                                Intent oIntent = getIntent();
//                                oIntent.setClass(AroundFavoritesGenreInquiry.this, RouteEdit.class);
//                                oIntent.putExtra(AppInfo.POI_DATA, oData);
//                                oIntent.putExtra(Constants.PARAMS_ROUTE_MOD_FLAG, true);
//                                oIntent.putExtra(Constants.ROUTE_TYPE_KEY,Constants.ROUTE_TYPE_NOW);
//                                startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
                              CommonLib.routeSearchFlag = true;
                              CommonLib.setChangePosFromSearch(true);
                              CommonLib.setChangePos(false);
//                              // 地図を遷移する
                              OpenMap.moveMapTo(AroundFavoritesGenreInquiry.this, oData,
                              		getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
                              //yangyang mod end Bug813
                              createDialog(Constants.DIALOG_WAIT, -1);

                          }
                      });
// Del 2011/11/18 Z01_h_yamada Start -->
//                  }
// Del 2011/11/18 Z01_h_yamada End <--
              } else {
                  oValue.setText("");
                  oDistanceView.setText("");
                  oBtn.setEnabled(false);
              }
//            } else {
//                oValue.setText("");
//                oDistanceView.setText("");
//                oBtn.setEnabled(false);
//            }
        } else {
            // 周辺の場合
        	NaviLog.d(NaviLog.PRINT_LOG_TAG, "AroundFavoritesGenreInquiry::AroundFavGetView  aroundList.size : "+aroundList.size()+" wPos : "+wPos);

// Add 2011/09/30 katsuta Start -->
            if (wPos >= aroundList.size()) {
                if (wPos % SearchCacheNum == 0) {
        			int genreIndex = getIntent().getIntExtra(Constants.PARAMS_GENRE_INDEX, 0);
                    reGetListAroundData(wPos, genreIndex);
                }
            }

// Add 2011/09/30 katsuta End <--
// Del 2011/09/30 katsuta Start -->
//            if (wPos < aroundList.size()) {
// Del 2011/09/30 katsuta End <--
            if (!"".equals(aroundList.get(wPos).getM_DispName())) {
// Del 2011/11/18 Z01_h_yamada Start -->
//                if (home1.equals(aroundList.get(wPos).getM_DispName())
//                 || home2.equals(aroundList.get(wPos).getM_DispName())) {
//                	NaviLog.d(NaviLog.PRINT_LOG_TAG, "AroundFavoritesGenreInquiry::AroundFavGetView STEP1");
//                    oValue.setText(aroundList.get(wPos).getM_DispName());
//                    oDistanceView.setText("");
//                   oBtn.setEnabled(false);
//               } else {
// Del 2011/11/18 Z01_h_yamada End <--
                    // 地点の名称を取得
                    oValue.setText(aroundList.get(wPos).getM_DispName());
                    // 地点の距離を取得
                    long dis = aroundList.get(wPos).getM_lDistance();
//                        String s = new String();
                    String s = String.valueOf(dis);
                    oDistanceView.setText("(" + s + "m" + ")");
                    if (dis >= 1000) {
                    	double distance = (double) dis / (double) 1000;
                    	int t = (int) (distance * 10);
                    	distance = (double) t / (double) 10;
                    	s = String.valueOf(distance);
                    	oDistanceView.setText("(" + s + "km" + ")");
                    }
                    // Click処理
                    oBtn.setEnabled(true);
                    oBtn.setOnClickListener(new OnClickListener() {
                    	@Override
                    	public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・お気に入り 周辺検索時の選択禁止 Start -->
                  			if(DrivingRegulation.CheckDrivingRegulation()){
                  				return;
                  			}
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・お気に入り 周辺検索時の選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
//Add 2012/02/07 katsuta Start --> #2698
//      						NaviLog.d(NaviLog.PRINT_LOG_TAG, "AroundFavoritesGenreInquiry onClick ================== bIsListClicked: " + bIsListClicked);
      			   			if (bIsListClicked) {
      			   				return;
      			   			}
      			   			else {
      			   				bIsListClicked = true;
      			   			}
//Add 2012/02/07 katsuta End <--#2698
                    		POI_UserFile_AroundRec poi = aroundList.get(wPos);
                    		POIData oData = new POIData();
                    		oData.m_sName = poi.getM_DispName();
                    		oData.m_sAddress = poi.getM_DispName();
                    		oData.m_wLong = poi.getM_lLon();
                    		oData.m_wLat = poi.getM_lLat();

                            // XuYang add start #1056
                            Intent oIntent = getIntent();
                            if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
                            	CommonLib.setChangePosFromSearch(true);
                            	CommonLib.setChangePos(false);
                            } else {
                            	CommonLib.setChangePosFromSearch(false);
                            	CommonLib.setChangePos(false);
                            }
                            // XuYang add end #1056
                            // 地図を遷移する
                            OpenMap.moveMapTo(AroundFavoritesGenreInquiry.this,
                            		oData, getIntent(), AppInfo.POI_LOCAL,
                            		Constants.LOCAL_INQUIRY_REQUEST);
                                createDialog(Constants.DIALOG_WAIT, -1);
                          }
                        });
// Del 2011/11/18 Z01_h_yamada Start -->
//                    }
// Del 2011/11/18 Z01_h_yamada End <--
                } else {
                    oValue.setText("");
                    oDistanceView.setText("");
                    oBtn.setEnabled(false);
                }
// Del 2011/09/30 katsuta Start -->
//            } else {
//                oValue.setText("");
//                oDistanceView.setText("");
//                oBtn.setEnabled(false);
//            }
// Del 2011/09/30 katsuta End <--
        }
        return oView;
    }

    /**
     * お気に入りの9つボタンを初期化する
     *
     * */
    protected void initGroupButton(ItemView oView) {
        // 沿いルート周辺であるかどうかを判断する
        menu_button = new ArrayList<Button>();
        if (mbAroundFlg) {
            if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN) {
                NaviRun.GetNaviRunObj().JNI_NE_POIRouteMybox_SetRoute(0);
            } else if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_CAR) {
                NaviRun.GetNaviRunObj().JNI_NE_POIRouteMybox_SetRoute(1);
            }

            ZPOI_Route_Param stRoutData = new ZPOI_Route_Param();

            // 沿い周辺検索の方向【デフォルト：両方】
            stRoutData.setEside(3);

            // 沿い周辺検索の幅【デフォルト：500】
            stRoutData.setUlnRange(500);

            // 沿い周辺検索の件数
            stRoutData.setLnCount(40);

            NaviRun.GetNaviRunObj().JNI_NE_POIRouteMybox_SetParam(stRoutData);
            NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRouteList();
            CountRouteArray = new POI_Mybox_ListGenreCount[(int) RecCount.lcount];
            int len = (int) RecCount.lcount - 1;

            for (int i = 0; i < RecCount.lcount; i++) {
                CountRouteArray[i] = new POI_Mybox_ListGenreCount();
                NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRouteRecCount(i,
                        CountRouteArray[i]);
            }
            for (int i = 0; i < len; i++) {
                int genreId = (int) CountRouteArray[i].getM_lGenreCode();
// Chg 2011/11/18 Z01_h_yamada Start -->
//                if (i==len-1) {
//                    int showFlag = 1;
//                    if (CountRouteArray[len].getM_sRecordCount() == 0 && CountRouteArray[i].getM_sRecordCount() == 0) {
//                        showFlag = 0;
//                    }
//                    initGroupStat(oView, favoritesName[genreId - 1],
//                            showFlag);
//---------------------------------------
                if (i == KIND_OF_HOUSE) {
                	// 自宅１、２
                	int count = 0;
            		count += CountRouteArray[KIND_OF_HOUSE].getM_sRecordCount();
            		count += CountRouteArray[KIND_OF_HOUSE2].getM_sRecordCount();
                    initGroupStat(oView, favoritesName[genreId - 1], count);
// Chg 2011/11/18 Z01_h_yamada End <--
                } else {
                    initGroupStat(oView, favoritesName[genreId - 1],
                            (int) CountRouteArray[i].getM_sRecordCount());
                }
            }
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(Coordinate);
            NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetAroundList(
                    Coordinate.getM_lLong(), Coordinate.getM_lLat());
            CountArray = new POI_Mybox_ListGenreCount[(int) RecCount.lcount];
            int len = (int) RecCount.lcount - 1;
            for (int i = 0; i < RecCount.lcount; i++) {
                CountArray[i] = new POI_Mybox_ListGenreCount();
                NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetAroundRecCount(i,
                        CountArray[i]);
            }
            for (int i = 0; i < len; i++) {
                int genreId = (int) CountArray[i].getM_lGenreCode();
// Chg 2011/11/18 Z01_h_yamada Start -->
//                if (i==len-1) {
//                    int showFlag = 1;
//                    if (CountArray[len].getM_sRecordCount() == 0 && CountArray[i].getM_sRecordCount() == 0) {
//                        showFlag = 0;
//                    }
//                    initGroupStat(oView, favoritesName[genreId - 1],
//                            showFlag);
//---------------------------------------
                if (i == KIND_OF_HOUSE) {
                	// 自宅１、２
                	int count = 0;
            		count += CountArray[KIND_OF_HOUSE].getM_sRecordCount();
            		count += CountArray[KIND_OF_HOUSE2].getM_sRecordCount();
                    initGroupStat(oView, favoritesName[genreId - 1], count);
// Chg 2011/11/18 Z01_h_yamada End <--
                } else {
                    initGroupStat(oView, favoritesName[genreId - 1],
                            (int) CountArray[i].getM_sRecordCount());
                }
            }
        }

        int intSearchId = oMyIntent.getIntExtra("PARAMS_SEARCH_ID", 0);
        if (intSearchId != 0) {
            btnItem = (Button) oView.findViewById(this, intSearchId);
            btnItem.setSelected(true);
        }
    }

    /**
     * お気に入りの9つボタン状態を設定する
     * */
    private void initGroupStat(ItemView oView, int btnId,
            final int mSRecordCount) {

    	NaviLog.d(NaviLog.PRINT_LOG_TAG, "AroundFavoritesGenreInquiry::initGroupStat START mSRecordCount ="+ mSRecordCount);
        Button btnGroupItem = (Button) oView.findViewById(this, btnId);
        // ボタン使用するかどうかを判断する
        if (mSRecordCount == 0) {
            btnGroupItem.setEnabled(false);
        } else {
            btnGroupItem.setEnabled(true);
            btnGroupItem.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・お気に入り ｼﾞｬﾝﾙ禁止 Start -->
          			if(DrivingRegulation.CheckDrivingRegulation()){
          				return;
          			}
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・お気に入り ｼﾞｬﾝﾙ禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                    int wId = oView.getId();

                        oMyIntent = getIntent();
                        int iPos = isExistDataInArray(wId);
                        oMyIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
                                AroundFavoritesGenreInquiry.this.getResources()
                                        .getString(favoritesStringName[iPos]));
                        oMyIntent.putExtra("PARAMS_SEARCH_ID",
                                favoritesName[iPos]);
                        selectFavoritesId = favoritesName[iPos];
                        oMyIntent.putExtra(Constants.PARAMS_GENRE_INDEX, favoritesId[iPos]);
                        int RecIdx = 0;
                        getSearchList(RecIdx, favoritesId[iPos]);
                        // タイトルであるかどうかを判断する
                        if (AppInfo.isEmpty(sTitle)) {
//                        	recordCount = CountRouteArray[favoritesId[iPos]].getM_sRecordCount();
                        	recordCount = mSRecordCount;
                        	oMyIntent.putExtra(Constants.PARAMS_RECORD_COUNT, mSRecordCount);
                            oMyIntent.setClass(AroundFavoritesGenreInquiry.this,
                                    AroundFavoritesTemp.class);
//Chg 2011/09/23 Z01yoneya Start -->
//                            AroundFavoritesGenreInquiry.this
//                                    .startActivityForResult(
//                                            oMyIntent,
//                                            AppInfo.ID_ACTIVITY_AROUND_FAVORITES_TEMP);
//------------------------------------------------------------------------------------------------------------------------------------
                            NaviActivityStarter.startActivityForResult(AroundFavoritesGenreInquiry.this, oMyIntent,AppInfo.ID_ACTIVITY_AROUND_FAVORITES_TEMP);
//Chg 2011/09/23 Z01yoneya End <--
                        } else {
                        	waitDialogType = GENRE_BUTTON;
                        	recordCount = mSRecordCount;
                            oMyIntent.putExtra(Constants.PARAMS_RECORD_COUNT, mSRecordCount);
//                            oMyIntent.putExtra(Constants.PARAMS_SEARCH_KEY,favoritesStringName[iPos]);
                        	startShowPage(NOTSHOWCANCELBTN);


                        }

                    for (int i = 0; i < menu_button.size(); i++) {
                        menu_button.get(i).setSelected(false);
                    }
                    oView.setSelected(true);
                }
            });
            menu_button.add(btnGroupItem);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id, Bundle args) {
        return super.onCreateDialog(id, args);
    }


    protected void showWaitDialog(String sReqId,boolean bIsBack)
    {
        m_sReqId = sReqId;
    }
    protected void showErrorDialog()
    {
    }


//Add 2011/11/23 Z01_h_yamada Start -->
    static class AroundDistanceComparator implements java.util.Comparator {
    	public int compare(Object s, Object t) {
    		if (((POI_UserFile_AroundRec) s).getM_lDistance() > ((POI_UserFile_AroundRec) t).getM_lDistance()) {
    			return 1;
    		} else if (((POI_UserFile_AroundRec) s).getM_lDistance() < ((POI_UserFile_AroundRec) t).getM_lDistance()) {
    			return -1;
    		}
    		return 0;
    	}
    }

    static class RouteDistanceComparator implements java.util.Comparator {
    	public int compare(Object s, Object t) {
    		if (((POI_UserFile_RouteRec) s).getM_lDistance() > ((POI_UserFile_RouteRec) t).getM_lDistance()) {
    			return 1;
    		} else if (((POI_UserFile_RouteRec) s).getM_lDistance() < ((POI_UserFile_RouteRec) t).getM_lDistance()) {
    			return -1;
    		}
    		return 0;
    	}
    }
//Add 2011/11/23 Z01_h_yamada End <--

    /**
     * 検索するデータのリストを取得
     *
     * @param RecIdx
     *            0
     * @param genreIndex
     *            お気に入りの種類
     */
    @SuppressWarnings("unchecked")
	protected void getSearchList(int RecIdx, int genreIndex) {
        Resources oRes = getResources();
        if (mbAroundFlg) {
            aroundRouteList = new ArrayList<POI_UserFile_RouteRec>();
            JNILong Rec = new JNILong();
            int size = CountRouteArray[genreIndex].getM_sRecordCount();
// Add 2011/10/03 katsuta Start -->
            if (size > SearchCacheNum) {
            	size = SearchCacheNum;
            }
// Add 2011/10/03 katsuta End <--
// Chg 2011/11/18 Z01_h_yamada Start -->
//            m_Poi_UserFile_RouteListitem = new POI_UserFile_RouteRec[size];
//            for (int i = 0; i < size; i++) {
//                m_Poi_UserFile_RouteListitem[i] = new POI_UserFile_RouteRec();
//            }
//
//            NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRouteListItem(
//                    (long) genreIndex, RecIdx, ONCE_GET_COUNT,
//                    m_Poi_UserFile_RouteListitem, Rec);
//            for (int i = 0; i < m_Poi_UserFile_RouteListitem.length; i++) {
//                aroundRouteList.add(m_Poi_UserFile_RouteListitem[i]);
//            }
//            NaviLog.e(NaviLog.PRINT_LOG_TAG, "AroundFavoritesGenreInquiry::getSearchList START genreIndex=" + genreIndex);
//
//            // 自宅の場合
//            if (genreIndex == KIND_OF_HOUSE) {
//                if (aroundRouteList.size() == 0) {
//                    POI_UserFile_RouteRec userFile = new POI_UserFile_RouteRec();
//                    userFile.setM_DispName(oRes.getString(R.string.dialog_myhome_save_button1));
//                    aroundRouteList.add(userFile);
//                }
//                POI_UserFile_RouteRec[] m_POI_Route = null;
//                m_POI_Route = new POI_UserFile_RouteRec[1];
//                for (int i = 0; i < 1; i++) {
//                    m_POI_Route[i] = new POI_UserFile_RouteRec();
//                }
//                // 自宅2を取得
//                NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRouteListItem(8,
//                        RecIdx, ONCE_GET_COUNT, m_POI_Route, Rec);
//                if (Rec.lcount != 0) {
//                    aroundRouteList.add(m_POI_Route[0]);
//                } else {
//                    POI_UserFile_RouteRec userFile2 = new POI_UserFile_RouteRec();
//                    userFile2.setM_DispName(oRes.getString(R.string.dialog_myhome_save_button2));
//                    aroundRouteList.add(userFile2);
//                }
//            }
//---------------------------------------
            if (genreIndex == KIND_OF_HOUSE) {
                // 自宅の場合
             	POI_Mybox_ListGenreCount countHome1 = new POI_Mybox_ListGenreCount();
            	POI_Mybox_ListGenreCount countHome2 = new POI_Mybox_ListGenreCount();

                NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRouteRecCount(KIND_OF_HOUSE, countHome1);
                NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRouteRecCount(KIND_OF_HOUSE2, countHome2);
                NaviLog.w(NaviLog.PRINT_LOG_TAG, "AroundFavoritesGenreInquiry::getSearchList(route) JNI_NE_POIMybox_GetRouteRecCount(1) => " + countHome1.getM_sRecordCount());
                NaviLog.w(NaviLog.PRINT_LOG_TAG, "AroundFavoritesGenreInquiry::getSearchList(route) JNI_NE_POIMybox_GetRouteRecCount(2) => " + countHome2.getM_sRecordCount());

                if (countHome1.getM_sRecordCount() > 0) {
                	POI_UserFile_RouteRec[] userFileRecList = new POI_UserFile_RouteRec[countHome1.getM_sRecordCount()];
                    for (int i = 0; i < countHome1.getM_sRecordCount(); i++) {
                    	userFileRecList[i] = new POI_UserFile_RouteRec();
                    }
                    NaviRun.GetNaviRunObj()
                    		.JNI_NE_POIMybox_GetRouteListItem(KIND_OF_HOUSE, RecIdx, ONCE_GET_COUNT, userFileRecList, Rec);

                    for (int i = 0; i < countHome1.getM_sRecordCount(); i++) {
                    	userFileRecList[i].setM_DispName(oRes.getString(R.string.dialog_myhome_save_button1));
                    	aroundRouteList.add(userFileRecList[i]);
                    }

                }
                if (countHome2.getM_sRecordCount() > 0) {
                	POI_UserFile_RouteRec[] userFileRecList = new POI_UserFile_RouteRec[countHome2.getM_sRecordCount()];
                    for (int i = 0; i < countHome2.getM_sRecordCount(); i++) {
                    	userFileRecList[i] = new POI_UserFile_RouteRec();
                    }
                    NaviRun.GetNaviRunObj()
                    		.JNI_NE_POIMybox_GetRouteListItem(KIND_OF_HOUSE2, RecIdx, ONCE_GET_COUNT, userFileRecList, Rec);

                    for (int i = 0; i < countHome2.getM_sRecordCount(); i++) {
                    	userFileRecList[i].setM_DispName(oRes.getString(R.string.dialog_myhome_save_button2));
                    	aroundRouteList.add(userFileRecList[i]);
                    }

                }
//Add 2011/11/23 Z01_h_yamada Start -->
                Collections.sort(aroundRouteList, new RouteDistanceComparator());
//Add 2011/11/23 Z01_h_yamada End <--

            } else {
            	// 自宅以外
                m_Poi_UserFile_RouteListitem = new POI_UserFile_RouteRec[size];
                for (int i = 0; i < size; i++) {
                    m_Poi_UserFile_RouteListitem[i] = new POI_UserFile_RouteRec();
                }
                NaviRun.GetNaviRunObj()
                        .JNI_NE_POIMybox_GetRouteListItem((long) genreIndex, RecIdx,
                        		ONCE_GET_COUNT, m_Poi_UserFile_RouteListitem, Rec);
                for (int i = 0; i < m_Poi_UserFile_RouteListitem.length; i++) {
                    aroundRouteList.add(m_Poi_UserFile_RouteListitem[i]);
                }
            }
// Chg 2011/11/18 Z01_h_yamada End <--
        } else {
            aroundList = new ArrayList<POI_UserFile_AroundRec>();
            JNILong Rec = new JNILong();
            int size = CountArray[genreIndex].getM_sRecordCount();
// Add 2011/10/03 katsuta Start -->
            if (size > SearchCacheNum) {
            	size = SearchCacheNum;
            }
// Add 2011/10/03 katsuta End <--
// Chg 2011/11/18 Z01_h_yamada Start -->
//            NaviLog.d(NaviLog.PRINT_LOG_TAG, "                                           STEP2 genreIndex=" + genreIndex + " RecIdx=" + RecIdx + " size="+size);
//
//            m_Poi_UserFile_Listitem = new POI_UserFile_AroundRec[size];
//            for (int i = 0; i < size; i++) {
//                m_Poi_UserFile_Listitem[i] = new POI_UserFile_AroundRec();
//            }
//            NaviRun.GetNaviRunObj()
//                    .JNI_NE_POIMybox_GetAroundListItem((long) genreIndex, RecIdx,
//                    		ONCE_GET_COUNT, m_Poi_UserFile_Listitem, Rec);
//            for (int i = 0; i < m_Poi_UserFile_Listitem.length; i++) {
//                aroundList.add(m_Poi_UserFile_Listitem[i]);
//            }
//            // 自宅の場合
//            if (genreIndex == KIND_OF_HOUSE) {
//
//                NaviLog.d(NaviLog.PRINT_LOG_TAG, "                                           STEP3 aroundList.size()="+aroundList.size());
//               if (aroundList.size() == 0) {
//                    POI_UserFile_AroundRec userFile = new POI_UserFile_AroundRec();
//                    userFile.setM_DispName(oRes.getString(R.string.dialog_myhome_save_button1));
//                    aroundList.add(userFile);
//                }
//                POI_UserFile_AroundRec[] m_POI_Route = null;
//                m_POI_Route = new POI_UserFile_AroundRec[1];
//                for (int i = 0; i < 1; i++) {
//                    m_POI_Route[i] = new POI_UserFile_AroundRec();
//                }
//                // 自宅2を取得
//                NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetAroundListItem(8,
//                        RecIdx, ONCE_GET_COUNT, m_POI_Route, Rec);
//                NaviLog.d(NaviLog.PRINT_LOG_TAG, "                                           STEP4 Rec.lcount="+Rec.lcount);
//                if (Rec.lcount != 0) {
//                    aroundList.add(m_POI_Route[0]);
//                } else {
//                    POI_UserFile_AroundRec userFile2 = new POI_UserFile_AroundRec();
//
//                    userFile2.setM_DispName(oRes.getString(R.string.dialog_myhome_save_button2));
//                    aroundList.add(userFile2);
//                }
//                for (int k = 0; k < aroundList.size(); k++) {
//                    NaviLog.d(NaviLog.PRINT_LOG_TAG, "                                           STEP5 Name[" + k + "]="+aroundList.get(k).getM_DispName());
//                }
//            }
//------------------------------------------
            if (genreIndex == KIND_OF_HOUSE) {
                // 自宅の場合
             	POI_Mybox_ListGenreCount countHome1 = new POI_Mybox_ListGenreCount();
            	POI_Mybox_ListGenreCount countHome2 = new POI_Mybox_ListGenreCount();

                NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetAroundRecCount(KIND_OF_HOUSE, countHome1);
                NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetAroundRecCount(KIND_OF_HOUSE2, countHome2);

                if (countHome1.getM_sRecordCount() > 0) {
                    POI_UserFile_AroundRec[] userFileRecList = new POI_UserFile_AroundRec[countHome1.getM_sRecordCount()];
                    for (int i = 0; i < countHome1.getM_sRecordCount(); i++) {
                    	userFileRecList[i] = new POI_UserFile_AroundRec();
                    }
                    NaviRun.GetNaviRunObj()
                    		.JNI_NE_POIMybox_GetAroundListItem(KIND_OF_HOUSE, RecIdx, ONCE_GET_COUNT, userFileRecList, Rec);

                    for (int i = 0; i < countHome1.getM_sRecordCount(); i++) {
                    	userFileRecList[i].setM_DispName(oRes.getString(R.string.dialog_myhome_save_button1));
                        aroundList.add(userFileRecList[i]);
                    }

               }
                if (countHome2.getM_sRecordCount() > 0) {
                    POI_UserFile_AroundRec[] userFileRecList = new POI_UserFile_AroundRec[countHome2.getM_sRecordCount()];
                    for (int i = 0; i < countHome2.getM_sRecordCount(); i++) {
                    	userFileRecList[i] = new POI_UserFile_AroundRec();
                    }
                    NaviRun.GetNaviRunObj()
                    		.JNI_NE_POIMybox_GetAroundListItem(KIND_OF_HOUSE2, RecIdx, ONCE_GET_COUNT, userFileRecList, Rec);

                    for (int i = 0; i < countHome2.getM_sRecordCount(); i++) {
                    	userFileRecList[i].setM_DispName(oRes.getString(R.string.dialog_myhome_save_button2));
                        aroundList.add(userFileRecList[i]);
                    }
                }
//Add 2011/11/23 Z01_h_yamada Start -->
                Collections.sort(aroundList, new AroundDistanceComparator());
//Add 2011/11/23 Z01_h_yamada End <--

            } else {
            	// 自宅以外
                m_Poi_UserFile_Listitem = new POI_UserFile_AroundRec[size];
                for (int i = 0; i < size; i++) {
                    m_Poi_UserFile_Listitem[i] = new POI_UserFile_AroundRec();
                }
                NaviRun.GetNaviRunObj()
                        .JNI_NE_POIMybox_GetAroundListItem((long) genreIndex, RecIdx,
                        		ONCE_GET_COUNT, m_Poi_UserFile_Listitem, Rec);
                for (int i = 0; i < m_Poi_UserFile_Listitem.length; i++) {
                    aroundList.add(m_Poi_UserFile_Listitem[i]);
                }
            }
// Chg 2011/11/18 Z01_h_yamada End <--
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            oMyIntent.removeExtra(Constants.PARAMS_SEARCH_KEY);
            oMyIntent.removeExtra("PARAMS_SEARCH_ID");
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
            Intent oIntent) {

        if (requestCode == AppInfo.ID_ACTIVITY_AROUND_FAVORITES_TEMP) {
        	btnItem = (Button) oview.findViewById(this, selectFavoritesId);
        	btnItem.setSelected(false);
        }
        super.onActivityResult(requestCode, resultCode, oIntent);
    }

	@Override
	protected boolean onStartShowPage() throws Exception {

		if (waitDialogType == DEFAULT_SORT_BUTTON) {
			initDataObj();
		}
		return true;
	}

	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {
		if(bGetData)
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					if (waitDialogType == GENRE_BUTTON) {
						String sTitle = oMyIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
	                    oText.setText(sTitle);
	                    oList.reset();
	                    oList.scroll(0);
					} else {
						initDialog();
					}
				}


			});
		}

	}
	//yangyang add start 20110423
	@Override
	protected boolean onClickGoBack() {
		if (AppInfo.isEmpty(sTitle)) {
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
		}
		return super.onClickGoBack();
	}

	@Override
	protected boolean onClickGoMap() {
		NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
		return super.onClickGoMap();
	}
	//yangyang add end 20110423

	protected void reGetListRouteData(long lRecIndex, long lGenreIndex) {
//Del 2011/11/21 Z01_h_yamada Start -->
//		  JNILong getRouteCnt = new JNILong();
//Del 2011/11/21 Z01_h_yamada End <--

	  JNILong Rec = new JNILong();

	  m_Poi_UserFile_RouteListitem = new POI_UserFile_RouteRec[SearchCacheNum];
	  for (int i = 0; i < SearchCacheNum; i++) {
		  m_Poi_UserFile_RouteListitem[i] = new POI_UserFile_RouteRec();
	  }
	  NaviRun.GetNaviRunObj()
	          .JNI_NE_POIMybox_GetRouteListItem((long)lGenreIndex, lRecIndex,
	        		  SearchCacheNum, m_Poi_UserFile_RouteListitem, Rec);
	  for (int i = 0; i < m_Poi_UserFile_RouteListitem.length; i++) {
	      aroundRouteList.add(m_Poi_UserFile_RouteListitem[i]);
	  }

	}

	protected void reGetListAroundData(long lRecIndex, long lGenreIndex) {
//Del 2011/11/21 Z01_h_yamada Start -->
//		  JNILong getAroundCnt = new JNILong();
//Del 2011/11/21 Z01_h_yamada End <--

	  JNILong Rec = new JNILong();

	  m_Poi_UserFile_Listitem = new POI_UserFile_AroundRec[SearchCacheNum];
	  for (int i = 0; i < SearchCacheNum; i++) {
	      m_Poi_UserFile_Listitem[i] = new POI_UserFile_AroundRec();
	  }
	  NaviRun.GetNaviRunObj()
	          .JNI_NE_POIMybox_GetAroundListItem((long)lGenreIndex, lRecIndex,
	        		  SearchCacheNum, m_Poi_UserFile_Listitem, Rec);
	  for (int i = 0; i < m_Poi_UserFile_Listitem.length; i++) {
	      aroundList.add(m_Poi_UserFile_Listitem[i]);
	  }
	}

}

