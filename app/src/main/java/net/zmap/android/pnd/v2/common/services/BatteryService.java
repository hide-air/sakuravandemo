package net.zmap.android.pnd.v2.common.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class BatteryService
{
	private BatteryReceiver m_oBatteryReceiver = null;
	private BatteryListener m_oListener = null;
	private Context m_oContext = null;


	public BatteryService()
	{
        if (m_oBatteryReceiver == null) {
            m_oBatteryReceiver = new BatteryReceiver();
        }
        // m_oContext.registerReceiver(m_oBatteryReceiver,
        // new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

	public void registerReceiver(Context oContext, BatteryListener oListener){
	    m_oContext = oContext;
	    m_oListener = oListener;

	    m_oContext.registerReceiver(m_oBatteryReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
	}

	public void release()
	{
		if(m_oContext != null && m_oBatteryReceiver != null)
		{
			m_oContext.unregisterReceiver(m_oBatteryReceiver);
		}
	}

	public class BatteryReceiver extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context oContext, Intent oIntent)
		{
			if(m_oListener != null)
			{
//Del 2011/07/11 Z01thedoanh Start -->
				//m_oListener.onBatteryChange(oIntent);
//Del 2011/07/11 Z01thedoanh End <--
			}
		}

	}
}
