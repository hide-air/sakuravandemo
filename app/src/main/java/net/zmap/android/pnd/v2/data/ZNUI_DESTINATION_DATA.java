/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNUI_DESTINATION_DATA.java
 * Description    DESTINATION_DATA情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNUI_DESTINATION_DATA {
    private long lnDistance = 0;
    private ZNUI_TIME stTime = new ZNUI_TIME();
    private long lnToll = 0;
    /**
    * Created on 2010/08/06
    * Title:       getLnDistance
    * Description:  距離を取得する
    * @param1  無し
    * @return       String
    * @author         long
    * @version        1.0
    */
    public long getLnDistance() {
        return lnDistance;
    }
    /**
    * Created on 2010/08/06
    * Title:       getStTime
    * Description:  時間を取得する
    * @param1  無し
    * @return       ZNUI_TIME

    * @version        1.0
    */
    public ZNUI_TIME getStTime() {
        return stTime;
    }
    /**
    * Created on 2010/08/06
    * Title:       getLnToll
    * Description:  料金を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLnToll() {
        return lnToll;
    }
}
