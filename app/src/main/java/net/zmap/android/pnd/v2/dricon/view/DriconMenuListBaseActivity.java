/**
 *  @file		DriconMenuListBaseActivity
 *  @brief		ドライブコンテンツのリスト表示時のベースクラス
 *
 *  @attention
 *  @note
 *
 *  @author		Manabu Watanabe [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2011-09-01)
 *  @version	$Revision: $ by $Author: $
 *
 */
package net.zmap.android.pnd.v2.dricon.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.activity.MenuBaseActivity;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.data.DC_ContentsData;
import net.zmap.android.pnd.v2.dricon.controller.DriverContentsCtrl;
import net.zmap.android.pnd.v2.inquiry.view.ItemView;

import java.util.List;

public abstract class DriconMenuListBaseActivity extends MenuBaseActivity{
	protected static final int 						ON = 1;
	protected static final int 						OFF = 0;

	private LayoutInflater 							m_oInflater;
	private MenuSettingList 						m_oMenuSetting;
	protected LinearLayout 							m_oLayout = null;
	protected String								m_sTitle = "";
	protected String								m_sIconPath = null;
	protected int									m_nCount = 0;
	protected static List<DC_ContentsData>			m_oContentsDataList = null;

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.common.activity.MenuBaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		m_oInflater = LayoutInflater.from(this);
		if(DriverContentsCtrl.getController() != null){
			m_oContentsDataList = DriverContentsCtrl.getController().getContentsDataList();
		}
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.common.activity.MenuBaseActivity#goMap()
	 */
	public void goMap(){
		DriverContentsCtrl.getController().hideDialog();
		super.goMap();
	}

	/**
	 * ボタンが選択された時に呼ばれる
	 * @param oSwitchOnBtn オン用ボタン
	 * @param oSwitchOffBtn オフ用ボタン
	 * @param type 現在の状態
	 */
	public void selectSwBtn(Button oSwitchOnBtn, Button oSwitchOffBtn, int type){
		if(oSwitchOnBtn == null || oSwitchOffBtn == null){
			return;
		}
		if(type == ON){
			oSwitchOnBtn.setBackgroundResource(R.drawable.btn_default_select);
			oSwitchOnBtn.setTextColor(getResources().getColor(R.color.switch_on));
			oSwitchOffBtn.setBackgroundResource(R.drawable.btn_default);
			oSwitchOffBtn.setTextColor(getResources().getColorStateList(R.color.button_color));
		}else{
			oSwitchOnBtn.setBackgroundResource(R.drawable.btn_default);
			oSwitchOnBtn.setTextColor(getResources().getColorStateList(R.color.button_color));
			oSwitchOffBtn.setBackgroundResource(R.drawable.btn_default_select);
			oSwitchOffBtn.setTextColor(getResources().getColor(R.color.switch_on));
		}
	}

	/**
	 * メニューのレイアウトの初期化を行う
	 */
	public void initDriConMenuLayout(){
		LinearLayout oLayout = (LinearLayout) m_oInflater.inflate(R.layout.dricon_base, null);
		LinearLayout oGroupList = (LinearLayout) oLayout.findViewById(R.id.dricon_base_LL_list);
		ItemView oview = new ItemView(this, 0, R.layout.dricon_menu_freescroll);

		TextView tvTitle = (TextView)oLayout.findViewById(R.id.dc_base_title);
		tvTitle.setText(m_sTitle);

		ImageView imgIcon = (ImageView)oLayout.findViewById(R.id.dc_base_icon);

		if(m_sIconPath != null){
			Bitmap bitmap = BitmapFactory.decodeFile(m_sIconPath);
			if(bitmap != null){
				imgIcon.setVisibility(View.VISIBLE);
				imgIcon.setImageBitmap(bitmap);
			} else {
				imgIcon.setVisibility(View.GONE);
			}
		}else{
			imgIcon.setVisibility(View.GONE);
		}

		ScrollList scrollList = (ScrollList) oview.findViewById(this, R.id.scrollList);
		m_oMenuSetting = new MenuSettingList();
		scrollList.setAdapter(m_oMenuSetting);
		scrollList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
//Del 2011/10/06 Z01_h_yamada Start -->
//		scrollList.setCountInPage(6);
//Del 2011/10/06 Z01_h_yamada End <--

		oGroupList.addView(oview.getView(), new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT));

		ScrollTool oTool = new ScrollTool(this);
		oTool.bindView(scrollList);
		removeOperArea();
		setViewInOperArea(oTool);

		getWorkArea().removeAllViews();
		setViewInWorkArea(oLayout);
	}
	public abstract View getViewList(int wPos, View oView, ViewGroup parent);

	/**
	 * リストが更新されたことを受ける
	 */
	public void notifyUpdataList(){
		m_oMenuSetting.notifyDataSetChanged();
	}

	/**
	 * タイトル情報を設定する
	 * @param title タイトル
	 */
	public void setTitleValue(String title) {
		m_sTitle = title;
	}

	/**
	 * アイコンのファイルパスを設定する
	 * @param iconPath ファイルパス
	 */
	public void setTitleIcon(String iconPath) {
		m_sIconPath = iconPath;
	}

	private class MenuSettingList extends BaseAdapter {
		@Override
		public int getCount() {
			return m_nCount;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int wPos, View oView, ViewGroup parent) {
			oView = getViewList(wPos, oView, parent);
			return oView;
		}
	}

}
