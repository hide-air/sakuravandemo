package net.zmap.android.pnd.v2.common;

import android.app.Application;

public class TrackCommon extends Application{

	// グローバルに扱う変数
	private int TrackDataCnt;
	
	// 変数の初期化
	public void init() {
		setTrackDataCnt(0);
	}

	public int getTrackDataCnt() {
		return TrackDataCnt;
	}

	public void setTrackDataCnt(int trackDataCnt) {
		TrackDataCnt = trackDataCnt;
	}

}
