package net.zmap.android.pnd.v2.sakuracust;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.exception.NaviInvalidValueException;
import net.zmap.android.pnd.v2.api.overlay.Icon;
import net.zmap.android.pnd.v2.common.utils.DbHelper;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;



// 図形情報を格納するクラス
public class UserIconInfo {

    // アイコンファイル名
    public static final String ICON_ARROW_LEFTUP = "arrow_leftup";
    public static final String ICON_ARROW_UP = "arrow_up";
    public static final String ICON_ARROW_RIGHTUP = "arrow_rightup";
    public static final String ICON_ARROW_LEFT = "arrow_left";
    public static final String ICON_ARROW_RIGHT = "arrow_right";
    public static final String ICON_ARROW_LEFTDOWN = "arrow_leftdown";
    public static final String ICON_ARROW_DOWN = "arrow_down";
    public static final String ICON_ARROW_RIGHTDOWN = "arrow_rightdown";

    public static final String ICON_IPPOU_UP = "ippou_up";
    public static final String ICON_IPPOU_LEFT = "ippou_left";
    public static final String ICON_IPPOU_RIGHT = "ippou_right";
    public static final String ICON_IPPOU_DOWN = "ippou_down";

    public static final String ICON_HYOSHIKI_1 = "hyoshiki1_s";
    public static final String ICON_HYOSHIKI_2 = "hyoshiki2_s";
    public static final String ICON_HYOSHIKI_3 = "hyoshiki3_s";
    public static final String ICON_HYOSHIKI_4 = "hyoshiki4_s";
    public static final String ICON_HYOSHIKI_5 = "hyoshiki5_s";
    public static final String ICON_HYOSHIKI_6 = "hyoshiki6_s";
    public static final String ICON_HYOSHIKI_7 = "hyoshiki7_s";
    public static final String ICON_HYOSHIKI_8 = "hyoshiki8_s";
    public static final String ICON_HYOSHIKI_9 = "hyoshiki9_s";

    // アイコンファイル識別番号
    public static final int ICON_ARROW_LEFTUP_INDEX = 1001;
    public static final int ICON_ARROW_UP_INDEX = 1002;
    public static final int ICON_ARROW_RIGHTUP_INDEX = 1003;
    public static final int ICON_ARROW_LEFT_INDEX = 1004;
    public static final int ICON_ARROW_RIGHT_INDEX = 1005;
    public static final int ICON_ARROW_LEFTDOWN_INDEX = 1006;
    public static final int ICON_ARROW_DOWN_INDEX = 1007;
    public static final int ICON_ARROW_RIGHTDOWN_INDEX = 1008;

    public static final int ICON_IPPOU_UP_INDEX = 1009;
    public static final int ICON_IPPOU_LEFT_INDEX = 1010;
    public static final int ICON_IPPOU_RIGHT_INDEX = 1011;
    public static final int ICON_IPPOU_DOWN_INDEX = 1012;

    public static final int ICON_HYOSHIKI_1_INDEX = 1013;
    public static final int ICON_HYOSHIKI_2_INDEX = 1014;
    public static final int ICON_HYOSHIKI_3_INDEX = 1015;
    public static final int ICON_HYOSHIKI_4_INDEX = 1016;
    public static final int ICON_HYOSHIKI_5_INDEX = 1017;
    public static final int ICON_HYOSHIKI_6_INDEX = 1018;
    public static final int ICON_HYOSHIKI_7_INDEX = 1019;
    public static final int ICON_HYOSHIKI_8_INDEX = 1020;
    public static final int ICON_HYOSHIKI_9_INDEX = 1021;


	public int mID;				// ID
	public int mIconIndex;      // アイコン識別ID
    public int mFigureKind;		// 図形種別（1:ライン、2:アイコン）
	//public long mFigureID;		// 図形ID
	public Icon mFigureIcon;
    public double dLat1;		// 配置点（始点）
    public double dLon1;		// 配置点（始点）
    public double dLat2;		// 配置点（終点）
    public double dLon2;		// 配置点（終点）
    public String mIconFile;	// アイコンファイル名（フルパス）
    
    // アイコンインデックスから、該当のアイコンファイル名を返却する
    public  String getIconFileNameByIconIndex(int index){
    	
    	if(index == ICON_ARROW_LEFTUP_INDEX)	return ICON_ARROW_LEFTUP;
    	if(index == ICON_ARROW_UP_INDEX)	return ICON_ARROW_UP;
    	if(index == ICON_ARROW_RIGHTUP_INDEX)	return ICON_ARROW_RIGHTUP;
    	if(index == ICON_ARROW_LEFT_INDEX)	return ICON_ARROW_LEFT;
    	if(index == ICON_ARROW_RIGHT_INDEX)	return ICON_ARROW_RIGHT;
    	if(index == ICON_ARROW_LEFTDOWN_INDEX)	return ICON_ARROW_LEFTDOWN;
    	if(index == ICON_ARROW_DOWN_INDEX)	return ICON_ARROW_DOWN;
    	if(index == ICON_ARROW_RIGHTDOWN_INDEX)	return ICON_ARROW_RIGHTDOWN;
    	if(index == ICON_IPPOU_UP_INDEX)	return ICON_IPPOU_UP;
    	if(index == ICON_IPPOU_LEFT_INDEX)	return ICON_IPPOU_LEFT;
    	if(index == ICON_IPPOU_RIGHT_INDEX)	return ICON_IPPOU_RIGHT;
    	if(index == ICON_IPPOU_DOWN_INDEX)	return ICON_ARROW_LEFTUP;
    	if(index == ICON_HYOSHIKI_1_INDEX)	return ICON_HYOSHIKI_1;
    	if(index == ICON_HYOSHIKI_2_INDEX)	return ICON_HYOSHIKI_2;
    	if(index == ICON_HYOSHIKI_3_INDEX)	return ICON_HYOSHIKI_3;
    	if(index == ICON_HYOSHIKI_4_INDEX)	return ICON_HYOSHIKI_4;
    	if(index == ICON_HYOSHIKI_5_INDEX)	return ICON_HYOSHIKI_5;
    	if(index == ICON_HYOSHIKI_6_INDEX)	return ICON_HYOSHIKI_6;
    	if(index == ICON_HYOSHIKI_7_INDEX)	return ICON_HYOSHIKI_7;
    	if(index == ICON_HYOSHIKI_8_INDEX)	return ICON_HYOSHIKI_8;
    	if(index == ICON_HYOSHIKI_9_INDEX)	return ICON_HYOSHIKI_9;
    	
    	return null;
    	
    }

    // ファイル名から該当のアイコンインデックスを返却する
    public  int getIconIndexByIconFileName(String szFileName){
    	
    	if(szFileName.equals(ICON_ARROW_LEFTUP) )	return ICON_ARROW_LEFTUP_INDEX;
    	if(szFileName.equals(ICON_ARROW_UP) ) return ICON_ARROW_UP_INDEX;
    	if(szFileName.equals(ICON_ARROW_RIGHTUP) )	return ICON_ARROW_RIGHTUP_INDEX;
    	if(szFileName.equals(ICON_ARROW_LEFT) ) return ICON_ARROW_LEFT_INDEX;
    	if(szFileName.equals(ICON_ARROW_RIGHT) )	return ICON_ARROW_RIGHT_INDEX;
    	if(szFileName.equals(ICON_ARROW_LEFTDOWN) )	return ICON_ARROW_LEFTDOWN_INDEX;
    	if(szFileName.equals(ICON_ARROW_DOWN) )	return ICON_ARROW_DOWN_INDEX;
    	if(szFileName.equals(ICON_ARROW_RIGHTDOWN) ) return ICON_ARROW_RIGHTDOWN_INDEX;
    	if(szFileName.equals(ICON_IPPOU_UP) )	return ICON_IPPOU_UP_INDEX;
    	if(szFileName.equals(ICON_IPPOU_LEFT) )	return ICON_IPPOU_LEFT_INDEX;
    	if(szFileName.equals(ICON_IPPOU_RIGHT) )	return ICON_IPPOU_RIGHT_INDEX;
    	if(szFileName.equals(ICON_ARROW_LEFTUP) )	return ICON_IPPOU_DOWN_INDEX;
    	if(szFileName.equals(ICON_HYOSHIKI_1) )	return ICON_HYOSHIKI_1_INDEX;
    	if(szFileName.equals(ICON_HYOSHIKI_2) )	return ICON_HYOSHIKI_2_INDEX;
    	if(szFileName.equals(ICON_HYOSHIKI_3) )	return ICON_HYOSHIKI_3_INDEX;
    	if(szFileName.equals(ICON_HYOSHIKI_4) )	return ICON_HYOSHIKI_4_INDEX;
    	if(szFileName.equals(ICON_HYOSHIKI_5) )	return ICON_HYOSHIKI_5_INDEX;
    	if(szFileName.equals(ICON_HYOSHIKI_6) )	return ICON_HYOSHIKI_6_INDEX;
    	if(szFileName.equals(ICON_HYOSHIKI_7) )	return ICON_HYOSHIKI_7_INDEX;
    	if(szFileName.equals(ICON_HYOSHIKI_8) )	return ICON_HYOSHIKI_8_INDEX;
    	if(szFileName.equals(ICON_HYOSHIKI_9) )	return ICON_HYOSHIKI_9_INDEX;
    	
    	return 0;
    	
    }
}

