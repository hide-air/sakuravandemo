/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           JNIThreeLong.java
 * Description    3��long�^�̃f�[�^��class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class JNIThreeLong {
    private long  lnNum1;
    private long  lnNum2;
    private long  lnNum3;
    /**
    * Created on 2010/08/06
    * Title:       getLnNum1
    * Description:  第一個のデータを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLnNum1() {
        return lnNum1;
    }
    /**
    * Created on 2010/08/06
    * Title:       getLnNum1
    * Description:  第2のデータを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLnNum2() {
        return lnNum2;
    }
    /**
    * Created on 2010/08/06
    * Title:       getLnNum1
    * Description:  第3のデータを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLnNum3() {
        return lnNum3;
    }
}
