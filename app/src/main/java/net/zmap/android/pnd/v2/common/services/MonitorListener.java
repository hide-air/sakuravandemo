package net.zmap.android.pnd.v2.common.services;

public interface MonitorListener
{
	public void onCommandReceived(int iCommand, String sOperand);
}

