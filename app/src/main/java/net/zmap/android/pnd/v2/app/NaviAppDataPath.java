package net.zmap.android.pnd.v2.app;

import java.io.File;
import java.util.Calendar;

import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.utils.NaviLog;

/*
参考までにNaviConfigData.iniの中身
---------------------------------------------------------------
[NaviPath]
BaseMapPath = /sdcard/ItsmoNaviDrive/map/
TownMapPath = /sdcard/ItsmoNaviDrive/town/
WalkDataPath = /sdcard/ItsmoNaviDrive/map/
UserDataPath = /sdcard/ItsmoNaviDrive/User/
TmpDataPath = /sdcard/ItsmoNaviDrive/TMP/
LogDataPath = /sdcard/ItsmoNaviDrive/log/
AssetFilePath = /data/data/net.zmap.android.pnd.v2/files/
[FileVer]
NaviDataRes = 1.0
---------------------------------------------------------------
*/

/*
 * ナビアプリデータパスクラス
 *
 * ナビアプリデータのルートフォルダや
 * 各データパスの管理、データの存在判定を行う
 *
 */
public class NaviAppDataPath {
    //PNDアプリ全体のルートディレクトリ名称
    public static final String NAVI_APP_ROOT_DIR_NAME = "ItsmoNaviDrive"; //"zdcpnd";

    //地図ダウンロードを行わずに、ダウンロード済みローカルデータを使用するフラグ。とそのパス。
    private static final boolean enableLocalData = true;
//@@MOD-START BB-0003 2012/10/19 Y.Hayashida
//    private static final String localDataPath = "/sdcard/ItsmoNaviDrive";
//@@MOD-START BB-0003 2012/10/19 Y.Hayashida

    //PNDで使う各種ディレクトリの名称定義
    private static final String dirNameBaseMap = "map";     //地図
    private static final String dirNameTownMap = "town";    //市街地図
    private static final String dirNameWalkData = "map";    //歩行者データ
//Chg 2011/11/17 Z01_h_yamada Start -->
//    private static final String dirNameUserData = "User"; //ユーザデータ
//--------------------------------------------
    private static final String dirNameUserData = "user";   //ユーザデータ
//Chg 2011/11/17 Z01_h_yamada End <--
    private static final String dirNameTmpData = "TMP";     //一時データ
    private static final String dirNameLogData = "log";     //ログデータ
    private final static String dirNameTTy = "tty"; //端末固有データ
    private final String dirNameExternalData = "naviicon";

// MOD.2013/03/25 N.Sasao Android4.2 以降 マルチユーザー対応 START
//    private final String dirAssetFilePath = "/data/data/net.zmap.android.pnd.v2/files/"; //AssetFilePath
    private String dirAssetFilePath = null; //AssetFilePath
// MOD.2013/03/25 N.Sasao Android4.2 以降 マルチユーザー対応  END

    private static String mNaviAppRootPath = null; //ナビアプリデータのルートフォルダのフルパス

    //データファイルの存在関連
    private boolean bExistTownMap = false;
    private boolean bExistWalkData = false;
//Add 2011/09/30 Z01yamaguchi Start -->
    private boolean bExistVicsData = false;
//Add 2011/09/30 Z01yamaguchi End <--

    private static final String existDatFile_townMap = "town_exists.dat";
    private static final String existDatFile_walkData = "walk_exists.dat";
//Add 2011/09/30 Z01yamaguchi Start -->
    private static final String existDatFile_vicsData = "vics_exists.dat";

//Add 2011/09/30 Z01yamaguchi End <--

    /* VP/DRログファイル格納フォルダ名保存 */
	private static String drLogPathFile = "";
	private static String drLogNPathFile = "";

    public NaviAppDataPath() {
//@@MOD-START BB-0003 2012/10/19 Y.Hayashida
//ここを通ってしまうと、後から設定不可になるため、コメント
//※必ず、Initialize時に設定されるから問題なし
//        if (enableLocalData) {
//            setNaviAppRootPath(localDataPath);
//        }
//@@MOD-END BB-0003 2012/10/19 Y.Hayashida
    }

    /*
     * 地図データの取得方法モード（ローカル or ダウンロード）
     *
     * @return true:ローカルデータモード false:地図ダウンロードモード
     */
    public static boolean isLocalDataMode() {
        return enableLocalData;
    }

    /*
     * ナビアプリのデータルートフォルダを設定
     *
     * "/sdcard/ItsmoNaviDrive"など。パスの最後はスラッシュを入れない
     * 同時に詳細地図データ、歩行者データの存在有無をチェックする
     *
     */
    public void setNaviAppRootPath(String path) throws IllegalStateException {
// DEL.2013/03/28 N.Sasao Bug #12040 地図データダウンロード後、詳細地図が表示できない START
//    	if (mNaviAppRootPath != null) {
//          NaviLog.d(NaviLog.PRINT_LOG_TAG, "setNaviAppRootPath already set! error!");
//          throw new IllegalStateException();
//      }
// DEL.2013/03/28 N.Sasao Bug #12040 地図データダウンロード後、詳細地図が表示できない  END

//@@MOD-START BB-0003 2012/10/29 Y.Hayashida
    	mNaviAppRootPath = path + File.separator + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME;

    	// FOUNTORO端末のSDCardの場合
    	//mNaviAppRootPath =  "/sdcard/extsd/ItsmoNaviPnd";

//        mNaviAppRootPath = path;
//@@MOD-START BB-0003 2012/10/29 Y.Hayashida

        try {
            bExistTownMap = isExistFile(getTownMapPath() + existDatFile_townMap);
        } catch (IllegalStateException e) {
            bExistTownMap = false;
        }

        try {
            bExistWalkData = isExistFile(getWalkDataPath() + existDatFile_walkData);
        } catch (IllegalStateException e) {
            bExistWalkData = false;
        }

//Add 2011/09/30 Z01yamaguchi Start -->
        try {
            bExistVicsData = isExistFile(getBaseMapPath() + existDatFile_vicsData);
        } catch (IllegalStateException e) {
            bExistVicsData = false;
        }
//Add 2011/09/30 Z01yamaguchi End <--
    }

    private boolean isExistFile(String existDatFilePath) {
        return CommonLib.fileExists(existDatFilePath);
    }

    /*
     * ナビアプリのデータルートフォルダを取得
     */
//Chg 2011/11/15 Z01_h_yamada Start -->
//    public String getNaviAppRootPath(String path) {
//        return mNaviAppRootPath;
//    }
//--------------------------------------------
    public String getNaviAppRootPath() {
        return mNaviAppRootPath;
    }
//Chg 2011/11/15 Z01_h_yamada End <--

    /**
     * 対象フォルダへのフルパス取得
     *
     * 初期化が完了していない場合は例外を発生する。
     *
     * @return 対象フォルダへのフルパス
     * @throws IllegalStateException
     */
    private String getFullPath(String targetDirName) throws IllegalStateException {
        if (mNaviAppRootPath == null) {
            throw new IllegalStateException("mNaviAppRootPath is null");
        }
        return mNaviAppRootPath + "/" + targetDirName + "/";
    }

    //地図
    public String getBaseMapPath() throws IllegalStateException {
        return getFullPath(dirNameBaseMap);
    }

    //市街地図
    public String getTownMapPath() throws IllegalStateException {
        return getFullPath(dirNameTownMap);
    }

    //歩行者データ
    public String getWalkDataPath() throws IllegalStateException {
        return getFullPath(dirNameWalkData);
    }

    //ユーザデータ
    public String getUserDataPath() throws IllegalStateException {
        return getFullPath(dirNameUserData);
    }

    //一時データ
    public String getTmpDataPath() throws IllegalStateException {
        return getFullPath(dirNameTmpData);
    }

    //ログデータ
    public String getLogDataPath() throws IllegalStateException {
        return getFullPath(dirNameLogData);
    }

    //Assetファイル
    public String getAssetFilePath() throws IllegalStateException {
        return dirAssetFilePath;
    }

    //RouteData.xmlのファイル名
    public static final String ROUTE_DATA_XML_FILENAME = "RouteData.xml";

    //RouteData.xmlのファイルフルパス
    public String getRouteDataXmlFileFullPath() {
        return getUserDataPath() + ROUTE_DATA_XML_FILENAME;
    }

    //案内再開ファイル
    public static final String NAVI_DATA_FILENAME = "NavigationData.txt";

    //NavigationData.txtのファイルフルパス
    public String getNavigationDataFileFullPath() {
        return getTmpDataPath() + NAVI_DATA_FILENAME;
    }

    /*
     * 市街地図データの存在判定(true:ある false:ない)
     */
    public boolean isExistTownMap() {
        return bExistTownMap;
    }

    /*
     * 歩行者データの存在判定(true:ある false:ない)
     */
    public boolean isExistWalkData() {
        return bExistWalkData;
    }

//Add 2011/09/30 Z01yamaguchi Start -->
    /*
     * VICSデータの存在判定(true:ある false:ない)
     */
    public boolean isExistVicsData() {
        return bExistVicsData;
    }

//Add 2011/09/30 Z01yamaguchi End <--

    /*
     * NaviDataResバージョン文字列取得関数
     *
     * NaviConfigData.iniとの互換のため。
     */
    public String getNaviDataResFileVerStr() {
        return "NaviDataRes = 1.0";
    }

// ADD.2013/03/25 N.Sasao Android4.2 以降 マルチユーザー対応 START
    /*
     * ナビアプリのアプリケーションデータフォルダを設定
     *
     * "/data/data/net.zmap.android.pnd.v2/files"など。パスの最後はスラッシュはつけておく
     * マルチユーザーにより、パスが変わるので注意
     *
     * 一度設定した後は変更できない、例外を発生する。
     */
    public void setNaviAppDataPath(String path) throws IllegalStateException {
        if (dirAssetFilePath != null) {
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "setNaviAppDataPath already set! error!");
            throw new IllegalStateException();
        }

        dirAssetFilePath = path + "/";
    }
// ADD.2013/03/25 N.Sasao Android4.2 以降 マルチユーザー対応  END

    /*** V2.5 対応  ***/
    private static String getFullPath2(String targetDirName) throws IllegalStateException {
        if (mNaviAppRootPath == null) {
            throw new IllegalStateException("mNaviAppRootPath is null");
        }
        //通常のパス
        return mNaviAppRootPath + "/" + targetDirName;
        // FOUNTORO端末のSDCardの場合
        //return  "/sdcard/extsd/ItsmoNaviPnd/" + targetDirName;
    }
    //　VP/DR ログデータ
    public String Getdate(){
    	String time = "";
    	final Calendar calendar = Calendar.getInstance();

        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);

        time = Integer.toString(year);
        if (month < 9){
        	time += "0" + (month + 1);
        }else{
        	time += (month + 1);
        }
        if (day < 10){
        	time += "0" + day;
        }else{
        	time += day;
        }
        if (hour < 10){
        	time += "0" + hour;
        }else{
        	time += hour;
        }
    if (minute < 10){
        	time += "0" + minute;
        }else{
        	time += minute;
        }
    	//time +=  "/";
    	return time;
    }
	//ディレクトリ確認
    public static boolean existsdir(String path){
		File file = new File(path);
		return file.exists();
	}
	//logファイル作成全体
	private boolean mkLog(String path){
		File file = new File(path);

		boolean res = true;

		if (!existsdir(path)){
			res = file.mkdir();
			res = file.canWrite();
			res = file.canRead();
			//NaviLog.v("LogData2Path", "mkdir "+path);
		}
		return res;
	}
	// V2.5用生ログファイルの存在確認
	public static boolean isgetDRLogFile() throws IllegalStateException {
		boolean res = false;
		drLogNPathFile = getFullPath2(dirNameLogData)+"/DRLog" ;
		if(existsdir(drLogNPathFile+"/DRLogN.txt")){
			res = true;
		}
		return res;
	}
	/* 生ログファイル格納パスを取得する */
	public static String getDRLogFilePath() throws IllegalStateException  {
		drLogNPathFile = getFullPath2(dirNameLogData)+"/DRLog/" ;
		return drLogNPathFile ;
	}
	/* V2.5用ログフォルダ作成 */
    public String getLogData2Path() throws IllegalStateException {
    	String wst="";
    	String vpwst = getFullPath2(dirNameLogData) ;
    	mkLog(vpwst);
		/* log ﾌｫﾙﾀﾞ削除時に、VPﾌｫﾙﾀﾞが作成されないので VPﾌｫﾙﾀﾞが無い場合mkdir する */
		vpwst = getFullPath2(dirNameLogData) + "/VP";
		mkLog(vpwst);
    	/* 日付別フォルダ作成 */
    	wst = getFullPath2(dirNameLogData) + "/VP/" + Getdate();
    	// カレント"log"フォルダがない場合、ログしない
    	if(mkLog(wst)) {
            //NaviLog.v("LogData2Path", "Good "+wst);
    		/* ログ日付別フォルダ作成済み */
    		wst += "/";
    	}
    	else {
    		/* フォルダ作成失敗、カレントlogパスにする */
    		wst = getFullPath2(dirNameLogData) + "/VP/";
    	}
	    drLogPathFile = wst;
        return wst;
    }
	/* ログファイル格納パスを取得する */
	public static String getLogFilePath() {
		return drLogPathFile ;
	}
	/* 端末固有フォルダ */
    public String getTTyPath() throws IllegalStateException {
    	String vpwst = getFullPath2(dirNameTTy) ;
    	mkLog(vpwst);
		vpwst = getFullPath2(dirNameTTy) + "/Pro";
		mkLog(vpwst);
        return vpwst;
    }
// ADD.2013/09/19 N.Sasao 外部連携固定パス変更・後処理対応実装 START
	public String getExternalDataPath() throws IllegalStateException {
        if (mNaviAppRootPath == null) {
            throw new IllegalStateException("mNaviAppRootPath is null");
        }
        return mNaviAppRootPath + File.separator + dirNameTmpData + File.separator + dirNameExternalData + File.separator;
	}
// ADD.2013/09/19 N.Sasao 外部連携固定パス変更・後処理対応実装  END
}
