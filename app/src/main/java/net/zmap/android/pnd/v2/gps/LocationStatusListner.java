package net.zmap.android.pnd.v2.gps;

/*
 * GPS捕捉状態の通知を受けるリスナークラス
 */

public interface LocationStatusListner {

    /*
     * GPS捕捉状態の表示更新
     *  @param usedInFixGpsCount
     *            測位使用GPSの数
     *  @param snrAverage
     *            測位使用GPSのSN比率の平均値
     */
    public void OnUpdateGpsStatus(int usedInFixGpsCount, float snrAverage);

    /*
     * GPS捕捉状態の表示更新
     *  @param bValidAccuracy
     *            trueなら有効/falseなら無効
     *  @param accuracy
     *            GPSの誤差円半径(メートル)
     */
    public void OnGetLocationGpsAccuracy(boolean bValidAccuracy, float accuracy);

}
