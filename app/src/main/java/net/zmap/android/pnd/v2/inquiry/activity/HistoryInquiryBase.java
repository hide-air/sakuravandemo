//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.common.view.ScrollBoxAdapter;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_History_FilePath;
import net.zmap.android.pnd.v2.inquiry.data.CommonMethd;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.maps.OpenMap;

/**
 * K-H0_履歴一覧
 * */
public class HistoryInquiryBase extends InquiryBaseLoading  implements ScrollBoxAdapter {


	/** スクロールできる総件数 */
	protected JNILong RecCount = new JNILong();
	/** カレント表示できるレコード数 */
	protected JNILong Rec = new JNILong();
	/**
	 * JNIからデータを取得するとき、スタートIndex
	 * */
	protected long RecIndex = 0;

	/** スクロール対象 */
	protected ScrollList oList = null;

	/** スクロールできる総件数 */
	protected int allRecordCount = 0;

	protected int fromFlagKey = -1;
	/** 沿いルート周辺と施設周辺のフラグ true:沿いルート周辺 */
	protected boolean mbAroundFlg = false;
	private int wOldPos = 0;
	private int getRecordIndex = 0;
	/** MainMenuから遷移のフラグ */
	private String strFromFlag = "";
	/** クリックした履歴のIndex */

	private int clickWPos = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent oIntent = getIntent();
		fromFlagKey = oIntent.getIntExtra(Constants.FROM_FLAG_KEY, AppInfo.ID_ACTIVITY_MAINMENU);
		mbAroundFlg = CommonLib.isAroundFlg();
		initClear();

		initHistory();
		initDialog();

	}

	/**
	 * ルート沿い周辺と施設周辺検索するとき、データを取得する前の準備処理
	 *
	 * */
	protected void initHistory() {
	}

	/**
	 * クリア処理
	 * */
	private void initClear() {
		POI_History_FilePath path = new POI_History_FilePath();
		NaviRun.GetNaviRunObj().JNI_NE_POIHist_Clear(path);
	}

	/**
	 * 画面を初期化する
	 * */
	private void initDialog() {
		Intent oIntent = getIntent();
//Del 2011/09/17 Z01_h_yamada Start -->
//		//タイトルを設定する
//		int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE,0);
//		if (0!= titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--

		if (oIntent.hasExtra(Constants.PARAMS_HISTORY_FROM_MAINMENU)) {
			strFromFlag = oIntent.getStringExtra(Constants.PARAMS_HISTORY_FROM_MAINMENU);
		} else {
			strFromFlag = "";
		}


		initResultObj();
		initCount();
		RecIndex=0;
		search();


		LayoutInflater oInflater = LayoutInflater.from(this);
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base_list_freescroll, null);

		Button obtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
		obtn.setVisibility(Button.GONE);
		TextView oText = (TextView)oLayout.findViewById(R.id.inquiry_title);
		LinearLayout oTitleLayout = (LinearLayout) oLayout.findViewById(R.id.LinearLayout01);

		oList = (ScrollList)oLayout.findViewById(R.id.scrollList);
//Del 2011/10/06 Z01_h_yamada Start -->
//		oList.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
		oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));


		if (AppInfo.isEmpty(strFromFlag)) {
//			String sTitle = oIntent.getStringExtra(Constants.FROM_FLAG_KEY);
//			if(sTitle == null)
//			{
			oText.setText(R.string.history_title);
//			}
//			else
//			{
//				oText.setText(this.getResources().getString(R.string.history_title) + sTitle);
//			}
		} else {
			oTitleLayout.setVisibility(LinearLayout.GONE);
			oText.setVisibility(TextView.GONE);
//Chg 2011/09/27 Z01_h_yamada Start -->
//			oLayout.setPadding(10, 45, 10, 45);
//--------------------------------------------
			Resources res = getResources();
			oLayout.setPadding((int)res.getDimension(R.dimen.HIB_Layout_PaddingLeft),
							   (int)res.getDimension(R.dimen.HIB_Layout_PaddingTop),
							   (int)res.getDimension(R.dimen.HIB_Layout_PaddingRight),
							   (int)res.getDimension(R.dimen.HIB_Layout_PaddingBottom));
//Chg 2011/09/27 Z01_h_yamada End <--

		}
//		oBox = (ScrollBox)oLayout.findViewById(R.id.scrollBox);
//		oBox.setAdapter(this);

//
		oList.setAdapter(listAdapter);


		ScrollTool oTool = new ScrollTool(this);
//		oTool.bindView(oBox);
		oTool.bindView(oList);

		setViewInOperArea(oTool);
		setViewInWorkArea(oLayout);

	}
	private ListAdapter listAdapter = new BaseAdapter (){

		@Override
		public int getCount() {
			int iCount = HistoryInquiryBase.this.getCount();
			if (iCount < 5) {
				return 5;
			} else {
				return iCount;
			}

		}

		//yangyang add start
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}
        //yangyang add end

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int wPos, View oView, ViewGroup parent) {

			return HistoryInquiryBase.this.getView(wPos, oView);
		}

	};

	@Override
	protected void search() {

		getRecordIndex = (int)RecIndex / ONCE_GET_COUNT;
		initOnceGetList();
		if (ONCE_GET_COUNT != 0 && RecIndex % ONCE_GET_COUNT != 0) {
			RecIndex = RecIndex/ONCE_GET_COUNT*ONCE_GET_COUNT;
		}
		getRecList(RecIndex) ;
		resetShowList(RecIndex,Rec);
	}


	/**
	 * 表示した施設の内容を取得する
	 * @param recIndex 開始のIndex
	 *
	 * */
	protected void getRecList(long recIndex) {
	}

	/**
	 * JNIからデータを取得するとき、取得したデータの保存対象の初期化
	 * */
	protected void initOnceGetList() {

	}

	/**
	 * 取得したデータによって、表示対象のリセット処理
	 * @param lRecIndex 取得したレコードの最小Index
	 * @param Rec 今回取得したデータの件数
	 *
	 * */
	protected void resetShowList(Long lRecIndex,JNILong Rec) {

	}


	/**
	 * 表示するとき、施設名称
	 * */
	protected String getShowObjName(int wPos) {

		return "";
	}
	/**
	 * 表示するとき、施設経度
	 * */
	protected long getShowObjLon(int wPos) {

		return 0;
	}
	/**
	 * 表示するとき、施設緯度
	 * */
	protected long getShowObjLat(int wPos) {

		return 0;
	}
	/**
	 * 表示するとき、施設日付
	 * */
	protected long getShowObjDate(int wPos) {

		return 0;
	}

	private int getShowIndex(int wPos) {
		if (wPos >= ONCE_GET_COUNT*2) {
			int pageIndex  = wPos/ONCE_GET_COUNT;
			wPos = wPos-ONCE_GET_COUNT*pageIndex+ONCE_GET_COUNT;
		}
		return wPos;
	}
	@Override
	public int getCount() {
		return allRecordCount;
	}

	@Override
	public int getCountInBox() {
		return 5;
	}

	@Override
	public View getView(final int wPos, View oView) {
		LinearLayout oDataLayout = null;
//		int pageIndex = 0;
		ImageView oDelView = null;
		if (oView == null || !(oView instanceof LinearLayout)) {
			LayoutInflater oInflater = LayoutInflater.from(this);
			oDataLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_history, null);
			oView  = oDataLayout;
		} else {
			oDataLayout = (LinearLayout)oView;
		}
		if (AppInfo.isEmpty(strFromFlag)) {
			oDelView = (ImageView) oDataLayout.findViewById(R.id.btn_delete);
			oDelView.setVisibility(ImageView.GONE);
		}
//		if (RecCount.lcount > 0 && wPos%getCountInBox() == 0) {
//			//ページIndexによって、データを取得する
//			//メモリを最優化するため、毎回は5つデータを取得する
//			NaviRun.GetNaviRunObj().JNI_NE_POIHist_GetRecList(RecIndex, getCountInBox(),
//					m_Poi_UserFile_Listitem, Rec);
//		}
		//下にスクロールの場合
		if (wOldPos <= wPos) {
			if (wPos < getCount() && ONCE_GET_COUNT != 0
					&& wPos >= ONCE_GET_COUNT && (wPos % ONCE_GET_COUNT == 0 ||
							(getRecordIndex != wPos / ONCE_GET_COUNT))) {
				//上にスクロール場合
				RecIndex = wPos;
				search();
			}
		}else {
			/*総件数76件、最後ページであるし、上にスクロール時、
			 * 70あるいは６９のデータを表示するとき、グレーボタンを表示しないように
			 * 最後ページ以外の場合、ずっと上と下にスクロールする場合、表示データを再度ダウンロードする
			 */
			if (getRecordIndex != wPos / ONCE_GET_COUNT) {
				RecIndex = wPos;
				search();
			}
		}
		LinearLayout oHistoryLayout = (LinearLayout)oDataLayout.findViewById(R.id.LinearLayout_history);
		TextView obtn = (TextView)oDataLayout.findViewById(R.id.btn_value);
		TextView oDateBtn = (TextView)oDataLayout.findViewById(R.id.btn_date);
		if (wPos < this.getCount() && this.getCount() > 0) {
			//日付を取得する
			String date = initDate(wPos);
			oDateBtn.setText(date);
			//日付+内容を表示する
			int showIndex = getShowIndex(wPos);
			obtn.setText(getShowObjName(showIndex));
			if (AppInfo.isEmpty(getShowObjName(showIndex))) {
				oDateBtn.setText("");
				oHistoryLayout.setEnabled(false);
			} else {
				oHistoryLayout.setEnabled(true);
			}

			if (!AppInfo.isEmpty(strFromFlag)) {
				oDelView = (ImageView) oDataLayout.findViewById(R.id.btn_delete);
				oDelView.setEnabled(true);
			}
		} else {
			oDateBtn.setText("");
			obtn.setText("");
			oHistoryLayout.setEnabled(false);
			if (!AppInfo.isEmpty(strFromFlag)) {
				oDelView = (ImageView) oDataLayout.findViewById(R.id.btn_delete);
				oDelView.setEnabled(false);
			}
		}

		oHistoryLayout.setOnClickListener(new OnClickListener(){

	        @Override
	        public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 履歴 選択禁止 Start -->
				if(DrivingRegulation.CheckDrivingRegulation()){
					return;
				}
// ADD 2013.08.08 M.Honma 走行規制 履歴 選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
//Add 2012/02/07 katsuta Start --> #2698
//				NaviLog.d(NaviLog.PRINT_LOG_TAG, "HistoryInquiryBase onClick ================== bIsListClicked: " + bIsListClicked);
	   			if (bIsListClicked) {
	   				return;
	   			}
	   			else {
	   				bIsListClicked = true;
	   			}
//Add 2012/02/07 katsuta End <--#2698

	        	RecIndex = wPos;
	        	search();
	        	int showIndex = getShowIndex(wPos);
	        	POIData oData = new POIData();
                oData.m_sName = getShowObjName(showIndex);//poi.getM_DispName();
                oData.m_sAddress = getShowObjName(showIndex);//poi.getM_DispName();
                oData.m_wLong = getShowObjLon(showIndex);//poi.getM_lLon();
                oData.m_wLat = getShowObjLat(showIndex);//poi.getM_lLat();
             // XuYang add start #1056
                Intent oIntent = getIntent();
                if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
                    CommonLib.setChangePosFromSearch(true);
                    CommonLib.setChangePos(false);
                } else {
                    CommonLib.setChangePosFromSearch(false);
                    CommonLib.setChangePos(false);
                }
                // XuYang add end #1056
                OpenMap.moveMapTo(HistoryInquiryBase.this,oData, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
                //waitDialogを追加する
                createDialog(Constants.DIALOG_WAIT, -1);
//	        	}
	        }
	    });

		if (!AppInfo.isEmpty(strFromFlag)) {
			oDelView = (ImageView) oDataLayout.findViewById(R.id.btn_delete);
			oDelView.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 履歴編集 削除禁止 Start -->
					if(DrivingRegulation.CheckDrivingRegulation()){
						return;
					}
// ADD 2013.08.08 M.Honma 走行規制 履歴編集 削除禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
					clickWPos = wPos;
					showDialog(Constants.DIALOG_DELETE_HISTORY);

				}});
		}

		//上にスクロールの場合
		if (wOldPos > wPos) {
			if (wPos < getCount() && ONCE_GET_COUNT != 0
					&& wPos >= ONCE_GET_COUNT && (wPos % ONCE_GET_COUNT == 0 ||
							(getRecordIndex != wPos / ONCE_GET_COUNT))) {
				//上にスクロール場合
				RecIndex = wOldPos - ONCE_GET_COUNT;

				search();
			}
		}
		wOldPos = wPos;
		return oView;
	}
	/**
	 * 履歴日付をフォーマットする
	 * @param dataIndex JNIから取得した日付
	 * @return String フォーマットしてから日付を戻す
	 *
	 * */
	private String initDate(int dataIndex) {
		int showIndex = getShowIndex(dataIndex);
		long nYear = (((getShowObjDate(showIndex) & 0xf0000000)>>28) * 1000) +
		(((getShowObjDate(showIndex) & 0x0f000000)>>24) * 100) +
		(((getShowObjDate(showIndex) & 0x00f00000)>>20) * 10) +
		((getShowObjDate(showIndex) & 0x000f0000)>>16);
		long nMonth = (((getShowObjDate(showIndex) & 0x0000f000)>>12) * 10) +
		((getShowObjDate(showIndex) & 0x00000f00)>>8);
		long nDay = (((getShowObjDate(showIndex) & 0x000000f0)>>4) * 10) +
		(getShowObjDate(showIndex) & 0x000000f);
		String sYear = (String.valueOf(nYear).length()==2)?"20" + String.valueOf(nYear):String.valueOf(nYear);
		String sMonth=(String.valueOf(nMonth).length()>1)?String.valueOf(nMonth):"0"+String.valueOf(nMonth);
		String sDay=(String.valueOf(nDay).length()>1)?String.valueOf(nDay):"0"+String.valueOf(nDay);
		return sYear + "/" + sMonth + "/" + sDay;
	}

	/**
	 * 総件数を取得する
	 * */
	protected void initCount() {

	}

	/**
	 * メモリを申請する
	 * */
	protected void initResultObj() {

	}
	/**
	 * Listを削除する
	 * @param int index   索引
	 */
	private void deleteItem(int index){
		NaviRun.GetNaviRunObj().JNI_NE_POIHist_RemoveRec(index);
		initCount();
		if (this.getCount() == 0) {
			this.setResult(Activity.RESULT_OK, getIntent());
			finish();
		} else {
			RecIndex = 0;
			search();
			oList.reset();
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		//履歴削除のダイアログ
		if (id == Constants.DIALOG_DELETE_HISTORY) {
			CustomDialog oDialog = new CustomDialog(this);
			oDialog.setTitle(R.string.history_delete_title);
			int showIndex = getShowIndex(clickWPos);
			String msg = CommonMethd.reSetMsg(getShowObjName(showIndex),this.getResources().getString(R.string.history_delete_msg));
			oDialog.setMessage(msg);
			oDialog.addButton(this.getResources().getString(R.string.btn_ok),new OnClickListener(){

				@Override
				public void onClick(View v) {
					removeDialog(Constants.DIALOG_DELETE_HISTORY);
					deleteItem((int)(clickWPos));
//					oBox.refresh();

				}});
			oDialog.addButton(R.string.btn_cancel, new OnClickListener(){

				@Override
				public void onClick(View v) {
					removeDialog(Constants.DIALOG_DELETE_HISTORY);

				}});
			return oDialog;

		}
		return super.onCreateDialog(id);
	}

	@Override
	protected boolean onClickGoBack() {
		initClear();
		return super.onClickGoBack();
	}

	@Override
	protected boolean onClickGoMap() {
		initClear();
		return super.onClickGoMap();
	}

	@Override
	protected boolean onStartShowPage() throws Exception {
		return false;
	}

	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {
	}
}
