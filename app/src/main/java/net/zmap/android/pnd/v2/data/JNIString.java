/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           JNIJumpKey.java
 * Description    String Class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class JNIString {
	private String m_StrAddressString;


	/**
	 * Stringの値を取得する
	 * @return Stringの値
	 */
	public String getM_StrAddressString() {
		return m_StrAddressString;
	}

}
