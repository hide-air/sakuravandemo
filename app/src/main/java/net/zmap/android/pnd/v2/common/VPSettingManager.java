/**
 *  @file       VPSettingManager.java
 *  @brief
 *
 *  @attention
 *  @note
 *
 *  @author     Nguyen The Doanh [Z01]
 *  @date       $Date:: 2011-05-12 00:00:00 +0900 #$ (Create at 2011-05-12)
 *  @version    $Revision: $ by $Author: $
 */
package net.zmap.android.pnd.v2.common;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import net.zmap.android.pnd.v2.common.utils.NaviLog;

public class VPSettingManager {
//Chg 2011/06/22 Z01thedoanh Start -->
    //private final static String VPSETTING_FILENAME = "/sdcard/zdcpnd/User/VPSetting.ini";
    private final static String VPSETTING_FILENAME = "VPSetting.ini"; //VPSetting名称
    private static String USER_BASEDIR = null; //Userフォルダのパス
//Chg 2011/06/22 Z01thedoanh End <--
    public static int VP_Enable_VPSensorLog_ListPlay = 0; //VPセンサーログのリスト再生機能切り替え (0:無効 1:有効)

    public static final int OFF = 0;
    public static final int ON = 1;
    private int VP_LoadMode = -1;
    private int VP_EnableOutput_VPSensorLog = -1;
    private int VP_Enable_MMLog = -1;
    private int VP_Enable_Monitor = -1;
//Add 2011/08/26 Z01yoneya Start -->
    //GPS信頼度を判断する方法
    public static final int GPS_RELIABILITY_JUDGE_GPS_ACCURACY = 0; //GPS誤差円半径
    public static final int GPS_RELIABILITY_JUDGE_GPS_SNR = 1; //測位使用GPSのSNR平均値
    public static final int GPS_RELIABILITY_JUDGE_GPS_COUNT = 2; //測位使用GPS数
    private static int VP_ReliabilityJudgeMode = GPS_RELIABILITY_JUDGE_GPS_ACCURACY; //判断方法の初期値
    private static float VP_ReliabilityJudgeSnrValue = 25.0f; //判断SNRの初期値
    private static float VP_ReliabilityJudgeAccuracyValue = 10.0f; //判断accuracyの初期値
//Add 2011/08/26 Z01yoneya End <--
    private static VPSettingManager mSingleton = new VPSettingManager();

    private VPSettingManager() {
    }

//Chg 2011/09/26 Z01yoneya Start -->
//	public static void init(Context context) {
//		mSingleton = new VPSettingManager();
//		mSingleton.VPSettingInitializes();
//	}
//
//    private void VPSettingInitializes() {
//
//        InputStream is = null;
//        BufferedReader br = null;
//
//        try {
////Chg 2011/06/22 Z01thedoanh Start --> VPSettingのフルパス設定
//        	//is = new FileInputStream(VPSETTING_FILENAME);
//        	String[] naviResFilePath = CommonLib.getNaviResFilePath();
//    		String[] userPath = naviResFilePath[4].split(" ");// USER_PATH
//    		USER_BASEDIR = userPath[2];
//            is = new FileInputStream(USER_BASEDIR + VPSETTING_FILENAME);
////Chg 2011/06/22 Z01thedoanh End <--
//------------------------------------------------------------------------------------------------------------------------------------
    public static void init(String userDataPath) {
        mSingleton.VPSettingInitializes(userDataPath);
    }

    private void VPSettingInitializes(String userDataPath) {

        InputStream is = null;
        BufferedReader br = null;

        try {
            USER_BASEDIR = userDataPath;
            is = new FileInputStream(USER_BASEDIR + VPSETTING_FILENAME);
//Chg 2011/09/26 Z01yoneya End <--

            br = new BufferedReader(new InputStreamReader(is));
            if (br != null) {
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    //Tab(\t)と前後のスペース（全角、半角）を削除
                    line = CustomTrim(line);
                    if (line.length() > 0) {
                        if (!line.substring(0, 1).equals(";") && !line.substring(0, 1).equals("[")) {
                            if (line.split("=")[0].trim().equals("VP_LoadMode")) {
                                VP_LoadMode = Integer.parseInt(line.split("=")[1].trim());
                            }
                            //VPセンサーログ出力の切り替え (0:無効 1:有効)
                            if (line.split("=")[0].trim().equals("VP_EnableOutput_VPSensorLog")) {
                                VP_EnableOutput_VPSensorLog = Integer.parseInt(line.split("=")[1].trim());
                            }
                            //VPセンサーログのリスト再生機能切り替え (0:無効 1:有効)
                            if (line.split("=")[0].trim().equals("VP_Enable_VPSensorLog_ListPlay")) {
                                VP_Enable_VPSensorLog_ListPlay = Integer.parseInt(line.split("=")[1].trim());
                            }
                            //MMLog出力の切り替え (0:無効 1:有効)
                            if (line.split("=")[0].trim().equals("VP_Enable_MMLog")) {
                                VP_Enable_MMLog = Integer.parseInt(line.split("=")[1].trim());
                            }
                            //モニタ機能の切り替え (0:無効 1:有効)
                            if (line.split("=")[0].trim().equals("VP_Enable_BTMon")) {
                                VP_Enable_Monitor = Integer.parseInt(line.split("=")[1].trim());
                            }
//Add 2011/08/29 Z01yoneya Start -->
                            //GPS信頼度判定方法(0:Accuracy 1:SNR 2:捕捉衛星数)
                            if (line.split("=")[0].trim().equals("VP_Gps_ReliabilityJudgeMode")) {
                                VP_ReliabilityJudgeMode = Integer.parseInt(line.split("=")[1].trim());
                            }
                            //GPS信頼度判定方法がAccuracyモードの時の良好判定しきい値(メートル)
                            if (line.split("=")[0].trim().equals("VP_Accuracy_FineValue")) {
                                VP_ReliabilityJudgeAccuracyValue = Float.parseFloat(line.split("=")[1].trim());
                            }
                            //GPS信頼度判定方法がSNRモードの時の良好判定しきい値
                            if (line.split("=")[0].trim().equals("VP_Snr_FineValue")) {
                                VP_ReliabilityJudgeSnrValue = Float.parseFloat(line.split("=")[1].trim());
                            }

//Add 2011/08/29 Z01yoneya End <--
                        }
                    }
                }
            }
//Add 2011/11/17 Z01_h_yamada Start -->
        } catch (FileNotFoundException e) {
			NaviLog.i(NaviLog.PRINT_LOG_TAG, "file not found for VPSetting.ini");
//Add 2011/11/17 Z01_h_yamada End <--
        } catch (Exception e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
    				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
    				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }
        }
// 		Log.i("[DEBUG]", "[VPSetting_J] VP_LoadMode="+String.valueOf(VP_LoadMode));
// 		Log.i("[DEBUG]", "[VPSetting_J] VP_Enable_VPSensorLog_ListPlay="+String.valueOf(VP_Enable_VPSensorLog_ListPlay));
// 		Log.i("[DEBUG]", "[VPSetting_J] VP_EnableOutput_VPSensorLog="+String.valueOf(VP_EnableOutput_VPSensorLog));
// 		Log.i("[DEBUG]", "[VPSetting_J] VP_Enable_MMLog="+String.valueOf(VP_Enable_MMLog));
// 		Log.i("[DEBUG]", "[VPSetting_J] VP_Enable_BTMon="+String.valueOf(VP_Enable_Monitor));

        if (VP_Enable_VPSensorLog_ListPlay == 1) {//リスト再生を有効にすると、VPセンサーログ再生に切り替える
            VP_LoadMode = 1;
        }

        //VPの再生モード 0:GPS/センサー 1:VPセンサーログ 2:GPSログ
        switch (VP_LoadMode) {
            case 0:
                //GPS/センサー
//Del 2011/08/29 Z01yoneya Start -->
//        	Constants.LoadMode_GPS = 0x00000000;
//Del 2011/08/29 Z01yoneya End <--
                Constants.LoadMode_SENSOR = ON; //SensorManagerを起動する
                Constants.VP_LoadMode_LocationManagerLOG = 0x00000000; //LocationManagerログ無効
                Constants.VP_LoadMode_LoggerManager = OFF; //LoggerManagerを無効
                break;
            case 1:
                //Logger.txt(VPセンサーログ)を使用する
//Del 2011/08/29 Z01yoneya Start -->
//            Constants.LoadMode_GPS = 0x00000000;					//GPS無効
//Del 2011/08/29 Z01yoneya End <--
                Constants.LoadMode_SENSOR = OFF; //センサー無効
                Constants.VP_LoadMode_LocationManagerLOG = 0x00000000; //LocationManagerログ無効
                Constants.LoadMode_LOG = ON; //ログ設定を有効にする
                Constants.VP_LoadMode_LoggerManager = ON; //VPLoggerManager有効
                break;
            case 2:
                //GPSログ(NEU定義のVPログ)
//Del 2011/08/29 Z01yoneya Start -->
//        	Constants.LoadMode_GPS = 0x00000000;					//GPS無効
//Del 2011/08/29 Z01yoneya End <--
                Constants.LoadMode_SENSOR = OFF; //センサー無効（SensorManagerを起動する）
                Constants.LoadMode_LOG = ON; //ログ設定を有効にする
                Constants.VP_LoadMode_LocationManagerLOG = 0x00000002; //VP_LocationManagerLOG有効
                Constants.VP_LoadMode_LoggerManager = OFF; //VPLoggerManagerを無効
                break;
            default:
//            Constants.LoadMode_GPS = 0x00000000;					//GPSデバイス(未使用)を無効
//            Constants.LoadMode_LOG = ON;							//ログ設定を有効にする
//            Constants.LoadMode_SENSOR = OFF;						//SensorManagerを無効
//            Constants.VP_LoadMode_LocationManagerLOG = 0x00000000;	//LocationManagerログを無効
//            Constants.VP_LoadMode_LoggerManager = ON;				//LoggerManagerを起動する(LocationManagerは無効)
//            Constants.VPLOG_OUTPUT = 0x00000000;					//VPログを出力しない
//            Constants.VPLOG_OUTPUT_LOGGER = OFF;					//センサーログを出力しない
//            Constants.VPLOG_OUTPUT_MONITOR = OFF;					//センサーモニタ機能を使用しない
//            Constants.VP_LocationCorrection = 0x00000000;
                break;
        }
        //VPセンサーログ出力の切り替え (0:無効 1:有効)
        switch (VP_EnableOutput_VPSensorLog) {
            case 0:
                Constants.VPLOG_OUTPUT_LOGGER = OFF; //センサーログを出力しない
                break;
            case 1:
//        	if(VP_LoadMode != 0){
//        		Constants.VPLOG_OUTPUT_LOGGER = ON;					//センサーログを出力する
//        	}else{
//        		Constants.VPLOG_OUTPUT_LOGGER = OFF;				//センサーログを出力しない
//        	}
                Constants.LoadMode_LOG = ON; //ログ設定を有効にする
                Constants.VPLOG_OUTPUT_LOGGER = ON; //センサーログを出力する
                break;
            default:
                break;
        }
        //モニタ機能の切り替え (0:無効 1:有効)
        switch (VP_Enable_Monitor) {
            case 0:
                Constants.VPLOG_OUTPUT_MONITOR = OFF; //センサーモニタ機能を使用しない
                break;
            case 1:
                Constants.VPLOG_OUTPUT_MONITOR = ON; //センサーモニタ機能を使用する
                break;
            default:
                break;
        }

//        Log.i("[DEBUG]", "[Constants] LoadMode_LOG="+String.valueOf(Constants.LoadMode_LOG));
//        Log.i("[DEBUG]", "[Constants] LoadMode_SENSOR="+String.valueOf(Constants.LoadMode_SENSOR));
//        Log.i("[DEBUG]", "[Constants] VP_LoadMode_LoggerManager="+String.valueOf(Constants.VP_LoadMode_LoggerManager));
//        Log.i("[DEBUG]", "[Constants] VP_LoadMode_LocationManagerLOG="+String.valueOf(Constants.VP_LoadMode_LocationManagerLOG));
//        Log.i("[DEBUG]", "[Constants] VPLOG_OUTPUT_LOGGER="+String.valueOf(Constants.VPLOG_OUTPUT_LOGGER));
//        Log.i("[DEBUG]", "[Constants] VPLOG_OUTPUT_MONITOR="+String.valueOf(Constants.VPLOG_OUTPUT_MONITOR));

    }

    /**
     * 文字列の前後からスペース(全角半角、改行、タブなど)を削除した文字列を取得
     *
     * @param string
     */
    public String CustomTrim(String string) {
        String str_tmp;
        str_tmp = string;
        StringBuilder sb;
        final char ZENKAKU_SPACE = '　';
        final char HANKAKU_SPACE = ' ';

        str_tmp = str_tmp.replaceAll("\\t", ""); //Tabの削除

        if (str_tmp == null || str_tmp.length() == 0) {
            return "";
        }
        sb = new StringBuilder(str_tmp);
        int len = sb.length(); //文字列の長さ
        //最後の文字は半角･全角スペースならば1文字ずつを削除
        //while(sb.charAt(len-1) == ' ' || sb.charAt(len-1) == '　'){
        while (sb.charAt(len - 1) == ZENKAKU_SPACE || sb.charAt(len - 1) == HANKAKU_SPACE) {
            sb.deleteCharAt(len - 1);
            len = sb.length();
        }

        sb = new StringBuilder(sb.toString());
        //先頭の文字は半角･全角スペースならば1文字ずつを削除
        while (sb.charAt(0) == ZENKAKU_SPACE || sb.charAt(0) == HANKAKU_SPACE) {
            sb.deleteCharAt(0);
        }
        return sb.toString();

    }

//Add 2011/08/26 Z01yoneya Start -->
    /*
     * GPS信頼度を判定するモード取得
     */
    public static int getGpsReliabilityJudgeMode() {
        return VP_ReliabilityJudgeMode;
    }

    /*
     * GPSをSNRで判定する時のしきい値取得
     */
    public static float getGpsReliabilityJudgeSnrValue() {
        return VP_ReliabilityJudgeSnrValue;
    }

    /*
     * GPSをaccuracyで判定する時のしきい値取得
     */
    public static float getGpsReliabilityJudgeAccuracyValue() {
        return VP_ReliabilityJudgeAccuracyValue;
    }
//Add 2011/08/26 Z01yoneya End <--

}
