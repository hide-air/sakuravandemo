package net.zmap.android.pnd.v2.route.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.activity.MenuBaseActivity;
import net.zmap.android.pnd.v2.inquiry.view.InputText;

/**
 * ルート名称編集
 *
 * @author XuYang
 *
 */
public class RouteNameEdit  extends MenuBaseActivity {

	private InputText oView = null;
	private Button oLoadBtn = null;
//Del 2011/12/15 Z01_h_yamada Start -->
//	private int DIALOG_CONFIRM = 1;
//Del 2011/12/15 Z01_h_yamada End <--

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//Del 2011/09/17 Z01_h_yamada Start -->
//		setMenuTitle(R.drawable.title_route_name_edit);
//Del 2011/09/17 Z01_h_yamada End <--
		oView = new InputText(this,0,oLoadBtn);
		//TwoBtnAndScrollView oBtnView = new TwoBtnAndScrollView(this, oView.getTextObj());
		initTwoBtn(oView);
		oView.setType(oView.FAVORITES);
		oView.setMinLimited(1,oLoadBtn);
		oView.setMaxLimited(40,oLoadBtn);

		Intent oIntent = getIntent();
		String sValue = oIntent.getStringExtra(Constants.FLAG_TITLE);
		oView.getTextObj().setText(sValue);

//		InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
//		imm.showSoftInput(oView.getTextObj(), 0);

		LinearLayout ocountView = (LinearLayout)oView.findViewById(this, R.id.LinearLayout_listcount);
		ocountView.setVisibility(TextView.GONE);
		oView.getTextObj().setFilters(new InputFilter[]{new InputFilter.LengthFilter(40)});
//		setViewInOperArea(oBtnView.getView());
		setViewInWorkArea(oView.getView());

//Add 2011/07/05 Z01thedoanh Start -->bug #1146: OSキーボード使用時に「完了」ボタンが使えるようにする
		 final InputMethodManager imm =
			 (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
		 imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
		 if(oView.getTextObj() != null){
			 oView.getTextObj().setOnEditorActionListener(new OnEditorActionListener() {
				 public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					 if (actionId == EditorInfo.IME_ACTION_DONE){
						 imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
					 }
					 return true;
				 }
			 });
		 }
//Add 2011/07/05 Z01thedoanh End <--
	}
	@Override
	protected void onStart() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStart");
//Del 2011/07/05 Z01thedoanh Start -->
		//InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//Del 2011/07/05 Z01thedoanh End <--
//		View view = this.getCurrentFocus();
//	    if (view != null){
//Del 2011/07/05 Z01thedoanh Start -->
        	//imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//Del 2011/07/05 Z01thedoanh End <--
//	    }
//	    imm.getInputMethodList()

		super.onStart();
	}

	@Override
	protected void onPause() {
		AppInfo.hiddenSoftInputMethod(this);
		super.onPause();
	}

	@Override
	protected void onStop()
	{
		AppInfo.hiddenSoftInputMethod(this);
		super.onStop();
	}
	/**
	 * ボタンの初期化
	 * @param oBtnView
	 */
	private void initTwoBtn(InputText oBtnView) {
//		Button btnEdit = (Button) oBtnView.findViewById(this, R.id.Button_edit);
//		btnEdit.setOnClickListener(onEditClickListener);
		oLoadBtn = (Button) oBtnView.findViewById(this, R.id.Button_search);
		Resources oRes = getResources();
		oLoadBtn.setText(oRes.getString(R.string.btn_load));
		oLoadBtn.setOnClickListener(onLoadClickListener);
//		oLoadBtn.setEnabled(false);

	}
//	OnClickListener onEditClickListener = new OnClickListener(){
//
//		@Override
//		public void onClick(View v) {
//			if (!AppInfo.isEmpty(oView.getText())) {
//				oView.setFlag_AddAndDel(true);
//
//				StringBuffer sb = new StringBuffer(oView.getText());
//				int selectionEnd = oView.getTextObj().getSelectionEnd();
//				if (selectionEnd == 0) {
//					sb.deleteCharAt(selectionEnd);
//				} else {
//					sb.deleteCharAt(selectionEnd-1);
//				}
//				oView.getTextObj().setText(sb.toString());
//				if (selectionEnd == 0) {
//					oView.getTextObj().setSelection(selectionEnd);
//				} else {
//					oView.getTextObj().setSelection(selectionEnd - 1);
//				}
//
//			}
//
//		}
//
//	};
	private OnClickListener onLoadClickListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//			AppInfo.hiddenSoftInputMethod(RouteNameEdit.this);
//Del 2011/09/08 Z01_h_yamada End <--
//Add 2011/10/15 Z01_h_yamada Start -->
			AppInfo.hiddenSoftInputMethod(RouteNameEdit.this);
//Add 2011/10/15 Z01_h_yamada End <--
//Chg 2011/12/15 Z01_h_yamada Start -->
//			showDialog(DIALOG_CONFIRM);
//--------------------------------------------
			Intent intEditName = new Intent();
			intEditName.putExtra(Constants.ROUTE_ROUTE_NAME_KEY, oView.getText());
			setResult(Activity.RESULT_OK, intEditName);
			finish();
//Chg 2011/12/15 Z01_h_yamada End <--
		}
	};
//Del 2011/12/15 Z01_h_yamada Start -->
//	/**
//	 * ダイアログの初期
//	 */
//	@Override
//    protected Dialog onCreateDialog(int id) {
//		final CustomDialog oDialog = new CustomDialog(this);
//		Resources oRes = getResources();
//		if (id == DIALOG_CONFIRM) {
//			oDialog.setTitle(oRes.getString(R.string.title_route_save));
//	        oDialog.setMessage("「" + oView.getText() +
//	        		oRes.getString(R.string.msg_save_confirm));
//
//	        oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					Intent intEditName = new Intent();
//					intEditName.putExtra(Constants.ROUTE_ROUTE_NAME_KEY, oView.getText());
//					setResult(Activity.RESULT_OK, intEditName);
//					finish();
//				}
//	        });
//	    }
//		// XuYang add start 走行中の操作制限
//        else {
//            return super.onCreateDialog(id);
//        }
//        // XuYang add end 走行中の操作制限
//		return oDialog;
//	}
//Del 2011/12/15 Z01_h_yamada End <--
}
