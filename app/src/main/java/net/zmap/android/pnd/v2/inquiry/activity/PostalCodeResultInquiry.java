//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************
package net.zmap.android.pnd.v2.inquiry.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.ScrollBoxAdapter;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_ZipCode_ListItem;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.maps.OpenMap;

/**
 * 郵便検索結果画面
 *
 * */
public class PostalCodeResultInquiry extends InquiryBase implements ScrollBoxAdapter{

	private static PostalCodeResultInquiry instance;
	/** データを取得するとき、開始のインディクス */
	private long RecIndex = 0;
	/** ページのインディクス */
//	private int pageIndex = 0;
	/** スクロールできる総件数 */
	private int allRecordCount = 0;
	/** JNIの出力引数 */

	private JNILong Rec = new JNILong();
	/** JNIから取得したデータ */
	private POI_ZipCode_ListItem[] m_Poi_PostalCode_Listitem = null;
	/** 表示した内容 */
	private POI_ZipCode_ListItem[] m_Poi_Show_Listitem = null;
	private int getRecordIndex = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		Intent oIntent = getIntent();
//Del 2011/09/17 Z01_h_yamada Start -->
//		//タイトルを設定する
//		int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE,0);
//		if (0!= titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--

		initDataObj();
		initCount();
		RecIndex=0;
		search();



		instance = this;
		//画面を初期化する
		LayoutInflater oInflater = LayoutInflater.from(this);
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base_list_freescroll, null);
		Button obtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
		obtn.setVisibility(Button.GONE);
		TextView oText = (TextView)oLayout.findViewById(R.id.inquiry_title);
		//検索内容を初期化する
		String sTitle = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
		if(sTitle == null)
		{
			oText.setText(R.string.postalcode_inquiry_title);
		}
		else
		{
			oText.setText(this.getResources().getString(R.string.postalcode_inquiry_title) + sTitle);
		}

//		final ScrollBox oBox = (ScrollBox)oLayout.findViewById(R.id.scrollBox);
//		oBox.setAdapter(this);
		ScrollList oList = (ScrollList)oLayout.findViewById(R.id.scrollList);
//Del 2011/10/06 Z01_h_yamada Start -->
//		oList.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
		oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
		oList.setAdapter(listAdapter);

		ScrollTool oTool = new ScrollTool(this);
//		oTool.bindView(oBox);
		oTool.bindView(oList);
		setViewInOperArea(oTool);

		this.setViewInWorkArea(oLayout);
	}

	private void initCount() {
		JNILong ListCount = new JNILong();
		NaviRun.GetNaviRunObj().JNI_NE_POIZip_GetRecCount(ListCount);
		showToast(this,ListCount);
		allRecordCount = initPageIndexAndCnt(ListCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"allRecordCount====" + allRecordCount);
	}

	public static PostalCodeResultInquiry getInstance() {
		return instance;
	}
	@Override
	public int getCount() {

		return allRecordCount;
	}

	@Override
	protected void search() {

		getRecordIndex = (int)RecIndex / ONCE_GET_COUNT;
		initOnceGetList();
		if (ONCE_GET_COUNT != 0 && RecIndex % ONCE_GET_COUNT != 0) {
			RecIndex = RecIndex/ONCE_GET_COUNT*ONCE_GET_COUNT;
		}
		NaviRun.GetNaviRunObj().JNI_NE_POIZip_GetRecList(RecIndex,ONCE_GET_COUNT, m_Poi_PostalCode_Listitem, Rec);
		resetShowList(RecIndex,Rec);
	}
	private void initOnceGetList() {
		m_Poi_PostalCode_Listitem = new POI_ZipCode_ListItem[ONCE_GET_COUNT];
		for (int i = 0 ; i < ONCE_GET_COUNT ; i++) {
			m_Poi_PostalCode_Listitem[i] = new POI_ZipCode_ListItem();
		}

	}

	private void initDataObj() {
		//メモリを申請する
		initOnceGetList();
		m_Poi_Show_Listitem = new POI_ZipCode_ListItem[ONCE_GET_COUNT*2];
		for (int i = 0 ; i < ONCE_GET_COUNT*2 ; i++) {
			m_Poi_Show_Listitem[i] = new POI_ZipCode_ListItem();
		}

	}

	private ListAdapter listAdapter = new BaseAdapter (){

		@Override
		public int getCount() {
			int iCount = PostalCodeResultInquiry.this.getCount();
			if (iCount <5) {
				return 5;
			} else {
				return iCount;
			}

		}

		// yangyang add start
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		// yangyang add end

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int wPos, View oView, ViewGroup parent) {

//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"getView wPos--------------->" + wPos);
			return PostalCodeResultInquiry.this.getView(wPos, oView);
		}

	};
	@Override
	public int getCountInBox() {
		return 5;
	}
	int wOldPos = 0;
	@Override
	public View getView(final int wPos, View oView) {
		LinearLayout oLayout = null;
		Button obtn = null;
		if(oView == null || !(oView instanceof LinearLayout))
		{
//			if (wPos == 0) {
//
//				RecIndex=0;
//
//
//			}
			oLayout = new LinearLayout(this);
			oLayout.setGravity(Gravity.CENTER_VERTICAL);
			oLayout.setPadding(5, 5, 5, 5);
			obtn = new Button(this);
			obtn.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
			obtn.setId(R.id.btnOk);
//Chg 2011/11/01 Z01_h_yamada Start -->
//			obtn.setTextSize(26);
//--------------------------------------------
			obtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.Common_Medium_textSize));
//Chg 2011/11/01 Z01_h_yamada End <--
			obtn.setBackgroundResource(R.drawable.btn_default);
			//前から「...」とする
			obtn.setSingleLine(true);
			obtn.setEllipsize(TruncateAt.START);
			oLayout.addView(obtn);
			oView = oLayout;
		}
		else
		{
//			if (wPos%getCountInBox() == 0) {
//				//ページIndexを取得する
//				pageIndex = wPos/getCountInBox();
//				RecIndex = getCountInBox() * pageIndex;
//			}
			oLayout = (LinearLayout)oView;
			obtn = (Button)oLayout.findViewById(R.id.btnOk);
		}
//		if (wPos%getCountInBox() == 0) {
//			//ページIndexによって、データを取得する
//			//メモリを最優化するため、毎回は5つデータを取得する
//			NaviRun.GetNaviRunObj().JNI_NE_POIZip_GetRecList(RecIndex,getCountInBox(), m_Poi_PostalCode_Listitem, Rec);
//		}

		//下にスクロールの場合
		if (wOldPos <= wPos) {
			if (wPos < getCount() && ONCE_GET_COUNT != 0
					&& (wPos % ONCE_GET_COUNT == 0 ||
							(getRecordIndex != wPos / ONCE_GET_COUNT))) {
				//上にスクロール場合
				RecIndex = wPos;
				search();
			}
		}else {
			/*総件数76件、最後ページであるし、上にスクロール時、
			 * 70あるいは６９のデータを表示するとき、グレーボタンを表示しないように
			 */
			if (wOldPos+1 == this.getCount() && (getRecordIndex != wPos / ONCE_GET_COUNT)) {
				RecIndex = wPos;
				search();
			}
		}


//		obtn.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
//		obtn.setTextSize(26);
//		obtn.setBackgroundResource(R.drawable.btn_548_70);
//		obtn.setSingleLine(true);
//		obtn.setEllipsize(TruncateAt.START);
		if (wPos < this.getCount()) {
//			obtn.setText(m_Poi_PostalCode_Listitem[wPos%getCountInBox()].getM_Name());

			if (getShowObj(wPos).getM_Name() == null || "".equals(getShowObj(wPos).getM_Name())) {
				obtn.setText( getShowObj(wPos).getM_FullZipCode() + this.getResources().getString(R.string.str_around));
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"getShowObj(wPos).getM_Tel()===" + getShowObj(wPos).getM_FullTel());
			} else {
				obtn.setText(getShowObj(wPos).getM_Name());// + "(" + getShowObj(wPos).getM_FullZipCode() + ")");
			}


			obtn.setEnabled(true);
		} else {
			obtn.setText("");
			obtn.setEnabled(false);
		}
		obtn.setPadding(10, 12, 10, 10);
		obtn.setOnClickListener(new OnClickListener(){

	        @Override
	        public void onClick(View v) {
//	        	POI_ZipCode_ListItem poi = m_Poi_PostalCode_Listitem[wPos%getCountInBox()];
	        	RecIndex = wPos;
				search();
	        	POI_ZipCode_ListItem poi = getShowObj(wPos);
//	            long lon = poi.getM_lLon();
//                long lat = poi.getM_lLat();
//                String address = poi.getM_Name();
//                openMap(null, lon, lat, address);
                POIData oData = new POIData();
                if (poi.getM_Name() == null || "".equals(poi.getM_Name())) {
	            	oData.m_sName = poi.getM_FullZipCode() +
	            	getResources().getString(R.string.str_around);
	            } else {
	            	oData.m_sName = poi.getM_Name() ;//+ "(" + poi.getM_FullZipCode() + ")";
	            }
	            if (getCount() != 1) {
	            	oData.m_sAddress = poi.getM_Name();
	            }
                oData.m_wLong = poi.getM_lLon();
                oData.m_wLat = poi.getM_lLat();
             // XuYang add start #1056
                Intent oIntent = getIntent();
                if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
                    CommonLib.setChangePosFromSearch(true);
                    CommonLib.setChangePos(false);
                } else {
                    CommonLib.setChangePosFromSearch(false);
                    CommonLib.setChangePos(false);
                }
                // XuYang add end #1056
                OpenMap.moveMapTo(PostalCodeResultInquiry.this,oData, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
	        }
	    });

		//上にスクロールの場合
		if (wOldPos > wPos) {
			if (wPos < getCount() && ONCE_GET_COUNT != 0
					&& wPos >= ONCE_GET_COUNT && (wPos % ONCE_GET_COUNT == 0 ||
							(getRecordIndex != wPos / ONCE_GET_COUNT))) {
	//			getRecordIndex = wPos/ONCE_GET_COUNT;
				//上にスクロール場合

				RecIndex = wOldPos-ONCE_GET_COUNT;

				search();
			}
		}
		wOldPos = wPos;
		return oView;
	}

	private POI_ZipCode_ListItem getShowObj(int wPos) {

		int showIndex = getShowIndex(wPos);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"showIndex=========" + showIndex);
		return m_Poi_Show_Listitem[showIndex];
	}

	private int getShowIndex(int wPos) {
		if (wPos >= ONCE_GET_COUNT*2) {
			int pageIndex  = wPos/ONCE_GET_COUNT;
			wPos = wPos-ONCE_GET_COUNT*pageIndex+ONCE_GET_COUNT;
		}
		return wPos;
	}

	private void resetShowList(Long lRecIndex,JNILong Rec) {
		if (lRecIndex < ONCE_GET_COUNT) {
			int len = m_Poi_PostalCode_Listitem.length;
			for (int i = 0 ; i < len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_PostalCode_Listitem[i];
			}
		} else {
			if (lRecIndex >= ONCE_GET_COUNT*2) {
				for (int i = m_Poi_PostalCode_Listitem.length-ONCE_GET_COUNT; i < ONCE_GET_COUNT*2 ; i++) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"resetShowList i===" + i);
					if (i <ONCE_GET_COUNT) {
						m_Poi_Show_Listitem[i] = m_Poi_Show_Listitem[i+ONCE_GET_COUNT];
					} else {
						m_Poi_Show_Listitem[i] = new POI_ZipCode_ListItem();
					}
				}
			}

			int len = (int)Rec.lcount;
			int j = 0;
			for (int i = ONCE_GET_COUNT ; i < ONCE_GET_COUNT + len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_PostalCode_Listitem[j];
//				System.out.println(m_Poi_Show_Listitem[i].getM_Name());
				j++;
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
//			NaviRun.GetNaviRunObj().JNI_NE_POITel_Cancel();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected boolean onClickGoMap() {
		NaviRun.GetNaviRunObj().JNI_NE_POIZip_Clear();//.JNI_NE_POITel_Clear();
		return super.onClickGoMap();
	}
}
