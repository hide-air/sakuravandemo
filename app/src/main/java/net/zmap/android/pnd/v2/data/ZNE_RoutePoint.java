/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNE_RouteInfoGuidePoint_t.java
 * Description    ルート案内情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNE_RoutePoint
{
    private String          szPointName;    // 経路探索地点名称
    private JNITwoLong      stPos;          // 経緯度
    private int             iPassed;        // 通過済みフラグ
    private int             iPointType;     // 地点タイプ
    /**
     * <p>Description:[...]</p>
     * @param szPointName The szPointName to set.
     */
    public void setSzPointName(String szPointName) {
        this.szPointName = szPointName;
    }
    /**
     * <p>Description:[...]</p>
     * @return String szPointName.
     */
    public String getSzPointName() {
        return szPointName;
    }
    /**
     * <p>Description:[...]</p>
     * @param iPassed The iPassed to set.
     */
    public void setIPassed(int iPassed) {
        this.iPassed = iPassed;
    }
    /**
     * <p>Description:[...]</p>
     * @return int iPassed.
     */
    public int getIPassed() {
        return iPassed;
    }
    /**
     * <p>Description:[...]</p>
     * @param iPointType The iPointType to set.
     */
    public void setIPointType(int iPointType) {
        this.iPointType = iPointType;
    }
    /**
     * <p>Description:[...]</p>
     * @return int iPointType.
     */
    public int getIPointType() {
        return iPointType;
    }
    /**
     * <p>Description:[...]</p>
     * @param stPos The stPos to set.
     */
    public void setStPos(JNITwoLong stPos) {
        this.stPos = stPos;
    }
    /**
     * <p>Description:[...]</p>
     * @return JNITwoLong stPos.
     */
    public JNITwoLong getStPos() {
        return stPos;
    }
}
