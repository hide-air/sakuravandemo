/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           POI_Facility_ListItem.java
 * Description    名称検索リスト
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class POI_Facility_ListItem {
    private long m_lNameSize;
    private String m_Name;
    private long m_lLatitude;
    private long m_lLongitude;
    private boolean m_bBtnSts;
    private int iOldIndex;
    private long m_lDistance;
    /**
    * Created on 2010/08/06
    * Title:       getM_lNameSize
    * Description:  表示名称サイズを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lNameSize() {
        return m_lNameSize;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_Name
    * Description:  表示名称を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getM_Name() {
        return m_Name;
    }
    /**
    * Created on 2010/08/06
    * Title:       setM_Name
    * Description:  表示名称を設定する
    * @param1  無し
    * @return       void

    * @version        1.0
    */
    public void setM_Name(String name) {
        m_Name = name;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_lLatitude
    * Description:  経度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLatitude() {
        return m_lLatitude;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_lLatitude
    * Description:  緯度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLongitude() {
        return m_lLongitude;
    }
    /**
     * <p>Description:[...]</p>
     * @param m_bBtnSts The m_bBtnSts to set.
     */
    public void setM_bBtnSts(boolean m_bBtnSts) {
        this.m_bBtnSts = m_bBtnSts;
    }
    /**
     * <p>Description:[...]</p>
     * @return boolean m_bBtnSts.
     */
    public boolean isM_bBtnSts() {
        return m_bBtnSts;
    }
    /**
     * <p>Description:[...]</p>
     * @param iOldIndex The iOldIndex to set.
     */
    public void setM_iOldIndex(int iOldIndex) {
        this.iOldIndex = iOldIndex;
    }
    /**
     * <p>Description:[...]</p>
     * @return int iOldIndex.
     */
    public int getM_iOldIndex() {
        return iOldIndex;
    }


	public long getM_lDistance() {
		return m_lDistance;
	}
	public void setM_lDistance(long mLDistance) {
		m_lDistance = mLDistance;
	}


}
