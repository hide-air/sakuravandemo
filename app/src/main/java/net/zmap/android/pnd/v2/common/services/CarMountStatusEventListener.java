package net.zmap.android.pnd.v2.common.services;

import android.content.Intent;

public interface CarMountStatusEventListener
{
	public void onCarMountEventChange(Intent oIntent);
}
