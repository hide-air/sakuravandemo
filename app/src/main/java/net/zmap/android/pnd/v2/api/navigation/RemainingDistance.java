package net.zmap.android.pnd.v2.api.navigation;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 目的地までの案内情報
 */
public class RemainingDistance implements Parcelable {

    private long mDistance;
    private long mTimeMillis;

    /**
     * コンストラクタ
     */
    public RemainingDistance() {
        mDistance = 0;
        mTimeMillis = 0;
    }

    private RemainingDistance(Parcel in) {
        mDistance = in.readLong();
        mTimeMillis = in.readLong();
    }


    /**
     * 目的地までの所要時間(ミリ秒)設定
     *
     * @param timeMillis
     *            目的地までの所要時間(ミリ秒)
     */
    public void setTime(long timeMillis ) {
        mTimeMillis = timeMillis;
    }

    /**
     * 目的地までの所要時間(ミリ秒)取得
     *
     * @return 目的地までの所要時間(ミリ秒)
     */
    public long getTime() {
        return mTimeMillis;
    }

    /**
     * 目的地までの所要距離(メートル)設定
     *
     * @param dist
     *            目的地までの所要距離(メートル)
     */
    public void setDistance( long dist ) {
        mDistance = dist;
    }

    /**
     * 目的地までの所要距離(メートル)取得
     *
     * @return 目的地までの所要距離(メートル)
     */
    public long getDistance() {
        return mDistance;

    }

    /* (非 Javadoc)
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong( mDistance );
        out.writeLong( mTimeMillis );
    }

    /* (非 Javadoc)
     * @see android.os.Parcelable#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /* (非 Javadoc)
     *
     */
    public static final Parcelable.Creator<RemainingDistance> CREATOR =
            new Parcelable.Creator<RemainingDistance>() {

                public RemainingDistance createFromParcel(Parcel in) {
                    return new RemainingDistance(in);
                }

                public RemainingDistance[] newArray(int size) {
                    return new RemainingDistance[size];
                }
            };
}
