package net.zmap.android.pnd.v2.app.launch;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

//import net.zmap.android.pnd.v2.app.control.MapDownloadBroadcastReceiver;
import net.zmap.android.pnd.v2.app.control.NaviAppController;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.WakeLockUtils;
//import net.zmap.android.pnd.v2.downloader.common.ExtraName;

/**
 * ナビ起動・終了サービスクラス
 */
public class NaviLaunchService extends Service {

    private NaviAppController mNaviController = null;
//Del 2011/10/12 Z01yoneya Start -->NaviAppControllerへ移動
//    private ScreenSwitchBroadcastReceiver mScreenSwitchBroadcastReceiver = null;
//Del 2011/10/12 Z01yoneya End <--
//    private MapDownloadBroadcastReceiver mMapDownloadBroadcastReceiver = null;


    @Override
    public IBinder onBind(Intent intent) {
        //return mBinder;
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviLaunchService onCreate");

        try {
//// ADD.2013.08.05 N.Sasao #14932 地図ダウンロード終了後、起動画面のまま現在地画面に遷移しない START
//        	NotificationStart();
//// ADD.2013.08.05 N.Sasao #14932 地図ダウンロード終了後、起動画面のまま現在地画面に遷移しない  END
//
            Context context = this.getApplicationContext();

            mNaviController = new NaviAppController(context);
////Chg 2011/10/12 Z01yoneya Start -->
////            mScreenSwitchBroadcastReceiver = new ScreenSwitchBroadcastReceiver(mNaviController);
////            mScreenSwitchBroadcastReceiver.reg(context);
////------------------------------------------------------------------------------------------------------------------------------------
//            mNaviController.onCreate();
////Chg 2011/10/12 Z01yoneya End <--
//
//            mMapDownloadBroadcastReceiver = new MapDownloadBroadcastReceiver(mNaviController);
//            mMapDownloadBroadcastReceiver.reg(context);
//
        } catch (NullPointerException e) {
            //とくに何もしない
        }

        WakeLockUtils.closeScreenLock(getApplicationContext());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        NaviLog.v(NaviLog.PRINT_LOG_TAG, "start launch service.");

//        if (NaviAppDataPath.isLocalDataMode()) {
            mNaviController.startMapActivity();
//        }
//        else {
//            int[] idList = null;
//            if (intent != null) {
//                Bundle bundle = intent.getExtras();
//                if (bundle != null) {
//                    idList = bundle.getIntArray(ExtraName.DL_CONTENTS);
//                    if (idList != null && idList.length > 0) {
//                        for (int i = 0; i < idList.length; i++) {
//                            NaviLog.d(NaviLog.PRINT_LOG_TAG, "id[" + i + "]=" + idList[i]);
//                        }
//                    }
//                }
//            }
//            NaviControllIntent.sendBroadcastDownloadMapStart(getApplicationContext(), idList);
//        }
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {

        WakeLockUtils.showScreenLock();

//        TRY {
//            MMAPDOWNLOADBROADCASTRECEIVER.UNREG(THIS.GETAPPLICATIONCONTEXT());
////CHG 2011/10/12 Z01YONEYA START -->
////            MSCREENSWITCHBROADCASTRECEIVER.UNREG(THIS.GETAPPLICATIONCONTEXT());
////------------------------------------------------------------------------------------------------------------------------------------
//            MNAVICONTROLLER.ONDESTROY();
////CHG 2011/10/12 Z01YONEYA END <--
//
//        } CATCH (NULLPOINTEREXCEPTION E) {
//            //とくに何もしない
//        }

        NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviLaunchService onDestroy");

//// ADD.2013.08.05 N.Sasao #14932 地図ダウンロード終了後、起動画面のまま現在地画面に遷移しない START
//        NotificationStop();
//// ADD.2013.08.05 N.Sasao #14932 地図ダウンロード終了後、起動画面のまま現在地画面に遷移しない  END

        super.onDestroy();
        NaviLog.v(NaviLog.PRINT_LOG_TAG, "finish launch service.");
    }

//// ADD.2013.08.05 N.Sasao #14932 地図ダウンロード終了後、起動画面のまま現在地画面に遷移しない START
//    private void NotificationStart() {
//        Notification status = new Notification(R.drawable.ic_notification, "", 0);
//        // Notification 選択時の動作インテントを生成
//        PendingIntent contentIntent =
//                PendingIntent.getActivity(this, 0,
//                        new Intent(
//                        		this, ItsmoNaviPND.class)
//                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK), 0);
//        // 通知領域に表示する情報を Notification に指定
//        status.setLatestEventInfo(getApplicationContext(),
//        		this.getString(R.string.notification_app_name), this.getString(R.string.notification_app_detail), contentIntent);
//        // Notification は自動で削除しない
//        status.flags |= Notification.FLAG_ONGOING_EVENT;
//
//        // サービスをフォアグランド状態に変更する
//        startForeground(1, status);
//    }
//
//    private void NotificationStop() {
//        stopForeground(true);
//    }
//// ADD.2013.08.05 N.Sasao #14932 地図ダウンロード終了後、起動画面のまま現在地画面に遷移しない  END
}
