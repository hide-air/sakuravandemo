package net.zmap.android.pnd.v2.route.data;

import java.io.Serializable;

/**
 ********************************************************************
 * Copyright (c) 2010.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project
 * File           RouteNodeData.java
 * Description    無し
 * Created on     2010/12/02
 *
 ********************************************************************
 */
public class RouteNodeData implements Serializable {

	/**
	 * serialVersionUI
	 */
	private static final long serialVersionUID = -6496193283631972103L;
	private String mode = null;
	private int ipass = 0;
	private String lon = null;
    private String lat = null;
    private String name = null;
    private String status = null;
    private String date = null;
    private String dateFormat = null;
	/**
	 * Created on 2010/12/02
	 * Description: Passflagを取得する
	 * @param:無し　
	 * @return：int
	 * @author:XuYang
	 */
	public int getIpass() {
		return ipass;
	}
	/**
	 * Created on 2010/12/02
	 * Description: Passflagを設定する
	 * @param:int　
	 * @return：無し
	 * @author:XuYang
	 */
	public void setIpass(int ipass) {
		this.ipass = ipass;
	}
	/**
	 * Created on 2010/12/02
	 * Description: モードを取得する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public String getMode() {
		return mode;
	}
	/**
	 * Created on 2010/12/02
	 * Description: モードを設定する
	 * @param:String
	 * @return：無し
	 * @author:XuYang
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}
	/**
	 * Created on 2010/12/02
	 * Description: 経度を取得する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public String getLon() {
		return lon;
	}
	/**
	 * Created on 2010/12/02
	 * Description: 経度を設定する
	 * @param: String
	 * @return： 無し
	 * @author:XuYang
	 */
	public void setLon(String lon) {
		this.lon = lon;
	}
	/**
	 * Created on 2010/12/02
	 * Description: 緯度を取得する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public String getLat() {
		return lat;
	}
	/**
	 * Created on 2010/12/02
	 * Description: 緯度を設定する
	 * @param: String
	 * @return： 無し
	 * @author:XuYang
	 */
	public void setLat(String lat) {
		this.lat = lat;
	}
	/**
	 * Created on 2010/12/02
	 * Description: 名を取得する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public String getName() {
		return name;
	}
	/**
	 * Created on 2010/12/02
	 * Description: 名を設定する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Created on 2010/12/02
	 * Description: ステータスを取得する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * Created on 2010/12/02
	 * Description: ステータスを設定する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * Created on 2010/12/02
	 * Description: 日付を取得する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public String getDate() {
		StringBuffer strbufText = new StringBuffer();
		strbufText.append("  ");
		strbufText.append(date.substring(2, 4));
		strbufText.append("/");
		strbufText.append(date.substring(4, 6));
		strbufText.append("/");
		strbufText.append(date.substring(6, 8));
		strbufText.append("  ");
		dateFormat = strbufText.toString();
		return dateFormat;
	}
	/**
	 * Created on 2010/12/02
	 * Description: 日付を設定する
	 * @param: String
	 * @return： 無し
	 * @author:XuYang
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * Created on 2010/12/02
	 * Description: UTC日付を取得する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public String getUTCDate() {
		return date;
	}

}
