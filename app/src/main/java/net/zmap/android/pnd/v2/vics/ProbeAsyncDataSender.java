package net.zmap.android.pnd.v2.vics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import net.zmap.android.pnd.v2.common.http.HttpUtils;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import android.app.Activity;
import android.os.AsyncTask;

public class ProbeAsyncDataSender  extends AsyncTask<Void, Void, Boolean>{
	private static final String strUID = "uid";
	private static final String strCount = "cnt";
	private static final String strStart = "start";
	private static final String strGoal = "goal";
	private static final String strProbe = "prb";

//	private static String strUIDParam = "pnd.c2,";
    private static String strUIDParam = "-,";

	private ProbeEventListener m_listener = null;
	private static final int PROBE_POINT_DATA_LENGTH = "11:pnd.c2:128378719:503148304:20120418115134:1000,".length();
    // 接続試行回数
    static private final int MAX_CONN_TRIAL = 3;

    // タイムアウトまでの時間 (10000ミリ秒)
    static private final int GET_DATA_NETWORK_TIMEOUT_INTERVAL = 10000;

    //HTTPアクセス用Utilクラス
    HttpUtils httpUtils = null;

    private JNITwoLong mStartPos = null;
    private JNITwoLong mEndPos = null;
    List<PROBE_POINT_DATA> mProbData = null;

    /** 非同期処理開始
     *
     */
	@Override
	protected Boolean doInBackground(Void... arg0) {
		return setProbeDataFromNetwork();
	}

    /**
     * コンストラクタ
     *
     */
    public ProbeAsyncDataSender(ProbeEventListener listener , Activity activity) {
//		String strVersion = "";
//
//		try {
//		    String packegeName = activity.getPackageName();
//		    PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(packegeName, PackageManager.GET_META_DATA);
//		    strVersion =  packageInfo.versionName;
//		    if(strVersion != null && strVersion.length() > 2){
//		    	strVersion = strVersion.substring(0, strVersion.length() - 2);
//		    }
//		} catch (NameNotFoundException e) {
//		}
//	    if(strVersion.length() == 0){
//	    	return;
//	    }
//		strUIDParam = "pnd.c2," + strVersion;
        strUIDParam = "-,";
    	m_listener = listener;
    }

    /**データ送信中断
     *
     */
    @Override
    protected void onCancelled() {

        if (httpUtils != null ) {
            httpUtils.shutdown();
        }

        m_listener.onCancel();

        return;
    }


    /**データ送信完了
     *
     */
    @Override
    protected void onPostExecute(Boolean result) {

        if(result == null) {
            result = false;
        }
        if(result){
            StringBuffer buf = new StringBuffer();
			try {
	        	BufferedReader reader = new BufferedReader(new InputStreamReader(httpUtils.getContent() , "UTF-8"/* 文字コード指定 */));
	            String str = null;
	            while ((str = reader.readLine()) != null) {
	                    buf.append(str);
	                    buf.append("\n");
	            }
			} catch (UnsupportedEncodingException e) {
	        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "probe server post error.");
				NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
			} catch (IOException e) {
	        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "probe server post error..");
				NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
			}
            m_listener.onFinish(buf.toString());
        }else{
        	m_listener.onError();
        }
        if (httpUtils != null ) {
            httpUtils.shutdown();
        }
        return;
    }

    /** データ送信
     *
     *
     * @return 成功した場合、true
     */
    private boolean setProbeDataFromNetwork()
    {
//        int j = 0;
//
//        // getvics.cgi アクセス用 URL を作成
//        httpUtils = MakeHttpUtils();
//        if(httpUtils == null){
//        	return false;
//        }
//        for (j = 0; j < MAX_CONN_TRIAL; j++)
//        {
//            if (isCancelled()) {
//                break;
//            }
//
//            if (httpUtils != null && httpUtils.httpPost()) {
//                return true;
//            }
//        }
//
//        return  false;
        return  true;
    }

    /** アクセス用 URL 作成
     *
     */
    private HttpUtils MakeHttpUtils()
    {
    	if(mProbData == null || mProbData.size() == 0){
    		return null;
    	}
    	//検証用サーバ
    	String strVicsRequestUrlHeader = "http://mdt.its-mo.net/android/zdcpnd_v25/cgi/probe/ProbeUp.php";
    	//本番用サーバ
//		String strVicsRequestUrlHeader = "http://mdp.its-mo.net/android/zdcpnd_v25/cgi/probe/ProbeUp.php";

        // URL 設定
        httpUtils = new HttpUtils(strVicsRequestUrlHeader);

        httpUtils.addParam(strUID, strUIDParam);
        httpUtils.addParam(strCount, "" + mProbData.size());
        if(mStartPos != null || mEndPos != null){
	        httpUtils.addParam(strStart, "" + mStartPos.getM_lLat() + "," + mStartPos.getM_lLong());
	        httpUtils.addParam(strGoal , "" + mEndPos.getM_lLat() + "," + mEndPos.getM_lLong());
        }
        int size = mProbData.size();
        StringBuilder strData = new StringBuilder(PROBE_POINT_DATA_LENGTH * size);
        for(int i = 0 ; i < size ; i++){
            strData.append(mProbData.get(i).toString()).append(",");
        }

        if(strData.length() > 0) {
            strData.deleteCharAt(strData.length() -1);
        }

        httpUtils.addParam(strProbe, strData.toString());

        // タイムアウトまでの時間
        httpUtils.setConnectionTimeout(GET_DATA_NETWORK_TIMEOUT_INTERVAL);
        httpUtils.setReadTimeout(GET_DATA_NETWORK_TIMEOUT_INTERVAL);

        return	httpUtils;
    }

	public JNITwoLong getStartPos() {
		return mStartPos;
	}

	public void setStartPos(JNITwoLong startPos) {
		mStartPos = startPos;
	}

	public JNITwoLong getEndPos() {
		return mEndPos;
	}

	public void setEndPos(JNITwoLong endPos) {
		mEndPos = endPos;
	}

	public List<PROBE_POINT_DATA> getProbData() {
		return mProbData;
	}

	public void setProbData(List<PROBE_POINT_DATA> probData) {
		mProbData = new ArrayList<PROBE_POINT_DATA>(probData);
	}
}
