//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;



import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.common.view.WaitDialog;
import net.zmap.android.pnd.v2.inquiry.view.KeyboardKanaView;
import net.zmap.android.pnd.v2.inquiry.view.TwoBtnAndScrollView;

public class KeyInquiry extends InquiryBase implements OnCancelListener{


	private Button oSearchBtn = null;
	private Button oEditBtn = null;
	private static int DIALOG_NODATA_ALARM = 0x01;
	private KeyboardKanaView oView = null;
	String[] KEYBOARD_RIGHT = null;
	private static KeyInquiry instance;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
		Intent oIntent = getIntent();
		if (AppInfo.KANA_LIST == null) {
			AppInfo.KANA_LIST = this.getResources().getStringArray(R.array.kana_check_list);
		}
		KEYBOARD_RIGHT = this.getResources().getStringArray(R.array.keyboard_right);
//Del 2011/09/17 Z01_h_yamada Start -->
//		//title show
//		int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE, 0);
//		if (0!= titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--
		oView = new KeyboardKanaView(this,0);
		//yangyang add start Bug1044
		TwoBtnAndScrollView oBtnView = new TwoBtnAndScrollView(this,oView.getTextObj());
		Button ochange = (Button) oBtnView.findViewById(this, R.id.Button_change);
		ochange.setVisibility(Button.GONE);
		initTwoBtn(oBtnView);
		oView.setM_oEditBtn(oEditBtn);
//Del 2011/11/01 Z01_h_yamada Start -->
//		oView.setO_oSearchButton(oSearchBtn);
//Del 2011/11/01 Z01_h_yamada End <--
		//yangyang add end Bug1044
		//件数取得のタイプ
		String strType = "";
		if (oIntent.hasExtra(Constants.FROM_NAVI_SEARCH)) {
			strType = oIntent.getStringExtra(Constants.FROM_NAVI_SEARCH);
		}

		oView.setType(strType);

		int min = oIntent.getIntExtra(Constants.PARAMS_MIN_LIMITED, 0);
		int max = oIntent.getIntExtra(Constants.PARAMS_MAX_LIMITED, 0);
//Chg 2011/11/01 Z01_h_yamada Start -->
//		if (0 != min) {
//			oView.setMinLimited(min,oSearchBtn);
//		}
//		if (0!= max) {
//			oView.setMaxLimited(max,oSearchBtn);
//			oView.getTextObj().setFilters(new InputFilter[]{new InputFilter.LengthFilter(max)});
//		}
//--------------------------------------------
		if (0 != min) {
			oView.setMinLimited(min);
		}
		if (0!= max) {
			oView.setMaxLimited(max);
			oView.getTextObj().setFilters(new InputFilter[]{new InputFilter.LengthFilter(max)});
		}
//Chg 2011/11/01 Z01_h_yamada End <--
		int inthint = oIntent.getIntExtra(Constants.PARAMS_HINT_VALUE, R.string.text_key_input);
		oView.getTextObj().setHint(inthint);

//Add 2011/11/01 Z01_h_yamada Start -->
		oView.setSearchListener(onSearchClickListener);
//Add 2011/11/01 Z01_h_yamada End <--
		//soft keyboard close
//		AppInfo.hiddenSoftInputMethod(this);
		int flagKey = oIntent.getIntExtra(Constants.FROM_FLAG_KEY,-1);
		if (flagKey != -1 && flagKey != AppInfo.ID_ACTIVITY_MAINMENU &&
				flagKey != AppInfo.ID_ACTIVITY_ROUTE_EDIT) {
			oView.getoCountView().setVisibility(TextView.GONE);
//Add 2011/11/01 Z01_h_yamada Start -->
			String buttonName = oIntent.getStringExtra(Constants.PARAMS_BUTTON_NAME);
			oView.setSearchBtnText(buttonName);
//Add 2011/11/01 Z01_h_yamada End <--
		} else {
			oView.getoCountView().setVisibility(TextView.VISIBLE);
		}

//		InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
//		imm.showSoftInput(oView.getTextObj(), 0);
		String freeword = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
		if (!AppInfo.isEmpty(freeword)) {
			oView.getTextObj().setText(freeword);
		}
		setViewInOperArea(oBtnView.getView());
		getOperArea().setPadding(0, 0, 0, 0);
		setViewInWorkArea(oView.getView());
	}
//	@Override
//	protected void onStart() {
////		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStart");
//
//
//
//		super.onStart();
//	}

	// Add 2011/09/17 r.itoh Start -->
    @Override
    protected void search()
    {
        oView.UpdateUiPoiListNum();
    };
    // Add 2011/09/17 r.itoh End <--

	@Override
	protected void onResume() {
		//yangyang mod start Bug1044
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//		AppInfo.hiddenSoftInputMethod(this);
//Del 2011/09/08 Z01_h_yamada End <--
//		InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//
//		View view = this.getCurrentFocus();
//	    if (view != null){
//	        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//	    }
		//yangyang mod end Bug1044
		super.onResume();
	}
	@Override
	protected void onPause() {
		closeWarningDialog();
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//		AppInfo.hiddenSoftInputMethod(this);
//Del 2011/09/08 Z01_h_yamada End <--
		super.onPause();
	}

	@Override
	protected void onStop()
	{
		closeWarningDialog();
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//		AppInfo.hiddenSoftInputMethod(this);
//Del 2011/09/08 Z01_h_yamada End <--
		super.onStop();
	}


	public static KeyInquiry getInstance() {
		return instance;
	}


	@Override
	protected void onDestroy() {
//		oView.closeThread();
		super.onDestroy();
	}
	//	@Override
//	protected void search() {
//		JNILong ListCount = new JNILong();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POIFacility_GetMatchCount start");
//		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetMatchCount(ListCount);
//		oView.setListCount(ListCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"182  =ListCount==" + ListCount.lcount);
//		if (ListCount.lcount == 0) {
//			oSearchBtn.setEnabled(false);
//		}
////		Intent oIntent = getIntent();
////
////		if (oIntent.hasExtra(Constants.FROM_WHICH_SEARCH_KEY)) {
////			if (oView.getRecordCount() > 40) {
////				oIntent.setClass(this, KeyProvinceInquiry.class);
////				startActivityForResult(oIntent, AppInfo.ID_ACITIVITY_KEYPROVINCEINQUIRY);
////			} else {
////
////				oIntent.setClass(this, KeyResultInquiry.class);
////				startActivityForResult(oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
////			}
////		}
//	}
	/**
	 * 修正ボタンと検索ボタンの初期化
	 *
	 * */
	private void initTwoBtn(TwoBtnAndScrollView oBtnView) {
		oEditBtn = (Button) oBtnView.findViewById(this, R.id.Button_edit);
		oEditBtn.setEnabled(false);
//		btnEdit.setOnClickListener(onEditClickListener);
//Chg 2011/11/01 Z01_h_yamada Start -->
//		oSearchBtn = (Button) oBtnView.findViewById(this, R.id.Button_search);
//		Intent oIntent = getIntent();
//		int flagKey = oIntent.getIntExtra(Constants.FROM_FLAG_KEY,-1);
//		if (flagKey != -1 && flagKey != AppInfo.ID_ACTIVITY_MAINMENU &&
//				flagKey != AppInfo.ID_ACTIVITY_ROUTE_EDIT) {
//			String buttonName = oIntent.getStringExtra(Constants.PARAMS_BUTTON_NAME);
//			oSearchBtn.setText(buttonName);
//		}
////	oSearchBtn.setOnClickListener(onSearchClickListener);
//		oSearchBtn.setEnabled(false);
////	oBtnView.setButtonListener(onEditClickListener, onSearchClickListener);
//--------------------------------------------
		oBtnView.setButtonListener(onEditClickListener);
//Chg 2011/11/01 Z01_h_yamada End <--
	}

	OnClickListener onEditClickListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 50音 削除ボタン禁止 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 50音 削除ボタン禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
//			InputText.setSearchEnable(oTitleValue.getText().toString().length()-1);
			KeyboardKanaView.setClickFlag(0);
//			if (KeyboardKanaView.isChangeTitle(oView.getText())) {
			if (!AppInfo.isEmpty(oView.getText())) {
				oView.setFlag_AddAndDel(true);

				StringBuffer sb = new StringBuffer(oView.getText());
				int selectionEnd = oView.getTextObj().getSelectionEnd();
				if (selectionEnd == 0) {
					sb.deleteCharAt(selectionEnd);
				} else {
					sb.deleteCharAt(selectionEnd-1);
				}
// Del 2011/09/19 r.itoh Start -->
//				if (KeyboardKanaView.isDoSearchFlag()) {
//					KeyboardKanaView.setDoSearchFlag(false);
//				}
// Del 2011/09/19 r.itoh End <--
				oView.getTextObj().setText(sb.toString());

				if (selectionEnd == 0) {
					oView.getTextObj().setSelection(selectionEnd);
				} else {
					oView.getTextObj().setSelection(selectionEnd - 1);
				}
				if (AppInfo.isEmpty(oView.getText())) {
					oView.getTextObj().setHint("");
				}

				//カーソルの初期化
				TwoBtnAndScrollView.initBtnEnable(oView.getTextObj().getSelectionEnd(), oView.getTextObj());
			}

            oView.updateKanaButton();
		}

//		}

	};
	OnClickListener onSearchClickListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 50音 検索ボタン禁止 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 50音 検索ボタン禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
			Intent oIntent = getIntent();
			int flagKey = oIntent.getIntExtra(Constants.FROM_FLAG_KEY,-1);

			if (flagKey != -1 && (flagKey == AppInfo.ID_ACTIVITY_MAINMENU || flagKey == AppInfo.ID_ACTIVITY_ROUTE_EDIT)) {
				doSearch();
			}  else {
				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, oView.getText());
				setResult(RESULT_OK, oIntent);
				finish();
			}

		}


	};
	private void doSearch() {
		Intent oIntent = getIntent();
		oIntent.putExtra(Constants.FROM_WHICH_SEARCH_KEY, "Key");
		//yangyang add start bug1044

		//検索ボタンを押下するとき、先頭の「-」を無視する
		String title = oView.getText();
		if (oView.getText().startsWith(KEYBOARD_RIGHT[0])) {
			char[] checkChar = KEYBOARD_RIGHT[0].toCharArray();
			char[] charLocal = title.toCharArray();
			int startIndex = 0;
			for (int i = 0 ; i < charLocal.length ; i++) {
				if (charLocal[i] != checkChar[0]) {
					startIndex = i;
					break;
				}
			}
			title = title.substring(startIndex, title.length());
		}
		//yangyang add end bug1044
		oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
		if (oIntent.hasExtra(Constants.FROM_WHICH_SEARCH_KEY)) {
//Chg 2011/05/25 Z01thedoanh Start -->
			//K-N2, K-N3削除
//			if (oView.getRecordCount() > 40) {
//			if (oView.getO_SearchButton().isEnabled()) {
//				oIntent.setClass(this, KeyProvinceInquiry.class);
//				startActivityForResult(oIntent, AppInfo.ID_ACITIVITY_KEYPROVINCEINQUIRY);
//
//			}
//		} else if (oView.getRecordCount() > 0){
			//K-N1表示
		if(oView.getRecordCount() > 0){
			oIntent.setClass(this, KeyResultInquiry.class);
//			oIntent.putExtra(Constants.FROM_WHICH_SEARCH_KEY, "KeyItem");
//Chg 2011/09/23 Z01yoneya Start -->
//			startActivityForResult(oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
			NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
		}
//Chg 2011/05/25 Z01thedoanh End <--
		}


	}

	private static WaitDialog oWarningDialog = null;
	@Override
	protected Dialog onCreateDialog(int wId) {
		if (wId == DIALOG_NODATA_ALARM) {
			CustomDialog oDialog = new CustomDialog(this);
//			oDialog.setTitle("電話番号検索結果");
//			oDialog.setMessage("該当する施設がありませんでした。");
			oDialog.addButton("OK",new OnClickListener(){

				@Override
				public void onClick(View v) {
					removeDialog(DIALOG_NODATA_ALARM);

				}});
			return oDialog;
		} else if (wId == Constants.DIALOG_WAIT) {
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//			AppInfo.hiddenSoftInputMethod(this);
//Del 2011/09/08 Z01_h_yamada End <--

			runOnUiThread(new Runnable(){
				public void run() {
					oWarningDialog = new WaitDialog(KeyInquiry.this);

					oView.putWaitDialog(oWarningDialog);

					oWarningDialog.setShowProGressBar(true);
					oWarningDialog.setOnKeyListener(new OnKeyListener() {

						@Override
						public boolean onKey(DialogInterface dialog, int keyCode,
								KeyEvent event) {
							if (keyCode == KeyEvent.KEYCODE_SEARCH) {
								return true;
							} else if (keyCode == KeyEvent.KEYCODE_BACK) {
//								closeWarningDialog();
							}
							return false;
						}
					});
					oWarningDialog.setCancelable(true);
					oWarningDialog.setOnCancelListener(KeyInquiry.this);
					oWarningDialog.show();
				}
			});
//			Thread oTd = new Thread();
//			oTd.start();
//			runOnUiThread(new Runnable(){
//				public void run()
//				{
//					oWarningDialog = new WaitDialog(KeyInquiry.this);
//
//					oView.putWaitDialog(oWarningDialog);
//
//					oWarningDialog.setShowProGressBar(true);
//					oWarningDialog.setOnKeyListener(new OnKeyListener() {
//
//						@Override
//						public boolean onKey(DialogInterface dialog, int keyCode,
//								KeyEvent event) {
//							if (keyCode == KeyEvent.KEYCODE_SEARCH) {
//								return true;
//							} else if (keyCode == KeyEvent.KEYCODE_BACK) {
////								closeWarningDialog();
//							}
//							return false;
//						}
//					});
//					oWarningDialog.setCancelable(true);
//					oWarningDialog.setOnCancelListener(KeyInquiry.this);
//					oWarningDialog.show();
//
//				}
//			});
			return oWarningDialog;
		}
		return super.onCreateDialog(wId);
	}

	@Override
	public void onCancel(DialogInterface arg0) {
//		closeWarningDialog();
	}

	public static void closeWarningDialog() {
		if (oWarningDialog != null) {
//			removeDialog(Constants.DIALOG_WAIT);
			oWarningDialog.cancel();
			oWarningDialog = null;
		}

	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
//			NaviRun.GetNaviRunObj().JNI_NE_POITel_Cancel();
//			NaviRun.GetNaviRunObj().JNI_NE_POITel_Clear();
			this.setResult(Activity.RESULT_CANCELED,getIntent());
			finish();
		} else if (keyCode == KeyEvent.KEYCODE_ENTER) {
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"keydown keycode_enter");
		} else if (keyCode == KeyEvent.KEYCODE_SEARCH) {
//			Intent oIntent = getIntent();
//			int flagKey = oIntent.getIntExtra(Constants.FROM_FLAG_KEY,-1);
//
//			if (flagKey != -1 && (flagKey == AppInfo.ID_ACTIVITY_MAINMENU || flagKey == AppInfo.ID_ACTIVITY_ROUTE_EDIT)) {
//				doSearch();
//			}  else {
//				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, oView.getText());
//				setResult(RESULT_OK, oIntent);
//				finish();
//			}
		}
		return super.onKeyDown(keyCode, event);
//		return true;
	}


}
