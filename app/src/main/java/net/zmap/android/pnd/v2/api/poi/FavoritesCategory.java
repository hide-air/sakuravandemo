package net.zmap.android.pnd.v2.api.poi;

/**
 * FavoritesCategory 定数定義クラス
 */
public final class FavoritesCategory {

    /** 友人・親戚 */
    public static final int FRIEND      = 0;

    /** 食事 */
    public static final int EATING      = 1;

    /** 買物 */
    public static final int SHOPPING    = 2;

    /** 趣味 */
    public static final int HOBBY       = 3;

    /** 観光 */
    public static final int SIGHTSEEING = 4;

    /** 仕事 */
    public static final int WORK        = 5;

    /** その他 */
    public static final int OTHERS      = 6;

    /** 自宅 1 */
    public static final int HOME_1      = 7;

    /** 自宅 2 */
    public static final int HOME_2      = 8;

    /**
     * カテゴリ ID 妥当性チェック
     *
     * @param categoryId カテゴリ ID
     * @return true:妥当 false:不正
     */
    public static boolean isValidCategory(int categoryId) {
        if (categoryId != FRIEND &&
            categoryId != EATING &&
            categoryId != SHOPPING &&
            categoryId != HOBBY &&
            categoryId != SIGHTSEEING &&
            categoryId != WORK &&
            categoryId != OTHERS &&
            categoryId != HOME_1 &&
            categoryId != HOME_2) {
            return false;
        }
        return true;
    }
}