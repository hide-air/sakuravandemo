/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNUI_UI_MARK_DATA.java
 * Description    設定目的地方向ガイド
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNUI_UI_MARK_DATA {
    private JNITwoLong stPixInfo = new JNITwoLong();        ///< ピクセル情報
    private ZNUI_ANGLE_DATA stAngle = new ZNUI_ANGLE_DATA();        ///< 角度
    /**
    * Created on 2010/08/06
    * Title:       getPixInfo
    * Description:   ピクセル情報を取得する
    * @param1  無し
    * @return       JNITwoLong

    * @version        1.0
    */
    public JNITwoLong getPixInfo() {
        return stPixInfo;
    }
    /**
    * Created on 2010/08/06
    * Title:       getAngle
    * Description:   角度を取得する
    * @param1  無し
    * @return       ZNUI_ANGLE_DATA

    * @version        1.0
    */
    public ZNUI_ANGLE_DATA getAngle() {
        return stAngle;
    }

}
