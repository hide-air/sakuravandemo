package net.zmap.android.pnd.v2.sensor;


//3つのfloat値を持つリングバッファクラス
public class CircularBuffer3f {
    private CircularBuffer[] m_buffer= new CircularBuffer[3];

	public CircularBuffer3f() {
		m_buffer[0] = new CircularBuffer();
		m_buffer[1] = new CircularBuffer();
		m_buffer[2] = new CircularBuffer();
	}

	//値の追加
	public void put(long timestamp, float val0, float val1, float val2) {
		synchronized (this) {
			m_buffer[0].put(timestamp, val0);
			m_buffer[1].put(timestamp, val1);
			m_buffer[2].put(timestamp, val2);
		}
	}

	//指定時間内のデータ数を取得
	public long getDataNum(long seconds){
		return m_buffer[0].getDataNum(seconds);
	}

	//指定時間内の最大値・最小値の差(絶対値)を返す
	public float[] getMinMaxDiffAbsInSeconds(long seconds) {
		synchronized (this) {
	    	float[] diff = new float[3];
	    	diff[0] = m_buffer[0].maxInSeconds(seconds) - m_buffer[0].minInSeconds(seconds);
	    	diff[1] = m_buffer[1].maxInSeconds(seconds) - m_buffer[1].minInSeconds(seconds);
	    	diff[2] = m_buffer[2].maxInSeconds(seconds) - m_buffer[2].minInSeconds(seconds);
	    	return diff;
		}
	}

//Add 2011/04/21 Z01yoneya Start -->
	//指定時間内の平均値を返す
	public float[] getAverageInSeconds(long seconds) {
		synchronized (this) {
	    	float[] ave = new float[3];
	    	ave[0] = m_buffer[0].averageInSeconds(seconds);
	    	ave[1] = m_buffer[1].averageInSeconds(seconds);
	    	ave[2] = m_buffer[2].averageInSeconds(seconds);
	    	return ave;
		}
	}
//Add 2011/04/21 Z01yoneya End <--
//Add 2011/09/06 Z01yoneya Start -->
	public void clear(){
	    synchronized (this) {
            m_buffer[0].clear();
            m_buffer[1].clear();
            m_buffer[2].clear();
        }
	}
//Add 2011/09/06 Z01yoneya End <--
}
