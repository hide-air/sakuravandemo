package net.zmap.android.pnd.v2.common.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;

import java.util.Calendar;

public class TimeIcon extends TextView implements OnUpdateListener
{
	public int mMinutes;
	public int mHour;
	private final Calendar mCalendar = Calendar.getInstance();
	public TimeIcon(Context context) {
		super(context);
		init(context);
		updateTime();
	}
	public TimeIcon(Context oContext,AttributeSet oSet)
	{
		super(oContext,oSet);
		init(oContext);
		updateTime();
	}
	// Bug #494 対応 start XuYang
	/* (non-Javadoc)
	 * @see android.widget.TextView#onDraw(android.graphics.Canvas)
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

        //時計文字の配置位置
		int baseV=-11;
        int baseH=0;

		Paint paint = new Paint();
        paint.setColor(android.graphics.Color.BLACK);	//縁取り色
        paint.setTextSize(40);

        //縁取り描画
        //パターン１
        int offsetH=2;	//縁取り横方向オフセット
        int offsetV=2;	//縁取り縦方向オフセット
        canvas.drawText((String)this.getText(), baseH-offsetH,	this.getHeight()+baseV, 		paint);
        canvas.drawText((String)this.getText(), baseH+offsetH,	this.getHeight()+baseV, 		paint);
        canvas.drawText((String)this.getText(), baseH, 			this.getHeight()+baseV-offsetV, paint);
        canvas.drawText((String)this.getText(), baseH,			this.getHeight()+baseV+offsetV, paint);

//        //縁取り描画
//        //パターン２
//        int offsetH=3;		//縁取り横方向オフセット
//        int offsetV=3;		//縁取り縦方向オフセット
//        canvas.drawText((String)this.getText(), baseH-offsetH,	this.getHeight()+baseV, 		paint);
//        canvas.drawText((String)this.getText(), baseH+offsetH,	this.getHeight()+baseV, 		paint);
//        canvas.drawText((String)this.getText(), baseH, 			this.getHeight()+baseV-offsetV, paint);
//        canvas.drawText((String)this.getText(), baseH,			this.getHeight()+baseV+offsetV, paint);
//        canvas.drawText((String)this.getText(), baseH-offsetH,this.getHeight()+baseV-offsetV, paint);
//        canvas.drawText((String)this.getText(), baseH+offsetH,this.getHeight()+baseV+offsetV, paint);
//        canvas.drawText((String)this.getText(), baseH-offsetH,this.getHeight()+baseV+offsetV, paint);
//        canvas.drawText((String)this.getText(), baseH+offsetH,this.getHeight()+baseV-offsetV, paint);

        //実際の文字描画
        Paint paintTime = new Paint();
        paintTime.setColor(getResources().getColor(R.color.time_icon));
        paintTime.setTextSize(40);
        canvas.drawText((String)this.getText(), baseH, this.getHeight()+baseV, paintTime);
	}
	// Bug #494 対応 end XuYang
	private void updateTime(){
        mCalendar.setTimeInMillis(System.currentTimeMillis());
        mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
        mMinutes = mCalendar.get(Calendar.MINUTE);
        StringBuilder sb = new StringBuilder();
        // Bug #494 対応 start XuYang
        //sb.append(mHour < 10 ? "0" : "");
        // Bug #494 対応 end XuYang
        sb.append(mHour);
        sb.append(" : ");
        sb.append(mMinutes < 10 ? "0" : "");
        sb.append(mMinutes);
        setText(sb.toString());
	}

	private void init(Context oContext)
	{/*
		new Thread(new Runnable(){

			@Override
			public void run()
			{
				try
				{
					while(true)
					{
						Thread.sleep(20000);
						post(new Runnable(){
							@Override
							public void run()
							{
							    updateTime();
							}
						});
					}
				}
				catch(Exception e){}
			}

		}).start();
	*/

	    UpdateThread.addUpdateListener(this);
	}

    @Override
    public void onUpdate() {
        updateTime();
    }
}
