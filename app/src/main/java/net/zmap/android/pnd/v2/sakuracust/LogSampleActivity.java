package net.zmap.android.pnd.v2.sakuracust;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import net.zmap.android.pnd.v2.R;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

public class LogSampleActivity extends Activity {
	private final static String LOGDIR = Environment.getExternalStorageDirectory().getPath() + "/rakuraku_log/";
	private final static String SDFILE = LOGDIR+"log2.txt";

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate( savedInstanceState);
        setContentView( R.layout.map_main);

        try {
            // ログのクリア
            Runtime.getRuntime().exec( "logcat -c");
        } catch ( Exception e) {
            // 例外処理
        }

        // ���O�ǉ�
        Log.w( "tag", "message1");
        Log.w( "tag", "message2");
        Log.w( "tag", "message3");
        Log.w( "aaa", "message4");
        Log.v( "bbb", "message5");
        Log.d( "ccc", "message6");
        Log.i( "ddd", "message7");
        Log.e( "tag", "message8");

        StringBuilder log = new StringBuilder();
        try {
            ArrayList<String> commandLine = new ArrayList<String>();
            // ログの取得
            commandLine.add( "logcat");
            commandLine.add( "-d");
            commandLine.add( "-v");
            commandLine.add( "time");
            commandLine.add( "-s");
            commandLine.add( "tag:W");
            commandLine.add( "-ｆ");
            commandLine.add( "SDFILE");

            Process process = Runtime.getRuntime().exec( commandLine.toArray( new String[commandLine.size()]));
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader( process.getInputStream()), 1024);
            String line = bufferedReader.readLine();
            while ( line != null) {
                log.append( line);
                log.append( "\n");
            }

        } catch ( IOException e) {
            // ��O����
        }
        Log.i("LogSample", log.toString());
    }
}
