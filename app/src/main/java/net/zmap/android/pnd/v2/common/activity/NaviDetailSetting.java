/**
 ********************************************************************
 * Copyright (c) 2010.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project        MarketV2
 * File           NaviDetailSetting.java
 * Description    ナビ詳細設定画面
 * Created on     2010/11/24
 *
 ********************************************************************
 */

package net.zmap.android.pnd.v2.common.activity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import net.zmap.android.pnd.v2.common.utils.StorageUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.app.control.NaviControllIntent;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.DrivingRegulation;
import net.zmap.android.pnd.v2.common.MapTypeListioner;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.services.PreferencesService;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollBox;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.common.view.SwitchGroup;
import net.zmap.android.pnd.v2.common.view.SwitchGroupListener;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_History_FilePath;
import net.zmap.android.pnd.v2.dricon.controller.DriverContentsCtrl;
import net.zmap.android.pnd.v2.sensor.SensorNavigationManager;
import net.zmap.android.pnd.v2.vics.VicsManager;

/**
 * Created on 2010/11/24
 * <p>
 * Title: ナビ詳細設定画面
 * <p>
 * Description
 * <p>
 * author　XuYang version 1.0
 */
public class NaviDetailSetting extends InquiryBaseLoading implements SeekBar.OnSeekBarChangeListener {
////ADD.2013.08.12 N.Sasao アプリケーション領域デバッグモード導入 START
//    private static final String DEF_BACKUP_FOLDER_NAME = "BACKUP";
////ADD.2013.08.12 N.Sasao アプリケーション領域デバッグモード導入  END

    private static final int CarNaviComplexProperty_UseFerry = (0x1 << 5); // フェリー使用
    // 冬季閉鎖道路使用
    private static final int CarNaviComplexProperty_ThinkWinterClose = (0x1 << 6);
    // 時間帯規制
    private static final int CarNaviComplexProperty_ThinkTimeRule = (0x1 << 10);
    // 渋滞を考慮した探索
    private static final int CarNaviComplexProperty_UseVICS = (0x1 << 4);
//Add 2014/06/01 CPJsunagawa Start -->
    // 案内板表示
    private static final int CarNaviComplexProperty_GuideBoardDisplay = (0x1 << 8);
//Add 2014/06/01 CPJsunagawa Start -->

	LinearLayout oLayout;
    // ガイド音量
    private SeekBar mSeekBar;

    private AudioManager mAudio;

    // 季節規制 考慮
    SwitchGroup switchSeason;

    // 時間帯規制 考慮
    SwitchGroup switchTime;

    // 渋滞を考慮した探索
    SwitchGroup switchTraffic;

//Add 2011/09/30 Z01yamaguchi Start -->
    //Vics更新間隔
    SwitchGroup switchVicsUpdateInterval;
//Add 2011/09/30 Z01yamaguchi End <--

    // 料金所の音声案内
    SwitchGroup switchToll;
//Add 2011/09/05 Z01yoneya Start -->
    // 自律航法
    SwitchGroup switchSensorNavi;
//Add 2011/09/05 Z01yoneya End <--

//Add 2014/06/01 CPJsunagawa Start -->
    // 案内板表示
    SwitchGroup switchGuideBoard;
//Add 2014/06/01 CPJsunagawa End <--

//// ADD 2013.08.08 M.Honma 走行規制 車載器連携設定追加 Start -->
//    // 車載器連携
//    SwitchGroup switchDrivingRegulation;
//// ADD 2013.08.08 M.Honma 走行規制 車載器連携設定追加 End -->

    // 車種設定
    SwitchGroup switchCarType;
    private static final int KINDCAR_SMALL = 0; // 軽自動車
    private static final int KINDCAR_NORMAL = 1; // 普通車
    private static final int KINDCAR_MEDIUM = 2; // 中型車
    private static final int KINDCAR_BIG = 3; // 大型車
    private static final int KINDCAR_EX_BIG = 4; // 特大車

    // ルート探索条件（徒歩）
    SwitchGroup switchWalkMode;
    private static final int ManNaviSearchProperty_Default = 0; // 推奨
    private static final int ManNaviSearchProperty_Roofed = 1; // 屋根
    private static final int ManNaviSearchProperty_Easiness = 2; // 楽
    // ルート探索条件（車）
    SwitchGroup switchCarMode;

    private static final int NE_CarNaviSearchProperty_Commend = 1;// 推奨   画面位置index=0
    private static final int NE_CarNaviSearchProperty_General = 2; // 一般  画面位置index=2
    private static final int NE_CarNaviSearchProperty_Distance = 3;// 距離  画面位置index=1
    private static final int NE_CarNaviSearchProperty_Width = 4; // 道幅  画面位置index=3
    private static final int NE_CarNaviSearchProperty_Another = 5; // 別ﾙｰﾄ  画面位置index=4

//Del 2011/09/30 Z01_h_yamada Start -->
//    // ユーザーモード
//    SwitchGroup switchUserMode;
//Del 2011/09/30 Z01_h_yamada End <--

//// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
//    // お知らせ情報
//    Button btn_notice;
//// ADD.2013.07.22 N.Sasao お知らせ機能実装  END

    // GPS測位情報
    Button btn_gps;
    // バージョン情報
    Button btn_version;

    // 走行軌跡一括削除
    Button btn_del_walk_line;
    // お気に入り一括削除
    Button btn_del_like;
    // 履歴一括削除
    Button btn_del_history;
    // システム初期化
    Button btn_system_init;
//Add 2011/09/16 Z01_h_yamada Start -->
    // ヘルプ
//    Button btn_help;
//Add 2011/09/16 Z01_h_yamada End <--
////Add 2011/10/05 Z01yoneya Start -->
//    // 地図ダウンロード
//    Button btn_map_download;
////Add 2011/10/05 Z01yoneya End <--
    Button btn_remove_widget;

// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
    // 車載器連携
    Button btn_driving_regulation;
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

    // バージョン情報ダイアログ
    private final int DIALOG_VERSION = 1;
    // 走行軌跡一括削除ダイアログ
    private final int DIALOG_WALK_LINE = 2;
    // お気に入り一括削除ダイアログ
    private final int DIALOG_DEL_LIKE = 3;
    // 履歴一括削除ダイアログ
    private final int DIALOG_DEL_HISTORY = 4;
    // システム初期化
    private final int DIALOG_DEL_SYSTEM_INIT = 5;
    // ウィジェットクリアダイアログ
    private final int DIALOG_REMOVE_WIDGET = 6;

    private static final int ON = 1;
    private Context contextThis;
    private int waitDialogType = -1;

    private int SYS_INIT = 0;
    private int HIS_DEL = 1;
    private int FAVORITES_DEL = 2;
    private int WALK_LINE_DEL = 3;
    private int REMOVE_WIDGET = 4;

    /**
     * 初期化処理
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contextThis = this;
//Del 2011/09/17 Z01_h_yamada Start -->
//		setMenuTitle(R.drawable.navi_detail_setting);
//Del 2011/09/17 Z01_h_yamada End <--
        /*PageBox oPage = new PageBox(this);
        oPage.setPadding(20, 10, 10, 10);

        LayoutInflater oInflater = LayoutInflater.from(this);
        // 画面を追加
        setMenuTitle(R.drawable.navi_detail_setting);
        oLayout = (LinearLayout)oInflater.inflate(R.layout.setting_navi_detail, null);

        // 各項目状態の状態
        InitState();

        oPage.addPage(oLayout);

        // page1
        oLayout = (LinearLayout)oInflater.inflate(R.layout.setting_navi_detail_1, null);
        InitStatePage1();
        oPage.addPage(oLayout);

        // page2
        oLayout = (LinearLayout)oInflater.inflate(R.layout.setting_navi_detail_2, null);
        InitStatePage2();
        oPage.addPage(oLayout);

        // page3
        oLayout = (LinearLayout)oInflater.inflate(R.layout.setting_navi_detail_3, null);
        InitStatePage3();
        oPage.addPage(oLayout);

        ScrollTool oTool = new ScrollTool(this);
        oTool.bindView(oPage);

        setViewInOperArea(oTool);
        setViewInWorkArea(oPage);*/
        LayoutInflater oInflater = LayoutInflater.from(this);
        oLayout = (LinearLayout)oInflater.inflate(R.layout.setting_navi_detail_data, null);
        ScrollBox oBox = (ScrollBox)oLayout.findViewById(R.id.setting_navi_area);
        InitState();
        InitStatePage1();
        InitStatePage2();
        InitStatePage3();

//Add 2011/08/31 Z01yoneya Start -->
        //歩行者データが無い時は、歩行者設定を表示しない
//Chg 2011/09/26 Z01yoneya Start -->
//        if (!CommonLib.isBIsExistsWalkerData()) {
//------------------------------------------------------------------------------------------------------------------------------------
        boolean isExistWalkData;
        NaviAppDataPath appPath = null;
        try {
            appPath = ((NaviApplication)getApplication()).getNaviAppDataPath();
            isExistWalkData = appPath.isExistWalkData();
        } catch (NullPointerException e) {
            isExistWalkData = false;
        }
        if (!isExistWalkData) {
//Chg 2011/09/26 Z01yoneya End <--
            View switchWalkCondition = oLayout.findViewById(R.id.LinearLayoutSwitchGroupeManRouteCondition);
            if (switchWalkCondition != null) {
                switchWalkCondition.setVisibility(View.GONE);
            }

            View textLayout = oLayout.findViewById(R.id.LinearLayoutTextManRouteCondition);
            if (textLayout != null) {
                textLayout.setVisibility(View.GONE);
            }
        }
//Add 2011/08/31 Z01yoneya End <--
//Add 2011/09/16 Z01_h_yamada Start -->
        //VICSデータが無い時は、渋滞を考慮した検索設定を表示しない
//Chg 2011/09/30 Z01yamaguchi Start -->
//        if (!CommonLib.isBIsExistsVicsData()) {
//--------------------------------------------
        boolean isExistVicsData;
        try {
            isExistVicsData = appPath.isExistVicsData();
        } catch (NullPointerException e) {
            isExistVicsData = false;
        }
        if (!isExistVicsData) {
//Chg 2011/09/30 Z01yamaguchi End <--

            View switchVicsSearch = oLayout.findViewById(R.id.LinearLayoutVicsSearch);
            if (switchVicsSearch != null) {
                switchVicsSearch.setVisibility(View.GONE);
            }

//Add 2011/09/30 Z01yamaguchi Start -->
            View switchVicsIntervalText = oLayout.findViewById(R.id.LinearLayoutVicsIntervalText);
            if (switchVicsIntervalText != null) {
                switchVicsIntervalText.setVisibility(View.GONE);
            }

            View switchVicsInterval = oLayout.findViewById(R.id.LinearLayoutVicsInterval);
            if (switchVicsInterval != null) {
                switchVicsInterval.setVisibility(View.GONE);
            }
//Add 2011/09/30 Z01yamaguchi End <--
        }
//Add 2011/09/16 Z01_h_yamada End <--

        ScrollTool oTool = new ScrollTool(this);
        oTool.bindView(oBox);

        setViewInOperArea(oTool);
        setViewInWorkArea(oLayout);
    }

    private void InitState() {
        // 音量の初期化
        InitSeekBar();
        // 季節規制 考慮状態の初期化
        InitSeasonSetting();
        // 時間帯規制 考慮状態の初期化
        InitTimeSetting();
        // 渋滞を考慮した探索
        InitTrafficSetting();
        // 料金所の音声案内
        InitTollSetting();
//Add 2011/09/30 Z01yamaguchi Start -->
        // VICS更新間隔
        InitVicsUpdateIntervalSetting();
//Add 2011/09/30 Z01yamaguchi End <--
//Add 2011/09/05 Z01yoneya Start -->
        InitSensorNaviSetting();
//Add 2011/09/05 Z01yoneya End <--
//Add 2014/06/01 CPJsunagawa Start -->
        InitGuideBoardSetting();
//Add 2014/06/01 CPJsunagawa End <--
//// ADD 2013.08.08 M.Honma 走行規制 車載器連携設定追加 Start -->
//        InitDrivingRegulationSetting();
//// ADD 2013.08.08 M.Honma 走行規制 車載器連携設定追加 End -->
        initPageStatu();
    }

    private void InitStatePage1() {
        // 車種設定
        InitCarType();
        // ルート探索条件（徒歩）
        InitWalkMode();
        // ルート探索条件（車）
        InitCarMode();
        initPage1Statu();

    }

    private void InitStatePage2() {
//Del 2011/09/30 Z01_h_yamada Start -->
//        // ユーザーモード
//        InitUserMode();
//Del 2011/09/30 Z01_h_yamada End <--
//// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
//    	// お知らせ情報
//    	InitNotification();
//// ADD.2013.07.22 N.Sasao お知らせ機能実装  END
        // GPS測位情報
        InitGPS();
        // バージョン情報
        InitVersion();
//Del 2011/09/30 Z01_h_yamada Start -->
//        initPage2Statu();
//Del 2011/09/30 Z01_h_yamada End <--
    }

    private void InitStatePage3() {
        // 走行軌跡一括削除
        InitDelWalkLine();
        // お気に入り一括削除
        InitDelLike();
        // 履歴一括削除
        InitDelHistory();
//        //地図ダウンロード
//        InitMapDownload();
        // システム初期化
        InitSystemInit();
//Add 2011/09/16 Z01_h_yamada Start -->
        // ヘルプ
//        InitHelp();
//Add 2011/09/16 Z01_h_yamada End <--
        // ウィジェットクリア
        InitRemoveWidget();
    }

////Add 2011/09/16 Z01_h_yamada Start -->
//    private void InitHelp() {
//        // ヘルプ
//        btn_help = (Button)oLayout.findViewById(R.id.btn_help);
//        btn_help.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v)
//            {
////// ADD 2013.08.08 M.Honma 走行規制 ナビ詳細設定 オンラインヘルプ禁止 Start -->
////            	if(DrivingRegulation.CheckDrivingRegulation()){
////            		return;
////            	}
////// ADD 2013.08.08 M.Honma 走行規制 ナビ詳細設定 オンラインヘルプ禁止 End <--
//                // ヘルプ
////Chg 2011/10/20 Z01_h_yamada Start -->
////              Intent intentURL = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.zmap.net/android/pnd/v2/index.html"));
////--------------------------------------------
//                Intent intentURL = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.its-mo.com/stc/help_pnd/index.html"));
////Chg 2011/10/20 Z01_h_yamada End <--
//                startActivity(intentURL);
//            }
//        });
//    }
//
////Add 2011/09/16 Z01_h_yamada End <--
    private void InitRemoveWidget() {
        // ウィジェットクリア
        btn_remove_widget = (Button)oLayout.findViewById(R.id.btn_remove_widget);
        btn_remove_widget.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_REMOVE_WIDGET);
            }

        });
        if (!((NaviApplication)getApplication()).isShowWidget()) {
            View view = oLayout.findViewById(R.id.LinearLayoutRemoveWidget);
            if (view != null) {
                view.setVisibility(View.GONE);
            }
        }
    }
    private void InitSystemInit() {
        // システム初期化
        btn_system_init = (Button)oLayout.findViewById(R.id.btn_system_init);
        btn_system_init.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                // システム初期化
                showDialog(DIALOG_DEL_SYSTEM_INIT);
            }

        });
    }

    private void InitDelHistory() {
        // 履歴一括削除
        btn_del_history = (Button)oLayout.findViewById(R.id.btn_del_history);
        btn_del_history.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                // 履歴一括削除
                showDialog(DIALOG_DEL_HISTORY);
            }

        });
    }

    private void InitDelLike() {
        // お気に入り一括削除
        btn_del_like = (Button)oLayout.findViewById(R.id.btn_del_like);
        btn_del_like.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                // お気に入り一括削除
                showDialog(DIALOG_DEL_LIKE);
            }

        });
    }

    private void InitDelWalkLine() {
        // 走行軌跡一括削除
        btn_del_walk_line = (Button)oLayout.findViewById(R.id.btn_del_walk_line);
        btn_del_walk_line.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                // 走行軌跡一括削除
                showDialog(DIALOG_WALK_LINE);
            }

        });
    }

    private void InitVersion() {
        // バージョン情報
        btn_version = (Button)oLayout.findViewById(R.id.btn_version);
        btn_version.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                // バージョン情報
                showDialog(DIALOG_VERSION);
            }

        });
    }
//// MOD.2013.08.12 N.Sasao アプリケーション領域デバッグモード導入 START
//	//指定ファイルやフォルダをコピーする
//	//フォルダの場合、中にあるすべてのファイルやサブフォルダをコピーします
//    public static boolean backupFile(File fromDirOrFile, File toDirOrFile) {
//		boolean bResult = true;
//
//		if( fromDirOrFile == null || toDirOrFile == null ){
//			return false;
//		}
//
//	    if (fromDirOrFile.isDirectory()) {//ディレクトリの場合
//	        String[] children = fromDirOrFile.list();//ディレクトリにあるすべてのファイルを処理する
//	        for (int i=0; i<children.length; i++) {
//	            boolean success = backupFile(new File(fromDirOrFile, children[i]), new File(toDirOrFile, children[i]));
//	            if (!success) {
//	            	bResult = false;
//	            	break;
//	            }
//	        }
//	    }
//	    else{
//	    	if( !toDirOrFile.getParentFile().exists() ){
//	    		toDirOrFile.getParentFile().mkdirs();
//	    	}
//
//		    FileChannel srcChannel = null;
//		    FileChannel destChannel = null;
//			try {
//				srcChannel = new
//				        FileInputStream(fromDirOrFile).getChannel();
//				destChannel = new
//					    FileOutputStream(toDirOrFile).getChannel();
//			} catch (FileNotFoundException e) {
//				e.printStackTrace();
//				bResult = false;
//			}
//	        try {
//	        	if( srcChannel != null && destChannel != null ){
//	        		srcChannel.transferTo(0, srcChannel.size(), destChannel);
//	        	}
//	        } catch (IOException e) {
//				e.printStackTrace();
//				bResult = false;
//			} finally {
//	            try {
//	            	if( srcChannel != null ){
//	            		srcChannel.close();
//	            	}
//		            if( destChannel != null ){
//		            	destChannel.close();
//		            }
//				} catch (IOException e) {
//					e.printStackTrace();
//					bResult = false;
//				}
//	        }
//	    }
//		return bResult;
//	}
//	private void AppFileToSDCard(){
//		String strAppDataIniPath = contextThis.getFilesDir().toString();
////		strAppDataIniPath = strAppDataIniPath + File.separator + NotificationAsyncDataGetter.GetContentsOpposingPath();
//
//		String strBackupIniPath = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) + File.separator + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME;
//		strBackupIniPath = strBackupIniPath + File.separator + DEF_BACKUP_FOLDER_NAME + File.separator;
//
//		NotificationDataManager.deleteFile( new File(strBackupIniPath) );
//		backupFile( new File(strAppDataIniPath), new File(strBackupIniPath) );
//	}
//
//
//// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
//	private void InitNotification() {
//        // お知らせ情報
//        btn_notice = (Button)oLayout.findViewById(R.id.btn_notice);
//        btn_notice.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v)
//            {
//                // お知らせ情報
//// MOD.2013.07.22 N.Sasao 設定メニューのお知らせは問い合わせをしない START
////                Intent intNotification = new Intent();
////                intNotification.setClass(contextThis, StartSettingsNotificationActivity.class);
////                intNotification.putExtra(Constants.NOTICE_TYPE_KEY, Constants.NOTICE_TYPE_SETTING);
////                NaviActivityStarter.startActivity(((Activity)v.getContext()),intNotification);
//            	NotificationCtrl noticeCtrl = NotificationCtrl.getController();
//		        if ( noticeCtrl != null ) {
//		        	noticeCtrl.updateActivity( contextThis );
//		        	noticeCtrl.showInformationDialog( false );
//		        }
//// MOD.2013.07.22 N.Sasao 設定メニューのお知らせは問い合わせをしない  END
//            }
//        });
//        // 開発モード時のみ有効
//        // アプリケーション領域のデータをItsmoNaviPndにコピーする
//        if( Constants.isDebug == true ){
//        	btn_notice.setOnLongClickListener(new View.OnLongClickListener() {
//		    	@Override
//		    	public boolean onLongClick(View v) {
//		    		AppFileToSDCard();
//		    		return true;    // 戻り値をtrueにするとOnClickイベントは発生しない
//		    	}
//		    });
//        }
//    }
//// ADD.2013.07.22 N.Sasao お知らせ機能実装  END
//// MOD.2013.08.12 N.Sasao アプリケーション領域デバッグモード導入  END
    private void InitGPS() {
        // GPS測位情報
        btn_gps = (Button)oLayout.findViewById(R.id.btn_gps);
        btn_gps.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ナビ詳細設定 GPS測位情報禁止 Start -->
            	if(DrivingRegulation.CheckDrivingRegulation()){
            		return;
            	}
// ADD 2013.08.08 M.Honma 走行規制 ナビ詳細設定 GPS測位情報禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                // GPS測位情報
                Intent intGPS = new Intent();
                intGPS.setClass(contextThis, NaviDetailSettingGPS.class);
//Chg 2011/09/22 Z01yoneya Start -->
//                startActivityForResult(intGPS, AppInfo.ID_ACTIVITY_GPS);
//------------------------------------------------------------------------------------------------------------------------------------
                NaviActivityStarter.startActivityForResult(((Activity)v.getContext()), intGPS, AppInfo.ID_ACTIVITY_GPS);
//Chg 2011/09/22 Z01yoneya End <--
            }
        });
    }

//Del 2011/09/30 Z01_h_yamada Start -->
//    private void InitUserMode() {
//        // ユーザーモード状態ボタンを取得
//        switchUserMode = (SwitchGroup)oLayout.findViewById(R.id.switchUserMode);
//        switchUserMode.setListener(new SwitchGroupListener() {
//
//            @Override
//            public void onSelectChange(int wSelect)
//            {
//                NaviRun.GetNaviRunObj().JNI_Java_SetDriveMode(wSelect);
//            }
//        });
////		switchUserMode.getView(0).setOnClickListener(new OnClickListener(){
////
////			@Override
////			public void onClick(View v)
////			{
////				// ユーザーモード状態を取得
////				// 運転手を設定
////				NaviRun.GetNaviRunObj().JNI_Java_SetDriveMode(0);
////			}
////
////		});
////		switchUserMode.getView(1).setOnClickListener(new OnClickListener(){
////
////			@Override
////			public void onClick(View v)
////			{
////				// ユーザーモード状態を取得
////				// 助手席を設定
////				NaviRun.GetNaviRunObj().JNI_Java_SetDriveMode(1);
////			}
////
////		});
//    }
//Del 2011/09/30 Z01_h_yamada End <--

    private void InitCarType() {
        // 車種設定状態ボタンを取得
        switchCarType = (SwitchGroup)oLayout.findViewById(R.id.switchCarType);
        switchCarType.setListener(new SwitchGroupListener() {

            @Override
            public void onSelectChange(int wSelect)
            {
                switch (wSelect) {
                    case 0:
                    // 車種設定状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetCarType(KINDCAR_SMALL);
                    break;
                case 1:
                    // 車種設定状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetCarType(KINDCAR_NORMAL);
                    break;
                case 2:
                    // 車種設定状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetCarType(KINDCAR_MEDIUM);
                    break;
                case 3:
                    // 車種設定状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetCarType(KINDCAR_BIG);
                    break;
                case 4:
                    // 車種設定状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetCarType(KINDCAR_EX_BIG);
                    break;
            }
        }
        });

//		switchCarType.getView(0).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// 車種設定状態を取得
//				//
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetCarType(KINDCAR_SMALL);
//			}
//
//		});
//		switchCarType.getView(1).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// 車種設定状態を取得
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetCarType(KINDCAR_NORMAL);
//			}
//
//		});
//		switchCarType.getView(2).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// 車種設定状態を取得
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetCarType(KINDCAR_MEDIUM);
//
//			}
//
//		});
//		switchCarType.getView(3).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// 車種設定状態を取得
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetCarType(KINDCAR_BIG);
//			}
//
//		});
//		switchCarType.getView(4).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// 車種設定状態を取得
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetCarType(KINDCAR_EX_BIG);
//			}
//
//		});
    }

    private void InitWalkMode() {
        // ルート探索条件（徒歩）状態ボタンを取得
        switchWalkMode = (SwitchGroup)oLayout.findViewById(R.id.switchWalkMode);
        switchWalkMode.setListener(new SwitchGroupListener() {

            @Override
            public void onSelectChange(int wSelect)
            {
                switch (wSelect) {
                    case 0:
                    // ルート探索条件（徒歩）状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Man_SetDefaultSearchProperty(
                            ManNaviSearchProperty_Default);
                    NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(8, ManNaviSearchProperty_Default);
                    break;
                case 1:
                    // ルート探索条件（徒歩）状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Man_SetDefaultSearchProperty(
                            ManNaviSearchProperty_Roofed);
                    NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(8, ManNaviSearchProperty_Roofed);
                    break;
                case 2:
                    // ルート探索条件（徒歩）状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Man_SetDefaultSearchProperty(
                            ManNaviSearchProperty_Easiness);
                    NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(8, ManNaviSearchProperty_Easiness);
                    break;
            }
        }
        });
//		switchWalkMode.getView(0).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// ルート探索条件（徒歩）状態を取得
//				NaviRun.GetNaviRunObj().JNI_NE_Man_SetDefaultSearchProperty(
//						ManNaviSearchProperty_Default);
//				NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(8,ManNaviSearchProperty_Default);
//			}
//
//		});
//		switchWalkMode.getView(1).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// ルート探索条件（徒歩）状態を取得
//				NaviRun.GetNaviRunObj().JNI_NE_Man_SetDefaultSearchProperty(
//						ManNaviSearchProperty_Roofed);
//				NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(8,ManNaviSearchProperty_Roofed);
//
//			}
//
//		});
//		switchWalkMode.getView(2).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// ルート探索条件（徒歩）状態を取得
//				NaviRun.GetNaviRunObj().JNI_NE_Man_SetDefaultSearchProperty(
//						ManNaviSearchProperty_Easiness);
//				NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(8,ManNaviSearchProperty_Easiness);
//
//			}
//
//		});

    }

    private void InitCarMode() {
        // ルート探索条件（車）状態ボタンを取得
        switchCarMode = (SwitchGroup)oLayout.findViewById(R.id.switchCarMode);
        switchCarMode.setListener(new SwitchGroupListener() {

            @Override
            public void onSelectChange(int wSelect)
            {
                switch (wSelect) {
                    case 0:
                    // ルート探索条件（車）状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetDefaultSearchProperty(
                            NE_CarNaviSearchProperty_Commend);
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(1, 8,
                            NE_CarNaviSearchProperty_Commend);
                    break;
                case 1:
                    // ルート探索条件（車）状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetDefaultSearchProperty(
                            NE_CarNaviSearchProperty_Distance);
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(1, 8,
                            NE_CarNaviSearchProperty_Distance);
                    break;
                case 2:
                    // ルート探索条件（車）状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetDefaultSearchProperty(
                            NE_CarNaviSearchProperty_General);
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(1, 8,
                            NE_CarNaviSearchProperty_General);
                    break;
                case 3:
                    // ルート探索条件（車）状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetDefaultSearchProperty(
                            NE_CarNaviSearchProperty_Width);
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(1, 8,
                            NE_CarNaviSearchProperty_Width);
                    break;
                case 4:
                    // ルート探索条件（車）状態を取得
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetDefaultSearchProperty(
                            NE_CarNaviSearchProperty_Another);
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(1, 8,
                            NE_CarNaviSearchProperty_Another);
                    break;
            }
        }

        });
//		switchCarMode.getView(0).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// ルート探索条件（車）状態を取得
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetDefaultSearchProperty(
//						NE_CarNaviSearchProperty_Commend);
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(1,8,
//						NE_CarNaviSearchProperty_Commend);
//			}
//		});
//		switchCarMode.getView(1).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// ルート探索条件（車）状態を取得
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetDefaultSearchProperty(
//						NE_CarNaviSearchProperty_Distance);
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(1,8,
//						NE_CarNaviSearchProperty_Distance);
//			}
//
//		});
//		switchCarMode.getView(2).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// ルート探索条件（車）状態を取得
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetDefaultSearchProperty(
//						NE_CarNaviSearchProperty_General);
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(1,8,
//						NE_CarNaviSearchProperty_General);
//			}
//
//		});
//		switchCarMode.getView(3).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// ルート探索条件（車）状態を取得
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetDefaultSearchProperty(
//						NE_CarNaviSearchProperty_Width);
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(1,8,
//						NE_CarNaviSearchProperty_Width);
//			}
//
//		});
//		switchCarMode.getView(4).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// ルート探索条件（車）状態を取得
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetDefaultSearchProperty(
//						NE_CarNaviSearchProperty_Another);
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(1,8,
//						NE_CarNaviSearchProperty_Another);
//			}
//
//		});
    }

    private void InitSeekBar() {
        mSeekBar = (SeekBar)oLayout.findViewById(R.id.seek);
        mSeekBar.setOnSeekBarChangeListener(this);

        mAudio = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        mSeekBar.setMax(mAudio.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        CommonLib.VOICE_PROGRESS = mAudio.getStreamVolume(AudioManager.STREAM_MUSIC);

//		JNILong getvolumescale = new JNILong();
//		NaviRun.GetNaviRunObj().JNI_GetVolumeScale(getvolumescale);
//		CommonLib.VOICE_PROGRESS = (int)getvolumescale.getLcount();
    }

    private void InitSeasonSetting() {
        // 季節規制 考慮状態ボタンを取得
        switchSeason = (SwitchGroup)oLayout.findViewById(R.id.switchSeason);
        switchSeason.setListener(new SwitchGroupListener() {

            @Override
            public void onSelectChange(int wSelect)
            {
                if (wSelect == 0)
                {
                    // 季節規制 考慮状態を取得
                    JNIInt JIComplexProperty = new JNIInt();
                    NaviRun.GetNaviRunObj()
                            .JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
                    int iSetProp = JIComplexProperty.getM_iCommon() | CarNaviComplexProperty_ThinkWinterClose;
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
                            iSetProp);
                }
                else
                {
                    // 季節規制 考慮状態を取得
                    JNIInt JIComplexProperty = new JNIInt();
                    NaviRun.GetNaviRunObj()
                            .JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
                    int iSetProp = JIComplexProperty.getM_iCommon() & (~CarNaviComplexProperty_ThinkWinterClose);
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
                            iSetProp);

                }
            }

        });
        /*
        switchSeason.getView(0).setOnClickListener(new OnClickListener(){

        	@Override
        	public void onClick(View v)
        	{

        		// 季節規制 考慮状態を取得
        		JNIInt JIComplexProperty = new JNIInt();
        		NaviRun.GetNaviRunObj()
        		.JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
        		int iSetProp = JIComplexProperty.getM_iCommon() | CarNaviComplexProperty_ThinkWinterClose;
        		NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
        		iSetProp);
        	}

        });
        switchSeason.getView(1).setOnClickListener(new OnClickListener(){

        	@Override
        	public void onClick(View v)
        	{
        		// 季節規制 考慮状態を取得
        		JNIInt JIComplexProperty = new JNIInt();
        		NaviRun.GetNaviRunObj()
        		.JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
        		int iSetProp = JIComplexProperty.getM_iCommon() & (~CarNaviComplexProperty_ThinkWinterClose);
        		NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
        		iSetProp);
        	}

        });*/
    }

    private void InitTimeSetting() {
        // 時間帯規制 考慮状態ボタンを取得
        switchTime = (SwitchGroup)oLayout.findViewById(R.id.switchTime);
        switchTime.setListener(new SwitchGroupListener() {

            @Override
            public void onSelectChange(int wSelect)
            {
                if (wSelect == 0)
                {
                    // 時間帯規制 考慮状態を取得
                    JNIInt JIComplexProperty = new JNIInt();
                    NaviRun.GetNaviRunObj()
                            .JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
                    int iSetProp = JIComplexProperty.getM_iCommon() | CarNaviComplexProperty_ThinkTimeRule;
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
                            iSetProp);
                }
                else
                {
                    // 時間帯規制 考慮状態を取得
                    JNIInt JIComplexProperty = new JNIInt();
                    NaviRun.GetNaviRunObj()
                            .JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
                    int iSetProp = JIComplexProperty.getM_iCommon() & (~CarNaviComplexProperty_ThinkTimeRule);
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
                            iSetProp);

                }
            }

        });
//		switchTime.getView(0).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//
//				// 時間帯規制 考慮状態を取得
//				JNIInt JIComplexProperty = new JNIInt();
//				NaviRun.GetNaviRunObj()
//				.JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
//				int iSetProp = JIComplexProperty.getM_iCommon() | CarNaviComplexProperty_ThinkTimeRule;
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
//				iSetProp);
//			}
//
//		});
//		switchTime.getView(1).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// 時間帯規制 考慮状態を取得
//				JNIInt JIComplexProperty = new JNIInt();
//				NaviRun.GetNaviRunObj()
//				.JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
//				int iSetProp = JIComplexProperty.getM_iCommon() & (~CarNaviComplexProperty_ThinkTimeRule);
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
//				iSetProp);
//			}
//
//		});
    }

    private void InitTrafficSetting() {
        // 渋滞を考慮した探索状態ボタンを取得
        switchTraffic = (SwitchGroup)oLayout.findViewById(R.id.switchTraffic);
        switchTraffic.setListener(new SwitchGroupListener() {

            @Override
            public void onSelectChange(int wSelect)
            {
                if (wSelect == 0)
                {
                    // 渋滞を考慮した探索状態を取得
                    JNIInt JIComplexProperty = new JNIInt();
                    NaviRun.GetNaviRunObj()
                            .JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
                    int iSetProp = JIComplexProperty.getM_iCommon() | CarNaviComplexProperty_UseVICS;
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
                            iSetProp);
//Add 2011/09/30 Z01yamaguchi Start -->
                    // Vics更新時間を15分に
                    if (switchVicsUpdateInterval != null) {
                        if (switchVicsUpdateInterval.getSelected() == Constants.VICSINFO_NONE) {
                            switchVicsUpdateInterval.setSelected(Constants.VICSINFO_15);
                        }
                    }
//Add 2011/09/30 Z01yamaguchi End <--
                }
                else
                {
                    // 渋滞を考慮した探索状態を取得
                    JNIInt JIComplexProperty = new JNIInt();
                    NaviRun.GetNaviRunObj()
                            .JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
                    int iSetProp = JIComplexProperty.getM_iCommon() & (~CarNaviComplexProperty_UseVICS);
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
                            iSetProp);

                }
            }

        });
//		switchTraffic.getView(0).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// 渋滞を考慮した探索状態を取得
//				JNIInt JIComplexProperty = new JNIInt();
//				NaviRun.GetNaviRunObj()
//				.JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
//				int iSetProp = JIComplexProperty.getM_iCommon() | CarNaviComplexProperty_UseVICS;
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
//				iSetProp);
//			}
//
//		});
//		switchTraffic.getView(1).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// 渋滞を考慮した探索状態を取得
//				JNIInt JIComplexProperty = new JNIInt();
//				NaviRun.GetNaviRunObj()
//				.JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
//				int iSetProp = JIComplexProperty.getM_iCommon() & (~CarNaviComplexProperty_UseVICS);
//				NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
//				iSetProp);
//			}
//
//		});
    }

//Add 2011/09/30 Z01yamaguchi Start -->
    private void InitVicsUpdateIntervalSetting() {
        // Vics更新間隔
        switchVicsUpdateInterval = (SwitchGroup)oLayout.findViewById(R.id.switchVicsInterval);
        switchVicsUpdateInterval.setListener(new SwitchGroupListener() {

            @Override
            public void onSelectChange(int wSelect)
            {
                // VICS更新間隔を設定
                NaviRun.GetNaviRunObj().JNI_NE_SetVicsInfo(wSelect);

                if (wSelect == Constants.VICSINFO_NONE) {
                    if (switchTraffic != null) {
                        switchTraffic.setSelected(1);
                    }
                }
            }

        });
    }

//Add 2011/09/30 Z01yamaguchi End <--

    private void InitTollSetting() {
        // 料金所の音声案内状態ボタンを取得
        switchToll = (SwitchGroup)oLayout.findViewById(R.id.switchToll);
        switchToll.setListener(new SwitchGroupListener() {

            @Override
            public void onSelectChange(int wSelect)
            {
                if (wSelect == 0)
                {
                    // 料金所の音声案内状態を取得
                    JNIInt TollGuide = new JNIInt();
                    NaviRun.GetNaviRunObj().JNI_NE_GetTollGuide(TollGuide);
                    // 料金所の音声案内状態を設定
                    NaviRun.GetNaviRunObj().JNI_NE_SetTollGuide(1);
                }
                else
                {
                    // 料金所の音声案内状態を取得
                    JNIInt TollGuide = new JNIInt();
                    NaviRun.GetNaviRunObj().JNI_NE_GetTollGuide(TollGuide);
                    // 料金所の音声案内状態を設定
                    NaviRun.GetNaviRunObj().JNI_NE_SetTollGuide(0);

                }
            }

        });
//		switchToll.getView(0).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// 料金所の音声案内状態を取得
//				JNIInt TollGuide = new JNIInt();
//				NaviRun.GetNaviRunObj().JNI_NE_GetTollGuide(TollGuide);
//				// 料金所の音声案内状態を設定
//				NaviRun.GetNaviRunObj().JNI_NE_SetTollGuide(1);
//			}
//
//		});
//		switchToll.getView(1).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v)
//			{
//				// 料金所の音声案内状態を取得
//				JNIInt TollGuide = new JNIInt();
//				NaviRun.GetNaviRunObj().JNI_NE_GetTollGuide(TollGuide);
//				// 料金所の音声案内状態を設定
//				NaviRun.GetNaviRunObj().JNI_NE_SetTollGuide(0);
//			}
//
//		});

    }

//Add 2011/09/05 Z01yoneya Start -->
    private void InitSensorNaviSetting() {
        if (oLayout == null) {
            return;
        }

        if (!SensorNavigationManager.isEnableSensorNaviFlagSwitch()) {
            //センサーを搭載しないハードでは自律航法切り替えボタンは非表示
            View sensorNaviLayout = oLayout.findViewById(R.id.LinearLayoutSensorNavi);
            sensorNaviLayout.setVisibility(View.GONE);
            switchSensorNavi = null;
        } else {
            // 自律航法する/しないボタンを取得
            switchSensorNavi = (SwitchGroup)oLayout.findViewById(R.id.switchSensorNavi);
            if (switchSensorNavi != null) {
                switchSensorNavi.setListener(new SwitchGroupListener() {
                    @Override
                    public void onSelectChange(int wSelect)
                    {
                        if (wSelect == 0) {
                            SensorNavigationManager.enableSensorNavi(true);
                        } else {
                            SensorNavigationManager.enableSensorNavi(false);
                        }
                    }
                });
            }
        }
    }

//Add 2011/09/05 Z01yoneya End <--
//Add 2014/06/01 CPJsunagawa Start -->
    /** 案内板表示ボタンの状態を保存する */
    public void saveGuideBoardDisplay( Context context, int status ){
        // プリファレンスの準備 //
        SharedPreferences pref = 
                context.getSharedPreferences( "display_status", Context.MODE_PRIVATE );
 
        // プリファレンスに書き込むためのEditorオブジェクト取得 //
        Editor editor = pref.edit();
 
        // "display_status" というキーで状態を登録
        editor.putInt( "display_status", status );
        
        // JNIに反映する。
// Mod by CPJsunagawa '2015-05-31
        NaviRun.GetNaviRunObj().JNI_NE_setIsGuide(status);
//        NaviRun.GetNaviRunObj().NE_setIsGuide(status);
 
        // 書き込みの確定（実際にファイルに書き込む）
        editor.commit();
    }
   
    /** 案内板表示ボタンの状態を取得する */
    public int loadGuideBoardDisplay( Context context ){
        // プリファレンスの準備 //
        SharedPreferences pref = 
                context.getSharedPreferences( "display_status", Context.MODE_PRIVATE );
 
        // "display_status" というキーで保存されている値を読み出す
        //return pref.getInt( "display_status", 0 );
        return pref != null ? pref.getInt( "display_status", NaviRun.NE_IsGuide_DISPLAY ) : NaviRun.NE_IsGuide_DISPLAY;
    }
//Add 2014/06/01 CPJsunagawa End <--


    
//Add 2014/06/01 CPJsunagawa Start -->
    private void InitGuideBoardSetting() {
        // 案内板表示ボタンを取得
    	switchGuideBoard = (SwitchGroup)oLayout.findViewById(R.id.switchGuideBoard);
        if (switchGuideBoard != null) {
        	switchGuideBoard.setListener(new SwitchGroupListener() {
                @Override
                public void onSelectChange(int wSelect)
                {
                    if (wSelect == 0) {
                    	saveGuideBoardDisplay(contextThis, 1);
                    } else {
                    	saveGuideBoardDisplay(contextThis, 0);
                    }
				}
            });
        }
    }
/*    	
    	switchGuideBoard = (SwitchGroup)oLayout.findViewById(R.id.switchGuideBoard);
    	switchGuideBoard.setListener(new SwitchGroupListener() {

            @Override
            public void onSelectChange(int wSelect)
            {
                if (wSelect == 0)
                {
                    // 案内板表示状態を取得
                    JNIInt JIComplexProperty = new JNIInt();
                    NaviRun.GetNaviRunObj()
                            .JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
                    int iSetProp = JIComplexProperty.getM_iCommon() | CarNaviComplexProperty_GuideBoardDisplay;
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
                            iSetProp);
                }
                else
                {
                    // 案内板表示状態を取得
                    JNIInt JIComplexProperty = new JNIInt();
                    NaviRun.GetNaviRunObj()
                            .JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
                    int iSetProp = JIComplexProperty.getM_iCommon() & (~CarNaviComplexProperty_GuideBoardDisplay);
                    NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
                            iSetProp);

                }
            }
        });
*/
//Add 2014/06/01 CPJsunagawa End <--


// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// MOD 2013.09.17 M.Honma 走行規制 SmartAccess監視 Start -->
// ADD 2013.08.08 M.Honma 走行規制 車載器連携設定追加 Start -->
    private void InitDrivingRegulationSetting() {
		// 車載器連携
        btn_driving_regulation = (Button)oLayout.findViewById(R.id.btn_driving_regulation);
	    if(DrivingRegulation.IsAllCheckCooperativeEnv()){
            btn_driving_regulation.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
	            	if(DrivingRegulation.CheckDrivingRegulation()){
	            		return;
	            	}
                	// 外部API走行規制権限譲渡中
                	if( DrivingRegulation.IsDrivingControlLock() ){
                		showDialog( Constants.DIALOG_DURING_EXTERNAL_CONTROL );
                		return;
                	}

	                // 車載器連携選択ページへ
	                Intent intDrivingRegulationIntent = new Intent();
	                intDrivingRegulationIntent.setClass(contextThis, SelectDrivingRegulation.class);
	                NaviActivityStarter.startActivityForResult(((Activity)v.getContext()), intDrivingRegulationIntent, AppInfo.ID_ACTIVITY_DRIVING_REGULATION);
                }
            });
        }
        else{
        	LinearLayout otmpLayout = (LinearLayout) oLayout.findViewById(R.id.LinearLayoutDrivingRegulation);
        	if (otmpLayout != null) {
        		otmpLayout.setVisibility( View.GONE );
        	}
        	DrivingRegulation.SetDrivingRegulationGuide( DrivingRegulation.DEF_VALUE_GUIDE_NONE );
        	DrivingRegulation.UnBind();
        }
    }
// ADD 2013.08.08 M.Honma 走行規制 車載器連携設定追加 End -->
// MOD 2013.09.17 M.Honma 走行規制 SmartAccess監視  END  -->
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
    public void onProgressChanged(SeekBar seekBar, int progress,
            boolean fromTouch) {
        CommonLib.VOICE_PROGRESS = progress;
    }

    @Override
    public void onStartTrackingTouch(SeekBar arg0) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // stop処理
//		NaviRun.GetNaviRunObj().JNI_SetVolumeScale(CommonLib.VOICE_PROGRESS);
        NaviRun.GetNaviRunObj().JNI_NE_Drv_PlayChangeVolume();

        mAudio.setStreamVolume(AudioManager.STREAM_MUSIC, CommonLib.VOICE_PROGRESS, 0);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        final CustomDialog oDialog = new CustomDialog(this);
        Resources oRes = getResources();
        switch (id) {
            case DIALOG_VERSION:
                // バージョン情報ダイアログ
                JNIString JITitle = new JNIString();
                NaviRun.GetNaviRunObj().JNI_NE_GetDBDiskTitle(JITitle);

                String versionName = "";
                try {
                    PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
                    versionName = "Ver " + packageInfo.versionName;
                } catch (NameNotFoundException e) {
                    NaviLog.e(NaviLog.PRINT_LOG_TAG, "Error when getting version name.");
                }

                oDialog.setTitle(oRes.getString(R.string.version_information));
                oDialog.setMessage(oRes.getString(R.string.version_title1) + "\r\n" + versionName +
                        "\r\n" + oRes.getString(R.string.version_title2) + "\r\n" + JITitle.getM_StrAddressString());
                oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        //oDialog.dismiss();
                        removeDialog(DIALOG_VERSION);
                    }
                });
                break;
            case DIALOG_WALK_LINE:
                // 走行軌跡一括削除ダイアログ
                waitDialogType = this.WALK_LINE_DEL;

                oDialog.setTitle(oRes.getString(R.string.title_trace_deletion));
                oDialog.setMessage(oRes.getString(R.string.msg_trace_deletion));
                oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        startShowPage(NOTSHOWCANCELBTN);
                    }
                });
                oDialog.addButton(oRes.getString(R.string.btn_cancel), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
//					oDialog.cancel();
                        removeDialog(DIALOG_WALK_LINE);
                    }
                });
                break;
            case DIALOG_DEL_LIKE:
                // お気に入り一括削除ダイアログ
                waitDialogType = FAVORITES_DEL;
                oDialog.setTitle(oRes.getString(R.string.title_favorite_batch_deletion));
                oDialog.setMessage(oRes.getString(R.string.msg_favorite_batch_deletion));
                oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        startShowPage(NOTSHOWCANCELBTN);
                    }
                });
                oDialog.addButton(oRes.getString(R.string.btn_cancel), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
//					oDialog.cancel();
                        removeDialog(DIALOG_DEL_LIKE);
                    }
                });
                break;
            case DIALOG_DEL_HISTORY:
                // 履歴一括削除ダイアログ
                waitDialogType = HIS_DEL;
                oDialog.setTitle(oRes.getString(R.string.title_history_batch_deletion));
                oDialog.setMessage(oRes.getString(R.string.msg_del_hist_title));
                oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        startShowPage(NOTSHOWCANCELBTN);
                    }
                });
                oDialog.addButton(oRes.getString(R.string.btn_cancel), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
//					oDialog.cancel();
                        removeDialog(DIALOG_DEL_HISTORY);
                    }
                });
                break;
            case DIALOG_DEL_SYSTEM_INIT:
                // システム初期化
                waitDialogType = SYS_INIT;
                oDialog.setTitle(oRes.getString(R.string.title_system_initialization));
                oDialog.setMessage(oRes.getString(R.string.msg_sys_init));
                oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        startShowPage(NOTSHOWCANCELBTN);
                    }
                });
                oDialog.addButton(oRes.getString(R.string.btn_cancel), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
//					oDialog.cancel();
                        removeDialog(DIALOG_DEL_SYSTEM_INIT);
                    }
                });
                break;
            case DIALOG_REMOVE_WIDGET:
                // ウィジェットクリア
                waitDialogType = REMOVE_WIDGET;
                LayoutInflater inflater = LayoutInflater.from( this );
                View textEntryView = inflater.inflate( R.layout.alert_dialog_text, null );

                TextView alertText2 = (TextView)textEntryView.findViewById( R.id.alert_text2 );
                TextView alertText3 = (TextView)textEntryView.findViewById( R.id.alert_text3 );
                alertText2.setText( R.string.title_remove_widget );
                alertText3.setText( R.string.str_showdialog );
                Builder builder = new AlertDialog.Builder( this );
                builder.setTitle( R.string.remove_widget );
                builder.setView( textEntryView );

                builder.setPositiveButton( "OK", new DialogInterface.OnClickListener() {
                    public void onClick( DialogInterface dialog, int whichButton ) {
                        NaviRun.getMapActivity().widgetClear();
                        showWidgetRemovedDialog();
                        redrawMap();
                    }
                } );

                builder.setNegativeButton( R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick( DialogInterface dialog, int whichButton ) {
                        dialog.cancel();
                    }
                } );
                return builder.create();
            default:
                // XuYang add start 走行中の操作制限
                return super.onCreateDialog(id);
                // XuYang add end 走行中の操作制限
        }
        return oDialog;
    }

//Del 2011/09/30 Z01_h_yamada Start -->
//    private void initPage2Statu() {
//
//        // ユーザーモードの初期化
//        if (switchUserMode != null) {
//            // xuyang del start bug1749
////			JNIInt DriveMode = new JNIInt();
////			NaviRun.GetNaviRunObj().JNI_Java_GetDriveMode(DriveMode);
////			if (DriveMode.getM_iCommon() == 1) {
////				switchUserMode.setSelected(1);
////			} else {
////				switchUserMode.setSelected(0);
////			}
//            // xuyang del end bug1749
//
//            // xuyang add start bug1749
//            JNIInt DriveMode = new JNIInt();
//            // 徒歩モード、アローモードの場合
//            if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_BICYCLE
//                    || CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN) {
//                switchUserMode.getView(0).setEnabled(false);
////Chg 2011/09/21 Z01_h_yamada Start -->
////                switchUserMode.getView(0).setBackgroundResource(R.drawable.common_84_non);
////--------------------------------------------
//                switchUserMode.getView(0).setBackgroundResource(R.drawable.btn_default_non);
////Chg 2011/09/21 Z01_h_yamada End <--
//                switchUserMode.getView(1).setEnabled(false);
////Chg 2011/09/21 Z01_h_yamada Start -->
////                switchUserMode.getView(1).setBackgroundResource(R.drawable.common_84_non);
////--------------------------------------------
//                switchUserMode.getView(1).setBackgroundResource(R.drawable.btn_default_non);
////Chg 2011/09/21 Z01_h_yamada End <--
//            } else {
//                switchUserMode.getView(0).setEnabled(true);
//                switchUserMode.getView(1).setEnabled(true);
//                NaviRun.GetNaviRunObj().JNI_Java_GetDriveMode(DriveMode);
//                if (DriveMode.getM_iCommon() == 1) {
//                    switchUserMode.setSelected(1);
//                } else {
//                    switchUserMode.setSelected(0);
//                }
//            }
//            // xuyang add end bug1749
//
//        }
//    }
//Del 2011/09/30 Z01_h_yamada End <--

    private void initPage1Statu() {
        // 車種設定の初期化
        if (switchCarType != null) {
            JNIInt JICarType = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_Drv_GetCarType(JICarType);
            int carType = JICarType.getM_iCommon();
            switchCarType.setSelected(carType);
        }
        // ルート探索条件（徒歩）の初期化
        if (switchWalkMode != null) {
            JNIInt JIManSearchProperty = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_Man_GetDefaultSearchProperty(
                    JIManSearchProperty);
            int walkMode = JIManSearchProperty.getM_iCommon();
            switchWalkMode.setSelected(walkMode);
        }
        // ルート探索条件（車）の初期化
        if (switchCarMode != null) {
            JNIInt JICarSearchProperty = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_Drv_GetDefaultSearchProperty(
                    JICarSearchProperty);
            switch (JICarSearchProperty.getM_iCommon()) {
                case NE_CarNaviSearchProperty_Commend:
                    switchCarMode.setSelected(NE_CarNaviSearchProperty_Commend - 1);
                    break;
                case NE_CarNaviSearchProperty_General:
                    switchCarMode.setSelected(NE_CarNaviSearchProperty_Distance - 1);
                    break;
                case NE_CarNaviSearchProperty_Distance:
                    switchCarMode.setSelected(NE_CarNaviSearchProperty_General - 1);
                    break;
                case NE_CarNaviSearchProperty_Width:
                    switchCarMode.setSelected(NE_CarNaviSearchProperty_Width - 1);
                    break;
                case NE_CarNaviSearchProperty_Another:
                    switchCarMode.setSelected(NE_CarNaviSearchProperty_Another - 1);
                    break;
            }
        }
    }

    private void initPageStatu() {
        mSeekBar.setProgress(CommonLib.VOICE_PROGRESS);
        // 季節規制 考慮状態の初期化
        if (switchSeason != null) {
            JNIInt JIComplexProperty = new JNIInt();
            // 季節規制 考慮状態の初期化
            NaviRun.GetNaviRunObj()
                    .JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
            if ((JIComplexProperty.getM_iCommon() & CarNaviComplexProperty_ThinkWinterClose) != 0) {
                switchSeason.setSelected(0);
            } else {
                switchSeason.setSelected(1);
            }
        }
        // 時間帯規制 考慮状態の初期化
        if (switchTime != null) {
            JNIInt JIComplexProperty = new JNIInt();
            // 時間帯規制 考慮状態の初期化
            NaviRun.GetNaviRunObj()
                    .JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
            if ((JIComplexProperty.getM_iCommon() & CarNaviComplexProperty_ThinkTimeRule) != 0) {
                switchTime.setSelected(0);
            } else {
                switchTime.setSelected(1);
            }
        }
        // 渋滞を考慮した探索状態の初期化
        if (switchTraffic != null) {
            JNIInt JIComplexProperty = new JNIInt();
            // 渋滞を考慮した探索状態の初期化
            NaviRun.GetNaviRunObj()
                    .JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
            if ((JIComplexProperty.getM_iCommon() & CarNaviComplexProperty_UseVICS) != 0) {
                switchTraffic.setSelected(0);
            } else {
                switchTraffic.setSelected(1);
            }
        }
//Add 2011/09/30 Z01yamaguchi Start -->
        // VICS更新間隔
        if (switchVicsUpdateInterval != null) {
            JNIInt JIVicsInfo = new JNIInt();
            // 渋滞を考慮した探索状態の初期化
            NaviRun.GetNaviRunObj().JNI_NE_GetVicsInfo(JIVicsInfo);
            switchVicsUpdateInterval.setSelected(JIVicsInfo.getM_iCommon());
        }
//Add 2011/09/30 Z01yamaguchi End <--
        // 料金所の音声案内状態の初期化
        if (switchToll != null) {
            JNIInt TollGuide = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_GetTollGuide(TollGuide);
            if (TollGuide.getM_iCommon() == 1) {
                switchToll.setSelected(0);
            } else {
                switchToll.setSelected(1);
            }
        }
//Add 2011/09/05 Z01yoneya Start -->
        // 自律航法
        if (switchSensorNavi != null) {
            if (SensorNavigationManager.isEnableSensorNavi()) {
                switchSensorNavi.setSelected(0);
            } else {
                switchSensorNavi.setSelected(1);
            }
        }
//Add 2011/09/05 Z01yoneya End <--
    	
//Add 2014/06/01 CPJsunagawa Start -->
        if (switchGuideBoard != null) {

        	// switchGuideBoard.setSelectedは、1と0が逆
            if (loadGuideBoardDisplay(contextThis) == 0) {
            	//switchGuideBoard.setSelected(0);
            	switchGuideBoard.setSelected(1);
// Mod by CPJsunagawa '2015-05-31
            	NaviRun.GetNaviRunObj().JNI_NE_setIsGuide(NaviRun.NE_IsGuide_NODISPLAY);
//            	NaviRun.GetNaviRunObj().NE_setIsGuide(NaviRun.NE_IsGuide_NODISPLAY);
            } else {
            	//switchGuideBoard.setSelected(1);
            	switchGuideBoard.setSelected(0);
// Mod by CPJsunagawa '2015-05-31
            	NaviRun.GetNaviRunObj().JNI_NE_setIsGuide(NaviRun.NE_IsGuide_DISPLAY);
//            	NaviRun.GetNaviRunObj().NE_setIsGuide(NaviRun.NE_IsGuide_DISPLAY);
            }
            
/*        	
        	JNIInt JIComplexProperty = new JNIInt();
            // 案内板表示
            NaviRun.GetNaviRunObj()
                    .JNI_NE_Drv_GetComplexProperty(JIComplexProperty);
            if ((JIComplexProperty.getM_iCommon() & CarNaviComplexProperty_GuideBoardDisplay) != 0) {
            	switchGuideBoard.setSelected(0);
            } else {
            	switchGuideBoard.setSelected(1);
            }
*/
        }
        else
        {
        	Toast.makeText(this, "switchGuideBoardがnull!!!", Toast.LENGTH_LONG).show();
        }
//Add 2014/06/01 CPJsunagawa End <--

//// ADD 2013.08.08 M.Honma 走行規制 車載器連携設定追加 Start -->
//        // 車載器連携
//        if(switchDrivingRegulation != null)
//        {
//            boolean value = DrivingRegulation.GetDrivingRegulationGuide();
//            if (value) {
//            	switchDrivingRegulation.setSelected(0);
//            } else {
//            	switchDrivingRegulation.setSelected(1);
//            }
//        }
//// ADD 2013.08.08 M.Honma 走行規制 車載器連携設定追加 End -->
    }

    private void updateNaviDetailSetting() {
        // 本画面更新
        initPageStatu();
        initPage1Statu();
//Del 2011/09/30 Z01_h_yamada Start -->
//        initPage2Statu();
//Del 2011/09/30 Z01_h_yamada End <--
    }

    /**
     * Created on 2010/12/16
     * Description: 地図は重さを更新してかきます
     *
     * @param void 無し
     * @return void 無し
     * @author: XuYang
     */
    public void redrawMap() {
        //int ActiveType = openMapActiveType;
        /*
        int ActiveType = 0;
        int TransType = 0;
        byte bMapLevel = 0;
        long Longitude = 0;
        long Latitude = 0;
        JNIByte bApplv = new JNIByte();
        JNITwoLong coordinate = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(coordinate);
        NaviRun.GetNaviRunObj().JNI_NE_GetMapLevel(bApplv);
        bMapLevel = (byte) bApplv.getM_bScale();
        Longitude = coordinate.getM_lLong();
        Latitude = coordinate.getM_lLat();
        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(ActiveType, TransType,
        		bMapLevel, Longitude, Latitude);
        */
        NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();
    }

    /**
     * Created on 2010/12/16
     * Description: 設定して音を操作します
     *
     * @param iOPSound
     *            音の設定値を操作します
     * @return void 無し
     * @author: XuYang
     */
    private void SetOPSound(int iOPSound) {
        Constants.gs_iOPSound = iOPSound;
    }

//Del 2011/11/23 Z01_h_yamada Start -->
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//    }
//Del 2011/11/23 Z01_h_yamada End <--

    @Override
    protected boolean onStartShowPage() throws Exception {
        if (waitDialogType == this.WALK_LINE_DEL) {
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStartShowPage WALK_LINE_DEL");
            removeDialog(DIALOG_WALK_LINE);
            NaviRun.GetNaviRunObj().JNI_NE_ClearVehicleTrack();
            return true;
        } else if (waitDialogType == this.FAVORITES_DEL) {
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStartShowPage FAVORITES_DEL");
            removeDialog(DIALOG_DEL_LIKE);
            //yangyang add start 20110415 Bug981,Bug982
            NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
            //yangyang add end 20110415 Bug981,Bug982
            NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Reset();
            return true;
            //yangyang add end Bug981
        } else if (waitDialogType == this.HIS_DEL) {
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStartShowPage HIS_DEL");
            removeDialog(DIALOG_DEL_HISTORY);
            //yangyang add start 20110415 Bug981,Bug982
            POI_History_FilePath path = new POI_History_FilePath();
            NaviRun.GetNaviRunObj().JNI_NE_POIHist_Clear(path);
            //yangyang add end 20110415 Bug981,Bug982
            NaviRun.GetNaviRunObj().JNI_NE_POIHist_Reset();
            redrawMap();
            return true;
        } else if (waitDialogType == this.SYS_INIT) {
            removeDialog(DIALOG_DEL_SYSTEM_INIT);
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStartShowPage SYS_INIT");
            //yangyang add start 20110415 Bug981,Bug982
            POI_History_FilePath path = new POI_History_FilePath();
            NaviRun.GetNaviRunObj().JNI_NE_POIHist_Clear(path);
            NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
            //yangyang add end 20110415 Bug981,Bug982
            NaviRun.GetNaviRunObj().JNI_NE_POIHist_Reset();
            NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Reset();
            // ガイド音量
            SetOPSound(ON);
            NaviRun.GetNaviRunObj().JNI_NE_SetGuideVoice(ON);
//			CommonLib.VOICE_PROGRESS = 50;
//			NaviRun.GetNaviRunObj().JNI_SetVolumeScale(CommonLib.VOICE_PROGRESS);
            CommonLib.VOICE_PROGRESS = mAudio.getStreamMaxVolume(AudioManager.STREAM_MUSIC) / 2; // メディア再生音量の中間値
// Add 2011/10/06 katsuta Start -->
            mAudio.setStreamVolume(AudioManager.STREAM_MUSIC, CommonLib.VOICE_PROGRESS, 0);
// Add 2011/10/06 katsuta End <--
//			CommonLib.VOICE_PROGRESS = mAudio.getStreamVolume(AudioManager.STREAM_MUSIC);
            // 季節規制 考慮   OR 時間帯規制 考慮 OR 渋滞を考慮した探索
            NaviRun.GetNaviRunObj()
                    .JNI_NE_Drv_SetComplexProperty(
                            CarNaviComplexProperty_UseFerry
                                    | CarNaviComplexProperty_ThinkWinterClose
                                    | CarNaviComplexProperty_ThinkTimeRule
                                    | CarNaviComplexProperty_UseVICS
                                    | CarNaviComplexProperty_GuideBoardDisplay);  // Add by CPJsunagawa '14-06-02

            // 料金所の音声案内
            NaviRun.GetNaviRunObj().JNI_NE_SetTollGuide(1);
            // 車種設定
            NaviRun.GetNaviRunObj().JNI_NE_Drv_SetCarType(KINDCAR_NORMAL);
            // ルート探索条件（徒歩）
            NaviRun.GetNaviRunObj().JNI_NE_Man_SetDefaultSearchProperty(
                    ManNaviSearchProperty_Default);
            NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(8, ManNaviSearchProperty_Default);
            // ルート探索条件（車）
            NaviRun.GetNaviRunObj().JNI_NE_Drv_SetDefaultSearchProperty(
                    NE_CarNaviSearchProperty_Commend);
            NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(1, 8,
                    NE_CarNaviSearchProperty_Commend);

            // ユーザーモード
            NaviRun.GetNaviRunObj().JNI_Java_SetDriveMode(0);

            // アイコンサイズ
            NaviRun.GetNaviRunObj().JNI_NE_SetMapIconSize(
                    Constants.MapIconSize_Standard);
            // 文字サイズ
            NaviRun.GetNaviRunObj().JNI_NE_SetMapWordSize(
                    Constants.MapWordSize_Standard);

            // 一方通行表示
            NaviRun.GetNaviRunObj().JNI_NE_SetDispMapOneway(
                    Constants.ONEWAY_ON);

            // ヘディングアップ
            NaviRun.GetNaviRunObj().JNI_NE_SetMapHeadingup(
                    Constants.HEADINGUP_ON);
            // フロントワイド
            NaviRun.GetNaviRunObj().JNI_NE_SetMapFrontWide(
                    Constants.FRONT_ON);
            // 走行軌跡表示
            NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(1);
// Add 2011/10/06 katsuta Start -->
            // 走行軌跡削除
            NaviRun.GetNaviRunObj().JNI_NE_ClearVehicleTrack();
// Add 2011/10/06 katsuta End <--
            // 画面の自動回転
            NaviRun.GetNaviRunObj().JNI_Java_SetSettingInfo(1);

            // 細街路侵入時の地図自動切換え
            NaviRun.GetNaviRunObj().JNI_NE_SetMinorRoad(1);

            // 初期表示縮尺
            NaviRun.GetNaviRunObj().JNI_NE_SetMapScale(
                    Constants.MAPSCALE_LOWERLEVEL);

            // 地図色設定
            MapTypeListioner.isActive = false;
            NaviRun.GetNaviRunObj().JNI_NE_SetMPMode(Constants.MAPMODE_AUTO);
            if (!CommonLib.isBMapColorAuto()) {
                JNITwoLong Coordinate = new JNITwoLong();
                NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);
                boolean isDay = MapTypeListioner.getClockStsIsDay((int)Coordinate.getM_lLat(), (int)Coordinate.getM_lLong());
                if (isDay) {
                    NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(
                                Constants.TGL_MAPCOLOR_DAY);
                } else {
                    NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(
                                Constants.MAPCOLOR_NIGHT);
                }
                CommonLib.setBMapColorAuto(true);
                Thread t = new Thread(new MapTypeListioner());
                MapTypeListioner.isRuning = true;
                MapTypeListioner.isActive = true;
                t.start();
            } else {
                MapTypeListioner.isActive = true;
            }
// Add 2011/10/06 katsuta Start -->
            // 自律航法設定
            if (SensorNavigationManager.isEnableSensorNaviFlagSwitch()) {
//Chg 2011/11/23 Z01_h_yamada Start -->
//                SensorNavigationManager.enableSensorNavi(true);
//--------------------------------------------
                SensorNavigationManager.enableSensorNavi(false);
//Chg 2011/11/23 Z01_h_yamada End <--
            }
// Add 2011/10/06 katsuta End <--

// Add 2011/10/09 katsuta Start -->
            if (!((NaviApplication)getApplication()).getNaviAppDataPath().isExistVicsData()) {
            	// VICS更新間隔を設定
            	NaviRun.GetNaviRunObj().JNI_NE_SetVicsInfo(Constants.VICSINFO_15);
            }
// Add 2011/10/09 katsuta End <--

// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 車載器連携設定追加 Start -->
            // 車載器連携 しない
            DrivingRegulation.SetDrivingRegulationGuide( DrivingRegulation.DEF_VALUE_GUIDE_NONE );
// ADD 2013.08.08 M.Honma 走行規制 車載器連携設定追加 End -->
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

			//スケール
			NaviRun.GetNaviRunObj().JNI_ct_uic_SetMapLevel(Constants.MAPSCALE_DEFAULTLEVEL_MAIN);
			//モード
			CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
			return true;
		}
		return false;
	}

    @Override
    protected void onFinishShowPage(boolean bGetData) throws Exception {
        if (bGetData) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage end");
                    // XuYang add start #1012,#1031
                    if (waitDialogType == SYS_INIT) {
                        updateNaviDetailSetting();
//Chg 2011/09/26 Z01yoneya Start -->
//                        CommonLib.deleteRouteFile();
//                        CommonLib.creatXml();
//------------------------------------------------------------------------------------------------------------------------------------
                        CommonLib.deleteRouteDataXml(((NaviApplication)getApplication()).getNaviAppDataPath());
                        CommonLib.creatRouteDataXml(((NaviApplication)getApplication()).getNaviAppDataPath());
//Chg 2011/09/26 Z01yoneya End <--
//// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
//                        // ドライブコンテンツ初期化
//                        DriverContentsCtrl.getController().sendEventCode(Constants.DC_DATA_CLEAR, null);
//// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
                    }
                    // XuYang add end #1012,#1031
                    redrawMap();
                    NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage end");
                }

            });
        }

    }

//	@Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//
//        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
//            return true;
//        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
//            return true;
//        }
//        return super.onKeyUp(keyCode, event);
//    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
//        boolean ret = super.onKeyUp(keyCode, event);
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            CommonLib.VOICE_PROGRESS = mAudio.getStreamVolume(AudioManager.STREAM_MUSIC);
            if (CommonLib.VOICE_PROGRESS < mAudio.getStreamMaxVolume(AudioManager.STREAM_MUSIC)) {
                CommonLib.VOICE_PROGRESS++;
            }
//    		mAudio.setStreamVolume( AudioManager.STREAM_MUSIC, CommonLib.VOICE_PROGRESS, AudioManager.FLAG_SHOW_UI);
//        	NaviLog.d(getPackageName(), "CommonLib.VOICE_PROGRESS:"+ CommonLib.VOICE_PROGRESS);
            initPageStatu();
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            CommonLib.VOICE_PROGRESS = mAudio.getStreamVolume(AudioManager.STREAM_MUSIC);
            if (CommonLib.VOICE_PROGRESS > 0) {
                CommonLib.VOICE_PROGRESS--;
            }
//    		mAudio.setStreamVolume( AudioManager.STREAM_MUSIC, CommonLib.VOICE_PROGRESS, AudioManager.FLAG_SHOW_UI);
//        	NaviLog.d(getPackageName(), "CommonLib.VOICE_PROGRESS:"+ CommonLib.VOICE_PROGRESS);
            initPageStatu();

        }
        return super.onKeyUp(keyCode, event); //ret;


    }

//Add 2011/11/14 Z01_h_yamada Start -->
    protected void onPause() {
    	NaviRun.GetNaviRunObj().JNI_saveConfig();
        super.onPause();
    }
//Add 2011/11/14 Z01_h_yamada End   <--

//Add 2011/09/30 Z01yamaguchi Start -->
    @Override
    protected void onDestroy() {
        super.onDestroy();

        // アクティビティの終了時に、VICS に更新間隔の変更を通知
        VicsManager vicsManager = VicsManager.getInstance();
        if (vicsManager != null) {
            vicsManager.onChangedOption();
        }
    }
//Add 2011/09/30 Z01yamaguchi End <--

    private void showWidgetRemovedDialog()
    {
        Builder dialog = new AlertDialog.Builder( this );
        dialog.setTitle( R.string.remove_widget );
        dialog.setMessage( R.string.widget_removed_msg );
        dialog.setPositiveButton( "OK", new DialogInterface.OnClickListener() {
            public void onClick( DialogInterface dialog, int whichButton )
            {
                dialog.cancel();
            }
        } );

        dialog.show();
    }
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading#onResume()
	 */
	@Override
	protected void onResume() {
		InitDrivingRegulationSetting();
		// TODO 自動生成されたメソッド・スタブ
		super.onResume();
	}
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
}
