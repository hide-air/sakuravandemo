/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNUI_IMAGE.java
 * Description    画像情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNUI_IMAGE {
    private short sWidth;
    private short sHeight;
    private int[] SPixRGBA32 = null;

    /**
    * Created on 2010/08/06
    * Title:       initSPixRGBA32
    * Description:  関数を初期化します
    * @param1  int len
    * @return       void

    * @version        1.0
    */
    public void initSPixRGBA32(int len){
        SPixRGBA32 = new int[len];
    }
    /**
    * Created on 2010/08/06
    * Title:       getSWidth
    * Description:  画像幅(ピクセル)   //現在は画像幅は4の倍数限定実装を取得する
    * @param1  無し
    * @return       short

    * @version        1.0
    */
    public short getSWidth() {
        return sWidth;
    }
    /**
    * Created on 2010/08/06
    * Title:       getSHeight
    * Description:  画像高さ(ピクセル)を取得する
    * @param1  無し
    * @return       short

    * @version        1.0
    */
    public short getSHeight() {
        return sHeight;
    }

    /**
    * Created on 2010/08/06
    * Title:       getSPixRGBA32
    * Description:  32bit RGBA ピクセルイメージを取得する
    * @param1  無し
    * @return       int[]

    * @version        1.0
    */
    public int[] getSPixRGBA32() {
        return SPixRGBA32;
    }
}
