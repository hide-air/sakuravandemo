package net.zmap.android.pnd.v2.sensor;

import android.hardware.Sensor;

public class GyroscopeSensor {
	private final static int NOISE_NUM_SAMPLES = 5;			///< ノイズ平滑化サンプル数
	private final static int OFFSET_SAMPLING_INTERVAL = 3;	///< ゼロ補正オフセットサンプリング秒 (sec)
	private final static int UPDATE_OFFSETS_INTERVAL = 1;	///< ゼロ補正オフセット更新間隔 (sec)
	private final float NS2S = 1.0f / 1000000000.0f;

	public Sensor sensor;

	//ジャイロセンサーのイベント生値は下記のとおりに格納される
	//[0] : azimath(yaw,左回転が正）
	//[1] : pitch(奥に倒すと正)
    //[2] : roll(左に傾けると正）
	public final static char GYRO_DIR_0_AZ		=0;	//ヨー	//Z
	public final static char GYRO_DIR_1_PITCH	=1;	//ピッチ//X
	public final static char GYRO_DIR_2_ROLL	=2;	//ロール//Y

	public float rvalues[]		= new float[3];	///< 生の値 (m/s)
	public float nrvalues[]		= new float[3];	///< ノイズ除去 (m/s)
	public float angularSpeed[] = new float[3];	///< 回転前 (m/s)
	public float angularSpeedT[]= new float[3];	///< 回転後 (m/s)
	public float offsets[]		= new float[3];	///< ゼロ補正オフセット
	public float dt;							///< サンプリング間隔

    // V2.5
	public long	Gyrotimestamp_Start = 0;		///< Gyro Start タイムスタンプ
	public long	Gyrotimestamp_End = 0;			///< Gyro Start タイムスタンプ
	public long	Gyro_1SectimeCnt = 0;			///< Gyro 1秒経過カウンタ

	public long gyro_t[]		= new long[201];		///< 送る値
	public float gyro_d[][]		= new float[3][201];	///< 送る値

	protected float _timestamp = 0;

	protected CircularBuffer _noiseBuffer[] = new CircularBuffer[3];
	protected CircularBuffer _zeroBuffer[] = new CircularBuffer[3];

    protected Matrix3D _matrix = new Matrix3D();

	public GyroscopeSensor(Sensor gyroscope) {
		sensor = gyroscope;

		// ノイズ平滑化用
		_noiseBuffer[GYRO_DIR_0_AZ	] = new CircularBuffer();
		_noiseBuffer[GYRO_DIR_1_PITCH	] = new CircularBuffer();
		_noiseBuffer[GYRO_DIR_2_ROLL	] = new CircularBuffer();

		// ゼロ補正用
		_zeroBuffer[GYRO_DIR_0_AZ		] = new CircularBuffer();
		_zeroBuffer[GYRO_DIR_1_PITCH	] = new CircularBuffer();
		_zeroBuffer[GYRO_DIR_2_ROLL	] = new CircularBuffer();

		_matrix.invert(0.0f, 0.0f, 0.0f);
	}
//Add 2011/09/07 Z01yoneya Start -->
    /*
     * バッファデータクリア
     */
    public void clearBuffer(){
        _timestamp = 0;
        if(_noiseBuffer[GYRO_DIR_0_AZ	] != null){ _noiseBuffer[GYRO_DIR_0_AZ	 ].clear(); }
        if(_noiseBuffer[GYRO_DIR_1_PITCH] != null){ _noiseBuffer[GYRO_DIR_1_PITCH].clear(); }
        if(_noiseBuffer[GYRO_DIR_2_ROLL	] != null){ _noiseBuffer[GYRO_DIR_2_ROLL ].clear(); }

        if(_zeroBuffer[GYRO_DIR_0_AZ	] != null){ _zeroBuffer[GYRO_DIR_0_AZ	 ].clear(); }
        if(_zeroBuffer[GYRO_DIR_1_PITCH	] != null){ _zeroBuffer[GYRO_DIR_1_PITCH ].clear(); }
        if(_zeroBuffer[GYRO_DIR_2_ROLL	] != null){ _zeroBuffer[GYRO_DIR_2_ROLL	 ].clear(); }
    }
//Add 2011/09/07 Z01yoneya End <--
	protected static long _prevTime = 0;

	public void zerofy() {
		long curTime = System.currentTimeMillis();
		if (_prevTime == 0 || curTime - _prevTime > UPDATE_OFFSETS_INTERVAL * 1000) {
			offsets[GYRO_DIR_0_AZ		] = _zeroBuffer[GYRO_DIR_0_AZ		].averageInSeconds(OFFSET_SAMPLING_INTERVAL);
			offsets[GYRO_DIR_1_PITCH	] = _zeroBuffer[GYRO_DIR_1_PITCH	].averageInSeconds(OFFSET_SAMPLING_INTERVAL);
			offsets[GYRO_DIR_2_ROLL	] = _zeroBuffer[GYRO_DIR_2_ROLL	].averageInSeconds(OFFSET_SAMPLING_INTERVAL);
			_prevTime = curTime;
		}
	}

	public void setMatrix(Matrix3D matrix) {
		_matrix = matrix;
	}

	public void onSensorChanged(RawSensorEvent event) {
		long now = System.currentTimeMillis();

		// 生の値を保存
		rvalues[GYRO_DIR_0_AZ		] = event.values[GYRO_DIR_0_AZ	];
		rvalues[GYRO_DIR_1_PITCH	] = event.values[GYRO_DIR_1_PITCH	];
		rvalues[GYRO_DIR_2_ROLL	] = event.values[GYRO_DIR_2_ROLL	];

		// ノイズ除去
		_noiseBuffer[GYRO_DIR_0_AZ	].put(now, rvalues[GYRO_DIR_0_AZ		]);
		_noiseBuffer[GYRO_DIR_1_PITCH	].put(now, rvalues[GYRO_DIR_1_PITCH	]);
		_noiseBuffer[GYRO_DIR_2_ROLL	].put(now, rvalues[GYRO_DIR_2_ROLL	]);
		nrvalues[GYRO_DIR_0_AZ	] = _noiseBuffer[GYRO_DIR_0_AZ	].average(NOISE_NUM_SAMPLES);
		nrvalues[GYRO_DIR_1_PITCH	] = _noiseBuffer[GYRO_DIR_1_PITCH	].average(NOISE_NUM_SAMPLES);
		nrvalues[GYRO_DIR_2_ROLL	] = _noiseBuffer[GYRO_DIR_2_ROLL	].average(NOISE_NUM_SAMPLES);

//Chg 2011/03/21 Z01yoneya Start-->
//		angularSpeed[0] = nrvalues[0];
//		angularSpeed[1] = nrvalues[1];
//		angularSpeed[2] = nrvalues[2];
//				// 回転
//		float[] tvalues = _matrix.convert(angularSpeed[0], angularSpeed[1], angularSpeed[2]);
//
//		// ゼロ補正用
//		_zeroBuffer[0].put(now, tvalues[0]);
//		_zeroBuffer[1].put(now, tvalues[1]);
//		_zeroBuffer[2].put(now, tvalues[2]);
//
//		angularSpeedT[0] = tvalues[0] - offsets[0];
//		angularSpeedT[1] = tvalues[1] - offsets[1];
//		angularSpeedT[2] = tvalues[2] - offsets[2];
//---------------------------------------------------------
		// ゼロ補正用 (ノイズ除去値の平均値をゼロ補正にする、回転前)
		_zeroBuffer[GYRO_DIR_0_AZ		].put(now,nrvalues[GYRO_DIR_0_AZ		]);
		_zeroBuffer[GYRO_DIR_1_PITCH	].put(now,nrvalues[GYRO_DIR_1_PITCH	]);
		_zeroBuffer[GYRO_DIR_2_ROLL	].put(now,nrvalues[GYRO_DIR_2_ROLL	]);

		angularSpeed[GYRO_DIR_0_AZ	] = nrvalues[GYRO_DIR_0_AZ	] - offsets[GYRO_DIR_0_AZ		];
		angularSpeed[GYRO_DIR_1_PITCH	] = nrvalues[GYRO_DIR_1_PITCH	] - offsets[GYRO_DIR_1_PITCH	];
		angularSpeed[GYRO_DIR_2_ROLL	] = nrvalues[GYRO_DIR_2_ROLL	] - offsets[GYRO_DIR_2_ROLL	];
		// 回転
		float[] tvalues = _matrix.convert(angularSpeed[GYRO_DIR_0_AZ], angularSpeed[GYRO_DIR_1_PITCH], angularSpeed[GYRO_DIR_2_ROLL]);

		angularSpeedT[GYRO_DIR_0_AZ	] = tvalues[GYRO_DIR_0_AZ		];
		angularSpeedT[GYRO_DIR_1_PITCH] = tvalues[GYRO_DIR_1_PITCH	];
		angularSpeedT[GYRO_DIR_2_ROLL	] = tvalues[GYRO_DIR_2_ROLL	];
//Chg 2011/03/21 Z01yoneya End <--


		if (_timestamp != 0) {
			dt = (event.timestamp - _timestamp) * NS2S;
		}
		_timestamp = event.timestamp;
	}
}
