package net.zmap.android.pnd.v2.common.data;

import java.io.Serializable;

public class PoiBaseData implements Serializable
{
	/**
	 *
	 */
	private static final long serialVersionUID = -6652109963371393561L;


	public String m_sName = "";
	public long m_wLat;
	public long m_wLong;
	public String m_sAddress = "";

}
