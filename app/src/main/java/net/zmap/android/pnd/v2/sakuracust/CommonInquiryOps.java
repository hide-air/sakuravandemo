package net.zmap.android.pnd.v2.sakuracust;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.DeliveryInfo;
import net.zmap.android.pnd.v2.common.ProductInfo;
import net.zmap.android.pnd.v2.common.utils.DbHelper;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.route.data.RouteNodeData;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;

/**
 * さくらのInquiryに関する共通の処理
 * @author Norimasa
 *
 */
public class CommonInquiryOps {
	/**
	 * DBから取得して配達リストとルートリストを格納する。
	 * @param upperAct
	 * @param cur
	 * @param db
	 * @param carNo
	 */
	public static void setDeliveryInfoFromDB(Activity upperAct, Cursor cur, SQLiteDatabase db, int carNo)
	{
        int idxCustID = cur.getColumnIndex("CustID");
        //int idxCode = cur.getColumnIndex("DeliveryOrder");
        int idxCode = cur.getColumnIndex("CustCode");
        int idxOrder = cur.getColumnIndex("DeliveryOrder");
        int idxCustomer = cur.getColumnIndex("Customer");
        int idxTelNo = cur.getColumnIndex("TelNo");
        //int idxPref = cur.getColumnIndex("PREF");
        //int idxCity = cur.getColumnIndex("CITY");
        int idxAddress = cur.getColumnIndex("Address");
        int idxMatchLevel = cur.getColumnIndex("MatchLevel");
        int idxChangeFlag = cur.getColumnIndex("ChangeFlag");
        int idxContract = cur.getColumnIndex("Contract");
        int idxSeiyakuDate = cur.getColumnIndex("SeiyakuDate");
        int idxTorihikiDate = cur.getColumnIndex("TorihikiDate");
        int idxKaiyakuDate = cur.getColumnIndex("KaiyakuDate");
        int idxKaiyakuReason = cur.getColumnIndex("KaiyakuReason");
        int idxKaishuKind = cur.getColumnIndex("KaishuKind");
        int idxCollectComment = cur.getColumnIndex("CollectingMemo");
        int idxPoint = cur.getColumnIndex("CustPoint");
        int idxCourse = cur.getColumnIndex("DeliCourse");
        int idxDeliOrder = cur.getColumnIndex("DeliOrder");
        int idxDeliComment = cur.getColumnIndex("DeliComment");
        int idxMapNo = cur.getColumnIndex("MapNo");
        int idxBoxPlace = cur.getColumnIndex("BoxPlace");
        int idxContactingMemo = cur.getColumnIndex("ContactingMemo");
        int idxBirthDay = cur.getColumnIndex("BirthDay");
/*
        int idxProduct = cur.getColumnIndex("ARTICLE");
        int idxProductRyaku = cur.getColumnIndex("ARTICLERYAKU");
        int idxProductCnt = cur.getColumnIndex("ARTICLECOUNT");
        //int idxLastDate = cur.getColumnIndex("LastDate");
        int idxClaim = cur.getColumnIndex("DELIVERYCOMMENT");
        //int idxWorldLat = cur.getColumnIndex("WorldLat");
        //int idxWorldLon = cur.getColumnIndex("WorldLon");
*/
        //int idxJpnLat = cur.getColumnIndex("LATITUDE");
        //int idxJpnLon = cur.getColumnIndex("LONGITUDE");
        int idxJpnLat = cur.getColumnIndex("Latitude");
        int idxJpnLon = cur.getColumnIndex("Longitude");
// カメラ連携等 Start
        int idxPhotoNum = cur.getColumnIndex("PhotoNum");
        int idxPhotoPath = cur.getColumnIndex("PhotoPath");
        int idxPHOTO1 = cur.getColumnIndex("PhotoFile1");
        int idxPHOTO2 = cur.getColumnIndex("PhotoFile2");
        int idxPHOTO3 = cur.getColumnIndex("PhotoFile3");
        int idxPHOTO4 = cur.getColumnIndex("PhotoFile4");
        int idxPHOTO5 = cur.getColumnIndex("PhotoFile5");
        int idxMemoPath = cur.getColumnIndex("HandWrittenMemoPath");
        int idxMemo = cur.getColumnIndex("HandWrittenMemoFile");
// カメラ連携等 End
		
        cur.moveToFirst();
        NaviApplication app = (NaviApplication)upperAct.getApplication();
        app.initRouteList(); // 基本あり得ないが、DBが更新されるケースを考慮して、画面に入る度に作り直す
        app.initInfoList();
        app.clearCloseUpPoint();
        
        // 開始地点(現在地)
        JNITwoLong nowPos = new JNITwoLong();
        if (NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(nowPos) != 0)
        {
        	Intent preIntent = upperAct.getIntent();
        	nowPos.setM_lLat(preIntent.getLongExtra(Constants.INTENT_LATITUDE, 0));
        	nowPos.setM_lLong(preIntent.getLongExtra(Constants.INTENT_LONGITUDE, 0));
        }
        //Intent preIntent = upperAct.getIntent();
        //Bundle bundle = preIntent.getExtras();
        RouteNodeData routeNodeDataNew = new RouteNodeData();
        routeNodeDataNew.setName(upperAct.getResources().getString(R.string.route_start_point_name));
        //routeNodeDataNew.setLon("" + preIntent.getLongExtra(Constants.INTENT_LONGITUDE, 0));
        //routeNodeDataNew.setLat("" + preIntent.getLongExtra(Constants.INTENT_LATITUDE, 0));
        routeNodeDataNew.setLon("" + nowPos.getM_lLong());
        routeNodeDataNew.setLat("" + nowPos.getM_lLat());
        routeNodeDataNew.setDate("");
        routeNodeDataNew.setStatus("0");
        routeNodeDataNew.setMode("0");
        app.addRoutePoint(routeNodeDataNew);
        //String adrstr;
        double latdeg, londeg;
        
        //'13-09-28 暫定措置 取り急ぎTKYにする。
        //GeoPoint tky;
        //StringBuffer oneProSQL;
        Cursor oneProCr;
        int prodCol = -1, cntCol = -1, statusCol = -1;
        int onecnt;
        boolean isFirst;
        String datestr;
        final String invalidDate = "0";
        long lastLon = -1, lastLat = -1, nowLon, nowLat;
        String naviPath = app.getNaviAppDataPath().getNaviAppRootPath() + "/";
//        String[] str_items;

		// データ数は6のみを想定
        for(int i =0, len = cur.getCount(); i < len; i++, cur.moveToNext()) {
            //routeNodeDataNew = new RouteNodeData();
            //routeNodeDataNew.setName(cur.getString(idxAddress));
            
            // ↓ここで止まっている様子　仮でコメント
            latdeg = cur.getFloat(idxJpnLat);
            londeg = cur.getFloat(idxJpnLon);
            //if ()
/*        	
            try{
                //routeNodeDataNew.setLat(new Long( (long)(cur.getFloat(idxJpnLat)* 100000) * 36 ).toString()); // -> msec
                //routeNodeDataNew.setLon(new Long( (long)(cur.getFloat(idxJpnLon)* 100000) * 36 ).toString()); // -> msec
                routeNodeDataNew.setLat(new Long( (long)(latdeg * 100000) * 36 ).toString()); // -> msec
                routeNodeDataNew.setLon(new Long( (long)(londeg * 100000) * 36 ).toString()); // -> msec
            } catch( NumberFormatException e) { // 緯度、経度が変換できないデータは飛ばす
                continue;
            }
            routeNodeDataNew.setDate("");
            routeNodeDataNew.setStatus("0");
            routeNodeDataNew.setMode("0");
            app.addRoutePoint(routeNodeDataNew);
*/            
            // 属性情報の設定
            DeliveryInfo info = new DeliveryInfo();
            info.mCarNo = carNo;
            info.mDeliveryOrder = cur.getInt(idxOrder);
            info.mCustCode = cur.getString(idxCode);
            info.mCustID = cur.getInt(idxCustID);
            info.mCustmer = cur.getString(idxCustomer);
            info.mTelNo = cur.getString(idxTelNo);
            //adrstr = cur.getString(idxPref);
            //adrstr += cur.getString(idxCity);
            //adrstr += cur.getString(idxAddress);
            info.mAddress = cur.getString(idxAddress);
            //info.mAddress = adrstr;
            info.mMatchLebel = cur.getInt(idxMatchLevel);
            if (info.mMatchLebel == 0)
            {
            	// マッチング失敗時はN35°、E135°の地点で名高い
            	// 西脇市の日本へそ公園を指定する。
            	latdeg = 35.0;
            	londeg = 135.0;
            }
            // 暫定
            //info.mCustmer += " - " + info.mMatchLebel;
            info.mChangeFlag = cur.getInt(idxChangeFlag);
            info.mContract = cur.getString(idxContract);
            datestr = cur.getString(idxSeiyakuDate);
            info.mSeiyakuDate = !invalidDate.equals(datestr) ? datestr : "";
            datestr = cur.getString(idxTorihikiDate);
            info.mTorihikiDate = !invalidDate.equals(datestr) ? datestr : "";
            datestr = cur.getString(idxKaiyakuDate);
            info.mKaiyakuDate = !invalidDate.equals(datestr) ? datestr : "";
            info.mKaiyakuReason = cur.getString(idxKaiyakuReason);
            info.mKaishuKind = cur.getString(idxKaishuKind);
            info.mCollectingMemo = cur.getString(idxCollectComment);
            info.mCustPoint = cur.getInt(idxPoint);
            info.mDeliCourse = cur.getString(idxCourse);
            info.mDeliOrder = cur.getInt(idxDeliOrder);
            info.mDeliComment = cur.getString(idxDeliComment);
            info.mMapNo = cur.getString(idxMapNo);
            info.mBoxPlace = cur.getString(idxBoxPlace);
            info.mContactingMemo = cur.getString(idxContactingMemo);
            info.mBirthDay = cur.getString(idxBirthDay);
            
// カメラ連携等 Start
            info.mPhotoNum = cur.getInt(idxPhotoNum);
            info.mPhotoPath = cur.getString(idxPhotoPath);
            // Pathが無ければ再設定する
//            if( info.mPhotoPath == null || "".equals(info.mPhotoPath) ) {
                info.mPhotoPath = DeliveryInfo.PHOTO_PATH + info.mCustID;
                db.execSQL("UPDATE DeliveryInfo Set PhotoPath = '" + info.mPhotoPath + 
                            "' WHERE CustID = '" + info.mCustID + "';");
// Add by M.Suna 2014-12-30 端末内に撮影画像があれば、その情報をDBに更新する Start

                File dir = new File(naviPath + info.mPhotoPath);
                if (!dir.exists()) {
                }
                File[] files = dir.listFiles();
                String[] str_items;
//                str_items = new String[files.length + 1];
                str_items = new String[5];
                // 何枚撮影しても保存できるのは5枚がMAX
                if(files == null) {
                	info.mPhotoNum = 0;
                } else {
	                if(files.length < 5) {
	                	info.mPhotoNum = files.length;
	                } else {
	                	info.mPhotoNum = 5;
	                }
                }
//                for (int j = 0; j < files.length; j++) {
                for (int j = 0; j < info.mPhotoNum; j++) {
                    File file = files[j];
                    str_items[j] = file.getName();
                }

                // 顧客IDフォルダ配下のファイルをDBに格納する
// Mod by M.Suna ここをコメントにする Start
/*
                for (int j = 0; j < info.mPhotoNum-1; j++) {
                	info.mPhotos[j] = str_items[j];
                    db.execSQL("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(j + 1) + " = '" + info.mPhotos[j + 1] + 
                            "' WHERE CustID = '" + info.mCustID + "';");
                }
*/
// Mod by M.Suna ここをコメントにする End

// Add by M.Suna 2014-12-30 端末内に撮影画像があれば、その情報をDBに更新する End
//            }
            info.mPhotos = new String[DeliveryInfo.PHOTO_MAX_NUM];
// Mod by M.Suna 2014-12-30 端末内に撮影画像があれば、その情報をDBに更新する
/*
            info.mPhotos[0] = cur.getString(idxPHOTO1);
            info.mPhotos[1] = cur.getString(idxPHOTO2);
            info.mPhotos[2] = cur.getString(idxPHOTO3);
            info.mPhotos[3] = cur.getString(idxPHOTO4);
            info.mPhotos[4] = cur.getString(idxPHOTO5);
*/
            int		real_num = 0;
            if( str_items[0] != null) {
            	info.mPhotos[0] = str_items[0];
            	real_num++;
            }
            if( str_items[1] != null) {
            	info.mPhotos[1] = str_items[1];
            	real_num++;
            }
            if( str_items[2] != null) {
            	info.mPhotos[2] = str_items[2];
            	real_num++;
            }
            if( str_items[3] != null) {
            	info.mPhotos[3] = str_items[3];
            	real_num++;
            }
            if( str_items[4] != null) {
            	info.mPhotos[4] = str_items[4];
            	real_num++;
            }

            // ファイルが存在するのかチェックする(ファイル操作される可能性があるので）
            //int real_num = info.mPhotoNum;

            // ファイル数をDBに登録
            if( real_num != 0 ) {
	            db.execSQL("UPDATE DeliveryInfo Set PhotoNum = '" + real_num  + 
	                    "' WHERE CustID = '" + info.mCustID + "'");
            }
            for( int j = 0; j < real_num; j++) {
                db.execSQL("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(j + 1) + " = '" + info.mPhotos[j] + 
                    "' WHERE CustID = '" + info.mCustID + "';");
                //info.mPhotos[j] = info.mPhotos[j+1];
            }
            info.mPhotoNum = real_num;

// 以下をコメントにする
/*            
            if( real_num >= 1) {
                if( info.mPhotos[0] == null || "".equals(info.mPhotos[0]) ||
                    !new File(naviPath + info.mPhotoPath + "/" + info.mPhotos[0]).exists()) {
                    real_num--;
                    for( int j = 0; j < real_num; j++) {
                        db.execSQL("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(j + 1) + " = '" + info.mPhotos[j + 1] + 
                            "' WHERE CustID = '" + info.mCustID + "'");
                        info.mPhotos[j] = info.mPhotos[j+1];
                    }
                }
            }
            if( real_num >= 2) {
                if( info.mPhotos[1] == null || "".equals(info.mPhotos[1]) || 
                    !new File(naviPath + info.mPhotoPath + "/" + info.mPhotos[1]).exists()) {
                    real_num--;
                    for( int j = 0; j < real_num; j++) {
                        db.execSQL("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(j + 1) + " = '" + info.mPhotos[j + 1] + 
                            "' WHERE CustID = '" + info.mCustID + "'");
                        info.mPhotos[j] = info.mPhotos[j+1];
                    }
                }
            }
            if( real_num >= 3) {
                if( info.mPhotos[2] == null || "".equals(info.mPhotos[2]) ||
                    !new File(naviPath + info.mPhotoPath + "/" + info.mPhotos[2]).exists()) {
                    real_num--;
                    for( int j = 0; j < real_num; j++) {
                        db.execSQL("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(j + 1) + " = '" + info.mPhotos[j + 1] + 
                            "' WHERE CustID = '" + info.mCustID + "'");
                        info.mPhotos[j] = info.mPhotos[j+1];
                    }
                } 
            }
            if( real_num >= 4) {
                if( info.mPhotos[3] == null || "".equals(info.mPhotos[3]) ||
                    !new File(naviPath + info.mPhotoPath + "/" + info.mPhotos[3]).exists()) {
                    real_num--;
                    for( int j = 0; j < real_num; j++) {
                        db.execSQL("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(j + 1) + " = '" + info.mPhotos[j + 1] + 
                            "' WHERE CustID = '" + info.mCustID + "'");
                        info.mPhotos[j] = info.mPhotos[j+1];
                    }
                }
            }
            if( real_num == 5) {
                if( info.mPhotos[4] == null || "".equals(info.mPhotos[4]) ||
                    !new File(naviPath + info.mPhotoPath + "/" + info.mPhotos[4]).exists()) {
                    real_num--;
                    db.execSQL("UPDATE DeliveryInfo Set PhotoFile4 = '" + info.mPhotos[5]  + 
                        "' WHERE CustID = '" + info.mCustID + "'");
                    info.mPhotos[5] = null;
                }
            }
            if( info.mPhotoNum != real_num ) {
                db.execSQL("UPDATE DeliveryInfo Set PhotoNum = '" + real_num  + 
                        "' WHERE CustID = '" + info.mCustID + "'");
                // ファイルが存在しないカラムはクリアする
                for( int j = real_num; j < info.mPhotoNum; j++) {
                    db.execSQL("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(j + 1) + " = ''" + 
                        "' WHERE CustID = '" + info.mCustID + "';");
                    info.mPhotos[j] = null;
                }
                info.mPhotoNum = real_num;
            }
*/
            info.mHandWrittenMemoPath = cur.getString(idxMemoPath);
            // Pathが無ければ再設定する
//            if( info.mHandWrittenMemoPath == null || "".equals(info.mHandWrittenMemoPath) ) {
                info.mHandWrittenMemoPath = DeliveryInfo.MEMO_PATH + info.mCustID;
                db.execSQL("UPDATE DeliveryInfo Set HandwrittenMemoPath = '" + info.mHandWrittenMemoPath + 
                            "' WHERE CustID = '" + info.mCustID + "';");
//            }
// Add by M.Suna 2014-12-30 端末内に手書きメモがあれば、その情報をDBに更新する Start
            dir = new File(naviPath + info.mHandWrittenMemoPath);
            if (!dir.exists()) {
            }
            files = dir.listFiles();
            if(files == null) {
            	str_items[0] = null;
            } else {
	            str_items = new String[files.length + 1];
	            for (int j = 0; j < files.length; j++) {
	                File file = files[j];
	                str_items[j] = file.getName();
	            }
            }
// Add by M.Suna 2014-12-30 端末内に手書きメモがあれば、その情報をDBに更新する End

//            info.mHandWrittenMemo = cur.getString(idxMemo);
            info.mHandWrittenMemo = str_items[0];
            if( info.mHandWrittenMemo == null || "".equals(info.mHandWrittenMemo) ) {
                // Mod 2015-02-26 文字列領域は、nullではなく””でクリアに統一
                //info.mHandWrittenMemo = null;
                info.mHandWrittenMemo = "";
            } else if(!new File(naviPath + info.mHandWrittenMemoPath + "/" + info.mHandWrittenMemo).exists()) {
                // dbにデータがあるが、ファイルは存在しない
                db.execSQL("UPDATE DeliveryInfo Set HandwrittenMemoFile = '' WHERE CustID = '" + info.mCustID + "';");
                // Mod 2015-02-26 文字列領域は、nullではなく””でクリアに統一
                //info.mHandWrittenMemo = null;
                info.mHandWrittenMemo = "";
            } else {
                db.execSQL("UPDATE DeliveryInfo Set HandwrittenMemoFile = '" + str_items[0] + 
                        "' WHERE CustID = '" + info.mCustID + "';");
            }

// カメラ連携等 End

            info.mPoint = new GeoPoint(latdeg, londeg, false);
            
            info.mMyIndex = i;
            info.reflectToIcon();
            
            // 代表的な商品名を1つ取得する。
            oneProCr = db.query("ProductInfo", null, "CustCode = ?", new String[]{info.mCustCode}, null, null, null);
            isFirst = true;
            
        	info.mProductRyaku = "";
            while (oneProCr.moveToNext())
            {
            	if (prodCol == -1 || cntCol == -1 || statusCol == -1)
            	{
            		prodCol = oneProCr.getColumnIndex("ProductRyaku");
            		cntCol = oneProCr.getColumnIndex("PeriodDeliCnt");
            		statusCol = oneProCr.getColumnIndex("DeliveryStatus");
            	}
            	
            	onecnt = oneProCr.getInt(cntCol);
            	if (onecnt <= 0) continue;
            	
            	if (!isFirst)
            	{
            		//info.mProductRyaku += "\n";
            		info.mProductRyaku += ",";
            	}
            	else
            	{
            		info.mDeliveryStatus = oneProCr.getInt(statusCol);
            	}
            	isFirst = false;
            	info.mProductRyaku += oneProCr.getString(prodCol);
            	//info.mProductRyaku += "\t";
            	info.mProductRyaku += ":";
            	//info.mProductRyaku += oneProCr.getInt(cntCol);
            	info.mProductRyaku += onecnt;
            	
            }
/*            	
            if (oneProCr.moveToFirst())
            {
            	if (prodCol == -1 || cntCol == -1 || statusCol == -1)
            	{
            		prodCol = oneProCr.getColumnIndex("ProductRyaku");
            		statusCol = oneProCr.getColumnIndex("DeliveryStatus");
            		//cntCol = 
            	}
            	info.mProductRyaku = oneProCr.getString(prodCol);
            	info.mDeliveryStatus = oneProCr.getInt(statusCol);
            }
*/
            oneProCr.close();
            app.addInfoItem(info);
            
            if (info.mDeliveryStatus == DeliveryInfo.DELIVERY_STATUS_NOTDELIVERED)
            {
            	nowLat = (long)((latdeg * 100000.0) * 36.0);
            	nowLon = (long)((londeg * 100000.0) * 36.0);
            	
            	// 直前と同じであれば登録しない。
            	if (nowLat == lastLat && nowLon == lastLon)
            	{
            		continue;
            	}
            	
	            routeNodeDataNew = new RouteNodeData();
	            routeNodeDataNew.setName(cur.getString(idxAddress));
	                        
	            try{
/*	            	
	                routeNodeDataNew.setLat(new Long( (long)(latdeg * 100000) * 36 ).toString()); // -> msec
	                routeNodeDataNew.setLon(new Long( (long)(londeg * 100000) * 36 ).toString()); // -> msec
*/	                
	                routeNodeDataNew.setLat(Long.valueOf(nowLat).toString()); // -> msec
	                routeNodeDataNew.setLon(Long.valueOf(nowLon).toString()); // -> msec
	            } catch( NumberFormatException e) { // 緯度、経度が変換できないデータは飛ばす
	                continue;
	            }
	            routeNodeDataNew.setDate("");
	            routeNodeDataNew.setStatus("0");
	            routeNodeDataNew.setMode("0");
	            app.addRoutePoint(routeNodeDataNew);
	            
	            lastLat = nowLat;
	            lastLon = nowLon;
            }
        }
	}
	
	/**
	 * 現状の配達リストのステータスを見て配達リストを更新する<br>
	 * また、現在地も更新する。
	 * @param upperAct
	 */
	public static void updateRouteData(Activity upperAct)
	{
		NaviApplication app = (NaviApplication)upperAct.getApplication();
		int delivCnt = app.getInfoListSize(); 
		if (delivCnt == 0) return;
		
		// 現在地を取る。
        JNITwoLong nowPos = new JNITwoLong();
        if (NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(nowPos) != 0)
        {
        	// ダメならば取り急ぎトップを取る
        	if (app.getRouteListSize() == 0) return;
        	RouteNodeData oldTop = app.getRouteInfo(0);
        	nowPos.setM_lLat(Long.parseLong(oldTop.getLat()));
        	nowPos.setM_lLong(Long.parseLong(oldTop.getLon()));
        }
	
        // クリアする
        app.initRouteList();
        app.clearCloseUpPoint();
        
        // 現在地を入れる。
        RouteNodeData routeNodeDataNew = new RouteNodeData();
        routeNodeDataNew.setName(upperAct.getResources().getString(R.string.route_start_point_name));
        routeNodeDataNew.setLon("" + nowPos.getM_lLong());
        routeNodeDataNew.setLat("" + nowPos.getM_lLat());
        routeNodeDataNew.setDate("");
        routeNodeDataNew.setStatus("0");
        routeNodeDataNew.setMode("0");
        app.addRoutePoint(routeNodeDataNew);
        
        // 配達リストを取る
        DeliveryInfo oneDI;
        boolean isFirst = true;
        for (int i = 0; i <= delivCnt - 1; i++)
        {
        	oneDI = app.getInfo(i);
        	// 未配達のみ入れる。
        	if (oneDI.mDeliveryStatus == DeliveryInfo.DELIVERY_STATUS_NOTDELIVERED)
        	{
	            routeNodeDataNew = new RouteNodeData();
	            routeNodeDataNew.setName(oneDI.mAddress);
	            try{
	                routeNodeDataNew.setLat(Long.valueOf( oneDI.mPoint.getLatitudeMs() ).toString()); // -> msec
	                routeNodeDataNew.setLon(Long.valueOf( oneDI.mPoint.getLongitudeMs() ).toString()); // -> msec
	            } catch( NumberFormatException e) { // 緯度、経度が変換できないデータは飛ばす
	                continue;
	            }
	            
	            routeNodeDataNew.setDate("");
	            routeNodeDataNew.setStatus("0");
	            routeNodeDataNew.setMode("0");
	            app.addRoutePoint(routeNodeDataNew);
	            
	            if (isFirst)
	            {
	            	app.setCloseUpPoint(oneDI.mPoint.getLatitudeMs(), oneDI.mPoint.getLongitudeMs());
	            	isFirst = false;
	            }
        	}
        }
	}
	
	/**
	 * 商品情報の設定
	 * @param upperAct
	 * @param custCode
	 */
	public static void setProductInfoFromDB(Activity upperAct, String custCode)
	{
		DbHelper mHelper = new DbHelper(upperAct);
        SQLiteDatabase mDb = mHelper.getReadableDatabase();
        if( mDb == null ) {
            //showError(DB_NOT_EXIST);
            return;
        }
        
        Cursor cur = mDb.query("ProductInfo", null, "CustCode = ?", new String[]{custCode}, null, null, null);
        if( cur == null) {
            return;
        }else if( cur.getCount() == 0 ) {
            cur.close();
            return;
        }
        
        int idxProductName = cur.getColumnIndex("ProductName");
        int idxProductRyaku = cur.getColumnIndex("ProductRyaku");
        int idxDeliveryMethod = cur.getColumnIndex("DeliveryMethod");
        int idxMonDeliCnt = cur.getColumnIndex("MonDeliCnt");
        int idxTueDeliCnt = cur.getColumnIndex("TueDeliCnt");
        int idxWedDeliCnt = cur.getColumnIndex("WedDeliCnt");
        int idxThuDeliCnt = cur.getColumnIndex("ThuDeliCnt");
        int idxFriDeliCnt = cur.getColumnIndex("FriDeliCnt");
        int idxSatDeliCnt = cur.getColumnIndex("SatDeliCnt");
        int idxSunDeliCnt = cur.getColumnIndex("SunDeliCnt");
        int idxSecondCnt = cur.getColumnIndex("SecondCnt");
        int idxDeliveryStart = cur.getColumnIndex("DeliveryStart");
        int idxDeliveryEnd = cur.getColumnIndex("DeliveryEnd");
        int idxDeliverySecond = cur.getColumnIndex("DeliverySecond");
        int idxPrice = cur.getColumnIndex("Price");
        int idxPeriodDeliCnt = cur.getColumnIndex("PeriodDeliCnt");
        int idxDeliveryStatus = cur.getColumnIndex("DeliveryStatus");
        
        // 商品情報の初期化
        boolean moveToResult = cur.moveToFirst();
        NaviApplication app = (NaviApplication)upperAct.getApplication();
        app.initProductList();
        
        ProductInfo info;
        String datestr;
        final String invalidDate = "0";
        
        while (moveToResult)
        {
        	info = new ProductInfo();
        	info.mCustCode = custCode;
        	info.mProductName = cur.getString(idxProductName);
        	info.mProductRyaku = cur.getString(idxProductRyaku);
        	info.mDeliveryMethod = cur.getString(idxDeliveryMethod);
        	info.mMonDeliCnt = cur.getInt(idxMonDeliCnt);
        	info.mTueDeliCnt = cur.getInt(idxTueDeliCnt);
        	info.mWedDeliCnt = cur.getInt(idxWedDeliCnt);
        	info.mThuDeliCnt = cur.getInt(idxThuDeliCnt);
        	info.mFriDeliCnt = cur.getInt(idxFriDeliCnt);
        	info.mSatDeliCnt = cur.getInt(idxSatDeliCnt);
        	info.mSunDeliCnt = cur.getInt(idxSunDeliCnt);
        	info.mSecondCnt = cur.getInt(idxSecondCnt);
        	datestr = cur.getString(idxDeliveryStart);
        	info.mDeliveryStart = !invalidDate.equals(datestr) ? datestr : "";
        	datestr = cur.getString(idxDeliveryEnd);
        	info.mDeliveryEnd = !invalidDate.equals(datestr) ? datestr : "";
        	datestr = cur.getString(idxDeliverySecond);
        	info.mDeliverySecond = !invalidDate.equals(datestr) ? datestr : "";
        	info.mPrice = cur.getDouble(idxPrice);
        	info.mPeriodDeliCnt = cur.getInt(idxPeriodDeliCnt);
        	info.mDeliveryStatus = cur.getInt(idxDeliveryStatus);
        	
        	app.addProductItem(info);
        	
        	moveToResult = cur.moveToNext();
        }
        
        cur.close();
        mDb.close();
        mHelper.close();
	}

// Add 20151209 Start
	/**
	 * DBから取得して軌跡データをメモリに格納する。
	 * @param 
	 * @param 
	 * @param 
	 * @param 
	 */
	public static void setTrackDataInfoFromDB(Activity upperAct, Cursor cur, SQLiteDatabase db, int carNo)
	{
        int idxID = cur.getColumnIndex("_ID");
        int idxCourseName = cur.getColumnIndex("CourseName");
        int idxSaveDir = cur.getColumnIndex("SaveDir");
        int idxFileName = cur.getColumnIndex("FileName");
		
        cur.moveToFirst();
        NaviApplication app = (NaviApplication)upperAct.getApplication();
        app.initRouteList(); // 基本あり得ないが、DBが更新されるケースを考慮して、画面に入る度に作り直す
        app.initInfoList();
        app.clearCloseUpPoint();
        
        for(int i=0, len = cur.getCount(); i < len; i++, cur.moveToNext()) {
 
/*
        	// 軌跡データ情報の設定
            TrackDataInfo tr_info = new TrackDataInfo();
            tr_info.mCarNo = carNo;
            tr_info.mDeliveryOrder = cur.getInt(idxOrder);
            tr_info.mCustCode = cur.getString(idxCode);
            tr_info.mCustID = cur.getInt(idxCustID);

            // ファイル数をDBに登録
            if( real_num != 0 ) {
	            db.execSQL("UPDATE DeliveryInfo Set PhotoNum = '" + real_num  + 
	                    "' WHERE CustID = '" + info.mCustID + "'");
            }
            for( int j = 0; j < real_num; j++) {
                db.execSQL("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(j + 1) + " = '" + info.mPhotos[j] + 
                    "' WHERE CustID = '" + info.mCustID + "';");
                //info.mPhotos[j] = info.mPhotos[j+1];
            }
            info.mPhotoNum = real_num;

            info.mPoint = new GeoPoint(latdeg, londeg, false);
            
            info.mMyIndex = i;
            info.reflectToIcon();
*/            
        }
	}
// Add 20151209 End


	/**
	 * 角度差から2点間の距離を求める。
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @return m単位のキョリ
	 */
	public static double getDistanceFromAngleDiff(long lat1, long lon1, long lat2, long lon2)
	{
		final double meterperms = 0.03086419753;
		long latdiff = lat1 - lat2;
		long londiff = lon1 - lon2;
		long latcenter = (lat1 + lat2) / 2;
		
		double latmet = (double)latdiff * meterperms;
		double relayval = (double)londiff * meterperms;
		double latrad = (double)latcenter * Math.PI / 648000000.0; // 円周率をミリ秒単位の180°で割る
		double lonmet = relayval * Math.cos(latrad); // 緯度の余弦で掛けると経度方向のキョリとなる（証明せよ）
		
		return Math.sqrt(Math.pow(latmet, 2) + Math.pow(lonmet, 2));
	}
	
	/**
	 * 現在地から指定点までのキョリを求める。
	 * @param lat
	 * @param lon
	 * @return
	 */
	public static double getDistanceFromMyPos(long lat, long lon)
	{
		JNITwoLong nowPos = new JNITwoLong();
		if (NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(nowPos) != 0) return 0;
		return getDistanceFromAngleDiff(nowPos.getM_lLat(), nowPos.getM_lLong(), lat, lon);
	}
	
	/**
	 * 現在地から目的地までの距離を求める
	 */
	public static double getDistanceFromMyPosToDestination()
	{
		JNITwoLong positon = new JNITwoLong();
        JNIString JNIpositonName = new JNIString();
        JNIInt bPassed = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(
        		Constants.NAVI_DEST_INDEX, positon, JNIpositonName, bPassed);
        return getDistanceFromMyPos(positon.getM_lLat(), positon.getM_lLong());
    }
}
