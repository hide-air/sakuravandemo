package net.zmap.android.pnd.v2.common.services;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

public class SignalService
{
	private TelephonyManager m_oTelePhonyManager = null;
	private SignalListener m_oListener = null;

	public SignalService(Context oContext)
	{
	    m_oTelePhonyManager = (TelephonyManager)oContext.getSystemService(Context.TELEPHONY_SERVICE);
	}
	public void setListen(SignalListener oListener) {

		SignalReceiver oReceiver = new SignalReceiver();
        //m_oTelePhonyManager.listen(oReceiver,PhoneStateListener.LISTEN_SERVICE_STATE);
        m_oTelePhonyManager.listen(oReceiver,
        		PhoneStateListener.LISTEN_SERVICE_STATE | PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

        m_oListener = oListener;
	}
	public void release()
	{
		if(m_oTelePhonyManager != null)
		{
			m_oTelePhonyManager.listen(null,
					PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
		}
	}
	public TelephonyManager getTelephonyManager()
	{
		if(m_oTelePhonyManager!=null) {
			return m_oTelePhonyManager;
		} else {
			return null;
		}
	}
	public class SignalReceiver extends PhoneStateListener
	{
		@Override
		public void onSignalStrengthsChanged(SignalStrength oSignalStrength)
		{
			if(m_oListener != null)
			{
				m_oListener.onSignalChange(oSignalStrength);
			}
			super.onSignalStrengthsChanged(oSignalStrength);
		}

		public void onServiceStateChanged(ServiceState serviceState)
		{
			if(m_oListener != null)
			{
				m_oListener.onServiceChanged(serviceState);
			}
			super.onServiceStateChanged(serviceState);
		}

	}

}
