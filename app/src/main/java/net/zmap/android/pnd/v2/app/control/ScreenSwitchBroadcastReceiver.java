package net.zmap.android.pnd.v2.app.control;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import net.zmap.android.pnd.v2.common.utils.NaviLog;

/**
 * スクリーンOn/Off/ロック解除のブロードキャストレシーバ
 */
public class ScreenSwitchBroadcastReceiver extends BroadcastReceiver {
    private NaviAppController mNaviController = null;
    private boolean bAlreadyReg = false;

    public ScreenSwitchBroadcastReceiver(NaviAppController naviController) {
        mNaviController = naviController;
    }

    public void reg(Context context) {
        if (context == null) {
            return;
        }
        if (bAlreadyReg) {
            return;
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        context.registerReceiver(this, filter);
        bAlreadyReg = true;
    }

    public void unreg(Context context) {
        if (context == null) {
            return;
        }
        if (!bAlreadyReg) {
            return;
        }
        context.unregisterReceiver(this);
        bAlreadyReg = false;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            String action = intent.getAction();
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "BroadcastReceiver::onReceive action=" + action.toString());

            if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                mNaviController.finishNaviApp();
            } else if (action.equals(Intent.ACTION_SCREEN_ON)) {
//Add 2011/10/12 Z01yoneya Start -->
                //ナビアプリは"ACTION_SCREEN_ON"か"ACTION_USER_PRESENT"のどちらかで起動する
                //ナビアプリはNaviAppStatusクラスで状態管理しているので、
                //両方で開始しても２重起動しないようになっている。
                mNaviController.startNaviApp(null);
//Add 2011/10/12 Z01yoneya End <--
            } else if (action.equals(Intent.ACTION_USER_PRESENT)) {
                mNaviController.startNaviApp(null);
            }
        } catch (NullPointerException e) {
            // 特に何もしない
        }
    }
}