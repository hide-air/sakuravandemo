package net.zmap.android.pnd.v2.common.view;

import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

import net.zmap.android.pnd.v2.common.utils.NaviLog;

public class UpdateThread {
    private static List<OnUpdateListener> listeners = new ArrayList<OnUpdateListener>();
    private static Thread thread;

    private UpdateThread(){
    }

    public static void addUpdateListener(OnUpdateListener oListener){
        if(oListener != null){
            listeners.add(oListener);
        }

        if(listeners.size() > 0){
            start();
        }
    }

    private static void start() {
        if (thread == null) {
            thread = new Thread(new Runnable() {
                final Handler handler = new Handler();

                @Override
                public void run() {
                    final int delay = 20 * 1000;
                    while (true) {

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                for (OnUpdateListener listener : listeners) {
                                    if (listener != null) {
                                        listener.onUpdate();
                                    }
                                }
                            }
                        });

                        try {
                            Thread.sleep(delay);
                        } catch (InterruptedException e) {
            				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                        }
                    }
                }
            });
            thread.start();
        }
    }


}
