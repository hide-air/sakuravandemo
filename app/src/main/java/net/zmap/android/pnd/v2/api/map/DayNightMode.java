package net.zmap.android.pnd.v2.api.map;

/**
 * DayNightMode 定数定義クラス
 */
public final class DayNightMode {
    /**
     * 昼モード
     */
    public static final int DAY   = 0;
    /**
     * 夜モード
     */
    public static final int NIGHT = 1;
    /**
     * 自動モード
     */
    public static final int AUTO  = 2;
}