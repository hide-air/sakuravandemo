/**
 *  @file		MeshManager
 *  @brief		メッシュ情報取得クラス
 *
 *  @attention
 *  @note
 *
 *  @author		Manabu Watanabe [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2011-09-01)
 *  @version	$Revision: $ by $Author: $
 *
 */

package net.zmap.android.pnd.v2.dricon.controller;


import android.graphics.Rect;

import net.zmap.android.pnd.v2.data.DC_Coordinate;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.TreeSet;

public class MeshManager {
	//緯度 1秒 約 31M 経度 1秒 約 31mとして計算(1秒辺りの最大値)
	//(3000 / 31) * 1000 = 96774;
	private static final int ORBIS_UNIT_DEGREE = 96774;
	private static final int ONE_MESH_TYPE = 0;
	private static final int TWO_MESH_TYPE = 1;
	public final int REQ_TIMEOUT = 3;
	private List<Integer>		m_oMeshThreeList 	= null;			//３次メッシュリスト
	private DC_Coordinate	 	m_oCarPosInfo 		= null;			//VPからの自己位置
	private Rect				m_oDisplayArea		= null;
	private List<Integer> 		m_oMeshOrbisSet 	= new ArrayList<Integer>();			//オービスリスト

	/**
	 * @return 保持する表示エリア(なければnull)
	 */
	public Rect getDisplayArea(){
		if(!chkRect(m_oDisplayArea)){
			return null;
		}
		return m_oDisplayArea;
	}


	/**
	 *
	 * 地図画面が変わる時に、メッシュリストを更新
	 * @param a_oDisplayArea		表示エリア（経緯度系）
	 */
	public void meshUpdate(Rect a_oDisplayArea){
		if(chkRect(a_oDisplayArea)){
			m_oMeshThreeList = calcMeshThreeList(a_oDisplayArea);
		}
	}

	/**
	 * 3次メッシュ計算：緯度差は0.5分、経度差は0.75分
	 * @param position　　地点
	 * @return　メッシュ情報
	 */
	private MeshDataInfo calcMeshDataFromPos(DC_Coordinate position){
		MeshDataInfo meshData = new MeshDataInfo();
		if(position == null || position.m_lnLatitude1000 < 0 || position.m_lnLongitude1000 < 0){
			return null;
		}
		meshData.MeshThreeLatNum = (int) ((double)position.m_lnLatitude1000/(60*1000)/0.5);
		meshData.MeshThreeLonNum = (int) ((double)position.m_lnLongitude1000/(60*1000)/0.75);
		return meshData;
	}

	/**
	 * メッシュ番号計算
	 * @param position	地点
	 * @param type		1次: 0、2次: 1、3次メッシュ: 2
	 * @return			メッシュ番号
	 */
	public int getMeshFromPos(DC_Coordinate position, int type){
		if(position == null){
			return 0;
		}
		MeshDataInfo meshData = calcMeshDataFromPos(position);
		if(meshData == null || meshData.MeshThreeLatNum < 0 || meshData.MeshThreeLonNum < 0){
			return 0;
		}
		int meshNum = getMeshNumFromMeshData(meshData);
		if(type == ONE_MESH_TYPE){
			meshNum = meshNum/(100*100);
		}
		if(type == TWO_MESH_TYPE){
			meshNum = meshNum/(100);
		}

		return meshNum;
	}

	/**
	 * メッシュ番号計算
	 * 1次メッシュLat　=　８の2次メッシュ　=　（8*10)の3次メッシュ
	 * 1次メッシュLon　=　８の2次メッシュ　=　（8*10)の3次メッシュ。原点は（100,0)なので、100を引く
	 *
	 * @param meshData	メッシュ情報
	 * @return			3次メッシュ番号（基準形式）
	 */
	private int getMeshNumFromMeshData(MeshDataInfo meshDataInfo){
		int lat1Num = 0;
		int lon1Num = 0;
		int lat2Num = 0;
		int lon2Num = 0;
		int lat3Num = 0;
		int lon3Num = 0;
		int meshNum = 0;
		if(meshDataInfo == null || meshDataInfo.MeshThreeLatNum < 0 || meshDataInfo.MeshThreeLonNum < 0){
			return meshNum;
		}
		//1次メッシュ計算
		lat1Num = meshDataInfo.MeshThreeLatNum/(10*8);
		lon1Num = meshDataInfo.MeshThreeLonNum/(10*8) -100;
		meshNum = lat1Num*100+lon1Num;

		//2次メッシュ計算
		lat2Num = (meshDataInfo.MeshThreeLatNum - lat1Num*10*8)/10;
		lon2Num = (meshDataInfo.MeshThreeLonNum - (lon1Num+100)*10*8)/10;
		meshNum *=100;
		meshNum += lat2Num*10 + lon2Num;

		//3次メッシュ計算
		lat3Num = (meshDataInfo.MeshThreeLatNum - lat1Num*10*8) - lat2Num*10;
		lon3Num = (meshDataInfo.MeshThreeLonNum - (lon1Num+100)*10*8) - lon2Num*10;
		meshNum *=100;
		meshNum += lat3Num*10 + lon3Num;
		return meshNum;
	}


	/**
	 * 3次メッシュ番号リスト計算
	 * @param a_oDisplayArea
	 * @return
	 */
	private List<Integer> calcMeshThreeList(Rect a_oDisplayArea){
		MeshDataInfo meshDataInfo1;
		MeshDataInfo meshDataInfo2;
		List<Integer> meshList = null;
		int diffLatNum;
		int diffLonNum;

		if(!chkRect(a_oDisplayArea)){
			return null;
		}
		DC_Coordinate aLeftBottom = new DC_Coordinate();
		DC_Coordinate aRightTop = new DC_Coordinate();
		Set<MeshDataInfo> meshThreeList = new HashSet<MeshDataInfo>();

		aLeftBottom.m_lnLatitude1000 = a_oDisplayArea.bottom;
		aLeftBottom.m_lnLongitude1000 = a_oDisplayArea.left;

		aRightTop.m_lnLatitude1000 = a_oDisplayArea.top;
		aRightTop.m_lnLongitude1000 = a_oDisplayArea.right;

		meshDataInfo1 = calcMeshDataFromPos(aLeftBottom);
		meshDataInfo2 = calcMeshDataFromPos(aRightTop);
		if(meshDataInfo1 == null || meshDataInfo2 == null){
			return null;
		}

		MeshDataInfo meshCenterInfo = getCenterMeshDataInfo();
		if(meshCenterInfo == null){
			return null;
		}

		diffLatNum = meshDataInfo2.MeshThreeLatNum - meshDataInfo1.MeshThreeLatNum;
		diffLonNum = meshDataInfo2.MeshThreeLonNum - meshDataInfo1.MeshThreeLonNum;
		for(int i = 0; i <= diffLatNum; i++){
			for(int j = 0; j <= diffLonNum; j++){
				MeshDataInfo data = new MeshDataInfo();
				data.MeshThreeLatNum = meshDataInfo1.MeshThreeLatNum + i;
				data.MeshThreeLonNum = meshDataInfo1.MeshThreeLonNum + j;
				meshThreeList.add(data);
			}
		}

		Map<Integer, MeshDataInfo> oMapMesh = new TreeMap<Integer,MeshDataInfo>();
		int nRangeLevel;
		Iterator<MeshDataInfo> it = meshThreeList.iterator();

        while ( it.hasNext() ) {
    		int nOffset = 0;

    		// 地図中心のメッシュ情報からリスト登録されたメッシュ情報を引くことで
			// いくつのメッシュ分離れているかを算出する
        	MeshDataInfo data = it.next();

			diffLatNum = data.MeshThreeLatNum - meshCenterInfo.MeshThreeLatNum;
			diffLonNum = data.MeshThreeLonNum - meshCenterInfo.MeshThreeLonNum;

			diffLatNum = Math.abs(diffLatNum);
			diffLonNum = Math.abs(diffLonNum);

			// 地図中心からの距離レベルの高い数値を取得する
			if( diffLatNum > diffLonNum){
				nRangeLevel = diffLatNum;
			}
			else{
				nRangeLevel = diffLonNum;
			}
			if( nRangeLevel != 0 ){
				// 地図中心を0レベルとして、距離レベルに対したソート比較値(オフセット)を取得
				// 3333333
				// 3222223
				// 3211123
				// 3210123
				// 3211123
				// 3222223
				// 3333333
				// ソート比較値(オフセット)計算(8は上下左右斜めの8方向)
				for(int nMath = nRangeLevel -1; nMath > 0; nMath-- ){
					nOffset += (nMath * 8);
				}
				// 地図中心分の個数を加算
				nOffset++;
			}

			// すでに登録されている（同列順序）の場合、ソート比較値(オフセット)をインクリメントする。
			int nCompareNum = getMeshNumFromMeshData(data);

			for(;;){
				if( !oMapMesh.containsKey(nOffset) ){
					break;
				}
				int nRegisterNum = getMeshNumFromMeshData(oMapMesh.get(nOffset));

				if( nRegisterNum < nCompareNum ){
					MeshDataInfo wkdata = new MeshDataInfo();
					wkdata = oMapMesh.get(nOffset);
					oMapMesh.put( nOffset, data );
					data = wkdata;
				}
				nOffset++;
			}
			// ソート比較値(オフセット)とメッシュ情報の関連付け
			oMapMesh.put( nOffset, data );
		}

		meshList = new ArrayList<Integer>();

		for( Entry<Integer, MeshDataInfo> entry :  oMapMesh.entrySet() ) {
			MeshDataInfo value = (MeshDataInfo) entry.getValue();
			int meshNum = getMeshNumFromMeshData(value);
			meshList.add(meshNum);
		}

		return meshList;
	}

	/**
	 * 2次メッシュ番号リスト計算
	 * @param lat  緯度
	 * @param lon  経度
	 * @param radius 半径
	 */
	public List<String> getMeshOrbisList(){
		Rect orbisRect = new Rect();
		List<Integer> threeMeshList = null;
		List<Integer> twoMeshList = null;
		if(m_oCarPosInfo != null){
			orbisRect.bottom = (int) (m_oCarPosInfo.m_lnLatitude1000 - ORBIS_UNIT_DEGREE);
			orbisRect.left = (int) (m_oCarPosInfo.m_lnLongitude1000 - ORBIS_UNIT_DEGREE);
			orbisRect.top = (int) (m_oCarPosInfo.m_lnLatitude1000 + ORBIS_UNIT_DEGREE);
			orbisRect.right = (int) (m_oCarPosInfo.m_lnLongitude1000 + ORBIS_UNIT_DEGREE);
			threeMeshList = calcMeshThreeList(orbisRect);
			twoMeshList = getMeshTwoList(threeMeshList);
		}
		List<String> stringList = null;
		if(twoMeshList != null){
			if(!twoMeshList.equals(m_oMeshOrbisSet)){
				stringList = chgIntListToStrList(twoMeshList);
				m_oMeshOrbisSet = twoMeshList;
			}
		}
		return stringList;
	}

	/**
	 *
	 * 3次メッシュリストから2次メッシュリストに変換
	 * @param	threeMeshList		3次メッシュリスト
	 * @return	meshList			2次メッシュリスト
	 */
	private List<Integer> getMeshTwoList(List<Integer> threeMeshList){
		Set<Integer> meshList = null;

		if(threeMeshList != null){
			Integer[] meshArray = new Integer[threeMeshList.size()];
			meshArray = threeMeshList.toArray(meshArray);
			meshList = new TreeSet<Integer>();
			for(int i = 0; i < meshArray.length; i++){
				int meshNum = meshArray[i].intValue()/100;
				meshList.add(meshNum);
			}
		}

		List<Integer> list = new ArrayList<Integer>(meshList);

		return list;
	}
	/**
	 * 再描画が必要か判断する
	 * @param nowDispArea	現在領域
	 * @return	1			必要(メッシュも異なる)
	 * 			0			不要
	 * 			-1			必要(メッシュは同じ)
	 */
	public int compMeshThreeList(Rect nowDispArea) {
		if(!chkRect(nowDispArea)){
			return 0;
		}
		if(m_oDisplayArea == null || !chkRect(m_oDisplayArea)){
			m_oDisplayArea = nowDispArea;
			return 1;
		}

		if(nowDispArea.equals(m_oDisplayArea)){
			return 0;
		}

		DC_Coordinate nowLeftBottom = new DC_Coordinate();
		DC_Coordinate nowRightTop = new DC_Coordinate();
		DC_Coordinate preLeftBottom = new DC_Coordinate();
		DC_Coordinate preRightTop = new DC_Coordinate();

		nowLeftBottom.m_lnLatitude1000 = nowDispArea.bottom;
		nowLeftBottom.m_lnLongitude1000 = nowDispArea.left;
		nowRightTop.m_lnLatitude1000 = nowDispArea.top;
		nowRightTop.m_lnLongitude1000 = nowDispArea.right;

		preLeftBottom.m_lnLatitude1000 = m_oDisplayArea.bottom;
		preLeftBottom.m_lnLongitude1000 = m_oDisplayArea.left;
		preRightTop.m_lnLatitude1000 = m_oDisplayArea.top;
		preRightTop.m_lnLongitude1000 = m_oDisplayArea.right;

		MeshDataInfo nowMeshDataInfoLeftBottom = calcMeshDataFromPos(nowLeftBottom);
		MeshDataInfo nowMeshDataInfoRightTop = calcMeshDataFromPos(nowRightTop);
		MeshDataInfo preMeshDataInfoLeftBottom = calcMeshDataFromPos(preLeftBottom);
		MeshDataInfo preMeshDataInfoRightTop = calcMeshDataFromPos(preRightTop);

		m_oDisplayArea = nowDispArea;

		if(nowMeshDataInfoLeftBottom.MeshThreeLatNum >= preMeshDataInfoLeftBottom.MeshThreeLatNum &&
				nowMeshDataInfoLeftBottom.MeshThreeLonNum >= preMeshDataInfoLeftBottom.MeshThreeLonNum &&
				nowMeshDataInfoRightTop.MeshThreeLatNum <= preMeshDataInfoRightTop.MeshThreeLatNum &&
				nowMeshDataInfoRightTop.MeshThreeLonNum <= preMeshDataInfoRightTop.MeshThreeLonNum)
		{
			return -1;
		}
		return 1;
	}

	/**
	 *
	 * Int型のリストをString型に変換
	 * @param 	intList		int型リスト
	 * @return	strList		String型リスト
	 */
	private List<String> chgIntListToStrList(List<Integer> intList){
		if(intList == null){
			return null;
		}
		List<String> strList = new ArrayList<String>();
		Integer[] listArray = new Integer[intList.size()];
		listArray = (Integer[]) intList.toArray(listArray);
		for (int i = 0; i < listArray.length; i++){
            if (listArray[i] instanceof Integer) {
            	strList.add(String.valueOf(listArray[i]));
            }
        }
		return strList;
	}

	/**
	 *3次メッシュリスト取得
	 * @return
	 */
	public List<Integer> getMeshIntThreeList(){
		if(m_oMeshThreeList == null){
			return null;
		}
		List<Integer> list = new ArrayList<Integer>(m_oMeshThreeList);
		return list;
	}

	/**
	 * @param mCarPosInfo セットする mCarPosInfo
	 */
	public void setCarPosInfo(DC_Coordinate mCarPosInfo) {
		this.m_oCarPosInfo = mCarPosInfo;
	}
	/**
	 *
	 * @param a_oRect
	 * @return
	 */
	private boolean chkRect(Rect a_oRect){
		boolean chkFlag = false;
		if(a_oRect == null){
			return chkFlag;
		}
		if(a_oRect.top < 0 || a_oRect.left < 0 || a_oRect.right < 0 || a_oRect.bottom < 0){
			return chkFlag;
		}
		if(a_oRect.left <= a_oRect.right && a_oRect.bottom <= a_oRect.top){
			chkFlag = true;
		}
		return chkFlag;
	}
	/**
	 * メッシュデータ情報クラス
	 *
	 */
	private static class MeshDataInfo{
		public int MeshThreeLatNum;			//3次メッシュ単位の緯度
		public int MeshThreeLonNum;			//3次メッシュ単位の経度
		public MeshDataInfo(){
			MeshThreeLatNum = 0;
			MeshThreeLonNum = 0;
		}
	}

	/**
	 * 地図中心のメッシュデータ取得
	 * @param なし
	 * @return MeshDataInfo : 地図中心のメッシュデータ情報
	 */
	private MeshDataInfo getCenterMeshDataInfo(){
		MeshDataInfo meshCenterInfo = new MeshDataInfo();

        JNITwoLong mapCenter = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(mapCenter);
        DC_Coordinate oCenterCoordinate = new DC_Coordinate();

        oCenterCoordinate.m_lnLatitude1000 = mapCenter.getM_lLat();
        oCenterCoordinate.m_lnLongitude1000 = mapCenter.getM_lLong();

        meshCenterInfo = calcMeshDataFromPos(oCenterCoordinate);

        return meshCenterInfo;
	}
}