package net.zmap.android.pnd.v2.api.event;


/**
 * ルート案内リスナインターフェース<br>
 * ルート案内状態の変更(開始・終了)やルート案内(音声)が発生したときに呼び出されるリスナインターフェース(車モードのみ)<br>
 */
public interface RouteGuidanceListener {
    /**
     * 案内を開始したときに呼び出されるリスナ関数.<br>
     *
     * <ul>
     * <li>案内開始ボタンを押したとき
     * <li>デモ開始ボタンを押したとき
     * <li>起動時に案内を再開したとき
     * <li>APIで案内開始したときに発生するイベント
     * </ul>
     *
     * @param routeGuidance
     *            ルート案内情報
     */
    public void onStarted(RouteGuidance routeGuidance);

    /**
     * 案内を終了したときに呼び出されるリスナ関数.<br>
     *
     * <ul>
     * <li>目的地に到着したとき
     * <li>案内終了ボタンを押したとき
     * <li>案内終了メニューを選んだとき(ルート編集画面、メニュー画面)
     * <li>メニュー画面の「目的地共有をON->OFF設定」
     * <li>デモ終了ボタンを押したとき
     * <li>APIで案内終了したときに発生するイベント
     * </ul>
     *
     * @param routeGuidance
     *            ルート案内情報
     */
    public void onStopped(RouteGuidance routeGuidance);

    /**
     * 案内を一時停止したときに呼び出されるリスナ関数.<br /><br />
     *
     * <code>NaviApi.pauseGuide()</code> が呼ばれたときに通知されます.
     *
     * @param routeGuidance
     *            ルート案内情報
     */
    public void onPaused(RouteGuidance routeGuidance);

    /**
     * 一時停止した再開したときに呼び出されるリスナ関数.<br /><br />
     *
     * <code>NaviApi.resumeGuide()</code> が呼ばれたときに通知されます.
     *
     * @param routeGuidance
     *            ルート案内情報
     */
    public void onResumed(RouteGuidance routeGuidance);

    /**
     * ルート案内中、目標案内(交差点)に接近して、音声案内したときに呼び出されるリスナ関数.<br>
     *
     * @param routeGuidance
     *            ルート案内情報
     */
    public void onNearToCrossroad(RouteGuidance routeGuidance);

    /**
     * ルート案内中、経由地に接近して、音声案内したときに呼び出されるリスナ関数.<br>
     *
     * @param routeGuidance
     *            ルート案内情報
     */
    public void onNearToWaypoint(RouteGuidance routeGuidance);

    /**
     * ルート案内中、目的地に接近して、音声案内したときに呼び出されるリスナ関数.<br>
     *
     * @param routeGuidance
     *            ルート案内情報
     */
    public void onNearToDestination(RouteGuidance routeGuidance);

    /**
     * ルート案内中、リルート後に案内を開始したときに呼び出されるリスナ関数.<br>
     *
     * @param routeGuidance
     *            ルート案内情報
     */
    public void onRerouted(RouteGuidance routeGuidance);

}
