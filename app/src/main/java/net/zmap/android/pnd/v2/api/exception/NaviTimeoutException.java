package net.zmap.android.pnd.v2.api.exception;

/**
 * 処理時間切れ (タイムアウト)
 */
public class NaviTimeoutException extends NaviException {
    private static final long serialVersionUID = 1L;

    /**
     * エラー詳細メッセージとして null を設定して NaviTimeoutException を構築します。
     */
    public NaviTimeoutException() {
        super();
    }

    /**
     * 詳細メッセージを指定して NaviTimeoutException を構築します。
     *
     * @param message
     *            詳細メッセージ
     */
    public NaviTimeoutException(String message) {
        super(message);
    }
}