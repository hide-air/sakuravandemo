/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNE_RouteInfo_t.java
 * Description    ルート情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNE_RouteInfo_t
{
    private int                         iViaValidCount ;
    private JNITwoLong                  stPos ;
    private ZNE_RouteInfoGuidePoint_t[] stGuidePoint = new ZNE_RouteInfoGuidePoint_t[7] ;
    private int                         iRouteSearchType ;
    /**
    * Created on 2010/08/06
    * Title:       getIRouteSearchType
    * Description:   探索条件を取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getIRouteSearchType()
    {
        return iRouteSearchType;
    }
    /**
    * Created on 2010/08/06
    * Title:       getRouteSearchType
    * Description:   ルート情報を取得する
    * @param1  無し
    * @return       ZNE_RouteInfoGuidePoint_t[]

    * @version        1.0
    */
    public ZNE_RouteInfoGuidePoint_t[] getRouteInfoGuidePoint()
    {
        return stGuidePoint;
    }
    /**
    * Created on 2010/08/06
    * Title:       getIViaValidCount
    * Description:   有効な案内点を取得する
    * @param1  無し
    * @return       JNITwoLong

    * @version        1.0
    */
    public int getIViaValidCount() {
        return iViaValidCount;
    }

    /**
    * Created on 2010/08/06
    * Title:       getStPos
    * Description:   出発地座標を取得する
    * @param1  無し
    * @return       JNITwoLong

    * @version        1.0
    */
    public JNITwoLong getStPos() {
        return stPos;
    }
}
