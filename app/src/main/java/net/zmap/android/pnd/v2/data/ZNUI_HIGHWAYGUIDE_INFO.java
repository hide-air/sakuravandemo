package net.zmap.android.pnd.v2.data;

public class ZNUI_HIGHWAYGUIDE_INFO {
	private int eKindCode = 0;
	private JNIShort punSAPAServiceCode = new JNIShort();
	private short unSAPAServiceCodeNum = 0;
	private long ulnDistMeter = 0;
	private long ulnTime = 0;
	public int getEKindCode() {
		return eKindCode;
	}
	public JNIShort getPunSAPAServiceCode() {
		return punSAPAServiceCode;
	}
	public short getUnSAPAServiceCodeNum() {
		return unSAPAServiceCodeNum;
	}
	public long getUlnDistMeter() {
		return ulnDistMeter;
	}
	public long getUlnTime() {
		return ulnTime;
	}


}
