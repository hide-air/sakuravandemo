package net.zmap.android.pnd.v2.common.utils;

import net.zmap.android.pnd.v2.common.DrivingRegulation;
import android.view.MotionEvent;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class ScrollBox extends ScrollView implements ScrollListener
{
	private LinearLayout m_oLayout = null;
	private ScrollControlListener m_oListener = null;
	private boolean m_bIsFirst = true;

	public ScrollBox(Context oContext)
	{
		super(oContext);
		init();
	}

	public ScrollBox(Context oContext, AttributeSet oSet)
	{
		super(oContext, oSet);
	}

	private void init()
	{
		m_oLayout = new LinearLayout(getContext());
		m_oLayout.setOrientation(LinearLayout.VERTICAL);

		LinearLayout.LayoutParams oParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT);

		addView(m_oLayout, oParams);
	}

	public void addChildView(View oView)
	{
		if(oView != null)
		{
			ViewGroup oLayoutGroup = null;
			if(getChildCount() == 0)
			{
				if(m_oLayout == null)
				{
					init();
				}
				oLayoutGroup = m_oLayout;
			}
			else
			{
				oLayoutGroup = (ViewGroup)getChildAt(0);
			}

			if(oLayoutGroup != null)
			{
				LinearLayout.LayoutParams oParams = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.FILL_PARENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);

				oLayoutGroup.addView(oView, oParams);
			}
		}
	}



	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w, h, oldw, oldh);
		update();
	}

	@Override
	protected void onDraw(Canvas oCanvas)
	{
		if(m_bIsFirst)
		{
			update();
			m_bIsFirst = false;
		}
		super.onDraw(oCanvas);
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt)
	{
		update();
		super.onScrollChanged(l, t, oldl, oldt);
	}

	private void update()
	{
		if(m_oListener != null)
		{
			m_oListener.update(getScrollY(), getHeight(), computeVerticalScrollRange());
		}
	}

	@Override
	public void moveNextPage()
	{
		pageScroll(FOCUS_DOWN);
	}

	@Override
	public void movePreviousPage()
	{
		pageScroll(FOCUS_UP);
	}

	@Override
	public void setControlListener(ScrollControlListener oListener)
	{
		m_oListener = oListener;
	}

	@Override
	public boolean isHasNextPage()
	{
		return (getScrollY() + getHeight() < computeVerticalScrollRange());
	}

	@Override
	public boolean isHasPreviousPage()
	{
		return (getScrollY() != 0);
	}
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 スクロール禁止 Start -->
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
    	if(DrivingRegulation.GetDrivingRegulationFlg())
    	{
			return false;
    	}
		else
		{
			return super.onTouchEvent(event);
		}
	}
// ADD 2013.08.08 M.Honma 走行規制 スクロール禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
}
