package net.zmap.android.pnd.v2.sensor;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.Surface;
import android.view.WindowManager;

import net.zmap.android.pnd.v2.data.NaviRun;

public class ElectricCompass implements SensorEventListener {
	private static ElectricCompass mSingleton;
	private static Activity mActivity;
    private static SensorManager mSensorManager;
    private static WindowManager mWindowManager;
    private static Sensor mAccelerometer;
    private static Sensor mMagneticField;

//    private final static int STATUS_UNKNOWN = -1;
    private final static int STATUS_FAILURE = 0;
    private final static int STATUS_SUCCESS = 1;

    private float[] mAccelerometerValues = new float[3];
    private float[] mGeomagneticValues = new float[3];
    private float[] mOrientationValuesAG = new float[3];

    public boolean enabled = false;
    public float[] values = new float[3];
    public long timestamp = 0;

    public ElectricCompass() {
    	mSingleton = this;
    }

    public static void init(Activity activity) {
    	mSingleton = new ElectricCompass();
    	mActivity = activity;
    }

	@Override
	synchronized public void onSensorChanged(SensorEvent event) {
		if (event.sensor == mAccelerometer) {
			mAccelerometerValues = event.values.clone();
		} else if (event.sensor == mMagneticField) {
			mGeomagneticValues = event.values.clone();
		}

		if (mAccelerometerValues != null && mGeomagneticValues != null) {
            float[] inR = new float[16];

            if (SensorManager.getRotationMatrix(inR, null, mAccelerometerValues, mGeomagneticValues)) {
                float[] outR = new float[16];

	            switch (mWindowManager.getDefaultDisplay().getRotation()) {
	            case Surface.ROTATION_0:
	            	// landscape [ :]
					SensorManager.remapCoordinateSystem(inR, SensorManager.AXIS_X, SensorManager.AXIS_Z, outR);
	                SensorManager.getOrientation(outR, mOrientationValuesAG);
	            	break;
	            case Surface.ROTATION_90:
	            	// portrait [_]
	                SensorManager.getOrientation(inR, mOrientationValuesAG);
	            	break;
	            case Surface.ROTATION_180:
	            	// landscape [: ]
					SensorManager.remapCoordinateSystem(inR, SensorManager.AXIS_MINUS_X, SensorManager.AXIS_Z, outR);
	                SensorManager.getOrientation(outR, mOrientationValuesAG);
	            	break;
	            case Surface.ROTATION_270:
	            	// portrait [~]
	                SensorManager.getOrientation(inR, mOrientationValuesAG);
	            	break;
	            }
            }
		}

		values[0] = (float)Math.toDegrees(mOrientationValuesAG[0]);
        if (values[0] < 0) {
        	values[0] += 360;
        }
        if (0 < values[0]&& values[0] < 360) {
        	values[0] = 360 - values[0];
        }
		values[1] = (float)Math.toDegrees(mOrientationValuesAG[1]);
		values[2] = (float)Math.toDegrees(mOrientationValuesAG[2]);

		// values[0] = [0, 359]
   		setCompassInfo(true, values[0]);

   		/*
 		Log.i("", String.format("(A) %f, %f, %f\r\n" +
								"(M) %f, %f, %f\r\n" +
								"(O) %f, %f, %f\r\n",
								mAccelerometerValues[0], mAccelerometerValues[1], mAccelerometerValues[2],
								mGeomagneticValues[0], mGeomagneticValues[1], mGeomagneticValues[2],
								values[0], values[1], values[2]));
		*/
	}

	public static void setCompassInfo(boolean enabled, float direction) {
		mSingleton.enabled = enabled;
		NaviRun.GetNaviRunObj().JNI_WG_SetCompassInfo((enabled == true) ? 1L : 0L, (long)direction);
	}

	@Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public static int enable() {
		mSensorManager = (SensorManager)mActivity.getSystemService(Activity.SENSOR_SERVICE);
		if (mSensorManager == null) {
			return STATUS_FAILURE;
		}
		mWindowManager = (WindowManager)mActivity.getSystemService(Activity.WINDOW_SERVICE);
		if (mWindowManager == null) {
			return STATUS_FAILURE;
		}
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mMagneticField = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		if (mAccelerometer == null || mMagneticField == null) {
			return STATUS_FAILURE;
		}
   		mSensorManager.registerListener(mSingleton, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
   		mSensorManager.registerListener(mSingleton, mMagneticField, SensorManager.SENSOR_DELAY_NORMAL);
   		setCompassInfo(true, -1);
    	return STATUS_SUCCESS;
    }

    public static int disable() {
    	mSensorManager.unregisterListener(mSingleton);
   		setCompassInfo(false, -1);
    	return STATUS_SUCCESS;
    }
}