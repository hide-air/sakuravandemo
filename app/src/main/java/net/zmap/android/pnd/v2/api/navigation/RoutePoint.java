package net.zmap.android.pnd.v2.api.navigation;

import android.os.Parcel;
import android.os.Parcelable;

import net.zmap.android.pnd.v2.api.GeoPoint;

/**
 * ナビ内 ポイントクラス
 */
public class RoutePoint implements Parcelable {

    protected GeoPoint mPoint = null;
    protected String mName    = null;

    /**
     * コンストラクタ
     */
    public RoutePoint() {

    }

    /**
     * コンストラクタ
     * @param original ナビ内 ポイント
     */
    public RoutePoint(RoutePoint original) {
        if (original != null) {
            setName(original.mName);
            setPoint(new GeoPoint(original.getPoint()));
        }
    }

    protected RoutePoint(Parcel in) {
        mPoint = in.readParcelable(GeoPoint.class.getClassLoader());
        mName = in.readString();
    }

    /**
     * 世界測地系　座標設定
     *
     * @param point
     *            世界測地系　座標
     */
    public void setPoint(GeoPoint point) {
        mPoint = new GeoPoint(point.getLatitude(), point.getLongitude(), point.getWorldPoint());
    }

    /**
     * 世界測地系　座標取得
     *
     * @return　世界測地系　座標
     */
    public GeoPoint getPoint() {
        return mPoint;
    }

    /**
     * ポイント名称設定
     *
     * @param name
     *            ポイント名称
     */
    public void setName(String name) {
        mName = name;
    }

    /**
     * ポイント名称取得
     *
     * @return　ポイント名称
     */
    public String getName() {
        return mName;
    }

    /* (非 Javadoc)
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(mPoint, flags);
        out.writeString(mName);
    }

    /* (非 Javadoc)
     * @see android.os.Parcelable#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /* (非 Javadoc)
     *
     */
    public static final Parcelable.Creator<RoutePoint> CREATOR = new Parcelable.Creator<RoutePoint>() {
        public RoutePoint createFromParcel(Parcel in) {
            return new RoutePoint(in);
        }

        public RoutePoint[] newArray(int size) {
            return new RoutePoint[size];
        }
    };
}
