/**
 ********************************************************************
 * COPYRIGHT
 ********************************************************************
 *
 * Project        PND Navi
 * File           HttpUtils.java
 * Description    Httpアクセスクラス
 * Created on     2011/07/14
 * @author        itoh@cslab.co.jp
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.common.http;

import android.os.Environment;

import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.StorageUtil;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * @class HttpUtils
 *
 */
public class HttpUtils {

    private static final int      DEFAULT_TIMEOUT_CONNECTION = 10000;
    private static final int      DEFAULT_TIMEOUT_READ       = 10000;


    protected HttpClient          mHttpClient                = null;

    // リクエスト情報
    protected String              mUrl                       = null;
    protected List<HttpUtilParam> mParams                    = null;
    protected String              mCharSet                   = HTTP.UTF_8;

    // 取得情報
    protected int                 mStatusCode                = HttpStatus.SC_OK;
    protected byte[]              mContentData               = null;
    Map<String, List<String>>     mHeaders                   = null;

    int                           mConnectTimeout            = DEFAULT_TIMEOUT_CONNECTION;
    int                           mReadTimeout               = DEFAULT_TIMEOUT_READ;
    Map<String,String>            mURLQueryList              = null;
    String 						  mRootPath					 = null;

//	private String   mLogFile = null;
    /**
     * アクセスURL設定
     *
     * @param url
     *            : アクセスURL
     */
    public HttpUtils(String url) {
//@@MOD-START BB-0003 2012/10/29 Y.Hayashida
        File sdCardPath = new File(StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME));
    	String strFilePath = sdCardPath.getPath() + File.separator + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME;
//    	File sdCardPath = Environment.getExternalStorageDirectory();
//    	String strFilePath = sdCardPath.getPath() + "/ItsmoNaviDrive/";
//@@MOD-START BB-0003 2012/10/29 Y.Hayashida
    	File outputFile  = new File(strFilePath);
    	if(!outputFile.exists()){
        	if(outputFile.getParentFile().mkdirs() == false){
        		NaviLog.d(NaviLog.PRINT_LOG_TAG,"File path create error\n");
        	}
    	}
    	mRootPath = outputFile.toString();

        mHttpClient = new DefaultHttpClient();

//        mLogFile = new String(strFilePath + "url.log");

        HttpParams params = mHttpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, mConnectTimeout);
        HttpConnectionParams.setSoTimeout(params, mReadTimeout);

        mUrl = url;
        mParams = new ArrayList<HttpUtilParam>();

        mHeaders = new HashMap<String, List<String>>();
        mURLQueryList = null;
    }
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
    public HttpUtils(String url, String path) {
    	String strFilePath = new String(path);

    	File outputFile  = new File(strFilePath);
    	if(!outputFile.exists()){
        	if(outputFile.getParentFile().mkdirs() == false){
        		NaviLog.d(NaviLog.PRINT_LOG_TAG,"File path create error\n");
        	}
    	}
    	mRootPath = outputFile.toString();

        mHttpClient = new DefaultHttpClient();

//        mLogFile = new String(strFilePath + "url.log");

        HttpParams params = mHttpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, mConnectTimeout);
        HttpConnectionParams.setSoTimeout(params, mReadTimeout);

        mUrl = url;
        mParams = new ArrayList<HttpUtilParam>();

        mHeaders = new HashMap<String, List<String>>();
        mURLQueryList = null;
    }
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
    /**
     * URLからQuery部分をリスト化する
     *
     * @param nothing
     * @return boolean  :true 分割成功
     * 					false 分割失敗(Queryなし)
     */
    public boolean SplitURL(){
    	if(mURLQueryList != null){
    		return true;
    	}
    	try{
    		URI urlObject = new URI(mUrl);
    		String query = urlObject.getRawQuery();
    		if(query == null){
    			return false;
    		}
    		mURLQueryList= new HashMap<String, String>();
    		Scanner scanner = new Scanner(query);
    		scanner.useDelimiter("&");
    		while (scanner.hasNext()) {
    		    final String[] nameValue = scanner.next().split("=");
    		    if (nameValue.length == 0 || nameValue.length > 2){
    		    }else{
    		        String name = nameValue[0];
    		        String value = nameValue[1];
    		        mURLQueryList.put(name, value);
    		    }
    		}
    	}catch(URISyntaxException e){
    		return false;
    	}
    	return true;
    }


    /**
     * URLからQueryのKeyに対応するValueを取得する
     *
     * @param key  :キー文字列
     *
     * @return String  :キーに対応する文字列
     *                  なければnull
     */
    public String getRequestValue(String key){
    	if(key == null){
    		return null;
    	}
    	if(mURLQueryList == null){
    		if(SplitURL() == false){
    			return null;
    		}
    	}

    	return mURLQueryList.get(key);
    }

    /**
     * URLからファイル名称を取得する
     *
     * @param nothing
     *
     * @return String  :ファイル名
     *                  なければnull
     */
    public String getFileName(){
    	if(mUrl == null){
    		return null;
    	}

    	URL url = null;
		try {
			url = new URL(mUrl);
			String f = url.getFile();
			String[] parts = f.split("/");
			String t = parts[parts.length-1];

			int idx = t.indexOf("?");
			if(-1 < idx ){
				return t.substring(0, t.indexOf("?"));
			}
			else{
				return t;
			}
		} catch (MalformedURLException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
		}
    	return null;
    }

    /**
     * URLからファイル名称を取得する
     *
     * @param nothing
     *
     * @return String  :ファイル名
     *                  なければnull
     */
    public String getFileNameIncludeFolder(){
    	if(mUrl == null){
    		return null;
    	}

    	URL url = null;
		try {
			url = new URL(mUrl);
			String f = url.getFile();
			return f;
		} catch (MalformedURLException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
		}
    	return null;
    }

    /**
     * 指定されたパスに、ファイルを保存する
     * パスは、保存可能領域(SDカード内)の定型フォルダ内の指定パスとなる
     * 例；入力が""icon/icon.png"
     * 出力先は "/sdcard/ItsmoNaviDrive/icon/icon.png"
     *
     * @param strPath ;ファイルパス
     *
     * @return String  :保存成功時、保存パスを返す。失敗でnull
     */
    public String saveFile2SD(String strPath) {
        OutputStream out = null;
        try{
        	String strFilePath = mRootPath + strPath;
        	File outputFile  = new File(strFilePath);
        	if(outputFile.exists() == false){
                if (mContentData == null) {
                	if(httpPost() == false){
                		return null;
                	}
                }

	        	if(!outputFile.getParentFile().exists()){
		        	if(outputFile.getParentFile().mkdirs() == false){
		        	}
	        	}
	            out = new FileOutputStream(outputFile.getPath());
	            out.write(mContentData, 0, mContentData.length);
	            out.flush();
	            return outputFile.getPath();
        	}else{
        		return strFilePath;
        	}
// ADD.2013.03.05 N.Sasao Bug #10373 アプリ起動に失敗する START
        } catch (SocketTimeoutException e) {
            return null;
        } catch (SocketException e) {
            return null;
// ADD.2013.03.05 N.Sasao Bug #10373 アプリ起動に失敗する  END
        }    // ファイルが開けない場合（SDカードが無い等）
        catch (FileNotFoundException e) {
            return null;
        }    // バイト配列wが不正な値だった場合
        catch (IndexOutOfBoundsException e) {
            return null;    }    // ファイル書き込みに失敗した場合
        catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            return null;
        }
        finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                    return null;
                }
            }
        }
    }

    public boolean deleteFile(String dirOrFile) {
    	String strFilePath = mRootPath + dirOrFile;
    	File outputFile  = new File(strFilePath);
    	return deleteFile(outputFile);
    }


/*    public boolean IsFileExist(String strPath) {
    	String strFilePath = mRootPath  + strPath;
    	File outputFile  = new File(strFilePath);
    	if(outputFile.exists()){
    		return outputFile.toString();
    	}
    	return null;
    }
*/
    /**
     * 指定されたフォルダ、ファイルを削除する
     * フォルダの場合、サブフォルダ含む全てのファイルを削除する
     *
     * @param File :ファイル(or フォルダ)
     *
     * @return boolean  :true 削除成功
     */
    private static boolean deleteFile(File dirOrFile) {
        if (dirOrFile.isDirectory()) {//ディレクトリ
            String[] children = dirOrFile.list();//ディレクトリにあるすべてのファイルを処理する
            for (int i=0; i<children.length; i++) {
                boolean success = deleteFile(new File(dirOrFile, children[i]));
                if (!success) {
                    continue;
                }
            }
        }

        // 削除
        return dirOrFile.delete();
    }

    /**
     * setConnectionTimeout
     * 接続時タイムアウト時間設定
     */
    public void setConnectionTimeout( int time ){
        mConnectTimeout = time;
    }

    /**
     * setConnectionTimeout
     * 読み込み時タイムアウト時間設定
     */
    public void setReadTimeout( int time ){
        mReadTimeout = time;
    }

    /**
     * shutdown アクセス終了
     */
    public void shutdown() {
        mHttpClient.getConnectionManager().shutdown();
    }

    /**
     * clearParams リクエストパラメータクリア
     *
     */
    public void clearParams() {
        mParams.clear();
    }

    /**
     * リクエストパラメータ追加
     *
     * @param name
     *            : パラメータ名称
     * @param value
     *            : パラメータ値
     */
    public void addParam(String name, String value) {

        mParams.add(new HttpUtilParam(name, value));
    }

    /**
     * リクエストパラメータ追加
     *
     * @param name
     *            : パラメータ名称
     * @param value
     *            : パラメータ値
     */
    public void addParam(String name, String postDataDirPath, String postDatafileName,
            String contentType) {

        mParams.add(new HttpUtilParam(name, postDataDirPath, postDatafileName, contentType));
    }

    /**
     * getHeaderString
     * ヘッダ情報文字列取得
     *
     * @param headerKey
     *            : ヘッダ情報名称
     * @return String : ヘッダ情報文字列
     */
    public String getHeaderString(String headerKey) {

        List<String> values = mHeaders.get(headerKey);
        if (values == null || values.size() == 0) {
            return null;
        }
        return values.get(0);
    }

    /**
     * ステータス情報取得
     *
     * @return StatusLine : ステータス情報
     */
    public int getStatusCode() {
        return mStatusCode;
    }

    /**
     * getContent
     * コンテンツデータ取得
     *
     * @return InputStream : コンテンツデータ
     */
    public InputStream getContent() {

        if (mContentData == null) {
            return null;
        }
        InputStream contentCopy = null;
        contentCopy = new ByteArrayInputStream(mContentData);
        return contentCopy;
    }

	public void OutputLog(String text){
		NaviLog.d("HttpUtils",text);
/*
		BufferedWriter bw = null;
		try {
			FileOutputStream file = new FileOutputStream(mLogFile, true);
			bw = new BufferedWriter(new OutputStreamWriter(file, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		try {
			bw.append(text+"\n");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		bw = null;
*/	}

    /**
     * httpGet
     * [HttpClient] HTTP GET メソッド実行
     *
     * @return : HTTP GET 実行ステータス
     */
    public boolean httpGet() {
    	long lStartTime =  System.currentTimeMillis();
    	boolean ret = false;
        HttpGet httpGet = null;

        try {
            // create request URL
            String requestUrl = "";
            {
                StringBuilder builder = new StringBuilder(mUrl);
                if(mUrl.indexOf("?") == -1){
                	builder.append("?");
                }else{
                	builder.append("&");
                }

                for (HttpUtilParam param : mParams) {
                    builder.append((String)param.getName());
                    builder.append("=");
                    builder.append((String)param.getValue());
                    builder.append("&");
                }
                requestUrl = builder.toString();
                requestUrl = requestUrl.substring(0, requestUrl.length() - 1);
                OutputLog("http Get URL=" + requestUrl);
            }

            httpGet = new HttpGet(requestUrl);

            HttpResponse response = mHttpClient.execute(httpGet);

            ret = saveResponseData(response);
        }catch (SocketException e){
        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server get error.");
			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
            ret = false;
            mContentData = null;
        }catch (SocketTimeoutException e){
        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server get error..");
			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
            ret = false;
            mContentData = null;
        } catch (ClientProtocolException e) {
        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server get error...");
			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
            ret = false;
            mContentData = null;
        } catch (IOException e) {
        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server get error....");
			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
            ret = false;
            mContentData = null;
        } catch (Exception e) {
        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server get error.....");
			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
            ret = false;
            mContentData = null;
        } finally {
            if (httpGet != null) {
            	try{
               		httpGet.abort();
	        	}catch(Exception e){
	            	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server get error......");
	    			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
	                ret = false;
	                mContentData = null;
	        	}
            }
        }
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"httpget time =\t" + ( System.currentTimeMillis() - lStartTime));
        return ret;
    }


    /**
     * httpPost
     * [HttpClient] HTTP POST メソッド実行
     *
     * @return : HTTP POST 実行ステータス
     */
    public boolean httpPost() {
        boolean ret = false;
        HttpPost httpPost = null;

        try {

            // param setting
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            for (HttpUtilParam param : mParams) {
                params.add(new BasicNameValuePair((String)param.getName(),
                        (String)param.getValue()));
            }

            httpPost = new HttpPost(mUrl);
            OutputLog("http Post URL=" + mUrl + "\tparam = " + params.toString());

            UrlEncodedFormEntity requestEntity = new UrlEncodedFormEntity(
                    params, HTTP.UTF_8);
            httpPost.setEntity(requestEntity);

            HttpResponse response = mHttpClient.execute(httpPost);

            ret = saveResponseData(response);
// ADD.2013.03.05 N.Sasao Bug #10373 アプリ起動に失敗する START
        } catch (SocketTimeoutException e) {
        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server post error.");
			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
            ret = false;
            mContentData = null;
        } catch (SocketException e) {
        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server post error.");
			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
            ret = false;
            mContentData = null;
// ADD.2013.03.05 N.Sasao Bug #10373 アプリ起動に失敗する  END
        } catch (UnsupportedEncodingException e) {
        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server post error.");
			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
            ret = false;
            mContentData = null;
        } catch (ClientProtocolException e) {
        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server post error..");
			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
            ret = false;
            mContentData = null;
        } catch (IOException e) {
        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server post error...");
			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
            ret = false;
            mContentData = null;
        } catch (Exception e) {
        	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server post error....");
			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
            ret = false;
            mContentData = null;
        } finally {
            if (httpPost != null) {
            	try{
            		httpPost.abort();
	        	}catch(Exception e){
	            	NaviLog.w(NaviLog.PRINT_LOG_TAG, "server post error.....");
	    			NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
	                ret = false;
	                mContentData = null;
	        	}
            }
        }
        return ret;
    }

    /**
     * saveResponseData
     * [HttpClient]レスポンスデータ保存
     *
     */
    private boolean saveResponseData(HttpResponse response) {

    	boolean ret = false;
        HttpEntity entity = null;
        try {
            mStatusCode = response.getStatusLine().getStatusCode();

            // convert headers
            {
                mHeaders.clear();
                Header[] headers = response.getAllHeaders();
                for (Header header : headers) {

                    String name = header.getName();
                    List<String> values = mHeaders.get(name);
                    if (values == null) {
                        mHeaders.put(name, new ArrayList<String>());
                        values = mHeaders.get(name);
                    }
                    values.add(header.getValue());
                }
            }

            entity = response.getEntity();
            mContentData = EntityUtils.toByteArray(entity);
            ret = true;
// ADD.2013.03.05 N.Sasao Bug #10373 アプリ起動に失敗する START
        } catch (SocketTimeoutException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            mContentData = null;
        } catch (SocketException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            mContentData = null;
// ADD.2013.03.05 N.Sasao Bug #10373 アプリ起動に失敗する  END
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            mContentData = null;
        } finally {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (SocketTimeoutException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                } catch (SocketException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                } catch (IOException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }
        }
        return ret;
    }

    private static String HTTP_UTILS_BOUNDARY_PREFIX = "HttpUtilsBondary";

    /**
     * HTTP POST メソッド実行
     *
     * @return : HTTP POST 実行ステータス
     */
   public boolean httpPostUpload() {

        boolean ret = false;
        String boundary = HTTP_UTILS_BOUNDARY_PREFIX + Long.toString(System.currentTimeMillis());

        try {

            URL url = new URL(mUrl);
            HttpURLConnection connection;
            connection = (HttpURLConnection)url.openConnection();

            connection.setConnectTimeout(mConnectTimeout);
            connection.setReadTimeout(mReadTimeout);

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Charset", "UTF-8");
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

            DataOutputStream dataOutput = new DataOutputStream(connection.getOutputStream());
            MultiPartStream multiPartStream = new MultiPartStream(dataOutput, boundary);

            //パラメータ設定
            for (HttpUtilParam param : mParams) {

                if (param.isUploadParam()) {
                   multiPartStream.addParam(param.getName(), param.getDirPath(), param.getFileName(), param.getContentType());
                } else {
                    multiPartStream.addParam(param.getName(), param.getValue());
                }

                NaviLog.d("HttpUtils", "key [" + param.getName() + "] value [" + param.getValue() + "]");
            }
            multiPartStream.addParamEnd();

            dataOutput.close();

            saveConnectionData(connection);

            connection.disconnect();
// ADD.2013.03.05 N.Sasao Bug #10373 アプリ起動に失敗する START
        } catch (SocketTimeoutException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } catch (SocketException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
// ADD.2013.03.05 N.Sasao Bug #10373 アプリ起動に失敗する  END
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }
        return ret;
    }

    /**
     * saveConnectionData
     *
     * @param connection
     */
    private void saveConnectionData(HttpURLConnection connection) {

        InputStream input = null;
        try {
            //status
            mStatusCode = connection.getResponseCode();

            //header
            {
                int idx = 0;
                while (connection.getHeaderField(idx) != null) {

                    String name = connection.getHeaderFieldKey(idx);
                    List<String> values = mHeaders.get(name);
                    if (values == null) {
                        mHeaders.put(name, new ArrayList<String>());
                        values = mHeaders.get(name);
                    }
                    values.add(connection.getHeaderField(idx));

                    idx++;
                }
            }

            //data
            {
                ByteArrayOutputStream output = new ByteArrayOutputStream();

                input = connection.getInputStream();
                byte[] buffer = new byte[1024];
                int read = input.read(buffer);
                while (read >= 0) {
                    if (read > 0) {
                        output.write(buffer, 0, read);
                    }
                    read = input.read(buffer);
                }
                mContentData = output.toByteArray();
            }
// ADD.2013.03.05 N.Sasao Bug #10373 アプリ起動に失敗する START
        } catch (SocketTimeoutException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } catch (SocketException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
// ADD.2013.03.05 N.Sasao Bug #10373 アプリ起動に失敗する  END
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }
        }

    }

    /**
     * @class HttpUtilParam
     *
     */
    static class HttpUtilParam {

        HttpUtilParam(String name, String value) {
            mName = name;
            mValue = value;
        }

        HttpUtilParam(String name, String fileDirPath, String fileName,
                String contentType) {

            mName = name;
            mValue = fileName;
            mDirPath = fileDirPath;
            mContentType = contentType;
        }

        String getName() {
            return mName;
        }

        String getValue() {
            return mValue;
        }

        String getDirPath() {
            return mDirPath;
        }

        String getFileName() {
            return mValue;
        }

        String getContentType() {
            return mContentType;
        }

        boolean isUploadParam() {
            if (mDirPath != null && mValue != null && mContentType != null) {
                return true;
            }
            return false;
        }

        protected String mName        = null;
        protected String mValue       = null;
        protected String mDirPath     = null;
        protected String mContentType = null;

    };

    /**
     * @class MultiPartStream
     *
     */
    public static class MultiPartStream {

        static private final String MARK_CRLF     = "\r\n";
        static private final String MARK_DBLQT    = "\"";
        static private final String MARK_BOUNDARY = "--";

        private DataOutputStream    mOutput       = null;
        private String              mBoundary     = null;

        MultiPartStream(DataOutputStream output, String boundary) {
            mOutput = output;
            mBoundary = boundary;
        }

        /**
         * addParam
         * パラメータ追加
         *
         * @param name
         * @param value
         * @throws IOException
         */
        public void addParam(String name, String value) throws IOException {

            mOutput.writeBytes(MARK_BOUNDARY + mBoundary + MARK_CRLF);
            mOutput.writeBytes("Content-Disposition: form-data;");
            mOutput.writeBytes(" name=" + MARK_DBLQT + name + MARK_DBLQT
                    + MARK_CRLF + MARK_CRLF);
            mOutput.writeBytes(value + MARK_CRLF);
        }

        /**
         * addParam
         * パラメータ追加
         *
         * @param name
         * @param dirPath
         * @param fileName
         * @throws IOException
         */
        public void addParam(String name, String dirPath, String fileName,
                String conentType) throws IOException {

            mOutput.writeBytes(MARK_BOUNDARY + mBoundary + MARK_CRLF);
            mOutput.writeBytes("Content-Disposition: form-data;");
            mOutput.writeBytes(" name=" + MARK_DBLQT + name + MARK_DBLQT + ";");
            mOutput.writeBytes(" filename=" + MARK_DBLQT + fileName
                    + MARK_DBLQT + MARK_CRLF);

            mOutput.writeBytes("Content-Type: " + conentType + MARK_CRLF
                    + MARK_CRLF);

            File file = CommonLib.openFile(dirPath, fileName);
            InputStream input = new FileInputStream(file);

            try {
                byte[] buffer = new byte[1024];
                int read = input.read(buffer);
                while (read >= 0) {
                    if (read > 0) {
                        mOutput.write(buffer, 0, read);
                    }
                    read = input.read(buffer);
                }
            } finally {
                input.close();
            }

            mOutput.writeBytes(MARK_CRLF);
        }

        /**
         * addParamEnd
         *
         * @throws IOException
         */
        public void addParamEnd() throws IOException {

            mOutput.writeBytes(MARK_BOUNDARY + mBoundary + MARK_BOUNDARY
                    + MARK_CRLF);
            mOutput.flush();
        }
    }

};