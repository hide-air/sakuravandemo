package net.zmap.android.pnd.v2.maps;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
// // マルチWindow幅/対応 Start
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageButton;
// マルチWindow幅/対応 End
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import net.zmap.android.pnd.v2.ExternalOpenMap;
import net.zmap.android.pnd.v2.ExternalStartNavi;
import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.api.ApiServiceConnection;
import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.ItsmoNaviDriveExternalApi;
import net.zmap.android.pnd.v2.api.exception.NaviException;
import net.zmap.android.pnd.v2.api.map.ZoomLevel;
import net.zmap.android.pnd.v2.api.overlay.Icon;
import net.zmap.android.pnd.v2.api.overlay.Line;
import net.zmap.android.pnd.v2.api.overlay.LineDrawParam;
import net.zmap.android.pnd.v2.api.util.GeoUtils;
import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.app.NaviAppStatus;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.DeliveryInfo;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.LoggerManager;
import net.zmap.android.pnd.v2.common.MapTypeListioner;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.NaviGuideRestoreFile;
import net.zmap.android.pnd.v2.common.SunClock;
import net.zmap.android.pnd.v2.common.VPSettingManager;
import net.zmap.android.pnd.v2.common.activity.BaseActivity;
import net.zmap.android.pnd.v2.common.activity.BaseActivityStack;
import net.zmap.android.pnd.v2.common.activity.MainMenu;
import net.zmap.android.pnd.v2.common.activity.MapSetting;
import net.zmap.android.pnd.v2.common.data.PoiBaseData;
import net.zmap.android.pnd.v2.common.services.CarMountStatusEventListener;
import net.zmap.android.pnd.v2.common.services.CarMountStatusService;
//import net.zmap.android.pnd.v2.common.services.ClarionService;
import net.zmap.android.pnd.v2.common.services.MonitorAdapter;
import net.zmap.android.pnd.v2.common.services.PreferencesService;
import net.zmap.android.pnd.v2.common.services.ShutDownListener;
import net.zmap.android.pnd.v2.common.services.ShutDownService;
import net.zmap.android.pnd.v2.common.services.WalkingStatusEventListener;
import net.zmap.android.pnd.v2.common.utils.DbHelper;
import net.zmap.android.pnd.v2.common.utils.InfoDialog;
import net.zmap.android.pnd.v2.common.utils.MsgDialog;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScreenAdapter;
import net.zmap.android.pnd.v2.common.utils.StorageUtil;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.common.view.OnUpdateListener;
import net.zmap.android.pnd.v2.common.view.PhotoCanvas;
import net.zmap.android.pnd.v2.common.view.UpdateThread;
import net.zmap.android.pnd.v2.common.view.WaitDialog;
import net.zmap.android.pnd.v2.data.DC_POIInfoData;
import net.zmap.android.pnd.v2.data.DC_POIInfoShowList;
import net.zmap.android.pnd.v2.data.JNIByte;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.Java_IconInfo;
import net.zmap.android.pnd.v2.data.MapInfo;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_History_FilePath;
import net.zmap.android.pnd.v2.data.POI_Mybox_ListGenreCount;
import net.zmap.android.pnd.v2.data.POI_UserFile_DataIF;
import net.zmap.android.pnd.v2.data.POI_UserFile_RecordIF;
import net.zmap.android.pnd.v2.data.PoiIconInfo;
import net.zmap.android.pnd.v2.data.ZGRL_SIZE;
import net.zmap.android.pnd.v2.data.ZNE_GuideRoutePoint;
import net.zmap.android.pnd.v2.data.ZNE_RoutePoint;
import net.zmap.android.pnd.v2.data.ZNUI_ANGLE_DATA;
import net.zmap.android.pnd.v2.data.ZNUI_DESTINATION_DATA;
import net.zmap.android.pnd.v2.data.ZNUI_TIME;
import net.zmap.android.pnd.v2.data.ZNUI_UI_MARK_DATA;
import net.zmap.android.pnd.v2.engine.NaviEngineUtils;
//import net.zmap.android.pnd.v2.dricon.controller.DriverContentsCtrl;
import net.zmap.android.pnd.v2.gps.GPSManager;
import net.zmap.android.pnd.v2.inquiry.activity.AddressProvinceInquiry;
import net.zmap.android.pnd.v2.inquiry.activity.AroundSearchMainMenuActivity;
import net.zmap.android.pnd.v2.inquiry.activity.FavoritesAddGenreInquiry;
import net.zmap.android.pnd.v2.inquiry.activity.SearchMainMenuActivity;
import net.zmap.android.pnd.v2.inquiry.activity.VehicleInquiry;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.overlay.DrawUserLayer;
//import net.zmap.android.pnd.v2.notice.controller.NotificationCtrl;
import net.zmap.android.pnd.v2.route.activity.RouteEdit;
import net.zmap.android.pnd.v2.route.activity.RouteEditMain;
import net.zmap.android.pnd.v2.route.data.RouteNodeData;
import net.zmap.android.pnd.v2.sensor.ElectricCompass;
import net.zmap.android.pnd.v2.sakuracust.CommonInquiryOps;
import net.zmap.android.pnd.v2.sakuracust.DebugLogOutput;
import net.zmap.android.pnd.v2.sakuracust.DrawTrackActivity;
import net.zmap.android.pnd.v2.sakuracust.SakuraCustPOIData;
import net.zmap.android.pnd.v2.sakuracust.TegakiMemoActivity;
import net.zmap.android.pnd.v2.sakuracust.TrackIconEdit;
import net.zmap.android.pnd.v2.sakuracust.UserIconInfo;
import net.zmap.android.pnd.v2.sakuracust.UserLineInfo;
import net.zmap.android.pnd.v2.sensor.ElectricCompass;
import net.zmap.android.pnd.v2.sensor.SensorNavigationManager;
import net.zmap.android.pnd.v2.vics.VicsManager;
import net.zmap.android.pnd.v2.widget.WidgetHostActivity;
import net.zmap.android.pnd.v2.common.view.ListDialogFragment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.reflect.Array;
/*
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
*/
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;

/**
 * @author wangdong
 *
 */
// Chg 2011/04/04 sawada Start -->
//public class MapActivity extends BaseActivity implements MapListener, OnUpdateListener, CarMountStatusEventListener, WalkingStatusEventListener {
// xuyang modi start #1662
// Chg 2011/10/19 katsuta Start -->
//public class MapActivity extends BaseActivity implements MapListener, OnUpdateListener, CarMountStatusEventListener, ShutDownListener, WalkingStatusEventListener {
public class MapActivity extends WidgetHostActivity implements MapListener, OnUpdateListener, CarMountStatusEventListener,
														 ShutDownListener, WalkingStatusEventListener, DialogInterface.OnDismissListener {
// Chg 2011/10/19 katsuta End <--
// xuyang modi end #1662
// Chg 2011/04/04 sawada End   <--
//Add 2011/06/22 Z01thedoanh Start -->
    private static Handler mHandler = null; //外部からメッセージを渡してHandlerでonExitApp()を呼び出す
    private static final int EXIT_APP_FROM_OUTSIDE = 0x1;
//Add 2011/06/22 Z01thedoanh End <--

    private static final int DIALOG_WALK_ROUTE_TOO_LONG = 0x0;

    private static final int DIALOG_ROUTE_SEARCH = 0x1;
    private static final int DIALOG_ROUTE_FAILURE = 0x2;
    private static final int DIALOG_NAVI_CANCEL = 0x3;

    private static final int DIALOG_HOME_ROUTE_ERROR = 0x5;
    private static final int DIALOG_NAVIGATION_START_AGAIN = 0x10;

    // XuYang add start 走行中の操作制限
    private static final int DIALOG_CHANGE_MODE_CAR = 0x11;
    private boolean dialogCloseFlag = true;
    // XuYang add end 走行中の操作制限

 // ADD -- 2012/04/11 -- Z01H.Fukushima --- Start ----- refs 4252
    private static final int POINTNAME_STRLEN = 20;
    //-- 定数定義 -- NEType.h の
//      #define	DEF_NE_STRLEN_POINTNAME		(40)						///< 経路地点名称文字列サイズ（NULL文字含まず）
// ADD -- 2012/04/11 -- Z01H.Fukushima --- End ------ refs 4252

    private MapView mapView;
    private MapTopView m_oTopView;
    
//    private TrackIconEdit mTrackIconEdit;
//Add 2011/09/02 Z01thedoanh Start -->
//    private DriverContentsCtrl driverContentsCtrl;
//Add 2011/09/02 Z01thedoanh End <--
////Add 2011/09/29 Z01yamaguchi Start -->
//    private VicsManager vicsManager;
////Add 2011/09/29 Z01yamaguchi End <--

    private PoiBaseData poiData;
    private SakuraCustPOIData sakuraPoiData;
    private int poi_Type = AppInfo.POI_LOCAL;
    private String strFromMode;
//    private boolean isRouteMap = false;

    private boolean isOpenMap = false;
    //private boolean isIntentOpenMap = false;
//    private boolean isNavigationStart = false;
    //   private boolean isNavigationEnd = false;
    private boolean isNavigationSuspend = false;
    //   private boolean isNavigationResumed = false;

    private JNITwoLong[] positon = null;
    private JNIString[] JNIpositonName = null;
    private JNIInt[] bPassed = null;
    private POI_UserFile_RecordIF[] UserFile_RecordIF = null;
    private POI_UserFile_DataIF[] Data = null;

//    private JNITwoLong myPosi = null;
//    private JNITwoLong targetPosi = null;
//    private JNIString targetPosiName = null;
//    private Location vehichelLocationForIntent = null;
//    private Location targetLocation = null;
//    private ZNE_RouteGuidance_t routeGuidance = null;

//    private ZNE_RouteInfo_t routeInfo = null;
//    private ZNE_RouteInfoGuidePoint_t[] guidePoint = null;
//    private Location[] locationrouteInfo = null;

//    private JNIString[] viaName = null;

    private ZNUI_DESTINATION_DATA destinationData = null;

    private POI_Mybox_ListGenreCount home1 = new POI_Mybox_ListGenreCount();
    private POI_Mybox_ListGenreCount home2 = new POI_Mybox_ListGenreCount();

    //For startNaviFromIntent Start
    //String goalName = new String();
    //List<String> lstViaRouteName = new ArrayList<String>();
    //List<Location> lstViaRouteLocation = new ArrayList<Location>();
//    private JNITwoLong[] coorViaLonlat = new JNITwoLong[5];
//    private ZCT_AreaName areaName = new ZCT_AreaName();
//    private long distinationCoorLon = 0;
//    private long distinationCoorLat = 0;
//    int routeCondition = 0;

//    private Location vehichelLocation = null;

    private int activeType = 1;
    private int transType = 0;
    private int simulationActiveType = 6;
    private int demoGuideActiveType = 6;

    private GuideView guide_Info;

    private int preMapViewType;

    private int iRemainingTime = 0;
    private int iEstimate = 0;

    private CarMountStatusService mCarMountStatusService = null;

    // xuyang add start #1662
    private ShutDownService shutDownService = null;
    // xuyang add end #1662
    private boolean bIsRestoreRoute = false;
    private boolean bIsGuidanceAgain = false;

    //Liugang add start
    private static boolean isManBackupRoute = false;
    private static boolean isCarBackupRoute = false;
    //Liugang add end

    //#907 Add by yinrongji
    private boolean bIsDemoExit = true;
    //End #907
    private boolean bIsCalculateCancle = true;
    private boolean bIsSearchDialogShow = false;

    // XuYang add start #978
    public boolean isHomeClickFlag = true;
    // XuYang add end #978
    private long splashTime = 0;

// Add 2011/10/15 katsuta Start -->
    private boolean bCreated = false;
    private boolean bDispWaitDialog = false;
    private CustomDialog mCustomDlg = null;
// Add 2011/10/15 katsuta End <--
//Add 2011/11/10 Z01_h_yamada Start -->
    private boolean  mBackupPos_Enable = false;
    private int  mBackupPos_ActiveType = 2;
    private int  mBackupPos_TransType  = 2;
    private MapInfo mBackupPos_MapInfo = new MapInfo();
    private boolean  mBackupPos_isMapScrolled = false;
    private int mBackupPos_Map_view_flag = 0;
//Add 2011/11/10 Z01_h_yamada End <--
//Chg 2011/09/27 Z01yoneya Start -->
//    private final static long MIN_SPLASH_SHOW_TIME = 4000;
//------------------------------------------------------------------------------------------------------------------------------------
    private final static long MIN_SPLASH_SHOW_TIME = 1000;
//Chg 2011/09/27 Z01yoneya End <--
//Add 2011/07/25 Z01thedoanh Start -->
    private ViewGroup.LayoutParams params;
//Add 2011/07/25 Z01thedoanh End <--
//Add 2011/10/11 Z01yoneya Start -->
//Del 2011/11/23 Z01_h_yamada Start -->
//    private static boolean bStopLaunchServiceOnDestroy = false; //マップアクティビティ終了時に起動サービスを終了するフラグ
//
//    public static void setStopServiceFlag(boolean bStopFlag) {
//        bStopLaunchServiceOnDestroy = bStopFlag;
//    }
//Del 2011/11/23 Z01_h_yamada End <--

//Add 2011/10/11 Z01yoneya End <--

    private Intent mIntent = null;

//Add 2012/04/12 Z01hirama Start --> #4223
    private boolean mDoEndBicycleNavi = false;
//Add 2012/04/12 Z01hirama End <-- #4223

// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
 // ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 Start -->
    private boolean mIsShown = false;
    public boolean IsShown(){return (mIsShown);}
 // ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END

// MOD 2014.01.22 N.Sasao OS4.4以降でWidgetが更新されない② START
	private boolean mIsActivityReturnCancel = false;
// MOD 2014.01.22 N.Sasao OS4.4以降でWidgetが更新されない②  END

// カメラ連携 Start
    // カメラで撮影するファイルのパス(一時保存用)
    private String mPhotoPath;
    private long currentTime;
    private int photoTypeNum = 0;
    private int nFP = 0;
    private int nWC = 0;
    private SQLiteDatabase mDb;
    private int mPhotoCnt;
    public int mTargetIndex;
    public View mTargetBtn; // 写真ボタンの状態を更新する為
// カメラ連携 End
// Add by CPJsunagawa '2015-07-08 Start
    //public int mIconDirection;
// Add by CPJsunagawa '2015-07-08 End

// Add by CPJsunagawa '2015-07-15 Start
    private ItsmoNaviDriveExternalApi   api;
// Add by CPJsunagawa '2015-07-15 End

    /* (non-Javadoc)
     * @see net.zmap.android.pnd.v2.common.activity.BaseActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
// MOD 2014.01.22 N.Sasao 目的地・周辺検索の地点地図で別Activityに遷移して戻るとちらつく(OS4.4から発生)対応 START
//Add 2012/01/30 h_yamada Start -->
//    	if (!NaviAppStatus.isAppRunning()) {
   		setTheme(R.style.NoAnimationTheme);
//    	}
//Add 2012/01/30 h_yamada End <--
// MOD 2014.01.22 N.Sasao 目的地・周辺検索の地点地図で別Activityに遷移して戻るとちらつく(OS4.4から発生)対応  END
        super.onCreate(savedInstanceState);

        mIntent = getIntent();

        splashTime = System.currentTimeMillis();
//Add 2011/07/25 Z01thedoanh Start -->
        View contentView = findViewById(android.R.id.content);
        View rootView = contentView.getRootView();
        View rootViewChild = ((ViewGroup)rootView).getChildAt(0);
        params = rootViewChild.getLayoutParams();
        if (CommonLib.getIsFullScreeen() == 0) {
            params.width = CommonLib.screenWidth;
            params.height = CommonLib.screenHeight;
            rootViewChild.setLayoutParams(params);
        }
//Add 2011/07/25 Z01thedoanh End <--


//@@MOD-START BB-0006 2012/12/07 Y.Hayashida
// onInitでウィジェットの有無を確認するため、onInitの後に移動
//        initScreen();
//@@MOD-END BB-0006 2012/12/07 Y.Hayashida
// MOD.2013.11.21 N.Sasao 起動速度改善 START
        mapView = MapView.getInstance();
        if (mapView == null) {
            showSplashDialog();
//Del 2011/10/12 Z01yoneya Start -->
////Add 2011/10/06 Z01yoneya Start -->
//            ((NaviApplication)getApplication()).foregroundSplashWindow();
////Add 2011/10/06 Z01yoneya End <--
//Del 2011/10/12 Z01yoneya End <--
        }
// ADD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる) START        
        else{
        	CommonLib.setSingleMapActivityStatus( false );
        }
// ADD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる)  END
		/* initScreen→onInitの順に行わないと、地点地図のボタンレイアウトが崩れるのでこの順番で行う */
    	initScreen();
        onInit();
// MOD.2013.11.21 N.Sasao 起動速度改善  END
        // XuYang add start 走行中
        // 走行中の操作制限
        mCarMountStatusService = new CarMountStatusService(this, this);

        // xuyang add start #1662
        shutDownService = new ShutDownService(this, this);
        // xuyang add end #1662
        systemLimit();
        // XuYang add end 走行中
        //for Map update
        hasRegistUpdate = true;
        IntentFilter filter = new IntentFilter();
        filter.addAction("co.jp.cslab.pnd.UPDATER_CREATED");

// Add 2011/04/04 sawada Start -->
        // 歩行・停止状態の連携
        //mWalkingStatusService = new WalkingStatusService(this, this);
// Add 2011/04/04 sawada End   <--

// Add 2011/10/15 katsuta Start -->
        bCreated = true;
// Add 2011/10/15 katsuta End <--

// Add by CPJsunagawa '13-12-25 Start
        // さくらヴァン用リスナの設定
        //SakuraRouteGuidanceListner.getMyInstance().Initialize(this);
// Add by CPJsunagawa '13-12-25 End

// Add by CPJsunagawa '15-06-07  Intent検証 Start
        // 別アプリから起動された場合の処理
    	Intent intent = getIntent();
    	String action = intent.getAction();
    	if (Intent.ACTION_SEND.equals(action)) {
    		String extras_lat = intent.getExtras().getString("latitude");
    		String extras_lon = intent.getExtras().getString("longitude");
    		if (extras_lat != null) {
    			Toast.makeText(this, "「" + extras_lat + "」を受け取りました", Toast.LENGTH_SHORT).show();

/*
    			CharSequence ext = extras.getCharSequence(Intent.EXTRA_TEXT);
    			if (ext != null) {
    				editText_.setText(ext);
          		}
      		}
    		Bundle extras = intent.getExtras();
    		if (extras != null) {
    			CharSequence ext = extras.getCharSequence(Intent.EXTRA_TEXT);
    			if (ext != null) {
    				editText_.setText(ext);
          			}
*/
      		}
    		if (extras_lon != null) {
    			Toast.makeText(this, "「" + extras_lon + "」を受け取りました", Toast.LENGTH_SHORT).show();
      		}
    	}
// Add by CPJsunagawa '15-06-07 End

    }

    private Dialog splashDialog = null;

    private void showSplashDialog() {
        //splash only
        splashDialog = new Dialog(this, R.style.dialog_fullscreen);
        Window oWindow = splashDialog.getWindow();
        if (oWindow != null) {
            // oWindow.requestFeature(Window.FEATURE_NO_TITLE);
            // oWindow.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            // WindowManager.LayoutParams.FLAG_FULLSCREEN);
            oWindow.setBackgroundDrawableResource(R.drawable.splash);
        }

//Del 2011/09/28 Z01yoneya Start -->
//        splashDialog.setOnCancelListener(new OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                // showDialog(Constants.DIALOG_EXIT_APP);
////Add 2011/09/16 Z01yoneya Start -->
//                NaviAppStatus.setAppFinishingFlag(true);
//                NaviApplication app = ((NaviApplication)getApplication());
//                app.exitApp();
////Add 2011/09/16 Z01yoneya End <--
//
//                onExitApp();
//                finish();
//            }
//        });
//Del 2011/09/28 Z01yoneya End <--
        splashDialog.show();
    }

    public void hideSplashDialog() {
        if (splashDialog != null) {
            splashDialog.dismiss();
            splashDialog = null;
        }
    }

    private void onInit() {

        // MapView
// MOD.2013.10.23 N.Sasao クラッシュレポート対応（意図しないContextを渡している）START
        MapView.init(MapActivity.this);
// MOD.2013.10.23 N.Sasao クラッシュレポート対応（意図しないContextを渡している） END
        mapView = MapView.getInstance();

        // TopView
        initView();

        showOpenMap();
// MOD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる) START
// DEL.2013.11.22 N.Sasao 起動速度改善対応 START
		// onResumeで行うため、ここでは行わない
        showMapView();
// DEL.2013.11.22 N.Sasao 起動速度改善対応  END
// MOD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる)  END

////Add 2011/09/02 Z01thedoanh Start -->
//        DriverContentsCtrl.init(this);
////Add 2011/09/02 Z01thedoanh End <--
////Add 2011/09/20 Z01sakamoto Start -->
//        driverContentsCtrl = DriverContentsCtrl.getController();
////Add 2011/09/20 Z01sakamoto End <--

////Add 2011/09/29 Z01yamaguchi Start -->
//        VicsManager.init(this);
//        vicsManager = VicsManager.getInstance();
////Add 2011/09/29 Z01yamaguchi End <--

// MOD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる) START
// DEL.2013.11.21 N.Sasao Widgetが更新されない START
//@@MOD-START BB-0006 2012/12/07 Y.Hayashida
        if( ((NaviApplication)getApplication()).isShowWidget() ){
            initializeWidgetHost(mapView.getWidgetParent(), mapView.getSurfaceView(), mapView.getTopWidget(), mapView.getRightWidget());
        }
////        initializeWidgetHost(mapView.getWidgetParent(), mapView.getSurfaceView(), mapView.getTopWidget(), mapView.getRightWidget());
//@@MOD-START BB-0006 2012/12/07 Y.Hayashida
// DEL.2013.11.21 N.Sasao Widgetが更新されない  END
// MOD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる)  END
    }

    public void onInitScreenInfo() {
        boolean isEnable = isEnableRatote();
        if (isEnable) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            onUpdate();
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        // init Screen Info
        initScreen();
    }

    private void initNaviView(boolean isCreate) {
//    	NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy initNaviView isCreate===" + isCreate + ",guide_Info==null" + (guide_Info == null));
        if (isCreate) {
            guide_Info = m_oTopView.getGuideView();
            if (guide_Info == null) {
                guide_Info = new GuideView(this);
            }
            m_oTopView.initGuideInfo(guide_Info);
        }
    }

    private void showMapView() {
        CommonLib.clearViewInLayout(mapView);
        setContentView(mapView);
    }

    private void showOpenMap() {

// Add by CPJsunagawa '13-12-25 Start
    	NaviApplication app = (NaviApplication)getApplication();
    	app.setIsManualMatching(false);
    	sakuraPoiData = null;
// Add by CPJsunagawa '13-12-25 End
    	preMapViewType = mapView.getMap_view_flag();

        Intent oIntent = getIntent();
        if (oIntent != null) {

            poi_Type = oIntent.getIntExtra(AppInfo.FLAG_POI_TYPE, AppInfo.POI_NONE);
            Serializable object = oIntent.getSerializableExtra(AppInfo.POI_DATA);
           NaviLog.d(NaviLog.PRINT_LOG_TAG,"showOpenMap-----------poi_Type==" + poi_Type );
            if (object != null && poi_Type != AppInfo.POI_NONE) {
                // Local Search
                if (poi_Type == AppInfo.POI_LOCAL) {
                    poiData = (POIData)object;
// Add by CPJsunagawa '13-12-25 Start
                    sakuraPoiData = SakuraCustPOIData.castMe(poiData);
                    //app.setIsManualMatching(true);
                    app.setIsManualMatching(sakuraPoiData != null);
                    // 必ず消す
                    //hideList();
                    showCustomerInfoAsNowMatching();
// Add by CPJsunagawa '13-12-25 End
                } else if (poi_Type == AppInfo.POI_SPOT) {
                    poiData = (PoiBaseData)object;
                }
            }

            mapView.setPointAngle(null, false, null);

            int mapViewType = mapView.getMap_view_flag();

            strFromMode = oIntent.getStringExtra(Constants.ROUTE_FLAG_KEY);

            if (poiData != null) {

                if (poiData.m_wLong == 0 || poiData.m_wLat == 0) {
                    JNITwoLong MapCenter = new JNITwoLong();
                    NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(MapCenter);

                    poiData.m_wLong = MapCenter.getM_lLong();
                    poiData.m_wLat = MapCenter.getM_lLat();
                }

//                if(poiData.m_sName == null || "".equals(poiData.m_sName)){
//                    JNIString Address = new JNIString();
//                    NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(poiData.m_wLong, poiData.m_wLat, Address);
//
//                    poiData.m_sName = Address.getM_StrAddressString();
//                }

                // updateWeather
                updateWeather(poiData.m_wLong, poiData.m_wLat);

// Chg 2011/10/07 Z01kkubo Start --> 課題No.190 高速案内中の経由地追加「地図から探す」でマップ表示不正対応
                //mapView.setMapScrolled(isMapScrolled());
                //----------------------------------------------------------------------------------------
                // 目的地検索画面から地点地図に遷移する場合は、true。
                // mapView.onMapPosChange() で必ず地図中心アイコン表示ONに持っていくため。
                // (ルート編集->追加->地図から探す->現在地ボタン押下時に正しく画面遷移するように対処)
                mapView.setMapScrolled(true);
// Chg 2011/10/07 Z01kkubo End <--

                if ((mapViewType == Constants.COMMON_MAP_VIEW) || (strFromMode != null)) {
                    mapViewType = Constants.OPEN_MAP_VIEW;
                }

                mapView.setMap_view_flag(mapViewType);

                isOpenMap = true;
                NaviLog.d(NaviLog.PRINT_LOG_TAG, "isOpenMap = true;");

                mapView.onMapPosChange();

                mapView.updateMapCentreName(poiData.m_sName);

                //hangeng add start bug1696
                PreferencesService oPre = new PreferencesService(this, "FAVORITES");
                oPre.clearAllValue();
                oPre.setValue("favoritesname", poiData.m_sName);
                oPre.save();
                //hangeng add end bug1696

            } else {
                NaviLog.d(NaviLog.PRINT_LOG_TAG, "isOpenMap = false;");
                isOpenMap = false;
            }

            //お気に入りへ登録後，表示される画面．
            boolean isFavoriteMap = oIntent.getBooleanExtra(Constants.FLAG_FAVORITE_TO_MAP, false);
            if (isFavoriteMap) {
                mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
                m_oTopView.backCarPos();
            }

            // 現在地
            if (oIntent.getBooleanExtra(Constants.FLAG_BACK_CAR_POS, false)) {
                showLocusMap();

                mapView.setMapScrolled(false);
                strFromMode = null;
            }
        } else {
            mapView.setMapScrolled(false);
            strFromMode = null;
        }

        onRouteResume();
    }

    public void updateWeather(long lon, long lat) {

    }

    private int onLocusMapMode() {
        int mapViewType = mapView.getMap_view_flag();
        // Route edit
        if (CommonLib.isNaviStart()) {
            //XuYang add start #996
            if (mapViewType != Constants.SIMULATION_MAP_VIEW) {
                //XuYang add end #996
                mapViewType = Constants.NAVI_MAP_VIEW;
                //XuYang add start #996
            }
            //XuYang add end #996
            //XuYang add start #1015
            if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_BICYCLE) {
                mapViewType = Constants.ROUTE_MAP_VIEW_BIKE;
            }
            //XuYang add end #1015
        } else {
            if (CommonLib.hasRoute()) {
                if (CommonLib.is5RouteMode()) {
                    mapViewType = Constants.FIVE_ROUTE_MAP_VIEW;
                } else {
                    mapViewType = Constants.ROUTE_MAP_VIEW;
                }
            } else {
//                	if(Constants.FROM_ROUTE_EDIT.equals(strFromMode)
//                         || Constants.FROM_ROUTE_ADD.equals(strFromMode)){
//                	} else {
                if (mapViewType == Constants.OPEN_MAP_VIEW) {
                    mapViewType = Constants.COMMON_MAP_VIEW;
                }
//                	}
            }
        }
        strFromMode = null;

        return mapViewType;
    }

    private boolean isMapScrolled() {

        JNITwoLong myPosi = new JNITwoLong();
        JNITwoLong mapCenter = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(myPosi);
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(mapCenter);
        return ((myPosi.getM_lLat() != mapCenter.getM_lLat()) && (myPosi.getM_lLong() != mapCenter.getM_lLong()));
    }

    @Override
    protected void onStart() {
        super.onStart();

        //yangyang add start 20110413  Bug813
        if (null != mapView && Constants.COMMON_MAP_VIEW == mapView.getMap_view_flag()) {
            Constants.GenreFlag = false;
        }
        //yangyang add end 20110413 Bug813
        NaviRun.setMapEventHandler(this);
        CommonLib.setBIsNaviForeground(false);

        CommonLib.setBIsAutoMap(true);
//        if (Constants.NAVI_MAP_VIEW == MapView.getInstance().getMap_view_flag()) {
//            showArrivalForecastInfo();
//            onNaviViewShow(true);
//        }
    }

    /* (non-Javadoc)
     * @see net.zmap.android.pnd.v2.common.activity.BaseActivity#onResume()
     */
    protected void onResume() {
// MOD.2013.11.21 N.Sasao 起動速度改善 START
        mapView.setMapListener(this);
        //Log.d("sakuraproblem", "NaviRun.setMapEventHandler() 2");
        NaviRun.setMapEventHandler(this);
        if (mapView != null) {
//Add 2011/11/10 Z01_h_yamada Start -->
        	NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity::onResume Activity=" + this.toString() + " mBackupPos_Enable=" + mBackupPos_Enable);
            if ( mBackupPos_Enable ) {
        	    // SavePosInfoで保存した位置情報の復帰
        	    NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity::onResume MapCenter isTaskRoot=" + isTaskRoot() + " mBackupPos_isMapScrolled=" + mBackupPos_isMapScrolled + " mBackupPos_Map_view_flag=" + mBackupPos_Map_view_flag);

        	    if ( isTaskRoot() && mBackupPos_isMapScrolled == false ) {
        		    // 現在地
        		    NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);
        		    showLocusMap();
                    NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
                    NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
        	    } else {
        		    // カーソル移動
	                MapInfo position = new MapInfo();
	        	    NaviRun.GetNaviRunObj().JNI_CT_GetReqMapPosition(position);

	        	    if (!position.isSameMap(mBackupPos_MapInfo)) {

	            	    NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(mBackupPos_ActiveType, mBackupPos_TransType, position.getAppLevel(),position.getScale(), position.getMapPoint().getLongitude(), position.getMapPoint().getLatitude(),position.getMapAngle());
	                }

	        	    if (isTaskRoot() && mBackupPos_Map_view_flag == Constants.OPEN_MAP_VIEW ) {
	        		    mBackupPos_Map_view_flag = Constants.COMMON_MAP_VIEW;
	        	    }

	        	    mapView.setMap_view_flag(mBackupPos_Map_view_flag);
        		    mapView.setMapScrolled( mBackupPos_isMapScrolled );
            	    mapView.updateMap();
        	    }

        	    mBackupPos_Enable = false;
            }
// Add by CPJsunagawa '13-12-25 Start
        	showMapView();
            mapView.onResume(this);
// Add by CPJsunagawa '13-12-25 End

// Add by CPJsunagawa 2015-08-08 Start 
            // DBに保存したアイコンをナビ上に表示する
            //ExecuteSetUserIconInfo();
            //ExecuteShowRegistIcon();
// Add by CPJsunagawa 2015-08-08 End

/*
// Add by CPJsunagawa '2015-08-08 Start
            // DBに格納されている登録した任意のアイコンを表示する
            ExecuteShowRegistIcon();
// Add by CPJsunagawa '2015-08-08 End
*/
        }
//Add 2011/11/10 Z01_h_yamada End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 Start -->
        mIsShown = true;
// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END

// MOD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる) START
// MOD.2013.11.28 N.Sasao 周辺検索経由の地点地図が描画されない(OS2.X系のみ) START
        // showMapView→mapView.onResume順番で行わないとダメ

        BaseActivityStack actStack = ((NaviApplication)getApplication()).getBaseActivityStack();
        // 周辺検索、目的地検索、履歴編集でmapActivityの2つ目を作成し、かつその場面が終了しmapActivityが一つになった時
// MOD 2014.01.22 N.Sasao OS4.4以降でWidgetが更新されない② START
        if( !CommonLib.isSingleMapActivity() && mIsActivityReturnCancel ){
			// この条件式だけは初期処理を行わないと、Layout設定タイミングがない or Widgetが更新されなくなる
        	showMapView();
// ADD.2013.11.21 N.Sasao Widgetが更新されない START
            if( ((NaviApplication)getApplication()).isShowWidget() ){
                initializeWidgetHost(mapView.getWidgetParent(), mapView.getSurfaceView(), mapView.getTopWidget(), mapView.getRightWidget());
            }
// ADD.2013.11.21 N.Sasao Widgetが更新されない  END
        	mIsActivityReturnCancel = false;
        }
    	if( actStack.getActivityCount() == 1 ){
            CommonLib.setSingleMapActivityStatus( true );
        }
// MOD 2014.01.22 N.Sasao OS4.4以降でWidgetが更新されない②  END
        mapView.onResume(this);
// MOD.2013.11.28 N.Sasao 周辺検索経由の地点地図が描画されない(OS2.X系のみ)  END
// MOD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる)  END

// MOD.2013.11.21 N.Sasao 起動速度改善  END
//// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 再バインド Start -->
//        if(DrivingRegulation.GetDrivingRegulationGuide()) {
//        	ClarionService.GetInstance().Bind();
//        }
//// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 再バインド End <--


// MOD.2013.11.22 N.Sasao 起動速度改善  END

// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.09.27 M.Honma 走行規制 通知領域非表示対応 Start -->
        DrivingRegulation.updateScreenActivity(this);
//ADD 2013.09.27 M.Honma 走行規制 通知領域非表示対応 End --<
//ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 再バインド Start -->
    	DrivingRegulation.Bind();
//ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 再バインド End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
        ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
        oNaviApi.connect(this);

        super.onResume();
    }
    
    public void ExecuteScatterIconWhenExpose()
    {
        // DBに保存したアイコンをナビ上に表示する
        ExecuteSetUserIconInfo();
        ExecuteShowRegistIcon();
    }

    protected void onPause()
    {
        ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
        oNaviApi.disconnect();
        if (mapView != null) {
// MOD.2014.03.05 N.Sasao mapActivityの上に別のActivityが表示された時、mapActivityの地図描画を止めない START
//            mapView.onPause();
// MOD.2014.03.05 N.Sasao mapActivityの上に別のActivityが表示された時、mapActivityの地図描画を止めない  END
        }
    	super.onPause();
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 Start -->
        mIsShown = false;
// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
    }

//Add 2011/11/10 Z01_h_yamada Start -->
    // 状態、位置情報の記憶関数
    //   本当は onPause() 内で処理したかったが、周辺検索の繰り返し画面遷移で、onPause前に違う位置情報が
    //   書き込まれてしまう(画面の切り替わり時のちらつき防止に必要)場合があるため
    //   必要に応じて呼んでもらう対応にした
    public void SavePosInfo() {

    	NaviRun.GetNaviRunObj().JNI_CT_GetReqMapPosition(mBackupPos_MapInfo);

        mBackupPos_isMapScrolled  = isMapScrolled();
        mBackupPos_Map_view_flag  = mapView.getMap_view_flag();
        mBackupPos_Enable = true;
    	NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity::onPause Activity=" + this.toString());
    	NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity::onPause MapCenter mBackupPos_isMapScrolled=" + mBackupPos_isMapScrolled + " mBackupPos_Map_view_flag=" + mBackupPos_Map_view_flag);
    }

//Add 2011/11/10 Z01_h_yamada End <--


    /* (non-Javadoc)
     * @see net.zmap.android.pnd.v2.common.activity.BaseActivity#onDestroy()
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapActivity::onDestroy() start isOpenMap=" + isOpenMap);

        String fileName = CommonLib.getMediaFileName();
        if (fileName != null && fileName.length() > 0) {
            CommonLib.releaseMediaPlayer();
            File file = new File(fileName);
            file.delete();
        }

        try {
            if (mapView != null) {
                mapView.onDestory();
            }
            if (mCarMountStatusService != null) {
                mCarMountStatusService.release();
            }

            // xuyang add start #1662
            if (shutDownService != null) {
                shutDownService.release();
            }
            // xuyang add end #1662

// DEL.2013.08.06 N.Sasao お知らせ機能ポップアップ非表示不具合対応 START
// ドライブコンテンツもこのタイミングのreleaseは問題があるため、削除する。
//// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
//        	if (driverContentsCtrl != null) {
//        		driverContentsCtrl.release();
//        	}
//// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
//// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
//        	NotificationCtrl noticeCtrl = NotificationCtrl.getController();
//        	if( noticeCtrl != null ){
//        		noticeCtrl.release();
//        	}
//// ADD.2013.07.22 N.Sasao お知らせ機能実装  END
// DEL.2013.08.06 N.Sasao お知らせ機能ポップアップ非表示不具合対応  END

//Add 2011/09/29 Z01yoneya Start -->
            hideSplashDialog();
//Add 2011/09/29 Z01yoneya End <--

//Add 2011/09/30 Z01yoneya Start -->
            //外部タスクマネージャアプリから終了させられた場合の対応
            //タスクマネージャが全アクティビティを終了する場合、アプリが終了しなかったのを修正
            BaseActivityStack actStack = ((NaviApplication)getApplication()).getBaseActivityStack();
            if (actStack != null) {
                NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapActivity::onDestroy() getActivityCount()=" + actStack.getActivityCount() + " isAppRunning=" + NaviAppStatus.isAppRunning() + " isAppFinishing=" + NaviAppStatus.isAppFinishing() + " bStopLaunchServiceOnDestroy=" + CommonLib.getStopServiceFlag());
//Chg 2011/10/11 Z01yoneya Start -->
//                if (actStack.getActivityCount() == 0) { //BaseActivityクラスのonDestroy()を通った後なので0でカウントする
//                  if (!NaviAppStatus.isAppFinishing()) {
//                      NaviAppStatus.setAppFinishingFlag(true);
//------------------------------------------------------------------------------------------------------------------------------------
// ADD -- 2012/04/11 -- Z01H.Fukushima(No.413) --- ↓ ----- ref 4121 追加
                if( NaviAppStatus.isAppAuthenticating() ){
            		NaviAppStatus.setAppStatus(NaviAppStatus.APP_STATE_INIT_NAVIENGINE);
                }
// ADD -- 2012/04/11 -- Z01H.Fukushima(No.413) --- ↑ ----- ref 4121 追加
// ADD -- 2012/03/22 -- Z01H.Fukushima(No.413) --- ↓ ----- ref 4121
            	if( NaviAppStatus.isAppPrepareNaviEngine() ){
            		NaviAppStatus.setAppStatus(NaviAppStatus.APP_STATE_RUNNING);
                    CommonLib.setStopServiceFlag(true);
            	}
// ADD -- 2012/03/22 -- Z01H.Fukushima(No.413) --- ↑ ----- ref 4121
                if ((NaviAppStatus.isAppRunning() || NaviAppStatus.isAppFinishing()) &&
                        actStack.getActivityCount() == 0) {//BaseActivityクラスのonDestroy()を通った後なので0でカウントする

                    CommonLib.deleteVoiceFile(((NaviApplication)getApplication()).getNaviAppDataPath().getTmpDataPath());

                    if (NaviAppStatus.isAppRunning()) {
                        try {
                            NaviAppStatus.setAppStatus(NaviAppStatus.APP_STATE_STOPPING);
                        } catch (IllegalStateException e) {
                            //例外が発生しないように状態遷移させること
                        }
                    }

//Chg 2011/11/23 Z01_h_yamada Start -->
//                    if (bStopLaunchServiceOnDestroy) {
//------------------------------------------------------------------------------------------------------------------------------------
                    if (CommonLib.getStopServiceFlag()) {
//Chg 2011/11/23 Z01_h_yamada End <--
                        NaviApplication app = ((NaviApplication)getApplication());
                        app.stopNaviService();
                    }
                    onExitApp();
                }
            }

// Add 2011/10/15 katsuta Start -->
            removeWaitDialog();
            bCreated = false;
// Add 2011/10/15 katsuta End <--

//Add 2011/09/30 Z01yoneya End <--
        } catch (Exception e) {
            //e.printStackTrace();
        }
// Add by CPJsunagawa '2015-07-15 Start
//        disconnect();
// Add by CPJsunagawa '2015-07-15 End

        NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapActivity::onDestroy() end");
    }

    /* (non-Javadoc)
     * @see net.zmap.android.pnd.v2.maps.MapListener#onMapCreated()
     */
    @Override
    public void onMapCreated() {

//Add 2011/11/10 Z01_h_yamada Start -->
    	NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapActivity::onMapCreated Activity=" + this.toString());
        mBackupPos_Enable = false;
//Add 2011/11/10 Z01_h_yamada End <--
        try {

            long time = System.currentTimeMillis() - splashTime;
            long delay = 0;
            if (time < MIN_SPLASH_SHOW_TIME) {
                delay = MIN_SPLASH_SHOW_TIME - time;
            }
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    startShowMap();
                }

            }, delay);

        } catch (Exception e) {
        }
// Add by CPJsunagawa '13-12-25 Start
    	// ここで、地図レベルを取得して、2であれば3にする。
        JNIByte beforeMapLv = new JNIByte();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapLevel(beforeMapLv);
        if (beforeMapLv.getM_bScale() == 2)
        {
        	NaviRun.GetNaviRunObj().JNI_ct_uic_SetMapLevel((byte)3);
        }
// Add by CPJsunagawa '13-12-25 End

    }

    private void startShowMap() {
//Del 2011/09/28 Z01yoneya Start -->//地図を描画したら消すように修正
//        splashDialog.dismiss();
//        splashDialog = null;
//Del 2011/09/28 Z01yoneya End <--

        finishMapUpdate();

////Add 2011/09/29 Z01yamaguchi Start -->
//        vicsManager.startServices();
////Add 2011/09/29 Z01yamaguchi End <--

        initLastRoute();
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 細街路表示制御 Start -->

// MOD.2013.12.18 N.Sasao 速度改善3 車載器連携の起動時にアプリが異常終了する対応 START
		// 地図が描画状態なら行う
		if( !NaviRun.GetNaviRunObj().isFirstDrawMap() ){
			// 状態を取得して通知
		    boolean drivingReg = DrivingRegulation.GetDrivingRegulationFlg();
		    NaviRun.GetNaviRunObj().JNI_NE_SetDrivingRegulation(drivingReg);
			NaviLog.v(NaviLog.PRINT_LOG_TAG, "MapActivity::startShowMap() JNI_NE_SetDrivingRegulation[" + drivingReg + "]");
		}
// MOD.2013.12.18 N.Sasao 速度改善3 車載器連携の起動時にアプリが異常終了する対応  END

// ADD 2013.08.08 M.Honma 走行規制 細街路表示制御 End <--
// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 Start -->
        mIsShown = true;
// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

// Add 2016-01-06 Start
        // 初回起動時は軌跡保存フラグはOFF
        NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(0);
        NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(0);
//Add 2016-01-06 End
    }

    /* (non-Javadoc)
     * @see net.zmap.android.pnd.v2.maps.MapListener#onMapUIChange()
     */
    @Override
    public void onMapUIChange() {

        if (m_oTopView != null) {
            m_oTopView.onConfigChanged();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus){
         super.onWindowFocusChanged(hasFocus);

         if (m_oTopView != null) {
             m_oTopView.AdjustGrayLeyout();
         }
// Add by CPJsunagawa '14-06-05 Start
         if (hasFocus) 
         {
        	 // リスト表示のリトライ
        	 NaviApplication na = (NaviApplication)getApplication();
        	 if (na.getIsRetryShowList())
        	 {
        		 hideList();
        		 showList(true, false, false);
        		 
        		 // 一度リトライを行ったら、フラグを下す
        		 na.clearRetryShowList();
        	 }
        	 
        	 // ボタン位置の修復を試みる。
        	 repairTopPosOfListBtn(na.getIsSetNoDelivFirst());
        	 na.clearSetNoDelivFirst();

        	 // プリファレンスに入れた、看板の表示非表示をNaviRunに反映する。
             SharedPreferences pref = getSharedPreferences( "display_status", Context.MODE_PRIVATE );
// Mod by CPJsunagawa '2015-05-31
        	 NaviRun.GetNaviRunObj().JNI_NE_setIsGuide(pref != null ? pref.getInt( "display_status", NaviRun.NE_IsGuide_DISPLAY ) : NaviRun.NE_IsGuide_DISPLAY );
//        	 NaviRun.GetNaviRunObj().NE_setIsGuide(pref != null ? pref.getInt( "display_status", NaviRun.NE_IsGuide_DISPLAY ) : NaviRun.NE_IsGuide_DISPLAY );
         }
// Add by CPJsunagawa '14-06-05 End
    }



    /* (non-Javadoc)
     * @see net.zmap.android.pnd.v2.maps.MapListener#onMapMove()
     */
    @Override
    public void onMapMove() {

        if (guide_Info != null) {
            guide_Info.onMapMove();
        }

        if (m_oTopView != null) {
            m_oTopView.onMapMove();
        }

        poi_Type = AppInfo.POI_LOCAL;
        poiData = null;
        //yangyang del start  20110505
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy onMapMove Constants.informationBarShow==="  + Constants.informationBarShow);
        //yangyang add start Bug1004

// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
//        if (!Constants.informationBarShow) {
            //yangyang add end Bug1004
            mapView.hideCurPosName();
            //yangyang add start Bug1004
//        }
// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END

        mapView.reSetPointString();
        //yangyang add end Bug1004
        //yangyang del end  20110505
        //yangyang add start Bug970
        Constants.GenreFlag = false;
        //yangyang add end Bug970
    }

    private void initMapColor() {
        JNILong mapMode = new JNILong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMPMode(mapMode);
        if (mapMode.lcount == 0) {
            SunClock sc = new SunClock(0, 0);
            JNITwoLong Coordinate = new JNITwoLong();
            NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);
            sc.recalc((int)Coordinate.getM_lLat(), (int)Coordinate.getM_lLong());
            boolean isDay = sc.isDaylight();
            if (isDay) {
                NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(Constants.TGL_MAPCOLOR_DAY);
            } else {
                NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(Constants.MAPCOLOR_NIGHT);
            }
            // CommonLib.setBMapColorAuto(true);
            Thread t = new Thread(new MapTypeListioner());
            MapTypeListioner.isRuning = true;
            MapTypeListioner.isActive = true;
            t.start();
        }
    }

    private void initAppConfig() {

        NaviAppDataPath naviAppDataPath = ((NaviApplication)getApplication()).getNaviAppDataPath();
        //データのルートパスが無い時はナビアプリは動作させない
        if (naviAppDataPath == null) {
            return;
        }

        VPSettingManager.init(naviAppDataPath.getUserDataPath());

        if (Constants.VPLOG_OUTPUT_MONITOR == Constants.ON) {
            MonitorAdapter monitor = MonitorAdapter.init();
            if (monitor != null) {
                monitor.connect(naviAppDataPath.getLogDataPath());
            }
        }

        CommonLib.readGPSIniFile(naviAppDataPath);
        int gpsRequestMinTime = CommonLib.getGPSRequestMinTime();

        //VPSettingManagerのinit()が済んでいる必要がある。
        GPSManager.init(this,
               VPSettingManager.getGpsReliabilityJudgeMode(),
               VPSettingManager.getGpsReliabilityJudgeSnrValue(),
               VPSettingManager.getGpsReliabilityJudgeAccuracyValue(),
               gpsRequestMinTime);

        if (Constants.LoadMode_SENSOR == Constants.ON) {
            SensorNavigationManager.initialize(this, SensorNavigationManager.NAVI_DRIVE_MODE_HARDWARE);
        }

        if (Constants.LoadMode_LOG == Constants.ON) {
            CommonLib.createVplogFile(naviAppDataPath);
             if (Constants.VP_LoadMode_LocationManagerLOG == 2) {
                CommonLib.openVplogFile(naviAppDataPath);
                GPSManager.start();
            }
            LoggerManager.init(naviAppDataPath.getLogDataPath());
//Add 2011/06/22 Z01thedoanh Start -->
            //Handlerの初期化（VPLogger.txtをリストで再生するため）
             initEventHandler();
//Add 2011/06/22 Z01thedoanh End <--
            if ((Constants.VP_LoadMode_LoggerManager == Constants.ON) &&
                    (Constants.LoadMode_SENSOR == Constants.OFF)) {
                SensorNavigationManager.initialize(this, SensorNavigationManager.NAVI_DRIVE_MODE_LOG);
                LoggerManager.openLogFile();
                LoggerManager.start();
            }

            if (Constants.VPLOG_OUTPUT_LOGGER == Constants.ON) {
                LoggerManager.openLogFileForWrite(naviAppDataPath.getLogDataPath());
            }
        }

// Add 2011/04/04 sawada Start -->
        ElectricCompass.init(this);
        if (Constants.USE_ELECTRIC_COMPASS == Constants.ON) {
            ElectricCompass.enable();
        }
// Add 2011/04/04 sawada End   <--
    }

    public void onInitMap() {

        net.zmap.android.pnd.v2.engine.PlatformWrapper.init(getApplicationContext());
        initAppConfig();

        // Map show Color
        initMapColor();

        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(1, 0, (byte)0xFF,1, 503186185, 128447280,0);

//        initLastRoute();
    }

    public void doExternalAction() {
        ExecutorService executor = ((NaviApplication)getApplication()).getExecutor();

        Intent intent;
        while ((intent = ((NaviApplication)getApplication()).dequeueExternalApiIntent()) != null) {
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapActivity---->CommonLib.doExternalAction() [" + intent.getAction() + "]");

            String action = intent.getAction();
            if (Constants.INTENT_VIEW_ACTION.equals(action)) {
                executor.execute(new ExternalOpenMap(this, intent));
            }
            else if (Constants.INTENT_NAVI_ACTION.equals(action)) {
                executor.execute(new ExternalStartNavi(this, intent));
            }
        }
    }

    private void initLastRoute() {
//Chg 2011/09/26 Z01yoneya Start -->
//        boolean naviDataFileExist = CommonLib.readNaviDataFile();
//------------------------------------------------------------------------------------------------------------------------------------
        boolean naviDataFileExist = CommonLib.readNaviDataFile(((NaviApplication)getApplication()).getNaviAppDataPath());
//Chg 2011/09/26 Z01yoneya End <--

        if (naviDataFileExist) {
            if (CommonLib.isExternalAction(mIntent)) {
                CommonLib.deleteNaviDataFile(((NaviApplication)getApplication()).getNaviAppDataPath());
            }
            else {
                this.showDialog(DIALOG_NAVIGATION_START_AGAIN);
            }
        }
    }

    boolean isEnableRatote() {
        if (mapView != null) {
            return mapView.isEnableSizeChange();
        }
        return true;
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

//Chg 2011/09/16 Z01yoneya Start -->
//        onMapSizeChanged();
//------------------------------------------------------------------------------------------------------------------------------------
        try {
            if (mapView != null && mapView.isInitNaviRun()) {
                onMapSizeChanged();
// Add -- 2011/10/18 -- Z01H.Fukushima --- ↓ ----- MarketV2 課題No.264
                if(m_oTopView != null){
                	onUpdate();
                }
// Add -- 2011/10/18 -- Z01H.Fukushima --- ↑ ----- MarketV2 課題No.264
            }
        } catch (Exception e) {
            //とくになにもしない
        }
//        if(driverContentsCtrl != null) {
//        	driverContentsCtrl.hideDialog();
//        }
//Chg 2011/09/16 Z01yoneya End <--
    }

    public void onMapSizeChanged() {
        onInitScreenInfo();

       // NaviLog.d(NaviLog.PRINT_LOG_TAG,"----<current> w=" + ScreenAdapter.getWidth() +", h=" + ScreenAdapter.getHeight() +", sw=" + NaviRun.GetNaviRunObj().getSurfaceWidth() +", sh=" + NaviRun.GetNaviRunObj().getSurfaceHeight());

        if (/*isEnableRatote() && */(NaviRun.GetNaviRunObj().getSurfaceHeight() != ScreenAdapter.getHeight() || NaviRun.GetNaviRunObj().getSurfaceWidth() != ScreenAdapter.getWidth())) {
            onSizeChanged();
        } else {
            if (mapView != null) {
                mapView.updateSurfaceSize();
            }
        }
    }

    private void onSizeChanged() {

        if (m_oTopView != null) {
            m_oTopView.onConfigChanged();
        }
        mapView.updateSurfaceSize();
        changeMPSize();
    }

    private void changeMPSize() {
        int lCnt = 5;
        ZGRL_SIZE[] size = new ZGRL_SIZE[lCnt];

        for (int i = 0; i < size.length; i++) {
            size[i] = new ZGRL_SIZE();
        }

//Chg 2011/08/02 Z01thedoanh (自由解像度対応) Start -->
//      //最上位プレーンのサイズ
//      size[0].setUnWidth((short) ScreenAdapter.getWidth());
//      size[0].setUnHigth((short) ScreenAdapter.getHeight());
//      //案内プレーンのサイズ
//      size[1].setUnWidth((short) 366);
//      size[1].setUnHigth((short) 455);
//      //自己位置プレーンのサイズ
//      size[2].setUnWidth((short) 100);
//      size[2].setUnHigth((short) 100);
//
//      size[3].setUnWidth((short) ScreenAdapter.getWidth());
//      size[3].setUnHigth((short) ScreenAdapter.getHeight());
//
//      size[4].setUnWidth((short) 366);
//      size[4].setUnHigth((short) 455);
        short unWindowHight = (short)(ScreenAdapter.getHeight() - ScreenAdapter.getStatusBarHeight());
        short unWindowWidth = (short)ScreenAdapter.getWidth();
//Chg 2011/10/19 Z01YUMISAKI Start -->
//        short unGuideMapsize = (short)(unWindowHight * Constants.GUIDE_AREA_ASPECT1);
//
//        NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapActivity::changeMPSize() w=" + unWindowWidth + " h=" + unWindowHight + " bar=" + ScreenAdapter.getStatusBarHeight());
//------------------------------------------------------------------------------------------------------------------------------------
        short unGuideMapWidth = 0;
        short unGuideMapHigth = 0;
        if (unWindowHight <= unWindowWidth) {
        	// Main and Guide views are arranged horizontally in landscape orientation.
        	unGuideMapHigth = unWindowHight;
        	unGuideMapWidth = (short)(unWindowWidth - unGuideMapHigth * Constants.GUIDE_AREA_ASPECT1);
        }
        else {
        	// Main and Guide views are arranged vertically in portrait orientation (currently unused).
        	unGuideMapWidth = unWindowWidth;
        	unGuideMapHigth = (short)(unWindowHight - unGuideMapWidth * Constants.GUIDE_AREA_ASPECT1);
        }

        NaviLog.d("MapView", "changeMPSize() Window = " + unWindowWidth + " x " + unWindowHight + " Guide = " + unGuideMapWidth + " x " + unGuideMapHigth);
//Chg 2011/10/19 Z01YUMISAKI End <--


        //最上位プレーンのサイズ
        size[0].setUnWidth(unWindowWidth);
        size[0].setUnHigth(unWindowHight);
        //案内プレーンのサイズ
//Chg 2011/10/19 Z01YUMISAKI Start -->
//        size[1].setUnWidth((short)(unWindowWidth - unGuideMapsize));
//        size[1].setUnHigth((short)unWindowHight);
//------------------------------------------------------------------------------------------------------------------------------------
        size[1].setUnWidth(unGuideMapWidth);
        size[1].setUnHigth(unGuideMapHigth);
//Chg 2011/10/19 Z01YUMISAKI End <--

        //自己位置プレーンのサイズ
        size[2].setUnWidth((short)100);
        size[2].setUnHigth((short)100);

        size[3].setUnWidth(unWindowWidth);
        size[3].setUnHigth(unWindowHight);
//Chg 2011/10/19 Z01YUMISAKI Start -->
//        size[4].setUnWidth((short)(unWindowWidth - unGuideMapsize));
//        size[4].setUnHigth((short)unWindowHight);
//------------------------------------------------------------------------------------------------------------------------------------
        size[4].setUnWidth(unGuideMapWidth);
        size[4].setUnHigth(unGuideMapHigth);
//Chg 2011/10/19 Z01YUMISAKI End <--
//Chg 2011/08/02 Z01thedoanh (自由解像度対応) End <--

//      NaviLog.d(NaviLog.PRINT_LOG_TAG,"----unWidth=" + size[0].getUnWidth() +", unHeight=" + size[0].getUnHigth()
//      +", w=" + ScreenAdapter.getWidth()+", h=" + ScreenAdapter.getHeight() );

        NaviRun.GetNaviRunObj().JNI_CT_SCM_ChangeSettingInfo(lCnt, size);

        //20100215
        NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();
    }

    /**
     * Init MapTopView.
     */
    private void initView() {
        m_oTopView = mapView.getMapTopView();
        if (m_oTopView == null) {
            m_oTopView = new MapTopView(this, mapView);
            mapView.setMapTopView(m_oTopView);
        }

        m_oTopView.onConfigChanged();
        m_oTopView.setMapEvent(this);

        CommonLib.clearViewInLayout(m_oTopView.getView());
        mapView.addView(m_oTopView.getView());

        initNaviView(CommonLib.isNaviState());
    }

    private void initScreen() {
        DisplayMetrics dm = getResources().getDisplayMetrics();
//Add 2011/07/25 Z01thedoanh Start -->
        if (CommonLib.getIsFullScreeen() == 0) {
            dm.widthPixels = params.width;
            dm.heightPixels = params.height;
        }
//Add 2011/07/25 Z01thedoanh End <--

        NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapActivity::initScreen() w=" + dm.widthPixels + " h=" + dm.heightPixels);

//@@MOD-START BB-0006 2012/12/07 Y.Hayashida
        NaviApplication app = ((NaviApplication)getApplication());
        if( app.isShowWidget() ){
            int widgetWidth = (int)getResources().getDimension(R.dimen.M_WidgetWidth);
            int widgetHeight = (int)getResources().getDimension(R.dimen.M_WidgetHeight);
            ScreenAdapter.init(dm.widthPixels, dm.heightPixels, dm.density, widgetWidth, widgetHeight);
        } else {
            Button topWidget = (Button)findViewById(R.id.top_dummy);
            Button rightWidget = (Button)findViewById(R.id.right_dummy);
            if( topWidget   != null ) topWidget.setVisibility(View.GONE);
            if( rightWidget != null ) rightWidget.setVisibility(View.GONE);
            ScreenAdapter.init(dm.widthPixels, dm.heightPixels, dm.density);
        }
//        ScreenAdapter.init(dm.widthPixels, dm.heightPixels, dm.density);
//@@MOD-START BB-0006 2012/12/07 Y.Hayashida

        CommonLib.setIDENSITY(dm.density);
    }

    /* (non-Javadoc)
     * @see net.zmap.android.pnd.v2.common.activity.BaseActivity#onKeyDown(int, android.view.KeyEvent)
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//Chg 2011/10/06 Z01yoneya Start -->
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//------------------------------------------------------------------------------------------------------------------------------------
        if (mapView != null && keyCode == KeyEvent.KEYCODE_BACK) {
//Chg 2011/10/06 Z01yoneya End <--
            boolean isAction = true;
            switch (mapView.getMap_view_flag()) {
                case Constants.COMMON_MAP_VIEW:
                    showDialog(Constants.DIALOG_EXIT_APP);
                    break;
                case Constants.ROUTE_MAP_VIEW:
                case Constants.FIVE_ROUTE_MAP_VIEW:
                    showDialog(DIALOG_NAVI_CANCEL);
                    break;
                case Constants.ROUTE_MAP_VIEW_BIKE:
                    clearBicycleNaviData();
                    break;
                case Constants.SIMULATION_MAP_VIEW:
                    showDialog(Constants.DIALOG_EXIT_SIMULATE_NAVI);
                    break;
                case Constants.NAVI_MAP_VIEW:
                    showDialog(Constants.DIALOG_EXIT_NAVIGATION);
                    break;
                default:
                    isAction = false;
                    break;
            }
            if (isAction) {
                return true;
            } else {
//                mapView.setMap_view_flag(preMapViewType);

                goBack();
            }
        }
        if (keyCode == KeyEvent.KEYCODE_P /*||
                                          keyCode == KeyEvent.KEYCODE_SEARCH*/) {
            JNITwoLong twoLong = new JNITwoLong();
            NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(twoLong);
            NaviRun.GetNaviRunObj().JNI_NE_AjustMyPosi(twoLong.getM_lLong(), twoLong.getM_lLat());

            showLocusMap();
            return true;
        }
        if (keyCode == KeyEvent.KEYCODE_SEARCH) {
            return true;
        }

//        if (keyCode == KeyEvent.KEYCODE_R) {
//            Constants.RunStopflag = true;
//        }
//        if (keyCode == KeyEvent.KEYCODE_S) {
//            Constants.RunStopflag = false;
//        }

        // クレードル（車載ホルダ）
//        if (keyCode == KeyEvent.KEYCODE_O) {
//         // XuYang add start 走行中の操作制限
//            dialogCloseFlag = true;
//            Constants.NaviOnOffflag = true;
////Del 2011/09/05 Z01yoneya Start -->//クレードルOnOffを廃止のため
//////Add 2011/04/06 Z01yoneya Start-->
////            SensorDataManager.onCarMountStatusChange(Constants.NaviOnOffflag);
//////Add 2011/04/06 Z01yoneya End <--
////Del 2011/09/05 Z01yoneya End <--
//            int naviMode = CommonLib.getNaviMode();
//            if(naviMode != Constants.NE_NAVIMODE_CAR) {
//                dialogCloseFlag = false;
//                showDialog(DIALOG_CHANGE_MODE_CAR);
//             }
//         // XuYang add end 走行中の操作制限
//        }
//        if (keyCode == KeyEvent.KEYCODE_F) {
//            Constants.NaviOnOffflag = false;
////Del 2011/09/05 Z01yoneya Start -->//クレードルOnOffを廃止のため
//////Add 2011/04/06 Z01yoneya Start-->
////            SensorDataManager.onCarMountStatusChange(Constants.NaviOnOffflag);
//////Add 2011/04/06 Z01yoneya End <--
////Del 2011/09/05 Z01yoneya End <--
//        }
        //yangyang add start Bug800
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            goMainMenu();
            return true;
        }
        //yangyang add end Bug800
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            // XuYang add start 走行中の操作制限
            if (dialogCloseFlag) {
                systemLimit();
            }
            // XuYang add end 走行中の操作制限
        }

        return super.onKeyDown(keyCode, event);
    }

    private void systemLimit() {
        if (m_oTopView != null) {
            m_oTopView.onSysLimit();
        }
        setLimit();
    }

    private void setLimit() {
    }

    private void onExitApp() {
        NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity::onexitApp() start");

//Add 2011/11/17 Z01_h_yamada Start -->
        ((NaviApplication)getApplication()).finalizeNaviApp();
//Add 2011/11/17 Z01_h_yamada End <--

        MapTypeListioner.isRuning = false;
        CommonLib.closeVplogFileForwrite();
        CommonLib.closeVplogFileForread();

        GPSManager.gpsStop();
        if (Constants.LoadMode_SENSOR == Constants.ON) {
//Chg 2011/09/05 Z01yoneya Start -->
//            SensorDataManager.stop();
//------------------------------------------------------------------------------------------------------------------------------------
            SensorNavigationManager.destroy();
//Chg 2011/09/05 Z01yoneya End <--
        }
        if (Constants.VP_LoadMode_LoggerManager == Constants.ON) {
            LoggerManager.stop();
//            LoggerManager.closeLogFile();
        }
        if (Constants.VPLOG_OUTPUT_LOGGER == Constants.ON) {
            LoggerManager.closeLogFileForWrite();
        }

// Add 2011/04/04 sawada Start -->
        if (Constants.USE_ELECTRIC_COMPASS == Constants.ON) {
            ElectricCompass.disable();
        }
// Add 2011/04/04 sawada End   <--

// Add 2011/03/22 Z01ONOZAWA Start --> モニタ機能対応
        if (Constants.VPLOG_OUTPUT_MONITOR == Constants.ON) {
            MonitorAdapter monitor = MonitorAdapter.getInst();
            if (monitor != null) monitor.close();
        }
// Add 2011/03/22 Z01ONOZAWA End   <--

        if (guide_Info != null) {
            guide_Info.destroyLaneImage();
        }

        // When Navigation End,Broadcast this Intent to another  application
        Intent intentBroadCast = new Intent();
        intentBroadCast.setAction(Constants.INTENT_NAVIEND);
        intentBroadCast.putExtra(Constants.INTENT_NAVIEND_EXTRA, Constants.NAVI_START_INTENT_BROADCAST);
        sendBroadcast(intentBroadCast);

        NaviRun.GetNaviRunObj().clearNaviRunObject();

//Del 2012/02/23 Z01_h_yamada Start --> #3790
//        CommonLib.setBIsNaviStart(false);
//Del 2012/02/23 Z01_h_yamada End <--

        //yangyang del start 20100420 Bug943
//        NaviRun.GetNaviRunObj().finalizeSystemResource();
        //yangyang del end 20100420 Bug943
//       	driverContentsCtrl.release();

//// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
//    	NotificationCtrl noticeCtrl = NotificationCtrl.getController();
//    	if( noticeCtrl != null ){
//    		noticeCtrl.release();
//    	}
//// ADD.2013.07.22 N.Sasao お知らせ機能実装  END

////Add 2012/02/07 Z01_h_yamada Start -->
//        vicsManager.stopServices();
////Add 2012/02/07 Z01_h_yamada End <--

        //yangyang add start 20100420 Bug943
        NaviRun.GetNaviRunObj().finalizeSystemResource();
        //yangyang add end 20100420 Bug943

//Chg 2011/11/23 Z01_h_yamada Start -->
//    	killMainThread();
//--------------------------------------------
        if (CommonLib.getStopServiceFlag()) {
        	killMainThread();
//Add 2012/02/27 Z01_h_yamada Start --> #3790
        }
      	Process.killProcess(Process.myPid());
//Add 2012/02/27 Z01_h_yamada End <--

//Chg 2011/11/23 Z01_h_yamada End <--


//Del 2012/02/07 Z01_h_yamada Start -->
////Add 2011/09/29 Z01yamaguchi Start -->
//        vicsManager.stopServices();
////Add 2011/09/29 Z01yamaguchi End <--
//Del 2012/02/07 Z01_h_yamada End <--
        NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity::onexitApp() end");
    }

    public void stopWebService()
    {
//        vicsManager.finalize();
//       	driverContentsCtrl.stopService();
//// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
//    	NotificationCtrl noticeCtrl = NotificationCtrl.getController();
//    	if( noticeCtrl != null ){
//    		noticeCtrl.release();
//    	}
//// ADD.2013.07.22 N.Sasao お知らせ機能実装  END
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        final CustomDialog oDialog = new CustomDialog(this);
// Add 2011/10/19 katsuta Start -->
        oDialog.setOnDismissListener(this);
// Add 2011/10/19 katsuta End <--
        boolean hasDialog = true;

// Add by CPJsunagawa 2015-05-13 Start
    	NaviApplication na = (NaviApplication)getApplication();
    	int matchingMode = na.getMatchigMode();
    	boolean isMatchingMode = (matchingMode >= 1);
// Add by CPJsunagawa 2015-05-13 End

        switch (id) {

            case Constants.DIALOG_MAX_FAVORITES:
                oDialog.setTitle(R.string.favorite_login_over_load_title);
                oDialog.setMessage(R.string.max_favorites_msg);
                oDialog.addCancelButton(R.string.btn_ok);
                break;

            // アプリ終了確認（ダイアログ表示）
            case Constants.DIALOG_EXIT_APP:

                oDialog.setTitle(R.string.title_dialog_exit_app);
                oDialog.setMessage(R.string.txt_exit_app_confirm);

                oDialog.addButton(R.string.btn_ok,
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (isOpenMap) {
                                    setResult(Constants.RESULTCODE_FINISH_ALL);
                                } else {
//Chg 2011/10/11 Z01yoneya Start -->
////Add 2011/09/16 Z01yoneya Start -->
//                                    NaviAppStatus.setAppFinishingFlag(true);
//                                    NaviApplication app = ((NaviApplication)getApplication());
//                                    app.stopNaviService();
////Add 2011/09/16 Z01yoneya End <--
//                                    onExitApp();
//------------------------------------------------------------------------------------------------------------------------------------
                                    try {
                                        NaviAppStatus.setAppStatus(NaviAppStatus.APP_STATE_STOPPING);
                                    } catch (IllegalStateException e) {
                                        //例外が発生しないように状態遷移させること
                                        NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapActivity::DIALOG_EXIT_APP setAppStatus exception!");
                                    }
//Chg 2011/11/23 Z01_h_yamada Start -->
//                                    bStopLaunchServiceOnDestroy = true;
//                                    stopWebService();
//
//                                    MapActivity.setStopServiceFlag(true);
//--------------------------------------------
                                    CommonLib.setStopServiceFlag(true);
                                    stopWebService();

                                    CommonLib.setStopServiceFlag(true);
//Chg 2011/11/23 Z01_h_yamada End <--

//Chg 2011/10/11 Z01yoneya End <--
                                }
                                finish();
                            }
                        });
                oDialog.addCancelButton(R.string.btn_cancel);
                break;
            // ルートシミュレーション終了確認
            case Constants.DIALOG_EXIT_SIMULATE_NAVI:

                oDialog.setTitle(R.string.navi_demo_exit_title);
                oDialog.setMessage(R.string.navi_demo_exit);
//#907 modify by yinrongji
                bIsDemoExit = false;
                oDialog.addButton(R.string.btn_ok,
                        new OnClickListener() {
                            public void onClick(View v) {
                                if (!bIsDemoExit)
                            {
                                CommonLib.setBIsDemo(false);
                                bIsDemoExit = true;
                                guideEnd(false);
                                oDialog.dismiss();
                                removeDialog(Constants.DIALOG_EXIT_SIMULATE_NAVI);
                            }
                        }
                        });
//End #907

                oDialog.addCancelButton(R.string.btn_cancel);

                break;
            // ルート案内終了確認（ダイアログ表示）
            case Constants.DIALOG_EXIT_NAVIGATION:

                oDialog.setTitle(R.string.txt_navi_exit_title);
                oDialog.setMessage(R.string.txt_navi_exit);

                oDialog.addButton(R.string.btn_ok,
                        new OnClickListener() {
                            public void onClick(View v) {
// Add by CPJsunagawa '13-12-25 Start
                            	m_oTopView.enableTapOrderNaviMode(false);
// Add by CPJsunagawa '13-12-25 End
                            	// xuyang add start #1114
                                CommonLib.setChangePos(false);
                                CommonLib.setChangePosFromSearch(false);
                                // xuyang add end #1114
                                //980 add start
                                isCarBackupRoute = false;
                                isManBackupRoute = false;
                                //980 add end
                                guideEnd(false);
//                            isNavigationEnd = true;
                                intentNavigation();
                                // Stop Orbis
                                if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_CAR) {

                                }
// Add by CPJsunagawa '13-12-25 Start
                                // ここで、リストを隠す
                                hideList();
                                
                                // 案内終了では、敢えてリストはクリアしないこととする。
                                //NaviApplication app = (NaviApplication)getApplication();
                                //app.initInfoList();
// Add by CPJsunagawa '13-12-25 End
                            	
                            	oDialog.dismiss();
                            }
                        });
                oDialog.addCancelButton(R.string.btn_cancel);

                break;
            // ルート消去
            case DIALOG_NAVI_CANCEL:
// Mod by CPJsunagawa 2015-05-13 Start
//                oDialog.setTitle(R.string.txt_navi_cancel_title);
//                oDialog.setMessage(R.string.txt_navi_cancel);
            	if (isMatchingMode)
            	{
	                oDialog.setTitle(R.string.txt_matching_end_title);
	                oDialog.setMessage(R.string.txt_matching_end);
            	}
            	else
            	{
	                oDialog.setTitle(R.string.txt_navi_cancel_title);
	                oDialog.setMessage(R.string.txt_navi_cancel);
            	}
// Mod by CPJsunagawa 2015-05-13 End
                oDialog.addButton(R.string.btn_ok,
                        new OnClickListener() {
                            public void onClick(View v) {
// Add by CPJsunagawa '13-12-25 Start
                                // リストをクリアする
                                {
                                	NaviApplication app = (NaviApplication)getApplication();
// Add by CPJsunagawa '2015-05-13 Start
                                	int mm = app.getMatchigMode();
                                	if (mm >= 1)
                                	{
                                		// マッチングモードから戻る
                                		hideList();
                                		oDialog.dismiss();
                                		
                                		goVehicleSearchForMatching(mm);
                                		
                                		return;
                                	}
// Add by CPJsunagawa '2015-05-13 End

/* Mod by CPJsunagawa '14-07-10 Start
                                 	app.clearNowRoutingCustomerIndex();
                                    app.initRouteList();
                                	app.clearCloseUpPoint();
                                	app.initInfoList();
                                	app.initProductList();
 Mod by CPJsunagawa '14-07-10 End */
                                	app.allClearList();
                                }
// Add by CPJsunagawa '13-12-25 End
                                
                                clearRoute();

// Add by CPJsunagawa '13-12-25 Start
                                // 右側のリストを引っ込める
                                hideList();
// Add by CPJsunagawa '13-12-25 End
                            	oDialog.dismiss();
                            }
                        });
                oDialog.addCancelButton(R.string.btn_cancel);
                break;
            // 自宅選択画面
            case Constants.DIALOG_HOME:

                oDialog.setTitle(R.string.title_home);
                oDialog.setMessage(R.string.show_home);
                // 自宅1
                Button btnHome1 = oDialog.addButton(R.string.btn_home1, new OnClickListener() {
                    public void onClick(View v) {
                        oDialog.dismiss();
                        naviHome(Constants.NE_HOME_1);
                    }
                });
                btnHome1.setEnabled(home1.getM_sRecordCount() > 0);

                // 自宅2
                Button btnHome2 = oDialog.addButton(R.string.btn_home2, new OnClickListener() {
                    public void onClick(View v) {
                        oDialog.dismiss();
                        naviHome(Constants.NE_HOME_2);
                    }
                });
                btnHome2.setEnabled(home2.getM_sRecordCount() > 0);

                // キャンセル
                oDialog.addCancelButton(R.string.btn_cancel);
                break;
            // 自宅未登録
            case Constants.DIALOG_NO_HOME:

                oDialog.setTitle(R.string.no_home_title);
                oDialog.setMessage(R.string.no_home);

                // キャンセル
                oDialog.addCancelButton(R.string.btn_ok);

                break;

            // ルート検索中画面
            case DIALOG_ROUTE_SEARCH:

                oDialog.setTitle(R.string.msg_route_search_waiting_title);
                oDialog.setMessage(R.string.msg_route_search_waiting);
                oDialog.setCancelable(true);
                oDialog.setShowProGressBar(true);
//Chg 2012/04/27 Z01hirama Start --> #4417
//                oDialog.addButton(R.string.btn_cancel, new OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        synchronized (NaviRun.GetNaviRunObj()) {
////Add 2012/04/17 Z01hirama Start --> #4267
//                        	if(bIsSearchDialogShow) {
////Add 2012/04/17 Z01hirama End <-- #4267
//	                            if (!bIsCalculateCancle) {
//	                                bIsCalculateCancle = true;
//	                                if (NaviRun.isRouteCalculateFinished()) {
//	                                    return;
//	                                }
//	                                NaviRun.setRouteCalculateFinished(false);
//	                                CommonLib.setIsStartCalculateRoute(false);
//	                                cancelRouteSearch();
//	                                if (0 == NaviRun.GetNaviRunObj().JNI_ct_uic_ShowRestoreRoute()) {
//	                                    bIsRestoreRoute = true;
//	                                } else {
//	                                    if (!CommonLib.IsOpenMap()) {
//	                                        mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
//	                                    }
//	                                }
//	                                oDialog.dismiss();
////Add 2012/04/17 Z01hirama Start --> #4267
//	                                bIsSearchDialogShow = false;
////Add 2012/04/17 Z01hirama End <-- #4267
//	                            }
////Add 2012/04/17 Z01hirama Start --> #4267
//                        	}
////Add 2012/04/17 Z01hirama End <-- #4267
//                        }
//                    }
//                });
//                oDialog.setOnCancelListener(new OnCancelListener() {
//                    @Override
//                    public void onCancel(DialogInterface dialog) {
//                        cancelRouteSearch();
//                        oDialog.dismiss();
////Add 2012/04/17 Z01hirama Start --> #4267
//                        bIsSearchDialogShow = false;
////Add 2012/04/17 Z01hirama End <-- #4267
//                    }
//                });
//------------------------------------------------------------------------------------------------------------------------------------
                oDialog.addButton(R.string.btn_cancel, new RouteSearchDialogCancelListner(oDialog));
                oDialog.setOnCancelListener(new RouteSearchDialogCancelListner(oDialog));
//Chg 2012/04/27 Z01hirama End <-- #4417
                break;

            case Constants.DIALOG_NO_WALKER_DATA:

                oDialog.setTitle(R.string.dialog_no_walkerdata_title);
                oDialog.setMessage(R.string.dialog_no_walkerdata);

                oDialog.addButton(R.string.btn_ok,
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                oDialog.dismiss();
                            }
                        });

                break;
            // ルート探索失敗（ダイアログ表示）
            case DIALOG_ROUTE_FAILURE:

                oDialog.setTitle(R.string.route_failure_title);
                oDialog.setMessage(R.string.route_failure_message);

//Chg 2012/04/26 Z01hirama Start --> #4417
//                oDialog.addButton(R.string.btn_ok,
//                        new OnClickListener() {
//                            public void onClick(View v) {
////Add 2012/04/12 Z01hirama Start --> #4223
//                                if(mDoEndBicycleNavi) {
//                                	clearBicycleNaviData();
//                                	mDoEndBicycleNavi = false;
//                                }
////Add 2012/04/12 Z01hirama End <-- #4223
//
//                                // NaviRun.GetNaviRunObj().JNI_ct_uic_RouteCalcAbort(NUI_MODE);
//                                NaviRun.GetNaviRunObj().JNI_CT_UIC_RouteFailerProc();
//                                // 980 bug add start
//                                if (true == isManBackupRoute) {
//                                    CommonLib.setNaviMode(Constants.NE_NAVIMODE_MAN);
//                                } else if (true == isCarBackupRoute) {
//                                    CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
//                                }
//                                // 980 bug add end
//                                if (0 == NaviRun.GetNaviRunObj().JNI_ct_uic_ShowRestoreRoute()) {
//                                    bIsRestoreRoute = true;
//                                    mapView.onNaviModeChange();
//                                    CommonLib.setIsCalcFromRouteEdit(false);
//                                } else {
//                                    CommonLib.setNaviStart(false);
//                                    CommonLib.setHasRoute(false);
//                                    CommonLib.setBIsRouteDisplay(false);
//                                    if (true == CommonLib.getIsCalcFromRouteEdit()) {
//                                        CommonLib.setIsCalcFromRouteEdit(false);
//                                        NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);
//                                        showLocusMap();
//                                        NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
//                                        mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
//                                    }
//                                }
//                                NaviRun.GetNaviRunObj().JNI_NE_MP_SetOffRoute(0);
//                                NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
//                                oDialog.dismiss();
//                            }
//                        });
//------------------------------------------------------------------------------------------------------------------------------------
                oDialog.addButton(R.string.btn_ok, new RouteFailureDialogListener(oDialog));
                oDialog.setOnCancelListener(new RouteFailureDialogListener(oDialog));
//Chg 2012/04/26 Z01hirama End <-- #4417

                break;
            // 案内再開確認
            case DIALOG_NAVIGATION_START_AGAIN:

                oDialog.setTitle(R.string.title_navi_start_again);
                oDialog.setMessage(R.string.navi_start_again);

                oDialog.addButton(R.string.btn_ok,
                        new OnClickListener() {
                            public void onClick(View v) {

                                initLastNaviInfo();
                                // xuyang add start #1662
                                mapView.onNaviModeChange();
                                // xuyang add end #1662
                                naviReStart();
                                bIsGuidanceAgain = true;
                                if (CommonLib.getNaviMode() != Constants.NE_NAVIMODE_BICYCLE) {
//Chg 2011/09/26 Z01yoneya Start -->
//                                    CommonLib.deleteNaviDataFile();
//------------------------------------------------------------------------------------------------------------------------------------
                                    CommonLib.deleteNaviDataFile(((NaviApplication)getApplication()).getNaviAppDataPath());
//Chg 2011/09/26 Z01yoneya End <--
                                }

                                oDialog.dismiss();
                            }
                        });
//Chg 2012/04/26 Z01hirama Start --> #4417
//                oDialog.addButton(R.string.btn_cancel,
//                        new OnClickListener() {
//                            public void onClick(View v) {
//                                oDialog.dismiss();
////Chg 2011/09/26 Z01yoneya Start -->
////                                CommonLib.deleteNaviDataFile();
////------------------------------------------------------------------------------------------------------------------------------------
//                                CommonLib.deleteNaviDataFile(((NaviApplication)getApplication()).getNaviAppDataPath());
////Chg 2011/09/26 Z01yoneya End <--
//                            }
//                        });
//------------------------------------------------------------------------------------------------------------------------------------
                oDialog.addButton(R.string.btn_cancel, new NavigationAgainDialogCancelListener(oDialog));
                oDialog.setOnCancelListener(new NavigationAgainDialogCancelListener(oDialog));
//Chg 2012/04/26 Z01hirama End <-- #4417

                break;

            // 徒歩ルート探索失敗
            case DIALOG_WALK_ROUTE_TOO_LONG:
                oDialog.setTitle(R.string.walk_search_failed_title);
                oDialog.setMessage(R.string.walk_search_failed);

                oDialog.addButton(R.string.btn_ok,
                        new OnClickListener() {
                            public void onClick(View v) {
                                oDialog.dismiss();
                                // 車モードに変更して、再探索開始。
                                CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
                                mapView.onNaviModeChange();
                                calcRoute();
                                //add liutch 0512 bug 1093 -->
                                onInitScreenInfo();
                                //<--
                            }
                        });
//Chg 2012/04/26 Z01hirama Start --> #4417
//                oDialog.addButton(R.string.btn_cancel,
//                        new OnClickListener() {
//                            public void onClick(View v) {
//                                oDialog.dismiss();
//                                if (CommonLib.isNaviStart()) {
//                                    NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);
//                                    showLocusMap();
//                                    NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
//                                    CommonLib.setBIsDisplayGuideInfo(false);
//                                }
//
//                            }
//                        });
//------------------------------------------------------------------------------------------------------------------------------------
                oDialog.addButton(R.string.btn_cancel, new WalkRouteTooLongDialogCancelListener(oDialog));
                oDialog.setOnCancelListener(new WalkRouteTooLongDialogCancelListener(oDialog));
//Chg 2012/04/26 Z01hirama End <-- #4417
                break;

            // 自宅探索エラー
            case DIALOG_HOME_ROUTE_ERROR:
                oDialog.setTitle(R.string.home_route_search_error_title);
                oDialog.setMessage(R.string.home_route_search_error);

                oDialog.addButton(R.string.btn_ok,
                        new OnClickListener() {
                            public void onClick(View v) {
                                oDialog.dismiss();
                            }
                        });
                break;
            // XuYang add start 走行中の操作制限
            case DIALOG_CHANGE_MODE_CAR:
                oDialog.setTitle(R.string.txt_navi_mode_change_title);
                oDialog.setMessage(R.string.mode_change_to_car_msg);
                oDialog.addButton(R.string.btn_ok,
                        new OnClickListener() {
                            public void onClick(View v) {
                                oDialog.dismiss();
                                removeDialog(DIALOG_CHANGE_MODE_CAR);
                                // 案内中であるかどうかを判断する。
                                if (CommonLib.isNaviStart()) {
                                    CommonLib.setBIsRouteDisplay(true);
                                    NaviRun.GetNaviRunObj().JNI_Java_ct_ChangeNaviMode(Constants.NE_NAVIMODE_CAR);
                                    NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
                                } else {
                                    CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
                                }
                                //add liutch 0512 bug 1093 -->
                                onInitScreenInfo();
                                //<--
                                // 地図のモード状態を更新
                                mapView.onNaviModeChange();
                                systemLimit();
                            }
                        });
                break;
            // XuYang add end 走行中の操作制限

// Add 2011/10/19 katsuta Start -->
            case Constants.DIALOG_WAIT:

//              NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity WaitDialog create bCreated=" + bCreated);
              	WaitDialog oDialog2 = new WaitDialog(this);
               	oDialog2.setShowProGressBar(true);
                oDialog2.setText(R.string.reroute_msg);
              	oDialog2.setOnKeyListener(new OnKeyListener() {
              		@Override
              		public boolean onKey(DialogInterface dialog, int keyCode,
              				KeyEvent event) {
              			if (keyCode == KeyEvent.KEYCODE_SEARCH) {
              				return true;
              			} else if (keyCode == KeyEvent.KEYCODE_BACK) {
              				removeWaitDialog();
              			}
              			return false;
              		}
               	});
               	oDialog2.setCancelable(false);
//                    NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity WaitDialog create end");
                bDispWaitDialog = true;
               	return oDialog2;
// Add 2011/10/19 katsuta End <--

            default:
                hasDialog = false;
                break;
        }

// Add 2011/10/19 katsuta Start -->
        if (mCustomDlg != null) {
        	mCustomDlg = null;
        }
        mCustomDlg = hasDialog ? oDialog : null;
        removeWaitDialog();
// Add 2011/10/19 katsuta End <--

        return hasDialog ? oDialog : super.onCreateDialog(id);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity::onActivityResult getCallingActivity=" + getCallingActivity().toShortString());
        } catch (NullPointerException e) {
            NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity::onActivityResult getCallingActivity is none");
        }
// MOD 2014.01.22 N.Sasao ルート編集経由でのルート検索で画面が黒くなる START
// MOD 2014.01.22 N.Sasao OS4.4以降でWidgetが更新されない② START
      	mIsActivityReturnCancel = true;
// MOD 2014.01.22 N.Sasao OS4.4以降でWidgetが更新されない②  END
// MOD 2014.01.22 N.Sasao ルート編集経由でのルート検索で画面が黒くなる  END
        if (resultCode == RESULT_CANCELED) {
            if (requestCode == AppInfo.ID_ACTIVITY_MAINMENU) {
                mapView.setMap_view_flag(preMapViewType);

// DEL -- 2012/03/08 -- Z01H.Fukushima --- ↓ ----- #Redmine3839 -- No.405
//            // xuyang add start #1914
//            } else if (requestCode == AppInfo.ID_ACTIVITY_MENU_NORMAL) {
//                mapView.setMap_view_flag(preMapViewType);
//                getBackMapPoint(2,2);
//                return;
//            } else if (requestCode == AppInfo.ID_ACTIVITY_MENU_NAVI) {
//                CommonLib.RouteEndNaviFlag = true;
//                getBackMapPoint(2,2);
//                getBackMapPoint(5,0);
//                return;
//            // xuyang add end #1914
// DEL -- 2012/03/08 -- Z01H.Fukushima --- ↑ ----- #Redmine3839 -- No.405

// Add by CPJsunagawa 2015-05-13 Start
            } else if (requestCode == AppInfo.ID_ACTIVITY_VEHICLE_INQUIRY) {
            	NaviApplication na = (NaviApplication)getApplication();
            	na.clearMatchigMode();
            	na.allClearList();
            	clearRoute();
// Add by CPJsunagawa 2015-05-13 End

// Add by CPJsunagawa 2016-01-26 Start
            // ライン描画
            } else if (requestCode == AppInfo.ID_ACTIVITY_DRAW_LINE) {
            	// 線の場合、1つキャンセルする必要がある。
            	NaviApplication na = (NaviApplication)getApplication();
            	try
            	{
            		Intent mapActInt = getIntent();
            		int nowIdx = mapActInt.getIntExtra(Constants.INTENT_EXTRA_DRAW_LINE, -1);
            		//if (nowIdx <= -1) return;
            		
            		if (nowIdx <= 0) {
            			// ライン描画の全キャンセル
            			na.rollbackDrawingUserLineInfo();
	        	         CommonLib.setChangePos(false);
	        	         CommonLib.setChangePosFromSearch(false);
	        	         CommonLib.setRegIcon(false);
	        	         CommonLib.setMoveIcon(false);
	        	         CommonLib.setDeleteIcon(false);
	        	         CommonLib.setDrawLine(false);
            		} else {
                    	UserLineInfo duli = na.getDrawingUserLineInfo();
                		duli.DeleteLastPoint(na.getApi(), LAYER_ID);
                		
                		boolean isEmergencyAllCancel = (duli.mFigureLine == null);
                		
        				mapActInt.putExtra(Constants.INTENT_EXTRA_DRAW_LINE, nowIdx - 1);
        				setIntent(mapActInt);
        				setResult(RESULT_CANCELED, mapActInt);
            			
            			// 全キャンセルフラグが立っていたら、これも一挙に落とす。
            			boolean isAllCancel = mapActInt.getBooleanExtra(Constants.INTENT_EXTRA_DRAW_LINE_ALL_CANCEL, false);
            			if (isEmergencyAllCancel || isAllCancel)
            			{
            				finish();
            			}
            		}
            	}
            	catch (NaviException e)
            	{
            		e.printStackTrace();
            	}
// Add by CPJsunagawa 2016-01-26 End
            	
            } else {
                return;
            }
        }

//        mapView.setMap_view_flag(preMapViewType);

        if (resultCode == Constants.RESULTCODE_FINISH_ALL) {
            NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity::onActivityResult RESULTCODE_FINISH_ALL isOpenMap=" + isOpenMap);
//Chg 2011/10/11 Z01yoneya Start -->
//            if (isOpenMap) {
//                setResult(resultCode, data);
//            } else {
//                onExitApp();
//            }
//
//            finish();
//------------------------------------------------------------------------------------------------------------------------------------
            stopWebService();
            setResult(resultCode, data);
            finish();
//Chg 2011/10/11 Z01yoneya End <--
        }

        onRouteResume();

        if (resultCode == Constants.RESULTCODE_ROUTE_DETERMINATION) {

//			if(CommonLib.IsStartCalculateRoute()){
            mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
//			}
            int NE_NEActType_RouteCalculate = 5;
            int NE_NETransType_NormalMap = 0;
            byte bScale = 0;
            long lLongitude = 0;
            long lLatitude = 0;
            NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(
                    NE_NEActType_RouteCalculate, NE_NETransType_NormalMap, bScale,1,
                    lLongitude, lLatitude,0);
            NaviRun.GetNaviRunObj().JNI_CT_ExposeMap();
            NaviRun.GetNaviRunObj().JNI_ct_ExposeMap();
            CommonLib.setIsStartCalculateRoute(true);
            showRouteSearchWaitingDialog();
            mapView.updateMapCentreName(null);
            CommonLib.setIsCalcFromRouteEdit(true); //if calculate route fail, return car position
// Add 2012/03/17 Z01_sawada(No.333) Start --> ref 2694
            CommonLib.setNaviStart(false);
// Add 2012/03/17 Z01_sawada End <-- ref 2694
            return;
        }

//Chg 2011/09/21 Z01yoneya Start -->
//        if (resultCode == Constants.RESULTCODE_FINISH_EXCEPT_MAPVIEW) {
//------------------------------------------------------------------------------------------------------------------------------------
        if (resultCode == Constants.RESULTCODE_GOBACK_TO_ROUTE_EDIT) {
            //本来はRouteEdit画面に戻るまで全てのActiviyをfinish()するのが正しいが、
            //MapActivityが全部消えるとどうなるかわからないので
            //このスコープのreturnは残しておく
//Chg 2011/09/21 Z01yoneya End <--

            if (isOpenMap) {
                setResult(resultCode, data);
                finish();
                isOpenMap = false;
            }

            mapView.setMap_view_flag(onLocusMapMode());
            //yangyang add start Bug1080
            if (CommonLib.isNaviStart() && getIntent().getBooleanExtra(Constants.PARAMS_IS_CHANGE_NAVIMODE, false)) {
                onGuideEnd();
                //
                CommonLib.setBIsRouteDisplay(true);

                int iMode = CommonLib.getNaviMode();

                if (data != null) {
                    iMode = data.getIntExtra(Constants.FLAG_NAVIMODE, iMode);
                }
//                showRouteSearchWaitingDialog();

                NaviRun.GetNaviRunObj().JNI_Java_ct_ChangeNaviMode(iMode);
                NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
            }
            //yangyang add END Bug1080
// Add by CPJsunagwa '13-12-25 Start
        	else
            {
            	int custID = data.getIntExtra(Constants.INTENT_CUST_ID, -1);
            	if (custID >= 1)
            	{
            		// ここではもう更新しない
//*            		
            		// 顧客IDがあるので、ここで座標の入れ替えを実行する。
            		long lat = data.getLongExtra(Constants.INTENT_LATITUDE, 0);
             		long lon = data.getLongExtra(Constants.INTENT_LONGITUDE, 0);
             		
             		// 変更SQLを構築する。
             		StringBuffer sb = new StringBuffer();
             		sb.append("UPDATE DeliveryInfo SET Longitude = ");
             		sb.append((double)lon / 3600000.0);
             		sb.append(",Latitude = ");
             		sb.append((double)lat / 3600000.0);
             		sb.append(",MatchLevel = 10 WHERE CustID = ");
             		sb.append(custID);
             		
             		// DBを開く
             		DbHelper dbHelper = new DbHelper(this);
             		SQLiteDatabase db = dbHelper.getWritableDatabase();
             		
             		db.execSQL(sb.toString());
             		
             		db.close();
             		dbHelper.close();
             		
             		// リストを出してみる。
             		//showList(true, false, false);
             		
             		// ルート情報の頂点を移動させる。
             		//CommonInquiryOps.setDeliveryInfoFromDB(this, cur, 0);
             		
                	//NaviApplication app = (NaviApplication)getApplication(); 
                	//RouteNodeData rnd = app.getRouteInfo(mSelectedIndex);
                	//rnd.setLat("" + lat);
                	//rnd.setLon("" + lon);
             		
             		//cur.close();
             		//db.close();
             		//dbHelper.close();
             		
             		// ルートを弾き直す
             		//onDeleteRoute();
             		//determineVehicleRoute(0);
             		//showInfoDialog(mSelectedIndex);
             	}
            }
// Add by CPJsunagwa '13-12-25 End
        	return;
        }

        // reroute
        if (resultCode == Constants.RESULT_REROUTE) {

            CommonLib.setBIsRouteDisplay(true);

            showRouteSearchWaitingDialog();
            NaviRun.GetNaviRunObj().JNI_Java_ct_SwitchNaviMode();
            NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();

            return;
        }

        // reroute mode change
        if (resultCode == Constants.RESULT_CHANGE_NAVI_MODE_MANUAL) {
            //
            onGuideEnd();
            //
            CommonLib.setBIsRouteDisplay(true);

            int iMode = CommonLib.getNaviMode();
            if (data != null) {
                iMode = data.getIntExtra(Constants.FLAG_NAVIMODE, iMode);
            }
//            showRouteSearchWaitingDialog();

            NaviRun.GetNaviRunObj().JNI_Java_ct_ChangeNaviMode(iMode);
            NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();

            return;
        }

// Add by CPJsunagawa '13-12-25 Start
        if(resultCode == Constants.RESULTCODE_VEHICLE_ROUTE_DETERMINATION) {
        	NaviApplication app = (NaviApplication)getApplication();
        	
        	// 座標に変化がなければリルートはしない。
        	boolean isNoChanged = (data != null) && data.hasExtra(Constants.INTENT_NO_CHANGE_COORD);
        	isNoChanged = isNoChanged && data.getBooleanExtra(Constants.INTENT_NO_CHANGE_COORD, false);
        	
        	boolean isDetermine = true;
        	//boolean isDetermine = !isNoChanged;
        	
        	if (data != null && data.hasExtra(Constants.CHANGE_COORD_LIST_INDEX))
        	{
        		// 座標を変更する。
        		int index = data.getIntExtra(Constants.CHANGE_COORD_LIST_INDEX, -1);
        		if (index >= 0)
        		{
        			// 編集対象が最初の未登録でなければリルートしない
        			int firstNoDelivIndex = app.getFirstNoDeliveryIndex();
        			boolean isNoFirst = (firstNoDelivIndex != index);
        			isNoChanged = isNoChanged || isNoFirst;
        			long lon = data.getLongExtra(Constants.INTENT_LONGITUDE, -1);
        			long lat = data.getLongExtra(Constants.INTENT_LATITUDE, -1);
        			if (lon != -1 && lat!= -1)
        			{
//*        				
/*        				
        				// 座標とマッチングレベルを更新
        				DeliveryInfo di = app.getInfo(index);
        				di.mPoint.setLongitudeMs(lon);
        				di.mPoint.setLatitudeMs(lat);
        				di.mMatchLebel = 10;
        				
        				// DBも更新
                 		// 変更SQLを構築する。
                 		StringBuffer sb = new StringBuffer();
                 		sb.append("UPDATE DeliveryInfo SET Longitude = ");
                 		sb.append((double)lon / 3600000.0);
                 		sb.append(",Latitude = ");
                 		sb.append((double)lat / 3600000.0);
                 		sb.append(",MatchLevel = 10 WHERE CustID = ");
                 		sb.append(di.mCustID);
                 		
                 		// DBを開く
                 		DbHelper dbHelper = new DbHelper(this);
                 		SQLiteDatabase db = dbHelper.getWritableDatabase();
                 		
                 		db.execSQL(sb.toString());
                 		db.close();
                 		dbHelper.close();
*/                 		
                 		
                 		// ボタンのカラーを変える。
/*                 		
                        LinearLayout ll = (LinearLayout)findViewById(R.id.infoList);
                        if( ll != null )
                        {
                        	LinearLayout itemLL = (LinearLayout)ll.findViewWithTag("" + index);
                        	if (itemLL != null)
                        	{
                        		itemLL.setBackgroundResource(getButtonBMPAsLevel(10));
                        	}
                        }
*/
                 		//updateListButtonColor();
                 		//showList(true, false, false);
                 		
                 		// ルートも更新
                 		CommonInquiryOps.updateRouteData(this);
                 		//showList(true, false, false);
//*/
        				isDetermine = false;
/*        				
				        CommonLib.setChangePos(true);
				        CommonLib.setChangePosFromSearch(false);
				        CommonLib.routeSearchFlag = false;

				        SakuraCustPOIData oData = new SakuraCustPOIData();
				        DeliveryInfo delivInfo = app.getInfo(index);
				        oData.m_MyIndex = index;
				        oData.m_CustID = delivInfo.mCustID;
				        oData.m_sName = delivInfo.mCustmer;
				        oData.m_sAddress = delivInfo.mAddress;
				        oData.m_wLong = lon;
				        oData.m_wLat = lat;
				        Intent intent = new Intent();
				        intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_EDIT);
				        
				        OpenMap.moveMapTo(this, oData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
*/
        			}
        		}
        	}
        	
        	app.setRoutePageFirstOnly();
//*        	
        	if (!isNoChanged)
        	{
        		final long sleepTime = 1000;
        		if (isDetermine)
        		{
        			bIsSearchDialogShow = true;
            		try
            		{
	        			Thread.sleep(sleepTime);
            		}
            		catch (InterruptedException e)
            		{
            			e.printStackTrace();
            		}
        			determineVehicleRouteFirst();
        		}
        		else
        		{
        			/*
        			AlertDialog.Builder adb = new AlertDialog.Builder(this);
        			adb.setMessage("リルートを行いますか？");
        			adb.setPositiveButton("はい", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							bIsSearchDialogShow = true;
							try
							{
								Thread.sleep(sleepTime);
							}
							catch (InterruptedException e)
			        		{
			        			e.printStackTrace();
			        		}
		        			determineVehicleRouteFirst();							
		        		}
					});
        			adb.setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							mapView.setMap_view_flag(Constants.ROUTE_MAP_VIEW);
		        		}
					});
        			adb.show();
        			*/
        			mapView.setMap_view_flag(Constants.ROUTE_MAP_VIEW);
        		}
        	}
        	else 
        	{
        		mapView.setMap_view_flag(Constants.ROUTE_MAP_VIEW);
        	}
//*/        	
    		//mapView.setMap_view_flag(Constants.ROUTE_MAP_VIEW);
        	
        	//determineVehicleRouteFirst();
        	//*
        	if (isDetermine)
        	{
	        	//app.setTapRouteStartPos(0); // 先頭に戻す。
	        	//app.setRoutePageFirstOnly();
	        	//determineVehicleRouteFirst();
	            //determineVehicleRoute(0);
	            //showList();
	            //showListPageZero();
	            //hideList();
	            showList(true, true, true);
	            app.setSetNoDelivFirst();
	        	//showList(true, false, false);
        	}
        	//*/
            return;
        }

/*        
        if(resultCode == Constants.RESULTCODE_SHOW_LIST) {
            showList();
            return;
        }
*/        
        // 位置を確かめるため、検索をした位置に飛ばす
        if (resultCode == Constants.RESULTCODE_CHECK_PLACE)
        {
        	if (data != null && data.hasExtra(Constants.CHANGE_COORD_LIST_INDEX))
        	{
        		// 座標を変更する。
        		int index = data.getIntExtra(Constants.CHANGE_COORD_LIST_INDEX, -1);
        		if (index >= 0)
        		{
        			long lon = data.getLongExtra(Constants.INTENT_LONGITUDE, -1);
        			long lat = data.getLongExtra(Constants.INTENT_LATITUDE, -1);
        			if (lon != -1 && lat!= -1)
        			{
/*        				
			        	NaviApplication app = (NaviApplication)getApplication();
				        CommonLib.setChangePos(true);
				        CommonLib.setChangePosFromSearch(false);
				        CommonLib.routeSearchFlag = false;
			
				        SakuraCustPOIData oData = new SakuraCustPOIData();
				        DeliveryInfo delivInfo = app.getInfo(index);
				        oData.m_MyIndex = index;
				        oData.m_CustID = delivInfo.mCustID;
				        oData.m_sName = delivInfo.mCustmer;
				        oData.m_sAddress = delivInfo.mAddress;
				        oData.m_wLong = lon;
				        oData.m_wLat = lat;
				        Intent intent = new Intent();
				        intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_EDIT);
				        
				        hideList();
				        OpenMap.moveMapTo(this, oData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
*/
        				NaviApplication app = (NaviApplication)getApplication();
        				DeliveryInfo delivInfo = app.getInfo(index);
        				startManualMatching(delivInfo, lat, lon);
				        return;
        			}
        		}
        	}
        }

// Add by CPJsunagawa '2015-07-08 Start
        // 選択されたシンボルを登録する
        if (resultCode == Constants.RESULTCODE_REGIST_ICON)
        {
        	if (data != null && data.hasExtra(Constants.SELECT_ICON_INDEX))
        	{
        		// 座標を変更する。
        		int index = data.getIntExtra(Constants.SELECT_ICON_INDEX, -1);
        		if (index >= 0)
        		{
        			long lon = data.getLongExtra(Constants.INTENT_LONGITUDE, -1);
        			long lat = data.getLongExtra(Constants.INTENT_LATITUDE, -1);
        			if (lon != -1 && lat!= -1)
        			{
        				NaviApplication app = (NaviApplication)getApplication();
        				DeliveryInfo delivInfo = app.getInfo(index);
        				startRegistIcon(delivInfo, index, lat, lon);
// Add '2015-08-06  DBへの登録　Start
        				// データベースにシンボル情報を登録する
        				//RegistFigureDB();
// Add '2015-08-06  DBへの登録　End

        				// 地図再描画
        				NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
        				return;
        			}
        		}
        	}
        }

        // 入力されたテキストを登録する
        if (resultCode == Constants.RESULTCODE_INPUT_TEXT)
        {
        	if (data != null && data.hasExtra(Constants.SELECT_ICON_INDEX))
        	{
        		// 座標を変更する。
        		int index = data.getIntExtra(Constants.SELECT_ICON_INDEX, -1);
        		if (index >= 0)
        		{
        			long lon = data.getLongExtra(Constants.INTENT_LONGITUDE, -1);
        			long lat = data.getLongExtra(Constants.INTENT_LATITUDE, -1);
        			if (lon != -1 && lat!= -1)
        			{
        				NaviApplication app = (NaviApplication)getApplication();
        				DeliveryInfo delivInfo = app.getInfo(index);
        				startInputText(delivInfo, lat, lon);
				        return;
        			}
        		}
        	}
        }
// Add by CPJsunagawa '2015-07-08 End

        // マッチングモード3タイプ
        if (resultCode == AppInfo.ID_ACTIVITY_DRICON_MATCHFROMMAP ||
        	resultCode == AppInfo.ID_ACTIVITY_DRICON_MATCHFROMADR ||
        	resultCode == AppInfo.ID_ACTIVITY_DRICON_MATCHCONF)
        {
        	// resultCodeをそのままマッチングモードのフラグとする。
        	goVehicleSearchForMatching(resultCode);
        	return;
        }
// Add by CPJsunagawa '13-12-25 End
        // Inquiry back
        if (requestCode == AppInfo.ID_ACTIVITY_MAINMENU) {
            mapView.setMap_view_flag(preMapViewType);
            // xuyang add start #1782
            getBackMapValue();
            // xuyang add end #1782
        }
// カメラ連携 Start
        if (requestCode == AppInfo.ID_ACTIVITY_IMAGE_CAPTURE) {
// Add by mitsuoka '14-11-21 Start
        	if(resultCode == RESULT_OK) {
                photoTypeNum = 0;
            	ListDialogFragment dialog = new ListDialogFragment();
    			dialog.setTitle(this, R.string.save_dlg_title);
    			dialog.setItems(this, R.array.save_photo_types);
    			dialog.setTargetFragment(null, AppInfo.ID_ACTIVITY_PHOTO_TYPE_LIST_DIALOG);
    			dialog.setOnCheckedChangeListener(new DialogInterface.OnClickListener() {
    				public void onClick(DialogInterface dialog, int which) {
                        photoTypeNum = which;
                        return ;
                    }
    			});
    			dialog.setOnPositiveClickListener(new DialogInterface.OnClickListener() {
    				public void onClick(DialogInterface dialog, int which) {
    					String[] items = getResources().getStringArray(R.array.save_photo_types);
    					onPhotoTypeSelectDialogPositiveClick(items[photoTypeNum]);
    					return ;
    				}
    			});
    			dialog.setOnNegativeClickListener(new DialogInterface.OnClickListener() {
    				public void onClick(DialogInterface dialog, int which) {
    					onPhotoTypeSelectDialogNegativeClick();
    					return ;
    				}
    			});
    			
    			dialog.show(getFragmentManager(), "photo_types_select_dialog");
    			//dialog.show(getSupportFragmentManager(), "photo_types_select_dialog"); // v4 supportライブラリ
            } else {
            	mSelectedView = null;
            }
// Add by mitsuoka '14-11-21 End
        } 
// カメラ連携 End
// 手書きメモ Start
        else if(requestCode == AppInfo.ID_ACTIVITY_TEGAKI_MEMO ) {
            NaviApplication app = ((NaviApplication)getApplication());
            DeliveryInfo info =  app.getInfo(mSelectedIndex);
            Bundle bundle = data.getExtras();
            String path = "";
            if (bundle != null) {
                path = bundle.getString("path", "");
            }
            File file = new File(path);
			
			// Add 2015-02-26
			if(path == null) {
				info.mHandWrittenMemo = "";
			}

			System.out.println("1:" + (resultCode == RESULT_OK) );
			System.out.println("2:" + ("".equals(info.mHandWrittenMemo)));
			System.out.println("3:" + (file != null));
			System.out.println("4:" + (file.exists()));
            // メモ無 -> 有
            // Mod 2015-02-26 文字列領域は、nullではなく””でクリアに統一
			//if(resultCode == RESULT_OK && info.mHandWrittenMemo == null && file != null && file.exists() ) {
			//if(resultCode == RESULT_OK && "".equals(info.mHandWrittenMemo) && file != null && file.exists() ) {
			if(resultCode == RESULT_OK && file != null && file.exists() ) {
                info.mHandWrittenMemo = file.getName();
                mSelectedView.setBackgroundColor(0xFF0000FF); // blue
                DbHelper dbHelper = new DbHelper(this);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("UPDATE DeliveryInfo Set HandWrittenMemoFile = '" + info.mHandWrittenMemo + "' WHERE CustID = '" + info.mCustID + "';");
                db.close();
                dbHelper.close();
// Mod 2015-02-05 手書きメモが削除された場合
            //}
        	} else {
                info.mHandWrittenMemo = "";
                mSelectedView.setBackgroundColor(0xFF000000); // black
                DbHelper dbHelper = new DbHelper(this);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
System.out.println("UPDATE DeliveryInfo Set HandWrittenMemoFile = '" + info.mHandWrittenMemo + "' WHERE CustID = '" + info.mCustID + "';");
				//db.execSQL("UPDATE DeliveryInfo Set HandWrittenMemoFile = null WHERE CustID = '" + info.mCustID + "';");
				db.execSQL("UPDATE DeliveryInfo Set HandWrittenMemoFile = '" + info.mHandWrittenMemo + "' WHERE CustID = '" + info.mCustID + "';");
                db.close();
                dbHelper.close();
        	}
            mSelectedView = null;
        }
// 手書きメモ End

     // Add by CPJsunagawa '2015-12-08 Start
        else if (resultCode == TrackIconEdit.PRESSED_SELECT_TRACK)
        {
        	// 軌跡データ選択
        	// Mod 20151221 下記関数をDrawTrackActivityに移動 
        	//executeSelectTrackData(null);
        	DrawTrackActivity.executeSelectTrackData(this, null);
        }
// Add by CPJsunagawa '2015-12-08 End
        
// Add by CPJsunagawa '2015-12-16 Start
        else if (resultCode == TrackIconEdit.PRESSED_CLEAR_TRACK)
        {
        	// 軌跡データクリア
        	// Mod 20151221 下記関数をDrawTrackActivityに移動 
        	//executeClearTrackData(null);
        	DrawTrackActivity.executeClearTrackData(this, null);
        }
// Add by CPJsunagawa '2015-12-16 End

// 2016/01/07 Yamamoto Add Start
        else if (resultCode == TrackIconEdit.PRESSED_EDIT_TRACK)
        {
            DrawTrackActivity.executeEditTrackData(this, null);
        }
// 2016/01/07 Yamamoto Add End

// Add by CPJsunagawa '2015-08-03 Start
        else if (resultCode == TrackIconEdit.PRESSED_NEW_ICON)
        {
        	// アイコン新規登録
        	executeSelectIconMain(null);
        }
        else if (resultCode == TrackIconEdit.PRESSED_MOVE_ICON)
        {
        	// アイコン移動
        	executeMoveIconMain(null);
        }
        else if (resultCode == TrackIconEdit.PRESSED_DELETE_ICON)
        {
        	// アイコン削除
        	executeDeleteIconMain(null);
        }
        else if (resultCode == TrackIconEdit.PRESSED_DRAW_LINE)
        {
        	// ライン描画
        	executeDrawUserLineMain(null);
        }
// Add by CPJsunagawa '2015-08-03 End

        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void onNewIntent(Intent intent) {
    	setIntent(intent);
    }

    // xuyang add start #1782
    private void getBackMapValue() {
        if (CommonLib.backMapFlag) {
            int ActiveType = 2;
            int TransType = 2;
            MapInfo map = new MapInfo();
            if (mBackupPos_Enable) {
            	map = mBackupPos_MapInfo;
            } else {
                NaviRun.GetNaviRunObj().JNI_CT_GetReqMapPosition(map);
            }

        	NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapActivity::getBackMapValue MapCenterInfo lon=" +map.getMapPoint().getLongitude() + " lat=" + map.getMapPoint().getLatitude());
            NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(ActiveType, TransType, map.getAppLevel(),map.getScale(),map.getMapPoint().getLongitude(), map.getMapPoint().getLatitude(),map.getMapAngle());
            // 自車の位置を取得
            JNITwoLong MyPosi = new JNITwoLong();
            NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(MyPosi);
            // 自車の位置と保存の位置を判断
            if (MyPosi.getM_lLat() ==map.getMapPoint().getLatitude() && MyPosi.getM_lLong() == map.getMapPoint().getLongitude()) {
                mapView.hideMapCentreIcon();
                NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
                NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
            }
        }
    }

    // xuyang add end #1782


//Del 2012/03/13 Z01_h_fukushima(No405) Start --> #2510
// // xuyang add start #1914
//
//    private void getBackMapPoint(int actType, int traType) {
//        if (CommonLib.backMapFlag) {
//            int ActiveType = actType;
//            int TransType = traType;
//            byte bScale = CommonLib.getScaleValue();
//            JNITwoLong MaplatLon = new JNITwoLong();
////            MaplatLon = CommonLib.getMapOldPoint();
//            if (mBackupPos_Enable) {
//            	MaplatLon.setM_lLong(mBackupPos_Longitude);
//            	MaplatLon.setM_lLat(mBackupPos_Latitude);
//            } else {
//            	MaplatLon = CommonLib.getMapOldPoint();
//            }
//            NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(ActiveType, TransType, bScale,MaplatLon.getM_lLong(), MaplatLon.getM_lLat());
//            // 自車の位置を取得
//            JNITwoLong MyPosi = new JNITwoLong();
//            NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(MyPosi);
//            // 自車の位置と保存の位置を判断
//            if (MyPosi.getM_lLat() == MaplatLon.getM_lLat() && MyPosi.getM_lLong() == MaplatLon.getM_lLong()) {
//                mapView.hideMapCentreIcon();
//                NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
//                NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
//            }
//        }
//    }
//    // xuyang add end #1914
//Del 2012/03/13 Z01_h_fukushima(No405) End <--

    private void onRouteResume() {
//        if (CommonLib.isNaviMode()) {
//        } else {
//            onGuideEnd();
//        }
    }

    /**
     * ルート消去
     */
    private void onDeleteRoute() {
        mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);

        CommonLib.setHasRoute(false);

        if (mapView.isMapScrolled()) {
            NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);
            onLocusMapDraw(false);
        }

        NaviRun.GetNaviRunObj().JNI_ct_uic_RouteCalcAbort(MapView.NUI_MODE);
        NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
        hideArrivalForecastInfo();

        NaviRun.GetNaviRunObj().setSearchKind(Constants.SCROLL_MAP_KIND);
        CommonLib.setBIsDisplayGuideInfo(false);
        mapView.setMapScrolled(isMapScrolled());
//        if (guide_Info != null) {
//            guide_Info();
//        }

        strFromMode = null;
        if (mapView != null) {
            mapView.onBackCarPos();
        }
    }

    /**
     * 案内再開
     */
    private void naviReStart() {
//        int iNaviMode = CommonLib.getNaviMode();
//        if (iNaviMode == Constants.NE_NAVIMODE_BICYCLE) {
//
//            ZNE_GuideRoutePoint RouteInfo = new ZNE_GuideRoutePoint();
//            NaviRun.GetNaviRunObj().JNI_Java_GetGuideRoutePoint(RouteInfo);
//            ZNE_RoutePoint[] guidePoint = RouteInfo.getStGuidePoint();
//
//            JNITwoLong goalInfo = guidePoint[8].getStPos();
//            saveBicycleData(goalInfo);
//        }
//        else
        {
            int NE_NEActType_RouteCalculate = 5;
            int NE_NETransType_NormalMap = 0;
            byte bScale = 0;
            long lLongitude = 0;
            long lLatitude = 0;

            NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(
                    NE_NEActType_RouteCalculate, NE_NETransType_NormalMap,
                    bScale,1, lLongitude, lLatitude,0);
            NaviRun.GetNaviRunObj().JNI_CT_ExposeMap();
            NaviRun.GetNaviRunObj().JNI_ct_ExposeMap();
            CommonLib.setIsStartCalculateRoute(true);
            showRouteSearchWaitingDialog();
        }
    }

    /**
     * ルート検索中画面
     */
    private void showRouteSearchWaitingDialog() {
        if (CommonLib.IsStartCalculateRoute()) {
            bIsCalculateCancle = false;
            bIsSearchDialogShow = true;
            showDialog(DIALOG_ROUTE_SEARCH);
        }
    }

    private void cancelRouteSearch() {

        synchronized (NaviRun.GetNaviRunObj()) {
            NaviRun.GetNaviRunObj().JNI_ct_uic_RouteCalcCancel();
            NaviRun.GetNaviRunObj().JNI_ct_uic_RouteCalcAbort(MapView.NUI_MODE);
            NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
            CommonLib.setBIsRouteCancel(true);
            if (CommonLib.is5RouteMode()) {
                CommonLib.set5RouteMode(false);
            }
            mapView.setMapScrolled(isMapScrolled());

            // calc map scroll
            mapView.updateScale();
            // if (isHomePoint){
            // NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
            // isMapScrolled = false;
            // isHomePoint = false;
            // imgMapCenterFlag.setVisibility(View.INVISIBLE);
            // }
            CommonLib.setNaviStart(false);
//Add 2012/04/17 Z01hirama Start --> #4267
            CommonLib.setHasRoute(false);
//Add 2012/04/17 Z01hirama End <-- #4267
//Add 2012/04/27 Z01hirama Start --> #4418
            // navigationStart()内で設定されているフラグクリア実施.
            isManBackupRoute = false;
            isCarBackupRoute = false;
            NaviRun.GetNaviRunObj().JNI_ct_uic_DelBackupRoute();
//Add 2012/04/27 Z01hirama End <-- #4418
            if (!CommonLib.IsOpenMap()) {
                mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
            }

        }

    }

    /**
     * ルート探索
     */
    private void routeSearch() {
        int NE_NEActType_RouteCalculate = 5;
        int NE_NETransType_NormalMap = 0;
        byte bScale = 0;
        long lLongitude = 0;
        long lLatitude = 0;

        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(
                NE_NEActType_RouteCalculate, NE_NETransType_NormalMap,
                bScale, 1, lLongitude, lLatitude, 0);
        NaviRun.GetNaviRunObj().JNI_CT_ExposeMap();
        CommonLib.setIsStartCalculateRoute(true);
        showRouteSearchWaitingDialog();
    }

// Add 2011/09/20 sawada [ExternalAPI] Start -->
    /** 案内開始 */
    public void startGuideForService(final int searchProperty, final int compexProperty, final ZNE_RoutePoint[] routePoints, final boolean routeGuidanceStarting) {
        NaviLog.d("MapActivity", "startGuideForService() start");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
		        DebugLogOutput.put("RakuRaku-Log: 案内開始（startGuideForService）");
// Add 2015-05-28 CPJsunagawa SDカードにログ出力 end
                // 案内中なら強制終了
                endGuide();

                // クルマモードへ強制変更
                CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
                mapView.onNaviModeChange();

                // 探索条件設定
                NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(
                        Constants.NAVI_START_INDEX, Constants.NAVI_DEST_INDEX, searchProperty);
                NaviRun.GetNaviRunObj().JNI_NE_Drv_SetComplexProperty(
                        compexProperty);

                // 地点削除＆設定
                for (int i = 0; i < Constants.NAVI_DEST_INDEX; i++) {
                    NaviRun.GetNaviRunObj().JNI_NE_DeleteRoutePoint(Constants.NAVI_START_INDEX + i);
                }
                initRoute(routePoints);

                if (routeGuidanceStarting) {
                    // 案内再開
                    naviReStart();
                    bIsGuidanceAgain = true;
                }
                else {
                    routeSearch();
                }
            }
        });
    }

    /** 案内開始 */
    public void startGuideForService() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
		        DebugLogOutput.put("RakuRaku-Log: 案内開始（startGuideForService2）");
// Add 2015-05-28 CPJsunagawa SDカードにログ出力 end

            	navigationStart(1);
            }
        });
    }

    public void startGuideForServiceDelayed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        navigationStart(1);
                    }
                }, 500);
            }
        });
    }

    /** 案内終了 */
    public void endGuideForService() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                endGuide();
            }
        });
    }

    public void endGuideForServiceDelayed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        endGuide();
                    }
                }, 500);
            }
        });
    }

    /** 案内終了 (内部) */
    private void endGuide() {
        switch (mapView.getMap_view_flag()) {
        case Constants.NAVI_MAP_VIEW:
            // 案内終了
            guideEnd(true);
            break;
        case Constants.SIMULATION_MAP_VIEW:
            // デモ終了
            guideEnd(false);
            onDeleteRoute();
            break;
        case Constants.ROUTE_MAP_VIEW_BIKE:
            // アロー終了
            clearBicycleNaviData();
            break;
        case Constants.ROUTE_MAP_VIEW:
        case Constants.FIVE_ROUTE_MAP_VIEW:
            // ルート取り消し
            onDeleteRoute();
            break;
        }
    }
// Add 2011/09/20 sawada [ExternalAPI] End   <--

    /**
     * 案内を終了する
     */
    private void guideEnd(boolean isExit) {
//Chg 2011/09/26 Z01yoneya Start -->
//        CommonLib.guideEnd(isExit);
//------------------------------------------------------------------------------------------------------------------------------------
        CommonLib.guideEnd(isExit, ((NaviApplication)getApplication()).getNaviAppDataPath());
//Chg 2011/09/26 Z01yoneya End <--
        onGuideEnd();

        strFromMode = null;

        mapView.setMapScrolled(isMapScrolled());

        //Reset compass Icon.
        //Bug 1127 add start
        //角度を設定する
        CommonLib.angle = 0;
        //Bug 1127 add end
        mapView.setPointAngle(null, false, null);

        //Reset car pos.
        NaviRun.GetNaviRunObj().JNI_NE_MP_SetOffRoute(0);

        onMapSizeChanged();
    }

    private void onGuideEnd() {
        if (mapView.getMap_view_flag() == Constants.ROUTE_MAP_VIEW
                || mapView.getMap_view_flag() == Constants.FIVE_ROUTE_MAP_VIEW) {
            showRouteMenu(iEstimate, destinationData);
            CommonLib.setHasRoute(true);
        } else {
            CommonLib.setHasRoute(false);
        }

        if (isNavigationSuspend) {
            isNavigationSuspend = false;
        }
        if (guide_Info != null) {
            guide_Info.guideEnd();
        }
    }

    public void showGuideEnd() {
        if (mapView.getMap_view_flag() == Constants.SIMULATION_MAP_VIEW) {
            showDialog(Constants.DIALOG_EXIT_SIMULATE_NAVI);
        } else {
            if (mapView.getMap_view_flag() != Constants.ROUTE_MAP_VIEW) {
                showDialog(Constants.DIALOG_EXIT_NAVIGATION);
            }
        }
    }

    public boolean IsManualMatching() {
        NaviApplication app = (NaviApplication)getApplication();
        return app.IsManualMatching();
    }

    /**
     * 案内終了
     */
//    private void StopGuidance() {
//        JNIInt GuideState = new JNIInt();
//        NaviRun.GetNaviRunObj().JNI_Java_GetGuideState(GuideState);
//        if (GuideState.getM_iCommon() != 0) {
//            NaviRun.GetNaviRunObj().JNI_NE_StopGuidance();
//        }
//        if (mapView.getMap_view_flag() == Constants.SIMULATION_MAP_VIEW) {
//            mapView.setMap_view_flag(Constants.NAVI_MAP_VIEW);
//        } else if (mapView.getMap_view_flag() == Constants.SIMULATION_MAP_VIEW) {
//            mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
//        }
//        // setGuideLayoutInvisibility();
//        // layoutLaneInfo.removeAllViews();
//        // guideGoalLayout.removeAllViews();
//        this.hideArrivalForecastInfo();
//    }

    /*clear all Activity except top map */
    public void goLocation() {

        if (isOpenMap) {
            //this.setResult(Constants.RESULTCODE_FINISH_EXCEPT_MAPVIEW);
            //this.finish();
            if (this.popAllActivityExceptMap()) {
                isOpenMap = false;
            }
        }
// ADD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる) START
		// 自車位置アイコン経由のmapActivityは上位のmapActivityを残すため
		// 無条件シングル扱いとする。
    	CommonLib.setSingleMapActivityStatus( true );
// ADD.2014.01.20 N.Sasao Widget初期化処理見直し(Widgetがスクロールされる)  END
    }

    public void onShow5Route(int routeType) {
        NaviRun.GetNaviRunObj().JNI_ct_uic_RouteView(routeType);
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
        updateRemainingTime();
    }

    /**
     * 現在地を中心に移動する
     */
    void showLocusMap() {

        mapView.setMap_view_flag(onLocusMapMode());
        mapView.setMapScrolled(false);

        JNITwoLong MyPosi = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(MyPosi);
        updateWeather(MyPosi.getM_lLong(), MyPosi.getM_lLat());

        onLocusMapDraw(true);

        CommonLib.setBIsDisplayGuideInfo(false);

        if (guide_Info != null) {
            guide_Info.onShowLocusMap();
        }

        if (mapView != null) {
            mapView.onBackCarPos();
        }
    }

    /**
     * 無操作地図を表示する
     */
    private void onLocusMapDraw(boolean isMove) {

        mapView.hideMapCentreIcon();
        mapView.hideCurPosName();

//        NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);
//        if(isMove){
//            NaviRun.GetNaviRunObj().setSearchKind(Constants.SCROLL_MAP_KIND);
//
//            NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
//            NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
//        }/*else{
//            NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();
//        }*/
    }

    private boolean isShowMenu() {
        if (m_oTopView != null) {
            return m_oTopView.isShowMenu();
        }
        return false;
    }

    /**
     * 総合メニューを遷移
     */
    public void goMainMenu() {
        if (isShowMenu()) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 メインメニュー メニューキー禁止 Start -->
        	if(DrivingRegulation.CheckDrivingRegulation()){
        		return;
        	}
// ADD 2013.08.08 M.Honma 走行規制 メインメニュー メニューキー禁止 <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
            int menu_show_type = Constants.MENU_NORMAL;
            int reqId = AppInfo.ID_ACTIVITY_MENU_NORMAL;

//Del 2012/03/13 Z01_h_fukushima(No.405) Start --> ref 2510
//            // xuyang add start #1914
//        	preMapViewType = MapView.getInstance().getMap_view_flag();
//        	setBackMapValue();
//        	JNITwoLong myPosi = new JNITwoLong();
//        	NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(myPosi);
//        	CommonLib.setMapOldPoint(myPosi);
//        	// xuyang add end #1914
//Del 2012/03/13 Z01_h_fukushima(No.405) End <--
        	switch (MapView.getInstance().getMap_view_flag()) {
                case Constants.NAVI_MAP_VIEW:
                case Constants.SIMULATION_MAP_VIEW:
                    menu_show_type = Constants.MENU_NAVI;
                    reqId = AppInfo.ID_ACTIVITY_MENU_NAVI;
                    break;
                default:
                    break;
            }

            Intent intent = new Intent();
            intent.setClass(this, MainMenu.class);
            onGoRoute(intent);
            intent.putExtra(Constants.FLAG_MENU_TYPE, menu_show_type);
//Chg 2011/09/23 Z01yoneya Start -->
//            startActivityForResult(intent, reqId);
//------------------------------------------------------------------------------------------------------------------------------------
            NaviActivityStarter.startActivityForResult(this, intent, reqId);
//Chg 2011/09/23 Z01yoneya End <--
//            NaviLog.d(NaviLog.PRINT_LOG_TAG," liutch =======================>menu_show_type= "+menu_show_type+"   isOpenMap="+isOpenMap+"   MapView.getInstance().getMap_view_flag()="+MapView.getInstance().getMap_view_flag());
            //add liutch 0507 bug 908 -->
            //if( menu_show_type==Constants.MENU_NAVI ){
            goLocation();
            //}
            //<--
        }
    }

    // xuyang add start #1782
    private int[] scaleValue = {10, 20, 40, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000};

    private void setBackMapValue() {

        JNILong lnDistance = new JNILong();
        NaviRun.GetNaviRunObj().JNI_Java_GetScaleDistance(lnDistance);
        byte bScale = 0;
        for (int i = 0; i < scaleValue.length; i++) {
            if (lnDistance.lcount == scaleValue[i]) {
                bScale = (byte)i;
                break;
            }
        }
        CommonLib.setScaleValue(bScale);
        CommonLib.backMapFlag = false;
    }

    // xuyang add end #1782

    /**
     * 周辺検索メニューを遷移
     */
    public void goSurroundInquiry(boolean flag) {
        preMapViewType = mapView.getMap_view_flag();
        //Modify start For Redmine 1660
        JNITwoLong myPosi = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(myPosi);
        CommonLib.setMapCenterInfo(myPosi);
        //Modify end For Redmine 1660

//Add 2011/11/10 Z01_h_yamada Start -->
        BaseActivityStack actStack = ((NaviApplication)getApplication()).getBaseActivityStack();
        if (actStack != null) {
            // 前回の周辺検索結果は破棄する
        	int count = actStack.getActivityCount(AroundSearchMainMenuActivity.class);
        	for (int i = 0; i < count; ++i) {
        		actStack.removeActivity(AroundSearchMainMenuActivity.class, MapActivity.class);
        	}
        }
//Add 2011/11/10 Z01_h_yamada End <--

        // XuYang add start
        Intent oIntent = new Intent();
        CommonLib.setAroundFlg(false);
        oIntent.setClass(this, AroundSearchMainMenuActivity.class);
        oIntent.putExtra(Constants.FROM_FLAG_KEY, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
//Chg 2011/09/23 Z01yoneya End <--
        // XuYang add end

//Del 2011/11/10 Z01_h_yamada Start -->
//      if (!flag) {
//      goLocation();
//  }
//Del 2011/11/10 Z01_h_yamada End <--
//// ADD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2 START
//        mapView.hideCurPosName();
//// ADD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2  END
   }

    /**
     * 目的地検索メニューを遷移
     */
    public void goDestInquiry() {
        Intent oIntent = new Intent();
        // xuyang add start #1782
        setBackMapValue();
        // xuyang add end #1782
        //Modify start For Redmine 1660
        JNITwoLong myPosi = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(myPosi);
        CommonLib.setMapCenterInfo(myPosi);
        //Modify end For Redmine 1660

        oIntent.setClass(this, SearchMainMenuActivity.class);
        oIntent.putExtra(Constants.FROM_FLAG_KEY, AppInfo.ID_ACTIVITY_MAINMENU);
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_MAINMENU);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_MAINMENU);
//Chg 2011/09/23 Z01yoneya End <--
        preMapViewType = mapView.getMap_view_flag();

        goLocation();
    }

//    private int iFlag = 1;
    /**
     * 自宅
     */
    public void onClickHome() {
        NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
        NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecCount(7, home1);
        NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecCount(8, home2);
//test offroute
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"----->>iFlag = " + iFlag);
//        NaviRun.GetNaviRunObj().JNI_NE_MP_SetOffRoute( iFlag );
//        iFlag = (0 == iFlag ) ? 1 : 0;

        if (home1.getM_sRecordCount() == 0 && home2.getM_sRecordCount() == 0) {
            showDialog(Constants.DIALOG_NO_HOME);
        } else {
            removeDialog(Constants.DIALOG_HOME);
            showDialog(Constants.DIALOG_HOME);
        }
    }

    private void naviHome(int index) {
        POI_UserFile_RecordIF[] m_Poi_UserFile_Listitem = new POI_UserFile_RecordIF[1];
        m_Poi_UserFile_Listitem[0] = new POI_UserFile_RecordIF();
        JNILong Rec = new JNILong();
        NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecList(index, 0, 1, m_Poi_UserFile_Listitem, Rec);
//        int activeType = 2;
//        int transType = 2;
//        byte bScale = 4;
        long Longitude = 0;
        long Latitude = 0;
        Latitude = m_Poi_UserFile_Listitem[0].m_lLat;
        Longitude = m_Poi_UserFile_Listitem[0].m_lLon;
//        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(activeType, transType, bScale, Longitude, Latitude);

        //Set Home as destination
        NaviRun.GetNaviRunObj().JNI_CT_ICU_SetDistination(Longitude, Latitude);

//        if (isOpenMap) {
//            setResult(Constants.RESULTCODE_FINISH_EXCEPT_MAPVIEW);
//            finish();
//        }

//Chg 2011/11/11 Z01_h_yamada Start -->
//        JNITwoLong carPos = new JNITwoLong();
//        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(carPos);
//
//        int naviMode = CommonLib.getNaviMode();
//        int dialog_id = -1;
//        long routeDist = CommonLib.calcDist(Longitude, Latitude, carPos.getM_lLong(), carPos.getM_lLat());
//        // XuYang add start #978
//        isHomeClickFlag = true;
//        // XuYang add end #978
//        if (naviMode == Constants.NE_NAVIMODE_CAR) {
//            if (routeDist < Constants.NAVI_HOME_CAR_ROUTE_MIN_DIST) {
//                dialog_id = DIALOG_HOME_ROUTE_ERROR;
//            }
//        } else {
//            if (routeDist < Constants.NAVI_HOME_WALK_BIKE_ROUTE_MIN_DIST) {
//                dialog_id = DIALOG_HOME_ROUTE_ERROR;
//            }
//            // XuYang modi start #978
//            if (naviMode == Constants.NE_NAVIMODE_MAN) {
//                isHomeClickFlag = false;
//                // XuYang modi end #978
//                if (routeDist > Constants.MAX_WALK_ROUTE_DIST) {
//                    dialog_id = DIALOG_WALK_ROUTE_TOO_LONG;
//                }
//            }
//        }
//
//        if (dialog_id == -1) {
//            calcRoute();
//        } else {
//            showDialog(dialog_id);
//        }
//-------------------------------------
        JNITwoLong carPos = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(carPos);

        int naviMode = CommonLib.getNaviMode();
        int dialog_id = -1;
        isHomeClickFlag = true;
        if (naviMode == Constants.NE_NAVIMODE_MAN) {
            isHomeClickFlag = false;
            long routeDist = CommonLib.calcDist(Longitude, Latitude, carPos.getM_lLong(), carPos.getM_lLat());
            if (routeDist > Constants.MAX_WALK_ROUTE_DIST) {
                dialog_id = DIALOG_WALK_ROUTE_TOO_LONG;
            }
        }

        if (dialog_id == -1) {
            calcRoute();
        } else {
            showDialog(dialog_id);
        }
//Chg 2011/11/11 Z01_h_yamada End <--

    }


// Add by CPJsunagawa '143-12-25  Start -->
    public void goTapOrderNAVI() {
        DbHelper helper = new DbHelper(getApplicationContext());
//        SQLiteDatabase db = helper.getReadableDatabase(Constants.DB_PASSWORD);
        SQLiteDatabase db = helper.getReadableDatabase();
        if(db == null ) {
            Toast.makeText(this, getString(R.string.maptop_tap_order_no_list_error), Toast.LENGTH_LONG).show();
            return;
        }
        // viewの存在確認
        Cursor cursor = db.query("sqlite_master", null, "type='view' AND name='tap_order_list_view'", null, null, null,null);
        if(cursor == null || cursor.getCount() == 0) { // Viewが存在しない等の場合
            Toast.makeText(this, getString(R.string.maptop_tap_order_no_list_error), Toast.LENGTH_LONG).show();
            if(cursor != null) {
                cursor.close();
            }
            db.close();
            return;
        }
        cursor.close();
        db.close();
        
        // 開始地点のデータ作成
        long lon = 0;
        long lat = 0;
        String name = null;
        /*
        if (poiData == null) {
            lon = poiData.m_wLong;
            lat = poiData.m_wLat;
            name = poiData.m_sName;
        }*/
//        if (lon == 0 || lat == 0) {
            mapView.getMapCentrePos();
            lon = mapView.getCoordinate().getM_lLong();
            lat = mapView.getCoordinate().getM_lLat();
//        }
        
        if (name == null || "".equals(name)) {
            JNIString Address = new JNIString();
            NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(lon, lat, Address);

            name = Address.getM_StrAddressString();
        }
        
        Intent intent = new Intent(getIntent());
// mod by M.Suna 2013/10/19
//        intent.setClass(this, TapOrderInquiry.class);
        intent.setClass(this, VehicleInquiry.class);
        intent.putExtra(Constants.INTENT_LONGITUDE, lon);
        intent.putExtra(Constants.INTENT_LATITUDE, lat);
        intent.putExtra(Constants.POINT_NAME, name);
        NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_NOSAI_TAP_ORDER_LIST);
    }

// RouteEdit.javaからコピー
    private static final String CarMode = "0"; // 車モード
    private static final int NE_CarNaviSearchProperty_Commend = 1;// 推奨   画面位置index=0
	//protected static Button photoBtn = null;
//	public Button photoBtn = null;

// RouteEditより移植して拡張
/*    
    public void determineTapOrderRoute(int startIndex) {
        // 引数チェックとIndexを求める
        NaviApplication app = (NaviApplication)getApplication();
        final int listSize = app.getTapOrderListSize();
        if( startIndex >= listSize) { // 引数のインデックスチェック
            return;
        }
        int lastIndex = listSize -1;
        if( lastIndex > startIndex + 7) {
            lastIndex = startIndex + 7;
        }
        // Activity開始時にタップオーダー順リストが0であるケースは弾いているので、その前提
        JNIInt JIMode = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetNaviMode(JIMode);
        String strMode = String.valueOf((JIMode.getM_iCommon() - 1));
        final String strSearchProperty = "1"; // 初回は推奨ルートで
        JNITwoLong Coordinate = new JNITwoLong();
        Constants.FROM_ROUTE_CAL = true;
        JNIInt GuideState = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_Java_GetGuideState(GuideState);
        if (GuideState.getM_iCommon() != 0) {
            NaviRun.GetNaviRunObj().JNI_NE_StopGuidance();
        }
        for (int i = 0; i < Constants.NAVI_DEST_INDEX; i++) {
            int NE_RoutePointType_Start = 1;
            NaviRun.GetNaviRunObj().JNI_NE_DeleteRoutePoint(
                    NE_RoutePointType_Start + i);
        }
        // 目的地のindext
        int iRoutePointType_Goal = Constants.NAVI_DEST_INDEX;
        // 出発地のindext
        int iRoutePointType_Start = Constants.NAVI_START_INDEX;
        long Longitude = 0;
        long Latitude = 0;
        String PointName = null;
        int iPassed = 0;
        for (int i = Constants.NAVI_START_INDEX; i <= Constants.NAVI_DEST_INDEX; i++) {
            JNITwoLong positon = new JNITwoLong();
            JNIString JNIpositonName = new JNIString();
            JNIInt bPassed = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(i, positon,
                    JNIpositonName, bPassed);
        }

        String strLon = app.getTapOrderInfo(startIndex).getLon();
        String strLat = app.getTapOrderInfo(startIndex).getLat();
        String strName = app.getTapOrderInfo(startIndex).getName();
        
        iPassed = app.getTapOrderInfo(startIndex).getIpass();
        if (strName.contains(getResources().getString(R.string.route_start_point_name))) {
            NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);
            strLon = String.valueOf(Coordinate.getM_lLong());
            strLat = String.valueOf(Coordinate.getM_lLat());
        }
        // 開始地点
        NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(iRoutePointType_Start,
                Long.parseLong(strLon), Long.parseLong(strLat),
                strName, iPassed);

        // 目的地点
        Longitude = Long.parseLong(app.getTapOrderInfo(lastIndex).getLon());
        Latitude = Long.parseLong(app.getTapOrderInfo(lastIndex).getLat());
        PointName = app.getTapOrderInfo(lastIndex).getName();
        iPassed = app.getTapOrderInfo(lastIndex).getIpass();
        NaviRun.GetNaviRunObj()
                .JNI_NE_SetRoutePoint(iRoutePointType_Goal, Longitude,
                        Latitude, PointName, iPassed);
        // 経由地
        for (int i = startIndex + 1, j=0; i < lastIndex; i++, j++) {
            Longitude = Long.parseLong(app.getTapOrderInfo(i).getLon());
            Latitude = Long.parseLong(app.getTapOrderInfo(i).getLat());
            PointName = app.getTapOrderInfo(i).getName();
            iPassed = app.getTapOrderInfo(i).getIpass();
            NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(j + 3, Longitude,
                    Latitude, PointName, iPassed);
        }

        if (strMode.equals(CarMode)) {
            int temp = NE_CarNaviSearchProperty_Commend;
            NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(
                    iRoutePointType_Start, iRoutePointType_Goal,
                    temp);
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(
                    iRoutePointType_Goal, 1/*Integer.parseInt(strSearchProperty)///);
        }
        CommonLib.SEARCHP_ROPERTY = strSearchProperty;
        NaviRun.GetNaviRunObj().JNI_NE_SetNaviMode(
                Integer.parseInt(strMode) + 1);
        //*int NE_NEActType_RouteCalculate = 5;
        int NE_NETransType_NormalMap = 0;
        byte bScale = 0;
        long lLongitude = 0;
        long lLatitude = 0;
        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(
                NE_NEActType_RouteCalculate, NE_NETransType_NormalMap, bScale,
                lLongitude, lLatitude);
        NaviRun.GetNaviRunObj().JNI_CT_ExposeMap();
        NaviRun.GetNaviRunObj().JNI_ct_ExposeMap();
        CommonLib.setIsStartCalculateRoute(true);///
        app.setTapRouteStartPos(startIndex); // 開始位置の保存
        
        // RESULTCODE_ROUTE_DETERMINATIONから移植
        mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
//          }
        int NE_NEActType_RouteCalculate = 5;
        int NE_NETransType_NormalMap = 0;
        byte bScale = 0;
        long lLongitude = 0;
        long lLatitude = 0;
// MOD S 2013/09/18 y_matsumoto
//        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(
//                NE_NEActType_RouteCalculate, NE_NETransType_NormalMap, bScale,
//                lLongitude, lLatitude);
        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(
                NE_NEActType_RouteCalculate, NE_NETransType_NormalMap, (byte) 0, bScale,
                lLongitude, lLatitude, 0);
// MOD E 2013/09/18 y_matsumoto
        NaviRun.GetNaviRunObj().JNI_CT_ExposeMap();
        NaviRun.GetNaviRunObj().JNI_ct_ExposeMap();
        CommonLib.setIsStartCalculateRoute(true);
        showRouteSearchWaitingDialog();
        mapView.updateMapCentreName(null);
        CommonLib.setIsCalcFromRouteEdit(true); //if calculate route fail, return car position

    }
*/    

    // タップ順設定
    public void goTapOrderSetting() {
        mapView.getMapCentrePos();
        long lon = mapView.getCoordinate().getM_lLong();
        long lat = mapView.getCoordinate().getM_lLat();
        NaviApplication app = (NaviApplication)getApplication();
        // 地図ビューアの起動
        String clazz = "jp.co.cpnet.app.activity.MapActivity";
        Intent intent = new Intent(Intent.ACTION_MAIN);

        int idx = clazz.lastIndexOf('.');
        String pkg = clazz.substring(0, idx);

        intent.setClassName(pkg, clazz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        
        // パラメータ設定
        intent.putExtra("mode", 5);
        intent.putExtra("latitude", (int)lat);
        intent.putExtra("longitude", (int)lon);
        //intent.putExtra("largeDistrictID", app.getCurBigDistrictCode());
        //intent.putExtra("smallDistrictID", app.getCurSmallDistrictCode());
        
        startActivity(intent);
    }

// Add by CPJsunagawa '2015-06-07 Intent検証 Start
    // サンプルアプリ起動テスト
    public void showSampleAppl() {
        mapView.getMapCentrePos();
        long lon = mapView.getCoordinate().getM_lLong();
        long lat = mapView.getCoordinate().getM_lLat();
        NaviApplication app = (NaviApplication)getApplication();
        // 地図ビューアの起動
        String clazz = "com.example.android.skeletonapp";
        Intent intent = new Intent(Intent.ACTION_MAIN);

        int idx = clazz.lastIndexOf('.');
        String pkg = clazz.substring(0, idx);

        intent.setClassName(pkg, clazz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        
        // パラメータ設定
        intent.putExtra("latitude", (int)lat);
        intent.putExtra("longitude", (int)lon);
        //intent.putExtra("largeDistrictID", app.getCurBigDistrictCode());
        //intent.putExtra("smallDistrictID", app.getCurSmallDistrictCode());
        
        startActivity(intent);
    }
// Add by CPJsunagawa '2015-06-07 Intent検証 End

    // 住宅地図表示
    public void showResidentialMap() {
        mapView.getMapCentrePos();
        long lon = mapView.getCoordinate().getM_lLong();
        long lat = mapView.getCoordinate().getM_lLat();
        NaviApplication app = (NaviApplication)getApplication();
        // 地図ビューアの起動
        String clazz = "jp.co.cpnet.app.activity.MapActivity";
        Intent intent = new Intent();//(Intent.ACTION_MAIN);

        int idx = clazz.lastIndexOf('.');
        String pkg = clazz.substring(0, idx);

        intent.setClassName(pkg, clazz);
        // パラメータ設定
        intent.putExtra("mode", 0);
        intent.putExtra("latitude", (int)lat);
        intent.putExtra("longitude", (int)lon);
        //intent.putExtra("largeDistrictID", app.getCurBigDistrictCode());
        //intent.putExtra("smallDistrictID", app.getCurSmallDistrictCode());
        startActivityForResult(intent, AppInfo.ID_ACTIVITY_NOSAI_MAP_VIEWER);
//        startActivity(intent);
    }
// Add by CPJsunagawa '13-12-25  End


    @Override
    public void onUpdate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int iSystemHour = calendar.get(Calendar.HOUR_OF_DAY);
        int iSystemMinute = calendar.get(Calendar.MINUTE);
        iEstimate = iSystemHour * 60 + iSystemMinute;
        iEstimate += iRemainingTime;
        iEstimate %= 1440;
        if (MapView.getInstance().getMap_view_flag() == Constants.ROUTE_MAP_VIEW
                || MapView.getInstance().getMap_view_flag() == Constants.FIVE_ROUTE_MAP_VIEW) {
            showRouteMenu(iEstimate, destinationData);
        } else {
            //add liutch 0505 bug1082 -->
            // オンルート、オフルート状況取得
            JNIInt val = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_WG_IsOnRoute(val);
            boolean isOffRoute = false;
            if (val.getM_iCommon() == 0) {//オフルートである
                isOffRoute = true;
            } else if (val.getM_iCommon() == 1) {//オンルートである
                isOffRoute = false;
            }
            if ((MapView.getInstance().getMap_view_flag() == Constants.NAVI_MAP_VIEW ||
                    MapView.getInstance().getMap_view_flag() == Constants.SIMULATION_MAP_VIEW) && !isOffRoute) {
                showArrivalForecastInfo(iEstimate, destinationData);
            }
            //<--
        }
    }

    public void onRouteCalcFinished() {
        if (bIsRestoreRoute || bIsGuidanceAgain) {
            return;
        }

        if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_BICYCLE) {
            mapView.setMap_view_flag(Constants.ROUTE_MAP_VIEW_BIKE);
        } else {
            if (CommonLib.is5RouteMode()) {
                mapView.setMap_view_flag(Constants.FIVE_ROUTE_MAP_VIEW);
            } else {
                mapView.setMap_view_flag(Constants.ROUTE_MAP_VIEW);
            }
        }
    }

    /**
     * ルート探索を終了する
     */
    public void onRouteSearchFinish() {

        mapView.setMapScrolled(isMapScrolled());

        UpdateThread.addUpdateListener(this);

        if (mapView.isMapScrolled()) {
            NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);
            onLocusMapDraw(false);
        }

        initNaviView(true);

        try {
            if (bIsSearchDialogShow) {
                dismissDialog(DIALOG_ROUTE_SEARCH);
                bIsSearchDialogShow = false;
            }
// Add 2011/12/14 sawada Start -->
            goLocation();
// Add 2011/12/14 sawada End <--
        } catch (Exception e) {
        }
        if (CommonLib.isBIsRouteDisplay()) {

            if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_BICYCLE) {
                mapView.setMap_view_flag(Constants.ROUTE_MAP_VIEW_BIKE);
//Add 2012/04/10 Z01hirama Start --> #4223
                // navigationStart()内で設定されているフラグクリア実施.
                isManBackupRoute = false;
                isCarBackupRoute = false;
                NaviRun.GetNaviRunObj().JNI_ct_uic_DelBackupRoute();
//Add 2012/04/10 Z01hirama End <-- #4223
            } else {
                mapView.setMap_view_flag(Constants.NAVI_MAP_VIEW);
                navigationStart(0);
            }
            CommonLib.setBIsRouteDisplay(false);
        }

        if (bIsRestoreRoute) {
            mapView.setMap_view_flag(Constants.NAVI_MAP_VIEW);
            navigationStart(0);
            bIsRestoreRoute = false;
        }
        if (bIsGuidanceAgain) {
            if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_BICYCLE) {
                mapView.setMap_view_flag(Constants.ROUTE_MAP_VIEW_BIKE);
//Add 2012/04/10 Z01hirama Start --> #4223
                // navigationStart()内で設定されているフラグクリア実施.
                isManBackupRoute = false;
                isCarBackupRoute = false;
                NaviRun.GetNaviRunObj().JNI_ct_uic_DelBackupRoute();
//Add 2012/04/10 Z01hirama End <-- #4223
            } else {
                mapView.setMap_view_flag(Constants.NAVI_MAP_VIEW);
                navigationStart(1);
            }
            bIsGuidanceAgain = false;
        }
        CommonLib.setHasRoute(true);
        mapView.updateScale();
        mapView.hideMapCentreIcon();

        if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_BICYCLE) {
            CommonLib.setNaviStart(true);
//Chg 2011/09/26 Z01yoneya Start -->
//            CommonLib.createNaviDataFile(false);
//------------------------------------------------------------------------------------------------------------------------------------
            CommonLib.createNaviDataFile(false, ((NaviApplication)getApplication()).getNaviAppDataPath());
//Chg 2011/09/26 Z01yoneya End <--
            //draw route
            NaviRun.GetNaviRunObj().JNI_NE_MP_SetGuideLineIsDisplayed(1, 0);
// Add 2012/03/17 Z01_sawada(No.332) Start --> ref 2683
            mapView.setMapScrolled(true);
// Add 2012/03/17 Z01_sawada End <-- ref 2683
        }

//Del 2012/03/13 Z01_h_fukushima(No.405) Start --> ref 2510
//        // xuyang add start #1914
//        if (CommonLib.RouteEndNaviFlag) {
//            navigationStart(1);
//            CommonLib.RouteEndNaviFlag = false;
//        }
//        // xuyang add end #1914
//Del 2012/03/13 Z01_h_fukushima(No.405) End <-- ref 2510

//Add 2012/03/17 Z01_h_yamada(No.401) Start --> ref 3818
        synchronized (NaviRun.GetNaviRunObj()) {
	        CommonLib.bIsModeChanging = false;
        }
//Add 2012/03/17 Z01_h_yamada(No.401) End <-- ref 3818
    }

    public void showRouteMenu(int iArrivalTime, ZNUI_DESTINATION_DATA destData) {
//        if (null == destinationData) {
//            destinationData = new ZNUI_DESTINATION_DATA();
//        }
//        int naviMode = CommonLib.getNaviMode();
//        if(naviMode == Constants.NE_NAVIMODE_CAR){
//            NaviRun.GetNaviRunObj().JNI_Java_GetDestination(destinationData);
//        }else if(naviMode == Constants.NE_NAVIMODE_MAN){
//            NaviRun.GetNaviRunObj().JNI_WG_GetDestination(destinationData);
//        }
        int iHour = iArrivalTime / 60;
        int iMinute = iArrivalTime % 60;
        if (destData != null) {
            //ZNUI_TIME time = destData.getStTime();
            //if (time != null) {
            String dist = CommonLib.getFormatDistance(destData
                        .getLnDistance());
            String spendTime = CommonLib.getFormatTime(iHour, iMinute);
            String strLnToll = 0 + Constants.STRING_YEN;
            if (destinationData.getLnToll() > 0) {
                strLnToll = String.valueOf(destData.getLnToll()) + Constants.STRING_YEN;
            }

            // 歩行者の場合，料金表示は"- -"とする．
            if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN) {
                strLnToll = "- -";
            }
            m_oTopView.showRouteMenu(dist, strLnToll, spendTime);

//                if (CommonLib.is5RouteMode()) {
//                    mapView.setMap_view_flag(Constants.FIVE_ROUTE_MAP_VIEW);
//                }
            //}
        }
    }

    /**
     * Route calc。
     */
    public void calcRoute() {
// Del 2011/12/14 sawada Start ->
////Add 2011/11/10 Z01_h_yamada Start -->
//    	goLocation();
////Add 2011/11/10 Z01_h_yamada End <--
// Del 2011/12/14 sawada End <--
//Add 2011/08/03 Z01thedoanh (自由解像度対応) Start -->
        //画面回転を確認
        if (ScreenAdapter.getOrientation() != CommonLib.getOrientation()) {
            CommonLib.setOrientation(ScreenAdapter.getOrientation());
            mapView.setAreaShowCalcRoute(ScreenAdapter.isLandMode());
        }
//Add 2011/08/03 Z01thedoanh (自由解像度対応) End <--

        switch (CommonLib.getNaviMode()) {
            case Constants.NE_NAVIMODE_MAN:
//Chg 2011/09/26 Z01yoneya Start -->
//                if (CommonLib.isBIsExistsWalkerData()) {
//------------------------------------------------------------------------------------------------------------------------------------
                boolean isExistWalkData;
                try {
                    isExistWalkData = ((NaviApplication)getApplication()).getNaviAppDataPath().isExistWalkData();
                } catch (NullPointerException e) {
                    isExistWalkData = false;
                }
                if (isExistWalkData) {
//Chg 2011/09/26 Z01yoneya End <--
                    // XuYang add start #978
                    if (isHomeClickFlag) {
                        // XuYang add end #978
                        long routeDist = CommonLib.calcRouteDistance();
//Chg 2011/11/11 Z01_h_yamada Start -->
//                        if (routeDist < Constants.NAVI_HOME_WALK_BIKE_ROUTE_MIN_DIST) {
//                            showDialog(DIALOG_ROUTE_FAILURE);
//                        } else if (routeDist > Constants.MAX_WALK_ROUTE_DIST) {
//                            showDialog(DIALOG_WALK_ROUTE_TOO_LONG);
//                        } else {
//                            routeCalculate();
//                        }
//--------------------------------------------
                        if (routeDist > Constants.MAX_WALK_ROUTE_DIST) {
                            showDialog(DIALOG_WALK_ROUTE_TOO_LONG);
                        } else {
                            routeCalculate();
                        }
//Chg 2011/11/11 Z01_h_yamada End <--
                        // XuYang add start #978
                    } else {
                        routeCalculate();
                        isHomeClickFlag = true;
                    }
                    // XuYang add end #978
                } else {
                    this.showDialog(Constants.DIALOG_NO_WALKER_DATA);
                }
                break;
            case Constants.NE_NAVIMODE_CAR:
                routeCalculate();
                break;
            case Constants.NE_NAVIMODE_BICYCLE:
                routeCalculate();
                break;
            default:
                break;
        }
    }

 // Add 2012/03/17 Z01_sawada(No.334) Start --> ref 2697
    private String getMapCenterName() {
        JNITwoLong mapCentre = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(mapCentre);

        String msPointString = mapView.getMapCentreName();

        long lon = mapCentre.getM_lLong();
        long lat = mapCentre.getM_lLat();
        JNIString Address = new JNIString();

        if (null == msPointString
                || Constants.MAP_POINT_NAME.equals(msPointString)) {
            NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(lon, lat, Address);
            msPointString = Address.getM_StrAddressString();
            if (null == msPointString || "".equals(msPointString.trim())) {
                msPointString = getResources().getString(R.string.name_null);
            }
        }
// ADD -- 2012/04/11 -- Z01H.Fukushima --- Start ----- refs 4252
        // 名称に格納できるのは20文字まで
        if( msPointString.length()>=POINTNAME_STRLEN){
        	return  msPointString.substring(0, POINTNAME_STRLEN-1);
        }
// ADD -- 2012/04/11 -- Z01H.Fukushima --- End ------ refs 4252

        return msPointString;
    }
// Add 2012/03/17 Z01_sawada(No.334) End <-- ref 2697

    /**
     * ルート探索
     */
    private void routeCalculate() {

        CommonLib.setIsStartCalculateRoute(true);
        // XuYang add start #1029
        if (CommonLib.getNaviMode() != Constants.NE_NAVIMODE_BICYCLE) {
            // XuYang add end #1029
            showRouteSearchWaitingDialog();
            // XuYang add start #1029
        }
        // XuYang add end #1029

// Chg 2012/03/17 Z01_sawada(No.334) Start --> ref 2697
//      NaviRun.GetNaviRunObj().JNI_ct_uic_RouteCalculate();
//      setRouteDestPoint();
        NaviRun.GetNaviRunObj().JNI_ct_uic_RouteCalculate(-1, getMapCenterName());
// Chg 2012/03/17 Z01_sawada(No.334) End <-- ref 2697
//        NaviRun.GetNaviRunObj().JNI_CT_Navi_StartCalculate(Constants.DEF_ROUTE_CALCULATE_TYPE_MAIN);

        //CommonLib.setBIsRouteCalculating(true);
        CommonLib.setBIsRouteCancel(false);

// Del 2012/03/17 Z01_sawada(No.334) Start -> ref 2697
//      JNITwoLong coordinate = mapView.getCoordinate();
//      NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(coordinate);
//      NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
//              coordinate.getM_lLong(), coordinate.getM_lLat(),
//              mapView.getAddressString());
// Del 2012/03/17 Z01_sawada(No.334) End <-- ref 2697

    }

    /**
     * アロー終了
     */
    public void clearBicycleNaviData() {

//Chg 2011/09/26 Z01yoneya Start -->
//        CommonLib.deleteNaviDataFile();
//------------------------------------------------------------------------------------------------------------------------------------
        CommonLib.deleteNaviDataFile(((NaviApplication)getApplication()).getNaviAppDataPath());
//Chg 2011/09/26 Z01yoneya End <--

        guideEnd(true);
        mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);

        // XuYang add start #1029
        if (mapView.isMapScrolled()) {
            OpenMap.locationBack(this);
        }
        // XuYang add end #1029
    }

//    public void saveBicycleData(JNITwoLong goalInfo) {
//
//        int iRoutePointType_Goal = Constants.NAVI_DEST_INDEX;
//        int iRoutePointType_Start = Constants.NAVI_START_INDEX;
//        JNIString Address = new JNIString();
//
//        //Start
//        ZNE_RoutePoint startPoint = new ZNE_RoutePoint();
//        JNITwoLong carPos = new JNITwoLong();
//        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(carPos);
//        startPoint.setStPos(carPos);
//
//        if (carPos.getM_lLat() == 0 || carPos.getM_lLong() == 0) {
//            NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
//                    startPoint.getStPos().getM_lLong(),
//                    startPoint.getStPos().getM_lLat(), Address);
//            NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(iRoutePointType_Start,
//                    startPoint.getStPos().getM_lLong(),
//                    startPoint.getStPos().getM_lLat(),
//                    Address.getM_StrAddressString(), startPoint.getIPassed());
//        } else {
//            NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
//                    carPos.getM_lLong(),
//                    carPos.getM_lLat(), Address);
//            NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(iRoutePointType_Start,
//                    carPos.getM_lLong(),
//                    carPos.getM_lLat(),
//                    Address.getM_StrAddressString(), 0);
//        }
//
//        // Goal
//        ZNE_RoutePoint goalPoint = new ZNE_RoutePoint();
//        goalPoint.setStPos(goalInfo);
//        NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
//                goalPoint.getStPos().getM_lLong(),
//                goalPoint.getStPos().getM_lLat(), Address);
//
//        NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(iRoutePointType_Goal,
//                goalPoint.getStPos().getM_lLong(),
//                goalPoint.getStPos().getM_lLat(),
//                Address.getM_StrAddressString(), goalPoint.getIPassed());
//
//    }

    /**
     * 案内開始
     */
    private void onStartNavi(long lIsStartVoice) {

// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
		DebugLogOutput.put("RakuRaku-Log: 案内開始（onStartNavi）");
// Add 2015-05-28 CPJsunagawa SDカードにログ出力 end

    	saveHistoryInfo();

        mapView.onNaviStart();

        NaviRun.GetNaviRunObj().JNI_ct_uic_StartGuide(lIsStartVoice);
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
//Add 2011/12/07 Z01thedoanh Start -->
        if(iEstimate == 0){
        	onUpdate();
        }
//Add 2011/12/07 Z01thedoanh End <--
        showArrivalForecastInfo(iEstimate, destinationData);
        if (CommonLib.Start_Demo_Guide_FLAG != 1) {
//Chg 2011/09/26 Z01yoneya Start -->
//            CommonLib.createNaviDataFile(false);
//------------------------------------------------------------------------------------------------------------------------------------
            CommonLib.createNaviDataFile(false, ((NaviApplication)getApplication()).getNaviAppDataPath());
//Chg 2011/09/26 Z01yoneya End <--
        }
        CommonLib.setNaviStart(true);

        onMapSizeChanged();

    }

    /**
     * 案内開始(Intent)
     */
    public void navigationStart(long lIsStartVoice) {

// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
		DebugLogOutput.put("RakuRaku-Log: 案内開始（navigationStart）");
// Add 2015-05-28 CPJsunagawa SDカードにログ出力 end

    	mapView.updateScale();

        NaviRun.GetNaviRunObj().setSearchKind(Constants.SCROLL_MAP_KIND);

        CommonLib.setBIsIntentNavi(false);

        intentNavigation();

// Add by CPJsunagawa '13-12-25 Start
    	JNITwoLong positon = new JNITwoLong();
        JNIString JNIpositonName = new JNIString();
        JNIInt bPassed = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(Constants.NAVI_DEST_INDEX,
            positon, JNIpositonName, bPassed);
        NaviApplication app = (NaviApplication)getApplication();
        app.setCloseUpPoint(positon.getM_lLat(), positon.getM_lLong());
// Add by CPJsunagawa '13-12-25 End

        mapView.setMap_view_flag(Constants.NAVI_MAP_VIEW);
        onStartNavi(lIsStartVoice);

        //980 add start
        if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN) {
            isManBackupRoute = true;
            isCarBackupRoute = false;
        }
        if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_CAR) {
            isManBackupRoute = false;
            isCarBackupRoute = true;
        }
        NaviRun.GetNaviRunObj().JNI_NE_BackupMainRoute();

// Add by CPJsunagawa '13-12-25 Start
    	JNITwoLong Coordinate = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_CT_Drv_GetVehiclePosition(Coordinate);
        //NaviApplication app = (NaviApplication)getApplication();
        double dist = CommonInquiryOps.getDistanceFromAngleDiff(
        		Coordinate.getM_lLat(), Coordinate.getM_lLong(), 
        		app.getCloseUpLatitude(), app.getCloseUpLongitude());
    	ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
    	//boolean isAlreadyClose = (dist < 100.0);
    	boolean isAlreadyClose = (dist < Constants.CLOSE_UP_DISTANCE);
    	try
    	{
    		oNaviApi.setZoom(isAlreadyClose ? ZoomLevel.M_10 : ZoomLevel.M_20);
    		if (isAlreadyClose)
    		{
    			showList(true, false, false);
    		}
    	}
    	catch (NaviException e)
    	{
    		e.printStackTrace();
    	}
    	
    	if (mapView.isMapScrolled()) {
            OpenMap.locationBack(this);
            goLocation();
        }  
//        vicsManager.onNaviStart();
// Add by CPJsunagawa '13-12-25 End

//        vicsManager.onNaviStart();
        //980 add end

// Add by CPJsunagawa '13-12-25 Start
        setDeliveryInfoToTop();
// Add by CPJsunagawa '13-12-25 End
    }

    /**
     * デモ開始
     */
    public void startDemoGuide() {
        CommonLib.Start_Demo_Guide_FLAG = 1;

        onStartNavi(1);
        NaviRun.GetNaviRunObj().JNI_DG_startDemoDrive(1);
        onNaviDemoStart();
        mapView.updateScale();
        mapView.setMap_view_flag(Constants.SIMULATION_MAP_VIEW);
        if (guide_Info != null) {
            guide_Info.onStartDemoGuide(iEstimate, destinationData);
        }
        this.showSimulationMap();

// Add by CPJsunagawa '2015-05-13 Start
    	setDeliveryInfoToTop();
// Add by CPJsunagawa '2015-05-13 End
    }

    /**
     * デモ開始。ルートシミュレーションを開始する．
     */
    public void onNaviDemoStart() {

        mapView.onNaviStart();

        activeType = demoGuideActiveType;
        CommonLib.setBIsDemo(true);
        this.showSimulationMap();
        showArrivalForecastInfo(iEstimate, destinationData);

    }

    /**
     * シミュレーションの地図を表示する
     */
    public void showSimulationMap() {
        if (activeType == simulationActiveType) {
            setHighWayScale();
            NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(activeType,
                    transType, (byte)0xFF,1, 0, 0,0);
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(activeType,
                    transType, (byte)0xFF,1, mapView.getLongitude(),
                    mapView.getLatitude(),0);
        }
    }

    /**
     *
     */
    public void setHighWayScale() {
        mapView.updateScale();
    }

    /**
     * ルート履歴の保存
     */
    public void saveHistoryInfo() {
        for (int i = 0; i <= 5; i++) {
            if (null == positon) {
                positon = new JNITwoLong[6];
                for (int j = 0; j <= 5; j++) {
                    positon[j] = new JNITwoLong();
                }
            }

            if (null == JNIpositonName) {
                JNIpositonName = new JNIString[6];
                for (int j = 0; j <= 5; j++) {
                    JNIpositonName[j] = new JNIString();
                }
            }

            if (null == bPassed) {
                bPassed = new JNIInt[6];
                for (int j = 0; j <= 5; j++) {
                    bPassed[j] = new JNIInt();
                }
            }

            NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(i + 3, positon[i],
                    JNIpositonName[i], bPassed[i]);

            if (null == UserFile_RecordIF) {
                UserFile_RecordIF = new POI_UserFile_RecordIF[6];
                for (int j = 0; j <= 5; j++) {
                    UserFile_RecordIF[j] = new POI_UserFile_RecordIF();
                }
            }

            if (null == Data) {
                Data = new POI_UserFile_DataIF[6];
                for (int j = 0; j <= 5; j++) {
                    Data[j] = new POI_UserFile_DataIF();
                }
            }

            UserFile_RecordIF[i].m_lLon = positon[i].getM_lLong();
            UserFile_RecordIF[i].m_lLat = positon[i].getM_lLat();
            if (UserFile_RecordIF[i].m_lLon != 0
                    && UserFile_RecordIF[i].m_lLat != 0) {
                String str = JNIpositonName[i].getM_StrAddressString();
                String msPointString = mapView.getMapCentreName();
                if ("".equals(str)) {
                    if (msPointString == null || Constants.MAP_POINT_NAME.equals(msPointString)) {

                        NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
                                UserFile_RecordIF[i].m_lLon,
                                UserFile_RecordIF[i].m_lLat,
                                mapView.getAddressString());

                        StringBuffer strbuffer = new StringBuffer(mapView
                                .getAddressString().getM_StrAddressString());
                        for (int j = 0; j < 2; j++) {
                            int cur = strbuffer.indexOf(" ");
                            if (cur >= 0) strbuffer.deleteCharAt(cur);
                        }

                        str = strbuffer.toString();
                    } else {
                        str = msPointString;
                    }
                }
                UserFile_RecordIF[i].m_DispName = str;
                UserFile_RecordIF[i].m_lDate = 0;
                UserFile_RecordIF[i].m_lIconCode = 1;
                UserFile_RecordIF[i].m_lUserFlag = 0;
                UserFile_RecordIF[i].m_lImport = 0;
                Data[i].m_Data = str;
                Data[i].m_sDataIndex = 1;
                Data[i].m_sDataSize = (short)(str.length());

                JNILong RecCount = new JNILong();
                POI_History_FilePath path = new POI_History_FilePath();
                NaviRun.GetNaviRunObj().JNI_NE_POIHist_Clear(path);

                POI_UserFile_RecordIF[] m_Poi_UserFile_Listitem = new POI_UserFile_RecordIF[Constants.MAX_OF_HISTORY_LIST];
                for (int j = 0; j < Constants.MAX_OF_HISTORY_LIST; j++) {
                    m_Poi_UserFile_Listitem[j] = new POI_UserFile_RecordIF();
                }
                NaviRun.GetNaviRunObj().JNI_NE_POIHist_GetRecList(0,
                        Constants.MAX_OF_HISTORY_LIST, m_Poi_UserFile_Listitem,
                        RecCount);
                // ///////////////
                // NaviLog.d(NaviLog.PRINT_LOG_TAG,"old RecCount.lcount==" +
                // RecCount.lcount);
                // //////////////////
                if (RecCount.lcount >= Constants.MAX_OF_HISTORY_LIST) {
                    NaviRun.GetNaviRunObj().JNI_NE_POIHist_RemoveRec(
                            RecCount.lcount - 1);
                }
                NaviRun.GetNaviRunObj().JNI_NE_POIHist_SetRec(
                        UserFile_RecordIF[i], 1, Data[i]);
                // ///////////////
                // NaviRun.GetNaviRunObj().JNI_NE_POIHist_GetRecList(0,
                // Constants.MAX_OF_HISTORY_LIST,
                // m_Poi_UserFile_Listitem, RecCount);
                // NaviLog.d(NaviLog.PRINT_LOG_TAG,"new RecCount.lcount==" +
                // RecCount.lcount);
                // //////////////////
                // NaviLog.d(NaviLog.PRINT_LOG_TAG,"saveHistoryInfo end");

            }
        }

    }

    /**
     * ナビブロードバンド
     */
    public void intentNavigation() {
//        JNITwoLong myPosi = new JNITwoLong();
//        Location vehichelLocation = new Location(LocationManager.GPS_PROVIDER);
//
//        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(myPosi);
//        if (myPosi != null) {
//            vehichelLocation = CommonLib.ConvertDBCoordinateSystemToWGS(myPosi);
//        }
//
//        Intent intentBroadCast = new Intent();
//        Bundle mBundle = new Bundle();
//        if (isNavigationStart) {
//            mBundle.putInt(Constants.INTENT_ROUTEGUIDE_TYPE_EXTRA,
//                    Constants.START_NAVIGATION_INTENT_BROADCAST);
//            isNavigationStart = false;
//        } else if (isNavigationEnd) {
//            mBundle.putInt(Constants.INTENT_ROUTEGUIDE_TYPE_EXTRA,
//                    Constants.END_NAVIGATION_INTENT_BROADCAST);
//            isNavigationEnd = false;
//        } else if (isNavigationSuspend) {
//            mBundle.putInt(Constants.INTENT_ROUTEGUIDE_TYPE_EXTRA,
//                    Constants.SUSPEND_NAVIGATION_INTENT_BROADCAST);
//        } else if (isNavigationResumed) {
//            mBundle.putInt(Constants.INTENT_ROUTEGUIDE_TYPE_EXTRA,
//                    Constants.RESUME_NAVIGATION_INTENT_BROADCAST);
//
//        }
//
//        intentBroadCast.setAction(Constants.ROUTEGUIDE_INTENT_ACTION_EXTRA);
//        mBundle.putParcelable(Constants.INTENT_VEHICLEINFO_EXTRA,
//                vehichelLocation);
//        intentBroadCast.putExtras(mBundle);
//        sendBroadcast(intentBroadCast);
//
//        return;
    }

    /**
     * 「探索失敗」ダイアログを表示する
     */
    public void showSearchRouteFailureDialog() {
        if (bIsSearchDialogShow) {
            dismissDialog(DIALOG_ROUTE_SEARCH);
            bIsSearchDialogShow = false;
        }
//Add 2012/04/10 Z01hirama Start --> #4223
        if(MapView.getInstance().getMap_view_flag() == Constants.ROUTE_MAP_VIEW_BIKE) {
        	mDoEndBicycleNavi = true;
        }
//Add 2012/04/10 Z01hirama End <-- #4223
        showDialog(DIALOG_ROUTE_FAILURE);
        CommonLib.set5RouteMode(false);
    }

    /**
     * Set dest info.
     */
    private void setRouteDestPoint() {
        /*
         * int iRoutePointType_Goal = Constants.NAVI_DEST_INDEX;
         *
         * JNITwoLong destPoint = new JNITwoLong(); JNIString Address = new
         * JNIString(); JNIInt bPassed = new JNIInt();
         * NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(iRoutePointType_Goal,
         * destPoint, Address, bPassed);
         *
         *
         * JNITwoLong mapCentre = new JNITwoLong();
         * NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(mapCentre);
         *
         * long lon = destPoint.getM_lLong(); long lat = destPoint.getM_lLat();
         * String addressStr = Address.getM_StrAddressString(); int passFlag =
         * bPassed.getM_iCommon(); if (lon == 0 || lat == 0) { lon =
         * mapCentre.getM_lLong(); lat = mapCentre.getM_lLat(); passFlag =
         * bPassed.getM_iCommon(); if(addressStr == null ||
         * "".equals(addressStr)){
         *
         * addressStr = mapView.getMapCentreName(); if(addressStr == null ||
         * Constants.MAP_POINT_NAME.equals(addressStr)){
         * NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(lon, lat, Address);
         * addressStr = Address.getM_StrAddressString(); } } }
         */

        JNITwoLong mapCentre = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(mapCentre);

        String msPointString = mapView.getMapCentreName();
        long lon = mapCentre.getM_lLong();
        long lat = mapCentre.getM_lLat();
        JNIString Address = new JNIString();

        if (null == msPointString
                || Constants.MAP_POINT_NAME.equals(msPointString)) {
            NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(lon, lat, Address);
            msPointString = Address.getM_StrAddressString();
            if (null == msPointString || "".equals(msPointString.trim())) {
                msPointString = getResources().getString(R.string.name_null);
            }
        }
        NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(Constants.NAVI_DEST_INDEX,
                lon, lat, msPointString, 0);
    }

    /**
     * Init Route info.
     */
    private void initRoute(ZNE_RoutePoint[] guidePoint) {
        int iRoutePointType_Goal = Constants.NAVI_DEST_INDEX;
        int iRoutePointType_Start = Constants.NAVI_START_INDEX;
        JNIString Address = new JNIString();
        String msPointString = new String();
        // Start
        ZNE_RoutePoint startPoint = guidePoint[iRoutePointType_Start];
        JNITwoLong carPos = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(carPos);
        if (carPos.getM_lLat() == 0 || carPos.getM_lLong() == 0) {
            NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
                    startPoint.getStPos().getM_lLong(),
                    startPoint.getStPos().getM_lLat(), Address);
            msPointString = Address.getM_StrAddressString();
            if (null == msPointString || "".equals(msPointString.trim())) {
                msPointString = getResources().getString(R.string.name_null);
            }
            NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(iRoutePointType_Start,
                    startPoint.getStPos().getM_lLong(),
                    startPoint.getStPos().getM_lLat(),
                    msPointString, startPoint.getIPassed());
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
                    carPos.getM_lLong(), carPos.getM_lLat(), Address);
            msPointString = Address.getM_StrAddressString();
            if (null == msPointString || "".equals(msPointString.trim())) {
                msPointString = getResources().getString(R.string.name_null);
            }
            NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(iRoutePointType_Start,
                    carPos.getM_lLong(), carPos.getM_lLat(),
                    msPointString, 0);
        }

        // 経由地
        int viaStart = iRoutePointType_Start + 2;
        int viaCount = 5;
        JNIString[] passedName = new JNIString[viaCount];
        for (int i = viaStart; i < viaStart + viaCount; i++) {
            if (guidePoint[i] == null) {
                continue;
            }
            if (guidePoint[i].getStPos().getM_lLong() == 0
                    || guidePoint[i].getStPos().getM_lLat() == 0) {
                continue;
            }
            passedName[i - viaStart] = new JNIString();
            NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
                    guidePoint[i].getStPos().getM_lLong(),
                    guidePoint[i].getStPos().getM_lLat(),
                    passedName[i - viaStart]);

            msPointString = guidePoint[i].getSzPointName();
            if (null == msPointString || "".equals(msPointString.trim())) {

                msPointString = passedName[i - viaStart].getM_StrAddressString();
                if (null == msPointString || "".equals(msPointString.trim())) {
                    msPointString = getResources().getString(R.string.name_null);
                }

            }

            NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(i,
                    guidePoint[i].getStPos().getM_lLong(),
                    guidePoint[i].getStPos().getM_lLat(),
                    msPointString,
                    guidePoint[i].getIPassed());
        }

        // Goal
        ZNE_RoutePoint goalPoint = guidePoint[iRoutePointType_Goal];
        NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
                goalPoint.getStPos().getM_lLong(),
                goalPoint.getStPos().getM_lLat(), Address);

        msPointString = goalPoint.getSzPointName();
        if (null == msPointString || "".equals(msPointString.trim())) {

            msPointString = Address.getM_StrAddressString();
            if (null == msPointString || "".equals(msPointString.trim())) {
                msPointString = getResources().getString(R.string.name_null);
            }

        }

        NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(iRoutePointType_Goal,
                goalPoint.getStPos().getM_lLong(),
                goalPoint.getStPos().getM_lLat(),
                msPointString, goalPoint.getIPassed());
    }

//Chg 2011/09/30 Z01yoneya Start -->
//    /**
//     * ナビの再開
//     */
//    private void initLastNaviInfo() {
//        BufferedReader bf = CommonLib.getBf();
//        String naviType = null;
//        String searchType = null;
//
//        try {
//            naviType = bf.readLine();
//            if (naviType != null && naviType.startsWith("#")) {
//                naviType = bf.readLine();
//            }
//            searchType = bf.readLine();
//            if (searchType != null && searchType.startsWith("#")) {
//                searchType = bf.readLine();
//            }
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }
//        final int iCarManMode = Integer.valueOf(naviType);
//        int iSearchType = Integer.valueOf(searchType);
//
//        NaviRun.GetNaviRunObj().JNI_NE_SetNaviMode(iCarManMode);
//
////        int iRoutePointType_Goal = 8;
////        int iRoutePointType_Start = 1;
//
//        if (Constants.NE_NAVIMODE_CAR == iCarManMode) {
//            NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(
//                    Constants.NAVI_START_INDEX, Constants.NAVI_DEST_INDEX, iSearchType);
//        } else if (Constants.NE_NAVIMODE_MAN == iCarManMode) {
//            NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(
//                    Constants.NAVI_DEST_INDEX, iSearchType);
//        }
//
//        ZNE_GuideRoutePoint RouteInfo = new ZNE_GuideRoutePoint();
//        NaviRun.GetNaviRunObj().JNI_Java_GetGuideRoutePoint(RouteInfo);
//
//        ZNE_RoutePoint[] guidePoint = RouteInfo.getStGuidePoint();
//        // if(guidePoint == null || guidePoint.length == 0){
//        // return;
//        // }
//        if (guidePoint == null) {
//            guidePoint = new ZNE_RoutePoint[9];
//        }
//
//        int size = guidePoint.length;
//        JNITwoLong stPos = null;
//        String strPointData = null;
//        String[] strNaviData = null;
//        for (int i = 0; i < size; i++) {
//
//            try {
//                strPointData = bf.readLine();
//                if (strPointData != null && strPointData.startsWith("#")) {
//                    strPointData = bf.readLine();
//                }
//                // // //NaviLog.d(NaviLog.PRINT_LOG_TAG,"naviStart: " + i +
//                // "["+strPointData+"]");
//            } catch (IOException e) {
//                e.printStackTrace();
//                continue;
//            }
//
//            if (strPointData == null || strPointData.length() == 0
//                    || strPointData.equalsIgnoreCase("NULL")) {
//                guidePoint[i] = null;
//            } else {
//                strNaviData = strPointData.split(",");
//                if (null != strNaviData) {
//
//                    guidePoint[i] = new ZNE_RoutePoint();
//                    stPos = new JNITwoLong();
//
//                    stPos.setM_lLong(Long.valueOf(strNaviData[0]));
//                    stPos.setM_lLat(Long.valueOf(strNaviData[1]));
//
//                    if (i > Constants.NAVI_START_INDEX) {
//                        guidePoint[i].setSzPointName(strNaviData[2]);
//                    }
//                    if (i > Constants.NAVI_START_INDEX + 1 && i < Constants.NAVI_DEST_INDEX) {
//                        int iPass = Integer.valueOf(strNaviData[3]);
//                        /*
//                        if (iPass == 1) {
//                            guidePoint[i] = null;
//                            continue;
//                        }
//                        */
//                        guidePoint[i].setIPassed(iPass);
//                        // guidePoint[i].setIPointType(Integer.valueOf(strNaviData[4]));
//                    }
//
//                    guidePoint[i].setStPos(stPos);
//                }
//            }
//        }
//
//        for (int j = 0; j < Constants.NAVI_DEST_INDEX; j++) {
//            NaviRun.GetNaviRunObj().JNI_NE_DeleteRoutePoint(Constants.NAVI_START_INDEX + j);
//        }
//
//        initRoute(guidePoint);
//    }
//------------------------------------------------------------------------------------------------------------------------------------
    /**
     * ナビの再開
     */
    private void initLastNaviInfo() {
        NaviGuideRestoreFile naviGuideRestoreFile = CommonLib.getNaviGuideRestoreFile();
        if (!naviGuideRestoreFile.isFileExist()) {
            return;
        }

        final int iCarManMode = naviGuideRestoreFile.getCarManMode();
        int iSearchType = naviGuideRestoreFile.getSearchType();

        NaviRun.GetNaviRunObj().JNI_NE_SetNaviMode(iCarManMode);

        if (Constants.NE_NAVIMODE_CAR == iCarManMode) {
            NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(Constants.NAVI_START_INDEX, Constants.NAVI_DEST_INDEX, iSearchType);
        } else if (Constants.NE_NAVIMODE_MAN == iCarManMode) {
            NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(Constants.NAVI_DEST_INDEX, iSearchType);
        }

        ZNE_GuideRoutePoint RouteInfo = new ZNE_GuideRoutePoint();
        NaviRun.GetNaviRunObj().JNI_Java_GetGuideRoutePoint(RouteInfo);

        ZNE_RoutePoint[] guidePoint = naviGuideRestoreFile.getRoutePoint();

        for (int j = 0; j < Constants.NAVI_DEST_INDEX; j++) {
            NaviRun.GetNaviRunObj().JNI_NE_DeleteRoutePoint(Constants.NAVI_START_INDEX + j);
        }

        initRoute(guidePoint);
    }

//Chg 2011/09/30 Z01yoneya End <--

    /**
     * 取り消す。ル ートを破棄する。
     */
    public void onClickRouteCancel() {
        showDialog(DIALOG_NAVI_CANCEL);
    }

    /**
     * 交差点拡大図の表示・非表示を設定します。
     *
     * @param visible
     */
    public void setJunctionVisible(boolean visible) {
        if (mapView != null) {
        	mapView.setJunctionVisible(visible);
        }
    }
    /**
     * 5ルート探索
     */
    public void navi5Route() {
        CommonLib.setHasRoute(false);
        CommonLib.set5RouteMode(true);
        CommonLib.setIsStartCalculateRoute(true);
        showRouteSearchWaitingDialog();

        NaviRun.GetNaviRunObj().JNI_ct_uic_5RouteCalculate();
//        CommonLib.setBIsRouteCalculating(true);
        CommonLib.setBIsRouteCancel(false);
//        JNITwoLong coordinate = mapView.getCoordinate();
//        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(coordinate);
//        NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
//                coordinate.getM_lLong(), coordinate.getM_lLat(),
//                mapView.getAddressString());
    }

    //
    /**
     * お気に入り
     */
    public void goFavourite() {
        // yangyang add start
        // if (poi_Type == AppInfo.POI_LOCAL)
        // {
        // yangyang add start

        //自宅以外は最大500件
        JNILong RecCount = new JNILong();
        NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetGenreCount(RecCount);
        int len = (int)RecCount.lcount;
        int allCount = 0;
        int myHomeCount = 0;
        for (int i = 0; i < len; i++) {
            POI_Mybox_ListGenreCount Count = new POI_Mybox_ListGenreCount();
            NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecCount(i, Count);
//          NaviLog.d(NaviLog.PRINT_LOG_TAG,"Count.getM_lGenreCode()=====" + Count.getM_lGenreCode());
            if (Count.getM_lGenreCode() != 8 && Count.getM_lGenreCode() != 9) {
                allCount += Count.getM_sRecordCount();
            } else {
                myHomeCount += Count.getM_sRecordCount();
            }
        }
//      NaviLog.d(NaviLog.PRINT_LOG_TAG,"allCount========" + allCount);
//        if (allCount + myHomeCount < Constants.MAX_RECORD_COUNT + 2) {
        Intent intent = new Intent();
//Del 2011/09/17 Z01_h_yamada Start -->
//            intent.putExtra(Constants.PARAMS_TITLE, R.drawable.menu_favorites_load);
//Del 2011/09/17 Z01_h_yamada End <--

        intent.putExtra(Constants.PARAMS_RECORD_COUNT, allCount);
        intent.putExtra(Constants.PARAMS_MYHOME_RECORD_COUNT, myHomeCount);
        intent.putExtra(AppInfo.FLAG_POI_TYPE, poi_Type);
        intent.putExtra(AppInfo.POI_DATA, poiData);
        intent.setClass(this, FavoritesAddGenreInquiry.class);
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(intent, AppInfo.ID_ACTIVITY_FAVORITESADDGENREINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_FAVORITESADDGENREINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
//        } else {
//            showDialog(Constants.DIALOG_MAX_FAVORITES);
//        }

    }

    /**
     * ルート編集
     */
    public void goRouteEdit() {
        Intent intent = new Intent();
        intent.setClass(this, RouteEdit.class);
        intent.putExtra(Constants.ROUTE_TYPE_KEY, Constants.ROUTE_TYPE_NOW);
        onGoRoute(intent);
//Chg 2011/09/23 Z01yoneya Start -->
//        this.startActivityForResult(intent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
//Chg 2011/09/23 Z01yoneya End <--
    }

    private void onGoRoute(Intent intent) {
        String msPointString = mapView.getMapCentreName();
        if (null == msPointString || Constants.MAP_POINT_NAME.equals(msPointString)) {
            intent.putExtra(Constants.AddressString, Constants.NotFromSearch);
        } else {
            intent.putExtra(Constants.AddressString, msPointString);
        }
    }

//    private String getMapPointName(){
//        String msPointString = mapView.getMapCentreName();
//        if (null == msPointString || Constants.MAP_POINT_NAME.equals(msPointString)) {
//            return null;
//        } else {
//            return msPointString;
//        }
//    }

    /**
     * ルート編集メニューを表示する
     */
    public void goRouteMain() {
        Intent intent = new Intent();
        intent.setClass(this, RouteEditMain.class);
        onGoRoute(intent);
//Chg 2011/09/23 Z01yoneya Start -->
//        this.startActivityForResult(intent, AppInfo.ID_ACTIVITY_ROUTE);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_ROUTE);
//Chg 2011/09/23 Z01yoneya End <--
    }

    /**
     *
     */
    public void goBack() {
        // XuYang add start #1056
        CommonLib.setChangePos(false);
        CommonLib.setChangePosFromSearch(false);
// Add by CPJsunagawa '13-12-25 Start
        if (((NaviApplication)getApplication()).IsManualMatching())
        {
        	showList(true, false, false);
        	((NaviApplication)getApplication()).setRetryShowList();
        }
        ((NaviApplication)getApplication()).setIsManualMatching(false);
// Add by CPJsunagawa '13-12-25 End
    	// XuYang add end #1056
        setResult(Activity.RESULT_CANCELED/*, getIntent()*/);
        finish();
        // xuyang add start #1782
        CommonLib.backMapFlag = true;
        // xuyang add end #1782
        mapView.hideCurPosName();
        mapView.setMap_view_flag(preMapViewType);
    }

    public void goBackRouteEdit() {

        long lon = 0;
        long lat = 0;
        String name = null;
// Add by CPJsunagawa '13-12-25 Start
    	int custID = -1, index = -1;
        boolean isNoChangeCoord = false;
// Add by CPJsunagawa '13-12-25 End

        if (poiData != null) {
            lon = poiData.m_wLong;
            lat = poiData.m_wLat;
            name = poiData.m_sName;
        }

        if (lon == 0 || lat == 0) {
            mapView.getMapCentrePos();
            lon = mapView.getCoordinate().getM_lLong();
            lat = mapView.getCoordinate().getM_lLat();
        }

// Add by CPJsunagawa '13-12-25 Start
        if (sakuraPoiData != null)
        {
        	name = sakuraPoiData.m_sName;
        	custID = sakuraPoiData.m_CustID;
        	index = sakuraPoiData.m_MyIndex;

        	NaviApplication app = (NaviApplication)getApplication();

        	// 座標とマッチングレベルを更新
        	DeliveryInfo di = app.getInfo(index);
        	
        	isNoChangeCoord = (
        			di.mPoint.getLongitudeMs() == lon &&
        			di.mPoint.getLatitudeMs() == lat
        	);
        	
        	di.mPoint.setLongitudeMs(lon);
        	di.mPoint.setLatitudeMs(lat);
        	di.mMatchLebel = 10;
        	
        	// アイコンも更新
        	DrawUserLayer myLayer = null;
        	if (di.mMapIcon != null)
        	{
	        	myLayer = di.mMapIcon.getParentLayer();
	        	NaviRun.GetNaviRunObj().JNI_NE_DeleteUserFigure(myLayer.getNELayerId(), di.mMapIcon.getNEID());
        	}
        	di.reflectToIcon();
        	if (myLayer == null) 
        	{
        		myLayer = app.getDrawUserLayerManager().getUserLayer(Constants.SAKURA_ICON_ID);
                if (myLayer == null)
                {
                	myLayer = new DrawUserLayer(Constants.SAKURA_ICON_ID);
                	app.getDrawUserLayerManager().registUserLayer(myLayer);
                }
        	}
    		myLayer.registDrawUserPoi(di.mMapIcon);

        	// DBも更新

        	// 変更SQLを構築する。
        	StringBuffer sb = new StringBuffer();
        	sb.append("UPDATE DeliveryInfo SET Longitude = ");
        	sb.append((double)lon / 3600000.0);
        	sb.append(",Latitude = ");
        	sb.append((double)lat / 3600000.0);
        	sb.append(",MatchLevel = 10 WHERE CustID = ");
        	sb.append(di.mCustID);

        	// DBを開く
        	DbHelper dbHelper = new DbHelper(this);
        	SQLiteDatabase db = dbHelper.getWritableDatabase();

        	db.execSQL(sb.toString());
        	db.close();
        	dbHelper.close();

        	// ボタンのカラーを変える。
        	updateListButtonColor();
        	
        	// リストを表示する。
        	showList(true, false, false);
        	
        	// リトライフラグを立てる
        	app.setRetryShowList();
        	
        	// フラグを寝かせる
        	app.setIsManualMatching(false);
        }
        sakuraPoiData = null;
// Add by CPJsunagawa '13-12-25 End

        if (name == null || "".equals(name)) {
            JNIString Address = new JNIString();
            NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(lon, lat, Address);

            name = Address.getM_StrAddressString();
        }
        CommonLib.setChangePos(false);
        CommonLib.setChangePosFromSearch(false);

//Del 2012/03/13 Z01_h_fukushima(No.405) Start --> ref 2510
//        mapView.setMap_view_flag(preMapViewType);
//Del 2012/03/13 Z01_h_fukushima(No.405) End <-- ref 2510
        strFromMode = null;
        // xuyang add start #813
        if (!CommonLib.routeSearchFlag) {
            // xuyang add end #813
            Intent intent = new Intent();
// Add by CPJsunagawa '13-12-25 Start
            if (index != -1) intent.putExtra(Constants.CHANGE_COORD_LIST_INDEX, index);
// Add by CPJsunagawa '13-12-25 End
            intent.putExtra(Constants.INTENT_LONGITUDE, lon);
            intent.putExtra(Constants.INTENT_LATITUDE, lat);
            intent.putExtra(Constants.POINT_NAME, name);
// Add by CPJsunagawa '13-12-25 Start
            intent.putExtra(Constants.INTENT_CUST_ID, custID);
            intent.putExtra(Constants.INTENT_NO_CHANGE_COORD, isNoChangeCoord);
// Add by CPJsunagawa '13-12-25 End

//Chg 2011/09/21 Z01yoneya Start -->
//            setResult(Constants.RESULTCODE_FINISH_EXCEPT_MAPVIEW, intent);
//------------------------------------------------------------------------------------------------------------------------------------

// Mod by CPJsunagawa '13-12-25 Start
//        	setResult(Constants.RESULTCODE_GOBACK_TO_ROUTE_EDIT, intent);
            int setres = (index != -1) ? Constants.RESULTCODE_VEHICLE_ROUTE_DETERMINATION : Constants.RESULTCODE_GOBACK_TO_ROUTE_EDIT;
            setResult(setres, intent);
// Mod by CPJsunagawa '13-12-25 End

//Chg 2011/09/21 Z01yoneya End <--
            finish();
            // xuyang add start #813
        } else {
            Intent oIntent = new Intent();
            oIntent.setClass(this, RouteEdit.class);
            oIntent.putExtra(Constants.INTENT_LONGITUDE, lon);
            oIntent.putExtra(Constants.INTENT_LATITUDE, lat);
            oIntent.putExtra(Constants.POINT_NAME, name);
            oIntent.putExtra(Constants.PARAMS_ROUTE_MOD_FLAG, true);
            oIntent.putExtra(Constants.ROUTE_TYPE_KEY, Constants.ROUTE_TYPE_NOW);
//Chg 2011/09/23 Z01yoneya Start -->
//            this.startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
//------------------------------------------------------------------------------------------------------------------------------------
            NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
//Chg 2011/09/23 Z01yoneya End <--
        }
        // xuyang add end #813
    }

// Add by CPJsunagawa '2015-07-08 Start
    // アイコン登録（新規）
    public void goRegistIcon() {

    	System.out.println("***　Into goRegistIcon ***");
        long lon = 0;
        long lat = 0;
        String name = null;
    	int custID = -1, index = -1;
    	int nIconIndex = 0;
        boolean isNoChangeCoord = false;

        mapView.getMapCentrePos();
        lon = mapView.getCoordinate().getM_lLong();
        lat = mapView.getCoordinate().getM_lLat();

    	System.out.println("*** goRegistIcon lon =>  ***" + lon);
    	System.out.println("*** goRegistIcon lat =>  ***" + lat);
    	
    	int mIconDirection = getIntent().getIntExtra(Constants.INTENT_EXTRA_SELECT_ICON, 0);
        
    	System.out.println("*** goRegistIcon mIconDirection =>  ***" + mIconDirection);
        // ファイル名生成
        String iconPath = null;
        switch(mIconDirection) {
        case R.drawable.arrow_up:
        	iconPath = "arrow_up.png";
        	nIconIndex = UserIconInfo.ICON_ARROW_UP_INDEX;
        	break;
        case R.drawable.arrow_rightup:
        	iconPath = "arrow_rightup.png";
        	nIconIndex = UserIconInfo.ICON_ARROW_RIGHTUP_INDEX;
        	break;
        case R.drawable.arrow_right:
        	iconPath = "arrow_right.png";
        	nIconIndex = UserIconInfo.ICON_ARROW_RIGHT_INDEX;
        	break;
        case R.drawable.arrow_rightdown:
        	iconPath = "arrow_rightdown.png";
        	nIconIndex = UserIconInfo.ICON_ARROW_RIGHTDOWN_INDEX;
        	break;
        case R.drawable.arrow_down:
        	iconPath = "arrow_down.png";
        	nIconIndex = UserIconInfo.ICON_ARROW_DOWN_INDEX;
        	break;
        case R.drawable.arrow_leftdown:
        	iconPath = "arrow_leftdown.png";
        	nIconIndex = UserIconInfo.ICON_ARROW_LEFTDOWN_INDEX;
        	break;
        case R.drawable.arrow_left:
        	iconPath = "arrow_left.png";
        	nIconIndex = UserIconInfo.ICON_ARROW_LEFT_INDEX;
        	break;
        case R.drawable.arrow_leftup:
        	iconPath = "arrow_leftup.png";
        	nIconIndex = UserIconInfo.ICON_ARROW_LEFTUP_INDEX;
        	break;
        case R.drawable.ippou_up:
        	iconPath = "ippou_up.png";
        	nIconIndex = UserIconInfo.ICON_IPPOU_UP_INDEX;
        	break;
        case R.drawable.ippou_right:
        	iconPath = "ippou_right.png";
        	nIconIndex = UserIconInfo.ICON_IPPOU_RIGHT_INDEX;
        	break;
        case R.drawable.ippou_down:
        	iconPath = "ippou_down.png";
        	nIconIndex = UserIconInfo.ICON_IPPOU_DOWN_INDEX;
        	break;
        case R.drawable.ippou_left:
        	iconPath = "ippou_left.png";
        	nIconIndex = UserIconInfo.ICON_IPPOU_LEFT_INDEX;
        	break;
        case R.drawable.hyoshiki1_s:
        	iconPath = "hyoshiki1_s.png";
        	nIconIndex = UserIconInfo.ICON_HYOSHIKI_1_INDEX;
        	break;
        case R.drawable.hyoshiki2_s:
        	iconPath = "hyoshiki2_s.png";
        	nIconIndex = UserIconInfo.ICON_HYOSHIKI_2_INDEX;
        	break;
        case R.drawable.hyoshiki3_s:
        	iconPath = "hyoshiki3_s.png";
        	nIconIndex = UserIconInfo.ICON_HYOSHIKI_3_INDEX;
        	break;
        case R.drawable.hyoshiki4_s:
        	iconPath = "hyoshiki4_s.png";
        	nIconIndex = UserIconInfo.ICON_HYOSHIKI_4_INDEX;
        	break;
        case R.drawable.hyoshiki5_s:
        	iconPath = "hyoshiki5_s.png";
        	nIconIndex = UserIconInfo.ICON_HYOSHIKI_5_INDEX;
        	break;
        case R.drawable.hyoshiki6_s:
        	iconPath = "hyoshiki6_s.png";
        	nIconIndex = UserIconInfo.ICON_HYOSHIKI_6_INDEX;
        	break;
        case R.drawable.hyoshiki7_s:
        	iconPath = "hyoshiki7_s.png";
        	nIconIndex = UserIconInfo.ICON_HYOSHIKI_7_INDEX;
        	break;
        case R.drawable.hyoshiki8_s:
        	iconPath = "hyoshiki8_s.png";
        	nIconIndex = UserIconInfo.ICON_HYOSHIKI_8_INDEX;
        	break;
        case R.drawable.hyoshiki9_s:
        	iconPath = "hyoshiki9_s.png";
        	nIconIndex = UserIconInfo.ICON_HYOSHIKI_9_INDEX;
        	break;
        }
    	System.out.println("*** goRegistIcon iconPath =>  ***" + iconPath);

    	double d_lat, d_lon;
    	//GeoPoint geoP = GeoUtils.toWGS84(lat, lon);
    	System.out.println("*** goRegistIcon lat =>  *** " + lat);
    	System.out.println("*** goRegistIcon lon =>  *** " + lon);

    	GeoPoint tky;
        //tky  = GeoUtils.toTokyoDatum((double)lat / 3600000.0, (double)lon / 3600000.0);
        tky  = GeoUtils.toWGS84 (lat, lon);
        d_lat = tky.getLatitude();
        d_lon = tky.getLongitude();
    	System.out.println("*** goRegistIcon d_lat  =>  *** " + d_lat);
    	System.out.println("*** goRegistIcon d_lon  =>  *** " + d_lon);
    	
        // アイコン表示
    	String iconName = iconPath;
        NaviApplication app = (NaviApplication)getApplication();
        NaviAppDataPath naviPath = app.getNaviAppDataPath();
        iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/" + iconPath;
        System.out.println("iconPath => " + iconPath );
        //onDrawUserIcon(d_lat, d_lon, iconPath, "");
    
// Add by CPJsunagawa '2015-08-06 Start
    	// 描画後、DBに登録する
//        mTrackIconEdit = new TrackIconEdit();
        int lastID = TrackIconEdit.ExecuteRegistFigureData(this, nIconIndex, TrackIconEdit.FIGURE_KIND_ICON, d_lat, d_lon, 0, 0, iconName);
        Icon figureIcon = onDrawUserIcon(d_lat, d_lon, Integer.toString(lastID), iconPath, "");
        UserIconInfo newItem = new UserIconInfo();
        
        newItem.dLat1 = d_lat;
        newItem.dLon1 = d_lon;
        //newItem.mFigureID = figureID;
        newItem.mFigureIcon = figureIcon;
        newItem.mIconFile = iconName;
        newItem.mID = lastID;
        newItem.mIconIndex = 1;
        
        app.addUserIconInfoItem(newItem);
// Add by CPJsunagawa '2015-08-06 End

        CommonLib.setChangePos(false);
        CommonLib.setChangePosFromSearch(false);
        CommonLib.setRegIcon(false);

        strFromMode = null;
    
    }
// Add by CPJsunagawa '2015-07-08 End

    
// Add by CPJsunagawa '2015-08-15 Start
     // アイコン移動
     public void goMoveIcon() {

     	System.out.println("***　Into goMoveIcon ***");
         long lon = 0;
         long lat = 0;
     	int nIconIndex = 0;

         mapView.getMapCentrePos();
         lon = mapView.getCoordinate().getM_lLong();
         lat = mapView.getCoordinate().getM_lLat();
     	
     	int mIconDirection = getIntent().getIntExtra(Constants.INTENT_EXTRA_MOVE_ICON, 0);
         
     	System.out.println("*** goRegistIcon mIconDirection =>  ***" + mIconDirection);
     	
     	double d_lat, d_lon;
     	//GeoPoint geoP = GeoUtils.toWGS84(lat, lon);
     	System.out.println("*** goRegistIcon lat =>  *** " + lat);
     	System.out.println("*** goRegistIcon lon =>  *** " + lon);

     	GeoPoint tky;
         //tky  = GeoUtils.toTokyoDatum((double)lat / 3600000.0, (double)lon / 3600000.0);
         tky  = GeoUtils.toWGS84 (lat, lon);
         d_lat = tky.getLatitude();
         d_lon = tky.getLongitude();
     	System.out.println("*** goRegistIcon d_lat  =>  *** " + d_lat);
     	System.out.println("*** goRegistIcon d_lon  =>  *** " + d_lon);
     	
     	// アイコンの情報を取得する。
     	NaviApplication app = (NaviApplication)getApplication();
     	UserIconInfo moveUII = app.getUserIconInfo(mIconDirection);
     	
// Add by CPJsunagawa '2015-08-06 Start
     	// 描画後、DBに登録する
//         mTrackIconEdit = new TrackIconEdit();
         int lastID = TrackIconEdit.ExecuteUpdateFigureData(this, moveUII.mID, d_lat, d_lon, 0, 0);
         
         // 地図上のアイコンを削除する
         NaviRun.GetNaviRunObj().JNI_NE_DeleteUserFigure(moveUII.mFigureIcon.getParentLayer().getNELayerId(), moveUII.mFigureIcon.getNEID());
         
         // 新たな場所にアイコンを描画する
         NaviAppDataPath naviPath = app.getNaviAppDataPath();
         String iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/" + moveUII.mIconFile; 
         moveUII.mFigureIcon = onDrawUserIcon(d_lat, d_lon, Integer.toString(moveUII.mID), iconPath, "");
         
         // メモリ上の経緯度も変更する。
         moveUII.dLat1 = d_lat;
         moveUII.dLon1 = d_lon;
         
         CommonLib.setChangePos(false);
         CommonLib.setChangePosFromSearch(false);
         CommonLib.setRegIcon(false);
         CommonLib.setMoveIcon(false);

         strFromMode = null;
     }

// アイコン削除
     public void goDeleteIcon(int mIconDirection) {

     	System.out.println("***　Into goDeleteIcon ***");
         long lon = 0;
         long lat = 0;
     	int nIconIndex = 0;

/*
     	mapView.getMapCentrePos();
         lon = mapView.getCoordinate().getM_lLong();
         lat = mapView.getCoordinate().getM_lLat();
*/     	
     	//int mIconDirection = getIntent().getIntExtra(Constants.INTENT_EXTRA_DELETE_ICON, 0);
         
     	System.out.println("*** goDeleteIcon mIconDirection =>  ***" + mIconDirection);

     	// アイコンの情報を取得する。
     	NaviApplication app = (NaviApplication)getApplication();
     	UserIconInfo deleteUII = app.getUserIconInfo(mIconDirection);
     	
     	// アイコンを削除する
         int lastID = TrackIconEdit.ExecuteDeleteFigureData(this, deleteUII.mID);
         
         // 地図上のアイコンを削除する
         NaviRun.GetNaviRunObj().JNI_NE_DeleteUserFigure(deleteUII.mFigureIcon.getParentLayer().getNELayerId(), deleteUII.mFigureIcon.getNEID());
         //NaviRun.GetNaviRunObj().JNI_NE_DeleteUserFigure(1, deleteUII.mFigureIcon.getNEID());
         
         // メモリ上の図形データも削除する
         app.removeUserIconInfo(mIconDirection);
         
         CommonLib.setChangePos(false);
         CommonLib.setChangePosFromSearch(false);
         CommonLib.setRegIcon(false);
         CommonLib.setMoveIcon(false);
         CommonLib.setDeleteIcon(false);
         CommonLib.setDrawLine(false);

     }
// Add by CPJsunagawa '2015-08-15 End
    	
// Add by CPJsunagawa '2015-08-18 Start
    // ライン描画
    public void goDrawLine() {

       	System.out.println("***　Into goDrawLine ***");
        long lon = 0;
        long lat = 0;
       	int nIconIndex = 0;

           mapView.getMapCentrePos();
           lon = mapView.getCoordinate().getM_lLong();
           lat = mapView.getCoordinate().getM_lLat();
       	
       	int mIconDirection = getIntent().getIntExtra(Constants.INTENT_EXTRA_DRAW_LINE, 0);
           
       	System.out.println("*** goDrawLine mIconDirection =>  ***" + mIconDirection);
       	
       	double d_lat, d_lon;
       	//GeoPoint geoP = GeoUtils.toWGS84(lat, lon);
       	System.out.println("*** goDrawLine lat =>  *** " + lat);
       	System.out.println("*** goDrawLine lon =>  *** " + lon);

       	GeoPoint tky;
           //tky  = GeoUtils.toTokyoDatum((double)lat / 3600000.0, (double)lon / 3600000.0);
           tky  = GeoUtils.toWGS84 (lat, lon);
           d_lat = tky.getLatitude();
           d_lon = tky.getLongitude();
       	System.out.println("*** goDrawLine d_lat  =>  *** " + d_lat);
       	System.out.println("*** goDrawLine d_lon  =>  *** " + d_lon);
       	
          try {

  	        System.out.println("Into drawLine");
  	        api = ((NaviApplication)getApplication()).getApi();

          	GeoPoint gPoint;
              List<GeoPoint> geoPoints = new ArrayList<GeoPoint>();
              // ラインの始点～終点の緯度経度
              geoPoints.add(new GeoPoint(LATITUDE05, LONGITUDE05));
              geoPoints.add(new GeoPoint(LATITUDE04, LONGITUDE04));
              // ライン線種
              LineDrawParam lineDrawParam = new LineDrawParam();
              lineDrawParam.setLineWidth(3);
              lineDrawParam.setLineColor(0xFFFF0000);
              Line line = new Line(lineDrawParam);
              line.setPath(geoPoints);

              api.drawLine(LAYER_ID, line);
          } catch (NaviException e) {
              e.printStackTrace();
          }
           
           CommonLib.setChangePos(false);
           CommonLib.setChangePosFromSearch(false);
           CommonLib.setRegIcon(false);
           CommonLib.setMoveIcon(false);

           strFromMode = null;
    }
// Add by CPJsunagawa '2015-08-18 End
       
    /**
     * リピート機能
     */
    public void doRepeat() {
        CommonLib.startMediaPlayer(CommonLib.getMediaFileName());

        // アローモード非表示
        if (CommonLib.getNaviMode() != Constants.NE_NAVIMODE_BICYCLE) {
            showRepeatImage();
        }
    }

    private Timer repeatTimer;
    private TimerTask task;

    public void showRepeatImage() {
        if (guide_Info != null) {
            guide_Info.showRepeatImage();
        }
        if (repeatTimer != null) {
            repeatTimer.cancel();
        }
        if (task != null) {
            task.cancel();
        }
        repeatTimer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {
                task.cancel();
                repeatTimer.cancel();
                hiddenRepeatImageButton();
            }
        };
        repeatTimer.schedule(task, 20 * 1000);
    }

    /**
     * ナビブロードバンド
     */
    public void intentRouteGuide() {/*
                                    if (null == myPosi) {
                                    myPosi = new JNITwoLong();
                                    }
                                    if (null == targetPosi) {
                                    targetPosi = new JNITwoLong();
                                    }
                                    if (null == targetPosiName) {
                                    targetPosiName = new JNIString();
                                    }
                                    if (null == vehichelLocationForIntent) {
                                    vehichelLocationForIntent = new Location(
                                    LocationManager.GPS_PROVIDER);
                                    }
                                    if (null == targetLocation) {
                                    targetLocation = new Location(LocationManager.GPS_PROVIDER);
                                    }
                                    if (null == routeGuidance) {
                                    routeGuidance = new ZNE_RouteGuidance_t();
                                    }

                                    NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(myPosi);
                                    if (myPosi != null) {
                                    vehichelLocationForIntent = CommonLib
                                    .ConvertDBCoordinateSystemToWGS(myPosi);
                                    }

                                    NaviRun.GetNaviRunObj().JNI_DG_GetGuidePointInfo(routeGuidance);

                                    // add by wangdong 2010/11/10
                                    if (NaviRun.isViaPassed(routeGuidance.getEventCode())) {
                                    CommonLib.createNaviDataFile(true);
                                    }

                                    targetPosi = routeGuidance.getGuidePoint().getstPos();
                                    NaviRun.GetNaviRunObj()
                                    .JNI_NE_GetAddressString(targetPosi.getM_lLong(),
                                    targetPosi.getM_lLat(), targetPosiName);
                                    if (targetPosi != null) {
                                    targetLocation = CommonLib
                                    .ConvertDBCoordinateSystemToWGS(targetPosi);
                                    }

                                    Intent intentBroadCast = new Intent();
                                    Bundle mBundle = new Bundle();

                                    intentBroadCast.setAction(Constants.ROUTEGUIDE_INTENT_ACTION_EXTRA);
                                    mBundle.putParcelable(Constants.INTENT_VEHICLEINFO_EXTRA,
                                    vehichelLocationForIntent);
                                    mBundle.putParcelable(Constants.INTENT_ROUTEGUIDE_TARGETLOCATION_EXTRA,
                                    targetLocation);
                                    mBundle.putInt(Constants.INTENT_ROUTEGUIDE_TYPE_EXTRA, routeGuidance
                                    .getEventCode());
                                    mBundle.putInt(Constants.INTENT_ROUTETARGET_TYPE_EXTRA, routeGuidance
                                    .getEventDetailCode());
                                    mBundle.putString(Constants.INTENT_ROUTEGUIDE_TARGETNAME_EXTRA,
                                    targetPosiName.getM_StrAddressString());
                                    mBundle.putFloat(Constants.INTENT_ROUTEGUIDE_DISTANCE_EXTRA,
                                    routeGuidance.getDist());
                                    mBundle.putInt(Constants.INTENT_ROUTEGUIDE_DIRECTION_EXTRA,
                                    routeGuidance.getTargetDirection());
                                    intentBroadCast.putExtras(mBundle);
                                    sendBroadcast(intentBroadCast);

                                    return;
                                    */
    }

    /**
     * ナビブロードバンド
     */
    public void intentRoadNameChange() {/*

                                        ZNE_VehicleInfo_t roadNameChange = new ZNE_VehicleInfo_t();
                                        JNITwoLong DBLonLat = new JNITwoLong();
                                        JNITwoInt roadType = new JNITwoInt();
                                        Location location = new Location(LocationManager.GPS_PROVIDER);
                                        NaviRun.GetNaviRunObj().JNI_DG_getVehiclePosInfo(roadNameChange);
                                        DBLonLat = roadNameChange.getstPos();
                                        roadType = roadNameChange.getRoadType();
                                        NaviLog.d("Intent", "preroadType" + roadType.getM_iSizeX());
                                        NaviLog.d("Intent", "CurroadType" + roadType.getM_iSizeY());
                                        // 東京座標系で表示する経緯度をにWGS84座標系で表示する経緯度変換する
                                        if (DBLonLat != null) {
                                        location = CommonLib.ConvertDBCoordinateSystemToWGS(DBLonLat);
                                        }

                                        // 道路移動の状態をブロードバンド
                                        Intent intentBroadCast = new Intent();
                                        Bundle mBundle = new Bundle();
                                        mBundle.putParcelable(Constants.INTENT_VEHICLEINFO_EXTRA, location);
                                        mBundle.putInt(Constants.ROADTYPE_EXTRA_FROM, roadType.getM_iSizeX());
                                        mBundle.putInt(Constants.ROADTYPE_EXTRA_TO, roadType.getM_iSizeY());
                                        intentBroadCast.setAction(Constants.ROADTYPE_INTENT_ACTION_EXTRA);
                                        intentBroadCast.putExtras(mBundle);
                                        sendBroadcast(intentBroadCast);

                                        return;

                                        */
    }

    /**
     * ナビブロードバンド
     */
    public void intentRoadInfo() {/*
                                  int viaNum = 0;

                                  int naviMode = CommonLib.getNaviMode();
                                  if (null == routeInfo) {
                                  routeInfo = new ZNE_RouteInfo_t();
                                  }
                                  if (null == guidePoint) {
                                  guidePoint = new ZNE_RouteInfoGuidePoint_t[7];
                                  }
                                  if (null == locationrouteInfo) {
                                  locationrouteInfo = new Location[7];
                                  }

                                  JNITwoLong myPosi = new JNITwoLong();
                                  JNIString vehichelName = new JNIString();
                                  Location vehichelLocation = new Location(LocationManager.GPS_PROVIDER);
                                  if (null == viaName) {
                                  viaName = new JNIString[7];
                                  }

                                  for (int i = 0; i < 7; i++) {
                                  viaName[i] = new JNIString();
                                  guidePoint[i] = new ZNE_RouteInfoGuidePoint_t();
                                  locationrouteInfo[i] = new Location(LocationManager.GPS_PROVIDER);
                                  }
                                  NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(myPosi);
                                  if (myPosi != null) {
                                  vehichelLocation = CommonLib.ConvertDBCoordinateSystemToWGS(myPosi);

                                  NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
                                  myPosi.getM_lLong(), myPosi.getM_lLat(), vehichelName);
                                  }

                                  if (naviMode == Constants.NE_NAVIMODE_CAR) {
                                  NaviRun.GetNaviRunObj().JNI_DG_GetRouteInfo(routeInfo);
                                  } else {
                                  NaviRun.GetNaviRunObj().JNI_CT_Man_WG_getRouteInfo(routeInfo);
                                  }

                                  guidePoint = routeInfo.getRouteInfoGuidePoint();
                                  ArrayList<Location> listViaLocation = null;
                                  ArrayList<String> listViaName = null;
                                  listViaLocation = new ArrayList<Location>();
                                  listViaName = new ArrayList<String>();

                                  if (naviMode == Constants.NE_NAVIMODE_CAR) {
                                  for (int j = 0; j < 7; j++) {
                                  locationrouteInfo[j] = CommonLib
                                  .ConvertDBCoordinateSystemToWGS(guidePoint[j]
                                  .getstPos());
                                  if (null != guidePoint[j].getstPos()) {
                                  NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
                                  guidePoint[j].getstPos().getM_lLong(),
                                  guidePoint[j].getstPos().getM_lLat(), viaName[j]);
                                  if (j != 0 && j != 6) {
                                  if (0 != guidePoint[j].getstPos().getM_lLat()
                                  && 0 != guidePoint[j].getstPos().getM_lLong()) {
                                  listViaName.add(viaName[viaNum]
                                    .getM_StrAddressString());
                                  listViaLocation.add(locationrouteInfo[viaNum]);

                                  viaNum++;
                                  }
                                  }
                                  } else {
                                  continue;
                                  }
                                  }
                                  } else {
                                  for (int j = 0; j < routeInfo.getIViaValidCount(); j++) {
                                  locationrouteInfo[j] = CommonLib
                                  .ConvertDBCoordinateSystemToWGS(guidePoint[j]
                                  .getstPos());
                                  if (0 != guidePoint[j].getstPos().getM_lLat()
                                  && 0 != guidePoint[j].getstPos().getM_lLong()) {
                                  NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
                                  guidePoint[j].getstPos().getM_lLong(),
                                  guidePoint[j].getstPos().getM_lLat(), viaName[j]);
                                  if (j != 0 && (routeInfo.getIViaValidCount()) > 2) {
                                  if (j != (routeInfo.getIViaValidCount() - 1)) {
                                  listViaName.add(viaName[j].getM_StrAddressString());
                                  listViaLocation.add(locationrouteInfo[j]);
                                  }

                                  viaNum++;
                                  }
                                  } else {
                                  listViaName.remove(viaName[viaNum].getM_StrAddressString());
                                  listViaLocation.remove(locationrouteInfo[viaNum]);
                                  break;
                                  }
                                  }
                                  }

                                  int routeSearchType = routeInfo.getIRouteSearchType();
                                  // roadType = roadNameChange.getRoadType();
                                  // 東京座標系で表示する経緯度をにWGS84座標系で表示する経緯度変換する

                                  // 道路移動の状態をブロードバンド
                                  Intent intentBroadCast = new Intent();
                                  Bundle mBundle = new Bundle();
                                  mBundle.putParcelable(Constants.INTENT_VEHICLEINFO_EXTRA,
                                  vehichelLocation);
                                  mBundle.putParcelable(
                                  Constants.INTENT_ROUTESEARCHED_STARTLOCATION_EXTRA,
                                  vehichelLocation);
                                  mBundle.putString(Constants.INTENT_ROUTESEARCHED_STARTNAME_EXTRA,
                                  vehichelName.getM_StrAddressString());

                                  if (naviMode == Constants.NE_NAVIMODE_CAR) {
                                  mBundle.putParcelable(
                                  Constants.INTENT_ROUTESEARCHED_GOALLOCATION_EXTRA,
                                  locationrouteInfo[6]);
                                  mBundle.putString(Constants.INTENT_ROUTESEARCHED_GOALNAME_EXTRA,
                                  viaName[6].getM_StrAddressString());
                                  } else if (naviMode == Constants.NE_NAVIMODE_MAN) {
                                  mBundle.putParcelable(
                                  Constants.INTENT_ROUTESEARCHED_GOALLOCATION_EXTRA,
                                  locationrouteInfo[routeInfo.getIViaValidCount() - 1]);
                                  mBundle.putString(Constants.INTENT_ROUTESEARCHED_GOALNAME_EXTRA,
                                  viaName[routeInfo.getIViaValidCount() - 1]
                                  .getM_StrAddressString());
                                  }

                                  mBundle.putInt(Constants.INTENT_ROUTESEARCHED_SEARCHTYPE_EXTRA,
                                  routeSearchType);
                                  mBundle.putStringArrayList(
                                  Constants.INTENT_ROUTESEARCHED_VIANAME_EXTRA, listViaName);
                                  mBundle.putParcelableArrayList(
                                  Constants.INTENT_ROUTESEARCHED_VIALOCATION_EXTRA,
                                  listViaLocation);
                                  intentBroadCast.setAction(Constants.ROUTESEARCHED_INTENT_ACTION_EXTRA);
                                  intentBroadCast.putExtras(mBundle);
                                  sendBroadcast(intentBroadCast);

                                  return;
                                  */
    }

    /**
     * ナビアプリのカーナビモードをiモード側と共有する。<br>
     * （徒歩ナビ時の省電力測位開始トリガに利用）
     */
    void onWGModeEvent() {
        Intent intentBroadCast = new Intent();
        sendBroadcast(intentBroadCast);
    }

    /**
     * ナビアプリの起動状態をiモード側と共有する。<br>
     * （ナビアプリ起動中はiモード側ステータスバーにナビアイコンを表示する）
     */
    void initIModeEvent() {
        Intent intentBroadCast = new Intent();
        sendBroadcast(intentBroadCast);
    }

    /**
     * Intent起動且つ経路計算が必要であれば、当該関数が呼出される
     */
    public void intentNaviStart() {/*
                                   JNITwoLong Coordinate = new JNITwoLong();
                                   JNIString Address = new JNIString();
                                   int iRoutePointType_Goal = 8;
                                   int iRoutePointType_Start = 1;

                                   int NE_NEActType_RouteCalculate = 5;
                                   int NE_NETransType_NormalMap = 0;
                                   byte bScale = 0;
                                   long lLongitude = 0;
                                   long lLatitude = 0;

                                   for (int i = 0; i < 8; i++) {
                                   int NE_RoutePointType_Start = 1;
                                   NaviRun.GetNaviRunObj().JNI_NE_DeleteRoutePoint(NE_RoutePointType_Start + i);
                                   }

                                   NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);
                                   NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(Coordinate.getM_lLong(), Coordinate.getM_lLat(), Address);

                                   NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(iRoutePointType_Start, Coordinate.getM_lLong(), Coordinate.getM_lLat(), Address.getM_StrAddressString(), 0);

                                   if(null != lstViaRouteLocation)
                                   {
                                   for (int i = 0; i < lstViaRouteLocation.size(); i++) {
                                   NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(i + 3,coorViaLonlat[i].getM_lLat(),coorViaLonlat[i].getM_lLong(),  lstViaRouteName.get(i), 0);
                                   }

                                   NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(iRoutePointType_Goal,distinationCoorLat,distinationCoorLon,goalName,0);

                                   if(0 != routeCondition)
                                   {
                                   JNIInt naviMode = new JNIInt();
                                   NaviRun.GetNaviRunObj().JNI_NE_GetNaviMode(naviMode);

                                   if (Constants.NE_NAVIMODE_CAR == naviMode.getM_iCommon()) {
                                   NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(iRoutePointType_Start, iRoutePointType_Goal,routeCondition);
                                   } else {
                                   NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(iRoutePointType_Goal,routeCondition);
                                   }
                                   }
                                   //            if (isOpenMap) {
                                   ////                NaviRun.GetNaviRunObj().clearOpenMapView();
                                   //                this.setResult(Constants.RESULTCODE_FINISH_EXCEPT_MAPVIEW);
                                   //                this.finish();
                                   //            }
                                   goLocation();

                                   showRouteSearchWaitingDialog();

                                   NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(NE_NEActType_RouteCalculate, NE_NETransType_NormalMap, bScale,
                                   lLongitude, lLatitude);
                                   NaviRun.GetNaviRunObj().JNI_CT_Navi_StartCalculate(Constants.DEF_ROUTE_CALCULATE_TYPE_MAIN);
                                   }else{
                                   //NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(2, 2, (byte)zoomLevel,
                                   //  distinationCoorLat, distinationCoorLon);

                                   //            if (isOpenMap) {
                                   ////                NaviRun.GetNaviRunObj().clearOpenMapView();
                                   //                this.setResult(Constants.RESULTCODE_FINISH_EXCEPT_MAPVIEW);
                                   //                this.finish();
                                   //            }
                                   goLocation();
                                   showRouteSearchWaitingDialog();

                                   NaviRun.GetNaviRunObj().JNI_ct_uic_RouteCalculate();
                                   }

                                   return;
                                   */
    }

    /**
     * ナビブロードバンド
     */
    public void intentAreaMoved() {/*
                                   if(null == vehichelLocation)
                                   {
                                   vehichelLocation = new Location(LocationManager.GPS_PROVIDER);
                                   }

                                   JNITwoLong Coordinate = mapView.getCoordinate();
                                   if(null == Coordinate)
                                   {
                                   Coordinate = new JNITwoLong();
                                   }

                                   JNIString addressString = mapView.getAddressString();
                                   if(null == addressString)
                                   {
                                   addressString = new JNIString();
                                   }

                                   NaviRun.GetNaviRunObj().JNI_CT_Drv_GetVehiclePosition(Coordinate);

                                   if (areaName.getPreAreaName() != null
                                   || areaName.getCurAreaName() != null) {
                                   NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(Coordinate.getM_lLong(), Coordinate
                                   .getM_lLat(), addressString);
                                   if (addressString.getM_StrAddressString().equals(
                                   areaName.getCurAreaName())) {
                                   return;
                                   } else {
                                   areaName.setPreAreaName(areaName.getCurAreaName());
                                   areaName.setCurAreaName(addressString.getM_StrAddressString());

                                   ArrayList<String> listPreAreaName = null;
                                   listPreAreaName = new ArrayList<String>();

                                   ArrayList<String> listCurAreaName = null;
                                   listCurAreaName = new ArrayList<String>();

                                   String []preAreaName = areaName.getPreAreaName().split(" ");
                                   String []curAreaName = areaName.getCurAreaName().split(" ");

                                   if(null != preAreaName[0])
                                   for (int i = 0; i < preAreaName.length; i++) {
                                   if(null != preAreaName[i])
                                   {
                                   listPreAreaName.add(preAreaName[i]);
                                   }
                                   }
                                   for (int j = 0; j < curAreaName.length; j++){
                                   if(null != curAreaName[j])
                                   {
                                   listCurAreaName.add(curAreaName[j]);
                                   }
                                   }
                                   Intent intentBroadCast = new Intent();
                                   Bundle mBundle = new Bundle();

                                   if (Coordinate != null) {
                                   vehichelLocation = CommonLib
                                   .ConvertDBCoordinateSystemToWGS(Coordinate);
                                   }

                                   intentBroadCast
                                   .setAction(Constants.AREA_MOVED_INTENT_ACTION_EXTRA);
                                   mBundle.putParcelable(Constants.INTENT_VEHICLEINFO_EXTRA,
                                   vehichelLocation);
                                   mBundle.putStringArrayList(Constants.PRE_AREATYPE_EXTRA_FROM,
                                   listPreAreaName);
                                   mBundle.putStringArrayList(Constants.CUR_AREATYPE_EXTRA_FROM,
                                   listCurAreaName);
                                   intentBroadCast.putExtras(mBundle);
                                   sendBroadcast(intentBroadCast);
                                   }
                                   } else {
                                   NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(Coordinate.getM_lLong(), Coordinate
                                   .getM_lLat(), addressString);
                                   areaName.setCurAreaName(addressString.getM_StrAddressString());
                                   areaName.setPreAreaName(addressString.getM_StrAddressString());
                                   }
                                   return;
                                   */
    }

    /**
     * POI タイトルを表示する
     */
    private void POiTitle() {
        JNIInt State = new JNIInt();
//        JNIInt DataCount = new JNIInt();
        JNIString strPoiTitle = new JNIString();
        int iPoi = 0;
//        boolean bViewFlg = false;

//// ADD.2013.06.11 N.Sasao お気に入り情報表示後、タッチ移動で情報が消えない START
//    	NaviRun.GetNaviRunObj().JNI_CT_ExposeMap();
//// ADD.2013.06.11 N.Sasao お気に入り情報表示後、タッチ移動で情報が消えない  END


        // ユーザ形状オーバーレイ ( by NaviApi )
        JNIInt userFigState = new JNIInt();
        JNILong drawFigIdObj = new JNILong();
        NaviRun.GetNaviRunObj().JNI_Java_GetMapCenterInfo_UserFig(userFigState, drawFigIdObj);
        iPoi = userFigState.getM_iCommon();
        if (iPoi != 0) {
            long drawFigureId = drawFigIdObj.getLcount();
            mapView.updateMapCenterContentByUserFig(drawFigureId);
            return;
//            bViewFlg = true;
        }

//        // ドライブコンテンツの名称表示
//// MOD.2013.06.12 N.Sasao POI情報リスト表示対応 START
//    	NaviRun.GetNaviRunObj().JNI_JAVA_GetDriConPOIIconInformationState( State, DataCount );
//// MOD.2013.06.12 N.Sasao POI情報リスト表示対応  END
//        iPoi = State.getM_iCommon();
//// MOD.2013.07.02 N.Sasao POI情報リスト表示負荷低減3 START
//// MOD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2 START
//// MOD.2013.03.05 N.Sasao Bug #10946 走行時、ドライブコンテンツの地点を通過すると、詳細データが自動で表示される START
//        if ( (iPoi != 0) && mapView.isMapScrolled() && NaviRun.GetNaviRunObj().JNI_JAVA_GetIsDriConShow() ) {
//// MOD.2013.06.12 N.Sasao POI情報リスト表示対応 START
//// MOD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2  END
//// MOD.2013.07.02 N.Sasao POI情報リスト表示負荷低減3  END
//        	int nAreaCount = DataCount.getM_iCommon();
//
//        	List<DC_POIInfoShowList> list = new ArrayList<DC_POIInfoShowList>();
//
//        	/* MOD .2013.04.05 N.Sasao 描画速度アップ対応 START	*/
//        	DC_POIInfoShowList[] dataList = new DC_POIInfoShowList[nAreaCount];
//        	for( int i = 0; i < nAreaCount; i++ ){
//        		dataList[i] = new DC_POIInfoShowList();
//        	}
//	        NaviRun.GetNaviRunObj().JNI_JAVA_GetDriConPOIIconInformation( nAreaCount, dataList );
//
//	        List<DC_POIInfoShowList> toList = new ArrayList<DC_POIInfoShowList>(Arrays.asList(dataList));
//
//// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
//        	mapView.updateDriConName(true, toList);
//// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
//// MOD.2013.06.12 N.Sasao POI情報リスト表示対応  END
//// MOD.2013.03.05 N.Sasao Bug #10946 走行時、ドライブコンテンツの地点を通過すると、詳細データが自動で表示される  END
//            bViewFlg = true;

// ADD.2013.06.11 N.Sasao お気に入り情報表示後、タッチ移動で情報が消えない START
        NaviRun.GetNaviRunObj().JNI_CT_ExposeMap();
// ADD.2013.06.11 N.Sasao お気に入り情報表示後、タッチ移動で情報が消えない  END
        NaviRun.GetNaviRunObj().JNI_Java_GetShowInformation(State);
        iPoi = State.getM_iCommon();
        //        NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy POiTitle iPoi==="  + iPoi);
        if (iPoi == 1) {
            //yangyang add start Bug974
            JNITwoLong CoordinateCenter = new JNITwoLong();
            JNITwoLong CoordinateMycar = new JNITwoLong();
            NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(CoordinateCenter);
            NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(CoordinateMycar);
            //現在地を押下し、地図中心の位置と自車の位置は一致する場合

            //yangyang add start Bug974
            if (!(CoordinateCenter.getM_lLat() == CoordinateMycar.getM_lLat() &&
                    CoordinateCenter.getM_lLong() == CoordinateCenter.getM_lLong()) && !Constants.informationBarShow) {
                //yangyang add end Bug974
                NaviRun.GetNaviRunObj().JNI_Java_GetInformationInfo(strPoiTitle);
                mapView.updateMapCentreName(false, strPoiTitle.getM_StrAddressString());
                //hangeng add start bug1696
                PreferencesService oPre = new PreferencesService(this, "FAVORITES");
                oPre.clearAllValue();
                NaviLog.d(NaviLog.PRINT_LOG_TAG,"come save");
                oPre.setValue("favoritesname", strPoiTitle.getM_StrAddressString());
                oPre.save();
                //hangeng add end bug1696

                //yangyang add start Bug974
//                bViewFlg = true;
            }
            //yangyang add end Bug974

        } else if (iPoi == 2) {
//        if ( !bViewFlg ) {
            //yangyang add start 20110505
            if (!Constants.informationBarShow) {
// MOD.2013.07.02 N.Sasao POI情報リスト表示負荷低減3 START
                mapView.hideCurPosName();
// MOD.2013.07.02 N.Sasao POI情報リスト表示負荷低減3  END
            }
            //yangyang add end  20110505
        }
    }

    /**
     * ライフログ重畳
     */
    public void lifeLogPOi() {
        PoiIconInfo poiIconInfo = new PoiIconInfo();
        NaviRun.GetNaviRunObj().JNI_Java_GetShowPOIIconInformation(poiIconInfo);
        long lFlag = 0;
        lFlag = poiIconInfo.getLFlag();
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy lifeLogPOi lFlag=="  + lFlag);
        if (lFlag == 1) {

            // long lLayer = poiIconInfo.getLLayerID();
            // String strTitle = "";
            long lLon = poiIconInfo.getLLon();
            long lLat = poiIconInfo.getLLat();

            JNITwoLong DBLatLon = new JNITwoLong();
            DBLatLon.setM_lLat(lLat);
            DBLatLon.setM_lLong(lLon);

            CommonLib.ConvertDBCoordinateSystemToWGS(DBLatLon);

            // CommonLib.mapIconInfo = PoiLogDisplay.GetPoiLogDisplayObj()
            // .POIDispaly(String.valueOf(lLayer), mapIconlocation);
            // MapIcon mapIcon = null;
            // mapIcon = CommonLib.mapIconInfo;
            // txt_Poi.setText(CommonLib.mapIconInfo.getContentTitle());
            // if (CommonLib.mapIconInfo.hasContent()) {
            // imgPoiConetnt.setVisibility(View.VISIBLE);
            // imgPoiConetnt.setOnClickListener(new OnClickListener() {
            // public void onClick(View v) {
            // openContent(CommonLib.mapIconInfo);
            // }
            // });
            // } else {
            // imgPoiConetnt.setVisibility(View.INVISIBLE);
            // }

        } else if (lFlag == 0) {
//            layoutPoi.setVisibility(View.INVISIBLE);
//            mapView.hideCurPosName();
            this.POiTitle();
        }
    }

    /**
     * 人モードの場合、方位マークのビットマップを設定する。
     */
    public void setGuideDirectionAngle() {
        if (guide_Info != null) {
            guide_Info.setGuideDirectionAngle();
        }
    }

//    /**
//     * POI 人モードの場合、方位マークのビットマップを隠れる
//     */
//    public void hideGoalLayout() {
//        if (guide_Info != null) {
//            guide_Info.hideGoalLayout();
//        }
//    }

    /**
     * 到着情報を表示する。
     */
    public void showArrivalForecastInfo(int iArrivalTime, ZNUI_DESTINATION_DATA destData) {
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
        DebugLogOutput.put("RakuRaku-Log: showArrivalForecastInfo()");
        DebugLogOutput.put("RakuRaku-Log: 到着情報表示");
        DebugLogOutput.put("RakuRaku-Log: iArrivalTime->" + String.valueOf(iArrivalTime));
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
        if (guide_Info != null) {
            guide_Info.showArrivalForecastInfo(iArrivalTime, destData);
        }
    }

    /**
     * 到着情報を隠す。
     */
    public void hideArrivalForecastInfo() {
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
        DebugLogOutput.put("RakuRaku-Log: hideArrivalForecastInfo()");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
        if (guide_Info != null) {
            guide_Info.hideArrivalForecastInfo();
        }
    }

    /**
     * 簡易案内対象変化の内容を更新する
     */
    public void updateSimpleGuideInfo() {
        if (guide_Info != null) {
            guide_Info.updateSimpleGuideInfo();
        }
    }

//
//    /**
//     * 残距離バー描画
//     *
//     * @param cnt
//     * @param bshow
//     */
//    public void CalcRectDistanceBar(int cnt, boolean bshow) {
//        if (guide_Info != null) {
//            guide_Info(cnt, bshow);
//        }
//    }
//
//    /**
//     * 残距離バー描画
//     *
//     * @param bshowJctBar
//     */
//    public void CalcJctRectDistanceBar(boolean bshowJctBar) {
//        if (guide_Info != null) {
//            guide_Info(bshowJctBar);
//        }
//    }
//
//    /**
//     * 残距離バーは表示しない
//     */
//    public void hidCalcRectDistanceBar() {
//        if (guide_Info != null) {
//            guide_Info.hidCalcRectDistanceBar();
//        }
//    }

    /**
     * レーン情報を更新する
     */
    public void updateLaneInfo() {
        if (guide_Info != null) {
            guide_Info.updateLaneInfo();
        }
    }

    /**
     * レーン情報の距離を更新します
     */
    public void updateLaneDis() {
        if (guide_Info != null) {
            guide_Info.updateLaneDis();
        }
    }

    /**
     * 高速画像案内を隠す
     */
    public void hideImageGuide() {
        if (guide_Info != null) {
            guide_Info.hideImageGuide();
        }
    }

    /**
     * 高速画像案内を表示する
     */
    public void showImageGuide() {
        if (guide_Info != null) {
            guide_Info.showImageGuide();
        }
    }

    /**
     * 交差点の名称を表示する
     */
    public void showIntersectionInfo() {
        if (guide_Info != null) {
            CommonLib.setIsShowDoubleMap(true);
            guide_Info.showIntersectionInfo();
        }
    }

    /**
     * 案交差点の名称を隠れる
     */
    public void hideIntersectionInfo() {
        if (guide_Info != null) {
            CommonLib.setIsShowDoubleMap(false);
            guide_Info.hideIntersectionInfo();
        }
    }

    /**
     * 方面看板を表示する
     */
    public void showImageDirectionBoard() {
        if (guide_Info != null) {
            guide_Info.showImageDirectionBoard();
        }
    }

    /**
     * 案内リストを表示する
     */
    public void showGuidanceList() {
        if (guide_Info != null) {
            guide_Info.showGuidanceList();
        }
    }

    /**
     * 案内リスト合計数を設定する
     */
    public void initGuideICList() {
        if (guide_Info != null) {
            guide_Info.initGuideList();
        }
    }

    /**
     * 案内リストを隠す
     */
    public void hideGuideList() {
        if (guide_Info != null) {
            guide_Info.hideGuideList();
        }
    }

    /**
     * 案内終了,Guide情報を隠れる
     */
    public void hideGuideInfomation() {
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
        DebugLogOutput.put("RakuRaku-Log: " + "hideGuideInfomation()");
        DebugLogOutput.put("RakuRaku-Log: 案内終了");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end

        if (guide_Info != null) {
            guide_Info.hideGuideInfomation();
        }
    }

    /**
     * 案内終了,Guide情報を隠れる
     */
    public void hideGuideInfomationForGuideEnd() {
// Add by CPJsunagawa '13-12-25 Start
    	//SakuraRouteGuidanceListner.getMyInstance().onNearToDestination(null);
// Add by CPJsunagawa '13-12-25 End

// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
        DebugLogOutput.put("RakuRaku-Log: " + "hideGuideInfomationForGuideEnd()");
        DebugLogOutput.put("RakuRaku-Log: 案内終了＋ガイド隠す");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end

        if (guide_Info != null) {
            guide_Info.hideGuideInfomationForGuideEnd();
        }
    }

    /**
     * リピートを隠す
     */
    public void hiddenRepeatImageButton() {
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
        DebugLogOutput.put("RakuRaku-Log: " + "hiddenRepeatImageButton()");
        DebugLogOutput.put("RakuRaku-Log: リピート隠す");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (guide_Info != null) {
                    guide_Info.hiddenRepeatImageButton();
                }
            }
        });
    }

    /**
     * 方面看板を隠す
     */
    public void hideDisplayBoard() {
        if (guide_Info != null) {
            guide_Info.hideDisplayBoard();
        }
    }

    /**
     * 案内終了,Guide情報を隠れる
     */
    public void hideGuideInfo() {
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
        DebugLogOutput.put("RakuRaku-Log: " + "hideGuideInfo()");
        DebugLogOutput.put("RakuRaku-Log: 案内終了,Guide情報を隠す");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
        if (guide_Info != null) {
            guide_Info.hideGuideInfo();
        }
    }

//Del 2011/09/23 Z01yoneya Start -->未使用だったので削除
//    public void goPoiInfo() {
//        Intent oIntent = new Intent();
////Chg 2011/09/23 Z01yoneya Start -->
////        startActivityForResult(oIntent, 0x00);
////------------------------------------------------------------------------------------------------------------------------------------
//        NaviActivityStarter.startActivityForResult(this, oIntent, 0x00);
////Chg 2011/09/23 Z01yoneya End <--
//    }
//Del 2011/09/23 Z01yoneya End <--

    public void onNaviViewShow(boolean isShow) {
//Chg 2012/04/25 Z01hirama Start --> #4415
//        if (m_oTopView != null && (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_CAR)) {
//------------------------------------------------------------------------------------------------------------------------------------
    	// 車モードからのモード変更により地図2画面表示が強制解除された場合の為に、
    	// IsShowDoubleMap()がtrueの場合はモードに依存せず処理実行する.
    	if (m_oTopView != null &&
    		((CommonLib.getNaviMode() == Constants.NE_NAVIMODE_CAR) || (CommonLib.IsShowDoubleMap()))) {
//Chg 2012/04/25 Z01hirama End <-- #4415

            if (isShow) {
                m_oTopView.onShowNaviView();
            } else {
                m_oTopView.onHideNaviView();
            }
        }
    }

    public void refreshAzimuth() {
        // 自動車・歩行者のモードを取得
        int naviMode = CommonLib.getNaviMode();
        if (naviMode == Constants.NE_NAVIMODE_MAN) {
            ZNUI_UI_MARK_DATA markData = new ZNUI_UI_MARK_DATA();
            NaviRun.GetNaviRunObj().JNI_Java_GetDestinationGuide(markData);
            ZNUI_ANGLE_DATA stAngle = markData.getAngle();
            mapView.setPointAngle(null, false, stAngle);
        } else {
            Java_IconInfo iconInfo = new Java_IconInfo();
            NaviRun.GetNaviRunObj().JNI_Java_GetCompass(iconInfo);
            // 方位マークのビットマップを回転
            mapView.setPointAngle(iconInfo, false, null);
        }
    }

    // 残り時間を更新します
    public void updateRemainingTime() {
        if (null == destinationData) {
            destinationData = new ZNUI_DESTINATION_DATA();
        }
        int naviMode = CommonLib.getNaviMode();
        if (naviMode == Constants.NE_NAVIMODE_CAR) {
            NaviRun.GetNaviRunObj().JNI_Java_GetDestination(destinationData);
        } else if (naviMode == Constants.NE_NAVIMODE_MAN) {
            NaviRun.GetNaviRunObj().JNI_WG_GetDestination(destinationData);
        }
        if (destinationData != null) {
            ZNUI_TIME time = destinationData.getStTime();
            if (time != null) {
                Calendar mCalendar = Calendar.getInstance();
                mCalendar.setTimeInMillis(System.currentTimeMillis());
                int iHour = mCalendar.get(Calendar.HOUR_OF_DAY);
                int iMinute = mCalendar.get(Calendar.MINUTE);
                int iSystemTime = iHour * 60 + iMinute;
                int iArrivalTime = time.getNHouer() * 60 + time.getNMinute();
                if (iArrivalTime >= iSystemTime) {
                    iRemainingTime = iArrivalTime - iSystemTime;
                } else {
                    iRemainingTime = iArrivalTime + 1440 - iSystemTime;
                }
                if (MapView.getInstance().getMap_view_flag() == Constants.ROUTE_MAP_VIEW
                        || MapView.getInstance().getMap_view_flag() == Constants.FIVE_ROUTE_MAP_VIEW) {
                    showRouteMenu(time.getNHouer() * 60 + time.getNMinute(), destinationData);
                }
                showArrivalForecastInfo(time.getNHouer() * 60 + time.getNMinute(), destinationData);
            }
        }
    }

    // ルート情報が更新した
    public void updateNaviDataFile() {

// Add by CPJsunagawa '13-12-25 Start
    	//SakuraRouteGuidanceListner.getMyInstance().onNearToWaypoint(null);
// Add by CPJsunagawa '13-12-25 End

        JNITwoLong JNIpositon = new JNITwoLong();
        JNIString JNIpositonName = new JNIString();
        JNIInt JNIpass = new JNIInt();
        for (int i = Constants.NAVI_VIA_INDEX; i < Constants.NAVI_DEST_INDEX; i++) {
            NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(i, JNIpositon, JNIpositonName, JNIpass);
            if ((JNIpositon.getM_lLong() != 0 || JNIpositon.getM_lLat() != 0) && (1 == JNIpass.getM_iCommon())) {
//Chg 2011/09/26 Z01yoneya Start -->
//                CommonLib.createNaviDataFile(true);
//------------------------------------------------------------------------------------------------------------------------------------
                CommonLib.createNaviDataFile(true, ((NaviApplication)getApplication()).getNaviAppDataPath());
//Chg 2011/09/26 Z01yoneya End <--
                return;
            }
        }
        /*if (null == routeGuidance) {
        	routeGuidance = new ZNE_RouteGuidance_t();
        }
        NaviRun.GetNaviRunObj().JNI_DG_GetGuidePointInfo(routeGuidance);
        if (NaviRun.isViaPassed(routeGuidance.getEventCode())) {
        	CommonLib.createNaviDataFile(true);
        }*/
    }

    // XuYang add start 走行中
    @Override
    public void onCarMountEventChange(Intent oIntent) {
    }

    // XuYang add end 走行中

// Add 2011/04/04 sawada Start -->
    @Override
    public void onWalkingEventChange(Intent intent) {
        boolean walking_status = true;
        if (walking_status) {
            // 歩行状態
            ElectricCompass.disable();
        } else {
            // 停止状態
            ElectricCompass.enable();
        }
        systemLimit();
    }

// Add 2011/04/04 sawada End   <--

    public boolean isHeadupCompass() {

        JNIInt intHeadingUp = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapHeadingup(intHeadingUp);
        if (intHeadingUp.getM_iCommon() == Constants.HEADINGUP_ON) {
            return true;
        } else {
            return false;
        }
    }

    public Bitmap getCrossName() {
        JNIString strName = new JNIString();
        NaviRun.GetNaviRunObj().JNI_Java_GetName(strName);
        String strCrossName = strName.getM_StrAddressString();
        return this.guide_Info.getCrossName(strCrossName);
    }

    public Bitmap getBackBmp() {
    	if(guide_Info == null)
    	{
    		return null;
    	}
        return guide_Info.getBackBmp();
    }

    public int getBackWidth() {
        return this.guide_Info.getBackWidth();
    }

    public int getBackHeight() {
        return this.guide_Info.getBackHeight();
    }

    public int getCrossNameWidth() {
        return this.guide_Info.getCrossNameWidth();
    }

    public int getCrossNameHeight() {
        return this.guide_Info.getCrossNameHeight();
    }

//Add 2011/10/01 Z01_h_yamada Start -->
    public int getCrossNamePosX() {
        return this.guide_Info.getCrossNamePosX();
    }

    public int getCrossNamePosY() {
        return this.guide_Info.getCrossNamePosY();
    }

//Add 2011/10/01 Z01_h_yamada End <--

    public Bitmap getCompassBMP() {

        return guide_Info.getCompassBMP(getIconInfo());
    }

    public Java_IconInfo getIconInfo() {
        return guide_Info.getIconInfo();
    }

    public int getCommpassBmpWidth() {
        return guide_Info.getCompassBmpWidth();
    }

    public int getCommpassBmpHeight() {
        return guide_Info.getCompassBmpHeight();
    }

    // xuyang add start #1662
    @Override
    public void onShutDown(Intent oIntent) {
        onExitApp();
    }

    // xuyang add end #1662

//Add 2011/07/27 Z01thedoanh (自由解像度対応) Start -->
    public float getGuideHeightScale() {
        return guide_Info.getGuideHeightScale();
    }

//Add 2011/07/27 Z01thedoanh (自由解像度対応) End <--

//Add 2011/06/22 Z01thedoanh Start -->　VPLogger.txtをリストで再生するため
    private void initEventHandler() {
        mHandler = new Handler() {
            public void handleMessage(final Message msg) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
    				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
                switch (msg.what) {
                    case EXIT_APP_FROM_OUTSIDE:
                   Activity act = getActivity();
                   boolean RepeatVPLoggerStarting = false;
                   if (act != null) {
                       RepeatVPLoggerStarting = act.getIntent().getBooleanExtra("RepeatVPLoggerOfParam", false);
                       NaviLog.d(NaviLog.PRINT_LOG_TAG,"[DEBUG] RepeatVPLoggerStarting = " + RepeatVPLoggerStarting);
                   }
                   if (RepeatVPLoggerStarting) {
                       Intent intentUPDstart = new Intent();
                       intentUPDstart.setClassName("jp.co.cslab.its", "jp.co.cslab.its.RepeatVPLogger");
                       intentUPDstart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                       NaviLog.d(NaviLog.PRINT_LOG_TAG,"[DEBUG] START other Application");
                       act.startActivity(intentUPDstart);
                       act.finish();
                       onExitApp();
                   }
                   break;
               default:
                   break;
           }
       }
        };
    }

    public static void onExitAppFromOutSide() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                msg.what = EXIT_APP_FROM_OUTSIDE;
                if (mHandler != null) {
                    mHandler.sendMessage(msg);
                }
            }
        }).start();
    }

    private Activity getActivity() {
        return (Activity)this;
    }

//Add 2011/06/22 Z01thedoanh End <--
//Add 2011/09/16 Z01yoneya Start -->
    /*
     * アプリを終了する(ユーザ操作ではなく、電源スリープ時の呼び出し用)
     *
     * アプリプロセスは終了するが、サービスプロセスは終了しない。
     * 主に電源スリープ時などの対応。
     *
     */
    public void exitApp() {
        try {
//Chg 2011/10/11 Z01yoneya Start -->
//            NaviAppStatus.setAppFinishingFlag(true);
//            setResult(Constants.RESULTCODE_FINISH_ALL);
//            BaseActivityStack actStack = ((NaviApplication)getApplication()).getBaseActivityStack();
//            Activity topActivity = actStack.getTopActivity();
//            NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapActivity::exitApp() topActivity=" + topActivity + " isOpenMap=" + isOpenMap);
//            if (topActivity.equals(this) && !isOpenMap) {
//                onExitApp();
//                finish();
//            } else {
//            actStack.exitAllActivity();
//            }
//------------------------------------------------------------------------------------------------------------------------------------
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapActivity::exitApp()");
           //アプリ実行状態の場合のみ、終了可能とする
            if (!NaviAppStatus.isAppRunning()) {
                return;
            }

            try {
                NaviAppStatus.setAppStatus(NaviAppStatus.APP_STATE_STOPPING);
            } catch (IllegalStateException e) {
                NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapActivity::exitApp() setAppStatus exception!");
                return;
            }

//Chg 2011/11/23 Z01_h_yamada Start -->
//            MapActivity.setStopServiceFlag(false);
//------------------------------------------------------------------------------------------------------------------------------------
            CommonLib.setStopServiceFlag(false);
//Chg 2011/11/23 Z01_h_yamada End <--

            BaseActivityStack actStack = ((NaviApplication)getApplication()).getBaseActivityStack();
            actStack.exitAllActivity();
//Chg 2011/10/11 Z01yoneya End <--
        } catch (IllegalStateException illegalState) {
            onExitApp();
            finish();
        } catch (NullPointerException illegalState) {
            onExitApp();
            finish();
        } catch (Exception e) {
            onExitApp();
            finish();
        }
    }

//Add 2011/09/16 Z01yoneya End <--

//Add 2011/09/16 Z01kkubo Start --> 「車モード」「歩行者モード」アイコン長押しで自車位置移動(デバッグ用)
    /**
     * 車モード,歩行者モードアイコン長押しで自車位置移動
     */
    public void moveVehicle() {
        JNITwoLong twoLong = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(twoLong);
        NaviRun.GetNaviRunObj().JNI_NE_AjustMyPosi(twoLong.getM_lLong(), twoLong.getM_lLat());

        showLocusMap();
    }

//Add 2011/09/16 Z01kkubo End <--

    public void updateCarPosition() {
        JNITwoLong Coordinate = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_CT_Drv_GetVehiclePosition(Coordinate);
//        if (driverContentsCtrl != null) {
//            driverContentsCtrl.updateCarPosition(Coordinate.getM_lLat(), Coordinate.getM_lLong());
//        }

// Add by CPJsunagawa '13-12-25 Start
//        if (vicsManager != null) {
//            vicsManager.updatePosition(Coordinate);
//        }
        
        if (CommonLib.isNaviMode())
        {
	        NaviApplication app = (NaviApplication)getApplication();
	        double dist = CommonInquiryOps.getDistanceFromAngleDiff(
	        		Coordinate.getM_lLat(), Coordinate.getM_lLong(), 
	        		app.getCloseUpLatitude(), app.getCloseUpLongitude());
	        //if (dist < 100.0)
	        if (dist < Constants.CLOSE_UP_DISTANCE)
	        {
	        	// クローズアップする
	        	ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
	        	try
	        	{
	        		// 10mにクローズアップする。
// Mod by CPJsunagawa '2015-06-04 Start
	        		oNaviApi.setZoom(ZoomLevel.M_10);
//	        		oNaviApi.setZoom(ZoomLevel.M_40);
// Mod by CPJsunagawa '2015-06-04 End
	        		
	        		// 現在地モードにする。
	        		if (mapView.isMapScrolled()) {
	                    OpenMap.locationBack(this);
	                    goLocation();
	                }        	
	        		
	        		// 即クリアする。
	        		app.clearCloseUpPoint();
	        	}
	        	catch (NaviException e)
	        	{
	        		e.printStackTrace();
	        	}
	        	
	        	//　リストを表示する。
	        	showList(true, false, false);
	        }
        }		
// Add by CPJsunagawa '13-12-25 End

//        if (((NaviApplication)getApplication()).isProbeOperation() && vicsManager != null) {
//        	vicsManager.updatePosition(Coordinate);
//        }
    }

//    public void release() {
//        if (driverContentsCtrl != null) {
//            driverContentsCtrl.release();
//        }
//    }

// Add 2011/10/11 katsuta Start -->
    public void showWaitDialog(){
// Add 2011/10/15 katsuta Start -->

// Add 2011/10/19 katsuta Start -->
    	if(mCustomDlg != null) {
	    	if(mCustomDlg.isShowing()){
	    		NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity Dialog is Showing");
	    		return;
	    	}
    	}
//        if (driverContentsCtrl != null) {
//	    	if (DriverContentsCtrl.getController().isShowDialog()) {
//	    		// ドライブコンテンツメニューが表示されている
//	    		NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity Dialog is Showing");
//	    		return;
//	    	}
//        }
// Add 2011/10/19 katsuta End <--

    	if (!bCreated) {
    		return;
    	}
// Add 2011/10/15 katsuta End <--
        this.showDialog(Constants.DIALOG_WAIT);
    }

    public void removeWaitDialog(){
// Chg 2011/10/15 katsuta Start -->
//		this.removeDialog(Constants.DIALOG_WAIT);
    	if (bDispWaitDialog) {
    		this.removeDialog(Constants.DIALOG_WAIT);
    		bDispWaitDialog = false;
    	}
// Chg 2011/10/15 katsuta End <--
    }
// Add 2011/10/11 katsuta End <--

//    public void getDriveContentsData() {
//        if (driverContentsCtrl != null) {
//            DriverContentsCtrl.getController().sendEventCode(Constants.DC_GET_CONTENTS, this);
//        }
//    }
//// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
//    public void confirmNotification() {
//    	NotificationCtrl noticeCtrl = NotificationCtrl.getController();
//        if ( noticeCtrl != null ) {
//        	noticeCtrl.updateActivity( this );
//// MOD.2013.07.22 N.Sasao 設定メニューのお知らせは問い合わせをしない START
//        	noticeCtrl.showInformationDialog( true );
//// MOD.2013.07.22 N.Sasao 設定メニューのお知らせは問い合わせをしない  END
//        }
//    }
//// ADD.2013.07.22 N.Sasao お知らせ機能実装  END

// Add 2011/10/19 katsuta Start -->
	@Override
	public void onDismiss(DialogInterface dialog) {
//		NaviLog.i(NaviLog.PRINT_LOG_TAG, "MapActivity onDismiss");
		mCustomDlg = null;
	}


//Add 2012/04/26 Z01hirama Start --> #4417
    /**
     * ルート探索失敗ダイアログのリスナークラス
     */
	private class RouteFailureDialogListener implements  OnClickListener,
														OnCancelListener
	{
		CustomDialog mDialog = null;

		public RouteFailureDialogListener(CustomDialog dialog) {
			mDialog = dialog;
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			BeforeCloseDialogProc();
		}

		@Override
		public void onClick(View v) {
			BeforeCloseDialogProc();
	        mDialog.dismiss();
		}

		private void BeforeCloseDialogProc() {
//Add 2012/04/12 Z01hirama Start --> #4223
	        if(mDoEndBicycleNavi) {
	        	clearBicycleNaviData();
	        	mDoEndBicycleNavi = false;
	        }
//Add 2012/04/12 Z01hirama End <-- #4223

	        // NaviRun.GetNaviRunObj().JNI_ct_uic_RouteCalcAbort(NUI_MODE);
	        NaviRun.GetNaviRunObj().JNI_CT_UIC_RouteFailerProc();
	        // 980 bug add start
	        if (true == isManBackupRoute) {
	            CommonLib.setNaviMode(Constants.NE_NAVIMODE_MAN);
	        } else if (true == isCarBackupRoute) {
	            CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
	        }
	        // 980 bug add end
	        if (0 == NaviRun.GetNaviRunObj().JNI_ct_uic_ShowRestoreRoute()) {
	            bIsRestoreRoute = true;
	            mapView.onNaviModeChange();
	            CommonLib.setIsCalcFromRouteEdit(false);
	        } else {
	            CommonLib.setNaviStart(false);
	            CommonLib.setHasRoute(false);
	            CommonLib.setBIsRouteDisplay(false);
	            if (true == CommonLib.getIsCalcFromRouteEdit()) {
	                CommonLib.setIsCalcFromRouteEdit(false);
	                NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);
	                showLocusMap();
	                NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
	                mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
	            }
	        }
	        NaviRun.GetNaviRunObj().JNI_NE_MP_SetOffRoute(0);
	        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
		}
	}


    /**
     * 案内再開確認ダイアログのキャンセルリスナークラス
     */
	private class NavigationAgainDialogCancelListener implements OnClickListener,
																 OnCancelListener
	 {
		CustomDialog mDialog = null;

		public NavigationAgainDialogCancelListener(CustomDialog dialog) {
			mDialog = dialog;
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			BeforeCloseDialogProc();
		}

		@Override
		public void onClick(View v) {
			mDialog.dismiss();
			BeforeCloseDialogProc();
		}


		private void BeforeCloseDialogProc() {
//Chg 2011/09/26 Z01yoneya Start -->
//          CommonLib.deleteNaviDataFile();
//------------------------------------------------------------------------------------------------------------------------------------
            CommonLib.deleteNaviDataFile(((NaviApplication)getApplication()).getNaviAppDataPath());
//Chg 2011/09/26 Z01yoneya End <--
		}
	 }


    /**
     * 徒歩ルート距離オーバーダイアログキャンセルリスナー
     */
	private class WalkRouteTooLongDialogCancelListener implements OnClickListener,
																  OnCancelListener
	{
		CustomDialog mDialog = null;

		public WalkRouteTooLongDialogCancelListener(CustomDialog dialog) {
			mDialog = dialog;
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			BeforeCloseDialogProc();
		}

		@Override
		public void onClick(View v) {
			mDialog.dismiss();
			BeforeCloseDialogProc();
		}

		private void BeforeCloseDialogProc() {
            if (CommonLib.isNaviStart()) {
                NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);
                showLocusMap();
                NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
                CommonLib.setBIsDisplayGuideInfo(false);
            }
		}
	}


    /**
     * ルート検索中ダイアログキャンセルリスナー
     */
	private class RouteSearchDialogCancelListner implements OnClickListener,
															OnCancelListener
	{
		CustomDialog mDialog = null;

		public RouteSearchDialogCancelListner(CustomDialog dialog) {
			mDialog = dialog;
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			BeforeCloseDialogProc();
		}

		@Override
		public void onClick(View v) {
			BeforeCloseDialogProc();
		}

		private void BeforeCloseDialogProc() {
	        synchronized (NaviRun.GetNaviRunObj()) {
//Add 2012/04/17 Z01hirama Start --> #4267
	        	if(bIsSearchDialogShow) {
//Add 2012/04/17 Z01hirama End <-- #4267
	                if (!bIsCalculateCancle) {
	                    bIsCalculateCancle = true;
	                    if (NaviRun.isRouteCalculateFinished()) {
	                        return;
	                    }
	                    NaviRun.setRouteCalculateFinished(false);
	                    CommonLib.setIsStartCalculateRoute(false);
	                    cancelRouteSearch();
	                    if (0 == NaviRun.GetNaviRunObj().JNI_ct_uic_ShowRestoreRoute()) {
	                        bIsRestoreRoute = true;
	                    } else {
	                        if (!CommonLib.IsOpenMap()) {
	                            mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
	                        }
	                    }
	                    mDialog.dismiss();
//Add 2012/04/17 Z01hirama Start --> #4267
	                    bIsSearchDialogShow = false;
//Add 2012/04/17 Z01hirama End <-- #4267
	                }
//Add 2012/04/17 Z01hirama Start --> #4267
	        	}
//Add 2012/04/17 Z01hirama End <-- #4267
	        }
		}
	}
//Add 2012/04/26 Z01hirama End <-- #4417

// Add by CPJsunagawa '13-12-25
    // RouteEdit.javaからコピー
// Cut by M.Suna 2013/10/18
//    private static final String CarMode = "0"; // 車モード
//    private static final int NE_CarNaviSearchProperty_Commend = 1;// 推奨   画面位置index=0
    public void determineVehicleRouteFirst()
    {
    	NaviApplication na = (NaviApplication)getApplication();
    	//if (na.getMatchigMode() != 0) return;
    	
    	determineVehicleRoute(0, 1);
    	//NaviApplication na = (NaviApplication)getApplication();
    	na.setNowRoutingCustomerIndexAsFirstNoDeliv();
    	//determineVehicleRoute(0, 2);
    }
    
    public void determineVehicleRoute(int startIndex)
    {
    	if (startIndex == NaviApplication.FIRST_ONLY_ROUTE_FLAG)
    	{
    		determineVehicleRouteFirst();
    	}
    	else
    	{
    		determineVehicleRoute(startIndex, -1);
    	}
    }
    
    public void determineVehicleRoute(int startIndex, int endIndex) {
        // 引数チェックとIndexを求める
        NaviApplication app = (NaviApplication)getApplication();
        final int listSize = app.getRouteListSize();
        
        m_oTopView.enableTapOrderNaviMode(true); // タップ順表示モードをenable
        
        // 引数のインデックスチェック
        if(startIndex >= listSize) { 
        	//m_oTopView.enableTapOrderNaviMode(false); // タップ順表示モードをunenable
            return;
        }

        // 現在地のみであった場合も処理を行わない。
        if (listSize <= 1)
        {
        	// 折角なので、配達完了のメッセージを出してみる。
        	final CustomDialog oDialog = new CustomDialog(this);
        	oDialog.setTitle(R.string.delivery_end_msg_title);
        	oDialog.setMessage(this.getResources().getString(R.string.delivery_end_msg));
            oDialog.addButton(this.getResources().getString(R.string.btn_ok), new OnClickListener() {

                @Override
                public void onClick(View v) {
                	oDialog.dismiss();
                }
            });
        	oDialog.show();
        	return;
        }
        
        //m_oTopView.enableTapOrderNaviMode(true); // タップ順表示モードをenable
        
        //int lastIndex = listSize -1;
        int lastIndex = (endIndex != -1) ? endIndex : listSize - 1;
        
        if( lastIndex > startIndex + 7) {
            lastIndex = startIndex + 7;
        }
        
        m_oTopView.enableTapOrderNaviMode(true); // タップ順表示モードをenable
        
        // Activity開始時にタップオーダー順リストが0であるケースは弾いているので、その前提
        JNIInt JIMode = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetNaviMode(JIMode);
        String strMode = String.valueOf((JIMode.getM_iCommon() - 1));
        final String strSearchProperty = "1"; // 初回は推奨ルートで
        JNITwoLong Coordinate = new JNITwoLong();
        Constants.FROM_ROUTE_CAL = true;
        JNIInt GuideState = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_Java_GetGuideState(GuideState);
        if (GuideState.getM_iCommon() != 0) {
            NaviRun.GetNaviRunObj().JNI_NE_StopGuidance();
        }
        for (int i = 0; i < Constants.NAVI_DEST_INDEX; i++) {
            int NE_RoutePointType_Start = 1;
            NaviRun.GetNaviRunObj().JNI_NE_DeleteRoutePoint(
                    NE_RoutePointType_Start + i);
        }
        // 目的地のindext
        int iRoutePointType_Goal = Constants.NAVI_DEST_INDEX;
        // 出発地のindext
        int iRoutePointType_Start = Constants.NAVI_START_INDEX;
        long Longitude = 0;
        long Latitude = 0;
        String PointName = null;
        int iPassed = 0;
        for (int i = Constants.NAVI_START_INDEX; i <= Constants.NAVI_DEST_INDEX; i++) {
            JNITwoLong positon = new JNITwoLong();
            JNIString JNIpositonName = new JNIString();
            JNIInt bPassed = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(i, positon,
                    JNIpositonName, bPassed);
        }

        String strLon = app.getRouteInfo(startIndex).getLon();
        String strLat = app.getRouteInfo(startIndex).getLat();
        String strName = app.getRouteInfo(startIndex).getName();
        
        iPassed = app.getRouteInfo(startIndex).getIpass();
        if (strName.contains(getResources().getString(R.string.route_start_point_name))) {
            NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);
            strLon = String.valueOf(Coordinate.getM_lLong());
            strLat = String.valueOf(Coordinate.getM_lLat());
        }
        // 開始地点
        NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(iRoutePointType_Start,
                Long.parseLong(strLon), Long.parseLong(strLat),
                strName, iPassed);

        // 目的地点
        Longitude = Long.parseLong(app.getRouteInfo(lastIndex).getLon());
        Latitude = Long.parseLong(app.getRouteInfo(lastIndex).getLat());
        PointName = app.getRouteInfo(lastIndex).getName();
        iPassed = app.getRouteInfo(lastIndex).getIpass();
        NaviRun.GetNaviRunObj()
                .JNI_NE_SetRoutePoint(iRoutePointType_Goal, Longitude,
                        Latitude, PointName, iPassed);
        
        // 目的地点をクローズアップ地点にする。
        app.setCloseUpPoint(Latitude, Longitude);
        
        // 経由地
        for (int i = startIndex + 1, j=0; i < lastIndex; i++, j++) {
            Longitude = Long.parseLong(app.getRouteInfo(i).getLon());
            Latitude = Long.parseLong(app.getRouteInfo(i).getLat());
            PointName = app.getRouteInfo(i).getName();
            iPassed = app.getRouteInfo(i).getIpass();
            NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(j + 3, Longitude,
                    Latitude, PointName, iPassed);
        }

        if (strMode.equals(CarMode)) {
            int temp = NE_CarNaviSearchProperty_Commend;
            NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(
                    iRoutePointType_Start, iRoutePointType_Goal,
                    temp);
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(
                    iRoutePointType_Goal, 1/*Integer.parseInt(strSearchProperty)*/);
        }
        CommonLib.SEARCHP_ROPERTY = strSearchProperty;
        NaviRun.GetNaviRunObj().JNI_NE_SetNaviMode(
                Integer.parseInt(strMode) + 1);
        /*int NE_NEActType_RouteCalculate = 5;
        int NE_NETransType_NormalMap = 0;
        byte bScale = 0;
        long lLongitude = 0;
        long lLatitude = 0;
        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(
                NE_NEActType_RouteCalculate, NE_NETransType_NormalMap, bScale,
                lLongitude, lLatitude);
        NaviRun.GetNaviRunObj().JNI_CT_ExposeMap();
        NaviRun.GetNaviRunObj().JNI_ct_ExposeMap();
        CommonLib.setIsStartCalculateRoute(true);*/
       // app.setTapRouteStartPos(startIndex); // 開始位置の保存
        
        // RESULTCODE_ROUTE_DETERMINATIONから移植
        mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
//          }
        int NE_NEActType_RouteCalculate = 5;
        int NE_NETransType_NormalMap = 0;
        byte bScale = 0;
        long lLongitude = 0;
        long lLatitude = 0;
        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(
                NE_NEActType_RouteCalculate, NE_NETransType_NormalMap, bScale,1,
                lLongitude, lLatitude,0);
        NaviRun.GetNaviRunObj().JNI_CT_ExposeMap();
        NaviRun.GetNaviRunObj().JNI_ct_ExposeMap();
        CommonLib.setIsStartCalculateRoute(true);
        //Log.d("sakuraproblem", "showRouteSearchWaitingDialog 7");
        showRouteSearchWaitingDialog();
        mapView.updateMapCentreName(null);
        CommonLib.setIsCalcFromRouteEdit(true); //if calculate route fail, return car position
        CommonLib.setNaviStart(false);
        //showList(true);

// Add by CPJsunagawa '2015-07-08  Start
        /*
        // 独自アイコン（落雷ポイント）の描画 　事務所
        double latitude = 0;
        double longitude = 0;
        String iconPath = null;
        NaviAppDataPath naviPath = app.getNaviAppDataPath();
        iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder1.png";
        System.out.println("iconPath => " + iconPath );
        latitude = 35.649062;
        longitude = 139.715014;
        onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル1");
        */
// Add by CPJsunagawa '2015-07-08  End
        
// Cut by CPJsunagawa '2015-06-22 Start
// Add CPJ 2015-06-07 Start
         // 独自アイコン（落雷ポイント）の描画
/*        
         double latitude = 0;
         double longitude = 0;
         String iconPath = null;
         NaviAppDataPath naviPath = app.getNaviAppDataPath();
         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder1.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.530502;
         longitude = 139.542565;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル1");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder1.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.529545;
         longitude = 139.544093;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル1");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder1.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.529538;
         longitude = 139.541072;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル1");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder1.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.531522;
         longitude = 139.539776;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル1");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder2.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.525920;
         longitude = 139.536591;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル2");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder2.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.527275;
         longitude = 139.534265;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル2");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder2.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.527604;
         longitude = 139.536806;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル2");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder2.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.528051;
         longitude = 139.538445;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル2");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder2.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.527303;
         longitude = 139.539655;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル2");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder3.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.546761;
         longitude = 139.551061;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル3");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder3.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.548339;
         longitude = 139.547799;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル3");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder3.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.550700;
         longitude = 139.549893;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル3");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder3.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.550623;
         longitude = 139.554674;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル3");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder3.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.549834;
         longitude = 139.559077;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル3");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder3.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.546433;
         longitude = 139.558785;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル3");

         iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder3.png";
         System.out.println("iconPath => " + iconPath );
         latitude = 35.545644;
         longitude = 139.553996;
         onDrawUserIcon(latitude, longitude, iconPath, "落雷レベル3");
 */        
         // 独自ライン（管路データ）の描画
         double latitude_s = 0;
         double longitude_s = 0;
         double latitude_e = 0;
         double longitude_e = 0;
         
         List<GeoPoint> geoPoints = new ArrayList<GeoPoint>();
         // ラインの始点～終点の緯度経度
         latitude_s = 35.530502;
         longitude_s = 139.542565;
         latitude_e = 35.530552;
         longitude_e = 139.542515;
         geoPoints.add(new GeoPoint(latitude_s, longitude_s));
         geoPoints.add(new GeoPoint(latitude_e, longitude_e));

         // 一次的にコメント
//         onDrawUserLine("1", geoPoints);
         
         // 検証用コード
//         drawLine();
// Add CPJ 2015-06-07 End
// Cut by CPJsunagawa '2015-06-22 End
         
// Add by CPJsunagawa 2015-08-08 Start 
         // DBに保存したアイコンをナビ上に表示する
         ExecuteSetUserIconInfo();
         ExecuteShowRegistIcon();
// Add by CPJsunagawa 2015-08-08 End

    }
    
    private boolean showFlag = false;
    
    public boolean getShowFlag() {
        return showFlag;
    }
    
    // 配送先リストを隠す
	public void hideList(){
		showList(false, false, false);
	}

/*	
	// 配送先リストを出す
	public void showList(){
		showList(true);
	}
	
	public void showList(boolean isShow)
	{
		showList(isShow, false);
	}
	
	public void showListPageZero()
	{
		showList(true, true);
	}
*/	
    
    // 配送コースのボタン選択後の配送先リスト表示の処理　by　M.Suna　'13-09-26
	private int mSelectedIndex = 0;
	private int mNowPage = 0;
    private View mSelectedView = null;
	//private LinearLayout mSelectedLL = null;
    public void showList(boolean isShow, boolean isResetPage, boolean isSetScrollView) {
        System.out.println("showList(" + isShow + ")" );
        
        LinearLayout ll = (LinearLayout)findViewById(R.id.infoList);
        if( ll == null ) return;
        showFlag = isShow;
        if( !isShow) {
// add mitsuoka
        	showPhotoView(false, null);
// add mitsuoka
            ll.setVisibility(View.GONE);
            return;
        }
        
        NaviApplication app = (NaviApplication)getApplication();
        if (!app.isExistInfoList())
        {
            ll.setVisibility(View.GONE);
            return;
        }
        
        // スクロールビューの位置を頭に持って行く
        ScrollView sv = (ScrollView)findViewById(R.id.infoListSV);
        if (isSetScrollView)
        {
	        //ScrollView sv = (ScrollView)findViewById(R.id.infoListSV);
	        sv.fullScroll(ScrollView.FOCUS_UP);
        }
        
        ll.setVisibility(View.VISIBLE);
        ll.removeAllViews();

        //NaviApplication app = (NaviApplication)getApplication();
        final int listSize = app.getInfoListSize();
        
        LayoutInflater Inflater = LayoutInflater.from(this);
        int textCol;
        
        int firstNoDelivIndex = app.getFirstNoDeliveryIndex();
        if (isResetPage)
        {
        	//mNowPage = 0;
        	//int firstNoDelivIndex = app.getFirstNoDeliveryIndex();
        	mNowPage = app.getPageFromIndex(firstNoDelivIndex);
        }
        
        int startIdx = mNowPage * NaviApplication.ONE_PAGE_ITEM_COUNT;
        int endIdx = (mNowPage + 1) * NaviApplication.ONE_PAGE_ITEM_COUNT - 1;
        if (endIdx >= listSize) endIdx = listSize - 1;

        //for( int i = 0; i < listSize; i++ ) {
        TableLayout proTL;
        String[] onePros, oneLineCols;
        int j;
        final int WC = ViewGroup.LayoutParams.WRAP_CONTENT; 
        final int FP = ViewGroup.LayoutParams.FILL_PARENT;
        nWC = WC;
        nFP = FP;
        TableRow oneRow;
        TextView ptv;
//        Button stBtn/*, rtBtn*/, detailBtn;
//        Button stBtn/*, rtBtn*/, detailBtn, cameraBtn, tegakiBtn, photoBtn;
        Button stBtn/*, rtBtn*/, detailBtn, cameraBtn, tegakiBtn, photoBtn, capBtn;
//        Button stBtn/*, rtBtn*/, detailBtn, cameraBtn, tegakiBtn;
        View.OnClickListener stOcl = new View.OnClickListener() {
            // 配達ステータスのボタンは、未配達→配達済み、保留→未配達にするだけとする。
			@Override
			public void onClick(View v) {
				NaviApplication app = (NaviApplication)getApplication();

// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
		        DebugLogOutput.put("RakuRaku-Log2: ステータス更新ボタンクリック");
//Add 2015-05-28 CPJsunagawa SDカードにログ出力 end
				if (app.IsManualMatching())
				{
//					
//					AlertDialog.Builder adb = new AlertDialog.Builder(MapActivity.this); 
//					adb.setMessage("只今手動マッチング中なので、ステータスの変更は出来ません。");
//					adb.setPositiveButton("OK", null);
//					adb.show();
//					

// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
			        DebugLogOutput.put("RakuRaku-Log2: ステータス更新ボタンクリックに来てるけどモードが手動マッチングのままなので、return");
// Add 2015-05-28 CPJsunagawa SDカードにログ出力 end
					return;
				}
				
				int selIdx = ((Integer)(v.getTag())).intValue();
				//NaviApplication app = (NaviApplication)getApplication();
				DeliveryInfo info =  app.getInfo(selIdx);

// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
		        DebugLogOutput.put("RakuRaku-Log2: ステータス更新ボタンクリック  selIdx ->" + String.valueOf(selIdx));
//Add 2015-05-28 CPJsunagawa SDカードにログ出力 end
		        
				if (info.mDeliveryStatus == DeliveryInfo.DELIVERY_STATUS_NOTDELIVERED)
				{
					// 未配達→配達済み
					info.mDeliveryStatus = DeliveryInfo.DELIVERY_STATUS_DELIVERED;
				}
				else if (info.mDeliveryStatus == DeliveryInfo.DELIVERY_STATUS_DELIVERED)
				{
					// 配達済み→未配達
					info.mDeliveryStatus = DeliveryInfo.DELIVERY_STATUS_NOTDELIVERED;
				}
				else if (info.mDeliveryStatus == DeliveryInfo.DELIVERY_STATUS_RESERVED)
				{
					// 保留→未配達
					info.mDeliveryStatus = DeliveryInfo.DELIVERY_STATUS_NOTDELIVERED;
				}
				else
				{
					// 配達中止は常にNOP
					return;
				}
				
				// DBを変える。
            	DbHelper helper = new DbHelper(MapActivity.this);
            	SQLiteDatabase db = helper.getWritableDatabase();
            	db.execSQL("UPDATE ProductInfo Set DeliveryStatus = " + info.mDeliveryStatus + " WHERE CustCode = '" + info.mCustCode + "'");
            	db.close();
            	helper.close();
				
// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
		        DebugLogOutput.put("RakuRaku-Log2: ステータス更新ボタンクリック  DB変更OK");
//Add 2015-05-28 CPJsunagawa SDカードにログ出力 end
		        
				// キャプションを変える
				//Button stBtn = (Button)v;
				//String[] cap = getResources().getStringArray(R.array.deliverystate);
				//stBtn.setText("【" + cap[info.mDeliveryStatus] + "】");
            	setStatusButton((Button)v, info.mDeliveryStatus);
				reflectDeliveredCount();
				
// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
		        DebugLogOutput.put("RakuRaku-Log2: ステータス更新ボタンクリック  キャプション変更OK");
//Add 2015-05-28 CPJsunagawa SDカードにログ出力 end
		        
// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
		        DebugLogOutput.put("RakuRaku-Log2: ステータス更新ボタンクリック  リルート開始");
//Add 2015-05-28 CPJsunagawa SDカードにログ出力 end

		        // リルートを行う。
				//StartDeliveryReroute sdr = new StartDeliveryReroute();
				//sdr.start();
				CommonInquiryOps.updateRouteData(MapActivity.this);
				//app.setTapRouteStartPos(0);
				app.setRoutePageFirstOnly();
				determineVehicleRouteFirst();

// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
		        DebugLogOutput.put("RakuRaku-Log2: ステータス更新ボタンクリック  リルート終了");
//Add 2015-05-28 CPJsunagawa SDカードにログ出力 end
		        
				//determineVehicleRoute(0);
				//showListPageZero();
				//app.setSetNoDelivFirst();
				// Mod by CPJsunagawa '14/06/10   修正箇所はここだけ。
				//showList(true, false, false);
				repairTopPosOfListBtn(false); // タップの都度位置を修正してみる。
				showList(true, true, false);
			}
		}, itemOcl = new View.OnClickListener() {
            public void onClick(View view) {
				NaviApplication app = (NaviApplication)getApplication();
				if (app.IsManualMatching())
				{
//					
//					AlertDialog.Builder adb = new AlertDialog.Builder(MapActivity.this); 
//					adb.setMessage("只今手動マッチング中なので、詳細表示は出来ません。");
//					adb.setPositiveButton("OK", null);
//					adb.show();
//					
					return;
				}
				
            	LinearLayout selectedLL = (LinearLayout)view;
            	String idxStr = (String)view.getTag();
            	//mSelectedIndex = ((Integer)(view.getTag())).intValue();
            	mSelectedIndex = Integer.parseInt(idxStr);
            	//showInfoDialog(mSelectedIndex, selectedLL);
            	setForDetail(mSelectedIndex, selectedLL);
            	repairTopPosOfListBtn(false); // タップの都度位置を修正してみる。
            }
        }/*, drawRoute = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int selIdx = ((Integer)(v.getTag())).intValue();
				NaviApplication app = (NaviApplication)getApplication();
				DeliveryInfo info =  app.getInfo(selIdx);
				
				ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
				oNaviApi.connect(MapActivity.this);
				try
				{
					oNaviApi.setZoom(ZoomLevel.M_20);
					oNaviApi.setCenter(GeoUtils.toWGS84(info.mPoint.getLatitudeMs(), info.mPoint.getLongitudeMs()));
				}
				catch (NaviException e)
				{
					e.printStackTrace();
					return;
				}
				calcRoute();
			}
		}*/
        , showDetail = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO 自動生成されたメソッド・スタブ
				Button selectedBtn = (Button)view;
				LinearLayout ll = (LinearLayout)selectedBtn.getTag();
				String intStr = (String)ll.getTag();
				int selIdx = Integer.parseInt(intStr);
				NaviApplication app = (NaviApplication)getApplication();
				DeliveryInfo info =  app.getInfo(selIdx);
				showInfoDialog(info, ll);
			}
		};;
        
        // マルチWindows幅対応 Start
        WindowManager wm = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
        Display disp = wm.getDefaultDisplay();
        final int listWidth = (int)(disp.getWidth() * 0.43f);
        final int winHeight = disp.getHeight();

        // 写真用layout作成
        if( mPhotoContainer == null ) { // 未作成なので作成する
            mPhotoContainer = (LinearLayout)Inflater.inflate(R.layout.photo_canvas, null);
            mPhotoContainer.setVisibility(View.GONE);
            LinearLayout infoContainer = (LinearLayout)findViewById(R.id.infoListContainer);
            infoContainer.addView(mPhotoContainer, new LinearLayout.LayoutParams(listWidth, LinearLayout.LayoutParams.MATCH_PARENT));
            initPhotoEdit();
        }
        // マルチWindows幅対応 End


        //int prevBtm = 0, oneHeight;
        for( int i = startIdx; i <= endIdx; i++ ) {
        	//oneHeight = 600;
            DeliveryInfo info =  app.getInfo(i);
            LinearLayout item = (LinearLayout)Inflater.inflate(R.layout.listrow_work_list, null);
            //ScrollView item = (ScrollView)Inflater.inflate(R.layout.listrow_work_list, null);
            TextView tv = (TextView)item.findViewById(R.id.infoIndex);
            textCol = getTextColorAsLevel(info.mMatchLebel);
            tv.setTextColor(textCol);
            tv.setText( Integer.toString(i + 1) );
            // 顧客名
            tv = (TextView)item.findViewById(R.id.infoCustomer);
            tv.setTextColor(textCol);
            tv.setText(info.mCustmer);
            // 顧客コードと配達順
            tv = (TextView)item.findViewById(R.id.infoCustCode);
            tv.setTextColor(textCol);
            tv.setText("（" + info.mCustCode + "）【" + info.mDeliOrder + "】");
            // 住所
            tv = (TextView)item.findViewById(R.id.infoAddress);
            tv.setTextColor(textCol);
            tv.setText(info.mAddress);
            // 電話番号
            tv = (TextView)item.findViewById(R.id.infoTelNo);
            tv.setTextColor(textCol);
            tv.setText(info.mTelNo);
            // 連絡メモ
            tv = (TextView)item.findViewById(R.id.infoMemo);
            tv.setTextColor(textCol);
            tv.setText(info.mContactingMemo);
///*            
            // 商品略称
//            tv = (TextView)item.findViewById(R.id.infoRyakuName);
//            tv.setTextColor(textCol);
//            tv.setText(info.mProductRyaku);
//*/      
            if (!"".equals(info.mProductRyaku))
            {
            	proTL = (TableLayout)item.findViewById(R.id.products);
            	onePros = info.mProductRyaku.split(",");
            	for (j = 0; j <= onePros.length - 1; j++)
            	{
            		oneLineCols = onePros[j].split(":");
            		
            		oneRow = new TableRow(this);
            		
            		ptv = new TextView(this);
            		ptv.setText(oneLineCols[0]);
            		ptv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
            		ptv.setTextColor(textCol);
                	oneRow.addView(ptv);
                	
            		ptv = new TextView(this);
            		ptv.setText("\t\t");
            		ptv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
            		ptv.setTextColor(textCol);
                	oneRow.addView(ptv);
                	
            		ptv = new TextView(this);
            		ptv.setText(oneLineCols[1]);
            		ptv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
            		ptv.setTextColor(textCol);
                	oneRow.addView(ptv);
                	
                	proTL.addView(oneRow, new TableLayout.LayoutParams(FP, WC));
                	
                	//oneHeight += 50;
            	}
            }
            		
            // 配達コメント
            tv = (TextView)item.findViewById(R.id.infoComment);
            tv.setTextColor(textCol);
            tv.setText(info.mDeliComment);
            // BOX位置
            tv = (TextView)item.findViewById(R.id.infoBoxPosition);
            tv.setTextColor(textCol);
            tv.setText(info.mBoxPlace);
            //
//*         
            // 図番号
            tv = (TextView)item.findViewById(R.id.infoWorkCode);
            tv.setTextColor(textCol);
            tv.setText(info.mMapNo);
///*            
//            tv = (TextView)item.findViewById(R.id.infoRyakuName);
//            tv.setTextColor(textCol);
//            tv.setText("宅配商品：" + info.mProduct);
//*/            
            // ルート引き
///*            
//            rtBtn = (Button)item.findViewById(R.id.btnDrawRoute);
//            rtBtn.setTag(new Integer(i));
//            rtBtn.setOnClickListener(drawRoute);
//*/            
            
            // 配達ステータス
            stBtn = (Button)item.findViewById(R.id.btnDeliveryStatus);
            //stBtn.setTextColor(textCol);
            //String[] cap = getResources().getStringArray(R.array.deliverystate);
            //stBtn.setText("【" + cap[info.mDeliveryStatus] + "】");
            setStatusButton(stBtn, info.mDeliveryStatus);
            stBtn.setTag(new Integer(i));
            stBtn.setOnClickListener(stOcl);
            
            //item.setTag( new Integer(i) ); // save
            item.setTag( "" + i ); // save
            //R.drawable.
            item.setBackgroundResource(getButtonBMPAsLevel(info.mMatchLebel));
            item.setOnClickListener(itemOcl);
            
            ll.addView(item,  new LinearLayout.LayoutParams(listWidth, LinearLayout.LayoutParams.MATCH_PARENT) );
            //item.u

/*
// Add by CPJsunagawa '2015-06-04 Start
            // 地図保存ボタン
            capBtn = (Button)item.findViewById(R.id.btnDispCap);
            setStatusButton2(capBtn, "地図保存");
            capBtn.setTag(new Integer(i));
            capBtn.setOnClickListener(stOcl);
// Add by CPJsunagawa '2015-06-04 End
*/
            
        	//info.mMyButtonPos = prevBtm;
            //prevBtm += item.getHeight();
            //prevBtm += oneHeight;
            //info.mMyButtonPos = item.getTop();
            if (i == firstNoDelivIndex)
            {
            	sv.smoothScrollTo(0, info.mMyButtonPos);
            }
            // 顧客詳細
            detailBtn = (Button)item.findViewById(R.id.btnDetails);
            detailBtn.setTag(item);
            detailBtn.setOnClickListener(showDetail);
            // 写真 Start
            photoBtn = (Button)item.findViewById(R.id.btnPhoto);
            photoBtn.setTag(item);
            photoBtn.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                	
                	// 撮影画像リストを表示する
//                	photoListDisp(view, FP, WC);
                	
                    // 起動された位置を記憶しておく
                    LinearLayout ll = (LinearLayout)view.getTag();
                    mSelectedIndex = Integer.parseInt(((String)(ll.getTag())));
                    mSelectedView = ll.findViewById(R.id.btnPhoto); // 写真ボタンの状態を更新する為
// 2015-02-22 Add Start	
                    mTargetIndex = Integer.parseInt(((String)(ll.getTag())));
                    mTargetBtn = ll.findViewById(R.id.btnPhoto); // 写真ボタンの状態を更新する為
// 2015-02-22 Add End	
                    NaviApplication app = (NaviApplication)getApplication();
                    DeliveryInfo info = app.getInfo(mSelectedIndex);
                    int num = app.getInfo(mSelectedIndex).mPhotoNum;
System.out.println("**** mPhotoNum => " + num);
                    final CustomDialog dlg = new CustomDialog(MapActivity.this);
                    dlg.setTitle(R.string.select_file);
                    dlg.EnableTitleIcon(false);  // タイトルのアイコンは消す
                    // リストを作る
                    ll = new LinearLayout(MapActivity.this);
                    ll.setOrientation(LinearLayout.VERTICAL);
                    for( int i = 0; i < num; i++ ) {
                        View layout = MapActivity.this.getLayoutInflater().inflate(R.layout.dialog_list, null);
                        TextView tv = (TextView)layout.findViewById(R.id.text_list_caption);
                        tv.setText(info.mPhotos[i]);
                        tv.setTag( new String(app.getNaviAppDataPath().getNaviAppRootPath() +  "/" + info.mPhotoPath + "/" + info.mPhotos[i]));
                        tv.setOnClickListener( new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                System.out.println("click");
                                String str = (String)view.getTag();
System.out.println("str = " + str);
                                showPhotoView(true, str );
                                dlg.dismiss();
                            }
                        });
                        ll.addView(layout, new LinearLayout.LayoutParams(FP, WC));
                    }   // for文の閉じカッコ
//                });
//                    }
                    ScrollView setView = new ScrollView(MapActivity.this);
                    setView.addView(ll, new LinearLayout.LayoutParams(FP, WC));
                    dlg.setContentView(setView);

                    dlg.addButton(MapActivity.this.getResources().getString(R.string.btn_cancel),new OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            dlg.dismiss();
                        }
                    });
                    dlg.show();

                }});
            // 写真 End

            // カメラ連携 Start
            cameraBtn = (Button)item.findViewById(R.id.btnCamera);
            cameraBtn.setTag(item);
            updatePhotoBtn(i, photoBtn);
            cameraBtn.setOnClickListener( new View.OnClickListener() {
                public void onClick(View view) {
                    // 起動された位置を記憶しておく
                    LinearLayout ll = (LinearLayout)view.getTag();
                    mSelectedIndex = Integer.parseInt(((String)(ll.getTag())));
                    mSelectedView = ll.findViewById(R.id.btnPhoto); // 写真ボタンの状態を更新する為
// 2015-02-22 Add Start
                    mTargetIndex = Integer.parseInt(((String)(ll.getTag())));
                    mTargetBtn = ll.findViewById(R.id.btnPhoto); // 写真ボタンの状態を更新する為
                    System.out.println("****** mTargetIndex => " + mTargetIndex);
                    System.out.println("****** mTargetBtn => " + mTargetBtn);

// 2015-02-22 Add End
                    NaviApplication app = (NaviApplication)getApplication();
                    // ファイル数のチェック
                    int num = app.getInfo(mSelectedIndex).mPhotoNum;
                    if( num == DeliveryInfo.PHOTO_MAX_NUM ) {
                        final CustomDialog dlg = new CustomDialog(MapActivity.this);
                        dlg.setTitle(R.string.warn_title);
                        dlg.setMessage(MapActivity.this.getResources().getString(R.string.warn_msg_photo_file_reach_max), Gravity.LEFT);
                        dlg.addButton(MapActivity.this.getResources().getString(R.string.btn_ok),new OnClickListener(){
                            @Override
                            public void onClick(View v) {
                                dlg.dismiss();
                            }
                        });
                        dlg.show();
                        return;
                    }
                    String photoPath = "/" + app.getInfo(mSelectedIndex).mPhotoPath; // Pathを取得する

                    Intent intent = new Intent();
                    intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    // 格納するフォルダが無ければ生成する
                    File file = new File(app.getNaviAppDataPath().getNaviAppRootPath() + photoPath );
                    file.mkdirs();
                    // URIを生成
                     file = new File(file.getPath() + "/" +  getCurrentTimeStr() + ".png" );
                    mPhotoPath = file.getPath(); // ファイルパスを保存
                    ContentValues values = new ContentValues();
                    values.put(Images.Media.TITLE, "title");
                    values.put(Images.Media.DISPLAY_NAME, file.getName());
                    values.put(Images.Media.MIME_TYPE, "image/png");
                    values.put(Images.Media.DATA, mPhotoPath);
                    values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis());
                    if (file.exists()) {
                        values.put(Images.Media.SIZE, file.length());
                    }
                    Uri uri = getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, values);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(intent, AppInfo.ID_ACTIVITY_IMAGE_CAPTURE);
                }
            });
            // カメラ連携 End
            // 手書きメモ Start
            tegakiBtn= (Button)item.findViewById(R.id.btnHandWrittenMemo);
            tegakiBtn.setTag(item);
            updateTegakiMemoBtn(i, tegakiBtn);
            tegakiBtn.setOnClickListener( new View.OnClickListener() {
                public void onClick(View view) {
// 2015-02-26
            		System.out.println("**** MapActivityの手書きメモStart ****"); 
                    // 起動された位置を記憶しておく
                    LinearLayout ll = (LinearLayout)view.getTag();
                    mSelectedIndex = Integer.parseInt(((String)(ll.getTag())));
                    mSelectedView = view;
                    NaviApplication app = (NaviApplication)getApplication();
                    DeliveryInfo info = app.getInfo(mSelectedIndex);
                    String memoPath = "/" + info.mHandWrittenMemoPath; // Pathを取得する
                    // 格納するフォルダが無ければ生成する
                    File file = new File(app.getNaviAppDataPath().getNaviAppRootPath() + memoPath );
                    file.mkdirs();
            		System.out.println("file ==> " + file);
            		System.out.println("mSelectedIndex).mHandWrittenMemo => " + app.getInfo(mSelectedIndex).mHandWrittenMemo);
            		System.out.println("info.mHandWrittenMemo => " + info.mHandWrittenMemo);
                    // 手書きメモが存在するか
                    // Mod 2015-02-26 文字列領域は、nullではなく””でクリアに統一
                    //if( app.getInfo(mSelectedIndex).mHandWrittenMemo == null ) {
                    if( "".equals(app.getInfo(mSelectedIndex).mHandWrittenMemo) ) {
                		System.out.println("app.getInfo(mSelectedIndex).mHandWrittenMemoがnull");
                        file = new File(file.getPath() + "/" +  getCurrentTimeStr() + ".png" );
                		System.out.println("file ==> " + file);
                    } else {
                		System.out.println("app.getInfo(mSelectedIndex).mHandWrittenMemoがnull以外");
                        file = new File(file.getPath() + "/" + info.mHandWrittenMemo);
                		System.out.println("file ==> " + file);
                    }

            		System.out.println("Intentで、TegakiMemoActivity呼び出し前");
                    Intent intent = new Intent(MapActivity.this, TegakiMemoActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("path", file.getPath());
                    intent.putExtras(bundle);
            		System.out.println("TegakiMemoActivityに、path => " + bundle + " を渡す");
                    startActivityForResult(intent, AppInfo.ID_ACTIVITY_TEGAKI_MEMO);
                }});
        }
        
        // 2ページ以上ある場合はページ送りのコントロールを入れる。
        int pageCnt = app.getInfoListPageCount();
        if (pageCnt >= 2)
        {
        	LinearLayout npLL = (LinearLayout)Inflater.inflate(R.layout.button_next_prev, null);
        	Button btn = (Button)npLL.findViewById(R.id.page_prev);
        	btn.setEnabled(mNowPage >= 1);
        	btn.setOnClickListener( new View.OnClickListener() {
                public void onClick(View view) {
                	if (mNowPage >= 1)
                	{
                		mNowPage--;
                		showList(true, false, true);
                		//repairTopPosOfListBtn();
                	}
                }});
        	
        	btn = (Button)npLL.findViewById(R.id.page_next);
        	btn.setEnabled(mNowPage <= pageCnt - 2);
        	btn.setOnClickListener( new View.OnClickListener() {
                public void onClick(View view) {
                	NaviApplication app = (NaviApplication)getApplication();
                	if (mNowPage <= app.getInfoListPageCount() - 2)
                	{
                		mNowPage++;
                		showList(true, false, true);
                		//repairTopPosOfListBtn();
                	}
                }});
        	
        	ll.addView(npLL);
        }
        
        //if (isShow) m_oTopView.showRightBar();
        //else m_oTopView.hideRightBar();
    }

	// Add 2015-02-06 Start
    /**
     * 撮影画像のリスト表示
     */
    private void photoListDisp(View view, final int nFP, final int nWC)
    {
        // 起動された位置を記憶しておく
        LinearLayout ll = (LinearLayout)view.getTag();
        mSelectedIndex = Integer.parseInt(((String)(ll.getTag())));
        NaviApplication app = (NaviApplication)getApplication();
        DeliveryInfo info = app.getInfo(mSelectedIndex);
        int num = app.getInfo(mSelectedIndex).mPhotoNum;
        final CustomDialog dlg = new CustomDialog(MapActivity.this);
        dlg.setTitle(R.string.select_file);
        dlg.EnableTitleIcon(false);  // タイトルのアイコンは消す
        // リストを作る
        ll = new LinearLayout(MapActivity.this);
        ll.setOrientation(LinearLayout.VERTICAL);
        for( int i = 0; i < num; i++ ) {
            View layout = MapActivity.this.getLayoutInflater().inflate(R.layout.dialog_list, null);
            TextView tv = (TextView)layout.findViewById(R.id.text_list_caption);
            tv.setText(info.mPhotos[i]);
            tv.setTag( new String(app.getNaviAppDataPath().getNaviAppRootPath() +  "/" + info.mPhotoPath + "/" + info.mPhotos[i]));
            tv.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println("click");
                    String str = (String)view.getTag();
                    showPhotoView(true, str );
                    dlg.dismiss();
                }
            });
            ll.addView(layout, new LinearLayout.LayoutParams(nFP, nWC));
        }   // for文の閉じカッコ
        ScrollView setView = new ScrollView(MapActivity.this);
        setView.addView(ll, new LinearLayout.LayoutParams(nFP, nWC));
        dlg.setContentView(setView);

        dlg.addButton(MapActivity.this.getResources().getString(R.string.btn_cancel),new OnClickListener(){
            @Override
            public void onClick(View v) {
                dlg.dismiss();
            }
        });
        dlg.show();
    }
 // Add 2015-02-06 End
    
    
    /**
     * ボタンのカラーを更新する。
     */
    public void updateListButtonColor()
    {
        LinearLayout ll = (LinearLayout)findViewById(R.id.infoList);
        if( ll == null ) return;
        
        View oneChild;
        String idxStr;
        int index;
        DeliveryInfo info;
        NaviApplication app = (NaviApplication)getApplication();
        for (int i = 0; i <= ll.getChildCount() - 1; i++)
        {
        	// Viewのタグに配達リストのインデックスを格納している。
        	oneChild = ll.getChildAt(i);
        	idxStr = (String)oneChild.getTag();
        	if (idxStr != null)
        	{
        		// ボタンのカラーを変更する。
	        	index = Integer.parseInt(idxStr);
	        	info = app.getInfo(index);
	        	oneChild.setBackgroundResource(getButtonBMPAsLevel(info.mMatchLebel));
	        	if (oneChild instanceof LinearLayout)
	        	{
	        		setTextColorAsLevel((LinearLayout)oneChild, info.mMatchLebel, info.mDeliveryStatus);
	        	}
        	}
        	//oneChild.
        }
    }
    
    private void setTextColorAsLevel(LinearLayout pal, int level, int status)
    {
    	View oneChild;
    	TextView tv;
    	for (int i = 0; i <= pal.getChildCount() - 1; i++)
    	{
    		oneChild = pal.getChildAt(i);
    		if (oneChild instanceof TextView)
    		{
    			tv = (TextView)oneChild;
    			if (tv instanceof Button)
    			{
    				setStatusButton((Button)tv, status);
    			}
    			else
    			{
    				tv.setTextColor(getTextColorAsLevel(level));
    			}
    		}
    		else if (oneChild instanceof LinearLayout)
    		{
    			setTextColorAsLevel((LinearLayout)oneChild, level, status);
    		}
    	}
    }
    
    public void setStatusButton(Button statusBtn, int status)
    {
    	setStatusButton(statusBtn, status, true);
    }
    
    public void setStatusButton(Button statusBtn, int status, boolean isSetSpace)
    {
		String[] cap = getResources().getStringArray(R.array.deliverystate);

		String text = cap[status];
    	statusBtn.setText(text);
		
		// 未配達は赤、保留はマゼンタにするので、文字カラーを白にする。
		if (status == DeliveryInfo.DELIVERY_STATUS_NOTDELIVERED ||
			status == DeliveryInfo.DELIVERY_STATUS_RESERVED)
		{
			statusBtn.setTextColor(0xFFFFFFFF);
			if (status == DeliveryInfo.DELIVERY_STATUS_NOTDELIVERED)
			{
				statusBtn.setBackgroundColor(0xFFFF0000);
			}
			else
			{
				statusBtn.setBackgroundColor(0xFFFF00FF);
			}
		}
		else
		{
			statusBtn.setTextColor(0xFF000000);
			statusBtn.setBackgroundColor(0xFFFFFFFF);
		}
		//reflectDeliveredCount();
    }

    private int getTextColorAsLevel(int level)
    {
    	boolean isWhite = (level == 2);
    	isWhite = isWhite || (level == 4);
    	//isWhite = isWhite || (level == 8);
    	//isWhite = isWhite || (level == 10);
    	
    	return isWhite ? Color.WHITE : Color.BLACK;
    }
    
    private int getButtonBMPAsLevel(int level)
    {
    	int result = R.drawable.btn_default;
    	
    	switch (level)
    	{
    	case 0: // 失敗
    		result = R.drawable.btn_list_lv0;
    		break;
    	case 2: // 都道府県
    		result = R.drawable.btn_list_lv2;
    		break;
    	case 4: // 市区町村
    		result = R.drawable.btn_list_lv4;
    		break;
    	case 5: // 大字
    		result = R.drawable.btn_list_lv5;
    		break;
    	case 6: // 字丁目
    		result = R.drawable.btn_list_lv6;
    		break;
    	case 7: // 街区
    		result = R.drawable.btn_list_lv7;
    		break;
    	case 8: // 地番戸番
    		result = R.drawable.btn_list_lv8;
    		break;
    	case 10: // マニュアルマッチング
    		result = R.drawable.btn_list_lv10;
    		break;
    	}
    	
    	return result;
    }
    
    public void showCaution() {
        MsgDialog dlg = new MsgDialog(this, "■道路情報\nエリアA1 ○○交差点は工事中のため通行できません。\n注意してください。\n\n■業務情報\nお客様クレーム情報の事前確認・報告の徹底をお願いします。\n\n2013/5/1 10：00", false);
        dlg.show();
    }
    
    //public void showInfoDialog(int idx, LinearLayout dispTV) {
    public void showInfoDialog(DeliveryInfo info, LinearLayout dispTV) {
        //NaviApplication app = (NaviApplication)getApplication();
        //final int listSize = app.getInfoListSize();
        //if( idx >= listSize) return; // fail safe
        //DeliveryInfo info =  app.getInfo(idx);
        //InfoDialog dlg = new InfoDialog(this, info.mAddress, info.mProduct, info.mLastDate, info.mClaim, idx);
        //InfoDialog dlg = new InfoDialog(this, info, idx, dispTV);
    	InfoDialog dlg = new InfoDialog(this, info, dispTV);
        dlg.show();
    }
    
    //private LinearLayout keptTV;
    //private DeliveryInfo keptDI;
    public void setForDetail(int idx, LinearLayout dispTV) {
        NaviApplication app = (NaviApplication)getApplication();
        final int listSize = app.getInfoListSize();
        if(idx >= listSize) return; // fail safe
        DeliveryInfo info =  app.getInfo(idx);
        
        int matMode = app.getMatchigMode();
        //if (info.mMatchLebel <= 7)
        if (matMode != 0)
        {
        	// 一気にマッチングモードに移行する。
        	//startManualMatching(info, info.mPoint.getLatitudeMs(), info.mPoint.getLongitudeMs());
        	//keptDI = info;
        	//keptTV = dispTV;
/*        	
        	DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener(){
        		public void onClick(DialogInterface i, int which)
        		{
        			operateWhenTappedList(keptDI, keptTV);
        		}
        	};
*/
    		//hideList(); // 何故か引っ込まないので引っ込める
        	switch (matMode)
        	{
        	case AppInfo.ID_ACTIVITY_DRICON_MATCHFROMMAP:
        		startManualMatching(info);
        		break;
        	case AppInfo.ID_ACTIVITY_DRICON_MATCHFROMADR:
        		startManualMatchingFromAdr(info);
        		break;
        	case AppInfo.ID_ACTIVITY_DRICON_MATCHCONF:
        		executeManualMatching(info, null);
        		break;
        	}
        	//executeManualMatching(info, null);
        	
        }
        else
        {
/*        	
			try
			{
		    	ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
				// 拡大して表示
				oNaviApi.setZoom(ZoomLevel.M_20);
				// 世界測地系BLを日本測地系に変換して中心に表示
				oNaviApi.setCenter(GeoUtils.toWGS84(info.mPoint.getLatitudeMs(), info.mPoint.getLongitudeMs()));
			}
			catch (NaviException e)
			{
				e.printStackTrace();
			}
	        m_oTopView.setDetailButton(info, dispTV);
*/	        
        	operateWhenTappedList(info, dispTV);
        }
    }
    
    private void operateWhenTappedList(DeliveryInfo tappedInfo, LinearLayout dispTV)
    {
		try
		{
	    	ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
			// 拡大して表示
// Mod by CPJsunagawa '2015-06-04 Start
			oNaviApi.setZoom(ZoomLevel.M_20);
//			oNaviApi.setZoom(ZoomLevel.M_40);
// Mod by CPJsunagawa '2015-06-04 End
			// 世界測地系BLを日本測地系に変換して中心に表示
			oNaviApi.setCenter(tappedInfo.mPoint);
		}
		catch (NaviException e)
		{
			e.printStackTrace();
		}
        m_oTopView.setDetailButton(tappedInfo, dispTV);
    }

    public void reflectDeliveredCount()
    {
    	m_oTopView.reflectDeliveredCount();
    }
    
    // 属性リストの存在有無を取得する
    public boolean isExistInfoList() {
    	NaviApplication app = (NaviApplication)getApplication();
    	return app.isExistInfoList();
    }
    
    // 現在のGPSの座標を、中心に表示させる（現在地モードではない）
    public void setPosAsNowGPS() throws NaviException
    {
    	JNITwoLong nowPos = new JNITwoLong();
    	if (NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(nowPos) != 0)
    	{
    		// 現在地が取れない旨を知らせたほうが良いか？
    		Toast ts = new Toast(this);
    		ts.setText("GPS座標が取得できません");
    		ts.show();
    	}
    	else
    	{
    		// 現在地の設定
    		ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
    		//GeoPoint nowPosGP = new GeoPoint();
    		//nowPosGP.setLatitudeMs(nowPos.getM_lLat());
    		//nowPosGP.setLongitudeMs(nowPos.getM_lLong());
    		GeoPoint nowPosGP = GeoUtils.toWGS84(nowPos.getM_lLat(), nowPos.getM_lLong());
    		oNaviApi.setCenter(nowPosGP);
    	}
    }
    
    private DeliveryInfo mmDelivInfo;
	private String[] workcaps;
    /**
     * マニュアルマッチングを開始する。
     * @param info
     * @param ocl キャンセル時に何かやりたいときにこれを定義する。
     */
    public void executeManualMatching(DeliveryInfo info, DialogInterface.OnClickListener ocl)
    {
    	String capOfFromMap = this.getString(R.string.infoMatchingFromMap);
    	String capOfFromMyPos = this.getString(R.string.infoMatchingFromMyPos);
    	String capOfFromAdr = this.getString(R.string.infoMatchingFromAdr);
    	String[] items = new String[]{capOfFromMap, capOfFromMyPos, capOfFromAdr};
    	AlertDialog.Builder adb = new AlertDialog.Builder(this);
    	adb.setTitle(R.string.infoMatching);
    	mmDelivInfo = info;
    	adb.setItems(items, new DialogInterface.OnClickListener()
    	{
    		public void onClick(DialogInterface i, int which)
    		{
    			switch (which)
    			{
    			case 0: // 地図から
    				startManualMatching(mmDelivInfo);
    				break;
    			case 1: // 現在地から
    			{
/*    				
    				CommonLib.setChangePos(true);
    				CommonLib.setChangePosFromSearch(false);
    				CommonLib.routeSearchFlag = false;

    				SakuraCustPOIData oData = new SakuraCustPOIData();
    				oData.m_MyIndex = mmDelivInfo.mMyIndex;
    				oData.m_CustID = mmDelivInfo.mCustID;
    				oData.m_sName = mmDelivInfo.mCustmer;
    				oData.m_sAddress = mmDelivInfo.mAddress;
    				if (which == 0)
    				{
    					// 地図からの場合は今のポジションを表示する
    					oData.m_wLong = mmDelivInfo.mPoint.getLongitudeMs();
    					oData.m_wLat = mmDelivInfo.mPoint.getLatitudeMs();
    				}
    				else
    				{
    					// 現在地からの場合は、現在地を表示する。
    			    	JNITwoLong nowPos = new JNITwoLong();
    			    	if (NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(nowPos) != 0)
    			    	{
    			    		// 現在地が取れない旨を知らせたほうが良いか？
    			    		Toast ts = new Toast(MapActivity.this);
    			    		ts.setText("GPS座標が取得できません");
    			    		ts.show();
    			    		return;
    			    	}
    			    	else
    			    	{
    			    		// 現在地の設定
    			    		//GeoPoint nowPosGP = GeoUtils.toWGS84(nowPos.getM_lLat(), nowPos.getM_lLong());
        					oData.m_wLong = nowPos.getM_lLong();
        					oData.m_wLat = nowPos.getM_lLat();
    			    	}
    				}
    				Intent intent = new Intent();
    				intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_EDIT);
    				
    				hideList();
    				OpenMap.moveMapTo(MapActivity.this, oData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
*/
    				long lat = 0, lon = 0;
					// 現在地からの場合は、現在地を表示する。
			    	JNITwoLong nowPos = new JNITwoLong();
			    	if (NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(nowPos) != 0)
			    	{
			    		// 現在地が取れない旨を知らせたほうが良いか？
			    		Toast ts = new Toast(MapActivity.this);
			    		ts.setText("GPS座標が取得できません");
			    		ts.show();
			    		return;
			    	}
			    	else
			    	{
			    		// 現在地の設定
			    		//GeoPoint nowPosGP = GeoUtils.toWGS84(nowPos.getM_lLat(), nowPos.getM_lLong());
    					lon = nowPos.getM_lLong();
    					lat = nowPos.getM_lLat();
			    	}
    				startManualMatching(mmDelivInfo, lat, lon);
    			}
    				break;
    			//case 1: // 住所から
    			case 2: // 住所から
/*    				
    			{
					Intent oIntent = new Intent(MapActivity.this.getIntent());
					oIntent.setClass(MapActivity.this, AddressProvinceInquiry.class);
					if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
						oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
					}
					NaviRun.GetNaviRunObj().setSearchKind(AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY);
					oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, MapActivity.this.getResources().getString(R.string.address_title));
					oIntent.putExtra(Constants.ADDRESS_SEARCH_FOR_MATCH, mmDelivInfo.mMyIndex); // 送る値は現状のインデックス

					
					oIntent.putExtra(Constants.ADDRESS_SEARCH_DB_ADR, mmDelivInfo.mAddress); // 送る値は、DBに格納された住所
					NaviActivityStarter.startActivityForResult(MapActivity.this, oIntent, AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY);
    					
    			}
*/    			
    				startManualMatchingFromAdr(mmDelivInfo);
    				break;
    			}
    		}
    	}
    	);
    	//adb.setPositiveButton(R.string.btn_cancel, null);
    	adb.setPositiveButton(R.string.btn_cancel, ocl);
    	adb.show();
    }
    
    /**
     * マニュアルマッチングを開始する（地図から）
     * @param delivInfo
     */
    private void startManualMatching(DeliveryInfo delivInfo)
    {
    	startManualMatching(delivInfo,
    			delivInfo.mPoint.getLatitudeMs(),
    			delivInfo.mPoint.getLongitudeMs());
    }
    
    /**
     * マニュアルマッチングを開始する（指定座標から）
     * @param delivInfo
     * @param lat
     * @param lon
     */
    private void startManualMatching(DeliveryInfo delivInfo, long lat, long lon)
    {
        CommonLib.setChangePos(true);
        CommonLib.setChangePosFromSearch(false);
        CommonLib.routeSearchFlag = false;

        SakuraCustPOIData oData = new SakuraCustPOIData();
        oData.m_MyIndex = delivInfo.mMyIndex;
        oData.m_CustID = delivInfo.mCustID;
        oData.m_sName = delivInfo.mCustmer;
        oData.m_sAddress = delivInfo.mAddress;
        oData.m_wLong = lon;
        oData.m_wLat = lat;
        Intent intent = new Intent();
        intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_EDIT);
        
        hideList();
        OpenMap.moveMapTo(this, oData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
    }

// Add by CPJsunagawa '2015-07-08 Start
    /**
     * アイコン登録を開始する
     * @param delivInfo
     */
    private void startRegistIcon(DeliveryInfo delivInfo, int index)
    {
    	startRegistIcon(delivInfo, index,
    			delivInfo.mPoint.getLatitudeMs(),
    			delivInfo.mPoint.getLongitudeMs());
    }
    
    /**
     * アイコン登録を開始する（指定座標から）
     * @param delivInfo
     * @param index
     * @param lat
     * @param lon
     */
    private void startRegistIcon(DeliveryInfo delivInfo, int index, long lat, long lon)
    {
    	System.out.println("*** MapActivity startRegistIcon Start ***");

    	NaviApplication app = (NaviApplication)getApplication();
        
        CommonLib.setChangePos(false);
        CommonLib.setChangePosFromSearch(false);
        CommonLib.setRegIcon(true);
        CommonLib.setInputText(false);
        CommonLib.routeSearchFlag = false;

        SakuraCustPOIData oData = new SakuraCustPOIData();
        oData.m_MyIndex = delivInfo.mMyIndex;
        oData.m_CustID = delivInfo.mCustID;
        oData.m_sName = delivInfo.mCustmer;
        oData.m_sAddress = delivInfo.mAddress;
        oData.m_wLong = lon;
        oData.m_wLat = lat;
        Intent intent = new Intent();
        intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_REGIST_ICON);

    	System.out.println("*** MapActivity startRegistIcon putExtra:FROM_REGIST_ICON ***");
    	
        hideList();
        
    	System.out.println("*** MapActivity startRegistIcon OpenMap.moveMapTo(Before):Constants.LOCAL_INQUIRY_REQUEST ***");

    	OpenMap.moveMapTo(this, oData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
    }

    /**
     * アイコン登録を開始する（指定座標から）
     * @param delivInfo
     * @param index
     * @param lat
     * @param lon
     */
    private void startRegistIcon2(int index)
    {
    	System.out.println("*** MapActivity startRegistIcon2 Start ***");

    	NaviApplication app = (NaviApplication)getApplication();
        
        CommonLib.setChangePos(false);
        CommonLib.setChangePosFromSearch(false);
        CommonLib.setRegIcon(true);
        CommonLib.setMoveIcon(false);
        CommonLib.setDeleteIcon(false);
        CommonLib.setDrawLine(false);
        CommonLib.setInputText(false);
        CommonLib.routeSearchFlag = false;

    	JNITwoLong myPosi = new JNITwoLong();
	    myPosi = CommonLib.getMapCenterInfo();
	    
/*
	    GeoPoint gPoint;
        api = ((NaviApplication)getApplication()).getApi();
        gPoint = api.getCenter();
*/
        POIData oData = new POIData();
        oData.m_wLong = myPosi.getM_lLong();
        oData.m_wLat = myPosi.getM_lLat();
        Intent intent = new Intent();
        intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_REGIST_ICON);
        intent.putExtra(Constants.INTENT_EXTRA_SELECT_ICON, index);

    	System.out.println("*** MapActivity startRegistIcon putExtra:FROM_REGIST_ICON ***");

        hideList();
        
    	System.out.println("*** MapActivity startRegistIcon OpenMap.moveMapTo(Before):Constants.LOCAL_INQUIRY_REQUEST ***");

    	OpenMap.moveMapTo(this, oData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
    }

// Add by CPJsunagawa '2015-08-13 Start
    /**
     * アイコン移動を開始する
     * @param delivInfo
     * @param index
     * @param lat
     * @param lon
     */
    private void startMoveIcon(int index)
    {
    	System.out.println("*** MapActivity startMoveIcon Start ***");

    	NaviApplication app = (NaviApplication)getApplication();
        
        CommonLib.setChangePos(false);
        CommonLib.setChangePosFromSearch(false);
        CommonLib.setRegIcon(false);
        CommonLib.setMoveIcon(true);
        CommonLib.setDeleteIcon(false);
        CommonLib.setDrawLine(false);
        CommonLib.setInputText(false);
        CommonLib.routeSearchFlag = false;

    	JNITwoLong myPosi = new JNITwoLong();
	    myPosi = CommonLib.getMapCenterInfo();
	    
        POIData oData = new POIData();
        oData.m_wLong = myPosi.getM_lLong();
        oData.m_wLat = myPosi.getM_lLat();
        Intent intent = new Intent();
        intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_MOVE_ICON);
        intent.putExtra(Constants.INTENT_EXTRA_MOVE_ICON, index);

    	System.out.println("*** MapActivity startMoveIcon putExtra:FROM_MOVE_ICON ***");

        //hideList();
        
    	System.out.println("*** MapActivity startMoveIcon OpenMap.moveMapTo(Before):Constants.LOCAL_INQUIRY_REQUEST ***");

    	OpenMap.moveMapTo(this, oData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
    }
    	
    /**
     * アイコンを削除する
     * @param delivInfo
     * @param index
     * @param lat
     * @param lon
     */
    private void startDeleteIcon(int index)
    {
    	System.out.println("*** MapActivity startDeleteIcon Start ***");
        
    	// アイコンを削除する
    	goDeleteIcon(index);
    	
    	// 地図の再描画
    	//NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
    	NaviEngineUtils.redrawSurface(NaviRun.CT_ICU_CtrlType_Map1);
    }
// Add by CPJsunagawa '2015-08-13 End

// Add by CPJsunagawa '2015-08-18 Start
    /**
     * ライン描画を開始する
     * @param delivInfo
     * @param index
     * @param lat
     * @param lon
     */
    public void startDrawLine(int index)
    {
    	System.out.println("*** MapActivity startDrawLine Start ***");

    	NaviApplication app = (NaviApplication)getApplication();
        
        CommonLib.setChangePos(false);
        CommonLib.setChangePosFromSearch(false);
        CommonLib.setRegIcon(false);
        CommonLib.setMoveIcon(false);
        CommonLib.setDeleteIcon(false);
        CommonLib.setDrawLine(true);
        CommonLib.setInputText(false);
        CommonLib.routeSearchFlag = false;

    	JNITwoLong myPosi = new JNITwoLong();
	    myPosi = CommonLib.getMapCenterInfo();
	    
        POIData oData = new POIData();
        oData.m_wLong = myPosi.getM_lLong();
        oData.m_wLat = myPosi.getM_lLat();
        Intent intent = new Intent();
        intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_DRAW_LINE);
        intent.putExtra(Constants.INTENT_EXTRA_DRAW_LINE, index);

    	System.out.println("*** MapActivity startDrawLine putExtra:FROM_DRAW_LINE ***");

        //hideList();
        
    	System.out.println("*** MapActivity startDrawLine OpenMap.moveMapTo(Before):Constants.LOCAL_INQUIRY_REQUEST ***");

    	OpenMap.moveMapTo(this, oData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
    }
// Add by CPJsunagawa '2015-08-18 End

    /**
     * コメント入力を開始する
     * @param delivInfo
     */
    private void startInputText(DeliveryInfo delivInfo)
    {
    	startInputText(delivInfo,
    			delivInfo.mPoint.getLatitudeMs(),
    			delivInfo.mPoint.getLongitudeMs());
    }
    
    /**
     * コメント入力を開始する（指定座標から）
     * @param delivInfo
     * @param lat
     * @param lon
     */
    private void startInputText(DeliveryInfo delivInfo, long lat, long lon)
    {
    	System.out.println("*** MapActivity startInputText ***");

    	CommonLib.setChangePos(false);
        CommonLib.setChangePosFromSearch(false);
        CommonLib.setRegIcon(false);
        CommonLib.setInputText(true);
        CommonLib.routeSearchFlag = false;

        SakuraCustPOIData oData = new SakuraCustPOIData();
        oData.m_MyIndex = delivInfo.mMyIndex;
        oData.m_CustID = delivInfo.mCustID;
        oData.m_sName = delivInfo.mCustmer;
        oData.m_sAddress = delivInfo.mAddress;
        oData.m_wLong = lon;
        oData.m_wLat = lat;
        Intent intent = new Intent();
        intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_INPUT_TEXT);
        
        hideList();
        OpenMap.moveMapTo(this, oData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
    }
// Add by CPJsunagawa '2015-07-08 End    
    
    
    /**
     * マニュアルマッチングを開始する（住所から）
     * @param delivInfo
     */
    private void startManualMatchingFromAdr(DeliveryInfo delivInfo)
    {
		Intent oIntent = new Intent(MapActivity.this.getIntent());
		oIntent.setClass(MapActivity.this, AddressProvinceInquiry.class);
		if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
			oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
		}
		NaviRun.GetNaviRunObj().setSearchKind(AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY);
		oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, MapActivity.this.getResources().getString(R.string.address_title));
		oIntent.putExtra(Constants.ADDRESS_SEARCH_FOR_MATCH, delivInfo.mMyIndex); // 送る値は現状のインデックス

		
		oIntent.putExtra(Constants.ADDRESS_SEARCH_DB_ADR, delivInfo.mAddress); // 送る値は、DBに格納された住所
		NaviActivityStarter.startActivityForResult(MapActivity.this, oIntent, AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY);
    }

    
    
// Add by CPJsunagawa '2015-07-08 Start
    private AlertDialog ad8DirForDismiss;

    /**
     * アイコン選択画面を表示する。（イメージボタン）
     * @param info
     * @param ocl キャンセル時に何かやりたいときにこれを定義する。
     */
    public void executeSelectIcon(final int index)
    {
    	String capUp = this.getString(R.string.infoSelectIcon1);
    	String capRightUp = this.getString(R.string.infoSelectIcon2);
    	String capRight = this.getString(R.string.infoSelectIcon3);
    	String capRightDown = this.getString(R.string.infoSelectIcon4);
    	String capDown = this.getString(R.string.infoSelectIcon5);
    	String capLeftDown = this.getString(R.string.infoSelectIcon6);
    	String capLeft = this.getString(R.string.infoSelectIcon7);
    	String capLeftUp = this.getString(R.string.infoSelectIcon8);
    	String[] items = new String[]{capUp, capRightUp, capRight, capRightDown, capDown, capLeftDown, capLeft, capLeftUp};

    	System.out.println("*** MapActivity executeSelectIcon ***");

    	ImageButton iconBtn;
        Button goBackBtn;

        LayoutInflater oInflater = LayoutInflater.from(this);
        // 画面を追加
        final LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.select_icon_main, null);
        
        // 共通リスナーを定義する。
        View.OnClickListener vocl = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            // 2015/12/22 Yamamoto Start
            //int tagID = (int)view.getTag();
            String strTag = view.getTag().toString();
            int tagID = new Integer(strTag).intValue();
            // 2015/12/22 Yamamoto End

                startRegistIcon2(tagID);
/*
            	switch (tagID)
            	{
            	case R.drawable.arrow_leftup:
            		startRegistIcon2(index);
            		break;
            	case R.drawable.arrow_up:
            		startRegistIcon2(index);
            		break;
            	case R.drawable.arrow_left:
            		startRegistIcon2(index);
            		break;
            	case R.drawable.arrow_leftdown:
            		startRegistIcon2(index);
            		break;
            	case R.drawable.arrow_down:
            		startRegistIcon2(index);
            		break;
            	case R.drawable.arrow_right:
            		startRegistIcon2(index);
            		break;
            	case R.drawable.arrow_rightup:
            		startRegistIcon2(index);
            		break;
            	case R.drawable.arrow_rightdown:
            		startRegistIcon2(index);
            		break;
            	}
*/            	
            	ad8DirForDismiss.dismiss();
            }	
    	};

        // 8方向矢印
        if(index == 0){
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon1);
        	iconBtn.setImageResource(R.drawable.arrow_leftup);
        	iconBtn.setTag(R.drawable.arrow_leftup);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon2);
        	iconBtn.setImageResource(R.drawable.arrow_up);
        	iconBtn.setTag(R.drawable.arrow_up);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon3);
        	iconBtn.setImageResource(R.drawable.arrow_rightup);
        	iconBtn.setTag(R.drawable.arrow_rightup);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon4);
        	iconBtn.setImageResource(R.drawable.arrow_left);
        	iconBtn.setTag(R.drawable.arrow_left);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon5);
        	iconBtn.setEnabled(false);
//        	iconBtn.setImageResource(R.drawable.arrow_rightdown);
//        	iconBtn.setTag(R.drawable.arrow_rightdown);
//        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon6);
        	iconBtn.setImageResource(R.drawable.arrow_right);
        	iconBtn.setTag(R.drawable.arrow_right);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon7);
        	iconBtn.setImageResource(R.drawable.arrow_leftdown);
        	iconBtn.setTag(R.drawable.arrow_leftdown);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon8);
        	iconBtn.setImageResource(R.drawable.arrow_down);
        	iconBtn.setTag(R.drawable.arrow_down);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon9);
        	iconBtn.setImageResource(R.drawable.arrow_rightdown);
        	iconBtn.setTag(R.drawable.arrow_rightdown);
        	iconBtn.setOnClickListener(vocl);
        // 一方通行
        } else if(index == 1){

        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon1);
        	//iconBtn.setImageResource(R.drawable.ippou_leftup);
        	//iconBtn.setTag(R.drawable.ippou_leftup);
        	iconBtn.setEnabled(false);
        	//iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon2);
        	iconBtn.setImageResource(R.drawable.ippou_up);
        	iconBtn.setTag(R.drawable.ippou_up);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon3);
        	//iconBtn.setImageResource(R.drawable.ippou_rightup);
        	//iconBtn.setTag(R.drawable.ippou_rightup);
        	iconBtn.setEnabled(false);
        	//iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon4);
        	iconBtn.setImageResource(R.drawable.ippou_left);
        	iconBtn.setTag(R.drawable.ippou_left);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon5);
        	//iconBtn.setImageResource(R.drawable.ippou_rightdown);
        	//iconBtn.setTag(R.drawable.ippou_rightdown);
        	iconBtn.setEnabled(false);
        	//iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon6);
        	iconBtn.setImageResource(R.drawable.ippou_right);
        	iconBtn.setTag(R.drawable.ippou_right);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon7);
        	//iconBtn.setImageResource(R.drawable.ippou_leftdown);
        	//iconBtn.setTag(R.drawable.ippou_leftdown);
        	iconBtn.setEnabled(false);
        	//iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon8);
        	iconBtn.setImageResource(R.drawable.ippou_down);
        	iconBtn.setTag(R.drawable.ippou_down);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon9);
        	//iconBtn.setImageResource(R.drawable.ippou_rightdown);
        	//iconBtn.setTag(R.drawable.ippou_rightdown);
        	iconBtn.setEnabled(false);
        	//iconBtn.setOnClickListener(vocl);
        	
        // 道路標識
        } else if(index == 2){
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon1);
        	iconBtn.setImageResource(R.drawable.hyoshiki1_s);
        	iconBtn.setTag(R.drawable.hyoshiki1_s);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon2);
        	iconBtn.setImageResource(R.drawable.hyoshiki2_s);
        	iconBtn.setTag(R.drawable.hyoshiki2_s);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon3);
        	iconBtn.setImageResource(R.drawable.hyoshiki3_s);
        	iconBtn.setTag(R.drawable.hyoshiki3_s);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon4);
        	iconBtn.setImageResource(R.drawable.hyoshiki4_s);
        	iconBtn.setTag(R.drawable.hyoshiki4_s);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon5);
        	iconBtn.setImageResource(R.drawable.hyoshiki5_s);
        	iconBtn.setTag(R.drawable.hyoshiki5_s);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon6);
        	iconBtn.setImageResource(R.drawable.hyoshiki6_s);
        	iconBtn.setTag(R.drawable.hyoshiki6_s);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon7);
        	iconBtn.setImageResource(R.drawable.hyoshiki7_s);
        	iconBtn.setTag(R.drawable.hyoshiki7_s);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon8);
        	iconBtn.setImageResource(R.drawable.hyoshiki8_s);
        	iconBtn.setTag(R.drawable.hyoshiki8_s);
        	iconBtn.setOnClickListener(vocl);
        	
        	iconBtn = (ImageButton)oLayout.findViewById(R.id.sel_icon9);
        	iconBtn.setImageResource(R.drawable.hyoshiki9_s);
        	iconBtn.setTag(R.drawable.hyoshiki9_s);
        	iconBtn.setOnClickListener(vocl);
        }

        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setView(oLayout);
    	adb.setPositiveButton(R.string.btn_cancel, null);
        ad8DirForDismiss = adb.show();
        //oLayout.show();

    }

    /**
     * アイコンを移動する
     * @param info
     * @param ocl キャンセル時に何かやりたいときにこれを定義する。
     */
    public void executeMoveIcon()
    {
    	System.out.println("*** MapActivity executeMoveIcon ***");
        
        // アイコン移動画面を表示する
        int nSelectIndex = 0;
        showIconList(nSelectIndex, TrackIconEdit.PRESSED_MOVE_ICON);
        
        // 選択されたアイコンを削除する
        
    }

    /**
     * アイコンを削除する
     * @param info
     * @param ocl キャンセル時に何かやりたいときにこれを定義する。
     */
    public void executeDeleteIcon()
    {
    	System.out.println("*** MapActivity executeDeleteIcon ***");
    	
        // アイコン移動画面を表示する
        int nSelectIndex = 0;
        showIconList(nSelectIndex, TrackIconEdit.PRESSED_DELETE_ICON);
    	
    }

    /**
     * ラインを描画する
     * @param info
     * @param ocl キャンセル時に何かやりたいときにこれを定義する。
     */
    public void executeDrawLine()
    {
    	System.out.println("*** MapActivity executeDrawLine ***");

    	// シンボルの移動を参考に、移動前を【始点】、移動後を【終点】として、ラインを描画宇する
    	startDrawLine(0);
    	
/*
    	ImageButton iconBtn;
        Button goBackBtn;

        LayoutInflater oInflater = LayoutInflater.from(this);
        // 画面を追加
        final LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.select_icon_main, null);
        
        // 共通リスナーを定義する。
        View.OnClickListener vocl = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            	int tagID = (int)view.getTag();
            	
        		startRegistIcon2(tagID);

        		ad8DirForDismiss.dismiss();
            }	
    	};

        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setView(oLayout);
    	adb.setPositiveButton(R.string.btn_cancel, null);
        ad8DirForDismiss = adb.show();
        //oLayout.show();
*/
    }


    /**
     * アイコン選択画面を表示する。
     * @param 
     * @param 
     */
    public void executeSelectIconMain(DialogInterface.OnClickListener ocl)
    {
    	String capArrow = this.getString(R.string.infoArrowIcon);
    	String capIppou = this.getString(R.string.infoIppouIcon);
    	String capHyoshiki = this.getString(R.string.infoHyoshikiIcon);
    	String[] items = new String[]{capArrow, capIppou, capHyoshiki};
    	AlertDialog.Builder adb = new AlertDialog.Builder(this);
    	adb.setTitle(R.string.infoRegIcon);
    	//mIconDirection = -1;

    	System.out.println("*** MapActivity Into executeSelectIcon ***");
    	
    	adb.setItems(items, new DialogInterface.OnClickListener()
    	{
    		public void onClick(DialogInterface i, int which)
    		{
    			
    			//mIconDirection = which;
    	    	//System.out.println("*** executeSelectIcon mIconDirection => " + mIconDirection);
    	    	System.out.println("*** executeSelectIcon mIconDirection => " + which);
    			// アイコン登録
	    		System.out.println("*** MapActivity Into executeSelectIcon : startRegistIcon2 ***");
	    		//startRegistIcon2(which);
	    		executeSelectIcon(which);
    		}
    	}
    	);
    	//adb.setPositiveButton(R.string.btn_cancel, null);
    	adb.setPositiveButton(R.string.btn_cancel, ocl);
    	adb.show();
    }

    /**
     * アイコンを移動する。
     * @param info
     * @param ocl キャンセル時に何かやりたいときにこれを定義する。
     */
/*
    public int executeMoveIconMain(final DeliveryInfo info, DialogInterface.OnClickListener ocl)
    {
    	String capArrow = this.getString(R.string.infoArrowIcon);
    	String capIppou = this.getString(R.string.infoIppouIcon);
    	String capHyoshiki = this.getString(R.string.infoHyoshikiIcon);
    	String[] items = new String[]{capArrow, capIppou, capHyoshiki};
    	AlertDialog.Builder adb = new AlertDialog.Builder(this);
    	adb.setTitle(R.string.infoMoveIcon);
    	mmDelivInfo = info;
    	//mIconDirection = -1;

    	System.out.println("*** MapActivity Into executeMoveIcon ***");
    	
    	// DBに登録されている情報を取得する
        TrackIconEdit.DbHelper helper = new TrackIconEdit.DbHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();

        // DBに登録されているレコード数を取得する
        Cursor cur = db.rawQuery("SELECT * FROM FigureData", null);
        if( cur == null ) return 0;
        int num = cur.getCount();

        int nIDindex = cur.getColumnIndex("nID");
        int nFigureKindindex = cur.getColumnIndex("nFigureKind");
        int nLat1index = cur.getColumnIndex("Lat1");
        int nLon1index = cur.getColumnIndex("Lon1");
        int nLat2index = cur.getColumnIndex("Lat2");
        int nLon2index = cur.getColumnIndex("Lon2");
        int nIconFileindex = cur.getColumnIndex("strIconFile");
        
        String strDir = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) +
                "/" +NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "icon2/";
        
        // 取得したアイコンを地図上に表示する
        while(cur.moveToNext()){
        	String iconFile = cur.getString(nIconFileindex);
        	double dLat1 = cur.getDouble(nLat1index);
        	double dLon1 = cur.getDouble(nLon1index);
        	
        	iconFile = strDir + iconFile;
        	
        	System.out.println("MapActivity ExecuteShowRegistIcon iconFile => " + iconFile);

        	onDrawUserIcon(dLat1, dLon1, iconFile, "");
        }
        
        cur.close();
        
    	
    	
    	adb.setItems(items, new DialogInterface.OnClickListener()
    	{
    		public void onClick(DialogInterface i, int which)
    		{
    			
    			//mIconDirection = which;
    	    	//System.out.println("*** executeSelectIcon mIconDirection => " + mIconDirection);
    	    	System.out.println("*** executeSelectIcon mIconDirection => " + which);
    			// アイコン登録
	    		System.out.println("*** MapActivity Into executeMoveIcon : execSelectIcon ***");
	    		//startRegistIcon2(which);
//	    		executeMoveIcon(which);
    		}
    	}
    	);
    	//adb.setPositiveButton(R.string.btn_cancel, null);
    	adb.setPositiveButton(R.string.btn_cancel, ocl);
    	adb.show();
    	
    	return 0;
    }
*/

    /**
     * アイコンを移動する。
     * @param info
     * @param ocl キャンセル時に何かやりたいときにこれを定義する。
     */
    public int executeMoveIconMain(DialogInterface.OnClickListener ocl)
    {

    	System.out.println("*** MapActivity Into executeMoveIcon ***");
    	
		// アイコンを移動する
    	executeMoveIcon();
    	
    	return 0;
    }

    /**
     * アイコンを削除する。
     * @param info
     * @param ocl キャンセル時に何かやりたいときにこれを定義する。
     */
    public int executeDeleteIconMain(DialogInterface.OnClickListener ocl)
    {

    	System.out.println("*** MapActivity Into executeMoveIcon ***");
    	
		// アイコンを削除する
		executeDeleteIcon();

		return 0;
    }

    /**
     * ラインを描画する。
     * @param info
     * @param ocl キャンセル時に何かやりたいときにこれを定義する。
     */
    public int executeDrawUserLineMain(DialogInterface.OnClickListener ocl)
    {

    	System.out.println("*** MapActivity Into executeDrawUserLine ***");
    	
		// ラインを描画する
		executeDrawLine();

		return 0;
    }

    /**
     * テキスト入力を開始する。
     * @param info
     * @param ocl キャンセル時に何かやりたいときにこれを定義する。
     */
    public void executeInputText(DeliveryInfo info, DialogInterface.OnClickListener ocl)
    {
    	String capOfFromMap = this.getString(R.string.infoMatchingFromMap);
    	String capOfFromMyPos = this.getString(R.string.infoMatchingFromMyPos);
    	String capOfFromAdr = this.getString(R.string.infoMatchingFromAdr);
    	String[] items = new String[]{capOfFromMap, capOfFromMyPos, capOfFromAdr};
    	AlertDialog.Builder adb = new AlertDialog.Builder(this);
    	adb.setTitle(R.string.infoRegText);
    	mmDelivInfo = info;
    	adb.setItems(items, new DialogInterface.OnClickListener()
    	{
    		public void onClick(DialogInterface i, int which)
    		{
    			switch (which)
    			{
    			case 0: // 地図から
    				startManualMatching(mmDelivInfo);
    				break;
    			case 1: // 現在地から
    			{
    				long lat = 0, lon = 0;
					// 現在地からの場合は、現在地を表示する。
			    	JNITwoLong nowPos = new JNITwoLong();
			    	if (NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(nowPos) != 0)
			    	{
			    		// 現在地が取れない旨を知らせたほうが良いか？
			    		Toast ts = new Toast(MapActivity.this);
			    		ts.setText("GPS座標が取得できません");
			    		ts.show();
			    		return;
			    	}
			    	else
			    	{
			    		// 現在地の設定
			    		//GeoPoint nowPosGP = GeoUtils.toWGS84(nowPos.getM_lLat(), nowPos.getM_lLong());
    					lon = nowPos.getM_lLong();
    					lat = nowPos.getM_lLat();
			    	}
    				startManualMatching(mmDelivInfo, lat, lon);
    			}
    				break;
    			}
    		}
    	}
    	);
    	//adb.setPositiveButton(R.string.btn_cancel, null);
    	adb.setPositiveButton(R.string.btn_cancel, ocl);
    	adb.show();
    }
    
// Add by CPJsunagawa '2015-07-08 End
    
    
    // 指定したインデックスの顧客を地図右上に表示させる
    private void showCustomerInfoAsIndex(int index)
    {
    	if (m_oTopView != null)
    	{
	    	NaviApplication app = (NaviApplication)getApplication();
	    	DeliveryInfo deliv = app.getInfo(index);
	    	m_oTopView.setNowMatchingCustomer(deliv);
    	}
    }
    
    // 現在マッチング中の住所と顧客名を表示する。
    private void showCustomerInfoAsNowMatching()
    {
    	if (sakuraPoiData != null)
    	{
    		showCustomerInfoAsIndex(sakuraPoiData.m_MyIndex);
    	}
    	if (m_oTopView != null)
    	{
    		m_oTopView.showMapTopRightForList();
    	}
    }
    
    private void setDeliveryInfoToTop()
    {
    	if (m_oTopView != null)
    	{
	    	NaviApplication app = (NaviApplication)getApplication();
	    	if (app.isNowRouting())
	    	{
		    	DeliveryInfo deliv = app.getInfo(app.getNowRoutingCustomerIndex());
		    	m_oTopView.setNowMatchingCustomer(deliv);
	    	}
    	}
    }
    
    // 
    private void repairTopPosOfListBtn(boolean isSetPosAtFirstNoDeliv)
    {
    	NaviApplication na = (NaviApplication)getApplication();
    	LinearLayout ll = (LinearLayout)findViewById(R.id.infoList);
    	if (ll == null) return;
    	int btnCnt = ll.getChildCount();
    	for (int i = 0; i <= btnCnt - 1; i++)
    	{
    		View cv = ll.getChildAt(i);
    		if (cv instanceof LinearLayout)
    		{
    			btnCnt = ll.getChildCount();
    			if (btnCnt <= i) break;
	    		LinearLayout cll = (LinearLayout)ll.getChildAt(i);
	    		Object tagobj = cll.getTag();
	    		if (tagobj != null && tagobj instanceof String)
	    		{
		    		int custIdx = Integer.parseInt((String)tagobj);
	    			btnCnt = ll.getChildCount();
	    			if (btnCnt <= i) break;
	    			if (!na.isExistInfoList()) break;
		    		DeliveryInfo di = na.getInfo(custIdx);
		    		if (di.mMyButtonPos == 0)
		    		{
		    			di.mMyButtonPos = cll.getTop();
		    		}
		    		
		    		if (isSetPosAtFirstNoDeliv && custIdx == na.getFirstNoDeliveryIndex())
		    		{
		    			ScrollView sv = (ScrollView)findViewById(R.id.infoListSV);
		    			sv.smoothScrollTo(0, di.mMyButtonPos);
		    		}
	    		}
    		}
    		
    		// ボタン数を更新する
    		btnCnt = ll.getChildCount();
    	}
    }
    
// 手書きメモ Start
    // 手書きメモボタンの表示状態更新
    public void updateTegakiMemoBtn(int idx, Button btn) {
        NaviApplication app = (NaviApplication)getApplication();
        DeliveryInfo info = app.getInfo(idx);
        String path = app.getNaviAppDataPath().getNaviAppRootPath() + "/" + info.mHandWrittenMemoPath;
        // ファイルの存在チェック
        if( info.mHandWrittenMemo == null || "".equals(info.mHandWrittenMemo) ||
            !new File(path + "/" + info.mHandWrittenMemo).exists()) {
            // 存在しない
            btn.setBackgroundColor(0xFF000000); // black
// Mod 2015-02-26 文字列領域は、nullではなく””でクリアに統一
            //info.mHandWrittenMemo = null; // 念の為、クリアしておく
            info.mHandWrittenMemo = ""; // 念の為、クリアしておく
        } else {
            btn.setBackgroundColor(0xFF0000FF); // blue
        }
    }
// 手書きメモ End

// 写真 Start
    // 写真ボタンの表示状態更新
    private void updatePhotoBtn(int idx, Button btn) {
    	
    	System.out.println("***　Into updatePhotoBtn　***");
    	System.out.println("***　Into updatePhotoBtn　idx => " + idx);

        NaviApplication app = (NaviApplication)getApplication();
        DeliveryInfo info = app.getInfo(idx);
        int num = info.mPhotoNum;
        
    	System.out.println("***　Into updatePhotoBtn　num => " + num);
    	
        // ファイル数のチェック
        if( num == 0) {
            // 存在しない
            btn.setBackgroundColor(0xFF000000); // black
            btn.setEnabled(false);
        	System.out.println("Into updatePhotoBtn ファイル無し設定（BLACK）");
        } else {
            btn.setBackgroundColor(0xFF0000FF); // blue
            btn.setEnabled(true);
        	System.out.println("Into updatePhotoBtn ファイル有り設定（BLUE）");
        }
    }

    // 写真ボタンの表示状態更新
    private void updatePhotoBtn2(int idx, Button btn) {
    	
    	System.out.println("***　Into updatePhotoBtn2　***");
    	System.out.println("***　Into updatePhotoBtn2　idx => " + idx);

        NaviApplication app = (NaviApplication)getApplication();
        DeliveryInfo info = app.getInfo(idx);
        int num = info.mPhotoNum;
        
    	System.out.println("***　Into updatePhotoBtn2　num => " + num);
    	
        // ファイル数のチェック
        if( num == 0) {
            // 存在しない
            btn.setBackgroundColor(0xFF000000); // black
            btn.setEnabled(false);
        	System.out.println("Into updatePhotoBtn2 ファイル無し設定（BLACK）");
        } else {
            btn.setBackgroundColor(0xFF0000FF); // blue
            btn.setEnabled(true);
        	System.out.println("Into updatePhotoBtn2 ファイル有り設定（BLUE）");
        }
    }
// 写真 End

    private void goVehicleSearchForMatching(int matchMode) {
        Intent oIntent = new Intent(getIntent());
        oIntent.setClass(this, VehicleInquiry.class);
        if (null != oIntent)
        {
	        if (oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
	            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
	        }
	        oIntent.putExtra(Constants.INTENT_EXTRA_MATCHING_MODE, matchMode);
        }
        
        //NaviApplication na = (NaviApplication)getApplication();
        //na.setMatchigMode(matchMode);
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_VEHICLE_INQUIRY);
    }

// Add by CPJsunagawa 2015-05-13 Start
    private void clearRoute()
    {
    	m_oTopView.enableTapOrderNaviMode(false);
        CommonLib.setChangePos(false);
        CommonLib.setChangePosFromSearch(false);
        isManBackupRoute = false;
        isCarBackupRoute = false;
        CommonLib.setNaviStart(false);
        CommonLib.setHasRoute(false);
        onDeleteRoute();
    }
// Add by CPJsunagawa 2015-05-13 End

// Add by CPJsunagawa '13-12-25 End

    @Override
    protected View getSurfaceView() {
        mapView.onResume(this);
        return mapView.getSurfaceView();
    }

    public void stopSimulation() {
        if (!bIsDemoExit) {
            removeDialog(Constants.DIALOG_EXIT_SIMULATE_NAVI);
        }

        CommonLib.setBIsDemo(false);
        bIsDemoExit = true;
        guideEnd(false);
    }

// カメラ連携 Start
    private String getCurrentTimeStr() {
        long currentTimeMillis = System.currentTimeMillis();
        Date today = new Date(currentTimeMillis);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        return dateFormat.format(today);
    }

    // 写真編集画面を起動
    LinearLayout mPhotoContainer;

    void showPhotoView(boolean isView, String path) {

        View v = findViewById(R.id.infoListSV);
        if(mPhotoContainer == null || v == null) {
        	return ; // fail safe
        }
        if( isView) {
            mPhotoContainer.setVisibility(View.VISIBLE);
            PhotoCanvas canvas = (PhotoCanvas)mPhotoContainer.findViewById(R.id.photoCanvas);
            canvas.initView(path, v.getWidth(), (int)(0.70*v.getHeight()) );
            v.setVisibility(View.GONE);
        } else {
            PhotoCanvas canvas = (PhotoCanvas)mPhotoContainer.findViewById(R.id.photoCanvas);
            canvas.clearPhotoObj();
            resetPhotoEdit(); // 初期状態に戻す
            mPhotoContainer.setVisibility(View.GONE);
            v.setVisibility(View.VISIBLE);
        }
    }

 // Add 2015-02-05 ファイル削除 Start
    /**
     * ファイルを削除する
     * @param filePath ロード対象フルパス
     */
    public boolean deletePhoto(String filePath) {
        File fl = new File(filePath);
        boolean result = fl.exists();
        if (result) {
/*        	
            draw_list.clear(); // 今までの記録はキャンセルする
            if (isLoaded()) lastLoadedBmp.recycle();
            lastLoadedBmp = BitmapFactory.decodeFile(filePath);
            
            Bitmap tmpBitmap = null; //回転後のビットマップ
*/
            if (fl.delete()) {
            	System.out.println("ファイルを削除しました");
            	
            	onPhotoDeleteFile(filePath);
            	
            } else {
            	System.out.println("ファイルの削除に失敗しました");
            }
            //copyFromLastLoaded();
            
        }
        return result;
    }
// Add 2015-02-05 ファイル削除 End
// カメラ連携 End

    void initPhotoEdit() {
        final PhotoCanvas canvas = (PhotoCanvas)mPhotoContainer.findViewById(R.id.photoCanvas);
        // ペン幅 大
        View v = findViewById(R.id.penLarge);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.setPenWidth(view, 36);
            }});
        // ペン幅 中
        v = findViewById(R.id.penMedium);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.setPenWidth(view, 18);
            }});
        // ペン幅 小
        v = findViewById(R.id.penSmall);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.setPenWidth(view, 6);
            }});
        canvas.setPenWidth(v, 6); // 初期値は小とする

        // 黒
        v = findViewById(R.id.colorBlack);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.setPenColor(view, 0xff000000);
            }});
        canvas.setPenColor(v, 0xff000000);
        // グレー
        v = findViewById(R.id.colorGray);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.setPenColor(view, 0xffc3c3c3);
            }});
        // 白
        v = findViewById(R.id.colorWhite);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.setPenColor(view, 0xffffffff);
            }});
        // 赤
        v = findViewById(R.id.colorRed);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.setPenColor(view, 0xffff0000);
            }});
        // 黄色
        v = findViewById(R.id.colorYellow);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.setPenColor(view, 0xffffff00);
            }});
        // 緑
        v = findViewById(R.id.colorGreen);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.setPenColor(view, 0xff00ff00);
            }});
        // 青
        v = findViewById(R.id.colorBlue);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.setPenColor(view, 0xff0000ff);
            }});
        // ピンク
        v = findViewById(R.id.colorPink);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.setPenColor(view, 0xffff00ff);
            }});
        // 消しゴム
        v = findViewById(R.id.viewEraser);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.setEraserMode(view);
            }});
        // クリア
        v = findViewById(R.id.btnClear);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.clear();
            }});
// Add 2015-02-10 Start
        // 削除
        v = findViewById(R.id.btnDelete);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {

                // 確認ダイアログの生成
                AlertDialog.Builder alertDlg = new AlertDialog.Builder(MapActivity.this);
                alertDlg.setTitle("写真画像削除");
                String	strMessage;
//                String strFileName = new File(mPhotoPath).getName();
                String strFileName = new File(canvas.mPath).getName();
                strMessage = strFileName + "を削除します。よろしいですか？";
                alertDlg.setMessage(strMessage);
                alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        PhotoCanvas canvas = (PhotoCanvas)mPhotoContainer.findViewById(R.id.photoCanvas);
                        // OK ボタンクリック処理
//                    	canvas.delete();
//                      deletePhoto( mPhotoPath );
                        deletePhoto( canvas.mPath );
                        showPhotoView(false, null);
// Mod 2015-02-22 Start
                        updatePhotoBtn2(mTargetIndex, (Button)mTargetBtn); // 写真ボタンの状態を更新する
// Mod 2015-02-22 End
                    }
                });
                alertDlg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Cancel ボタンクリック処理
                    }
                });
                // 表示
                alertDlg.create().show();

                updatePhotoBtn2(mTargetIndex, (Button)mTargetBtn); // 写真ボタンの状態を更新する
                
            }});
// Add 2015-02-10 End
        // Undo
        v = findViewById(R.id.btnUndo);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.undo();
            }});
        // キャンセル
        v = findViewById(R.id.btnCancel);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                showPhotoView(false, null);
            }});
        // 保存
        v = findViewById(R.id.btnSave);
        v.setOnClickListener( new View.OnClickListener() {
            public void onClick(View view) {
                canvas.save();
                showPhotoView(false, null);
            }});
    }

    // 初期状態に戻す
    void resetPhotoEdit() {
        PhotoCanvas canvas = (PhotoCanvas)mPhotoContainer.findViewById(R.id.photoCanvas);
        View v = findViewById(R.id.penSmall);
        canvas.setPenWidth(v, 6); // 初期値は小とする
        v = findViewById(R.id.colorBlack);
        canvas.setPenColor(v, 0xff000000);
        v = findViewById(R.id.viewEraser);
        canvas.resetEraserMode(v);
    }

// Add by mitsuoka '14-11-21 Start
	public void onPhotoTypeSelectDialogPositiveClick(String itemName) {
		// 保存された画像ファイルをリネーム
		String fileName = new File(mPhotoPath).getName();
		String filePath = new File(mPhotoPath).getParent();
		File renameFile = new File(filePath + "/" + itemName + "_" + fileName);
		(new File(mPhotoPath)).renameTo(renameFile);
		mPhotoPath = renameFile.getPath();
		
		// onActivityResultから移動 by mitsuoka
		NaviApplication app = ((NaviApplication)getApplication());
        DeliveryInfo info =  app.getInfo(mSelectedIndex);
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        info.mPhotos[info.mPhotoNum] = new File(mPhotoPath).getName();
        info.mPhotoNum++;
        db.execSQL("UPDATE DeliveryInfo Set PhotoNum = '" + info.mPhotoNum + "' WHERE CustID = '" + info.mCustID + "';");
System.out.println("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(info.mPhotoNum) + " = '" + new File(mPhotoPath).getName() +
            "' WHERE CustID = '" + info.mCustID + "';");
        db.execSQL("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(info.mPhotoNum) + " = '" + new File(mPhotoPath).getName() +
            "' WHERE CustID = '" + info.mCustID + "';");
        
    	System.out.println("写真登録：UPDATE DeliveryInfo Set PhotoNum => " + info.mPhotoNum);

    	db.close();
        dbHelper.close();
        updatePhotoBtn(mSelectedIndex, (Button)mSelectedView); // 写真ボタンの状態を更新する

        mSelectedView = null;
        
		Toast.makeText(this, "写真「" + itemName + "」を保存しました", Toast.LENGTH_SHORT).show();
        
		return ;
	}

	public void onPhotoTypeSelectDialogNegativeClick() {
		mSelectedView = null;
		Toast.makeText(this, "保存をキャンセルしました", Toast.LENGTH_SHORT).show();
		return ;
	}
// Add by mitsuoka '14-11-21 End

// Add 2015-02-06 Start
	public void onPhotoDeleteFile(String itemName) {
		
    	System.out.println("Into onPhotoDeleteFile");

    	System.out.println(itemName);
//    	System.out.println(mPhotoPath);

		// 削除する画像ファイル
		String fileName = new File(itemName).getName();
		String filePath = new File(itemName).getParent();
//		String fileName = new File(mPhotoPath).getName();
//		String filePath = new File(mPhotoPath).getParent();
		
    	System.out.println(fileName);
    	System.out.println(filePath);

		// onActivityResultから移動 by mitsuoka
		NaviApplication app = ((NaviApplication)getApplication());
        DeliveryInfo info =  app.getInfo(mSelectedIndex);
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        info.mPhotos[info.mPhotoNum] = new File(mPhotoPath).getName();
        
    	System.out.println(info.mPhotoNum);
    	for(int i = 0; i < info.mPhotoNum; i++) {
    		System.out.println(info.mPhotos[i]);
    	}
// Add 2015-02-20 Start
    	int	nDeleteIndex=0;
    	for(int i = 0; i < info.mPhotoNum; i++) {
    		System.out.println(fileName);
    		System.out.println(info.mPhotos[i]);
    		if(fileName.equals(info.mPhotos[i]) == true) {
        		System.out.println("一致ファイル=>" + info.mPhotos[i]);
    			info.mPhotos[i] = "";
    			nDeleteIndex = i;
    			break;
    		}
    	}
		System.out.println("nDeleteIndex => " + nDeleteIndex);
		System.out.println("======");
		// DEBUG Start
		for(int i = 0; i < info.mPhotoNum; i++) {
    		System.out.println("info.mPhotos => " + info.mPhotos[i]);
    	}
		System.out.println("======");
		// DEBUG End
    	String[] szFileName = new String[info.mPhotoNum];
    	for(int i = 0; i < info.mPhotoNum; i++) {
    		szFileName[i] = info.mPhotos[i];
    		System.out.println("szFileName=>" + szFileName[i]);
    		System.out.println("info.mPhotos=>" + info.mPhotos[i]);
    		info.mPhotos[i] = "";
    	}
		System.out.println("----");
		System.out.println("info.mPhotoNum => " + info.mPhotoNum);
    	for(int i=0, j=0; i < info.mPhotoNum; i++) {
    		System.out.println("szFileName=>" + szFileName[i]);
    		if(szFileName[i].equals("") == true) {
        		System.out.println("szFileName is Null");
    			continue;
    		} else {
    			info.mPhotos[j] = szFileName[i];
    			j++;
    		}
    	}
		System.out.println("********");
    	// DEBUG
    	for(int i=0; i < info.mPhotoNum; i++) {
    		System.out.println("info.mPhotos=>" + info.mPhotos[i]);
    	}
// Add 2015-02-20 End

//		db.execSQL("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(nDeleteIndex+1) + " = '' WHERE CustID = '" + info.mCustID + "';");
    	for(int i=0; i < info.mPhotoNum; i++) {
//    		if(info.mPhotos[i].equals("") == false) {
//    			db.execSQL("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(i+1) + " = '' WHERE CustID = '" + info.mCustID + "';");
//    			System.out.println("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(i+1) + " = '' WHERE CustID = '" + info.mCustID + "';");
    			db.execSQL("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(i+1) + " = '" + info.mPhotos[i] + "' WHERE CustID = '" + info.mCustID + "';");
    			System.out.println("UPDATE DeliveryInfo Set PhotoFile" + Integer.toString(i+1) + " = '" + info.mPhotos[i] + "' WHERE CustID = '" + info.mCustID + "';");
//    		}
    	}
        info.mPhotoNum--;
        db.execSQL("UPDATE DeliveryInfo Set PhotoNum = '" + info.mPhotoNum + "' WHERE CustID = '" + info.mCustID + "';");
        
		System.out.println("UPDATE DeliveryInfo PhotoNum=>" + info.mPhotoNum);

//Add 2015-02-20 End
        db.close();
        dbHelper.close();

        updatePhotoBtn2(mTargetIndex, (Button)mTargetBtn); // 写真ボタンの状態を更新する
        
//        mSelectedView = null;
        
        mPhotoCnt = info.mPhotoNum;
		Toast.makeText(this, "写真「" + fileName + "」を削除しました", Toast.LENGTH_SHORT).show();
		return ;
	}
// Add 2015-02-06 End
// 写真編集 End
	
// Cut by CPJsunagawa '2015-06-22 Start
// Add 2015-06-07 Start
		// 独自アイコンの描画
		//public void onDrawUserIcon(double latitude, double longitude, String iconPath, String szComment) {
		public Icon onDrawUserIcon(double latitude, double longitude, String iconName, String iconPath, String szComment) {
			
	    	System.out.println("Into onDrawUserIcon");

	        NaviApplication app = (NaviApplication)getApplication();
	        ItsmoNaviDriveExternalApi api = app.getExtApi();
//*
	    	Icon icon = new Icon();
	        try {
	            icon.setPoint(latitude, longitude);
	            icon.setUrl(iconPath);
//	            icon.setDescription("a"); // descriptionを設定しないとエラーとなってしまう為
	            icon.setDescription(szComment); // descriptionを設定しないとエラーとなってしまう為
	            /*int nUserFigureID =*/ api.drawIcon("1", icon); // layerは、"1"固定とする
	        } catch(NaviException e) {
	            e.printStackTrace();
	        }
	        
	        return icon;
	      //*/
/*	        
	        double dLat = latitude * 3600000.0;
	        double dLon = longitude * 3600000.0;
	        
        	JNILong nFigureID = new JNILong();
        	long ret = NaviRun.GetNaviRunObj().JNI_NE_AddUserFigure_Point(app.getUserIconLayerID(), (long)dLon, (long)dLat, iconName, iconPath, nFigureID);

	        System.out.println("Out onDrawUserIcon");
			return nFigureID.getLcount();
*/			
		}

/* 2015-07-15 一旦コメント
		// 独自ラインの描画
		public void onDrawUserLine(String szLayId, List<GeoPoint> geoPoint) {
			
	    	System.out.println("Into onDrawUserLine");

	        NaviApplication app = (NaviApplication)getApplication();
	    	System.out.println("Into onDrawUserLine(1)");
//	        ItsmoNaviDriveExternalApi api = app.getExtApi();
	    	System.out.println("Into onDrawUserLine(2)");

	        LineDrawParam lineDrawParam = new LineDrawParam();
//	        lineDrawParam.setLineColor(0xFFFF0000);
//	        Line line = new Line(lineDrawParam);
	    	System.out.println("Into onDrawUserLine(3)");

//	        LineDrawParam lineDrawParam = null;
	        lineDrawParam.setLineWidth(3);
	        lineDrawParam.setLineColor(getTitleColor());
	    	Line line = new Line(lineDrawParam);
	        try {
	        	double latitude, longitude;
				line.setPath(geoPoint);
	            api.drawLine(szLayId, line);
	        } catch(NaviException e) {
	            e.printStackTrace();
	        }

	        System.out.println("Out onDrawUserLine");
			return ;
		}
*/
// Add 2015-06-07 End
		
	    private final static String LAYER_ID        = "12345";
	    private final static String POINT_NAME04    = "東京スカイツリー";
	    private final static double LATITUDE04      = 35.71007d;
	    private final static double LONGITUDE04     = 139.80948d;
	    private final static String POINT_NAME05    = "事務所";
	    private final static double LATITUDE05      = 35.648576d;
	    private final static double LONGITUDE05     = 139.715226d;

	    // ライン描画
	    public void drawLine(boolean isDecide) {
//	    private void drawLine() {
	        try {

		        System.out.println("Into drawLine");
//		        api = ((ExternalApiApplication)getApplication()).getApi();
//		        api = ((NaviApplication)getApplication()).getApi();
		        NaviApplication na = (NaviApplication)getApplication();
		        api = na.getApi();
//        		connect();

/*
	        	GeoPoint gPoint;
	            List<GeoPoint> geoPoints = new ArrayList<GeoPoint>();
	            // ラインの始点～終点の緯度経度
//	            geoPoints.add(new GeoPoint(LATITUDE01, LONGITUDE01));
	            //gPoint = oNaviApi.getCenter();
//	            gPoint = api.getCenter();
//	            geoPoints.add(gPoint);
	            geoPoints.add(new GeoPoint(LATITUDE05, LONGITUDE05));
	            geoPoints.add(new GeoPoint(LATITUDE04, LONGITUDE04));
	            // ライン線種
	            LineDrawParam lineDrawParam = new LineDrawParam();
	            lineDrawParam.setLineWidth(3);
	            lineDrawParam.setLineColor(0xFFFF0000);
	            Line line = new Line(lineDrawParam);
	            line.setPath(geoPoints);

	            api.drawLine(LAYER_ID, line);
*/
//	            api.wakeupNaviApp();
		        // 現時点の中心座標を取得する。
		        mapView.getMapCentrePos();
		        long lon = mapView.getCoordinate().getM_lLong();
		        long lat = mapView.getCoordinate().getM_lLat();
		        GeoPoint tky = GeoUtils.toWGS84 (lat, lon);
		        double d_lat = tky.getLatitude();
		        double d_lon = tky.getLongitude();
		        // 最新の中心座標を格納する。
		        UserLineInfo uli = na.getDrawingUserLineInfo();
		        uli.AddPoint(api, LAYER_ID, d_lat, d_lon); // ←この時に、地図画面上に描画される。
		        // 次点を選択させる
		        Intent mapActInt = getIntent();
		        if(isDecide) {
            		// 確定させる
            		UserLineInfo lastULI = na.commitDrawingUserLineInfo();
            		if (lastULI == null) {
            			AlertDialog.Builder adb = new AlertDialog.Builder(this);
            			adb.setMessage("頂点が確定できません。");
            			adb.setPositiveButton(R.string.btn_ok, null);
            			adb.show();
            		} else {
            			// DBへ格納する。
            			Calendar c = Calendar.getInstance();
            			StringBuilder dateKey = new StringBuilder();
            			dateKey.append(c.get(Calendar.YEAR));
            			dateKey.append("/");
            			dateKey.append(c.get(Calendar.MONTH) + 1);
            			dateKey.append("/");
            			dateKey.append(c.get(Calendar.DAY_OF_MONTH));
            			dateKey.append("-");
            			dateKey.append(c.get(Calendar.HOUR_OF_DAY));
            			dateKey.append(":");
            			dateKey.append(c.get(Calendar.MINUTE));
            			dateKey.append(":");
            			dateKey.append(c.get(Calendar.SECOND));
            			
            			TrackIconEdit.ExecuteDrawUserLine(this, 2, lastULI.mFigureLine.getPath(), dateKey.toString());
            			
            			// 全戻しのフラグを立てる
            			mapActInt.putExtra(Constants.INTENT_EXTRA_DRAW_LINE_ALL_CANCEL, true);
            			//setIntent(mapActInt);
            			//mapActInt.putExtra(Constants.INTENT_EXTRA_DRAW_LINE, mapActInt.getIntExtra(Constants.INTENT_EXTRA_DRAW_LINE, 0) - 1);
            			setResult(RESULT_CANCELED, mapActInt);
            			finish();
            		}
            	} else {
	            	int nowIdx = mapActInt.getIntExtra(Constants.INTENT_EXTRA_DRAW_LINE, -1);
	            	if (nowIdx <= -1) {return;}
	            	
	            	startDrawLine(nowIdx + 1);//	            api.wakeupNaviApp();
            	}
	        } catch (NaviException e) {
	            e.printStackTrace();
	        }
	    }
// Cut by CPJsunagawa '2015-06-22 End

// Add by CPJsunagawa '2015-08-08 Start
    /**
     * 登録されているアイコンを地図画面に表示する。
     * @param info
     * @param ocl キャンセル時に何かやりたいときにこれを定義する。
     */
    public int ExecuteShowRegistIcon()
    {

    	System.out.println("*** MapActivity ExecuteShowRegistIcon ***");

    	String strDir = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) +
                "/" +NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "icon2/";
    	NaviApplication app = (NaviApplication)getApplication();
    	ItsmoNaviDriveExternalApi api = app.getExtApi();
    	try
    	{
    		api.removeLayer("1");
    	}
    	catch (NaviException e)
    	{
    		e.printStackTrace();
    	}
    	
    	for (int i = 0; i <= app.getUserIconInfoCount() - 1; i++)
    	{
    		UserIconInfo uii = app.getUserIconInfo(i);
    		String iconFile = strDir + uii.mIconFile;
    		if(uii.mFigureKind == 1) {
    			uii.mFigureIcon = onDrawUserIcon(uii.dLat1, uii.dLon1, Integer.toString(uii.mID), iconFile, "");
    		}
    	}
    	
// Add 2016-01-08 デモ用 Start
        double latitude = 0;
        double longitude = 0;
        String iconPath = null;
        String iconFile = null;
        NaviAppDataPath naviPath = app.getNaviAppDataPath();
        //iconPath = naviPath.getNaviAppRootPath() +  "/" + "icon2/thunder1.png";
        iconPath = naviPath.getNaviAppRootPath() +  "/";
        iconFile = iconPath + "icon2/pizzla_logo.png";
        System.out.println("iconPath => " + iconPath );
        latitude = 35.653528;
        longitude = 139.713797;
        onDrawUserIcon(latitude, longitude, iconFile, iconFile, "店舗");
// Add 2016-01-08 デモ用 End
    	
    	for (int i = 0; i <= app.getUserLineInfoCount() - 1; i++)
    	{
    		UserLineInfo uli = app.getUserLineInfo(i);
    		try
    		{
    			api.drawLine(LAYER_ID, uli.mFigureLine);
    		}
    		catch (NaviException e)
    		{
    			e.printStackTrace();
    		}
    	}
        return 1;
    }
// Add by CPJsunagawa '2015-08-08 Start


// Add by CPJsunagawa '2015-08-08 Start
    /**
     * 登録されている図形情報をクラスに格納する。
     * @param 
     * @param 
     */
    public int ExecuteSetUserIconInfo()
    {

    	System.out.println("*** MapActivity ExecuteSetUserIconInfo ***");
    	
        NaviApplication app = (NaviApplication)getApplication();
        
        app.clearUserIconInfo();

        TrackIconEdit.DbHelper helper = new TrackIconEdit.DbHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();

        // DBに登録されているレコード数を取得する
        Cursor cur = db.rawQuery("SELECT * FROM FigureData", null);
        if( cur == null ) return 0;
        int num = cur.getCount();

        int nIDindex = cur.getColumnIndex("_id");
        int nIconIndexindex = cur.getColumnIndex("nIconIndex");
        int nFigureKindindex = cur.getColumnIndex("nFigureKind");
        int nLat1index = cur.getColumnIndex("Lat1");
        int nLon1index = cur.getColumnIndex("Lon1");
        int nLat2index = cur.getColumnIndex("Lat2");
        int nLon2index = cur.getColumnIndex("Lon2");
        int nIconFileindex = cur.getColumnIndex("strIconFile");

/*
        String strDir = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) +
                "/" +NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "icon2/";
*/        
        // 取得したアイコン情報をクラスに設定する
        while(cur.moveToNext()){
/*        	
            // アイコン情報の設定
            UserIconInfo info = new UserIconInfo();
            info.mID = cur.getInt(nIDindex);
            info.mIconIndex = cur.getInt(nIconIndexindex);
            info.mFigureKind = cur.getInt(nFigureKindindex);
            info.dLat1 = cur.getDouble(nLat1index);
            info.dLon1 = cur.getDouble(nLon1index);
            info.dLat2 = cur.getDouble(nLat2index);
            info.dLon2 = cur.getDouble(nLon2index);
            info.mIconFile = cur.getString(nIconFileindex);

            app.addUserIconInfoItem(info);
*/        
        	int id = cur.getInt(nIDindex);
        	int kind = cur.getInt(nFigureKindindex);
        	
        	if (kind == 2) {
        		UserLineInfo info = new UserLineInfo();
        		info.mID = id;
        		info.mIconIndex = 0;
        		info.mKeyComment = cur.getString(nIconFileindex);
        		
        		// ここでもうLineを生成してしまう。
        		info.mFigureLine = new Line(info.mLineDrawParam);
        		
        		Cursor curP = db.rawQuery("SELECT * FROM FigureCoord WHERE _id = " + id + " ORDER BY _order", null);
        		if (curP != null)
        		{
        			int nLat1indexP = curP.getColumnIndex("Lat1");
        	        int nLon1indexP = curP.getColumnIndex("Lon1");
        	        while (curP.moveToNext())
        	        {
        	        	info.mFigureLine.addPoint(curP.getDouble(nLat1indexP), curP.getDouble(nLon1indexP));
        	        }
        		}
        		curP.close();
        		
        		app.addUserLineInfoItem(info);
        	} else {
	            // アイコン情報の設定
	            UserIconInfo info = new UserIconInfo();
	            //info.mID = cur.getInt(nIDindex);
	            info.mID = id;
	            info.mIconIndex = cur.getInt(nIconIndexindex);
	            info.mFigureKind = cur.getInt(nFigureKindindex);
	            info.dLat1 = cur.getDouble(nLat1index);
	            info.dLon1 = cur.getDouble(nLon1index);
	            info.dLat2 = cur.getDouble(nLat2index);
	            info.dLon2 = cur.getDouble(nLon2index);
	            info.mIconFile = cur.getString(nIconFileindex);
	
	            app.addUserIconInfoItem(info);
        	}
        }
        cur.close();
 		db.close();
 		helper.close();
        
        return 1;
    }
// Add by CPJsunagawa '2015-08-08 End
    
// Add by CPJsunagawa '2015-08-09 Start
    // 登録済みのアイコンをリスト表示する
	private int mSelectedIconIndex = 0;
	private AlertDialog mSelectUserIconForDismiss;
	
	//
	// 登録済みのアイコンリストをダイアログで表示する
	//
//    public void showIconList(boolean isShow, boolean isResetPage, boolean isSetScrollView) {
    public void showIconList(final int index, final int kind) {
        System.out.println("MapActivity INTO showIconList");

        LayoutInflater oInflater = LayoutInflater.from(this);
        // 画面を追加
        LinearLayout ll = (LinearLayout)oInflater.inflate(R.layout.select_icon_list, null);
        if( ll == null ) return;
        
        NaviApplication app = (NaviApplication)getApplication();
        if (!app.isExistUserInfoList())
        {
            ll.setVisibility(View.GONE);
            return;
        }
// Add by CPJsunagawa '2015-08-14 Start
        // スクロールビューの位置を頭に持って行く
        ScrollView sv = (ScrollView)findViewById(R.id.infoListSV);
        sv.fullScroll(ScrollView.FOCUS_UP);
// Add by CPJsunagawa '2015-08-14 End
        
        ll.setVisibility(View.VISIBLE);
        //ll.removeAllViews();

        // マルチWindows幅対応 Start
        WindowManager wm = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
        Display disp = wm.getDefaultDisplay();
        final int listWidth = (int)(disp.getWidth() * 0.43f);
        final int winHeight = disp.getHeight();

// Add by CPJsunagawa '2015-08-14 Start
        final int WC = ViewGroup.LayoutParams.WRAP_CONTENT; 
        final int FP = ViewGroup.LayoutParams.FILL_PARENT;
// Add by CPJsunagawa '2015-08-14 End

//        LayoutInflater Inflater = LayoutInflater.from(this);
        ImageButton iconBtn;

        View.OnClickListener stOcl = new View.OnClickListener() {
            // 
			@Override
			public void onClick(View view) {
				
            	//LinearLayout selectedLL = (LinearLayout)view;
            	String idxIcon = (String)view.getTag();
            	mSelectedIconIndex = Integer.parseInt(idxIcon);
            	//setForDetail(mSelectedIconIndex, selectedLL);
            	
                NaviApplication app = (NaviApplication)getApplication();
                UserIconInfo mUserIconInfo = app.getUserIconInfo(mSelectedIconIndex);

                // 選択されたシンボルを移動する
            	selectMoveIcon(mSelectedIconIndex);

            	if(kind == TrackIconEdit.PRESSED_DELETE_ICON) {
            		// アイコン削除
            		
            	} else {
                    // 移動元のアイコンを消す
                    NaviRun.GetNaviRunObj().JNI_NE_DeleteUserFigure(mUserIconInfo.mFigureIcon.getParentLayer().getNELayerId(),
                    		mUserIconInfo.mFigureIcon.getNEID());
            	}
			}
		};
        
        int nMaxIconCount = app.getUserIconInfoCount();

    	String navidatapath = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) +
                "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME;
        String ICON_PATHNAME = navidatapath + "/icon2/";
        
        // findViewById()はsetContentView()より後に記述しなければいけません
        //setContentView(R.layout.select_icon_list); 
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setPositiveButton("OK",  new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

            	if(kind == TrackIconEdit.PRESSED_DELETE_ICON) {
            		AlertDialog.Builder adb2 = new AlertDialog.Builder(MapActivity.this);
            		adb2.setMessage("指定されたアイコンを削除します。よろしいですか？");
                    adb2.setPositiveButton("はい",  new DialogInterface.OnClickListener() {
                    	@Override
            			public void onClick(DialogInterface dialog, int which) {
                    		// アイコン削除
    	            		startDeleteIcon(mSelectedIconIndex);
                    	}
                    });
                    adb2.setNegativeButton("いいえ", null);
                    adb2.show();
            	} else {
					// アイコン移動
					startMoveIcon(mSelectedIconIndex);
            	}
    		}
		});
        adb.setNegativeButton("キャンセル", null);
        adb.setView(ll);
        
        // TableLayoutを生成する
        TableLayout iconTbl = (TableLayout)ll.findViewById(R.id.selectIconTable);
        
//        for( int i = 0; i <= nMaxIconCount; i++ ) {
        for( int i = 0; i < nMaxIconCount; i++ ) {
//            LinearLayout item = (LinearLayout)Inflater.inflate(R.layout.select_icon_list, null);

            UserIconInfo mUserIconInfo = app.getUserIconInfo(i);
            
            // TableLayoutを生成する
            //TableLayout iconTbl = (TableLayout)ll.findViewById(R.id.selectIconTable);
//            TableLayout iconTbl = (TableLayout)sv.findViewById(R.id.selectIconTable);
            
            // TableRowを生成する
            TableRow oneRow = new TableRow(this);
            
            LinearLayout llInner = (LinearLayout)oInflater.inflate(R.layout.select_icon_list_inner, null);
            
        	// アイコンボタン
        	iconBtn = (ImageButton)llInner.findViewById(R.id.sel_icon_image);
//        	iconBtn.setImageResource(R.drawable.arrow_leftup);
        	
        	String uriString;
        	uriString = "file://" + ICON_PATHNAME + mUserIconInfo.mIconFile;
//        	uriString = ICON_PATHNAME + mUserIconInfo.mIconFile;
        	
        	String szIconName = mUserIconInfo.getIconFileNameByIconIndex(mUserIconInfo.mIconIndex);
        	// 暫定対応
        	switch(mUserIconInfo.mIconIndex) {
        	case UserIconInfo.ICON_ARROW_LEFTUP_INDEX:
            	iconBtn.setImageResource(R.drawable.arrow_leftup);
            	break;
        	case UserIconInfo.ICON_ARROW_UP_INDEX:
            	iconBtn.setImageResource(R.drawable.arrow_up);
            	break;
        	case UserIconInfo.ICON_ARROW_RIGHTUP_INDEX:
            	iconBtn.setImageResource(R.drawable.arrow_rightup);
            	break;
        	case UserIconInfo.ICON_ARROW_LEFT_INDEX:
            	iconBtn.setImageResource(R.drawable.arrow_left);
            	break;
        	case UserIconInfo.ICON_ARROW_RIGHT_INDEX:
            	iconBtn.setImageResource(R.drawable.arrow_right);
            	break;
        	case UserIconInfo.ICON_ARROW_LEFTDOWN_INDEX:
            	iconBtn.setImageResource(R.drawable.arrow_leftdown);
            	break;
        	case UserIconInfo.ICON_ARROW_DOWN_INDEX:
            	iconBtn.setImageResource(R.drawable.arrow_down);
            	break;
        	case UserIconInfo.ICON_ARROW_RIGHTDOWN_INDEX:
            	iconBtn.setImageResource(R.drawable.arrow_rightdown);
            	break;
        	case UserIconInfo.ICON_IPPOU_UP_INDEX:
            	iconBtn.setImageResource(R.drawable.ippou_up);
            	break;
        	case UserIconInfo.ICON_IPPOU_LEFT_INDEX:
            	iconBtn.setImageResource(R.drawable.ippou_left);
            	break;
        	case UserIconInfo.ICON_IPPOU_RIGHT_INDEX:
            	iconBtn.setImageResource(R.drawable.ippou_right);
            	break;
        	case UserIconInfo.ICON_IPPOU_DOWN_INDEX:
            	iconBtn.setImageResource(R.drawable.ippou_down);
            	break;
        	case UserIconInfo.ICON_HYOSHIKI_1_INDEX:
            	iconBtn.setImageResource(R.drawable.hyoshiki1_s);
            	break;
        	case UserIconInfo.ICON_HYOSHIKI_2_INDEX:
            	iconBtn.setImageResource(R.drawable.hyoshiki2_s);
            	break;
        	case UserIconInfo.ICON_HYOSHIKI_3_INDEX:
            	iconBtn.setImageResource(R.drawable.hyoshiki3_s);
            	break;
        	case UserIconInfo.ICON_HYOSHIKI_4_INDEX:
            	iconBtn.setImageResource(R.drawable.hyoshiki4_s);
            	break;
        	case UserIconInfo.ICON_HYOSHIKI_5_INDEX:
            	iconBtn.setImageResource(R.drawable.hyoshiki5_s);
            	break;
        	case UserIconInfo.ICON_HYOSHIKI_6_INDEX:
            	iconBtn.setImageResource(R.drawable.hyoshiki6_s);
            	break;
        	case UserIconInfo.ICON_HYOSHIKI_7_INDEX:
            	iconBtn.setImageResource(R.drawable.hyoshiki7_s);
            	break;
        	case UserIconInfo.ICON_HYOSHIKI_8_INDEX:
            	iconBtn.setImageResource(R.drawable.hyoshiki8_s);
            	break;
        	case UserIconInfo.ICON_HYOSHIKI_9_INDEX:
            	iconBtn.setImageResource(R.drawable.hyoshiki9_s);
            	break;
        	}
        	//        	iconBtn.setImageResource(R.drawable.arrow_leftup);
//        	iconBtn.setTag(R.drawable.arrow_leftup);

//        	iconBtn.setImageURI(Uri.parse(uriString));
//        	iconBtn.setTag(R.drawable.arrow_leftup);
        	iconBtn.setTag(Integer.toString(i));
        	
        	// TableRowにイメージボタンを追加
        	//oneRow.addView(iconBtn);
        	
        	iconBtn.setOnClickListener(stOcl);
            
        	long Longitude, Latitude;
        	String szAddress;
        	Latitude = (long) (mUserIconInfo.dLat1 * 3600000);
        	Longitude = (long) (mUserIconInfo.dLon1 * 3600000);
        	JNIString addressString = new JNIString();
            NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(Longitude, Latitude, addressString);
            szAddress = addressString.getM_StrAddressString();

            // アイコン登録位置
            TextView tv = (TextView)llInner.findViewById(R.id.plot_place);
            tv.setText(szAddress);
            
        	// TableRowにアイコン位置を追加
        	oneRow.addView(llInner);
        	
        	iconTbl.addView(oneRow, new TableLayout.LayoutParams(FP, WC));
        	
            //addView(iconTbl, new LinearLayout.LayoutParams(listWidth, LinearLayout.LayoutParams.MATCH_PARENT) );
            
        }
        //AlertDialog.Builder adb = new AlertDialog.Builder(this);
        //adb.setPositiveButton("キャンセル", null);
        //adb.setView(ll);
        
//        mSelectUserIconForDismiss = adb.show();
        
        //AlertDialog ad = adb.create();
        
        mSelectUserIconForDismiss = adb.create();
        WindowManager.LayoutParams wmlp = mSelectUserIconForDismiss.getWindow().getAttributes();

        wmlp.gravity = Gravity.TOP | Gravity.LEFT;
        wmlp.x = 10; //x位置
        wmlp.y = 10; //y位置

        mSelectUserIconForDismiss.getWindow().setAttributes(wmlp);
        mSelectUserIconForDismiss.show();
        
    }
// Add by CPJsunagawa '2015-08-09 End

 // Add by CPJsunagawa '2015-08-09 End
	// 選択されたシンボルを移動する
    public void selectMoveIcon(int mSelectedIconIndex) {
        System.out.println("MapActivity INTO selectMoveIcon");
        
        //aaa
        NaviApplication app = (NaviApplication)getApplication();
        UserIconInfo mUserIconInfo = app.getUserIconInfo(mSelectedIconIndex);
        
        // 選択されたアイコンの位置に飛ぶ
		try
		{
	    	ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
			// 拡大して表示
			oNaviApi.setZoom(ZoomLevel.M_40);
			
			// 世界測地系BLを日本測地系に変換して中心に表示
			GeoPoint gPoint = new GeoPoint();
			gPoint.setLatitude(mUserIconInfo.dLat1);
			gPoint.setLongitude(mUserIconInfo.dLon1);
			gPoint.setWorldPoint(true);
			oNaviApi.setCenter(gPoint);
		}
		catch (NaviException e)
		{
			e.printStackTrace();
		}
/*        
        // まず、移動元のアイコンを消す
        NaviRun.GetNaviRunObj().JNI_NE_DeleteUserFigure(mUserIconInfo.mFigureIcon.getParentLayer().getNELayerId(),
        		mUserIconInfo.mFigureIcon.getNEID());
*/
        
    }
    
// Cut 20160118 Start
/*
    //
    // 軌跡データ情報格納DB用Helper
    // 
    static class DbHelper extends SQLiteOpenHelper {
        private static String navidatapath = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) +
             "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME;
        public static String DATABASE_NAME = navidatapath + "/track_data.db";

        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // DBが存在しない時にのみ呼ばれる
            db.execSQL("CREATE TABLE TrackData (_id INTEGER PRIMARY KEY, CourseName TEXT, SaveDir TEXT, FileName TEXT);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // DBの変更があれば、ここで行う
        }

        // DBに登録されているレコード数を取得する
        public static int getTrackDataCount(SQLiteDatabase db) {
            Cursor cur = db.rawQuery("SELECT * FROM TrackData", null);
            if( cur == null ) return 0;
            int num = cur.getCount();
            cur.close();
            return num;
        }

        // DBに登録されているコース名を取得する
        public static String[] getTrackData(SQLiteDatabase db) {
            Cursor cur = db.rawQuery("SELECT CourseName FROM TrackData", new String[] {});
            if( cur == null ) return null;
            int num = cur.getCount();
            String courseName[] = new String[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	courseName[i] = cur.getString(0);
                cur.moveToNext();
            }
            cur.close();
            return courseName;
        }

        // DBに登録されているインデックスを取得する
        public int[] getTrackDataIndex(SQLiteDatabase db) {
        	String  szSql = "SELECT `_id` FROM TrackData";
            Cursor cur = db.rawQuery(szSql, null);
            if( cur == null ) return null;
            int num = cur.getCount();
            int nIndex[] = new int[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	nIndex[i] = cur.getInt(0);
                cur.moveToNext();
            }
            cur.close();
            return nIndex;
        }

        // DBに登録されているコース名を取得する
        public String[] getTrackDataFileName(SQLiteDatabase db) {
            Cursor cur = db.rawQuery("SELECT FileName FROM TrackData", new String[] {});
            if( cur == null ) return null;
            int num = cur.getCount();
            String FileName[] = new String[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	FileName[i] = cur.getString(0);
                cur.moveToNext();
            }
            cur.close();
            return FileName;
        }

        // コース名から該当の軌跡データファイル名を返却する
        public String[] getTrackFileByCourseName(SQLiteDatabase db, String CourseName) {
            Cursor cur = db.rawQuery("SELECT * FROM TrackData", new String[] {});
            //Cursor cur = db.rawQuery("SELECT FileName FROM TrackData", new String[] {});
            if( cur == null ) return null;
            int num = cur.getCount();
            String FileName[] = new String[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	String strWork;
            	strWork = cur.getString(0);		// _ID
            	strWork = cur.getString(1);		// コース名
            	strWork = cur.getString(2);		// 格納フォルダ
            	strWork = cur.getString(3);		// ファイル名
//            	if(CourseName == cur.getString(1)) {
               	if(CourseName.equals(cur.getString(1))) {
            		FileName[0] = cur.getString(3);
            		break;
            	}
                cur.moveToNext();
            }
            cur.close();
            return FileName;
        }

        // コース名から該当の格納フォルダを返却する
        public String[] getSaveDirByCourseName(SQLiteDatabase db, String CourseName) {
            Cursor cur = db.rawQuery("SELECT * FROM TrackData", new String[] {});
            if( cur == null ) return null;
            int num = cur.getCount();
            String SaveDir[] = new String[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
               	if(CourseName.equals(cur.getString(1))) {
            		SaveDir[0] = cur.getString(2);
            		break;
            	}
            	cur.moveToNext();
            }
            cur.close();
            return SaveDir;
        }
    }
*/
// Cut 20160118 End
	
}
// Add 2011/10/19 katsuta End <--
