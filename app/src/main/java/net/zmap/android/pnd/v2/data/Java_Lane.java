/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           Java_Lane.java
 * Description    Lane情報設定
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class Java_Lane {
    private long lnLaneRestDist;
    private int iExistLaneData;
    /**
    * Created on 2010/08/06
    * Title:       getLnLaneRestDist
    * Description:  Lane情報距離メートルを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLnLaneRestDist() {
        return lnLaneRestDist;
    }

    /**
    * Created on 2010/08/06
    * Title:       getIExistLaneData
    * Description:  Lane情報存在かを判断する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public int getIExistLaneData() {
        return iExistLaneData;
    }
}
