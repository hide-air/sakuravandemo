package net.zmap.android.pnd.v2.common.utils;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.StringTokenizer;

import android.os.Environment;
import android.os.StatFs;

/**
 * 外部ストレージ(SDCARD)用ユーティリティクラス
 *
 * @author Y.Hayashida
 * @version 1.0
 * @since 2012/10/19
 */
public class StorageUtil {

	// 決められた環境変数から取得できない場合に、
	// 環境変数すべてをチェックする。その際に、パスと認識するための文字列定義
	private final static String[] device_string = {"/mnt", "/storage", "/sdcard"};

	/**
	 * 外部ストレージのパスを取得する。<br/>
	 * /sdcard/ItsmoNaviDriveが存在すれば、/sdcard返却する。<br/>
	 * ※末尾に"/"はつかないので注意
	 *
	 * @return String 外部ストレージ
	 */
	public static String getSdStoragePath(String packageDir) {
		String path;
		File externalStoragePath = Environment.getExternalStorageDirectory();

		// 既存のディレクトリチェック
		// 既に動作している環境対応
		// /sdcard/ItsmoNaviDriveがあれば、そのまま動作させるため
		if (checkExistsDir(externalStoragePath.getPath(), packageDir)) {
			return externalStoragePath.getPath();
		}

		// for Motorola Series
		path = System.getenv("EXTERNAL_ALT_STORAGE");
		if (checkExistsDir(path, packageDir))
			return path;

		// for Sumsung
		path = System.getenv("EXTERNAL_STORAGE2");
		if (checkExistsDir(path, packageDir))
			return path;

		// for 旧Sumsung + 標準的
		path = System.getenv("EXTERNAL_STORAGE");
		if (checkExistsDir(path, packageDir))
			return path;

		// for Acer
		path = System.getenv("EXTERNAL_REMOVABLE_SDCARD");
		if (checkExistsDir(path, packageDir))
			return path;

		// for CASIO
		path = System.getenv("MICROSD_STORAGE");
		if (checkExistsDir(path, packageDir))
			return path;

		// for dtab
		path = "/mnt/sdcard2";
		if (checkExistsDir(path, packageDir))
			return path;

		// for Clarion(ソリッドナビ)
		path = "/mnt/xdata";
        if (checkExistsDir(path, packageDir)) {
            return path;
        }

		List<String> storagePathList = getStoragePaths();
		for (String storagePath : storagePathList) {
	        path = searchSdStoragePath(new File(storagePath), packageDir);
	        if (path != null) {
	            return path;
	        }
		}

//        long checkSize = 0L;
//        String checkPath = null;
//        if (!storagePathList.contains(externalStoragePath.getPath())) {
//            storagePathList.add(externalStoragePath.getPath());
//        }
//        for (String storagePath : storagePathList) {
//            try {
//                StatFs statFs = new StatFs(storagePath);
//                long blockSize = statFs.getBlockSize();
//                long availableBlocks = statFs.getAvailableBlocks();
//                long available = blockSize * availableBlocks;
//                File workPath = new File(storagePath);
//                if (workPath.canRead() && workPath.canWrite() && available > checkSize) {
//                    checkSize = available;
//                    checkPath = storagePath;
//                }
//            }
//            catch (IllegalArgumentException e) {
//                NaviLog.e(NaviLog.PRINT_LOG_TAG, "StatFs [" + storagePath + "]");
//            }
//        }
//        if (checkPath != null) {
//            return checkPath;
//        }

		// ここまでで取得できない場合、環境変数から探し出す
		path = searchStoragePathFromEnv(packageDir);
		if( path != null ){
			return path;
		}

//		// 見つからないが、とりあえず、/sdcardを返却
//		return externalStoragePath.getPath();
        // B向け
        return null;
	}

	/**
	 * 環境変数からマウントらしきパスを探し出し、総当たりチェックを行う。
	 * @param packageDir 存在しなければ行けないディレクトリ名
	 * @return  見つかった場合は、そのパス（パッケージ名含まず）
	 * 			見つからない場合は、null
	 */
	public static String searchStoragePathFromEnv(String packageDir) {

		Map<String, String> mapEnv = System.getenv();

		for (Entry<String, String> entry : mapEnv.entrySet()) {
            String strData = entry.getValue();

			StringTokenizer stPeriod = new StringTokenizer(strData, File.pathSeparator);
			while (stPeriod.hasMoreTokens()) {

				String strPathChk = stPeriod.nextToken().toString();

				boolean bl_find_device = false;
				for(int ix = 0, cnt = device_string.length ; ix < cnt ; ix++ ){
					if( strPathChk.indexOf(device_string[ix]) != -1 ){
						bl_find_device = true;
						break;
					}
				}

				// チェック条件を満たしているので存在チェックを行う
				if ( bl_find_device ) {
					String strExist = strPathChk + File.separator + packageDir + File.separator;
					File fleExist = new File(strExist);
					if (fleExist.exists()) {
						// 外部ストレージパスを返却
						return strPathChk;
					}
				}
			}
		}

		return null;
	}

	/**
	 * SDカードに、ItsmoNaviDriveのデータディレクトリがあるかチェック
	 *
	 * @param sdpath
	 * @return boolean 存在有無
	 */
	private static boolean checkExistsDir(String sdpath, String packageDir) {
		File chkExists = new File(sdpath + File.separator + packageDir);
		if (chkExists.exists()) {
			return true;
		}
		return false;
	}

    private static String searchSdStoragePath(File path, String packageDir) {
        if (!path.isDirectory() || !path.canRead() || !path.canWrite() || packageDir == null || packageDir.length() == 0) {
            return null;
        }

        File[] fileList = path.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isDirectory() && !file.isHidden();
            }
        });
        if (fileList == null) {
            return null;
        }

        String itsmoNaviRootPath = null;
        for (File file : fileList) {
            if (packageDir.equals(file.getName())) {
                itsmoNaviRootPath = file.getParent();
                if (itsmoNaviRootPath == null) {
                    itsmoNaviRootPath = "";
                }
                break;
            }
//
//            itsmoNaviRootPath = searchSdStoragePath(file, packageDir);
//            if (itsmoNaviRootPath != null) {
//                break;
//            }
        }

        return itsmoNaviRootPath;
    }

    private static final String[] FSTAB_FILE_LIST = {"/system/etc/vold.fstab", "/etc/vold.fstab"};
    private static List<String> getStoragePaths() {
        List<String> mountList = new ArrayList<String>();

        File file = null;
        for (int i = 0; i < FSTAB_FILE_LIST.length; i++) {
            file = new File(FSTAB_FILE_LIST[i]);
            if (file.exists()) {
                break;
            }
        }

        Scanner scanner = null;
        try {
            // マウント情報を取得する
            scanner = new Scanner(new FileInputStream(file));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.startsWith("dev_mount") || line.startsWith("fuse_mount")) {
                    // dev_mount または fuse_mount のパスを登録する（同じパスは登録しない）
                    String path = line.replaceAll("\t", " ").split(" ")[2];
                    if (!mountList.contains(path)) {
                        mountList.add(path);
                    }
                }
            }
        }
        catch (FileNotFoundException e) {
            NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
        }
        finally {
            if (scanner != null) {
                scanner.close();
            }
        }

        for (int i = 0; i < mountList.size(); i++) {
            if (!isMounted(mountList.get(i))) {
                mountList.remove(i--);
            }
        }

        return mountList;
    }

    private static final String MOUNTS_FILE = "/proc/mounts";
    private static boolean isMounted (String path) {
        boolean isMounted = false;

        Scanner scanner = null;
        try{
            scanner = new Scanner(new FileInputStream(new File(MOUNTS_FILE)));

            while (scanner.hasNextLine()) {
                if (scanner.nextLine().contains(path)) {
                    isMounted = true;
                     break;
                }
            }
        } catch (FileNotFoundException e) {
            NaviLog.i(NaviLog.PRINT_LOG_TAG, e);
            // ファイルがなければこのチェックは行わない。
            isMounted = true;
        } finally {
            if(scanner != null){
                scanner.close();
            }
        }

        return isMounted;
    }
}
