package net.zmap.android.pnd.v2.vics;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.zmap.android.pnd.v2.data.JNITwoLong;

public class PROBE_POINT_DATA {
	private int nFuncCode = 0;
	private int nRoadFuncCode = 0;
//	private static final String strNaviID = "pnd.c2";
    private static final String strNaviID = "-";
	private JNITwoLong VehiclePos = new JNITwoLong();
	private Date pointDate = null;
	private int roadType = 0;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");


	public void setFuncCode(int funcCode){
		nFuncCode = funcCode;
	}

	public int getFuncCode(){
		return nFuncCode;
	}

	public void setRoadFuncCode(int funcCode){
		nRoadFuncCode = funcCode;
	}

	public int getRoadFuncCode(){
		return nRoadFuncCode;
	}

	public String getNaviID() {
		return strNaviID;
	}

//	public void setNaviID(String strNaviID) {
//		this.strNaviID = strNaviID;
//	}

	public JNITwoLong getVehiclePos() {
		return VehiclePos;
	}

	public void setVehiclePos(JNITwoLong Pos) {
		VehiclePos = Pos;
	}

	public Date getPointDate() {
		return pointDate;
	}

	public void setPointDate(Date date) {
		this.pointDate = date;
	}

	public int getRoadType() {
		return roadType;
	}

	public void setRoadType(int type) {
		this.roadType = type;
	}

	@Override
	public String toString() {
		String strRoadType = String.format("%d%03d", nRoadFuncCode , roadType);

		return "" + nFuncCode + ":" + strNaviID + ":" + VehiclePos.getM_lLat() + ":" + VehiclePos.getM_lLong()+ ":" +
					sdf.format(pointDate) + ":" + strRoadType;
	}
}
