package net.zmap.android.pnd.v2.common.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.SeekBar;

public class SeekImgBar extends SeekBar
{
	private final SeekImgBar seekBar = this;
	private Bitmap mImgThumb;
	private Bitmap mImgSeekBarOff;
	private Bitmap mImgSeekBarOn;

	public SeekImgBar(Context c){
		super(c);

		this.setThumb(new Thumb());
		this.setProgressDrawable(new Progress());
	}

	public SeekImgBar(Context context, AttributeSet attrs) {
		super(context, attrs);

		InitBmp(attrs);

	    this.setThumb(new Thumb());
	    this.setProgressDrawable(new Progress());
	}

	private void InitBmp(AttributeSet attrs)
	{
		String  Value;
		int		resId;

		Resources r = getResources();

		Value = attrs.getAttributeValue(null, "myThumb");
		if(Value != null)
		{
			resId = getResources().getIdentifier(Value, "drawable", getContext().getPackageName());
			mImgThumb = BitmapFactory.decodeResource(r, resId);
		}

		Value = attrs.getAttributeValue(null, "myImgOff");
		if(Value != null)
		{
			resId = getResources().getIdentifier(Value, "drawable", getContext().getPackageName());
			mImgSeekBarOff = BitmapFactory.decodeResource(r, resId);
		}

		Value = attrs.getAttributeValue(null, "myImgOn");
		if(Value != null)
		{
			resId = getResources().getIdentifier(Value, "drawable", getContext().getPackageName());
			mImgSeekBarOn = BitmapFactory.decodeResource(r, resId);
		}

//		Resources r = getResources();
//        mImgThumb      = BitmapFactory.decodeResource(r, R.drawable.seekbar2);
//        mImgSeekBarOff = BitmapFactory.decodeResource(r, R.drawable.seekbar1);
//        mImgSeekBarOn  = BitmapFactory.decodeResource(r, R.drawable.seekbar3);

	}

	private class Thumb extends Drawable
	{
	    @Override
	    public void setColorFilter(ColorFilter cf){};

	    @Override
	    public void setAlpha(int alpha){};

	    @Override
	    public int getOpacity(){return PixelFormat.OPAQUE;};

	    @Override
	    public boolean isStateful() { return true; }

	    @Override
	    public int getIntrinsicWidth(){ return getIntrinsicHeight(); }

	    @Override
	    public int getIntrinsicHeight(){ return seekBar.getHeight() - seekBar.getPaddingTop() - seekBar.getPaddingBottom(); };

	    @Override
	    public void draw(Canvas canvas){

	        if (seekBar.mImgThumb != null)
	        {
		    	Rect rect = new Rect();
		    	rect.set(getBounds());
		        Paint paint = new Paint();
		        paint.setAntiAlias(true);

		        int w = seekBar.mImgThumb.getWidth();
		        int h = seekBar.mImgThumb.getHeight();
		        Rect src = new Rect(0, 0, w, h);
		        Rect dst = new Rect(rect);
		        canvas.drawBitmap(seekBar.mImgThumb, src, dst, null);
	        }
		}
	}

	private class Progress extends Drawable
	{
	    @Override
	    public void setColorFilter(ColorFilter cf){};

	    @Override
	    public void setAlpha(int alpha){};

	    @Override
	    public int getOpacity(){return PixelFormat.OPAQUE;};

	    private static final int OFFSET_SEEKBAR_POS = 12;

	    @Override
	    public void draw(Canvas canvas){

	        if (seekBar.mImgSeekBarOff != null
	        	&& seekBar.mImgSeekBarOn != null)
	        {
				Paint paint = new Paint();
				paint.setAntiAlias(true);

				Rect rect = new Rect();
				rect.set(getBounds());
				rect.left  += OFFSET_SEEKBAR_POS;
				rect.right -= OFFSET_SEEKBAR_POS;

				int w = seekBar.mImgSeekBarOff.getWidth();
				int h = seekBar.mImgSeekBarOff.getHeight();
				Rect src = new Rect(0, 0, w, h);
				Rect dst = new Rect(rect.left, 0, rect.right, 0);

				int dstHeight = rect.height()/3;
				dst.top    = (rect.top +rect.height()/2) - dstHeight/2;
				dst.bottom = dst.top +dstHeight;

				canvas.drawBitmap(seekBar.mImgSeekBarOff, src, dst, null);

			//Log.i("test", "getLevel=" + getLevel());
				src.set(0, 0, w*getLevel()/10000, h);
				dst.right = dst.left + dst.width()*getLevel()/10000;
				canvas.drawBitmap(seekBar.mImgSeekBarOn, src, dst, null);
	        }
		}
	}
}

