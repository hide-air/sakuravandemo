package net.zmap.android.pnd.v2.api.event;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * 行政界移動情報クラス
 *
 * @see AreaListener
 * @see AreaListener#onChanged(Location, Area, Area)
 */
public final class Area implements Parcelable {
    /** 旧行政界名 */
    private List<String> mName;

    /**
     * コンストラクタ
     */
    public Area() {
    }

    private Area(Parcel in) {
        readFromPercel(in);
    }

    /**
     * コンストラクタ
     *
     * @param name
     *            行政界名
     */
    public Area(List<String> name) {
        mName = name;
    }

    /**
     * 行政界名の取得.<br /><br />
     *
     * リストは最初の要素から順に [都道府県] [市区町村] [大字] [字町名] のようになります.
     *
     * @return 行政界名
     */
    public List<String> getName() {
        return mName;
    }

    /**
     * 行政界名の設定
     *
     * @param name
     *            行政界名
     */
    public void setName(List<String> name) {
        mName = name;
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeStringList(mName);
    }

    private void readFromPercel(Parcel in) {
        mName = new ArrayList<String>();
        in.readStringList(mName);
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /* (非Javadoc)
     *
     */
    public static final Parcelable.Creator<Area> CREATOR = new Parcelable.Creator<Area>() {
        public Area createFromParcel(Parcel in) {
            return new Area(in);
        }
        public Area[] newArray(int size) {
            return new Area[size];
        }
    };
}