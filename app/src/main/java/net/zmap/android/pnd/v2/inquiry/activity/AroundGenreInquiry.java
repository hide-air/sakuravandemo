package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Around_ListItem;
import net.zmap.android.pnd.v2.data.ZPOI_Route_ListItem;
import net.zmap.android.pnd.v2.data.ZPOI_Route_Param;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.maps.OpenMap;

import java.util.ArrayList;
import java.util.List;

/**
 * 周辺検索（K-J5_ジャンル検索結果（shortCut））
 * @author XuYang
 *
 */
public class AroundGenreInquiry extends InquiryBaseLoading  {

	private static AroundGenreInquiry instance;
	private int iNextTreeIndex = 0;
	private ScrollTool oTool = null;
	private TextView oText = null;
	private JNILong RecCount = new JNILong();
	private List<POI_Around_ListItem> ardList = new ArrayList<POI_Around_ListItem>();
	private List<ZPOI_Route_ListItem> ardRouteList = new ArrayList<ZPOI_Route_ListItem>();
	private POI_Around_ListItem[] poi_Around_Listitem = null;
	private ZPOI_Route_ListItem[] poi_Route_Listitem = null;
	private JNILong GetCount;
	private long RecCnt = 0;
	private long RecIndex = 0;
	private JNITwoLong myPosi = new JNITwoLong();
	private  int  SearchCacheNum = 0;
	private boolean bListDealing = false;
//	private boolean listClickAble = true;
	private String msPointName = "";
	ScrollList scroll = null;
	private int DIALOG_NO_SEARCH_DATA = 1;
	private boolean mbAroundFlg = false; 	//沿いルート周辺と施設周辺のフラグ
	private int MAX_LIST_COUNT = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		this.startShowPage(NOTSHOWCANCELBTN);

	}
	static public AroundGenreInquiry GetGenreSrchGlobalRef() {
		return instance;
	}

	/**
	 * データを初期化
	 */
	private void initData() {
		if(mbAroundFlg){//沿いルート周辺検索
			//沿い周辺ジャンル検索の検索初期化
			//NaviRun.GetNaviRunObj().JNI_NE_POIRoute_Cancel();
		    NaviRun.GetNaviRunObj().JNI_NE_POIRoute_Clear();
			ZPOI_Route_Param stRoutData = new ZPOI_Route_Param();

			//沿い周辺検索の方向【デフォルト：両方】
			stRoutData.setEside(3);

			//沿い周辺検索の幅【デフォルト：500】
			stRoutData.setUlnRange(500);

			//沿い周辺検索の件数
// Chg 2011/10/03 katsuta Start -->
//			stRoutData.setLnCount(40);
			stRoutData.setLnCount(MAX_ROUTE_POINUM);
// Chg 2011/10/03 katsuta End <--

			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SetParam(stRoutData);

// Del 2011/09/27 katsuta Start --> 課題No.177
//			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchList();
// Del 2011/09/27 katsuta End <--

			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SetRoute();
// Add 2011/09/27 katsuta Start --> 課題No.177
			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchList();
// Add 2011/09/27 katsuta End <--

			long longCutCode = Long.valueOf(CommonLib.getStrShortcutCode());
			//yangyang add start QA49
			long longPointCode = Long.valueOf(CommonLib.getStrShortPointCode());
			//yangyang add end QA49
          //yangyang add start QA49
//			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_MiddleList_Search(longCutCode);
			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_MiddleList_Search( longCutCode, longPointCode);
          //yangyang add end QA49

			//沿いルート周辺
			if(poi_Route_Listitem == null)
			{
				poi_Route_Listitem = new ZPOI_Route_ListItem[(int) SearchCacheNum];
				for (int i = 0; i < SearchCacheNum; i++) {
					poi_Route_Listitem[i] = new ZPOI_Route_ListItem();
				}
			}
		}else {//施設周辺検索
			// データのclear
			NaviRun.GetNaviRunObj().JNI_NE_POI_Around_Clear();
			//yangyang mod start Bug774
//			NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SetDistance(10000);
			NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SetDistance(6000);
			//yangyang mod end Bug774

			//Modify start For Redmine 1660
			//NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(myPosi);
		    myPosi = CommonLib.getMapCenterInfo();
		    //Modify end For Redmine 1660

			NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SetPosition(myPosi.getM_lLong(),myPosi.getM_lLat());
			long longCutCode = Long.valueOf(CommonLib.getStrShortcutCode());
			//yangyang add start QA49
			long longPointCode = Long.valueOf(CommonLib.getStrShortPointCode());
			//yangyang add end QA49

			//yangyang mod start QA49
//			NaviRun.GetNaviRunObj().JNI_NE_POIArnd_MiddleList_Search(longCutCode);
			NaviRun.GetNaviRunObj().JNI_NE_POIArnd_MiddleList_Search(longCutCode,longPointCode);
			//yangyang mod end QA49
			if(poi_Around_Listitem == null)
			{
				poi_Around_Listitem = new POI_Around_ListItem[(int) SearchCacheNum];
				for (int i = 0; i < SearchCacheNum; i++) {
					poi_Around_Listitem[i] = new POI_Around_ListItem();
				}
			}
		}
		showList();
	}

	/**
	 * ダイアログ表示
	 */
	@Override
    protected Dialog onCreateDialog(int id) {
		final CustomDialog oDialog = new CustomDialog(this);
		Resources oRes = getResources();
		// 検索結果がない場合、ダイアログを表示
		if (id == DIALOG_NO_SEARCH_DATA) {
			oDialog.setTitle(oRes.getString(R.string.around_search_no_data_title));
	        oDialog.setMessage(oRes.getString(R.string.tel_dialog_msg));
	        oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {

				@Override
				public void onClick(View v) {
					finish();
				}

	        });
//Add 2012/04/26 Z01hirama Start --> #4417
	        oDialog.setOnCancelListener(new OnCancelListener() {

	        	@Override
				public void onCancel(DialogInterface dialog) {
					finish();
				}
			});
//Add 2012/04/26 Z01hirama End <-- #4417
	        return oDialog;
		}
		// XuYang add start 走行中の操作制限
        return super.onCreateDialog(id);
        // XuYang add end 走行中の操作制限
	}


	/**
	 * 画面表示の検索結果リストを取得
	 */
	private void showList() {
		if(mbAroundFlg){	//沿いルート周辺検索
			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetRecCount(RecCount);
			if(ardRouteList == null){
				ardRouteList = new ArrayList<ZPOI_Route_ListItem>();
			}else{
				if(!ardRouteList.isEmpty())
					ardRouteList.clear();
			}
		}else{	//施設周辺検索
			NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetRecCount(RecCount);
			if(ardList == null){
				ardList = new ArrayList<POI_Around_ListItem>();
			}else{
				if(!ardList.isEmpty())
					ardList.clear();
			}
		}
		showToast(this,RecCount);
		MAX_LIST_COUNT = initPageIndexAndCnt(RecCount);
//Del 2011/06/16 Z01thedoanh Start -->
//		if (MAX_LIST_COUNT < 5 && MAX_LIST_COUNT > 0) {
//		MAX_LIST_COUNT = 5;
//	}
//Del 2011/06/16 Z01thedoanh End <--
		if (RecCount.lcount > 0) {
			RecIndex = 0;
			RecCnt = 0;
			getSearchList(RecIndex);
		}else{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					showDialog(DIALOG_NO_SEARCH_DATA);
				}

			});

		}
	}

	/**
	 * 周辺の検索を取得
	 * @param lRecIndex
	 */
	private void getSearchList(long lRecIndex) {
		bListDealing = true;
		GetCount = new JNILong();
		if(mbAroundFlg){	//沿いルート周辺検索
			if(ardRouteList!=null && !ardRouteList.isEmpty()) {
				ardRouteList.clear();
			}

			long recCount = RecCount.lcount ;
            if (RecCount.lcount > SearchCacheNum) {
                recCount = SearchCacheNum;
            }

//Add 2011/05/26 Z01thedoanh Start -->
    		if ( poi_Route_Listitem == null )
    		{
    			poi_Route_Listitem = new ZPOI_Route_ListItem[(int)recCount];
    			for ( int i = 0; i < recCount; i++ )
    			{
    				poi_Route_Listitem[i] = new ZPOI_Route_ListItem();
    			}
    		}
//Add 2011/05/26 Z01thedoanh End <--

			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetRecList(lRecIndex,
			        recCount, poi_Route_Listitem, GetCount);
			for (int i = 0; i < GetCount.lcount; i++) {
				ardRouteList.add(poi_Route_Listitem[i]);
			}
			RecCnt=GetCount.lcount;
			RecIndex = lRecIndex;
			bListDealing = false;
		} else {//施設周辺検索
			if(ardList!=null && !ardList.isEmpty()){
				ardList.clear();
			}

			long recCount = RecCount.lcount ;
			if (RecCount.lcount > SearchCacheNum) {
				recCount = SearchCacheNum;
			}

//Add 2011/05/26 Z01thedoanh Start -->
    		if ( poi_Around_Listitem == null )
    		{
    			poi_Around_Listitem = new POI_Around_ListItem[(int)recCount];
    			for ( int i = 0; i < recCount; i++ )
    			{
    				poi_Around_Listitem[i] = new POI_Around_ListItem();
    			}
    		}
//Add 2011/05/26 Z01thedoanh End <--
			NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetRecList(lRecIndex,
					recCount, poi_Around_Listitem, GetCount);
			for (int i = 0; i < GetCount.lcount; i++) {
				ardList.add(poi_Around_Listitem[i]);
			}
			RecCnt=GetCount.lcount;
			RecIndex = lRecIndex;
			bListDealing = false;
		}
	}
//	public static AroundGenreInquiry getInstance() {
//		return instance;
//	}

	/**
	 * 沿いルート周辺検索の場合、ボタンをクリックする処理
	 * @param wPos
	 */
    private void itemRouteClick(int wPos) {
		do
		{
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 沿いルート周辺検索結果禁止 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 沿いルート周辺検索結果禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
//Add 2012/02/07 katsuta Start --> #2698
//			NaviLog.d(NaviLog.PRINT_LOG_TAG, "AroundGenreInquiry itemRouteClick ================== bIsListClicked: " + bIsListClicked);
   			if (bIsListClicked) {
   				return;
   			}
   			else {
   				bIsListClicked = true;
   			}
//Add 2012/02/07 katsuta End <--#2698
			if(bListDealing == true)
				break;
			if (RecIndex > wPos || wPos > (RecIndex + RecCnt)) {
				break;
			}
			//yangyang mod start Bug1115
//			int index = (int)(wPos-RecIndex);
			int index = (int)wPos;
			//yangyang mod end Bug1115
			if(index >= ardRouteList.size())
			{
				break;
			}
			//yangyang del start 20110602
//		if (iNextTreeIndex >= 2) {
			//yangyang del end 20110602
//			listClickAble = false;
				msPointName = ardRouteList.get(index).getPucName();
			long Longitude = 0;
			long Latitude = 0;
			JNIInt MapScale = new JNIInt();
			NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
			Latitude = ardRouteList.get(index).getLatitude();
			Longitude = ardRouteList.get(index).getLongitude();
			POIData oData = new POIData();
			oData.m_sAddress = msPointName;
            oData.m_sName = msPointName;
            oData.m_wLong = Longitude;
            oData.m_wLat = Latitude;
            //yangyang mod start Bug813
//            Intent oIntent = getIntent();
//            oIntent.setClass(this, RouteEdit.class);
//            oIntent.putExtra(AppInfo.POI_DATA, oData);
//            oIntent.putExtra(Constants.PARAMS_ROUTE_MOD_FLAG, true);
//            oIntent.putExtra(Constants.ROUTE_TYPE_KEY,Constants.ROUTE_TYPE_NOW);
//            this.startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
            CommonLib.routeSearchFlag = true;
            CommonLib.setChangePosFromSearch(true);
            CommonLib.setChangePos(false);
            OpenMap.moveMapTo(AroundGenreInquiry.this, oData,
            		getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
          //yangyang mod end Bug813
            this.createDialog(Constants.DIALOG_WAIT, -1);

          //yangyang del start 20110602
//		} else {
//			JNILong JNIRecCnt = new JNILong();
//			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetNextTreeIndex(index, JNIRecCnt);
//			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchNextList(index);
//			iNextTreeIndex = (int) JNIRecCnt.lcount;
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"222222");
//		}
		 //yangyang del end 20110602
		}while(false);
	}
    /**
     * 周辺検索の場合、ボタンをクリックする処理
     * @param wPos
     */
	private void itemClick(int wPos) {
		do
		{
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 周辺検索結果禁止 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 周辺検索結果禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
//Add 2012/02/07 katsuta Start --> #2698
//			NaviLog.d(NaviLog.PRINT_LOG_TAG, "AroundGenreInquiry itemClick ================== bIsListClicked: " + bIsListClicked);
   			if (bIsListClicked) {
   				return;
   			}
   			else {
   				bIsListClicked = true;
   			}
//Add 2012/02/07 katsuta End <--#2698

			if(bListDealing == true){
				break;
			}
			//yangyang mod start Bug1115
			if (RecIndex>wPos && wPos>(RecIndex+RecCnt)){
				//yangyang mod end Bug1115
				break;
			}
			//yangyang mod start Bug1115
//			int index = (int)(wPos-RecIndex);
			int index = (int)wPos;
			//yangyang mod end Bug1115
			//yangyang del start 20110414
//			if (iNextTreeIndex >= 2) {
			//yangyang del end 20110414
//				listClickAble = false;
					msPointName = ardList.get(index).getPucName();
				long Longitude = 0;
				long Latitude = 0;
				JNIInt MapScale = new JNIInt();
				NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
				Latitude = ardList.get(index).getLatitude();
				Longitude = ardList.get(index).getLongitude();

				// 地図を遷移する
				POIData oData = new POIData();
	            oData.m_sName = msPointName;
	            oData.m_sAddress  = msPointName;
	            oData.m_wLong = Longitude;
	            oData.m_wLat = Latitude;
	         // XuYang add start #1056
                Intent oIntent = getIntent();
                if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
                    CommonLib.setChangePosFromSearch(true);
                    CommonLib.setChangePos(false);
                } else {
                    CommonLib.setChangePosFromSearch(false);
                    CommonLib.setChangePos(false);
                }
                // XuYang add end #1056
	            OpenMap.moveMapTo(AroundGenreInquiry.this, oData,
	            		getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
	            this.createDialog(Constants.DIALOG_WAIT, -1);
	            //yangyang del start 20110414
//			} else {
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"iNextTreeIndex >= 2 else");
//				JNILong JNIRecCnt = new JNILong();
//				Log.v("xy","615__index" + index);
//				NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetNextTreeIndex(index, JNIRecCnt);
//				NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SearchNextList(index);
//				iNextTreeIndex = (int) JNIRecCnt.lcount;
//			}
	            //yangyang del end 20110414
		}while(false);
	}



	/**
	 * 表示データを取得
	 */
	public View AroundGetView(int pos, View view) {
		LinearLayout oLayout;
		if(view == null)
		{
			LayoutInflater inflater;
			inflater = LayoutInflater.from(this);
			// ボタンを追加
			oLayout = (LinearLayout) inflater.inflate(
					R.layout.inquiry_distance_button_freescroll, null);
			view = oLayout;
		} else {
			oLayout = (LinearLayout)view;
		}
		final int iPos = pos;
		LinearLayout oButton = (LinearLayout)oLayout.findViewById(R.id.LinearLayout_distance);
		TextView txtCon = (TextView)oLayout.findViewById(R.id.TextView_value);
		TextView txtDis = (TextView)oLayout.findViewById(R.id.TextView_distance);
		if(mbAroundFlg){	//沿いルート周辺検索
            if (pos >= ardRouteList.size()) {
                if (pos % SearchCacheNum == 0) {
                    reGetListRouteData(pos);
                }
            }

            //yangyang mod start
			if (ardRouteList.size() > pos && !"".equals(ardRouteList.get(pos).getPucName())) {
				//yangyang mod end
				oButton.setEnabled(true);
				txtCon.setText(ardRouteList.get(pos).getPucName());
				long dis = ardRouteList.get(pos).getDistance();
				String s = String.valueOf(dis);
				txtDis.setText("(" + s + "m" + ")");
				if (dis >= 1000) {
					double distance = (double) dis / (double) 1000;
					int t = (int) (distance * 10);
					distance = (double) t / (double) 10;
					s = String.valueOf(distance);
					txtDis.setText("(" + s + "km" + ")");
				}
				oButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						NaviLog.d(NaviLog.PRINT_LOG_TAG,"ssssssssssssssssss-------onClick");
						itemRouteClick(iPos);
					}
				});
			} else {
				oButton.setEnabled(false);
				txtDis.setText("");
				txtCon.setText("");
		    }

		} else {
			if (pos >= ardList.size()) {
			    if (pos % SearchCacheNum == 0) {
//			    	NaviNaviLog.d("AroundGenreInquiry","AroundGetView reGetListAroundData call");
	                reGetListAroundData(pos);
	            }
			}
//	    	NaviNaviLog.d("AroundGenreInquiry","AroundGetView pos : "+pos);

			if (!"".equals(ardList.get(pos).getPucName())) {
                oButton.setEnabled(true);
                txtCon.setText(ardList.get(pos).getPucName());
                long dis = ardList.get(pos).getDistance();
                String s = String.valueOf(dis);
                txtDis.setText("(" + s + "m" + ")");
                if (dis >= 1000) {
                    double distance = (double) dis / (double) 1000;
                    int t = (int) (distance * 10);
                    distance = (double) t / (double) 10;
                    s = String.valueOf(distance);
                    txtDis.setText("(" + s + "km" + ")");
                }
                oButton.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        itemClick(iPos);
                    }
                });
            } else {
                oButton.setEnabled(false);
                txtDis.setText("");
                txtCon.setText("");
            }
		}
		return view;
	}

	public void reGetListRouteData(long lRecIndex) {
	    JNILong getCnt = new JNILong();
	    poi_Route_Listitem = new ZPOI_Route_ListItem[(int) SearchCacheNum];
        for (int i = 0; i < SearchCacheNum; i++) {
            poi_Route_Listitem[i] = new ZPOI_Route_ListItem();
        }
	    NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetRecList(lRecIndex,
	            SearchCacheNum, poi_Route_Listitem, getCnt);
        for (int i = 0; i < getCnt.lcount; i++) {
            ardRouteList.add(poi_Route_Listitem[i]);
        }
        //yangyang add start Bug1115
        RecCnt=GetCount.lcount;
		RecIndex = lRecIndex;
        //yangyang add end Bug1115
	}
	public void reGetListAroundData(long lRecIndex) {
	    JNILong getAroundCnt = new JNILong();
	    poi_Around_Listitem = new POI_Around_ListItem[(int) SearchCacheNum];
        for (int i = 0; i < SearchCacheNum; i++) {
            poi_Around_Listitem[i] = new POI_Around_ListItem();
        }



	    NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetRecList(lRecIndex,
	            SearchCacheNum, poi_Around_Listitem, getAroundCnt);
        for (int i = 0; i < getAroundCnt.lcount; i++) {
            ardList.add(poi_Around_Listitem[i]);
        }
        //yangyang add start Bug1115
        RecCnt=GetCount.lcount;
		RecIndex = lRecIndex;
        //yangyang add end Bug1115
	}
	private void initDialog() {
//Del 2011/09/17 Z01_h_yamada Start -->
//		this.setMenuTitle(R.drawable.menu_genre);
//Del 2011/09/17 Z01_h_yamada End <--
		LayoutInflater oInflater = LayoutInflater.from(this);
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base_list_freescroll, null);
		Button obtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
		obtn.setVisibility(Button.GONE);

	    oText = (TextView)oLayout.findViewById(R.id.inquiry_title);
	    if (CommonLib.getShortcutName() != null && !"".equals(CommonLib.getShortcutName())) {
	    	oText.setText(CommonLib.getShortcutName());
	    } else {
	        oText.setText(R.string.genre_title);
	    }

	    scroll = (ScrollList)oLayout.findViewById(R.id.scrollList);

		scroll.setAdapter(new BaseAdapter(){

            @Override
            public int getCount() {
                return MAX_LIST_COUNT;
//                if(mbAroundFlg){    //沿いルート周辺検索
//                    return ardRouteList.size();
//                } else {
//                    return ardList.size();
//                }
            }

			@Override
			public boolean areAllItemsEnabled() {
				return false;
			}

			@Override
			public boolean isEnabled(int position) {
				return false;
			}
            @Override
            public Object getItem(int arg0) {
                return null;
            }

            @Override
            public long getItemId(int arg0) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
            	NaviLog.d(NaviLog.PRINT_LOG_TAG,"getView position===" + position);
                return AroundGetView(position, convertView);
            }

		});
//Del 2011/10/06 Z01_h_yamada Start -->
//		scroll.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
		scroll.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
		scroll.setPadding(0, 5, 0, 5);
		oTool = new ScrollTool(this);
		oTool.bindView(scroll);
		setViewInOperArea(oTool);
		setViewInWorkArea(oLayout);

	}
	@Override
	protected boolean onStartShowPage() throws Exception {
		SearchCacheNum = ONCE_GET_COUNT;
		// 沿いルート周辺と施設周辺のフラグを取得
		mbAroundFlg = CommonLib.isAroundFlg();

//		instance = this;
		CommonLib.isAround = true;
		// データを初期化
	    initData();
		return true;
	}

	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {
		if(bGetData)
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					initDialog();
				}




			});
		}

	}


	//yangyang add start 20110423
	@Override
	protected boolean onClickGoBack() {
		if(mbAroundFlg) {
			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_Clear();
		} else {
			NaviRun.GetNaviRunObj().JNI_NE_POI_Around_Clear();
		}

		return super.onClickGoBack();
	}

	@Override
	protected boolean onClickGoMap() {
		if(mbAroundFlg) {
			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_Clear();
		} else {
			NaviRun.GetNaviRunObj().JNI_NE_POI_Around_Clear();
		}
		return super.onClickGoMap();
	}
	//yangyang add end 20110423
	@Override
	protected void search(){

	}

}
