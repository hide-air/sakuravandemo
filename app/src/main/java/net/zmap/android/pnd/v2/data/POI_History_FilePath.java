/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           POI_History_FilePath.java
 * Description     HISTORY 履歴
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class POI_History_FilePath {
    public short m_stRet;

    /**
    * Created on 2010/08/06
    * Title:       getStRet
    * Description:  外部ファイルパスを取得する
    * @param1  無し
    * @return       short

    * @version        1.0
    */
    public short getStRet() {
        return m_stRet;
    }
}
