package net.zmap.android.pnd.v2.data;

public class ZPOI_Route_ListItem {
	private long	NameSize;
	private String	pucName;
	private long	Distance;
	private long	Latitude;
	private long	Longitude;
	private long	ExistenceFlg;
	private int		eside;
	private long     kindeCode;

	public long getKindeCode() {
        return kindeCode;
    }
    public void setKindeCode(long kindeCode) {
        this.kindeCode = kindeCode;
    }
    public long getNameSize() {
		return NameSize;
	}
	public void setNameSize(long nameSize) {
		NameSize = nameSize;
	}
	public String getPucName() {
		return pucName;
	}
	public void setPucName(String pucName) {
		this.pucName = pucName;
	}
	public long getDistance() {
		return Distance;
	}
	public void setDistance(long distance) {
		Distance = distance;
	}
	public long getLatitude() {
		return Latitude;
	}
	public void setLatitude(long latitude) {
		Latitude = latitude;
	}
	public long getLongitude() {
		return Longitude;
	}
	public void setLongitude(long longitude) {
		Longitude = longitude;
	}
	public long getExistenceFlg() {
		return ExistenceFlg;
	}
	public void setExistenceFlg(long existenceFlg) {
		ExistenceFlg = existenceFlg;
	}
	public int getEside() {
		return eside;
	}
	public void setEside(int eside) {
		this.eside = eside;
	}
}
