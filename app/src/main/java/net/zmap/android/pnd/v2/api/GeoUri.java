package net.zmap.android.pnd.v2.api;

import android.net.Uri;
import android.util.Log;

import java.text.MessageFormat;
import java.text.ParseException;

public class GeoUri
{
	public static final String			TAG					= "GeoUri";

	public static final String			SIMPLE_URI_PATTERN	= "geo:{0},{1}";
	public static final String			URI_PATTERN			= "geo:{0},{1}?z={2}";
	private static final MessageFormat	simpleUriFormatter;
	private static final MessageFormat	uriFormatter;

	static {
		simpleUriFormatter = new MessageFormat( SIMPLE_URI_PATTERN );
		uriFormatter = new MessageFormat( URI_PATTERN );
	}

	private Double		latitude;
	private Double		longitude;
	private Integer		zoom;

	public GeoUri()
	{
	}

	public GeoUri( String uri )
	{
		Object[] objects = null;
		try
		{
			objects = uriFormatter.parse( uri );
		}
		catch ( ParseException e )
		{
			try
			{
				objects = simpleUriFormatter.parse( uri );
			}
			catch ( ParseException e1 )
			{
				Log.e( TAG, "Unknown uri pattern.", e1 );
			}
		}

		if ( objects != null && objects.length >= 2 )
		{
			try
			{
				latitude = Double.valueOf( (String)objects[0] );
				longitude = Double.valueOf( (String)objects[1] );
				if ( objects.length == 3 )
				{
					zoom = Integer.valueOf( (String)objects[2] );
				}
			}
			catch ( NumberFormatException e )
			{
				latitude = null;
				longitude = null;
				zoom = null;
				Log.e( TAG, "Invalid value.", e );
			}
		}
	}

	public double getLatitude()
	{
		return latitude;
	}

	public void setLatitude( double latitude )
	{
		this.latitude = latitude;
	}

	public double getLongitude()
	{
		return longitude;
	}

	public void setLongitude( double longitude )
	{
		this.longitude = longitude;
	}

	public boolean hasLocation()
	{
		return latitude != null && longitude != null;
	}

	public Integer getZoom()
	{
		return zoom;
	}

	public void setZoom( Integer zoom )
	{
		this.zoom = zoom;
	}

	public boolean hasZoom()
	{
		return zoom != null;
	}

	public Uri getUri()
	{
		if ( hasLocation() )
		{
			String uri = null;
			if ( hasZoom() )
			{
				uri = uriFormatter.format( new Object[]{ latitude, longitude, zoom } );
			}
			else
			{
				uri = simpleUriFormatter.format( new Object[]{ latitude, longitude } );
			}

			return Uri.parse( uri );
		}

		return null;
	}
}
