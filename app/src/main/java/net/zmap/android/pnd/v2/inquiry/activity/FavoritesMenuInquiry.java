//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.os.Bundle;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_UserFile_RecordIF;

/**
 * K-F1_お気に入り一覧
 * */
public class FavoritesMenuInquiry extends FavoritesGenreMenuInquiry {



	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
//Del 2011/09/17 Z01_h_yamada Start -->
//		//タイトルを設定する
//		this.setMenuTitle(R.drawable.menu_favorites_mod);
//Del 2011/09/17 Z01_h_yamada End <--
//		Intent oIntent = getIntent();
		recordCount = oMyIntent.getIntExtra(Constants.PARAMS_RECORD_COUNT, 0);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onCreate ====recordCount===" + recordCount);
	}
//	private OnClickListener reSetGroupListClick = new OnClickListener(){
//
//		@Override
//		public void onClick(View v) {
//			Intent oIntent = getIntent();
//			int kindKey = oIntent.getIntExtra(Constants.PARAMS_SEARCH_KEY, -1);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"kindKey============" + kindKey);
//
//		}};
	@Override
	protected POI_UserFile_RecordIF[] reShowData(long start, int count,
			long genreIndex, POI_UserFile_RecordIF[] mPoiUserFileListitem) {

		for (int i = 0; i < getCountInBox(); i++) {
			mPoiUserFileListitem[i] = new POI_UserFile_RecordIF();
		}
		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"FavoritesMenuInquiry reShowData===genreIndex==" +
		// genreIndex);
		NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecList(genreIndex, start,
				count, mPoiUserFileListitem, Rec);
		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"start========Rec==="+ start + "==========="
		// +Rec.lcount);
//		super.recordCount = (int)Rec.lcount;
		//自宅の場合
		if (genreIndex == super.KIND_OF_HOUSE) {
			POI_UserFile_RecordIF[] xyTest = null;
			xyTest = new POI_UserFile_RecordIF[1];
			for (int i = 0; i < 1; i++) {
				xyTest[i] = new POI_UserFile_RecordIF();
			}
//			int flagIndex = 1;
			// hangeng del start Bug1696
			// if (Rec.lcount == 0) {
			// hangeng del end Bug1696
//				flagIndex = 0;
			mPoiUserFileListitem[0].m_DispName = this.getResources().getString(
					R.string.dialog_myhome_save_button1);
			// hangeng add start Bug1696
			if (Rec.lcount == 0) {
				// hangeng add end Bug1696
				mPoiUserFileListitem[0].m_lLat = 0;
				mPoiUserFileListitem[0].m_lLon = 0;
//				super.recordCount = 0;
//			} else {
//				super.recordCount = 1;
			}
			real_house_count = (int)Rec.lcount;
			//自宅２の場合
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecList(8, start, count,
					xyTest, Rec);
			if (Rec.lcount != 0 ) {
				mPoiUserFileListitem[1] = xyTest[0];
				// hangeng add start Bug1696
				mPoiUserFileListitem[1].m_DispName = this.getResources()
						.getString(R.string.dialog_myhome_save_button2);
				// hangeng add end Bug1696
//				super.recordCount += 1;
//				oIntent.putExtra(Constants.PARAMS_RECORD_COUNT, recordCount);
			} else {
				mPoiUserFileListitem[1].m_DispName = this.getResources()
						.getString(R.string.dialog_myhome_save_button2);
				mPoiUserFileListitem[1].m_lLat = 0;
				mPoiUserFileListitem[1].m_lLon = 0;
			}
			real_house_count += (int)Rec.lcount;


		}

		return mPoiUserFileListitem;
	}


}
