//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_UserFile_DataIF;
import net.zmap.android.pnd.v2.data.POI_UserFile_RecordIF;
import net.zmap.android.pnd.v2.inquiry.data.CommonMethd;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.inquiry.view.InputText;
import net.zmap.android.pnd.v2.maps.OpenMap;

import java.io.Serializable;

/**
 * M-F1_お気に入り登録名称編集
 *
 *
 * */
public class FavoritesModInquiry  extends InquiryBaseLoading {

	/** 入力テキストとソフトキーボードの対象 */
	private InputText oView = null;
	/** 登録ボタン対象 */
	private Button oLoadBtn = null;
	/**自宅のIndex*/
	public int KIND_OF_HOUSE = 7;
	private int MIN_LIMITED = 1;
	private int MAX_LIMITED = 40;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		oView = new InputText(this,0,oLoadBtn);
//Del 2011/09/17 Z01_h_yamada Start -->
//		this.setMenuTitle(R.drawable.menu_favorites_load);
//Del 2011/09/17 Z01_h_yamada End <--
//		TwoBtnAndScrollView oBtnView = new TwoBtnAndScrollView(this, oView.getTextObj());
		initTwoBtn(oView);
		//件数取得のタイプ
		oView.setType(oView.FAVORITES);
		oView.setMinLimited(MIN_LIMITED,oLoadBtn);
		oView.setMaxLimited(MAX_LIMITED,oLoadBtn);
		Intent oIntent = getIntent();
//Del 2011/09/17 Z01_h_yamada Start -->
//		//タイトルを設定する
//		int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE,0);
//		if (0!= titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--

		int key = oIntent.getIntExtra(Constants.FROM_FLAG_KEY, -1);
		if(key ==  AppInfo.ID_ACTIVITY_FAVORITESADDGENREINQUIRY) {
			//上記のActivityから来るときは必ず地図スクロール
			String sValue = oIntent.getStringExtra(Constants.FLAG_TITLE);
			oView.getTextObj().setText(sValue);
		} else {
            Serializable object = oIntent.getSerializableExtra(AppInfo.POI_DATA);
            if (  object != null ) {

                //検索類から遷移する場合、名称を表示する
                POIData poiData = (POIData) object;
                if (AppInfo.isEmpty(poiData.m_sAddress)) {
                    /** 位置（アドレス） */
                    JNIString AddressString = new JNIString();
                    NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(poiData.m_wLong, poiData.m_wLat, AddressString);
                    oView.getTextObj().setText(AddressString.getM_StrAddressString());
                    poiData.m_sAddress = AddressString.getM_StrAddressString();
                    poiData.m_sName = AddressString.getM_StrAddressString();
                    oIntent.putExtra(AppInfo.POI_DATA, poiData);
                } else {
                    oView.getTextObj().setText(poiData.m_sAddress);
                }
            } else {
                //遷移する前に、地図をスクロールした場合、地図のアドレスを表示する
                String sValue = oIntent.getStringExtra(Constants.FLAG_TITLE);
                oView.getTextObj().setText(sValue);
            }
        }
//Chg 2011/07/05 Z01thedoanh Start -->
		//InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
//		final InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
//Chg 2011/07/05 Z01thedoanh End <--
//		imm.showSoftInput(oView.getTextObj(), 0);

		LinearLayout ocountView = (LinearLayout)oView.findViewById(this, R.id.LinearLayout_listcount);
		ocountView.setVisibility(TextView.GONE);
		//soft keyboard close
//		AppInfo.hiddenSoftInputMethod(this);

		oView.getTextObj().setFilters(new InputFilter[]{new InputFilter.LengthFilter(40)});
//		setViewInOperArea(oBtnView.getView());
		setViewInWorkArea(oView.getView());

//Chg 2011/09/17 Z01_h_yamada Start -->
//		if (titleId == R.drawable.menu_favorites_mod) {
//			oLoadBtn.setEnabled(false);
//		}
//--------------------------------------------
		boolean isFavoritesEdit = oIntent.getBooleanExtra(Constants.PARAMS_FAVORITES_EDIT, false);
		if (isFavoritesEdit) {
			oLoadBtn.setEnabled(false);
		}
//Chg 2011/09/17 Z01_h_yamada End <--
//Add 2011/07/05 Z01thedoanh Start -->bug #1146: OSキーボード使用時に「完了」ボタンが使えるようにする
		 final InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
		 imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
		 if(oView.getTextObj() != null){
			 oView.getTextObj().setOnEditorActionListener(new OnEditorActionListener() {
				 public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					 if (actionId == EditorInfo.IME_ACTION_DONE){
						 imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
					 }
					 return true;
				 }
			 });
		 }

//Add 2011/07/05 Z01thedoanh End <--
/*
		oView.getTextObj().setOnFocusChangeListener(new View.OnFocusChangeListener(){

    		@Override
    		public void onFocusChange(View v, boolean hasFocus){
				NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFocusChange1!! = " + hasFocus);
				if (v != null) {
//					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFocusChange2!!");
	    			if(hasFocus == false){
	    				NaviLog.d(NaviLog.PRINT_LOG_TAG,"hide!!");
	//    				imm.hideSoftInputFromWindow(v.getWindowToken(),0);
	    			} else {
	    				NaviLog.d(NaviLog.PRINT_LOG_TAG,"show!!");
//	    				imm.showSoftInput(v, InputMethodManager.SHOW_FORCED);
	    			}
				}
    		}
    	});
*/

		oView.getTextObj().requestFocus();

	}

	@Override
	protected void onStart() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStart");
//Del 2011/07/05 Z01thedoanh Start -->
		//InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//Del 2011/07/05 Z01thedoanh End <--
//		View view = this.getCurrentFocus();
//	    if (view != null){
//Del 2011/07/05 Z01thedoanh Start -->
			//imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//Del 2011/07/05 Z01thedoanh End <--
//	    }
//	    imm.getInputMethodList()

//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStart");
		super.onStart();
	}


	@Override
	protected void onPause() {
		AppInfo.hiddenSoftInputMethod(this);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onPause");
//		if(this.getWindow().getCurrentFocus()!=null){
//			InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
//			imm.hideSoftInputFromWindow(this.getWindow().getCurrentFocus().getWindowToken(),0);
//		}

//		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		super.onPause();
	}

	@Override
	protected void onStop()
	{
		AppInfo.hiddenSoftInputMethod(this);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStop");
		super.onStop();
	}
	/**
	 * 修正ボタンと検索ボタンの初期化
	 * @param oBtnView 修正ボタンと検索ボタンのレイアウト
	 *
	 * */
	private void initTwoBtn(InputText oBtnView) {
//		Button btnEdit = (Button) oBtnView.findViewById(this, R.id.Button_edit);
//		btnEdit.setOnClickListener(onEditClickListener);
		oLoadBtn = (Button) oBtnView.findViewById(this, R.id.Button_search);
		Intent oIntent = getIntent();
		if (oIntent.hasExtra(Constants.PARAMS_BUTTON_NAME)) {
			String buttonName = oIntent.getStringExtra(Constants.PARAMS_BUTTON_NAME);
			oLoadBtn.setText(buttonName);
		} else {
			oLoadBtn.setText(R.string.btn_load);//.setBackgroundResource(R.drawable.)
		}
		oLoadBtn.setOnClickListener(onLoadClickListener);


	}
//	/**
//	 * 修正ボタンのクリック処理
//	 *
//	 * */
//	OnClickListener onEditClickListener = new OnClickListener(){
//
//		@Override
//		public void onClick(View v) {
////			NaviLog.d(NaviLog.PRINT_LOG_TAG,"onEditClick");
//			if (!AppInfo.isEmpty(oView.getText())) {
//				oView.setFlag_AddAndDel(true);
//
//				StringBuffer sb = new StringBuffer(oView.getText());
//				int selectionEnd = oView.getTextObj().getSelectionEnd();
//				if (selectionEnd == 0) {
//					sb.deleteCharAt(selectionEnd);
//				} else {
//					sb.deleteCharAt(selectionEnd-1);
//				}
//				oView.getTextObj().setText(sb.toString());
//				if (selectionEnd == 0) {
//					oView.getTextObj().setSelection(selectionEnd);
//				} else {
//					oView.getTextObj().setSelection(selectionEnd - 1);
//				}
//				if (AppInfo.isEmpty(oView.getText())) {
//					oView.getTextObj().setHint("");
//				}
//			}
//
//		}
//
//	};
	/**
	 * 登録ボタンの押下処理
	 *
	 * */
	private OnClickListener onLoadClickListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 お気に入り登録 登録禁止 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 お気に入り登録 登録禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
            AppInfo.hiddenSoftInputMethod(FavoritesModInquiry.this);
            showDialog(Constants.DIALOG_LOAD_FAVORITES);
        }

	};

	@Override
	protected Dialog onCreateDialog(int id) {
		/**
		 * M-F6_お気に入り登録確認
		 * */
		if (id == Constants.DIALOG_LOAD_FAVORITES) {

			Intent oIntent = getIntent();
			final int genreIndex = oIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX, 0);
			final int recordIndex = oIntent.getIntExtra(Constants.PARAMS_LIST_INDEX, 0);
			final String genreName = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
			final String oValue = oView.getText();

			final int oValueLen = CommonMethd.reSetlen(oValue);//;
//			final int oValueLen = oValue.getBytes().length;//;
//			final int oValueLen = oValue.getBytes().length;//;
//			Byte b = Byte.parseByte(oView.getText());


//			final int oValueLen = charList.length;//.vaoValue..toString().length();


			CustomDialog oDialog = new CustomDialog(this);
			oDialog.setTitle(R.string.load_favorites_title);
			String msg = CommonMethd.reSetMsg(oValue,
					genreName,
					this.getResources().getString(R.string.load_favorites_msg));
			oDialog.setMessage(msg);
			oDialog.addButton(this.getResources().getString(R.string.btn_ok),new OnClickListener(){

				@Override
				public void onClick(View v) {

					//お気に入り登録処理
					AddFavorites();
				}
				/**
				 * お気に入り登録処理
				 * */
				private void AddFavorites() {

					createDialog(Constants.DIALOG_WAIT, -1);
					Intent oIntent = getIntent();
					int key = oIntent.getIntExtra(Constants.FROM_FLAG_KEY, -1);
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"key=====" + key);
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"genreIndex=====" + genreIndex);

    					if (-1 != key && key == AppInfo.ID_ACTIVITY_FAVORITESADDGENREINQUIRY) {
    						//お気に入り登録処理
    						AddFavoritesGenre(oIntent);
    					} else {
    						//お気に入り編集処理
    //						NaviLog.d(NaviLog.PRINT_LOG_TAG,"157  genreIndex===" + genreIndex);
    //						NaviLog.d(NaviLog.PRINT_LOG_TAG,"157  recordIndex===" + recordIndex);
//Chg 2011/12/15 Z01_h_yamada Start -->
//    						//自宅２の場合
//    						if (genreIndex == KIND_OF_HOUSE && recordIndex == 1) {
//    							NaviRun.GetNaviRunObj().JNI_NE_POI_Mybox_ChangeData(8, 0, oValue, oValueLen);
//
//    						} else {
//    							//それ以外の場合
//    							NaviRun.GetNaviRunObj().JNI_NE_POI_Mybox_ChangeData(genreIndex, recordIndex, oValue, oValueLen);
//    						}
//--------------------------------------------
    						long lRet;
    						//自宅２の場合
    						if (genreIndex == KIND_OF_HOUSE && recordIndex == 1) {
    							lRet = NaviRun.GetNaviRunObj().JNI_NE_POI_Mybox_ChangeData(8, 0, oValue, oValueLen);

    						} else {
    							//それ以外の場合
    							lRet = NaviRun.GetNaviRunObj().JNI_NE_POI_Mybox_ChangeData(genreIndex, recordIndex, oValue, oValueLen);
    						}
    						NaviLog.w(NaviLog.PRINT_LOG_TAG, " JNI_NE_POI_Mybox_ChangeData lRet=" + lRet);
    						if ( lRet != NaviRun.NaviEngine.NE_SUCCESS ) {
    							removeDialog(Constants.DIALOG_LOAD_FAVORITES);
    							closeWarningDialog();
    							showDialog(Constants.DIALOG_FILE_WRITE_FAILED);
    							return ;
    						}
//Chg 2011/12/15 Z01_h_yamada End <--
    						removeDialog(Constants.DIALOG_LOAD_FAVORITES);
    						setResult(Activity.RESULT_OK,oIntent);
    						finish();
    					}
				}

				/**
				 * お気に入り登録処理
				 *
				 * */
				private void AddFavoritesGenre(Intent oIntent) {
					JNITwoLong Coordinate = new JNITwoLong();
					int GenreIndex = oIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX, 0);
//					String address = oIntent.getStringExtra(Constants.FLAG_TITLE);
					NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(Coordinate);
					POI_UserFile_RecordIF UserFile_RecordIF = new POI_UserFile_RecordIF();
//					UserFile_RecordIF.m_DispName = new String(address);
					UserFile_RecordIF.m_DispName = oView.getText().toString();
					UserFile_RecordIF.m_lDate = 0;
					UserFile_RecordIF.m_lLon = Coordinate.getM_lLong();
					UserFile_RecordIF.m_lLat = Coordinate.getM_lLat();
					UserFile_RecordIF.m_lUserFlag = 0;
					UserFile_RecordIF.m_lImport = 0;
					UserFile_RecordIF.m_lIconCode = 1;
					POI_UserFile_DataIF Data = new POI_UserFile_DataIF();
					Data.m_sDataIndex = 0;
//					Data.m_sDataSize = (short) address.length();
//					Data.m_sDataSize = (short) oView.getText().toString().length();
//					String name = oView.getText().toString();
//					Data.m_sDataSize = (short) CommonMethd.reSetlen(name);
					Data.m_sDataSize = (short) oView.getText().getBytes().length;
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"Data.m_sDataSize ====" + Data.m_sDataSize );
//					Data.m_Data = new String(address);
					Data.m_Data = oView.getText().toString();
					String myhomeFlag = oIntent.getStringExtra(Constants.PARAMS_MYHOME_FLAG);
//Chg 2011/12/16 Z01_h_yamada Start -->
//					if (GenreIndex == KIND_OF_HOUSE && !AppInfo.isEmpty(myhomeFlag) && myhomeFlag.equals(Constants.MYHOME_02)) {
//						NaviRun.GetNaviRunObj().JNI_NE_POIMybox_SetRec(8, UserFile_RecordIF, 1, Data);
//					} else {
//						NaviRun.GetNaviRunObj().JNI_NE_POIMybox_SetRec(GenreIndex, UserFile_RecordIF, 1, Data);
//					}
//--------------------------------------------
					long lRet = 0;
					if (GenreIndex == KIND_OF_HOUSE && !AppInfo.isEmpty(myhomeFlag) && myhomeFlag.equals(Constants.MYHOME_02)) {

						lRet = NaviRun.GetNaviRunObj().JNI_NE_POIMybox_SetRec(8, UserFile_RecordIF, 1, Data);
					} else {
						lRet = NaviRun.GetNaviRunObj().JNI_NE_POIMybox_SetRec(GenreIndex, UserFile_RecordIF, 1, Data);
					}
					if ( lRet != NaviRun.NaviEngine.NE_SUCCESS ) {
						removeDialog(Constants.DIALOG_LOAD_FAVORITES);
						closeWarningDialog();
						showDialog(Constants.DIALOG_FILE_WRITE_FAILED);
						return ;
					}
//Chg 2011/12/16 Z01_h_yamada End <--


//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"AddFavoritesGenre");
//					setResult(Activity.RESULT_OK,oIntent);
//					finish();
					onAddSucc(UserFile_RecordIF);

				}
				/**
				 * 地図へ遷移する
				 * */
				private void onAddSucc(POI_UserFile_RecordIF poi){

					removeDialog(Constants.DIALOG_LOAD_FAVORITES);
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"remove");
                    POIData oData = new POIData();
                    oData.m_sName = poi.getM_DispName();
                    oData.m_sAddress = oData.m_sName;
                    oData.m_wLong = poi.getM_lLon();
                    oData.m_wLat = poi.getM_lLat();

                    Intent intent = getIntent();
                    if(intent == null){
                        intent = new Intent();
                    }
                    intent.putExtra(Constants.FLAG_FAVORITE_TO_MAP, true);
                    NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
                    OpenMap.moveMapTo(FavoritesModInquiry.this, oData, intent, AppInfo.POI_LOCAL, Constants.FAVORITE_ADD_REQUEST);

                }
				});
			oDialog.addButton(R.string.btn_cancel, new OnClickListener(){

				@Override
				public void onClick(View v) {
					removeDialog(Constants.DIALOG_LOAD_FAVORITES);

				}});
			return oDialog;
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected boolean onStartShowPage() throws Exception {
		return false;
	}

	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {
	}
}
