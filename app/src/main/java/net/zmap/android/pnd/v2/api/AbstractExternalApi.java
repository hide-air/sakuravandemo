package net.zmap.android.pnd.v2.api;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.util.List;

/**
 * API 抽象クラス
 */
public abstract class AbstractExternalApi {

    private final static String    TAG                        = "AbstractExternalApi";

    /** コンテキスト */
    protected Context              mContext                   = null;
    /** サービスのクラス名 (ex. <code>foo.bar.Baz</code>) */
    protected String               mServiceClassName          = null;

    /** コネクションリスナ */
    protected ApiServiceConnection mServiceConnectionListener = null;

    protected Handler              mHandler                   = new Handler();

    /**
     * コンストラクタ
     *
     * @param serviceClassName
     *            サービスのクラス名 (ex. <code>foo.bar.Baz</code>)
     */
    public AbstractExternalApi(String serviceClassName) {
        mServiceClassName = serviceClassName;
    }

    /**
     * サービスの起動状態を確認します。
     *
     * @param context
     *            コンテキスト
     *
     * @return true:起動済み false:起動していません。
     */
    protected boolean isServiceRunning(Context context) {
        if (mServiceClassName == null) {
            throw new IllegalStateException();
        }
        ActivityManager activityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
        for (RunningServiceInfo info : services) {
            if (mServiceClassName.equals(info.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * サービスへ接続します。
     *
     * @param context
     *            コンテキスト
     *
     * @throws IllegalStateException
     *             サービスが利用できない場合にスローします。
     */
    public void connect(Context context) {
        if (context == null) {
            throw new NullPointerException();
        }
        if (mConnection == null) {
            throw new IllegalStateException();
        }

        mContext = context;

        if (getBindServiceFlags() == 0) {
            // bindService(..., 0) でも自動起動するので、サービス必須か見る
            if (!isServiceRunning(context)) {
                throw new IllegalStateException();
            }
        }
        boolean result = mContext.bindService(new Intent(getServiceAction()), mConnection, getBindServiceFlags());
        Log.d(TAG, "s1> bindService  result: " + result + " mContext: " + mContext.toString() + " mConnection: " + mConnection.toString() + getServiceAction() );
    }

    /**
     * サービスへ接続します。
     *
     * @param context
     *            コンテキスト
     * @param listener
     *            サービス接続イベント通知リスナー
     *
     * @throws IllegalStateException
     *             サービスが利用できない場合にスローします。
     */
    public void connect(Context context, ApiServiceConnection listener) {
        mServiceConnectionListener = listener;
        connect(context);
    }

//    public void connectForWidget(BroadcastReceiver receiver, Context context, ApiServiceConnection listener) {
//        if (context == null || listener == null) {
//            throw new NullPointerException();
//        }
//
//        mContext = context;
//
//        IBinder binder = receiver.peekService(context, new Intent(getServiceAction()));
//        Log.d(TAG, "connectForWidget binder[" + binder + "]");
//        onApiServiceConnected(null, binder);
//    }

    /**
     * サービスを切断します。
     *
     * @throws IllegalStateException
     *             サービスが接続していない場合にスローします。
     */
    public void disconnect() {
        if (mContext == null || mConnection == null) {
            throw new IllegalStateException();
        }
        mContext.unbindService(mConnection);
    }

    /** サービスに接続したときに通知されるリスナ。 */
    protected abstract void onApiServiceConnected(ComponentName name, IBinder service);

    /** サービスから切断したときに通知されるリスナ。 */
    protected abstract void onApiServiceDisconnected(ComponentName name);

    /** サービスのインタフェース名称を取得する。 */
    protected abstract String getInterfaceName();

    /** サービスのアクション名を取得する。 */
    protected abstract String getServiceAction();

    /** bindService() のフラグ (自動起動を許さない場合など) */
    protected abstract int getBindServiceFlags();

    protected ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected("+name.toString()+")");
            onApiServiceDisconnected(name);
            if (mServiceConnectionListener != null) {
                mServiceConnectionListener.onServiceDisconnected(getInterfaceName());
                mServiceConnectionListener = null;
            }
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected("+name.toString()+", "+service.toString()+")");
            onApiServiceConnected(name, service);
            if (mServiceConnectionListener != null) {
                mServiceConnectionListener.onServiceConnected(getInterfaceName());
            }
        }
    };
}