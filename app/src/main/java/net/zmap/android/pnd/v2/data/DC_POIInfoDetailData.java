package net.zmap.android.pnd.v2.data;

import java.io.Serializable;
import java.util.List;

public class DC_POIInfoDetailData implements Serializable {
	private static final long serialVersionUID = -5579429897019895610L;

	public String			m_sTellPhone;			//電話番号
	public String			m_sAddress;				//住所
	public String			m_sUrl;					//URL
	public String			m_sUrlText;				//URL用ボタン表示名
	public String			m_sDescription;			//ディスクリプション
	public DC_POIInfoData	m_POIInfo = new DC_POIInfoData();	//POI情報
	public List<SectionInfo>		m_oSectionInfo = null;	//セクション情報
	public List<TextInfo> 	m_oTextInfo = null;			//テキスト情報
	public String 		m_sOrbisType = null;			//オービス情報

// ADD.2013.03.01 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(3) START
	public int				m_nContentsID = 0;		//作成元コンテンツID
// ADD.2013.03.01 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(3)  END
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
	public String 			m_sWebtoLabel;			//提供企業URLラベル
	public String 			m_sWebtoUrl;			//提供企業URL
	public String			m_sWebtoUrlText;		//提供企業URLボタン表示名
	public boolean 			m_bDetailUpdate = false;//POI詳細情報更新フラグ
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
	public class SectionInfo implements Serializable{
		private static final long serialVersionUID = -2613064758303846376L;
		public short	sectionID;
		public String	textValue;
	}
	public class TextInfo implements Serializable{
		private static final long serialVersionUID = 6226188183599899391L;
		public short	sectionID;
		public String	textLabel;
		public String	textValue;
	}
}
