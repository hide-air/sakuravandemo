/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           POI_ZipCode_ListItem.java
 * Description    ZipCode 郵便番号検索
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class POI_ZipCode_ListItem {
    private long m_lZipCodeSize;
    private String m_ZipCode;
    private long m_lFullZipCodeSize;
    private String m_FullZipCode;
    private long m_lNameSize;
    private String m_Name;
    private long m_lLon;
    private long m_lLat;
    /**
    * Created on 2010/08/06
    * Title:       getM_lZipCodeSize
    * Description:   郵便番号サイズを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lZipCodeSize() {
        return m_lZipCodeSize;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_ZipCode
    * Description:   郵便番号を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getM_ZipCode() {
        return m_ZipCode;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_lFullZipCodeSize
    * Description:   装飾ありの郵便番号サイズを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lFullZipCodeSize() {
        return m_lFullZipCodeSize;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_lFullZipCodeSize
    * Description:   装飾ありの郵便番号を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getM_FullZipCode() {
        return m_FullZipCode;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lNameSize
    * Description:   住所名称サイズを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lNameSize() {
        return m_lNameSize;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_Name
    * Description:   住所名称を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getM_Name() {
        return m_Name;
    }
    /**
    * Created on 2010/08/06
    * Title:       setM_Name
    * Description:   住所名称を設定する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public void setM_Name(String name) {
        m_Name = name;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_lLon
    * Description:  経度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLon() {
        return m_lLon;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lLat
    * Description:  緯度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLat() {
        return m_lLat;
    }
}
