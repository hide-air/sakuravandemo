/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           JNIByte.java
 * Description    Float Class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class JNIFloat {
    private float m_fCommon;

    /**
    * Created on 2010/08/06
    * Title:       getM_fCommon
    * Description:  floatの値を取得する
    * @param1  無し
    * @return       float

    * @version        1.0
    */
    public float getM_fCommon() {
        return m_fCommon;
    }

}
