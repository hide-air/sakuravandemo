package net.zmap.android.pnd.v2.common.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

//Del 2011/11/11 Z01_h_yamada Start -->
//import net.zmap.android.pnd.v2.common.view.BatteryIcon;
//Del 2011/11/11 Z01_h_yamada End <--
//Del 2011/11/11 Z01_h_yamada Start -->
//import net.zmap.android.pnd.v2.common.view.SignalIcon;
//Del 2011/11/11 Z01_h_yamada End <--
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.services.CarMountStatusEventListener;
import net.zmap.android.pnd.v2.common.services.CarMountStatusService;
//import net.zmap.android.pnd.v2.common.services.ClarionService;
import net.zmap.android.pnd.v2.common.services.SystemService;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.data.NaviRun;

public class MenuBaseActivity extends BaseActivity implements CarMountStatusEventListener {
    public static final String SIGN_ID = "id";
    public static final String SIGN_TITLE = "title";
    public static final String SIGN_TYPE = "type";

    public int ONCE_GET_COUNT = 1;

    // XuYang add start 走行中の操作制限
    private boolean dialogCloseFlag = true;
    // XuYang add end 走行中の操作制限

    protected static final short DIALOG_MENUBASE_MAX = 0x10;

    private LinearLayout m_oOperArea = null;
    private LinearLayout m_oWorkArea = null;
//Del 2011/09/09 Z01_h_yamada Start -->
//	private NaviStateIcon naviState;
//Del 2011/09/09 Z01_h_yamada End <--

    private CarMountStatusService mCarMountStatusService = null;

//Del 2011/11/11 Z01_h_yamada Start -->
//    // XuYang add start #805
//    private BatteryIcon oBattery;
//    private SignalIcon imgSignal;
//    // XuYang add end #805
//Del 2011/11/11 Z01_h_yamada End <--

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ONCE_GET_COUNT = Constants.GET_JNI_COUNT;
        //full screen
        SystemService.setFullScreen(this);

        //set menu layout
        setContentView(R.layout.menu_base);
//Add 2011/07/25 Z01thedoanh Start -->
        View contentView = findViewById(android.R.id.content);
        View rootView = contentView.getRootView();
        View rootViewChild = ((ViewGroup)rootView).getChildAt(0);
        ViewGroup.LayoutParams params = rootViewChild.getLayoutParams();
        if (CommonLib.getIsFullScreeen() == 0) {
            params.width = CommonLib.screenWidth;
            params.height = CommonLib.screenHeight;
            rootViewChild.setLayoutParams(params);
        }
//Add 2011/07/25 Z01thedoanh End <--

        //set button of go back action
        Button oButton = (Button)findViewById(R.id.btnGoBack);
        oButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                goBack();
            }

        });

        //set button of go map action
        oButton = (Button)findViewById(R.id.btnGoMap);
        oButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                goMap();
            }

        });

        //set operator area
        m_oOperArea = (LinearLayout)findViewById(R.id.operArea);

        //set work area
        m_oWorkArea = (LinearLayout)findViewById(R.id.workArea);

//Del 2011/09/09 Z01_h_yamada Start -->
//		naviState = (NaviStateIcon) findViewById(R.id.imgIcon);
//		updateNaviStateIcon();
//Del 2011/09/09 Z01_h_yamada End <--

        // XuYang add start 走行中
        // 走行中の操作制限
        mCarMountStatusService = new CarMountStatusService(this, this);
        systemLimit();
        // XuYang add end 走行中

//Del 2011/11/11 Z01_h_yamada Start -->
//        // XuYang add start #805
//        oBattery = (BatteryIcon)findViewById(R.id.imgBattery);
//        imgSignal = (SignalIcon)findViewById(R.id.imgSignal);
//        // XuYang add end #805
//Del 2011/11/11 Z01_h_yamada End <--
    }

    @Override
    protected void onDestroy() {
//Del 2011/11/11 Z01_h_yamada Start -->
//        // XuYang add start #805
//        try {
//            if (oBattery != null) {
//                oBattery.release();
//            }
//        } catch (Exception e) {
//        }
//        try {
//            if (imgSignal != null) {
//                imgSignal.release();
//            }
//        } catch (Exception e) {
//        }
//        // XuYang add end #805
//Del 2011/11/11 Z01_h_yamada End <--
        if (mCarMountStatusService != null) {
            mCarMountStatusService.release();
        }
        super.onDestroy();
    }

//Del 2011/10/06 Z01yoneya Start -->
//    // XuYang add start 走行中
//    WakeLock wakeLock = null;
//
//    private void acquireWakeLock() {
//        if (wakeLock == null) {
//            PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
//            wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, this.getClass().getCanonicalName());
//            wakeLock.acquire();
//        }
//    }
//
//    private void releaseWakeLock() {
//        if (wakeLock != null && wakeLock.isHeld()) {
//            wakeLock.release();
//            wakeLock = null;
//        }
//    }
//Del 2011/10/06 Z01yoneya End <--

    // XuYang add end 走行中
    protected void onPause() {
        super.onPause();
//Del 2011/10/06 Z01yoneya Start -->
//        releaseWakeLock();
//Del 2011/10/06 Z01yoneya End <--
    }

    /**
     * Action of go front activity.
     */
    protected void goBack() {
        if (!onClickGoBack()) {
            finish();
        }
    }

    /**
     * 「現在地」のメソッド
     */
    protected void goMap() {
        if (!onClickGoMap()) {
//Chg 2011/12/14 Z01_h_yamada Start -->
//            CommonLib.locusMap(this);
//--------------------------------------------
            CommonLib.goCarLocation(this);
//Chg 2011/12/14 Z01_h_yamada End <--
        }
    }

    /**
     *
     * @return
     */
    protected LinearLayout getOperArea() {
        return m_oOperArea;
    }

    /**
     *
     * @return
     */
    protected LinearLayout getWorkArea() {
        return m_oWorkArea;
    }

    protected void setViewInOperArea(View oView) {
        if (oView != null && m_oOperArea != null) {
            m_oOperArea.addView(oView, new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.FILL_PARENT,
                    LinearLayout.LayoutParams.FILL_PARENT));
        }
    }

    protected void removeOperArea() {
        m_oOperArea.removeAllViews();
    }

    protected void setViewInWorkArea(View oView) {
        if (oView != null && m_oWorkArea != null) {
            m_oWorkArea.addView(oView, new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.FILL_PARENT,
                    LinearLayout.LayoutParams.FILL_PARENT));
        }
    }

    /**
     * 戻るボタンのクリック処理
     *
     * */
    protected boolean onClickGoBack() {
        return false;
    }

    /**
     * 現在地ボタンのクリック処理
     *
     * */
    protected boolean onClickGoMap() {
        return false;
    }

//Del 2011/09/17 Z01_h_yamada Start -->
//	/**
//	 * 左側タイトルの設定処理
//	 * @param wId タイトルアイコンのID
//	 *
//	 * */
//	public void setMenuTitle(int wId)
//	{
//Del 2011/09/09 Z01_h_yamada Start -->
//		ImageView oImage = (ImageView)findViewById(R.id.imgTitle);
//		if(oImage != null)
//		{
//			oImage.setImageResource(wId);
//		}
//Del 2011/09/09 Z01_h_yamada End <--
//	}
//Del 2011/09/17 Z01_h_yamada End <--

    @Override
    protected void onResume() {
        super.onResume();
//Del 2011/09/09 Z01_h_yamada Start -->
//	    updateNaviStateIcon();
//Del 2011/09/09 Z01_h_yamada End <--
//Del 2011/10/06 Z01yoneya Start -->
//        acquireWakeLock();
//Del 2011/10/06 Z01yoneya End <--

// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
        DrivingRegulation.updateScreenActivity(this);
//ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 再バインド Start -->
    	DrivingRegulation.Bind();
//ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 再バインド End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
    }

//Del 2011/09/09 Z01_h_yamada Start -->
//	protected void updateNaviStateIcon() {
//	    if(naviState != null){
//            naviState.updateStateIcon();
//        }
//    }
//Del 2011/09/09 Z01_h_yamada End <--

    public void showWorkArea() {
        m_oWorkArea.setVisibility(View.VISIBLE);
    }

    public void hideWorkArea() {
        m_oWorkArea.setVisibility(View.INVISIBLE);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_SEARCH) {
            return true;
        }
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            return true;
        }
        // XuYang add start 走行中
        // 走行／停止
//		if (keyCode == KeyEvent.KEYCODE_R && event.getAction() == KeyEvent.ACTION_DOWN) {
//			Constants.RunStopflag = true;
//		}
//		if (keyCode == KeyEvent.KEYCODE_S && event.getAction() == KeyEvent.ACTION_DOWN) {
//			Constants.RunStopflag = false;
//		}

        // クレードル（車載ホルダ）
//		if (keyCode == KeyEvent.KEYCODE_O && event.getAction() == KeyEvent.ACTION_DOWN) {
//			Constants.NaviOnOffflag = true;
//			SensorDataManager.onCarMountStatusChange(Constants.NaviOnOffflag);
//			int naviMode = CommonLib.getNaviMode();
//			dialogCloseFlag = true;
//	        if(naviMode != Constants.NE_NAVIMODE_CAR) {
//	            dialogCloseFlag = false;
//	            showDialog(Constants.DIALOG_CHANGE_MODE_CAR_MENU);
//	        }
//		}
//		if (keyCode == KeyEvent.KEYCODE_F && event.getAction() == KeyEvent.ACTION_DOWN) {
//			Constants.NaviOnOffflag = false;
//			SensorDataManager.onCarMountStatusChange(Constants.NaviOnOffflag);
//		}

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            if (dialogCloseFlag) {
                systemLimit();
            }
        }
        // XuYang add end 走行中
        return super.onKeyDown(keyCode, event);
    }

    // XuYang add start 走行中
    protected void systemLimit() {
        if (CommonLib.isLimit()) {
            goLocationInfo();
            setLimit(true);
        } else {
            setLimit(false);
        }
    }

    public void goLocationInfo() {
        Intent intent = new Intent();
        intent.setClass(this, LocationInfo.class);
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivity(intent);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivity(this, intent);
//Chg 2011/09/23 Z01yoneya End <--
    }

    public void setLimit(boolean flg) {
    }

    // XuYang add end 走行中

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "MenuBaseActivity::onActivityResult resultCode=" + resultCode + " class=" + this.toString());
        try {
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "MenuBaseActivity::onActivityResult callingAcitivity=" + getCallingActivity().toShortString());
        } catch (NullPointerException e) {
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "MenuBaseActivity::onActivityResult callingAcitivity is none");
        }

        if (resultCode == Constants.RESULTCODE_FINISH_ALL) {
            setResult(resultCode, data);
            finish();
            return;
        }
//Chg 2011/09/21 Z01yoneya Start -->
//        if (resultCode == Constants.RESULTCODE_FINISH_EXCEPT_MAPVIEW) {
//------------------------------------------------------------------------------------------------------------------------------------
        if (resultCode == Constants.RESULTCODE_GOBACK_TO_ROUTE_EDIT) {
//Chg 2011/09/21 Z01yoneya End <--
            setResult(resultCode, data);
            finish();
            return;
        }

        if (resultCode == Constants.RESULT_REROUTE) {
            setResult(resultCode, data);
            finish();
            return;
        }
        if (resultCode == Constants.RESULT_CHANGE_NAVI_MODE_MANUAL) {
            setResult(resultCode, data);
            finish();
            return;
        }

        if (resultCode == Constants.RESULTCODE_ROUTE_DETERMINATION) {
            this.setResult(resultCode, data);
            this.finish();
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    // XuYang add start 走行中の操作制限
    @Override
    public void onCarMountEventChange(Intent oIntent) {
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        final CustomDialog oDialog = new CustomDialog(this);
        switch (id) {
            case Constants.DIALOG_CHANGE_MODE_CAR_MENU:
                oDialog.setTitle(R.string.txt_navi_mode_change_title);
                oDialog.setMessage(R.string.mode_change_to_car_msg);

                oDialog.addButton(R.string.btn_ok,
                        new OnClickListener() {
                            public void onClick(View v) {
                                oDialog.dismiss();
                                removeDialog(Constants.DIALOG_CHANGE_MODE_CAR_MENU);
                                // 案内中であるかどうかを判断する。
                                if (CommonLib.isNaviStart()) {
                                    CommonLib.setBIsRouteDisplay(true);
                                    NaviRun.GetNaviRunObj().JNI_Java_ct_ChangeNaviMode(Constants.NE_NAVIMODE_CAR);
                                    NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
                                    goMap();
                                } else {
                                    CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
                                    // モード状態を更新
//Del 2011/09/09 Z01_h_yamada Start -->
//                              updateNaviStateIcon();
//Del 2011/09/09 Z01_h_yamada End <--
                                    systemLimit();
                                }

                            }
                        });
                return oDialog;
//Add 2011/12/15 Z01_h_yamada Start -->
            case Constants.DIALOG_FILE_WRITE_FAILED:

                oDialog.setTitle(R.string.dialog_configdata_save_title);
                oDialog.setMessage(R.string.dialog_file_write_error_msg);
                oDialog.addButton(R.string.btn_ok, new OnClickListener() {
	                    @Override
	                    public void onClick(View v) {
	                        removeDialog(Constants.DIALOG_FILE_WRITE_FAILED);
	                        oDialog.cancel();
	                    }
	                });
                return oDialog;
//Add 2011/12/15 Z01_h_yamada End <--
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
            case Constants.DIALOG_DURING_EXTERNAL_CONTROL:

                oDialog.setTitle(R.string.dialog_external_control_title);
                oDialog.setMessage(R.string.dialog_external_control_msg);
                oDialog.addButton(R.string.btn_ok, new OnClickListener() {
	                    @Override
	                    public void onClick(View v) {
	                        removeDialog(Constants.DIALOG_DURING_EXTERNAL_CONTROL);
	                        oDialog.cancel();
	                    }
	                });
                return oDialog;
            case Constants.DIALOG_NOT_ALREADY_ENVIRONMENT:

                oDialog.setTitle(R.string.dialog_not_already_environment_title);
                oDialog.setMessage(R.string.dialog_not_already_environment_msg);
                oDialog.addButton(R.string.btn_ok, new OnClickListener() {
	                    @Override
	                    public void onClick(View v) {
	                        removeDialog(Constants.DIALOG_NOT_ALREADY_ENVIRONMENT);
	                        oDialog.cancel();
	                    }
	                });
                return oDialog;
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
            default:
                break;
        }
        return super.onCreateDialog(id);
    }
    // XuYang add end 走行中の操作制限
}
