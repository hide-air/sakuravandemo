package net.zmap.android.pnd.v2.sakuracust;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
//import java.io.File;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.common.utils.StorageUtil;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.maps.MapActivity;


public class TegakiMemoActivity extends Activity {
    
    private boolean isEraser = false;
    private DrawView mCanvas = null;
// Add 2015-02-02 Start
    private MapActivity mMapAct = null;
 // Add 2015-02-02 End
    private int mColor = 0xff000000; // 主にPicker用
    private View mViewColor;
// 2015-04-20 Add Start
    private boolean mBackKey = false;
// 2015-04-20 Add End
    
    private static final String[] lineWidthVals = { "1", "3", "6", "12", "24", "48"};
    private static final Integer[] colorPaletteVals = {new Integer(0xff000000)/*黒*/, new Integer(0xffff0000)/*赤*/, new Integer(0xffff00ff)/*ピンク*/, 
        new Integer(0xff0000ff)/*青*/, new Integer(0xff00ffff)/*水色*/, new Integer(0xff00ff00)/*緑*/, new Integer(0xffffff00)/*黄*/,
        new Integer(0xff7f0000)/*茶*/, new Integer(0xff7f007f)/*紫*/, new Integer(0xffff7f27)/*オレンジ*/, new Integer(0xffc3c3c3)/*グレー*/, 
        new Integer(0xffffffff)/*白*/};

    private int mLineWidth = Integer.parseInt(lineWidthVals[0]);
    private static final int mEraserWidth = 48;
    FrameLayout mContainer; // キャンバスのコンテナ
    Spinner mSpinnerPenWidth; // ペンの太さスピナー
    LinearLayout mTextSizeContainer; // テキストサイズのコンテナ
    private LinearLayout mColorPalette; // カラーパレット
    private int mDrawMode; // 矩形などのドローモード
    private int offsetX, offsetY, viewX, viewY; // View移動用
    
    // ドローモード定義
    private static final int DRAW_MODE_NORMAL = 0;
    private static final int DRAW_MODE_RECT = 1;
    private static final int DRAW_MODE_RECT_FILL = 2;
    private static final int DRAW_MODE_POLYLINE = 3;
    private static final int DRAW_MODE_STRING = 4;
    
    // Naviからの値(保存先指定用)
    private String mFilePath = ""; // ファイルPath
    
    // ボタンの選択カラー
    private static final int SELECTED_COLOR = 0xff40B3DD;
    
    public String[] mWords;
    CustomDialog mDlg;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        mCanvas = new DrawView(this);
        mCanvas.setBackgroundColor(0xffffffff); // white
        setContentView(R.layout.tegaki_canvas);
        mContainer = (FrameLayout)findViewById(R.id.canvasContainer);
        mContainer.addView(mCanvas);

        // 戻るボタン
        View btnEnd = findViewById(R.id.btnGoBack);
        btnEnd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                mCanvas.deleteInternalObj();
                finish();
            }});
        // ペンの太さ選択スピナー
        mSpinnerPenWidth = (Spinner)findViewById(R.id.spinnerPenWidth);
        penWidthAdapter adapter = new penWidthAdapter(this, R.id.penWidthVal, lineWidthVals);
        adapter.setDropDownViewResource(R.layout.draw_drop_down_item);
        mSpinnerPenWidth.setAdapter(adapter);
        mSpinnerPenWidth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                    View view, int position, long id) {
                // ライン幅取得
                mLineWidth = Integer.parseInt(lineWidthVals[position]);
                // ライン幅設定
                mCanvas.setPenWidth(mLineWidth);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        mSpinnerPenWidth.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent event) {
                showColorPalette( false );
                commitPolyline();
                commitDrawText();
                procedureBeforeCancel();
                return false;
            }});
        // テキストサイズスピナー関連
        mTextSizeContainer = (LinearLayout)findViewById(R.id.textSizeContainer);
        Spinner mSpinnerTextSize = (Spinner)findViewById(R.id.spinnerTextSize);
        ArrayAdapter<CharSequence> textSizeAdapter = ArrayAdapter.createFromResource(this, R.array.text_sizes, R.layout.my_spinner_item);
        textSizeAdapter.setDropDownViewResource(R.layout.draw_drop_down_item);
        mSpinnerTextSize.setAdapter(textSizeAdapter);
        mSpinnerTextSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                    View view, int position, long id) {
                String[] arrays = getResources().getStringArray(R.array.text_sizes);
                // テキストサイズ取得
                int textSize = Integer.parseInt(arrays[position]);
                // テキストサイズ設定
                mCanvas.setTextSize(textSize);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        // 色
        mViewColor = findViewById(R.id.viewPenColor);
        mViewColor.setBackgroundColor(0xff000000); // 再起動時に、内部状態と一致しない色が表示されていたことがあったので初期設定しておく
        mViewColor.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                commitPolyline();
                showColorPalette( (mColorPalette.getVisibility() == View.VISIBLE)? false : true);
            }});
        // カラーパレットの作成
        mColorPalette = new LinearLayout(this);
        mColorPalette.setOrientation(LinearLayout.VERTICAL);
        mColorPalette.setBackgroundColor(0x807fcfcf);
        colorPaletteAdapter colorAdapter = new colorPaletteAdapter(this, R.layout.color_palette_item, colorPaletteVals);
        GridView gridView = new GridView(this);
        gridView.setAdapter(colorAdapter);
        gridView.setNumColumns(4);
        gridView.setHorizontalSpacing(5);
        gridView.setVerticalSpacing(5);
        gridView.setBackgroundColor(0x807fcfcf);
        gridView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
                @SuppressWarnings("rawtypes")
                public void onItemClick(AdapterView parent, View v, int position, long id) {
                    showColorPalette(false);
                    selectColor(position);
                }});
        mColorPalette.addView(gridView, LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Button btnColorPicker = new Button(this);
        btnColorPicker.setText(R.string.btn_color_picker);
        btnColorPicker.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showColorPalette(false);
                colorPickerDialog();
            }});
        mColorPalette.addView(btnColorPicker, LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(300, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.TOP;
        mContainer.addView(mColorPalette, params);

        // モード切替
        Spinner mSpinnerDrawMode = (Spinner)findViewById(R.id.spinnerDrawMode);
        ArrayAdapter<CharSequence> modeAdapter = ArrayAdapter.createFromResource(this, R.array.draw_mode, R.layout.my_spinner_item);
        modeAdapter.setDropDownViewResource(R.layout.draw_drop_down_item);
        mSpinnerDrawMode.setAdapter(modeAdapter);
        
        
        mSpinnerDrawMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                    View view, int position, long id) {
                mDrawMode = position;
                switch(mDrawMode) {
                    case DRAW_MODE_NORMAL: // 標準
                        setTextMode(false);
                        if( !isEraser ) {
                            mCanvas.clearCommandMode();
                        }
                        break;
                    case DRAW_MODE_RECT: // 矩形(枠線のみ)
                        setTextMode(false);
                        if( !isEraser ) {
                            mCanvas.startDrawRectangle();
                        }
                        break;
                    case DRAW_MODE_RECT_FILL: // 矩形(塗りつぶし)
                        setTextMode(false);
                        if( !isEraser ) {
                            mCanvas.startDrawRectangleFill();
                        }
                        break;
                    case DRAW_MODE_POLYLINE: // 折れ線
                        setTextMode(false);
                        if( !isEraser ) {
                            mCanvas.startDrawPolyline();
                        }
                        break;
                    case DRAW_MODE_STRING: // 文字列
                        setTextMode(true);
                        if( !isEraser ) {
                            mCanvas.startDrawString(mContainer);
                        }

                        break;
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        mSpinnerDrawMode.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent event) {
                showColorPalette( false );
                commitPolyline();
                commitDrawText();
                procedureBeforeCancel();
                return false;
            }});

        // 消しゴム
        View eraser = findViewById(R.id.viewEraser);
        eraser.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                
                showColorPalette(false);
                commitPolyline();
                commitDrawText();
                procedureBeforeCancel();
                changeEraserMode();
            }});
        // Undoボタン
        View btnUndo = findViewById(R.id.btnUndo);
        btnUndo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                procedureBeforeCancel();
                mCanvas.undo();
            }});
        
        // クリアボタン
        View btnClear = findViewById(R.id.btnClear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                procedureBeforeCancel();
                mCanvas.clear();
            }});
        // 保存ボタン
        View btnStore = findViewById(R.id.btnStore);
        btnStore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                procedureBeforeCancel();
                executeStore();
            }});
// Add 2015-02-02 削除ボタン追加　Start
        // 削除ボタン
        View btnDelete = findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
/*
                // 確認ダイアログの生成
                //AlertDialog.Builder alertDlg = new AlertDialog.Builder(this);
            	AlertDialog.Builder alertDlg = new AlertDialog.Builder(TegakiMemoActivity.this);
                alertDlg.setTitle("ダイアログタイトル");
                alertDlg.setMessage("メッセージ");
                alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // OK ボタンクリック処理
                            procedureBeforeCancel();
                            executeDelete(mFilePath);
                        }
                    });
                alertDlg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Cancel ボタンクリック処理
                        }
                    });
                // 表示
                alertDlg.create().show();
*/            	
            	System.out.println("mFilePath => " + mFilePath);
                procedureBeforeCancel();
                executeDelete(mFilePath);
                
                /* mMapAct.updateTegakiMemoBtn(0, tegakiBtn)　でエラーになる
                Button tegakiBtn;
                LayoutInflater Inflater = LayoutInflater.from(TegakiMemoActivity.this);
                LinearLayout item = (LinearLayout)Inflater.inflate(R.layout.listrow_work_list, null);
                tegakiBtn= (Button)item.findViewById(R.id.btnHandWrittenMemo);
                tegakiBtn.setTag(item);
                mMapAct.updateTegakiMemoBtn(0, tegakiBtn);
                 */
                
            }});
// Add 2015-02-02 削除ボタン追加　End
        
        // pathを取得
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
    	System.out.println("TegakiMemoActivity Pathの取得");
    	System.out.println("bundle => " + bundle);
        if (bundle != null) {
        	System.out.println("bundle is not NULL");
            mFilePath = bundle.getString("path", "");
        	System.out.println("mFilePath => " + mFilePath);
            if( "".equals(mFilePath) ) {
            	System.out.println("mFilePath is not NULL");
                // 基本的にあり得ないが、パスが取得できなければ終了する
                finish();
            }
            File file = new File(mFilePath);
            if( file.exists() ) { // 既にシンボルに関連付けられた手書きメモが存在する？
            	if(file.isDirectory() == false) {
            		System.out.println("mFilePath => " + mFilePath + "はファイル！");
            		executeLoad(mFilePath);
            	} else {
            		System.out.println("mFilePath => " + mFilePath + "はディレクトリ！");
                    //finish();
            	}
            }
        	
        } else {
            // 基本的にあり得ないが、パスが取得できなければ終了する
            finish();
        }

        // 頻出単語の取得
        DbHelper helper = new DbHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();
        mWords = helper.getWords(db);
        db.close();
        helper.close();
    }
    
    protected void onResume() {
        super.onResume();
        showColorPalette(false);
    }

    protected void onPause() {
        super.onPause();
        if( mDlg != null ) {
            mDlg.dismiss();
        }
        mCanvas.hideSoftKeyboard();
    }

    
// Add 2015-04-02 BACKキー対応 Start
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (mapView != null && keyCode == KeyEvent.KEYCODE_BACK) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
// 2015-04-20 Mod
            if (keyCode == KeyEvent.KEYCODE_BACK ) {
        	
            // BACKキーを連続して押しても初回のみメッセージを出力
            if( mBackKey == false ) {
            	Toast.makeText(this, "前の画面に戻る場合は左上の「戻る」ボタンを押してください", Toast.LENGTH_SHORT).show();
            }
            mBackKey = true;
        	return false;
        }
        return super.onKeyDown(keyCode, event);
    }
// Add 2015-04-02 BACKキー対応 End
    
    /**
     * カラーパレットを開く(引数true) or 閉じる(引数false)
     */
    void showColorPalette(boolean showFlag) {
        if( showFlag) {
            mColorPalette.setVisibility(View.VISIBLE);
        } else {
            mColorPalette.setVisibility(View.GONE);
        }
    }

    /**
     * カラーパレットからのカラー選択
     */
    private void selectColor(int pos) {
        mColor = colorPaletteVals[pos].intValue();
        mViewColor.setBackgroundColor(mColor);
        if(!isEraser) {
            mCanvas.setColor(mColor);
        }
    }

    private void colorPickerDialog() {
        mDlg = new CustomDialog(this);
        mDlg.setTitle(R.string.title_color_select);
        mDlg.EnableTitleIcon(false);  // タイトルのアイコンは消す
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogView = inflater.inflate(R.layout.color_picker, null);
       
        final TextView textR = (TextView)dialogView.findViewById(R.id.colorPickerTextR);
        textR.setText(String.format("赤(%03d)",  (mColor & 0x00ff0000) >> 16));
        final TextView textG = (TextView)dialogView.findViewById(R.id.colorPickerTextG);
        textG.setText(String.format("緑(%03d)", (mColor & 0x0000ff00) >> 8));
        final TextView textB = (TextView)dialogView.findViewById(R.id.colorPickerTextB);
        textB.setText(String.format("青(%03d)", (mColor & 0x000000ff)));
        final View viewColor = dialogView.findViewById(R.id.colorPickerColorSample);
        viewColor.setBackgroundColor(mColor);
        // red
        final SeekBar seekBarR = (SeekBar)dialogView.findViewById(R.id.colorPickerSeekBarR);
        seekBarR.setProgress((mColor & 0x00ff0000) >>16);
        seekBarR.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int value = seekBar.getProgress();
                textR.setText(String.format("Red(%03d)", value));
                mColor = (mColor & 0xff00ffff) | value << 16;
                viewColor.setBackgroundColor(mColor);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                int value = seekBar.getProgress();
                textR.setText(String.format("赤(%03d)", value));
                mColor = (mColor & 0xff00ffff) | value << 16;
                viewColor.setBackgroundColor(mColor);
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
                int value = seekBar.getProgress();
                textR.setText(String.format("赤(%03d)", value));
                mColor = (mColor & 0xff00ffff) | value << 16;
                viewColor.setBackgroundColor(mColor);
            }
        });
        // green
        final SeekBar seekBarG = (SeekBar)dialogView.findViewById(R.id.colorPickerSeekBarG);
        seekBarG.setProgress((mColor & 0x0000ff00) >> 8);
        seekBarG.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int value = seekBar.getProgress();
                textG.setText(String.format("緑(%03d)", value));
                mColor = (mColor & 0xffff00ff) | value << 8;
                viewColor.setBackgroundColor(mColor);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                int value = seekBar.getProgress();
                textG.setText(String.format("緑(%03d)", value));
                mColor = (mColor & 0xffff00ff) | value << 8;
                viewColor.setBackgroundColor(mColor);
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
                int value = seekBar.getProgress();
                textG.setText(String.format("緑(%03d)", value));
                mColor = (mColor & 0xffff00ff) | value << 8;
                viewColor.setBackgroundColor(mColor);
            }
        });
        // Blue
        final SeekBar seekBarB = (SeekBar)dialogView.findViewById(R.id.colorPickerSeekBarB);
        seekBarB.setProgress((mColor & 0x000000ff));
        seekBarB.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                    int value = seekBar.getProgress();
                textB.setText(String.format("青(%03d)", value));
                mColor = (mColor & 0xffffff00) | value;
                viewColor.setBackgroundColor(mColor);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                int value = seekBar.getProgress();
                textB.setText(String.format("青(%03d)", value));
                mColor = (mColor & 0xffffff00) | value;
                viewColor.setBackgroundColor(mColor);
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
                int value = seekBar.getProgress();
                textB.setText(String.format("青(%03d)", value));
                mColor = (mColor & 0xffffff00) | value;
                viewColor.setBackgroundColor(mColor);
            }
        });
        mDlg.setContentView(dialogView);
        mDlg.addButton(this.getResources().getString(R.string.btn_ok),new OnClickListener(){
            @Override
            public void onClick(View v) {
                mViewColor.setBackgroundColor(mColor);
                if(!isEraser) {
                    mCanvas.setColor(mColor);
                }
                mDlg.dismiss();
                mDlg = null;
            }
        });
        mDlg.show();
    }

    /**
     * 保存を実行する
     */
    public void executeStore() {
        try {
            mCanvas.storeImage(CompressFormat.PNG, mFilePath);
            Intent intent = new Intent();
            
            Bundle bundle = new Bundle();
            bundle.putString("path", mFilePath);
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            mCanvas.deleteInternalObj();
            finish();
        }
        catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

// Add 2015-02-02 Start
    /**
     * ファイル削除を実行する
     */
    public void executeDelete(final String filePath) {
        try {
        	File file = new File(filePath);

            if (file.exists()) {

                // 確認ダイアログの生成
                //AlertDialog.Builder alertDlg = new AlertDialog.Builder(this);
            	AlertDialog.Builder alertDlg = new AlertDialog.Builder(TegakiMemoActivity.this);
                alertDlg.setTitle("手書きメモ削除");
                alertDlg.setMessage("手書きメモを削除します。よろしいですか？");
                alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // OK ボタンクリック処理
                            procedureBeforeCancel();
            	        	mCanvas.deleteImage(filePath);
            	        	
                            Intent intent = new Intent();
            	            
            	            Bundle bundle = new Bundle();
            	            // Mod 2015-02-26 削除したのだからパスはクリアしておく
//            	            bundle.putString("path", mFilePath);
            	            bundle.putString("path", "");
            	            intent.putExtras(bundle);
            	            setResult(RESULT_OK, intent);
            	            mCanvas.deleteInternalObj();
            	            finish();
                        }
                    });
                alertDlg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Cancel ボタンクリック処理
                        }
                    });
                // 表示
                alertDlg.create().show();
/*            	
	        	mCanvas.deleteImage(filePath);
	            Intent intent = new Intent();
	            
	            Bundle bundle = new Bundle();
	            bundle.putString("path", mFilePath);
	            intent.putExtras(bundle);
	            setResult(RESULT_OK, intent);
	            mCanvas.deleteInternalObj();
	            finish();
*/	            
            }
        }
        catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

/* mMapAct.updateTegakiMemoBtn(0, tegakiBtn)　でエラーになる
        Button tegakiBtn;
        LayoutInflater Inflater = LayoutInflater.from(this);
        LinearLayout item = (LinearLayout)Inflater.inflate(R.layout.listrow_work_list, null);
        tegakiBtn= (Button)item.findViewById(R.id.btnHandWrittenMemo);
        tegakiBtn.setTag(item);
        mMapAct.updateTegakiMemoBtn(0, tegakiBtn);
*/    	
    }
 // Add 2015-02-02 End
    
    /**
     * ロードを実行する
     */

    public void executeLoad(String filePath) {
        mCanvas.loadImage(filePath);
    }

    /**
     * 消しゴムモードに応じたボタンの設定
     */
    private void setAsEraserMode() {
        View v = findViewById(R.id.viewEraser);
        if( isEraser ) {
            v.setBackgroundColor(SELECTED_COLOR); // 選択された感を出す為
            mCanvas.clearCommandMode();
            mCanvas.setColor(0xffffffff); // white
            mCanvas.setPenWidth(mEraserWidth);
        } else {
            v.setBackgroundColor(0xffa9a9a9); // 元に戻す
            switch(mDrawMode) {
                case DRAW_MODE_NORMAL: // 標準
                    mCanvas.clearCommandMode();
                    break;
                case DRAW_MODE_RECT: // 矩形(枠線のみ)
                    mCanvas.startDrawRectangle();
                    break;
                case DRAW_MODE_RECT_FILL: // 矩形(塗りつぶし)
                    mCanvas.startDrawRectangleFill();
                    break;
                case DRAW_MODE_POLYLINE: // 折れ線
                    mCanvas.startDrawPolyline();
                    break;
                case DRAW_MODE_STRING: // 文字列
                    mCanvas.startDrawString(mContainer);
                    break;
            }
            mCanvas.setColor(mColor);
            mCanvas.setPenWidth(mLineWidth);
        }
    }

    /**
     * 消しゴムモードの切り替え
     */
    private void changeEraserMode() {
        isEraser = !isEraser;
        setAsEraserMode();
    }
    
    /**
     * 消しゴムモードの解除
     */
    private void releaseEraserMode() {
        isEraser = false;
        setAsEraserMode();
    }
    
    /**
     * 文字列モードの切り替え(ON：線幅無効&文字サイズ有効、OFF：線幅有効&文字サイズ無効)
     */
    private void setTextMode(boolean flag) {
        if( flag) { // ON
            mSpinnerPenWidth.setVisibility(View.GONE);
            mTextSizeContainer.setVisibility(View.VISIBLE);
        } else { // OFF
            mSpinnerPenWidth.setVisibility(View.VISIBLE);
            mTextSizeContainer.setVisibility(View.GONE);
        }
    }
    
    /**
     * 文字列モードならば、文字列の確定をする
     */
    private void commitDrawText(){
        if( mCanvas.getCommandMode() == DrawView.COMMAND_MODE_STRING) {
            mCanvas.commitDrawText();
        }
        mCanvas.ClearEditText();
    }
    
    /**
     * ポリラインモードならば、編集中ポリラインの確定をする
     */
    private void commitPolyline(){
        if( mCanvas.getCommandMode() == DrawView.COMMAND_MODE_POLYLINE) {
            mCanvas.commitPolyline();
        }
    }
    
    
    /**
     * あらゆるキャンセル、モードチェンジ前に行う共通手続き
     */
    private void procedureBeforeCancel()
    {
        showColorPalette(false);
        commitPolyline();
        commitDrawText();
    }

}

/**
 * ペン幅スピナー用Adapter
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
class penWidthAdapter extends ArrayAdapter{
    private LayoutInflater mInflater;

    public penWidthAdapter(Context context,  int textViewResourceId, String[] objects) {
        super(context, textViewResourceId, objects);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        if( convertView == null ) {
            convertView = mInflater.inflate(R.layout.draw_line_width, null);
        }
        TextView txtWidth = (TextView)(convertView.findViewById(R.id.penWidthVal));
        txtWidth.setText("線幅:" + (String)(this.getItem(position)));
        ImageView imgWidth = (ImageView)(convertView.findViewById(R.id.penWidth));

        switch(position) {
            case 0: // width 1
                imgWidth.setImageResource(R.drawable.pen_width1);
                break;
            case 1: // width 3
                imgWidth.setImageResource(R.drawable.pen_width3);
                break;
            case 2: // width 6(default)
                imgWidth.setImageResource(R.drawable.pen_width6);
                break;
            case 3: // width 12
                imgWidth.setImageResource(R.drawable.pen_width12);
                break;
            case 4: // width 24
                imgWidth.setImageResource(R.drawable.pen_width24);
                break;
            case 5: // width 48
                imgWidth.setImageResource(R.drawable.pen_width48);
                break;
        }
        return (View)convertView;
    }
}

/**
 * カラーパレット用Adapter
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
class colorPaletteAdapter extends ArrayAdapter{
    Context mCtx;
    private LayoutInflater mInflater;
    
    public colorPaletteAdapter(Context ctx, int resId, Integer[] vals) {
        super(ctx, resId, vals);
        mInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mCtx = ctx;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        if( convertView == null ) {
            convertView = mInflater.inflate(R.layout.color_palette_item, null);
        }
        int colVal = ((Integer)this.getItem(position)).intValue();
        convertView.setBackgroundColor(colVal);
        return (View)convertView;
    }
}


/**
 * 手書きメモ よく使う単語DB用Helper
 */
class DbHelper extends SQLiteOpenHelper {
    private static final String navidatapath = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) +
         "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME;
    public static final String DATABASE_NAME = navidatapath + "/tegaki_words.db";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // DBが存在しない時にのみ呼ばれる
        db.execSQL("CREATE TABLE FrequencyWords  (_id INTEGER PRIMARY KEY, Word TEXT);");
        // デフォルト値をInsert
        db.execSQL("INSERT INTO FrequencyWords VALUES(1, '見込み');");
        db.execSQL("INSERT INTO FrequencyWords VALUES(2, '継続');");
        db.execSQL("INSERT INTO FrequencyWords VALUES(3, '解約');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // DBの変更があれば、ここで行う
    }

    public String[] getWords(SQLiteDatabase db) {
        Cursor cur = db.rawQuery("SELECT Word FROM FrequencyWords", new String[] {});
        if( cur == null ) return null;
        int num = cur.getCount();
        String words[] = new String[num];
        cur.moveToFirst();
        for(int i = 0; i < num; i++ ) {
            words[i] = cur.getString(0);
            cur.moveToNext();
        }
        cur.close();
        return words;
    }
}
