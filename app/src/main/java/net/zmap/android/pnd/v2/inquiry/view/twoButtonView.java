//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.view;

import android.content.Context;
import android.view.View.OnClickListener;
import android.widget.Button;

import net.zmap.android.pnd.v2.R;

public class twoButtonView extends ItemView {

	private Button btnEdit = null;
//Del 2011/11/01 Z01_h_yamada Start -->
//	private Button btnSearch = null;
//Del 2011/11/01 Z01_h_yamada End <--

	public twoButtonView(Context oContext, int wId, int wResId) {
		super(oContext, wId, wResId);
		initButton(oContext);
	}
	public twoButtonView(Context oContext) {
		super(oContext, 0, R.layout.two_button);
		initButton(oContext);
	}

	private void initButton(Context oContext) {
		btnEdit = (Button) this.findViewById(oContext, R.id.Button_edit);
//Del 2011/11/01 Z01_h_yamada Start -->
//		btnSearch = (Button) this.findViewById(oContext, R.id.Button_search);
//Del 2011/11/01 Z01_h_yamada End <--

	}

//Chg 2011/11/01 Z01_h_yamada Start -->
//	public void setButtonListener(OnClickListener oEditListener,OnClickListener oSearchListener) {
//		btnEdit.setOnClickListener(oEditListener);
//		btnSearch.setOnClickListener(oSearchListener);
//	}
//--------------------------------------------
	public void setButtonListener(OnClickListener oEditListener) {
		btnEdit.setOnClickListener(oEditListener);
	}
//Chg 2011/11/01 Z01_h_yamada End <--

}
