package net.zmap.android.pnd.v2;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import net.zmap.android.pnd.v2.api.ItsmoNaviDriveExternalApi;
import net.zmap.android.pnd.v2.api.NaviIntent;
import net.zmap.android.pnd.v2.api.overlay.Icon;
import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.app.control.NaviControllReceiver;
//import net.zmap.android.pnd.v2.app.launch.DownloadMapActivity;
import net.zmap.android.pnd.v2.app.launch.NaviLaunchService;
import net.zmap.android.pnd.v2.broadcast.NaviApiBroadcastSender;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.DeliveryInfo;
import net.zmap.android.pnd.v2.common.ProductInfo;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.MediaCheckTimer;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.activity.BaseActivity;
import net.zmap.android.pnd.v2.common.activity.BaseActivityStack;
//import net.zmap.android.pnd.v2.common.services.ClarionService;
import net.zmap.android.pnd.v2.common.utils.DbHelper;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.StorageUtil;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.overlay.DrawUserLayer;
import net.zmap.android.pnd.v2.overlay.DrawUserLayerManager;
import net.zmap.android.pnd.v2.overlay.DrawIconPathManager;
import net.zmap.android.pnd.v2.overlay.DrawUserLayerManager;
import net.zmap.android.pnd.v2.route.data.RouteNodeData;
import net.zmap.android.pnd.v2.sakuracust.UserIconInfo;
import net.zmap.android.pnd.v2.sakuracust.UserLineInfo;

//import net.zmap.android.pnd.v2.downloader.common.ExtraName;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * PndApplication
 * PNDアプリケーションクラス - 共有データ管理
 *
 * ナビアプリプロセスのみ使用する(ナビ起動サービスでは使用しない)
 *
 */
public class NaviApplication extends Application {

    protected NaviControllReceiver mNaviCtrlReceiver = null;
    protected BaseActivityStack mBaseActivibyStack = null;
    protected NaviAppDataPath mNaviAppDataPath = null;
//Add 2011/11/16 Z01_h_yamada Start -->
    protected MediaCheckTimer mMediaCheckTimer = null;
    protected final int MEDIA_SIZE_CHECK_INTERVAL = 10000;	// 10s
//Add 2011/11/16 Z01_h_yamada End <--
    protected ItsmoNaviDrive mMainActivity = null;

// Add by CPJsunagawa '13-2-12-25 Start
	/** 1ページあたりのアイテム数 */
    public static final int ONE_PAGE_ITEM_COUNT = 20;
// Add by CPJsunagawa '13-2-12-25 End

// Add 2011/09/06 sawada [ExternalAPI] Start [ExternalAPI] -->
    protected NaviApiBroadcastSender mNaviApiBroadcastSender = null;
// Add 2011/09/06 sawada [ExternalAPI] End [ExternalAPI] <--

    private Queue<Intent> mExternalApiIntentQueue = new LinkedList<Intent>();
    private ExecutorService  executor;

// Add 2011/10/11 r.itoh Start [ExternalAPI] -->
    protected DrawUserLayerManager mDrawUserLayerManager = new DrawUserLayerManager();
//    protected DestShareCom mDestShareCom = null;
// Add 2011/10/11 r.itoh End [ExternalAPI] <--

    private boolean mStarted = false;

//@@MOD-START BB-0003 2012/10/19 Y.Hayashida
    private boolean mShowWidgetTop = false;
    private boolean mShowWidgetRight = false;
//@@MOD-START BB-0003 2012/10/19 Y.Hayashida

    private boolean mProbeFlag = false;

    // V2.5 外部Icon とDR ProFile 指定を追加する
    private static boolean	mMapIcon_ON = false;				// Exconfig 読み出し完了で True
    private static String	mcsvname = "DrawParameter.csv";		// name= がない場合用
    private static String	mcsvdirpath = "";					// dir= がない場合用
    private static boolean	mDR_ON = false;						// Exconfig 読み出し完了で True
	private static boolean	mDR_flag = false;					// DR 使用しないでTrue(センサアシストより強い)
	private static String	mprofilename = "";					// ProFileファイル名
	private static String	mdrdirpath = "";					// ProFile パス
    private static boolean	mMap_HighResolution_OFF = false;	// 高解像度表示しない場合 True

// Add by CPJsunagawa'13-12-25 End
    protected String mSelectedCourseName = "";
	protected ArrayList<RouteNodeData> mRouteNodeList = null; // 目的地リストから作成した経路リスト
    protected ArrayList<DeliveryInfo> mDeliveryInfo = null; // 顧客リストから作成した顧客属性リスト
    protected ArrayList<ProductInfo> mProductInfo = null; // 商品情報リストから作成した商品情報リスト
    //protected int curRouteStartPos = 0; // 目的地リストでの選択範囲の先頭位置

// Add by CPJsunagawa '2015-08-08 Start
    //protected long mUserIconLayerID = -1;
    protected ArrayList<UserIconInfo> mUserIconInfo = null; // 登録された図形情報
// Add by CPJsunagawa '2015-08-08 End
    
    // 線情報
    //protected long mUserLineLayerID = -1;
    protected ArrayList<UserLineInfo> mUserLineInfo = null;
    protected UserLineInfo mDrawingUserLineInfo = null;

    // 1ページあたりの行き先数（スタート地点を除く）
    public static final int ROUTE_ONE_PAGE_COUNT = 6;
    public static final int FIRST_ONLY_ROUTE_FLAG = -1;
    private int mCurRoutePage = FIRST_ONLY_ROUTE_FLAG;
    
    // 10m拡大地点の座標
    private long mCloseUpLatitude = 0;
    private long mCloseUpLongitude = 0;
    
    // マニュアルマッチング中か？
    private boolean mIsManualMatching = false;
    
    public boolean IsManualMatching(){return mIsManualMatching;}
    public void setIsManualMatching(boolean value){mIsManualMatching = value;}
    
    // 現在ルート中の配達先
    // 詳細画面から「ルート」ボタンを押すと、ルート先はそこになる。
    public static final int NO_ROUTING = -1;
    private int mNowRoutingCustomerIndex = NO_ROUTING;
    
    // 指定した配達情報に設定する。
    public void setNowRoutingCustomerIndex(DeliveryInfo delivInfo)
    {
    	mNowRoutingCustomerIndex = delivInfo.mMyIndex;
    }
    
    // 最初の未配達のインデックスを、ルート先にする。
    public void setNowRoutingCustomerIndexAsFirstNoDeliv()
    {
    	mNowRoutingCustomerIndex = getFirstNoDeliveryIndex();
    }

    // 配達先をクリアする。
    public void clearNowRoutingCustomerIndex()
    {
    	mNowRoutingCustomerIndex = NO_ROUTING;
    }
    
    // 現在ルート中の顧客インデックスを取得する。
    public int getNowRoutingCustomerIndex(){return mNowRoutingCustomerIndex;}
    
    public boolean isNowRouting(){return (mNowRoutingCustomerIndex != NO_ROUTING);}
    
    // マッチングモード
    private int mMatchingMode = 0;
    public int getMatchigMode(){return mMatchingMode;}
    public void setMatchigMode(int matchingMode){mMatchingMode = matchingMode;}
    public void clearMatchigMode(){setMatchigMode(0);}
    
    // リスト表示のリトライ
    private boolean isRetryShowList = false;
    public void setRetryShowList(){isRetryShowList = true;}
    public void clearRetryShowList(){isRetryShowList = false;}
    public boolean getIsRetryShowList(){return isRetryShowList;}
    
    // 初回リスト修復で最初の配達先に飛ばすか？
    private boolean isSetNoDelivFirst = false;
    public void setSetNoDelivFirst(){isSetNoDelivFirst = true;}
    public void clearSetNoDelivFirst(){isSetNoDelivFirst = false;}
    public boolean getIsSetNoDelivFirst(){return isSetNoDelivFirst;}
    
    // ルートリストを初期化する
    public void initRouteList() {
        if( mRouteNodeList == null) {
            mRouteNodeList = new ArrayList<RouteNodeData>();
        } else {
            mRouteNodeList.clear();
        }
    }

    // ルートリストに地点を追加する
    public void addRoutePoint(RouteNodeData node) {
        if( mRouteNodeList == null) { // fail safe
            mRouteNodeList = new ArrayList<RouteNodeData>();
        }
        mRouteNodeList.add(node);
    }

    // ルートリストの地点データを取得する
    public RouteNodeData getRouteInfo(int index) {
        if( index < 0 || index >= mRouteNodeList.size()){ // fail safe
            return null;
        }
        return mRouteNodeList.get(index);
    }
    
    // ルートリストのサイズを取得する
    public int getRouteListSize() {
        if( mRouteNodeList == null) { // fail safe
            mRouteNodeList = new ArrayList<RouteNodeData>();
        }
        return mRouteNodeList.size();
    }
    
    // ルートのページ数を取得する
    public int getRoutePageCount() {
    	int routeNodeCnt = getRouteListSize();
    	// 3減算する理由
    	// 「現在地」の分がある
    	// 0ページ目は1から（0は一本道だから）
    	// 一本道は-1ページ目である。
    	int result = (routeNodeCnt - 3) / ROUTE_ONE_PAGE_COUNT + 1;
    	return result;
    }
    
    // ルートのページを設定する。
    public void setRoutePage(int page)
    {
    	int maxPageCnt = getRoutePageCount();
    	if (page >= maxPageCnt) mCurRoutePage = maxPageCnt - 1;
    	else if (page <= FIRST_ONLY_ROUTE_FLAG) mCurRoutePage = FIRST_ONLY_ROUTE_FLAG;
    	else mCurRoutePage = page;
    }
    
    // ルートのページをインクリメントする。
    public void incrementRoutePage()
    {
    	int nowPage = getRoutePage();
    	setRoutePage(nowPage + 1);
    }
    
    // ルートのページをデクリメントする。
    public void decrementRoutePage()
    {
    	int nowPage = getRoutePage();
    	setRoutePage(nowPage - 1);
    }
    
    // ルートのページをデフォルト（-1）にする。
    public void setRoutePageFirstOnly()
    {
    	setRoutePage(FIRST_ONLY_ROUTE_FLAG);
    	setNowRoutingCustomerIndexAsFirstNoDeliv();
    }
    
    // ルートのページを取得する
    public int getRoutePage(){return mCurRoutePage;}
    
    // 現ルートのページから、スタートインデックスを取得する。
    public int getRouteStartIndex()
    {
    	if (mCurRoutePage <= FIRST_ONLY_ROUTE_FLAG) return FIRST_ONLY_ROUTE_FLAG;
    	else return mCurRoutePage * ROUTE_ONE_PAGE_COUNT + 1;
    }

    // 最初のルートへの一本道検索を行うか？
    public boolean isFirstOnlyRoute(){return getRouteStartIndex() == FIRST_ONLY_ROUTE_FLAG;}
    
    // 商品情報リストを初期化する
    public void initProductList() {
        if( mProductInfo == null) {
            mProductInfo = new ArrayList<ProductInfo>();
        } else {
        	mProductInfo.clear();
        }
    }

    // 商品情報リストを追加する
    public void addProductItem(ProductInfo info) {
        if( mProductInfo == null) { // fail safe
        	mProductInfo = new ArrayList<ProductInfo>();
        }
        mProductInfo.add(info);
    }

    // 商品情報リストの地点データを取得する
    public ProductInfo getProduct(int index) {
    	if (mProductInfo == null) return null;
        if( index < 0 || index >= mProductInfo.size()){ // fail safe
        }
        return mProductInfo.get(index);
    }
    
    // 商品情報リストのサイズを取得する
    public int getProductListSize() {
        if( mProductInfo == null) { // fail safe
        	mProductInfo = new ArrayList<ProductInfo>();
        }
        return mProductInfo.size();
    }

    // 属性リストを初期化する
    public void initInfoList() {
        if( mDeliveryInfo == null) {
            mDeliveryInfo = new ArrayList<DeliveryInfo>();
        } else {
            mDeliveryInfo.clear();
        }
        
        if (mDrawUserLayerManager != null)
        {
        	mDrawUserLayerManager.unregistAllUserLayer();
        }
    }
// Add by CPJsunagawa '2015-08-08 Start
    // 図形情報リストを初期化する
    public void initUserIconInfoList() {
        if( mUserIconInfo == null) {
        	mUserIconInfo = new ArrayList<UserIconInfo>();
        } else {
        	mUserIconInfo.clear();
        }
        
        if (mUserLineInfo == null) {
        	mUserLineInfo = new ArrayList<UserLineInfo>();
        } else {
        	mUserLineInfo.clear();
        }
        
        if (mDrawUserLayerManager != null)
        {
        	mDrawUserLayerManager.unregistAllUserLayer();
        }
    }
/*    
    // ユーザアイコン用のレイヤIDを生成する
    public long getUserIconLayerID()
    {
    	if (mUserIconLayerID <= -1)
    	{
    		JNILong userLayerID = new JNILong();
    	    NaviRun.GetNaviRunObj().JNI_NE_AddUserLayer(userLayerID);
    	    mUserIconLayerID = userLayerID.getLcount();
    	}
    	
    	return mUserIconLayerID;
    }
*/
// Add by CPJsunagawa '2015-08-08 End

    // 属性リストを追加する
    public void addInfoItem(DeliveryInfo info) {
        if( mDeliveryInfo == null) { // fail safe
            mDeliveryInfo = new ArrayList<DeliveryInfo>();
        }
        info.mMyIndex = mDeliveryInfo.size(); // 追加前のサイズが自分自身のインデックス
        mDeliveryInfo.add(info);
        
        // アイコンを描画する
        //String sakuraIconID = Constants.SAKURA_ICON_ID;
        DrawUserLayer dul = mDrawUserLayerManager.getUserLayer(Constants.SAKURA_ICON_ID);
        if (dul == null)
        {
        	dul = new DrawUserLayer(Constants.SAKURA_ICON_ID);
        	mDrawUserLayerManager.registUserLayer(dul);
        }
        
        //info.reflectToIcon();
        if (info.mMapIcon != null) dul.registDrawUserPoi(info.mMapIcon);
    }

// Add by CPJsunagawa '2015-08-08 Start
    // 図形情報リストを追加する
    public void addUserIconInfoItem(UserIconInfo info) {
        if( mUserIconInfo == null) { // fail safe
            mUserIconInfo = new ArrayList<UserIconInfo>();
        }
        //info.mID = mUserIconInfo.size(); // 追加前のサイズが自分自身のインデックス
        mUserIconInfo.add(info);
    }
    
    // 図形情報リストの地点データを取得する
    public UserIconInfo getUserIconInfo(int index) {
        if( index < 0 || index >= mUserIconInfo.size()){ // fail safe
        }
        return mUserIconInfo.get(index);
    }
    
    // 図形情報リストの地点データを削除する
    public void removeUserIconInfo(int index) {
        if( index < 0 || index >= mUserIconInfo.size()){ // fail safe
        }
        mUserIconInfo.remove(index);
    }
    
    // 図形情報リストのレコード数を返却する
    public int getUserIconInfoCount() {

    	return mUserIconInfo != null ? (int)mUserIconInfo.size() : 0;
    }
    
    public void clearUserIconInfo() {
    	if( mUserIconInfo != null) mUserIconInfo.clear();
    	if( mUserLineInfo != null) mUserLineInfo.clear();
    }
    
// Add by CPJsunagawa '2015-08-08 End
    
    // 線情報
    // 線情報リストを追加する
    public void addUserLineInfoItem(UserLineInfo info) {
        if( mUserLineInfo == null) { // fail safe
        	mUserLineInfo = new ArrayList<UserLineInfo>();
        }
        //info.mID = mUserIconInfo.size(); // 追加前のサイズが自分自身のインデックス
        mUserLineInfo.add(info);
    }
    
    // 線情報リストの地点データを取得する
    public UserLineInfo getUserLineInfo(int index) {
        if( index < 0 || index >= mUserLineInfo.size()){ // fail safe
        }
        return mUserLineInfo.get(index);
    }
    
    // 線情報リストの地点データを削除する
    public void removeUserLineInfo(int index) {
        if( index < 0 || index >= mUserLineInfo.size()){ // fail safe
        }
        mUserLineInfo.remove(index);
    }
    
    // 線情報リストのレコード数を返却する
    public int getUserLineInfoCount() {

    	return mUserLineInfo != null ? (int)mUserLineInfo.size() : 0;
    }
    
    // 目下描画中の線情報
    public UserLineInfo getDrawingUserLineInfo(){
    	if (mDrawingUserLineInfo == null) mDrawingUserLineInfo = new UserLineInfo();
    	return mDrawingUserLineInfo;
    }
    
    // 目下描画中の線情報を確定する。
    public UserLineInfo commitDrawingUserLineInfo(){
    	if (mDrawingUserLineInfo == null) return null;
    	if (mDrawingUserLineInfo.mFigureLine == null) return null;
    	if (mDrawingUserLineInfo.mFigureLine.getNumberOfPoints() <= 1) return null;
    	
    	addUserLineInfoItem(mDrawingUserLineInfo);
    	UserLineInfo decidedULI = mDrawingUserLineInfo;
    	rollbackDrawingUserLineInfo();
    	
    	return decidedULI;
    }
    
    // 目下描画中の線情報をキャンセルする。
    public void rollbackDrawingUserLineInfo(){
    	mDrawingUserLineInfo = null;
    }
    
    
    // 属性リストの地点データを取得する
    public DeliveryInfo getInfo(int index) {
        if( index < 0 || index >= mDeliveryInfo.size()){ // fail safe
        }
        return mDeliveryInfo.get(index);
    }
    
    // 属性リストのサイズを取得する
    public int getInfoListSize() {
        if( mDeliveryInfo == null) { // fail safe
            mDeliveryInfo = new ArrayList<DeliveryInfo>();
        }
        return mDeliveryInfo.size();
    }
    
    // 属性リストの存在有無を取得する
    public boolean isExistInfoList() {
    	return (getInfoListSize() >= 1);
    }
    
    public boolean isExistUserInfoList() {
    	return (getUserIconInfoCount() >= 1);
    }

    // 属性リストのページ数を取得する。
    public int getInfoListPageCount() {
    	int listSize = getInfoListSize();
    	return (listSize - 1) / ONE_PAGE_ITEM_COUNT + 1;
    }
    
    // コース名を設定する
    public void setSelectedCourseName(String courseName)
    {
    	mSelectedCourseName = courseName;
    }
    
    // コース名を取得する
    public String getSelectedCourseName()
    {
    	return mSelectedCourseName;
    }
    
    /**
     * クローズアップする地点を設定する。
     * @param lat
     * @param lon
     */
    public void setCloseUpPoint(long lat, long lon)
    {
    	mCloseUpLatitude = lat;
    	mCloseUpLongitude = lon;
    }
    
    /**
     * クローズアップする地点をクリアする。
     */
    public void clearCloseUpPoint()
    {
    	setCloseUpPoint(0, 0);
    }
    
    /**
     * クローズアップする地点の緯度を求める。
     * @return
     */
    public long getCloseUpLatitude(){return mCloseUpLatitude;}
    
    /**
     * クローズアップする地点の経度を求める。
     * @return
     */
    public long getCloseUpLongitude(){return mCloseUpLongitude;}
    
/*    
    // 配達済みの数を取得する
    public int getDeliveredCount() {
    	int result = 0;
    	for (int i = 0; i <= getInfoListSize() - 1; i++)
    	{
    		if (mDeliveryInfo.get(i).mDeliveryStatus == DeliveryInfo.DELIVERY_STATUS_DELIVERED)
    		{
    			result++;
    		}
    	}
    	return result;
    }
    
    // 未配達の数を取得する。
    public int getNotDeliveredCount() {
    	int result = 0;
    	for (int i = 0; i <= getInfoListSize() - 1; i++)
    	{
    		if (mDeliveryInfo.get(i).mDeliveryStatus == DeliveryInfo.DELIVERY_STATUS_NOTDELIVERED)
    		{
    			result++;
    		}
    	}
    	return result;
    }
    
    // 配達済みの数 / 全数
    public String getDisplayOfDeliveredPerAll()
    {
    	int total = getInfoListSize();
    	if (total == 0) return "";
    	return "" + getDeliveredCount() + " / " + total;
    }
    
    // 未配達の数 / 全数
    public String getDisplayOfNotDeliveredPerAll()
    {
    	int total = getInfoListSize();
    	if (total == 0) return "";
    	return "" + getDeliveredCount() + " / " + total;
    }
*/
    /**
     * 配達数の格納
     * @author User
     *
     */
    class DeliveryCounts
    {
    	/** 合計 */
    	int total;
    	
    	/** 未配達 */
    	int noDelivery;
    	
    	/** 配達完了 */
    	int delivered;
    	
    	/** 配達中止 */
    	int aborted;
    	
    	/** 保留 */
    	int reserved;
    	
    	DeliveryCounts()
    	{
    		total = 0;
    		noDelivery = 0;
    		delivered = 0;
    		aborted = 0;
    		reserved = 0;
    	}
    }
    
    /**
     * 配達先に関する各種数を取得する。
     */
    DeliveryCounts getCounts()
    {
    	DeliveryCounts result = new DeliveryCounts();
    	result.total = getInfoListSize();
    	
    	for (int i = 0; i <= result.total - 1; i++)
    	{
    		switch (mDeliveryInfo.get(i).mDeliveryStatus)
    		{
    		case DeliveryInfo.DELIVERY_STATUS_NOTDELIVERED:
    			result.noDelivery++;
    			break;
    		case DeliveryInfo.DELIVERY_STATUS_DELIVERED:
    			result.delivered++;
    			break;
    		case DeliveryInfo.DELIVERY_STATUS_ABORTED:
    			result.aborted++;
    			break;
    		case DeliveryInfo.DELIVERY_STATUS_RESERVED:
    			result.reserved++;
    			break;
    		}
    	}
    	
    	return result;
    }
    
    /**
     * 数に関するキャプションを取得する。
     * @return
     */
    public String getCountCaption()
    {
    	DeliveryCounts dc = getCounts();
    	if (dc.total == 0) return "";
    	
    	String result = "" + dc.delivered + " / " + dc.total;
/*    	
    	if (dc.aborted >= 1 || dc.reserved >= 1)
    	{
    		result += "\n" + "R" + dc.reserved + " A" + dc.aborted;
    	}
*/
    	if (dc.reserved >= 1)
    	{
    		result += "\n" + "保留 " + dc.reserved;
    	}
    	
    	if (dc.aborted >= 1)
    	{
    		result += "\n" + "中止 " + dc.aborted;
    	}
    	
    	return result;
    }
    
    /**
     * 最初の未配達インデックスを取得する<br>但し、存在しないときは-1
     * @return
     */
    public int getFirstNoDeliveryIndex()
    {
    	DeliveryInfo oneItem;
    	for (int i = 0; i <= getInfoListSize() - 1; i++)
    	{
    		oneItem = mDeliveryInfo.get(i);
    		if (oneItem.mDeliveryStatus == DeliveryInfo.DELIVERY_STATUS_NOTDELIVERED)
    		{
    			return i;
    		}
    	}
    	
    	return -1;
    }
    
    /**
     * 最初の保留インデックスを取得する<br>但し、存在しないときは-1
     * @return
     */
    public int getFirstReservedIndex()
    {
    	DeliveryInfo oneItem;
    	for (int i = 0; i <= getInfoListSize() - 1; i++)
    	{
    		oneItem = mDeliveryInfo.get(i);
    		if (oneItem.mDeliveryStatus == DeliveryInfo.DELIVERY_STATUS_RESERVED)
    		{
    			return i;
    		}
    	}
    	
    	return -1;
    }
    
    /**
     * インデックスから、該当ページ番号を求める。
     * @param index
     * @return
     */
    public int getPageFromIndex(int index)
    {
    	return index / ONE_PAGE_ITEM_COUNT;
    }
    
    /**
     * 未配達の最初の頁を取得する。
     * @return
     */
    public int getFirstNoDelivPage()
    {
    	int fnd = getFirstNoDeliveryIndex();
    	return (fnd >= 0) ? getPageFromIndex(fnd) : -1;
    }
    
/*    
    // タップ順ルートに地点を追加する
    public void addTapOrderPoint(RouteNodeData node) {
        if( mRouteNodeList == null) { // fail safe
            mRouteNodeList = new ArrayList<RouteNodeData>();
        }
        mRouteNodeList.add(node);
    }
*/    

/*    
    // タップ順ルートリストの地点データを取得する
    public RouteNodeData getTapOrderInfo(int index) {
        if( index < 0 || index >= mRouteNodeList.size()){ // fail safe
            return null;
        }
        return mRouteNodeList.get(index);
    }
    
    // タップ順ルートリストのサイズを取得する
    public int getTapOrderListSize() {
        if( mRouteNodeList == null) { // fail safe
            mRouteNodeList = new ArrayList<RouteNodeData>();
        }
        return mRouteNodeList.size();
    }
*/    

    // タップ順ルートでの選択範囲の先頭位置設定
/*    
    public void setTapRouteStartPos(int pos) {
        if( pos >= mRouteNodeList.size() || pos < 0) { // fail safe
            return ;
        }
        curRouteStartPos = pos;
    }
*/    
     
    // タップ順ルートでの選択範囲の先頭位置取得
    public int getTapRouteStartPos() {
        //return curRouteStartPos;
    	return getRouteStartIndex();
    }
   
    
    // 前へボタンを有効とするか否か
    public boolean isEnableBackwardBtn() {
        //if( curRouteStartPos > 0 ) {
    	if ( mCurRoutePage > FIRST_ONLY_ROUTE_FLAG ) {
            return true;
        } else {
            return false;
        }
    }
     
    // 次へボタンを有効とするか否か
    public boolean isEnableForwardBtn() {
    	if (mRouteNodeList == null) return false;
    	int routeNodeCnt = getRouteListSize();
    	if (routeNodeCnt <= 2) return false; // 現在地しかなければfalse
        //if( curRouteStartPos + 6 < mRouteNodeList.size() -1) {
    	if ( mCurRoutePage < getRoutePageCount() - 1 ) {
            return true;
        } else {
            return false;
        }
    }
// Add by CPJsunagawa'13-12-25 End

// Add CPJ 2015-06-07 Start
    private ItsmoNaviDriveExternalApi mExtApi = null;
    
    public ItsmoNaviDriveExternalApi getExtApi() {
        return mExtApi;
    }
// Add CPJ 2015-06-07 End

// Add CPJ 2015-07-15 Start
    private ItsmoNaviDriveExternalApi api;
    
    public ItsmoNaviDriveExternalApi getApi() {
        return api;
    }
// Add CPJ 2015-07-15 End


    /**
      * @return
      */
     public NaviApiBroadcastSender getNaviApiBroadcastSender() {
         return mNaviApiBroadcastSender;
     }

    @Override
    public void onCreate() {
        super.onCreate();

// Add 2011/09/06 sawada Start [ExternalAPI] -->
        mNaviApiBroadcastSender = new NaviApiBroadcastSender(this);
// Add 2011/09/06 sawada End [ExternalAPI] -->
        mBaseActivibyStack = new BaseActivityStack();
        splashTime = System.currentTimeMillis();

        NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviApplication::onCreate");

        NaviRun.setApplication(this);

        executor = Executors.newSingleThreadExecutor();
    }

    @Override
    public void onTerminate() {

        mBaseActivibyStack = null;
// Add 2011/09/06 sawada [AgentHMI] Start -->
        mNaviApiBroadcastSender = null;
// Add 2011/09/06 sawada [AgentHMI] End -->
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviApplication::onTerminate");

        executor.shutdown();

        super.onTerminate();
    }

    /*
     * ナビアプリの初期処理
     */
    public void initializeNaviApp(ItsmoNaviDrive mainActivity) {
        mMainActivity = mainActivity;

        mNaviCtrlReceiver = new NaviControllReceiver();
        mNaviCtrlReceiver.setAppInstance(this);
        mNaviCtrlReceiver.reg(getApplicationContext());

        mapIntent = new Intent();
        mapIntent.setClass(mainActivity, MapActivity.class);
        mapIntent.setAction(mainActivity.getIntent().getAction());
        Uri data = mainActivity.getIntent().getData();
        if (data != null) {
            mapIntent.setData(data);
        }
        Bundle bundle = mainActivity.getIntent().getExtras();
        if (bundle != null) {
            mapIntent.putExtras(bundle);
        }
//Del 2011/10/06 Z01yoneya Start -->
//        mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//Del 2011/10/06 Z01yoneya End <--

        String sdStoragePath = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME);
        if (sdStoragePath == null || sdStoragePath.length() == 0) {
            throw new IllegalStateException("map data not found.");
        }
        mNaviAppDataPath = new NaviAppDataPath();
        try {
            mNaviAppDataPath.setNaviAppRootPath(sdStoragePath);
        } catch (IllegalStateException e) {
            //パスを設定済み(ローカルデータ使用)の場合。とくに何もしない。
        }
// ADD.2013/03/25 N.Sasao Android4.2 以降 マルチユーザー対応 START
        try {
            mNaviAppDataPath.setNaviAppDataPath( this.getFilesDir().toString() );
        } catch (IllegalStateException e) {
            //パスを設定済み(ローカルデータ使用)の場合。とくに何もしない。
        }
// ADD.2013/03/25 N.Sasao Android4.2 以降 マルチユーザー対応  END
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 車載器連携設定追加 Start -->
        DrivingRegulation.Init(getApplicationContext());
// ADD 2013.08.08 M.Honma 走行規制 車載器連携設定追加 End -->
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
//// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 Start -->
//        // ClarionService初期化
//        ClarionService.GetInstance().Init(getApplicationContext());
//// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 End <--
    }

    /*
     * ナビアプリの終了処理
     */
    public void finalizeNaviApp() {
//Add 2011/11/16 Z01_h_yamada Start -->
        if ( mMediaCheckTimer != null ) {
        	mMediaCheckTimer.stop();
        	mMediaCheckTimer = null;
        }
//Add 2011/11/16 Z01_h_yamada End <--

// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 Start -->
        DrivingRegulation.UnBind();
// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
//MOD.2014.01.06 N.Sasao クラッシュレポート対応 START
//ADD.2013.09.19 N.Sasao 外部連携固定パス変更・後処理対応実装 START
        if( mNaviAppDataPath != null ){
        	String path = mNaviAppDataPath.getExternalDataPath();
        	if( path != null && path.length() > 0){
        		DrawIconPathManager.deleteTmpPath( path );
        	}
        }
//ADD.2013.09.19 N.Sasao 外部連携固定パス変更・後処理対応実装  END
//MOD.2014.01.06 N.Sasao クラッシュレポート対応  END
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
        DrivingRegulation.Finalize();
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
    }

    /*
     * Activityスタッククラスを取得する
     */
    public BaseActivityStack getBaseActivityStack() {
        return mBaseActivibyStack;
    }

//Add 2011/11/16 Z01_h_yamada Start -->
    public void sendShowDialog(int id) {
        try {
        	if ( mBaseActivibyStack != null ) {
				BaseActivity act = mBaseActivibyStack.getTopActivity();
	    		if ( act != null ) {
	    			boolean ret = act.RequestShowDialog(id);
	    			if ( ret == true && mMediaCheckTimer != null ) {
	    				mMediaCheckTimer.stop();
	    			}
	    		}
        	}
        } catch (Exception e) {
        }
    }
//Add 2011/11/16 Z01_h_yamada End <--

    /*
     * メインアクティビティ終了
     */
    public void finishMainActivity() {
        try {
            NaviLog.i(NaviLog.PRINT_LOG_TAG, "NaviApplication::finishMainActivity()");
            mMainActivity.finish();
        } catch (NullPointerException e) {
            //とくに何もしない
        }
    }

//Del 2011/10/12 Z01yoneya Start -->
//        public void foregroundSplashWindow() {
//        if (mMainActivity != null) {
//            mMainActivity.showSplash();
//        }
//    }
//Del 2011/10/12 Z01yoneya End <--

    /*
     * ナビ起動サービスを停止する。
     */
    public void stopNaviService() {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviApplication::stopNaviService()");

        try {
            mNaviCtrlReceiver.unreg(getApplicationContext());
            stopNaviLaunchService();
            finishMainActivity();
        } catch (NullPointerException e) {
            //とくになにもしない
        }

        onTerminate();
        NaviLog.v(NaviLog.PRINT_LOG_TAG, "finish navigation system.");
    }

    /*
     * ナビ起動サービス開始
     *
     * @param int[] idList 地図ダウンロードコンテンツIDリスト
     */
    public ComponentName startNaviLaunchService(int[] idList) {
        Context context = getApplicationContext();
        Intent intent = new Intent(context, NaviLaunchService.class);
////Add 2011/10/05 Z01yoneya Start -->
//        if (idList != null && idList.length > 0) {
//            intent.putExtra(ExtraName.DL_CONTENTS, idList);
//        }
////Add 2011/10/05 Z01yoneya End <--
//Add 2011/11/23 Z01_h_yamada Start -->
        CommonLib.setStopServiceFlag(false);
//Add 2011/11/23 Z01_h_yamada End <--
        
// Add CPJ 2015-06-07 Start
        // ItsmoNaviDriveExternalApiに接続する
        mExtApi = ItsmoNaviDriveExternalApi.getInstance();
        mExtApi.connect(context);
        setStarted(true);
// Add CPJ 2015-06-07 End

// Add CPJ 2015-07-15 Start
        // ItsmoNaviDriveExternalApiに接続する
        api = ItsmoNaviDriveExternalApi.getInstance();
// Add CPJ 2015-07-15 End

        return context.startService(intent);
    }

    /*
     * ナビ起動サービス停止
     */
    private boolean stopNaviLaunchService() {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviApplication::stopNaviLaunchService()");
//Add 2011/11/23 Z01_h_yamada Start -->
        CommonLib.setStopServiceFlag(true);
//Add 2011/11/23 Z01_h_yamada End <--
        Context context = getApplicationContext();
        return context.stopService(new Intent(context, NaviLaunchService.class));
    }

    /*
     * ナビアプリデータパスクラスを取得する
     */
    public NaviAppDataPath getNaviAppDataPath() {
        return mNaviAppDataPath;
    }

//Add 2011/09/24 Z01yoneya Start -->

    private static String naviResFilePath;
    private final static int ibuffersz = 10 * 1024;
    private boolean existsResFile = false;
    private boolean isCopyResFile = false;

    private Intent mapIntent;

    private long splashTime = 0;
    private final static long MIN_SPLASH_SHOW_TIME = 500;
// ADD.2013.11.20 N.Sasao 起動速度改善 START
	private boolean mSplashTimeControl = false;
// ADD.2013.11.20 N.Sasao 起動速度改善  END

    public void initExConfig() {
        AssetManager as = getResources().getAssets();
        String filename = "ExConfig.ini";

        InputStream is = null;
        BufferedReader br = null;

        try {
            is = as.open(filename);
            br = new BufferedReader(new InputStreamReader(is));
            if (br != null) {
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    if (line.startsWith("[Config]")) {
                        int count = Integer.parseInt(line.trim().split(" ")[1]);
                        if (count <= 0) {
                            continue;
                        }

                        int index = 0;
                        while (index < count) {
                            line = br.readLine();
                            if (line != null) {
                                if (line.startsWith("Time")) {
                                    Constants.times = Integer.parseInt(line.split("=")[1].trim());
                                }
                                if (line.startsWith("Flag")) {
                                    Constants.flag = Integer.parseInt(line.split("=")[1].trim());
                                }
                                if (line.startsWith("getJNICount")) {
                                    Constants.GET_JNI_COUNT = Integer.parseInt(line.split("=")[1].trim());
                                }
                                if (line.startsWith("vp_speed")) {
                                    Constants.vp_speed = Integer.parseInt(line.split("=")[1].trim());
                                }
                            }
                            index++;
                        }
                    }
                    if (line != null && line.startsWith("[Mail]")) {
                        line = br.readLine();
                        if (line != null) {
                            // 件名
                            Constants.mail_title = line.split("=")[1];
                        }

                        line = br.readLine();
                        if (line != null) {
                            // メール本文
                            Constants.mail_Content = line.split("=")[1];
                        }
                    }
//@@MOD-START BB-0006 2012/12/07 Y.Hayashida
                    if (line != null && line.startsWith("[Widget]")) {
                        for(int idx = 0 ; idx < 2 ; idx++ ){
                            line = br.readLine();
                            if (line != null) {
                                if (line.startsWith("bShow")) {
                                    mShowWidgetRight = mShowWidgetTop = (Integer.parseInt(line.split("=")[1].trim()) == 1);
                                    break;
                                }
                                if (line.startsWith("bShowTop")) {
                                    mShowWidgetTop = (Integer.parseInt(line.split("=")[1].trim()) == 1);
                                }
                                if (line.startsWith("bShowRight")) {
                                    mShowWidgetRight = (Integer.parseInt(line.split("=")[1].trim()) == 1);
                                }
                            }
                        }
                    }
//@@MOD-END BB-0006 2012/12/07 Y.Hayashida
                    if (line != null && line.startsWith("[Probe]")) {
                        line = br.readLine();
                        if (line != null && line.startsWith("operation")) {
                            String data = line.split("=")[1].trim();
                            if ("on".equals(data) || "ON".equals(data) || "On".equals(data)) {
                                mProbeFlag = true;
                            }
                        }
                    }
                    // V2.5 外部Icon とDR ProFile 指定を追加する
                    if(line != null)				// 他のifでreadしている可能性がるのでチェック
                    {
                    	// 指定キーがあれば連続行の処理を行う
                    	// 外部Icon
                    	if(line.startsWith("[MapIcon]")) {
                    		// 続く行は dir= , name= のみ　順番違いは以下のない場合扱い
                    		// パスあり／なしは、"dir"があるか、ないかで判断
                    		mMapIcon_ON = true;
                    		NaviRun.SetisMapIconKeyRead(mMapIcon_ON);
                    		mcsvname = "DrawParameter.csv";		// name= がない場合用
                    		mcsvdirpath = "";									// dir= がない場合用
                    		line = br.readLine();
                    		if(line != null) {
                    			if(line.startsWith("dir")) {
                       				mcsvdirpath = line.split("=")[1];
                       				NaviRun.SetMapIcon_Extpath(mcsvdirpath);
                       				line = br.readLine();
                    			}
                    			if(line != null) {
                    				if(line.startsWith("name")) {
                     					mcsvname = "";
                    					mcsvname = line.split("=")[1];
                    					NaviRun.SetMapIcon_Extname(mcsvname);
                    				}
                    			}
                    		}
                    	//DR ProFile
                    	}
                    	else if(line.startsWith("[DR]")) {
                    		mDR_ON = true;
                    		NaviRun.setDRon(mDR_ON);			//MLocationListener
                    		mDR_flag = false;
                    		mprofilename = "";
                    		mdrdirpath = "";
                    		line = br.readLine();
                    		if(line != null) {
                    			if(line.startsWith("operation")) {
                    				String data = line.split("=")[1].trim();
                    				if ("on".equals(data) ) {
                    					mDR_flag = true;
                    				}
                    				NaviRun.setoperation(mDR_flag);			//MLocationListener
                       				line = br.readLine();
                    			}
                    			if(line != null) {
	                    			if(line.startsWith("dir")) {
	                       				mdrdirpath = line.split("=")[1];
	                       				NaviRun.setprofilepath(mdrdirpath);
	                       				line = br.readLine();
	                    			}
                    			}
                    			if(line != null) {
                    				if(line.startsWith("name")) {
                    					mprofilename = line.split("=")[1];
                    					NaviRun.setprofilename(mprofilename);
                    				}
                    			}
                    		}
                    	//地図高解像度表示 ON/OFF(デフォルトON)
                    	}
                    	else if(line.startsWith("[MapHighResolution]")) {
                    		line = br.readLine();
                    		if(line != null) {
                    			if(line.startsWith("operation")) {
                    				String data = line.split("=")[1].trim();
                    				if ("off".equals(data) ) {
                    					mMap_HighResolution_OFF = true;
// MOD.2013.07.23 N.Sasao 高解像度切り替え対応 START
                    					NaviRun.GetNaviRunObj().setMap_HighResolution_OFF(mMap_HighResolution_OFF);			//高解像度表示しない
// MOD.2013.07.23 N.Sasao 高解像度切り替え対応  END
                    				}
                    			}
                    		}
                    	}
                        else if(line.startsWith("[SimulationMode]")) {
                            line = br.readLine();
                            if(line != null) {
                                if(line.startsWith("showButton")) {
                                    boolean isShowButton = false;
                                    String data = line.split("=")[1].trim();
                                    if ("on".equals(data) || "true".equals(data) || "1".equals(data)) {
                                        isShowButton = true;
                                    }
                                    NaviRun.GetNaviRunObj().setShowSimulationButton(isShowButton);
                                    line = br.readLine();
                                }
                            }
                            if(line != null) {
                                if(line.startsWith("loop")) {
                                    boolean loopFlag = false;
                                    String data = line.split("=")[1].trim();
                                    if ("on".equals(data) || "true".equals(data) || "1".equals(data)) {
                                        loopFlag = true;
                                    }
                                    NaviRun.GetNaviRunObj().setSimulationLoopMode(loopFlag);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }
        }

        // Test
        // if(Constants.times != null){
        // for(int i = 0;i<Constants.times.length;i++){
        // NaviLog.d(NaviLog.PRINT_LOG_TAG,"Time" + i +"=" + Constants.times[i]);
        // }
        // }
        // NaviLog.d(NaviLog.PRINT_LOG_TAG,"title=" + Constants.mail_title);
        // NaviLog.d(NaviLog.PRINT_LOG_TAG,"content=" + Constants.mail_Content);
    }

    public void startNormal() {
//        DownloadMapActivity.setAppLaunchedFlag();
        initApp();
    }

//Chg 2011/09/26 Z01yoneya Start -->
//    private void initApp() {
//        // identify Navigation Started
//        CommonLib.setBIsNaviStart(true);
//        String[] tmpDataPath = judgeDataExist();
//
//        String[] strPath = tmpDataPath[7].split(" ");
//
//        for (int i = 0; i < tmpDataPath.length; i++) {
//            NaviLog.d("", "yoneya path=" + tmpDataPath[i]);
//        }
//
//        naviResFilePath = strPath[2] + "NaviResFile.txt";
//
//        existsResFile = CommonLib.fileExists(naviResFilePath);
//
//        CommonLib.setNaviResFilePath(tmpDataPath);
//
//        IsCopyResFile(tmpDataPath);
//        intentBroadCastForNaviStart();
//
//        if (isCopyResFile) {
//            copyVehiclePictureToFiles();
//            createResFile(tmpDataPath);
//        }
//        // Ini ファイルをコピーする
//        copyIniFiles();
//
//        CommonLib.setBIsNaviStart(true);
//
//        long delay = System.currentTimeMillis() - splashTime;
//
//        if (delay <= MIN_SPLASH_SHOW_TIME) {
//            delay = MIN_SPLASH_SHOW_TIME - delay;
//        } else {
//            delay = 0;
//        }
//        new Handler().postDelayed(new StartActivityTask(mapIntent), delay);
//    }
//------------------------------------------------------------------------------------------------------------------------------------
    private void initApp() {
    	NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviApplication::initApp");

//Del 2012/02/23 Z01_h_yamada Start --> #3790
//    	// identify Navigation Started
//        CommonLib.setBIsNaviStart(true);
//Del 2012/02/23 Z01_h_yamada End <--

//Add 2011/11/16 Z01_h_yamada Start -->
        if ( mMediaCheckTimer == null ) {
        	mMediaCheckTimer = new MediaCheckTimer(getApplicationContext(), mNaviAppDataPath.getNaviAppRootPath());
        	mMediaCheckTimer.start( 0, MEDIA_SIZE_CHECK_INTERVAL );
        }
//Add 2011/11/16 Z01_h_yamada End <--

        naviResFilePath = mNaviAppDataPath.getAssetFilePath() + "NaviResFile.txt";

        existsResFile = CommonLib.fileExists(naviResFilePath);

        IsCopyResFile(mNaviAppDataPath.getNaviDataResFileVerStr());
        intentBroadCastForNaviStart();

        copyVehiclePictureToFiles();
        copyDrawParamFile();
        copyDRprofile();
        if (isCopyResFile) {
//            copyVehiclePictureToFiles();
            createResFile(mNaviAppDataPath.getNaviDataResFileVerStr());
        }
        // Ini ファイルをコピーする
        copyIniFiles();

//Del 2012/02/23 Z01_h_yamada Start --> #3790
//        CommonLib.setBIsNaviStart(true);
//Del 2012/02/23 Z01_h_yamada End <--

        long delay = System.currentTimeMillis() - splashTime;
// MOD.2013.11.20 N.Sasao 起動速度改善 START
		if( mSplashTimeControl ){
	        if (delay <= MIN_SPLASH_SHOW_TIME) {
	            delay = MIN_SPLASH_SHOW_TIME - delay;
	        } else {
	            delay = 0;
	        }
	 	}
	 	else{
	 		delay = 0;
	 	}
        new Handler().postDelayed(new StartActivityTask(mMainActivity, mapIntent), delay);
// MOD.2013.11.20 N.Sasao 起動速度改善  END
    }

//Chg 2011/09/26 Z01yoneya End <--

    /**
     * ナビブロードバンド
     */
    private void intentBroadCastForNaviStart() {
        // When Navigation Start,Broadcast this Intent to another application
        Intent intentBroadCast = new Intent();
        intentBroadCast.setAction(Constants.INTENT_NAVISTART);
        intentBroadCast.putExtra(Constants.INTENT_NAVISTART_EXTRA,
                Constants.NAVI_START_INTENT_BROADCAST);
        sendBroadcast(intentBroadCast);
    }

    /**
     * 資源のファイルを着いてディレクトリを指定することに複製します
     */
    private void copyVehiclePictureToFiles() {
        AssetManager as = getResources().getAssets();
        String filename = "Common-Icon.png";
        String csvFileName = "Common-Icon.csv";
        InputStream is = null;
        FileOutputStream os = null;
        InputStream fileInput = null;
        FileOutputStream fileOutput = null;

        try {
            is = as.open(filename);
            os = openFileOutput(filename, Context.MODE_WORLD_READABLE);

            fileInput = as.open(csvFileName);
            fileOutput = openFileOutput(csvFileName, Context.MODE_WORLD_READABLE);

            // copy
            byte[] buf = new byte[ibuffersz];
            int ret = -1;
            while ((ret = is.read(buf, 0, ibuffersz)) != -1) {
                os.write(buf, 0, ret);
            }

            while ((ret = fileInput.read(buf, 0, ibuffersz)) != -1) {
                fileOutput.write(buf, 0, ret);
            }

        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }
            if (fileOutput != null) {
                try {
                    fileOutput.close();
                } catch (IOException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }
            if (fileInput != null) {
                try {
                    fileInput.close();
                } catch (IOException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }

        }
    }

    /**
     * 資源のファイルを着いてディレクトリを指定することに複製します
     */
    private void copyIniFiles() {
        AssetManager as = getResources().getAssets();

        InputStream IniFileInput = null;
        FileOutputStream IniFileOutput = null;
        String IniFileName = "its-moNaviPND.ini";

        try {
            IniFileInput = as.open(IniFileName);
            IniFileOutput = openFileOutput(IniFileName, Context.MODE_WORLD_READABLE);
//Add 2011/07/25 Z01thedoanh Start -->screen設定
            String its_moNaviPNDOption[] = null;
//Add 2011/07/25 Z01thedoanh End <--
            // copy
            byte[] buf = new byte[ibuffersz];
            int ret = -1;
            while ((ret = IniFileInput.read(buf, 0, ibuffersz)) != -1) {
                IniFileOutput.write(buf, 0, ret);
                String xx = new String(buf, "MS932");
                its_moNaviPNDOption = xx.split("\r\n");
                if (its_moNaviPNDOption != null) {
                    for (int i = 0; i < its_moNaviPNDOption.length; i++) {
                        if (its_moNaviPNDOption[i].equals("[Screen]")) {
                            int bFlag = Integer.valueOf(its_moNaviPNDOption[i + 1].split("=")[1]);
                            CommonLib.setIsFullScreeen(bFlag);
                            CommonLib.screenWidth = Integer.valueOf(its_moNaviPNDOption[i + 2].split("=")[1]);
                            CommonLib.screenHeight = Integer.valueOf(its_moNaviPNDOption[i + 3].split("=")[1]);
                            break;
                        }
                    }
                }
            }
        } catch (IOException e1) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e1);
        } finally {
            if (IniFileOutput != null) {
                try {
                    IniFileOutput.close();
                } catch (IOException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }
            if (IniFileInput != null) {
                try {
                    IniFileInput.close();
                } catch (IOException e) {
        			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }

        }
    }

    /**
     * 資源のファイルを複製するかどうかを判断する
     *
     * @param tmpDataPath
     */
    private void IsCopyResFile(String naviDataResFileVerStr) {
        if (existsResFile) {
            FileReader fr = null;
            BufferedReader bf = null;
            try {
                fr = new FileReader(naviResFilePath);
                bf = new BufferedReader(fr);

                String str = null;
                str = bf.readLine();
                if (str != null && str.equals(naviDataResFileVerStr)) {
                    isCopyResFile = false;
                } else {
                    isCopyResFile = true;
                }
            } catch (IOException e) {
    			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            } finally {
                if (bf != null) {
                    try {
                        bf.close();
                    } catch (IOException e) {
            			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                    }
                }
                if (fr != null) {
                    try {
                        fr.close();
                    } catch (IOException e) {
            			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                    }
                }
            }
        } else {
            isCopyResFile = true;
        }
    }


    private void copyDrawParamFile() {
        String pathName = mNaviAppDataPath.getAssetFilePath() + NaviRun.GetMapIcon_Extpath();
        String csvFileName = NaviRun.GetMapIcon_Extname();
        String pngFileName = "DrawParameter.png";

        mkdir(pathName);

        File file = new File(pathName + csvFileName);
        if (!file.exists()) {
            copyAssetsFile(csvFileName, pathName);
        }
        file = new File(pathName + pngFileName);
        if (!file.exists()) {
            copyAssetsFile(pngFileName, pathName);
        }
    }

    private void copyDRprofile() {
        String pathName = mNaviAppDataPath.getAssetFilePath() + NaviRun.getprofilepath();
        String fileName = NaviRun.getprofilename();

        File file = new File(pathName + fileName);
        if (file.exists()) {
            return;
        }

        mkdir(pathName);
        copyAssetsFile(fileName, pathName);
    }

    private void mkdir(String pathName) {
        File file = new File(pathName);
        if (!file.exists()) {
            file.mkdir();
        }
        else if (file.isFile()) {
            file.delete();
        }
    }

    private void copyAssetsFile(String fileName, String pathName) {
        AssetManager assetManager = getResources().getAssets();

        InputStream inputStream = null;
        FileOutputStream outputStream = null;
        try {
            inputStream = assetManager.open(fileName);
            outputStream = new FileOutputStream(pathName + fileName);

            int ret = -1;
            byte[] buff = new byte[ibuffersz];
            while ((ret = inputStream.read(buff, 0, ibuffersz)) != -1) {
                outputStream.write(buff, 0, ret);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 資源のファイルを創建します
     *
     * @param tmpDataPath
     */
    private void createResFile(String naviDataResFileVerStr) {
        FileWriter fw = null;
        FileReader fr = null;
        BufferedReader bf = null;

        try {

            if (existsResFile) {

                fr = new FileReader(naviResFilePath);
                bf = new BufferedReader(fr);

                String str = bf.readLine();
                if (str != null && !str.equals(naviDataResFileVerStr)) {
                    File file = new File(naviResFilePath);

                    if (file.isFile() && file.exists()) {
                        boolean result = file.delete();
                        if (!result) {
                            NaviLog.e(NaviLog.PRINT_LOG_TAG, "file delete error[" + naviResFilePath + "]");
                        }
                    }

                    fw = new FileWriter(file, false);
                    fw.write(naviDataResFileVerStr);

                    fw.flush();
                }
            } else {
                File file = new File(naviResFilePath);

                if (file.isFile() && file.exists()) {
                    boolean result = file.delete();
                    if (!result) {
                        NaviLog.e(NaviLog.PRINT_LOG_TAG, "file delete error[" + naviResFilePath + "]");
                    }
                }

                fw = new FileWriter(file, false);
                fw.write(naviDataResFileVerStr);

                fw.flush();
                fw.close();
            }

        } catch (Exception e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } finally {
            try {
                if (bf != null) {
                    bf.close();
                }
            } catch (IOException e1) {
                NaviLog.e(NaviLog.PRINT_LOG_TAG, e1);
            }
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException e1) {
                NaviLog.e(NaviLog.PRINT_LOG_TAG, e1);
            }
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e1) {
    			NaviLog.e(NaviLog.PRINT_LOG_TAG, e1);
            }
        }
    }

    private static class StartActivityTask implements Runnable {
        private Intent intent;
        private ItsmoNaviDrive activity;

        public StartActivityTask(ItsmoNaviDrive activity, Intent intent) {
            this.intent = intent;
            this.activity = activity;
        }

        @Override
        public void run() {
            try {
                NaviActivityStarter.startActivity(activity, intent);
            } catch (NullPointerException e) {
                //地図アクティビティが起動できない場合は、アプリを終了する
            }
        }
    }
//Add 2011/09/24 Z01yoneya End <--

// Add 2011/10/11 r.itoh Start [ExternalAPI] -->
    public DrawUserLayerManager getDrawUserLayerManager() {
        return mDrawUserLayerManager;
    }
// Add 2011/10/11 r.itoh End [ExternalAPI] <--}

    public void setStarted(boolean started) {
        mStarted = started;
        if (mStarted) {
            getApplicationContext().sendBroadcast(new Intent(NaviIntent.ACTION_STARTED));
        } else {
            getApplicationContext().sendBroadcast(new Intent(NaviIntent.ACTION_STOPPED));
        }
    }

    public boolean isStarted() {
        return mStarted;
    }

//@@MOD-START BB-0003 2012/10/19 Y.Hayashida
    public boolean isShowTopWidget() {
        return mShowWidgetTop;
    }
    public boolean isShowRightWidget() {
        return mShowWidgetRight;
    }
    public boolean isShowWidget() {
        return mShowWidgetTop || mShowWidgetRight;
    }
//@@MOD-START BB-0003 2012/10/19 Y.Hayashida

    public boolean isProbeOperation() {
        return mProbeFlag;
    }

    public synchronized void enqueueExternalApiIntent(Intent intent) {
        mExternalApiIntentQueue.offer(intent);
    }
    public synchronized Intent dequeueExternalApiIntent() {
        return mExternalApiIntentQueue.poll();
    }
    
    public ExecutorService getExecutor() {
        return executor;
    }
    /**
     * 全クリア
     */
    public void allClearList()
    {
    	clearNowRoutingCustomerIndex();
    	initInfoList();
    	initProductList();
    	initRouteList();
    	clearCloseUpPoint();
    }
}
