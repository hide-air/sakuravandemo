/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNE_VehicleInfo_t.java
 * Description    出発地情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNE_VehicleInfo_t
{
    private int             iValid;
    private JNITwoLong      stPos;
    private JNITwoInt       stRoadType;
    /**
    * Created on 2010/08/06
    * Title:       getValid
    * Description:   有効な状態を取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getValid()
    {
        return iValid;
    }
    /**
    * Created on 2010/08/06
    * Title:       getstPos
    * Description:   出発地座標を取得する
    * @param1  無し
    * @return       JNITwoLong

    * @version        1.0
    */
    public JNITwoLong getstPos()
    {
        return stPos;
    }
    /**
    * Created on 2010/08/06
    * Title:       getRoadType
    * Description:   道路種別を取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public JNITwoInt getRoadType()
    {
        return stRoadType;
    }
}
