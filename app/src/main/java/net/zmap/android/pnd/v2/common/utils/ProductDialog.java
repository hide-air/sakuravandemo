
package net.zmap.android.pnd.v2.common.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.LinearLayout;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.NaviService;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.ItsmoNaviDriveExternalApi;
import net.zmap.android.pnd.v2.api.exception.NaviException;
import net.zmap.android.pnd.v2.api.map.ZoomLevel;
import net.zmap.android.pnd.v2.api.util.GeoUtils;
import net.zmap.android.pnd.v2.common.DeliveryInfo;
import net.zmap.android.pnd.v2.common.ProductInfo;
import net.zmap.android.pnd.v2.maps.MapActivity;

/**
 * メッセージとOK、キャンセル(デフォルトは非表示)ボタンを表示するダイアログ
 */
public class ProductDialog extends Dialog {

    //private Button mbtnOk;
    private Context mCtx;
    
    private Button mReturn;

    //private static ItsmoNaviDriveExternalApi oNaviApi;

/*    
    public static void InitializeInfoDialog(Context context)
    {
    	
    	//oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
    	//oNaviApi.connect(context);
    }
*/    

    //public InfoDialog(Context context, String address, String product, String date, int claim, int idx) {
    public ProductDialog(Context context) {
        super(context);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        // Add by M.Suna '13-09-03 start
//        NaviService oNavi = new NaviService();
        //ItsmoNaviDriveExternalApi oNaviApi = new ItsmoNaviDriveExternalApi();
        //ItsmoNaviDriveExternalApi oNaviApi = ItsmoNaviDriveExternalApi.getInstance();
        //GeoPoint point = new GeoPoint();
        //point.

/* cut by M.Suna 2013/10/20 Start
		int zoomLevel;
		zoomLevel = 1;
		try
		{
// Mod by M.Suna '13-09-13 Start
			// 拡大して表示
//			oNaviApi.setZoom(ZoomLevel.M_20);
			// 世界測地系BLを日本測地系に変換して中心に表示
//			oNaviApi.setCenter(GeoUtils.toWGS84(ProdInfo.mPoint.getLatitudeMs(), ProdInfo.mPoint.getLongitudeMs()));
// Mod by M.Suna '13-09-13 End
		}
		catch (NaviException e)
		{
			e.printStackTrace();
			//return;
		}
        // Add by M.Suna '13-09-03 End
 cut by M.Suna 2013/10/20 End  */

// 以下を顧客詳細情報画面に変えたい　by M.Suna '13-09-20
		// ダイアログレイアウト
//        LinearLayout dlgLayout = (LinearLayout) LayoutInflater.from(context).inflate(
//                R.layout.dlg_info, null);

        // ボタン周りの初期化
//        InitComponent(dlgLayout);
//        mCtx = context;

//        TextView tv = (TextView) dlgLayout.findViewById(R.id.tvAddress);
        //tv.setText(address);
//        tv.setText(delivInfo.mAddress);
//        tv = (TextView) dlgLayout.findViewById(R.id.tvCode);
        //tv.setText("宅配商品：" + product );
//        tv.setText("宅配商品：" + delivInfo.mProduct );
//        tv = (TextView) dlgLayout.findViewById(R.id.tvDate);
        //tv.setText("契約日：" + date);
//        tv.setText("契約日：" + delivInfo.mLastDate);
//        tv = (TextView) dlgLayout.findViewById(R.id.tvClaim);
        //if( claim > 0) {
//        if( delivInfo.mClaim > 0) {
//            tv.setText("クレーム　あり");
//        } else {
//            tv.setText("クレーム　なし");
//        }
        // 以下を顧客詳細情報画面に変えたい　by M.Suna '13-09-20

        LinearLayout sv = (LinearLayout)LayoutInflater.from(context).inflate(R.layout.layout_product_info, null);
        //TableLayout dlgLayout = (TableLayout)sv.inflate(context, R.id.productTable, null);
        TableLayout dlgLayout = (TableLayout)sv.findViewById(R.id.productTable);

        // ボタン周りの初期化
        InitComponent(dlgLayout);
        mCtx = context;
        
        if (!(mCtx instanceof MapActivity)) return;
        
        Activity palAct = (Activity)mCtx;
        NaviApplication app = (NaviApplication)palAct.getApplication();
        
        TableRow oneRow;

/* ここがポイント！だと思う　by M.Suna '13-09-27 */
/*
        if( context instanceof MapActivity ) {
            Button btn = (Button)dlgLayout.findViewById(R.id.);
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ((MapActivity)mCtx).showCaution();
                }});
        }
*/
        // タイトル非表示
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        int proCnt = app.getProductListSize();
        TextView tv;
        ProductInfo ProdInfo;
        
        // TODO ここに、コントロールにデータを入れる処理を入れる。

        // ポイント
        //EditText dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText1);
        //dispCtrl.setText("" + delivInfo.mCustPoint);

        // 商品略名
        //EditText dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText1);
//        EditText dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText3);
        
        final int WC = ViewGroup.LayoutParams.WRAP_CONTENT; 
        final int FP = ViewGroup.LayoutParams.FILL_PARENT; 
        
        final int weekWidth = 70;
        //Layoutp
        final int textColor = 0xff000000;
        final int backColor = 0xffc8c8c8;
        
        for (int i = 0; i <= proCnt - 1; i++)
        {
        	ProdInfo = app.getProduct(i);
        	oneRow = new TableRow(mCtx);
        	
        	// 商品略称
        	tv = new TextView(mCtx);
        	tv.setText(ProdInfo.mProductRyaku);
        	//tv.setLayoutParams(null);
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
        	
        	// 配達方法
        	tv = new TextView(mCtx);
        	tv.setText(ProdInfo.mDeliveryMethod);
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
        	
            // 月
        	tv = new TextView(mCtx);
        	tv.setText("" + ProdInfo.mMonDeliCnt);
        	//tv.setLayoutParams(new LayoutParams(WC, WC));
        	//tv.setWidth(pixels)
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
            
            // 火
        	tv = new TextView(mCtx);
        	tv.setText("" + ProdInfo.mTueDeliCnt);
        	//tv.setLayoutParams(new LayoutParams(WC, WC));
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
            
            // 水
        	tv = new TextView(mCtx);
        	tv.setText("" + ProdInfo.mWedDeliCnt);
        	//tv.setLayoutParams(new LayoutParams(WC, WC));
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
            
            // 木
        	tv = new TextView(mCtx);
        	tv.setText("" + ProdInfo.mThuDeliCnt);
        	//tv.setLayoutParams(new LayoutParams(WC, WC));
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
            
            // 金
        	tv = new TextView(mCtx);
        	tv.setText("" + ProdInfo.mFriDeliCnt);
        	//tv.setLayoutParams(new LayoutParams(WC, WC));
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
            
            // 土
        	tv = new TextView(mCtx);
        	tv.setText("" + ProdInfo.mSatDeliCnt);
        	//tv.setLayoutParams(new LayoutParams(WC, WC));
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
            
            // 日
        	tv = new TextView(mCtx);
        	tv.setText("" + ProdInfo.mSunDeliCnt);
        	//tv.setLayoutParams(new LayoutParams(WC, WC));
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
            
            // 他
        	tv = new TextView(mCtx);
        	tv.setText("" + ProdInfo.mSecondCnt);
        	//tv.setLayoutParams(new LayoutParams(WC, WC));
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
            
            // 単価
        	tv = new TextView(mCtx);
        	tv.setText("" + ProdInfo.mPrice);
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
            
            // 配達開始日
        	tv = new TextView(mCtx);
        	tv.setText(ProdInfo.mDeliveryStart);
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
            
            // 配達終了日
        	tv = new TextView(mCtx);
        	tv.setText("" + ProdInfo.mDeliveryEnd);
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
            
            // 配達日
        	tv = new TextView(mCtx);
        	tv.setText(ProdInfo.mDeliverySecond);
        	tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        	tv.setTextColor(textColor);
        	tv.setBackgroundColor(backColor);
        	oneRow.addView(tv);
        	
        	dlgLayout.addView(oneRow, createParam(FP, WC));
        }
        
        oneRow = new TableRow(mCtx);
        mReturn = new Button(mCtx);
        mReturn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				ProductDialog.this.dismiss();
			}
		});
        mReturn.setText(R.string.infoBack);
        mReturn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
        oneRow.addView(mReturn);
        dlgLayout.addView(oneRow, createParam(FP, WC));
        
/*        
        EditText dispCtrl = (EditText)dlgLayout.findViewById(R.id.textView1);
        dispCtrl.setText(ProdInfo.mProductRyaku);
        
        // 配達方法
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText12);
        dispCtrl.setText(ProdInfo.mDeliveryMethod);
        
        // 月
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText4);
        dispCtrl.setText("" + ProdInfo.mMonDeliCnt);
        
        // 火
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText5);
        dispCtrl.setText("" + ProdInfo.mTueDeliCnt);
        
        // 水
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText13);
        dispCtrl.setText("" + ProdInfo.mWedDeliCnt);
        
        // 木
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText6);
        dispCtrl.setText("" + ProdInfo.mThuDeliCnt);
        
        // 金
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText14);
        dispCtrl.setText("" + ProdInfo.mFriDeliCnt);
        
        // 土
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText7);
        dispCtrl.setText("" + ProdInfo.mSatDeliCnt);
        
        // 日
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText15);
        dispCtrl.setText("" + ProdInfo.mSunDeliCnt);
        
        // 他
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText8);
        dispCtrl.setText("" + ProdInfo.mSecondCnt);
        
        // 単価
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText16);
        dispCtrl.setText("" + ProdInfo.mPrice);
        
        // 配達開始日
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText9);
        dispCtrl.setText(ProdInfo.mDeliveryStart);
        
        // 配達終了日
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText17);
        dispCtrl.setText("" + ProdInfo.mDeliveryEnd);
        
        // 配達日
        dispCtrl = (EditText)dlgLayout.findViewById(R.id.editText10);
        dispCtrl.setText(ProdInfo.mDeliverySecond);
*/

        // モーダル状態セット
        this.setCanceledOnTouchOutside(false);

        // レイアウトセット
        //this.setContentView(dlgLayout);
        this.setContentView(sv);
    }

    private TableLayout.LayoutParams createParam(int w, int h){
        return new TableLayout.LayoutParams(w, h);
    }
    
    /**
     * ボタン周りの初期化
     *
     * @param dlgLayout ダイアログレイアウト
     */
    private void InitComponent(LinearLayout dlgLayout) {

        // OKボタンイベントセット
/*    	
        mbtnOk = (Button) dlgLayout.findViewById(R.id.btnOK);
        mbtnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                InfoDialog.this.dismiss();
            }
        });
*/        
 /*       
        // 戻るボタンイベントセット
        mReturn = (Button)dlgLayout.findViewById(R.id.button4);
        mReturn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				ProductDialog.this.dismiss();
			}
		});
*/
    }

}
