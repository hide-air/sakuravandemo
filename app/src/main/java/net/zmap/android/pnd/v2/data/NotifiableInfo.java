package net.zmap.android.pnd.v2.data;

import java.io.Serializable;
import java.util.Date;

public class NotifiableInfo implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -4246933013027222181L;
	/**
	 *
	 */
	public String 			m_sHash = null;				// ハッシュ値(ID)
	public String			m_Model =null;				// モデル名
	public String			m_DialogMsg =null;			// お知らせメッセージ
	public Date				m_StartDate = null;			// お知らせ開始日付
	public Date				m_EndDate = null;			// お知らせ終了日付
	public int				m_NoticeCnt = -1;			// お知らせ回数
	public String			m_sUrl = null;				// URL
	public String			m_sMapSettingName = null;	// マップアイコン設定ファイル名
	public String			m_sMapSettingHash = null;	// マップアイコン設定ファイルのハッシュ値
	public String			m_sMapIconName = null;		// マップアイコンファイル名
	public String			m_sMapIconHash = null;		// マップアイコンのハッシュ値
	public String			m_sDRSettingName = null;	// DR設定ファイル名
	public String			m_sDRSettingHash = null;	// DR設定ファイルのハッシュ値
// ADD.2013.08.21 N.Sasao お知らせ追加機能(描パラダウンロード)実装 START
	public String			m_sExtMapParam0Name = null;	// 描パラ(昼)設定ファイル名
	public String			m_sExtMapParam0Hash = null;	// 描パラ(昼)設定ファイルのハッシュ値
	public String			m_sExtMapParam1Name = null;	// 描パラ(夜)設定ファイル名
	public String			m_sExtMapParam1Hash = null;	// 描パラ(夜)設定ファイルのハッシュ値
// ADD.2013.08.21 N.Sasao お知らせ追加機能(描パラダウンロード)実装  END
	public boolean			m_bInfoChange = false;		// ドライブコンテンツリストファイルのハッシュ値
}