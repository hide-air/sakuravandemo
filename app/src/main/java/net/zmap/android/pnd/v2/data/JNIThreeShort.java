/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           JNIThreeShort.java
 * Description    3��long�^�̃f�[�^��class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class JNIThreeShort {


    private short m_sWideCode;
    private short m_sMiddleCode;
    private short m_sNarrowCode;
    /**
    * Created on 2010/08/06
    * Title:       getM_sWideCode
    * Description:  都道府県コードを取得する
    * @param1  無し
    * @return       short

    * @version        1.0
    */
    public short getM_sWideCode() {
        return m_sWideCode;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_sMiddleCode
    * Description:   市区町村コードを取得する
    * @param1  無し
    * @return       short

    * @version        1.0
    */
    public short getM_sMiddleCode() {
        return m_sMiddleCode;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_sNarrowCode
    * Description:   大字コードを取得する
    * @param1  無し
    * @return       short

    * @version        1.0
    */
    public short getM_sNarrowCode() {
        return m_sNarrowCode;
    }
}
