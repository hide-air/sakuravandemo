/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           POI_Tel_ListItem.java
 * Description    Tel 電話番号検索
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class POI_Tel_ListItem {
    private long m_lTelSize;
    private String m_Tel;
    private long m_lFullTelSize;
    private String m_FullTel;
    private long m_lNameSize;
    private String m_Name;
    private long m_lLon;
    private long m_lLat;
    /**
    * Created on 2010/08/06
    * Title:       getM_lTelSize
    * Description:  電話番号サイズを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lTelSize() {
        return m_lTelSize;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_lTelSize
    * Description:  電話番号を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getM_Tel() {
        return m_Tel;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_lFullTelSize
    * Description:  装飾ありの電話番号サイズを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lFullTelSize() {
        return m_lFullTelSize;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_FullTel
    * Description:  装飾ありの電話番号を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getM_FullTel() {
        return m_FullTel;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_lNameSize
    * Description:  名称サイズを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lNameSize() {
        return m_lNameSize;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_Name
    * Description:  名称を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getM_Name() {
        return m_Name;
    }
    /**
    * Created on 2010/08/06
    * Title:       setM_Name
    * Description:  名称を設定する
    * @param1  無し
    * @return       void

    * @version        1.0
    */
    public void setM_Name(String name) {
        m_Name = name;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_lLon
    * Description:  経度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLon() {
        return m_lLon;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_lLat
    * Description:  緯度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLat() {
        return m_lLat;
    }
}
