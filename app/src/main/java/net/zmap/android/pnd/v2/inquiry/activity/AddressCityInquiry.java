//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.JNIJumpKey;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIShort;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Address_ListItem;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.maps.OpenMap;

import java.util.ArrayList;

/**
 * K-A1_市区町村一覧
 * K-A2_住所第３階層
 *
 * */
public class AddressCityInquiry extends CityInquiry {

	/**
	 * 住所のレベル<br>
	 * 2:K-A1_市区町村一覧
	 * 3:K-A2_住所第３階層
	 * */
	private JNILong Treeindex = new JNILong();
	/** JumpKey５０音の内容 */
	private JNIJumpKey[] JumpKey = null;
	/** JumpRecCountは50音の個数 */
	private JNIShort JumpRecCount = new JNIShort();

	/** 表示した市区町村或は、大字の内容 */
	private POI_Address_ListItem[] m_Poi_Add_Listitem = null;
	/** 表示した市区町村或は、大字の内容 */
	private POI_Address_ListItem[] m_Poi_Show_Listitem = null;
	private static AddressCityInquiry instance;


	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		instance = this;
//		Button oBtn = (Button) this.findViewById(R.id.inquiry_btn);
//		oBtn.setBackgroundResource(R.drawable.btn_92_70);
//		oBtn.setText(R.string.address_ok);
	}

	public static AddressCityInquiry getInstance() {
		return instance;
	}

	@Override
	protected void initCityBtn(Button oBtn) {
//		Button oBtn = (Button) this.findViewById(R.id.inquiry_btn);
		oBtn.setBackgroundResource(R.drawable.btn_default);
		oBtn.setText(R.string.address_ok);
	}

	@Override
	protected void initCount() {
		Intent oIntent = getIntent();
		if (null == ListCount) {
			ListCount = new JNILong();
		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"tttt 1394 ====Index==" + Index.lcount);


//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"tttt start ====128======" + RecIndex);
		int pos = oIntent.getIntExtra(Constants.PARAMS_PROVINCE_ID, 0);
		long lTreeRecIndex = oIntent.getLongExtra(Constants.PARAMS_TREERECINDEX, (long)0);
//		short WideCode = oIntent.getShortExtra(Constants.PARAMS_WIDECODE, (short) 0);
//		short MiddleCode = oIntent.getShortExtra(Constants.PARAMS_MIDDLECODE, (short) 0);
//
//		short NarrowCode = oIntent.getShortExtra(Constants.PARAMS_NARROWCODE, (short) 0);
		//市区町村の場合、lTreeRecIndexをリセットする、大字の場合は直接に次階段を取得する
		if (!oIntent.getBooleanExtra(Constants.PARAMS_IS_SHOW_BUTTON, false)) {
			lTreeRecIndex = pos;
		}
//		lTreeRecIndex = 25;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"tttt 30 ====lTreeRecIndex==" + lTreeRecIndex);
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"31  WideCode==" + WideCode + "==MiddleCode==" + MiddleCode + "==NarrowCode==" + NarrowCode);
		// 住所レベル（Treeindex）を取得する
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetNextTreeIndex(lTreeRecIndex, Treeindex);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"1466 ====Treeindex==" + Treeindex.lcount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ====Treeindex==" + Treeindex.lcount);
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchNextList(lTreeRecIndex);

//		searchJUMP();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ====lTreeRecIndex==" + lTreeRecIndex);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POIAddr_GetRecCount start");

// Del 2011/05/25 katsuta Start -->
//		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecCount(RecCount);
// Del 2011/05/25 katsuta End <--
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POIAddr_GetRecCount end");
		//yangyang mod start 4/12mail
//		super.allRecordCount = initPageIndexAndCnt(RecCount);
// Del 2011/05/25 katsuta Start -->
//		super.allRecordCount = (int) RecCount.lcount;
// Del 2011/05/25 katsuta End <--
		//yangyang mod end 4/12mail
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"1239 ====JumpRecCount==" + JumpRecCount.getM_sJumpRecCount());
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"1240 ====JumpKey==" + JumpKey.length);
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"1392 ====Index==" + Index.lcount);
//		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_AddrIndex(Index);//
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"initCount ====allRecordCount==" + allRecordCount);

	}

	@Override
	protected void getCurrentPageShowData(boolean bool) {
// Chg 2011/05/24 katsuta Start -->
//		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecCount(ListCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"43   ListCount===" + ListCount.lcount);
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecCount(ListCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"43   ListCount===" + ListCount.lcount);
		super.allRecordCount = (int) ListCount.lcount;
// Chg 2011/05/24 katsuta End <--
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"44  RecIndex ====" + RecIndex);
		long startIndex = RecIndex;
		if (ONCE_GET_COUNT != 0 && RecIndex % ONCE_GET_COUNT != 0) {
			startIndex = RecIndex/ONCE_GET_COUNT*ONCE_GET_COUNT;
		}
		if (startIndex <=0) {
			startIndex = 0;
		}

		getRecordIndex = (int)startIndex/ONCE_GET_COUNT;
		initOnceGetList();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy JNI_NE_POIAddr_GetRecList startIndex====" + startIndex);
//		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecList(RecIndex,
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecList(startIndex,
				ONCE_GET_COUNT, m_Poi_Add_Listitem, ListCount);// Rec 有効なリスト数、例えば：50中、10だけあるはRec=10;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"ONCE_GET_COUNT===" + ONCE_GET_COUNT);
//		search();
		resetShowList(bool);
//		if (!bool && !isGroupBtnClick) {
//			getRecordIndex++;
//
//		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"45  ListCount===" + ListCount.lcount);
	}


	private void resetShowList(boolean bool) {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"bool===" + bool);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"RecIndex===" + RecIndex);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"resetShowList getRecordIndex===" + getRecordIndex);
		if (bool) {
			int len = m_Poi_Add_Listitem.length;
			for (int i = 0 ; i < len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_Add_Listitem[i];
			}
		} else {

			if (getRecordIndex > 2 ) {
				for (int i = m_Poi_Add_Listitem.length-ONCE_GET_COUNT; i < ONCE_GET_COUNT*2 ; i++) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"resetShowList i===" + i);
					if (!AppInfo.isEmpty(m_Poi_Show_Listitem[i].getM_Name())) {
						m_Poi_Show_Listitem[i] = m_Poi_Show_Listitem[i];
					} else {
						m_Poi_Show_Listitem[i] = new POI_Address_ListItem();
					}
				}
			}
			if (RecIndex >= ONCE_GET_COUNT) {
				int len = (int)ListCount.lcount;
				int j = 0;
				for (int i = ONCE_GET_COUNT ; i < ONCE_GET_COUNT + len ; i++) {
					m_Poi_Show_Listitem[i] = m_Poi_Add_Listitem[j];
//					System.out.println(m_Poi_Show_Listitem[i].getM_Name());
					j++;
				}
			} else {
				int len = m_Poi_Add_Listitem.length;
				for (int i = 0 ; i < len ; i++) {
					m_Poi_Show_Listitem[i] = m_Poi_Add_Listitem[i];
				}
//				getRecordIndex = 1;
			}
		}

	}
	@Override
	protected void initDataObj() {


		initOnceGetList();
		if(JumpKey  == null){
			JumpKey = new JNIJumpKey[50];
			for (int i = 0; i < 50; i++) {
				JumpKey[i] = new JNIJumpKey();
			}
		}
		m_Poi_Show_Listitem = new POI_Address_ListItem[ONCE_GET_COUNT*2];
		for (int i = 0 ; i < ONCE_GET_COUNT*2 ; i++) {
			m_Poi_Show_Listitem[i] = new POI_Address_ListItem();
		}
		if (null == ListCount) {
			ListCount = new JNILong();
		}
	}
	private void initOnceGetList() {
		m_Poi_Add_Listitem = new POI_Address_ListItem[ONCE_GET_COUNT];
		for (int i = 0 ; i < ONCE_GET_COUNT ; i++) {
			m_Poi_Add_Listitem[i] = new POI_Address_ListItem();
		}

	}

	@Override
	protected String getButtonValue(int wPos) {
//		return m_Poi_Add_Listitem[wPos%getCountInBox()].getM_Name();
//		if (wPos >= getCountInBox()) {
//			int pageIndex  = wPos/getCountInBox();
//			wPos = wPos-getCountInBox()*pageIndex;
//		}
		int wlocalPos = resetIndex(wPos);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"getButtonValue wPos====" + wPos + ",====wlocalPos===" + wlocalPos);
//		return m_Poi_Add_Listitem[wPos].getM_Name();
		return m_Poi_Show_Listitem[wlocalPos].getM_Name();
	}



	@Override
	protected void initJumpList() {
		//yangyang mod start Bug617
//		if (AppInfo.isEmpty(beforeGroupValue)) {
//			checkGroupShow(this.getResources().getString(R.string.btn_A_name));
//		} else {
////			checkGroupShow(oGroupValue.getText().toString());
//			checkGroupShow(beforeGroupValue);
//		}
		checkGroupShow();
		//yangyang mod end Bug617
//		initGroupButton();
		initScrollGroupShowValue(JumpKey,JumpRecCount);
//		if (-1 != Index.lcount) {
//			oGroupValue.setText(getGroupValue((int)Index.lcount));
//		} else {
//			oGroupValue.setText(checkGroupBtnShow(this.getResources().getString(R.string.btn_A_name)));
//		}
		// super.searchJUMP();
	}
	ArrayList<String[]> listGroupValueIndex = new ArrayList<String[]>();
	private void initScrollGroupShowValue(JNIJumpKey[] jumpKey2,
			JNIShort jumpRecCount2) {
		for (int i = 0 ; i < jumpRecCount2.getM_sJumpRecCount(); i++) {
			String msName= jumpKey2[i].pucName;
			long NameSize= jumpKey2[i].NameSize;
			String[] sValue = new String[2];

			JNILong JumpRecIndex = new JNILong();
			NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetJumpList2RecordIndex(
					NameSize, msName, JumpRecIndex);
			sValue[0] = String.valueOf(JumpRecIndex.lcount);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"hahah sValue[0]========sValue[1]====" + sValue[0] + "======" + msName);
			sValue[1] = msName;
			listGroupValueIndex.add(sValue);

		}
	}

	protected String getGroupValue(int startIndex) {
		String sResult = "";
//		int scrollGroupIndex = 0;
		for (int i = 0 ; i < listGroupValueIndex.size() ; i++) {
			String[] sValue = (String[])listGroupValueIndex.get(i);
			int index = Integer.parseInt(sValue[0]);
			//yangyang mod start bug617
//			if (index <= startIndex) {
//				if (AppInfo.isEmpty(sResult)) {
//					sResult = sValue[1];
//					scrollGroupIndex= index;
//				} else {
//					if (scrollGroupIndex <= index) {
//						sResult = sValue[1];
//						scrollGroupIndex= index;
//					}
//				}
//
//			}
			if (index == startIndex) {
				sResult = sValue[1];
//				scrollGroupIndex= index;
			}
			//yangyang mod end bug617
		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"hahah localIndex======sResult======" + localIndex + "======" + sResult);
//		if (AppInfo.isEmpty(sResult)) {
//			sResult = oGroupValue.getText().toString();
//		}
//		Toast toast = Toast.makeText(this, null, Toast.LENGTH_SHORT);
//		toast.setText(sResult);
//		toast.show();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"hahah ==sResult==" + sResult);
		return checkGroupBtnShow(sResult);
	}
	@Override
	protected void setCurrentGroupStartShow() {
//		search();
		checkGroupBtnShow(JumpKey,JumpRecCount,true);
		super.isGroupBtnClick = true;
		if (!AppInfo.isEmpty(msName)) {
			initGroupIndex();
		}
		////////////////
//		getCurrentPageShowData();
//		oBox.setM_wIndex((int)RecIndex);
//		oBox.reset();
		getCurrentPageShowData(false);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy setCurrentGroupStartShow===" + RecIndex);
		oList.scroll((int)RecIndex);

	}

	protected int getFirstShowIndex() {
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_AddrIndex(Index);//
		int iReturn = (int) Index.lcount;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"getFirstShowIndex Index===" + Index.lcount);
//		int pageIndex  = (int)Index.lcount/getCountInBox();
//
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"getFirstShowIndex iReturn%5===" + (iReturn%5));
//		int iReturn = (int)getCountInBox() * pageIndex;
		//市区町村の場合、自車の位置は中間に表示する
		if (Treeindex.lcount == 2) {
			if (iReturn > this.getCount()-3){
				iReturn = this.getCount()-this.getCountInBox()+1;
			} else if (iReturn > 2) {
				iReturn -= 2;
			} else {
				iReturn = 0;
			}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"getFirstShowIndex iReturn===" + iReturn);
//		oBox.setM_wIndex(iReturn);
		} else {
			//大字の場合
			iReturn = 0;
		}
		return iReturn;
	}

	@Override
	protected void search() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"search start =================");

// Add 2011/05/25 katsuta Start -->
		JNILong RecCount = new JNILong();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecCount(RecCount);
		super.allRecordCount =(int)RecCount.lcount;
// Add 2011/05/25 katsuta End <--

//		JNILong Rec = new JNILong();
//
////		if (null == oGroupValue) {
////			checkGroupShow(this.getResources().getString(R.string.btn_A_name));
////		} else {
////			checkGroupShow(oGroupValue.getText().toString());
////		}
////		NaviLog.d(NaviLog.PRINT_LOG_TAG,"--------back---->" + back);
//		if (!back && !AppInfo.isEmpty(msName)) {
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"initGroupIndex start =================");
//			initGroupIndex();
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"initGroupIndex end =================");
//		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"initGroupButton start =================");
//		initGroupButton();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"initGroupButton end =================");
////		NaviLog.d(NaviLog.PRINT_LOG_TAG,"tttt JumpRecCount===" + JumpRecCount.getM_sJumpRecCount());
//		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_AddrIndex(Index);//2と3レベル問題がある
//
//		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecList(RecIndex,
//				getCountInBox(), m_Poi_Add_Listitem, Rec);// Rec 有効なリスト数、例えば：50中、10だけあるはRec=10;
////		NaviLog.d(NaviLog.PRINT_LOG_TAG,"137yy ===" + m_Poi_Add_Listitem[0].getM_Name());
////		NaviLog.d(NaviLog.PRINT_LOG_TAG,"1842 =JNI_NE_POIAddr_GetRecCount===RecCount==" + RecCount.lcount);
////		NaviLog.d(NaviLog.PRINT_LOG_TAG,"1843 =JNI_NE_POIAddr_GetRecList===Rec==" + Rec.lcount);
////		initDialog();
//
////		if(bIsRefresh) {
////			oBox.refresh();
////		}
//
////		oBox.setM_wIndex((int)RecIndex);
////		oBox.reset();


	}

//	private boolean bIsRefresh = true;

	private void initGroupIndex() {
		JNILong JumpRecIndex = new JNILong();
//		NameSize = JumpKey[(int) RecIndex].NameSize;
//		msName = JumpKey[(int) RecIndex].pucName;
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"1588 ===size===" +NameSize);

		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"1589 ===str===" +msName);
//		Toast toast = Toast.makeText(this, null, Toast.LENGTH_SHORT);
//		toast.setText(msName);
//		toast.show();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetJumpList2RecordIndex(
				NameSize, msName, JumpRecIndex);
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"start ====140======" + RecIndex);
		RecIndex = JumpRecIndex.lcount;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"JumpRecIndex===" + JumpRecIndex.lcount);


	}

	/**
	 * グループボタンの表示状態をチェックする
	 *
	 * */
	private void checkGroupShow() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"checkGroupShow  start");
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetJumpList(JumpKey,//[out]
				JumpRecCount);//JumpRecCountは50音の個数,JumpKey５０音の内容
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"checkGroupShow  end");
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"tttt JNI_NE_POIAddr_GetJumpList do JumpRecCount===" + JumpRecCount.getM_sJumpRecCount());

		checkGroupBtnShow(JumpKey,JumpRecCount,false);



	}

	@Override
	protected void goResultInquiry(View oView,int wPos) {
		//yangyang mod start Bug617
//		ButtonImg obtn = (ButtonImg)oView;
//		LinearLayout obtn = (LinearLayout)oView;
		//yangyang mod end Bug617
//		POI_Address_ListItem poi = new POI_Address_ListItem();
//		Object oIndex = hashIndex.get(wPos);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"goResultInquiry city Index = ===" + wPos);
//		int iIndex = Integer.parseInt(oIndex.toString());
//		poi = m_Poi_Add_Listitem[iIndex];
		/*
		 * たとえば：グループボタンのスタートIndexは41、それ場合、画面の表示内容は41,42,43,44,45
		 * Indexは45の場合、再度JNIからデータを取得し、対象「m_Poi_Show_Listitem」に格納した。
		 * Indexは41の対象を押下したら、取得した対象poiは間違えるかもしれない
		*/
		RecIndex = wPos;
		getCurrentPageShowData(false);
		//wPosをリセットする
		int wIndex = resetIndex(wPos);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy goResultInquiry reset wIndex====" + wIndex + ",wPos====" + wPos);
		POI_Address_ListItem poi = m_Poi_Show_Listitem[wIndex];
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"vvvv Treeindex.lcount-====" + Treeindex.lcount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"vvvv m_Poi_Add_Listitem[wPos%getCountInBox()].getM_iNextCategoryFlag()====" + m_Poi_Add_Listitem[wPos%getCountInBox()].getM_iNextCategoryFlag());
		if (Treeindex.lcount == 2) {

//			if (m_Poi_Add_Listitem[iIndex].getM_iNextCategoryFlag() == 1) {
			if (poi.getM_iNextCategoryFlag() == 1) {
				Intent oIntent = new Intent(getIntent());
				String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY) +
				Constants.FLAG_TITLE +
				//yangyang mod start bug617
//				obtn.getText().toString();
				getButtonValue(wPos);
//				yangyang mod end bug617
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ======obtn.getText().toString()===" + obtn.getText().toString());
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ======poi===" + poi.getM_Name());
				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
				oIntent.putExtra(Constants.PARAMS_IS_SHOW_BUTTON, true);
				oIntent.putExtra(Constants.PARAMS_TREERECINDEX, (long)wPos);
				//確定ボタン使用のデータを保存する
				String[] list = new String [STRINGLEN];
				list[INDEX_0] = String.valueOf(poi.getM_lLongitude());
				list[INDEX_1] = String.valueOf(poi.getM_lLatitude());
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"list====long==" + list[INDEX_0]+ "=====lat==" + list[INDEX_1]);

//				list[INDEX_2] = poi.getM_Name();
//				TextView oText = (TextView) this.findViewById(R.id.inquiry_title);

				list[INDEX_2] = title.replace(Constants.FLAG_TITLE, " ");

				hashSelectedPoi.add(list);

				//NaviLog.d(NaviLog.PRINT_LOG_TAG,"wPos=====" + (long)wPos);
				oIntent.setClass(this,AddressCityInquiry.class);
//Chg 2011/09/23 Z01yoneya Start -->
//				this.startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_ADDRESSCITYINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
				NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_ADDRESSCITYINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
			} else {
				//地図へ遷移する
//				POI_Address_ListItem poi = m_Poi_Add_Listitem[wPos%getCountInBox()];
//	            long lon = poi.getM_lLongitude();
//                long lat = poi.getM_lLatitude();
//                String address = poi.getM_Name();
//                openMap(null, lon, lat, address);
				Intent oIntent = getIntent();
// Add by CPJsunagawa '13-12-25 Start
				// ここで、住所検索によるマッチングであるフラグがあった場合、
				// extraにINDEXを入れて返してゆく
				if (oIntent.hasExtra(Constants.ADDRESS_SEARCH_FOR_MATCH))
				{
					int index = oIntent.getIntExtra(Constants.ADDRESS_SEARCH_FOR_MATCH, -1);
					if (index >= 0)
					{
						oIntent.putExtra(Constants.CHANGE_COORD_LIST_INDEX, index);
						oIntent.putExtra(Constants.INTENT_LONGITUDE, poi.getM_lLongitude());
						oIntent.putExtra(Constants.INTENT_LATITUDE, poi.getM_lLatitude());
						setResult(Constants.RESULTCODE_CHECK_PLACE, oIntent);
						finish();
						return;
					}
				}
// Add by CPJsunagawa '13-12-25 End
				
// Add by CPJsunagawa '2015-07-08 Start
				// アイコン登録であるフラグがあった場合、extraにINDEX、選択された位置情報を返す
				if (oIntent.hasExtra(Constants.REGIST_ICON))
				{
					int index = oIntent.getIntExtra(Constants.REGIST_ICON, -1);
					if (index >= 0)
					{
						oIntent.putExtra(Constants.SELECT_ICON_INDEX, index);
						oIntent.putExtra(Constants.INTENT_LONGITUDE, poi.getM_lLongitude());
						oIntent.putExtra(Constants.INTENT_LATITUDE, poi.getM_lLatitude());
						setResult(Constants.RESULTCODE_REGIST_ICON, oIntent);
						finish();
						return;
					}
				}

				// テキスト入力であるフラグがあった場合、extraにINDEX、選択された位置情報を返す
				if (oIntent.hasExtra(Constants.INPUT_TEXT))
				{
					int index = oIntent.getIntExtra(Constants.INPUT_TEXT, -1);
					if (index >= 0)
					{
						oIntent.putExtra(Constants.SELECT_ICON_INDEX, index);
						oIntent.putExtra(Constants.INTENT_LONGITUDE, poi.getM_lLongitude());
						oIntent.putExtra(Constants.INTENT_LATITUDE, poi.getM_lLatitude());
						setResult(Constants.RESULTCODE_INPUT_TEXT, oIntent);
						finish();
						return;
					}
				}
// Add by CPJsunagawa '2015-07-08 End
				
				String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY) +
				Constants.FLAG_TITLE +
				getButtonValue(wPos);


                POIData oData = new POIData();
//                oData.m_sName = poi.getM_Name();
                oData.m_sName = title.replace(Constants.FLAG_TITLE, " ");
                oData.m_sAddress = oData.m_sName;
                oData.m_wLong = poi.getM_lLongitude();
                oData.m_wLat = poi.getM_lLatitude();
                // XuYang add start #1056
                if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
                    CommonLib.setChangePosFromSearch(true);
                    CommonLib.setChangePos(false);
// Add by CPJsunagawa '2015-07-08 Start
	                CommonLib.setRegIcon(false);
    	            CommonLib.setInputText(false);
// Add by CPJsunagawa '2015-07-08 End
                } else {
                    CommonLib.setChangePosFromSearch(false);
                    CommonLib.setChangePos(false);
// Add by CPJsunagawa '2015-07-08 Start
	                CommonLib.setRegIcon(false);
    	            CommonLib.setInputText(false);
// Add by CPJsunagawa '2015-07-08 End
                }
                // XuYang add end #1056
    			NaviLog.d(NaviLog.PRINT_LOG_TAG, "AddressCity goResultInquiry2 ================== bIsListClicked: " + bIsListClicked);
                OpenMap.moveMapTo(AddressCityInquiry.this, oData, oIntent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
                this.createDialog(Constants.DIALOG_WAIT, -1);
			}
		} else {
//			if (m_Poi_Add_Listitem[iIndex].getM_iNextCategoryFlag() == 1) {
			if (poi.getM_iNextCategoryFlag() == 1) {
				Intent oIntent = new Intent(getIntent());
				oIntent.setClass(this, AddressResultInquiry.class);
				String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY)
						+ Constants.FLAG_TITLE +
						//yangyang mod start Bug617
//						obtn.getText().toString();
						getButtonValue(wPos);
				//yangyang mod end Bug617
				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
//				NaviRun.GetNaviRunObj();
//				NaviRun.setiActivityId(AppInfo.ID_ACTIVITY_ADDRESSRESULTINQUIRY);
				NaviRun.GetNaviRunObj().setSearchKind(AppInfo.ID_ACTIVITY_ADDRESSRESULTINQUIRY);
				oIntent.putExtra(Constants.PARAMS_TREERECINDEX, (long)wPos);
				//NaviLog.d(NaviLog.PRINT_LOG_TAG,"462  wPos=====" + wPos);
//				oIntent.putExtra(Constants.PARAMS_CLICK_LAT, poi.getM_lLatitude());
//				oIntent.putExtra(Constants.PARAMS_CLICK_LON, poi.getM_lLongitude());
//				oIntent.putExtra(Constants.PARAMS_CLICK_NAME, poi.getM_Name());
				//確定ボタン使用のデータを保存する
				String[] list = new String [STRINGLEN];
				list[INDEX_0] = String.valueOf(poi.getM_lLongitude());
				list[INDEX_1] = String.valueOf(poi.getM_lLatitude());
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"list====long==" + list[INDEX_0]+ "=====lat==" + list[INDEX_1]);

//				list[INDEX_2] = poi.getM_Name();
//				TextView oText = (TextView) this.findViewById(R.id.inquiry_title);

//				list[INDEX_2] = oText.getText().toString();
				list[INDEX_2] = title.replace(Constants.FLAG_TITLE, " ");
				hashSelectedPoi.add(list);




				//NaviLog.d(NaviLog.PRINT_LOG_TAG,"AddressResultInquiry do");
//Chg 2011/09/23 Z01yoneya Start -->
//				this.startActivityForResult(oIntent,AppInfo.ID_ACTIVITY_ADDRESSRESULTINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
				NaviActivityStarter.startActivityForResult(this, oIntent,AppInfo.ID_ACTIVITY_ADDRESSRESULTINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
			} else {
				//地図へ遷移する
//				POI_Address_ListItem poi = m_Poi_Add_Listitem[wPos%getCountInBox()];
//	            long lon = poi.getM_lLongitude();
//                long lat = poi.getM_lLatitude();
//                String address = poi.getM_Name();
//                openMap(null, lon, lat, address);
				Intent oIntent = getIntent();
// Add by CPJsunagawa '14-12-25 Start
				// ここで、住所検索によるマッチングであるフラグがあった場合、
				// extraにINDEXを入れて返してゆく
				if (oIntent.hasExtra(Constants.ADDRESS_SEARCH_FOR_MATCH))
				{
					int index = oIntent.getIntExtra(Constants.ADDRESS_SEARCH_FOR_MATCH, -1);
					if (index >= 0)
					{
						oIntent.putExtra(Constants.CHANGE_COORD_LIST_INDEX, index);
						oIntent.putExtra(Constants.INTENT_LONGITUDE, poi.getM_lLongitude());
						oIntent.putExtra(Constants.INTENT_LATITUDE, poi.getM_lLatitude());
						setResult(Constants.RESULTCODE_CHECK_PLACE, oIntent);
						finish();
						return;
					}
				}
// Add by CPJsunagawa '14-12-25 End
				
// Add by CPJsunagawa '2015-07-08 Start
				// アイコン登録であるフラグがあった場合、extraにINDEX、選択された位置情報を返す
				if (oIntent.hasExtra(Constants.REGIST_ICON))
				{
					int index = oIntent.getIntExtra(Constants.REGIST_ICON, -1);
					if (index >= 0)
					{
						oIntent.putExtra(Constants.SELECT_ICON_INDEX, index);
						oIntent.putExtra(Constants.INTENT_LONGITUDE, poi.getM_lLongitude());
						oIntent.putExtra(Constants.INTENT_LATITUDE, poi.getM_lLatitude());
						setResult(Constants.RESULTCODE_REGIST_ICON, oIntent);
						finish();
						return;
					}
				}

				// テキスト入力であるフラグがあった場合、extraにINDEX、選択された位置情報を返す
				if (oIntent.hasExtra(Constants.INPUT_TEXT))
				{
					int index = oIntent.getIntExtra(Constants.INPUT_TEXT, -1);
					if (index >= 0)
					{
						oIntent.putExtra(Constants.SELECT_ICON_INDEX, index);
						oIntent.putExtra(Constants.INTENT_LONGITUDE, poi.getM_lLongitude());
						oIntent.putExtra(Constants.INTENT_LATITUDE, poi.getM_lLatitude());
						setResult(Constants.RESULTCODE_INPUT_TEXT, oIntent);
						finish();
						return;
					}
				}
// Add by CPJsunagawa '2015-07-08 End
				String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY) +
				Constants.FLAG_TITLE +
				getButtonValue(wPos);

                POIData oData = new POIData();
//              oData.m_sName = poi.getM_Name();
                oData.m_sName = title.replace(Constants.FLAG_TITLE, " ");
                oData.m_sAddress = oData.m_sName;
                oData.m_wLong = poi.getM_lLongitude();
                oData.m_wLat = poi.getM_lLatitude();
             // XuYang add start #1056
                if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
                    CommonLib.setChangePosFromSearch(true);
                    CommonLib.setChangePos(false);
                } else {
                    CommonLib.setChangePosFromSearch(false);
                    CommonLib.setChangePos(false);
                }
    			NaviLog.d(NaviLog.PRINT_LOG_TAG, "AddressCity goResultInquiry3 ================== bIsListClicked: " + bIsListClicked);

                // XuYang add end #1056
                OpenMap.moveMapTo(AddressCityInquiry.this, oData, oIntent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
                this.createDialog(Constants.DIALOG_WAIT, -1);
			}
		}
	}

	@Override
	protected void doAllCityClick(Button obtn) {
		//地図へ遷移する
//		POI_Address_ListItem poi = m_Poi_Add_Listitem[wPos%getCountInBox()];
//        long lon = poi.getM_lLongitude();
//        long lat = poi.getM_lLatitude();
//        String address = poi.getM_Name();
		//NaviLog.d(NaviLog.PRINT_LOG_TAG,"city 497 Treeindex.lcount==" + Treeindex.lcount);
		String[] list = hashSelectedPoi.get(hashSelectedPoi.size()-1);
//		long lon = Long.parseLong(list[INDEX_0]);
//		long lat = Long.parseLong(list[INDEX_1]);
//		String address = list[INDEX_2];
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"list======" + hashSelectedPoi.size());
//		openMap(null, lon, lat, address);

// Add by CPJsunagawa '13-12-25 Start
		Intent oIntent = getIntent();
		// ここで、住所検索によるマッチングであるフラグがあった場合、
		// extraにINDEXを入れて返してゆく
		if (oIntent.hasExtra(Constants.ADDRESS_SEARCH_FOR_MATCH))
		{
			int index = oIntent.getIntExtra(Constants.ADDRESS_SEARCH_FOR_MATCH, -1);
			if (index >= 0)
			{
				oIntent.putExtra(Constants.CHANGE_COORD_LIST_INDEX, index);
				oIntent.putExtra(Constants.INTENT_LONGITUDE, Long.parseLong(list[INDEX_0]));
				oIntent.putExtra(Constants.INTENT_LATITUDE, Long.parseLong(list[INDEX_1]));
				setResult(Constants.RESULTCODE_CHECK_PLACE, oIntent);
				finish();
				return;
			}
		}
// Add by CPJsunagawa '13-12-25 End
		POIData oData = new POIData();
        oData.m_sName = list[INDEX_2];
        oData.m_sAddress = list[INDEX_2];
        oData.m_wLong = Long.parseLong(list[INDEX_0]);
        oData.m_wLat = Long.parseLong(list[INDEX_1]);
     // XuYang add start #1056
        //Intent oIntent = getIntent();
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            CommonLib.setChangePosFromSearch(true);
            CommonLib.setChangePos(false);
        } else {
            CommonLib.setChangePosFromSearch(false);
            CommonLib.setChangePos(false);
        }
        // XuYang add end #1056
		NaviLog.d(NaviLog.PRINT_LOG_TAG, "AddressCity doAllCityClick ================== bIsListClicked: " + bIsListClicked);
		OpenMap.moveMapTo(AddressCityInquiry.this, oData, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
        this.createDialog(Constants.DIALOG_WAIT, -1);
	}

//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onKeyDown");
//		if (keyCode == KeyEvent.KEYCODE_BACK) {
//			//NaviLog.d(NaviLog.PRINT_LOG_TAG,"tttt city back");
//			JNIGoBack();
////			return true;
////			oBox.refresh();
////			back = true;
//		}
//		return super.onKeyDown(keyCode, event);
//	}

	@Override
	protected boolean onClickGoMap() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"Treeindex.lcount===" + Treeindex.lcount);
//		JNIGoBack();
//		JNIGoBack();
//		if (Treeindex.lcount == 1) {
//			JNIGoBack();
//		}
		hashSelectedPoi.clear();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddrKey_Clear();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_Clear();
		return super.onClickGoMap();
	}

	@Override
	protected boolean onClickGoBack() {
//		JNIGoBack();
		return super.onClickGoBack();
	}

	private void JNIGoBack() {
//		bIsRefresh = false;
		if (hashSelectedPoi.size() != 0) {
			hashSelectedPoi.remove(hashSelectedPoi.size()-1);
		}
//		if (Treeindex == null ) {
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"Treeindex is null");
//		} else {
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"Treeindex is not null");
//		}
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetPrevTreeIndex(Treeindex);
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchPrevList();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent oIntent) {
		if (requestCode == AppInfo.ID_ACTIVITY_ADDRESSRESULTINQUIRY
				|| requestCode == AppInfo.ID_ACTIVITY_ADDRESSCITYINQUIRY) {
			this.setResult(resultCode, oIntent);
// Add by CPJsunagawa '13-12-25 Start
			//JNIGoBack();
			// RESULTCODE_CHECK_PLACEが結果であれば即落とす
			//if (resultCode == Constants.RESULTCODE_VEHICLE_ROUTE_DETERMINATION)
			if (resultCode == Constants.RESULTCODE_CHECK_PLACE)
			{
				finish();
			}
			else
			{
				JNIGoBack();
			}
// Add by CPJsunagawa '13-12-25 End

		}
		super.onActivityResult(requestCode, resultCode, oIntent);
	}


}
