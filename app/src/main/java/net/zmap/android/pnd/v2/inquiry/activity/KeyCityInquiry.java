//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.data.JNIJumpKey;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIShort;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Facility_ListItem;

import java.util.ArrayList;

/**
 *
 * K-N3_名称市区町村絞込み
 *
 * */
public class KeyCityInquiry extends CityInquiry {
	/** ひょうじした市区町村の内容 */
	public POI_Facility_ListItem[] m_Poi_Key_Listitem = null;
	public POI_Facility_ListItem[] m_Poi_Show_Listitem = null;
//	private JNILong Index = new JNILong();
	/** JumpKey５０音の内容 */
	private JNIJumpKey[] JumpKey = null;
	/** JumpRecCountは50音の個数 */
	private JNIShort JumpRecCount = new JNIShort();

//	private JNILong RecCount = new JNILong();
//	private boolean localTest = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
//		instance = this;
//		Button oBtn = (Button) this.findViewById(R.id.inquiry_btn);
//		oBtn.setBackgroundResource(R.drawable.btn_120_70);
//		oBtn.setText(R.string.btn_all_city);
	}

	@Override
	protected void initCityBtn(Button oBtn) {
//		Button oBtn = (Button) this.findViewById(R.id.inquiry_btn);
		oBtn.setBackgroundResource(R.drawable.btn_default);
		oBtn.setText(R.string.btn_all_city);
//Add 2011/11/01 Z01_h_yamada Start -->
		oBtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.Common_Medium_textSize));
//Add 2011/11/01 Z01_h_yamada End <--
	}

	@Override
	protected void goResultInquiry(View oView,int wPos) {
		//yangyang mod start Bug617
//		ButtonImg obtn = (ButtonImg)oView;
		//yangyang mod end Bug617
		Intent oIntent = new Intent(getIntent());
		oIntent.putExtra(Constants.FROM_WHICH_SEARCH_KEY, "CityItem");
		//市のIndex
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"wPos + 1===" + (wPos + 1));
		oIntent.putExtra(Constants.PARAMS_TREERECINDEX, wPos + 1);
		String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY) +
		Constants.FLAG_TITLE +
		//yangyang mod start Bug617
//		obtn.getText().toString();
		getButtonValue(wPos);
		//yangyang mod end Bug617
		oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
		oIntent.setClass(KeyCityInquiry.this, KeyResultInquiry.class);
//Chg 2011/09/23 Z01yoneya Start -->
//		startActivityForResult(oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
		NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
	}


	@Override
	protected void getCurrentPageShowData(boolean bool) {

		if (this.getCount() > 0) {
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"yangyang JNI_NE_POI_Facility_GetMunicipalityRecList ===RecIndex="  + RecIndex);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"yangyang JNI_NE_POI_Facility_GetMunicipalityRecList allRecordCount ===" + allRecordCount);

			long startIndex = RecIndex;
			if (RecIndex % ONCE_GET_COUNT != 0) {
				startIndex = RecIndex / ONCE_GET_COUNT * ONCE_GET_COUNT;
			} else {
				startIndex++;
			}
			//全市区町村のレコードは表示しない
			if (startIndex <= 0) {
				startIndex = 1;
			}
			getRecordIndex = (int)startIndex/ONCE_GET_COUNT;
			initOnceGetList();
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"yangyang JNI_NE_POI_Facility_GetMunicipalityRecList startIndex ===" + startIndex);

//			NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetMunicipalityRecList(RecIndex, getCountInBox(), m_Poi_Key_Listitem, ListCount);
			NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetMunicipalityRecList(startIndex, ONCE_GET_COUNT, m_Poi_Key_Listitem, ListCount);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"yangyang JNI_NE_POI_Facility_GetMunicipalityRecList ListCount ===" + ListCount.lcount);


			resetShowList(bool);
//			if (!bool && !isGroupBtnClick) {
//				getRecordIndex++;
//			}
		}
	}
	@Override
	protected void setCurrentGroupStartShow() {
//		search();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"setCurrentGroupStartShow do");
		//yangyang mod start Bug617
//		String value = "";
//		value = oGroupValue.getText().toString();
//		value = beforeGroupValue;
		//yangyang mod end Bug617
//		Toast toast = Toast.makeText(this, null, Toast.LENGTH_SHORT);
//		toast.setText(value);
//		toast.show();

		checkGroupBtnShow(JumpKey,JumpRecCount,true);
		isGroupBtnClick = true;
		initGroupIndex();
		//名称検索だけ実行する
//		initJumpList();
//		getCurrentPageShowData();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"617 setCurrentGroupStartShow RecIndex===" + RecIndex);
//		if (RecIndex > this.getCountInBox()) {
//			oBox.setM_wIndex((int)RecIndex);
//		oList.setSelection((int)RecIndex);
		getCurrentPageShowData(false);
		oList.scroll((int) RecIndex - 1);
		//グループボタンを押下した場合、表示開始のフラグをリセットする
//		getRecordIndex = (int)RecIndex/ONCE_GET_COUNT;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"setCurrentGroupStartShow getRecordIndex===" + getRecordIndex);
//			oBox.reset();
//		} else {
//			oBox.refresh();
//		}


	}
//	/**
//	 *
//	 *
//	 * */
//	private String halfGroupValue(String string) {
//		String bReturn = "";
//		if (string.equals(this.getResources().getString(R.string.kana_a))) {
//			bReturn = this.getResources().getString(R.string.btn_A_name);
//		} else if (string.equals(this.getResources().getString(R.string.kana_ka))){
//			bReturn = this.getResources().getString(R.string.btn_KA_name);
//		} else if (string.equals(this.getResources().getString(R.string.kana_sa))){
//			bReturn = this.getResources().getString(R.string.btn_SA_name);
//		} else if (string.equals(this.getResources().getString(R.string.kana_ta))){
//			bReturn = this.getResources().getString(R.string.btn_TA_name);
//		} else if (string.equals(this.getResources().getString(R.string.kana_na))){
//			bReturn = this.getResources().getString(R.string.btn_NA_name);
//		} else if (string.equals(this.getResources().getString(R.string.kana_ha))){
//			bReturn = this.getResources().getString(R.string.btn_HA_name);
//		} else if (string.equals(this.getResources().getString(R.string.kana_ma))){
//			bReturn = this.getResources().getString(R.string.btn_MA_name);
//		} else if (string.equals(this.getResources().getString(R.string.kana_ya))){
//			bReturn = this.getResources().getString(R.string.btn_YA_name);
//		} else if (string.equals(this.getResources().getString(R.string.kana_ra))){
//			bReturn = this.getResources().getString(R.string.btn_RA_name);
//		} else if (string.equals(this.getResources().getString(R.string.kana_wa))){
//			bReturn = this.getResources().getString(R.string.btn_WA_name);
//		}
//		return bReturn;
//	}
	private void resetShowList(boolean bool) {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"bool===" + bool);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"RecIndex===" + RecIndex);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"getRecordIndex===" + getRecordIndex);
		if (bool) {
			int len = m_Poi_Key_Listitem.length;
			for (int i = 0 ; i < len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_Key_Listitem[i];
			}
		} else {

			if (getRecordIndex > 2 ) {
				for (int i = m_Poi_Key_Listitem.length-ONCE_GET_COUNT; i < ONCE_GET_COUNT*2 ; i++) {
					//NaviLog.d(NaviLog.PRINT_LOG_TAG,"i===" + i);
					if (!AppInfo.isEmpty(m_Poi_Show_Listitem[i].getM_Name())) {
						m_Poi_Show_Listitem[i-ONCE_GET_COUNT] = m_Poi_Show_Listitem[i];
					} else {
						m_Poi_Show_Listitem[i-ONCE_GET_COUNT] = new POI_Facility_ListItem();
					}
				}
			}
			if (RecIndex >= ONCE_GET_COUNT) {
				int len = (int)ListCount.lcount;
				int j = 0;
				for (int i = ONCE_GET_COUNT ; i < ONCE_GET_COUNT + len ; i++) {
					m_Poi_Show_Listitem[i] = m_Poi_Key_Listitem[j];
//					System.out.println(m_Poi_Show_Listitem[i].getM_Name());
					j++;
				}
			} else {
				int len = m_Poi_Key_Listitem.length;
				for (int i = 0 ; i < len ; i++) {
					m_Poi_Show_Listitem[i] = m_Poi_Key_Listitem[i];
				}
//				getRecordIndex = 1;
			}
		}

	}
	private void initGroupIndex() {
		JNILong JumpRecIndex = new JNILong();
		//50音によって、対応したIndex番号を取得する
//		Toast toast = Toast.makeText(this, null, Toast.LENGTH_SHORT);
//		toast.setText(msName);
//		toast.show();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yangyang JNI_NE_POI_Facility_GetJumpKey2RecordNumber NameSize===" + NameSize);

		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetJumpKey2RecordNumber(NameSize, msName, JumpRecIndex);
		RecIndex = (int)JumpRecIndex.lcount;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"617 JNI_NE_POI_Facility_GetJumpKey2RecordNumber   RecIndex===" + RecIndex);
	}

	protected int getFirstShowIndex() {
		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetMypositon(2, Index);//
		int iReturn = 0;
//		int pageIndex  = (int)Index.lcount/(getCountInBox());
//
//		iReturn = getCountInBox() * pageIndex ;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"Index=====" + Index.lcount);
		iReturn = (int) Index.lcount;
		if (iReturn > this.getCount() - 3) {
			iReturn = this.getCount() - this.getCountInBox();
		} else if (iReturn > 2) {
			iReturn -= 3;
		} else {
			iReturn = 0;
		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"iReturn====" + iReturn);
//		oBox.setM_wIndex(iReturn);

		return (int)iReturn ;
	}
	@Override
	protected void initJumpList() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"initJumpList do");
		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetJumpList(JumpKey, JumpRecCount);
		//yangyang del start Bug617
//		String value = "";
////		if (null == oGroupValue || AppInfo.isEmpty(oGroupValue.getText().toString())) {
//		String groupValue = getGroupValue((int)Index.lcount);
//		if (AppInfo.isEmpty(groupValue)) {
////			NaviLog.d(NaviLog.PRINT_LOG_TAG,"142  true");
//			if (showa) {
//				value = this.getResources().getString(R.string.btn_A_name);
////				NaviLog.d(NaviLog.PRINT_LOG_TAG,"showa  true");
//			} else if (showka) {
//				value = this.getResources().getString(R.string.btn_KA_name);
////				NaviLog.d(NaviLog.PRINT_LOG_TAG,"showka  true");
//			} else if (showsa) {
//				value = this.getResources().getString(R.string.btn_SA_name);
////				NaviLog.d(NaviLog.PRINT_LOG_TAG,"showsa  true");
//			} else if (showta) {
//				value = this.getResources().getString(R.string.btn_TA_name);
////				NaviLog.d(NaviLog.PRINT_LOG_TAG,"showta  true");
//			} else if (showna) {
//				value = this.getResources().getString(R.string.btn_NA_name);
////				NaviLog.d(NaviLog.PRINT_LOG_TAG,"showna  true");
//			} else if (showha) {
//				value = this.getResources().getString(R.string.btn_HA_name);
////				NaviLog.d(NaviLog.PRINT_LOG_TAG,"showha  true");
//			} else if (showma) {
//				value = this.getResources().getString(R.string.btn_MA_name);
////				NaviLog.d(NaviLog.PRINT_LOG_TAG,"showma  true");
//			} else if (showya) {
//				value = this.getResources().getString(R.string.btn_YA_name);
////				NaviLog.d(NaviLog.PRINT_LOG_TAG,"showya  true");
//			} else if (showra) {
//				value = this.getResources().getString(R.string.btn_RA_name);
////				NaviLog.d(NaviLog.PRINT_LOG_TAG,"showra  true");
//			} else if (showwa) {
//				value = this.getResources().getString(R.string.btn_WA_name);
////				NaviLog.d(NaviLog.PRINT_LOG_TAG,"showwa  true");
//			}
//		} else {
//
////			value = oGroupValue.getText().toString();
//			value = groupValue;
//
//		}
		//yangyang del end Bug617
//		Toast toast = Toast.makeText(this, null, Toast.LENGTH_SHORT);
//		toast.setText(value);
//		toast.show();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy value===" + value);

		checkGroupBtnShow(JumpKey,JumpRecCount,false);
//		initGroupButton();
		initScrollGroupShowValue(JumpKey,JumpRecCount);
//		if (-1 != Index.lcount) {
//			oGroupValue.setText(getGroupValue((int)Index.lcount));
//		} else {
//			oGroupValue.setText(checkGroupBtnShow(this.getResources().getString(R.string.btn_A_name)));
//		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POI_Facility_GetJumpKey2RecordNumber before===" + NameSize);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POI_Facility_GetJumpKey2RecordNumber before===" + msName);
//		if (!AppInfo.isEmpty(msName)) {
//			initGroupIndex();
//		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"allRecordCount===" +allRecordCount);
	}

	ArrayList<String[]> listGroupValueIndex = new ArrayList<String[]>();
	private void initScrollGroupShowValue(JNIJumpKey[] jumpKey2,
			JNIShort jumpRecCount2) {
		for (int i = 0 ; i < jumpRecCount2.getM_sJumpRecCount(); i++) {
			String msName= jumpKey2[i].pucName;
			long NameSize= jumpKey2[i].NameSize;
			String[] sValue = new String[2];

			JNILong JumpRecIndex = new JNILong();
			NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetJumpKey2RecordNumber(NameSize, msName, JumpRecIndex);
			sValue[0] = String.valueOf(JumpRecIndex.lcount);
			sValue[1] = msName;
			listGroupValueIndex.add(sValue);

		}

	}

	protected String getGroupValue(int startIndex) {
		String sResult = "";
//		int localIndex = 0;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"listGroupValueIndex===" + listGroupValueIndex.size());
		for (int i = 0 ; i < listGroupValueIndex.size() ; i++) {
			String[] sValue = (String[])listGroupValueIndex.get(i);
			int index = Integer.parseInt(sValue[0]);
			//yangyang mod start bug617
//			if (index <= startIndex) {
//				if (AppInfo.isEmpty(sResult)) {
//					sResult = sValue[1];
//					localIndex= index;
//				} else {
//					if (localIndex <= index) {
//						sResult = sValue[1];
//						localIndex= index;
//					}
//				}
//
//			}
			if (index -1 == startIndex) {
				sResult = sValue[1];
//				scrollGroupIndex= index;
			}
			//yangyang mod end bug617
		}
		//yangyang mod start Bug617
//		if (AppInfo.isEmpty(sResult)) {
//			sResult = oGroupValue.getText().toString();
//		}
		//yangyang mod end Bug617
//		Toast toast = Toast.makeText(this, null, Toast.LENGTH_SHORT);
//		toast.setText(sResult);
//		toast.show();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"sResult====" + sResult);
		return checkGroupBtnShow(sResult);
	}
	@Override
	protected void doAllCityClick(Button oBtn) {
		Intent oIntent = new Intent(getIntent());
		oIntent.putExtra(Constants.FROM_WHICH_SEARCH_KEY, "AllCity");
		String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY) +
		Constants.FLAG_TITLE + oBtn.getText().toString();
		oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
		oIntent.setClass(this, KeyResultInquiry.class);
//Chg 2011/09/23 Z01yoneya Start -->
//		startActivityForResult(oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
		NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
//		this.createDialog(Constants.DIALOG_WAIT, -1);
	}

	@Override
	protected void initDataObj() {

		initOnceGetList();
		if(JumpKey  == null){
			JumpKey = new JNIJumpKey[50];
			for (int i = 0; i < 50; i++) {
				JumpKey[i] = new JNIJumpKey();
			}
		}

		m_Poi_Show_Listitem = new POI_Facility_ListItem[ONCE_GET_COUNT*2];
		for (int i = 0 ; i < ONCE_GET_COUNT*2 ; i++) {
			m_Poi_Show_Listitem[i] = new POI_Facility_ListItem();
		}
		if (null == ListCount) {
			ListCount = new JNILong();
		}
	}

	private void initOnceGetList() {
		m_Poi_Key_Listitem = new POI_Facility_ListItem[ONCE_GET_COUNT];
		for (int i = 0 ; i < ONCE_GET_COUNT ; i++) {
			m_Poi_Key_Listitem[i] = new POI_Facility_ListItem();
		}

	}

	@Override
	protected void initCount() {
		Intent oIntent = getIntent();
		if (null == ListCount) {
			ListCount = new JNILong();
		}
		int iProvinceId = oIntent.getIntExtra(Constants.PARAMS_PROVINCE_ID, 0);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yangyang JNI_NE_POI_Facility_GetMunicipalityRecListCount iProvinceId=====" + (long)iProvinceId);
//		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetPointList(iProvinceId,1);
		//全市区町村listのレコード数を取得する
		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetMunicipalityRecListCount((long)iProvinceId,ListCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yangyang JNI_NE_POI_Facility_GetMunicipalityRecListCount ListCount=====" + ListCount.lcount);
		//500件のチェック
		//JNIから取得した総件数には「市区町村」が含む、「-1」の必要がある
		showToast(this,ListCount);
		allRecordCount = initPageIndexAndCnt(ListCount);
		allRecordCount = allRecordCount-1;
	}

	@Override
	protected String getButtonValue(int wPos) {
//		if (wPos >= getCountInBox()) {
//			int pageIndex  = wPos/getCountInBox();
//			wPos = wPos-getCountInBox()*pageIndex;
//		}
		wPos = resetIndex(wPos);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"wPos===" + wPos);
//		return m_Poi_Key_Listitem[wPos].getM_Name();
		return m_Poi_Show_Listitem[wPos].getM_Name();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent oIntent) {
		if (resultCode == Activity.RESULT_CANCELED) {
			if (requestCode == AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY) {
				String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
				if (-1 != title.lastIndexOf(Constants.FLAG_TITLE)) {
					title = title.substring(0, title.lastIndexOf(Constants.FLAG_TITLE));
				}
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"title====" + title);
				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
//				initCount();
//				oBox.refresh();
//				initDataObj();

				//yangyang del start 20110413
//				initCount();
				//yangyang del end 20110413
//				oList.setSelection(0);
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"oBox.setM_wIndex((int)RecIndex);=========" + RecIndex);

//				oBox.refresh();
//				initDialog();
			}
		}
//		this.setResult(resultCode, oIntent);
		super.onActivityResult(requestCode, resultCode, oIntent);
	}

	@Override
	protected boolean onClickGoMap() {

		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_Clear();
		return super.onClickGoMap();
	}

	@Override
	protected boolean onClickGoBack() {
		doBack();

		return super.onClickGoBack();
	}

	private void doBack() {
		Intent oIntent = getIntent();
		String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
		if (-1 != title.lastIndexOf(Constants.FLAG_TITLE)) {
			title = title.substring(0, title.lastIndexOf(Constants.FLAG_TITLE));
		}
		oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
		this.setResult(Activity.RESULT_CANCELED,getIntent());
		finish();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			doBack();
		}
		return super.onKeyDown(keyCode, event);
	}


}
