package net.zmap.android.pnd.v2.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DC_ContentsItemData implements Serializable{
	private static final long serialVersionUID = 2556847619577309731L;
	public final static int ITEM_TYPE_UNKNOWN = 0;
	public final static int ITEM_TYPE_SWITCH = 1;
	public final static int ITEM_TYPE_SELECT = 2;
	public final static int ITEM_TYPE_MULTI_SELECT = 3;
	public final static int ITEM_TYPE_SLIDER = 4;
	public final static int ITEM_TYPE_DATETIME = 5;
	public final static int ITEM_TYPE_DATE = 6;
	public final static int ITEM_TYPE_TIME = 7;
	public final static int MULTI_ALL_TYPE_NOTHING = 0;
	public final static int MULTI_ALL_TYPE_ON = 1;
	public final static int MULTI_ALL_TYPE_OFF = 2;

	public String 	m_sItemKey;			//アイテムキー
	public String	m_sItemText;		//アイテムテキスト
	public int	 	m_nItemKind;		//アイテム種別
	public String 	m_sItemDefaultValue = null;		//アイテムのデフォルト値
	public int	 	m_nMinimum = 0;		//アイテム最小値
	public int	 	m_nMaximum = 0;		//アイテム最大値
	public int	 	m_nGroupNum = 0;	//グループ数
	public int		m_nAllType  = MULTI_ALL_TYPE_NOTHING;		//マルチセレクトのallボタン設定
	public boolean		m_bAllSelect  = false;		//マルチセレクト時のallボタン押下状態
	public Set<String> 	m_sItemValueList = null;		//アイテムの戻り値
	public List<DC_SectionOptionData> m_OptionList = new ArrayList<DC_SectionOptionData>();	//オプションリスト
}
