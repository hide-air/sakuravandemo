package net.zmap.android.pnd.v2.api.overlay;

import android.os.Parcel;
import android.os.Parcelable;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.exception.NaviInvalidValueException;
import net.zmap.android.pnd.v2.api.poi.Content;
import net.zmap.android.pnd.v2.api.util.GeoCheckUtils;

/**
 * アイコン情報
 */
public final class Icon extends OverlayItem {

    /*　座標 */
    private GeoPoint mPoint;

    /*　アイコン画像ファイルパス */
    private String   mUrl;

    /*  */
    private String   mDescription;

    /* コンテンツ */
    private Content  mContent;

    /**
     *  アイコン情報ビルダー
     */
    public static class Builder {
        private final GeoPoint point;
        private final String   url;
        private String         description;
        private Content        content;

        /**
         *  コンストラクタ
         */
        public Builder(GeoPoint point, String url) {
            this.point = point;
            this.url = url;
        }

        /**
         *  コンストラクタ
         */
        public Builder(GeoPoint point, String url, String description) {
            this.point = point;
            this.url = url;
            this.description = description;
        }

        /**
         *  コンストラクタ
         */
        public Builder(double latitude, double longitude, String url) {
            this.point = new GeoPoint(latitude, longitude);
            this.url = url;
        }

        /**
         *  コンストラクタ
         */
        public Builder(double latitude, double longitude, String url, String description) {
            this.point = new GeoPoint(latitude, longitude);
            this.url = url;
            this.description = description;
        }

        /**
         * 説明を設定
         * @param description 説明
         * @return アイコン情報ビルダー
         */
        public Builder description(String description) {
            this.description = description;
            return this;
        }

        /**
         * コンテンツ情報設定
         * @param content コンテンツ情報
         * @return アイコン情報ビルダー
         */
        public Builder content(Content content) {
            this.content = content;
            return this;
        }

        /**
         * アイコン情報生成
         * @return アイコン情報
         */
        public Icon build() {
            return new Icon(this);
        }
    }

    private Icon(Builder builder) {
        this.mUrl = builder.url;
        this.mPoint = builder.point;
        this.mDescription = builder.description;
        this.mContent = builder.content;
    }

    /**
     * コンストラクタ
     */
    public Icon() {
        super();
    }

    /**
     * アイコン画像ファイルパス取得
     *
     * @return　アイコン画像ファイルパス
     */
    public String getUrl() {
        return mUrl;
    }

    /**
     * アイコン画像ファイルパス設定
     *
     * @param url
     *            アイコン画像ファイルパス
     */
    public void setUrl(String url) {
        mUrl = url;
    }

    /**
     * アイコン座標取得
     *
     * @return アイコン座標
     */
    public GeoPoint getPoint() {
        return mPoint;
    }

    /**
     * アイコン座標設定
     *
     * @param point
     *            アイコン座標
     * @throws NaviInvalidValueException 不正な値、無効値
     */
    public void setPoint(GeoPoint point) throws NaviInvalidValueException {
        if (!validateRangeLocation(point)) {
            throw new NaviInvalidValueException();
        }
        mPoint = point;
    }

    /**
     * アイコン座標設定
     *
     * @param latitude
     *            経度
     * @param longitude
     *            緯度
     * @throws NaviInvalidValueException 不正な値、無効値
     */
    public void setPoint(double latitude, double longitude) throws NaviInvalidValueException {
        GeoPoint point = new GeoPoint(latitude, longitude);
        if (!validateRangeLocation(point)) {
            throw new NaviInvalidValueException();
        }

        mPoint = point;
    }

    /**
     * 説明を取得
     * @return 説明
     */
    public String getDescription() {
        return mDescription;
    }

    /**
     * 説明を設定
     * @param description 説明
     */
    public void setDescription(String description) {
        this.mDescription = description;
    }

    /**
     * コンテンツ情報取得
     * @return コンテンツ情報
     */
    public Content getContent() {
        return mContent;
    }

    /**
     * コンテンツ情報設定
     * @param content コンテンツ情報
     */
    public void setContent(Content content) {
        this.mContent = content;
    }

    /**
     * コンテンツ情報有無判定
     * @return true : コンテンツ情報あり　/ false : コンテンツ情報なし
     */
    public boolean hasContent() {
        return mContent != null && mContent.hasContent();
    }

    /**
     * ポイント座標有効範囲内判定
     *
     * @return true : 範囲内 / false : 範囲外
     */
    private boolean validateRangeLocation(GeoPoint point) {
        if (point == null) {
            return false;
        }
        return GeoCheckUtils.inBoundMapWGS(point);
    }

    /**
     * IconPath 空判定
     * @return true : 空 / false : 空でない
     */
    public boolean isEmptyIconPath() {

        if( mUrl == null || mUrl.trim().length() == 0 )
        {
            return true;
        }
        return false;
    }

    private Icon(Parcel in) {
        super(in);
        mPoint = in.readParcelable(GeoPoint.class.getClassLoader());
        mUrl = in.readString();
        mDescription = in.readString();
        mContent = in.readParcelable(Content.class.getClassLoader());
    }

    /* (非 Javadoc)
     * @see net.zmap.android.pnd.v2.agenthmi.api.data.NaviPoint#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeParcelable(mPoint, flags);
        out.writeString(mUrl);
        out.writeString(mDescription);
        out.writeParcelable(mContent, flags);
    }

    /* (非 Javadoc)
     *
     */
    public static final Parcelable.Creator<Icon> CREATOR = new Parcelable.Creator<Icon>() {

        public Icon createFromParcel(Parcel in) {
            return new Icon(in);
        }

        public Icon[] newArray(int size) {
            return new Icon[size];
        }
    };

    /*
     * (非 Javadoc)
     * @see net.zmap.android.pnd.v2.api.overlay.OverlayItem#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }
}
