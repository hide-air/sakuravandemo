package net.zmap.android.pnd.v2.route.data;

/**
 ********************************************************************
 * Copyright (c) 2010.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project
 * File           RouteData.java
 * Description    無し
 * Created on     2010/12/02
 *
 ********************************************************************
 */
import java.util.ArrayList;
import java.util.List;

/*
 * ルート情報クラス
 *
 * ルートのファイル保存・読み込みなどで使用する情報。
 *
 *
 */
public class RouteData {
    private String mRouteDate;
    private int mRouteMode = 0; //車・人モード
    private String mRouteName;

    private List<RouteNode> mRouteNodeDataList = new ArrayList<RouteNode>();

    public class RouteNode {
        private long mLatMSec;
        private long mLonMSec;
        private String mName;
        private int mStatus; //探索条件

        RouteNode(long lat, long lon, String name, int status) {
            mLatMSec = lat;
            mLonMSec = lon;
            mName = name;
            mStatus = status;
        }
    };

    /*
     * ルート保存日時の設定
     *
     * システムミリ秒を"yyyyMMddHHmmSS"でフォーマットした時刻。SSはミリ秒なので注意。
     */
    public void setRouteDate(String date) {
        mRouteDate = date;
    }

    /*
     * ルート保存日時の取得
     */
    public String getRouteDate() {
        return mRouteDate;
    }

    /*
     * ルート名称の設定
     */
    public void setRouteName(String name) {
        mRouteName = name;
    }

    /*
     * ルート名称の取得
     */
    public String getRouteName() {
        return mRouteName;
    }

    //ルートモードの定義
    public final int ROUTE_MODE_CAR = 0;
    public final int ROUTE_MODE_MAN = 1;

    /*
     * モードの設定
     * 0:車　1:徒歩
     */
    public void setRouteMode(int mode) {
        mRouteMode = mode;
    }

    /*
     * モードの取得
     * 0:車　1:徒歩
     */
    public Integer getRouteMode() {
        return mRouteMode;
    }

    /*
     * ルートのノード(現在地、経由地、目的地)を追加する
     */
    public void addRouteNode(long lat, long lon, String name, int status) {
        mRouteNodeDataList.add(new RouteNode(lat, lon, name, status));
    }

    /*
     * 探索条件を取得する
     *
     * 探索条件はルートノード間ごとに異なることは無い(内部仕様)ので
     * ０番目のルートノードの探索条件を返す
     */
    public Integer getSearchProperty() {
        return mRouteNodeDataList.get(0).mStatus;
    }

    /*
     * ルートノードのリストを返す
     */
    public List<RouteNodeData> getRouteNodeDataList() {
        List<RouteNodeData> nodeList = new ArrayList<RouteNodeData>();

        for (int i = 0; i < mRouteNodeDataList.size(); i++) {
            RouteNode node = mRouteNodeDataList.get(i);

            RouteNodeData routeNodeData = new RouteNodeData();
            routeNodeData.setDate(getRouteDate());
            routeNodeData.setLat(Long.toString(node.mLatMSec));
            routeNodeData.setLon(Long.toString(node.mLonMSec));
            routeNodeData.setMode(getRouteMode().toString());
            routeNodeData.setStatus(Integer.toString(node.mStatus));
            routeNodeData.setName(node.mName);
            nodeList.add(routeNodeData);
        }

        return nodeList;
    }
}
