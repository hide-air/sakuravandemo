package net.zmap.android.pnd.v2.common;

import android.app.Activity;
import android.content.Intent;

import net.zmap.android.pnd.v2.app.NaviAppStatus;
import net.zmap.android.pnd.v2.common.utils.NaviLog;

/*
 * アクティビティを起動するクラス
 *
 * アプリケーションが終了中などのアプリステータス(NaviAppStatus)を参照して
 * アクティビティを起動できるか判断してから起動する。
 */
public class NaviActivityStarter {

    /**
     * ナビ状態を考慮したアクティビティ起動
     *
     * アクティビティを起動できない状態の場合は
     * 例外を発生する
     *
     * @param activity
     *            呼び出しアクティビティ
     * @param intent
     *            startActivityForResultに渡すIntent
     * @param requestCode
     *            startActivityForResultに渡すrequestCode
     * @return なし
     * @throws IllegalStateException
     */
    public static void startActivityForResult(Activity activity, Intent intent, int requestCode)
            throws IllegalStateException {
        if (NaviAppStatus.isAppFinishing()) {
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "#-- startActivityForResult error!! isAppFinishing()!! reqCode=" + requestCode + "from=" + activity.toString() + " to=" + intent.getClass().toString());
            throw new IllegalStateException();
        }

        NaviLog.d(NaviLog.PRINT_LOG_TAG, "#-- startActivityForResult reqCode=" + requestCode + "from=" + activity.toString() + " to=" + intent.getClass().toString());

        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * ナビ状態を考慮したアクティビティ起動
     *
     * アクティビティを起動できない状態の場合は
     * 例外を発生する
     *
     * @param activity
     *            呼び出しアクティビティ
     * @param intent
     *            startActivityに渡すIntent
     * @param requestCode
     *            startActivityに渡すrequestCode
     * @return なし
     * @throws IllegalStateException
     */
    public static void startActivity(Activity activity, Intent intent)
            throws IllegalStateException {
        if (NaviAppStatus.isAppFinishing()) {
            //NaviLog.d("NaviActivityStarter", "#-- startActivity error!! isAppFinishing()!!" + "from=" + activity.toString() + " to=" + intent.getClass().toString());
            throw new IllegalStateException();
        }

        NaviLog.d(NaviLog.PRINT_LOG_TAG, "#-- startActivity " + "from=" + activity.toString() + " to=" + intent.getClass().toString());

        activity.startActivity(intent);
    }

}
