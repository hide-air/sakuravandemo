package net.zmap.android.pnd.v2.common;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.exception.NaviInvalidValueException;
import net.zmap.android.pnd.v2.api.overlay.Icon;
import net.zmap.android.pnd.v2.common.utils.DbHelper;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;


// 軌跡データ情報を格納するクラス
public class TrackDataInfo {
	public int m_ID;       			// インデックス
    public String mCourseName;		// コース名
    public String mSaveDir;			// 保存先フォルダ名
    public String mFileName;		// 保存ファイル名
	
}

