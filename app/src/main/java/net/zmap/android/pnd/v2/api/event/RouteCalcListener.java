package net.zmap.android.pnd.v2.api.event;

/**
 * ルート探索イベントリスナタフェース.<br>
 * ルート探索完了時に呼び出されるリスナインターフェース(車モードのみ)<br>
 */
public interface RouteCalcListener {

    /**
     * ルート探索完了時に呼び出されるリスナ関数.<br>
     * <br>
     * 探索に利用した探索情報({@link net.zmap.android.pnd.v2.api.navigation.RouteRequest})を提供します。<br>
     * 5ルート探索を行った場合、探索情報::探索種別は {@link net.zmap.android.pnd.v2.api.navigation.RouteRequest#SEARCH_TYPE_NONE}で情報を送信します<br>
     * <br>
     * 下記の場合、リスナは呼び出されません.<br>
     * <ul>
     * <li>リルート
     * <li>ルート探索失敗
     * <li>車以外のルート探索
     * </ul>
     *
     * @param routeCalculated
     *            ルート探索終了情報
     *
     */
    public void onRouteCalculated(RouteCalculated routeCalculated );
}
