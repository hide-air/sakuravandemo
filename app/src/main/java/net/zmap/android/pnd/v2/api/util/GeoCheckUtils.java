package net.zmap.android.pnd.v2.api.util;

import net.zmap.android.pnd.v2.api.GeoPoint;
import android.location.Location;

/**
 * 座標チェッククラス
 */
public class GeoCheckUtils {

    /**
     * 指定座標が地図範囲内にあるか
     *
     * @param locate
     *            経緯度(世界測地系)
     * @return true: 範囲内 /false: 範囲外
     */
    public static boolean inBoundMapWGS(Location locate) {
        return inBoundMapWGS(locate.getLatitude(), locate.getLongitude());
    }

    /**
     * 指定座標が地図範囲内にあるか
     *
     * @param coordinate
     *            経緯度(世界測地系)
     * @return true: 範囲内 /false: 範囲外
     */
    public static boolean inBoundMapWGS(GeoPoint coordinate) {
        return inBoundMapWGS(coordinate.getLatitude(), coordinate.getLongitude());
    }

    /**
     * 指定座標が地図範囲内にあるか
     *
     * @param latitude
     *            緯度(世界測地系)
     * @param longitude
     *            経度(世界測地系)
     * @return true: 範囲内 /false: 範囲外
     */
    public static boolean inBoundMapWGS(double latitude, double longitude) {
        return latitude <= 90.0 && latitude >= -90.0 &&
                longitude >= -180.0 && longitude <= 180.0;
    }
}