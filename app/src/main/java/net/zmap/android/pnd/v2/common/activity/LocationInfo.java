/**
 ********************************************************************
 * Copyright (c) 2009.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project        MarketV2
 * File           RouteEdit.java
 * Description    走行中の操作制限半透明画面
 * Created on     2010/12/17
 *
 ********************************************************************
 */

package net.zmap.android.pnd.v2.common.activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.services.CarMountStatusEventListener;
import net.zmap.android.pnd.v2.common.services.CarMountStatusService;

/**
 * Created on 2010/12/17
 * <p>
 * Title: 走行中の操作制限半透明画面
 * <p>
 * Description
 * <p>
 * author　XuYang
 * version 1.0
 */
public class LocationInfo extends BaseActivity implements CarMountStatusEventListener {
	private Button btnCommonGoMap = null;
	public CarMountStatusService mCarMountStatusService = null;
	/**
	 * 初期化処理
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 画面を追加
		setContentView(R.layout.location_only);

		// 現在地ボタンを取得
		btnCommonGoMap = (Button)this.findViewById(R.id.btnCommonGoMap);
		btnCommonGoMap.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				goMap();
			}
		});
		mCarMountStatusService = new CarMountStatusService(this,this);
	}
	@Override
    protected void onDestroy()
    {
        if (mCarMountStatusService != null) {
            mCarMountStatusService.release();
        }
        super.onDestroy();
    }
	/**
	 * 現在地ボタンの処理
	 */
	public void goMap() {
		CommonLib.locusMap(this);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			CommonLib.locusMap(this);
			this.finish();
		}
//
//		// 走行／停止
//		if (keyCode == KeyEvent.KEYCODE_R && event.getAction() == KeyEvent.ACTION_DOWN) {
//			Constants.RunStopflag = true;
//		}
//		if (keyCode == KeyEvent.KEYCODE_S && event.getAction() == KeyEvent.ACTION_DOWN) {
//			Constants.RunStopflag = false;
//		}
		// クレードル（車載ホルダ）
		if (keyCode == KeyEvent.KEYCODE_O && event.getAction() == KeyEvent.ACTION_DOWN) {
			Constants.NaviOnOffflag = true;
		}
		if (keyCode == KeyEvent.KEYCODE_F && event.getAction() == KeyEvent.ACTION_DOWN) {
			Constants.NaviOnOffflag = false;
		}

		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			if (!CommonLib.isLimit()) {
				setLimit(false);
				this.finish();
			} else {
				setLimit(true);
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	public void setLimit(boolean flg) {

	}
	// XuYang add start 走行中
    @Override
    public void onCarMountEventChange(Intent oIntent) {
    }
    // XuYang add end 走行中
}
