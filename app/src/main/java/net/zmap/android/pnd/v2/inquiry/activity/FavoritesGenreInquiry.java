//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.ScrollBoxAdapter;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Mybox_ListGenreCount;
import net.zmap.android.pnd.v2.data.POI_UserFile_RecordIF;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.inquiry.view.ItemView;
import net.zmap.android.pnd.v2.maps.OpenMap;
/**
 * K-F0_お気に入りジャンル一覧
 *
 * */
public class FavoritesGenreInquiry extends InquiryBaseLoading implements ScrollBoxAdapter {

	private Intent oIntent = null;
//	protected ScrollBox oBox = null;
	protected ScrollList oList = null;
	protected ScrollTool oTool = null;
	protected JNILong Rec = new JNILong();
	private POI_UserFile_RecordIF[] m_Poi_UserFile_Listitem = null;
	private POI_UserFile_RecordIF[] m_Poi_Show_Listitem = null;
	private JNILong RecCount = new JNILong();
	private POI_Mybox_ListGenreCount[] Count;
	protected int KIND_OF_FRIEND = 0;
	protected int KIND_OF_EAT = 1;
	protected int KIND_OF_SHOPPING = 2;
	protected int KIND_OF_INTEREST = 3;
	protected int KIND_OF_SIGHTSEEING = 4;
	protected int KIND_OF_WORK = 5;
	protected int KIND_OF_OTHERS = 6;
	protected int KIND_OF_HOUSE = 7;
	protected int HOUSE_COUNT = 2;
	protected int real_house_count = 0;
//	private int SCROLL_UP= 1;
//	private int SCROLL_DOWN= 0;
	private String sTitle = "";
	protected int recordCount = 0;
	private ItemView m_oGroupView = null ;//グループボタンのレイアウト対象
	private int getRecordIndex = 0;
	private static int genreCount = 0;

	/** 0:初期化　1：ジャンルタイプ */
	private int waitDialogType = 0;
	private int GENRE_BUTTON = 1;
	private int DEFAULT_SORT_BUTTON = 0;
//	private static FavoritesGenreInquiry instance;

    /** 現在の接続リクエストのid */
    protected String m_sReqId = null;
	//<--
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		instance = this;
//		NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
//		NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetGenreCount(RecCount);


		initPoiDataObj();
		oIntent = getIntent();
		sTitle = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
		if (AppInfo.isEmpty(sTitle)) {
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
			initRecordCount();
			initDialog();
		} else {
			waitDialogType = DEFAULT_SORT_BUTTON;
			this.startShowPage(NOTSHOWCANCELBTN);
		}

	}

//	public static FavoritesGenreInquiry getInstance(){
//	    return instance;
//	}

	private void initDialog() {
		oIntent = getIntent();
//Del 2011/09/17 Z01_h_yamada Start -->
//		int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE,0);
//		if (0!= titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--
		LayoutInflater oInflater = LayoutInflater.from(this);
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base, null);
		Button obtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
		obtn.setVisibility(Button.GONE);

		TextView oText = (TextView)oLayout.findViewById(R.id.inquiry_title);

		if(sTitle == null)
		{
			oText.setText(R.string.genre_title);
		}
		else
		{
			oText.setText(sTitle);
		}



		m_oGroupView = new ItemView(this,0,R.layout.inquiry_favorites_genre_freescroll);
		initGroupFromJNI(m_oGroupView);
//		oBox = (ScrollBox)m_oGroupView.findViewById(this,R.id.scrollBox);
//		oBox.setAdapter(this);

		oList = (ScrollList)m_oGroupView.findViewById(this,R.id.scrollList);

//Del 2011/10/06 Z01_h_yamada Start -->
//		oList.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
		oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
//Del 2011/07/21 Z01thedoanh Start -->
		//oList.setPadding(5, 5, 2, 0);
//Del 2011/07/21 Z01thedoanh End <--
		LinearLayout oGroupList = (LinearLayout)oLayout.findViewById(R.id.LinearLayout_list);
		oGroupList.addView(m_oGroupView.getView(), new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT));
//
//		final ScrollBox oBox = (ScrollBox)oLayout.findViewById(R.id.scrollBox);
//		oBox.setAdapter(this);
//
		oList.setAdapter(listAdapter);


		setViewInWorkArea(oLayout);
		oTool = new ScrollTool(this);
//		oTool.bindView(oBox);
		oTool.bindView(oList);
		//yangyang del start Bug1062
		//yangyang mod start Bug1035
//		if(!AppInfo.isEmpty(sTitle)) {
		//yangyang mod end Bug1035
		//yangyang del end Bug1062
			setViewInOperArea(oTool);
			//yangyang del start Bug1062
		//yangyang mod start Bug1035
//		}
		//yangyang mod end Bug1035
			//yangyang del end Bug1062
	}

	private ListAdapter listAdapter = new BaseAdapter (){

		@Override
		public int getCount() {
			int iCount = FavoritesGenreInquiry.this.getCount();
			if (iCount <5) {
				return 5;
			} else {
				return iCount;
			}

		}

		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int wPos, View oView, ViewGroup parent) {

			NaviLog.d(NaviLog.PRINT_LOG_TAG,"getView wPos--------------->" + wPos);
			return FavoritesGenreInquiry.this.getView(wPos, oView);
		}

	};
	@Override
	public int getCount() {
		if (AppInfo.isEmpty(sTitle)) {
			return 0;
		} else {
			return recordCount;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent oIntent) {
		if (resultCode == Activity.RESULT_CANCELED) {
			if (requestCode == AppInfo.ID_ACTIVITY_FAVORITESINQUIRY) {
				initGroupFromJNI(m_oGroupView);
			}
		}

		super.onActivityResult(requestCode, resultCode, oIntent);
	}

	@Override
	public int getCountInBox() {

		return 5;
	}

//	private void initPoiDataObj()() {
//
//	}
	int wOldPos = 0;
	private long dataRecIndex = 0;
	@Override
	public View getView(final int wPos, View oView) {
		LinearLayout oListLayout = null;
//		long RecIndex = 0;
//		int pageIndex = 0;
		if (oView == null || !(oView instanceof LinearLayout)) {
			LayoutInflater oInflater = LayoutInflater.from(this);
			oListLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_distance_button_freescroll, null);

			oView  = oListLayout;
//			if (wPos == 0) {
//				initPoiDataObj();
//
//			}


		} else {
//			if (wPos%getCountInBox() == 0) {
//				//ページIndexを取得する
//				pageIndex = wPos/getCountInBox();
//				RecIndex = getCountInBox() * pageIndex;
//			}
			oListLayout = (LinearLayout)oView;
		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"1111 wPos=====wOldPos========" + wPos + "======" + wOldPos);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"1111 getSelectedItemId=============" +oList.getSelectedItemId());
		LinearLayout oBtn = (LinearLayout) oListLayout.findViewById(R.id.LinearLayout_distance);
		oBtn.setBackgroundResource(R.drawable.btn_default);
		int flagKey = oIntent.getIntExtra(Constants.FROM_FLAG_KEY,-1);
		if (flagKey != -1 && flagKey == AppInfo.ID_ACTIVITY_MAINMENU) {
			TextView oDistanceiew = (TextView) oBtn.findViewById(R.id.TextView_distance);
			oDistanceiew.setVisibility(TextView.GONE);
//		} else {
//			TextView oDistanceiew = (TextView) oBtn.findViewById(R.id.TextView_distance);
////			oDistanceiew.setText(m_Poi_UserFile_Listitem[wPos%getCountInBox()]);
//			oDistanceiew.setVisibility(TextView.VISIBLE);
		}
		TextView oValue = (TextView) oBtn.findViewById(R.id.TextView_value);

		if (AppInfo.isEmpty(sTitle)) {
			oValue.setVisibility(TextView.GONE);//.setText("");

//			oValue.setEnabled(false);
			oBtn.setEnabled(false);
//			setEnabled(oListLayout,false);
		} else {
			initGroupList();
//			if (wPos%ONCE_GET_COUNT == 0) {
			// 下にスクロール或は、正常にデータを再取得する
//			if (wPos < getCount() && ((ONCE_GET_COUNT != 0 && wPos%ONCE_GET_COUNT == 0) ||
//
//					(wPos > wOldPos && wPos % ONCE_GET_COUNT < getCountInBox())
//					)) {
//				Intent oIntent = getIntent();
//				int genreIndex = oIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX, 0);
//				initGroupFromJNI(m_oGroupView);
//				//選択したジャンルの初期化
//				initSelectGroupBtn(genreIndex);
//
//				if (wOldPos > wPos) {
//					dataRecIndex = wPos-ONCE_GET_COUNT;
//
//				} else {
//					dataRecIndex = wPos/ONCE_GET_COUNT*ONCE_GET_COUNT;
//				}
//				if (dataRecIndex <0) {
//					dataRecIndex = 0;
//				}
//
//				m_Poi_UserFile_Listitem = reShowData(dataRecIndex,ONCE_GET_COUNT,genreIndex,m_Poi_UserFile_Listitem);
//				if (wOldPos > wPos) {
//					resetShowList(dataRecIndex,Rec,SCROLL_UP);
//				} else {
//					resetShowList(dataRecIndex,Rec,SCROLL_DOWN);
//				}
//
//				//上にスクロール
//			} else if (wOldPos - wPos > 0 && wOldPos - wPos <= getCountInBox() + 1 && dataRecIndex + ONCE_GET_COUNT > wPos) {
//				Intent oIntent = getIntent();
//				int genreIndex = oIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX, 0);
//				dataRecIndex = wPos/ONCE_GET_COUNT*ONCE_GET_COUNT;
//				m_Poi_UserFile_Listitem = reShowData(dataRecIndex,ONCE_GET_COUNT,genreIndex,m_Poi_UserFile_Listitem);
//				resetShowList(dataRecIndex,Rec,SCROLL_UP);
//			}


			//下にスクロールの場合
			Intent oIntent = getIntent();
			int genreIndex = oIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX, 0);
			if (wOldPos <= wPos) {
				if (wPos < getCount() && ONCE_GET_COUNT != 0
						&& (wPos % ONCE_GET_COUNT == 0 ||
								(getRecordIndex != wPos / ONCE_GET_COUNT))) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview  1==== wPos===="
//							+ wPos + ",==getRecordIndex==" + getRecordIndex
//							+ ",==(wPos / ONCE_GET_COUNT)=="
//							+ (wPos / ONCE_GET_COUNT));

					getRecordIndex = wPos / ONCE_GET_COUNT;

					if (wPos == 0) {
						initGroupFromJNI(m_oGroupView);
						//選択したジャンルの初期化
						initSelectGroupBtn(genreIndex);
					}
					dataRecIndex = wPos/ONCE_GET_COUNT*ONCE_GET_COUNT;
					m_Poi_UserFile_Listitem = reShowData(dataRecIndex,ONCE_GET_COUNT,genreIndex,m_Poi_UserFile_Listitem);
					resetShowList(dataRecIndex,Rec);
				}
			} else {
				if (getRecordIndex != wPos / ONCE_GET_COUNT) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview  2==== wPos===="
//							+ wPos + ",==getRecordIndex==" + getRecordIndex
//							+ ",==(wPos / ONCE_GET_COUNT)=="
//							+ (wPos / ONCE_GET_COUNT));
					dataRecIndex = wPos/ONCE_GET_COUNT*ONCE_GET_COUNT;
					getRecordIndex = wPos / ONCE_GET_COUNT;
					m_Poi_UserFile_Listitem = reShowData(dataRecIndex, ONCE_GET_COUNT,
							genreIndex, m_Poi_UserFile_Listitem);
					resetShowList(dataRecIndex,Rec);
				}
			}


//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"1111 RecIndex====" + RecIndex);
//			if (null != m_Poi_UserFile_Listitem && m_Poi_UserFile_Listitem[wPos%getCountInBox()] != null
//					&& !AppInfo.isEmpty(m_Poi_UserFile_Listitem[wPos%getCountInBox()].getM_DispName())) {
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"m_Poi_UserFile_Listitem[wPos%getCountInBox()].getM_DispName()==" + m_Poi_UserFile_Listitem[wPos%getCountInBox()].getM_DispName());
//				oValue.setText(m_Poi_UserFile_Listitem[wPos%getCountInBox()].getM_DispName());
//				oValue.setVisibility(TextView.VISIBLE);
//				oBtn.setEnabled(true);
//				oBtn.setOnClickListener(new OnClickListener(){
//
//					@Override
//					public void onClick(View v) {
//						NaviLog.d(NaviLog.PRINT_LOG_TAG,"onclick=============" + wPos);
//						POI_UserFile_RecordIF poi = m_Poi_UserFile_Listitem[wPos%getCountInBox()];
//
////		                int ActiveType = 2;
////		                int TransType = 2;
////		                byte bScale = 0;
////		                long Longitude = 0;
////		                long Latitude = 0;
////		                JNIInt MapScale = new JNIInt();
////		                NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
////		                bScale = (byte) MapScale.getM_iCommon();
////		                // xuyang modi start
////		                //Latitude = m_Poi_UserFile_Listitem[Idx].getM_lLat();
////		                //Longitude = m_Poi_UserFile_Listitem[Idx].getM_lLon();
////		                Latitude = poi.getM_lLat();
////		                Longitude = poi.getM_lLon();
////		                // xuyang modi end
////		                NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(ActiveType, TransType, bScale,
////		                        Longitude, Latitude);
////		                NaviRun.GetNaviRunObj().setSearchKind(Constants.MYBOX_SEARCH_KIND);
//						POIData oData = new POIData();
//		                oData.m_sName = poi.getM_DispName();
//		                oData.m_sAddress = poi.getM_DispName();
//		                oData.m_wLong = poi.getM_lLon();
//		                oData.m_wLat = poi.getM_lLat();
//		                OpenMap.moveMapTo(FavoritesGenreInquiry.this,oData, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
//
//
//					}});
			if (null != getShowObj(wPos) && !AppInfo.isEmpty(getShowObj(wPos).getM_DispName())) {
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"getShowObj(wPos).getM_DispName()==" + getShowObj(wPos).getM_DispName());
				oValue.setText(getShowObj(wPos).getM_DispName());
				oValue.setVisibility(TextView.VISIBLE);
				if (getShowObj(wPos).getM_lLat() != 0 && getShowObj(wPos).getM_lLon() != 0) {

					oBtn.setEnabled(true);
				} else {
					oBtn.setEnabled(false);
				}

				oBtn.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・お気に入り選択禁止 Start -->
				    	if(DrivingRegulation.CheckDrivingRegulation()){
				    		return;
				    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・お気に入り選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

//Add 2012/02/07 katsuta Start --> #2698
//						NaviLog.d(NaviLog.PRINT_LOG_TAG, "FavoritesGenreInquiry onClick ================== bIsListClicked: " + bIsListClicked);
			   			if (bIsListClicked) {
			   				return;
			   			}
			   			else {
			   				bIsListClicked = true;
			   			}
//Add 2012/02/07 katsuta End <--#2698

						Intent oIntent = getIntent();
						int genreIndex = oIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX, 0);
						dataRecIndex = wPos/ONCE_GET_COUNT*ONCE_GET_COUNT;
						m_Poi_UserFile_Listitem = reShowData(dataRecIndex,ONCE_GET_COUNT,genreIndex,m_Poi_UserFile_Listitem);
						resetShowList(dataRecIndex,Rec);

						POI_UserFile_RecordIF poi = getShowObj(wPos);

						POIData oData = new POIData();
		                oData.m_sName = poi.getM_DispName();
		                oData.m_sAddress = poi.getM_DispName();
		                oData.m_wLong = poi.getM_lLon();

		                oData.m_wLat = poi.getM_lLat();
		                // XuYang add start #1056
                        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
                            CommonLib.setChangePosFromSearch(true);
                            CommonLib.setChangePos(false);
                        } else {
                            CommonLib.setChangePosFromSearch(false);
                            CommonLib.setChangePos(false);
                        }
                        // XuYang add end #1056
		                OpenMap.moveMapTo(FavoritesGenreInquiry.this,oData, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
		                createDialog(Constants.DIALOG_WAIT, -1);
					}});
			} else {
				oValue.setVisibility(TextView.GONE);
				oBtn.setEnabled(false);
			}

			//上にスクロールの場合
			if (wOldPos > wPos) {
				if (wPos < getCount() && ONCE_GET_COUNT != 0
						&& wPos >= ONCE_GET_COUNT && (wPos % ONCE_GET_COUNT == 0 ||
								(getRecordIndex != wPos / ONCE_GET_COUNT))) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"wOldPos > wPos  ==== wPos====" + wPos);
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview  3==== wPos===="
//							+ wPos + ",==getRecordIndex==" + getRecordIndex
//							+ ",==((wOldPos-ONCE_GET_COUNT) / ONCE_GET_COUNT)=="
//							+ ((wOldPos-ONCE_GET_COUNT) / ONCE_GET_COUNT));
					//上にスクロール場合
					getRecordIndex = (wOldPos-ONCE_GET_COUNT) / ONCE_GET_COUNT;
//					Intent oIntent = getIntent();
//					int genreIndex = oIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX, 0);
					dataRecIndex = (wOldPos-ONCE_GET_COUNT)/ONCE_GET_COUNT*ONCE_GET_COUNT;
					m_Poi_UserFile_Listitem = reShowData(dataRecIndex,ONCE_GET_COUNT,genreIndex,m_Poi_UserFile_Listitem);
					resetShowList(dataRecIndex,Rec);
				}
			}
			wOldPos = wPos;
		}

		return oView;
	}

	private void resetShowList(Long lRecIndex,JNILong Rec) {
		if (lRecIndex < ONCE_GET_COUNT) {
			int len = m_Poi_UserFile_Listitem.length;
			for (int i = 0 ; i < len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_UserFile_Listitem[i];
			}
		} else {

			if (lRecIndex >= ONCE_GET_COUNT*2) {
				for (int i = m_Poi_UserFile_Listitem.length-ONCE_GET_COUNT; i < ONCE_GET_COUNT*2 ; i++) {
					if (i <ONCE_GET_COUNT) {
						m_Poi_Show_Listitem[i] = m_Poi_Show_Listitem[i+ONCE_GET_COUNT];
					} else {
						m_Poi_Show_Listitem[i] = new POI_UserFile_RecordIF();
					}
				}
			}

			int len = (int)Rec.lcount;
			int j = 0;
			for (int i = ONCE_GET_COUNT ; i < ONCE_GET_COUNT + len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_UserFile_Listitem[j];
				j++;
			}
		}

	}
	private POI_UserFile_RecordIF getShowObj(int wPos) {
//		int pageIndex  = wPos/ONCE_GET_COUNT;
		int showIndex = getShowIndex(wPos);

//		//31件、41件の状態で、上にスクロールの問題を決めるために
//		if (pageIndex >= 2 && wOldPos > wPos) {
//			if (m_Poi_Show_Listitem[showIndex].getM_DispName() == null) {
//				showIndex = showIndex-ONCE_GET_COUNT;
//			}
//		} else if (wOldPos > wPos) {
//			if (showIndex-ONCE_GET_COUNT > 0) {
//				showIndex = showIndex-ONCE_GET_COUNT;
//			}
//		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"1111 showIndex=========" + showIndex);
		return m_Poi_Show_Listitem[showIndex];
	}

	private int getShowIndex(int wPos) {
		if (wPos >= ONCE_GET_COUNT*2) {
			int pageIndex  = wPos/ONCE_GET_COUNT;
			wPos = wPos - ONCE_GET_COUNT * pageIndex + ONCE_GET_COUNT;
		}
		return wPos;
	}
	private void initSelectGroupBtn(int genreIndex) {
		for (int i = KIND_OF_FRIEND; i < KIND_OF_HOUSE + 1 ; i++) {
			initAllGroupBtn(i,false);
		}
		initAllGroupBtn(genreIndex,true);

	}

	private void initAllGroupBtn(int genreIndex,boolean isSelected) {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"genreIndex=======" + genreIndex);
		switch (genreIndex) {

		case 0://KIND_OF_FRIEND
			initGroupItem(R.id.Btn_Group_friend,isSelected);
			break;
		case 1://KIND_OF_EAT://
			initGroupItem(R.id.Btn_Group_eat,isSelected);
			break;
		case 2://KIND_OF_SHOPPING://
			initGroupItem(R.id.Btn_Group_shopping,isSelected);
			break;
		case 3://KIND_OF_INTEREST://
			initGroupItem( R.id.Btn_Group_interest,isSelected);
			break;
		case 4://KIND_OF_SIGHTSEEING://
			initGroupItem( R.id.Btn_Group_sightseeing,isSelected);
			break;
		case 5://KIND_OF_WORK://
			initGroupItem( R.id.Btn_Group_work,isSelected);
			break;
		case 6://KIND_OF_OTHERS://
			initGroupItem( R.id.Btn_Group_other,isSelected);
			break;
		case 7://KIND_OF_HOUSE://
		case 8:
			initGroupItem( R.id.Btn_Group_myhome,isSelected);
			break;
		default:
			break;
		}

	}
	private void initRecordCount() {

		NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetGenreCount(RecCount);
		recordCount = (int) RecCount.lcount;

	}
	private void initGroupItem(int btnId,boolean isSelected) {
		Button btnGroupItem = (Button) m_oGroupView.findViewById(this,btnId);
		btnGroupItem.setSelected(isSelected);

	}

	/**
	 * 全部右側のデータを表示する
	 *
	 * */

	protected POI_UserFile_RecordIF[] reShowData(long start, int count, long genreId,POI_UserFile_RecordIF[] m_Poi_UserFile_Listitem) {

		return m_Poi_UserFile_Listitem;

	};


	/**
	 * データリストを初期化する
	 *
	 *
	 * */
	private void initGroupList() {
		if (AppInfo.isEmpty(sTitle)) {
			Count = new POI_Mybox_ListGenreCount[recordCount];
		}

	}


	/**
	 * データリストを初期化する
	 *
	 *
	 * */
	private void initPoiDataObj() {
		m_Poi_UserFile_Listitem = new POI_UserFile_RecordIF[ONCE_GET_COUNT];
		for (int i = 0; i < ONCE_GET_COUNT; i++) {
			m_Poi_UserFile_Listitem[i] = new POI_UserFile_RecordIF();
		}
		m_Poi_Show_Listitem = new POI_UserFile_RecordIF[ONCE_GET_COUNT*2];
		for (int i = 0 ; i < ONCE_GET_COUNT*2 ; i++) {
			m_Poi_Show_Listitem[i] = new POI_UserFile_RecordIF();
		}
	}



	/**
	 * グループボタンを初期化する
	 *
	 * */
	protected void initGroupFromJNI(ItemView oView) {

		if (genreCount == 0) {
			genreCount = (int) RecCount.lcount;
		}
		Count = new POI_Mybox_ListGenreCount[genreCount];

//		int len = (int)RecCount.lcount;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG," initGroupFromJNI genreCount====" + genreCount);
		for (int i = 0 ; i < genreCount; i++) {
			Count[i] = new POI_Mybox_ListGenreCount();
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecCount(i, Count[i]);
		}
		int localCount = 0;
		for (int i = 0 ; i < genreCount ; i++) {
			int genreId = (int) Count[i].getM_lGenreCode();

			switch (genreId) {
			case 1://this.KIND_OF_FRIEND
				initGroupItem(oView,R.id.Btn_Group_friend,(int)Count[i].getM_sRecordCount());
				break;
			case 2://Btn_Group_eat
				initGroupItem(oView, R.id.Btn_Group_eat,(int)Count[i].getM_sRecordCount());
				break;
			case 3:
				initGroupItem(oView, R.id.Btn_Group_shopping,(int)Count[i].getM_sRecordCount());
				break;
			case 4:
				initGroupItem(oView, R.id.Btn_Group_interest,Count[i].getM_sRecordCount());
				break;
			case 5:
				initGroupItem(oView, R.id.Btn_Group_sightseeing,(int)Count[i].getM_sRecordCount());
				break;
			case 6:
				initGroupItem(oView, R.id.Btn_Group_work,(int)Count[i].getM_sRecordCount());
				break;
			case 7:
				initGroupItem(oView, R.id.Btn_Group_other,(int)Count[i].getM_sRecordCount());
				break;
			case 8:
			case 9:
				localCount += (int)Count[i].getM_sRecordCount();
				if (localCount != 0) {
					localCount = HOUSE_COUNT;
				}
				initGroupItem(oView, R.id.Btn_Group_myhome,localCount);
				break;
			default:

				break;
			}
		}



//		recordCount = (int) RecCount.lcount;

	}
	/**
	 * グループボタンの表示状態をチェックする
	 * */
	private void initGroupItem(ItemView oView, int btnId, final int mSRecordCount) {
		Button btnGroupItem = (Button) oView.findViewById(this, btnId);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"mSRecordCount==" + mSRecordCount);
		if (mSRecordCount == 0) {
			btnGroupItem.setEnabled(false);
			btnGroupItem.setSelected(false);

		} else {
			btnGroupItem.setEnabled(true);
			btnGroupItem.setSelected(false);
			btnGroupItem.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・お気に入りｼﾞｬﾝﾙ選択禁止 Start -->
			    	if(DrivingRegulation.CheckDrivingRegulation()){
			    		return;
			    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・お気に入りｼﾞｬﾝﾙ選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
					int wId = oView.getId();
					Intent oIntent = getIntent();
					oView.setSelected(true);
					switch(wId) {
					case R.id.Btn_Group_friend:
						oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
								FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_friend));
						oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_FRIEND);

						break;
					case R.id.Btn_Group_work:
						oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
								FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_work));
						oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_WORK);
						break;
					case R.id.Btn_Group_eat:
						oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
								FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_eat));
						oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_EAT);
						break;
					case R.id.Btn_Group_shopping:
						oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
								FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_shopping));
						oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_SHOPPING);
						break;
					case R.id.Btn_Group_interest:
						oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
								FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_interest));
						oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_INTEREST);
						break;
					case R.id.Btn_Group_sightseeing:
						oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
								FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_sightseeing));
						oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_SIGHTSEEING);
						break;
					case R.id.Btn_Group_other:
						oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
								FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_other));
						oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_OTHERS);
						break;
					case R.id.Btn_Group_myhome:
						oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
								FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_myhome));
						oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_HOUSE);
						break;
					}
					if (AppInfo.isEmpty(sTitle)) {
						wOldPos= 0;
						oIntent.setClass(FavoritesGenreInquiry.this, FavoritesInquiry.class);
						oIntent.putExtra(Constants.PARAMS_RECORD_COUNT, mSRecordCount);
//Chg 2011/09/23 Z01yoneya Start -->
//						FavoritesGenreInquiry.this.startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_FAVORITESINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
						NaviActivityStarter.startActivityForResult(FavoritesGenreInquiry.this, oIntent, AppInfo.ID_ACTIVITY_FAVORITESINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
					} else {
						waitDialogType = GENRE_BUTTON;
						recordCount = mSRecordCount;
						startShowPage(NOTSHOWCANCELBTN);
					}
				}});
		}

	}
    protected void showWaitDialog(String sReqId,boolean bIsBack)
    {
        m_sReqId = sReqId;
    }
    protected void showErrorDialog()
    {

    }


//	/**
//	 * グループボタンのクリック事件を初期化する
//	 *
//	 * */
//	private OnClickListener onGroupBtnClick = new OnClickListener(){
//
//		@Override
//		public void onClick(View oView) {
//			int wId = oView.getId();
//			Intent oIntent = new Intent(getIntent());
////			NaviLog.d(NaviLog.PRINT_LOG_TAG,"onGroupBtnClick");
//			switch(wId) {
//			case R.id.Btn_Group_friend:
//				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
//						FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_friend));
//				oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_FRIEND);
//
//				break;
//			case R.id.Btn_Group_work:
//				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
//						FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_work));
//				oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_WORK);
//				break;
//			case R.id.Btn_Group_eat:
//				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
//						FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_eat));
//				oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_EAT);
//				break;
//			case R.id.Btn_Group_shopping:
//				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
//						FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_shopping));
//				oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_SHOPPING);
//				break;
//			case R.id.Btn_Group_interest:
//				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
//						FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_interest));
//				oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_INTEREST);
//				break;
//			case R.id.Btn_Group_sightseeing:
//				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
//						FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_sightseeing));
//				oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_SIGHTSEEING);
//				break;
//			case R.id.Btn_Group_other:
//				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
//						FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_other));
//				oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_OTHERS);
//				break;
//			case R.id.Btn_Group_myhome:
//				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
//						FavoritesGenreInquiry.this.getResources().getString(R.string.genre_title_myhome));
//				oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_HOUSE);
//				break;
//			}
//			if (AppInfo.isEmpty(sTitle)) {
//				oIntent.setClass(FavoritesGenreInquiry.this, FavoritesInquiry.class);
//				FavoritesGenreInquiry.this.startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_FAVORITESINQUIRY);
//			} else {
//				oBox.refresh();
//			}
//
//
//		}};



	@Override
    protected Dialog onCreateDialog(int id, Bundle args) {
        return super.onCreateDialog(id, args);
    }

    @Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			oIntent.removeExtra(Constants.PARAMS_SEARCH_KEY);

		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected boolean onClickGoBack() {
		if (AppInfo.isEmpty(sTitle)) {
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
		}
		return super.onClickGoBack();
	}

	@Override
	protected boolean onClickGoMap() {
		NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
		return super.onClickGoMap();
	}

	@Override
	protected boolean onStartShowPage() throws Exception {
		if (this.waitDialogType == this.GENRE_BUTTON) {

			wOldPos = 0;

			initPoiDataObj();

		}
		return true;
	}

	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {
		if(bGetData)
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					if (waitDialogType == GENRE_BUTTON) {
						TextView oText = (TextView)FavoritesGenreInquiry.this.findViewById(R.id.inquiry_title);
						sTitle = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
						oText.setText(sTitle);
						oList.reset();
						oList.scroll(0);

					} else {
						initDialog();
					}
				}


			});
		}
	}

}
