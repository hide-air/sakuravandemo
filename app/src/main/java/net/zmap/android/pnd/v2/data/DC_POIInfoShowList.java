package net.zmap.android.pnd.v2.data;

import java.io.Serializable;

public class DC_POIInfoShowList implements Serializable {
	private static final long serialVersionUID = 6742492430794846402L;
	public long				m_POIId;				//POIのID
	public String			m_sPOIName;				//POI名称
}