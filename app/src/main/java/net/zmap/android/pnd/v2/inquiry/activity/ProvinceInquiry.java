//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollBox;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.ScrollBoxAdapter;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNIThreeShort;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.inquiry.view.ButtonImg;

/**
 * 都道府県画面の共通クラス
 * */
public class ProvinceInquiry extends InquiryBaseLoading implements ScrollBoxAdapter {

    /** JNIの戻り値　取得した都道府県のレコード数 */
    protected JNILong Rec = new JNILong();
    /** 選択した都道府県の市区町村のレコード数 */
    protected JNILong ListCount;
    /** 自車の位置（市区町村の名称） */
    protected String ms2Str = "";//new String();

    /** 総件数 */
    protected int allRecordCount = 0;
    /** クリックした都道府県のIndex */
    protected int iProvinceId = 0;
    /** 定数都道府県の内容 */
    protected String[] province = null;
    /** 全国ボタン対象 */
    protected Button AllCountry = null;
    /** スクロール対象 */
    protected ScrollBox oBox = null;

    /** 自車の位置（アドレス） */
    protected JNIString AddressString = new JNIString();

    /** 自車の位置（都道府県の名称） */
    protected String ms1Str = "";//new String();
    /** 自車の位置（大字の名称） */
    private String ms3Str = "";//new String();

    /** 検索するため、スタートIndex */
    private int RecIndex = 0;
    /** カレントページのIndex */
    private int pageIndex = 0;
    /** 都道府県ボタンのIndex */
    private int dataIndex = 0;
    /** タイトルの内容 */
    private String searchValue = "";

//	private boolean onlyRunOne = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startShowPage(NOTSHOWCANCELBTN);

    }

    private ListAdapter listAdapter = new BaseAdapter() {

        @Override
        public int getCount() {
            return ProvinceInquiry.this.getCount();
        }

        //yangyang add start

//      @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        //yangyang add end
        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int wPos, View oView, ViewGroup parent) {

            return ProvinceInquiry.this.getView(wPos, oView);
        }

    };

    /**
     * 都道府県ボタンの名称を取得する
     *
     * @param dataIndex
     *            ボタンのIndex
     * @return String 名称を戻す
     * */
    protected String getButtonValue(int dataIndex) {
        String str = "";
        return str;
    }

    /**
     * カレント画面に表示した都道府県の内容を取得する
     * */
    protected void getCurrentPageShowData() {

    }

    /**
     * データオブジェクトのメモリを申請する
     * */
    protected void initDataObj() {

    }

    /**
     *
     * 都道府県をクリックする場合、該当関数を再実現する
     *
     * @param provinceIndex
     *            都道府県ボタンのIndex
     * @param oView
     *            レイアウト対象
     *
     * */

    protected void onProvinceclick(int provinceIndex, View oView) {

    }

    /**
     * 都道府県ボタンの表示状態をチェックする
     *
     * @param text
     *            ボタンに表示した内容
     * @param wPos
     *            該当ボタンの位置（行数）
     * @param dataIndex
     * @return int -1:レコードがない都道府県と判断した。<br>
     *         >-1:レコードがある都道府県リストのインディクス
     *
     * */
    protected int checkHasData(String text, int dataIndex) {
//		int bResult = -1;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"checkHasData---283==" + dataIndex);
        return dataIndex;
    }

    /**
     * 都道府県コート、市区町村コード、大字コードによって、検索処理前にJNI内部を初期化する
     * 名称検索とき、該当関数を再度実現するのは必要がある
     *
     * @param narrowCode
     *            都道府県コート
     * @param middleCode
     *            市区町村コード
     * @param wideCode
     *            大字コード
     * */
    protected void setSearchValuetoJNI(short wideCode, short middleCode, short narrowCode) {
    }

    /**
     * データがある都道府県を取得する
     * */
    protected void getProvinceValue() {

    }

    /**
     *
     * カレント表示した都道府県を初期化する
     *
     * @param startIndex
     *            開始Index
     * @param count
     *            取得したいレコード数
     *
     * */
    protected void initShowProvinceValue(int startIndex, int count) {

    }

    @Override
    public int getCount() {

        int dataCount = province.length;
        if (dataCount % 2 > 0) {
            return dataCount / 2 + 1;
        } else {
            return dataCount / 2;
        }
    }

    @Override
    public int getCountInBox() {
        return 5;
    }

//	/**
//	 * 500件を表示するため、ページIndexと表示できる総件数を初期化する
//	 *
//	 * @author yangyang
//	 *
//	 * */
//	public void initPageIndexAndCnt() {
//		if (ListCount.lcount >= 500) {
//			allRecordCount = 500;
//		} else {
//			allRecordCount = (int) ListCount.lcount;
//		}
//
//	}

    @Override
    public View getView(final int wPos, View oView) {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"1635 yy =====getview");
        LinearLayout dataLayout = null;
        if (oView == null || !(oView instanceof LinearLayout)) {
//			//最初に初期化する
//			if (wPos == 0) {
//
//				if (null == ListCount){
//					ListCount = new JNILong();
//				}
//				RecIndex =0;
//
////				//データを取得する
////				getCurrentPageShowData();
////				//重新定位检索
////
////				//データある都道府県を取得する
////				getProvinceValue();
//
//			}

            LayoutInflater oInflater = LayoutInflater.from(this);
            dataLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_province, null);
            oView = dataLayout;
        } else {
            //次ページへ遷移するとき、初期化
            if (wPos % getCountInBox() == 0) {
                //ページIndexを取得する
                pageIndex = wPos / getCountInBox();
                RecIndex = getCountInBox() * pageIndex * 2;
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy pageIndex===" + pageIndex);
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy RecIndex===" + RecIndex);
            }
            dataLayout = (LinearLayout)oView;
        }
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"wPos====pageIndex===" + wPos + "====" + pageIndex);
//		//カレントページデータを取得する
//		if (wPos%getCountInBox() == 0) {
//			//ページIndexによって、データを取得する
//			//メモリを最優化するため、毎回は5つデータを取得する
////			NaviLog.d(NaviLog.PRINT_LOG_TAG,"RecIndex===" + RecIndex);
//			//カレント表示した都道府県を初期化する
////			if (!onlyRunOne) {
////				RecIndex = getFirstShowIndex();
////				onlyRunOne = true;
////			}
//			//freescroll
////			initShowProvinceValue(RecIndex,getCountInBox()*2);
//
//		}

        ButtonImg obtnLeft = (ButtonImg)dataLayout.findViewById(R.id.inquiry_line_left);

        ButtonImg obtnRight = (ButtonImg)dataLayout.findViewById(R.id.inquiry_line_right);
        //yangyang mod start Bug1032
        obtnLeft.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        obtnRight.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        //yangyang mod end Bug1032
        obtnLeft.requestFocus();
        obtnRight.requestFocus();
        //freescroll
//		dataIndex = (wPos % getCountInBox()) * 2;
        dataIndex = wPos * 2;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"163 dataIndex== " + dataIndex);
        //都道府県ボタン表示状態をチェックして、表示する
        if (wPos < this.getCount()) {

            obtnLeft.setText(getButtonValue(dataIndex));
            int iResult = checkHasData(obtnLeft.getText().toString(), dataIndex);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"left iResult===" + iResult);
            if (-1 != iResult) {

                obtnLeft.setEnabled(true);
            } else {
                obtnLeft.setEnabled(false);
            }

            obtnRight.setText(getButtonValue(dataIndex + 1));
            iResult = checkHasData(obtnRight.getText().toString(), dataIndex + 1);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"right iResult===" + iResult);
            if (-1 != iResult) {
//				m_Poi_Key_Listitem[dataIndex + 1] = m_Poi_Gnr_Listitem[iResult];
//				m_Poi_Key_Listitem[dataIndex + 1].setM_iOldIndex(iResult);
                obtnRight.setEnabled(true);
            } else {
                obtnRight.setEnabled(false);
            }

        } else {
            obtnLeft.setText("");
            obtnLeft.setEnabled(false);
            obtnRight.setText("");
            obtnRight.setEnabled(false);
        }
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"obtnLeft .getText().toString().trim()===" + obtnLeft.getText().toString().trim());
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"obtnRight.getText().toString().trim()===" + obtnRight.getText().toString().trim());
        //test
//		ms1Str = "東京都";
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"ms1Str   .getText().toString().trim()===" + ms1Str);
        //自車アイコンの表示をチェックする
        Intent oIntent = getIntent();
        if (obtnLeft.getText().toString().trim().equals(ms1Str)) {

//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"22 ---getM_iOldIndex---dataIndex------>" + getM_iOldIndex(dataIndex) + "==========" + dataIndex);
            oIntent.putExtra(Constants.PARAMS_MYPOS_PROVINCE, dataIndex);

            obtnLeft = obtnLeft.initBtn(obtnLeft, R.drawable.icon_current_normal, ButtonImg.RIGHT);
        } else {
            obtnLeft = obtnLeft.initBtn(obtnLeft, -1, ButtonImg.RIGHT);
        }
        //yangyang mod start Bug1032
//		obtnLeft.setOneLine(obtnLeft);
//		obtnLeft.setPadding(10, 12, 10, 10);
//Chg 2011/08/12 Z01_h_yamada Start -->
//		obtnLeft.superPadding(5, 8, 10, 12);
//--------------------------------------------
        final int paddingLeft = 5;
        final int paddingRight = 10;
        final int paddingTop = 4; // 自車マークサイズ調整用
        final int paddingBottom = 4;

        obtnLeft.superPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
//Chg 2011/08/12 Z01_h_yamada End <--
        //yangyang mod end Bug1032
        if (obtnRight.getText().toString().trim().equals(ms1Str)) {
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"22 ---getM_iOldIndex--------->" + getM_iOldIndex(dataIndex + 1) + "==========" + (dataIndex+1));
            oIntent.putExtra(Constants.PARAMS_MYPOS_PROVINCE, (dataIndex + 1));
            obtnRight = obtnRight.initBtn(obtnRight, R.drawable.icon_current_normal, ButtonImg.RIGHT);
        } else {
            obtnRight = obtnRight.initBtn(obtnRight, -1, ButtonImg.RIGHT);
        }
//		obtnRight.setOneLine(obtnRight);
//		obtnRight.setPadding(10, 12, 10, 10);
//Chg 2011/08/12 Z01_h_yamada Start -->
//		obtnRight.superPadding(5, 8, 10, 12);
//--------------------------------------------
        obtnRight.superPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
//Chg 2011/08/12 Z01_h_yamada End <--
        obtnLeft.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View oView) {
                //省的Id
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"211 pageIndex==" + pageIndex);
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"215  " + wPos);
//				iProvinceId = (wPos%getCountInBox))*2 + pageIndex*getCountInBox()*2;
                //freescroll
//				iProvinceId = (wPos % getCountInBox()) * 2;
                iProvinceId = wPos * 2;
                onProvinceclick(iProvinceId, oView);

            }

        });
        obtnRight.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View oView) {
                //省的Id
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"230 pageIndex==" + pageIndex);
//				iProvinceId = (wPos%getCountInBox())*2  + pageIndex*getCountInBox()*2 + 1;
                //freescroll
//				iProvinceId = (wPos % getCountInBox()) * 2 + 1;
                iProvinceId = wPos * 2 + 1;
                onProvinceclick(iProvinceId, oView);

            }

        });
        return oView;
    }

    protected int getM_iOldIndex(int dataIndex2) {
        return dataIndex2;
    }

    protected int getFirstShowIndex() {

        return 0;
    }

    /**
     * 自車の位置
     * */
    private void getCarPos() {
        JNITwoLong Coordinate = new JNITwoLong();
        JNIThreeShort KeyCode = new JNIThreeShort();
        short WideCode;
        short MiddleCode;
        short NarrowCode;

        //自車の経緯度を取得する
        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);

        //自車の経緯度に通して、都道府県のコード、市区町村コード、大字コードを取得する
        NaviRun.GetNaviRunObj().JNI_NE_GetAddressCode(Coordinate.getM_lLong(), Coordinate.getM_lLat(), KeyCode);

        WideCode = KeyCode.getM_sWideCode();
        MiddleCode = KeyCode.getM_sMiddleCode();
        NarrowCode = KeyCode.getM_sNarrowCode();
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"WideCode==" + WideCode + "==MiddleCode==" + MiddleCode + "==NarrowCode==" + NarrowCode);
        //住所文字列を取得する
        NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
                Coordinate.getM_lLong(), Coordinate.getM_lLat(), AddressString);

        if (!AppInfo.isEmpty(AddressString.getM_StrAddressString())) {
            int iFirst = AddressString.getM_StrAddressString().indexOf(" ");
//		int iSecond = AddressString.getM_StrAddressString().lastIndexOf(" ");
            //都道府県の名称
            ms1Str = AddressString.getM_StrAddressString().substring(0, iFirst);

//		//市区町村の名称
//		ms2Str = AddressString.getM_StrAddressString().substring(iFirst + 1,
//				iSecond);
//		//大字の名称
//		ms3Str = AddressString.getM_StrAddressString().substring(iSecond + 1,
//				AddressString.getM_StrAddressString().length());

            Intent oIntent = getIntent();
            oIntent.putExtra(Constants.PARAMS_MYPOS_CITY_NAME, ms2Str);
            oIntent.putExtra(Constants.PARAMS_MYPOS_CITYNEXT_NAME, ms3Str);
            oIntent.putExtra(Constants.PARAMS_WIDECODE, WideCode);
            oIntent.putExtra(Constants.PARAMS_MIDDLECODE, MiddleCode);
            oIntent.putExtra(Constants.PARAMS_NARROWCODE, NarrowCode);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"ms1Str===" + ms1Str);
//		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_Clear();

        }
        /**
		 *
		 * */

        setSearchValuetoJNI(WideCode, MiddleCode, NarrowCode);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"searchValue===" + searchValue);

    }

    // 全国ボタンのクリック処理
    private OnClickListener OnAllCountryListener = new OnClickListener() {

        @Override
        public void onClick(View oView) {
//			if (null == ListCount){
//				ListCount = new JNILong();
//			}
//			NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetRecCount(ListCount);
//			NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetRecList(SearchCacheNum * (pageListIndex-1), SearchCacheNum,
//					m_Poi_Facility_Listitem, ListCount);// all list result

            Intent oIntent = getIntent();
            oIntent.putExtra(Constants.FROM_WHICH_SEARCH_KEY, "AllCountry");
            TextView oText = (TextView)ProvinceInquiry.this.findViewById(R.id.inquiry_title);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"AllCountry oText.getText().toString()===" + oText.getText().toString());

            String buttonText = null;
            if (oView instanceof Button) {
                buttonText = ((Button)oView).getText().toString();
            }
            String title = oText.getText().toString() + Constants.FLAG_TITLE + buttonText;
            oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
            oIntent.setClass(ProvinceInquiry.this, KeyResultInquiry.class);
//Chg 2011/09/23 Z01yoneya Start -->
//			startActivityForResult(oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
            NaviActivityStarter.startActivityForResult(ProvinceInquiry.this, oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
        }
    };

    @Override
    protected boolean onStartShowPage() throws Exception {
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStartShowPage start");
        initDataObj();
        if (null == ListCount) {
            ListCount = new JNILong();
        }
        RecIndex = 0;
//		//自車の位置を取得する
        getCarPos();
        // 都道府県定数を取得する
        province = this.getResources().getStringArray(R.array.inquiry_province);
        initShowProvinceValue(RecIndex, 50);
        //データある都道府県を取得する
        getProvinceValue();
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStartShowPage end");
        return true;
    }

    @Override
    protected void onFinishShowPage(boolean bGetData) throws Exception {
        if (bGetData) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage start");
                    initDialog();
                    NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage end");
                }

            });
        }

    }

    private void initDialog() {

        Intent oIntent = getIntent();
//Del 2011/09/17 Z01_h_yamada Start -->
//		//タイトルを設定する
//		int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE,0);
//		if (0!= titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--
        //レイアウトを初期化する
        LayoutInflater oInflater = LayoutInflater.from(this);
        LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base_list_freescroll, null);

        //検索内容を初期化する
        TextView oText = (TextView)oLayout.findViewById(R.id.inquiry_title);
        searchValue = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
        if (searchValue == null) {
            oText.setText("");
        } else {
        	// マッチングのための大本の住所が入っているかどうかを確かめる。
        	String dispTitle = searchValue;
        	if (oIntent.hasExtra(Constants.ADDRESS_SEARCH_DB_ADR))
        	{
        		dispTitle = "（" + oIntent.getStringExtra(Constants.ADDRESS_SEARCH_DB_ADR) + "）";
        	}
            //oText.setText(searchValue);
        	oText.setText(dispTitle);
        }

        //全国ボタンを初期化する
        AllCountry = (Button)oLayout.findViewById(R.id.inquiry_btn);
        AllCountry.setBackgroundResource(R.drawable.btn_default);
        AllCountry.setText(R.string.btn_all_country);

        AllCountry.setOnClickListener(OnAllCountryListener);
        initCountryBtn();
//		oBox = (ScrollBox)oLayout.findViewById(R.id.scrollBox);
//		oBox.setAdapter(this);
        ScrollList oList = (ScrollList)oLayout.findViewById(R.id.scrollList);
//Del 2011/10/06 Z01_h_yamada Start -->
//        oList.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
        oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
        oList.setAdapter(listAdapter);

        ScrollTool oTool = new ScrollTool(this);

//		oTool.bindView(oBox);
        oTool.bindView(oList);

        setViewInOperArea(oTool);

        this.setViewInWorkArea(oLayout);
        int LineIndex = getFirstShowIndex();
        oList.setSelection(LineIndex);
    }

    protected void initCountryBtn() {
    }
}
