package net.zmap.android.pnd.v2.common;

import net.zmap.android.pnd.v2.api.GeoPoint;
import android.app.Activity;
import android.content.Context;


// 商品情報を格納するクラス
public class ProductInfo {
	public String mCustCode;		// 顧客コード
	public String mProductName;		// 商品名称
	public String mProductRyaku;	// 商品略名
	public String mDeliveryMethod;	// 配達方法
	public int mMonDeliCnt;			// 月曜日の配達数量
	public int mTueDeliCnt;			// 火曜日の配達数量
	public int mWedDeliCnt;			// 水曜日の配達数量
	public int mThuDeliCnt;			// 木曜日の配達数量
	public int mFriDeliCnt;			// 金曜日の配達数量
	public int mSatDeliCnt;			// 土曜日の配達数量
	public int mSunDeliCnt;			// 日曜日の配達数量
	public int mSecondCnt;			// 二次商品数量
	public String mDeliveryStart;	// 配達開始日
	public String mDeliveryEnd;		// 配達終了日
	public String mDeliverySecond;	// 二次商品 配達日
	public double mPrice;			// 単価
	public int mPeriodDeliCnt;		// 指定期間配達数量
	public int mDeliveryStatus;	// 配達結果
	
	/** 未配達 */
	//public static final int DELIVERY_STATUS_NOTDELIVERED = 0;
	/** 配達済み */
	//public static final int DELIVERY_STATUS_DELIVERED = 1;
	/** 配達中止 */
	//public static final int DELIVERY_STATUS_ABORTED = 2;
}

/*  
public class DeliveryInfo {
    public int mCarNo;
    public int mCustID;
    public int mCustCode;
    public String mCustmer;
    public String mCustTel;
    public String mAddress;
    public String mAddress2;
    public String mAddress3;
    public int mMatchLebel;
    public int mModFlag;
    public String mProduct;
    public int mProductCnt;
    public String mLastDate;
    public int mClaim;
    public GeoPoint mPoint;
}
*/