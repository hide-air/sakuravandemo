package net.zmap.android.pnd.v2.api.event;

import android.location.Location;
import android.location.LocationManager;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * ルート案内情報クラス
 *
 * @see RouteGuidanceListener
 * @see RouteGuidanceListener#onStarted(RouteGuidance)
 * @see RouteGuidanceListener#onStopped(RouteGuidance)
 * @see RouteGuidanceListener#onPaused(RouteGuidance)
 * @see RouteGuidanceListener#onResumed(RouteGuidance)
 * @see RouteGuidanceListener#onNearToCrossroad(RouteGuidance)
 * @see RouteGuidanceListener#onNearToWaypoint(RouteGuidance)
 * @see RouteGuidanceListener#onNearToDestination(RouteGuidance)
 * @see RouteGuidanceListener#onRerouted(RouteGuidance)
 */
public final class RouteGuidance implements Parcelable {
    /**
     * ルート案内イベント種別
     */

    /** 不明 (初期値) */
    public final static int TYPE_EVENT_NONE           = 0;
    /** 案内開始(ルート案内を開始したときに発生するイベント) */
    public final static int TYPE_EVENT_STARTGUIDE     = 1;
    /** 案内終了(ルート案内を終了したときに発生するイベント) */
    public final static int TYPE_EVENT_ENDGUIDE       = 2;
    /** 案内一時停止 (API で案内停止したときに発生するイベント) */
    public final static int TYPE_EVENT_PAUSEGUIDE     = 3;
    /** 案内再開 (API で案内再開したときに発生するイベント) */
    public final static int TYPE_EVENT_RESUMEGUIDE    = 4;
    /** 目標案内 (交差点に接近して、音声案内したときに発生するイベント) */
    public final static int TYPE_EVENT_CROSSGUIDE     = 5;
    /** 経由地案内 (経由地に接近して、音声案内したときに発生するイベント) */
    public final static int TYPE_EVENT_VIAGUIDE       = 6;
    /** 目的地案内 (目的地に接近して、音声案内したときに発生するイベント) */
    public final static int TYPE_EVENT_GOALGUIDE      = 7;
    /** リルート (リルート後、案内を開始したときに発生するイベント) */
    public final static int TYPE_EVENT_REROUTE        = 8;

    /**
     * 目標種別
     */
    /** なし */
    public final static int TYPE_TARGET_NONE          = 0;
    /** 交差点 (次の目標地点は交差点) */
    public final static int TYPE_TARGET_CROSS         = 1;
    /** 分岐点 (次の目標地点は分岐点) */
    public final static int TYPE_TARGET_TILT          = 2;
    /** 合流地点 (次の目標地点は合流地点) */
    public final static int TYPE_TARGET_JOINT         = 3;
    /** 高速入口 (次の目標地点は高速道路の入口) */
    public final static int TYPE_TARGET_ONHIGHWAY     = 4;
    /** 高速出口 (次の目標地点は高速道路の出口) */
    public final static int TYPE_TARGET_OFFHIGHWAY    = 5;
    /** 料金所 (次の目標地点は料金所) */
    public final static int TYPE_TARGET_TOLL          = 6;

    /**
     * 進行方向種別
     */
    /** なし */
    public final static int TYPE_DIRECTION_NONE       = 0;
    /** 直進・道なり (次の目標地点での進行方向が直進または道なり) */
    public final static int TYPE_DIRECTION_MITINARI   = 1;
    /** 右方向 (次の目標地点での進行方向は右折) */
    public final static int TYPE_DIRECTION_RIGHT      = 2;
    /** 左方向 (次の目標地点での進行方向は左折) */
    public final static int TYPE_DIRECTION_LEFT       = 3;
    /** 斜め右方向 (次の目標地点での進行方向は斜めに右折) */
    public final static int TYPE_DIRECTION_RIGHTFRONT = 4;
    /** 斜め左方向 (次の目標地点での進行方向は斜めに左折) */
    public final static int TYPE_DIRECTION_LEFTFRONT  = 5;
    /** 大きく右方向 (次の目標地点での進行方向は大きく右折) */
    public final static int TYPE_DIRECTION_RIGHTBACK  = 6;
    /** 大きく左方向 (次の目標地点での進行方向は大きく左折) */
    public final static int TYPE_DIRECTION_LEFTBACK   = 7;
    /** Uターン (次の目標地点でUターン) */
    public final static int TYPE_DIRECTION_U_TURN     = 8;

    /**
     * メンバ変数
     */
    /** ルート案内イベント種別 */
    private int             mGuideType;
    /** イベント発生ロケーション (世界測地と現在日時) */
    private Location        mLocation;
    /** 目標種別 */
    private int             mTargetType;
    /** 目標名称 */
    private String          mTargetName;
    /** 目標座標 */
    private Location        mTargetLocation;
    /** 目標距離 */
    private float           mTargetDistance;
    /** 進行方向種別 */
    private int             mTargetDirection;

    /**
     * コンストラクタ
     */
    public RouteGuidance() {
        mLocation = new Location(LocationManager.GPS_PROVIDER);
        mLocation.setLatitude(0.0);
        mLocation.setLongitude(0.0);
        mGuideType = TYPE_EVENT_NONE;
        mTargetLocation = new Location(LocationManager.GPS_PROVIDER);
        mTargetLocation.setLatitude(0.0);
        mTargetLocation.setLongitude(0.0);
        mTargetDistance = 0.0F;
        mTargetType = TYPE_TARGET_NONE;
        mTargetDirection = TYPE_DIRECTION_NONE;
    }

    /**
     * コンストラクタ(ルート案内情報指定)
     * @param guidetype
     *            ルート案内イベント種別(TYPE_EVENT_XXXX)
     * @param location
     *            イベント発生ロケーション (世界測地座標と現在日時)
     * @param targettype
     *            目標種別(TYPE_TARGET_XXXX)
     * @param targetname
     *            目標名称
     * @param targetlocation
     *            目標座標 (世界測地)
     * @param targetdistance
     *            目標距離
     * @param targetdirection
     *            進行方向種別(TYPE_DIRECTION_XXXX)
     */
    public RouteGuidance(int guidetype, Location location, int targettype,
                      String targetname, Location targetlocation, float targetdistance,
                      int targetdirection) {
        setCrossTargetInfo(guidetype, location, targettype, targetname,
                           targetlocation, targetdistance, targetdirection);
    }

    private RouteGuidance(Parcel in) {
        readFromPercel(in);
    }

    //---------------
    // getter
    //---------------
    /**
     * ルート案内イベント種別取得<br>
     * 案内状態の変更内容または、ルート案内中に発生するイベントを識別するコードを返却する<br>
     *
     * @see #TYPE_EVENT_NONE
     * @see #TYPE_EVENT_STARTGUIDE
     * @see #TYPE_EVENT_ENDGUIDE
     * @see #TYPE_EVENT_CROSSGUIDE
     * @see #TYPE_EVENT_VIAGUIDE
     * @see #TYPE_EVENT_GOALGUIDE
     * @see #TYPE_EVENT_REROUTE
     *
     * @return ルート案内イベント種別
     */
    public int getEventType() {
        return mGuideType;
    }

    /**
     * イベント発生ロケーション取得
     *
     * @return イベント発生ロケーション (世界測地座標と現在日時)
     */
    public Location getLocation() {
        return mLocation;
    }

    /**
     * 目標地点種別取得<br>
     * 次の目標(案内)地点の種別(交差点、分岐点、高速出入り口など)を識別するコードを返却する<br>
     * ルート案内イベント種別=TYPE_EVENT_CROSSGUIDE のときのみ設定。それ以外は TYPE_TARGET_NONEを返却する<br>
     *
     * @see #TYPE_EVENT_CROSSGUIDE
     * @see #TYPE_TARGET_NONE
     * @see #TYPE_TARGET_CROSS
     * @see #TYPE_TARGET_TILT
     * @see #TYPE_TARGET_JOINT
     * @see #TYPE_TARGET_ONHIGHWAY
     * @see #TYPE_TARGET_OFFHIGHWAY
     * @see #TYPE_TARGET_TOLL
     *
     * @return 目標地点種別
     */
    public int getTargetType() {
        return mTargetType;
    }

    /**
     * 目標地点(次の目標地点、経由地、目的地)名称取得
     *
     * @return 目標地点名称
     */
    public String getTargetName() {
        return mTargetName;
    }

    /**
     * 目標地点(次の目標地点、経由地、目的地)座標取得
     *
     * @return 目標地点座標
     */
    public Location getTargetLocation() {
        return mTargetLocation;
    }

    /**
     * 目標地点(次の目標地点、経由地、目的地)までの距離取得
     *
     * @return 目標地点までの距離(m)
     */
    public float getTargetDistance() {
        return mTargetDistance;
    }

    /**
     * 目標地点進行方向種別取得<br>
     * 次の目標(案内)地点での進行方向(右、左、斜め右、斜め左など)を識別するコードを返却する<br>
     * ルート案内イベント種別=TYPE_EVENT_CROSSGUIDE のときのみ設定。それ以外は TYPE_DIRECTION_NONEを返却する<br>
     *
     * @see #TYPE_EVENT_CROSSGUIDE
     * @see #TYPE_DIRECTION_NONE
     * @see #TYPE_DIRECTION_MITINARI
     * @see #TYPE_DIRECTION_RIGHT
     * @see #TYPE_DIRECTION_LEFT
     * @see #TYPE_DIRECTION_RIGHTFRONT
     * @see #TYPE_DIRECTION_LEFTFRONT
     * @see #TYPE_DIRECTION_LEFTBACK
     *
     * @return 進行方向種別
     */
    public int getTargetDirection() {
        return mTargetDirection;
    }

    //---------------
    // setter
    //---------------
    /**
     * 案内状態設定(案内開始/終了/リルート)
     *
     * @param guidetype
     *            ルート案内イベント種別(TYPE_EVENT_XXXX)
     * @param location
     *            イベント発生ロケーション (世界測地座標と現在日時)
     */
    public void setGuideStatus(int guidetype, Location location) {
        mGuideType = guidetype;
        mLocation = location;
    }

    /**
     * 案内地点(経由地/目的地)情報設定
     *
     * @param guidetype
     *            ルート案内イベント種別(TYPE_EVENT_XXXX)
     * @param location
     *            イベント発生ロケーション (世界測地座標と現在日時)
     * @param targetname
     *            目標名称
     * @param targetlocation
     *            目標座標 (世界測地)
     * @param targetdistance
     *            目標距離
     */
    public void setTargetInfo(int guidetype, Location location,
                              String targetname, Location targetlocation, float targetdistance) {
        setGuideStatus(guidetype, location);
        mTargetName = targetname;
        mTargetLocation = targetlocation;
        mTargetDistance = targetdistance;
    }

    /**
     * 交差点情報設定
     *
     * @param guidetype
     *            ルート案内イベント種別(TYPE_EVENT_XXXX)
     * @param location
     *            イベント発生ロケーション (世界測地座標と現在日時)
     * @param targettype
     *            目標種別(TYPE_TARGET_XXXX)
     * @param targetname
     *            目標名称
     * @param targetlocation
     *            目標座標 (世界測地)
     * @param targetdistance
     *            目標距離
     * @param targetdirection
     *            進行方向種別(TYPE_DIRECTION_XXXX)
     */
    public void setCrossTargetInfo(int guidetype, Location location,
                                   int targettype, String targetname, Location targetlocation,
                                   float targetdistance, int targetdirection) {
        setTargetInfo(guidetype, location, targetname, targetlocation, targetdistance);
        mTargetType = targettype;
        mTargetDirection = targetdirection;
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(mGuideType);
        out.writeParcelable(mLocation, flags);
        out.writeInt(mTargetType);
        out.writeString(mTargetName);
        out.writeParcelable(mTargetLocation, flags);
        out.writeFloat(mTargetDistance);
        out.writeInt(mTargetDirection);
    }

    private void readFromPercel(Parcel in) {
        mGuideType = in.readInt();
        mLocation = in.readParcelable(Location.class.getClassLoader());
        mTargetType = in.readInt();
        mTargetName = in.readString();
        mTargetLocation = in.readParcelable(Location.class.getClassLoader());
        mTargetDistance = in.readFloat();
        mTargetDirection = in.readInt();
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /* (非 Javadoc)
     *
     */
    public static final Parcelable.Creator<RouteGuidance> CREATOR = new Parcelable.Creator<RouteGuidance>() {
        public RouteGuidance createFromParcel(Parcel in) {
            return new RouteGuidance(in);
        }
        public RouteGuidance[] newArray(int size) {
            return new RouteGuidance[size];
        }
    };
}