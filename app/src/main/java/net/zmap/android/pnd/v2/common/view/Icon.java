package net.zmap.android.pnd.v2.common.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.Button;

public class Icon extends Button{
    private Context context;
    private Bitmap foreImage;
    private Bitmap icon;
    private final static int PADDING = 5;

    public Icon(Context context) {
        super(context);
        this.context = context;
        onInit();
    }

    public Icon(Context oContext,AttributeSet oSet)
    {
        super(oContext,oSet);
        this.context = oContext;
        onInit();
    }

    private void onInit(){
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int w = this.getWidth();
        int h = this.getHeight();

        if(foreImage != null){
//Chg 2011/08/15 Z01_h_yamada Start -->
//            int imageW = foreImage.getWidth();
//            int imageH = foreImage.getHeight();
//            int wX = (w - imageW) / 2;
//            int wY = (h - imageH) / 2;
//            canvas.drawBitmap(foreImage, wX, wY, null);
//--------------------------------------------
            Rect src = new Rect(0, 0, foreImage.getWidth(), foreImage.getHeight());
        	Rect dst = new Rect();
        	dst.left = (w - foreImage.getWidth()) / 2;
        	dst.top  = (h - foreImage.getHeight()) / 2;
        	if (dst.left < 0) {
        		dst.left = 0;
        	}
        	if (dst.top < 0) {
        		dst.top = 0;
        	}
        	dst.right = w - dst.left;
        	dst.bottom = h - dst.top;
            canvas.drawBitmap(foreImage, src, dst, null);
//Chg 2011/08/15 Z01_h_yamada End <--
        }
        if(icon != null){
           int x = w - icon.getWidth() - PADDING;
           int y = h - icon.getHeight() - PADDING;
           canvas.drawBitmap(icon, x, y, null);
        }
    }

    public void setBitmap(Bitmap bitmap){
        this.foreImage = bitmap;
    }

    public void setBitmap(int iconId){
        this.foreImage = BitmapFactory.decodeResource(this.context.getResources(), iconId);;
    }

    public void setIcon(Bitmap icon){
        this.icon = icon;
    }

    public void setIcon(int iconId){
        this.icon = BitmapFactory.decodeResource(this.context.getResources(), iconId);
    }
}
