package net.zmap.android.pnd.v2.vics;

import android.content.Intent;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNITwoInt;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.ZNE_VehicleInfo_t;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.maps.MapView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class VicsManager {

    // Singleton
    static private VicsManager theObj;

    static private MapActivity m_mapActivity;
    static private MapView m_mapView;

    // Vics更新間隔 (分)
    private int m_updateIntervalMinute = -1;

    // Vics最終更新時間 (ミリ秒)
    private long m_lastUpdatedTime = -1;

    // Vics更新用タイマー
    private Timer m_vicsTimer;

    // Vicsデータ取得処理
    private VicsAsyncDataGetter m_asyncDataGetter = null;
    // プレゼンス情報設定処理
    private ProbeAsyncDataSender m_asyncProbeSetter = null;

    //ナビモード
    private int  m_nNaviMode = Constants.NE_NAVIMODE_CAR;

	private static final int DEF_ROUTE_TYPE_NONE			=	-1;		///<	 NONE
/*	private static final int DEF_ROUTE_TYPE_EXPRESSWAY		=	0;		///<	 高速
	private static final int DEF_ROUTE_TYPE_CITYEXPRESSWAY	=	1;		///<	 都市高速
	private static final int DEF_ROUTE_TYPE_NATIONALROAD	=	2;		///<	 国道
	private static final int DEF_ROUTE_TYPE_MAJORROAD		=	3;		///<	主要地方道
	private static final int DEF_ROUTE_TYPE_PREFECTURALROAD	=	4;		///<	県道
	private static final int DEF_ROUTE_TYPE_GENERALROAD1	=	5;		///<	一般道路1(幹線)
	private static final int DEF_ROUTE_TYPE_GENERALROAD2	=	6;		///<	一般道路2(その他の案内道)
	private static final int DEF_ROUTE_TYPE_INROUTE			=	7;		///<	JoinToICRoad
	private static final int DEF_ROUTE_TYPE_MINORROAD1		=	8;		///<	MinorRoad1
	private static final int DEF_ROUTE_TYPE_MINORROAD2		=	9;		///<	MinorRoad2(Not Display in driving)
	private static final int DEF_ROUTE_TYPE_FERRYROUTE		=	10;		///<	フェリー航路
*/
	private static final int NEW_DEF_ROUTE_TYPE_NONE		=	15;		///<	 NONE(道路外)

	private static final int FUNC_CAR = 11;
	private static final int ROAD_FUNC_CAR = 1;

    private int m_nCountTime = 0;
    static private final int DEF_TIME_COUNT  = 3;

//    private boolean m_bOnDemo = false;
//    private boolean m_bOnGuide = false;

    private List<PROBE_POINT_DATA> m_PointList = new ArrayList<PROBE_POINT_DATA>();
    private List<PROBE_POINT_DATA> m_sendPointList = null;
    private JNITwoLong mStartPos = null;
    private JNITwoLong mEndPos = null;
//    private boolean mbDataClear= false;
    private int mnVicsTimerCount = 0;


    /** コンストラクタ
     *
     */
    private VicsManager(MapActivity o) {
        m_mapActivity = o;
    }

    /**
     * ファイナライザ
     *
     */
    public void finalize() {
        // VICS データ取得を中断
        if (m_asyncDataGetter != null) {
            m_asyncDataGetter.cancel(true);
        }

        if (m_asyncProbeSetter != null) {
        	m_asyncProbeSetter.cancel(true);
        }

        if (m_vicsTimer != null) {
            m_vicsTimer.cancel();
        }

        VicsAsyncDataGetter.DeleteGetVicsFile();
    }

    /**
     * インスタンス取得
     *
     * @return VicsDataManager インスタンス
     */
    static public VicsManager getInstance() {
        return theObj;
    }

    /**
     * 初期化 (最初に一度必ず実行)
     *
     * @param activity
     *            アクティビティ
     */
    static public void init(MapActivity activity) {
        if (theObj == null) {
            theObj = new VicsManager(activity);
        }
        VicsAsyncDataGetter.DeleteGetVicsFile();

        m_mapView = MapView.getInstance();
    }

    /**
     * 初期化 (Navi起動時に実行)
     *
     */
    public void startServices() {

        boolean bRet = false;

        // Vics
        bRet = startVics();
        if (!bRet) {
        	NaviLog.i("VicsDataManager", "Cannot start Vics service.");
        }

        return;
    }

    /**
     * 終了処理 (Navi終了時に実行)
     *
     * @return 成功:true, 失敗:false
     */
    public void stopServices() {
        // Vics
        stopVics();
    }

    /**
     * 設定変更
     *
     */
    public void onChangedOption() {

        // Vics
        onChangedVicsOption();
    }

    /**
     * Vics サービス起動
     *
     * @return
     */
    private boolean startVics() {
        // 設定取得
        boolean bRet = getVicsOption();
        if (!bRet) {
            stopVics();
            return false;
        }

        // VICS の自動更新 : しない
        if (m_updateIntervalMinute == 0) {
            stopVics();
            return true;
        }

        NaviRun.GetNaviRunObj().JNI_NE_VICS_SetDisplayFlag(true);
        // 地図の更新
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();

        // VICS の自動更新 : ○分ごと
        // 1回目更新
        return requestVics();
    }

    /**
     * Vics の設定取得
     *
     * @return
     */
    private boolean getVicsOption() {

        boolean isExistVicsData = false;
    	if(m_mapActivity != null){
    		if(m_mapActivity.getApplication() != null){
        		if(((NaviApplication)m_mapActivity.getApplication()).getNaviAppDataPath() != null){
        			isExistVicsData = ((NaviApplication)m_mapActivity.getApplication()).getNaviAppDataPath().isExistVicsData();
        		}
    		}
    	}

        if (!isExistVicsData) {
            return false;
        }

        // 更新間隔
        m_updateIntervalMinute = getVicsUpdateIntervalMinute();

        return true;
    }

    /**
     * 即座に Vics 取得
     *
     * @return
     */
    public boolean requestVics() {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "VicsManager.requestVics()");
        if (m_asyncDataGetter != null) {
            m_asyncDataGetter.cancel(true);
        }

        m_asyncDataGetter = new VicsAsyncDataGetter(autoVicsListener);
        m_asyncDataGetter.execute();

        return true;
    }

    /**
     * Vics 自動取得開始 / タイマーをスタート
     *
     * @return
     */
    private boolean autoRequestVics() {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "VicsManager.autoRequestVics()");

    	mnVicsTimerCount  = 0;
        if (m_vicsTimer != null) {
            m_vicsTimer.cancel();
        }

        m_vicsTimer = new Timer(true);
        m_vicsTimer.schedule(new TimerTask(){

            @Override
            public void run()
            {
                m_mapActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                    	//ナビモードの変更
                        if(CommonLib.getNaviMode() != m_nNaviMode){
                        	if(m_nNaviMode == Constants.NE_NAVIMODE_CAR){
                	        	clearData();
//                		    	mbDataClear= true;
                		    	stopVics();
                        	}

                        	m_nNaviMode = CommonLib.getNaviMode();
                        	m_nCountTime = DEF_TIME_COUNT;
                        	if(m_nNaviMode == Constants.NE_NAVIMODE_CAR){
                		    	startVics();
                        	}
                        }

                        if(m_nNaviMode != Constants.NE_NAVIMODE_CAR){
                			return;
                		}

                		if(m_updateIntervalMinute > 0){
                        	mnVicsTimerCount++;
        	            	if(mnVicsTimerCount >=  m_updateIntervalMinute * 60){
        	            		mnVicsTimerCount = 0;

        		                if (m_asyncDataGetter != null) {
        		                    m_asyncDataGetter.cancel(true);
        		                }
        		                // VICS リクエスト
        		                m_asyncDataGetter = new VicsAsyncDataGetter(autoVicsListener);
        		                m_asyncDataGetter.execute();
        	            	}
                    	}
                    }
                });
            }
        }, 0 , 1000 );
        return true;
    }

    /**
     * Vics 自動取得中断 / タイマーをリセット
     *
     * @return
     */
    private void stopVics() {
        VicsAsyncDataGetter.DeleteGetVicsFile();

        m_lastUpdatedTime = -1;
        updateLastUpdateTime();

        NaviRun.GetNaviRunObj().JNI_NE_VICS_SetDisplayFlag(false);
        // 地図の更新
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
    }

    /**
     * Vics 設定変更
     *
     */
    private void onChangedVicsOption() {

        boolean bIsChanged = false;

        // 更新間隔
        {
            int i = getVicsUpdateIntervalMinute();
            if (m_updateIntervalMinute != i) {
                bIsChanged = true;
            }
        }

        if (bIsChanged) {
            startVics();
        }

        return;
    }

    /**
     * VICS更新間隔を取得 (分単位)
     *
     */
    private int getVicsUpdateIntervalMinute() {
        JNIInt JIVicsInfo = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetVicsInfo(JIVicsInfo);
        switch(JIVicsInfo.getM_iCommon())
        {
            case Constants.VICSINFO_5:      return 5;
            case Constants.VICSINFO_10:     return 10;
            case Constants.VICSINFO_15:     return 15;
            case Constants.VICSINFO_NONE:   return 0;
        }

        return 0;
    }

    /**
     * VICSボタンの最終更新時刻を更新
     *
     */
    private void updateLastUpdateTime()
    {
        if (m_lastUpdatedTime == -1) {
            m_mapView.setVicsTime("--:--");
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            int iHour = calendar.get(Calendar.HOUR_OF_DAY);
            int iMinute = calendar.get(Calendar.MINUTE);
            m_mapView.setVicsTime(CommonLib.getFormatTime(iHour, iMinute));
        }
    }

    /** ナビスタートを受け取る
     *	案内中に目的地を変更した場合、isNaviStartが変わらない。
     *	そのため、直接イベントを受けて、案内中フラグを初期化する
     *	基本的な状態保存などは全てupdatePosition内で行う。
     *	そのため、それ以外の箇所で受け取った情報は、フラグの変更のみに留める
     */
    public void onNaviStart() {
//		m_bOnGuide = false;
    }

    /** 自車位置更新を受け取り、その点を保持する
     * 保持する条件は、以下の場合
     * 前回取得より3秒経過(本関数が3回呼ばれる度)
     * 案内開始、デモ開始時
     * また、データ送信は、以下のタイミングとする
     * VICSが
     * デモの開始終了
     * 案内の開始終了時
     * ナビモード変更時
     * @param Coordinate 自車位置
     */
    public void updatePosition(JNITwoLong Coordinate) {
        if (CommonLib.isBIsDemo()) {
            return;
        }

        m_nCountTime++;
        if(m_nCountTime < DEF_TIME_COUNT){
            return;
        }
        m_nCountTime = 0;

        if (m_PointList.size() < 10) {
            Date date= new Date(System.currentTimeMillis());
            PROBE_POINT_DATA data = new PROBE_POINT_DATA();
            data.setPointDate(date);
            data.setFuncCode(FUNC_CAR);
            data.setRoadFuncCode(ROAD_FUNC_CAR);
            data.setVehiclePos(Coordinate);

            ZNE_VehicleInfo_t roadNameChange = new ZNE_VehicleInfo_t();
            NaviRun.GetNaviRunObj().JNI_DG_getVehiclePosInfo(roadNameChange);
            JNITwoInt roadType = roadNameChange.getRoadType();
            int nroadType = (roadType.getM_iSizeY() != DEF_ROUTE_TYPE_NONE)? roadType.getM_iSizeY() : NEW_DEF_ROUTE_TYPE_NONE;
            data.setRoadType(nroadType);

            m_PointList.add(data);
        }
        else {
            sendProbeData();
            clearData();
            m_PointList.clear();
        }

//    	//デモ中かどうか
//    	if(CommonLib.isBIsDemo() != m_bOnDemo){
//    		if(!m_bOnDemo){
//    			sendProbeData();
//	        	clearData();
//    		}
//    		m_bOnDemo = CommonLib.isBIsDemo();
//    		m_nCountTime = DEF_TIME_COUNT;
//    	}
//
//    	//案内中かどうか
//    	if(CommonLib.isNaviStart() != m_bOnGuide){
//    		sendProbeData();
//        	clearData();
//    		m_bOnGuide = CommonLib.isNaviStart();
//    		m_nCountTime = DEF_TIME_COUNT;
//
//    		//案内開始でスタート、ゴールの設定
//    		if(m_bOnGuide){
//            	//目的地
//                JNITwoLong JNIpositon = new JNITwoLong();
//                JNIString JNIpositonName = new JNIString();
//                JNIInt JNIpass = new JNIInt();
//                NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(Constants.NAVI_DEST_INDEX, JNIpositon, JNIpositonName, JNIpass);
//                mEndPos = JNIpositon;
//                mStartPos = Coordinate;
//    		}else{
//        		//案内終了でスタート、ゴールの初期化
//                mEndPos = null;
//                mStartPos = null;
//    		}
//    	}
//
//
//    	if(mbDataClear){
//        	m_PointList.clear();
//        	mbDataClear = false;
//        }
//
//    	if(isSkipPoint()){
//    		return;
//    	}
//
//    	m_nCountTime++;
//
//        //3秒間隔での更新
//        if(m_nCountTime < DEF_TIME_COUNT){
//        	return;
//        }
//
//        //カウントの初期化
//        m_nCountTime = 0;
//
//        //データ保存
//        Date date= new Date(System.currentTimeMillis());
//        PROBE_POINT_DATA data = new PROBE_POINT_DATA();
//        data.setPointDate(date);
//        data.setFuncCode(FUNC_CAR);
//        data.setRoadFuncCode(ROAD_FUNC_CAR);
//        data.setVehiclePos(Coordinate);
//
//        ZNE_VehicleInfo_t roadNameChange = new ZNE_VehicleInfo_t();
//        NaviRun.GetNaviRunObj().JNI_DG_getVehiclePosInfo(roadNameChange);
//        JNITwoInt roadType = roadNameChange.getRoadType();
//        //道路外のみ、エンジン側から来る情報とサーバへ送信する情報が違う
//        int nroadType = (roadType.getM_iSizeY() != DEF_ROUTE_TYPE_NONE)? roadType.getM_iSizeY() : NEW_DEF_ROUTE_TYPE_NONE;
//        data.setRoadType(nroadType);
//
//        m_PointList.add(data);
    }

    /** データの送信
     * @return
     */
   public boolean sendProbeData(){
       NaviLog.d(NaviLog.PRINT_LOG_TAG, "VicsManager.sendProbeData()");
    	if(m_PointList.size() == 0){
    		return true;
    	}

    	if(m_asyncProbeSetter != null){
    		m_asyncProbeSetter = null;
    	}
    	m_asyncProbeSetter = new ProbeAsyncDataSender(autoProbeListener , m_mapActivity);

    	if(m_sendPointList == null){
	    	m_sendPointList = new  ArrayList<PROBE_POINT_DATA>(m_PointList);
//	    	mbDataClear= true;
    	}

    	if(mStartPos != null && mEndPos != null){
    		m_asyncProbeSetter.setStartPos(mStartPos);
    		m_asyncProbeSetter.setEndPos(mEndPos);
    	}
    	m_asyncProbeSetter.setProbData(m_sendPointList);
    	m_asyncProbeSetter.execute();

    	return true;
    }

	/** データのクリア
	* @return
	*/
	public void clearData(){
		if(m_sendPointList != null){
			m_sendPointList.clear();
			m_sendPointList = null;
		}
	}

//	private boolean isSkipPoint(){
//		return (m_nNaviMode != Constants.NE_NAVIMODE_CAR || m_bOnDemo || m_lastUpdatedTime <= 0 );
//	}

	/** VICSリクエストで返したデータを処理する */
    private VicsListener autoVicsListener = new VicsListener()
    {
        @Override
        public void onCancel(Intent oIntent)
        {
            // GETVICS ファイルの取得がキャンセルされた
            if (m_asyncDataGetter != null) {
                m_asyncDataGetter = null;
            }
            sendProbeData();
        }

        @Override
        public void onError(Intent oIntent)
        {
            // GETVICS ファイルの取得に失敗
            if (m_lastUpdatedTime > 0) {
                long currentTime = System.currentTimeMillis();

                // 最終更新から、更新間隔の3倍の時間が経過してもデータの取得に失敗
                if (currentTime - m_lastUpdatedTime >= m_updateIntervalMinute * 60 * 1000 * 3) {
                    VicsAsyncDataGetter.DeleteGetVicsFile();

                    NaviRun.GetNaviRunObj().JNI_NE_VICS_SetDisplayFlag(false);
                    // 地図の更新
                    NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();

                    m_lastUpdatedTime = -1;
                    updateLastUpdateTime();
                }
            }

            if (m_asyncDataGetter != null) {
                m_asyncDataGetter = null;
            }
            sendProbeData();

            autoRequestVics();
        }

        @Override
        public void onFinish(Intent oIntent)
        {
            // GETVICS ファイルの取得に成功
            m_lastUpdatedTime = System.currentTimeMillis();

            NaviRun.GetNaviRunObj().JNI_NE_VICS_SetDisplayFlag(true);

            // VICS モジュールに GETVICS ファイルを読ませる
            NaviRun.GetNaviRunObj().JNI_NE_VICS_ReloadVicsFile();

            // 地図更新
           updateLastUpdateTime();

            if (m_asyncDataGetter != null) {
                m_asyncDataGetter = null;
            }

            //Probeデータの更新
            sendProbeData();

            // 次回更新のタイマーセット
            autoRequestVics();
        }

        @Override
        public void onTimeOut(Intent oIntent)
        {
            if (m_asyncDataGetter != null) {
                m_asyncDataGetter = null;
            }

            autoRequestVics();
        }
    };

    /** VICSリクエストで返したデータを処理する */
    private ProbeEventListener autoProbeListener = new ProbeEventListener()
    {
        /** データ送信の終了
         * @return
         */
    	@Override
    	public void onFinish(String strData) {
    		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinish return value = " + strData);
    		m_asyncProbeSetter = null;
    		clearData();
    	}

    	@Override
    	public void onCancel() {
    		m_asyncProbeSetter = null;
    		clearData();
    	}

    	@Override
    	public void onError() {
    		m_asyncProbeSetter = null;
    		clearData();
    	}

    	@Override
    	public void onTimeOut() {
    		m_asyncProbeSetter = null;
    		clearData();
    	}
    };
}
