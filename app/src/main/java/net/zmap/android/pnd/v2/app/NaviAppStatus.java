package net.zmap.android.pnd.v2.app;

//import net.zmap.android.pnd.v2.app.launch.DownloadMapActivity;
import net.zmap.android.pnd.v2.common.utils.NaviLog;

/*
 * ナビアプリの状態データクラス
 *
 * このクラスではアプリの状態を管理範囲とする（起動～終了）
 * ナビエンジンの状態は管理させない(案内中とか探索中とか)
 */
public class NaviAppStatus {
//Del 2011/10/12 Z01yoneya Start -->
//    private static boolean isAlreadyLaunch = false; //アプリが起動済みか？
//    private static boolean isAppFinishing = false; //アプリが終了中か？
//Del 2011/10/12 Z01yoneya End <--

    public static int APP_STATE_NOT_START = 0; //非起動
    public static int APP_STATE_STARTING = 1; //起動中
    public static int APP_STATE_AUTHENTICATE_AND_DOWNLOAD = 2; //起動時の認証＆ダウンロード中
    public static int APP_STATE_INIT_NAVIENGINE = 3; //ナビエンジン初期化中
    public static int APP_STATE_RUNNING = 4; //アプリ実行中
    public static int APP_STATE_PREPARE_MAP_DOWNLOAD = 5; //メニューからの地図ダウンロード中
    public static int APP_STATE_STOPPING = 6; //アプリ終了中

    private static int appStatus = APP_STATE_NOT_START; //アプリ状態

    /*
     * ナビアプリの状態は、下記を想定している。
     *
     * ・アプリ起動
     * 非起動→(起動開始)→起動中→(起動完了)→(認証開始)→認証中→(認証完了)
     * →(ナビエンジン初期化開始)→ナビエンジン初期化中→(ナビエンジン初期化完了)→アプリ実行中
     *
     * ・メニューからの地図ダウンロード
     * アプリ実行中→メニューからの地図ダウンロード中→(ダウンロード完了)
     * →(ナビエンジン初期化開始)→ナビエンジン初期化中→(ナビエンジン初期化完了)→アプリ実行中
     *
     * ・アプリ終了時
     * アプリ実行中→(終了開始)→アプリ終了中→(終了完了)→非起動
     *
     *
     *
     * また、アプリ起動時に地図のダウンロード認証を行う
     * 地図のダウンロード認証が完了するまではナビアクティビティ(NaviActivityBase)は
     * 起動できない事とする(isCompleteDownloadAuthentication()フラグ)
     *
     */

    /*
     * アプリケーション状態設定
     *
     * アプリケーション状態の遷移順番には決まりがある。
     * 遷移の順番が守られない場合は例外を発生する
     *
     * APP_STATE_NOT_START
     *         ↓
     * APP_STATE_STARTING
     *         ↓
     * APP_STATE_AUTHENTICATE_AND_DOWNLOAD
     *         ↓
     * APP_STATE_INIT_NAVIENGINE
     *         ↓
     * APP_STATE_RUNNING → APP_STATE_PREPARE_MAP_DOWNLOAD →(プロセス終了のため、次回起動時はAPP_STATE_NOT_STARTから開始)
     *         ↓
     * APP_STATE_STOPPING
     *         ↓
     *     プロセス終了
     *
     */
    public static void setAppStatus(int status) throws IllegalStateException {
        if (status == APP_STATE_STARTING) {
            if (appStatus != APP_STATE_NOT_START) {
                NaviLog.e(NaviLog.PRINT_LOG_TAG, "NaviAppStatus::setAppStatus() exception! " + appStatus + "-->" + status);
                throw new IllegalStateException();
            }
        } else if (status == APP_STATE_AUTHENTICATE_AND_DOWNLOAD) {
            if (appStatus != APP_STATE_STARTING) {
                NaviLog.e(NaviLog.PRINT_LOG_TAG, "NaviAppStatus::setAppStatus() exception! " + appStatus + "-->" + status);
                throw new IllegalStateException();
            }
        } else if (status == APP_STATE_INIT_NAVIENGINE) {
            int prevExpectStatus = APP_STATE_AUTHENTICATE_AND_DOWNLOAD;
            if (NaviAppDataPath.isLocalDataMode()) {
                prevExpectStatus = APP_STATE_STARTING;
            }
            if (appStatus != prevExpectStatus) {
                NaviLog.e(NaviLog.PRINT_LOG_TAG, "NaviAppStatus::setAppStatus() exception! " + appStatus + "-->" + status);
                throw new IllegalStateException();
            }
        } else if (status == APP_STATE_RUNNING) {
            if (!(appStatus == APP_STATE_INIT_NAVIENGINE || appStatus == APP_STATE_PREPARE_MAP_DOWNLOAD)) {
                NaviLog.e(NaviLog.PRINT_LOG_TAG, "NaviAppStatus::setAppStatus() exception! " + appStatus + "-->" + status);
                throw new IllegalStateException();
            }
        } else if (status == APP_STATE_PREPARE_MAP_DOWNLOAD) {
            if (appStatus != APP_STATE_RUNNING) {
                NaviLog.e(NaviLog.PRINT_LOG_TAG, "NaviAppStatus::setAppStatus() exception! " + appStatus + "-->" + status);
                throw new IllegalStateException();
            }
        } else if (status == APP_STATE_STOPPING) {
//Chg 2012/02/23 Z01_h_yamada Start --> #3790
//          if (!(appStatus == APP_STATE_RUNNING || appStatus == APP_STATE_PREPARE_MAP_DOWNLOAD)) {
//--------------------------
            if (!(appStatus == APP_STATE_RUNNING || appStatus == APP_STATE_PREPARE_MAP_DOWNLOAD || appStatus == APP_STATE_AUTHENTICATE_AND_DOWNLOAD)) {
//Chg 2012/02/23 Z01_h_yamada End <--
                NaviLog.e(NaviLog.PRINT_LOG_TAG, "NaviAppStatus::setAppStatus() exception! " + appStatus + "-->" + status);
                throw new IllegalStateException();
            }
        }

        NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviAppStatus::setAppStatus " + appStatus + "-->" + status);
        appStatus = status;
    }

    /*
     * アプリが起動しているか判定する(非起動以外のステータスならtrue)
     */
    public static boolean isAppStart() {
        return appStatus != APP_STATE_NOT_START;
    }

    /*
     * 認証中＆ダウンロード中か判定する
     */
    public static boolean isAppAuthenticating() {
        return appStatus == APP_STATE_AUTHENTICATE_AND_DOWNLOAD;
    }

    /*
     * ナビエンジン起動中か判定する
     */
    public static boolean isAppPrepareNaviEngine() {
        return appStatus == APP_STATE_INIT_NAVIENGINE;
    }

    /*
     * アプリが実行中か判定する
     */
    public static boolean isAppRunning() {
        return appStatus == APP_STATE_RUNNING || appStatus == APP_STATE_PREPARE_MAP_DOWNLOAD;
    }

//Del 2011/10/12 Z01yoneya Start -->
//    public static void setAppLaunchFlag(boolean isLaunch) {
//        isAlreadyLaunch = isLaunch;
//    }
//
//    public static boolean isAlreadyLaunch() {
//        return isAlreadyLaunch;
//    }
//Del 2011/10/12 Z01yoneya End <--

//Del 2011/10/11 Z01yoneya Start -->
//    public static void setAppFinishingFlag(boolean isFinishing) {
//        isAppFinishing = isFinishing;
//    }
//Del 2011/10/11 Z01yoneya End <--

    public static boolean isAppFinishing() {
//Chg 2011/10/11 Z01yoneya Start -->
//                return isAppFinishing;
//------------------------------------------------------------------------------------------------------------------------------------
        return appStatus == APP_STATE_STOPPING;
//Chg 2011/10/11 Z01yoneya End <--
    }

//    /*
//     * アプリ起動時のダウンロード認証が完了したかの判定
//     *
//     * true:認証OK false:認証NG(または認証完了前)
//     * ※ローカルデータモードの時は常にtrue
//     */
//    public static boolean isCompleteDownloadAuthentication() {
//        if (NaviAppDataPath.isLocalDataMode()) {
//            return true;
//        } else {
//            return DownloadMapActivity.isCompleteDownloadAuthentication();
//        }
//    }
}
