// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
package net.zmap.android.pnd.v2.common.services;

import net.zmap.android.pnd.v2.common.DrivingRegulation;
import android.app.Service;
import android.content.Context;
import android.content.Intent;

/**
 *  @file		DrivingRegulationServiceBase
 *  @brief		走行規制関連のサービスベースクラス
 *
 *  @attention
 *  @note
 *
 *  @author		N.Sasao
 *  @date		$Date:: 2013-08-20 00:00:00 +0900 #$ (Create at 2013-08-20)
 *  @version	$Revision: $ by $Author: $
 *
 */
public abstract class DrivingRegulationServiceBase extends Service {
	protected Context m_Context = null;
	/**
	 * 下位サービスへの接続
	 */
	public void Init( Context context )
	{
		m_Context = context;
	}

	/**
	 * 下位サービスへの接続
	 */
	public abstract void Bind();
	/**
	 * 下位サービスの切断
	 */
	public abstract void UnBind();
	/**
	 * 走行状態取得
	 */
	public abstract boolean IsDriving();
	/**
	 * 接続状態の取得
	 */
	public abstract boolean IsConnect();

	/**
	 * 接続状態通知
	 */
	public void sendConnectInformation( boolean bConnecting )
	{
        Intent intent = new Intent();

        intent.setAction(DrivingRegulation.ACTION_DRIVING_REGULATION_CONNECT);

        intent.putExtra( DrivingRegulation.EXTRA_CONNECT, bConnecting );
        this.sendBroadcast(intent);
	}
	/**
	 * 走行規制状態通知
	 */
	public void sendDrivingRegulateInformation( boolean bDriving )
	{
        Intent intent = new Intent();

        intent.setAction(DrivingRegulation.ACTION_DRIVING_REGULATION_DRIVING);

        intent.putExtra( DrivingRegulation.EXTRA_DRIVING, bDriving );
        this.sendBroadcast(intent);
	}
}
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END