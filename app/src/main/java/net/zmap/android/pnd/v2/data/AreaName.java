/**
 *  @file		AreaName.java
 *  @brief		エリア名称データ
 *
 *  @attention
 *  @note
 *
 *  @author		TheDoanh [Z01]
 *  @date		$Date:: 2011-03-22 00:00:00 +0900 #$ (Create at 2011-03-22)
 *  @version	$Revision: $ by $Author: $
 *
 */
package net.zmap.android.pnd.v2.data;


public class AreaName{
	private String 	m_sAreaName;


	/**
	 * Created on 	2011/03/22
	 * Description:	コンストラクタ
	 * @param		なし
	 * @return		なし
	 * @author		thedoanh
	 */
	public AreaName() {
		m_sAreaName = "";
	}

    /**
	 * Created on 	2011/03/18
	 * Description:	エリア名称の設定
	 * @param		a_sAreaName
	 * @return		なし
	 * @author		thedoanh
	 */
	public void setAreaName(String a_sAreaName){
		m_sAreaName = a_sAreaName;
	}

	/**
	 * Created on 	2011/03/18
	 * Description:	エリア名称の取得
	 * @param		なし
	 * @return		String エリア名称
	 * @author		thedoanh
	 */
	public String getAreaName(){
		return m_sAreaName;
	}
}
