package net.zmap.android.pnd.v2.common.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import net.zmap.android.pnd.v2.R;

public class ScrollBar extends View
{
	private static final int HEIGHT_MIN = 3;
	private static final float MIN_BAR_HEIGHT = 10.0f;

	private int m_wPos = 0;
	private float m_wAreaHeight = HEIGHT_MIN;
	private int m_wHeight = HEIGHT_MIN;
	private boolean m_bIsEnd = false;

	public ScrollBar(Context oContext)
	{
		super(oContext);

	}

	public ScrollBar(Context oContext,AttributeSet oSet)
	{
		super(oContext,oSet);
	}

	@Override
	protected void onDraw(Canvas oCanvas)
	{

		Paint oPaint = new Paint();
		int wX = getPaddingLeft();
		int wY = getPaddingTop();
		int wW = getWidth() - getPaddingLeft() - getPaddingRight();
		int wH = getHeight() - getPaddingTop() - getPaddingBottom();
		float wBlockHeight = wH * m_wAreaHeight / m_wHeight;
//NaviLog.d("ScrollBar", "onDraw  wX=" + wX + " wY=" + wY + " wW=" + wW + " wH=" + wH + " m_wAreaHeight=" + m_wAreaHeight + " m_wHeight=" + m_wHeight+ " wBlockHeight=" + wBlockHeight + "m_bIsEnd=" + m_bIsEnd);


//Chg 2011/10/06 Z01_h_yamada Start -->
//		Paint oPaint = new Paint();
//		int wX = getPaddingLeft();
//		int wY = getPaddingTop();
//		int wW = getWidth() - getPaddingLeft() - getPaddingRight();
//		int wH = getHeight() - getPaddingTop() - getPaddingBottom();
//		float wBlockHeight = wH * m_wAreaHeight / m_wHeight;
//
//		float wPos = 0.0f;
//		if( wBlockHeight<MIN_BAR_HEIGHT ) {
//			float minH = wH - (MIN_BAR_HEIGHT -wBlockHeight);
//			wBlockHeight = MIN_BAR_HEIGHT;
//			wPos = 1.0f * minH * m_wPos / m_wHeight;
//		} else {
//			wPos = 1.0f * wH * m_wPos / m_wHeight;
//		}
//
//		if(m_bIsEnd)
//		{
//			wBlockHeight = wH - wPos;
//		}
//--------------------------------------------
		float wPos = 0.0f;
		if( wBlockHeight<MIN_BAR_HEIGHT ){
			wBlockHeight = MIN_BAR_HEIGHT;
			wPos = (wH - MIN_BAR_HEIGHT) * m_wPos / m_wHeight;
		}else{
			wPos = 1.0f * wH * m_wPos / m_wHeight;
		}

		if(m_bIsEnd || wH < (wPos + wBlockHeight) )
		{
			wBlockHeight = wH - wPos;
		}
//Chg 2011/10/06 Z01_h_yamada End <--


//NaviLog.d("ScrollBar", "        wPos=" + wPos + " wBlockHeight=" + wBlockHeight + " 余り=" + (wH - wPos - wBlockHeight));

		if(isEnabled())
		{
			oPaint.setStyle(Paint.Style.FILL);
			oPaint.setColor(getResources().getColor(R.color.scroll_tool_bg));
			oCanvas.drawRect(wX, wY, wX + wW,wY + wH, oPaint);

//Chg 2011/10/04 Z01_h_yamada Start -->
//			float blockHeight = wBlockHeight;
//			if(wBlockHeight < 1){
//			    blockHeight = 1f;
//			}
//			oPaint.setColor(getResources().getColor(R.color.scroll_bar_in));
//			oCanvas.drawRect(wX, wY + wPos, wX + wW, wY + wPos + blockHeight, oPaint);
//--------------------------------------------
			oPaint.setColor(getResources().getColor(R.color.scroll_bar_in));
			oCanvas.drawRect(wX, wY + wPos, wX + wW, wY + wPos + wBlockHeight, oPaint);
//Chg 2011/10/04 Z01_h_yamada End <--

			oPaint.setStyle(Paint.Style.STROKE);
			oPaint.setColor(getResources().getColor(R.color.scroll_tool_out));
			oCanvas.drawRect(wX, wY, wX + wW - 1,wY + wH - 1, oPaint);
		}
		else
		{
			oPaint.setStyle(Paint.Style.FILL);
			oPaint.setColor(getResources().getColor(R.color.scroll_tool_dis_bg));
			oCanvas.drawRect(wX, wY, wX + wW,wY + wH, oPaint);

			oPaint.setStyle(Paint.Style.STROKE);
			oPaint.setColor(getResources().getColor(R.color.scroll_tool_out));
			oCanvas.drawRect(wX, wY, wX + wW - 1,wY + wH - 1, oPaint);
		}
		super.onDraw(oCanvas);
	}

	// スクロールバーのサイズ設定
	// wPos        = 現在位置
	// wAreaHeight = 表示アイテム数
	// wHeight     = 全体アイテム数
	// bEnd        =
	//
	public void update(int wPos, float wAreaHeight,int wHeight,boolean bEnd)
	{
//	NaviLog.d("ScrollBar", "update  wPos=" + wPos + " wAreaHeight=" + wAreaHeight + " wHeight=" + wHeight + " bEnd=" + bEnd);
		m_bIsEnd = bEnd;

		if(wPos < 0)
		{
			m_wPos = 0;
		}
		else
		{
			m_wPos = wPos;
		}

		if(wAreaHeight < HEIGHT_MIN)
		{
			m_wAreaHeight = HEIGHT_MIN;
		}
		else
		{
			m_wAreaHeight = wAreaHeight;
		}

		if(wHeight > 0)
		{
			m_wHeight = wHeight;
		}
		postInvalidate();
	}

}
