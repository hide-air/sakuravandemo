package net.zmap.android.pnd.v2.data;

public class ZNE_Satellite {
	private short sSatID;
	private short sAzimuth;
	private short sElevation;
	private short sSNRate;
	private int   iEnable;

	public short getSatID() {
		return sSatID;
	}
	public short getAzimuth() {
		return sAzimuth;
	}
	public short getElevation() {
		return sElevation;
	}
	public short getSNRate() {
		return sSNRate;
	}
	public boolean getEnable() {
		if(iEnable > 0){
			return true;
		}
		else{
			return false;
		}
	}

}
