package net.zmap.android.pnd.v2.widget;

import android.app.Activity;
import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetHostView;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.activity.BaseActivity;
// MOD.2014.01.21 N.Sasao OS4.4以降でWidgetが更新されない START
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.maps.MapActivity;
// MOD.2014.01.21 N.Sasao OS4.4以降でWidgetが更新されない  END
import net.zmap.android.pnd.v2.widget.WidgetSaver.WidgetLocation;

/**
 * AppWidgetのHostを行うActivity継承して利用されることを想定。
 * AppWidgetのIDはユーザーのPick時にIDとのひも付けが行われ、
 * そのIDを使って当該Widgetを呼び出すことができる。
 *
 * @author Itaru OGAWA (itaru@zero-sum.co.jp)
 */
public abstract class WidgetHostActivity extends BaseActivity
        implements View.OnClickListener {
    /** マネージャ */
    private AppWidgetManager mAppWidgetManager;

    /** ホスト */
    private AppWidgetHost    mAppWidgetHost;

    /** このホストのID */
    static final int         APPWIDGET_HOST_ID        = 2037;

    /** Intent で利用する ID(作成リクエスト) */
    private static final int REQUEST_CREATE_APPWIDGET = 5;

    /** Intent で利用する ID(選択リクエスト) */
    private static final int REQUEST_PICK_APPWIDGET   = 9;

    /** メインのビュー */
    private View             mMainView;

    /** サーフェイスビュー */
    private View             mSurfaceView;

    /** 右側のビュー(Button -> AppWidgetHostView) */
    private View             mRightWidget;

    /** 右側のビュー(Button -> AppWidgetHostView) */
    private View             mTopWidget;

    /** ビューの親のレイアウト */
    private RelativeLayout   mParent;

    /** ターゲットのID */
    private int              mTargetId;

    /** カウンタ */
    private int              mIdCounter               = 100;

    /** widgetの永続化 */
    private WidgetSaver      mSaver                   = null;

    /** メニューのID (衝突注意) */
    private static final int MENU_ID_CLEAR_WIDGET     = ( Menu.FIRST ) + 1000;
// ADD.2014.01.21 N.Sasao OS4.4以降でWidgetが更新されない START
    private boolean 		mIsListening              = false;
// ADD.2014.01.21 N.Sasao OS4.4以降でWidgetが更新されない  END
    @Override
    /**
     * 構築します。
     * WidgetHostの初期化を行います。
     */
    protected void onCreate( Bundle savedInstanceState ) {
        Log.v( "WidgetHostActivity", "onCreate start" );

        super.onCreate( savedInstanceState );

//        mAppWidgetManager = AppWidgetManager.getInstance( this );
//        mAppWidgetHost = new AppWidgetHost( this, APPWIDGET_HOST_ID );
////        mAppWidgetHost.startListening();
//
//        mTopWidget = (View)findViewById( R.id.top_dummy );
//        mRightWidget = (View)findViewById( R.id.right_dummy );
//        mParent = (RelativeLayout)mRightWidget.getParent();
//
//        mMainView = (View)findViewById( R.id.main );
//
//        mSaver = WidgetSaver.getInstance( this );
//        mSaver.load();
//
//        int rightId = mSaver.getWidget( WidgetLocation.RIGHT );
//        if ( rightId < 0 ) {
//            mRightWidget.setOnClickListener( this );
//        } else {
//            mTargetId = mRightWidget.getId();
//            completeAddAppWidget( rightId );
//        }
//
//        int topId = mSaver.getWidget( WidgetLocation.TOP );
//        if ( topId < 0 ) {
//            mTopWidget.setOnClickListener( this );
//        } else {
//            mTargetId = mTopWidget.getId();
//            completeAddAppWidget( topId );
//        }
        mAppWidgetManager = AppWidgetManager.getInstance( this );
        mAppWidgetHost = new AppWidgetHost( this, APPWIDGET_HOST_ID );
        mSaver = WidgetSaver.getInstance( this );
        mSaver.load();
// ADD.2014.01.21 N.Sasao OS4.4以降でWidgetが更新されない START
    	// 自身以外のMapActivityへアクセス
        MapActivity otherActivity = NaviRun.getMapActivity();
    	if( otherActivity != null ){
    		// 周辺検索関連でMapActivityが2個ある時、下層レイヤーのMapActivityをstopListeningする
    		otherActivity.stopListening();
    	}
// ADD.2014.01.21 N.Sasao OS4.4以降でWidgetが更新されない  END
        Log.v( "WidgetHostActivity", "onCreate end" );
    }

    protected void initializeWidgetHost( RelativeLayout widgetParent, View surfaceView, View topWidget, View rightWidget) {
        if (widgetParent != null && topWidget != null && rightWidget == null) {
            throw new NullPointerException( "Widget view is null." );
        }

        mParent = widgetParent;
        mSurfaceView = surfaceView;
        mTopWidget = topWidget;
        mRightWidget = rightWidget;

//        mMainView = (View)findViewById( R.id.main );
        mMainView = (View)findViewById( R.id.main_view );

        int rightId = mSaver.getWidget( WidgetLocation.RIGHT );
        if ( rightId < 0 ) {
            mRightWidget.setOnClickListener( this );
        } else {
            Log.d("WidgetHostActivity", " initializeWidgetHost rightId[" + rightId + "] -> completeAddAppWidget");
            mTargetId = mRightWidget.getId();
            completeAddAppWidget( rightId );
        }

        int topId = mSaver.getWidget( WidgetLocation.TOP );
        if ( topId < 0 ) {
            mTopWidget.setOnClickListener( this );
        } else {
            Log.d("WidgetHostActivity", " initializeWidgetHost topId[" + topId + "] -> completeAddAppWidget");
            mTargetId = mTopWidget.getId();
            completeAddAppWidget( topId );
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
// MOD.2014.01.21 N.Sasao OS4.4以降でWidgetが更新されない START
        startListening();
// MOD.2014.01.21 N.Sasao OS4.4以降でWidgetが更新されない  END
    }

    @Override
    protected void onPause() {
        super.onPause();
// MOD.2014.01.21 N.Sasao OS4.4以降でWidgetが更新されない START
        // このタイミングでのstopListeningは早いので
        // ここではしない
// MOD.2014.01.21 N.Sasao OS4.4以降でWidgetが更新されない  END
    }

    @Override
    /**
     * 破棄します。
     * WidgetHostの終了処理を行います。
     */
    protected void onDestroy() {
        Log.v( "WidgetHostActivity", "onDestroy start" );
        super.onDestroy();

//        if ( mAppWidgetHost != null ) {
//            mAppWidgetHost.stopListening();
//        }

//        mSaver.save();

        Log.e( "WidgetHostActivity", "onCreate end" );
    }

    @Override
    /**
     * クリック時のハンドラ
     */
    public void onClick( View view ) {
        if ( view == mRightWidget || view == mTopWidget ) {
//			if (((ItsmoNaviPnd)getApplication()).isAuditLog())
//			{
//				AuditLog log = (new AuditLog.Builder(SmarTaxiCenterAuditLogConstants.ACTION_ID_WIDGET_HOST_CLICK, System.currentTimeMillis()))
//					.build();
//				AuditLogLogger.write(this, log);
//			}

            Log.v( "onClick", "Widget area clicked." );
            doWidgetPick( view );
        } else {
            Log.v( "onClick", "none-Widget click Ignored." );
        }
    }

    /**
     * Widget の選択を開始します。
     *
     * @param view
     */
    protected void doWidgetPick( View view ) {
        mTargetId = view.getId();

        int appWidgetId = mAppWidgetHost.allocateAppWidgetId();
        Intent pickIntent = new Intent( AppWidgetManager.ACTION_APPWIDGET_PICK );
        pickIntent.putExtra( AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId );
        startActivityForResult( pickIntent, REQUEST_PICK_APPWIDGET );
    }

    @Override
    /**
     * ActivityResultハンドラ
     */
    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
        Log.v( "result", requestCode + ", " + resultCode + ", " + data );

        switch ( requestCode ) {
            case REQUEST_PICK_APPWIDGET:
            case REQUEST_CREATE_APPWIDGET:
                checkActivityResult( requestCode, resultCode, data );
                break;

            default:
                Log.v( "onActivityResult", String.format( "ignoring request code=%d", requestCode ) );
                break;
        }
    }

    /**
     * ActivityResultを検査し、適切な処理を呼び出します。
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    protected void checkActivityResult( int requestCode, int resultCode, Intent data ) {
        if (data != null) {
            if ( resultCode == RESULT_OK ) {
                switch ( requestCode ) {
                    case REQUEST_PICK_APPWIDGET:
                        addAppWidget( data );
                        break;

                    case REQUEST_CREATE_APPWIDGET:
                        completeAddAppWidget( data );
                        break;
                }
            } else if ( resultCode == RESULT_CANCELED ) {
                /* キャンセル時の処理 */
                int appWidgetId = data.getIntExtra( AppWidgetManager.EXTRA_APPWIDGET_ID, -1 );
                if ( appWidgetId != -1 ) {
                    mAppWidgetHost.deleteAppWidgetId( appWidgetId );
                }
            }
        }
    }

    /**
     * Widget作成要求を発行します。
     *
     * @param data
     */
    void addAppWidget( Intent data ) {
        int appWidgetId = data.getIntExtra( AppWidgetManager.EXTRA_APPWIDGET_ID, -1 );
        Log.w( "AppWidget ID", String.valueOf( appWidgetId ) );

        AppWidgetProviderInfo appWidget = mAppWidgetManager.getAppWidgetInfo( appWidgetId );
        if ( appWidget.configure != null ) {
            Intent intent = new Intent( AppWidgetManager.ACTION_APPWIDGET_CONFIGURE );
            intent.setComponent( appWidget.configure );
            intent.putExtra( AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId );
            startActivityForResult( intent, REQUEST_CREATE_APPWIDGET );
        } else {
            onActivityResult( REQUEST_CREATE_APPWIDGET, Activity.RESULT_OK, data );
        }
    }

    /**
     * Widget追加処理を行います。
     *
     * @param data
     */
    private void completeAddAppWidget( Intent data ) {
        Bundle extras = data.getExtras();
        int appWidgetId = extras.getInt( AppWidgetManager.EXTRA_APPWIDGET_ID, -1 );

        mSaver.setWidgetSize( WidgetLocation.TOP, new PointF( mTopWidget.getWidth(), mTopWidget.getHeight() ) );
        mSaver.setWidgetSize( WidgetLocation.RIGHT, new PointF( mRightWidget.getWidth(), mRightWidget.getHeight() ) );
        mSaver.save();

        completeAddAppWidget( appWidgetId );
    }

    /**
     * Widget追加処理の本体
     *
     * @param id
     *            widgetのID
     */
    private void completeAddAppWidget( int id ) {
        int appWidgetId = id;
        mParent.removeAllViews();

        AppWidgetProviderInfo appWidgetInfo = mAppWidgetManager.getAppWidgetInfo( appWidgetId );

        PointF rightSize = mSaver.getWidgetSize( WidgetLocation.RIGHT );
        if (rightSize == null) {
            Log.d("WidgetHostActivity", "completeAddAppWidget rightSize == null id[" + id + "]");
//            rightSize = new PointF(mRightWidget.getWidth(), mRightWidget.getHeight());
//            mSaver.setWidgetSize( WidgetLocation.RIGHT, rightSize);
        }
        int rightWidth = (int)rightSize.x;
        int rightHeight = (int)rightSize.y;

        if ( mRightWidget.getId() == mTargetId ) {
            mRightWidget = createWidget( appWidgetId, appWidgetInfo );
            mSaver.setWidget( WidgetLocation.RIGHT, appWidgetId );
        } else {
            mTopWidget = createWidget( appWidgetId, appWidgetInfo );
            mSaver.setWidget( WidgetLocation.TOP, appWidgetId );
        }

        PointF topSize = mSaver.getWidgetSize( WidgetLocation.TOP );
        if (topSize == null) {
            Log.d("WidgetHostActivity", "completeAddAppWidget topSize == null id[" + id + "]");
//            topSize = new PointF(mTopWidget.getWidth(), mTopWidget.getHeight());
//            mSaver.setWidgetSize( WidgetLocation.TOP, topSize);
        }
        RelativeLayout.LayoutParams topParams = new RelativeLayout.LayoutParams( (int)topSize.x, (int)topSize.y );
        mTopWidget.setLayoutParams( topParams );

        mParent.addView( mTopWidget );

        RelativeLayout.LayoutParams rightParams = new RelativeLayout.LayoutParams( rightWidth, rightHeight );
        rightParams.addRule( RelativeLayout.RIGHT_OF, mTopWidget.getId() );
        mRightWidget.setLayoutParams( rightParams );
        mParent.addView( mRightWidget );

        RelativeLayout.LayoutParams mainParams = new RelativeLayout.LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT );
        mainParams.addRule( RelativeLayout.BELOW, mTopWidget.getId() );
        mainParams.addRule( RelativeLayout.LEFT_OF, mRightWidget.getId() );

        mSurfaceView = getSurfaceView();
        mSurfaceView.setLayoutParams(mainParams);
        mParent.addView(mSurfaceView);

        mMainView.setLayoutParams( mainParams );
        mParent.addView( mMainView );

        Log.v( "widget was added", "the ID of the widget view is " + mRightWidget.getId() );

    }

    /**
     * AppWidgetHostView を作成します。
     *
     * @param appWidgetInfo
     * @param appWidgetId
     * @param widget
     * @param w
     * @param h
     * @return
     */
    private AppWidgetHostView createWidget( int appWidgetId, AppWidgetProviderInfo appWidgetInfo ) {
        AppWidgetHostView widget = mAppWidgetHost.createView( this, appWidgetId, appWidgetInfo );
        widget.setId( mIdCounter++ );
        return widget;
    }

    /**
     * メニューの準備
     * onPrepareMenu から呼び出されることを想定しています。
     *
     * @param menu
     *            追加先
     * @param index
     *            追加先index
     * @return true
     */
    public boolean onPrepareWidgetClearMenu( Menu menu, int index ) {
        MenuItem item = menu.findItem( MENU_ID_CLEAR_WIDGET );
        if ( item == null ) {
            menu.add( Menu.NONE, MENU_ID_CLEAR_WIDGET, index, "ウィジェット クリア(要リスタート)" );
            item = menu.findItem( MENU_ID_CLEAR_WIDGET );
        }

        item.setVisible( true );

        return true;

    }

    /**
     * メニューハンドラ
     */
    public boolean onOptionsItemSelected( MenuItem item ) {
        switch ( item.getItemId() ) {
            case MENU_ID_CLEAR_WIDGET:
//    			if (((ItsmoNaviPnd)getApplication()).isAuditLog())
//    			{
//    				AuditLog log = (new AuditLog.Builder(SmarTaxiCenterAuditLogConstants.ACTION_ID_WIDGET_HOST_CLEAR_WIDGET, System.currentTimeMillis()))
//    					.build();
//    				AuditLogLogger.write(this, log);
//    			}
// MOD.2013.11.12 N.Sasao Widgetクリア後のアプリ再起動時、アプリがフリーズする START
        		widgetClear();
// MOD.2013.11.12 N.Sasao Widgetクリア後のアプリ再起動時、アプリがフリーズする  END
                return true;
        }

        return super.onOptionsItemSelected( item );
    }

    /**
     * ウィジェット クリア
     */
    public void widgetClear() {
// MOD.2013.11.12 N.Sasao Widgetクリア後のアプリ再起動時、アプリがフリーズする START
    	if( mAppWidgetHost != null && mSaver != null ){
    		int nRightId = mSaver.getWidget( WidgetLocation.RIGHT );
    		int nTopId = mSaver.getWidget( WidgetLocation.TOP );

    		if( nRightId >= 0 ){
    			mAppWidgetHost.deleteAppWidgetId( nRightId );
    		}
    		if( nTopId >= 0 ){
    			mAppWidgetHost.deleteAppWidgetId( nTopId );
    		}
        	mSaver.clear();
    	}
// MOD.2013.11.12 N.Sasao Widgetクリア後のアプリ再起動時、アプリがフリーズする  END
    }
// ADD.2014.01.21 N.Sasao OS4.4以降でWidgetが更新されない START
    public void startListening(){
        if ( mAppWidgetHost != null ) {
        	if( !mIsListening ){
        		mAppWidgetHost.startListening();
        		mIsListening = true;
        	}
        }
    }
    public void stopListening(){
        if ( mAppWidgetHost != null ) {
        	if( mIsListening ){
        		mAppWidgetHost.stopListening();
        		mIsListening = false;
        	}
        }
    }
// ADD.2014.01.21 N.Sasao OS4.4以降でWidgetが更新されない  END
    protected abstract View getSurfaceView();
}
