package net.zmap.android.pnd.v2.api.exception;

/**
 * サービス接続エラー
 */
public class NaviServiceException extends NaviException {
    private static final long serialVersionUID = 1L;

    /**
     * エラー詳細メッセージとして null を設定して NaviServiceException を構築します。
     */
    public NaviServiceException() {
        super();
    }

    /**
     * 詳細メッセージを指定して NaviServiceException を構築します。
     *
     * @param message
     *            詳細メッセージ
     */
    public NaviServiceException(String message) {
        super(message);
    }
}