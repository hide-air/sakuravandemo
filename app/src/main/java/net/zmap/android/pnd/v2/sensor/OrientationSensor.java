package net.zmap.android.pnd.v2.sensor;

import android.hardware.Sensor;
import android.hardware.SensorManager;

public class OrientationSensor {
	final static public int FROM_ACCELEROMETER = 0;
	final static public int FROM_ORIENTATION = 1;
	final static public int FROM_ACCELEROMETER_AND_MAGNETIC_FIELD = 2;

	public float orientation[] = new float[3];

	protected int _fromType;

	public OrientationSensor(int fromType) {
		_fromType = fromType;
	}

// Add 2011/03/10 Z01ONOZAWA Start --> 内部情報ログ出力
	final static public int WAIT_SENSOR = 0;
	final static public int SET_ACCELEROMETER = 1;
	final static public int SET_MAGNETIC_FIELD = 2;
	public int stateOrient = WAIT_SENSOR;

	public Sensor sensor;
	public long timestamp;
	public float acceleration[] = new float[3];
	public float magnetic[] = new float[3];

	/* 回転行列 */
	private static final int MATRIX_SIZE = 16;
	private float[]  inR = new float[MATRIX_SIZE];
	private float[] outR = new float[MATRIX_SIZE];
	private float[]    I = new float[MATRIX_SIZE];

	public OrientationSensor(int fromType, Sensor orientation) {
		_fromType = fromType;
		sensor = orientation;
	}
// Add 2011/03/10 Z01ONOZAWA End   <--

//Add 2011/09/07 Z01yoneya Start -->
    /*
     * バッファデータクリア
     */
    public void clearBuffer(){
        //特にクリアすべきバッファは無し
    }
//Add 2011/09/07 Z01yoneya End <--

// Chg 2011/02/24 sawada Start -->
//	public void onSensorChanged(SensorEvent event) {
	public void onSensorChanged(RawSensorEvent event) {
// Chg 2011/02/24 sawada End   <--
		switch (_fromType) {
		case FROM_ACCELEROMETER:
// Chg 2011/02/24 sawada Start -->
//			if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			if (true) {
// Chg 2011/02/24 sawada End   <--
				final double EPSILON = 0.00001;
				final float DIFF = 0.2f;
				float[] accelerometer = event.values.clone();
				float roll = 0;
				float pitch = 0;
				float yaw = 0;

				// roll
				if (Math.abs(accelerometer[2]) >= SensorManager.GRAVITY_EARTH) {
					if (accelerometer[2] >= 0) {
						roll = 0;
					 } else {
						roll = (float)Math.PI;
					 }
				} else {
					roll = (float)Math.asin(accelerometer[2] / SensorManager.GRAVITY_EARTH);
				}

				// pitch
				if (Math.abs(accelerometer[0]) < EPSILON) {
					if (accelerometer[1] > SensorManager.GRAVITY_EARTH - DIFF) {
						pitch = (float)(Math.PI / 2);
					} else if (accelerometer[1] < (-1) * (SensorManager.GRAVITY_EARTH - DIFF)) {
						pitch = (float)(Math.PI / 2);
					}
					if (accelerometer[2] < (-1) * (SensorManager.GRAVITY_EARTH - DIFF)) {
						pitch = (float)Math.PI;
					} else if (accelerometer[2] > (SensorManager.GRAVITY_EARTH - DIFF)) {
						pitch = 0.0f;
					}
				} else {
					pitch = (float)Math.atan(accelerometer[1] / accelerometer[0]);
				}

				// yaw
// Chg 2011/02/25 sawada Start -->
//				if (Math.abs(accelerometer[2]) < 0.01) {
//					if (Math.abs(accelerometer[1]) > 0.05) {
//						yaw = (float)(Math.PI / 2 * accelerometer[1] / Math.abs(accelerometer[1]));
//					} else {
//						//yaw = Float.NaN;
//						yaw = 0.0f;
//					}
//				} else {
//					yaw = (float)Math.atan(accelerometer[1] / accelerometer[2]);
//				}
//				if (yaw == 0 && accelerometer[2] < 0) {
//					yaw = (float)Math.PI;
//				}
				if (accelerometer[2] != 0) {
					yaw = (float)Math.atan(accelerometer[1] / Math.abs(accelerometer[2]));
				}
// Chg 2011/02/25 sawada End   <--

				orientation[2] = roll * (float)(180 / Math.PI);
				orientation[1] = -pitch * (float)(180 / Math.PI);
				orientation[0] = yaw * (float)(180 / Math.PI);
			}
			break;

		case FROM_ORIENTATION:
// Chg 2011/03/10 Z01ONOZAWA Start -->  内部情報ログ出力
			orientation = event.values.clone();
			timestamp = System.currentTimeMillis();
// Chg 2011/03/10 Z01ONOZAWA End   <--
			break;

		case FROM_ACCELEROMETER_AND_MAGNETIC_FIELD:
// Chg 2011/03/10 Z01ONOZAWA Start -->  内部情報ログ出力
			if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
				stateOrient = SET_ACCELEROMETER;
				acceleration = event.values.clone();
			} else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
				if (stateOrient == SET_ACCELEROMETER) {
					stateOrient = SET_MAGNETIC_FIELD;
					magnetic = event.values.clone();
					timestamp = System.currentTimeMillis();

					// 傾き計算
					SensorManager.getRotationMatrix(inR, I, acceleration, magnetic);

					//Activityの表示が縦固定の場合。横向きになる場合、修正が必要です
//					SensorManager.remapCoordinateSystem(inR, SensorManager.AXIS_X, SensorManager.AXIS_Z, outR);
					SensorManager.remapCoordinateSystem(inR, SensorManager.AXIS_Y, SensorManager.AXIS_Z, outR);
					SensorManager.getOrientation(outR, orientation);
				} else {
					stateOrient = WAIT_SENSOR;
				}
			}
// Chg 2011/03/10 Z01ONOZAWA End   <--
			break;
		}
	}
}
