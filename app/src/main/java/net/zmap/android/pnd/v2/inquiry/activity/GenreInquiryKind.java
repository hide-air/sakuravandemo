//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.inquiry.view.ItemView;

public class GenreInquiryKind extends GenreInquiryBase{

    private int curGenreKind = -1;
    private GenreKind genreLayoutList;
//Add 2011/06/09 Z01thedoanh Start -->
    private final long INIT_LAYOUT_BIGSHOW = 0;		//最初の大分類はグレーボタン数を表示
//Add 2011/06/09 Z01thedoanh End <--


    private final int []btn_id = new int[]{
            R.id.btn_amusement_120_59,
            R.id.btn_shopping_120_59,
            R.id.btn_eating_120_59,
            R.id.btn_life_120_59,
            R.id.btn_journey_120_59,
            R.id.btn_traffic_120_59,
    };
    private final int[] oNames = {
           R.string.genre_amusement_name,
           R.string.genre_shopping_name,
           R.string.genre_eating_name,
           R.string.genre_life_name,
           R.string.genre_travel_name,
           R.string.genre_traffic_name
           };
    private Button []genreBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_Clear();

        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_SetAddrKey(wideCode, middleCode, narrowCode);

        // add 20110216
        JNILong treeIndex_ = new JNILong();
        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetNextTreeIndex(0, treeIndex_);

        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_SearchNextList(lTreeRecIndex);

        doSearch();

        genreLayoutList = new GenreKind();
        initGenreKindLayout();
    }

    private void initGenreKindLayout(){

        oLayout = (LinearLayout) oInflater.inflate(R.layout.inquiry_base, null);
        Button obtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
        obtn.setVisibility(Button.GONE);

        oTextTitle = (TextView) oLayout.findViewById(R.id.inquiry_title);
        oTextTitle.setText(meTextName);

        ItemView oview = new ItemView(this,0,R.layout.inquiry_genre_freescroll);
        initBigKind(oview.getView());
        scrollList = (ScrollList) oview.findViewById(this, R.id.scrollList);
        scrollList.setAdapter(genreLayoutList);
//Del 2011/10/06 Z01_h_yamada Start -->
//        scrollList.setCountInPage(ONE_PAGE_COUNT);
//Del 2011/10/06 Z01_h_yamada End <--
        //yangyang mod start 20110420 Bug746
        scrollList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
//Del 2011/07/21 Z01thedoanh Start -->
        //scrollList.setPadding(5, 5, 2, 5);
//Del 2011/07/21 Z01thedoanh End <--
        //yangyang mod end 20110420 Bug746
//

        oGroupList = (LinearLayout)oLayout.findViewById(R.id.LinearLayout_list);
        if(oGroupList != null){
            oGroupList.removeAllViews();
            oGroupList.addView(oview.getView(), new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.FILL_PARENT,
                    LinearLayout.LayoutParams.FILL_PARENT));
        }

        //
        oTool = new ScrollTool(this);
        oTool.bindView(scrollList);
        removeOperArea();
        setViewInOperArea(oTool);

        getWorkArea().removeAllViews();
        setViewInWorkArea(oLayout);
    }


    /**
     * 大分类
     * @param oview
     */
    private void initBigKind(View oview) {

//        if(genreBtn == null)
        {

            genreBtn = new Button[btn_id.length];

            int i = 0;
            for(final int id : btn_id){
                final int index = i;
                genreBtn[index] = (Button) oview.findViewById(id);

                if(genreBtn[index] != null){

                    genreBtn[index].setSelected(curGenreKind == index)  ;

                    genreBtn[index].setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ ｼﾞｬﾝﾙ選択禁止 Start -->
        			    	if(DrivingRegulation.CheckDrivingRegulation()){
        			    		return;
        			    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ ｼﾞｬﾝﾙ選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                            if(LRecIdx == null || LRecIdx.length == 0){
                                showDialog(DIALOG_NODATA_ALARM);
                                return;
                            }

                            curGenreKind = index;

                            g_RecIndex = LRecIdx[index];
                            btnIndex = g_RecIndex;

                            //交通の場合
                            if(index == TRAFFIC){
                                recCount.lcount = lMiddleKindTotleCount - LRecIdx[TRAFFIC];
                            }else{
                            	//それ以外の場合
                                recCount.lcount = LRecIdx[index+1] - LRecIdx[index];
                            }

                            //選択した大分類中に、中分類の件数
                            remCount = recCount.lcount;
//                            NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy 157 remCount===" + remCount);
                            initGenreList();
                            recIndexNum = (int)btnIndex;

                            //タイトルの内容を初期化する
                            meTextName = getResources().getString(oNames[index]);
                            oTextTitle.setText(meTextName);

                            //大分類の初期化
                            initGenreBigKind(index);
                            //大分類のクリック事件
                            onClickBigKind((int)g_RecIndex, treeIndex);

                            scrollTo(0);
                        }
                    });
                }
                i ++;
            }
        }

        initGenreBigKind(curGenreKind);
    }

    private void initGenreBigKind(int focused_id){
        for (int i = 0; i < btn_id.length; i++) {
            if (genreBtn[i] != null) {
                genreBtn[i].setSelected(i == focused_id);
            }
        }
    }

    /**
     * Created on   2009/12/30
     * Title:search
     * Description: 検索レベルによって、当該Listを取得する
     * @param  無し
     * @return void 無し
     * @author: sun.dw
     * @version 1.0
     */
    protected void search() {
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"GenreKind: case " + treeIndex.lcount);
        switch ((int) treeIndex.lcount) {
        case 0:
            showBigKind();
            break;
        case 1:
            initGenreList();

            recCount.lcount = remCount;
            showList(btnIndex, (int) TEN_PAGE_COUNT, 0);

            break;
        case 2:
//            initGenreList();
//            NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetRecCount(recCount);
//
////            NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy recCount====" +  recCount.lcount) ;
//            if(recCount.lcount == 0){
//                showDialog(DIALOG_NODATA_ALARM);
//            }else{
//                g_RecIndex = 0;
//                showList(g_RecIndex, (int) TEN_PAGE_COUNT, 0);
//            }
        	startShowPage(NOTSHOWCANCELBTN);
            break;
        case 3: // 住所の一レベル

            Intent intent = new Intent();
            Intent oIntent = getIntent();
            if (oIntent != null) {
                intent.putExtras(oIntent);
            }

            intent.putExtra(Constants.PARAMS_TREERECINDEX, treeIndex.lcount);

            intent.putExtra(Constants.PARAMS_SEARCH_KEY, meTextName);
            intent.putExtra(Constants.PARAMS_LIST_INDEX, lTreeRecIndex);

            intent.setClass(this, GenreInquiryProvince.class);
//Chg 2011/09/23 Z01yoneya Start -->
//            startActivityForResult(intent, 0x3);
//------------------------------------------------------------------------------------------------------------------------------------
            NaviActivityStarter.startActivityForResult(this, intent, 0x3);
//Chg 2011/09/23 Z01yoneya End <--

            break;
        default:
            break;
        }
    }


    @Override
	protected boolean initWaitBackData() {
		if (treeIndex.lcount == 2) {
			initGenreList();
            NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetRecCount(recCount);

//            NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy 252 recCount====" +  recCount.lcount) ;
            if(recCount.lcount == 0){
                showDialog(DIALOG_NODATA_ALARM);
            }else{
                g_RecIndex = 0;
                showList(g_RecIndex, (int) TEN_PAGE_COUNT, -1);

            }
            return true;
		}
		return false;
	}

	@Override
	protected void resetDialog() {
		if (treeIndex.lcount == 2) {

			scrollList.reset();
			//yangyang add start 20110420
			scrollList.scroll(0);
			//yangyang add end 20110420
		}
		super.resetDialog();
	}

	/**
     * 大分類を表示
     */
    private void showBigKind(){
        int KindCount = jumpRecCount.getM_sJumpRecCount();
        LRecIdx = new Long[KindCount];
        recCount = new JNILong();
        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetRecCount(recCount);
        lMiddleKindTotleCount = recCount.lcount;

        JNILong jumpRecIndex = new JNILong();
        for (int i = 0; i < jumpRecCount.getM_sJumpRecCount(); i++) {
            NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetJumpList2RecordIndex(jumpKey[i].NameSize, jumpKey[i].pucName, jumpRecIndex);
            LRecIdx[i] = jumpRecIndex.lcount;
        }

        g_RecIndex = 0;
//        numRecIndex = 0;
        recCnt = 5;
//Chg 2011/06/10 Z01thedoanh Start -->
//      recCount.lcount = ONE_PAGE_COUNT;
        recCount.lcount = INIT_LAYOUT_BIGSHOW;
//Chg 2011/06/10 Z01thedoanh End <--
    }

    @Override
    protected boolean onClickGoMap() {

        boolean flag = super.onClickGoMap();
        clear();
        return flag;
    }

    @Override
    protected boolean onClickGoBack() {

        if(iconList != null){
            iconList.clear();
        }
        if(treeIndex.lcount >= 2){
            NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetPrevTreeIndex(treeIndex);
            NaviRun.GetNaviRunObj().JNI_NE_POIGnr_SearchPrevList();

            CommonLib.setBIsSearching(true);

            doSearch();

            int x = meTextName.lastIndexOf(">");
            if (x > 0) {
                meTextName = meTextName.substring(0, x);
            }
            oTextTitle.setText(meTextName);

            return true;
        }else {
            if(curGenreKind != -1){
                genreBtn[curGenreKind].setSelected(false);

                curGenreKind = -1;

                oTextTitle.setText(R.string.genre_title);
                meTextName = oTextTitle.getText().toString();

                if (genreList != null) {
                    genreList.clear();
                }

                remCount = 0;

                scrollList.reset();
//yangyang del start Bug957
//                return true;
//yangyang del end Bug957
            }
        }
        return super.onClickGoBack();
    }

    private void onClickBigKind(int RecIndex, JNILong TreeIndex) {

        if (TreeIndex.lcount >= 2) {
            NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetPrevTreeIndex(TreeIndex);
            NaviRun.GetNaviRunObj().JNI_NE_POIGnr_SearchPrevList();
            CommonLib.setBIsSearching(true);

            doSearch();

        }else{

            showList(RecIndex, (int) TEN_PAGE_COUNT, 1);
        }
    }

    private class GenreKind extends BaseAdapter{

        @Override
        public int getCount() {
            return getItemCount();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }
      //yangyang add start
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		// yangyang add end
		@Override
		public long getItemId(int position) {
			return 0;
		}

        @Override
        public View getView(final int wPos, View oView, ViewGroup parent) {

            LinearLayout oListLayout = null;
            if(oView == null || !(oView instanceof LinearLayout))
            {
                LayoutInflater inflater = LayoutInflater.from(GenreInquiryKind.this);
                oListLayout = (LinearLayout)inflater.inflate(R.layout.inquiry_distance_button_freescroll, null);
                oView = oListLayout;
            }
            else
            {
                oListLayout = (LinearLayout)oView;
            }

            LinearLayout oLayout = (LinearLayout) oListLayout.findViewById(R.id.LinearLayout_distance);
            oLayout.setBackgroundResource(R.drawable.btn_default);
            TextView oText = (TextView) oLayout.findViewById(R.id.TextView_value);
            ImageView oIcon = (ImageView) oLayout.findViewById(R.id.icon);

            if(iconList != null && iconList.size() > wPos){
                Bitmap bitmap = iconList.get(wPos);
                if(bitmap != null){

                    oIcon.setBackgroundDrawable(new BitmapDrawable(bitmap));
                    oIcon.setVisibility(View.VISIBLE);
                }else{
                	//yangyang mod start
                	//すべてなど、アイコンがない場合、アイコンを表示した場所は保留する
//                    oIcon.setVisibility(View.GONE);
                	oIcon.setVisibility(View.INVISIBLE);
                	//yangyang mod end
                }

            }

            boolean hasData = false;
//            NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy getview genreList.size()===" +
//            		genreList.size() + ",wPos====" + wPos +
//            		",recCnt===" + recCnt + ",treeIndex.lcount==" + treeIndex.lcount);
            if ((!bListDealing) && (wPos < recCnt)) {

            	//小分類の場合、次ページのデータをダウンロードする
				if (treeIndex.lcount == 2) {
					if (wPos != 0 && wPos % TEN_PAGE_COUNT == 0
							&& wPos >= recCnt) {
						showList(wPos, (int) TEN_PAGE_COUNT, -1);
					}
				}
                if(wPos < genreList.size()){
                    String text = genreList.get(wPos).getM_Name();
                    if (null != text && (!"".equalsIgnoreCase(text))) {
                        oLayout.setEnabled(true);
                        oText.setText(text);
                        oLayout.setOnClickListener(new OnClickListener() {
                            public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ ｼﾞｬﾝﾙ詳細選択禁止 Start -->
            			    	if(DrivingRegulation.CheckDrivingRegulation()){
            			    		return;
            			    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ ｼﾞｬﾝﾙ詳細選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                                onItemClick(wPos);
                                //yangyang add start Bug468
                                if (iconList.size() > wPos ) {
                                	clickGenreBMP = iconList.get(wPos);
                                } else {
                                	clickGenreBMP = null;
                                }
                              //yangyang add end Bug468
                            }
                        });

                        hasData = true;
                    }
                }
            }
            if(!hasData ){
            	//yangyang add start 20110420 Bug746
            	if (!(treeIndex.lcount == 2 && recCnt == 0)) {
            	//yangyang add end 20110420 Bug746
	                oIcon.setVisibility(View.GONE);
	                oText.setText("");
	                oLayout.setEnabled(false);
            	}
            }
            return oView;
        }
    }
}
