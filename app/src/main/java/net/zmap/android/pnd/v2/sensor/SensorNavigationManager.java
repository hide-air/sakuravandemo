package net.zmap.android.pnd.v2.sensor;

import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.NaviRun;
import android.app.Activity;

/**
 * 自律航法管理クラス
 *
 * 自律航法が可能かどうか（センサー有無）や
 * 自律航法を有効にするかどうか等の状態を管理する。
 *
 * @author Z01yoneya
 *
 */
public class SensorNavigationManager {
    private static SensorNavigationManager mSingleton = null;

    //自律航法の有効・無効フラグ
    private static boolean bEnableSensorNavigation = true;

    //ハードウェアがセンサー（加速度・ジャイロ両方)を持つかどうかのフラグ
    private static boolean bExistSensor = true;


    //GPS・センサー走行、またはログ走行
    public static final int NAVI_DRIVE_MODE_HARDWARE = 0; //GPS+センサー走行
    public static final int NAVI_DRIVE_MODE_LOG = 1;     //ログ走行
    //走行モード
    private static int naviDriveMode = NAVI_DRIVE_MODE_HARDWARE;

    /*
     * 初期化(生成)
     */
    public static void initialize(Activity activity, int initNaviDriveMode) {
        if(mSingleton != null){
            return;
        }
        mSingleton = new SensorNavigationManager();

        naviDriveMode = initNaviDriveMode;

        SensorDataManager.init(activity);

        bExistSensor = SensorDataManager.isExistSensor();

        //システム設定から自律航法する/しないを取得する
        bEnableSensorNavigation = SensorDataManager.getSystemSettingEnableSensorFlag();

        //NaviLog.d(NaviLog.PRINT_LOG_TAG, "SensorNavigationManager initialize bEnableSensorNavigation=" + bEnableSensorNavigation);


        //GPS/センサー走行の場合はセンサー搭載端末のみ自律航法可能にする
        if( !isEnableSensorNaviFlagSwitch() ){
            bEnableSensorNavigation = false;
        }

        if(bEnableSensorNavigation){
            resumeSensorNavi();
        }else{
            suspendSensorNavi();
        }
    }

    /*
     * 終了(破棄)
     */
    public static void destroy()
    {
        SensorDataManager.destroy();
    }

    /*
     * アプリ一時停止での処理
     */
    public static void suspend() {
        suspendSensorNavi();
    }

    /*
     * アプリ再開時の処理
     */
    public static void resume() {
        resumeSensorNavi();
    }


    /*
     * 自律航法の有効・無効を設定する
     */
    public static void enableSensorNavi(boolean bEnableSensorNavi)
    {

	   // V2.5 メニュー設定より呼ばれるので、変更状態をVPへ戻す(senser側からの通知削除)
        if (NaviRun.isNaviRunInitial()) {
            NaviRun obj = NaviRun.GetNaviRunObj();
            // V2.5ではセンサは常に動作、メニュー設定はVP内で使う、使わない判断とする
            if (obj != null) {
            	int flag = 0;
            	if(bEnableSensorNavi) flag = 1;
                obj.JNI_VP_SetEnableSensorNaviFlag(flag);
            }
        }

    	//GPS/センサーモードでセンサー非搭載なら切り替え不可にする
        if( !isEnableSensorNaviFlagSwitch()){return;}

        if(bEnableSensorNavigation == bEnableSensorNavi){ return; }

        bEnableSensorNavigation = bEnableSensorNavi;

        if(bEnableSensorNavi){
            resumeSensorNavi();
        }else{
            suspendSensorNavi();
        }
    }

    /*
     * 自律航法の有効・無効が切り替え可能か判定する
     * TRUEなら切り替え可能
     */
    public static boolean isEnableSensorNaviFlagSwitch()
    {
        //GPS/センサーモードでセンサー非搭載なら切り替え不可にする
        return !( isNaviDriveModeHardware() && !isExistSensor());
    }

    /*
     * 自律航法の有効・無効を取得する
     */
    public static boolean isEnableSensorNavi()
    {
        return bEnableSensorNavigation;
    }

    /*
     * 自律航法用のセンサー有無を取得する
     */
    private static boolean isExistSensor()
    {
        return bExistSensor;
    }

    /*
     * 自律航法を停止
     */
    private static void suspendSensorNavi()
    {
        //NaviLog.d(NaviLog.PRINT_LOG_TAG, "suspendSensorNavi");

        //SensorDataManager.stop(isNaviDriveModeHardware());
    }

    /*
     * 自律航法を再開
     */
    private static void resumeSensorNavi()
    {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "resumeSensorNavi");

        SensorDataManager.start(isNaviDriveModeHardware());
    }

    /*
     * 走行モードの判定
     *
     * 走行モードがGPS/センサーの場合 TRUE
     * ログ走行の場合、FALSE
     */
    private static boolean isNaviDriveModeHardware()
    {
        return naviDriveMode == NAVI_DRIVE_MODE_HARDWARE;
    }
}
