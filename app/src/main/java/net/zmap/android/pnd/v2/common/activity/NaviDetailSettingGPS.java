/**
 ********************************************************************
 * Copyright (c) 2010.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project        MarketV2
 * File           NaviDetailSetting.java
 * Description    ナビ詳細設定画面
 * Created on     2010/11/24
 *
 ********************************************************************
 */

package net.zmap.android.pnd.v2.common.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.data.JNIFloat;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.ZVP_SatInfoList_t;
import net.zmap.android.pnd.v2.data.ZVP_SatInfo_t;
import net.zmap.android.pnd.v2.gps.LocationStatusController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created on 2010/12/13
 * <p>
 * Title: ナビ詳細設定画面_GPS設定情報
 * <p>
 * Description
 * <p>
 * author　XuYang
 * version 1.0
 */
public class NaviDetailSettingGPS extends MenuBaseActivity  {

	LinearLayout oLayout;
	private TextView txtLatitude_Info = null;
	private TextView txtlongtitude_Info = null;
	private TextView txtheight_Info = null;
	private TextView txtdate_Info = null;
	private TextView txttime_Info = null;
	private Timer timer = null;
	private RelativeLayout layoutReceiveCnt = null;
	private RelativeLayout layoutRadarArea = null;
	private long LnX = 0;
	private long LnY = 0;
	private String strLatitude = null;
	private String strLongitude = null;


	private int intCnt = 13;
	/**
	 * 初期化処理
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		LayoutInflater oInflater = LayoutInflater.from(this);
		// 画面を追加
//Del 2011/09/17 Z01_h_yamada Start -->
//		setMenuTitle(R.drawable.gps_title);
//Del 2011/09/17 Z01_h_yamada End <--
		oLayout = (LinearLayout)oInflater.inflate(R.layout.setting_navi_detail_gps, null);
		txtLatitude_Info = (TextView) oLayout.findViewById(R.id.latitude_Info);
		txtlongtitude_Info = (TextView) oLayout.findViewById(R.id.longtitude_Info);
		txtheight_Info = (TextView) oLayout.findViewById(R.id.height_Info);
		txtdate_Info = (TextView) oLayout.findViewById(R.id.date_Info);
		txttime_Info = (TextView) oLayout.findViewById(R.id.time_Info);
		layoutReceiveCnt = (RelativeLayout) oLayout.findViewById(R.id.ReceiveRadarCountLayout);
        layoutRadarArea = (RelativeLayout) oLayout.findViewById(R.id.RadarAreaLayout);
		setViewInWorkArea(oLayout);
		onDraw();
		timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				Message message = new Message();
				message.what = 1;
				handler.sendMessage(message);
			}
		}, 0, 1000);
	}
	/**
	 * Timerのメッセージの処理ハンドラ
	 */
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			layoutReceiveCnt.removeAllViewsInLayout();
			layoutRadarArea.removeAllViewsInLayout();
			onDraw();
			super.handleMessage(msg);
		}
	};

	/**
	 * Created on 2010/12/16
	 * Title:       onDraw
	 * Description: 衛星の情報を絵する
	 * @param 無し
	 * @return 		無し
	 * @author         XuYang
	 * @version        1.0
	 */
	private void onDraw() {
		int iReceiveCount = 0;
		ZVP_SatInfoList_t stSatelitesInfo = null;
		// Test start
/*
		stSatelitesInfo.dAltitude = 20;
		stSatelitesInfo.lnLatitude1000 = 1111;
		stSatelitesInfo.lnLongitude1000 = 22222;
		stSatelitesInfo.wTotalSatsInView = intCnt;
		ZVP_SatInfo_t[] info = new ZVP_SatInfo_t[intCnt];

		for (int i = 0; i < intCnt; i++) {
			ZVP_SatInfo_t zvp1 = new ZVP_SatInfo_t();

			int	int1 = 6 + i * 5;
			int	int2 = 9 + i * 5;
			int	int3 = 30+ i * 5;

			zvp1.wSatID = i+1;
			zvp1.wAzimuth = int1;
			zvp1.wElevation = int2;
			zvp1.wSNRate= int3;
			//zvp1.bEnable = true;
			if (i%2 == 0) {
			    zvp1.bEnable = false;
			} else {
				zvp1.bEnable = true;
			}
			info[i] = zvp1;
		}
		stSatelitesInfo.stVPSatInfo = info;
*/
		// Test end
		stSatelitesInfo = CommonLib.getSatInfoList_t();
		ZVP_SatInfo_t[] Satellite = stSatelitesInfo.getStVPSatInfo();
//Add 2011/09/01 Z01yoneya Start -->
        //衛星状態イベント受信中しか描画しない
        if(LocationStatusController.IsGpsStatusReceive()){
//Add 2011/09/01 Z01yoneya End <--

//Add 2011/09/21 Z01_h_yamada Start -->
			int offsetX = 0;
        	int offsetY = 0;
        	RelativeLayout layoutGPSLeft = (RelativeLayout) oLayout.findViewById(R.id.GPSInfoLeftLayout);
        	if (layoutGPSLeft != null) {
        		offsetX = layoutGPSLeft.getTop() -layoutRadarArea.getTop();
        		offsetY = layoutGPSLeft.getLeft() -layoutRadarArea.getLeft();
        	}
//        	Log.i( "test", "imgTop=" + layoutGPSLeft.getTop() + " imgLeft=" + layoutGPSLeft.getLeft() +  " raderTop=" + layoutRadarArea.getTop() + " raderLeft=" + layoutRadarArea.getLeft() + " offsetX=" + offsetX + " offsetY=" + offsetY );
//Add 2011/09/21 Z01_h_yamada End <--

    		// Draw radar image of round area
    		for (int i = 0; i < stSatelitesInfo.getWTotalSatsInView(); i++) {
    			short sAzimuth = (short) Satellite[i].getWAzimuth();
    			short sElevation = (short) Satellite[i].getWElevation();
//Add 2011/09/30 Z01_h_yamada Start -->
				int gpspointWidth  = (int)(getResources().getDimensionPixelSize(R.dimen.SNDG_GPS_size));
				int gpspointHeight = gpspointWidth;

//Add 2011/09/30 Z01_h_yamada End <--
    			if (!getRadarPos(sAzimuth, sElevation, layoutRadarArea.getWidth(),
    					layoutRadarArea.getHeight(), Constants.DEF_RADRA_X,
    					Constants.DEF_RADRA_Y)) {
    				continue;
    			}
    			if (Satellite[i].isBEnable()) {
    				iReceiveCount++;
    				ImageView imgRadar = new ImageView(this);
    				imgRadar.setImageResource(R.drawable.gps_point_red);
//Chg 2011/09/21 Z01_h_yamada Start -->
//    				RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
//    						RelativeLayout.LayoutParams.WRAP_CONTENT,
//    						RelativeLayout.LayoutParams.WRAP_CONTENT);
//    				oParams.leftMargin = (int) LnX;
//    				oParams.topMargin = (int) LnY + 2;
//--------------------------------------------
    				imgRadar.setScaleType(ImageView.ScaleType.FIT_START);
    				RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
    						gpspointWidth,
    						gpspointHeight);
    				oParams.leftMargin = (int) LnX +offsetX;
    				oParams.topMargin = (int) LnY + offsetY;
//Chg 2011/09/21 Z01_h_yamada End <--
    				layoutRadarArea.addView(imgRadar, oParams);
    				layoutRadarArea.setVisibility(View.VISIBLE);
    			} else {
    				ImageView imgRadar = new ImageView(this);
    				imgRadar.setImageResource(R.drawable.gps_point_gray);
//Chg 2011/09/21 Z01_h_yamada Start -->
//    				RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
//    						RelativeLayout.LayoutParams.WRAP_CONTENT,
//    						RelativeLayout.LayoutParams.WRAP_CONTENT);
//    				oParams.leftMargin = (int) LnX;
//    				oParams.topMargin = (int) LnY + 2;
//--------------------------------------------
    				imgRadar.setScaleType(ImageView.ScaleType.FIT_START);
    				RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
    						gpspointWidth,
    						gpspointHeight);
    				oParams.leftMargin = (int) LnX +offsetX;
    				oParams.topMargin = (int) LnY + offsetY;
//Chg 2011/09/21 Z01_h_yamada End <--
    				layoutRadarArea.addView(imgRadar, oParams);
    				layoutRadarArea.setVisibility(View.VISIBLE);
    			}
    		}

    		// Draw received radar image of count area
//Chg 2011/09/20 Z01_h_yamada Start -->
//    		if (iReceiveCount > 0) {
//    			int iMove = layoutReceiveCnt.getWidth() / 6;
//    			if (iMove > Constants.DEF_ICON_W) {
//    				iMove = Constants.DEF_ICON_W;
//    			}
//    			int iX = Constants.DEF_RCEIVECOUNT_X;
//    			int iY = Constants.DEF_RCEIVECOUNT_Y;
//    			int iXbak = iX;
//    			int showCnt = 12;
//    			if (iReceiveCount < 13) {
//    				showCnt = iReceiveCount;
//    			}
//    			for (int i = 0; i < showCnt; i++) {
//    				ImageView imgRadar = new ImageView(this);
//    				if (iReceiveCount < 7) {
//    				    imgRadar.setImageResource(R.drawable.gps_point_red);
//    				} else {
//    				    imgRadar.setImageResource(R.drawable.gps_point_small_red);
//    				}
//
//    				RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
//    						RelativeLayout.LayoutParams.WRAP_CONTENT,
//    						RelativeLayout.LayoutParams.WRAP_CONTENT);
//
//    				if (i < 6) {
//    				    oParams.topMargin = iY;
//    				    oParams.leftMargin = iX;
//    				} else {
//    					oParams.topMargin = iY + iMove;
//    					oParams.leftMargin = iXbak;
//    					iXbak += iMove;
//    				}
//
//    				layoutReceiveCnt.addView(imgRadar, oParams);
//    				layoutReceiveCnt.setVisibility(View.VISIBLE);
//    				iX += iMove;
//    			}
//    		}
//--------------------------------------------
    		if (iReceiveCount > 0) {
    			final int RECEIVE_HORZ_COUNT = 6;
    			final int RECEIVE_VERT_COUNT = 2;
    			final int RECEIVE_MARGIN = 2;
    			final int RECEIVE_ALL_COUNT =RECEIVE_HORZ_COUNT *  RECEIVE_VERT_COUNT;

    			int iOneWidth  = (layoutReceiveCnt.getWidth() -(RECEIVE_MARGIN *RECEIVE_HORZ_COUNT)) / RECEIVE_HORZ_COUNT;
    			int iOneHeight = (layoutReceiveCnt.getHeight() -(RECEIVE_MARGIN *RECEIVE_VERT_COUNT)) /RECEIVE_VERT_COUNT;

    			int showReceiveCnt = iReceiveCount;
    			if (iReceiveCount > RECEIVE_ALL_COUNT) {
    				showReceiveCnt = RECEIVE_ALL_COUNT;
    			}

    			for (int i = 0; i < showReceiveCnt; i++) {
    				ImageView imgRadar = new ImageView(this);
   				    imgRadar.setImageResource(R.drawable.gps_point_red);
    				imgRadar.setScaleType(ImageView.ScaleType.FIT_START);

    				RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
    						iOneWidth,
    						iOneHeight);

    				oParams.topMargin  = (iOneHeight +RECEIVE_MARGIN) *(int)(i/RECEIVE_HORZ_COUNT);
    				oParams.leftMargin = (iOneWidth +RECEIVE_MARGIN) *(i%RECEIVE_HORZ_COUNT);

    				layoutReceiveCnt.addView(imgRadar, oParams);
    			}
				layoutReceiveCnt.setVisibility(View.VISIBLE);

    		}
//Chg 2011/09/20 Z01_h_yamada End <--


//Add 2011/09/01 Z01yoneya Start -->
	    }
//Add 2011/09/01 Z01yoneya End <--

		// Draw Latitude and Longitude
//Chg 2011/09/01 Z01yoneya Start -->
//		if (iReceiveCount > 2) {
//--------------------------------------------
        //座標イベント受信中しか経緯度を描画しない
        if(LocationStatusController.IsGpsLocationReceive()){
//Chg 2011/09/01 Z01yoneya End <--
			String strAltitude = null;
			long lnLatitude = stSatelitesInfo.lnLatitude1000;
			long lnLongitude = stSatelitesInfo.lnLongitude1000;

			getLatitudeStr(lnLatitude);
			txtLatitude_Info.setText(strLatitude);

			getLongitudeStr(lnLongitude);
			txtlongtitude_Info.setText(strLongitude);

//Chg 2011/09/02 Z01yoneya Start -->
//			strAltitude = String.valueOf(stSatelitesInfo.getDAltitude());
//--------------------------------------------
			strAltitude = String.format("%.1f", stSatelitesInfo.getDAltitude());
//Chg 2011/09/02 Z01yoneya End <--
            txtheight_Info.setText(strAltitude + "m");

			// Draw current date
			Date date = new Date(System.currentTimeMillis());
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
			txtdate_Info.setText(formatter.format(date));
//Chg 2011/09/17 Z01_h_yamada Start -->
//			Calendar systemTime= Calendar.getInstance();
//	        int systemHours = systemTime.getTime().getHours();
//	        String strHours = String.valueOf(systemHours);
//	        int systemMinutes = systemTime.getTime().getMinutes();
//	        String strMinutes = String.valueOf(systemMinutes);
//	        String time = strHours + ":" + strMinutes;
//			txttime_Info.setText(time + " 現在");
//--------------------------------------------
			SimpleDateFormat formatterTime = new SimpleDateFormat("HH:mm");
			txttime_Info.setText(formatterTime.format(date) + " 現在");
//Chg 2011/09/17 Z01_h_yamada End <--

		} else {
			txtLatitude_Info.setText("--°--′--″");
			txtlongtitude_Info.setText("--°--′--″");
			txtheight_Info.setText("-m");
			txtdate_Info.setText("----年--月--日");
			txttime_Info.setText("--:--" + " 現在");
		}
	}
	/**
	* Created on 2010/12/16
	* Title:       getLongitudeStr
	* Description:  衛星の地区を取得する
	* @param1  int wAzimuth
	* @param2  int wElevation
	* @param3  long lnRadarW
	* @param4  long lnRadarH
	* @param5  long lnRadarX
	* @param6  long lnRadarY
	* @return 		void
	* @author         XuYang
	* @version        1.0
	*/
	private boolean getRadarPos(int wAzimuth, int wElevation, long lnRadarW,
			long lnRadarH, long lnRadarX, long lnRadarY) {
		if (wAzimuth < 0 || wAzimuth > 360 || wElevation < 0 || wElevation > 90) {
			return false;
		}
		LnX = 0;
		LnY = 0;
		long lnCenterX = lnRadarX + lnRadarW / 2;
		long lnCenterY = lnRadarY + lnRadarH / 2;

		long lnRadarR = lnRadarW / 2;
		if (lnRadarW > lnRadarH) {
			lnRadarR = lnRadarH / 2;
		}

		long lnLen = (90 - wElevation) * lnRadarR / 90;

		JNIFloat objfCosA = new JNIFloat();
		JNIFloat objfSinA = new JNIFloat();
		NaviRun.GetNaviRunObj().JNI_CMN_sin((short) wAzimuth, objfSinA);
		NaviRun.GetNaviRunObj().JNI_CMN_cos((short) wAzimuth, objfCosA);
		float fCosA = objfCosA.getM_fCommon();
		float fSinA = objfSinA.getM_fCommon();
		long lnTempX = (long) ((float) 0.0 * fCosA + (float) (lnLen) * fSinA);
		long lnTempY = (long) ((float) 0.0 * fSinA - (float) (lnLen) * fCosA);
//Chg 2011/09/30 Z01_h_yamada Start -->
//		long lnIconW = Constants.DEF_ICON_W;
//		long lnIconH = Constants.DEF_ICON_H;
//--------------------------------------------
		long lnIconW = (int)(getResources().getDimensionPixelSize(R.dimen.SNDG_GPS_size));
		long lnIconH = lnIconW;
//Chg 2011/09/30 Z01_h_yamada End <--

		long ln_x = (long) (lnCenterX + lnTempX - lnIconW / 2);
		long ln_y = (long) (lnCenterY + lnTempY - lnIconH / 2);
		LnX = ln_x;
		LnY = ln_y;
		return true;
	}
	/**
	* Created on 2010/12/16
	* Title:       getLatitudeStr
	* Description:  緯度を取得する
	* @param1  long lnLatitude  緯度
	* @return 		void
	* @author         XuYang
	* @version        1.0
	*/
	private void getLatitudeStr(long lnLatitude) {

		long lnDeg = 0;
		long lnMin = 0;
		long lnSec = 0;
		strLatitude = null;
		if (lnLatitude <= 0 || lnLatitude > 90 * 60 * 60 * 1000) {
			strLatitude = "--°--′--″";
			return;
		}
		lnDeg = lnLatitude / (60 * 60 * 1000);
		lnMin = lnLatitude / (60 * 1000) % 60;
		lnSec = lnLatitude / 1000 % 60;

		strLatitude = String.valueOf(lnDeg);
		strLatitude = strLatitude + "°";
		strLatitude = strLatitude + String.valueOf(lnMin);
		strLatitude = strLatitude + "′";
		strLatitude = strLatitude + String.valueOf(lnSec);
		strLatitude = strLatitude + "″";
	}
	/**
	* Created on 2010/12/16
	* Title:       getLongitudeStr
	* Description:  経度を取得する
	* @param1  long lnLongitude  経度
	* @return 		void
	* @author         XuYang
	* @version        1.0
	*/
	private void getLongitudeStr(long lnLongitude) {

		long lnDeg = 0;
		long lnMin = 0;
		long lnSec = 0;
		strLongitude = null;
		if (lnLongitude <= 0 || lnLongitude > 180 * 60 * 60 * 1000) {
			strLongitude = "--°--′--″";
			return;
		}

		lnDeg = lnLongitude / (60 * 60 * 1000);
		lnMin = lnLongitude / (60 * 1000) % 60;
		lnSec = lnLongitude / 1000 % 60;

		strLongitude = String.valueOf(lnDeg);
		strLongitude = strLongitude + "°";
		strLongitude = strLongitude + String.valueOf(lnMin);
		strLongitude = strLongitude + "′";
		strLongitude = strLongitude + String.valueOf(lnSec);
		strLongitude = strLongitude + "″";
	}
	/**
	* Created on 2010/12/16
	* Title:       onKeyDown
	* Description:  onKeyDownの処理
	* @param1  keyCode キーコードを押す
	* @param2  event イベント
	* @return 		boolean
	* @author         XuYang
	* @version        1.0
	*/
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			timer.cancel();
			this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected boolean onClickGoBack() {
	    super.onClickGoBack();
		timer.cancel();
		return false;
	}
	@Override
	protected boolean onClickGoMap() {
	    super.onClickGoMap();
		timer.cancel();
		return false;
	}

}
