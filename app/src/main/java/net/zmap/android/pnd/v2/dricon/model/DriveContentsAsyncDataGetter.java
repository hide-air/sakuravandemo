/**
 *  @file		DriveContentsAsyncDataGetter
 *  @brief		ドライブコンテンツ WEB情報取得クラス
 *
 *  @attention
 *  @note		非同期で実行される
 *
 *  @author		Manabu Watanabe [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2011-09-01)
 *  @version	$Revision: $ by $Author: $
 *
 */
package net.zmap.android.pnd.v2.dricon.model;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import net.zmap.android.pnd.v2.common.http.HttpUtils;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.xml.XmlUtils;
import net.zmap.android.pnd.v2.data.DC_ContentsData;
import net.zmap.android.pnd.v2.data.DC_ContentsDetailData;
import net.zmap.android.pnd.v2.data.DC_ContentsItemData;
import net.zmap.android.pnd.v2.data.DC_MapIconData;
import net.zmap.android.pnd.v2.data.DC_POIInfoDetailData;
import net.zmap.android.pnd.v2.data.DC_SectionOptionData;
import net.zmap.android.pnd.v2.data.DC_POIInfoDetailData.SectionInfo;
import net.zmap.android.pnd.v2.data.DC_POIInfoDetailData.TextInfo;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
//ADD -- 2012/03/09 -- Z01H.Fukushima(No.407) --- ↓ ----- ref #3930
import java.io.UnsupportedEncodingException;
//ADD -- 2012/03/09 -- Z01H.Fukushima(No.407) --- ↑ ----- ref #3930
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * @author watanabe
 *
 */
public class DriveContentsAsyncDataGetter extends AsyncTask<String,Void,Integer> {

// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
	private static final String CONTENT_FILE_NAME = "ContentsTmp";
	private static final String CONTENT_OPPOSING_PATH = "dricon/";
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
	private static final String USER_SETTING_NAME = "ContextDC.xml";
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

    private static final String CONTENTS_ID_ORBIS    	= "zdcorbis";

    private static final String TAG_ROOT    	= "drivecontents";
    private static final String TAG_STATUS		= "status";
    private static final String TAG_CODE		= "code";
    private static final String TAG_MESSAGE		= "message";
    private static final String TAG_CONTENTS	= "contents";
    private static final String TAG_ID			= "id";
    private static final String TAG_AVAILABLE	= "available";
    private static final String TAG_LAYERING	= "layering";
    private static final String TAG_NAME		= "name";
    private static final String TAG_ICON		= "icon";
    private static final String TAG_MSEARCH		= "msearch";
    private static final String TAG_CONTENTSID	= "contentsID";
    private static final String TAG_RESOURCE	= "resource";

    private static final String TAG_SETTING		= "settings";
    private static final String TAG_SECTION		= "section";
    private static final String TAG_OPTION		= "option";
    private static final String TAG_ITEM		= "item";
    private static final String TAG_SELECT		= "select";
    private static final String TAG_MULTISELECT	= "multiselect";
    private static final String TAG_SWITCH		= "switch";
    private static final String TAG_SLIDER		= "slider";
    private static final String TAG_CREATED		= "created";
    private static final String TAG_DURATION	= "duration";
    private static final String TAG_GROUP		= "group";

    private static final String TAG_QUERY		= "query";
    private static final String TAG_MESH		= "mesh";
    private static final String TAG_RESULT		= "result";
    private static final String TAG_POI			= "poi";
    private static final String TAG_LAT			= "lat";
    private static final String TAG_LON			= "lon";
    private static final String TAG_DESCRIPTION	= "desc";
    private static final String TAG_ADDRESS		= "address";
    private static final String TAG_TEL			= "tel";
    private static final String TAG_URL			= "url";
    private static final String TAG_FIELDS		= "fields";
    private static final String TAG_TEXT		= "text";
    private static final String TAG_MIN			= "min";
    private static final String TAG_MAX			= "max";

    private static final String TAG_NATIVE		= "native";
    private static final String TAG_ROADTYPE	= "roadtype";

// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
    private static final String TAG_REQURL		= "requrl";
    private static final String TAG_WEBTOLABEL	= "webtolabel";
    private static final String TAG_WEBTOURL	= "webtourl";
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END

    private static final String KEY_MESH		= "mesh";
    private static final String KEY_ID			= "id";
    private static final String KEY_ANCHOR		= "anchor";
    private static final String KEY_KEY			= "key";
    private static final String KEY_VALUE		= "value";
    private static final String KEY_TEXT		= "text";
    private static final String KEY_NAME		= "name";
    private static final String KEY_LABEL		= "label";
    private static final String KEY_DEFAULT		= "default";
    private static final String KEY_ALL			= "all";

    private static final String KEY_URLTEXT_DEFAULT		= "ブラウザで見る";
    private static final String MULTISELECT_ALL		= "all";


    public final static int DOWNLOAD_MAIN 		= 1;
    public final static int DOWNLOAD_MESH 		= 2;
    public final static int DOWNLOAD_MAIN_INNER = 3;
    public final static int DOWNLOAD_ORBIS_MESH = 4;
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
    public final static int DOWNLOAD_POI_DETAIL = 5;
    private static final String POIDETAILFILE	= "showPoiDetail";
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END

// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
    public final static int READ_CONTENTS_LIST  = 6;
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END

    private static final int	CHECKITEM_CHECKED		= 0x1;
    private static final int	CHECKITEM_LAYERING		= 0x2;
    private static final int	CHECKITEM_DISPLAY		= 0x4;

    private static final short	UNKNOWN_SECTION		= Short.MAX_VALUE;

    final static int FILETYPE_CONTENTS	= 0;
    final static int FILETYPE_MESH 		= 1;
    private final static int FILETYPE_ICON 		= 2;
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
    private final static int FILETYPE_POIDETAIL	= 3;
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
    final static int FILETYPE_MESH_ALL 		= 4;
    final static int FILETYPE_USER_SETTING = 5;
    final static int FILETYPE_ALL 		= 6;
    private static final int CONNECTION_RETRY_COUNT = 3;
    private static final int CONNECTION_DELAY_TIME = 300;
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

    private final static int DELETE_MESH_TIME	= -1;
    private final static int IGNORE_MESH_TIME	= -2;
    private Object		mObjectLock = null;

    public List<DC_ContentsData> lstContents = new ArrayList<DC_ContentsData>();
    public Map<Integer , DC_ContentsDetailData> mapDetail = new HashMap<Integer , DC_ContentsDetailData>();

    //MapKey format is  "MESHNUM(8 digit)" + "Object Key(2 digit)"+ "POI Num(3 digit)" .
    //Object Key is mapDetail's key.
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
    public Map<Long , DC_POIInfoDetailData> mapPOIDetail = new LinkedHashMap<Long , DC_POIInfoDetailData>();
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
    public DC_POIInfoDetailData showPOIDetailData = new DC_POIInfoDetailData();
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END

    public List<DC_POIInfoDetailData> mOrbisPOIDetailList = null;

    public List<Integer> mMeshList = null;
    public List<String> mOrbisMeshList = null;
//    public Rect			mdisplayRect = null;

    String mUserPath = null;
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
    String mDataPath = null;
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
	private int mTask = DOWNLOAD_MAIN;

	private onEndGetListener _listener = null;        //リスナー

// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2) START
	private int mlstDataOffset = 0;
// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2)  END

    public interface onEndGetListener{
    	public void onEndData(List<DC_ContentsData> list , Map<Integer , DC_ContentsDetailData> mapData);
    	public void onEndAroundSearch( Map<Long , DC_POIInfoDetailData> mapData);
    	public void onEndOrbisData(List<DC_POIInfoDetailData> list);
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
    	public void onCancel(int nTask, Object obj);
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
    	public void onResultDetailPOIData(DC_POIInfoDetailData poiDetailData);
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
    }

    /**
     * コンテンツ保持情報のクリア
     */
    public void  clearContents(){
    	if(lstContents != null){
    		lstContents = null;
    	}
    	if(mapDetail != null){
    		mapDetail = null;
    	}
    	if(mapPOIDetail != null){
    		mapPOIDetail = null;
    	}
    	if(mMeshList != null){
    		mMeshList = null;
    	}
    }

// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2) START
	 /**
	  * 作成するデータのオフセットを取得する
  	 * @return 作成データのオフセット
  	 */
    public int GetlstDataOffset(){
    	return mlstDataOffset;
    }
	 /**
	  * 作成するデータのオフセットを設定する
  	 * @param 作成データのオフセット
  	 */
    public void SetlstDataOffset( int nlstDataOffset ){
    	mlstDataOffset = nlstDataOffset;
    }
// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2)  END

// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
	 /**
	  * コンストラクタ
	 * @param listener 解析終了時のイベントを送るためのポインタ
	 * @param obj 同期処理を行うためのオブジェクト
	 */
	public  DriveContentsAsyncDataGetter(onEndGetListener listener , Object obj, String userPath, String dataPath){
		_listener = listener;
		mObjectLock = obj;
        mUserPath = userPath;
        mDataPath = dataPath;
	}
// MDD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
    public static String GetContentsOpposingPath(){
    	return CONTENT_OPPOSING_PATH;
    }
    public static String GetContentsName(){
    	return CONTENT_FILE_NAME;
    }
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END

	 /**
	 * 作業内容を設定する
	 * @param Task 作業内容
	 *	 DOWNLOAD_MAIN
	 *	 DOWNLOAD_MESH
	 *	 DOWNLOAD_ORBIS_MESH
	 */
	public void SetTask(int Task){
		mTask = Task;
	}
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
	/**
	* メッシュ情報を読み解くためのコンテンツ情報を指定する
	* @param list コンテンツ情報
	* @param DetailMap コンテンツ詳細情報
	*/
	public void SetContentsList(List<DC_ContentsData> list ,
			Map<Integer, DC_ContentsDetailData> DetailMap,
			Map<Long , DC_POIInfoDetailData> POIDetail ){
	   if(list != null){
		   lstContents = new ArrayList<DC_ContentsData>(list);
		   mapDetail = new HashMap<Integer , DC_ContentsDetailData>(DetailMap);
		   mapPOIDetail = new LinkedHashMap<Long , DC_POIInfoDetailData>(POIDetail);
	   }
   }
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
  	/**
  	 * 読み込むメッシュの番号リストを取得する
  	 * @param meshList 情報取得のためのメッシュリスト
  	 * @return メッシュリストが存在する場合、true
  	 */
  	public boolean SetMeshList(List<Integer> meshList )
	{
		if(meshList == null){
			return false;
		}
		mMeshList = null;
		mMeshList = new ArrayList<Integer>(meshList);
		return true;
	}
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
  	/**
  	 * 読み込むメッシュの番号リストを取得する
  	 * @param meshList 情報取得のためのメッシュリスト
  	 * @return メッシュリストが存在する場合、true
  	 */
  	public void SetRequestPoiId(long Id){
  		showPOIDetailData = new DC_POIInfoDetailData();
		showPOIDetailData = mapPOIDetail.get(Id);
  	}
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
  	/**
  	 * オービスを読み込むメッシュの番号リストを取得する
  	 * @param meshList 情報取得のためのメッシュリスト
  	 * @return メッシュリストが存在する場合、true
  	 */
  	public boolean SetOrbisMeshList(List<String> meshList){
		if(meshList == null){
			return false;
		}
		mOrbisMeshList = null;
		mOrbisMeshList = new ArrayList<String>(meshList);
		return true;
   }

	/* (非 Javadoc)
	// メインスレッドで実行する処理
	 * @see android.os.AsyncTask#onPostExecute(Result)
	 */
	@Override
	protected void onPostExecute(Integer result) {
		if(_listener != null){
		    if(result == null) {
		        result = 0;
		    }
			switch(mTask){
			case DOWNLOAD_MAIN:
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
			case READ_CONTENTS_LIST:
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
				if(result !=0){
					_listener.onEndData(lstContents , mapDetail);    // イベントの通知
				}else{
					_listener.onEndData(null,null);    // イベントの通知
				}
				break;
			case DOWNLOAD_MESH:
// MOD.2013.04.05 N.Sasao POI詳細情報取得失敗対応 START
				if(result !=0){
					_listener.onEndAroundSearch(mapPOIDetail);    // イベントの通知
				}
				else{
					_listener.onEndAroundSearch(null);			// イベントの通知
				}
// MOD.2013.04.05 N.Sasao POI詳細情報取得失敗対応  END
				break;
			case DOWNLOAD_ORBIS_MESH:
				_listener.onEndOrbisData(mOrbisPOIDetailList);   // イベントの通知
				break;
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
			case DOWNLOAD_POI_DETAIL:
				if(result !=0){
					_listener.onResultDetailPOIData(showPOIDetailData);  // イベントの通知
				}
				else{
					_listener.onResultDetailPOIData(null);  // イベントの通知
				}
			}
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
		}
		clearContents();
	}

	/* (非 Javadoc)
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected Integer doInBackground(String... params) {
		synchronized(mObjectLock){
// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2) START
			if(isCancelled()){
				return 0;
			}
// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2)  END
			switch(mTask){
			case DOWNLOAD_MAIN:
				String paramURL = null;
				if(params != null){
					paramURL = params[0];
				}
				return GetContentsData(paramURL);
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
			case READ_CONTENTS_LIST:
				return GetCashContentsData();
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
			case DOWNLOAD_MESH:
				return GetMeshData();
			case DOWNLOAD_ORBIS_MESH:
				return GetOrbisMeshData();
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
			case DOWNLOAD_POI_DETAIL:
				return GetPOIDataDetail();
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
			}
		}
		return 0;
	}

    /* (非 Javadoc)
     * @see android.os.AsyncTask#onCancelled()
     */
    @Override
    protected void onCancelled() {
		if(_listener != null){
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
			_listener.onCancel(mTask, mapPOIDetail);
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
		}
		clearContents();
    }

   	/**
   	 * メッシュデータを取得する
   	 * @return 成功すればtrue
   	 */
   	private Integer  GetMeshData(){

// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2) START
   		int k = mlstDataOffset;
// ADD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2)  END

   		if(lstContents == null || mapDetail == null || mMeshList == null){
   			return 0;
   		}

   		if(mapPOIDetail == null){
// MOD.2013.04.05 N.Sasao POI詳細情報取得失敗対応 START
        	return 0;
// MOD.2013.04.05 N.Sasao POI詳細情報取得失敗対応  END
        }
// MOD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2) START
   		Set<Integer> orbisList = new HashSet<Integer>();

		for( ; k < lstContents.size() ; k++){
			mlstDataOffset = k;
// MOD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2)  END
			if(lstContents.get(k).m_bDisplayItem == false){
				continue;
			}
			if(lstContents.get(k).m_bLayering == false && lstContents.get(k).m_bCheckItem == false){
				continue;
			}

			int nFileDestroy = 0;
			for(int i = 0 ; i < mMeshList.size() ; i++){
	        	if(isCancelled()){
	        		return 0;
	        	}

				String meshUrl = mapDetail.get(lstContents.get(k).m_iContentsID).m_sURL;
				int lMeshNum =  mMeshList.get(i);
				if(meshUrl.indexOf(CONTENTS_ID_ORBIS) != -1){
					lMeshNum = lMeshNum/100;
					if(orbisList.contains(lMeshNum)){
						continue;
					}
					orbisList.add(lMeshNum);
				}

				String meshStr = ((Integer)lMeshNum).toString();
				String sIDStr = "" + meshStr + "" + String.format("%02d", lstContents.get(k).m_iContentsID) + "000";
				if(mapPOIDetail.containsKey(Long.parseLong(sIDStr)) == true){
					continue;
				}

				long lParseID = Long.parseLong(sIDStr);

				int nReplaceNum = meshUrl.indexOf("$m");
				StringBuffer sURLBuf = new StringBuffer(meshUrl);
				meshUrl = sURLBuf.replace(nReplaceNum , nReplaceNum + 2 , meshStr ).toString();
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
				HttpUtils httpUtils = new HttpUtils(meshUrl, mDataPath);
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

				if(lstContents.get(k).m_mapSectionItemList != null){
					for(int j = 0; j < lstContents.get(k).m_mapSectionItemList.size() ; j++){
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1) START
			        	if(isCancelled()){
			        		return 0;
			        	}
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1)  END
						List<DC_ContentsItemData> contentsData = lstContents.get(k).m_mapSectionItemList.get(lstContents.get(k).m_mapSectionItemList.keySet().toArray()[j]);
						for(int n = 0; n < contentsData.size() ; n++){
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1) START
				        	if(isCancelled()){
				        		return 0;
				        	}
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1)  END
							if(contentsData.get(n).m_sItemValueList != null && contentsData.get(n).m_sItemKey  != null){
                                if(contentsData.get(n).m_nItemKind != DC_ContentsItemData.ITEM_TYPE_MULTI_SELECT){
                                    StringBuilder strValue = new StringBuilder();
                                    int size = contentsData.get(n).m_sItemValueList.size();
                                    for(int nValue = 0 ; nValue < size; nValue++){
                                          strValue.append(contentsData.get(n).m_sItemValueList.toArray()[0]).append(",");
                                    }

                                    if(strValue.length() > 0){
                                        strValue.deleteCharAt(strValue.length() -1);
                                    }

                                    httpUtils.addParam(contentsData.get(n).m_sItemKey , strValue.toString());
								}else{
									if(contentsData.get(n).m_nAllType != DC_ContentsItemData.MULTI_ALL_TYPE_NOTHING){
										String strValue = "";
										if(!contentsData.get(n).m_bAllSelect){
											for(int nValue = 0 ; nValue < contentsData.get(n).m_sItemValueList.size(); nValue++){
												strValue = strValue + contentsData.get(n).m_sItemValueList.toArray()[nValue] + ",";
											}
											if(strValue.length() > 0){
												strValue = strValue.substring(0, strValue.length() - 1);
											}
										}else{
											strValue = MULTISELECT_ALL;
										}
										httpUtils.addParam(contentsData.get(n).m_sItemKey , strValue);
									}
								}
							}
						}
					}
				}

				String sID = httpUtils.getRequestValue(KEY_ID);
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
				String strPath = getLocalFilePathEx( httpUtils , FILETYPE_MESH );
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
				Element root = null;
				if(strPath != null){
					root = readXmlData(new File(strPath));
		           	if(isCancelled()){
	            		return 0;
	            	}

					if(root == null){
						File deleteFile = new File(strPath);
						if(deleteFile.delete()){
							if(nFileDestroy==0){
								i--;
							}else{
								nFileDestroy = 0;
								continue;
							}
							nFileDestroy++;
						}
						continue;
					}

					if(root != null){
// MOD.2013.03.01 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(3) START
						int nID = parseMeshData(root , sID , meshStr , lParseID, lstContents.get(k).m_iContentsID );
// MOD.2013.03.01 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(3)  END

						root = null;
						if(nID == DELETE_MESH_TIME){
							File deleteFile = new File(strPath);
							if(deleteFile.delete()){
								if(nFileDestroy==0){
									i--;
								}else{
									nFileDestroy = 0;
									continue;
								}
								nFileDestroy++;
							}
							continue;
						//file time is future time or duration = 0.
						}else if(nID == IGNORE_MESH_TIME){
							nFileDestroy = 0;
							continue;
						}else{
							nFileDestroy = 0;
						}
					}
				}
			}
// MOD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2) START
			break;
		}
		mlstDataOffset = k + 1;
// MOD.2013.02.28 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(2)  END
    	return 1;
    }

  	/**
  	 * オービス用のメッシュデータを取得する
  	 * @return 成功すればtrue
  	 */
  	private Integer  GetOrbisMeshData(){
		mOrbisPOIDetailList = null;
   		if(lstContents == null || mapDetail == null || mOrbisMeshList == null){
   			return 0;
   		}

   		List<Integer> contentsIDList = CreateMeshURLIDs();
   		if(contentsIDList == null){
   			return 0;
   		}

   		mOrbisPOIDetailList = new ArrayList<DC_POIInfoDetailData>();

		int nID = 0;
		int nFileDestroy = 0;
   		for(int i = 0 ; i < mOrbisMeshList.size() ; i++){
			String meshStr = mOrbisMeshList.get(i);
			for(int j = 0 ; j < contentsIDList.size() ; j ++){
				String meshUrl = mapDetail.get(contentsIDList.get(j)).m_sURL;
				if(meshUrl.indexOf(CONTENTS_ID_ORBIS) == -1){
					continue;
				}

				int nReplaceNum = meshUrl.indexOf("$m");
				StringBuffer sURLBuf = new StringBuffer(meshUrl);
				meshUrl = sURLBuf.replace(nReplaceNum , nReplaceNum + 2 , meshStr ).toString();
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
				HttpUtils httpUtils = new HttpUtils(meshUrl, mDataPath);

				String sID = httpUtils.getRequestValue(KEY_ID);
				String sMesh = httpUtils.getRequestValue(KEY_MESH);

				String strPath = getLocalFilePathEx( httpUtils , FILETYPE_MESH );
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
				if(strPath != null){
					Element root = null;
					root = readXmlData(new File(strPath));
		           	if(isCancelled()){
	            		return 0;
	            	}

		           	if(root == null){
						File deleteFile = new File(strPath);
						if(deleteFile.delete()){
							if(nFileDestroy==0){
								i--;
							}else{
								nFileDestroy = 0;
								continue;
							}
							nFileDestroy++;
						}
						continue;
					}

					if(root != null){
						int nreturnID = parseOrbisMeshData(root , sID , meshStr , nID++);
						root = null;
						if(nreturnID == DELETE_MESH_TIME){
							File deleteFile = new File(strPath);
							if(deleteFile.delete()){
								if(nFileDestroy==0){
									i--;
								}else{
									nFileDestroy = 0;
									continue;
								}
								nFileDestroy++;
							}
							continue;
						//file time is future time or duration = 0.
						}else if(nreturnID == IGNORE_MESH_TIME){
							nFileDestroy = 0;
							continue;
						}else{
							nFileDestroy = 0;
						}
					}

				}
			}
		}

    	return 1;
    }

	/**
	 * 描画を行うメッシュ情報のリストを取得する
	 * @return 描画するメッシュ情報リスト
	 */
	private List<Integer> CreateMeshURLIDs()
	{
		List<Integer>	lstIDs = null;
		for(int j = 0 ; j < lstContents.size() ; j ++){
			if(lstContents.get(j).m_bDisplayItem == false){
				continue;
			}
			if(lstContents.get(j).m_bLayering == true ||
				lstContents.get(j).m_bCheckItem == true){
				if(lstIDs == null){
					lstIDs = new ArrayList<Integer>();
				}
				lstIDs.add(lstContents.get(j).m_iContentsID);
			}
		}
		return lstIDs;
	}

    /**
     * 指定された情報からURLを取得し、解析を行う
     * @param url 情報取得URL
     * @return 成功すれば 1、失敗なら0
     */
    private Integer  GetContentsData(String url){

    	if(url != null){
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
		    HttpUtils httpUtils = new HttpUtils(url, mDataPath);

			String strPath = getLocalFilePathEx( httpUtils , FILETYPE_CONTENTS );
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

		    if(strPath == null){
		    	return 0;
		    }
		    if(lstContents.size() == 0){
		    	Element root = null;
		    	root = readXmlData(new File(strPath));
	           	if(isCancelled()){
            		return 0;
            	}
		    	boolean bSuccess = false;
		    	if(root != null){
		    		bSuccess = parseContentsData(root);
		    	}
// DEL.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
//				File deleteFile = new File(strPath);
//				if(deleteFile.delete()){
//				}
// DEL.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
				if(!bSuccess){
					return 0;
				}
		    }
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
			String strFilePath = mUserPath + USER_SETTING_NAME;
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
		    File file = new File(strFilePath);
		    if(file.exists()){
		    	Element root = null;
		    	root = readXmlData(new File(strFilePath));
	           	if(isCancelled()){
            		return 0;
            	}
		    	boolean bSuccess = false;
		    	if(root != null){
		    		bSuccess = parseContentsUserData(root);
		    	}
		    	if(!bSuccess){
					File deleteFile = new File(strFilePath);
					if(deleteFile.delete()){
						return 1;
					}
		    	}
		    }
	    	return 1;
    	}

    	return 0;
    }
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
    private Integer  GetCashContentsData(){

// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
    	String strPath = new String(mDataPath);
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

	    strPath += CONTENT_OPPOSING_PATH + CONTENT_FILE_NAME;

	    if(lstContents.size() == 0){
	    	Element root = null;
	    	root = readXmlData(new File(strPath));
           	if(isCancelled()){
        		return 0;
        	}
	    	boolean bSuccess = false;
	    	if(root != null){
	    		bSuccess = parseContentsData(root);
	    	}
			File deleteFile = new File(strPath);
			if(deleteFile.delete()){
			}
			if(!bSuccess){
				return 0;
			}
	    }
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
		String strFilePath = mUserPath + USER_SETTING_NAME;
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
	    File file = new File(strFilePath);
	    if(file.exists()){
	    	Element root = null;
	    	root = readXmlData(new File(strFilePath));
           	if(isCancelled()){
        		return 0;
        	}
	    	boolean bSuccess = false;
	    	if(root != null){
	    		bSuccess = parseContentsUserData(root);
	    	}
	    	if(!bSuccess){
				File deleteFile = new File(strFilePath);
				if(deleteFile.delete()){
					return 1;
				}
	    	}
	    }
    	return 1;
    }
// ADD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
    /**
     * XMLデータを解析用の変換を行う
     * @param file ファイル
     * @return DOMで解析した結果。失敗の場合、NULL。
     */
    public Element readXmlData(File file) {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        Element root = null;

        try {
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document Doc = docBuilder.parse(file);

            root = Doc.getDocumentElement();

        } catch (ParserConfigurationException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } catch (SAXException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } catch (Exception e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }
        return root;
    }

// ADD -- 2012/03/09 -- Z01H.Fukushima(No.407) --- ↓ ----- ref #3930
    protected String replaceHyphen(String str) {
        if (str != null) {
            final byte ed[]  = { (byte)0xe2, (byte)0x80, (byte)0x94 };  // em dash
            final byte wd[]  = { (byte)0xe3, (byte)0x80, (byte)0x9c };  // wave dash
            final byte dvl[] = { (byte)0xe2, (byte)0x80, (byte)0x96 };  // double vertical line
            final byte ms[]  = { (byte)0xe2, (byte)0x88, (byte)0x92 };  // minus sign
            final byte cs[]  = { (byte)0xc2, (byte)0xa2 };  // cent sign
            final byte ps[]  = { (byte)0xc2, (byte)0xa3 };  // pound sign
            final byte ns[]  = { (byte)0xc2, (byte)0xac };  // not sign
            try {
                str = str.replace(new String(ed,  "UTF-8"), "ー");
                str = str.replace(new String(wd,  "UTF-8"), "～");
                str = str.replace(new String(dvl, "UTF-8"), "∥");
                str = str.replace(new String(ms,  "UTF-8"), "ー");
                str = str.replace(new String(cs,  "UTF-8"), "￠");
                str = str.replace(new String(ps,  "UTF-8"), "￡");
                str = str.replace(new String(ns,  "UTF-8"), "￢");
             } catch (UnsupportedEncodingException e) {
                // 何もしない
            }
        }
        return str;
    }
// ADD -- 2012/03/09 --Z01H.Fukushima(No.407) --- ↑ ----- ref #3930

    /**
     * DOMの解析した結果を元に、コンテンツ情報を収集する
     * @param root DOM解析結果
     * @return 成功した場合、trueを返す。
     */
    public boolean parseContentsData(Element root) {

        if (root == null) {
            return false;
        }

	    String rootTagName = root.getNodeName();

	    if (rootTagName == null || rootTagName.compareTo(TAG_ROOT) != 0) {
	        //error
	        return false;
	    }
        String strStatus[] ={TAG_STATUS};
        Node statusList = XmlUtils.getFirstNode(root , strStatus);
    	if(statusList == null){
    		return false;
    	}else{
            Node codeNode  = XmlUtils.FindFirstChildNode(statusList , TAG_CODE);
            if(codeNode == null){
            	return false;
            }
        	String codeStr  = codeNode.getFirstChild().getNodeValue();
        	if(!codeStr.equals("0000")){
        		return false;
        	}
    	}

	    NodeList ContentsList = root.getElementsByTagName(TAG_CONTENTS);
	    for(int i = 0 ; i < ContentsList.getLength(); i++){
	    	Node ContentsNode = ContentsList.item(i);

	        String[] availableStr = {TAG_AVAILABLE};
	        Node availableNode  = XmlUtils.getNodeValue(ContentsNode , availableStr);
	        if(availableNode != null){
	        	String id = availableNode.getFirstChild().getNodeValue();
	        	if(id == null || id .equals("0") ){
	        		continue;
	        	}
	        }else{
	    		continue;
	        }

	        Node IconNode  = XmlUtils.FindFirstChildNode(ContentsNode, TAG_ICON);
	        Node idNode  = XmlUtils.FindFirstChildNode(ContentsNode , TAG_ID);
	        Node NameNode  = XmlUtils.FindFirstChildNode(ContentsNode , TAG_NAME);
	        Node SearchNode  = XmlUtils.FindFirstChildNode(ContentsNode , TAG_MSEARCH);

	        DC_ContentsData data = null;
	    	DC_ContentsDetailData detailData = null;

	        if(IconNode == null || idNode == null || NameNode == null || SearchNode == null){
	        	continue;
	        }
	    	data = new DC_ContentsData();
	    	detailData = new DC_ContentsDetailData();

        	detailData.m_sContentsID = idNode.getFirstChild().getNodeValue();
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
        	HttpUtils httpUtils = new HttpUtils(IconNode.getFirstChild().getNodeValue(), mDataPath);
// MOD.2013.03.05 N.Sasao Bug #10373 アプリ起動に失敗する START

        	data.m_sIconPath = getLocalFilePathEx( httpUtils , FILETYPE_ICON );

        	if(isCancelled()){
        		return false;
        	}
// MOD.2013.03.05 N.Sasao Bug #10373 アプリ起動に失敗する  END
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
            if (data.m_sIconPath == null) {
    	        continue;
    	    }
            data.m_sContentsName = NameNode.getFirstChild().getNodeValue();

        	detailData.m_sURL = SearchNode.getFirstChild().getNodeValue();
            for(int j = 1 ; j < SearchNode.getChildNodes().getLength() ; j++){
            	if(SearchNode.getChildNodes().item(j).getNodeType() != Node.ELEMENT_NODE){
            		detailData.m_sURL += (SearchNode.getChildNodes().item(j).getNodeValue());
            	}
            }

	    	detailData.m_mapIconList = new HashMap<String,DC_MapIconData>();

	        Node layerNode  = XmlUtils.FindFirstChildNode(ContentsNode , TAG_LAYERING);
    		data.m_bDisplayItem = true;

	        if(layerNode != null){
	        	String id = layerNode.getFirstChild().getNodeValue();
	        	if(id != null && id.equals("1")){
	        		data.m_iStateFlag = DC_ContentsData.DISPLAY_STATE_LAYER;
	        		data.m_bLayering = true;
	        	}else{
	        		data.m_iStateFlag = DC_ContentsData.DISPLAY_STATE_DEFAULT;
	        	}
	        }

	        String[] resourceStr = {TAG_RESOURCE};
	        Node IconListNode  = XmlUtils.getNodeValue(ContentsNode , resourceStr);
	        if(IconListNode != null){
	        	List<Node> IconList = XmlUtils.FindChildNodes(IconListNode ,TAG_ICON );
	        	if(IconList == null){
	        		continue;
	        	}
	        	for(int j = 0 ; j < IconList.size() ;j++){
	        		String urlStr = IconList.get(j).getFirstChild().getNodeValue();

	            	DC_MapIconData mapIcon = new DC_MapIconData();
	        		NamedNodeMap mapAttr = IconList.get(j).getAttributes();
	        		if(mapAttr == null ){
	        			continue;
	        		}

        			mapIcon.m_sID = mapAttr.getNamedItem(KEY_ID).getNodeValue();
	            	String elemAnchor = mapAttr.getNamedItem(KEY_ANCHOR).getNodeValue();
	            	String b[] = elemAnchor.split(",");
	            	mapIcon.m_nX = Integer.parseInt(b[0]);
	            	mapIcon.m_nY = Integer.parseInt(b[1]);

	            	mapIcon.m_sIconPath = null;
	            	mapIcon.m_sURL = urlStr;
	        		detailData.m_mapIconList.put(mapIcon.m_sID , mapIcon);
	        	}
	        }

	        Node settingNode  = XmlUtils.FindFirstChildNode(ContentsNode , TAG_SETTING);
	        if(settingNode !=null){
	        	data.m_mapSectionItemList = new HashMap<String , List<DC_ContentsItemData>>();

	        	List<Node> SectionList = XmlUtils.FindChildNodes(settingNode ,TAG_SECTION );
	        	for(int j = 0 ; j < SectionList.size() ;j++){

	        		Node SectionAttr = SectionList.get(j).getAttributes().getNamedItem(KEY_NAME);
	        		if(SectionAttr == null){
	        			continue;
	        		}
	        		String SectionName = SectionAttr.getNodeValue();
	        		List<Node> itemList = XmlUtils.FindChildNodes(SectionList.get(j) ,TAG_ITEM );

/* MOD .2013.04.05 N.Sasao コベリティ指摘対応 START	*/
	        		if( itemList != null ){
		        		List<DC_ContentsItemData> ItemList = new ArrayList<DC_ContentsItemData>();

		        		for(int k = 0 ; k < itemList.size() ;k++){
		            		DC_ContentsItemData itemData = new DC_ContentsItemData();
		            		Node KeyAttr = itemList.get(k).getAttributes().getNamedItem(KEY_KEY);
		            		Node valueAttr = itemList.get(k).getAttributes().getNamedItem(KEY_TEXT);
	                		Node DefaultAttr = itemList.get(k).getAttributes().getNamedItem(KEY_DEFAULT);

		            		if(KeyAttr != null && valueAttr != null){
		            			itemData.m_sItemKey = KeyAttr.getNodeValue();
		            			itemData.m_sItemText = valueAttr.getNodeValue();
	                    		if(DefaultAttr != null){
	                    			itemData.m_sItemDefaultValue = DefaultAttr.getNodeValue();
	                    			if(itemData.m_sItemDefaultValue!= null){
	                					itemData.m_sItemValueList = new HashSet<String>();

	                    				String[] strValList = itemData.m_sItemDefaultValue.split(",");
	                    				for(int nValue = 0 ; nValue < strValList.length ; nValue++){
	                    					itemData.m_sItemValueList.add(strValList[nValue]);
	                    				}
	                    			}
	                    		}

		            			Node itemKindnode = XmlUtils.FindFirstChildNode(itemList.get(k));

		                    	if(itemKindnode != null){
		                    		String strKindName = itemKindnode.getNodeName();
		                    		if(strKindName.equals(TAG_SELECT)){
		                    			itemData.m_nItemKind = DC_ContentsItemData.ITEM_TYPE_SELECT;
		                    		}else if(strKindName.equals(TAG_MULTISELECT)){
		                    			itemData.m_nItemKind = DC_ContentsItemData.ITEM_TYPE_MULTI_SELECT;
		                    			Node multiAttr = itemKindnode.getAttributes().getNamedItem(KEY_ALL);
		    	            			if(multiAttr != null){
		    	            				if(Integer.parseInt(multiAttr.getNodeValue()) == 1){
		    	            					itemData.m_nAllType = DC_ContentsItemData.MULTI_ALL_TYPE_ON;
		    	            					itemData.m_bAllSelect = true;
		    	            				}else{
		    	            					itemData.m_nAllType = DC_ContentsItemData.MULTI_ALL_TYPE_OFF;
		    	            					itemData.m_bAllSelect = false;
		    	            				}
		    	            			}else{
		    	            				itemData.m_nAllType = DC_ContentsItemData.MULTI_ALL_TYPE_NOTHING;
		    	            			}
		                    		}else if(strKindName.equals(TAG_SLIDER)){
		                    			itemData.m_nItemKind = DC_ContentsItemData.ITEM_TYPE_SLIDER;
		                    		}else if(strKindName.equals(TAG_SWITCH)){
		                    			itemData.m_nItemKind = DC_ContentsItemData.ITEM_TYPE_SWITCH;
		                    			Node switchAttr = itemKindnode.getAttributes().getNamedItem(KEY_VALUE);
		    	            			if(switchAttr != null){
		    	            				DC_SectionOptionData option = new DC_SectionOptionData();
		    	            				option.m_sOptionValue = switchAttr.getNodeValue();
		    	            				itemData.m_OptionList.add(option);
		    	            			}
		                    		}
		                	        Node minNode  = XmlUtils.FindFirstChildNode(itemKindnode , TAG_MIN);
		                	        if(minNode != null){
		                	        	itemData.m_nMinimum = Integer.parseInt(minNode.getFirstChild().getNodeValue());
		                	        }else{
		                	        	itemData.m_nMinimum = 0;
		                	        }
		                	        Node maxNode  = XmlUtils.FindFirstChildNode(itemKindnode , TAG_MAX);
		                	        if(maxNode != null){
		                	        	itemData.m_nMaximum = Integer.parseInt(maxNode.getFirstChild().getNodeValue());
		                	        }else{
		                	        	itemData.m_nMaximum = 0;
		                	        }

		                    		itemData = parseOptionData(itemData , itemKindnode , 0 , 0);
		                    	}
		            		}
		            		ItemList.add(itemData);
		            	}
		            	data.m_mapSectionItemList.put(SectionName, ItemList);
	        		}
/* MOD .2013.04.05 N.Sasao コベリティ指摘対応  END  */
	        	}
	        }

	        data.m_iContentsID = i;
	        lstContents.add(data);
	        mapDetail.put(i, detailData);
	    }
        return true;
    }

    /**
     * コンテンツの内部情報を取得する
     * @param data 登録用元データ
     * @param parentNode 取得データ
     * @param nParentID 親のID
     * @param nChildID 子のID
     * @return 取得したデータ
     */
    private DC_ContentsItemData  parseOptionData(DC_ContentsItemData data , Node parentNode , int nParentID , int nChildID) {
		List<Node> optionList = XmlUtils.FindChildNodes(parentNode ,TAG_OPTION);
		if(optionList != null){
        	for(int l = 0 ; l < optionList.size() ;l++){
        		DC_SectionOptionData optionData = new DC_SectionOptionData();
        		Node nodeOptionText = optionList.get(l).getAttributes().getNamedItem(KEY_TEXT);
        		Node nodeOptionValue = optionList.get(l).getAttributes().getNamedItem(KEY_VALUE);
        		optionData.m_nParentID = nParentID;
        		optionData.m_nChildID = 0;

        		if(nodeOptionText != null && nodeOptionValue != null){
        			optionData.m_sOptionText = nodeOptionText.getNodeValue();
        			optionData.m_sOptionValue = nodeOptionValue.getNodeValue();
        			data.m_OptionList.add(optionData);
        		}
        	}
		}
		data = parseGroupData(data , parentNode , nParentID , nChildID) ;

		return data;
    }

    /**
     * コンテンツの内部情報から、グループ情報を取得する
     * @param data 登録用元データ
     * @param parentNode 取得データ
     * @param nParentID 親のID
     * @param nChildID 子のID
     * @return 取得したデータ
     */
    public DC_ContentsItemData  parseGroupData(DC_ContentsItemData data , Node parentNode ,  int nParentID , int nChildID) {
		List<Node> groupList = XmlUtils.FindChildNodes(parentNode ,TAG_GROUP);
		if(groupList != null){
        	for(int l = 0 ; l < groupList.size() ;l++){
        		DC_SectionOptionData optionData = new DC_SectionOptionData();
        		Node nodeOptionText = groupList.get(l).getAttributes().getNamedItem(KEY_TEXT);
        		optionData.m_nParentID = nParentID;
        		optionData.m_nChildID = nChildID++;

        		if(nodeOptionText != null){
        			optionData.m_sOptionText = nodeOptionText.getNodeValue();
        			optionData.m_sOptionValue = null;
        			data.m_OptionList.add(optionData);
        		}
        		data.m_nGroupNum = nChildID;
        		data = parseOptionData(data , groupList.get(l) , nChildID , 0);
        		nChildID = data.m_nGroupNum;
        	}
		}

		return data;
    }

    /**
     * ユーザが保存したコンテンツ情報を取得する
     * @param root ユーザ保存のDOM展開情報
     * @return 成功の場合true。
     */
    public boolean parseContentsUserData(Element root) {
        if (root == null) {
            return false;
        }

        NodeList ContentsInnerList = root.getElementsByTagName(TAG_CONTENTS);

        for(int i = 0 ; i < ContentsInnerList.getLength(); i++){
    		int contentsCode = Integer.MAX_VALUE;
            String[] idStr = {TAG_ID};
            Node layerNode  = XmlUtils.getNodeValue(ContentsInnerList.item(i) , idStr);
            if(layerNode != null){
            	String id = layerNode.getFirstChild().getNodeValue();
				for(Entry<Integer, DC_ContentsDetailData> entry :  mapDetail.entrySet()){
            		int keyCode = entry.getKey();
            		DC_ContentsDetailData data = entry.getValue();

            		if(data.m_sContentsID.equals(id)){
            			for(int k = 0 ; k < lstContents.size(); k++){
            				if(lstContents.get(k).m_iContentsID ==(Integer) keyCode){
            					contentsCode = k;
        	                	DC_ContentsData contentsData = lstContents.get(contentsCode);
        	                	if(contentsData != null){
	            	                String[] availableStr = {TAG_AVAILABLE};
	            	                Node availableNode  = XmlUtils.getNodeValue(ContentsInnerList.item(i) , availableStr);
	            	                if(availableNode != null){
	            	                	String sAvailable = availableNode.getFirstChild().getNodeValue();
	            	                	int nCheck = Integer.parseInt(sAvailable);
	            	                	if(contentsData.m_iStateFlag == contentsData.DISPLAY_STATE_LAYER){
	            	                		contentsData.m_bLayering = ((nCheck & CHECKITEM_LAYERING) != 0);
	            	                	}
            	                		contentsData.m_bDisplayItem =((nCheck & CHECKITEM_DISPLAY) != 0);
            	                		contentsData.m_bCheckItem =((nCheck & CHECKITEM_CHECKED) != 0);
	            	                }else{
	            	            		continue;
	            	                }

	            	                String[] settingStr = {TAG_SETTING};
	            	                Node settingNode  = XmlUtils.getNodeValue(ContentsInnerList.item(i) , settingStr);
	            	    	        if(settingNode !=null){
	            	    	        	List<Node> SectionList = XmlUtils.FindChildNodes(settingNode ,TAG_SECTION );
	            	    	        	if(SectionList == null){
	            	    	        		continue;
	            	    	        	}
	            						for(int m = 0 ; m < SectionList.size() ; m++){
	            	    	        		Node SectionAttr = SectionList.get(m).getAttributes().getNamedItem(KEY_NAME);
	            	    	        		if(SectionAttr == null){
	            	    	        			continue;
	            	    	        		}

	            	    	        		String SectionName = SectionAttr.getNodeValue();

	            				    		List<DC_ContentsItemData> contentsItemData = contentsData.m_mapSectionItemList.get(SectionName);
	            	    	        		if(contentsItemData == null){
	            	    	        			continue;
	            	    	        		}

	            			        		List<Node> itemList = XmlUtils.FindChildNodes(SectionList.get(m) ,TAG_ITEM );
	            	    	        		if(itemList == null){
	            	    	        			continue;
	            	    	        		}

		            	    	        	for(int n = 0 ; n < itemList.size() ; n++){
	            	    	            		Node KeyAttr = itemList.get(n).getAttributes().getNamedItem(KEY_KEY);
	            	    	            		Node valueAttr = itemList.get(n).getAttributes().getNamedItem(KEY_VALUE);
	            	    	            		Node allAttr = itemList.get(n).getAttributes().getNamedItem(KEY_ALL);
	            	    	            		if(KeyAttr != null && valueAttr != null){
		            	    	            		String KeyText = KeyAttr.getNodeValue();
		            	    	            		String valueText = valueAttr.getNodeValue();
			            	    	        		for(int nItemIndex = 0 ; nItemIndex < contentsItemData.size(); nItemIndex++){
			            	    	        			if(contentsItemData.get(nItemIndex).m_sItemKey.equals(KeyText)){
			            	    	        				contentsItemData.get(nItemIndex).m_sItemValueList = null;
			            	    	        				contentsItemData.get(nItemIndex).m_sItemValueList = new HashSet<String>();
			            	    	        				if(contentsItemData.get(nItemIndex).m_nItemKind == DC_ContentsItemData.ITEM_TYPE_MULTI_SELECT){
			            	    	        					if(contentsItemData.get(nItemIndex).m_nAllType != DC_ContentsItemData.MULTI_ALL_TYPE_NOTHING){
			            	    	        						if(allAttr != null){
			            	    	        							contentsItemData.get(nItemIndex).m_bAllSelect = (allAttr.getNodeValue().equals("1"));
			            	    	        						}
			            	    	        					}
			            	                    				String[] strValList = valueText.split(",");
			            	                    				for(int nValue = 0 ; nValue < strValList.length ; nValue++){
			            	                    					contentsItemData.get(nItemIndex).m_sItemValueList.add(strValList[nValue]);
			            	                    				}
			            	    	        				}else{
		            	                    					contentsItemData.get(nItemIndex).m_sItemValueList.add(valueText);
			            	    	        				}
			            	    	        				break;
			            	    	        			}
			            	    	        		}
	            	    	            		}

	            	    	            		contentsData.m_mapSectionItemList.put(SectionName, contentsItemData);
		            	    	        	}
		            	    	        }
	            	    			}
	    		                	lstContents.remove( contentsCode);
	    		                	lstContents.add(contentsCode ,contentsData);
            					}
            					break;
            				}
            			}
            			if(contentsCode != Integer.MAX_VALUE){
            				break;
            			}
            		}
            	}
            }
        }
        return true;
    }

    /**
     * メッシュデータを解析する
     * @param root DOM解析したメッシュ情報
     * @param sContentsID コンテンツID
     * @param sMeshName メッシュ名称
     * @param lParseID 連番用ID
     * @param nContentsID コンテンツID( No )
     * @return 取得したPOIの数。ファイルエラーの場合、以下の値を返す
     *         -1 ファイルの削除が必要な場合
     *         -2 このファイルを無視する場合
     */
// ADD.2013.03.01 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(3) START
    public int parseMeshData(Element root , String sContentsID , String sMeshName, long lParseID, int nContentsID) {
// ADD.2013.03.01 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(3)  END
    	if (root == null) {
            return 0;
        }

		DC_ContentsDetailData data =null;
    	for(int j = 0 ; j < mapDetail.size() ; j++){
    		Object keyCode = mapDetail.keySet().toArray()[j];

    		if(sContentsID.equals( mapDetail.get(keyCode).m_sContentsID)){
    			data = mapDetail.get(keyCode);
    			break;
    		}
    	}

    	if(data == null){
    		return 0;
    	}

        String strStatus[] ={TAG_STATUS};
        Node statusList = XmlUtils.getFirstNode(root , strStatus);
    	if(statusList == null){
    		return 0;
    	}else{
            Node codeNode  = XmlUtils.FindFirstChildNode(statusList , TAG_CODE);
            if(codeNode == null){
            	return 0;
            }
        	String codeStr  = codeNode.getFirstChild().getNodeValue();
        	if(!codeStr.equals("0000")){
        		return 0;
        	}
    	}


        String strResult[] ={TAG_RESULT};
        Node resultList = XmlUtils.getFirstNode(root , strResult);

        if(resultList == null){
    		return 0;
        }

        //For delete
        Node durationNode  = XmlUtils.FindFirstChildNode(resultList , TAG_DURATION);
        if(durationNode != null){
        	long lDurationSec = Integer.parseInt(durationNode.getFirstChild().getNodeValue());
        	Node created  = XmlUtils.FindFirstChildNode(resultList , TAG_CREATED);
        	if(created != null){
	            long lTime =  System.currentTimeMillis();
	            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	            Date getDate = null;
	            try {
					getDate = sdf.parse(created.getFirstChild().getNodeValue());
				} catch (DOMException e) {
					NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
				} catch (ParseException e) {
					NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
				}

				if(getDate != null){
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(getDate);
					long getmilitime = calendar.getTimeInMillis();
		            long lDiffTime = lTime - getmilitime;
		            if(lDurationSec * 1000 < lDiffTime){
						if(lDiffTime < 0  || lDurationSec == 0){
							return IGNORE_MESH_TIME;
						}
		            	return DELETE_MESH_TIME;
		            }
				}
        	}
       }

        List<Node> resultPOI = XmlUtils.FindChildNodes(resultList, TAG_POI);
	    if(resultPOI == null){
	    	return 0;
	    }

	    int nID = 0;
	    for(int i = 0 ; i < resultPOI.size() ; i++){
	    	Node resultNode = resultPOI.get(i);
			if (this.isCancelled()) {
				return 0;
			}

			DC_POIInfoDetailData detailData = getOneMeshData(resultNode , sMeshName ,data);
			if(detailData != null){
				detailData.m_POIInfo.m_POIId = lParseID + nID;

// ADD.2013.03.01 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(3) START
				detailData.m_nContentsID = nContentsID;
// ADD.2013.03.01 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(3)  END

				mapPOIDetail.put( lParseID + nID , detailData);
		        nID++;
			}
	    }

        return nID;
    }

    /**
     * メッシュデータを解析する
     * @param root DOM解析したメッシュ情報
     * @param sContentsID コンテンツID
     * @param sMeshName メッシュ名称
     * @param lParseID 連番用ID
     * @return 取得したPOIの数。ファイルエラーの場合、以下の値を返す
     *         -1 ファイルの削除が必要な場合
     */
    public int parseOrbisMeshData(Element root , String sContentsID , String sMeshName, long lParseID) {
    	 if (root == null) {
            return 0;
        }

		DC_ContentsDetailData data =null;
    	for(int j = 0 ; j < mapDetail.size() ; j++){
    		Object keyCode = mapDetail.keySet().toArray()[j];

    		if(sContentsID.equals( mapDetail.get(keyCode).m_sContentsID)){
    			data = mapDetail.get(keyCode);
    			break;
    		}
    	}

    	if(data == null){
    		return 0;
    	}

        String strResult[] ={TAG_RESULT};
        Node resultList = XmlUtils.getFirstNode(root , strResult);

        if(resultList == null){
    		return 0;
        }

        //For delete
        Node durationNode  = XmlUtils.FindFirstChildNode(resultList , TAG_DURATION);
        if(durationNode != null){
        	long lDurationSec = Integer.parseInt(durationNode.getFirstChild().getNodeValue());
        	Node created  = XmlUtils.FindFirstChildNode(resultList , TAG_CREATED);
        	if(created != null){
	            Calendar calendar = Calendar.getInstance();
	            long lTime = Long.parseLong("" + calendar.get(Calendar.YEAR) + (calendar.get(Calendar.MONTH) + 1) + calendar.get(Calendar.DAY_OF_MONTH) +
	            		calendar.get(Calendar.HOUR_OF_DAY) +  calendar.get(Calendar.MINUTE) +  calendar.get(Calendar.SECOND));
	            long lOrgTime = Long.parseLong(created.getFirstChild().getNodeValue());
	            long lDiffTime = lTime - lOrgTime;
	            if(lDurationSec < lDiffTime){
	            	return DELETE_MESH_TIME;
	            }
        	}
       }

        List<Node> resultPOI = XmlUtils.FindChildNodes(resultList, TAG_POI);
	    if(resultPOI == null){
	    	return 0;
	    }

    	int nID = 0;
	    for(int i = 0 ; i < resultPOI.size() ; i++){
	    	Node resultNode = resultPOI.get(i);
			DC_POIInfoDetailData detailData = getOneMeshData(resultNode , sMeshName ,data);
			if(detailData != null){
				detailData.m_POIInfo.m_POIId = lParseID*100 + nID++;
				mOrbisPOIDetailList.add(detailData);
			}
	    }
       return nID;
    }

    /**
     * POIに表示するべきテキスト情報を取得する
     * @param data POIの詳細情報
     * @param nodeList テキストタグ情報リスト
     * @param nID 保存する際の親ID
     * @return 成功した場合、true
     */
    private boolean getTextInfo(DC_POIInfoDetailData data ,  List<Node> nodeList , short nID ){
    	for(int i = 0 ; i < nodeList.size() ; i++){
	    	DC_POIInfoDetailData.TextInfo textInfo = data.new TextInfo();
	    	textInfo.sectionID = nID;
	    	textInfo.textLabel = null;
    		textInfo.textValue = null;
    		if(nodeList.get(i) != null){
		    	if(nodeList.get(i).getAttributes() != null){
		    		 if(nodeList.get(i).getAttributes().getNamedItem(KEY_LABEL) != null){
// CHG -- 2012/03/09 -- Z01H.Fukushima(No.407) --- ↓ ----- ref #3930
//                       textInfo.textLabel = nodeList.get(i).getAttributes().getNamedItem(KEY_LABEL).getNodeValue();
		    		     textInfo.textLabel = replaceHyphen(nodeList.get(i).getAttributes().getNamedItem(KEY_LABEL).getNodeValue());
// CHG -- 2012/03/09 -- Z01H.Fukushima(No.407) --- ↑ ----- ref #3930
		    		 }
		    	}
		    	if(nodeList.get(i).getFirstChild() != null){
// CHG -- 2012/03/09 -- Z01H.Fukushima(No.407) --- ↓ ----- ref #3930
//                  textInfo.textValue = nodeList.get(i).getFirstChild().getNodeValue();
		    	    textInfo.textValue = replaceHyphen(nodeList.get(i).getFirstChild().getNodeValue());
// CHG -- 2012/03/09 -- Z01H.Fukushima(No.407) --- ↑ ----- ref #3930
		    	}
	    		if(data.m_oTextInfo.add(textInfo) == false){
	    			return false;
	    		}
    		}
    	}
    	return true;
    }

    //Save Contents File.
    /**
     * ユーザが指定する内容をファイルを保存する
     * @param list 保存情報
     * @param mapData 保存情報
     * @return 成功した場合true
     */
    public boolean writeContentsOutputFile(List<DC_ContentsData> list , Map<Integer , DC_ContentsDetailData> mapData) {

// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
        String strFilePath = mUserPath + USER_SETTING_NAME;
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

		boolean ret = true;

		Document document = null;
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			document = docBuilder.newDocument();
		} catch (ParserConfigurationException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
			return false;
		}

		Element root = document.createElement(TAG_ROOT);
		Element childNode = null;

		for(int i = 0 ; i < list.size(); i++){
			Element contentsNode = null;
			DC_ContentsDetailData Detail = mapData.get(list.get(i).m_iContentsID);
			if(Detail == null){
				continue;
			}

			contentsNode = document.createElement(TAG_CONTENTS);

			// id
			childNode = document.createElement(TAG_ID);
			childNode.appendChild(document.createTextNode(Detail.m_sContentsID));
			contentsNode.appendChild(childNode);

			// AVAILABLE
			childNode = document.createElement(TAG_AVAILABLE);

        	int nCheck = 0;
        	if(list.get(i).m_bLayering == true){
        		nCheck = (nCheck | CHECKITEM_LAYERING);
        	}
        	if(list.get(i).m_bDisplayItem == true){
        		nCheck = (nCheck | CHECKITEM_DISPLAY);
        	}
        	if(list.get(i).m_bCheckItem == true){
        		nCheck = (nCheck | CHECKITEM_CHECKED);
        	}

			childNode.appendChild(document.createTextNode("" + nCheck));
			contentsNode.appendChild(childNode);

			if(list.get(i).m_mapSectionItemList != null){
				if(list.get(i).m_mapSectionItemList.size()>0){
					Element settingNode = document.createElement(TAG_SETTING);
					boolean bAddValueFlag = false;
					for(int j = 0 ; j < list.get(i).m_mapSectionItemList.size() ; j++){
			    		Object keyCode = list.get(i).m_mapSectionItemList.keySet().toArray()[j];
			    		List<DC_ContentsItemData> contentsData = list.get(i).m_mapSectionItemList.get(keyCode);
						// section
						Element sectionNode = document.createElement(TAG_SECTION);
						sectionNode.setAttribute(KEY_NAME, (String) keyCode);
			    		for(int k = 0 ; k < contentsData.size() ; k++){
							// item
			    			Element itemNode = document.createElement(TAG_ITEM);
			    			itemNode.setAttribute(KEY_KEY, contentsData.get(k).m_sItemKey);
			    			itemNode.setAttribute(KEY_ALL, (String) ((contentsData.get(k).m_bAllSelect)? "1" : "0"));
			    			if(contentsData.get(k).m_sItemValueList != null){
			    				bAddValueFlag = true;
			    				StringBuilder strValue = new StringBuilder();
								for(int nValue = 0 ; nValue < contentsData.get(k).m_sItemValueList.size(); nValue++){
								    strValue.append(contentsData.get(k).m_sItemValueList.toArray()[nValue]).append(",");
								}
								int length = strValue.length();
								if(length > 0){
									strValue.deleteCharAt(length-1);
								}

			    				itemNode.setAttribute(KEY_VALUE, strValue.toString());
			    			}

			    			sectionNode.appendChild(itemNode);
			    		}
						if(bAddValueFlag){
							settingNode.appendChild(sectionNode);
						}
					}
					contentsNode.appendChild(settingNode);
				}
			}
			root.appendChild(contentsNode);
		 }
		document.appendChild(root);

		File file = new File( strFilePath);

		ret = XmlUtils.writeDocument(file, document, "UTF-8");
		return ret;
     }

    /**
     * WEB情報をファイルに保存する
     * @param httpUtils HTTPようUtilクラスオブジェクト
     * @param nFileType ファイルタイプ(CONTENTS,MESH,PNGデータ)
     * @return 成功した場合、そのファイルのパス。失敗するとNULL。
     */
    private String getLocalFilePath(HttpUtils httpUtils , int nFileType){
    	if (this.isCancelled()) {
			return null;
		}

    	if(httpUtils == null){
    		return null;
    	}
	    String strFilePath = null;
    	switch(nFileType){
// MOD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
    	case FILETYPE_CONTENTS:
    		String strContents	= CONTENT_FILE_NAME;
// MOD.2013.07.22 N.Sasao お知らせ機能実装 START
			// httpUtils内部で使用するパスはセパレータがないため、セパレータをつけて指定する
			strFilePath = httpUtils.saveFile2SD(File.separator + CONTENT_OPPOSING_PATH + strContents);
    		break;
    	case FILETYPE_MESH:
    		String strMeshName	= httpUtils.getRequestValue(KEY_MESH);
    		String strID		= httpUtils.getRequestValue(KEY_ID);
    		if(strMeshName != null && strID != null){
    			strFilePath = httpUtils.saveFile2SD(File.separator + CONTENT_OPPOSING_PATH + "mesh/"+ strID + "/" + strMeshName);
    		}
    		break;
    	case FILETYPE_ICON:
    		strFilePath = httpUtils.saveFile2SD(File.separator + CONTENT_OPPOSING_PATH + "icon/" + httpUtils.getFileNameIncludeFolder());
    		break;
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
    	case FILETYPE_POIDETAIL:
    		String strPoiDetailFile		= POIDETAILFILE;
    		strFilePath = httpUtils.saveFile2SD(File.separator + CONTENT_OPPOSING_PATH + strPoiDetailFile);
    		break;
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
// MOD.2013.07.22 N.Sasao お知らせ機能実装  END
    	}

    	return strFilePath;
    }
    private String getCashLocalFilePath( HttpUtils httpUtils , int nFileType ){
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
    	String strPath = new String(mDataPath);
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
    	if(httpUtils == null){
    		return null;
    	}
	    String strFilePath = null;

    	switch(nFileType){
	    	case FILETYPE_CONTENTS:
	    		String strContents	= CONTENT_FILE_NAME;
				strFilePath = strPath + CONTENT_OPPOSING_PATH + strContents;
	    		break;
	    	case FILETYPE_MESH:
	    		String strMeshName	= httpUtils.getRequestValue(KEY_MESH);
	    		String strID		= httpUtils.getRequestValue(KEY_ID);
	    		if(strMeshName != null && strID != null){
	    			strFilePath = strPath + CONTENT_OPPOSING_PATH + "mesh/"+ strID + "/" + strMeshName;
	    		}
	    		break;
	    	case FILETYPE_ICON:
	    		strFilePath = strPath + CONTENT_OPPOSING_PATH + "icon/" + httpUtils.getFileNameIncludeFolder();
	    		break;
	    	case FILETYPE_POIDETAIL:
	    		String strPoiDetailFile		= POIDETAILFILE;
	    		strFilePath = strPath + CONTENT_OPPOSING_PATH + strPoiDetailFile;
	    		break;
	    }

    	if( strFilePath == null || strFilePath.length() <= 0 ){
    		return null;
    	}

    	File isFoundFile = new File( strFilePath );

    	if( isFoundFile.exists() ){
    		return strFilePath;
    	}
    	else{
    		return null;
    	}
    }
 // ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START

// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
    /**
     * ファイルを削除する
     * @param strPath ファイルパス
     * @param nFileType ファイルタイプ
     * @param exPath SDカード以外の保存領域
     * @return 成功した場合true
     */
    static boolean DeleteFolder(String strPath, int nFileType, String exPath){

    	boolean bExPath = false;

    	if( exPath != null && exPath.length() > 0 ){
    		bExPath = true;
    	}

 		HttpUtils httpUtils = new HttpUtils(strPath);
 		HttpUtils httpUtilsExPath = null;
 		if( bExPath ){
 			httpUtilsExPath = new HttpUtils(strPath, exPath);
 		}

    	switch(nFileType){
// MOD.2013.07.22 N.Sasao お知らせ機能実装 START
// MOD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
		// httpUtils内部で使用するパスはセパレータがないため、セパレータをつけて指定する
    	case FILETYPE_CONTENTS:
   			String strContents	= CONTENT_FILE_NAME;
   			if( bExPath ){
   				return httpUtilsExPath.deleteFile(File.separator + CONTENT_OPPOSING_PATH+ strContents);
   			}
   			else{
   				return httpUtils.deleteFile(File.separator + CONTENT_OPPOSING_PATH+ strContents);
   			}
    	case FILETYPE_MESH:
    		if( bExPath ){
    			String strID		= httpUtilsExPath.getRequestValue(KEY_ID);
    			if( strID != null){
    				return httpUtilsExPath.deleteFile(File.separator + CONTENT_OPPOSING_PATH + "mesh/"+ strID + "/");
    			}
    		}
    		else{
    			String strID		= httpUtils.getRequestValue(KEY_ID);
    			if( strID != null){
    				return httpUtils.deleteFile(File.separator + CONTENT_OPPOSING_PATH + "mesh/"+ strID + "/");
    			}
    		}
    		break;
    	case FILETYPE_ICON:
			if( bExPath ){
				return httpUtilsExPath.deleteFile(File.separator + CONTENT_OPPOSING_PATH + "icon/");
			}
			else{
				return httpUtils.deleteFile(File.separator + CONTENT_OPPOSING_PATH + "icon/");
			}
    	case FILETYPE_MESH_ALL:
			if( bExPath ){
				return httpUtilsExPath.deleteFile(File.separator + CONTENT_OPPOSING_PATH + "mesh/");
			}
			else{
				return httpUtils.deleteFile(File.separator + CONTENT_OPPOSING_PATH + "mesh/");
			}
    	case FILETYPE_USER_SETTING:
			if( bExPath ){
	    		String strFilePath = exPath + USER_SETTING_NAME;
			    File file = new File(strFilePath);
			    if(file.exists()){
			    	file.delete();
			    	return true;
			    }
			}
    		break;
    	case FILETYPE_ALL:
    		if( bExPath ){
    			return httpUtilsExPath.deleteFile(File.separator +CONTENT_OPPOSING_PATH);
    		}
    		else{
    			return httpUtils.deleteFile(File.separator +CONTENT_OPPOSING_PATH);
    		}
// MOD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
// MOD.2013.07.22 N.Sasao お知らせ機能実装  END
    	}

    	return false;
    }
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
    /**
     * メッシュファイルからPOI情報を取得する
     * @param resultNode メッシュ情報
     * @param sMeshName メッシュ名称
     * @param data コンテンツデータ
     * @return メッシュから取得したPOI情報
     */
    private DC_POIInfoDetailData getOneMeshData(Node resultNode , String sMeshName , DC_ContentsDetailData data){

        Node latNode  = XmlUtils.FindFirstChildNode(resultNode , TAG_LAT);
        Node lonNode  = XmlUtils.FindFirstChildNode(resultNode , TAG_LON);
        Node nameNode  = XmlUtils.FindFirstChildNode(resultNode , TAG_NAME);
        Node reqUrlNode  = XmlUtils.FindFirstChildNode(resultNode , TAG_REQURL);

    	DC_POIInfoDetailData POIDetailData = null;

    	//The data must be needed.
        if(latNode != null && lonNode != null && nameNode != null && reqUrlNode != null){
        	int lat = Integer.parseInt(latNode.getFirstChild().getNodeValue());
        	int lon = Integer.parseInt(lonNode.getFirstChild().getNodeValue());

	    	POIDetailData = new DC_POIInfoDetailData();
        	POIDetailData.m_POIInfo.m_lnLatitude1000 = lat;
        	POIDetailData.m_POIInfo.m_lnLongitude1000 = lon;
// CHG -- 2012/03/09 -- Z01H.Fukushima(No.407) --- ↓ ----- ref #3930
//          POIDetailData.m_POIInfo.m_sPOIName = nameNode.getFirstChild().getNodeValue();
        	POIDetailData.m_POIInfo.m_sPOIName = replaceHyphen(nameNode.getFirstChild().getNodeValue());
// CHG -- 2012/03/09 -- Z01H.Fukushima(No.407) --- ↑ ----- ref #3930

        	POIDetailData.m_POIInfo.m_sReqURL = reqUrlNode.getFirstChild().getNodeValue();
            for(int j = 1 ; j < reqUrlNode.getChildNodes().getLength() ; j++){
            	if(reqUrlNode.getChildNodes().item(j).getNodeType() != Node.ELEMENT_NODE){
            		POIDetailData.m_POIInfo.m_sReqURL += (reqUrlNode.getChildNodes().item(j).getNodeValue());
            	}
            }
        }else{
        	return null;
        }

        POIDetailData.m_POIInfo.m_lMeshNum = Integer.parseInt(sMeshName);

        String[] iconStr = {TAG_ICON};
        Node iconNode  = XmlUtils.getNodeValue(resultNode , iconStr);

        if(iconNode != null){
	        if(data.m_mapIconList.get(iconNode.getFirstChild().getNodeValue()) != null){
	        	POIDetailData.m_POIInfo.m_nX = data.m_mapIconList.get(iconNode.getFirstChild().getNodeValue()).m_nX;
	        	POIDetailData.m_POIInfo.m_nY = data.m_mapIconList.get(iconNode.getFirstChild().getNodeValue()).m_nY;

	        	if(data.m_mapIconList.get(iconNode.getFirstChild().getNodeValue()).m_sIconPath == null){
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
	        		HttpUtils httpUtils = new HttpUtils(data.m_mapIconList.get(iconNode.getFirstChild().getNodeValue()).m_sURL, mDataPath);
// MOD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1 START
	        		String strPath = getCashLocalFilePath(httpUtils , FILETYPE_ICON);
	        		if( strPath == null || strPath.length() <= 0 ){
	    				data.m_mapIconList.get(iconNode.getFirstChild().getNodeValue()).m_sIconPath = getLocalFilePathEx( httpUtils , FILETYPE_ICON );
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
	        		}
	        		else{
	        			data.m_mapIconList.get(iconNode.getFirstChild().getNodeValue()).m_sIconPath = strPath;
	        		}
// MOD.2013.07.05 N.Sasao ドライブコンテンツリストキャッシュ対応1  END
	        		if(data.m_mapIconList.get(iconNode.getFirstChild().getNodeValue()).m_sIconPath == null){
	        			return null;
	        		}
	        	}
		        POIDetailData.m_POIInfo.m_sIconPath = data.m_mapIconList.get(iconNode.getFirstChild().getNodeValue()).m_sIconPath;
	        }
        }
        return POIDetailData;
    }

    private Integer GetPOIDataDetail(){
    	boolean bRes = parsePOIDataDetail();
    	if( bRes ){
    		return 1;
    	}
    	else{
    		return 0;
    	}
    }

    private boolean parsePOIDataDetail(){

    	if(showPOIDetailData == null){
    		return false;
    	}

    	String sReqURL = showPOIDetailData.m_POIInfo.m_sReqURL;
    	if( (sReqURL == null) || (sReqURL.length() == 0) ){
    		return false;
    	}

// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
    	HttpUtils httpUtils = new HttpUtils(sReqURL, mDataPath);

    	String strPath = getLocalFilePathEx( httpUtils , FILETYPE_POIDETAIL );
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
		Element root = null;
		if(strPath != null){
			root = readXmlData(new File(strPath));
			File deleteFile = new File(strPath);

			if(root == null){
				deleteFile.delete();
				return false;
			}
			else{
		        String strStatus[] ={TAG_STATUS};
		        Node statusList = XmlUtils.getFirstNode(root , strStatus);
		    	if(statusList == null){
		    		return false;
		    	}else{
		            Node codeNode  = XmlUtils.FindFirstChildNode(statusList , TAG_CODE);
		            if(codeNode == null){
		            	return false;
		            }
		        	String codeStr  = codeNode.getFirstChild().getNodeValue();
		        	if(!codeStr.equals("0000")){
		        		return false;
		        	}
		    	}

		        String strResult[] ={TAG_RESULT};
		        Node resultList = XmlUtils.getFirstNode(root , strResult);

		        if(resultList == null){
		    		return false;
		        }

		        List<Node> resultPOI = XmlUtils.FindChildNodes(resultList, TAG_POI);
			    if(resultPOI == null || resultPOI.size() != 1){
			    	return false;
			    }

			    Node resultNode = resultPOI.get(0);

			    getOnePOIDataDetail(resultNode);

				deleteFile.delete();

				return true;
			}
		}

    	return false;
    }
    /**
     * ノードからPOI詳細情報を取得する
     * @param resultNode POI詳細情報をノード
     * @return ノードから取得したPOI詳細情報
     */
    private void getOnePOIDataDetail(Node resultNode){
        //The datas may be needed.
    	Node descNode  = XmlUtils.FindFirstChildNode(resultNode ,TAG_DESCRIPTION);
        if(descNode != null){
        	showPOIDetailData.m_sDescription = replaceHyphen(descNode.getFirstChild().getNodeValue());
        }
        Node addrNode  = XmlUtils.FindFirstChildNode(resultNode , TAG_ADDRESS);
        if(addrNode != null){
        	showPOIDetailData.m_sAddress = replaceHyphen(addrNode.getFirstChild().getNodeValue());
        }
        Node telNode  = XmlUtils.FindFirstChildNode(resultNode , TAG_TEL);
        if(telNode != null){
        	showPOIDetailData.m_sTellPhone = telNode.getFirstChild().getNodeValue();
        }
        Node urlNode  = XmlUtils.FindFirstChildNode(resultNode , TAG_URL);
        if(urlNode != null){
        	showPOIDetailData.m_sUrl = urlNode.getFirstChild().getNodeValue();
        	showPOIDetailData.m_sUrlText = KEY_URLTEXT_DEFAULT;
        	if(urlNode.getAttributes() != null){
        		if(urlNode.getAttributes().getNamedItem(KEY_TEXT) != null){
        			showPOIDetailData.m_sUrlText = urlNode.getAttributes().getNamedItem(KEY_TEXT).getNodeValue();
        		}
        	}
        }

        String[] fieldsStr = {TAG_FIELDS};
        Node fieldNode  = XmlUtils.getNodeValue(resultNode , fieldsStr);
        if(fieldNode != null){
	        List<Node> sectionNodeList  = XmlUtils.FindChildNodes(fieldNode , TAG_SECTION);

	        if(sectionNodeList != null ){
	        	showPOIDetailData.m_oSectionInfo = new ArrayList<SectionInfo>();
	        	showPOIDetailData.m_oTextInfo = new ArrayList<TextInfo>();
	        	for(short k = 0 ; k < sectionNodeList.size() ; k++){
		        	DC_POIInfoDetailData.SectionInfo secInfo = showPOIDetailData.new SectionInfo();
		        	secInfo.textValue = null;
		        	if(sectionNodeList.get(k).getAttributes() != null){
		        		if(sectionNodeList.get(k).getAttributes().getNamedItem(KEY_LABEL) != null){
		        		    secInfo.textValue = replaceHyphen(sectionNodeList.get(k).getAttributes().getNamedItem(KEY_LABEL).getNodeValue());
		        		}
		        	}
		        	secInfo.sectionID = k;
		        	showPOIDetailData.m_oSectionInfo.add(secInfo);

			        List<Node> textNodeList  = XmlUtils.FindChildNodes(sectionNodeList.get(k) , TAG_TEXT);
			        if(textNodeList != null ){
		        		getTextInfo(showPOIDetailData , textNodeList , k);
			        }
	        	}
	        }

	        List<Node> textNoSecNodeList  = XmlUtils.FindChildNodes(fieldNode , TAG_TEXT);
	        if(textNoSecNodeList != null ){
	        	showPOIDetailData.m_oTextInfo = new ArrayList<TextInfo>();
        		if(getTextInfo(showPOIDetailData , textNoSecNodeList , UNKNOWN_SECTION)){
		        	DC_POIInfoDetailData.SectionInfo secInfo = showPOIDetailData.new SectionInfo();
		        	secInfo.textValue = null;
		        	secInfo.sectionID = UNKNOWN_SECTION;
		        	showPOIDetailData.m_oSectionInfo = new ArrayList<SectionInfo>();
		        	showPOIDetailData.m_oSectionInfo.add(secInfo);
        		}
	        }
        }

        String[] nativeStr = {TAG_NATIVE};
        Node nativeNode  = XmlUtils.getNodeValue(resultNode , nativeStr);
        if(nativeNode != null){
            Node roadNode  = XmlUtils.FindFirstChildNode(nativeNode , TAG_ROADTYPE);
            if(roadNode != null){
            	showPOIDetailData.m_sOrbisType = roadNode.getFirstChild().getNodeValue();
            }
        }

        Node webtolabelNode  = XmlUtils.FindFirstChildNode(resultNode , TAG_WEBTOLABEL);
        if(webtolabelNode != null){
        	showPOIDetailData.m_sWebtoLabel = webtolabelNode.getFirstChild().getNodeValue();
        }
        Node webtourlNode  = XmlUtils.FindFirstChildNode(resultNode , TAG_WEBTOURL);
        if(webtourlNode != null){
        	showPOIDetailData.m_sWebtoUrl = webtourlNode.getFirstChild().getNodeValue();
            for(int j = 1 ; j < webtourlNode.getChildNodes().getLength() ; j++){
            	if(webtourlNode.getChildNodes().item(j).getNodeType() != Node.ELEMENT_NODE){
            		showPOIDetailData.m_sWebtoUrl += (webtourlNode.getChildNodes().item(j).getNodeValue());
            	}
            }
        	showPOIDetailData.m_sWebtoUrlText = KEY_URLTEXT_DEFAULT;
        }
    }
// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
    /**
     * データ取得時に失敗した場合、リトライを行う
     * @param httpUtils 取得URL
     * @param nType 取得するデータの種類
     * @return 取得したデータのパス
     */
    private String getLocalFilePathEx( HttpUtils httpUtils , int nType ){

		String strPath = null;

		if( httpUtils == null ){
			return null;
		}

		for( int nCount = 0; nCount < CONNECTION_RETRY_COUNT; nCount++ ){
	    	if (this.isCancelled()) {
	    		break;
			}
			strPath = getLocalFilePath(httpUtils , nType);
			httpUtils.shutdown();
			if( strPath != null && strPath.length() > 0 ){
				break;
			}
			else{
	            try {
	                Thread.sleep(CONNECTION_DELAY_TIME);
	            } catch (InterruptedException e) {
	            	break;
	            }
			}
		}

		if( strPath != null && strPath.length() > 0 ){
			return strPath;
		}
		else{
			return null;
		}
	}
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
};