package net.zmap.android.pnd.v2.common.view;

import android.view.View;

public interface ScrollBoxAdapter
{
	public int getCountInBox();
	public int getCount();
	public View getView(int wPos,View oView);
}
