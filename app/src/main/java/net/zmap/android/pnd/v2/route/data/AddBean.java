package net.zmap.android.pnd.v2.route.data;
/**
 ********************************************************************
 * Copyright (c) 2010.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project
 * File           AddBean.java
 * Description    無し
 * Created on     2010/12/02
 *
 ********************************************************************
 */
public class AddBean {
    private int index = 0;
	private int ipass = 0;
    private String text = null;
    private String strTextNo = null;
	/**
	 * Created on 2010/12/02
	 * Description: Passflagを取得する
	 * @param:無し　
	 * @return：int
	 * @author:XuYang
	 */
    public int getIpass() {
		return ipass;
	}
	/**
	 * Created on 2010/12/02
	 * Description: Passflagを設定する
	 * @param:int　
	 * @return：無し
	 * @author:XuYang
	 */
	public void setIpass(int ipass) {
		this.ipass = ipass;
	}
	/**
	 * Created on 2010/12/02
	 * Description: テキストの番号を取得する
	 * @param:無し
	 * @return：String　 
	 * @author:XuYang
	 */
	public String getStrTextNo() {
		return strTextNo;
	}
	/**
	 * Created on 2010/12/02
	 * Description: テキストの番号を設定する
	 * @param:String　
	 * @return：無し
	 * @author:XuYang
	 */
	public void setStrTextNo(String strTextNo) {
		this.strTextNo = strTextNo;
	}
	/**
	 * Created on 2010/12/02
	 * Description: 索引値を取得する
	 * @param: 無し
	 * @return： int
	 * @author:XuYang
	 */
	public int getIndex() {
		return index;
	}
	/**
	 * Created on 2010/12/02
	 * Description: 索引値を設定する
	 * @param: int
	 * @return：無し
	 * @author:XuYang
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	/**
	 * Created on 2010/12/02
	 * Description: テキストを取得する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public String getText() {
		return text;
	}
	/**
	 * Created on 2010/12/02
	 * Description: テキストを設定する
	 * @param: String
	 * @return： 無し
	 * @author:XuYang
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * Created on 2010/12/02
	 * Description: 時間を取得する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public String getTime() {
		return text;
	}
	/**
	 * Created on 2010/12/02
	 * Description: 時間を設定する
	 * @param: String
	 * @return： 無し
	 * @author:XuYang
	 */
	public void setTime(String text) {
		this.text = text;
	}

}
