package net.zmap.android.pnd.v2.common.activity;

import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.maps.MapActivity;

import java.util.Vector;

public class BaseActivityStack {

    private static Vector<BaseActivity> mActivityStack = new Vector<BaseActivity>();

//Add 2011/11/10 Z01_h_yamada Start -->
    private void log() {
        NaviLog.i(NaviLog.PRINT_LOG_TAG, "#--BaseActivityStackList-----------------");
        for (int i = 0; i < mActivityStack.size(); ++i) {
        	if ( mActivityStack.elementAt(i).isTaskRoot() ) {
        		NaviLog.i(NaviLog.PRINT_LOG_TAG, "#---activity [" + i + "]" + mActivityStack.get(i) + " <<TaskRoot>>");
        	} else {
        		NaviLog.i(NaviLog.PRINT_LOG_TAG, "#---activity [" + i + "]" + mActivityStack.get(i));
        	}
        }
        NaviLog.i(NaviLog.PRINT_LOG_TAG, "#----------------------------------------");
    }
  //Add 2011/11/10 Z01_h_yamada End <--

    /**
     * @param activity
     */
    void addActivity(BaseActivity activity) {
        mActivityStack.add(activity);
        NaviLog.i(NaviLog.PRINT_LOG_TAG, "#---BaseActivityStack::addActivity => " + activity + " --------");
        log();
    }

    /**
     * @param activity
     */
    void removeActivity(BaseActivity activity) {
//Chg 2011/12/13 Z01_h_yamada Start -->
//    	//Add 2011/11/23 Z01_h_yamada End <--
//    	if ( mActivityStack.size() > 0 ) {
////Add 2011/11/23 Z01_h_yamada End <--
//	        mActivityStack.remove(activity);
//	        NaviLog.i(NaviLog.PRINT_LOG_TAG, "#---BaseActivityStack::removeActivity => " + activity + " --------");
//	        log();
////Add 2011/11/23 Z01_h_yamada End <--
//    	}
////Add 2011/11/23 Z01_h_yamada End <--
//--------------------------------------------
        int ActivityStackNum = mActivityStack.size();
      	if ( ActivityStackNum > 0 ) {
      		for (int i = 0; i < ActivityStackNum; ++i) {
  	            if (mActivityStack.get(i) == activity) {
  	            	mActivityStack.remove(activity);
  	            	NaviLog.i(NaviLog.PRINT_LOG_TAG, "#---BaseActivityStack::removeActivity => " + activity + " --------");
  	            	log();
  	            	break;
  	            }
      		}
    	}
//Chg 2011/12/13 Z01_h_yamada End <--

    }

    /**
     *
     */
    public boolean popAllActivityExceptMap() {

        BaseActivity activity = mActivityStack.elementAt(mActivityStack.size() - 1);
        if (!(activity instanceof MapActivity)) {
            return false;
        }

        NaviLog.i(NaviLog.PRINT_LOG_TAG, "#-- popAllActivityExceptMap");
        for (int i = 0; mActivityStack.size() > 1; i++) {
            mActivityStack.elementAt(0).finish();
            mActivityStack.remove(mActivityStack.elementAt(0));
        }
        log();

        return true;
    }

//Add 2011/12/13 Z01_h_yamada Start -->
    // メインのmapActivity以外はすべて終了する
    public void removeSubAllActivity() {

        NaviLog.i(NaviLog.PRINT_LOG_TAG, "#-- removeSubAllActivity size=" + mActivityStack.size());
        int ActivityStackNum = mActivityStack.size();
        for (int i = ActivityStackNum; i > 0; i--) {
            BaseActivity activity = mActivityStack.get(i - 1);
            if (i - 1 == 0 && activity.getClass().equals(MapActivity.class)) {
                break;
        	}
        	activity.finish();
            mActivityStack.remove(activity);
        }
        log();
    }
//Add 2011/12/13 Z01_h_yamada End <--
//Add 2011/11/10 Z01_h_yamada Start -->
    // タスクのﾙｰﾄ方向から、初めに見つけたform～to間のアクティビティを終了させる
    public void removeActivity(Class<? extends BaseActivity> from, Class<? extends BaseActivity> to) {

        int mActivityStackNum = mActivityStack.size();
//Add 2011/11/23 Z01_h_yamada Start -->
       	if ( mActivityStackNum > 0 ) {
//Add 2011/11/23 Z01_h_yamada End <--
	    	NaviLog.i(NaviLog.PRINT_LOG_TAG, "#-- removeActivity_range");
	        for (int i = 0; i < mActivityStackNum; ++i) {

	        	BaseActivity activity = mActivityStack.get(i);
	            if (activity.getClass().equals(from)) {

	                for (int k = i; k < mActivityStackNum; ++k) {

	                   BaseActivity remove_activity = mActivityStack.elementAt(i);
	            	   remove_activity.finish();
	            	   removeActivity(remove_activity);

	            	   if (remove_activity.getClass().equals(to)) {
	                       break;
	                   }
	                }
	                break;
	            }
	        }
	        log();
//Add 2011/11/23 Z01_h_yamada Start -->
       	}
//Add 2011/11/23 Z01_h_yamada End <--
    }

    // 指定クラスのアクティビティの数を取得する
    public int getActivityCount(Class<? extends BaseActivity> findClass)  {

    	int count = 0;
        int mActivityStackNum = mActivityStack.size();
        for (int i = 0; i < mActivityStackNum; ++i) {
            BaseActivity activity = mActivityStack.elementAt(i);
            if (activity.getClass().equals(findClass)) {
            	count++;
            }
        }
        return count;
    }
//Add 2011/11/10 Z01_h_yamada End <--

    /**
     * Activityスタック内のActivityを最下層を残し全て終了する通知を出す
     */
    public void exitAllActivity() {
//Add 2011/11/23 Z01_h_yamada Start -->
    	try {
//Add 2011/11/23 Z01_h_yamada End <--
            BaseActivity topActivity = getTopActivity();
            if (topActivity != null) {
                NaviLog.i(NaviLog.PRINT_LOG_TAG, "#-- exitAllActivity " + topActivity.toString());
                topActivity.setResult(Constants.RESULTCODE_FINISH_ALL);
                topActivity.finish();
            }
//Add 2011/11/23 Z01_h_yamada Start -->
        } catch (IllegalStateException e) {
            return;
        }
//Add 2011/11/23 Z01_h_yamada End <--
    }

//    /**
//     * Activityスタック内のActivityを最下層を残し全て終了する通知を出す
//     */
//    public void returnRootActivity() {
//        /*
//        int mActivityStackNum = mActivityStack.size();
//
//        int index = mActivityStackNum - 1;
//        if (index > 0) {
//
//            Activity activity = mActivityStack.get(index);
//            if (activity != null) {
//                activity.setResult(Constants.RESULTCODE_RETURN_ROOT_ACTIVITY);
//                activity.finish();
//            }
//        }
//        */
//    }

    /**
     * クラス指定 Activityスタック内の下層のActivity取得
     *
     * @param activityClass
     * @return　下層のActivity
     * @throws IllegalStateException
     */
    public BaseActivity getPrevActivity(BaseActivity current, Class<? extends BaseActivity> findClass) throws IllegalStateException {

        int mActivityStackNum = mActivityStack.size();
        if (mActivityStackNum == 0) {
            throw new IllegalStateException();
        }

        boolean findCurrent = false;
        for (int i = mActivityStackNum - 1; i >= 0; i--) {
            BaseActivity activity = mActivityStack.get(i);
            if (activity == current) {
                findCurrent = true;
                continue;
            }
            if (findCurrent == true) {
                if (activity.getClass().equals(findClass)) {
                    return activity;
                }
            }
        }
        return null;
    }

    /**
     * @return
     */
    public BaseActivity getRootActivity() {

        int mActivityStackNum = mActivityStack.size();
        if (mActivityStackNum == 0) {
            throw new IllegalStateException();
        }

        return mActivityStack.get(0);
    }

    /**
     * @return
     */
    public BaseActivity getTopActivity() {

        int mActivityStackNum = mActivityStack.size();
        if (mActivityStackNum == 0) {
            throw new IllegalStateException();
        }

        return mActivityStack.get(mActivityStackNum - 1);
    }

    /**
     * @param current
     * @return
     */
    public boolean isRootActivity(BaseActivity current) {

        if (mActivityStack.size() == 0) {
            throw new IllegalStateException();
        }

        if (mActivityStack.get(0) == current) {
            return true;
        }
        return false;
    }

    public int getActivityCount() {
        int count = mActivityStack.size();
        while(count > 0 && mActivityStack.get(count-1) != null && mActivityStack.get(count-1).isFinishing()) {
            count--;
        }
        return count;
    }
}
