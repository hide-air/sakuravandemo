package net.zmap.android.pnd.v2.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

public class PageBox extends FrameLayout implements ScrollListener
{
	private int m_wIndex = 0;

	public PageBox(Context oContext)
	{
		super(oContext);

	}

	public PageBox(Context oContext, AttributeSet oSet)
	{
		super(oContext, oSet);

	}

	public void addPage(View oView)
	{
		if(oView != null)
		{
			FrameLayout.LayoutParams oParams = new FrameLayout.LayoutParams(
					FrameLayout.LayoutParams.FILL_PARENT,
					FrameLayout.LayoutParams.FILL_PARENT);
			addView(oView,oParams);
//			selectView(0);
		}
	}

	@Override
	public int getCount()
	{
		return getChildCount();
	}

	@Override
	public int getMaxShow()
	{
		return 1;
	}

	@Override
	public boolean isHasNext()
	{
		if(m_wIndex < getChildCount() - 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean isHasPrevious()
	{
		if(m_wIndex > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public void moveDown()
	{
		if(m_wIndex < getChildCount() - 1)
		{
			selectView(++m_wIndex);
		}
	}

	@Override
	public void moveUp()
	{
		if(m_wIndex > 0)
		{
			selectView(--m_wIndex);
		}
	}

	@Override
	public int getPos()
	{
		return m_wIndex;
	}

	public void setCurPage(int wIndex)
	{
		if(wIndex >= 0 && wIndex < getChildCount())
		{
			selectView(wIndex);
			m_wIndex = wIndex;
		}
	}

	private void selectView(int wIndex)
	{
		View oView = null;
		for(int i = 0;i < getChildCount();++i)
		{
			oView = getChildAt(i);
			if(i == wIndex)
			{
				oView.setVisibility(View.VISIBLE);
			}
			else
			{
				oView.setVisibility(View.GONE);
			}
		}
	}

	public void setScrollToolListener(ScrollToolListener oListener)
	{
	}
}
