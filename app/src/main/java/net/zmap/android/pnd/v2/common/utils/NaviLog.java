package net.zmap.android.pnd.v2.common.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import android.util.Log;

/*
 * LogCatログ出力クラス
 *
 * デバッグ時・開発中だけ出力するログは
 * NaviLog.v()
 * NaviLog.d()
 * NaviLog.i()
 * NaviLog.w()
 * NaviLog.e()
 * を使用します。bEnableLogをtrueにした時のみ出力します。
 * 開発者はローカルソースでtrueにしてください。
 *
 *
 * 製品版で出力する必要のあるログは
 * NaviLog.V()
 * NaviLog.D()
 * NaviLog.I()
 * NaviLog.W()
 * NaviLog.E()
 * を使用します。こちらは常にログを出力します。
 *
 */
public class NaviLog {
//Chg 2011/11/11 Z01_h_yamada Start -->
//    private static final boolean bEnableLog = true; //SVNコミット時は常にfalseにする事
//--------------------------------------------
    private static final boolean bEnableLogV = true;
    private static final boolean bEnableLogD = false;
    private static final boolean bEnableLogI = false;
    private static final boolean bEnableLogW = true;
    private static final boolean bEnableLogE = true;
//Chg 2011/11/11 Z01_h_yamada End <--
//Add 2011/11/10 Z01thedoanh Start -->
    public final static String PRINT_LOG_TAG = "ItsmoNaviDrive";
//Add 2011/11/10 Z01thedoanh End <--
    public static int v(String tag, String msg) {
//Chg 2011/11/11 Z01_h_yamada Start -->
//    	if (!bEnableLog) {
//            return 0;
//        }
//--------------------------------------------
    	if (!bEnableLogV) {
            return 0;
        }
//Chg 2011/11/11 Z01_h_yamada End <--

        return Log.v(tag, msg);
    }

    public static int d(String tag, String msg) {
//Chg 2011/11/11 Z01_h_yamada Start -->
//    	if (!bEnableLog) {
//            return 0;
//        }
//--------------------------------------------
    	if (!bEnableLogD) {
            return 0;
        }
//Chg 2011/11/11 Z01_h_yamada End <--

        return Log.d(tag, msg);
    }

    public static int i(String tag, String msg) {
//Chg 2011/11/11 Z01_h_yamada Start -->
//    	if (!bEnableLog) {
//            return 0;
//        }
//--------------------------------------------
    	if (!bEnableLogI) {
            return 0;
        }
//Chg 2011/11/11 Z01_h_yamada End <--

        return Log.i(tag, msg);
    }

    public static int w(String tag, String msg) {
    	//Chg 2011/11/11 Z01_h_yamada Start -->
//    	if (!bEnableLog) {
//            return 0;
//        }
//--------------------------------------------
    	if (!bEnableLogW) {
            return 0;
        }
//Chg 2011/11/11 Z01_h_yamada End <--

        return Log.w(tag, msg);
    }

    public static int e(String tag, String msg) {
    	//Chg 2011/11/11 Z01_h_yamada Start -->
//    	if (!bEnableLog) {
//            return 0;
//        }
//--------------------------------------------
    	if (!bEnableLogE) {
            return 0;
        }
//Chg 2011/11/11 Z01_h_yamada End <--

        return Log.e(tag, msg);
    }

    public static int V(String tag, String msg) {
        return Log.v(tag, msg);
    }

    public static int D(String tag, String msg) {
        return Log.d(tag, msg);
    }

    public static int I(String tag, String msg) {
        return Log.i(tag, msg);
    }

    public static int W(String tag, String msg) {
        return Log.w(tag, msg);
    }

    public static int E(String tag, String msg) {
        return Log.e(tag, msg);
    }

    public static int i(String tag, Throwable throwable) {
    	if (!bEnableLogI) {
            return 0;
        }
        return Log.i(tag, getStackTrace(throwable));
    }

    public static int w(String tag, Throwable throwable) {
    	if (!bEnableLogW) {
            return 0;
        }
        return Log.w(tag, getStackTrace(throwable));
    }

    public static int e(String tag, Throwable throwable) {
    	if (!bEnableLogE) {
            return 0;
        }
        return Log.e(tag, getStackTrace(throwable));
    }

    public static final String getStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        throwable.printStackTrace(new PrintWriter(sw));

        String value = sw.toString();
        if (sw!=null) {
            try {
                sw.close();
            } catch (IOException e) {
                e(PRINT_LOG_TAG, e);
            }
            sw = null;
        }
    	return value;
    }
}
