package net.zmap.android.pnd.v2.inquiry.view;

import android.content.Context;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

public class TwoBtnAndScrollView extends twoButtonView {

	private EditText o_mText = null;
	public TwoBtnAndScrollView(Context oContext,EditText oText) {
		super(oContext);
		o_mText = oText;
		reSetLayout(oContext);

	}
	public TwoBtnAndScrollView(Context oContext) {
		super(oContext);
		reSetLayout(oContext);

	}
	public void setEditText(EditText oText) {
		o_mText = oText;
	}

	private void reSetLayout(Context oContext) {
		LinearLayout oLayout = (LinearLayout) super.findViewById(oContext, R.id.LinearLayout_mid);
		LayoutInflater oInflater = LayoutInflater.from(oContext);
		LinearLayout oTool = (LinearLayout)oInflater.inflate(R.layout.scroll_tool_hor, null);

		initBtn(oTool);

		Button oBtn = (Button)oTool.findViewById(R.id.Button_change);
		oBtn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {

				InputConnection ic  = getCurrentInputConnection();
				if (ic == null) return;
		        ic.beginBatchEdit();
//		        if (mComposing.length() > 0) {
//		            commitTyped(ic);
//		        }
		        ic.commitText(o_mText.getText().toString(), o_mText.getText().toString().length());
				//入力が完成する
				ic.endBatchEdit();
//				 InputMethodManager im = ((InputMethodManager) oContext.getSystemService(oContext.INPUT_METHOD_SERVICE));

//				 if(im.isActive()){
//					 NaviLog.d(NaviLog.PRINT_LOG_TAG,"ssssss");
////					 im.g.getEnabledInputMethodList().
//				 }
			}


		});
		oLayout.addView(oTool,new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT));
//		oLayout.addView(oBtn,new LinearLayout.LayoutParams(
//				LinearLayout.LayoutParams.FILL_PARENT,
//				LinearLayout.LayoutParams.FILL_PARENT));
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"3333");
		oLayout.setGravity(Gravity.CENTER);

	}

	private static Button btn_left = null;
	private static Button btn_right = null;
	private void initBtn(LinearLayout oTool) {
		btn_left = (Button) oTool.findViewById(R.id.btn_left);
		btn_right = (Button) oTool.findViewById(R.id.btn_right);
		if (null == o_mText) {
			btn_left.setVisibility(Button.GONE);
			btn_right.setVisibility(Button.GONE);
		} else {
			btn_left.setVisibility(Button.VISIBLE);
			btn_right.setVisibility(Button.VISIBLE);
			btn_left.setEnabled(false);
			btn_right.setEnabled(false);
//		if (AppInfo.isEmpty(oText.getText().toString())) {
//			btn_left.setEnabled(false);
//			btn_right.setEnabled(false);
//		}

		btn_left.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 かな入力 左ボタン禁止 Start -->
				// 走行規制中の場合、ボタン操作を許可しない
				if(DrivingRegulation.CheckDrivingRegulation()){
					return;
				}
// ADD 2013.08.08 M.Honma 走行規制 かな入力 左ボタン禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
				int selection = o_mText.getSelectionEnd();

				if (!AppInfo.isEmpty(o_mText.getText().toString())) {

					KeyboardKanaView.setClickFlag(0);

					if (selection-1 >=0) {
						selection--;
					}
					o_mText.setSelection(selection);
					KeyboardKanaView.delLineForText(o_mText);
					initBtnEnable(selection, o_mText);

				}
			}


			});

		btn_right.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 かな入力 右ボタン禁止 Start -->
				// 走行規制中の場合、ボタン操作を許可しない
				if(DrivingRegulation.CheckDrivingRegulation()){
					return;
				}
// ADD 2013.08.08 M.Honma 走行規制 かな入力 右ボタン禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END

				int selection = o_mText.getSelectionEnd();

				if (!AppInfo.isEmpty(o_mText.getText().toString())) {
					KeyboardKanaView.setClickFlag(0);
					int textLen = o_mText.getText().toString().length();
					if (!Html.toHtml(o_mText.getText()).contains("<u>")) {
						if (selection + 1 < textLen) {

							selection += 1;

						} else {
							selection = textLen;
						}
					}
					o_mText.setSelection(selection);
					KeyboardKanaView.delLineForText(o_mText);
					initBtnEnable(selection, o_mText);


				}
			}});
		}

	}
	public static void initBtnEnable(int selection, EditText o_mText) {
		if (null != o_mText) {
			if (o_mText.getText().toString().length() != 0) {
				if (Html.toHtml(o_mText.getText()).contains("<u>")) {
					btn_left.setEnabled(true);
					btn_right.setEnabled(true);
				} else {
					if (selection == 0) {
						if (o_mText.getText().toString().length() == 0) {
							btn_right.setEnabled(false);
						} else {
							btn_right.setEnabled(true);
						}
						btn_left.setEnabled(false);
					} else {
						btn_left.setEnabled(true);
						if (selection == o_mText.getText().toString().length()) {
							btn_right.setEnabled(false);
						} else {
							btn_right.setEnabled(true);
						}
					}
				}
			} else {
				btn_right.setEnabled(false);
				btn_left.setEnabled(false);
			}
		}

	}
}
