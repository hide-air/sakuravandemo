//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_UserFile_AroundRec;
import net.zmap.android.pnd.v2.data.POI_UserFile_RouteRec;
import net.zmap.android.pnd.v2.data.ZPOI_Route_Param;

/**
 * K-H0_履歴一覧(ルート沿い周辺、周辺履歴検索)
 * */
public class HistoryInquiryAround extends HistoryInquiryBase {

	/** カレント表示した内容 */
	private POI_UserFile_RouteRec[] m_Poi_UserFile_Routeitem = null;
	private POI_UserFile_AroundRec[] m_Poi_UserFile_Arounditem = null;
	private POI_UserFile_RouteRec[] m_Poi_Show_Routeitem = null;
	private POI_UserFile_AroundRec[] m_Poi_Show_Arounditem = null;
	private JNITwoLong Coordinate = new JNITwoLong();


	@Override
	protected void getRecList(long RecIndex) {
		if (this.mbAroundFlg) {
			//沿い周辺
			NaviRun.GetNaviRunObj().JNI_NE_POIHistory_GetRouteListItem(RecIndex, ONCE_GET_COUNT,
					m_Poi_UserFile_Routeitem, Rec);
		} else {
			NaviRun.GetNaviRunObj().JNI_NE_POIHistory_GetAroundListItem(RecIndex, ONCE_GET_COUNT,
					m_Poi_UserFile_Arounditem, Rec);
		}

	}

	@Override
	protected void initHistory() {

		if (this.mbAroundFlg) {
			//沿い周辺)
			if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN) {
				NaviRun.GetNaviRunObj().JNI_NE_POIHistory_SetRoute(0);
            } else if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_CAR) {
                NaviRun.GetNaviRunObj().JNI_NE_POIHistory_SetRoute(1);
            }
			ZPOI_Route_Param stRoutData = new ZPOI_Route_Param();

            // 沿い周辺検索の方向【デフォルト：両方】
            stRoutData.setEside(3);

            // 沿い周辺検索の幅【デフォルト：500】
            stRoutData.setUlnRange(500);

            // 沿い周辺検索の件数
            stRoutData.setLnCount(40);
			NaviRun.GetNaviRunObj().JNI_NE_POIHistory_SetRouteParam(stRoutData);
			NaviRun.GetNaviRunObj().JNI_NE_POIHistory_GetRouteList();

		} else {
			NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(Coordinate);
			NaviRun.GetNaviRunObj().JNI_NE_POIHistory_GetAroundList(Coordinate.getM_lLong(),Coordinate.getM_lLat());
		}
	}

	protected void initOnceGetList() {
		if (this.mbAroundFlg) {
			//沿い周辺
			m_Poi_UserFile_Routeitem = new POI_UserFile_RouteRec[ONCE_GET_COUNT];

			for (int i = 0; i < ONCE_GET_COUNT; i++) {
				m_Poi_UserFile_Routeitem[i] = new POI_UserFile_RouteRec();
			}
		} else {
			m_Poi_UserFile_Arounditem = new POI_UserFile_AroundRec[ONCE_GET_COUNT];

			for (int i = 0; i < ONCE_GET_COUNT; i++) {
				m_Poi_UserFile_Arounditem[i] = new POI_UserFile_AroundRec();
			}
		}

	}

	protected void resetShowList(Long lRecIndex,JNILong Rec) {
		if (this.mbAroundFlg) {
			//沿い周辺
			if (lRecIndex < ONCE_GET_COUNT) {
				int len = m_Poi_UserFile_Routeitem.length;
				for (int i = 0 ; i < len ; i++) {
					m_Poi_Show_Routeitem[i] = m_Poi_UserFile_Routeitem[i];
				}
			} else {
				if (lRecIndex >= ONCE_GET_COUNT*2) {
					for (int i = m_Poi_UserFile_Routeitem.length-ONCE_GET_COUNT; i < ONCE_GET_COUNT*2 ; i++) {
						if (i <ONCE_GET_COUNT) {
							m_Poi_Show_Routeitem[i] = m_Poi_Show_Routeitem[i+ONCE_GET_COUNT];
						} else {
							m_Poi_Show_Routeitem[i] = new POI_UserFile_RouteRec();
						}
					}
				}

				int len = (int)Rec.lcount;
				int j = 0;
				for (int i = ONCE_GET_COUNT ; i < ONCE_GET_COUNT + len ; i++) {
					m_Poi_Show_Routeitem[i] = m_Poi_UserFile_Routeitem[j];
					j++;
				}
			}
		} else {
			if (lRecIndex < ONCE_GET_COUNT) {
				int len = m_Poi_UserFile_Arounditem.length;
				for (int i = 0 ; i < len ; i++) {
					m_Poi_Show_Arounditem[i] = m_Poi_UserFile_Arounditem[i];
				}
			} else {
				if (lRecIndex >= ONCE_GET_COUNT*2) {
					for (int i = m_Poi_UserFile_Arounditem.length-ONCE_GET_COUNT; i < ONCE_GET_COUNT*2 ; i++) {
						if (i <ONCE_GET_COUNT) {
							m_Poi_Show_Arounditem[i] = m_Poi_Show_Arounditem[i+ONCE_GET_COUNT];
						} else {
							m_Poi_Show_Arounditem[i] = new POI_UserFile_AroundRec();
						}
					}
				}

				int len = (int)Rec.lcount;
				int j = 0;

				for (int i = ONCE_GET_COUNT ; i < ONCE_GET_COUNT + len ; i++) {
					m_Poi_Show_Arounditem[i] = m_Poi_UserFile_Arounditem[j];
	//				System.out.println(m_Poi_Show_Listitem[i].getM_Name());
					j++;
				}
			}
		}
	}

	/**
	 * 総件数を取得する
	 * */
	protected void initCount() {
		JNILong recCount  =  new JNILong();
		if (this.mbAroundFlg) {
			//沿い周辺
			NaviRun.GetNaviRunObj().JNI_NE_POIHistory_GetRouteRecCount(recCount);

		} else {
			NaviRun.GetNaviRunObj().JNI_NE_POIHistory_GetAroundRecCount(recCount);

		}
		showToast(this,recCount);
		allRecordCount = initPageIndexAndCnt(recCount);
	}

	/**
	 * メモリを申請する
	 * */
	protected void initResultObj() {

		initOnceGetList();
		if (this.mbAroundFlg) {
			//沿い周辺
			m_Poi_Show_Routeitem = new POI_UserFile_RouteRec[ONCE_GET_COUNT*2];

			for (int i = 0; i < ONCE_GET_COUNT*2; i++) {
				m_Poi_Show_Routeitem[i] = new POI_UserFile_RouteRec();
			}
		} else {
			m_Poi_Show_Arounditem = new POI_UserFile_AroundRec[ONCE_GET_COUNT*2];

			for (int i = 0; i < ONCE_GET_COUNT*2; i++) {
				m_Poi_Show_Arounditem[i] = new POI_UserFile_AroundRec();
			}
		}
	}

	protected String getShowObjName(int wPos) {
		if (this.mbAroundFlg) {
			//沿い周辺
			return m_Poi_Show_Routeitem[wPos].getM_DispName();
		} else {
			return m_Poi_Show_Arounditem[wPos].getM_DispName();
		}
	}
	protected long getShowObjLon(int wPos) {
		if (this.mbAroundFlg) {
			//沿い周辺
			return m_Poi_Show_Routeitem[wPos].getM_lLon();
		} else {
			return m_Poi_Show_Arounditem[wPos].getM_lLon();
		}
	}
	protected long getShowObjLat(int wPos) {
		if (this.mbAroundFlg) {
			//沿い周辺
			return m_Poi_Show_Routeitem[wPos].getM_lLat();
		} else {
			return m_Poi_Show_Arounditem[wPos].getM_lLat();
		}
	}
	protected long getShowObjDate(int wPos) {
		if (this.mbAroundFlg) {
			//沿い周辺
			return m_Poi_Show_Routeitem[wPos].getM_lDate();
		} else {
			return m_Poi_Show_Arounditem[wPos].getM_lDate();
		}
	}
}
