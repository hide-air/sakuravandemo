/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           JNIJumpKey.java
 * Description    short Class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class JNIShort {
	private short m_sJumpRecCount;

	/**
	 * shortの値を取得する
	 * @return shortの値
	 */
	public short getM_sJumpRecCount() {
		return m_sJumpRecCount;
	}
}
