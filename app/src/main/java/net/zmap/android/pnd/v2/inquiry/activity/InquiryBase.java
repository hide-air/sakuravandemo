//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.activity.MenuBaseActivity;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.data.JNILong;

import java.util.ArrayList;

/**
 * 検索クラスのベースクラス
 *
 * */
public class InquiryBase extends MenuBaseActivity {

    //RDBへ切替して、Event処理の必要がないから、削除する
    private static final int POI_SUCCESS = 0x10091001;

    /** 対象「hashSelectedPoi」のIndex　経度 */
    public static int INDEX_0 = 0;//LONG
    /** 対象「hashSelectedPoi」のIndex　緯度 */
    public static int INDEX_1 = 1;//LAT
    /** 対象「hashSelectedPoi」のIndex　名称 */
    public static int INDEX_2 = 2;//name
    /** 対象「hashSelectedPoi」の長さ */
    public static int STRINGLEN = 3;
    /** 戻る処理ため、全部クリックした情報を保存する */
    public static ArrayList<String[]> hashSelectedPoi = new ArrayList<String[]>();
    /** データがないダイアログのID */
    public static int DIALOG_NODATA_ALARM = 0x01;
    public static int TYPE_DEFAULT = 0;
    public static int TYPE_MYTEL = 1;//自番号
    public static int TYPE_TELNUMS = 2;//正常のアドレス帳
//	public static int TYPE_ADDRESS = 3;
//	public static int TYPE_FREEWORDSEARCH = 4;
    //RDBへ切替して、Event処理の必要がないから、削除する

    public void SrchProc(int msg1, int msg2) {
        if (msg2 == POI_SUCCESS) {
            search();
        }
    }

//	public void SrchJUMPProc(int msg1, int msg2) {
//		if (msg2 == POI_SUCCESS) {
//			searchJUMP();
//		}
//	}
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
//			Constants.GenreFlag = false;
    }

    /**
     * 市区町村画面にEventを戻してから、コールした関数
     * */
    protected void searchJUMP() {
    };

    /**
     * 検索処理を行う
     *
     * */
    protected void search() {
    };

//	public void openMap(){
//	    openMap(null, -1, -1, null);
//	}
//    /**
//     * OpenMap
//     * @param intent has no extra, put NULL
//     * @param lon
//     * @param lat
//     * @param address
//     */
//    protected void openMap(Intent intent, long lon, long lat, String address) {
//
//        if (intent == null) {
//            intent = new Intent();
//        }
//        Intent oIntent = getIntent();
//
//        String strFrom = Constants.FROM_SEARCH;
//        if(oIntent != null){
//            String s1 = oIntent.getStringExtra(Constants.ROUTE_FLAG_KEY);
//            if(s1 != null){
//                strFrom = s1;
//            }
//        }
//        POIData poi = new POIData();
//        poi.m_wLat = lat;
//        poi.m_wLong = lon;
//        poi.m_sAddress = address;
//
//        intent.putExtra(AppInfo.POI_DATA, poi);
//        intent.putExtra(AppInfo.FLAG_POI_TYPE, AppInfo.POI_LOCAL);
//
//        intent.putExtra(Constants.ROUTE_FLAG_KEY, strFrom);
//
//        int activity_id = AppInfo.ID_ACTIVITY_OPEN_MAP;
//        if(Constants.FROM_ROUTE_SEARCH.equals(strFrom)){
//            activity_id = AppInfo.ID_ACTIVITY_ROUTE_MAP;
//        }
//        if(Constants.FROM_NAVI_SEARCH.equals(strFrom)){
//            activity_id = AppInfo.ID_ACTIVITY_NAVI_MAP;
//        }
//        intent.setClass(this, MapActivity.class);
//        startActivityForResult(intent, activity_id);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent oIntent) {
        if (resultCode == Activity.RESULT_OK) {
            // M-F1_お気に入り登録名称編集
//			if (requestCode == AppInfo.ID_ACTIVITY_FAVORITESMODINQUIRY) {
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"1111111111");
//				setResult(resultCode,oIntent);
//				finish();
//				return;
//			}
        }
        super.onActivityResult(requestCode, resultCode, oIntent);
    }

    /**
     * 500件を表示するため、ページIndexと表示できる総件数を初期化する
     *
     * @param RecCount
     *            JNIから取得した総件数
     * @return int 表示できる総件数
     * @author yangyang
     *
     * */
    public static int initPageIndexAndCnt(JNILong RecCount) {
        int allRecordCount = 0;
        if (RecCount.lcount >= Constants.MAX_RECORD_COUNT) {
            allRecordCount = (int)Constants.MAX_RECORD_COUNT;
        } else {
            allRecordCount = (int)RecCount.lcount;
        }
        return allRecordCount;

    }

    public int checkGroupBtnShow(String value) {

        if (check(value, this.getResources().getStringArray(R.array.check_group_a))) {

            return R.string.kana_a;
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ka))) {

            return R.string.kana_ka;

        } else if (check(value, this.getResources().getStringArray(R.array.check_group_sa))) {

            return R.string.kana_sa;

        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ta))) {

            return R.string.kana_ta;

        } else if (check(value, this.getResources().getStringArray(R.array.check_group_na))) {

            return R.string.kana_na;

        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ha))) {

            return R.string.kana_ha;

        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ma))) {

            return R.string.kana_ma;

        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ya))) {

            return R.string.kana_ya;

        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ra))) {
            return R.string.kana_ra;

        } else if (check(value, this.getResources().getStringArray(R.array.check_group_wa))) {

            return R.string.kana_wa;

        }
        return 0;

    }

    public boolean check(String value, String[] checkGroupValue) {
        boolean bReturn = false;
        int len = checkGroupValue.length;
        if (len > 0) {
            for (int i = 0; i < len; i++) {
                if (checkGroupValue[i].equals(value.trim())) {
                    bReturn = true;
                    break;
                }
            }
        }

        return bReturn;
    }

    public Dialog createDialog(int wId, int titleId) {
        if (wId == DIALOG_NODATA_ALARM) {
            final CustomDialog oDialog = new CustomDialog(this);
            oDialog.setTitle(titleId);
            oDialog.setMessage(this.getResources().getString(R.string.dialog_nodata_msg));
            oDialog.addButton(this.getResources().getString(R.string.btn_ok), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    removeDialog(DIALOG_NODATA_ALARM);
                    oDialog.cancel();

                }
            });
            oDialog.show();
            return oDialog;
        }
        return super.onCreateDialog(wId);
    }

    public static void showToast(Activity oActivity, JNILong RecCount) {
// Chg 2011/10/08 katsuta Start -->
//        if (RecCount.lcount > Constants.MAX_RECORD_COUNT) {
    	if (RecCount.lcount >= Constants.MAX_RECORD_COUNT) {
// Chg 2011/10/08 katsuta End <--
            Toast toast = Toast.makeText(oActivity, null, Toast.LENGTH_SHORT);
            toast.setText(R.string.max_limited_msg);
            toast.show();
        }
    }

}
