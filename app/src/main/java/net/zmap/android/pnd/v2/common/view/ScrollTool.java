package net.zmap.android.pnd.v2.common.view;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import net.zmap.android.pnd.v2.R;

public class ScrollTool extends LinearLayout implements ScrollToolListener
{

	private OnClickListener m_oUpListener = null;
	private OnClickListener m_oDownListener = null;
	private ScrollListener m_oListener = null;

	private Button m_oUpButton = null;
	private Button m_oDownButton = null;
	private ScrollBar m_oBar = null;

//	private int m_wIndex = 0;

	public ScrollTool(Context oContext)
	{
		super(oContext);

		setGravity(Gravity.CENTER_HORIZONTAL);

		LayoutInflater oInflater = LayoutInflater.from(oContext);
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.scroll_tool, null);

		LinearLayout.LayoutParams oParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

		//set up event
		m_oUpButton = (Button)oLayout.findViewById(R.id.btn_up);
		m_oUpButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View oView)
			{
				clickUp(oView);
			}

		});


		m_oDownButton = (Button)oLayout.findViewById(R.id.btn_down);
		m_oDownButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View oView)
			{
				clickDown(oView);
			}

		});

		m_oBar = (ScrollBar)oLayout.findViewById(R.id.scrollBar);

		addView(oLayout,oParams);
	}


	private void clickUp(View oView)
	{
		if(m_oListener != null)
		{
			m_oListener.moveUp();
			update();
		}
		else if(m_oUpListener != null)
		{
			m_oUpListener.onClick(oView);
		}
	}

	private void clickDown(View oView)
	{
		if(m_oListener != null)
		{
			m_oListener.moveDown();
			update();
		}
		else if(m_oDownListener != null)
		{
			m_oDownListener.onClick(oView);
		}
	}

	private void update()
	{
		if(m_oListener != null)
		{
			m_oDownButton.setEnabled(m_oListener.isHasNext());
			m_oDownButton.setPressed(false);
			m_oUpButton.setEnabled(m_oListener.isHasPrevious());
			m_oUpButton.setPressed(false);
			m_oBar.setPos(m_oListener.getPos());
		}
	}

	public void setOnClickUpButton(OnClickListener oListener)
	{
		m_oUpListener = oListener;
	}

	public void setOnClickDownButton(OnClickListener oListener)
	{
		m_oDownListener = oListener;
	}

	public void bindView(ScrollListener oListener)
	{
		m_oListener = oListener;
		if(m_oListener != null)
		{
			m_oListener.setScrollToolListener(this);
		}
		reset();
	}

	public void reset()
	{
		if(m_oListener != null)
		{
			m_oBar.setMax(m_oListener.getCount());
		}
		update();
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w, h, oldw, oldh);
		reset();
	}

}
