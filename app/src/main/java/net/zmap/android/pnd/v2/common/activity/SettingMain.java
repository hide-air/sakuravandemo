/**
 ********************************************************************
 * Copyright (c) 2010.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project        MarketV2
 * File           SettingMain.java
 * Description    設定/情報メイン画面
 * Created on     2010/11/24
 *
 ********************************************************************
 */

package net.zmap.android.pnd.v2.common.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.DrivingRegulation;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.dricon.controller.DriverContentsCtrl;

/**
 * Created on 2010/11/24
 * <p>
 * Title: 設定/情報メイン画面
 * <p>
 * Description
 * <p>
 * author　XuYang
 * version 1.0
 */
public class SettingMain extends MenuBaseActivity  {
	private Button butNaviSet = null;
	private Button butMapSet = null;
//Add 2011/09/13 Z01thedoanh Start -->
//	private Button btnDriverContentsSet = null;
//Add 2011/09/13 Z01thedoanh End <--
	/**
	 * 初期化処理
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LayoutInflater oInflater = LayoutInflater.from(this);
//Del 2011/09/17 Z01_h_yamada Start -->
//		// 画面を追加
//		setMenuTitle(R.drawable.setting_main);
//Del 2011/09/17 Z01_h_yamada End <--
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.setting_main, null);

		// ナビ詳細設定
		butNaviSet = (Button)oLayout.findViewById(R.id.butNaviSet);
		butNaviSet.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v)
			{
				goNaviDetailSetting();
			}

		});
		// 地図設定
		butMapSet =  (Button)oLayout.findViewById(R.id.butMapSet);
		butMapSet.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v)
			{
				goMapSetting();
			}

		});
////Add 2011/09/13 Z01thedoanh Start -->
//		btnDriverContentsSet = (Button)oLayout.findViewById(R.id.butDriveContents);
//		btnDriverContentsSet.setEnabled(DriverContentsCtrl.getController().isGetContents());
//		btnDriverContentsSet.setOnClickListener(new OnClickListener(){
//			@Override
//			public void onClick(View v)
//			{
//				goDriverContentsSetting();
//			}
//		});
////Add 2011/09/13 Z01thedoanh End <--
		setViewInWorkArea(oLayout);
	}
	private void goNaviDetailSetting() {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 設定 ナビ詳細禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 設定 ナビ詳細禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
		Intent intent = new Intent();
		intent.setClass(this, NaviDetailSetting.class);
//Chg 2011/09/23 Z01yoneya Start -->
//		this.startActivityForResult(intent, AppInfo.ID_ACTIVITY_SETTING_NAVI);
//------------------------------------------------------------------------------------------------------------------------------------
		NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_SETTING_NAVI);
//Chg 2011/09/23 Z01yoneya End <--
	}
	private void goMapSetting() {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 設定 地図設定禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 設定 地図設定禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
		Intent intent = new Intent();
		intent.setClass(this, MapSetting.class);
//Chg 2011/09/23 Z01yoneya Start -->
//		this.startActivityForResult(intent, AppInfo.ID_ACTIVITY_SETTING_MAP);
//------------------------------------------------------------------------------------------------------------------------------------
		NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_SETTING_MAP);
//Chg 2011/09/23 Z01yoneya End <--

	}

////Add 2011/09/13 Z01thedoanh Start -->
//	private void goDriverContentsSetting() {
//// ADD 2013.08.08 M.Honma 走行規制 設定 ドライブコンテンツ禁止 Start -->
//    	if(DrivingRegulation.CheckDrivingRegulation()){
//    		return;
//    	}
//// ADD 2013.08.08 M.Honma 走行規制 設定 ドライブコンテンツ禁止 End <--
//		DriverContentsCtrl.getController().sendEventCode(Constants.DC_SHOW_SETTING_MENU, this);
//	}
////Add 2011/09/13 Z01thedoanh End <--
}
