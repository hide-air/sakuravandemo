package net.zmap.android.pnd.v2.common.view;

public interface OnUpdateListener {
    public void onUpdate();
}
