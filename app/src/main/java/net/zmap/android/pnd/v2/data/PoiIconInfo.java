/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           PoiIconInfo.java
 * Description    POI ���O
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class PoiIconInfo {
    private long lFlag;
    private long lLon;
    private long lLat;
    private long lLayerID;
    /**
    * Created on 2010/08/06
    * Title:       getLLon
    * Description:  定義フラグを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLFlag() {
        return lFlag;
    }
    /**
    * Created on 2010/08/06
    * Title:       getLLon
    * Description:  経度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLLon() {
        return lLon;
    }
    /**
    * Created on 2010/08/06
    * Title:       getLLat
    * Description:  緯度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLLat() {
        return lLat;
    }
    /**
    * Created on 2010/08/06
    * Title:       getLLayerID
    * Description:  Layer IDを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLLayerID() {
        return lLayerID;
    }
}
