package net.zmap.android.pnd.v2.common.utils;

import java.io.File;
import java.util.HashMap;

import net.zmap.android.pnd.v2.app.NaviAppDataPath;

import android.content.Context;
import android.content.ContentValues;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;


 // DBへのアクセス用 他のクラスで使用することも考慮してインナークラスとする
public class DbHelper extends SQLiteOpenHelper {
	
    //public static final String DATABASE_NAME = Environment.getExternalStorageDirectory() + "/ItsmoNaviDrive/" + "delivery.db";
	private static final String path = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME);
	//public static final String DATABASE_NAME = path + "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME + "/delivery.db";
	private static final String navidatapath = path + "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME;
	//public static final String DATABASE_NAME = path + "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME + "/sakuradata.db";
	public static final String DATABASE_NAME = navidatapath + "/sakuradata.db";
	public static final String INPORTCSV_NAME = navidatapath + "/sakuradata.csv";
	public static final String EXPORTCSV_NAME = navidatapath + "/sakuradata.out.csv";
//	public static final String DATABASE_NAME = navidatapath + "/plotnavidata.db";
//	public static final String INPORTCSV_NAME = navidatapath + "/plotnavidata.csv";
//	public static final String EXPORTCSV_NAME = navidatapath + "/plotnavidata.out.csv";
	public static final String ICON_PATH = navidatapath + "/icon/";

    public DbHelper(Context context) {
        // super(context, DATABASE_NAME, null, 1);
        // カメラ連携 & 手書き対応の為DB Verを2に変更
        super(context, DATABASE_NAME, null, 2);
    }

    /**
     * DBファイルの存在をチェックする。
     * @return
     */
    public static final boolean isExistDB()
    {
    	File chkExists = new File(DATABASE_NAME);
    	return chkExists.exists();
    }
    
    @Override
    public void onCreate(SQLiteDatabase db) {
  /*  	
        // 必要なテーブルがあれば、ここで作成する
        db.execSQL("CREATE TABLE destination (_id INTEGER PRIMARY KEY, CarNo INTEGER, Address TEXT, Product TEXT, LastDate TEXT, Claim INTEGER, WorldLat REAL, WorldLon REAL, JpnLat REAL, JpnLon REAL)");
        // 1号車
        insert(db, 1, "東京都渋谷区道玄坂2丁目20−12", "健康ミルク", "2013年2月18日", 0, 35.6593805, 139.6958453, 35.656142222, 139.699058333);
        insert(db, 1, "東京都渋谷区松濤1丁目10−21", "牛乳", "2013年3月17日", 1, 35.6611936, 139.6935343, 35.657955556, 139.696747222);
        insert(db, 1, "東京都渋谷区富ヶ谷1丁目38−17", "牛乳", "2013年5月1日", 1, 35.6667751, 139.6900677, 35.663537778, 139.693280556);
        insert(db, 1, "東京都渋谷区上原2丁目37−27", "健康ミルク", "2010年11月30日", 0, 35.6656727, 139.6807352, 35.662435556, 139.683947222);
        insert(db, 1, "東京都渋谷区西原3丁目38−10", "ヨーグルト", "2011年4月28日", 0, 35.6717165, 139.6798017, 35.66848, 139.683013889);
        insert(db, 1, "東京都渋谷区本町4丁目4−7", "牛乳", "2012年11月14日", 0, 35.6859193, 139.6813566, 35.682684444, 139.684569444);
        // 2号車
        insert(db, 2, "東京都渋谷区神宮前2丁目18−19", "牛乳", "2010年9月30日", 1, 35.6731927, 139.709888, 35.669955556, 139.713102778);
        insert(db, 2, "東京都渋谷区神宮前1丁目2−2", "牛乳", "2013年1月25日", 0, 35.6739036, 139.706155, 35.670666667, 139.709369444);
        insert(db, 2, "東京都渋谷区神宮前3丁目15−19", "牛乳", "2012年7月17日", 0, 35.6704907, 139.7097103, 35.667253333, 139.712925);
        insert(db, 2, "東京都渋谷区渋谷4丁目4−25", "健康飲料", "2011年5月5日", 0, 35.6574078, 139.7109552, 35.654168889, 139.714169444);
        insert(db, 2, "東京都渋谷区広尾4丁目3−1", "健康ミルク", "2012年10月3日", 0, 35.6521463, 139.7178882, 35.648906667, 139.721102778);
        insert(db, 2, "東京都渋谷区東2丁目12−15", "牛乳", "2011年8月8日", 0 , 35.6525728, 139.7104221, 35.649333333, 139.713636111);
        // 3号車
        insert(db, 3, "東京都渋谷区恵比寿4丁目21−10", "ヨーグルト", "2012年2月10日",  1, 35.6443249, 139.714511, 35.641084444, 139.717725);
        insert(db, 3, "東京都渋谷区恵比寿南3丁目1−8", "健康飲料", "2013年3月15日", 0, 35.6457468, 139.7065116, 35.642506667, 139.709725);
        insert(db, 3, "東京都渋谷区代官山町11−1", "ヨーグルト", "2010年11月20日", 0, 35.6508661, 139.7040227, 35.647626667, 139.707236111);
        insert(db, 3, "東京都渋谷区南平台町16−26", "健康ミルク", "2011年4月10日", 0, 35.6547054, 139.6951344, 35.651466667, 139.698347222);
        insert(db, 3, "東京都渋谷区道玄坂1丁目19−10", "牛乳", "2012年8月20日", 0, 35.6568386, 139.6960232, 35.6536, 139.699236111);
        insert(db, 3, "東京都渋谷区桜丘町15−3", "牛乳", "2012年12月15日", 0, 35.6564121, 139.7018894, 35.653173333, 139.705102778);
*/
    }
    
    /*
    public void insert(SQLiteDatabase db, int carNo, String address, String product, String lastDate, int claim, 
        double world_lat, double world_lon, double jpn_lat, double jpn_lon) {
        ContentValues cv = new ContentValues();
        cv.put("CarNo", carNo);
        cv.put("Address", address);
        cv.put("Product", product);
        cv.put("LastDate", lastDate);
        cv.put("Claim", claim);
        cv.put("WorldLat", world_lat);
        cv.put("WorldLon", world_lon);
        cv.put("JpnLat", jpn_lat);
        cv.put("JpnLon", jpn_lon);
        db.insert("destination", "", cv);
    }
    */

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // DBの変更があれば、ここで行う
        //onCreate(db);
        // カメラ連携 & 手書き対応 Start
        System.out.println("DbHelper#onUpgrade() " + oldVersion + " to " + newVersion);
        if( oldVersion <= 1 && newVersion >= 2) {
            upgradeV1toV2(db);
        }
        // カメラ連携 & 手書き対応 End
    }
    // カメラ連携 & 手書き対応 Start
    private void upgradeV1toV2(SQLiteDatabase db) {
        // カラムの追加
        final String SQL_ADD_COLUMN_PHOTO_NUM = 
            "ALTER TABLE DeliveryInfo ADD COLUMN PhotoNum integer DEFAULT 0;";
        final String SQL_ADD_COLUMN_PHOTO_PATH = 
            "ALTER TABLE DeliveryInfo ADD COLUMN PhotoPath text DEFAULT '';";
        final String SQL_ADD_COLUMN_PHOTO1 = 
            "ALTER TABLE DeliveryInfo ADD COLUMN PhotoFile1 text DEFAULT '';";
        final String SQL_ADD_COLUMN_PHOTO2 = 
            "ALTER TABLE DeliveryInfo ADD COLUMN PhotoFile2 text DEFAULT '';";
        final String SQL_ADD_COLUMN_PHOTO3 = 
            "ALTER TABLE DeliveryInfo ADD COLUMN PhotoFile3 text DEFAULT '';";
        final String SQL_ADD_COLUMN_PHOTO4 = 
            "ALTER TABLE DeliveryInfo ADD COLUMN PhotoFile4 text DEFAULT '';";
        final String SQL_ADD_COLUMN_PHOTO5 = 
            "ALTER TABLE DeliveryInfo ADD COLUMN PhotoFile5 text DEFAULT '';";
        final String SQL_ADD_COLUMN_MEMO_PATH = 
            "ALTER TABLE DeliveryInfo ADD COLUMN HandWrittenMemoPath text DEFAULT '';";
        final String SQL_ADD_COLUMN_MEMO = 
            "ALTER TABLE DeliveryInfo ADD COLUMN HandWrittenMemoFile text DEFAULT '';";
        db.execSQL(SQL_ADD_COLUMN_PHOTO_NUM);
        db.execSQL(SQL_ADD_COLUMN_PHOTO_PATH);
        db.execSQL(SQL_ADD_COLUMN_PHOTO1);
        db.execSQL(SQL_ADD_COLUMN_PHOTO2);
        db.execSQL(SQL_ADD_COLUMN_PHOTO3);
        db.execSQL(SQL_ADD_COLUMN_PHOTO4);
        db.execSQL(SQL_ADD_COLUMN_PHOTO5);
        db.execSQL(SQL_ADD_COLUMN_MEMO_PATH);
        db.execSQL(SQL_ADD_COLUMN_MEMO);
    }
    // カメラ連携 & 手書き対応 End
}

