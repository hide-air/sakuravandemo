/**
 *  @file		DriverContentsCtrl
 *  @brief		ドライブコンテンツ コントロールクラス
 *
 *  @attention
 *  @note
 *
 *  @author		Manabu Watanabe [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2011-09-01)
 *  @version	$Revision: $ by $Author: $
 *
 */


package net.zmap.android.pnd.v2.dricon.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
//ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 再バインド Start -->
import net.zmap.android.pnd.v2.common.DrivingRegulation;
//ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 再バインド End <--
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
////ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 再バインド Start -->
//import net.zmap.android.pnd.v2.common.services.ClarionService;
////ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 再バインド End <--
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.view.WaitDialog;
import net.zmap.android.pnd.v2.data.DC_ContentsData;
import net.zmap.android.pnd.v2.data.DC_Coordinate;
import net.zmap.android.pnd.v2.data.DC_POIInfoData;
import net.zmap.android.pnd.v2.data.DC_POIInfoDetailData;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.ZDG_ORBIS_INFO;
import net.zmap.android.pnd.v2.dricon.model.DriverContentsDataManager;
import net.zmap.android.pnd.v2.dricon.view.DriConDetailInfoActivity;
import net.zmap.android.pnd.v2.dricon.view.DriConMainMenuActivity;
import net.zmap.android.pnd.v2.dricon.view.DriConMenuListDialog;
import net.zmap.android.pnd.v2.maps.MapView;
//// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
//import net.zmap.android.pnd.v2.notice.controller.NotificationCtrl;
//// ADD.2013.07.22 N.Sasao お知らせ機能実装  END
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
// ADD 2013.04.18 N.Sasao Bug #12760 デモ走行中にPOI詳細情報を表示した後、アプリ異常終了 START
import android.view.WindowManager.BadTokenException;
// ADD 2013.04.18 N.Sasao Bug #12760 デモ走行中にPOI詳細情報を表示した後、アプリ異常終了  END
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
import android.view.Gravity;
import android.widget.Toast;
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない START
import net.zmap.android.pnd.v2.data.JNILong;
// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  END


/**
 * @author watanabe
 *
 */
/**
 * @author watanabe
 *
 */
/**
 * @author watanabe
 *
 */
public class DriverContentsCtrl  implements DialogInterface.OnDismissListener{
	private static Handler 					mHandler;
	private static DriverContentsCtrl		mController			= null;
	private DriverContentsDataManager		mDataManager		= null;
	private MeshManager						mMeshManager		= null;
	private Activity						mActivity 			= null;
	private DriConMenuListDialog 			mCustomizeDialog	= null;
	private WaitDialog 						mWaitDialog			= null;
	private static List<DC_ContentsData>	m_oContentsDataList = null;
	public 	DC_Coordinate 					mCarLocation 		= null;
	private char							mHighwayChar		= 'K';
	private static Map<Integer, Bitmap>		mIconImageData		= null;
    private Timer							mtimer				= null;
// ADD/MOD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1) START
    private boolean							mForceDrawCancel    = false;
    private Timer							mtimer2				= null;
    private final static int				TIMER_REPEAT_MINUTE	= 60*1000 * 1; 	//1分
    private final static int				TIMER_REPEAT_REDRAW	= 1000 * 3; 	//3秒
// ADD/MOD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1)  END
// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない START
    private long              lOldScale          = 0;
    private long              lNewScale          = 0;
// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  END
// ADD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2 START
    private boolean							mIsMapChangeRestriction	= false;
// ADD.2013.07.26 N.Sasao ドライブコンテンツキャンセル機能実装 START
    private boolean 						mIsEventCancel = false;
// ADD.2013.07.26 N.Sasao ドライブコンテンツキャンセル機能実装  END
// ADD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2  END
	/**
	 * Constructor
	 * @param context	MapActivityから渡す
	 */
	private DriverContentsCtrl(Context context){
		mActivity = (Activity) context;
		mController = this;
		initEventHandler();
		mDataManager = new DriverContentsDataManager(mHandler , mActivity);
		mMeshManager = new MeshManager();
		mCarLocation = new DC_Coordinate();
		mIconImageData = new HashMap<Integer, Bitmap>();
// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない START
		JNILong lnDistance = new JNILong();
        NaviRun.GetNaviRunObj().JNI_Java_GetScaleDistance(lnDistance);

        lOldScale = lnDistance.getLcount();
        lNewScale = lOldScale;
// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  END
	}

	/**
	 * init
	 * @param context MapActivityから渡す
	 */
	public static void init(Context context) {
		if(mController == null){
			mController = new DriverContentsCtrl(context);
		}
	}

	/**
	 * release all data.
	 *
	 */
	public void release() {
// ADD.2013.03.15 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
		if( mtimer != null ){
			mtimer.cancel();
			mtimer = null;
		}
		if( mtimer2 != null ){
			mtimer2.cancel();
			mtimer2 = null;
		}
// ADD.2013.03.15 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
		mDataManager.release();
	}
// ADD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2 START
	public void mapChangeRestrictionStart(){
		stopService();
		mIsMapChangeRestriction = true;
	}
	public void mapChangeRestrictionEnd(){
		if( mIsMapChangeRestriction ){
			mIsMapChangeRestriction = false;
			sendEventCode(Constants.DC_FORCE_REDRAW, null);
		}
	}
	public boolean isDriveContentsRestriction(){
		return mIsMapChangeRestriction;
	}
// ADD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2  END
	/**
	 * all asynctask stopped.
	 */
	public void stopService() {
		mDataManager.stopService();
	}

	/**
	 * Get the list of Contents.
	 * @return contents data
	 */
	public List<DC_ContentsData> getContentsDataList(){
		if(m_oContentsDataList != null){
			return m_oContentsDataList;
		}
		return null;
	}

	/**
	 * Which has the contents data.
	 * @return true if the control has contents data.
	 */
	public boolean isGetContents() {
		if(mDataManager != null){
			return mDataManager.isGetContents();
		}
		return false;
	}

	/**
	 * Set the contents data.
	 * @param data
	 *
	 */
	public void setContentsDataList(List<DC_ContentsData> data){
		if(data != m_oContentsDataList){
			m_oContentsDataList = null;
			m_oContentsDataList = new ArrayList<DC_ContentsData>(data);
		}
		return;
	}

	/**
	 * Notify the dialog data is changesd.
	 *
	 */
	public void notifyDialogDataChanged(){
		if(mCustomizeDialog != null){
			mCustomizeDialog.updateData(m_oContentsDataList);
			mCustomizeDialog.notifyDataSetChanged();
		}
	}

	/**
	 * Save the setting file which the control has.
	 */
	public void saveSettingFile(){
		if(m_oContentsDataList != null){
		}
		if(mDataManager.setSettingMenu(m_oContentsDataList)){
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
			mDataManager.stopService();
       		mDataManager.requestRecreate();
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
			callMapChange(true);
		}
		alertOrbis();
	}

	/**
	 * ハンドル作成
	 */
	private void initEventHandler(){
		mHandler = new Handler() {
            public void handleMessage(final Message msg) {
// ADD.2013.07.26 N.Sasao ドライブコンテンツキャンセル機能実装 START
            	if( mIsEventCancel ){
            		if(mWaitDialog != null){
                		mWaitDialog.cancel();
                		mWaitDialog.dismiss();
                		mWaitDialog = null;
                	}
            		mIsEventCancel = false;
//            		MapView.getInstance().getMapTopView().setDriverContentsMenuClickable(true);
            		return;
            	}
// ADD.2013.07.26 N.Sasao ドライブコンテンツキャンセル機能実装  END
                if (msg.what == Constants.DC_SHOW_LIST_MENU) {
					if(mWaitDialog != null){
                		mWaitDialog.cancel();
                		mWaitDialog = null;
                	}
					showDialog();
				} else if (msg.what == Constants.DC_SHOW_SETTING_MENU) {
					if(mWaitDialog != null){
                		mWaitDialog.cancel();
                		mWaitDialog = null;
                	}
					showDriverContentsMenu(m_oContentsDataList);
				} else if (msg.what == Constants.DC_AROUND_SEARCH) {
					drawDriverContents((List<DC_POIInfoData>) msg.obj);
				} else if (msg.what == Constants.DC_UPDATE_DATA) {
					drawDriverContents((List<DC_POIInfoData>) msg.obj);
				} else if (msg.what == Constants.DC_DETAIL_POI) {
					Long poiID = (Long) msg.obj;
// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
					RequestDetailPOI(poiID.longValue());
// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
				} else if (msg.what == Constants.DC_ORBIS_DATA_CHANGED) {
					alertOrbis();
					callMapChange(true);
				} else if (msg.what == Constants.DC_CANCEL) {
					if(mWaitDialog != null){
        				mWaitDialog.cancel();
        				mWaitDialog = null;
        			}
				} else if (msg.what == Constants.DC_GET_CONTENTS) {
					if(mWaitDialog != null){
                		mWaitDialog.cancel();
                		mWaitDialog = null;
                	}
					if(msg.obj != null){
                   		initContentsData((List<DC_ContentsData>)msg.obj);
        				callMapChange(true);
//        				MapView.getInstance().getMapTopView().setDriveContentsEnable(true);
//// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
//        				NotificationCtrl noticeCtrl = NotificationCtrl.getController();
//        				if( noticeCtrl != null ){
//        					// 次回起動時はドライブコンテンツを再取得しない
//        					noticeCtrl.setInfoChange( false );
//        				}
//// ADD.2013.07.22 N.Sasao お知らせ機能実装  END
        			}else{
                        mtimer = new Timer();
                        mtimer.schedule(new TimerTask() {
                            public void run() {
                            	sendEventCode(Constants.DC_GET_CONTENTS, null);
                            }
                        }, TIMER_REPEAT_MINUTE);
        			}
				} else if (msg.what == Constants.DC_POI_DETAIL_DATA) {
					if(mWaitDialog != null){
        				mWaitDialog.cancel();
        				mWaitDialog = null;
        			}
					if(msg.obj != null){
        				DC_POIInfoDetailData dc_POIInfoDetailData = new DC_POIInfoDetailData();
        				dc_POIInfoDetailData = (DC_POIInfoDetailData)msg.obj;

        				Intent intent = new Intent();
	        			intent.putExtra(Constants.DC_INTENT_DETAIL_INFO, dc_POIInfoDetailData);
	        			intent.setClass(mActivity, DriConDetailInfoActivity.class);
	        			NaviActivityStarter.startActivityForResult(mActivity, intent, AppInfo.ID_ACTIVITY_DRICON_DETAIL_INFO);
// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
        				callMapChange(true);
// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
	        		}
        			else{
        				String sTostText = mActivity.getResources().getString(R.string.dc_detail_info_get_error);
        				// Toastのインスタンスを生成
        				Toast toast = Toast.makeText(mActivity, sTostText, Toast.LENGTH_LONG);
        				// 表示位置を設定
        				toast.setGravity(Gravity.CENTER, 0, 0);
        				// メッセージを表示
        				toast.show();
        			}
				} else {
				}
            }
    	};
	}

	/**
	 * Initialize the contents data and bitmap data of menu.
	 * @param dataList the data for initializing.
	 */
	private void initContentsData(List<DC_ContentsData> dataList){
		if(dataList != null){
			int nIconSize = 0;
			m_oContentsDataList = dataList;
			for(int i = 0; i < m_oContentsDataList.size(); i++){
				Integer key = m_oContentsDataList.get(i).m_iContentsID;
/* MOD .2013.04.05 N.Sasao コベリティ指摘対応 START	*/
				if((key != null) && (mDataManager.getDetailMenuData(key) != null)){
					if(mDataManager.getDetailMenuData(key).m_mapIconList != null){
						if( mDataManager.getDetailMenuData(key) != null ){
							nIconSize += mDataManager.getDetailMenuData(key).m_mapIconList.size();
						}
/* MOD .2013.04.05 N.Sasao コベリティ指摘対応  END  */
					}
				}
				if(key != null && m_oContentsDataList.get(i).m_sIconPath != null){
					//アイコン画像情報をメモリに入れる
					Bitmap value = BitmapFactory.decodeFile(m_oContentsDataList.get(i).m_sIconPath);
					if(value != null){
						mIconImageData.put(key, value);
					}
	    		}
			}
			NaviRun.GetNaviRunObj().JNI_MP_DC_SetPOIIconSize(nIconSize);
		}
	}

	/**
	 * コントローラのインスタンス
	 * @return 自身のインスタンス
	 */
	public static DriverContentsCtrl getController(){
		return mController;
	}

	/**
	 * ViewとInputデータから受け取るEventCode
	 *
	 * @param eventcode		イベントコード
	 * @param obj			パラメタが必要ならば使う、必要がなかったらnullにする。
	 */
	public void sendEventCode(int eventcode, Object obj) {
		switch(eventcode){
		case Constants.DC_SHOW_LIST_MENU:
			if(isGetContents()){
				if(obj != null){
					mActivity = (Activity) obj;
				}
				showWaitDialog();
				mDataManager.getMenuData(Constants.DC_SHOW_LIST_MENU);
			}
			break;
		case Constants.DC_DETAIL_POI:
/* MOD .2013.04.05 N.Sasao コベリティ指摘対応 START	*/
			if( obj != null ){
				Long iconID = (Long) obj;
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
				RequestDetailPOI(iconID.longValue());
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
			}
/* MOD .2013.04.05 N.Sasao コベリティ指摘対応  END	*/
			break;
		case Constants.DC_SHOW_SETTING_MENU:
			if(isGetContents()){
				if(obj != null){
					mActivity = (Activity) obj;
				}
				showWaitDialog();
				mDataManager.getMenuData(Constants.DC_SHOW_SETTING_MENU);
			}
			break;
		case Constants.DC_CHANGES_SETTING:
			if(mDataManager.setSettingMenu((List<DC_ContentsData>) obj)){
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
				mDataManager.stopService();
				mDataManager.requestRecreate();
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
				callMapChange(true);
			}
			break;
		case Constants.DC_MAP_CHANGED:
			callMapChange(false);
			break;
		case Constants.DC_GET_CONTENTS:
//// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
//			NotificationCtrl noticeCtrl = NotificationCtrl.getController();
//			if( noticeCtrl != null && noticeCtrl.isInfoChange() ){
//				// お知らせのInformationが変更されたため、ドライブコンテンツを再ダウンロードする
//				mDataManager.cacheDriconDataClear();
//			}
//// ADD.2013.07.22 N.Sasao お知らせ機能実装  END
			mDataManager.getMenuData(Constants.DC_GET_CONTENTS);
			break;
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1) START
		case Constants.DC_FORCE_REDRAW:
			callMapChange(true);
			break;
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1)  END
// MOD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない START
		case Constants.DC_CHANGES_SCALE:
			callScaleChange();
			break;
// MOD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  END
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
		case Constants.DC_DATA_CLEAR:
			driconSystemClear();
			break;
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
		default:
			break;
		}
	}
// ADD.2013.06.21 N.Sasao お気に入り経由のPOI情報詳細表示時、プログレスが表示されない START
	/**
	 * ViewとInputデータから受け取るEventCode(Activity同時更新用)
	 *
	 * @param eventcode		イベントコード
	 * @param activity		更新したいアクティビティ(必要がなかったらnull)
	 * @param obj			パラメタが必要ならば使う、必要がなかったらnullにする。
	 */
	public void sendEventCode(int eventcode, Object activity, Object data ) {
		switch(eventcode){
			case Constants.DC_DETAIL_POI:
				if(activity != null){
					mActivity = (Activity) activity;
				}
				sendEventCode(eventcode, data);
				break;
		}
	}
// ADD.2013.06.21 N.Sasao お気に入り経由のPOI情報詳細表示時、プログレスが表示されない  END
	/**
	 * Get the icon image.
	 * @param key Key of the icon.
	 * @return Bitmap data .if key is out of bounds, return null.
	 */
	public Bitmap getIconImgData(int key){
		Bitmap bitmap = null;
		bitmap = mIconImageData.get(key);
		return bitmap;
	}

	/**
	 * Required the redraw map.
	 * If the displaying mesh is changed ot param is true , call redraw.
	 * @param bForceRedraw call redraw always.
	 */
	private void callMapChange(boolean bForceRedraw){
		JNITwoLong LeftBottom = new JNITwoLong();
		JNITwoLong RightTop = new JNITwoLong();

		NaviRun.GetNaviRunObj().JNI_MP_DC_GetDisplayArea(LeftBottom, RightTop);
		if(LeftBottom.getM_lLat() == 0 || LeftBottom.getM_lLong() == 0 ){
			return;
		}

// ADD.2013.03.19 N.Sasao 初回のドライブコンテンツ一覧ダウンロードがキャンセルされる START
		if( !mDataManager.isGetContents() ){
			return;
		}
// ADD.2013.03.19 N.Sasao 初回のドライブコンテンツ一覧ダウンロードがキャンセルされる  END
// ADD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2 START
		/* マップ変更(ドライブコンテンツ描画)制限中は何もしない */
		if( mIsMapChangeRestriction ){
			return;
		}
// ADD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2  END
		Rect curRect = mMeshManager.getDisplayArea();

		Rect dispArea = new Rect();
		dispArea.left = (int) LeftBottom.getM_lLong();
		dispArea.bottom = (int) LeftBottom.getM_lLat();
		dispArea.right = (int) RightTop.getM_lLong();
		dispArea.top = (int) RightTop.getM_lLat();

        if(bForceRedraw){
			mMeshManager.meshUpdate(dispArea);
			List<Integer> meshIntThreeList = mMeshManager.getMeshIntThreeList();
// MOD.2013.03.01 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(3) START
        	if(!mDataManager.preAroundSearch(meshIntThreeList)){
// MOD.2013.03.01 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(3)  END
        		mMeshManager.meshUpdate(curRect);
			}
        }else{
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
        	int nRedrawFlag = mMeshManager.compMeshThreeList(dispArea);

        	/* メッシュ再生成が必要なとき */
			if( (nRedrawFlag == 1) || (mForceDrawCancel == true) ){
				/* メッシュ再生成が必要な場合は、メッシュ生成処理を中断させ、最初から作り直す */
				/* (生成済みメッシュデータは再利用する) */
// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
//				mDataManager.stopService();
// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
				mDataManager.requestRecreate();

				if( null != mtimer2 ){
					mtimer2.cancel();
					mtimer2 = null;
				}
				/* 縮尺による描画キャンセル要求のみ、メッシュデータ生成処理を遅らせる */
				if( mForceDrawCancel == true ){
	        		mtimer2 = new Timer();
	                mtimer2.schedule(new TimerTask() {
	                    public void run() {
	                    	sendEventCode(Constants.DC_FORCE_REDRAW, null);
	                    	mtimer2 = null;
	                    }
	                }, TIMER_REPEAT_REDRAW);
	                mForceDrawCancel = false;
				}
				else{
					sendEventCode(Constants.DC_FORCE_REDRAW, null);
				}
			}
// MOD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
        }
	}

	/**
	 *WaitDialog表示
	 */
	private void showWaitDialog() {
		if(mWaitDialog == null){
			if(mActivity != null){
				mActivity.runOnUiThread(new Runnable(){
				public void run() {
						mWaitDialog = new WaitDialog(mActivity);
						mWaitDialog.setShowProGressBar(true);
						mWaitDialog.setOnKeyListener(new OnKeyListener() {
						@Override
						public boolean onKey(DialogInterface dialog, int keyCode,
								KeyEvent event) {
							if (keyCode == KeyEvent.KEYCODE_SEARCH) {
								return true;
							} else if (keyCode == KeyEvent.KEYCODE_BACK) {
// MOD.2013.07.26 N.Sasao ドライブコンテンツキャンセル機能実装 START
								mIsEventCancel = true;
								stopService();
								if( mWaitDialog != null ){
									mWaitDialog.cancel();
									mWaitDialog.dismiss();
									mWaitDialog = null;
								}
								return true;
							}
							return false;
						}
					});
					mWaitDialog.setCancelable(true);
// MOD.2013.07.26 N.Sasao ドライブコンテンツキャンセル機能実装  END
// MOD 2013.04.18 N.Sasao Bug #12760 デモ走行中にPOI詳細情報を表示した後、アプリ異常終了 START
					try{
						mWaitDialog.show();
					}catch(BadTokenException e){
						NaviLog.e(NaviLog.PRINT_LOG_TAG, "showWaitDialog errmsg = " + e.toString());
						mWaitDialog = null;
					}
// MOD 2013.04.18 N.Sasao Bug #12760 デモ走行中にPOI詳細情報を表示した後、アプリ異常終了  END
				}
			});
			}
		}
	}

	/**
	 *一覧リスト表示
	 * @return
	 */
	private void showDialog(){
		if(m_oContentsDataList != null && m_oContentsDataList.size() > 0){
			mCustomizeDialog = new DriConMenuListDialog(mActivity, m_oContentsDataList);
	    	mCustomizeDialog.setOnDismissListener(this);
	    	mCustomizeDialog.show();
//// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 再バインド Start -->
//	        if(DrivingRegulation.GetDrivingRegulationGuide()) {
//	        	ClarionService.GetInstance().Bind();
//	        }
//// ADD 2013.08.08 M.Honma 走行規制 Clarionサービス追加 再バインド End <--
		}
	}

	/**
	 * 設定メニューへ
	 * @param list
	 */
	private void showDriverContentsMenu(List<DC_ContentsData> list){
		if(m_oContentsDataList != null && m_oContentsDataList.size() > 0){
			if(mActivity != null){
				Intent intent = new Intent();
				intent.setClass(mActivity, DriConMainMenuActivity.class);
				NaviActivityStarter.startActivityForResult(mActivity, intent, AppInfo.ID_ACTIVITY_DRICON_SETTING_MAIN);
			}
		}
	}

	/**
	 * ドライブコンテンツ描画
	 * @param list POI information list.
	 */
	private void drawDriverContents(List<DC_POIInfoData> list){
		if(list != null){
/* MOD .2013.04.05 N.Sasao 描画速度アップ対応 START	*/
	 		DC_POIInfoData[] dataList = (DC_POIInfoData[]) list.toArray(new DC_POIInfoData[list.size()]);
/* MOD .2013.04.05 N.Sasao 描画速度アップ対応  END	*/
	 		NaviRun.GetNaviRunObj().JNI_MP_DC_SetPOIInfoData(dataList);
    		NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
 		}
	}

// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
	/**
	 * @param iconID POI detail information
	 * @return true if the POI id is valid.
	 */
	private boolean RequestDetailPOI(long iconID){
		boolean bResult = false;
		if(mDataManager != null){
// MOD.2013.06.12 N.Sasao POI情報リスト表示対応 START
			bResult = mDataManager.requestDetailPOI(iconID);
			if(bResult){
			showWaitDialog();
			}
			else{
				String sTostText = mActivity.getResources().getString(R.string.dc_detail_info_get_error);
				// Toastのインスタンスを生成
				Toast toast = Toast.makeText(mActivity, sTostText, Toast.LENGTH_LONG);
				// 表示位置を設定
				toast.setGravity(Gravity.CENTER, 0, 0);
				// メッセージを表示
				toast.show();
			}
// MOD.2013.06.12 N.Sasao POI情報リスト表示対応  END
		}
		return bResult;
	}
// MOD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END

	/**
	 * update car position for alert orbis.
	 * @param lat car position latitude.
	 * @param lon car position longitude.
	 */
	public void updateCarPosition(long lat,long lon){
		if(mDataManager != null){
			if(!mDataManager.isGetContents()){
				return;
			}
			if(mCarLocation.m_lnLatitude1000 != lat && mCarLocation.m_lnLongitude1000 != lon){
				DC_Coordinate coordinate = new DC_Coordinate();
				coordinate.m_lnLatitude1000 = lat;
				coordinate.m_lnLongitude1000 = lon;

				mMeshManager.setCarPosInfo(coordinate);
				List<String> meshList = mMeshManager.getMeshOrbisList();
				if(meshList != null){
					if(mDataManager.setOrbisData( meshList)) {
						mCarLocation.m_lnLatitude1000 = lat;
						mCarLocation.m_lnLongitude1000 = lon;
					}
				}
			}
		}
	}

	/**
	 * send the orbis data to NaviEngine.
	 */
	public void alertOrbis(){
		if(mDataManager == null){
			return;
		}

		List<DC_POIInfoDetailData> list = mDataManager.getOrbisList();
		//一旦フラグを0にしてからでないとオービスデータクリアが動作しない
		NaviRun.GetNaviRunObj().JNI_NE_SetOrbisGuide(0);
		NaviRun.GetNaviRunObj().JNI_DG_ClearOrbisData();

		if(list != null){
			ZDG_ORBIS_INFO[]	orbisInfo = new ZDG_ORBIS_INFO[list.size()];
			for(int i = 0 ; i < list.size();i++){
				orbisInfo[i] = new ZDG_ORBIS_INFO();
				DC_POIInfoDetailData data = list.get(i);
				if(data.m_sOrbisType.length() == 1 && data.m_sOrbisType.toCharArray()[0] == mHighwayChar) {
					orbisInfo[i].setOnHighway(true);
				}
				else {
					orbisInfo[i].setOnHighway(false);
				}

				orbisInfo[i].setLnPosLatMilliSec(data.m_POIInfo.m_lnLatitude1000);
				orbisInfo[i].setLnPosLonMilliSec(data.m_POIInfo.m_lnLongitude1000);
				orbisInfo[i].setUlnID(String.valueOf(data.m_POIInfo.m_POIId));
			}

			//フラグを1にしてからでないとオービスデータを更新できない
			NaviRun.GetNaviRunObj().JNI_NE_SetOrbisGuide(1);
			NaviRun.GetNaviRunObj().JNI_DG_UpdateOrbisData(list.size() , orbisInfo);
		}
	}

	/**
	 *  hide the customize dialog.
	 */
	public void hideDialog(){
		if(mCustomizeDialog != null){
			mCustomizeDialog.cancel();
		}
	}

// Add 2011/10/19 katsuta Start -->
	/**
	 * @return true if the customize dialog is shown.
	 */
	public boolean isShowDialog(){
		return mCustomizeDialog != null;
	}
// Add 2011/10/19 katsuta End <--

	/* (非 Javadoc)
	 * get the dialog disabled event.
	 * @see android.content.DialogInterface.OnDismissListener#onDismiss(android.content.DialogInterface)
	 *
	 */
	@Override
	public void onDismiss(DialogInterface dialog) {
//	    MapView.getInstance().getMapTopView().setDriverContentsMenuClickable(true);

		List<DC_ContentsData> list = mCustomizeDialog.getChangedList();
		if(list != null){
			m_oContentsDataList = null;
			m_oContentsDataList = new ArrayList<DC_ContentsData>(list);
			if(mDataManager.setSettingMenu(m_oContentsDataList)){
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4) START
				mDataManager.stopService();
	       		mDataManager.requestRecreate();
// ADD.2013.03.18 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(4)  END
				callMapChange(true);
			}
		}
		mCustomizeDialog = null;
	}
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1) START
	/**
	 * MapViewクラスからの描画キャンセル要求.
	 * @param なし.
	 */
	public void DrawCancelFromView(){
		if( DriverContentsDataManager.STATE_TYPE_MESH == mDataManager.GetDataManagerState() ){
			stopService();
			mForceDrawCancel = true;
		}
	}
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1)  END
// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない START
	/**
	 * MapViewクラスからの描画キャンセル要求.
	 * @param なし.
	 */
	public void callScaleChange(){
		if( !mDataManager.isGetContents() ){
			return;
		}
		// 他動作中の時はキャッシュ情報で描画しない
		if( DriverContentsDataManager.STATE_TYPE_NONE == mDataManager.GetDataManagerState() ){
			JNILong lnDistance = new JNILong();
	        NaviRun.GetNaviRunObj().JNI_Java_GetScaleDistance(lnDistance);
			lOldScale = lNewScale;
			lNewScale = lnDistance.getLcount();
			// 詳細地図⇔ベース地図へ遷移した時
			if( (lOldScale < 50 && lNewScale >= 50) || (lOldScale >= 50 && lNewScale < 50) ){
				mDataManager.cacheRedrawPOIInfo();
			}
		}
	}
// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  END
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
	/**
	 * NaviDetailSettingクラスからの初期化要求。再取得も行う
	 * @param なし.
	 */
	public void driconSystemClear(){
		if( mDataManager != null ){
//			MapView.getInstance().getMapTopView().setDriveContentsEnable(false);
			mDataManager.cacheDriconDataClear();
			sendEventCode(Constants.DC_GET_CONTENTS, null);
		}
	}
// ADD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
}
