/**
 *  @file		OriSensorData.java
 *  @brief		�X���Z���T�[�ʒm�f�[�^
 *
 *  @attention
 *  @note
 *
 *  @author		Yuki Sawada [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2010-10-13)
 *  @version	$Revision: $ by $Author: $
 *
 */

package net.zmap.android.pnd.v2.data;

public class OriSensorData {
    public int      accuracy;       ///< 精度
    public long     timestamp;      ///< タイムスタンプ
    public float    azimuth;        ///< アジマス
    public float    pitch;          ///< ピッチ
    public float    roll;           ///< ロール
// Add 2011/02/28 sawada Start -->
    public long		status;			///< センサ状態 (SENSOR_STATUS_*)
// Add 2011/02/28 sawada End   <--

	public int		getAccuracy() {
		return accuracy;
	}
	public long		getTimestamp() {
		return timestamp;
	}
	public float	getAzimuth() {
		return azimuth;
	}
	public float	getPitch() {
		return pitch;
	}
	public float	getRoll() {
		return roll;
	}
// Add 2011/02/28 sawada Start -->
	public long		getStatus() {
		return status;
	}
// Add 2011/02/28 sawada End   <--
}
