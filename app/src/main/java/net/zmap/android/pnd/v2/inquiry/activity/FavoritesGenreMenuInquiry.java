//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.common.view.ScrollBoxAdapter;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Mybox_ListGenreCount;
import net.zmap.android.pnd.v2.data.POI_Mybox_ListGenreName;
import net.zmap.android.pnd.v2.data.POI_UserFile_RecordIF;
import net.zmap.android.pnd.v2.inquiry.data.CommonMethd;

/**
 *
 * S-F1_お気に入り一覧
 * */
public class FavoritesGenreMenuInquiry extends InquiryBaseLoading implements ScrollBoxAdapter {

	private final static int INITIALIZED_INDEX = -1;
	// データ引き渡す対象
	protected Intent oMyIntent = null;
	/** スクロール対象 */
//	protected ScrollBox oBox = null;
	protected ScrollList oList = null;
	/** JNIから取得したデータ */
	private POI_UserFile_RecordIF[] m_Poi_UserFile_Listitem = null;
	/** 表示した内容の対象 */
	private POI_UserFile_RecordIF[] m_Poi_Show_Listitem = null;
	/** 毎回、実際取得したレコード数 */
	protected JNILong Rec = new JNILong();
	/** 選択ジャンル種類に施設の件数 */
	private POI_Mybox_ListGenreCount[] Count;
	/** 選択ジャンル種類に施設の名称 */
	private POI_Mybox_ListGenreName[] ListGenreName;
//	private int SCROLL_UP= 1;
//	private int SCROLL_DOWN= 0;
	// public int KIND_OF_FRIEND = 0;
	// public int KIND_OF_EAT = 1;
	// public int KIND_OF_SHOPPING = 2;
	// public int KIND_OF_INTEREST = 3;
	// public int KIND_OF_SIGHTSEEING = 4;
	// public int KIND_OF_WORK = 5;
	// public int KIND_OF_OTHERS = 6;
	/** 自宅のタイプ */
	protected int KIND_OF_HOUSE = 7;
	/** ディフォルトの自宅件数 */
	protected int HOUSE_COUNT = 2;
	/** 実の自宅件数 */
	protected int real_house_count = 0;

	/** タイトル部分の内容 */
	private String sTitle = "";
	/** 表示の総件数 */
	protected int recordCount = 0;
	/** 取得データの開始Index */
	private long RecIndex = 0;
	private int clickGenreIndex = -1;
	/** 先回、表示リストのIndex */
	private int wOldPos = 0;
	private int getRecordIndex = 0;
	// private int clickItem = -1;

	/** 0:初期化　1：ジャンルタイプ */
	private int waitDialogType = -1;
	private int GENRE_GROUP_BUTTON = 0;
	private int GENRE_LIST_BUTTON = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//オレンジ部分のタイトルを設定する
//Del 2011/09/17 Z01_h_yamada Start -->
//		this.setMenuTitle(R.drawable.menu_favorites_load);
//Del 2011/09/17 Z01_h_yamada End <--
		oMyIntent = getIntent();
//Del 2011/09/17 Z01_h_yamada Start -->
//		int titleId = oMyIntent.getIntExtra(Constants.PARAMS_TITLE, 0);
//		if (0 != titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--

		//タイトルの内容を取得する
		sTitle = oMyIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);


		//データリストを初期化する
		initPoiDataObj();
		if (AppInfo.isEmpty(sTitle)) {
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
			initRecordCount();
		}

		RecIndex = 0;


		LayoutInflater oInflater = LayoutInflater.from(this);
		LinearLayout oLayout = (LinearLayout) oInflater.inflate(
				R.layout.inquiry_base_list_freescroll, null);
		LinearLayout oTitleLayout = (LinearLayout) oLayout.findViewById(R.id.LinearLayout01);
		TextView oText = (TextView) oLayout.findViewById(R.id.inquiry_title);
		Button obtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
		obtn.setVisibility(Button.GONE);

		oList = (ScrollList)oLayout.findViewById(R.id.scrollList);
//Del 2011/10/06 Z01_h_yamada Start -->
//		oList.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
		oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));


		if (AppInfo.isEmpty(sTitle)) {
			oTitleLayout.setVisibility(LinearLayout.GONE);
			oText.setVisibility(TextView.GONE);// .setText(R.string.history_title);
//Chg 2011/09/27 Z01_h_yamada Start -->
//			oLayout.setPadding(10, 45, 10, 45);
//--------------------------------------------
			Resources res = getResources();
			oLayout.setPadding((int)res.getDimension(R.dimen.HIB_Layout_PaddingLeft),
							   (int)res.getDimension(R.dimen.HIB_Layout_PaddingTop),
							   (int)res.getDimension(R.dimen.HIB_Layout_PaddingRight),
							   (int)res.getDimension(R.dimen.HIB_Layout_PaddingBottom));
//Chg 2011/09/27 Z01_h_yamada End <--
		} else {
			oTitleLayout.setVisibility(LinearLayout.VISIBLE);
			oText.setVisibility(TextView.VISIBLE);
			oText.setText(sTitle);
		}
//		oBox = (ScrollBox) oLayout.findViewById(R.id.scrollBox);
//		oBox.setAdapter(this);

		oList.setAdapter(listAdapter);

		ScrollTool oTool = new ScrollTool(this);
//		oTool.bindView(oBox);
		oTool.bindView(oList);
		setViewInOperArea(oTool);

		this.setViewInWorkArea(oLayout);
	}
	private ListAdapter listAdapter = new BaseAdapter (){

		@Override
		public int getCount() {
			int iCount = FavoritesGenreMenuInquiry.this.getCount();
			if (iCount <5) {
				return 5;
			} else {
				return iCount;
			}

		}

		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int wPos, View oView, ViewGroup parent) {

//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"1111 getView wPos--------------->" + wPos);
			return FavoritesGenreMenuInquiry.this.getView(wPos, oView);
		}

	};

	/**
	 * 表示したデータの対象をリセットする
	 * @param lRecIndex データを取得するとき、スタートのIndex
	 * @param Rec 実際の件数
	 *
	 * */
	private void resetShowList(Long lRecIndex,JNILong Rec) {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"resetShowList lRecIndex======" + lRecIndex);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"resetShowList Rec======" + Rec.lcount);
		if (lRecIndex < ONCE_GET_COUNT) {
			int len = m_Poi_UserFile_Listitem.length;
			for (int i = 0 ; i < len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_UserFile_Listitem[i];
			}
		} else {
			//メモリは二倍とする
			if (lRecIndex >= ONCE_GET_COUNT*2) {
				for (int i = m_Poi_UserFile_Listitem.length-ONCE_GET_COUNT; i < ONCE_GET_COUNT*2 ; i++) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"resetShowList i===" + i);
					if (i <ONCE_GET_COUNT) {
						m_Poi_Show_Listitem[i] = m_Poi_Show_Listitem[i+ONCE_GET_COUNT];
					} else {
						m_Poi_Show_Listitem[i] = new POI_UserFile_RecordIF();
					}
				}
			}

			int len = (int)Rec.lcount;
			int j = 0;

			for (int i = ONCE_GET_COUNT ; i < ONCE_GET_COUNT + len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_UserFile_Listitem[j];
//				System.out.println(m_Poi_Show_Listitem[i].getM_Name());
				j++;
			}
		}

	}
	/**
	 * 表示リストのIndexによって、表示対象に対応した内容を取得する
	 * @param wPos 表示リストのIndex
	 * @return POI_UserFile_RecordIF 表示対象に対応した内容
	 * */
	private POI_UserFile_RecordIF getShowObj(int wPos) {

		int showIndex = getShowIndex(wPos);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"showIndex=========" + showIndex);
		//31件、41件の状態で、上にスクロールの問題を決めるために
//		if (pageIndex >= 2 && wOldPos > wPos) {
//			if (m_Poi_Show_Listitem[showIndex].getM_DispName() == null) {
//				showIndex = showIndex-ONCE_GET_COUNT;
//			}
//		} else if (wOldPos > wPos) {
//			if (showIndex-ONCE_GET_COUNT > 0) {
//				showIndex = showIndex-ONCE_GET_COUNT;
//			}
//		}
		return m_Poi_Show_Listitem[showIndex];
	}

	/**
	 * 表示リストのIndexによって、表示対象のIndexを取得する
	 * @param wPos 表示リストのIndex
	 * @return int 表示対象のIndex
	 *
	 * */
	private int getShowIndex(int wPos) {
		if (wPos >= ONCE_GET_COUNT*2) {
			int pageIndex  = wPos/ONCE_GET_COUNT;
			wPos = wPos-ONCE_GET_COUNT*pageIndex+ONCE_GET_COUNT;
		}
		return wPos;
	}
	@Override
	public int getCount() {
		return recordCount;
	}

	@Override
	public int getCountInBox() {

		return 5;
	}


	@Override
	public View getView(final int wPos, View oView) {
		LinearLayout oListLayout = null;
		ImageView oDelView = null;
		long dataRecIndex = 0;
		if (oView == null || !(oView instanceof LinearLayout)) {
			LayoutInflater oInflater = LayoutInflater.from(this);
			oListLayout = (LinearLayout) oInflater.inflate(
					R.layout.inquiry_delete_button, null);
			oView = oListLayout;
//			if (wPos == 0) {
//				initPoiDataObj();
//				NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
//				initRecordCount();
//				RecIndex = 0;
//			}

		} else {
//			if (wPos % getCountInBox() == 0) {
//				// ページIndexを取得する
//				pageIndex = wPos / getCountInBox();
//				RecIndex = getCountInBox() * pageIndex;
//			}
			oListLayout = (LinearLayout) oView;
		}

//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"wPos%ONCE_GET_COUNT=====" + (wPos%ONCE_GET_COUNT));
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"recordCount=====" + recordCount);
//		if (wPos < getCount() &&(wPos%ONCE_GET_COUNT == 0 ||
//
//				(wPos > wOldPos && wPos % ONCE_GET_COUNT < getCountInBox())
//				)) {
//			// ページIndexによって、データを取得する
//			// メモリを最優化するため、毎回は5つデータを取得する
//			pageIndex= wPos/ONCE_GET_COUNT;
//			if (AppInfo.isEmpty(sTitle)) {
//				initListData();
//			} else {
//				Intent oIntent = getIntent();
//				int genreIndex = oIntent.getIntExtra(
//						Constants.PARAMS_GENRE_INDEX, 0);
//
//				if (wOldPos > wPos) {
//					dataRecIndex = wPos-ONCE_GET_COUNT;
//
//				} else {
//					dataRecIndex = wPos/ONCE_GET_COUNT*ONCE_GET_COUNT;
//				}
//				if (dataRecIndex <0) {
//					dataRecIndex = 0;
//				}
//				m_Poi_UserFile_Listitem = reShowData(dataRecIndex, ONCE_GET_COUNT,
//						genreIndex, m_Poi_UserFile_Listitem);
//				if (wOldPos > wPos) {
//					resetShowList(dataRecIndex,Rec,SCROLL_UP);
//				} else {
//					resetShowList(dataRecIndex,Rec,SCROLL_DOWN);
//				}
//			}
//		} else if (wOldPos - wPos <= getCountInBox() + 1 && dataRecIndex + ONCE_GET_COUNT > wPos) {
//			if (!AppInfo.isEmpty(sTitle)) {
//				Intent oIntent = getIntent();
//				int genreIndex = oIntent.getIntExtra(
//						Constants.PARAMS_GENRE_INDEX, 0);
//				dataRecIndex = wPos/ONCE_GET_COUNT*ONCE_GET_COUNT;
//				m_Poi_UserFile_Listitem = reShowData(dataRecIndex, ONCE_GET_COUNT,
//						genreIndex, m_Poi_UserFile_Listitem);
//				resetShowList(dataRecIndex,Rec,SCROLL_UP);
//			}
//		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ---wOldPos <= wPos---->RecIndex===" +wOldPos + "----------->>>" +  wPos + "---->" + RecIndex);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ---wOldPos <= wPos---->dataRecIndex===" + wPos + "---->" + dataRecIndex);
		if (AppInfo.isEmpty(sTitle)) {
			//ジャンル画面のデータを初期化
			initListData();
		} else {
			//下にスクロールの場合
			Intent oIntent = getIntent();
			int genreIndex = oIntent.getIntExtra(
					Constants.PARAMS_GENRE_INDEX, 0);
			if (wOldPos <= wPos) {
				if (wPos < getCount() && ONCE_GET_COUNT != 0
						 && (wPos % ONCE_GET_COUNT == 0 ||
									(getRecordIndex != wPos / ONCE_GET_COUNT))) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview  1==== wPos===="
//							+ wPos + ",==getRecordIndex==" + getRecordIndex
//							+ ",==(wPos / ONCE_GET_COUNT)=="
//							+ (wPos / ONCE_GET_COUNT));

					dataRecIndex = wPos/ONCE_GET_COUNT*ONCE_GET_COUNT;
					getRecordIndex = wPos / ONCE_GET_COUNT;
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"===============>" + getCount());
					m_Poi_UserFile_Listitem = reShowData(dataRecIndex, ONCE_GET_COUNT,
							genreIndex, m_Poi_UserFile_Listitem);
					resetShowList(dataRecIndex,Rec);
					if (real_house_count == 0 &&
							(genreIndex == KIND_OF_HOUSE || genreIndex == KIND_OF_HOUSE + 1)) {
						setResult(Activity.RESULT_CANCELED, oIntent);
						finish();
					}
				}
			} else {
				if ( getRecordIndex != wPos / ONCE_GET_COUNT) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview  2==== wPos===="
//							+ wPos + ",==getRecordIndex==" + getRecordIndex
//							+ ",==(wPos / ONCE_GET_COUNT)=="
//							+ (wPos / ONCE_GET_COUNT));
					dataRecIndex = wPos/ONCE_GET_COUNT*ONCE_GET_COUNT;
					getRecordIndex = wPos / ONCE_GET_COUNT;
					m_Poi_UserFile_Listitem = reShowData(dataRecIndex, ONCE_GET_COUNT,
							genreIndex, m_Poi_UserFile_Listitem);
					resetShowList(dataRecIndex,Rec);
				}
			}
		}
		// hangeng del start Bug1696
		//Button obtn = (Button) oListLayout.findViewById(R.id.btn_value);
		// hangeng del end Bug1696
		// hangeng add start Bug1696
		final Button obtn = (Button) oListLayout.findViewById(R.id.btn_value);
		// hangeng add end Bug1696
		oDelView = (ImageView) oListLayout.findViewById(R.id.btn_delete);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ---1---->wPos===" + wPos);
		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ---1---->getCount===" + getCount());
		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ---1---->RecCount.lcount===" +
		// RecCount.lcount);
		if (wPos < this.getCount()) {

			if (AppInfo.isEmpty(sTitle)) {

				if (Count[getShowIndex(wPos)].getM_sRecordCount() == 0
						|| KIND_OF_HOUSE + 1 == wPos) {
					obtn.setEnabled(false);

					oDelView.setEnabled(false);

				} else {
					obtn.setEnabled(true);
					oDelView.setEnabled(true);
				}

				// 自宅の場合
				if (KIND_OF_HOUSE == wPos) {
					int localCount = Count[getShowIndex(wPos)]
							.getM_sRecordCount()
							+ Count[getShowIndex(wPos) + 1]
									.getM_sRecordCount();
					obtn.setText(ListGenreName[getShowIndex(wPos)]
							.getM_GenreName()
							+ "  " + "(" + localCount + ")");
					if (localCount == 0) {
						obtn.setEnabled(false);

						oDelView.setEnabled(false);
					} else {
						obtn.setEnabled(true);

						oDelView.setEnabled(true);
					}
					// 自宅2のIndexは8ですから、8の場合、該当部分は0とする
				} else if (KIND_OF_HOUSE + 1 == wPos) {
					int localCount = 0;
					obtn.setText(ListGenreName[getShowIndex(wPos)]
							.getM_GenreName()
							+ "  " + "(" + localCount + ")");
				} else {
					obtn.setText(ListGenreName[getShowIndex(wPos)]
							.getM_GenreName()
							+ "  "
							+ "("
							+ Count[getShowIndex(wPos)].getM_sRecordCount()
							+ ")");
				}

			} else {
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"157  name===" + getShowObj(wPos).getM_DispName());
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"157  lat===" + getShowObj(wPos).getM_lLat());
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"157  long===" + getShowObj(wPos).getM_lLon());
				if (null != getShowObj(wPos) && !AppInfo.isEmpty(getShowObj(wPos).getM_DispName())) {


					obtn.setText(getShowObj(wPos).getM_DispName());
					if (getShowObj(wPos).getM_lLat() != 0 && getShowObj(wPos).getM_lLon() != 0) {
						obtn.setEnabled(true);
						oDelView.setEnabled(true);
					} else {
						obtn.setEnabled(false);
						oDelView.setEnabled(false);
					}
				} else {
					obtn.setText("");
					obtn.setEnabled(false);
					oDelView.setEnabled(false);
				}
			}
		} else {
			obtn.setText("");
			obtn.setEnabled(false);
			oDelView.setEnabled(false);
		}
		obtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 お気に入り地点編集 選択禁止 Start -->
				if(DrivingRegulation.CheckDrivingRegulation()){
					return;
				}
//ADD 2013.08.08 M.Honma 走行規制 住所 お気に入り地点編集 選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
				// hangeng add start Bug1696
				if (obtn.getText().toString().equalsIgnoreCase(
						FavoritesGenreMenuInquiry.this.getResources().getString(R.string.dialog_myhome_save_button1))
					|| obtn.getText().toString().equalsIgnoreCase(
							FavoritesGenreMenuInquiry.this.getResources().getString(R.string.dialog_myhome_save_button2))) {
					//自宅の場合   nothing to do when onClick
				}else{
			    // hangeng add end Bug1696
				if (AppInfo.isEmpty(sTitle)) {
					oMyIntent.putExtra(Constants.PARAMS_SEARCH_KEY,
							ListGenreName[getShowIndex(wPos)].getM_GenreName());
					recordCount = Count[getShowIndex(wPos)].getM_sRecordCount();
					if (wPos == KIND_OF_HOUSE) {
//						recordCount = Count[getShowIndex(wPos)].getM_sRecordCount() + Count[getShowIndex(wPos+1)].getM_sRecordCount() ;
//					} else if (wPos != KIND_OF_HOUSE+1){
//						recordCount = Count[getShowIndex(wPos)].getM_sRecordCount();
						recordCount = HOUSE_COUNT;
					}
					wOldPos= 0;
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"getShowIndex(wPos)==" + getShowIndex(wPos));
					oMyIntent.putExtra(Constants.PARAMS_RECORD_COUNT,recordCount);
					oMyIntent.putExtra(Constants.PARAMS_GENRE_INDEX, wPos);
					oMyIntent.setClass(FavoritesGenreMenuInquiry.this,FavoritesMenuInquiry.class);
//Chg 2011/09/23 Z01yoneya Start -->
//					FavoritesGenreMenuInquiry.this.startActivityForResult(
//							oMyIntent, AppInfo.ID_ACTIVITY_FAVORITESMENUINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
					NaviActivityStarter.startActivityForResult(FavoritesGenreMenuInquiry.this, oMyIntent, AppInfo.ID_ACTIVITY_FAVORITESMENUINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
				} else {
					// oBox.refresh();

					oMyIntent.putExtra(Constants.PARAMS_LIST_INDEX, wPos);
					// m_Poi_UserFile_Listitem[wPos%getCountInBox()].getM_lLat()
					// m_Poi_UserFile_Listitem[wPos%getCountInBox()].getM_lLon()
////////////////////////////
					int genreIndex = oMyIntent.getIntExtra(
							Constants.PARAMS_GENRE_INDEX, 0);
					long localIndex = wPos/ONCE_GET_COUNT*ONCE_GET_COUNT;
					m_Poi_UserFile_Listitem = reShowData(localIndex, ONCE_GET_COUNT,
							genreIndex, m_Poi_UserFile_Listitem);
					resetShowList(localIndex,Rec);
					//////////////////////////////
					oMyIntent.putExtra(Constants.FLAG_TITLE, getShowObj(wPos).getM_DispName());
					oMyIntent.putExtra(Constants.PARAMS_BUTTON_NAME,
							FavoritesGenreMenuInquiry.this.getResources().getString(R.string.btn_name_mod));
					oMyIntent.setClass(FavoritesGenreMenuInquiry.this,
							FavoritesModInquiry.class);
//Chg 2011/09/23 Z01yoneya Start -->
//					startActivityForResult(oMyIntent,
//							AppInfo.ID_ACTIVITY_FAVORITESMODINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
					NaviActivityStarter.startActivityForResult(((Activity)v.getContext()), oMyIntent,AppInfo.ID_ACTIVITY_FAVORITESMODINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
				}
				 // hangeng add start Bug1696
				  }
				  // hangeng add end Bug1696
			}
		});
		oDelView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 お気に入り地点編集 削除禁止 Start -->
				if(DrivingRegulation.CheckDrivingRegulation()){
					return;
				}
//ADD 2013.08.08 M.Honma 走行規制 住所 お気に入り地点編集 削除禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
// Chg 2011/10/18 Z01kkubo Start --> 課題No.272 お気に入り101件目以降を削除すると51～100件目が削除される問題対処
//				clickGenreIndex = getShowIndex(wPos);
//------------------------------------------------------------------------------------------------------------------------------------
				// お気に入り削除時は、削除対象のデータ位置をそのままIndexに設定する
				clickGenreIndex = wPos;
// Chg 2011/10/18 Z01kkubo End <--

//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"clickGenreIndex====" + clickGenreIndex);
				if (AppInfo.isEmpty(sTitle)) {
					oMyIntent.putExtra(Constants.PARAMS_GENRE_INDEX, wPos);
					showDialog(Constants.DIALOG_DELETE_FAVORITESGENRE);
				} else {
					int genreIndex = oMyIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX, -1);
					int genreCount = oMyIntent.getIntExtra(Constants.PARAMS_RECORD_COUNT, -1);
					if (genreIndex == KIND_OF_HOUSE && genreCount == 1 && wPos == 0) {
						POI_UserFile_RecordIF[] mPoiUserFileListitem = new POI_UserFile_RecordIF[1];
						mPoiUserFileListitem[0] = new POI_UserFile_RecordIF();
						JNILong Rec = new JNILong();
						NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecList((long)genreIndex, (long)0, (long)1,
								mPoiUserFileListitem, Rec);
						if (Rec.lcount == 0) {
							oMyIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_HOUSE+1);
						}
					} else if (genreIndex == KIND_OF_HOUSE && genreCount == 2 && wPos == 1) {
						oMyIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_HOUSE+1);
					}

					long localIndex = wPos/ONCE_GET_COUNT*ONCE_GET_COUNT;
					m_Poi_UserFile_Listitem = reShowData(localIndex, ONCE_GET_COUNT,
							genreIndex, m_Poi_UserFile_Listitem);
					resetShowList(localIndex,Rec);

					showDialog(Constants.DIALOG_DELETE_FAVORITES);
				}

			}
		});

		if (!AppInfo.isEmpty(sTitle)) {
			//上にスクロールの場合

			if (wOldPos > wPos) {
				if (wPos < getCount() && ONCE_GET_COUNT != 0
						&& wPos >= ONCE_GET_COUNT && (wPos % ONCE_GET_COUNT == 0 ||
								(getRecordIndex != wPos / ONCE_GET_COUNT))) {
		//			getRecordIndex = wPos/ONCE_GET_COUNT;
					//上にスクロール場合
					NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview  3==== wPos===="
							+ wPos + ",==getRecordIndex==" + getRecordIndex
							+ ",==(wPos / ONCE_GET_COUNT)=="
							+ ((wOldPos - ONCE_GET_COUNT)  / ONCE_GET_COUNT));
					Intent oIntent = getIntent();
					int genreIndex = oIntent.getIntExtra(
							Constants.PARAMS_GENRE_INDEX, 0);
					dataRecIndex = (wOldPos - ONCE_GET_COUNT)/ONCE_GET_COUNT*ONCE_GET_COUNT;
					getRecordIndex = (wOldPos - ONCE_GET_COUNT) / ONCE_GET_COUNT;
					m_Poi_UserFile_Listitem = reShowData(dataRecIndex, ONCE_GET_COUNT,
							genreIndex, m_Poi_UserFile_Listitem);
					resetShowList(dataRecIndex,Rec);

				}
			}
		}
		wOldPos = wPos;
		return oView;
	}
	/**
	 * ジャンル画面に表示した内容を取得する
	 *
	 * */
	private void initListData() {

		//yangyang mod start Bug1122
//		Count = new POI_Mybox_ListGenreCount[ONCE_GET_COUNT];
//		ListGenreName = new POI_Mybox_ListGenreName[ONCE_GET_COUNT];
//		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ------->pageIndex===" + pageIndex);
//		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ------->RecIndex===" + RecIndex);
//		for (int i = (int)RecIndex; i < ONCE_GET_COUNT ; i++) {
//			Count[i % ONCE_GET_COUNT] = new POI_Mybox_ListGenreCount();
//			ListGenreName[i % ONCE_GET_COUNT] = new POI_Mybox_ListGenreName();
//			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecCount(i,
//					Count[i % ONCE_GET_COUNT]);
//			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecName(i,
//					ListGenreName[i % ONCE_GET_COUNT]);
//		}

		Count = new POI_Mybox_ListGenreCount[recordCount+1];
		ListGenreName = new POI_Mybox_ListGenreName[recordCount+1];
		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ------->pageIndex===" + pageIndex);
		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ------->RecIndex===" + RecIndex);
		for (int i = (int)RecIndex; i < recordCount+1 ; i++) {
			Count[i % (recordCount+1)] = new POI_Mybox_ListGenreCount();
			ListGenreName[i % (recordCount+1)] = new POI_Mybox_ListGenreName();
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecCount(i,
					Count[i % (recordCount+1)]);
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecName(i,
					ListGenreName[i % (recordCount+1)]);
		}

		//yangyang mod start Bug1122
	}

	/**
	 * ジャンル種類の件数を取得する
	 *
	 * */
	private void initRecordCount() {
		JNILong RecCount = new JNILong();
		NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetGenreCount(RecCount);
		recordCount = (int) RecCount.lcount-1;
	}

	/**
	 * 全部右側のデータを表示する
	 * @param start 開始のIndex
	 * @param count 件数
	 * @param genreId ジャンルのId
	 * @param m_Poi_UserFile_Listitem データの対象
	 * @return POI_UserFile_RecordIF[] 取得したデータの対象
	 *
	 * */

	protected POI_UserFile_RecordIF[] reShowData(long start, int count,
			long genreId, POI_UserFile_RecordIF[] m_Poi_UserFile_Listitem) {

		return m_Poi_UserFile_Listitem;

	};

	/**
	 * データリストを初期化する
	 *
	 *
	 * */
	private void initPoiDataObj() {

		m_Poi_UserFile_Listitem = new POI_UserFile_RecordIF[ONCE_GET_COUNT];

		for (int i = 0; i < ONCE_GET_COUNT; i++) {
			m_Poi_UserFile_Listitem[i] = new POI_UserFile_RecordIF();
		}
		m_Poi_Show_Listitem = new POI_UserFile_RecordIF[ONCE_GET_COUNT*2];
		for (int i = 0 ; i < ONCE_GET_COUNT*2 ; i++) {
			m_Poi_Show_Listitem[i] = new POI_UserFile_RecordIF();
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		//ジャンルグループの削除
		if (id == Constants.DIALOG_DELETE_FAVORITESGENRE) {
			CustomDialog oDialog = new CustomDialog(this);
			oDialog.setTitle(R.string.favoritesgenremenu_del_title);
			String msg = CommonMethd.reSetMsg(ListGenreName[clickGenreIndex]
					.getM_GenreName(), this.getResources().getString(
					R.string.favoritesgenremenu_del_msg));
			oDialog.setMessage(msg);
			oDialog.addButton(this.getResources().getString(R.string.btn_ok),
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							waitDialogType = GENRE_GROUP_BUTTON;
							startShowPage(NOTSHOWCANCELBTN);


						}
					});
			oDialog.addButton(R.string.btn_cancel, new OnClickListener() {

				@Override
				public void onClick(View v) {
					removeDialog(Constants.DIALOG_DELETE_FAVORITESGENRE);

				}
			});
			return oDialog;

			//ジャンルリストの削除
		} else if (id == Constants.DIALOG_DELETE_FAVORITES) {
			CustomDialog oDialog = new CustomDialog(this);
			oDialog.setTitle(R.string.favoritesmenu_del_title);
			String msg = CommonMethd.reSetMsg(
//					m_Poi_UserFile_Listitem[clickGenreIndex].getM_DispName(),
					getShowObj(clickGenreIndex).getM_DispName(),
					this.getResources().getString(
							R.string.favoritesmenu_del_msg));
			oDialog.setMessage(msg);
			oDialog.addButton(this.getResources().getString(R.string.btn_ok),
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							waitDialogType = GENRE_LIST_BUTTON;
							startShowPage(NOTSHOWCANCELBTN);

//
						}

					});
			oDialog.addButton(R.string.btn_cancel, new OnClickListener() {

				@Override
				public void onClick(View v) {
					removeDialog(Constants.DIALOG_DELETE_FAVORITES);

				}
			});
			return oDialog;
		}
		return super.onCreateDialog(id);
	}

	/**
	 * Created on 2010/12/12 Title: deleteGroup Description: お気に入りジャンルを削除する
	 *
	 * @return void 無し
	 * @version 1.0
	 */
	private void deleteGroup() {
		Intent oIntent = getIntent();
		int genreIndex = oIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX, -1);
		NaviRun.GetNaviRunObj().JNI_NE_POIMybox_RemoveGenreAllRec(genreIndex);
		if (clickGenreIndex + RecIndex == KIND_OF_HOUSE) {
			//yangyang mod start Bug984
//			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_RemoveRec(8, 0);
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_RemoveGenreAllRec(genreIndex +1);
			//yangyang mod end Bug984
		}
		initListData();
	}

	/**
	 * Created on 2010/12/12 Title: deleteGroup Description: お気に入りジャンルのリストを削除する
	 *
	 * @return void 無し
	 * @version 1.0
	 */
	private void deleteList() {
		Intent oIntent = getIntent();
		int genreIndex = oIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX, -1);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"deleteList genreIndex===" + genreIndex);
		// 自宅２の場合（KIND_OF_HOUSE+1 ）、リストに2つ目のレコード
		if (genreIndex == KIND_OF_HOUSE + 1) {
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_RemoveRec(8, 0);
		} else {
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_RemoveRec(genreIndex,
					(clickGenreIndex + RecIndex));
		}
	}

	// @Override
	// public boolean onKeyDown(int keyCode, KeyEvent event) {
	// if (keyCode == KeyEvent.KEYCODE_BACK) {
	// oIntent.removeExtra(Constants.PARAMS_SEARCH_KEY);
	// sTitle = "";
	// oBox.refresh();
	//
	// }
	// return super.onKeyDown(keyCode, event);
	// }

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent oIntent) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == AppInfo.ID_ACTIVITY_FAVORITESMENUINQUIRY || requestCode == AppInfo.ID_ACTIVITY_FAVORITESMODINQUIRY) {
				setResult(resultCode, oIntent);
				finish();
				return;
			}
			// 編集画面の戻るボタンために修正した、587バグ対応した
			// } else if (resultCode == Activity.RESULT_CANCELED) {
			// initPoiDataObj();
			// NaviLog.d(NaviLog.PRINT_LOG_TAG,"RESULT_CANCELED");
			// if (requestCode != AppInfo.ID_ACTIVITY_FAVORITESMENUINQUIRY) {
			// NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
			// sTitle = "";
			// initRecordCount();
			// }
			//
			// RecIndex = 0;
			// // if (oIntent != null) {
			// // oIntent.removeExtra(Constants.PARAMS_SEARCH_KEY);
			// // }
			//
			// oBox.refresh();
			//because Delete List 620
		} else if (resultCode == Activity.RESULT_CANCELED) {
			if (requestCode == AppInfo.ID_ACTIVITY_FAVORITESMENUINQUIRY) {
				initRecordCount();
				sTitle = "";
				if (oIntent != null) {
					oIntent.removeExtra(Constants.PARAMS_SEARCH_KEY);
				}
				initListData();
			}
//			oBox.refresh();
			oList.scroll(0);
			oList.reset();
		}
		super.onActivityResult(requestCode, resultCode, oIntent);
	}

	@Override
	protected boolean onClickGoBack() {
		if (AppInfo.isEmpty(sTitle)) {
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
		}
		return super.onClickGoBack();
	}
	@Override
	protected boolean onClickGoMap() {
		NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
		return super.onClickGoMap();
	}
	@Override
	protected boolean onStartShowPage() throws Exception {
		if (waitDialogType == GENRE_GROUP_BUTTON) {
			removeDialog(Constants.DIALOG_DELETE_FAVORITESGENRE);
			deleteGroup();
			return true;
		} else if (waitDialogType == GENRE_LIST_BUTTON) {
			removeDialog(Constants.DIALOG_DELETE_FAVORITES);
			deleteList();
			initPoiDataObj();
			// initRecordCount();
//			recordCount = oIntent.getIntExtra(Constants.PARAMS_RECORD_COUNT, 0);
			if (recordCount > 0) {
				recordCount--;
			}
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"ondelclick----------recordCount=====" + recordCount);
			if (recordCount == 0) {
				setResult(Activity.RESULT_CANCELED, oMyIntent);
				finish();
			} else {
				wOldPos = recordCount;
				getRecordIndex = INITIALIZED_INDEX;

				if (oMyIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX, -1) == KIND_OF_HOUSE + 1 ||
						oMyIntent.getIntExtra(Constants.PARAMS_GENRE_INDEX, -1) == KIND_OF_HOUSE) {
					oMyIntent.putExtra(Constants.PARAMS_GENRE_INDEX, KIND_OF_HOUSE);
					recordCount = HOUSE_COUNT;
				}
				oMyIntent.putExtra(Constants.PARAMS_RECORD_COUNT, recordCount);
//							oBox.refresh();


			}
			return true;
		}
		return false;
	}
	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {
		if(bGetData)
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					RecIndex = 0;
//						oBox.refresh();
					oList.scroll(0);
					oList.reset();
					//yangyang mod start Bug1112
					if (waitDialogType == GENRE_GROUP_BUTTON || waitDialogType == GENRE_LIST_BUTTON) {
						NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();
					}
					//yangyang mod end Bug1112
				}


			});
		}

	}


}
