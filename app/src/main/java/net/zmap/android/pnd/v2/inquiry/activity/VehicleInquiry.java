//******************************************************************
// added by furusawa
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.util.GeoUtils;
import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.DeliveryInfo;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.utils.DbHelper;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollBox;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.ScrollBoxAdapter;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNIThreeShort;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.inquiry.view.ButtonImg;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.maps.OpenMap;
import net.zmap.android.pnd.v2.route.data.RouteNodeData;
import net.zmap.android.pnd.v2.sakuracust.CommonInquiryOps;
import net.zmap.android.pnd.v2.sakuracust.DrawTrackActivity;


/**
 * EXPO用の表示クラス
 * */
public class VehicleInquiry extends InquiryBaseLoading implements ScrollBoxAdapter {

    /** JNIの戻り値　取得した都道府県のレコード数 */
    protected JNILong Rec = new JNILong();
    /** 選択した都道府県の市区町村のレコード数 */
    protected JNILong ListCount;
    /** 自車の位置（市区町村の名称） */
    protected String ms2Str = "";//new String();

    /** 総件数 */
    protected int allRecordCount = 0;
    /** 全国ボタン対象 */
    protected Button AllCountry = null;
    /** スクロール対象 */
    protected ScrollBox oBox = null;

    /** 自車の位置（アドレス） */
    protected JNIString AddressString = new JNIString();

    /** 自車の位置（都道府県の名称） */
    protected String ms1Str = "";//new String();
    /** 自車の位置（大字の名称） */
    private String ms3Str = "";//new String();

    /** 検索するため、スタートIndex */
    private int RecIndex = 0;
    /** カレントページのIndex */
    private int pageIndex = 0;
    /** 都道府県ボタンのIndex */
    private int dataIndex = 0;
    /** タイトルの内容 */
    private String searchValue = "";
    /** 選択された配車No index */
    private int mCarPos;
    /** タイトル */
    private TextView mTitle = null;

    // Error Code
    static final int DB_NOT_EXIST = 1;
    static final int DB_HAS_NO_DATA = 2;
    static final int DB_HAS_NO_COORDINATE_DATA = 3;
    
    
    private DbHelper mHelper;
    private SQLiteDatabase mDb;
// Mod by CPJsunagawa '2015-07-28 Start 他のクラスから参照するために変更
//    private String[] mCars = null;
//    private int mCarNum = 0;
    private static String[] mCars = null;
    private static int mCarNum = 0;
// Mod by CPJsunagawa '2015-07-28 End

//  private boolean onlyRunOne = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (!DbHelper.isExistDB())
        {
        	Toast ts = Toast.makeText(getApplicationContext(), "CSVが取り込まれておりません。", Toast.LENGTH_LONG);
        	ts.show();
        	return;
        }
        
        oIntent = this.getIntent();
        mHelper = new DbHelper(getApplicationContext());
//        mDb = mHelper.getReadableDatabase();
// カメラ連携 エラー処理の為変更
        mDb = mHelper.getWritableDatabase();
        if( mDb == null ) {
            showError(DB_NOT_EXIST);
            finish();
        } else {
             startShowPage(NOTSHOWCANCELBTN);
        }
        startShowPage(NOTSHOWCANCELBTN);

    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if( mDb != null ) {
            mDb.close();
        }
    }

    private ListAdapter listAdapter = new BaseAdapter() {

        @Override
        public int getCount() {
            return mCarNum;
        }

        //yangyang add start

//      @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public boolean areAllItemsEnabled() {
            // TODO 自動生成されたメソッド・スタブ
            return false;
        }

        //yangyang add end
        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int wPos, View oView, ViewGroup parent) {

            return VehicleInquiry.this.getView(wPos, oView);
        }

    };

    /**
     * 都道府県ボタンの名称を取得する
     *
     * @param dataIndex
     *            ボタンのIndex
     * @return String 名称を戻す
     * */
    protected String getButtonValue(int dataIndex) {
        String str = "";
        // 号車に紐づく目的地リスト
        if( dataIndex < mCarNum ) {
            return mCars[dataIndex];
        }
        return str;
    }

    /**
     * カレント画面に表示した都道府県の内容を取得する
     * */
    protected void getCurrentPageShowData() {

    }

    /**
     * データオブジェクトのメモリを申請する
     * */
    //protected void initDataObj() throws UnsupportedEncodingException {
    protected void initDataObj() {
        if( mCars == null ) { // 初回だけ
            // 目的地リストの生成
            //mCarNum = 3;
            //mCars = new String[mCarNum];
            //mCars[0] = "1号車\n作業エリア：A-1　配達コース：北コース";
            //mCars[1] = "2号車\n作業エリア：B-3　配達コース：西コース";
            //mCars[2] = "3号車\n作業エリア：C-2　配達コース：南コース";
        	
        	// DBから情報を取得する。
//        	Cursor cur = mDb.query(true, "CUSTOMERS2", new String[]{"COURSE"},
           	Cursor cur = mDb.query(true, "DeliveryInfo", new String[]{"DeliCourse"},
        			null, null, null, null, null, null);
        	if( cur == null) {
                return;
            }else if( cur.getCount() == 0) {
                cur.close();
                return;
            }
        	
        	mCarNum = cur.getCount();
        	mCars = new String[mCarNum];
        	
        	// 0に決まっているが念のため
        	int idx = cur.getColumnIndex("DeliCourse");
        	//cur.moveToFirst();
        	int counter = 0;
        	while (cur.moveToNext())
        	{
        		//Mod by M.Suna '13-09-03
        		mCars[counter] = cur.getString(idx);
            	System.out.println("VehicleInquiry mCars[counter] => " + mCars[counter]);
/*        		
        		try{
        			mCars[counter] = getStrFromSJIS(cur.getBlob(idx));
        		} catch(UnsupportedEncodingException e) {
        		    System.err.println(e.getMessage());
        	    } finally {
        	        System.out.println("MethodA was finished");
        	    }
        	    */
        		counter++;
        	}
        	
        	cur.close();
        }
        /*
        
        
        else { // Place
            Cursor cur = mDb.query("destination", null, "CarNo = ?", new String[]{mCars[mCarPos]}, null, null, "reservedNo ASC");
            if( cur == null) {
                return;
            }else if( cur.getCount() == 0) {
                cur.close();
                return;
            }
            cur.moveToFirst();
            mSubPlaceNum = cur.getCount();
            mPlaces = new Place[mSubPlaceNum];
            int latitudeIdx, longitudeIdx, carNoIdx, subcontractedNameIdx, reservedNoIdx, placeCodeIdx, placeNameIdx, districtNameIdx, addressIdx;
            try {
                latitudeIdx = cur.getColumnIndex("Latitude");
                longitudeIdx = cur.getColumnIndex("Longitude");
                carNoIdx = cur.getColumnIndex("CarNo");
                subcontractedNameIdx = cur.getColumnIndex("SubcontractedName");
                reservedNoIdx = cur.getColumnIndex("ReservedNo");
                placeCodeIdx = cur.getColumnIndex("PlaceCode");
                placeNameIdx = cur.getColumnIndex("PlaceName");
                districtNameIdx = cur.getColumnIndex("DistrictName");
                addressIdx = cur.getColumnIndex("Address");
            } catch ( IllegalArgumentException e) { // コラムのIndex取得に失敗
                cur.close();
                return ;
            }
            cur.moveToFirst();
            for (int i = 0; i < mSubPlaceNum; i++, cur.moveToNext()) {
                mPlaces[i] = new Place( cur.getFloat(latitudeIdx), cur.getFloat(longitudeIdx), cur.getString(carNoIdx),
                    cur.getString(subcontractedNameIdx), cur.getString(reservedNoIdx), cur.getString(placeCodeIdx),
                    cur.getString(placeNameIdx), cur.getString(districtNameIdx), cur.getString(addressIdx) );
            }
            cur.close();
        }*/
    }

	//Add by M.Suna '13-09-03
    /**
     * SJISに変換した文字列を取得します。
     * @param sjisStream 変換する文字列データ
     * @return SJISに変換したSJISの文字列
     */
    //private static final String getStrFromSJIS(byte[] sjisStream) throws UnsupportedEncodingException
/*    
    public String getStrFromSJIS(byte[] sjisStream) throws UnsupportedEncodingException
    {
        return (sjisStream != null) ? new String(sjisStream, "SJIS") : null;
    }
    */

    
    @Override
    public int getCount() {
        return mCarNum;
    }

    @Override
    public int getCountInBox() {
    	
    	// *******************************************
    	// ここを5件固定なので、配達コースの配達先数に
    	// 変更する！！ by M.Suna 2013/10/18
    	// *******************************************
        //return 5;
        return mCarNum;
    }

    /**
     * 表示データを取得
     */
    @Override
    public View getView(final int wPos, View oView) {
        LinearLayout dataLayout = null;
        if (oView == null) {
            LayoutInflater oInflater = LayoutInflater.from(this);
            dataLayout = (LinearLayout)oInflater.inflate(R.layout.listrow_delivery_car, null);
            oView = dataLayout;
        } else {
            //次ページへ遷移するとき、初期化
            if (wPos % getCountInBox() == 0) {
                //ページIndexを取得する
                pageIndex = wPos / getCountInBox();
                RecIndex = getCountInBox() * pageIndex * 2;
            }
            dataLayout = (LinearLayout)oView;
        }
        TextView tv = (TextView)dataLayout.findViewById(R.id.car_info);
        if( tv == null ) { // Place Layoutが渡されている時には、getに失敗するので作り直す
            LayoutInflater oInflater = LayoutInflater.from(this);
            dataLayout = (LinearLayout)oInflater.inflate(R.layout.listrow_delivery_car, null);
            oView = dataLayout;
            tv = (ButtonImg)dataLayout.findViewById(R.id.inquiry_line_left);
        }
        tv.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        tv.requestFocus();
        //freescroll
        dataIndex = wPos;
        tv.setText(getButtonValue(dataIndex));

        tv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View oView) {
                mCarPos = wPos;
                naviStart();
            }

        });
/*
// Add 20151224 Start
    	// 軌跡データの表示フラグを設定する
    	//aaa
        // 走行軌跡表示開始
        NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(1);
        NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(1);
// Add 20151224 End
*/
        return oView;
    }

    protected int getM_iOldIndex(int dataIndex2) {
        // TODO 自動生成されたメソッド・スタブ
        return dataIndex2;
    }

    protected int getFirstShowIndex() {

        return 0;
    }



    // 全国ボタンのクリック処理
    private OnClickListener OnAllCountryListener = new OnClickListener() {

        @Override
        public void onClick(View oView) {
            Button oBtn = (Button)oView;
//          if (null == ListCount){
//              ListCount = new JNILong();
//          }
//          NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetRecCount(ListCount);
//          NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetRecList(SearchCacheNum * (pageListIndex-1), SearchCacheNum,
//                  m_Poi_Facility_Listitem, ListCount);// all list result

            Intent oIntent = getIntent();
            oIntent.putExtra(Constants.FROM_WHICH_SEARCH_KEY, "AllCountry");
            TextView oText = (TextView)VehicleInquiry.this.findViewById(R.id.inquiry_title);
//          NaviLog.d(NaviLog.PRINT_LOG_TAG,"AllCountry oText.getText().toString()===" + oText.getText().toString());

            String title = oText.getText().toString() + Constants.FLAG_TITLE + oBtn.getText().toString();
            oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
            oIntent.setClass(VehicleInquiry.this, KeyResultInquiry.class);
//Chg 2011/09/23 Z01yoneya Start -->
//          startActivityForResult(oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
            NaviActivityStarter.startActivityForResult(VehicleInquiry.this, oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
        }
    };

    @Override
    protected boolean onStartShowPage() throws Exception {
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStartShowPage start");
        initDataObj();
        if (null == ListCount) {
            ListCount = new JNILong();
        }
        RecIndex = 0;
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStartShowPage end");
        return true;
    }

    @Override
    protected void onFinishShowPage(boolean bGetData) throws Exception {
        if (bGetData) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage start");
                    initDialog();
                    NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage end");
                }

            });
        }

    }

    private void initDialog() {

        if( mTitle == null ) {
            Intent oIntent = getIntent();
            //レイアウトを初期化する
            LayoutInflater oInflater = LayoutInflater.from(this);
            LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base_list_freescroll, null);


            //全国ボタンを初期化する
            AllCountry = (Button)oLayout.findViewById(R.id.inquiry_btn);
            AllCountry.setVisibility(View.GONE); // 不要なので消す by furusawa

            AllCountry.setOnClickListener(OnAllCountryListener);
            initCountryBtn();
            ScrollList oList = (ScrollList)oLayout.findViewById(R.id.scrollList);
            oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
            oList.setAdapter(listAdapter);

            ScrollTool oTool = new ScrollTool(this);

            oTool.bindView(oList);

            setViewInOperArea(oTool);

            //this.setViewInWorkArea(oLayout);
            int LineIndex = getFirstShowIndex();
            oList.setSelection(LineIndex);
            mTitle = (TextView)oLayout.findViewById(R.id.inquiry_title);

            // add by M.Suna 2013/10/19  5件ループはこの処理？　
//        	scroll.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
//          scroll.setPadding(0, 5, 0, 5);
        	oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
            oList.setPadding(0, 5, 0, 5);
            oTool = new ScrollTool(this);
            oTool.bindView(oList);
            setViewInOperArea(oTool);
            setViewInWorkArea(oLayout);
        }
        ((BaseAdapter)listAdapter).notifyDataSetChanged();
        //タイトルを初期化する
        //mTitle.setText("目標物リスト");
        mTitle.setText("配達先リスト");

// if分の中に移動  add by M.Suna 2013/10/19
/*
    	// add by M.Suna 2013/10/19  5件ループはこの処理？　
//    	scroll.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
//      scroll.setPadding(0, 5, 0, 5);
    	oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
        oList.setPadding(0, 5, 0, 5);
        oTool = new ScrollTool(this);
        oTool.bindView(scroll);
        setViewInOperArea(oTool);
        setViewInWorkArea(oLayout);
*/
    }

    protected void initCountryBtn() {
        // TODO 自動生成されたメソッド・スタブ

    }
    
    void showError(int error_code) {
        String errorMsg = "DBファイルが存在しません。"; // DB_NOT_EXIST
        switch(error_code) {
            case DB_HAS_NO_DATA:
                errorMsg = "DBからのデータ取得に失敗しました。";
                break;
            case DB_HAS_NO_COORDINATE_DATA:
                errorMsg = "DBからの座標データ取得に失敗しました。";
                break;
        }
        Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
    }
    
    @Override
    protected boolean onClickGoBack() {
        return super.onClickGoBack();
    }
    
    // カレントのコース名を返却する
//    public String getNowCourseName(){
    public String getNowCourseName(){
//    	if(mCars.equals(null)) {
       	if(mCars == null) {
    		return null;
    	}
    	System.out.println("Into getNowCourseName()");
    	System.out.println("getNowCourseName() mCarPos=> " + mCarPos);
    	System.out.println("getNowCourseName() => " + mCars[mCarPos]);
    	return mCars[mCarPos];
    }
    
/*    
    public void naviStart()
    {
    	naviStart(false);
    }
*/    
    
    //public void naviStart() {
    private void naviStart() {
        // DBからのデータ取得
        //Cursor cur = mDb.query("destination", null, "CarNo = ?", new String[]{Integer.toString(mCarPos+1)}, null, null, "_id ASC");
        //Cursor cur = mDb.query("CUSTOMERS", null, "COURSE = ?", new String[]{mCars[mCarPos]}, null, null, "DELIVERY_ORDER ASC");
        //Cursor cur = mDb.query("CUSTOMERS2", null, "COURSE = ?", new String[]{mCars[mCarPos]}, null, null, "DELIVERY_ORDER ASC");
        //Cursor cur = mDb.query("DeliveryInfo", null, "DeliCourse = ?", new String[]{mCars[mCarPos]}, null, null, "DELIVERY_ORDER ASC");
        //Cursor cur = mDb.query("DeliveryInfo", null, "DeliCourse = ?", new String[]{mCars[mCarPos]}, null, null, "DeliOrder ASC");
    	Intent data = getIntent();
    	boolean isMatchingMode = (data != null && data.hasExtra(Constants.INTENT_EXTRA_MATCHING_MODE));
    	int matchingMode = isMatchingMode ? data.getIntExtra(Constants.INTENT_EXTRA_MATCHING_MODE, 0) : 0;
    	
        NaviApplication na = (NaviApplication)getApplication();
        na.setMatchigMode(matchingMode);

    	int maxMLvl = isMatchingMode ? 7 : 999; 
        Cursor cur = mDb.query("DeliveryInfo", null, "DeliCourse = ? AND MatchLevel <= ?", new String[]{getNowCourseName(), "" + maxMLvl}, null, null, "DeliOrder ASC");
        if( cur == null) {
        	AlertDialog.Builder adb = new AlertDialog.Builder(this);
        	adb.setMessage("配達先がございません。");
        	adb.setPositiveButton("OK", null);
        	adb.show();
            return;
        }else if( cur.getCount() == 0 ) {
        	AlertDialog.Builder adb = new AlertDialog.Builder(this);
        	adb.setMessage("配達先がございません。");
        	adb.setPositiveButton("OK", null);
        	adb.show();
            cur.close();
            return;
        }
        
// Add by CPJsunagawa '2015-08-06 Start
        na.setSelectedCourseName(getButtonValue(mCarPos));
        
        // 選択されたコースに該当する軌跡データをコピーする
        String szCourseName = na.getSelectedCourseName();
// Add by CPJsunagawa '2015-08-06 End

// Cut 20160118 Start
/*
// Add '2016-01-06 Start
    	// 走行軌跡表示フラグをOFFにする
        NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(0);
// Add '2016-01-06 End
*/
// Cut 20160118 End

        CommonInquiryOps.setDeliveryInfoFromDB(this, cur, mDb, mCarPos+1);

/*        
        int idxCustID = cur.getColumnIndex("CustID");
        int idxCode = cur.getColumnIndex("DeliveryOrder");
        int idxCustomer = cur.getColumnIndex("Customer");
        int idxTelNo = cur.getColumnIndex("TelNo");
        //int idxPref = cur.getColumnIndex("PREF");
        //int idxCity = cur.getColumnIndex("CITY");
        int idxAddress = cur.getColumnIndex("Address");
        int idxMatchLevel = cur.getColumnIndex("MatchLevel");
        int idxChangeFlag = cur.getColumnIndex("ChangeFlag");
        int idxContract = cur.getColumnIndex("Contract");
        int idxSeiyakuDate = cur.getColumnIndex("SeiyakuDate");
        int idxTorihikiDate = cur.getColumnIndex("TorihikiDate");
        int idxKaiyakuDate = cur.getColumnIndex("KaiyakuDate");
        int idxKaiyakuReason = cur.getColumnIndex("KaiyakuReason");
        int idxKaishuKind = cur.getColumnIndex("KaishuKind");
        int idxCollectComment = cur.getColumnIndex("CollectingMemo");
        int idxPoint = cur.getColumnIndex("CustPoint");
        int idxCourse = cur.getColumnIndex("DeliCourse");
        int idxDeliOrder = cur.getColumnIndex("DeliOrder");
        int idxDeliComment = cur.getColumnIndex("DeliComment");
        int idxMapNo = cur.getColumnIndex("MapNo");
        int idxBoxPlace = cur.getColumnIndex("BoxPlace");
        int idxContactingMemo = cur.getColumnIndex("ContactingMemo");
        int idxBirthDay = cur.getColumnIndex("BirthDay");

        //int idxProduct = cur.getColumnIndex("ARTICLE");
        //int idxProductRyaku = cur.getColumnIndex("ARTICLERYAKU");
        //int idxProductCnt = cur.getColumnIndex("ARTICLECOUNT");
        //int idxLastDate = cur.getColumnIndex("LastDate");
        //int idxClaim = cur.getColumnIndex("DELIVERYCOMMENT");
        //int idxWorldLat = cur.getColumnIndex("WorldLat");
        //int idxWorldLon = cur.getColumnIndex("WorldLon");

        //int idxJpnLat = cur.getColumnIndex("LATITUDE");
        //int idxJpnLon = cur.getColumnIndex("LONGITUDE");
        int idxJpnLat = cur.getColumnIndex("Latitude");
        int idxJpnLon = cur.getColumnIndex("Longitude");
        cur.moveToFirst();
        NaviApplication app = (NaviApplication)getApplication();
        app.initRouteList(); // 基本あり得ないが、DBが更新されるケースを考慮して、画面に入る度に作り直す
        app.initInfoList();
        // 開始地点(現在地)
        Intent preIntent = getIntent();
        Bundle bundle = preIntent.getExtras();
        RouteNodeData routeNodeDataNew = new RouteNodeData();
        routeNodeDataNew.setName(getResources().getString(R.string.route_start_point_name));
        routeNodeDataNew.setLon("" + preIntent.getLongExtra(Constants.INTENT_LONGITUDE, 0));
        routeNodeDataNew.setLat("" + preIntent.getLongExtra(Constants.INTENT_LATITUDE, 0));
        routeNodeDataNew.setDate("");
        routeNodeDataNew.setStatus("0");
        routeNodeDataNew.setMode("0");
        app.addRoutePoint(routeNodeDataNew);
        String adrstr;
        double latdeg, londeg;
        
        //'13-09-28 暫定措置 取り急ぎTKYにする。
        GeoPoint tky;
        
        // データ数は6のみを想定
        for(int i =0, len = cur.getCount(); i < len; i++, cur.moveToNext()) {
            routeNodeDataNew = new RouteNodeData();
            routeNodeDataNew.setName(cur.getString(idxAddress));
            
            // ↓ここで止まっている様子　仮でコメント 
            latdeg = cur.getFloat(idxJpnLat);
            londeg = cur.getFloat(idxJpnLon);
            
            //'13-09-28 暫定措置 取り急ぎTKYにする。
            //tky  = GeoUtils.toTokyoDatum(latdeg, londeg);
            //latdeg = tky.getLatitude();
            //londeg = tky.getLongitude();
            
            try{
                //routeNodeDataNew.setLat(new Long( (long)(cur.getFloat(idxJpnLat)* 100000) * 36 ).toString()); // -> msec
                //routeNodeDataNew.setLon(new Long( (long)(cur.getFloat(idxJpnLon)* 100000) * 36 ).toString()); // -> msec
                routeNodeDataNew.setLat(new Long( (long)(latdeg * 100000) * 36 ).toString()); // -> msec
                routeNodeDataNew.setLon(new Long( (long)(londeg * 100000) * 36 ).toString()); // -> msec
            } catch( NumberFormatException e) { // 緯度、経度が変換できないデータは飛ばす
                continue;
            }
            routeNodeDataNew.setDate("");
            routeNodeDataNew.setStatus("0");
            routeNodeDataNew.setMode("0");
            app.addRoutePoint(routeNodeDataNew);
            
            // 属性情報の設定
            DeliveryInfo info = new DeliveryInfo();
            info.mCarNo = mCarPos+1;
            info.mCustCode = cur.getString(idxCode);
            info.mCustID = cur.getInt(idxCustID);
            info.mCustmer = cur.getString(idxCustomer);
            info.mTelNo = cur.getString(idxTelNo);
            //adrstr = cur.getString(idxPref);
            //adrstr += cur.getString(idxCity);
            //adrstr += cur.getString(idxAddress);
            info.mAddress = cur.getString(idxAddress);
            //info.mAddress = adrstr;
            info.mMatchLebel = cur.getInt(idxMatchLevel);
            // 暫定
            info.mCustmer += " - " + info.mMatchLebel;
            info.mChangeFlag = cur.getInt(idxChangeFlag);
            info.mContract = cur.getString(idxContract);
            info.mSeiyakuDate = cur.getString(idxSeiyakuDate);
            info.mTorihikiDate = cur.getString(idxTorihikiDate);
            info.mKaiyakuDate = cur.getString(idxKaiyakuDate);
            info.mKaiyakuReason = cur.getString(idxKaiyakuReason);
            info.mKaishuKind = cur.getString(idxKaishuKind);
            info.mCollectingMemo = cur.getString(idxCollectComment);
            info.mCustPoint = cur.getInt(idxPoint);
            info.mDeliCourse = cur.getString(idxCourse);
            info.mDeliOrder = cur.getInt(idxDeliOrder);
            info.mDeliComment = cur.getString(idxDeliComment);
            info.mMapNo = cur.getString(idxMapNo);
            info.mBoxPlace = cur.getString(idxBoxPlace);
            info.mBirthDay = cur.getString(idxBirthDay);

            //info.mProduct = cur.getString(idxProduct);
            //info.mProductRyaku = cur.getString(idxProductRyaku);
            //info.mProductCnt = cur.getInt(idxProductCnt);

            
            //info.mLastDate = cur.getString(idxLastDate);
            //info.mLastDate = "2013年8月31日";
            //info.mClaim = cur.getInt(idxClaim);
            
           info.mPoint = new GeoPoint(latdeg, londeg);
            
            app.addInfoItem(info);
        }
*/        
        System.out.println("naviStart");
        cur.close();
        // XuYang add start #1056
/*        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            CommonLib.setChangePosFromSearch(true);
            CommonLib.setChangePos(false);
        } else {
            CommonLib.setChangePosFromSearch(false);
            CommonLib.setChangePos(false);
        }
        // XuYang add end #1056
        OpenMap.moveMapTo(VehicleInquiry.this, opoiData, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
        */
        
        
        this.setResult(Constants.RESULTCODE_VEHICLE_ROUTE_DETERMINATION);
        this.finish();
        /*
        Intent intent = getIntent();
        if (null != intent && intent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            CommonLib.setChangePosFromSearch(true);
            CommonLib.setChangePos(false);
        } else {
            CommonLib.setChangePosFromSearch(false);
            CommonLib.setChangePos(false);
        }
        intent.setClass(getApplicationContext(), MapActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);*/
    }

/*
	private void CopyDispOnlyTrackData(String szCourseName) {
		// TODO 自動生成されたメソッド・スタブ
		
		// 選択されたコース名を基に、該当の軌跡データを取得する
        DrawTrackActivity.DbHelper helper = new DrawTrackActivity.DbHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();

        String szSQL = null;
        szSQL = "SELECT * FROM TrackData WHERE CourseName = " + "'" + szCourseName + "';";
		Cursor cur = db.rawQuery(szSQL, null);
        if( cur == null ) {
        	return;
        } 
        if( cur.getCount() != 0 ) {
        	// 表示すべき軌跡データが存在するので、カレントにコピーする
        	int nIndex = cur.getColumnIndex("FileName");
        	String szTrackDataFile = cur.getString(nIndex);
        	
            FileChannel srcChannel = null;
            FileChannel destChannel = null;
            String stdDataFile = null;
            stdDataFile = NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "TMP/VT/TravelTrack.dat";

            try {
                srcChannel = new FileInputStream(szTrackDataFile).getChannel();
                destChannel = new FileOutputStream(stdDataFile).getChannel();
                
                srcChannel.transferTo(0, srcChannel.size(), destChannel);

            } catch (IOException e) {
                e.printStackTrace();

            } finally {
                if (srcChannel != null) {
                    try {
                        srcChannel.close();
                    } catch (IOException e) {
                    }
                }
                if (destChannel != null) {
                    try {
                        destChannel.close();
                    } catch (IOException e) {
                    }
                }
            }
        }
        cur.close();
        db.close();
        helper.close();
	}
*/
    
    // 検索結果へのルート表示
    /*
    void OpenMap(int place_index) {
        // 座標系変換

        long[] coord = CoordTransUtil.degreeToMsec(mPlaces[place_index].latitude, mPlaces[place_index].longitude);
        long latitude = coord[0];
        long longitude = coord[1];
        // 内部データ形式に変換
        coord = CoordTransUtil.wgs2tky(latitude, longitude);

        long internal_latitude =  0;//coord[0];
        long internal_longitude =  0;//coord[1];

        Intent intent = getIntent();
        if (null != intent && intent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            CommonLib.setChangePosFromSearch(true);
            CommonLib.setChangePos(false);
        } else {
            CommonLib.setChangePosFromSearch(false);
            CommonLib.setChangePos(false);
        }
        POIData ozData = new POIData();
        ozData.m_sName = mPlaces[place_index].address;
        ozData.m_sAddress = mPlaces[place_index].address;
        ozData.m_wLat = internal_latitude;
        ozData.m_wLong = internal_longitude;

        intent.setClass(getApplicationContext(), MapActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Intent.FLAG_ACTIVITY_NEW_TASK);
        OpenMap.moveMapTo( this, ozData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
        startActivity(intent);
    }*/
}
