package net.zmap.android.pnd.v2.api.exception;

/**
 * ファイル・メモリ入出力失敗
 */
public class NaviIOException extends NaviException {
    private static final long serialVersionUID = 1L;

    /**
     * エラー詳細メッセージとして null を設定して NaviIOException を構築します。
     */
    public NaviIOException() {
        super();
    }

    /**
     * 詳細メッセージを指定して NaviIOException を構築します。
     *
     * @param message
     *            詳細メッセージ
     */
    public NaviIOException(String message) {
        super(message);
    }
}