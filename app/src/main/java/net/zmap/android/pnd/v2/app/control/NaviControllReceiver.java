package net.zmap.android.pnd.v2.app.control;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.app.NaviAppStatus;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.NaviRun;

/*
 * ナビ制御ブロードキャスト受信クラス
 */
public class NaviControllReceiver extends BroadcastReceiver {
    NaviApplication mApp = null;

    public void setAppInstance(NaviApplication app) {
        mApp = app;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String action = intent.getAction();
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviControllReceiver action=" + action);
            if (action.equals(NaviControllIntent.ACTION_NAVI_APP_FINISH)) {
                if (NaviRun.isNaviRunInitial()) {
                    NaviRun.GetNaviRunObj().finishApp();
                }
            } else if (action.equals(NaviControllIntent.ACTION_MAP_ACTIVITY_START)) {
//Add 2011/10/10 Z01yoneya Start -->
                NaviAppStatus.setAppStatus(NaviAppStatus.APP_STATE_INIT_NAVIENGINE);
//Add 2011/10/10 Z01yoneya End <--
                mApp.initExConfig();
                mApp.startNormal();
            } else if (action.equals(NaviControllIntent.ACTION_MAP_FIRST_DRAW_FINISH)) {
                mApp.finishMainActivity();
//Add 2011/11/16 Z01_h_yamada Start -->
            } else if (action.equals(NaviControllIntent.ACTION_NAVI_APP_MEDIA_SIZE_INSUFFICIENT)) {
                mApp.sendShowDialog(Constants.DIALOG_MEDIA_SIZE_INSUFFICIENT);
//Add 2011/11/16 Z01_h_yamada End <--
            }
        } catch (NullPointerException e) {
            //とくになにもしない
//Add 2011/10/10 Z01yoneya Start -->
        } catch (IllegalStateException e) {
            //例外の発生しない順番で遷移させること
//Add 2011/10/10 Z01yoneya End <--
        }
    }

    public void reg(Context context) {
        if (context == null) {
            return;
        }

        IntentFilter filter = new IntentFilter();
        if (filter != null) {
            filter.addAction(NaviControllIntent.ACTION_NAVI_APP_FINISH);
            filter.addAction(NaviControllIntent.ACTION_MAP_ACTIVITY_START);
            filter.addAction(NaviControllIntent.ACTION_MAP_FIRST_DRAW_FINISH);
//Add 2011/11/16 Z01_h_yamada Start -->
            filter.addAction(NaviControllIntent.ACTION_NAVI_APP_MEDIA_SIZE_INSUFFICIENT);
//Add 2011/11/16 Z01_h_yamada End <--
            context.registerReceiver(this, filter);
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviControllReceiver::reg()");
        }
    }

    public void unreg(Context context) {
//Chg 2011/10/12 Z01yoneya Start -->
//        if (context == null) {
//            return;
//        }
//        context.unregisterReceiver(this);
//------------------------------------------------------------------------------------------------------------------------------------
        try {
            context.unregisterReceiver(this);
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviControllReceiver::unreg()");
        } catch (NullPointerException e) {
            //特に何もしない
        } catch (Exception e) {
            //特に何もしない
        }
//Chg 2011/10/12 Z01yoneya End <--
    }
}
