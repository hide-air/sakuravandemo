package net.zmap.android.pnd.v2.engine;

public class NaviEngineError {

    /*各種ナビエンジン内エラーコード暫定定義*/
    //-- リザルトコード --
    public static final long NE_SUCCESS         = 0x00000000; ///< 成功
    public static final long NE_ERR_PARAM       = 0x10200001; ///< パラメータエラー
    public static final long NE_ERR_MALLOC      = 0x10200002; ///< メモリ確保エラー
    public static final long NE_ERR_WIN32SDK    = 0x10200003; ///< Win32SDKエラー
    public static final long NE_ERR_OVERFLOW    = 0x10200004; ///< オーバーフローエラー
    public static final long NE_ERR_UNDERFLOW   = 0x10200005; ///< アンダーフローエラー
    public static final long NE_ERR_MML         = 0x10200010; ///< GraphicsLibrary 内部エラー
    public static final long NE_ERR_GRL         = 0x10200011; ///< GraphicsLibrary 内部エラー
    public static final long NE_ERR_DAL         = 0x10200012; ///< DataAccessLibrary 内部エラー
    public static final long NE_ERR_POI         = 0x10200013; ///< PointsOfInterest 内部エラー
    public static final long NE_ERR_MP          = 0x10200014; ///< MapEngine 内部エラー
    public static final long NE_ERR_NUI         = 0x10200015; ///< NaviEngineUI 内部エラー
    public static final long NE_ERR_VP          = 0x10200016; ///< VehiclePositioning 内部エラー
    public static final long NE_ERR_WG          = 0x10200017; ///< WalkGuidance 内部エラー
    public static final long NE_ERR_DG          = 0x10200018; ///< DriveGuidance 内部エラー
    public static final long NE_ERR_BG          = 0x10200019; ///< BikeGuidance 内部エラー add by wangmeng
    public static final long NE_ERR_VICS        = 0x10200019; ///< VICS 内部エラー
    public static final long NE_WRN_BUSY        = 0x20200001; ///< ビジー 警告

    //NUI
    public static final long NUI_SUCCESS         = 0x00000000; ///< 成功
    //MP
    public static final long MP_SUCCESS         = 0x00000000; ///< 成功
    //DG
    public static final long DG_SUCCESS         = 0x00000000; ///< 成功
    //BG
    public static final long BG_FAILURE     = 0x10040400;
    public static final long BG_SUCCESS     = 0x10040101;
    //CMN
    public static final long CMN_SUCCESS    = 0x00000000; ///< 成功
    public static final long CT_SUCCESS     = NE_SUCCESS; ///< 成功
    public static final long CT_NONPROC     = 0x00000001; ///< 成功=処理なし;

    /** エンジン側エラーコード */
    public static final long JNI_ERR_OK                = 0;
    public static final long JNI_ERR_NG                = 1;
    public static final long JNI_ERR_ILLEGAL_ARGUMENT  = 1001;
    public static final long JNI_ERR_ILLEGAL_STATE     = 1002;
    public static final long JNI_ERR_OUT_OF_MEMORY     = 1003;
    public static final long JNI_ERR_NULL_POINTER      = 1004;
    public static final long JNI_ERR_OUT_OF_RANGE      = 1005;
    public static final long JNI_ERR_INVALID_VALUE     = 1006;
    public static final long JNI_ERR_NOT_FOUND         = 1007;
    public static final long JNI_ERR_REQUEST_FAILURE   = 1008;
    public static final long JNI_ERR_REMOTE_ERROR      = 1009;
    public static final long JNI_ERR_IO_ERROR          = 1010;
    public static final long JNI_ERR_NAVI_ENGINE_ERROR = 1011;
    public static final long JNI_ERR_SERVICE_ERROR     = 1012;
    public static final long JNI_ERR_UNKNOWN_ERROR     = 1013;

}
