/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           POI_Mybox_ListGenreName.java
 * Description    各ジャンルの名称取得
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class POI_Mybox_ListGenreName {
    private long m_lGenreCode;
    private String m_GenreName;
    /**
    * Created on 2010/08/06
    * Title:       getM_lGenreCode
    * Description:  ジャンルコードを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lGenreCode() {
        return m_lGenreCode;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_GenreName
    * Description:  ジャンル名称を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getM_GenreName() {
        return m_GenreName;
    }
}
