package net.zmap.android.pnd.v2.api.exception;

/**
 * 不正な値、無効値
 */
public class NaviInvalidValueException extends NaviException {
    private static final long serialVersionUID = 1L;

    /**
     * エラー詳細メッセージとして null を設定して NaviInvalidValueException を構築します。
     */
    public NaviInvalidValueException() {
        super();
    }

    /**
     * 詳細メッセージを指定して NaviInvalidValueException を構築します。
     *
     * @param message
     *            詳細メッセージ
     */
    public NaviInvalidValueException(String message) {
        super(message);
    }
}