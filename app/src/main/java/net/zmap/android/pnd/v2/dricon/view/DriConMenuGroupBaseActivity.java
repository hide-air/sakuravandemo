/**
 *  @file		DriConMenuGroupBaseActivity
 *  @brief		ドライブコンテンツのグループ(select、multiselect)を表示する
 *
 *  @attention
 *  @note		ボタンリストや、ON/OFFリストを設定する
 *
 *  @author		Manabu Watanabe [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2011-09-01)
 *  @version	$Revision: $ by $Author: $
 *
 */

package net.zmap.android.pnd.v2.dricon.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.DC_ContentsData;
import net.zmap.android.pnd.v2.data.DC_ContentsItemData;
import net.zmap.android.pnd.v2.data.DC_SectionOptionData;
import net.zmap.android.pnd.v2.dricon.controller.DriverContentsCtrl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DriConMenuGroupBaseActivity extends DriconMenuListBaseActivity {

	private DC_ContentsItemData	m_oContentsItemData = null;
	private DC_ContentsData		m_oContentsData = null;

	private int 				m_nParentIndex	= 0;
	private int 				m_nParentDataIndex	= 0;
	private boolean 			m_bMulti		= false;
	private List<Integer> 		m_displayIndexes = new ArrayList<Integer>();
	private String 				m_strKey       = null;
	private int 				m_nIndex       = -1;
	private Intent				returnIntent  = null;
	private int 				m_nMultiOffset  = 0;
	private boolean				m_bUpdate		= false;

	private Button 				m_oSwitchOnBtn;
	private Button 				m_oSwitchOffBtn;
	private Button 				m_oComboGroupBtn;
	private Button 				m_oSelectBtn;
	private Set<String> 	m_DefaultSet = new HashSet<String>();

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.view.DriconMenuListBaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent oIntent = getIntent();
		if(oIntent != null){
			try{
				m_oContentsItemData	= (DC_ContentsItemData) oIntent.getSerializableExtra(Constants.DC_INTENT_GROUP_CONTENT_EXTRA);
				m_oContentsData = (DC_ContentsData)oIntent.getSerializableExtra(Constants.DC_INTENT_GROUP_PARENT_ITEM_EXTRA);
				m_nParentIndex	= oIntent.getIntExtra(Constants.DC_INTENT_GROUP_PARENT_EXTRA, -1);
				m_strKey		= oIntent.getStringExtra(Constants.DC_INTENT_GROUP_SECTION_KEY_EXTRA);
				m_nIndex		= oIntent.getIntExtra(Constants.DC_INTENT_GROUP_SECTION_ITEM_EXTRA, -1);
				m_nParentDataIndex = oIntent.getIntExtra(Constants.DC_INTENT_GROUP_PARENT_INDEX_EXTRA, -1);
			}
			catch(Exception e)
			{
				NaviLog.d(NaviLog.PRINT_LOG_TAG," Error at bundle " + e.toString());
			}
		}

		if(m_oContentsItemData == null || m_nParentIndex < 0 ){
			m_nCount = 0;
		}else{
			if(m_oContentsItemData.m_sItemDefaultValue != null){
				String[] strList = m_oContentsItemData.m_sItemDefaultValue.split(",");
				for(int i = 0 ; i < strList.length ; i++){
					if(strList[i].length()>0){
						m_DefaultSet.add(strList[i]);
					}
				}
			}
			m_nMultiOffset = 0;
			m_bMulti = (m_oContentsItemData.m_nItemKind == DC_ContentsItemData.ITEM_TYPE_MULTI_SELECT);

			if(m_bMulti){
				if(m_oContentsItemData.m_nAllType != DC_ContentsItemData.MULTI_ALL_TYPE_NOTHING){
					m_nMultiOffset = 2;
				}
			}

			if(m_oContentsItemData.m_OptionList == null){
				m_nCount = 0;
			}else{
				if(m_oContentsItemData.m_OptionList != null){
					for(int k = 0 ; k < m_oContentsItemData.m_OptionList.size(); k++){
						if(m_oContentsItemData.m_OptionList.get(k).m_nParentID == m_nParentIndex){
							m_displayIndexes.add(k);
							m_nCount++;
						}
					}
				}
				setTitleValue(m_oContentsItemData.m_sItemText);
			}
		}
		m_nCount += m_nMultiOffset;

		returnIntent = new Intent();
		setResult(RESULT_CANCELED, returnIntent);

		initDriConMenuLayout();
	}

	/**
	 * @author watanabe
	 * アイテムデータ
	 *
	 */
	private static class ViewItemData{
		public DC_SectionOptionData itemData;
	}

	/**
	 * アイテム情報取得
	 * @param position 指定アイテム位置
	 * @return 取得したアイテム
	 */
	private ViewItemData getItemInfo(int position){
		if(position < 0){
			return null;
		}
		ViewItemData data = new ViewItemData();
		if(m_bMulti && m_nMultiOffset == 2){
			if(position == 1 || position == 0){
				return data;
			}
		}
		if(data != null){
			data.itemData = m_oContentsItemData.m_OptionList.get(m_displayIndexes.get(position - m_nMultiOffset));
		}
		return data;
	}

	/**
	 * アイテム情報を変更する
	 * @param position 変更するアイテムインデックス
	 * @param data 変更情報
	 */
	private void setItemInfo(int position , ViewItemData data){
		if(position < 0){
			return;
		}

		if(data != null && data.itemData != null){
			m_oContentsItemData.m_OptionList.get(m_displayIndexes.get(position - m_nMultiOffset)).m_sOptionValue  = data.itemData.m_sOptionValue;
		}
		m_bUpdate = true;
		return;
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.view.DriconMenuListBaseActivity#getViewList(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getViewList(int postion, View oView, ViewGroup parent) {
		if(m_bMulti){
			if(oView == null)
			{
				LayoutInflater oInflater = LayoutInflater.from(DriConMenuGroupBaseActivity.this);
				m_oLayout = (LinearLayout)oInflater.inflate(R.layout.dricon_group_detail_item, null);
				oView = m_oLayout;
			} else {
				m_oLayout = (LinearLayout)oView;
			}

			final int wPos = postion;
			TextView txtView = null;
			txtView = (TextView)m_oLayout.findViewById(R.id.dc_group_detail_item_text);
			m_oSwitchOnBtn = (Button)m_oLayout.findViewById(R.id.dc_group_detail_item_switchON);
			m_oSwitchOffBtn = (Button)m_oLayout.findViewById(R.id.dc_group_detail_item_switchOFF);
			m_oComboGroupBtn = (Button)m_oLayout.findViewById(R.id.dc_group_detail_item_comboGroup);
		    if(m_oContentsItemData != null){
				final ViewItemData data = getItemInfo(wPos);
				if(data != null){
					m_oSwitchOnBtn.setOnClickListener(new Button.OnClickListener() {
						@Override
						public void onClick(View v){
							int position = wPos - m_nMultiOffset;
							if(position >= 0){
								if(data.itemData != null){
									m_oContentsItemData.m_sItemValueList.add(data.itemData.m_sOptionValue);
								}
							}else{
								if(m_oContentsItemData.m_nAllType != DC_ContentsItemData.MULTI_ALL_TYPE_NOTHING ){
									if(wPos == 0){
										m_oContentsItemData.m_bAllSelect = true;
									}
								}
							}
							setItemInfo(wPos ,data);
							notifyUpdataList();
						}
					});

					m_oSwitchOffBtn.setOnClickListener(new Button.OnClickListener() {
						@Override
						public void onClick(View v){
							int position = wPos - m_nMultiOffset;
							if(position >= 0){
								if(data.itemData != null){
									if(m_oContentsItemData.m_sItemValueList.size() != 0){
										if(m_oContentsItemData.m_sItemValueList.contains(data.itemData.m_sOptionValue)){
											m_oContentsItemData.m_sItemValueList.remove(data.itemData.m_sOptionValue);
										}
									}
								}
							}else{
								if(m_oContentsItemData.m_nAllType != DC_ContentsItemData.MULTI_ALL_TYPE_NOTHING ){
									if(wPos == 0){
										m_oContentsItemData.m_bAllSelect = false;
									}
								}
							}
							setItemInfo(wPos ,data);
							notifyUpdataList();
						}
					});

					final ViewItemData viewData = getItemInfo(wPos);
					if(viewData != null){
						if(m_nMultiOffset == 2){
							if(wPos == 1){
								txtView.setVisibility(Button.INVISIBLE);
								m_oSwitchOnBtn.setVisibility(Button.INVISIBLE);
								m_oSwitchOffBtn.setVisibility(Button.INVISIBLE);
								m_oComboGroupBtn.setVisibility(Button.INVISIBLE);
								m_oComboGroupBtn.setVisibility(Button.INVISIBLE);
								return oView;
							}else if(wPos == 0){
								txtView.setText(getResources().getString(R.string.dc_search_info_multi_all));
							}
						}
						txtView.setVisibility(Button.VISIBLE);
						m_oSwitchOnBtn.setVisibility(Button.VISIBLE);
						m_oSwitchOffBtn.setVisibility(Button.VISIBLE);
						if(viewData.itemData != null){
							txtView.setText(viewData.itemData.m_sOptionText);
							if(viewData.itemData.m_sOptionValue == null){
								m_oSwitchOnBtn.setVisibility(Button.INVISIBLE);
								m_oSwitchOffBtn.setVisibility(Button.INVISIBLE);
								m_oComboGroupBtn.setVisibility(Button.VISIBLE);
								m_oComboGroupBtn.setOnClickListener(new Button.OnClickListener() {
									@Override
									public void onClick(View v){
										goComboGroup(viewData);
									}
								});
							}
							if(m_oContentsItemData.m_bAllSelect){
								m_oSwitchOnBtn.setBackgroundResource(R.drawable.btn_default_non);
								m_oSwitchOnBtn.setTextColor(getResources().getColor(R.color.switch_off));
								m_oSwitchOnBtn.setClickable(false);
								m_oSwitchOffBtn.setBackgroundResource(R.drawable.btn_default_non);
								m_oSwitchOffBtn.setTextColor(getResources().getColor(R.color.switch_off));
								m_oSwitchOffBtn.setClickable(false);
							}else{
								if(m_oContentsItemData.m_sItemValueList.contains(data.itemData.m_sOptionValue)){
									selectSwBtn(m_oSwitchOnBtn, m_oSwitchOffBtn, ON);
								}else{
									selectSwBtn(m_oSwitchOnBtn, m_oSwitchOffBtn, OFF);
								}
							}
						}else{
							if(m_oContentsItemData.m_bAllSelect){
								selectSwBtn(m_oSwitchOnBtn, m_oSwitchOffBtn, ON);
							}else{
								selectSwBtn(m_oSwitchOnBtn, m_oSwitchOffBtn, OFF);
							}
						}
					}
				}
		    }
		}else{
			if(oView == null)
			{
				LayoutInflater oInflater = LayoutInflater.from(DriConMenuGroupBaseActivity.this);
				m_oLayout = (LinearLayout)oInflater.inflate(R.layout.dricon_group_detail_select_item, null);
				oView = m_oLayout;
			} else {
				m_oLayout = (LinearLayout)oView;
			}

			final int wPos = postion;
			m_oSelectBtn = (Button)m_oLayout.findViewById(R.id.dc_group_detail_item_all_btn);
		    if(m_oContentsItemData != null){
				final ViewItemData data = getItemInfo(wPos);
				if(data != null){
					m_oSelectBtn.setOnClickListener(new Button.OnClickListener() {
						public void onClick(View v){
							m_oContentsItemData.m_sItemValueList.clear();
							m_oContentsItemData.m_sItemValueList.add(data.itemData.m_sOptionValue);
							setItemInfo(wPos ,data);
							notifyUpdataList();
							}
					});

					m_oSelectBtn.setText(data.itemData.m_sOptionText);
					m_oSelectBtn.setVisibility(Button.VISIBLE);
					if(m_oContentsItemData.m_sItemValueList.contains(data.itemData.m_sOptionValue)){
						m_oSelectBtn.setBackgroundResource(R.drawable.btn_default_select);
						m_oSelectBtn.setTextColor(getResources().getColor(R.color.switch_on));
					}else{
						m_oSelectBtn.setBackgroundResource(R.drawable.btn_default);
						m_oSelectBtn.setTextColor(getResources().getColorStateList(R.color.button_color));
					}
				}
		    }
		}
		return oView;
	}

	/**
	 * コンテンツの変更情報を更新する
	 */
	private void updContentsData(){
		if(!m_bUpdate){
			return;
		}

		Map<String , List<DC_ContentsItemData>> map = m_oContentsData.m_mapSectionItemList;
		if(map != null){
            ArrayList<DC_ContentsItemData> value = (ArrayList<DC_ContentsItemData>) map.get(m_strKey);
            if(value != null){
            	value.set(m_nIndex, m_oContentsItemData);
            	m_oContentsData.m_mapSectionItemList.put(m_strKey, value);
            	m_oContentsDataList.remove(m_nParentDataIndex);
            	m_oContentsDataList.add( m_nParentDataIndex,m_oContentsData );
            	notifyUpdataList();
            }
		}

    	DriverContentsCtrl.getController().setContentsDataList(m_oContentsDataList);
    	DriverContentsCtrl.getController().saveSettingFile();
		DriverContentsCtrl.getController().notifyDialogDataChanged();
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.common.activity.MenuBaseActivity#goBack()
	 */
	public void goBack(){
		updContentsData();
		super.goBack();
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.view.DriconMenuListBaseActivity#goMap()
	 */
	public void goMap(){
		updContentsData();
	    super.goMap();
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.common.activity.MenuBaseActivity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
    		updContentsData();
        }
        return super.onKeyDown(keyCode, event);
    }

	/**
	 * グループからグループへ移行する
	 * @param data 移行した先で表示するデータ
	 * @note 現在未使用
	 */
	public void goComboGroup(ViewItemData data){
		if(data != null){
			Intent intent = new Intent();
			intent.putExtra(Constants.DC_INTENT_GROUP_CONTENT_EXTRA, m_oContentsItemData);
			intent.putExtra(Constants.DC_INTENT_GROUP_PARENT_EXTRA, data.itemData.m_nChildID);
			intent.putExtra(Constants.DC_INTENT_GROUP_SECTION_KEY_EXTRA, m_strKey);
			intent.putExtra(Constants.DC_INTENT_GROUP_SECTION_ITEM_EXTRA, m_nIndex);

			intent.setClass(this, DriConMenuGroupBaseActivity.class);
			NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_DRICON_SETTING_GROUP);
		}
	}

}
