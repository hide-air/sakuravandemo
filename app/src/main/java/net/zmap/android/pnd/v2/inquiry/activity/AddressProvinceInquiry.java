//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Address_ListItem;
import net.zmap.android.pnd.v2.inquiry.view.ButtonImg;

/**
 * K-A0_都道府県一覧
 * */
public class AddressProvinceInquiry extends ProvinceInquiry {

	private JNILong Index = new JNILong();
	/** JNIから、取得した全部都道府県内容 */
	private POI_Address_ListItem[] m_Poi_Add_Listitem = null;
	/** 表示した都道府県の内容 */
	private POI_Address_ListItem[] m_Poi_Add_Data = null;
	private JNILong Treeindex = new JNILong();
	/** NaviRunに使用した対象 */
	private static AddressProvinceInquiry instance;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//最初にクリア処理を行う
		NaviRun.GetNaviRunObj().JNI_NE_POIAddrKey_Clear();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_Clear();
		super.onCreate(savedInstanceState);
		instance = this;

	}
	@Override
	protected void initCountryBtn() {
		//全国ボタンが不表示とする
		super.AllCountry.setVisibility(Button.GONE);
	}
	/** 本クラスを取得する */
	public static AddressProvinceInquiry getInstance() {
		return instance;
	}
	@Override
	protected void onProvinceclick(int provinceIndex, View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 住所 都道府県禁止 Start -->
	if(DrivingRegulation.CheckDrivingRegulation()){
		return;
	}
// ADD 2013.08.08 M.Honma 走行規制 住所 都道府県禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
		ButtonImg obtn = (ButtonImg) oView;

//Add 2012/03/23 Z01katsuta  Start --> ref 4034
		POI_Address_ListItem[] Listitem =	 new POI_Address_ListItem[1];
//Add 2012/03/23 Z01katsuta  End <-- ref 4034

//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onProvinceclick");
		iProvinceId = m_Poi_Add_Data[provinceIndex].getM_iOldIndex();
		Intent oIntent = getIntent();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"iProvinceId=========" + iProvinceId);

//Add 2012/03/23 Z01katsuta  Start --> ref 4034
		// 選択したレコードを1レコードだけ読み込む
		// ※POIに選択した都道府県名を保持させるため
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecList(iProvinceId, 1, Listitem, Rec);
//Add 2012/03/23 Z01katsuta  End <-- ref 4034

		oIntent.putExtra(Constants.PARAMS_PROVINCE_ID, iProvinceId);
		oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, provinceIndex);
		oIntent.putExtra(Constants.PARAMS_MYPOS_CITY_NAME, super.ms2Str);
		//戻る処理に使える情報を準備する
		String[] list = new String [STRINGLEN];
		list[INDEX_0] = String.valueOf(m_Poi_Add_Data[provinceIndex].getM_lLongitude());
		list[INDEX_1] = String.valueOf(m_Poi_Add_Data[provinceIndex].getM_lLatitude());
		list[INDEX_2] = m_Poi_Add_Data[provinceIndex].getM_Name();
		hashSelectedPoi.add(list);

		String title = obtn.getText().toString();
		oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
		oIntent.putExtra(Constants.PARAMS_IS_SHOW_BUTTON, false);
		oIntent.setClass(this, AddressCityInquiry.class);
		//RDBへ移植してから、スレッド部分を削除したから、直接に次処理をコールする
// 		NaviRun.GetNaviRunObj();
// 		NaviRun.setiActivityId(AppInfo.ID_ACTIVITY_ADDRESSCITYINQUIRY);
		NaviRun.GetNaviRunObj().setSearchKind(AppInfo.ID_ACTIVITY_ADDRESSCITYINQUIRY);
//Chg 2011/09/23 Z01yoneya Start -->
//		this.startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_ADDRESSCITYINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
		NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_ADDRESSCITYINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
	}


	@Override
	protected void setSearchValuetoJNI(short WideCode, short MiddleCode,
			short NarrowCode) {
		long lTreeRecIndex = 0;

		//リストの数
//		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecCount(ListCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"758 ====lTreeRecIndex==" + lTreeRecIndex);
		//次階段のIndexを取得する
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetNextTreeIndex(lTreeRecIndex, Treeindex);
		NaviLog.d(NaviLog.PRINT_LOG_TAG,"760 ====Treeindex==" + Treeindex.lcount);
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SetAddrKey(WideCode, MiddleCode, NarrowCode);
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchNextList(lTreeRecIndex);
		NaviLog.d(NaviLog.PRINT_LOG_TAG,"763 ====lTreeRecIndex==" + lTreeRecIndex);
		Intent oIntent = getIntent();
		oIntent.putExtra(Constants.PARAMS_TREERECINDEX, lTreeRecIndex);
		oIntent.putExtra(Constants.PARAMS_WIDECODE, WideCode);
		oIntent.putExtra(Constants.PARAMS_MIDDLECODE, MiddleCode);
		oIntent.putExtra(Constants.PARAMS_NARROWCODE, NarrowCode);
		//RDBへ移植してから、スレッド部分を削除したから、直接に次処理をコールする
		//search();

	}

	@Override
	protected void initShowProvinceValue(int startIndex, int count) {
		int j = 0;
		int dataCount = getCount()*2;
		//設定ファイルから取得した都道府県の名称をオブジェクトにインストールする
		for (int i = startIndex ; i < startIndex + count ; i++) {
			if (i <= dataCount && i < province.length) {

				m_Poi_Add_Data[j].setM_Name(province[i]);

			} else {
				m_Poi_Add_Data[j].setM_Name("");
			}
			j++;
		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"startIndex===" + startIndex);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"count===" + count);


	}

	protected int getFirstShowIndex() {
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_AddrIndex(Index);//

//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"Index.lcount====" + Index.lcount);
		//行のIndexを取得する
		int iLineIndex = (int)Index.lcount/2;
//		int pageIndex  = (int)Index.lcount/(getCountInBox()* 2);
//		int iReturn = (int)Index.lcount;

//		int iReturn = (int)getCountInBox() * pageIndex ;

// Chg 2011/10/12 Z01kkubo Start --> 課題No.127 佐賀県,長崎県に自車がある時の都道府県リスト表示不正
		//if (iLineIndex > this.getCount()-5) {
		//	iLineIndex = this.getCount()-this.getCountInBox();
		//-----------------------------------------------------------------------------------------
		// リスト最下段から数えて3段分だけ特別対応。リスト最下段を基準(iLineIndex)とする
		if (iLineIndex > this.getCount()-4) {
			iLineIndex = this.getCount();
// Chg 2011/10/12 Z01kkubo End <--
		} else if (iLineIndex > 2) {
			//現在地が、北海道、青森県、岩手県、宮城県以外の場合
			iLineIndex -= 2;
		} else {
			iLineIndex = 0;
		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"getCount====" + getCount());
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"iLineIndex====" + iLineIndex);
//		oBox.setIndex(iLineIndex);
		return iLineIndex;
	}

	@Override
	protected void search() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"1392 ====Index==" + Index.lcount);

//		oBox.setM_wIndex((int)Index.lcount);
		ListCount = new JNILong();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecCount(ListCount);

		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetRecList(0,
				50, m_Poi_Add_Listitem, Rec);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"25====" + m_Poi_Add_Listitem[25].getM_Name());
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"5====" + m_Poi_Add_Listitem[5].getM_Name());
//		oBox.refresh();
	}

	@Override
	protected String getButtonValue(int dataIndex) {
		if (dataIndex < m_Poi_Add_Data.length && m_Poi_Add_Data[dataIndex] != null) {
			return (String) m_Poi_Add_Data[dataIndex].getM_Name();
		} else {
			return "";
		}
	}

	@Override
	protected int checkHasData(String text, int dataIndex) {
		int bResult = -1;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"checkHasData==5==="  + m_Poi_Add_Listitem1111[5].getM_Name());
		if (null != m_Poi_Add_Listitem) {
			int len = m_Poi_Add_Listitem.length;
			for (int i = 0 ; i < len; i++) {
				if (null != m_Poi_Add_Listitem[i].getM_Name() &&
						!AppInfo.isEmpty(m_Poi_Add_Listitem[i].getM_Name()) &&
						m_Poi_Add_Listitem[i].getM_Name().equals(text)) {
					bResult = i;
					break;
				}
			}

			if (-1 != bResult) {
				m_Poi_Add_Data[dataIndex].setM_Name(m_Poi_Add_Listitem[bResult].getM_Name());
				m_Poi_Add_Data[dataIndex].setM_iOldIndex(bResult);
			}
		}
		return bResult;
	}

	@Override
	protected int getM_iOldIndex(int dataIndex) {

		return m_Poi_Add_Data[dataIndex].getM_iOldIndex();
	}
	@Override
	protected void initDataObj() {
		m_Poi_Add_Listitem = new POI_Address_ListItem[50];
		int len = m_Poi_Add_Listitem.length;
		for (int i = 0 ; i < len ; i++) {
			m_Poi_Add_Listitem[i] = new POI_Address_ListItem();
		}

//		m_Poi_Add_Data = new POI_Address_ListItem[getCountInBox()*2];
//		for (int i = 0 ; i < getCountInBox()*2 ; i++) {
//			m_Poi_Add_Data[i] = new POI_Address_ListItem();
//		}
		m_Poi_Add_Data = new POI_Address_ListItem[50];
		len = m_Poi_Add_Data.length;
		for (int i = 0 ; i < len ; i++) {
			m_Poi_Add_Data[i] = new POI_Address_ListItem();
		}


	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			NaviRun.GetNaviRunObj().JNI_NE_POIAddrKey_Clear();
			NaviRun.GetNaviRunObj().JNI_NE_POIAddr_Clear();
			//freescroll mod
//			oBox.refresh();
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected boolean onClickGoMap() {
//		if (Treeindex.lcount == 1) {
//			NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetPrevTreeIndex(Treeindex);
//			NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchPrevList();
//		}
		hashSelectedPoi.clear();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddrKey_Clear();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_Clear();
		return super.onClickGoMap();
	}

	@Override
	protected boolean onClickGoBack() {

//		hashSelectedPoi.remove(hashSelectedPoi.size()-1);
		hashSelectedPoi.clear();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetPrevTreeIndex(Treeindex);
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchPrevList();
		if (Treeindex.lcount == 1) {
			NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetPrevTreeIndex(Treeindex);
			NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchPrevList();
		}

		NaviRun.GetNaviRunObj().JNI_NE_POIAddrKey_Clear();
		NaviRun.GetNaviRunObj().JNI_NE_POIAddr_Clear();
		return super.onClickGoBack();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent oIntent) {
		if (resultCode == Activity.RESULT_CANCELED) {
			if (requestCode == AppInfo.ID_ACTIVITY_ADDRESSCITYINQUIRY) {
				JNILong Treeindex = new JNILong();
				hashSelectedPoi.remove(hashSelectedPoi.size()-1);
				NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetPrevTreeIndex(Treeindex);
				NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchPrevList();

			}
		}
// Add by CPJsunagawa '13-12-25 Start
		//else if (resultCode == Constants.RESULTCODE_VEHICLE_ROUTE_DETERMINATION)
		else if (resultCode == Constants.RESULTCODE_CHECK_PLACE)
		{
			this.setResult(resultCode, oIntent);
			this.finish();
		}
// Add by CPJsunagawa '13-12-25 End

		super.onActivityResult(requestCode, resultCode, oIntent);
	}


}
