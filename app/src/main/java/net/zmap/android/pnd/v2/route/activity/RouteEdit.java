/**
 ********************************************************************
 * Copyright (c) 2010.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project        MarketV2
 * File           RouteEdit.java
 * Description    ルート編集画面
 * Created on     2010/11/24
 *
 ********************************************************************
 */

package net.zmap.android.pnd.v2.route.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.RouteDataFile;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.services.PreferencesService;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.common.view.SwitchGroup;
//ADD 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 Start -->
import net.zmap.android.pnd.v2.common.view.SwitchGroupEx;
//ADD 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 End <--
import net.zmap.android.pnd.v2.common.view.SwitchGroupListener;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.inquiry.activity.SearchMainMenuActivity;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.maps.OpenMap;
import net.zmap.android.pnd.v2.route.data.AddBean;
import net.zmap.android.pnd.v2.route.data.RouteData;
import net.zmap.android.pnd.v2.route.data.RouteNodeData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2010/11/29
 * <p>
 * Title: 保存/新規/現在ルート編集画面
 * <p>
 * Description
 * <p>
 * author　XuYang version 1.0
 */
public class RouteEdit extends InquiryBaseLoading {
    private JNIString Address = new JNIString();
    private JNITwoLong Coordinate = new JNITwoLong();
    private List<RouteNodeData> lstRouteNodeDate = new ArrayList<RouteNodeData>();
    // xuyang add start bug1748
    private List<RouteNodeData> lstRouteNodeDateOldSave = null;
    // xuyang add end bug1748
    // xuyang add start bug1748_2
    private String SaveFirstName = null;
    // xuyang add end bug1748_2
    // xuyang add start #1114
    // 保存のルート内容 または 現在のルート
    private List<RouteNodeData> lstRouteNodeDateSave = new ArrayList<RouteNodeData>();
    // xuyang add end #1114

    //hangeng add start bug1696
    private PreferencesService oPre;
    //hangeng add end bug1696

    private Bundle bundle = null;
    private Intent preIntent = null;
    private String routeType = null;
    private List<AddBean> lstAddBean = null;
    private ScrollList oBox = null;
    private String filePath = "";
    private String strMode = null; // 探索 モード
    private String strSearchProperty = null; // 探索 条件
    private static final String CarMode = "0"; // 車モード
    private static final String ManMode = "1"; // 徒歩モード
    private static final String CarModeSearchRoute1 = "1"; // ルート探索条件（車）推奨
    private static final String CarModeSearchRoute2 = "2"; // ルート探索条件（車）距離
    private static final String CarModeSearchRoute3 = "3"; // ルート探索条件（車）一般
    private static final String CarModeSearchRoute4 = "4"; // ルート探索条件（車）道幅
    private static final String CarModeSearchRoute5 = "5"; // ルート探索条件（車）別ﾙｰﾄ
    private static final String ManModeSearchRoute1 = "0"; // ルート探索条件（徒歩）推奨
    private static final String ManModeSearchRoute2 = "1"; // ルート探索条件（徒歩）推奨
    private static final String ManModeSearchRoute3 = "2"; // ルート探索条件（徒歩）推奨
    private static final int NE_CarNaviSearchProperty_Commend = 1;// 推奨   画面位置index=0
    private static final int NE_CarNaviSearchProperty_General = 2; // 一般  画面位置index=2
    private static final int NE_CarNaviSearchProperty_Distance = 3;// 距離  画面位置index=1
    private static final int NE_CarNaviSearchProperty_Width = 4; // 道幅  画面位置index=3
    private static final int NE_CarNaviSearchProperty_Another = 5; // 別ﾙｰﾄ  画面位置index=4

    private Button btnSave = null;
    private Button btnDetermine = null;
    private SwitchGroup btnMode = null;
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
 // CHG 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 Start -->
    //private SwitchGroup btnModeSearch = null;
    private SwitchGroupEx btnModeSearch = null;
 // CHG 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
    private SwitchGroup btnModeManSearch = null;
    public int addItemIndex = 0;
    public int modifyItemIndex = 0;
    // xuyang add #1114 start
    public boolean addItemFlag = false;
    public boolean modifyItemFlag = false;
    // xuyang add #1114 end
    private ScrollTool oTool = null;
    private static final int DIALOG_SAVE = 1;
    private static final int DIALOG_ROUTE_FAILURE = 3;
    private static final int DIALOG_WALK_ROUTE_TOO_LONG = 4;
//Add 2011/12/15 Z01_h_yamada Start -->
    private static final int DIALOG_ROUTE_SAVE_OK = 5;
    /** 経由地追加不可 */
    private static final int DIALOG_NOT_ADD_ROUTE = 6;

    //Add 2011/12/15 Z01_h_yamada End <--
    private int ROUTE_SAVE_MAX_COUNT = 10; // ルート保存の最大件数
    private String routeName = null;
    // #875 XuYang add start
//	private String strAddressString = Constants.NotFromSearch; // address name
    // #875 XuYang add end
    private int listCount = 14;
    // #905 XuYang add start
    private boolean firstInitFlag = true;
    // #905 XuYang add end
    // xuyang add start #980
    private boolean addButtonFromAddflag = false;
    // xuyang add end #980
    /**
     * 初期化処理
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // xuyang add start #1114
        addItemFlag = false;
        modifyItemFlag = false;
        // xuyang add end #1114
        // xuyang add start #1114
        addItemFlag = false;
        modifyItemFlag = false;
        // xuyang add end #1114
//Chg 2011/10/11 Z01yoneya Start -->
//        filePath = ((NaviApplication)getApplication()).getNaviAppDataPath().getRouteDataXmlFileFullPath();
//------------------------------------------------------------------------------------------------------------------------------------
        NaviAppDataPath appPath = null;
        try {
            appPath = ((NaviApplication)getApplication()).getNaviAppDataPath();
            filePath = appPath.getRouteDataXmlFileFullPath();
        } catch (NullPointerException e) {
            filePath = null;
        }
//Chg 2011/10/11 Z01yoneya End <--
        // 画面を追加

        LayoutInflater oInflater = LayoutInflater.from(this);
        LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.route_edit, null);

        //歩行者データが無い時は、歩行者ボタンを表示しない
//Chg 2011/10/11 Z01yoneya Start -->
//        if (!((NaviApplication)getApplication()).getNaviAppDataPath().isExistWalkData()) {
//------------------------------------------------------------------------------------------------------------------------------------
        boolean isExistWalkData;
        try {
            isExistWalkData = appPath.isExistWalkData();
        } catch (NullPointerException e) {
            isExistWalkData = false;
        }
        if (!isExistWalkData) {
//Chg 2011/10/11 Z01yoneya End <--
            View btnMode = oLayout.findViewById(R.id.btnMode);
            if (btnMode != null) {
                btnMode.setVisibility(View.GONE);
            }
        }

        // 「決定」ボタン
        btnDetermine = (Button)oLayout.findViewById(R.id.btnDetermine);
        btnDetermine.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 保存/新規/現在ルート編集 決定禁止 Start -->
            	if(DrivingRegulation.CheckDrivingRegulation()){
            		return;
            	}
// ADD 2013.08.08 M.Honma 走行規制 保存/新規/現在ルート編集 決定禁止 <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
                // #940 XuYang start
                long routeDist = 0;
                for (int i = 0; i < lstRouteNodeDate.size() - 1; i++) {
                    long startPointLon = Long.parseLong(lstRouteNodeDate.get(i).getLon());
                    long startPointLat = Long.parseLong(lstRouteNodeDate.get(i).getLat());
                    long endPointLon = Long.parseLong(lstRouteNodeDate.get(i + 1).getLon());
                    long endPointLat = Long.parseLong(lstRouteNodeDate.get(i + 1).getLat());
                    long routeDistTemp = CommonLib.calcDist(startPointLon, startPointLat, endPointLon, endPointLat);
                    routeDist = routeDist + routeDistTemp;
                }
                // #940 XuYang start
                if (strMode.equals(ManMode)) {
//Chg 2011/10/11 Z01yoneya Start -->
//                    if (((NaviApplication)getApplication()).getNaviAppDataPath().isExistWalkData()) {
//------------------------------------------------------------------------------------------------------------------------------------
                    boolean isExistWalkData;
                    try {
                        isExistWalkData = ((NaviApplication)getApplication()).getNaviAppDataPath().isExistWalkData();
                    } catch (NullPointerException e) {
                        isExistWalkData = false;
                    }
                    if (isExistWalkData) {
//Chg 2011/10/11 Z01yoneya End <--
//Chg 2011/11/11 Z01_h_yamada Start -->
//                        if (routeDist < Constants.NAVI_HOME_WALK_BIKE_ROUTE_MIN_DIST) {
//                            showDialog(DIALOG_ROUTE_FAILURE);
//                        } else if (routeDist > Constants.MAX_WALK_ROUTE_DIST) {
//                            showDialog(DIALOG_WALK_ROUTE_TOO_LONG);
//                        } else {
//                            determineRoute();
//                        }
//--------------------------------------------
                        if (routeDist > Constants.MAX_WALK_ROUTE_DIST) {
                            showDialog(DIALOG_WALK_ROUTE_TOO_LONG);
                        } else {
                            determineRoute();
                        }
//Chg 2011/11/11 Z01_h_yamada End <--


                    } else {
                        showDialog(Constants.DIALOG_NO_WALKER_DATA);
                    }
                    // #940 XuYang start
                } else {
//Chg 2011/11/11 Z01_h_yamada Start -->
//                    if (routeDist < Constants.NAVI_HOME_CAR_ROUTE_MIN_DIST) {
//                        showDialog(DIALOG_ROUTE_FAILURE);
//                    } else {
//                        determineRoute();
//                    }
//--------------------------------------------
                    determineRoute();
//Chg 2011/11/11 Z01_h_yamada End <--
                }
                // #940 XuYang end
            }

        });

        // 保存ボタン
        btnSave = (Button)oLayout.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
                // TODO Auto-generated method stub
// ADD 2013.08.08 M.Honma 走行規制 保存/新規/現在ルート編集 保存禁止 Start -->
            	if(DrivingRegulation.CheckDrivingRegulation()){
            		return;
            	}
// ADD 2013.08.08 M.Honma 走行規制 保存/新規/現在ルート編集 保存禁止 <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
                saveRoute();
            }
        });
        // モード切り替える
        btnMode = (SwitchGroup)oLayout.findViewById(R.id.btnMode);
        btnMode.setListener(new SwitchGroupListener() {

            @Override
            public void onSelectChange(int wSelect)
            {
                switch (wSelect) {
                    case 0:
                    strMode = CarMode;
                    modeChange(strMode);
                    break;
                case 1:
                    strMode = ManMode;
                    modeChange(strMode);
                    break;
            }
        }
        });

        // 前画面データを取得
        preIntent = getIntent();
        bundle = preIntent.getExtras();
        if ((null != bundle.get(Constants.ROUTE_TYPE_KEY))) {
            // #875 XuYang add start
//		    strAddressString = (String) bundle.get(Constants.AddressString);
            // #875 XuYang end start
            routeType = (String)bundle.get(Constants.ROUTE_TYPE_KEY);
            // ルートデータの初期化
            InitRouteDate();
            // モードの初期化
            InitMode();
        }
        // ルート編集区
        oBox = (ScrollList)oLayout.findViewById(R.id.scrollRouteBox);

        oBox.setAdapter(new BaseAdapter() {

            @Override
            public int getCount() {
                return listCount;
            }

            @Override
            public Object getItem(int arg0) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return routeDataGetView(position, convertView);
            }

            @Override
            public boolean isEnabled(int position) {
                return false;
            }

            @Override
            public boolean areAllItemsEnabled() {
                return false;
            }

        });
//Del 2011/10/06 Z01_h_yamada Start -->
//        oBox.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--

        oTool = new ScrollTool(this);
        oTool.bindView(oBox);
        setViewInOperArea(oTool);
        setViewInWorkArea(oLayout);

    }

    /**
     * ルートデータを初期化処理
     */
    private void InitRouteDate() {
        if (routeType.equals(Constants.ROUTE_TYPE_NEW)) {
//Del 2011/09/17 Z01_h_yamada Start -->
//			setMenuTitle(R.drawable.title_new_route);
//Del 2011/09/17 Z01_h_yamada End <--
            // 新規ルートの場合、ルートデートを取得
            NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);
            NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
                    Coordinate.getM_lLong(), Coordinate.getM_lLat(), Address);
            //int newRoute = 0;
            //String strAddressName = Address.getM_StrAddressString();
            String strLon = String.valueOf(Coordinate.getM_lLong());
            String strLat = String.valueOf(Coordinate.getM_lLat());
            RouteNodeData routeNodeDataNew = new RouteNodeData();
            routeNodeDataNew.setName("[S] " + getResources().getString(R.string.route_start_point_name));
            routeNodeDataNew.setLat(strLat);
            routeNodeDataNew.setLon(strLon);
            routeNodeDataNew.setDate("");
            routeNodeDataNew.setStatus("0");
            routeNodeDataNew.setMode("0");
            lstRouteNodeDate.add(routeNodeDataNew);

        } else if (routeType.equals(Constants.ROUTE_TYPE_NOW)) {
//Del 2011/09/17 Z01_h_yamada Start -->
//			setMenuTitle(R.drawable.title_now_route);
//Del 2011/09/17 Z01_h_yamada End <--
            // 現在ルートの場合、ルートデートを取得
            getRouteInfo(Constants.NAVI_START_INDEX);
            for (int i = Constants.NAVI_VIA_INDEX; i <= Constants.NAVI_DEST_INDEX; i++) {
                getRouteInfo(i);
            }
            // xuyang add start #1114
            lstRouteNodeDateSave = new ArrayList<RouteNodeData>();
            for (int i = 0; i < lstRouteNodeDate.size(); i++) {
                lstRouteNodeDateSave.add(i, lstRouteNodeDate.get(i));
            }
            // xuyang add end #1114
            // xuyang add start #813
            routeSearchFrom();
            // xuyang add end #813
        } else if (routeType.equals(Constants.ROUTE_TYPE_EDIT)) {
//Del 2011/09/17 Z01_h_yamada Start -->
//			setMenuTitle(R.drawable.title_save_route);
//Del 2011/09/17 Z01_h_yamada End <--
            // 保存のルートの場合、ルートデートを取得
            String routeDate = (String)bundle.get("ItemData");
            lstRouteNodeDateOldSave = new ArrayList<RouteNodeData>();

            RouteDataFile routeDataFile = new RouteDataFile();
            try {
                List<RouteData> routeDataList = routeDataFile.readData(filePath);
                RouteData data = routeDataFile.getRouteData(routeDataList, routeDate);
                lstRouteNodeDate = data.getRouteNodeDataList();
            } catch (IllegalStateException e) {
                lstRouteNodeDate.clear();
            }
            RouteNodeData RNodedata =null;
            int size = lstRouteNodeDate.size();
            for (int i = 0; i < size; i++) {
                RNodedata = lstRouteNodeDate.get(i);
                lstRouteNodeDateOldSave.add(RNodedata);
            }
        }

        lstAddBean = getAdapterList(lstRouteNodeDate);
    }

    // xuyang add start #813
    private void routeSearchFrom() {
        boolean routeTypeRouteSearch = bundle.getBoolean(Constants.PARAMS_ROUTE_MOD_FLAG, false);
        if (routeTypeRouteSearch) {
            RouteNodeData routeNodeDataNew = new RouteNodeData();
            routeNodeDataNew.setName(bundle.getString(Constants.POINT_NAME));
            routeNodeDataNew.setLon("" + preIntent.getLongExtra(Constants.INTENT_LONGITUDE, 0));
            routeNodeDataNew.setLat("" + preIntent.getLongExtra(Constants.INTENT_LATITUDE, 0));
            routeNodeDataNew.setDate("");
            routeNodeDataNew.setStatus("0");
            routeNodeDataNew.setMode("0");
            int addIndex = 0;
            if (lstRouteNodeDate.size() == 7) {
            	showDialog(DIALOG_NOT_ADD_ROUTE);
            } else {
                addIndex = lstRouteNodeDate.size() - 1;
                lstRouteNodeDate.add(addIndex, routeNodeDataNew);
            }
        }
    }

//Add 2011/10/04 Z01yoneya Start -->
    private void updateDataOnEdit(String routeDate) {
        boolean bSuccessRead = false;
        try {
            RouteDataFile routeDataFile = new RouteDataFile();
            List<RouteData> routeDataList = routeDataFile.readData(filePath);
            RouteData data = routeDataFile.getRouteData(routeDataList, routeDate);
            btnMode.setSelected(data.getRouteMode());
            strMode = data.getRouteMode().toString();
            strSearchProperty = data.getSearchProperty().toString();
            bSuccessRead = true;
        } catch (IllegalStateException e) {
            bSuccessRead = false;
        } catch (IndexOutOfBoundsException e) {
            bSuccessRead = false;
        }

        //なんらかの理由でファイルを読み込めない場合は、
        //初期値相当をいれる
        if (!bSuccessRead) {
            strMode = "0"; //車
            btnMode.setSelected(Integer.parseInt(strMode));
            strSearchProperty = "1"; //推奨
        }
    }

//Add 2011/10/04 Z01yoneya End <--
    // xuyang add end #813
    /**
     * モードの初期化
     *
     */
    private void InitMode() {
        // システムモードを取得
        JNIInt JIMode = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetNaviMode(JIMode);
        String sysMode = String.valueOf((JIMode.getM_iCommon() - 1));
        int selMode = Integer.parseInt(sysMode);
        // モードを設定
        if (routeType.equals(Constants.ROUTE_TYPE_NEW)) {
            // 新規ルート
            btnMode.setSelected(selMode);
            strMode = sysMode;
            int modeSearch = 0;
            if (sysMode.equals(CarMode)) {
//				btnModeSearch.setVisibility(View.VISIBLE);
//				btnModeManSearch.setVisibility(View.GONE);
                // システム設定にルート探索条件（車）を取得
                JNIInt JICarSearchProperty = new JNIInt();
                NaviRun.GetNaviRunObj().JNI_NE_Drv_GetDefaultSearchProperty(
                        JICarSearchProperty);
                // ルート探索条件（車）を設定
                switch (JICarSearchProperty.getM_iCommon()) {
                    case NE_CarNaviSearchProperty_Commend:
                        modeSearch = 1;
//					btnModeSearch.setSelected(modeSearch-1);
                        break;
                    case NE_CarNaviSearchProperty_General:
                        modeSearch = 3;
//					btnModeSearch.setSelected(modeSearch-1);
                        break;
                    case NE_CarNaviSearchProperty_Distance:
                        modeSearch = 2;
//					btnModeSearch.setSelected(modeSearch-1);
                        break;
                    case NE_CarNaviSearchProperty_Width:
                        modeSearch = 4;
//					btnModeSearch.setSelected(modeSearch-1);
                        break;
                    case NE_CarNaviSearchProperty_Another:
                        modeSearch = 5;
//					btnModeSearch.setSelected(modeSearch-1);
                        break;
                }
            } else {
//				btnModeSearch.setVisibility(View.GONE);
//				btnModeManSearch.setVisibility(View.VISIBLE);
                // システム設定にルート探索条件（徒歩）を取得
                JNIInt JIManSearchProperty = new JNIInt();
                NaviRun.GetNaviRunObj().JNI_NE_Man_GetDefaultSearchProperty(
                        JIManSearchProperty);
                modeSearch = JIManSearchProperty.getM_iCommon();
                // ルート探索条件（徒歩）を設定
//				btnModeManSearch.setSelected(modeSearch);
            }
            strSearchProperty = String.valueOf(modeSearch);
        } else if (routeType.equals(Constants.ROUTE_TYPE_NOW)) {
            // 現在ルート
            // 現在ルートのモードを取得
            //String nowMode = lstRouteNodeDate.get(1).getMode();
            //String nowStatus = lstRouteNodeDate.get(1).getStatus();
            int nowStatus = 0;
            // システムのモードを取得
            btnMode.setSelected(selMode);
            strMode = sysMode;
            if (sysMode.equals(CarMode)) {
//				btnModeSearch.setVisibility(View.VISIBLE);
//				btnModeManSearch.setVisibility(View.GONE);
                JNIInt carProperty = new JNIInt();
                NaviRun.GetNaviRunObj().JNI_NE_Drv_GetSectionSearchProperty(Constants.NAVI_START_INDEX, Constants.NAVI_DEST_INDEX, carProperty);
                switch (carProperty.getM_iCommon()) {
                    case NE_CarNaviSearchProperty_Commend:
                        nowStatus = 0;
                        break;
                    case NE_CarNaviSearchProperty_Distance:
                        nowStatus = 1;
                        break;
                    case NE_CarNaviSearchProperty_General:
                        nowStatus = 2;
                        break;
                    case NE_CarNaviSearchProperty_Width:
                        nowStatus = 3;
                        break;
                    case NE_CarNaviSearchProperty_Another:
                        nowStatus = 4;
                        break;
                }
//				btnModeSearch.setSelected(nowStatus);
            } else {
//				btnModeSearch.setVisibility(View.GONE);
//				btnModeManSearch.setVisibility(View.VISIBLE);
                JNIInt manProperty = new JNIInt();
                NaviRun.GetNaviRunObj().JNI_NE_Man_GetPointSearchProperty(Constants.NAVI_DEST_INDEX, manProperty);
                nowStatus = manProperty.getM_iCommon();
//				btnModeManSearch.setSelected(nowStatus);
            }
            strSearchProperty = String.valueOf(nowStatus + 1);
        } else if (routeType.equals(Constants.ROUTE_TYPE_EDIT)) {
            // 保存のルート
            String routeDate = (String)bundle.get("ItemData");
            updateDataOnEdit(routeDate);
            /*if (strMode.equals(CarMode)) {
            	btnModeSearch.setVisibility(View.VISIBLE);
            	btnModeManSearch.setVisibility(View.GONE);
            	btnModeSearch.setSelected(Integer.parseInt(strSearchProperty)-1);
            } else {
            	btnModeSearch.setVisibility(View.GONE);
            	btnModeManSearch.setVisibility(View.VISIBLE);
            	btnModeManSearch.setSelected(Integer.parseInt(strSearchProperty));
            }*/
        }
    }

    /**
     * モード切り替える処理
     *
     * @param sysMode
     */
    private void modeChange(String sysMode) {
        int modeSearch = 0;
        if (sysMode.equals(CarMode)) {
            if (btnModeSearch != null) {
                btnModeSearch.setVisibility(View.VISIBLE);
                btnModeManSearch.setVisibility(View.GONE);
                // システム設定にルート探索条件（車）を取得
                JNIInt JICarSearchProperty = new JNIInt();
                NaviRun.GetNaviRunObj().JNI_NE_Drv_GetDefaultSearchProperty(
                        JICarSearchProperty);
                // ルート探索条件（車）を設定
                switch (JICarSearchProperty.getM_iCommon()) {
                    case NE_CarNaviSearchProperty_Commend:
                        modeSearch = 1;
                        btnModeSearch.setSelected(modeSearch - 1);
                        break;
                    case NE_CarNaviSearchProperty_General:
                        modeSearch = 3;
                        btnModeSearch.setSelected(modeSearch - 1);
                        break;
                    case NE_CarNaviSearchProperty_Distance:
                        modeSearch = 2;
                        btnModeSearch.setSelected(modeSearch - 1);
                        break;
                    case NE_CarNaviSearchProperty_Width:
                        modeSearch = 4;
                        btnModeSearch.setSelected(modeSearch - 1);
                        break;
                    case NE_CarNaviSearchProperty_Another:
                        modeSearch = 5;
                        btnModeSearch.setSelected(modeSearch - 1);
                        break;
                }
            }
        } else {
            // 歩行者データが存在するかどうかを判断する
//Chg 2011/10/11 Z01yoneya Start -->
//            if (!((NaviApplication)getApplication()).getNaviAppDataPath().isExistWalkData()) {
//------------------------------------------------------------------------------------------------------------------------------------
            boolean isExistWalkData;
            try {
                isExistWalkData = ((NaviApplication)getApplication()).getNaviAppDataPath().isExistWalkData();
            } catch (NullPointerException e) {
                isExistWalkData = false;
            }
            if (!isExistWalkData) {
//Chg 2011/10/11 Z01yoneya End <--
                this.showDialog(Constants.DIALOG_NO_WALKER_DATA);
                strMode = CarMode;
                btnMode.setSelected(Integer.parseInt(strMode));
                return;
            } else {
                if (btnModeSearch != null) {
                    btnModeSearch.setVisibility(View.GONE);
                    btnModeManSearch.setVisibility(View.VISIBLE);
                    // システム設定にルート探索条件（徒歩）を取得
                    JNIInt JIManSearchProperty = new JNIInt();
                    NaviRun.GetNaviRunObj().JNI_NE_Man_GetDefaultSearchProperty(
                            JIManSearchProperty);
                    modeSearch = JIManSearchProperty.getM_iCommon();
                    // ルート探索条件（徒歩）を設定
                    btnModeManSearch.setSelected(modeSearch);
                }
            }
        }
        strSearchProperty = String.valueOf(modeSearch);
    }

    /**
     * Created on 2010/12/02
     * Description: AdapterList情報を取得する
     *
     * @param <RouteNodeData>　lstRouteNodeDate　ルート情報のリスト
     * @return List<AddBean>　ListViewのデータ
     * @author: XuYang
     */
    private List<AddBean> getAdapterList(List<RouteNodeData> lstRouteNodeDate) {
        List<AddBean> lstAddBean = new ArrayList<AddBean>();
        for (int i = 0; i < lstRouteNodeDate.size(); i++) {
            AddBean addBean = new AddBean();
            RouteNodeData routeNodeData = (RouteNodeData)lstRouteNodeDate.get(i);
            addBean.setIndex(i);
            if (i == 0) {
                addBean.setText("[S] " + routeNodeData.getName().replace("[S] ", ""));
            } else if (i == lstRouteNodeDate.size() - 1) {
                addBean.setStrTextNo(routeNodeData.getName());
                if (routeNodeData.getName() != null) {
                    addBean.setText("[G] " + routeNodeData.getName().replaceAll(" ", "").replace("[S] ", ""));
                }
            } else {
                addBean.setStrTextNo(routeNodeData.getName());
                if (routeNodeData.getName() != null) {
                    addBean.setText("[" + i + "] " + routeNodeData.getName().replaceAll(" ", "").replace("[S] ", ""));
                }
            }
            addBean.setIpass(routeNodeData.getIpass());
            lstAddBean.add(addBean);
        }
        return lstAddBean;
    }

    /**
     * 表示したデータを取得
     */
    private View routeDataGetView(int pos, View view) {
        LinearLayout oLayout;
        if (view == null) {
            LayoutInflater inflater;
            inflater = LayoutInflater.from(this);
            // レイアウト画面を追加
            oLayout = (LinearLayout)inflater.inflate(
                    R.layout.route_add_list, null);
            view = oLayout;
        } else {
            oLayout = (LinearLayout)view;
        }

        // ルートのアドレス
        LinearLayout btnDestination = (LinearLayout)oLayout.findViewById(R.id.btn_Destination);
        TextView txtTitle = (TextView)oLayout.findViewById(R.id.btn_Destination_title);
        TextView txtContent = (TextView)oLayout.findViewById(R.id.btn_Destination_content);
        // 削除
        ImageView imgDel = (ImageView)oLayout.findViewById(R.id.btn_RouteEditDelete);
        // 切り替える
        ImageView imgSwitch = (ImageView)oLayout.findViewById(R.id.imgswitch);
        // 追加
        Button imgAdd = (Button)oLayout.findViewById(R.id.imgadd);
        LinearLayout laySwitch = (LinearLayout)oLayout.findViewById(R.id.btnswitch);
        LinearLayout layAdddress = (LinearLayout)oLayout.findViewById(R.id.btnadddress);
        LinearLayout layRouteSearch = (LinearLayout)oLayout.findViewById(R.id.routeSearchLayout);

        int size = lstAddBean.size();
        if (pos == 0) {
            laySwitch.setVisibility(View.GONE);
            btnDestination.setVisibility(View.INVISIBLE);
            layAdddress.setVisibility(View.GONE);
            layRouteSearch.setVisibility(View.VISIBLE);
            initSearchCondition(layRouteSearch);
        } else {
            // ルート探索条件以外
            pos = pos - 1;
            layRouteSearch.setVisibility(View.GONE);
            if (pos % 2 == 0) {
                // アドレスと削除レイアウトを表示
                final int intPos = pos - (pos / 2);
                laySwitch.setVisibility(View.GONE);
                btnDestination.setVisibility(View.VISIBLE);
                layAdddress.setVisibility(View.VISIBLE);

                if (intPos < size) {
                    AddBean addBean = lstAddBean.get(intPos);
                    // 通過済み経由地の判断
                    if (addBean.getIpass() == 1) {
                        btnDestination.setEnabled(false);
                        // #841対応 start 2011-04-01 XuYang
                        String strMsg = "";
                        String strTitle = "";
                        if (addBean.getText() != null && !"".equals(addBean.getText())) {
                            strMsg = addBean.getText().substring(4);
                            strTitle = addBean.getText().substring(0, 4);
                        }
                        txtTitle.setText(strTitle);
                        txtTitle.setTextColor(Color.GRAY);
                        txtContent.setText(strMsg);
                        txtContent.setTextColor(Color.GRAY);
                        // #841対応 end 2011-04-01 XuYang
                    } else {
                        btnDestination.setEnabled(true);
                        btnDestination.setOnClickListener(new OnClickListener() {
                            public void onClick(View oView) {
                                modifyItem(intPos);
                            }
                        });
                        String strMsg = "";
                        String strTitle = "";
                        if (addBean.getText() != null && !"".equals(addBean.getText())) {
                            strMsg = addBean.getText().substring(4);
                            strTitle = addBean.getText().substring(0, 4);
                        }
                        txtTitle.setText(strTitle);
                        txtContent.setText(strMsg);
                        // #841対応 start 2011-04-01 XuYang
                        txtTitle.setTextColor(Color.BLACK);
                        txtContent.setTextColor(Color.BLACK);
                        // #841対応 end 2011-04-01 XuYang
                    }

                    // 削除ボタン表示かどうかを判断する
                    if (size != 1) {
                        // 通過済み経由地の判断
                        if (addBean.getIpass() == 1) {
                            imgDel.setEnabled(false);
                        } else {
                            imgDel.setEnabled(true);
                            imgDel.setOnClickListener(new OnClickListener() {
                                public void onClick(View oView) {
                                    deleteItem(intPos);
                                }
                            });
                        }
                    } else {
                        // １地点しかない場合は削除ボタンをグレーアウトに設定する
                        imgDel.setEnabled(false);
                    }
                } else {
                    if (pos > 12) {
                        layAdddress.setVisibility(View.GONE);
                    } else {
                        btnDestination.setEnabled(false);
                        imgDel.setEnabled(false);
                        layAdddress.setVisibility(View.VISIBLE);
                        txtTitle.setText("");
                        txtContent.setText("");
                    }
                }
            } else if (pos % 2 != 0) {
                // 切り替えると追加レイアウトを表示
                final int index = pos / 2 + 1;
                laySwitch.setVisibility(View.VISIBLE);
                btnDestination.setVisibility(View.INVISIBLE);
                layAdddress.setVisibility(View.GONE);
                if (pos == size + (size - 1)) {
                    if (pos > 12) {
                        laySwitch.setVisibility(View.GONE);
                    } else {
                        laySwitch.setVisibility(View.VISIBLE);
                        // 目的地の下1つの切り替えると追加状態を設定
                        imgSwitch.setEnabled(false);
                        // 5つ経由地時、最後追加ボタン不可用を設定
                        if (size != 7) {
                            imgAdd.setEnabled(true);
                            imgAdd.setOnClickListener(new OnClickListener() {
                                public void onClick(View oView) {
                                    addItem(index);
                                }
                            });
                        } else {
                            imgAdd.setEnabled(false);
                        }
                    }
                } else if (pos > size + (size - 1)) {
                    // 目的地以後（目的地の下1つ以外）の切り替えると追加状態を設定
                    if (pos > 12) {
                        laySwitch.setVisibility(View.GONE);
                    } else {
                        laySwitch.setVisibility(View.VISIBLE);
                        imgSwitch.setEnabled(false);
                        imgAdd.setEnabled(false);
                    }

                } else {
                    // 開始地点から、目的地までの切り替えると追加状態を設定
                    if (pos == 1 && lstAddBean.get(0).getText().toString()
                            .contains(getResources().getString(R.string.route_start_point_name))) {
                        // 開始地点かつ、「現在地」が含まれる場合、切り替える不可用を設定
                        // 現在地編集前、切り替える不可用
                        imgSwitch.setEnabled(false);
                    } else {
                        AddBean addBean = lstAddBean.get(index);
                        int pass = addBean.getIpass();
                        if (pass == 1) {
                            imgSwitch.setEnabled(false);
                        } else {
                            // XuYang add strat #905
                            AddBean addBeanNext = lstAddBean.get(index - 1);
                            int passNext = addBeanNext.getIpass();
                            if (passNext == 1) {
                                imgSwitch.setEnabled(false);
                            } else {
                                // XuYang add end #905
                                imgSwitch.setEnabled(true);
                                imgSwitch.setOnClickListener(new OnClickListener() {
                                    public void onClick(View oView) {
                                        SwitchItem(index);
                                    }
                                });
                                // XuYang add strat #905
                            }
                            // XuYang add end #905
                        }
                    }
                    // 5つ経由地時、全部追加ボタン不可用を設定
                    if (size != 7) {
                        AddBean addBean = lstAddBean.get(index);
                        int pass = addBean.getIpass();
                        if (pass == 1) {
                            imgAdd.setEnabled(false);
                        } else {
                            imgAdd.setEnabled(true);
                            imgAdd.setOnClickListener(new OnClickListener() {
                                public void onClick(View oView) {
                                    addItem(index);
                                }
                            });
                        }
                    } else {
                        imgAdd.setEnabled(false);
                    }
                }
            }
        }

        updateFinishBtn();

        return view;
    }

//Add 2011/10/15 Z01_h_yamada Start -->
    private void updateFinishBtn() {

    	int i, ie = lstRouteNodeDate.size();
    	int next_num = 0;
    	for (i = 1; i < ie; ++i) {
    		if ( lstRouteNodeDate.get(i).getIpass() == 1 ) {
    			// 通過済み
    		} else {
    			next_num++;
    		}
    	}
    	if (next_num == 0) {
    		// 残りの案内地点がない場合、決定ボタンを不可用に設定する
            btnDetermine.setEnabled(false);
        } else {
            btnDetermine.setEnabled(true);
        }
    	if ( lstRouteNodeDate.size() > 1 ) {
	        btnSave.setEnabled(true);
    	} else {
	        btnSave.setEnabled(false);
    	}
    }
//Add 2011/10/15 Z01_h_yamada End <--

    private void initSearchCondition(LinearLayout oLayout) {

// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// CHG 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 Start -->
    	//btnModeSearch = (SwitchGroup)oLayout.findViewById(R.id.btnModeSearch);
    	btnModeSearch = (SwitchGroupEx)oLayout.findViewById(R.id.btnModeSearch);
// CHG 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        btnModeSearch.setListener(new SwitchGroupListener() {

            @Override
            public void onSelectChange(int wSelect)
            {
                switch (wSelect) {
                    case 0:
                    strSearchProperty = CarModeSearchRoute1;
                    break;
                case 1:
                    strSearchProperty = CarModeSearchRoute2;
                    break;
                case 2:
                    strSearchProperty = CarModeSearchRoute3;
                    break;
                case 3:
                    strSearchProperty = CarModeSearchRoute4;
                    break;
                case 4:
                    strSearchProperty = CarModeSearchRoute5;
                    break;
            }
        }
        });

        btnModeManSearch = (SwitchGroup)oLayout.findViewById(R.id.btnModeManSearch);
        btnModeManSearch.setListener(new SwitchGroupListener() {

            @Override
            public void onSelectChange(int wSelect)
            {
                switch (wSelect) {
                    case 0:
                    strSearchProperty = ManModeSearchRoute1;
                    break;
                case 1:
                    strSearchProperty = ManModeSearchRoute2;
                    break;
                case 2:
                    strSearchProperty = ManModeSearchRoute3;
                    break;
            }
        }
        });
        // #905 XuYang add start
        if (firstInitFlag) {
            initSearchCondStat();
            firstInitFlag = false;
        } else {
            notInitSearchCondStat();
        }
        // #905 XuYang add end
    }

    // #905 XuYang add start
    private void notInitSearchCondStat() {

        if (strMode.equals(CarMode)) {
            btnModeSearch.setVisibility(View.VISIBLE);
            btnModeManSearch.setVisibility(View.GONE);
            btnModeSearch.setSelected(Integer.parseInt(strSearchProperty) - 1);
        } else {
            btnModeSearch.setVisibility(View.GONE);
            btnModeManSearch.setVisibility(View.VISIBLE);
            btnModeManSearch.setSelected(Integer.parseInt(strSearchProperty));
        }
    }

    // #905 XuYang add end
    private void initSearchCondStat() {
        if (routeType.equals(Constants.ROUTE_TYPE_NEW)) {
            // 新規ルート
            int modeSearch = 0;
            if (strMode.equals(CarMode)) {
                btnModeSearch.setVisibility(View.VISIBLE);
                btnModeManSearch.setVisibility(View.GONE);
                // システム設定にルート探索条件（車）を取得
                JNIInt JICarSearchProperty = new JNIInt();
                NaviRun.GetNaviRunObj().JNI_NE_Drv_GetDefaultSearchProperty(
                        JICarSearchProperty);
                // ルート探索条件（車）を設定
                switch (JICarSearchProperty.getM_iCommon()) {
                    case NE_CarNaviSearchProperty_Commend:
                        modeSearch = 1;
                        btnModeSearch.setSelected(modeSearch - 1);
                        break;
                    case NE_CarNaviSearchProperty_General:
                        modeSearch = 3;
                        btnModeSearch.setSelected(modeSearch - 1);
                        break;
                    case NE_CarNaviSearchProperty_Distance:
                        modeSearch = 2;
                        btnModeSearch.setSelected(modeSearch - 1);
                        break;
                    case NE_CarNaviSearchProperty_Width:
                        modeSearch = 4;
                        btnModeSearch.setSelected(modeSearch - 1);
                        break;
                    case NE_CarNaviSearchProperty_Another:
                        modeSearch = 5;
                        btnModeSearch.setSelected(modeSearch - 1);
                        break;
                }
            } else {
                btnModeSearch.setVisibility(View.GONE);
                btnModeManSearch.setVisibility(View.VISIBLE);
                // システム設定にルート探索条件（徒歩）を取得
                JNIInt JIManSearchProperty = new JNIInt();
                NaviRun.GetNaviRunObj().JNI_NE_Man_GetDefaultSearchProperty(
                        JIManSearchProperty);
                modeSearch = JIManSearchProperty.getM_iCommon();
                // ルート探索条件（徒歩）を設定
                btnModeManSearch.setSelected(modeSearch);
            }
            strSearchProperty = String.valueOf(modeSearch);
        } else if (routeType.equals(Constants.ROUTE_TYPE_NOW)) {
            // 現在ルート
            // 現在ルートのモードを取得
            //String nowMode = lstRouteNodeDate.get(1).getMode();
            //String nowStatus = lstRouteNodeDate.get(1).getStatus();
            int nowStatus = 0;
            // システムのモードを取得

            if (strMode.equals(CarMode)) {
                btnModeSearch.setVisibility(View.VISIBLE);
                btnModeManSearch.setVisibility(View.GONE);
                JNIInt carProperty = new JNIInt();
                NaviRun.GetNaviRunObj().JNI_NE_Drv_GetSectionSearchProperty(Constants.NAVI_START_INDEX, Constants.NAVI_DEST_INDEX, carProperty);
                switch (carProperty.getM_iCommon()) {
                    case NE_CarNaviSearchProperty_Commend:
                        nowStatus = 0;
                        break;
                    case NE_CarNaviSearchProperty_Distance:
                        nowStatus = 1;
                        break;
                    case NE_CarNaviSearchProperty_General:
                        nowStatus = 2;
                        break;
                    case NE_CarNaviSearchProperty_Width:
                        nowStatus = 3;
                        break;
                    case NE_CarNaviSearchProperty_Another:
                        nowStatus = 4;
                        break;
                }
                btnModeSearch.setSelected(nowStatus);
                // #905 XuYang add start
                strSearchProperty = String.valueOf(nowStatus + 1);
                // #905 XuYang add end
            } else {
                btnModeSearch.setVisibility(View.GONE);
                btnModeManSearch.setVisibility(View.VISIBLE);
                JNIInt manProperty = new JNIInt();
                NaviRun.GetNaviRunObj().JNI_NE_Man_GetPointSearchProperty(Constants.NAVI_DEST_INDEX, manProperty);
                nowStatus = manProperty.getM_iCommon();
                btnModeManSearch.setSelected(nowStatus);
                // #905 XuYang add start
                strSearchProperty = String.valueOf(nowStatus);
                // #905 XuYang add end
            }
        } else if (routeType.equals(Constants.ROUTE_TYPE_EDIT)) {
            // 保存のルート
            String routeDate = (String)bundle.get("ItemData");
            updateDataOnEdit(routeDate);
            if (strMode.equals(CarMode)) {
                btnModeSearch.setVisibility(View.VISIBLE);
                btnModeManSearch.setVisibility(View.GONE);
                btnModeSearch.setSelected(Integer.parseInt(strSearchProperty) - 1);
            } else {
                btnModeSearch.setVisibility(View.GONE);
                btnModeManSearch.setVisibility(View.VISIBLE);
                btnModeManSearch.setSelected(Integer.parseInt(strSearchProperty));
            }
        }
    }

    /**
     * ルート修正の処理
     */
    private void modifyItem(int index) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 保存/新規/現在ルート編集 修正禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 保存/新規/現在ルート編集 修正禁止 <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END

//Add 2012/02/07 katsuta Start --> #2698
//			NaviLog.d(NaviLog.PRINT_LOG_TAG, "RouteEdit modifyItem ================== bIsListClicked: " + bIsListClicked);
 			if (bIsListClicked) {
 				return;
 			}
 			else {
 				bIsListClicked = true;
 			}
//Add 2012/02/07 katsuta End <--#2698


    	//yangyang add WaitDialog start
        this.createDialog(Constants.DIALOG_WAIT, -1);
        //yangyang add WaitDialog end
        modifyItemIndex = index;
        // pointを設定

//		int ActiveType = 2;
//        int TransType = 2;
//        byte bScale = 4;
//        long Longitude = 0;
//        long Latitude = 0;
//        Latitude = Long.parseLong(lstRouteNodeDate.get(index).getLat());
//        Longitude = Long.parseLong(lstRouteNodeDate.get(index).getLon());
//        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(ActiveType, TransType, bScale, Longitude, Latitude);
//        NaviRun.GetNaviRunObj().setSearchKind(Constants.ROUTE_EDIT_KIND);

        CommonLib.setChangePos(true);
        CommonLib.setChangePosFromSearch(false);
        // xuyang add start #813
        CommonLib.routeSearchFlag = false;
        // xuyang add end #813

        POIData oData = new POIData();
        oData.m_sName = lstRouteNodeDate.get(index).getName();
        oData.m_sAddress = lstRouteNodeDate.get(index).getName();
        oData.m_wLong = Long.parseLong(lstRouteNodeDate.get(index).getLon());
        oData.m_wLat = Long.parseLong(lstRouteNodeDate.get(index).getLat());

        Intent intent = new Intent();
        intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_EDIT);

        OpenMap.moveMapTo(RouteEdit.this, oData, intent, AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);

    }

    /**
     * ルート削除の処理
     *
     * @param index
     */
    private void deleteItem(int index) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 保存/新規/現在ルート編集 削除禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 保存/新規/現在ルート編集 削除禁止 <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
//Add 2011/10/15 Z01_h_yamada Start -->
    	if (index < 0 || index >= lstRouteNodeDate.size()) {
        	NaviLog.e(NaviLog.PRINT_LOG_TAG, "RouteEdit::deleteItem error index=" + index + " lstRouteNodeDate.size=" + lstRouteNodeDate.size() );
    		return ;
    	}
//Add 2011/10/15 Z01_h_yamada End <--
        lstRouteNodeDate.remove(index);
        lstAddBean = getAdapterList(lstRouteNodeDate);
        oBox.reset();
    }

    /**
     * ルート追加の処理
     *
     * @param pIndex
     */
    private void addItem(int pIndex) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 保存/新規/現在ルート編集 追加禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 保存/新規/現在ルート編集 追加禁止 <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        // xuyang add start #980
        addButtonFromAddflag = true;
        // xuyang add end #980
        addItemIndex = pIndex;
        Intent intent = new Intent();
        intent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        intent.putExtra(Constants.FROM_FLAG_KEY, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
        intent.setClass(this, SearchMainMenuActivity.class);
        CommonLib.setChangePosFromSearch(true);
        CommonLib.setChangePos(false);
        // xuyang add start #813
        CommonLib.routeSearchFlag = false;
        // xuyang add end #813
        NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_ROUTE_ADD);
    }

    /**
     * Created on 2010/12/01　
     * Description:上下2つの経由地位置を切り替える
     *
     * @param　List<RouteNodeData> pLstRouteNodeDate　ルート情報リスト
     * @param　int pIndex　経由地位置の番号
     * @return　List<RouteNodeData>　　ルート情報リスト
     * @author: XuYang
     */
    private void SwitchItem(int pIndex) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 保存/新規/現在ルート編集 入れ替え禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 保存/新規/現在ルート編集 入れ替え禁止 <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
//Add 2011/10/15 Z01_h_yamada Start -->
    	if (pIndex < 1 || pIndex >= lstRouteNodeDate.size()) {
        	NaviLog.e(NaviLog.PRINT_LOG_TAG, "RouteEdit::SwitchItem error pIndex=" + pIndex + " lstRouteNodeDate.size=" + lstRouteNodeDate.size() );
    		return ;
    	}
//Add 2011/10/15 Z01_h_yamada End <--
//		RouteNodeData tempRouteUp = new RouteNodeData();
//		RouteNodeData tempRouteDown = new RouteNodeData();
        RouteNodeData tempRouteUp = lstRouteNodeDate.get(pIndex - 1);
        RouteNodeData tempRouteDown = lstRouteNodeDate.get(pIndex);
        lstRouteNodeDate.remove(pIndex - 1);
        lstRouteNodeDate.add(pIndex - 1, tempRouteDown);
        lstRouteNodeDate.remove(pIndex);
        lstRouteNodeDate.add(pIndex, tempRouteUp);
        lstAddBean = getAdapterList(lstRouteNodeDate);
        oBox.reset();
    }

    /**
     * ダイアログの処理
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        final CustomDialog oDialog = new CustomDialog(this);
        Resources oRes = getResources();
        if (id == DIALOG_SAVE) {
            oDialog.setTitle(oRes.getString(R.string.title_route_save));
            oDialog.setMessage(oRes.getString(R.string.msg_save_overflow));

            oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    oDialog.dismiss();
                    removeDialog(DIALOG_SAVE);
                }

            });
        } else if (id == Constants.DIALOG_NO_WALKER_DATA) {
            oDialog.setTitle(oRes.getString(R.string.dialog_no_walkerdata_title));
            oDialog.setMessage(oRes.getString(R.string.dialog_no_walkerdata));

            oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {
                @Override
                public void onClick(View v) {
                    oDialog.dismiss();
                    removeDialog(Constants.DIALOG_NO_WALKER_DATA);
                }
            });
        } else if (id == DIALOG_ROUTE_FAILURE) {

            oDialog.setTitle(R.string.route_failure_title);
            oDialog.setMessage(R.string.route_failure_message);

            oDialog.addButton(R.string.btn_ok,
                    new OnClickListener() {
                        public void onClick(View v) {
                            oDialog.dismiss();
                            removeDialog(DIALOG_ROUTE_FAILURE);
                        }
                    });

        } else if (id == DIALOG_WALK_ROUTE_TOO_LONG) {

            oDialog.setTitle(R.string.walk_search_failed_title);
            oDialog.setMessage(R.string.walk_search_failed);

            oDialog.addButton(R.string.btn_ok,
                    new OnClickListener() {
                        public void onClick(View v) {
                            oDialog.dismiss();
                            removeDialog(DIALOG_WALK_ROUTE_TOO_LONG);
                            // 車モードに変更して、再探索開始。
                            strMode = CarMode;
                            modeChange(strMode);
                            determineRoute();
                        }
                    });
            oDialog.addButton(R.string.btn_cancel,
                    new OnClickListener() {
                        public void onClick(View v) {
                            oDialog.dismiss();
                            removeDialog(DIALOG_WALK_ROUTE_TOO_LONG);
                        }
                    });
        }
//Add 2011/12/15 Z01_h_yamada Start -->
        else if (id == DIALOG_ROUTE_SAVE_OK) {

            oDialog.setTitle(R.string.title_route_save);
            oDialog.setMessage("「" + routeName + oRes.getString(R.string.msg_save_confirm));
            oDialog.addButton(R.string.btn_ok,
                    new OnClickListener() {
                        public void onClick(View v) {
                            oDialog.dismiss();
                            removeDialog(DIALOG_ROUTE_SAVE_OK);
                        }
                    });
     	} else if(id == DIALOG_NOT_ADD_ROUTE) {
            oDialog.setTitle(R.string.dialog_not_add_route_title);
            oDialog.setMessage(R.string.dialog_not_add_route_message);

            oDialog.addButton(R.string.btn_ok,
                    new OnClickListener() {
                        public void onClick(View v) {
                            oDialog.dismiss();
                            removeDialog(DIALOG_NOT_ADD_ROUTE);
                        }
                    });
     	}
//Add 2011/12/15 Z01_h_yamada End <--
        // XuYang add start 走行中の操作制限
        else {
            return super.onCreateDialog(id);
        }
        // XuYang add end 走行中の操作制限
        return oDialog;
    }

    /**
     * ルート名称編集画面に遷移する処理
     */
    // XuYang modi start #1084
    private void goRouteNameEdit(String routeName) {
        // XuYang modi end #1084
        Intent intent = new Intent();
        // XuYang modi start #1084
        intent.putExtra(Constants.FLAG_TITLE, routeName);
        // XuYang modi end #1084
        intent.setClass(this, RouteNameEdit.class);
        NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_ROUTE_NAME_EDIT);
    }

    /**
     * ルート保存の処理
     */
    private void saveRoute() {
        if (routeType.equals(Constants.ROUTE_TYPE_EDIT)) {
            // 保存のルート
            // XuYang modi start #1084
            String routeName = (String)bundle.get("ItemName");
            // xuyang modi start bug1748
            if (isChangeGoal()) {
                goRouteNameEdit(routeName);
            } else {
                goRouteNameEdit(lstRouteNodeDate.get(lstRouteNodeDate.size() - 1).getName());
            }
//			goRouteNameEdit(routeName);
            // xuyang modi end bug1748
            // XuYang modi end #1084
        } else {
            // xuyang add start bug1748_2
            if (SaveFirstName != null) {

                if (isChangeGoal()) {
                    goRouteNameEdit(routeName);
                } else {
                    SaveFirstName = null;
//Chg 2011/10/03 Z01yoneya Start -->
//                    int size = readData();
//------------------------------------------------------------------------------------------------------------------------------------
                    int size = 0;
                    RouteDataFile routeDataFile = new RouteDataFile();
                    List<RouteData> routeDataList = routeDataFile.readData(filePath);
                    size = routeDataList.size();
//Chg 2011/10/03 Z01yoneya End <--
                    if (size > ROUTE_SAVE_MAX_COUNT - 1) {
                        this.showDialog(DIALOG_SAVE);
                    } else {
                        goRouteNameEdit(lstRouteNodeDate.get(lstRouteNodeDate.size() - 1).getName());
                    }

                }
            } else {
                // xuyang add end bug1748_2
//Chg 2011/10/03 Z01yoneya Start -->
//                int size = readData();
//------------------------------------------------------------------------------------------------------------------------------------
                int size = 0;
                RouteDataFile routeDataFile = new RouteDataFile();
                List<RouteData> routeDataList = routeDataFile.readData(filePath);
                size = routeDataList.size();
//Chg 2011/10/03 Z01yoneya End <--
                if (size > ROUTE_SAVE_MAX_COUNT - 1) {
                    this.showDialog(DIALOG_SAVE);
                } else {
                    // XuYang modi start #1084
                    goRouteNameEdit(lstRouteNodeDate.get(lstRouteNodeDate.size() - 1).getName());
                    // XuYang modi end #1084
                }
                // xuyang add start bug1748_2
            }
            // xuyang add end bug1748_2
        }
    }

    // xuyang add start bug1748
    private boolean isChangeGoal() {
        String oldName = lstRouteNodeDateOldSave.get(lstRouteNodeDateOldSave.size() - 1).getName();
        String newName = lstRouteNodeDate.get(lstRouteNodeDate.size() - 1).getName();
        if (oldName.equals(newName)) {
            return true;
        }
        return false;
    }

    private RouteData getAddRouteData() {
        RouteData routeData = new RouteData();

        java.util.Date date = new java.util.Date(System.currentTimeMillis());
        String strUTC;
        // ルート修正を判断する
        if (routeType.equals(Constants.ROUTE_TYPE_EDIT)) {
            // 保存のルート
            strUTC = (String)bundle.get("ItemData");
        } else {
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyyMMddHHmmSS");
            strUTC = sdf.format(date);
            // xuyang add start bug1748_2
            SaveFirstName = strUTC;
            // xuyang add end bug1748_2
        }

        routeData.setRouteDate(strUTC);
        routeData.setRouteName(routeName);
        routeData.setRouteMode(Integer.parseInt(strMode));
        for (int i = 0; i < lstRouteNodeDate.size(); i++) {
            long lat = Long.parseLong(lstRouteNodeDate.get(i).getLat());
            long lon = Long.parseLong(lstRouteNodeDate.get(i).getLon());
            String name = lstRouteNodeDate.get(i).getName();
            int status = Integer.parseInt(strSearchProperty);
            routeData.addRouteNode(lat, lon, name, status);
        }
        return routeData;

    }

    /**
     * Created on 2010/12/07
     * Description: 経由地の名前を取得する　
     *
     * @param int　pRoutePoint　経由地の番号
     * @return boolean
     * @author: XuYang
     */
    private boolean getRouteInfo(int pRoutePoint) {
        JNITwoLong JNIpositon = new JNITwoLong();
        JNIString JNIpositonName = new JNIString();
        JNIInt JNIpass = new JNIInt();
        JNITwoLong CenterCoordinate = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(pRoutePoint, JNIpositon,
                JNIpositonName, JNIpass);
        String strPointName = null;
        if (JNIpositon.getM_lLat() != 0 && JNIpositon.getM_lLong() != 0) {
            String strPointlat = String.valueOf(JNIpositon.getM_lLat());
            String strPointlon = String.valueOf(JNIpositon.getM_lLong());
            // modify
            // #875 XuYang add start
//			if (!Constants.NotFromSearch.equals(strAddressString) && pRoutePoint == 8
////			        &&("".equals(JNIpositonName.getM_StrAddressString())
////	                        || null == JNIpositonName.getM_StrAddressString())
//			        ) {
//			    Log.v("xy","8888888888888888888888pRoutePoint:" + pRoutePoint);
//                strPointName = strAddressString;
//            } else {
            // #875 XuYang add end
            if ("".equals(JNIpositonName.getM_StrAddressString())
                        || null == JNIpositonName.getM_StrAddressString()) {
                NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(
                            CenterCoordinate);

                if (JNIpositon.getM_lLong() == CenterCoordinate
                            .getM_lLong()
                            && JNIpositon.getM_lLat() == CenterCoordinate
                                    .getM_lLat()) {
                    JNIString RouteAddress = new JNIString();
                    NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
                                CenterCoordinate.getM_lLong(),
                                CenterCoordinate.getM_lLat(), RouteAddress);
                    strPointName = RouteAddress.getM_StrAddressString();
                    // xuyang add start bug115426
                    if (pRoutePoint == 1) {
                        strPointName = getResources().getString(R.string.route_start_point_name);
                    }
                    // xuyang add end bug115426
                } else {
                    JNIString RouteAddress = new JNIString();
                    NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(
                                JNIpositon.getM_lLong(),
                                JNIpositon.getM_lLat(), RouteAddress);
                    //hangeng add start bug1696
                    oPre = new PreferencesService(this, "FAVORITES");
                    if (!"".equalsIgnoreCase(oPre.getString("favoritesname", "")) && pRoutePoint != 1) {
                        NaviLog.d(NaviLog.PRINT_LOG_TAG,"come get");
                        strPointName = oPre.getString("favoritesname", "");
                        oPre.clearAllValue();
                        oPre.save();
                    } else {
                        strPointName = RouteAddress.getM_StrAddressString();
                    }
                    //hangeng add end bug1696

                    // xuyang add start bug115426
                    if (pRoutePoint == 1) {
                        strPointName = getResources().getString(R.string.route_start_point_name);
                    }
                    // xuyang add end bug115426
//						if (null == strPointName
//						        || "".equals(strPointName.trim())) {
//							strPointName = "なぃ";
//						}
                }
            } else {
                strPointName = JNIpositonName.getM_StrAddressString();
            }
            if (null == strPointName
                        || "".equals(strPointName.trim())) {
                strPointName = getResources().getString(R.string.name_null);
            }
            // #875 XuYang add start
//			}
            // #875 XuYang add end
            // For Bug 110153 End
            RouteNodeData RouteInfo = new RouteNodeData();
            RouteInfo.setDate("");
            RouteInfo.setLat(strPointlat);
            RouteInfo.setLon(strPointlon);
            RouteInfo.setMode("");
            RouteInfo.setName(strPointName);
            RouteInfo.setStatus("");
            RouteInfo.setIpass(JNIpass.getM_iCommon());
            lstRouteNodeDate.add(RouteInfo);
        } else {
            return false;
        }
        return true;
    }

    /**
     * Created on 2010/12/07
     * Description: 決定ボタンの処理
     *
     * @param 無し
     * @return void 無し
     * @author: XuYang
     */
    private void determineRoute() {

        Constants.FROM_ROUTE_CAL = true;
        JNIInt GuideState = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_Java_GetGuideState(GuideState);
        if (GuideState.getM_iCommon() != 0) {
            NaviRun.GetNaviRunObj().JNI_NE_StopGuidance();
        }
        for (int i = 0; i < Constants.NAVI_DEST_INDEX; i++) {
            int NE_RoutePointType_Start = 1;
            NaviRun.GetNaviRunObj().JNI_NE_DeleteRoutePoint(
                    NE_RoutePointType_Start + i);
        }
        // 目的地のindext
        int iRoutePointType_Goal = Constants.NAVI_DEST_INDEX;
        // 出発地のindext
        int iRoutePointType_Start = Constants.NAVI_START_INDEX;
        long Longitude = 0;
        long Latitude = 0;
        String PointName = null;
        int iPassed = 0;
        for (int i = Constants.NAVI_START_INDEX; i <= Constants.NAVI_DEST_INDEX; i++) {
            JNITwoLong positon = new JNITwoLong();
            JNIString JNIpositonName = new JNIString();
            JNIInt bPassed = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_GetRoutePoint(i, positon,
                    JNIpositonName, bPassed);
        }
        String strLon = lstRouteNodeDate.get(0).getLon();
        String strLat = lstRouteNodeDate.get(0).getLat();
        String strName = lstRouteNodeDate.get(0).getName();
        iPassed = lstRouteNodeDate.get(0).getIpass();
        if (strName.contains(getResources().getString(R.string.route_start_point_name))) {
            NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);
            strLon = String.valueOf(Coordinate.getM_lLong());
            strLat = String.valueOf(Coordinate.getM_lLat());
        }
        // 開始地点
        NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(iRoutePointType_Start,
                Long.parseLong(strLon), Long.parseLong(strLat),
                strName, iPassed);

        // 目的地点
        Longitude = Long.parseLong(lstRouteNodeDate.get(lstRouteNodeDate.size() - 1).getLon());
        Latitude = Long.parseLong(lstRouteNodeDate.get(lstRouteNodeDate.size() - 1).getLat());
        PointName = lstRouteNodeDate.get(lstRouteNodeDate.size() - 1).getName();
        iPassed = lstRouteNodeDate.get(lstRouteNodeDate.size() - 1).getIpass();
        NaviRun.GetNaviRunObj()
                .JNI_NE_SetRoutePoint(iRoutePointType_Goal, Longitude,
                        Latitude, PointName, iPassed);

        // 経由地
        for (int i = 1; i < lstRouteNodeDate.size() - 1; i++) {
            Longitude = Long.parseLong(lstRouteNodeDate.get(i).getLon());
            Latitude = Long.parseLong(lstRouteNodeDate.get(i).getLat());
            PointName = lstRouteNodeDate.get(i).getName();
            iPassed = lstRouteNodeDate.get(i).getIpass();
            NaviRun.GetNaviRunObj().JNI_NE_SetRoutePoint(i + 2, Longitude,
                    Latitude, PointName, iPassed);
        }

        if (strMode.equals(CarMode)) {
            int temp = 0;
            switch (Integer.parseInt(strSearchProperty)) {
                case 1:
                    temp = NE_CarNaviSearchProperty_Commend;
                    break;
                case 2:
                    temp = NE_CarNaviSearchProperty_Distance;
                    break;
                case 3:
                    temp = NE_CarNaviSearchProperty_General;
                    break;
                case 4:
                    temp = NE_CarNaviSearchProperty_Width;
                    break;
                case 5:
                    temp = NE_CarNaviSearchProperty_Another;
                    break;
            }

            NaviRun.GetNaviRunObj().JNI_NE_Drv_SetSectionSearchProperty(
                    iRoutePointType_Start, iRoutePointType_Goal,
                    temp);
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_Man_SetPointSearchProperty(
                    iRoutePointType_Goal, Integer.parseInt(strSearchProperty));
        }

        CommonLib.SEARCHP_ROPERTY = strSearchProperty;
        NaviRun.GetNaviRunObj().JNI_NE_SetNaviMode(
                Integer.parseInt(strMode) + 1);
        /*int NE_NEActType_RouteCalculate = 5;
        int NE_NETransType_NormalMap = 0;
        byte bScale = 0;
        long lLongitude = 0;
        long lLatitude = 0;
        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(
                NE_NEActType_RouteCalculate, NE_NETransType_NormalMap, bScale,
                lLongitude, lLatitude);
        NaviRun.GetNaviRunObj().JNI_CT_ExposeMap();
        NaviRun.GetNaviRunObj().JNI_ct_ExposeMap();
        CommonLib.setIsStartCalculateRoute(true);*/
        this.setResult(Constants.RESULTCODE_ROUTE_DETERMINATION);
        this.finish();
    }

//Chg 2011/12/15 Z01_h_yamada Start -->
//    private void saveEditRouteData(String filePath, String strRouteDate) {
//--------------------------------------------
     private boolean saveEditRouteData(String filePath, String strRouteDate) {
//Chg 2011/12/15 Z01_h_yamada End <--
        RouteDataFile routeDataFile = new RouteDataFile();
        List<RouteData> routeDataList = routeDataFile.readData(filePath);

        //編集するデータは削除
        for (int i = 0; i < routeDataList.size(); i++) {
            RouteData routeData = routeDataList.get(i);
            if (strRouteDate != null && routeData.getRouteDate().equals(strRouteDate)) {
                routeDataList.remove(i);
                break;
            }
        }

        List<RouteData> newRouteDataList = new ArrayList<RouteData>();
        RouteData addRouteData = getAddRouteData();

        newRouteDataList.add(addRouteData);
        for (int i = 0; i < routeDataList.size(); i++) {
            newRouteDataList.add(routeDataList.get(i));
        }

//Chg 2011/12/15 Z01_h_yamada Start -->
//        routeDataFile.writeData(filePath, newRouteDataList);
//--------------------------------------------
        boolean bRet = routeDataFile.writeData(filePath, newRouteDataList);
        if ( bRet == false ) {
        	// 保存失敗
        	SaveFirstName = null;
        }
        return bRet;
//Chg 2011/12/15 Z01_h_yamada End <--
    }


    /**
     * 戻った該当画面の処理
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //yangyang add WaitDialog start
    	bIsListClicked = false;
        closeWarningDialog();
        //yangyang add WaitDialog end
        RouteNodeData routeNodeDataNew = new RouteNodeData();
        if (resultCode == RESULT_CANCELED) {
            // xuyang add start #1114
            lstAddBean = getAdapterList(lstRouteNodeDate);
            oBox.reset();
            // xuyang add end #1114
            return;
        }
        switch (requestCode) {

            case AppInfo.ID_ACTIVITY_ROUTE_NAME_EDIT:
                routeName = data.getStringExtra(Constants.ROUTE_ROUTE_NAME_KEY);
//Chg 2011/12/15 Z01_h_yamada Start -->
//                if (routeType.equals(Constants.ROUTE_TYPE_EDIT)) {
//                    // 保存のルート
//                    String routeDate = (String)bundle.get("ItemData");
//                    saveEditRouteData(filePath, routeDate);
//                } else {
//                    if (SaveFirstName != null) {
//                        saveEditRouteData(filePath, SaveFirstName);
//                    } else {
//                        saveEditRouteData(filePath, null);
//                    }
//                    lstRouteNodeDateOldSave = new ArrayList<RouteNodeData>();
//                    for (int i = 0; i < lstRouteNodeDate.size(); i++) {
//                        RouteNodeData RNodedata = new RouteNodeData();
//                        RNodedata = lstRouteNodeDate.get(i);
//                        lstRouteNodeDateOldSave.add(RNodedata);
//                    }
//                }
//
//                lstRouteNodeDateSave = new ArrayList<RouteNodeData>();
//                for (int i = 0; i < lstRouteNodeDate.size(); i++) {
//                    lstRouteNodeDateSave.add(i, lstRouteNodeDate.get(i));
//                }
//--------------------------------------------
                boolean bRet;
                if (routeType.equals(Constants.ROUTE_TYPE_EDIT)) {
                    // 保存のルート
                    String routeDate = (String)bundle.get("ItemData");
                    bRet = saveEditRouteData(filePath, routeDate);
                    if (bRet == false) {
                    	showDialog(Constants.DIALOG_FILE_WRITE_FAILED);
                     	return ;
                    }
                } else {
                    if (SaveFirstName != null) {
                    	bRet = saveEditRouteData(filePath, SaveFirstName);
                    } else {
                    	bRet = saveEditRouteData(filePath, null);
                    }
                    if (bRet == false) {
                    	showDialog(Constants.DIALOG_FILE_WRITE_FAILED);
                     	return ;
                    }
                    lstRouteNodeDateOldSave = new ArrayList<RouteNodeData>();
                    for (int i = 0; i < lstRouteNodeDate.size(); i++) {
                        RouteNodeData RNodedata = new RouteNodeData();
                        RNodedata = lstRouteNodeDate.get(i);
                        lstRouteNodeDateOldSave.add(RNodedata);
                    }
                }

                lstRouteNodeDateSave = new ArrayList<RouteNodeData>();
                for (int i = 0; i < lstRouteNodeDate.size(); i++) {
                    lstRouteNodeDateSave.add(i, lstRouteNodeDate.get(i));
                }

                showDialog(DIALOG_ROUTE_SAVE_OK);
//Chg 2011/12/15 Z01_h_yamada End <--
                break;

            case AppInfo.ID_ACTIVITY_ROUTE_ADD:
                if (data != null) {
                    String routeAddName = data.getStringExtra(Constants.POINT_NAME);
                    if (null == routeAddName
                            || "".equals(routeAddName.trim())) {
                        routeAddName = getResources().getString(R.string.name_null);
                    }
                    routeNodeDataNew.setName(routeAddName);
                    routeNodeDataNew.setLon("" + data.getLongExtra(Constants.INTENT_LONGITUDE, 0));
                    routeNodeDataNew.setLat("" + data.getLongExtra(Constants.INTENT_LATITUDE, 0));
                }
                // 地点追加の場合
                routeNodeDataNew.setDate("");
                routeNodeDataNew.setStatus("0");
                routeNodeDataNew.setMode("0");
                // xuyang add start #1114
//Chg 2011/10/15 Z01_h_yamada Start -->
//                if (lstRouteNodeDate.size() == 7) {
//                    lstRouteNodeDate.remove(addItemIndex);
//                }
//--------------------------------------------
                if ( lstRouteNodeDate.size() == 7 && addItemIndex < lstRouteNodeDate.size() ) {
                    lstRouteNodeDate.remove(addItemIndex);
                }
//Chg 2011/10/15 Z01_h_yamada End <--

                // xuyang modi start #980
                if (!addButtonFromAddflag) {
                    if (addItemIndex >= lstRouteNodeDate.size()) {
                        if (lstRouteNodeDate.size() == 1 || lstRouteNodeDate.size() == 2) {
                            addItemIndex = 1;
                        } else {
                            addItemIndex = lstRouteNodeDate.size() - 1;
                        }
                    }
                } else {
                    addButtonFromAddflag = false;
                }
                // xuyang modi end #980
                addItemFlag = true;
                modifyItemFlag = false;
                // xuyang add end #1114
                lstRouteNodeDate.add(addItemIndex, routeNodeDataNew);
                lstAddBean = getAdapterList(lstRouteNodeDate);
                oBox.reset();
                break;
            case AppInfo.ID_ACTIVITY_ROUTE_MODIFY:
                if (data != null) {
                    String routeModifyName = data.getStringExtra(Constants.POINT_NAME);
                    if (null == routeModifyName
                            || "".equals(routeModifyName.trim())) {
                        routeModifyName = getResources().getString(R.string.name_null);
                    }
                    routeNodeDataNew.setName(routeModifyName);
                    routeNodeDataNew.setLon("" + data.getLongExtra(Constants.INTENT_LONGITUDE, 0));
                    routeNodeDataNew.setLat("" + data.getLongExtra(Constants.INTENT_LATITUDE, 0));
                }
                // xuyang add start #1114
                if (modifyItemIndex >= lstRouteNodeDate.size()) {
                    modifyItemIndex = lstRouteNodeDate.size() - 1;
                } else {
                    lstRouteNodeDate.remove(modifyItemIndex);
                }
                modifyItemFlag = true;
                addItemFlag = false;
                // xuyang add end #1114
                // 地点修正の場合
                // xuyang del start #1114
//			lstRouteNodeDate.remove(modifyItemIndex);
                // xuyang del end #1114
                routeNodeDataNew.setDate("");
                routeNodeDataNew.setStatus("0");
                routeNodeDataNew.setMode("0");
                lstRouteNodeDate.add(modifyItemIndex, routeNodeDataNew);
                lstAddBean = getAdapterList(lstRouteNodeDate);
                oBox.reset();
                break;
// Add by CPJsunagawa '2015-07-08 Start
        	case AppInfo.ID_ACTIVITY_REGIST_ICON:
            	System.out.println("*** RouteEdit onActivityResult Case: ID_ACTIVITY_REGIST_ICON ***");
        	    break;
        	case AppInfo.ID_ACTIVITY_INPUT_TEXT:
            	System.out.println("*** RouteEdit onActivityResult Case: ID_ACTIVITY_INPUT_TEXT ***");
        	    break;
// Add by CPJsunagawa '2015-07-08 Start
        }
//Add 2011/09/21 Z01yoneya Start -->
        if (resultCode == Constants.RESULTCODE_GOBACK_TO_ROUTE_EDIT) {
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
//Add 2011/09/21 Z01yoneya End <--
    }

    // xuyang add start #1114
    protected void goBack() {
        if (CommonLib.routeSearchFlag) {
            addItemFlag = true;
//Del 2012/03/13 Z01_h_fukushima(No.405) Start --> ref 2510 案内中画面での地点追加
//            CommonLib.routeSearchFlag = false;
//Del 2012/03/13 Z01_h_fukushima(No.405) End <-- ref 2510
        }
        if (routeType.equals(Constants.ROUTE_TYPE_NOW) && (addItemFlag == true || modifyItemFlag == true)) {
            int oldListSize = lstRouteNodeDate.size();
            lstRouteNodeDate = new ArrayList<RouteNodeData>();
            for (int i = 0; i < lstRouteNodeDateSave.size(); i++) {
                lstRouteNodeDate.add(i, lstRouteNodeDateSave.get(i));
            }
            int newListSize = lstRouteNodeDate.size();

            if (addItemFlag) {
                if (lstRouteNodeDate.size() == 7) {
                    addItemIndex = 1;
                } else {
                    //保存の場合、上の次追加の以下追加
                    if (oldListSize == newListSize) {
                        addItemIndex = addItemIndex + 1;
                    }
                }
                CommonLib.setChangePosFromSearch(true);
            } else {
                CommonLib.setChangePosFromSearch(false);
            }
            if (modifyItemFlag) {
                if (oldListSize != newListSize) {
                    modifyItemIndex = lstRouteNodeDate.size();
                }
                CommonLib.setChangePos(true);
            } else {
                CommonLib.setChangePos(false);
            }
//Chg 2012/03/13 Z01_h_fukushima(No.405) Start --> #2510 案内中画面での地点追加
//          POIData poi = new POIData();
//           OpenMap.moveMapTo(this, poi, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
//----------------------------------------------------------------------
           finish();
//Chg 2012/03/13 Z01_h_fukushima(No.405) End <--
        } else {
            super.goBack();
        }
    }

    // xuyang add end #1114

    @Override
    protected void onDestroy() {
        //1128 start
        Intent intent = new Intent();
        intent.putExtra(Constants.ROUTE_FLAG_KEY, 0);
        //1128 end
        super.onDestroy();
        CommonLib.setChangePos(false);
        CommonLib.setChangePosFromSearch(false);
    }

    //yangyang add WaitDialog start
    @Override
    protected boolean onStartShowPage() throws Exception {
        return false;
    }

    @Override
    protected void onFinishShowPage(boolean bGetData) throws Exception {
    }
    //yangyang add WaitDialog end
}
