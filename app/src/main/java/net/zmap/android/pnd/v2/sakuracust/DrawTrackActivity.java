/**
 * 
 */
package net.zmap.android.pnd.v2.sakuracust;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.sql.SQLException;
import java.util.Calendar;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.DeliveryInfo;
import net.zmap.android.pnd.v2.common.TrackCommon;
import net.zmap.android.pnd.v2.common.utils.StorageUtil;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.inquiry.activity.VehicleInquiry;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.common.utils.DbHelper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.text.SpannableStringBuilder;     // 2015/01/07 Yamamoto Add

/**
 * @author cpjsuna
 *
 */
public class DrawTrackActivity extends Activity {

	//private static VehicleInquiry       mVehicleInquiry;

    public static String[] mTrackdata;
	public static String mCurrentCourse = "";

    // Naviからの値(保存先指定用)
    private static String mFilePath = ""; // ファイルPath
	
    private static int nMaxCount;    // レコードMAXカウント
    private static int nCurIndex;    // カレントINDEX
    
//    MapActivity mapActivity = NaviRun.GetNaviRunObj().getMapActivity();
    public static MapActivity mapActivity = NaviRun.GetNaviRunObj().getMapActivity();

    private static TrackCommon common;
    //public static TrackCommon common;
    
    private static String[] strTrackData;

    private static int nCancelFlag;    // コース名入力キャンセルフラグ

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

    	System.out.println("DrawTrackActivity onCreate");

    	VehicleInquiry vi = new VehicleInquiry();
    	
    	// BUG なぜか先頭のコース名しか取れない？
        String strCourse = vi.getNowCourseName();
        mCurrentCourse = strCourse;
        
        // グローバル変数を扱うクラスを取得する
        common = (TrackCommon)getApplication();
        
        // グローバル変数を初期化する
        common.init();

        System.out.println("DrawTrackActivity mCurrentCourse => " + mCurrentCourse);
    }
        
     /**
	 * 任意の軌跡履歴の出力開始
	 * @param csvFileName CSVファイル名
	 * @param dbFileName  SQLiteファイル名
	 * @return 成功/失敗
	 */
//	public final int ExecuteTrackSaveCSV()
	public static final int ExecuteTrackSaveCSV(final Context ctx)
	{
    	String strDir = null;
    	//final String strCourse = null;

    	System.out.println("DrawTrackActivity Into ExecuteTrackSaveCSV ");
    	
        LayoutInflater oInflater = LayoutInflater.from(ctx);
        // 画面を追加
        final LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.map_left, null);

    	// 保存するコース名の入力を促す
        //テキスト入力を受け付けるビューを作成します。
        final EditText editView = new EditText(ctx);
    	AlertDialog.Builder alertDlg = new AlertDialog.Builder(ctx);
    	alertDlg.setIcon(android.R.drawable.ic_dialog_info);
        alertDlg.setTitle("コース名を入力してください");
        //setViewにてビューを設定します。
        alertDlg.setView(editView);
        alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            	System.out.println("DrawTrackActivity ExecuteTrackSaveCSV OK Click");
                //入力した文字をトースト出力する
                //Toast.makeText(ctx, editView.getText().toString(), Toast.LENGTH_LONG).show();
            	mCurrentCourse = editView.getText().toString();
            	System.out.println("DrawTrackActivity Dialog Input =>" + mCurrentCourse);

                // 大本の軌跡データファイルを削除する
                NaviRun.GetNaviRunObj().JNI_NE_ClearVehicleTrack();

            	// 走行軌跡保存開始
                NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(1);
//                NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(1);

                NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
                //NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();

            }
        });
        alertDlg.setNegativeButton("キャンセル", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            	mCurrentCourse = null;
            	System.out.println("DrawTrackActivity ExecuteTrackSaveCSV Cancel Click");
            }
        });
        //alertDlg.show();
    	System.out.println("DrawTrackActivity ExecuteTrackSaveCSV alertDlg.create().show() Before");
        alertDlg.create().show();

        if(mCurrentCourse == null) {
        	System.out.println("DrawTrackActivity ExecuteTrackSaveCSV mCurrentCourse is NULL return");
        	return 1;
        }
    	System.out.println("DrawTrackActivity ExecuteTrackSaveCSV mCurrentCourse is not NULL =>" + mCurrentCourse);

    	return 0;
	}

	/**
	 * 任意の軌跡履歴の出力終了
	 * @param csvFileName CSVファイル名
	 * @param dbFileName  SQLiteファイル名
	 * @return 成功/失敗
	 */
	public final static int ExecuteTrackExportCSV(Context ctx, String strCouseName)
	{
    	String strDir = null;
    	
    	System.out.println("DrawTrackActivity ExecuteTrackExportCSV ");

    	// コース名が入力されているか確認する
//        if(mCurrentCourse == null || mCurrentCourse == "") {
        if(strCouseName == null || strCouseName == "") {
        	AlertDialog.Builder alertDlg = new AlertDialog.Builder(ctx);
            alertDlg.setTitle("軌跡保存");
            alertDlg.setMessage("保存する軌跡データ名称が入力されていません");
            alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OK ボタンクリック処理
                }
            });
	        // 表示
	        alertDlg.show();
        	return 1;
        }

        // 走行軌跡保存終了
        NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(0);
        
        // ワーク用の軌跡データファイルを保存する（一旦、軌跡ファイルを閉じる）
        NaviRun.GetNaviRunObj().JNI_NE_SaveVehicleTrack();

        // 地図画面の再表示
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
        //NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();

        // 保存が終了したらDBを更新する
        nMaxCount = 0;
		nCurIndex = 1;
		
        String strCourse = strCouseName;

        final Calendar calendar = Calendar.getInstance();

        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH) + 1;  // 月は0始まりなので+1する
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);
        final int second = calendar.get(Calendar.SECOND);
        final int ms = calendar.get(Calendar.MILLISECOND);

        String strKind;
//        strKind = String.valueOf(year) + String.valueOf(month) + String.valueOf(day) + String.valueOf(hour) + String.valueOf(minute) + String.valueOf(second) + String.valueOf(ms);
        strKind = String.format("%1$04d",year) + String.format("%1$02d",month) + String.format("%1$02d",day) + String.format("%1$02d",hour) + String.format("%1$02d",minute) + String.format("%1$02d",second);
        
        // 軌跡表示が終了したら、出力されたファイルを軌跡保存用フォルダにリネームしてコピーする
        // 保存先フォルダ
        String saveDir = null;
        String saveFile = null;
        saveDir = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) + "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "track";
//        saveDir = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) + "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "TMP/VT/TravelTrack2.dat";
        System.out.println("saveDir => " + saveDir );
        
        // フォルダの存在チェック
        File file = new File(saveDir);
        if (file.exists() == false){
        	// 無ければ作成する
        	System.out.println("軌跡データ格納フォルダを作成");
        	boolean result = file.mkdirs();
        	if(result){
        		System.out.println("軌跡データ格納フォルダを作成：成功");
        	}
        }
        
        saveFile = saveDir + "/TravelTrack" + strKind + ".dat";

        // 所定の軌跡データ格納先にデータが存在するか確認
        String stdDataFile = null;
        stdDataFile = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) + "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "TMP/VT/TravelTrack.dat";
//        stdDataFile = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) + "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "TMP/VT2/TravelTrack.dat";
        
        File file2 = new File(stdDataFile);  
        boolean isExists = file2.exists();
        if(isExists == false) {
        	AlertDialog.Builder alertDlg = new AlertDialog.Builder(ctx);
            alertDlg.setTitle("軌跡保存");
            alertDlg.setMessage("軌跡データが存在しません");
            alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OK ボタンクリック処理
                }
            });
	        // 表示
	        alertDlg.show();
        	return 1;
        }
        
        // 出力した軌跡データを所定のフォルダにコピーする 
        long nRet = copyTrackFile(stdDataFile, saveFile);
        if(nRet != 0) {
        	AlertDialog.Builder alertDlg = new AlertDialog.Builder(ctx);
            alertDlg.setTitle("軌跡保存");
            alertDlg.setMessage("軌跡データの保存に失敗しました");
            alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OK ボタンクリック処理
                }
            });
	        // 表示
	        alertDlg.show();
        	return 1;
        }

        // DB登録
    	String strSQL;
    	int nID = 0;
    	String strFilename = null;
        strDir = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) +
                "/" +NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "track/" + nCurIndex;
        System.out.println("saveDir => " + strDir );
        strFilename = strDir + "/TravelTrack.dat";
        
        strSQL = "INSERT INTO TrackData (CourseName, SaveDir, FileName) VALUES(" + "'" + strCourse + "', '" + strDir + "', '" + saveFile + "');";
    	System.out.println("DrawTrackActivity ExecuteTrackSaveCSV strSQL => " + strSQL);

        // 軌跡データ情報をDBに保存する
        DbHelper helper = new DbHelper(ctx);
        SQLiteDatabase db = helper.getWritableDatabase();

        db.execSQL(strSQL);
        db.close();
        helper.close();

        // 保存したらワーク用の軌跡データファイルをオープンする
        NaviRun.GetNaviRunObj().JNI_NE_AgainOpenVehicleTrack();

		return 0;
		
	}

	/**
	 * 選択されているコースの軌跡履歴を全て消去する
	 * @param csvFileName CSVファイル名
	 * @param dbFileName  SQLiteファイル名
	 * @return 成功/失敗
	 */
	public final int ExecuteTrackClear(final String filePath)
	{

    	System.out.println("DrawTrackActivity ExecuteTrackClear ");

		// 該当するコースの軌跡データファイルを削除する
        File file = new File(filePath);  
        boolean isExists = file.exists();
        if(isExists == true) {
			file.delete();
        } else {
        	return -1;
        }

        // 大本の軌跡データファイルを削除する
        NaviRun.GetNaviRunObj().JNI_NE_ClearVehicleTrack();

        System.out.println("TrackIconEdit ExecuteDeleteFigureData End");
        
		return 0;
	}

// Add by CPJsunagawa 2015-07-24 Start
    /**
     * 任意の軌跡データをクリアする
     */
    public final int executeTrackDataClear(final String filePath) {
        try {
        	System.out.println("DrawTrackActivity executeTrackDataClear ");

        	File file = new File(filePath);

            if (file.exists()) {

                // 確認ダイアログの生成
                //AlertDialog.Builder alertDlg = new AlertDialog.Builder(this);
            	AlertDialog.Builder alertDlg = new AlertDialog.Builder(DrawTrackActivity.this);
                alertDlg.setTitle(R.string.btn_additional_track_log_clear);
                String strMsg;
                strMsg = "コース：" + "コース１" + "の軌跡データを削除します。よろしいですか？";
                alertDlg.setMessage(strMsg);
                alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // OK ボタンクリック処理

                        	// 軌跡の消去
                        	ExecuteTrackClear(filePath);
                            NaviRun.GetNaviRunObj();
                            return;
                        	
                        }
                    });
                alertDlg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Cancel ボタンクリック処理
                        }
                    });
                // 表示
                alertDlg.show();
                return 1;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
		return 0;
    }
// Add 2015-07-24 End

    // 2016/02/12 Yamamoto Add Start
    /**
     * 軌跡名称のチェック
     * 未入力または同一名称が存在する場合はfalse
     * @param  ctx コンテキスト
     * @return 成功/失敗
     */
    public static final boolean checkTrackCSVName(final Context ctx,  String strCourseName) {
        String strMsg = "";
        // 名称の未入力チェック
        if(strCourseName.length() == 0){
            strMsg = "保存する軌跡データ名称が入力されていません。";
        }
        // 既存データに同一名称が存在しないかをチェック
        else{
            // 入力されたコース名が既にDBに存在するか？
            DbHelper helper = new DbHelper(ctx);
            SQLiteDatabase db = helper.getReadableDatabase();

            // DBに登録されている軌跡データのコース名を取得
            String[] strTrackNames = helper.getTrackData(db);
            for(int i = 0; i < strTrackNames.length; i++ ) {
                if(strCourseName.equals(strTrackNames[i])){
                    strMsg = "同一のコース名称がすでに登録済みです。";
                }
            }
        }
        if(strMsg.length() != 0){
            AlertDialog.Builder alertDlg = new AlertDialog.Builder(ctx);
            alertDlg.setTitle("入力エラー");
            alertDlg.setMessage(strMsg);
            alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OK ボタンクリック処理 → 名称入力へ戻る
                    //ExecuteTrackSaveCSV(ctx);
                }
            });
            // 表示
            alertDlg.show();
            return false;
        }
        else{
            return true;
        }
    }
    // 2016/02/12 Yamamoto Add End

// Add 20151221 Start
    /**
     * 軌跡データ削除画面を表示する。
     * @param 
     * @param 
     */
    public static void executeClearTrackData(final Context ctx, DialogInterface.OnClickListener ocl)
    {
        //String[] strTrackdata;
        
    	System.out.println("*** DrawTrackActivity Into executeClearTrackData ***");

    	// 入力されたコース名が既にDBに存在するか？
        DbHelper helper = new DbHelper(ctx);
        SQLiteDatabase db = helper.getReadableDatabase();
    	
        // DBに登録されている軌跡データのコース名を取得
        strTrackData = helper.getTrackData(db);
    	int nMax = helper.getTrackDataCount(db);
		int i = 0;
    	String[] items = strTrackData;

    	db.close();
    	helper.close();

    	AlertDialog.Builder adb = new AlertDialog.Builder(ctx);
    	adb.setTitle("消去する軌跡データを選択");

    	adb.setItems(items, new DialogInterface.OnClickListener()
    	{
    		public void onClick(DialogInterface i, int which)
    		{
    			
    			//mIconDirection = which;
    	    	//System.out.println("*** executeSelectIcon mIconDirection => " + mIconDirection);
    	    	System.out.println("*** executeClearTrackData which => " + which);
    			// 選択された軌跡データの削除
	    		System.out.println("*** DrawTrackActivity Into executeClearTrackData : executeDisplayBySelectData Call ***");
	    		//startRegistIcon2(which);
	    		executeClearBySelectData(ctx, which);
    		}
    	}
    	);
    	adb.setPositiveButton(R.string.btn_cancel, ocl);
    	adb.show();
    }

    /**
     * 軌跡データ選択画面を表示する。
     * @param 
     * @param 
     */
    public static void executeSelectTrackData(final Context ctx, DialogInterface.OnClickListener ocl)
    {
        //String[] strTrackdata;
        
		System.out.println("*** DrawTrackActivity Into executeSelectTrackData ***");

		// 入力されたコース名が既にDBに存在するか？
        
//        DbHelper helper = new DbHelper(getApplicationContext());
        DbHelper helper = new DbHelper(ctx);
//        DbHelper helper = new DbHelper(DrawTrackActivity.this);
        SQLiteDatabase db = helper.getReadableDatabase();
    	
        // DBに登録されている軌跡データのコース名を取得
        strTrackData = helper.getTrackData(db);
    	int nMax = helper.getTrackDataCount(db);
		int i = 0;
    	String[] items = strTrackData;
    	db.close();
    	helper.close();
    	
    	AlertDialog.Builder adb = new AlertDialog.Builder(ctx);
        adb.setTitle("表示する軌跡データを選択");

    	adb.setItems(items, new DialogInterface.OnClickListener()
    	{
    		public void onClick(DialogInterface i, int which)
    		{
    			
    			//mIconDirection = which;
    	    	//System.out.println("*** executeSelectIcon mIconDirection => " + mIconDirection);
    	    	System.out.println("*** executeSelectTrackData which => " + which);
    			// 選択された軌跡データの表示
	    		System.out.println("*** DrawTrackActivity Into executeSelectTrackData : executeDisplayBySelectData Call ***");
	    		//startRegistIcon2(which);
// Mod 20151224 Start
	    		//executeDisplayBySelectData(ctx, which);
	    		executeDisplayBySelectData2(ctx, which);
// Mod 20151224 End
    		}
    	}
    	);
    	//adb.setPositiveButton(R.string.btn_cancel, null);
    	adb.setPositiveButton(R.string.btn_cancel, ocl);
    	adb.show();
    }

// 2016/01/07 Yamamoto Add Start
    /**
     * 軌跡データ名称変更画面を表示する。
     * @param
     * @param
     */
    public static void executeEditTrackData(final Context ctx, DialogInterface.OnClickListener ocl)
    {
        System.out.println("*** DrawTrackActivity Into executeEditTrackData ***");

        DbHelper helper = new DbHelper(ctx);
        SQLiteDatabase db = helper.getReadableDatabase();

        // DBに登録されている軌跡データのコース名を取得
        strTrackData = helper.getTrackData(db);
        int nMax = helper.getTrackDataCount(db);
        int i = 0;
        String[] items = strTrackData;

        db.close();
        helper.close();

        AlertDialog.Builder adb = new AlertDialog.Builder(ctx);
        adb.setTitle("名称変更する軌跡データを選択");

        adb.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface i, int which) {
                        System.out.println("*** executeEditTrackData which => " + which);
                        executeEditBySelectData(ctx, which);
                    }
                }
        );
        adb.setPositiveButton(R.string.btn_cancel, ocl);
        adb.show();
    }
// 2016/01/07 Yamamoto Add End

    /**
     * 選択された軌跡データを表示する。
     * @param 
     * @param 
     */
//    public static void executeDisplayBySelectData(Context ctx, final int which)
    public final static void executeDisplayBySelectData(Context ctx, final int which)
    {

		System.out.println("*** DrawTrackActivity Into executeDisplayBySelectData ***");
		
// Add 20151222 Start 表示の動作確認用
		
        // 固定で軌跡データを表示してみる
		String strFilePath = "/storage/emulated/0/ItsmoNaviDrive/TMP2/";
		NaviRun.GetNaviRunObj().JNI_NE_RestartOpenVehicleTrack(strFilePath);
    	// 軌跡データの表示フラグを設定する
        // 走行軌跡表示開始
        //NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(1);
        NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(1);
		if(strFilePath != ""){
			return;
		}
// Add 20151224 End
		
// Add 20151222 End 表示の動作確認用
		
		String strDir = null;
    	
    	// 選択されたコース名を保持する
    	String strCourse = strTrackData[which];
    	
    	System.out.println("executeDisplayBySelectData SelectCourse => " + strCourse);

    	DbHelper helper = new DbHelper(ctx);
    	SQLiteDatabase db = helper.getReadableDatabase();

    	//aaa
    	// 選択されたコース名から該当の軌跡データファイル名を取得する
    	String TrackFile[];
    	TrackFile = helper.getTrackFileByCourseName(db, strCourse); 
    	
    	// 選択されたコース名から該当の格納フォルダ名を取得する
    	String SaveDir[];
    	SaveDir = helper.getSaveDirByCourseName(db, strCourse); 
    	
    	db.close();
    	helper.close();

    	String TrackDataFile = TrackFile[0];
        
    	System.out.println("executeDisplayBySelectData TrackDataFile => " + TrackDataFile);

    	String stdDataFile = null;
        stdDataFile = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) + "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "TMP/VT/TravelTrack.dat";

        String stdDataFile2 = null;
        stdDataFile2 = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) + "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "track/TravelTrack2.dat";
        
        String stdDataFile3 = null;
        stdDataFile3 = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) + "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "TMP/VT2/TravelTrack.dat";

// Add 20151221 ファイルがロックされているか確認 Start
        FileOutputStream  fs = null;
        // stdDataFile(TMP/VT/TravelTrack.dat)のロック状態を調べる
        try {
            //FileChannel fs = null;
            File lockFile = new File(stdDataFile);
            lockFile.deleteOnExit();

            fs = new FileOutputStream(stdDataFile);
            FileChannel fc = fs.getChannel();

            FileLock lock = fc.tryLock();

            if (lock == null) {
                new RuntimeException("ロック中");
            }
            lock.release();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
            	fs.close();
            } catch (IOException e) {
            	e.printStackTrace();
            }
        }

        // stdDataFile3(TMP/VT2/TravelTrack.dat)のロック状態を調べる
        try {
            //FileChannel fs = null;
            File lockFile = new File(stdDataFile3);
            lockFile.deleteOnExit();

            fs = new FileOutputStream(stdDataFile3);
            FileChannel fc = fs.getChannel();

            FileLock lock = fc.tryLock();

            if (lock == null) {
                new RuntimeException("ロック中");
            }
            lock.release();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
            	fs.close();
            } catch (IOException e) {
            	e.printStackTrace();
            }
        }
// Add 20151221 ファイルがロックされているか確認 End
        
        File file = new File(TrackDataFile);  
        boolean isExists = file.exists();
        if(isExists == false) {
        	AlertDialog.Builder alertDlg = new AlertDialog.Builder(ctx);
            alertDlg.setTitle("軌跡選択");
            alertDlg.setMessage("選択された軌跡データが存在しません");
            alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OK ボタンクリック処理
                }
            });
	        // 表示
	        alertDlg.show();
        	return;
        }

// Add 20151221 Start
        // ワーク用の軌跡データファイルを保存する（一旦、軌跡ファイルを閉じる）
        NaviRun.GetNaviRunObj().JNI_NE_SaveVehicleTrack();
// Add 20151221 End

        System.out.println("executeDisplayBySelectData srcChannel(2) => " + TrackDataFile);
    	System.out.println("executeDisplayBySelectData destChannel(2) => " + stdDataFile);

        // 出力した軌跡データを所定のフォルダにコピーする 
//        long nRet = copyTrackFile(TrackDataFile, stdDataFile2);
        long nRet = copyTrackFile(TrackDataFile, stdDataFile);
        if(nRet != 0) {
        	AlertDialog.Builder alertDlg = new AlertDialog.Builder(ctx);
            alertDlg.setTitle("軌跡保存");
            alertDlg.setMessage("軌跡データの保存に失敗しました");
            alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OK ボタンクリック処理
                }
            });
	        // 表示
	        alertDlg.show();
        	return;
        }

// Add 20151221 Start
        // TravelTrack2.datでコピーしてから、TravelTrack.datにリネームする
        //変更前ファイル名
        File fileA = new File(stdDataFile2);
        
        //変更後のファイル名
      File fileB = new File(stdDataFile3);
//      File fileB = new File("/storage/emulated/0/ItsmoNaviDrive/TMP/VT2/TravelTrack.dat");
        
        try {
	        if(fileA.renameTo(fileB)){
	           //ファイル名変更成功
	           System.out.println("ファイル名変更成功");
	        }else{
	           //ファイル名変更失敗
	           System.out.println("ファイル名変更失敗");
	        }
        } catch (Exception ex) {
        	System.out.println("Error!");
        }
// Add 20151221 Start
        
// Add 20151216 Start
        // 軌跡データを切り替えたら、ワーク用の軌跡データファイルをリロードする
        NaviRun.GetNaviRunObj().JNI_NE_AgainOpenVehicleTrack();
// Add 20151216 End

        // 走行軌跡表示のみ
        NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(1);
        //NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(1);

        // 地図画面の再表示
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
        //NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();
    }
// Add 20151221 End

    /**
     * 選択された軌跡データを表示する。
     * @param 
     * @param 
     */
//    public static void executeDisplayBySelectData(Context ctx, final int which)
    public final static void executeDisplayBySelectData2(Context ctx, final int which)
    {

		System.out.println("*** DrawTrackActivity Into executeDisplayBySelectData ***");
		
		String strDir = null;
    	
    	// 選択されたコース名を保持する
    	String strCourse = strTrackData[which];
    	
    	System.out.println("executeDisplayBySelectData SelectCourse => " + strCourse);

    	DbHelper helper = new DbHelper(ctx);
    	SQLiteDatabase db = helper.getReadableDatabase();

    	// 選択されたコース名から該当の軌跡データファイル名を取得する
    	String TrackFile[];
    	TrackFile = helper.getTrackFileByCourseName(db, strCourse); 
    	
    	// 選択されたコース名から該当の格納フォルダ名を取得する
    	String SaveDir[];
    	SaveDir = helper.getSaveDirByCourseName(db, strCourse); 
    	
    	db.close();
    	helper.close();

    	String TrackDataFile = TrackFile[0];
        
    	System.out.println("executeDisplayBySelectData TrackDataFile => " + TrackDataFile);

    	String stdDataFile = null;
        stdDataFile = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) + "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "TMP2/VT/TravelTrack.dat";

        String stdDataFile2 = null;
        stdDataFile2 = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) + "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME +  "/" + "TMP2/VT/TravelTrack2.dat";
        
        File file = new File(TrackDataFile);  
        boolean isExists = file.exists();
        if(isExists == false) {
        	AlertDialog.Builder alertDlg = new AlertDialog.Builder(ctx);
            alertDlg.setTitle("軌跡選択");
            alertDlg.setMessage("選択された軌跡データが存在しません");
            alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OK ボタンクリック処理
                }
            });
	        // 表示
	        alertDlg.show();
        	return;
        }

// Add 20151224 Start
        // ファイルのロックを解除するため、一旦VTプロセスを終わらせる
        NaviRun.GetNaviRunObj().JNI_NE_OptionalVTFinalize();
// Add 20151224 End

        // 出力した軌跡データを所定のフォルダにコピーする 
        long nRet = copyTrackFile(TrackDataFile, stdDataFile);
        if(nRet != 0) {
        	AlertDialog.Builder alertDlg = new AlertDialog.Builder(ctx);
            alertDlg.setTitle("軌跡データ選択");
            alertDlg.setMessage("軌跡データの選択に失敗しました");
            alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OK ボタンクリック処理
                }
            });
	        // 表示
	        alertDlg.show();
        	return;
        }

// Cut 20160212 Start　　試しに以下をコメントにしてみる　Start  ここをコメントにすると軌跡が表示できないのでコメントにしない
// Add 20151222 Start 表示の動作確認用
        // 選択された軌跡ファイルでVTイニシャライズを再起動
		NaviRun.GetNaviRunObj().JNI_NE_VT_Initialize_Cust(TrackDataFile);

		// 軌跡データの表示フラグを設定する
        // 走行軌跡表示開始
        NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(1);
        NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(1);
        
        // 地図画面の再表示
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
        //NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();

		if(TrackDataFile != ""){
			return;
		}
// Add 20151222 End 表示の動作確認用
// Cut 20160212 Start　　試しに以下をコメントにしてみる　End
		
    }
// Add 20151221 End
    
/**
     * 選択された軌跡データを削除する。
     * @param 
     * @param 
*/
    public final static void executeClearBySelectData(final Context ctx, final int which)
    {

		System.out.println("*** DrawTrackActivity Into executeClearBySelectData ***");
		
		String strDir = null;
    	
    	// 選択されたコース名を保持する
    	final String strCourse = strTrackData[which];
    	
    	System.out.println("executeClearBySelectData SelectCourse => " + strCourse);

    	DbHelper helper = new DbHelper(ctx);
    	SQLiteDatabase db = helper.getReadableDatabase();

    	// 選択されたコース名から該当の軌跡データファイル名を取得する
    	String TrackFile[];
    	TrackFile = helper.getTrackFileByCourseName(db, strCourse); 
    	
    	// 選択されたコース名から該当の格納フォルダ名を取得する
    	String SaveDir[];
    	SaveDir = helper.getSaveDirByCourseName(db, strCourse); 
    	
    	final String TrackDataFile = TrackFile[0];
        
    	System.out.println("executeClearBySelectData TrackDataFile => " + TrackDataFile);

    	File file = new File(TrackDataFile);  
        boolean isExists = file.exists();
        if(isExists == true) {
        	AlertDialog.Builder alertDlg = new AlertDialog.Builder(ctx);
            alertDlg.setTitle(R.string.btn_additional_track_log_clear);
            String strMsg = "選択された軌跡データ：【" + strCourse + "】を消去します。よろしいですか？";
            alertDlg.setMessage(strMsg);
            alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OK ボタンクリック処理
                    ClearBySelectDataAndDB(ctx, strCourse);
                    // 2016/01/07 Yamamoto Add Start
                    // 消去の際は、軌跡表示を一旦リセットする
                    NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(0);
                    // 2016/01/07 Yamamoto Add End
                }
            });
            // 2016/01/07 Yamamoto Add Start
            alertDlg.setNegativeButton("キャンセル", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            // 2016/01/07 Yamamoto Add End
	        // 表示
	        alertDlg.show();
        	return;
        }
    }
    
    /**
     * 選択された軌跡データをDB、格納フォルダから削除する。
     * @param 
     * @param 
*/
    public final static void ClearBySelectDataAndDB(Context ctx, String strCourse)
    {

		System.out.println("*** DrawTrackActivity Into ClearBySelectDataAndDB ***");
		
    	DbHelper helper = new DbHelper(ctx);
    	SQLiteDatabase db = helper.getReadableDatabase();

    	// 選択されたコース名から該当の軌跡データファイル名を取得する
    	String TrackFile[];
    	TrackFile = helper.getTrackFileByCourseName(db, strCourse); 
    	
    	String TrackDataFile = TrackFile[0];
        
    	System.out.println("executeClearBySelectData TrackDataFile => " + TrackDataFile);

        // 選択されたファイルを削除する
        File file2 = new File(TrackDataFile);
        try {
        	boolean result = file2.delete();
	        if(result != true) {
	           //ファイル削除失敗
	           System.out.println("ファイル削除失敗");
	        } else {
	           //ファイル削除成功
	           System.out.println("ファイル削除成功");
	        }
        } catch (Exception ex) {
        	System.out.println("file2.delete()　Error!");
        }

    	// 選択されたコースのレコードを削除する
    	helper.deleteTrackData(db, strCourse); 
    	
    	db.close();
    	helper.close();
    }

// 2016/01/07 Yamamoto Add Start
    /**
     * 選択された軌跡データを名称変更する。
     * @param
     * @param
     */
    public final static void executeEditBySelectData(final Context ctx, final int which)
    {

        System.out.println("*** DrawTrackActivity Into executeEditBySelectData ***");

        // 選択されたコース名を保持する
        final String strCourse = strTrackData[which];

        System.out.println("executeEditBySelectData SelectCourse => " + strCourse);

        DbHelper helper = new DbHelper(ctx);
        SQLiteDatabase db = helper.getReadableDatabase();

        // 選択されたコース名から該当の軌跡データファイル名を取得する
        String TrackFile[];
        TrackFile = helper.getTrackFileByCourseName(db, strCourse);

        final String TrackDataFile = TrackFile[0];

        // 入力ダイアログを表示
        //テキスト入力を受け付けるビューを作成します。
        final EditText editView = new EditText(ctx);
        editView.setText(strCourse);
        editView.selectAll();
        AlertDialog.Builder alertDlg = new AlertDialog.Builder(ctx);
        //alertDlg.setIcon(android.R.drawable.ic_dialog_info);
        alertDlg.setTitle(R.string.btn_additional_trackedit);
        alertDlg.setView(editView)
            // OK ボタンクリック処理
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    SpannableStringBuilder sb = (SpannableStringBuilder)editView.getText();
                    String strCourseAfter = sb.toString();
                    //DBのデータをUPDATE
                    EditBySelectDataAndDB(ctx, strCourse, strCourseAfter);
                }
            })
            // キャンセル ボタンクリック処理
                .setNegativeButton("キャンセル", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
        ;

        // 表示
        alertDlg.show();
        return;
    }

    /**
     * 選択された軌跡データの名称を変更する。
     * @param
     * @param
     */
    public final static void EditBySelectDataAndDB(Context ctx, String strCourse, String strCourseAfter)
    {
        DbHelper helper = new DbHelper(ctx);
        SQLiteDatabase db = helper.getReadableDatabase();

        // 選択されたコースの名称を変更する
        boolean blnRet = helper.updateTrackData(db, strCourse, strCourseAfter);
        db.close();
        helper.close();

        AlertDialog.Builder alertDlg = new AlertDialog.Builder(ctx);
        alertDlg.setTitle(R.string.btn_additional_trackedit);
        String strMsg = "軌跡データ名称は【" + strCourseAfter + "】に変更されました。";
        if (!blnRet) {
            strMsg = "軌跡データ名称は変更できませんでした。";
        }
        alertDlg.setMessage(strMsg);
        alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDlg.show();
        return;
    }
// 2016/01/07 Yamamoto Add End

// Add by CPJsunagawa '2015-07-28 Start
    /**
     * 軌跡保存用DB用Helper
     */
    public static class DbHelper extends SQLiteOpenHelper {
    	
    	public final static String navidatapath = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) +
             "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME;
        public final static String DATABASE_NAME = navidatapath + "/track_data.db";

        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // DBが存在しない時にのみ呼ばれる
            db.execSQL("CREATE TABLE TrackData  (_id INTEGER PRIMARY KEY, CourseName TEXT, SaveDir TEXT, FileName TEXT );");
            // デフォルト値をInsert
/*
            db.execSQL("INSERT INTO FrequencyWords VALUES(1, '見込み');");
            db.execSQL("INSERT INTO FrequencyWords VALUES(2, '継続');");
            db.execSQL("INSERT INTO FrequencyWords VALUES(3, '解約');");
*/
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // DBの変更があれば、ここで行う
        }
        
        // DBに登録されているレコード数を取得する
        public static int getTrackDataCount(SQLiteDatabase db) {
            Cursor cur = db.rawQuery("SELECT * FROM TrackData", null);
            if( cur == null ) return 0;
            int num = cur.getCount();
            cur.close();
            return num;
        }

        // DBに登録されているコース名を取得する
        public static String[] getTrackData(SQLiteDatabase db) {
            Cursor cur = db.rawQuery("SELECT CourseName FROM TrackData", new String[] {});
            if( cur == null ) return null;
            int num = cur.getCount();
            String courseName[] = new String[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	courseName[i] = cur.getString(0);
                cur.moveToNext();
            }
            cur.close();
            return courseName;
        }

        // DBに登録されているインデックスを取得する
        public int[] getTrackDataIndex(SQLiteDatabase db) {
        	String  szSql = "SELECT `_id` FROM TrackData";
            Cursor cur = db.rawQuery(szSql, null);
            if( cur == null ) return null;
            int num = cur.getCount();
            int nIndex[] = new int[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	nIndex[i] = cur.getInt(0);
                cur.moveToNext();
            }
            cur.close();
            return nIndex;
        }

        // DBに登録されている軌跡データファイル名を取得する
        public String[] getTrackDataFileName(SQLiteDatabase db) {
            Cursor cur = db.rawQuery("SELECT FileName FROM TrackData", new String[] {});
            if( cur == null ) return null;
            int num = cur.getCount();
            String FileName[] = new String[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	FileName[i] = cur.getString(0);
                cur.moveToNext();
            }
            cur.close();
            return FileName;
        }
        // コース名から該当の軌跡データファイル名を返却する
        public String[] getTrackFileByCourseName(SQLiteDatabase db, String CourseName) {
            Cursor cur = db.rawQuery("SELECT * FROM TrackData", new String[] {});
            //Cursor cur = db.rawQuery("SELECT FileName FROM TrackData", new String[] {});
            if( cur == null ) return null;
            int num = cur.getCount();
            String FileName[] = new String[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	String strWork;
            	strWork = cur.getString(0);		// _ID
            	strWork = cur.getString(1);		// コース名
            	strWork = cur.getString(2);		// 格納フォルダ
            	strWork = cur.getString(3);		// ファイル名
//            	if(CourseName == cur.getString(1)) {
               	if(CourseName.equals(cur.getString(1))) {
            		FileName[0] = cur.getString(3);
            		break;
            	}
                cur.moveToNext();
            }
            cur.close();
            return FileName;
        }

        // コース名から該当の格納フォルダを返却する
        public String[] getSaveDirByCourseName(SQLiteDatabase db, String CourseName) {
            Cursor cur = db.rawQuery("SELECT * FROM TrackData", new String[] {});
            if( cur == null ) return null;
            int num = cur.getCount();
            String SaveDir[] = new String[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
               	if(CourseName.equals(cur.getString(1))) {
            		SaveDir[0] = cur.getString(2);
            		break;
            	}
            	cur.moveToNext();
            }
            cur.close();
            return SaveDir;
        }

        // 指定されたコース名に該当するレコードを削除する
        public void deleteTrackData(SQLiteDatabase db, String CourseName) {
        	
            // DB更新（軌跡データ削除）
     		StringBuffer sb = new StringBuffer();
     		sb.append("DELETE FROM TrackData WHERE CourseName = '");
     		sb.append(CourseName + "'");
            
           	System.out.println("DrawTrackActivity deleteTrackData strSQL => " + sb.toString());

           	db.execSQL(sb.toString());

           	return;
        }

        // 2016/01/07 Yamamoto Add End
        // 指定されたコース名に該当するレコードを指定された名称で変更する
        public boolean updateTrackData(SQLiteDatabase db, String CourseName, String CourseNameAfter){

            // DB更新（軌跡データ名称変更）
            StringBuffer sb = new StringBuffer();
            sb.append("UPDATE TrackData SET CourseName = '");
            sb.append(CourseNameAfter + "' ");
            sb.append("WHERE CourseName = '");
            sb.append(CourseName + "'");

            try{
                db.execSQL(sb.toString());
            }catch (Exception ex){
                return false;
            }
            return true;
        }
        // 2016/01/07 Yamamoto Add End
    }
// Add by CPJsunagawa '2015-07-28 End
    
	private static int getTrackDataCount(SQLiteDatabase db) {
		// TODO 自動生成されたメソッド・スタブ
    
		Cursor cur = db.rawQuery("SELECT * FROM TrackData", null);
        if( cur == null ) return 0;
        int num = cur.getCount();
        cur.close();
        return num;
	}

    /**
     * 標準の軌跡データファイルを任意の軌跡データファイルの格納先にコピーする
     */
    public void executeCopyTrackData(String filePath) {
        
        // 格納先のフォルダが無ければ生成する
        NaviApplication app = (NaviApplication)getApplication();
        File file = new File(app.getNaviAppDataPath().getNaviAppRootPath() + filePath );
        file.mkdirs();
		System.out.println("file ==> " + file);
        // 軌跡データが存在するか
        if( file.exists() ) { // 既に軌跡データが存在するか？
        	if(file.isDirectory() == false) {
        		System.out.println("filePath => " + filePath + "は存在する");
        		//executeLoad(mFilePath);
        	} else {
        		System.out.println("filePath => " + filePath + "は存在しない");
                //finish();
        	}
        }
    }

    /**
     * 標準の軌跡データファイルと指定された任意の軌跡データファイルを差し替える
     */
    public void executeChangeTrackData(String filePath) {
        
    	// 指定されたファイルを、標準の軌跡データファイルと差し替える
    	
    }
    
    //
    // Add 20151222 ファイルコピーの共通関数
    //
	private static int copyTrackFile(String srcFile, String dstFile) {
	
        FileChannel srcChannel = null;
        FileChannel destChannel = null;

        try {
            srcChannel = new FileInputStream(srcFile).getChannel();
            destChannel = new FileOutputStream(dstFile).getChannel();

            srcChannel.transferTo(0, srcChannel.size(), destChannel);

        } catch (IOException e) {
            e.printStackTrace();
    		return 1;

        } finally {
            if (srcChannel != null) {
                try {
                    srcChannel.close();
                } catch (IOException e) {
            		return 1;
                }
            }
            if (destChannel != null) {
                try {
                    destChannel.close();
                } catch (IOException e) {
            		return 1;
                }
            }
        }
		return 0;
	}

}
