package net.zmap.android.pnd.v2.maps;

/**
 *
 * A interface used for Listening Map mode、position、state changes.
 * @author wangdong
 *
 */
public interface MapListener {

    /**
     * called after map is drawn.
     */
    public void onMapCreated();

    /**
     *called when ui mode is changed.
     */
    public void onMapUIChange();

    /**
     * called after map moved.
     */
    public void onMapMove();
}
