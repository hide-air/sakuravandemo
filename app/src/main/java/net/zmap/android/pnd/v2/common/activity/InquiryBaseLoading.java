//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.common.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.common.view.WaitDialog;
import net.zmap.android.pnd.v2.data.JNILong;

import java.util.ArrayList;

/**
 * 検索クラスのベースクラス
 *
 * */
public abstract class InquiryBaseLoading extends MenuBaseActivity implements Runnable, OnCancelListener {

    //RDBへ切替して、Event処理の必要がないから、削除する
    private static final int POI_SUCCESS = 0x10091001;

    /** 対象「hashSelectedPoi」のIndex　経度 */
    public static int INDEX_0 = 0;//LONG
    /** 対象「hashSelectedPoi」のIndex　緯度 */
    public static int INDEX_1 = 1;//LAT
    /** 対象「hashSelectedPoi」のIndex　名称 */
    public static int INDEX_2 = 2;//name
    /** 対象「hashSelectedPoi」の長さ */
    public static int STRINGLEN = 3;
    /** 戻る処理ため、全部クリックした情報を保存する */
    public static ArrayList<String[]> hashSelectedPoi = new ArrayList<String[]>();
    /** データがないダイアログのID */
    public static int DIALOG_NODATA_ALARM = 0x01;
    public static int TYPE_DEFAULT = 0;
    public static int TYPE_MYTEL = 1;//自番号
    public static int TYPE_TELNUMS = 2;//正常のアドレス帳
    public static int NOTSHOWCANCELBTN = 0;
    public static int SHOWCANCELBTN = 1;
    public int isShowCancelBtn = 0;

 // Add 2011/10/03 katsuta Start -->
	protected final int MAX_ROUTE_POINUM=500;
// Add 2011/10/03 katsuta End <--

//Add 2012/02/07 katsuta Start --> #2698
	protected boolean bIsListClicked = false;
//Add 2012/02/07 katsuta End <-- #2698

//	public static int TYPE_ADDRESS = 3;
//	public static int TYPE_FREEWORDSEARCH = 4;
    //RDBへ切替して、Event処理の必要がないから、削除する
    public void SrchProc(int msg1, int msg2) {
        if (msg2 == POI_SUCCESS) {
            search();
        }
    }

    public void SrchJUMPProc(int msg1, int msg2) {
        if (msg2 == POI_SUCCESS) {
            searchJUMP();
        }
    }

    /**
     * 市区町村画面にEventを戻してから、コールした関数
     * */
    protected void searchJUMP() {
    };

    /**
     * 検索処理を行う
     *
     * */
    protected void search() {
    };

    /** カレントクエリーを取消すかを標識する */
    private boolean m_bIsCancel = false;
    /** ダウンロードスレッド */
    private Thread m_oThread = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        m_bIsCancel = false;
//		Constants.GenreFlag = false;
//Add 2012/02/07 katsuta Start --> #2698
		bIsListClicked = false;
//Add 2012/02/07 katsuta End <-- #2698
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent oIntent) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"resultCode====requestCode=" + resultCode + ",===" + requestCode + ",oIntent==" + (oIntent == null));
        if (resultCode == Activity.RESULT_CANCELED) {
            cancel();
//			if (m_oThread != null) {
////				m_oThread.interrupt();
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"resultCode====m_oThread != null");
//				m_oThread.stop();
//			}
//			removeDialog(Constants.DIALOG_WAIT);
//			if (oWarningDialog != null) {
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"resultCode====oWarningDialog != null");
//				oWarningDialog.cancel();
//			}
        }
//Add 2012/02/07 katsuta Start --> #2698
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"InquiryBaseLoading onActivityResult resultCode====" + resultCode);
  		bIsListClicked = false; // フラグ初期化
//Add 2012/02/07 katsuta  End <-- #2698

        super.onActivityResult(requestCode, resultCode, oIntent);
    }

    /**
     * 500件を表示するため、ページIndexと表示できる総件数を初期化する
     *
     * @param RecCount
     *            JNIから取得した総件数
     * @return int 表示できる総件数
     * @author yangyang
     *
     * */
    public static int initPageIndexAndCnt(JNILong RecCount) {
        int allRecordCount = 0;
        if (RecCount.lcount >= Constants.MAX_RECORD_COUNT) {
            allRecordCount = (int)Constants.MAX_RECORD_COUNT;
        } else {
            allRecordCount = (int)RecCount.lcount;
        }
        return allRecordCount;

    }

    public Dialog createDialog(int wId, final int titleId) {
        final CustomDialog oDialog = new CustomDialog(this);
        if (wId == DIALOG_NODATA_ALARM) {

            oDialog.setTitle(titleId);
            oDialog.setMessage(this.getResources().getString(R.string.dialog_nodata_msg));
            oDialog.addButton(this.getResources().getString(R.string.btn_ok), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    removeDialog(DIALOG_NODATA_ALARM);
                    oDialog.cancel();
                    //yangyang add start Bug1106 20110517
                    if (titleId == R.string.genre_dialog_title) {
                        setResult(Constants.RESULTCODE_BACK, getIntent());
                        finish();
                    }
                    //yangyang add end Bug1106 20110517

                }
            });
            oDialog.show();
            return oDialog;
        } else if (wId == Constants.DIALOG_WAIT) {
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"oPDialog.isShowing()====" + oWarningDialog.isShowing());
            //R.style.dialogの意味：ダイアログの白い枠を削除する
//			WaitDialog oPDialog = new WaitDialog(this,R.style.dialog);
            WaitDialog oPDialog = new WaitDialog(this);

            oWarningDialog = oPDialog;
            oPDialog.setShowProGressBar(true);
            oPDialog.setOnKeyListener(new OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface dialog, int keyCode,
                        KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_SEARCH) {
                        return true;
                    } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                        cancel();
                    }
                    return false;
                }
            });
//			if (isShowCancelBtn == SHOWCANCELBTN) {
//			oDialog.addCancelButton("キャンセル");
            oPDialog.setCancelable(true);
            oPDialog.setOnCancelListener(this);
            oPDialog.show();

            return oPDialog;
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"createDialog start");
//			final ProgressDialog oDialog = new ProgressDialog(this);
//			oDialog.setButton("cancle", new onClickListener (){});
//			oDialog.setIndeterminate(true);
//			oDialog.setMessage(this.getResources().getString(R.string.msg_loading));
//			oDialog.setCancelable(true);
//			oDialog.setOnKeyListener(new OnKeyListener() {
//
//				@Override
//				public boolean onKey(DialogInterface dialog, int keyCode,
//						KeyEvent event) {
//					if (keyCode == KeyEvent.KEYCODE_SEARCH) {
//						return true;
//					} else if (keyCode == KeyEvent.KEYCODE_BACK) {
//						removeDialog(Constants.DIALOG_WAIT);
//						if (oWarningDialog != null) {
//							oWarningDialog.cancel();
//						}
//					}
//					return false;
//				}
//
//			});
//			oDialog.setOnCancelListener(this);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"createDialog end");
//			oDialog.show();
//            return oDialog;
        }
        return super.onCreateDialog(wId);
    }

//	@Override
//	protected void onPrepareDialog(int wId, Dialog dialog) {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"wId====" + wId);
//		View v = dialog.getWindow().getDecorView();
//		v.setBackgroundColor(0xffffff);
//		v.setPadding(5, 5, 5, 5);
//		RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
//		RelativeLayout.LayoutParams.WRAP_CONTENT);
//		v.setLayoutParams(oParams);
//		super.onPrepareDialog(wId, dialog);
//	}
    @Override
    public void onCancel(DialogInterface arg0) {
        cancel();
    }
//Add 2011/10/13 Z01_h_yamada Start -->
    @Override
    protected void onDestroy() {
        cancel();
        super.onDestroy();
    }
//Add 2011/10/13 Z01_h_yamada End <--

    protected abstract boolean onStartShowPage() throws Exception;

    protected abstract void onFinishShowPage(boolean bGetData) throws Exception;

    private Dialog oWarningDialog = null;

    /**
     * 検索し始める
     */
    protected void startShowPage(int isShowCancelBtn) {

        NaviLog.d(NaviLog.PRINT_LOG_TAG,"startShowPage start");
        this.isShowCancelBtn = isShowCancelBtn;
        m_bIsCancel = false;
        removeDialog(Constants.DIALOG_WAIT);
        if (oWarningDialog != null) {
            oWarningDialog.cancel();
        }
        Constants.doMoveNextPage = true;
        oWarningDialog = createDialog(Constants.DIALOG_WAIT, -1);
        m_oThread = new Thread(this);
        m_oThread.start();
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"startShowPage end");
    }

    @Override
    public void run() {
        boolean bGetData = false;
        try {
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"run start");
            bGetData = onStartShowPage();
            if (bGetData) {
                NaviLog.d(NaviLog.PRINT_LOG_TAG,"Constants.doMoveNextPage start===" + Constants.doMoveNextPage);
                onFinishShowPage(bGetData);
                NaviLog.d(NaviLog.PRINT_LOG_TAG,"Constants.doMoveNextPage end===" + Constants.doMoveNextPage);
            }
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"run end");
        } catch (Exception e) {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (!m_bIsCancel) {
//						Intent oIntent = getIntent();
//						setResult(AppInfo.RESULT_ERROR,oIntent);
                        finish();
                    }
                }
            });
        } finally {
            bGetData = false;
            removeDialog(Constants.DIALOG_WAIT);
            if (oWarningDialog != null) {
                oWarningDialog.dismiss();
                oWarningDialog.cancel();

            }
            cancel();
//			Constants.doMoveNextPage = true;
        }

    }

    /**
     * 検索を消す
     */
    private void cancel() {
        m_bIsCancel = true;

        if (m_oThread != null) {
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"m_oThread != null");

            try {
                closeWarningDialog();

                m_oThread.interrupt();
//				m_oThread.stop();
            } catch (Exception e) {
            }
        } else {
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"m_oThread == null");
            closeWarningDialog();
        }
//		if(m_bAutoClose)
//		{
//			finish();
//		}
    }

    public void closeWarningDialog() {
        if (oWarningDialog != null) {
            removeDialog(Constants.DIALOG_WAIT);
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"oWarningDialog != null");
            oWarningDialog.cancel();
            oWarningDialog.dismiss();
            Constants.doMoveNextPage = false;
            oWarningDialog = null;
        }

    }

    public Intent oIntent = null;

    public Intent getData() {
        return oIntent;
    }

    @Override
    protected void onResume() {
//		removeDialog(Constants.DIALOG_WAIT);
//		if (oWarningDialog != null) {
//			oWarningDialog.cancel();
//		}
        super.onResume();
    }

//	public void initListSelectedFalse(ScrollList oList,ArrayList<Integer> objId) {
//		int iCount = oList.getChildCount();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"iCount===" + iCount);
//		for (int i = 0 ; i < iCount; i++) {
//			LinearLayout obtn = (LinearLayout)oList.getChildAt(i);
//			obtn.setSelected(false);
//			int iNum = objId.size();
//			for (int j = 0; j < iNum; j++) {
//				TextView oValue = (TextView) obtn.findViewById(objId.get(j));
//				oValue.setTextColor(0xff000000);
//			}
//
////			TextView oDis = (TextView) obtn.findViewById(R.id.TextView_distance);
////			oDis.setTextColor(0xff000000);
//		}
//	}

    //yangyang mod start 20110420 Bug1026
    public String checkGroupBtnShow(String value) {

        if (check(value, this.getResources().getStringArray(R.array.check_group_a))) {

            return this.getResources().getStringArray(R.array.check_group_a)[0];//R.string.kana_a;
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_i))) {

            return this.getResources().getStringArray(R.array.check_group_i)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_u))) {

            return this.getResources().getStringArray(R.array.check_group_u)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_e))) {

            return this.getResources().getStringArray(R.array.check_group_e)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_o))) {

            return this.getResources().getStringArray(R.array.check_group_o)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ka))) {

            return this.getResources().getStringArray(R.array.check_group_ka)[0];//R.string.kana_ka;
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ki))) {

            return this.getResources().getStringArray(R.array.check_group_ki)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ku))) {

            return this.getResources().getStringArray(R.array.check_group_ku)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ke))) {

            return this.getResources().getStringArray(R.array.check_group_ke)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ko))) {

            return this.getResources().getStringArray(R.array.check_group_ko)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_sa))) {

            return this.getResources().getStringArray(R.array.check_group_sa)[0];//R.string.kana_sa;
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_si))) {

            return this.getResources().getStringArray(R.array.check_group_si)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_su))) {

            return this.getResources().getStringArray(R.array.check_group_su)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_se))) {

            return this.getResources().getStringArray(R.array.check_group_se)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_so))) {

            return this.getResources().getStringArray(R.array.check_group_so)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ta))) {

            return this.getResources().getStringArray(R.array.check_group_ta)[0];//R.string.kana_ta;
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ti))) {

            return this.getResources().getStringArray(R.array.check_group_ti)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_tu))) {

            return this.getResources().getStringArray(R.array.check_group_tu)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_te))) {

            return this.getResources().getStringArray(R.array.check_group_te)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_to))) {

            return this.getResources().getStringArray(R.array.check_group_to)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_na))) {

            return this.getResources().getStringArray(R.array.check_group_na)[0];//R.string.kana_na;
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ni))) {

            return this.getResources().getStringArray(R.array.check_group_ni)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_nu))) {

            return this.getResources().getStringArray(R.array.check_group_nu)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ne))) {

            return this.getResources().getStringArray(R.array.check_group_ne)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_no))) {

            return this.getResources().getStringArray(R.array.check_group_no)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ha))) {

            return this.getResources().getStringArray(R.array.check_group_ha)[0];//R.string.kana_ha;
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_hi))) {

            return this.getResources().getStringArray(R.array.check_group_hi)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_hu))) {

            return this.getResources().getStringArray(R.array.check_group_hu)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_he))) {

            return this.getResources().getStringArray(R.array.check_group_he)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ho))) {

            return this.getResources().getStringArray(R.array.check_group_ho)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ma))) {

            return this.getResources().getStringArray(R.array.check_group_ma)[0];//R.string.kana_ma;
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_mi))) {

            return this.getResources().getStringArray(R.array.check_group_mi)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_mu))) {

            return this.getResources().getStringArray(R.array.check_group_mu)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_me))) {

            return this.getResources().getStringArray(R.array.check_group_me)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_mo))) {

            return this.getResources().getStringArray(R.array.check_group_mo)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ya))) {

            return this.getResources().getStringArray(R.array.check_group_ya)[0];//R.string.kana_ya;
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_yu))) {

            return this.getResources().getStringArray(R.array.check_group_yu)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_yo))) {

            return this.getResources().getStringArray(R.array.check_group_yo)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ra))) {
            return this.getResources().getStringArray(R.array.check_group_ra)[0];//R.string.kana_ra;
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ri))) {
            return this.getResources().getStringArray(R.array.check_group_ri)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ru))) {
            return this.getResources().getStringArray(R.array.check_group_ru)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_re))) {
            return this.getResources().getStringArray(R.array.check_group_re)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_ro))) {
            return this.getResources().getStringArray(R.array.check_group_ro)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_wa))) {

            return this.getResources().getStringArray(R.array.check_group_wa)[0];//R.string.kana_wa;
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_wo))) {

            return this.getResources().getStringArray(R.array.check_group_wo)[0];
        } else if (check(value, this.getResources().getStringArray(R.array.check_group_nn))) {

            return this.getResources().getStringArray(R.array.check_group_nn)[0];
        }
        return "";

    }

    //yangyang mod end 20110420 Bug1026

    public static boolean check(String value, String[] checkGroupValue) {
        boolean bReturn = false;
        int len = checkGroupValue.length;
        if (len > 0) {
            for (int i = 0; i < len; i++) {
                if (checkGroupValue[i].equals(value.trim())) {
                    bReturn = true;
                    break;
                }
            }
        }

        return bReturn;
    }

    public static void showToast(final Activity oActivity, JNILong RecCount) {
// Chg 2011/10/08 katsuta Start -->
//        if (RecCount.lcount > Constants.MAX_RECORD_COUNT) {
        if (RecCount.lcount >= Constants.MAX_RECORD_COUNT) {
// Chg 2011/10/08 katsuta End <--
            oActivity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast toast = Toast.makeText(oActivity, null, Toast.LENGTH_SHORT);
                    toast.setText(R.string.max_limited_msg);
                    toast.show();
                }
            });

        }
    }
}
