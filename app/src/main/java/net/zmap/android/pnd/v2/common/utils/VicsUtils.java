package net.zmap.android.pnd.v2.common.utils;

import android.util.Base64;
import android.util.Xml;

import net.zmap.android.pnd.v2.app.NaviAppDataPath;

import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.CRC32;

public class VicsUtils {
    private static final String PATH = "/data/data/com.neusoft.liu.test/vicsrequestparam.001";
//@@MOD-START BB-0003 2012/10/29 Y.Hayashida
    private static final String VTPATH = "tmp/VT/TravelTrack.data";
//    private static final String VTPATH = "/sdcard/zdcpnd/TMP/VT/TravelTrack.data";
//@@MOD-END BB-0003 2012/10/29 Y.Hayashida

    public VicsUtils(){

    }

    public static String getPresence(){
        String dataBody=null;
        try{
//@@MOD-START BB-0003 2012/10/29 Y.Hayashida
// ログディレクトリの組み立て
        	String logPath = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) + File.separator + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME;
        	logPath += File.separator + VTPATH;
        	dataBody = getFile(logPath);
//            dataBody = getFile(VTPATH);
//@@MOD-END BB-0003 2012/10/29 Y.Hayashida
        }catch(Exception e){
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }
        return dataBody;
    }

    public static String writeStream(){
        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        DataOutputStream dos1 = new DataOutputStream(baos1);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);

        String dataAll = null;
        try {

            int filenum = 2;

            String infoFileName = "TILIST001.xml";
            String infoXml = createXml();
            int infoXml_length = infoXml.length();
            String fileName = "TILIST001.001";
            String dataBody = getFile(PATH);
            int data_length = dataBody.length();

            int all_length = infoFileName.length() + infoXml_length + fileName.length()
                + data_length + infoXml.length() + dataBody.length();
//            NaviLog.d(NaviLog.PRINT_LOG_TAG," liutch -----------------> "+all_length +" "+infoFileName.length()+
//                    "   "+infoXml_length+"   "+fileName.length()+"   "+data_length+"   "+infoXml.length()+"  "+dataBody.length());

            dos.writeInt(filenum);
            dos.writeInt(all_length);
            dos.writeBytes(infoFileName);
            dos.writeBytes(fileName);
            dos.writeInt(data_length);
            dos.writeBytes(infoXml);
            dos.writeBytes(dataBody);

            int be_data_length = dos.toString().getBytes().length;
            int af_data_length = ZLibUtils.compress(dos.toString().getBytes()).length;

            dos1.writeInt(be_data_length);
            dos1.writeInt(af_data_length);
            dos1.writeInt(filenum);
            dos1.writeInt(all_length);
            dos1.writeBytes(infoFileName);
            dos1.writeBytes(fileName);
            dos1.writeInt(data_length);
            dos1.writeBytes(infoXml);
            dos1.writeBytes(dataBody);

            int crc = (int) getCRC(baos1.toString());
            dos1.writeInt(crc);
//            NaviLog.d(NaviLog.PRINT_LOG_TAG," liutch ---------> "+baos1.toString());

//            dataAll = new String(Base64.encode(baos1.toString().getBytes(),Base64.DEFAULT));
            dataAll = Base64.encodeToString(baos1.toString().getBytes(), Base64.DEFAULT);
//            NaviLog.d(NaviLog.PRINT_LOG_TAG," liutch ***************> "+dataAll);
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }finally{
            try{
                baos1.close();
                baos.close();
                dos.close();
                dos1.close();
            }catch(Exception e){
				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            }
        }
        return dataAll;
        }
    private static String getFile(String filepath) throws IOException {
        File file = new File(filepath);
        FileInputStream fileInputStream = new FileInputStream(file);
        DataInputStream din = new DataInputStream(fileInputStream);

        StringBuilder hexData = new StringBuilder();
//        NaviLog.d(NaviLog.PRINT_LOG_TAG," liutch ==============> "+file.length());
        byte temp = 0;
        for(int i=0;i<file.length();i++) {
         temp = din.readByte();
         String str = Integer.toHexString(temp);
         if(str.length() == 8) {
          str = str.substring(6);
         }
         if(str.length() == 1) {
          str = "0"+str;
         }
         hexData.append(str.toUpperCase());
        }
//        NaviLog.d(NaviLog.PRINT_LOG_TAG," liutch ---------------> "+hexData.toString());

        din.close();
        fileInputStream.close();

        return hexData.toString();
       }
    private static long getCRC(String data){
        CRC32 crc32 = new CRC32();
        crc32.update(data.getBytes());

        return crc32.getValue();
    }
    private static String createXml() {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);

            /*
             * <?xml version="1.0" encoding="Shift_JIS" standalone="no"?> <hdnn
             * version="1.0"> <aut_inf target_id="**12345678"
             * time=”0215130”></aut_inf> <srv_inf> <app name="TI"> </app>
             * </srv_inf> </hdnn>
             */

            // <?xml version="1.0" encoding="Shift_JIS" standalone="no"?>
            serializer.startDocument("Shift_JIS", false);

            // <blog number=”1″>
            serializer.startTag("", "hdnn");
            serializer.attribute("", "version", "1.0");

            // <aut_inf target_id="**12345678" time=”0215130”></aut_inf>
            serializer.startTag("", "aut_inf");
            serializer.attribute("", "target_id", "DFJ001902008");

            SimpleDateFormat formatter = new SimpleDateFormat("MMddHHmm");
            Date currentTime = new Date();
            String dateString = formatter.format(currentTime);

            serializer.attribute("", "time", dateString);
            serializer.endTag("", "aut_inf");

            //
            serializer.startTag("", "srv_inf");
            serializer.startTag("", "app");
            serializer.attribute("", "name", "TI");
            serializer.endTag("", "app");
            serializer.endTag("", "srv_inf");

            // </message>
            serializer.endTag("", "hdnn");
            serializer.endDocument();
            return writer.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
