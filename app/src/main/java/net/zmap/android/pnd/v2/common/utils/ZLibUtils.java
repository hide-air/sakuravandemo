package net.zmap.android.pnd.v2.common.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;


public abstract class ZLibUtils {

    public static byte[] compress(byte[] data) {
        byte[] output = null;

        Deflater compresser = new Deflater();

        compresser.reset();
        compresser.setInput(data);
        compresser.finish();
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);
        try {
            byte[] buf = new byte[1024];
            while (!compresser.finished()) {
                int i = compresser.deflate(buf);
                bos.write(buf, 0, i);
            }
            output = bos.toByteArray();
        } catch (Exception e) {
            output = data;
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            }
        }
        compresser.end();
        return output;
    }


    public static void compress(byte[] data, OutputStream os) {
        DeflaterOutputStream dos = new DeflaterOutputStream(os);

        try {
            dos.write(data, 0, data.length);

            dos.finish();

            dos.flush();
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }
    }

    public static byte[] decompress(byte[] data) {
        byte[] output = null;

        Inflater decompresser = new Inflater();
        decompresser.reset();
        decompresser.setInput(data);

        ByteArrayOutputStream o = new ByteArrayOutputStream(data.length);
        try {
            byte[] buf = new byte[1024];
            while (!decompresser.finished()) {
                int i = decompresser.inflate(buf);
                o.write(buf, 0, i);
            }
            output = o.toByteArray();
        } catch (Exception e) {
            output = data;
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } finally {
            try {
                o.close();
            } catch (IOException e) {
				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            }
        }

        decompresser.end();
        return output;
    }

    public static byte[] decompress(InputStream is) {
        InflaterInputStream iis = new InflaterInputStream(is);
        ByteArrayOutputStream o = new ByteArrayOutputStream(1024);
        try {
            int i = 1024;
            byte[] buf = new byte[i];

            while ((i = iis.read(buf, 0, i)) > 0) {
                o.write(buf, 0, i);
            }

        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }
        return o.toByteArray();
    }
}
