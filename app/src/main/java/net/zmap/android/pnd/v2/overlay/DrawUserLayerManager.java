package net.zmap.android.pnd.v2.overlay;

import android.util.Log;

import net.zmap.android.pnd.v2.api.overlay.OverlayItem;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.engine.NaviEngineError;
import net.zmap.android.pnd.v2.engine.NaviEngineUtils;

import java.util.ArrayList;
import java.util.List;

public final class DrawUserLayerManager {

    private static final String TAG = "DrawUserLayerManager";

//    String mLayer = "";

    List<DrawUserLayer> mDrawUserLayers = new ArrayList<DrawUserLayer>();

    public boolean registUserLayer(DrawUserLayer DrawUserLayer) {

        JNILong layerId = new JNILong();
        long ret = NaviRun.GetNaviRunObj().JNI_NE_AddUserLayer(layerId);
        if (ret != 0) {
            return false;
        }
        DrawUserLayer.setNELayerId( layerId.getLcount());
        mDrawUserLayers.add(DrawUserLayer);

        return true;
    }

    public void unregistAllUserLayer()
    {
        int layerNum = mDrawUserLayers.size();
        for (int i = 0; i < layerNum; i++) {
            DrawUserLayer layer = mDrawUserLayers.get(i);
            NaviRun.GetNaviRunObj().JNI_NE_DeleteUserLayer(layer.getNELayerId());
        }
// Add 2011/11/04 r.itoh Start [ExternalAPI] -->
        if (layerNum != 0) {
            NaviEngineUtils.redrawSurface(NaviRun.CT_ICU_CtrlType_Map1);
        }
// Add 2011/11/04 r.itoh End [ExternalAPI] <--

        mDrawUserLayers.clear();
    }

    public void unregistUserLayer(String userLayerId) {
        DrawUserLayer layer = getUserLayer(userLayerId);
        if (layer == null) {
            return;
        }

        Log.d(TAG,"##---- unregistUserLayer " + layer.getLayerId() + " : " +layer.getNELayerId() );

        long ret = NaviRun.GetNaviRunObj().JNI_NE_DeleteUserLayer(layer.getNELayerId());

// Add 2011/11/04 r.itoh Start [ExternalAPI] -->
        if (ret == NaviEngineError.NE_SUCCESS) {
            NaviEngineUtils.redrawSurface(NaviRun.CT_ICU_CtrlType_Map1);
        }
// Add 2011/11/04 r.itoh End [ExternalAPI] <--

        mDrawUserLayers.remove(layer);
    }

    public DrawUserLayer getUserLayer(String userLayerId) {
        int layerNum = mDrawUserLayers.size();
        for (int i = 0; i < layerNum; i++) {
            DrawUserLayer layer = mDrawUserLayers.get(i);
            if (userLayerId.equals(layer.getLayerId())) {
                return layer;
            }
        }
        return null;
    }

    public OverlayItem findUserFig(long drawFigureId) {
        int layerNum = mDrawUserLayers.size();
        for (int i = 0; i < layerNum; i++) {
            DrawUserLayer layer = mDrawUserLayers.get(i);
            OverlayItem fig = layer.getUserFigure(drawFigureId);
            if( fig != null ){
                return fig;
            }
        }
        return null;
    }

}
