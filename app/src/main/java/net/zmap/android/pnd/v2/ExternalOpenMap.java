package net.zmap.android.pnd.v2;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.GeoUri;
import net.zmap.android.pnd.v2.api.map.ZoomLevel;
import net.zmap.android.pnd.v2.common.utils.GeoUtils;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.maps.MapView;
import android.content.Intent;
import android.net.Uri;

public class ExternalOpenMap extends AbstractExternalAction {

    public ExternalOpenMap(MapActivity mapActivity, Intent intent) {
        super(mapActivity, intent);
    }

    @Override
    public void doExternalAction() {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "ExternalOpenMap::doExternalAction");
        Uri data = mIntent.getData();

        GeoUri geo = new GeoUri(data.toString());
        GeoPoint coordinate = GeoUtils.toTokyoDatum(geo.getLatitude(), geo.getLongitude());
        byte zoomLevel = (geo.hasZoom() ? (byte)ZoomLevel.convertFromGoogleMapsZoomLevel(geo.getZoom()) : (byte)0xFF);

        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(
                NaviRun.NE_NEActType_ShowMapNormal, NaviRun.NE_NETransType_AroundSearchPoint,
                zoomLevel, 1, coordinate.getLongitudeMs(), coordinate.getLatitudeMs(), 0);
        MapView.getInstance().setScrollUi();
        MapView.getInstance().setMapScrolled(true);
    }
}
