/**
 *  @file		DriConMenuListDialog
 *  @brief		ドライブコンテンツの一覧ダイアログ
 *
 *  @attention
 *  @note		MAPから表示できる、コンテンツの一覧
 *
 *  @author		Manabu Watanabe [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2011-09-01)
 *  @version	$Revision: $ by $Author: $
 *
 */

package net.zmap.android.pnd.v2.dricon.view;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
//ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ一覧 Move禁止 Start -->
import android.graphics.PointF;
import android.view.MotionEvent;
//ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ一覧 Move禁止 End <--
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
//ADD 2013.08.08 M.Honma 走行規制 メッセージ表示とフラグ取得 Start -->
import net.zmap.android.pnd.v2.common.DrivingRegulation;
//ADD 2013.08.08 M.Honma 走行規制 メッセージ表示とフラグ取得 End <--
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.data.DC_ContentsData;
import net.zmap.android.pnd.v2.dricon.controller.DriverContentsCtrl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author watanabe
 *
 */
/**
 * @author watanabe
 *
 */
public class DriConMenuListDialog extends Dialog{
	private ListView 						m_lvICListView;
	private	Context							m_oContext;
	private ListViewAdapter 				m_lvAdapter;
	private List<DC_ContentsData>			m_oDataList = null;
	private List<Integer>					m_oDataIndexList = null;
	private List<DC_ContentsData>			m_oOrgDataList = null;
	private boolean							m_bChangedFlag = false;
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ一覧 Move禁止 Start -->
	private PointF 							m_LastPoint = new PointF();
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ一覧 Move禁止 End <--

    /**
     * コンストラクタ
     * @param context コンテキスト
     * @param dataList 表示するデータ
     */
    public DriConMenuListDialog(Context context, List<DC_ContentsData> dataList) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dricon_dialog_menu);

        if(dataList != null){
        	updateData(dataList);
        }
        m_oContext = context;
        m_lvICListView = (ListView)findViewById(R.id.dc_listview);
        m_lvAdapter = new ListViewAdapter(context);
        m_lvICListView.setAdapter(m_lvAdapter);

	    DisplayMetrics metrics = context.getResources().getDisplayMetrics();
	    int dialogWidth = (int) (metrics.widthPixels);
	    WindowManager.LayoutParams lp = getWindow().getAttributes();
	    lp.width = dialogWidth;
	    this.getWindow().setAttributes(lp);

		Button bnt = (Button) findViewById(R.id.dc_btnBack);
		bnt.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v){
				dismiss();
			}
		});
	}

// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ一覧 Move禁止 Start -->
    @Override
    public boolean dispatchTouchEvent(MotionEvent event)
    {
    	// 走行規制する場合
    	if(DrivingRegulation.GetDrivingRegulationFlg())
    	{
    		if(event.getAction() == MotionEvent.ACTION_DOWN)
    		{
    			// ダウン時の位置を保存
    			m_LastPoint = new PointF(event.getX(),event.getY());
    		}
    		else if(event.getAction() == MotionEvent.ACTION_MOVE)
    		{
    			// スクロールしない
    			return true;
    		}
    		else if(event.getAction() == MotionEvent.ACTION_UP)
    		{
    			// アップ時の位置がマージン内でなければイベントをキャンセルする
    			// マージンはMapViewのドラッグ判定と同様の10とする
    			PointF current = new PointF(event.getX(),event.getY());
        		if(Math.abs(current.x - m_LastPoint.x) >= 10 ||  Math.abs(current.y - m_LastPoint.y) >= 10)
        		{
        			event.setAction(MotionEvent.ACTION_CANCEL);
        		}
    		}
    	}
    	return super.dispatchTouchEvent(event);
    }
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ一覧 Move禁止 End <--

    /**
     * リスト内で変更された情報を取得する
     * @return 変更情報を取得する
     */
    public List<DC_ContentsData> getChangedList(){
    	if(m_bChangedFlag){
    		return m_oOrgDataList;
    	}
    	return null;
    }

    /**
     * リストの更新
     * @param dataList 更新データ
     */
    public void updateData(List<DC_ContentsData> dataList){
    	if(m_oDataList == null){
        	m_oDataList = new ArrayList<DC_ContentsData>();
        	m_oDataIndexList = new ArrayList<Integer>();
    	}else{
    		m_oDataList.clear();
    		m_oDataIndexList.clear();
    	}
		m_oOrgDataList = null;

        if(dataList != null){
    		m_oOrgDataList = new  ArrayList<DC_ContentsData>(dataList);
	        for(int i = 0; i < dataList.size(); i++){
	        	if(dataList.get(i) != null){
	        		if(dataList.get(i).m_bDisplayItem){
	        			m_oDataList.add( dataList.get(i));
	        			m_oDataIndexList.add(i);
	        		}
	        	}
	        }
        }
        notifyDataSetChanged();
    }

    /**
     * データ変更イベント取得
     */
    public void notifyDataSetChanged(){
    	if( m_lvAdapter!= null){
    		m_lvAdapter.notifyDataSetChanged();
		}
    }
    /**
     * リストから指定されたキーを取得する
     * @param index 指定値
     * @return 取得するキー

     */
    private int getDataListKey(int index){
    	int key = -1;
		if(m_oDataIndexList != null){

			key = m_oDataIndexList.get(index);
		}
	   	return key;
    }
    /**
     * リストから指定された値を取得する
     * @param index 指定値
     * @return 取得する値
     */
    private DC_ContentsData getDataListValue(int index){
    	DC_ContentsData value = null;
		if(m_oDataList != null){
			value = m_oDataList.get(index);
		}
    	return value;
    }

    /**
     * @author watanabe
     *リストのアイテムを管理するクラス
     */
    private class ListViewAdapter extends BaseAdapter{

    	private LayoutInflater mInflater;
		private LinearLayout m_oLayout;
        public ListViewAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        /* (非 Javadoc)
         * @see android.widget.Adapter#getCount()
         */
        public int getCount()
        {
            if(m_oDataList == null){
            	return 0;
            }else{
            	return m_oDataList.size();
            }
        }

        /* (非 Javadoc)
         * @see android.widget.Adapter#getItem(int)
         */
        public Object getItem(int position)
        {
            return null;
        }

        /* (非 Javadoc)
         * @see android.widget.Adapter#getItemId(int)
         */
        public long getItemId(int position)
        {
            return position;
        }

        /* (非 Javadoc)
         * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
         */
        public View getView(int position, View oView, ViewGroup parent) {
    		if(oView == null)
    		{
    			m_oLayout = (LinearLayout)mInflater.inflate(R.layout.dricon_dialog_item, null);
    			oView = m_oLayout;
    		} else {
    			m_oLayout = (LinearLayout)oView;
    		}

    		final int wPos = position;
    		LinearLayout oLayoutBtn = (LinearLayout)m_oLayout.findViewById(R.id.dc_dialog_item_layoutBtn);
    		TextView txtView = (TextView)m_oLayout.findViewById(R.id.dc_dialog_item_text);
    		ImageView imgIconState = (ImageView)m_oLayout.findViewById(R.id.dc_dialog_item_imgIconState);
    		ImageView imgIconDC = (ImageView)m_oLayout.findViewById(R.id.dc_dialog_item_imgIconDC);
    		Button btnGoMenu = (Button)m_oLayout.findViewById(R.id.dc_dialog_item_btnGoMenu);

    		//鍵マーク設定
            if(getDataListValue(wPos).m_bLayering){
            	imgIconState.setImageResource(R.drawable.icon_key);
            }else{
            	//チェックアイテム
            	if(getDataListValue(wPos).m_bCheckItem){
            		imgIconState.setImageResource(R.drawable.icon_check);
            	}else{
            		imgIconState.setImageResource(R.color.info_bg);
            	}
            }
    		oLayoutBtn.setOnClickListener(new LinearLayout.OnClickListener() {
    			@Override
				public void onClick(View oView) {
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ一覧 変更禁止 Start -->
    		    	if(DrivingRegulation.CheckDrivingRegulation()){
    		    		return;
    		    	}
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ一覧 変更禁止 End <--
    				if(!getDataListValue(wPos).m_bLayering){
    					boolean	bOrgFlag = getDataListValue(wPos).m_bCheckItem;
	    				for(int i = 0; i < m_oDataList.size(); i++){
	    					getDataListValue(i).m_bCheckItem = false;
	    					if(i == wPos){
    							getDataListValue(i).m_bCheckItem = !bOrgFlag;
    							m_bChangedFlag = true;
	    					}
	    				}
	    				notifyDataSetChanged();
	    				dismiss();
    				}
				}
			});

    		btnGoMenu.setOnClickListener(new Button.OnClickListener() {
    			@Override
    			public void onClick(View v){
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ一覧 詳細禁止 Start -->
    		    	if(DrivingRegulation.CheckDrivingRegulation()){
    		    		return;
    		    	}
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ一覧 詳細禁止 End <--
    				Intent oIntent = new Intent();
    				oIntent.setClass(m_oContext,DriConDetailMenuActivity.class);
    				oIntent.putExtra(Constants.DC_INTENT_DATA_SETTING_INDEX_EXTRA, getDataListKey(wPos));
    				oIntent.putExtra(Constants.DC_INTENT_DIALOG_TO_MENU_EXTRA, true);
    				NaviActivityStarter.startActivityForResult(((Activity) m_oContext), oIntent, AppInfo.ID_ACTIVITY_DRICON_SETTING_DETAIL);
    			}
    		});
    		int key = getDataListValue(wPos).m_iContentsID;
    		Bitmap bitmap = DriverContentsCtrl.getController().getIconImgData(key);
    		if(bitmap != null){
    			imgIconDC.setImageBitmap(bitmap);
    		}
    		if(m_oDataList != null){
    			if (wPos < m_oDataList.size()) {
    				txtView.setText(getDataListValue(wPos).m_sContentsName);
    			}
    		}
    		return oView;
        }
    }

}