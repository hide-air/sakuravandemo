package net.zmap.android.pnd.v2.common.utils;

import android.content.Context;
import android.os.PowerManager;

/**
 * Keep Screen on when the App is Running.
 *
 * @author wangdong
 *
 */
public class WakeLockUtils {

	private static PowerManager.WakeLock wakeLock;

	public static void closeScreenLock(Context context) {
		wakeLock = ((PowerManager)context.getSystemService(Context.POWER_SERVICE)).newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "MyActivity");

		wakeLock.acquire();
	}

    public static void showScreenLock() {
        if (wakeLock != null) {
            wakeLock.release();
        }
    }
}
