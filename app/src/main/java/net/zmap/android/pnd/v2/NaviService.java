package net.zmap.android.pnd.v2;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.INaviService;
import net.zmap.android.pnd.v2.api.NaviIntent;
import net.zmap.android.pnd.v2.api.NaviResult;
import net.zmap.android.pnd.v2.api.event.Area;
import net.zmap.android.pnd.v2.api.event.IApplicationStatusListener;
import net.zmap.android.pnd.v2.api.event.IAreaListener;
import net.zmap.android.pnd.v2.api.event.IRoadListener;
import net.zmap.android.pnd.v2.api.event.IRouteCalcListener;
import net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener;
import net.zmap.android.pnd.v2.api.event.IVehiclePositionListener;
import net.zmap.android.pnd.v2.api.event.Road;
import net.zmap.android.pnd.v2.api.event.RouteCalculated;
import net.zmap.android.pnd.v2.api.event.RouteGuidance;
import net.zmap.android.pnd.v2.api.map.DayNightMode;
import net.zmap.android.pnd.v2.api.map.ZoomLevel;
import net.zmap.android.pnd.v2.api.navigation.RemainingDistance;
import net.zmap.android.pnd.v2.api.navigation.RoutePoint;
import net.zmap.android.pnd.v2.api.navigation.RouteRequest;
import net.zmap.android.pnd.v2.api.overlay.Icon;
import net.zmap.android.pnd.v2.api.overlay.Line;
import net.zmap.android.pnd.v2.api.poi.FavoritesCategory;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.MapTypeListioner;
import net.zmap.android.pnd.v2.common.activity.BaseActivity;
import net.zmap.android.pnd.v2.common.activity.BaseActivityStack;
import net.zmap.android.pnd.v2.common.utils.GeoUtils;
import net.zmap.android.pnd.v2.data.JNIByte;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_UserFile_DataIF;
import net.zmap.android.pnd.v2.data.POI_UserFile_RecordIF;
import net.zmap.android.pnd.v2.data.ZNUI_DESTINATION_DATA;
import net.zmap.android.pnd.v2.data.ZNUI_TIME;
import net.zmap.android.pnd.v2.engine.NaviEngineError;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.maps.MapView;
import net.zmap.android.pnd.v2.maps.OpenMap;
import net.zmap.android.pnd.v2.overlay.DrawUserLayer;
import net.zmap.android.pnd.v2.overlay.DrawUserLayerManager;
import net.zmap.android.pnd.v2.sakuracust.DebugLogOutput;
import net.zmap.android.pnd.v2.api.event.VehiclePositionEx;
import net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener;
import net.zmap.android.pnd.v2.app.NaviAppStatus;

import java.util.Calendar;
import java.util.List;

/**
 * ナビサービスサーバ
 */
public class NaviService extends Service {
    private static final String TAG = "NaviService";
// ADD.2013.11.14 N.Sasao NaviService起動時のアイコン制御対応 START
    private static final String DEF_CONFIRM_VERSION = "4.3";
// ADD.2013.11.14 N.Sasao NaviService起動時のアイコン制御対応  END

    @Override
    public void onCreate() {
        super.onCreate();

        startForeground();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        stopForeground(true);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind(" + (intent == null ? "null" : intent.toString()) + ")");
        return mBinder;
    }

    private void startForeground() {

// MOD.2013.11.14 N.Sasao NaviService起動時のアイコン制御対応 START

    	// OSバージョンが指定バージョン以上か判定
    	boolean bUpper = CommonLib.isUpperValueVersion( DEF_CONFIRM_VERSION );

    	Notification notification;

    	// OS4.3以上の時にstartForegroundを行うと、Notificationアイコンは消えなくなった。
    	// そのため、4.3以上は正式にアイコンを出す処理を実装する。
    	if( bUpper ){
	        notification = new Notification(R.drawable.ic_notification, getString(R.string.app_name), System.currentTimeMillis());
	        // Notification 選択時の動作インテントを生成
	        PendingIntent contentIntent =
	                PendingIntent.getActivity(this, 0,
	                        new Intent(
	                        		this, ItsmoNaviDrive.class)
	                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK), 0);
	        // 通知領域に表示する情報を Notification に指定
	        notification.setLatestEventInfo(getApplicationContext(),
	        		this.getString(R.string.notification_app_name), this.getString(R.string.notification_app_detail), contentIntent);
	        // Notification は自動で削除しない
	        notification.flags |= Notification.FLAG_ONGOING_EVENT;
    	}
    	else{
    		// OS4.3より低い場合は、以前のままアイコンを消す。
    		notification = new Notification(0, getString(R.string.app_name), System.currentTimeMillis());
    	}
// MOD.2013.11.14 N.Sasao NaviService起動時のアイコン制御対応  END

        startForeground(1, notification);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    private final INaviService.Stub mBinder = new INaviService.Stub() {

        @Override
        public boolean isStarted() throws RemoteException {
            return ((NaviApplication) getApplication()).isStarted();
        }

        /**
         * 地図を表示
         */
        @Override
        public void _openMap(NaviResult result, boolean forced) throws RemoteException {
            if (!forced) {
                return;
            }
            NaviApplication naviApp = (NaviApplication) getApplication();
            if (naviApp == null) {
                result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "naviApp == null");
                return;
            }
            BaseActivity topActivity = null;
            try {
                topActivity = naviApp.getBaseActivityStack().getTopActivity();
            } catch (Exception e) {
                result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "getTopActivity() failed");
                return;
            }
            CommonLib.goCarLocation(topActivity);
        }

        /**
         * 経緯度・縮尺を指定して地図を表示
         */
        @Override
        public void openMap(NaviResult result, GeoPoint point, int zoomLevel, boolean forced) throws RemoteException {
            Log.d(TAG, "openMap(" + (point != null ? "(" + point.getLatitude() + ", " + point.getLongitude() + ")" : "null") + ", " + zoomLevel + ")");
            if (point == null) {
                result.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "location == null");
                return;
            }
            if (!GeoUtils.inBoundMapWGS(point)) {
                result.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "Invalid location (out of range)");
                return;
            }
            if (!ZoomLevel.validateRange(zoomLevel)) {
                result.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "zoomLevel == " + zoomLevel);
                return;
            }

// MOD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
	    	GeoPoint coordinate;
	    	if( point.getWorldPoint() ){
	    		coordinate = GeoUtils.toTokyoDatum(point);
	    	}
	    	else{
	    		coordinate = point;
	    	}
// MOD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END

            NaviRun.GetNaviRunObj().setSearchKind(Constants.SCROLL_MAP_KIND);
            NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(NaviRun.NE_NEActType_ShowMapNormal, NaviRun.NE_NETransType_AroundSearchPoint,
                    (byte) zoomLevel, 1, coordinate.getLongitudeMs(), coordinate.getLatitudeMs(), 0);

            MapView.getInstance().setScrollUi();
            MapView.getInstance().setMapScrolled(true);

            if (forced) {
                NaviApplication naviApp = (NaviApplication) getApplication();
                if (naviApp == null) {
                    result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "naviApp == null");
                    return;
                }
                BaseActivity topActivity = null;
                try {
                    topActivity = naviApp.getBaseActivityStack().getTopActivity();
                } catch (Exception e) {
                    result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "getTopActivity() failed");
                    return;
                }
                topActivity.removeSubAllActivity();
            }
        }

        @Override
        public GeoPoint getCenter(NaviResult res) throws RemoteException {
            JNITwoLong coordinate = new JNITwoLong();
            NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(coordinate);

            return GeoUtils.toWGS84(coordinate.getM_lLat(), coordinate.getM_lLong());
        }

        @Override
        public void setCenter(NaviResult res, GeoPoint point) throws RemoteException {
            if (point == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "location == null");
                return;
            }
            if (!GeoUtils.inBoundMapWGS(point)) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "Invalid location (out of range)");
                return;
            }

// MOD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
	    	GeoPoint coordinate;
	    	if( point.getWorldPoint() ){
	    		coordinate = GeoUtils.toTokyoDatum(point);
	    	}
	    	else{
	    		coordinate = point;
	    	}
// MOD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END

            NaviRun.GetNaviRunObj().setSearchKind(Constants.SCROLL_MAP_KIND);
            NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(NaviRun.NE_NEActType_ShowMapNormal, NaviRun.NE_NETransType_AroundSearchPoint,
                    (byte) 0xff, 1, coordinate.getLongitudeMs(), coordinate.getLatitudeMs(), 0);

            MapView.getInstance().setScrollUi();
            MapView.getInstance().setMapScrolled(true);
        }

        @Override
        public int getZoom(NaviResult res) throws RemoteException {
            JNIByte mapLevel = new JNIByte();
            NaviRun.GetNaviRunObj().JNI_NE_GetMapLevel(mapLevel);
            return mapLevel.getM_bScale();
        }

        @Override
        public void setZoom(NaviResult res, int zoomLevel) throws RemoteException {
            if (!ZoomLevel.validateRange(zoomLevel)) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "zoomLevel == " + zoomLevel);
                return;
            }
// MOD.2014.02.24 N.Sasao 外部API経由の縮尺処理で必ず地点地図表示になる START
            byte bMapLevelOffset = (byte) (zoomLevel & 0xff);
            NaviRun.GetNaviRunObj().JNI_ct_uic_SetMapLevel( bMapLevelOffset );
            NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
// MOD.2014.02.24 N.Sasao 外部API経由の縮尺処理で必ず地点地図表示になる  END
        }

        @Override
        public void setDayNightMode(NaviResult res, int mode) throws RemoteException {
            if (mode == DayNightMode.DAY) {
                NaviRun.GetNaviRunObj().JNI_NE_SetMPMode(Constants.MAPMODE_DAY_OR_NIGHT);
                NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(Constants.TGL_MAPCOLOR_DAY);
                MapTypeListioner.isActive = false;
            }
            else if (mode == DayNightMode.NIGHT) {
                NaviRun.GetNaviRunObj().JNI_NE_SetMPMode(Constants.MAPMODE_DAY_OR_NIGHT);
                NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(Constants.MAPCOLOR_NIGHT);
                MapTypeListioner.isActive = false;
            }
            else if (mode == DayNightMode.AUTO) {
                MapTypeListioner.isActive = false;
                NaviRun.GetNaviRunObj().JNI_NE_SetMPMode(Constants.MAPMODE_AUTO);
                if (!CommonLib.isBMapColorAuto()) {
                    JNITwoLong Coordinate = new JNITwoLong();
                    NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);
                    boolean isDay = MapTypeListioner.getClockStsIsDay((int) Coordinate.getM_lLat(), (int) Coordinate.getM_lLong());
                    if (isDay) {
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(Constants.TGL_MAPCOLOR_DAY);
                    }
                    else {
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(Constants.MAPCOLOR_NIGHT);
                    }
                    CommonLib.setBMapColorAuto(true);
                    Thread t = new Thread(new MapTypeListioner());
                    MapTypeListioner.isRuning = true;
                    MapTypeListioner.isActive = true;
                    t.start();
                } else {
                    MapTypeListioner.isActive = true;
                }
            }
            NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();
        }

        /**
         * ユーザレイヤ管理クラス取得
         *
         * @param res
         * @return NaviRouteService
         */
        private DrawUserLayerManager getDrawUserLayerManager(NaviResult res) {
            NaviApplication naviApp = (NaviApplication) getApplication();
            if (naviApp == null) {
                res.setResultCode(NaviResult.RESULT_ILLEGAL_STATE);
                res.setDescription("getting Application class goes wrong.");
                return null;
            }
            DrawUserLayerManager userLayerMng = naviApp.getDrawUserLayerManager();
            if (userLayerMng == null) {
                res.setResultCode(NaviResult.RESULT_ILLEGAL_STATE);
                res.setDescription("getting DrawUserLayerManager class goes wrong.");
                return null;
            }
            return userLayerMng;
        }

        /**
         * 指定した地点にユーザーアイコンを描画する.
         */
        @Override
        public void drawIcon(NaviResult res, String layerId, Icon icon) throws RemoteException {
            if (layerId == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "layerId is null");
                return;
            }
            String layerIdTrim = layerId.trim();
            if (layerIdTrim.length() == 0) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "layerId is empty");
                return;
            }

            if (icon == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "userPoi is null");
                return;
            }

            if (icon.getDescription() == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "userPoi contentTitle is null.");
                return;
            }

            if (icon.isEmptyIconPath()) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "userPoi　icon path is empty.");
                return;
            }

            DrawUserLayerManager userLayerMng = getDrawUserLayerManager(res);
            if (res.getResultCode() != NaviResult.RESULT_OK) {
                return;
            }

            DrawUserLayer layer = userLayerMng.getUserLayer(layerIdTrim);
            if (layer == null) {
//MOD.2013.09.19 N.Sasao 外部連携固定パス変更・後処理対応実装 START
                userLayerMng.registUserLayer(new DrawUserLayer(layerIdTrim, ((NaviApplication) getApplication()).getNaviAppDataPath().getExternalDataPath()));
//MOD.2013.09.19 N.Sasao 外部連携固定パス変更・後処理対応実装  END
                layer = userLayerMng.getUserLayer(layerIdTrim);
            }

            if (layer != null && !layer.registDrawUserPoi(icon)) {
                res.setResultCode(NaviResult.RESULT_ILLEGAL_STATE);
                res.setDescription("error fucntion : registDrawUserPoi");
                return;
            }
        }

        /**
         * 指定した座標点列でライン形状を描画する.
         */
        @Override
        public void drawLine(NaviResult res, String layerId, Line line)
                throws RemoteException {
            if (layerId == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "layerId is null");
                return;
            }
            String layerIdTrim = layerId.trim();
            if (layerIdTrim.length() == 0) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "layerId is empty");
                return;
            }

            if (line == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "userLine is null");
                return;
            }
            if (line.isEmptyLocations()) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "userLine location is empty.");
                return;
            }
            if (line.getNumberOfPoints() < 2) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "userLine location point number is few.");
                return;
            }

            if (line.getDrawParam() == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "userLine drawparam is null.");
                return;
            }
            if (!line.getDrawParam().IsValidDrawParam()) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "userLine drawparam is invalid.");
                return;
            }

            DrawUserLayerManager userLayerMng = getDrawUserLayerManager(res);
            if (res.getResultCode() != NaviResult.RESULT_OK) {
                return;
            }

            DrawUserLayer layer = userLayerMng.getUserLayer(layerIdTrim);
            if (layer == null) {
//MOD.2013.09.19 N.Sasao 外部連携固定パス変更・後処理対応実装 START
                userLayerMng.registUserLayer(new DrawUserLayer(layerIdTrim, ((NaviApplication) getApplication()).getNaviAppDataPath().getExternalDataPath()));
//MOD.2013.09.19 N.Sasao 外部連携固定パス変更・後処理対応実装  END
                layer = userLayerMng.getUserLayer(layerIdTrim);
            }

            if (layer != null && !layer.registDrawUserLine(line)) {
                res.setResultCode(NaviResult.RESULT_ILLEGAL_STATE);
                res.setDescription("error fucntion : registDrawUserLine");
                return;
            }

            return;
        }

        /**
         * 指定したユーザレイヤを消去する.
         */
        @Override
        public void removeLayer(NaviResult res, String layerId)
                throws RemoteException {
            if (layerId == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "layerId is null");
                return;
            }
            String layerIdTrim = layerId.trim();
            if (layerIdTrim.length() == 0) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "layerId is empty");
                return;
            }

            DrawUserLayerManager userLayerMng = getDrawUserLayerManager(res);
            if (res.getResultCode() != NaviResult.RESULT_OK) {
                return;
            }

            userLayerMng.unregistUserLayer(layerIdTrim);
        }

        /**
         * 全てのユーザレイヤを消去する.
         */
        @Override
        public void removeAllLayers(NaviResult res) throws RemoteException {
            DrawUserLayerManager userLayerMng = getDrawUserLayerManager(res);
            if (res.getResultCode() != NaviResult.RESULT_OK) {
                return;
            }

            userLayerMng.unregistAllUserLayer();
        }

        @Override
        public RemainingDistance getRemainingDistance(NaviResult res) throws RemoteException {
            if (CommonLib.getNaviMode() != Constants.NE_NAVIMODE_CAR) {
                res.setResultCode(NaviResult.RESULT_ILLEGAL_STATE);
                res.setDescription("navi mode is not car mode.");
                return null;
            }
            if (!CommonLib.hasRoute()) {
                res.setResultCode(NaviResult.RESULT_ILLEGAL_STATE);
                res.setDescription("no target route.");
                return null;
            }

            ZNUI_DESTINATION_DATA destinationData = new ZNUI_DESTINATION_DATA();
            NaviRun.GetNaviRunObj().JNI_Java_GetDestination(destinationData);
            ZNUI_TIME time = destinationData.getStTime();
            Calendar mCalendar = Calendar.getInstance();
            mCalendar.setTimeInMillis(System.currentTimeMillis());
            int systemTime = mCalendar.get(Calendar.HOUR_OF_DAY) * 60 + mCalendar.get(Calendar.MINUTE);
            int arrivalTime = time.getNHouer() * 60 + time.getNMinute();
            long remainingTime;
            if (arrivalTime >= systemTime) {
                remainingTime = (arrivalTime - systemTime) * 60 * 1000L;
            } else {
                remainingTime = (arrivalTime + 1440 - systemTime) * 60 * 1000L;
            }
            RemainingDistance remainingDistance = new RemainingDistance();
            remainingDistance.setDistance(destinationData.getLnDistance());
            remainingDistance.setTime(remainingTime);
            return remainingDistance;
        }

        @Override
        public void calculateRoute(NaviResult res, RouteRequest routeRequest, boolean routeGuidanceStarting) throws RemoteException {
//            // ルート探索条件チェック
//            if (!CommonLib.checkRouteRequest(res, routeRequest)) {
//                return;
//            }

            // ナビモードチェック
            int naviMode = CommonLib.getNaviMode();
            if (naviMode != Constants.NE_NAVIMODE_CAR) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "naviMode == " + naviMode);
                return;
            }
            // 画面状態チェック
            NaviApplication naviApp = (NaviApplication) getApplication();
            if (naviApp == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "naviApp == null");
                return;
            }
            BaseActivity topActivity = null;
            BaseActivityStack baseActivityStack = naviApp
                    .getBaseActivityStack();
            if (baseActivityStack != null) {
                if (baseActivityStack.getTopActivity().isShowingDialog()) {
                    res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "isShowingDialog() returns true");
                    return;
                }

                baseActivityStack.removeSubAllActivity();
                try {
                    topActivity = baseActivityStack.getTopActivity();
                } catch (Exception e) {
                    res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "getTopActivity() failed");
                    return;
                }
            }
            if (topActivity == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "topActivity == null");
                return;
            }
            if (topActivity.getClass().equals(MapActivity.class)) {
                OpenMap.locationBack((MapActivity) topActivity);
            }
            int viewFlag = MapView.getInstance().getMap_view_flag();
            if (viewFlag != Constants.COMMON_MAP_VIEW
                    && viewFlag != Constants.OPEN_MAP_VIEW
                    && viewFlag != Constants.ROUTE_MAP_VIEW
                    && viewFlag != Constants.NAVI_MAP_VIEW
                    && viewFlag != Constants.SIMULATION_MAP_VIEW
                    && viewFlag != Constants.FIVE_ROUTE_MAP_VIEW
                    && viewFlag != Constants.ROUTE_MAP_VIEW_BIKE) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "viewFlag == " + viewFlag);
                return;
            }
            ((MapActivity) topActivity).startGuideForService(
                    routeRequest.getSearchType(), routeRequest.getRegulations(),
                    CommonLib.getRoutePoints(routeRequest.getOrigin(), routeRequest.getDestination(), routeRequest.getWaypoints()),
                    routeGuidanceStarting);
        }

        @Override
        public void startRouteGuidance(NaviResult res) throws RemoteException {
            // ナビモードチェック
            int naviMode = CommonLib.getNaviMode();
            if (naviMode != Constants.NE_NAVIMODE_CAR) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "naviMode == " + naviMode);
                return;
            }
            // 画面状態チェック
            NaviApplication naviApp = (NaviApplication) getApplication();
            if (naviApp == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "naviApp == null");
                return;
            }
            BaseActivity topActivity = null;
            BaseActivityStack baseActivityStack = naviApp
                    .getBaseActivityStack();
            if (baseActivityStack == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "baseActivityStack is null");
                return;
            }
            try {
                topActivity = baseActivityStack.getTopActivity();
            } catch (Exception e) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "getTopActivity() failed");
                return;
            }
            if (topActivity == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "topActivity == null");
                return;
            }
            if (topActivity.isShowingDialog()) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "isShowingDialog() returns true");
                return;
            }
            if (baseActivityStack.getActivityCount() == 1 && topActivity.getClass().equals(MapActivity.class)) {
                OpenMap.locationBack((MapActivity) topActivity);
            } else {
                baseActivityStack.removeSubAllActivity();

                topActivity = baseActivityStack.getTopActivity();
                ((MapActivity) topActivity).startGuideForServiceDelayed();

                return;
            }
            int viewFlag = MapView.getInstance().getMap_view_flag();
            if (viewFlag != Constants.OPEN_MAP_VIEW && viewFlag != Constants.ROUTE_MAP_VIEW) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "viewFlag == " + viewFlag);
                return;
            }
            ((MapActivity) topActivity).startGuideForService();
        }

        @Override
        public void stopRouteGuidance(NaviResult res) throws RemoteException {
            // ナビモードチェック
            int naviMode = CommonLib.getNaviMode();
            if (naviMode != Constants.NE_NAVIMODE_CAR) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "naviMode == " + naviMode);
                return;
            }
            // 画面状態チェック
            NaviApplication naviApp = (NaviApplication) getApplication();
            if (naviApp == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "naviApp == null");
                return;
            }
            BaseActivity topActivity = null;
            BaseActivityStack baseActivityStack = naviApp
                    .getBaseActivityStack();
            if (baseActivityStack == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "baseActivityStack is null");
                return;
            }
            try {
                topActivity = baseActivityStack.getTopActivity();
            } catch (Exception e) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "getTopActivity() failed");
                return;
            }
            if (topActivity == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "topActivity == null");
                return;
            }
            if (topActivity.isShowingDialog()) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "isShowingDialog() returns true");
                return;
            }
            if (baseActivityStack.getActivityCount() == 1 && topActivity.getClass().equals(MapActivity.class)) {
                OpenMap.locationBack((MapActivity) topActivity);
            }
            else {
                CommonLib.setChangePos(false);
                CommonLib.setChangePosFromSearch(false);
// Add by CPJsunagawa '2015-07-08 Start
                CommonLib.setRegIcon(false);
                CommonLib.setInputText(false);
// Add by CPJsunagawa '2015-07-08 End
            	CommonLib.setChangePos(false);
                baseActivityStack.removeSubAllActivity();

                topActivity = baseActivityStack.getTopActivity();
                ((MapActivity) topActivity).endGuideForServiceDelayed();

                return;
            }
            int viewFlag = MapView.getInstance().getMap_view_flag();
            if (viewFlag != Constants.ROUTE_MAP_VIEW
                    && viewFlag != Constants.NAVI_MAP_VIEW
                    && viewFlag != Constants.SIMULATION_MAP_VIEW
                    && viewFlag != Constants.FIVE_ROUTE_MAP_VIEW
                    && viewFlag != Constants.ROUTE_MAP_VIEW_BIKE) {
                res.setResult(NaviResult.RESULT_ILLEGAL_STATE, "viewFlag == " + viewFlag);
                return;
            }
            ((MapActivity) topActivity).endGuideForService();
        }

        @Override
        public void addFavorites(NaviResult res, RoutePoint point, int categoryId) throws RemoteException {
            if (point == null) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "naviPoint == null");
                return;
            }
            if (!GeoUtils.inBoundMapWGS(point.getPoint())) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "Invalid location (out of range)");
                return;
            }
            if (point.getName() == null || point.getName().length() == 0) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "Point name is blank");
                return;
            }
            if (!FavoritesCategory.isValidCategory(categoryId)) {
                res.setResult(NaviResult.RESULT_ILLEGAL_ARGUMENT, "categoryId == " + categoryId);
                return;
            }

// MOD.2013.12.04 N.Sasao 緯度経度日本測地系対応 START
	    	GeoPoint coordinate;
	    	if( point.getPoint().getWorldPoint() ){
	    		coordinate = GeoUtils.toTokyoDatum(point.getPoint());
	    	}
	    	else{
	    		coordinate = point.getPoint();
	    	}
// MOD.2013.12.04 N.Sasao 緯度経度日本測地系対応  END

            POI_UserFile_RecordIF record = new POI_UserFile_RecordIF();
            record.m_DispName = point.getName();
            record.m_lLon = coordinate.getLongitudeMs();
            record.m_lLat = coordinate.getLatitudeMs();
            record.m_lIconCode = 1;

            POI_UserFile_DataIF data = new POI_UserFile_DataIF();
            data.m_sDataIndex = 0;
            data.m_sDataSize = (short) point.getName().getBytes().length;
            data.m_Data = point.getName();

            long rc = NaviRun.GetNaviRunObj().JNI_NE_POIMybox_SetRec(categoryId, record, 1, data);
            if (rc != NaviEngineError.NE_SUCCESS) {
                res.setResult(NaviResult.RESULT_NAVI_ENGINE_ERROR, "JNI_NE_POIMybox_SetRec() returns: 0x" + Long.toHexString(rc));
                return;
            }
        }

        @Override
        public boolean isNaviAppRunning() {
            return NaviAppStatus.isAppRunning();
        }

        // Add 2011/11/02 sawada Start -->
        /**
         * 自車位置更新イベントリスナの登録
         */
        @Override
        public void registerVehiclePositionListener(IVehiclePositionListener listener) throws RemoteException {
            IntentFilter filter = new IntentFilter(NaviIntent.ACTION_VEHICLE_POSITION);
            registerReceiver(mVehiclePositionBroadcastReceiver, filter);
            mVehiclePositionListeners.register(listener);
        }

        /**
         * 自車位置更新イベントリスナの解除
         */
        @Override
        public void unregisterVehiclePositionListener(IVehiclePositionListener listener) throws RemoteException {
            unregisterReceiver(mVehiclePositionBroadcastReceiver);
            mVehiclePositionListeners.unregister(listener);
        }

        /**
         * 道路種別変更イベントリスナの登録
         */
        @Override
        public void registerRoadListener(IRoadListener listener) throws RemoteException {
            IntentFilter filter = new IntentFilter(NaviIntent.ACTION_ROAD_MOVED);
            registerReceiver(mRoadBroadcastReceiver, filter);
            mRoadListeners.register(listener);
        }

        /**
         * 自車位置更新イベントリスナの解除
         */
        @Override
        public void unregisterRoadListener(IRoadListener listener) throws RemoteException {
            unregisterReceiver(mRoadBroadcastReceiver);
            mRoadListeners.unregister(listener);
        }

        /**
         * 行政界移動イベントリスナの登録
         */
        @Override
        public void registerAreaListener(IAreaListener listener) throws RemoteException {
            IntentFilter filter = new IntentFilter(NaviIntent.ACTION_AREA_MOVED);
            registerReceiver(mAreaBroadcastReceiver, filter);
            mAreaListeners.register(listener);
        }

        /**
         * 行政界移動イベントリスナの解除
         */
        @Override
        public void unregisterAreaListener(IAreaListener listener) throws RemoteException {
            unregisterReceiver(mAreaBroadcastReceiver);
            mAreaListeners.unregister(listener);
        }

//        /**
//         * 表示地図操作イベントリスナの登録
//         */
//        @Override
//        public void registerMapControlListener(IMapControlListener listener) throws RemoteException {
//            IntentFilter filter = new IntentFilter(NaviIntent.ACTION_MAP_CONTROL);
//            registerReceiver(mMapControlBroadcastReceiver, filter);
//            mMapControlListeners.register(listener);
//        }
//
//        /**
//         * 表示地図操作イベントリスナの解除
//         */
//        @Override
//        public void unregisterMapControlListener(IMapControlListener listener) throws RemoteException {
//            unregisterReceiver(mMapControlBroadcastReceiver);
//            mMapControlListeners.unregister(listener);
//        }

        /**
         * ルート案内イベントリスナの登録
         */
        @Override
        public void registerRouteGuidanceListener(IRouteGuidanceListener listener) throws RemoteException {
            IntentFilter filter = new IntentFilter(NaviIntent.ACTION_GUIDE);
            registerReceiver(mRouteGuidanceBroadcastReceiver, filter);
            mRouteGuidanceListeners.register(listener);
        }

        /**
         * ルート案内イベントリスナの解除
         */
        @Override
        public void unregisterRouteGuidanceListener(IRouteGuidanceListener listener) throws RemoteException {
            unregisterReceiver(mRouteGuidanceBroadcastReceiver);
            mRouteGuidanceListeners.unregister(listener);
        }

        /**
         * ルート探索イベントリスナの登録
         */
        @Override
        public void registerRouteCalcListener(IRouteCalcListener listener) throws RemoteException {
            IntentFilter filter = new IntentFilter(NaviIntent.ACTION_ROUTE_CALCULATED);
            registerReceiver(mRouteCalcBroadcastReceiver, filter);
            mRouteCalcListeners.register(listener);
        }

        /**
         * ルート探索イベントリスナの解除
         */
        @Override
        public void unregisterRouteCalcListener(IRouteCalcListener listener) throws RemoteException {
            unregisterReceiver(mRouteCalcBroadcastReceiver);
            mRouteCalcListeners.unregister(listener);
        }

        // Add 2011/11/02 sawada End <--
        /**
         * アプリケーションステータスリスナの登録
         */
        @Override
        public void registerApplicationStatusListener(IApplicationStatusListener listener) throws RemoteException {
            IntentFilter filter = new IntentFilter();
            filter.addAction(NaviIntent.ACTION_STARTED);
            filter.addAction(NaviIntent.ACTION_STOPPED);
            registerReceiver(mApplicationStatusBroadcastReceiver, filter);
            mApplicationStatusListeners.register(listener);
        }

        /**
         * アプリケーションステータスリスナの解除
         */
        @Override
        public void unregisterApplicationStatusListener(IApplicationStatusListener listener) throws RemoteException {
            unregisterReceiver(mApplicationStatusBroadcastReceiver);
            mApplicationStatusListeners.unregister(listener);
        }
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応 START
        /**
         * 自車位置更新イベントリスナの登録
         */
        @Override
        public void registerVehiclePositionExListener(IVehiclePositionExListener listener) throws RemoteException {
            IntentFilter filter = new IntentFilter(NaviIntent.ACTION_VEHICLE_EX_POSITION);
            registerReceiver(mVehiclePositionExBroadcastReceiver, filter);
            mVehiclePositionExListeners.register(listener);
        }

        /**
         * 自車位置更新イベントリスナの解除
         */
        @Override
        public void unregisterVehiclePositionExListener(IVehiclePositionExListener listener) throws RemoteException {
            unregisterReceiver(mVehiclePositionExBroadcastReceiver);
            mVehiclePositionExListeners.unregister(listener);
        }
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応  END
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
    	/**
         * 走行規制機能の制御許可申請
         */
        @Override
        public boolean requestDrivingControlLock( int nKey ) throws RemoteException {
            return NaviRun.GetNaviRunObj().requestDrivingControlLock( nKey );
        }
    	/**
         * 走行規制機能の制御解放
         */
        @Override
        public boolean requestDrivingControlUnLock( int nKey ) throws RemoteException {
            return NaviRun.GetNaviRunObj().requestDrivingControlUnLock( nKey );
        }
    	/**
         * 走行規制指示
         */
        @Override
        public boolean requestDrivingControl( int nKey, boolean bControl ) throws RemoteException {
            return NaviRun.GetNaviRunObj().requestDrivingControl( nKey, bControl );
        }
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
    };

    /**
     * 自車位置更新イベント
     */
    private RemoteCallbackList<IVehiclePositionListener> mVehiclePositionListeners = new RemoteCallbackList<IVehiclePositionListener>();
    private BroadcastReceiver mVehiclePositionBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals(NaviIntent.ACTION_VEHICLE_POSITION)) {
                    Location location = (Location) intent.getParcelableExtra(NaviIntent.EXTRA_LOCATION);
                    int n = mVehiclePositionListeners.beginBroadcast();
                    for (int i = 0; i < n; i++) {
                        try {
                            mVehiclePositionListeners.getBroadcastItem(i).onChanged(location);
                        }
                        catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    mVehiclePositionListeners.finishBroadcast();
                }
            }
        }
    };

    /**
     * 道路種別変更イベント
     */
    private RemoteCallbackList<IRoadListener> mRoadListeners = new RemoteCallbackList<IRoadListener>();
    private BroadcastReceiver mRoadBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals(NaviIntent.ACTION_ROAD_MOVED)) {
                    Location location = (Location) intent.getParcelableExtra(NaviIntent.EXTRA_LOCATION);
                    int roadFrom = intent.getIntExtra(NaviIntent.EXTRA_ROAD_FROM, -1);
                    int roadTo = intent.getIntExtra(NaviIntent.EXTRA_ROAD_TO, -1);
                    Road from = new Road(roadFrom, null);
                    Road to = new Road(roadTo, null);
                    int n = mRoadListeners.beginBroadcast();
                    for (int i = 0; i < n; i++) {
                        try {
                            mRoadListeners.getBroadcastItem(i).onChanged(location, from, to);
                        }
                        catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    mRoadListeners.finishBroadcast();
                }
            }
        }
    };

    /**
     * 行政界移動イベント
     */
    private RemoteCallbackList<IAreaListener> mAreaListeners = new RemoteCallbackList<IAreaListener>();
    private BroadcastReceiver mAreaBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals(NaviIntent.ACTION_AREA_MOVED)) {
                    Location location = (Location) intent.getParcelableExtra(NaviIntent.EXTRA_LOCATION);
                    List<String> nameFrom = intent.getStringArrayListExtra(NaviIntent.EXTRA_ADMIN_FROM);
                    List<String> nameTo = intent.getStringArrayListExtra(NaviIntent.EXTRA_ADMIN_TO);
                    Area from = new Area(nameFrom);
                    Area to = new Area(nameTo);
                    int n = mAreaListeners.beginBroadcast();
                    for (int i = 0; i < n; i++) {
                        try {
                            mAreaListeners.getBroadcastItem(i).onChanged(location, from, to);
                        }
                        catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    mAreaListeners.finishBroadcast();
                }
            }
        }
    };

//    /**
//     * 表示地図操作イベント
//     */
//    private RemoteCallbackList<IMapControlListener> mMapControlListeners = new RemoteCallbackList<IMapControlListener>();
//    private BroadcastReceiver mMapControlBroadcastReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            if (action != null) {
//                if (action.equals(NaviIntent.ACTION_MAP_CONTROL)) {
//                    GeoPoint point = (GeoPoint) intent.getParcelableExtra(NaviIntent.EXTRA_POINT);
//                    int n = mMapControlListeners.beginBroadcast();
//                    for (int i = 0; i < n; i++) {
//                        try {
//                            mMapControlListeners.getBroadcastItem(i).onMoved(point);
//                        }
//                        catch (RemoteException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    mMapControlListeners.finishBroadcast();
//                }
//            }
//        }
//    };

    /**
     * ルート案内イベント
     */
    private RemoteCallbackList<IRouteGuidanceListener> mRouteGuidanceListeners = new RemoteCallbackList<IRouteGuidanceListener>();
    private BroadcastReceiver mRouteGuidanceBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals(NaviIntent.ACTION_GUIDE)) {
                    int eventType = intent.getIntExtra(NaviIntent.EXTRA_GUIDE_TYPE, RouteGuidance.TYPE_EVENT_NONE);
                    Location location = (Location) intent.getParcelableExtra(NaviIntent.EXTRA_LOCATION);
                    int targetType = intent.getIntExtra(NaviIntent.EXTRA_TARGET_TYPE, RouteGuidance.TYPE_TARGET_NONE);
                    String targetName = intent.getStringExtra(NaviIntent.EXTRA_TARGET_NAME);
                    Location targetLocation = (Location) intent.getParcelableExtra(NaviIntent.EXTRA_TARGET_LOCATION);
                    float targetDistance = intent.getFloatExtra(NaviIntent.EXTRA_TARGET_DISTANCE, 0.0F);
                    int targetDirection = intent.getIntExtra(NaviIntent.EXTRA_TARGET_DIRECTION, RouteGuidance.TYPE_DIRECTION_NONE);
                    RouteGuidance routeGuidance = new RouteGuidance(eventType, location, targetType, targetName, targetLocation,
                            targetDistance, targetDirection);

                    int n;
                    switch (eventType) {
                    case RouteGuidance.TYPE_EVENT_STARTGUIDE: // 案内開始
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
                        DebugLogOutput.put("RakuRaku-Log: BroadcastReceiver-onReceive TYPE_EVENT_STARTGUIDE Start");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
                        n = mRouteGuidanceListeners.beginBroadcast();
                        for (int i = 0; i < n; i++) {
                            try {
                                mRouteGuidanceListeners.getBroadcastItem(i).onStarted(routeGuidance);
                            }
                            catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
                        DebugLogOutput.put("RakuRaku-Log: BroadcastReceiver-onReceive mRouteGuidanceListenersCount->" + String.valueOf(n));
                        DebugLogOutput.put("RakuRaku-Log: BroadcastReceiver-onReceive TYPE_EVENT_STARTGUIDE End");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
                        mRouteGuidanceListeners.finishBroadcast();
                        break;
                    case RouteGuidance.TYPE_EVENT_ENDGUIDE: // 案内終了
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
                        DebugLogOutput.put("RakuRaku-Log: BroadcastReceiver-onReceive TYPE_EVENT_ENDGUIDE Start");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
                        n = mRouteGuidanceListeners.beginBroadcast();
                        for (int i = 0; i < n; i++) {
                            try {
                                mRouteGuidanceListeners.getBroadcastItem(i).onStopped(routeGuidance);
                            }
                            catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
                        DebugLogOutput.put("RakuRaku-Log: BroadcastReceiver-onReceive mRouteGuidanceListenersCount->" + String.valueOf(n));
                        DebugLogOutput.put("RakuRaku-Log: BroadcastReceiver-onReceive TYPE_EVENT_ENDGUIDE End");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
                        mRouteGuidanceListeners.finishBroadcast();
                        break;
                    case RouteGuidance.TYPE_EVENT_CROSSGUIDE: // 目標案内
                        n = mRouteGuidanceListeners.beginBroadcast();
                        for (int i = 0; i < n; i++) {
                            try {
                                mRouteGuidanceListeners.getBroadcastItem(i).onNearToCrossroad(routeGuidance);
                            }
                            catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                        mRouteGuidanceListeners.finishBroadcast();
                        break;
                    case RouteGuidance.TYPE_EVENT_VIAGUIDE: // 経由地案内
                        n = mRouteGuidanceListeners.beginBroadcast();
                        for (int i = 0; i < n; i++) {
                            try {
                                mRouteGuidanceListeners.getBroadcastItem(i).onNearToWaypoint(routeGuidance);
                            }
                            catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                        mRouteGuidanceListeners.finishBroadcast();
                        break;
                    case RouteGuidance.TYPE_EVENT_GOALGUIDE: // 目的地案内
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
                        DebugLogOutput.put("RakuRaku-Log: BroadcastReceiver-onReceive TYPE_EVENT_GOALGUIDE Start");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
                        n = mRouteGuidanceListeners.beginBroadcast();
                        for (int i = 0; i < n; i++) {
                            try {
                                mRouteGuidanceListeners.getBroadcastItem(i).onNearToDestination(routeGuidance);
                            }
                            catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
                        DebugLogOutput.put("RakuRaku-Log: BroadcastReceiver-onReceive mRouteGuidanceListenersCount->" + String.valueOf(n));
                        DebugLogOutput.put("RakuRaku-Log: BroadcastReceiver-onReceive TYPE_EVENT_GOALGUIDE End");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
                        mRouteGuidanceListeners.finishBroadcast();
                        break;
                    case RouteGuidance.TYPE_EVENT_REROUTE: // リルート
                        n = mRouteGuidanceListeners.beginBroadcast();
                        for (int i = 0; i < n; i++) {
                            try {
                                mRouteGuidanceListeners.getBroadcastItem(i).onRerouted(routeGuidance);
                            }
                            catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                        mRouteGuidanceListeners.finishBroadcast();
                        break;
                    default:
                        break;
                    }
                }
            }
        }
    };

    /**
     * ルート探索イベント
     */
    private RemoteCallbackList<IRouteCalcListener> mRouteCalcListeners = new RemoteCallbackList<IRouteCalcListener>();
    private BroadcastReceiver mRouteCalcBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals(NaviIntent.ACTION_ROUTE_CALCULATED)) {
                    RouteCalculated routeCalculated = (RouteCalculated) intent.getParcelableExtra(NaviIntent.EXTRA_ROUTE_CALCULATED);
                    int n = mRouteCalcListeners.beginBroadcast();
                    for (int i = 0; i < n; i++) {
                        try {
                            mRouteCalcListeners.getBroadcastItem(i).onRouteCalculated(routeCalculated);
                        }
                        catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    mRouteCalcListeners.finishBroadcast();
                }
            }
        }
    };

    /**
     * アプリケーションステータス
     */
    private RemoteCallbackList<IApplicationStatusListener> mApplicationStatusListeners = new RemoteCallbackList<IApplicationStatusListener>();
    private BroadcastReceiver mApplicationStatusBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                int n = mApplicationStatusListeners.beginBroadcast();
                for (int i = 0; i < n; i++) {
                    try {
                        if (action.equals(NaviIntent.ACTION_STARTED)) {
                            mApplicationStatusListeners.getBroadcastItem(i).onStarted();
                        } else if (action.equals(NaviIntent.ACTION_STOPPED)) {
                            mApplicationStatusListeners.getBroadcastItem(i).onStopped();
                        }
                    }
                    catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                mApplicationStatusListeners.finishBroadcast();
            }
        }
    };
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応 START
    /**
	* 自車位置更新イベント(高速道路上有無付)
     */
    private RemoteCallbackList<IVehiclePositionExListener> mVehiclePositionExListeners = new RemoteCallbackList<IVehiclePositionExListener>();
    private BroadcastReceiver mVehiclePositionExBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals(NaviIntent.ACTION_VEHICLE_EX_POSITION)) {
                    VehiclePositionEx vehiclePosition = (VehiclePositionEx) intent.getParcelableExtra(NaviIntent.EXTRA_LOCATION_EX);
                    int n = mVehiclePositionExListeners.beginBroadcast();
                    for (int i = 0; i < n; i++) {
                        try {
                            mVehiclePositionExListeners.getBroadcastItem(i).onChanged(vehiclePosition);
                        }
                        catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    mVehiclePositionExListeners.finishBroadcast();
                }
            }
        }
    };
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応  END
}
