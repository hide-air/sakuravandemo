package net.zmap.android.pnd.v2.api.event;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 道路種別変更情報クラス
 *
 * @see RoadListener
 * @see RoadListener#onChanged(Location, Road, Road)
 */
public final class Road implements Parcelable {
    private int             mType;
    private String          mName;

    /** 不明 (初期値) */
    public final static int TYPE_UNKNOWN          = -1;
    /** 高速 */
    public final static int TYPE_EXPRESS_WAY      = 0;
    /** 都市高速 */
    public final static int TYPE_CITY_EXPRESS_WAY = 1;
    /** 国道 */
    public final static int TYPE_NATIONAL_ROAD    = 2;
    /** 主要地方道 */
    public final static int TYPE_MAJOR_ROAD       = 3;
    /** 県道 */
    public final static int TYPE_PREFECTURAL_ROAD = 4;
    /** 一般道路 1 */
    public final static int TYPE_GENERAL_ROAD_1   = 5;
    /** 一般道路 2 */
    public final static int TYPE_GENERAL_ROAD_2   = 6;
    /** 導入路 */
    public final static int TYPE_IN_ROUTE         = 7;
    /** 細街路 1 */
    public final static int TYPE_MINOR_ROAD_1     = 8;
    /** 細街路 2 */
    public final static int TYPE_MINOR_ROAD_2     = 9;
    /** フェリー航路 */
    public final static int TYPE_FERRY_ROUTE      = 10;

    /**
     * コンストラクタ
     */
    public Road() {
    }

    private Road(Parcel in) {
        readFromPercel(in);
    }

    /**
     * コンストラクタ
     *
     * @param type　道路種別
     *
     * @param name　道路名称
     *
     */
    public Road(int type, String name) {
        mType = type;
        mName = name;
    }

    /**
     * 道路種別の取得
     *
     * @return 道路種別
     */
    public int getType() {
        return mType;
    }

    /**
     * 道路種別の設定
     *
     * @param type　道路種別
     *
     */
    public void setType(int type) {
        mType = type;
    }

    /**
     * 道路名称の取得
     *
     * @return 道路名称
     */
    public String getName() {
        return mName;
    }

    /**
     * 道路名称の設定
     *
     * @param name
     *            道路名称
     */
    public void setName(String name) {
        mName = name;
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(mType);
        out.writeString(mName);
    }

    private void readFromPercel(Parcel in) {
        mType = in.readInt();
        mName = in.readString();
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /* (非 Javadoc)
     *
     */
    public static final Parcelable.Creator<Road> CREATOR = new Parcelable.Creator<Road>() {
        public Road createFromParcel(Parcel in) {
            return new Road(in);
        }
        public Road[] newArray(int size) {
            return new Road[size];
        }
    };
}