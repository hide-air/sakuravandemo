package net.zmap.android.pnd.v2.common.data;

public class Position
{
	private long m_wLong;
	private long m_wLat;

	public Position()
	{

	}

	public Position(long wLong,long wLat)
	{
		m_wLong = wLong;
		m_wLat = wLat;
	}

	public long getLongitude()
	{
		return m_wLong;
	}

	public long getLatitude()
	{
		return m_wLat;
	}

	public void setLongitude(long wLong)
	{
		m_wLong = wLong;
	}

	public void setLatitude(long wLat)
	{
		m_wLat = wLat;
	}
}
