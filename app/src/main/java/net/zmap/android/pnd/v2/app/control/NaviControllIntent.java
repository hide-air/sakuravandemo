package net.zmap.android.pnd.v2.app.control;

import android.content.Context;
import android.content.Intent;

import net.zmap.android.pnd.v2.common.utils.NaviLog;
//import net.zmap.android.pnd.v2.downloader.common.ExtraName;

/**
 * ナビを制御するインテントの定義
 */
public final class NaviControllIntent {

    //アプリ側プロセスで受け取るブロードキャスト
    /**
     * ナビアプリのアクション名：アプリ終了
     */
    public static final String ACTION_NAVI_APP_FINISH = "net.zmap.android.pnd.v2.app.action.NAVI_APP_FINISH";

    /**
     * ナビアプリのアクション名：地図アクティビティ起動
     */
    public static final String ACTION_MAP_ACTIVITY_START = "net.zmap.android.pnd.v2.app.action.MAP_ACTIVITY_START";

    /**
     * ナビアプリのアクション名：地図表示完了通知
     */
    public static final String ACTION_MAP_FIRST_DRAW_FINISH = "net.zmap.android.pnd.v2.app.action.MAP_FIRST_DRAW_FINISH";

//    //サービス側プロセスで受け取るブロードキャスト
//    /**
//     * ナビアプリのアクション名：地図ダウンロード処理開始
//     */
//    public static final String ACTION_DOWNLOAD_MAP_START = "net.zmap.android.pnd.v2.app.action.DOWNLOAD_MAP_START";
//
    /**
     * ナビアプリのアクション名：地図ダウンロード処理完了通知
     */
    public static final String ACTION_DOWNLOAD_MAP_FINISHED = "net.zmap.android.pnd.v2.app.action.DOWNLOAD_MAP_FINISHED";

//    /**
//     * ナビアプリのアクション名：ナビアプリ終了(地図ダウンロード由来、起動サービスのプロセスは終了しない)
//     */
//    public static final String ACTION_NAVI_APP_FINISH_ON_DOWNLOAD_MAP = "net.zmap.android.pnd.v2.app.action.NAVI_APP_FINISH_ON_DOWNLOAD_MAP";

//Add 2011/11/16 Z01_h_yamada Start -->
    /**
     * ナビアプリのアクション名：SDカード容量不足検知通知
     */
    public static final String ACTION_NAVI_APP_MEDIA_SIZE_INSUFFICIENT = "net.zmap.android.pnd.v2.app.action.NAVI_APP_MEDIA_SIZE_INSUFFICIENT";
//Add 2011/11/16 Z01_h_yamada End <--


//    /*
//     * 地図ダウンロードブロードキャスト送信
//     *
//     * @param int[] idList 地図ダウンロードコンテンツIDリスト
//     */
//    public static void sendBroadcastDownloadMapStart(Context context, int[] idList) {
//        Intent intent = new Intent();
//        intent.setAction(NaviControllIntent.ACTION_DOWNLOAD_MAP_START);
////Add 2011/10/05 Z01yoneya Start -->
//        if (idList != null && idList.length > 0) {
//            intent.putExtra(ExtraName.DL_CONTENTS, idList);
//        }
////Add 2011/10/05 Z01yoneya End <--
//        context.sendBroadcast(intent);
//    }
//
//    /*
//     * ナビアプリ終了(地図ダウンロード由来)送信
//     */
//    public static void sendBroadcastNaviAppFinishOnDownloadMap(Context context, int[] idList) {
//        Intent intent = new Intent();
//        intent.setAction(NaviControllIntent.ACTION_NAVI_APP_FINISH_ON_DOWNLOAD_MAP);
//        intent.putExtra(ExtraName.DL_CONTENTS, idList);
//        context.sendBroadcast(intent);
//    }

//Add 2011/11/16 Z01_h_yamada Start -->
    /*
     * SDカード容量不足検知通知送信
     */
    public static void sendMediaSizeInsufficient(Context context) {
        Intent intent = new Intent();
        intent.setAction(NaviControllIntent.ACTION_NAVI_APP_MEDIA_SIZE_INSUFFICIENT);
        context.sendBroadcast(intent);
    }
//Add 2011/11/16 Z01_h_yamada End <--

}
