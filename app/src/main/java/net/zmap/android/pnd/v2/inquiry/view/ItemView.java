package net.zmap.android.pnd.v2.inquiry.view;

import android.content.Context;
import android.inputmethodservice.InputMethodService;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;

public class ItemView extends InputMethodService
{

	public static final short STYLE_DEFAULT = 0x00;

	/** ItemView ID*/
	private int m_wId = -1;

//	private boolean m_bIsEnable = true;

//	private boolean m_bDoItemClick = false;

//	private boolean m_bDoItemLongClick = false;
	protected View m_oView = null;

//	protected Intent m_oData = new Intent();


	private int m_oResId;

	public ItemView(int wId,View oView)
	{
		m_oView = oView;
		m_wId = wId;
//		ItemView o_View = new ItemView(m_wId,m_oView);
//		o_View.clone();
	}

	public ItemView(Context oContext,int wId,int wResId)
	{
		m_oResId = wResId;
		LayoutInflater oInflater = LayoutInflater.from(oContext);
		m_oView = (View)oInflater.inflate(wResId, null);
		m_oView.setOnFocusChangeListener(new OnFocusChangeListener(){

			@Override
			public void onFocusChange(View oView, boolean bHasFocus)
			{
				if(bHasFocus)
				{
					onFocus();
				}
				else
				{
					onLostFocus();
				}
			}

		});
		m_wId = wId;
	}

	public View findViewById(Context oContext,int wId)
	{
		return m_oView.findViewById(wId);
	}


//	public void setEnable(boolean bEnable)
//	{
//		m_bIsEnable = bEnable;
//	}
//
//	public boolean isEnable()
//	{
//		return m_bIsEnable;
//	}


//	public boolean isM_bDoItemClick() {
//		return m_bDoItemClick;
//	}
//
//	public void setM_bDoItemClick(boolean mBDoItemClick) {
//		m_bDoItemClick = mBDoItemClick;
//	}

//	public boolean isM_bDoItemLongClick() {
//		return m_bDoItemLongClick;
//	}
//
//	public void setM_bDoItemLongClick(boolean mBDoItemLongClick) {
//		m_bDoItemLongClick = mBDoItemLongClick;
//	}


	public View getView()
	{
		return m_oView;
	}

	public void onClick()
	{
	}

	public void onLongClick()
	{
	}

	public void onFocus()
	{

	}

	public void onLostFocus()
	{

	}

	public int getId()
	{
		return m_wId;
	}

//	public Intent getData()
//	{
//		return m_oData;
//	}

	public int getResId(){
		return m_oResId;
	}
}
