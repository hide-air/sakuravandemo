/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZVP_SatInfo_t.java
 * Description    個別のGPS衛星情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZVP_SatInfo_t {
    public int      wSatID;
    public float    wAzimuth;
    public float    wElevation;
    public float    wSNRate;
    public boolean      bEnable;
    /**
    * Created on 2010/08/06
    * Title:       getWSatID
    * Description:   衛星番号を取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getWSatID() {
        return wSatID;
    }

    /**
    * Created on 2010/08/06
    * Title:       getWAzimuth
    * Description:   方位角（北0°で時計回り）を取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public float getWAzimuth() {
        return wAzimuth;
    }
    /**
    * Created on 2010/08/06
    * Title:       getWElevation
    * Description:   仰角（水平方向0°で上方が正）を取得する
    * @param1  無し
    * @return       float

    * @version        1.0
    */
    public float getWElevation() {
        return wElevation;
    }

    /**
    * Created on 2010/08/06
    * Title:       getWSNRate
    * Description:   S/N比を取得する
    * @param1  無し
    * @return       float

    * @version        1.0
    */
    public float getWSNRate() {
        return wSNRate;
    }

    /**
    * Created on 2010/08/06
    * Title:       isBEnable
    * Description:   TRUE：受信、FALSE：未受信 を取得する
    * @param1  無し
    * @return       boolean

    * @version        1.0
    */
    public boolean isBEnable() {
        return bEnable;
    }
}
