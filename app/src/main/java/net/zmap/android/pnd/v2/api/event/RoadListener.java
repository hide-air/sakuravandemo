package net.zmap.android.pnd.v2.api.event;

import android.location.Location;

/**
 * 道路種別変更リスナインタフェース
 *
 * @see Road
 */
public interface RoadListener {
    /**
     * 道路種別が変化したときに呼ばれるリスナ関数 (車モードのみ)
     *
     * @param location
     *
     * @param from
     *
     * @param to
     *
     */
    public void onChanged(Location location, Road from, Road to);
}
