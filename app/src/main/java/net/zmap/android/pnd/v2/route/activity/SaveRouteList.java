/**
 ********************************************************************
 * Copyright (c) 2010.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project        MarketV2
 * File           SaveRouteList.java
 * Description    ルート保存画面
 * Created on     2010/11/24
 *
 ********************************************************************
 */

package net.zmap.android.pnd.v2.route.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.RouteDataFile;
import net.zmap.android.pnd.v2.common.activity.MenuBaseActivity;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.route.data.RouteData;
import net.zmap.android.pnd.v2.route.data.RouteNodeData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2010/11/29
 * <p>
 * Title: 保存ルート一覧画面
 * <p>
 * Description
 * <p>
 * author　XuYang version 1.0
 */
public class SaveRouteList extends MenuBaseActivity {

    private String filePath = "";
    private List<String> UTCList = null;
    private List<String> RouteName = null;
    private List<RouteNodeData> lstRouteNodeDate = new ArrayList<RouteNodeData>();
    private ScrollList oBox = null;
    private ScrollTool oTool = null;
    private int DIALOG_DEL_CONFIRM = 1;
    private String delDate = null;
    private String delRouteName = null;

    /**
     * 初期化処理
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//Chg 2011/10/11 Z01yoneya Start -->
//        filePath = ((NaviApplication)getApplication()).getNaviAppDataPath().getRouteDataXmlFileFullPath();
//------------------------------------------------------------------------------------------------------------------------------------
        try {
            filePath = ((NaviApplication)getApplication()).getNaviAppDataPath().getRouteDataXmlFileFullPath();
        } catch (NullPointerException e) {
            filePath = null;
        }
//Chg 2011/10/11 Z01yoneya End <--

//Chg 2011/09/30 Z01yoneya Start -->
//        readData();
//------------------------------------------------------------------------------------------------------------------------------------
        updateRouteList();
//Chg 2011/09/30 Z01yoneya End <--
        RouteBeanRead(RouteName, UTCList);
        LayoutInflater oInflater = LayoutInflater.from(this);
//Del 2011/09/17 Z01_h_yamada Start -->
//		// 画面を追加
//		setMenuTitle(R.drawable.title_save_route_list);
//Del 2011/09/17 Z01_h_yamada End <--
        LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.save_route_list, null);

        oBox = (ScrollList)oLayout.findViewById(R.id.scrollRouteList);
        oBox.setAdapter(new BaseAdapter() {

            @Override
            public int getCount() {
                if (lstRouteNodeDate.size() < 5) {
                    return 5;
                }
                return lstRouteNodeDate.size();
            }

            @Override
            public Object getItem(int arg0) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return saveRouteListGetView(position, convertView);
            }

            @Override
            public boolean isEnabled(int position) {
                return false;
            }

            @Override
            public boolean areAllItemsEnabled() {
                return false;
            }
        });
//Del 2011/10/06 Z01_h_yamada Start -->
//        oBox.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
        oBox.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
        oTool = new ScrollTool(this);
        oTool.bindView(oBox);

        setViewInOperArea(oTool);
        setViewInWorkArea(oLayout);
    }

    /**
     * ルート名称と時間を作成
     *
     * @param pDisplayNameList
     * @param pTimeList
     */
    private void RouteBeanRead(List<String> pDisplayNameList, List<String> pTimeList) {
        if (lstRouteNodeDate.size() != 0) {
            lstRouteNodeDate.clear();
        }
        for (int i = 0; i < pDisplayNameList.size(); i++) {
            RouteNodeData routeNodeData = new RouteNodeData();
            routeNodeData.setName(pDisplayNameList.get(i));
            routeNodeData.setDate(pTimeList.get(i));
            routeNodeData.setLat(" ");
            routeNodeData.setLon(" ");
            routeNodeData.setStatus(" ");
            lstRouteNodeDate.add(routeNodeData);
        }
    }

    /*
     * ルート保存ファイルを読み込んでルートリスト情報(UTCList,RouteName)を更新する
     */
    private void updateRouteList() {
        UTCList = new ArrayList<String>();
        RouteName = new ArrayList<String>();
        RouteDataFile routeDataFile = new RouteDataFile();
        List<RouteData> routeList = routeDataFile.readData(filePath);
        for (int i = 0; i < routeList.size(); i++) {
            RouteData data = routeList.get(i);
            UTCList.add(data.getRouteDate());
            RouteName.add(data.getRouteName());
        }
    }

    /**
     * 表示したデータを取得
     */
    public View saveRouteListGetView(int pos, View view) {
        LinearLayout oLayout;
        final int intPos = pos;
        if (view == null) {
            LayoutInflater oInflater = LayoutInflater.from(this);
            oLayout = (LinearLayout)oInflater.inflate(R.layout.save_route_add_list, null);
            view = oLayout;
        } else {
            oLayout = (LinearLayout)view;
        }

        LinearLayout butRouteList = (LinearLayout)oLayout.findViewById(R.id.butRouteList);
        TextView butRouteTitle = (TextView)oLayout.findViewById(R.id.butRouteList_title);
        TextView butRouteContent = (TextView)oLayout.findViewById(R.id.butRouteList_content);
        Button butDelRoute = (Button)oLayout.findViewById(R.id.butDelRoute);
        if (pos > lstRouteNodeDate.size() - 1) {
            butRouteTitle.setText("");
            butRouteContent.setText("");
            butRouteList.setEnabled(false);
            butDelRoute.setEnabled(false);
        } else {
            butRouteTitle.setText(getUTCToDate(lstRouteNodeDate.get(pos).getUTCDate()));
            butRouteContent.setText(lstRouteNodeDate.get(pos).getName());
            butRouteList.setEnabled(true);
            butDelRoute.setEnabled(true);
            butRouteList.setOnClickListener(new OnClickListener() {
                public void onClick(View oView) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 保存ルート一覧 選択禁止 Start -->
                	if(DrivingRegulation.CheckDrivingRegulation()){
                		return;
                	}
// ADD 2013.08.08 M.Honma 走行規制 保存ルート一覧 選択禁止 <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
                    // XuYang modi start #1084
                    modifyRoute(lstRouteNodeDate.get(intPos).getUTCDate(), lstRouteNodeDate.get(intPos).getName());
                    // XuYang modi end #1084
                }
            });
            butDelRoute.setOnClickListener(new OnClickListener() {
                public void onClick(View oView) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 保存ルート一覧 削除禁止 Start -->
                	if(DrivingRegulation.CheckDrivingRegulation()){
                		return;
                	}
// ADD 2013.08.08 M.Honma 走行規制 保存ルート一覧 削除禁止 <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
                    delDate = lstRouteNodeDate.get(intPos).getUTCDate();
                    delRouteName = lstRouteNodeDate.get(intPos).getName();
                    delRoute();
                }
            });
        }
        return view;
    }

    /**
     * ルート修正処理画面に遷移する処理
     *
     * @param delDate
     */
    // XuYang modi start #1084
    private void modifyRoute(String delDate, String modiRouteName) {
        // XuYang modi end #1084
        Intent intent = new Intent();
        intent.setClass(this, RouteEdit.class);
        intent.putExtra("ItemData", delDate);
        // XuYang modi start #1084
        intent.putExtra("ItemName", modiRouteName);
        // XuYang modi end #1084
        intent.putExtra(Constants.ROUTE_TYPE_KEY, Constants.ROUTE_TYPE_EDIT);
//Chg 2011/09/23 Z01yoneya Start -->
//        this.startActivityForResult(intent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
//Chg 2011/09/23 Z01yoneya End <--
    }

    /**
     * 削除確認処理
     */
    private void delRoute() {
        this.removeDialog(DIALOG_DEL_CONFIRM);
        this.showDialog(DIALOG_DEL_CONFIRM);
    }

    /**
     * ダイアログ処理
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        final CustomDialog oDialog = new CustomDialog(this);
        if (id == DIALOG_DEL_CONFIRM) {
            Resources oRes = getResources();
            oDialog.setTitle(oRes.getString(R.string.title_route_del));
            oDialog.setMessage("「" + delRouteName + oRes.getString(R.string.msg_del_route));
            oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {

                @Override
                public void onClick(View v) {
//Chg 2011/09/30 Z01yoneya Start -->
//                    deleteRoute(delDate);
//                    readData();
//------------------------------------------------------------------------------------------------------------------------------------
                    RouteDataFile routeDataFile = new RouteDataFile();
                    routeDataFile.deleteRoute(filePath, delDate);
                    updateRouteList();
//Chg 2011/09/30 Z01yoneya End <--
                    RouteBeanRead(RouteName, UTCList);
                    oBox.reset();
                    oDialog.dismiss();
                    removeDialog(DIALOG_DEL_CONFIRM);
                }

            });
            oDialog.addButton(oRes.getString(R.string.btn_cancel), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    oDialog.dismiss();
                    removeDialog(DIALOG_DEL_CONFIRM);
                }

            });
        }
        // XuYang add start 走行中の操作制限
        else {
            return super.onCreateDialog(id);
        }
        // XuYang add end 走行中の操作制限
        return oDialog;
    }

    /**
     * 戻るの処理
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.RESULTCODE_ROUTE_DETERMINATION) {
            this.setResult(Constants.RESULTCODE_ROUTE_DETERMINATION);
            this.finish();
        }
//Add 2011/09/21 Z01yoneya Start -->
        super.onActivityResult(requestCode, resultCode, data);
//Add 2011/09/21 Z01yoneya End <--
    }

    /**
     * 時間のファーマット処理
     *
     * @param date
     * @return
     */
    public String getUTCToDate(String date) {
        StringBuffer strbufText = new StringBuffer();
        strbufText.append("  ");
        strbufText.append(date.substring(2, 4));
        strbufText.append("/");
        strbufText.append(date.substring(4, 6));
        strbufText.append("/");
        strbufText.append(date.substring(6, 8));
        strbufText.append("  ");
        return strbufText.toString();
    }

    /**
     * 画面更新処理
     */
    @Override
    protected void onResume() {
        if (oBox != null) {
//Chg 2011/09/30 Z01yoneya Start -->
//            readData();
//------------------------------------------------------------------------------------------------------------------------------------
            updateRouteList();
//Chg 2011/09/30 Z01yoneya End <--
            RouteBeanRead(RouteName, UTCList);
            oBox.reset();
        }
        super.onResume();
    }
}
