/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZCT_AreaName.java
 * Description    政界の名称
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZCT_AreaName {
    private String preAreaName;
    private String curAreaName;
    /**
    * Created on 2010/08/06
    * Title:       getPreAreaName
    * Description:  前１つの政界の名称を取得する
    * @param1   無し
    * @return       String

    * @version        1.0
    */
    public String getPreAreaName() {
        return preAreaName;
    }
    /**
    * Created on 2010/08/06
    * Title:       setPreAreaName
    * Description:  前１つの政界の名称を設定する
    * @param1   String preAreaName 前１つの政界の名称
    * @return       void

    * @version        1.0
    */
    public void setPreAreaName(String preAreaName) {
        this.preAreaName = preAreaName;
    }
    /**
    * Created on 2010/08/06
    * Title:       getCurAreaName
    * Description:  当面の政界の名称を取得する
    * @param1   無し
    * @return       String

    * @version        1.0
    */
    public String getCurAreaName() {
        return curAreaName;
    }
    /**
    * Created on 2010/08/06
    * Title:       setCurAreaName
    * Description: 当面の政界の名称を設定する
    * @param1   String curAreaName 当面の政界の名称
    * @return       void

    * @version        1.0
    */
    public void setCurAreaName(String curAreaName) {
        this.curAreaName = curAreaName;
    }
}
