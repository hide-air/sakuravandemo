/**
 *  @file		DriConDetailMenuActivity
 *  @brief		ドライブコンテンツの詳細メニューを表示する
 *
 *  @attention
 *  @note		一覧へ表示や、ON/OFF設定、セレクト画面への遷移ボタンなどを管理する
 *
 *  @author		Manabu Watanabe [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2011-09-01)
 *  @version	$Revision: $ by $Author: $
 *
 */
package net.zmap.android.pnd.v2.dricon.view;


import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
//ADD 2013.08.08 M.Honma 走行規制 メッセージ表示とフラグ取得 Start -->
import net.zmap.android.pnd.v2.common.DrivingRegulation;
//ADD 2013.08.08 M.Honma 走行規制 メッセージ表示とフラグ取得 End <--
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.DC_ContentsData;
import net.zmap.android.pnd.v2.data.DC_ContentsItemData;
import net.zmap.android.pnd.v2.dricon.controller.DriverContentsCtrl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DriConDetailMenuActivity extends DriconMenuListBaseActivity implements SeekBar.OnSeekBarChangeListener
{
	private DC_ContentsData					m_oContentsData = null;		//Indexに対するデータリストからの要素
	private int								m_nDataIndex = 0;			//データリストのIndex
	private SeekBar 						m_oSeekBar;					//スライダーバー
	private boolean							m_oDlgRunningFlag = false;	//ダイアログで設定メニューを呼ぶフラグ
	private ArrayList<ViewItemData>			m_lstViewItemData = null;
	private boolean							m_bUpdate = false;			//情報更新フラグ

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.view.DriconMenuListBaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent oIntent = getIntent();
		if(oIntent != null){
			try{
				m_nDataIndex = oIntent.getIntExtra(Constants.DC_INTENT_DATA_SETTING_INDEX_EXTRA, -1);
				m_oDlgRunningFlag = oIntent.getBooleanExtra(Constants.DC_INTENT_DIALOG_TO_MENU_EXTRA, false);
			}
			catch(Exception e)
			{
				NaviLog.d(NaviLog.PRINT_LOG_TAG," Error at bundle " + e.toString());
			}
		}

		if(m_oContentsDataList.size() > m_nDataIndex){
			try {
				m_oContentsData = m_oContentsDataList.get(m_nDataIndex).clone();
			} catch (CloneNotSupportedException e) {
				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
			}
			if(m_oContentsData != null){
				//タイトル設定
				setTitleValue(m_oContentsData.m_sContentsName);
				int count = 0;
				Map<String , List<DC_ContentsItemData>> map = m_oContentsData.m_mapSectionItemList;
				if(map != null){
					for(Entry<String, List<DC_ContentsItemData>> entry :  map.entrySet()){
			            ArrayList<DC_ContentsItemData> value = (ArrayList<DC_ContentsItemData>) entry.getValue();
						if(value != null){
							count+= value.size();
						}
						count++;
			        }
				}
				initLstViewItem(count);
			}
		}
		if(m_lstViewItemData == null){
			m_nCount = 0;
		}else{
			m_nCount = m_lstViewItemData.size();
		}
		initDriConMenuLayout();


	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.common.activity.MenuBaseActivity#onResume()
	 */
	public void onResume(){
		super.onResume();
		updListView(m_nCount);
		notifyUpdataList();
	}
	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.common.activity.MenuBaseActivity#goBack()
	 */
	public void goBack(){
		updContentsData();
    	super.goBack();
	}
	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.view.DriconMenuListBaseActivity#goMap()
	 */
	public void goMap(){
		updContentsData();
    	super.goMap();
	}

	/**
	 * マスターとなるコンテンツデータを更新する
	 */
	private void updContentsData(){
		if(!m_bUpdate){
			return;
		}
		if(!m_oContentsData.m_bDisplayItem){
			m_oContentsData.m_bCheckItem = false;
		}
		m_oContentsDataList.remove(m_nDataIndex);
    	m_oContentsDataList.add(m_nDataIndex ,m_oContentsData );
    	DriverContentsCtrl.getController().setContentsDataList(m_oContentsDataList);
    	DriverContentsCtrl.getController().saveSettingFile();
		if(m_oDlgRunningFlag){
			DriverContentsCtrl.getController().notifyDialogDataChanged();
		}
	}
	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.common.activity.MenuBaseActivity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	updContentsData();
    		if(m_oDlgRunningFlag){
    			DriverContentsCtrl.getController().notifyDialogDataChanged();
    		}
        }
        return super.onKeyDown(keyCode, event);
    }

	/**
	 * 相手見情報を取得する
	 * @param postion リスト内アイテムの位置
	 * @return 取得成功時に、アイテムを返す
	 */
	private ViewItemData getItemInfo(int postion){
		int mIndex = 0;
		int mCnt = 0;
		if(postion < 0){
			return null;
		}
		ViewItemData data = new ViewItemData();
		int pos = postion;
		Map<String , List<DC_ContentsItemData>> map = m_oContentsData.m_mapSectionItemList;
			if(map != null){
				for(Entry<String, List<DC_ContentsItemData>> entry :  map.entrySet()){
		        	if( pos == mIndex){
		        		data.itemData = new DC_ContentsItemData();
		        		data.itemData.m_sItemText = entry.getKey();
		        		data.itemData.m_nItemKind = ViewItemData.ITEM_TYPE_TITLE;
		        		return data;
		        	}else{
		        		ArrayList<DC_ContentsItemData> value = (ArrayList<DC_ContentsItemData>) entry.getValue();
						if(value != null){
							mIndex++;
							int tmpPos = pos - mIndex;
							mIndex += value.size();
							if(value.size() > tmpPos){
								data.itemData = value.get(tmpPos);
								data.sectionItemListIndex = mCnt;
								data.optionListIndex = tmpPos;
								return data;
							}
						}
		        	}
		        	mCnt++;
		        }
			}

		return null;
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.view.DriconMenuListBaseActivity#getViewList(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getViewList(int position, View oView, ViewGroup parent) {
		if(oView == null)
		{
			LayoutInflater oInflater = LayoutInflater.from(DriConDetailMenuActivity.this);
			m_oLayout = (LinearLayout)oInflater.inflate(R.layout.dricon_menu_detail_item, null);
			oView = m_oLayout;
		} else {
			m_oLayout = (LinearLayout)oView;
		}

		final int wPos = position;
		TextView oTxtView = (TextView)m_oLayout.findViewById(R.id.dc_menu_detail_item_text);
		Button oSwitchOnBtn = (Button)m_oLayout.findViewById(R.id.dc_menu_detail_item_switchON);
		Button oSwitchOffBtn = (Button)m_oLayout.findViewById(R.id.dc_menu_detail_item_switchOFF);
		Button oComboGroupBtn = (Button)m_oLayout.findViewById(R.id.dc_menu_detail_item_comboGroup);
		m_oSeekBar = (SeekBar)m_oLayout.findViewById(R.id.dc_menu_detail_item_seek);
	    m_oSeekBar.setOnSeekBarChangeListener(this);
	    if(m_oContentsData != null){
			final ViewItemData data = m_lstViewItemData.get(wPos);
			if(data != null){
				oSwitchOnBtn.setOnClickListener(new Button.OnClickListener() {
					@Override
					public void onClick(View v){
						m_bUpdate =true;
						if(wPos == 0){
							if(m_oContentsData.m_iStateFlag == DC_ContentsData.DISPLAY_STATE_LAYER){
								if(!m_oContentsData.m_bLayering){
									m_oContentsData.m_bLayering = true;
								}
							}
							m_oContentsData.m_bDisplayItem = true;
						}
						if(wPos == 1){
							m_oContentsData.m_bDisplayItem = true;
						}
						if(wPos > 1){
							DC_ContentsItemData itemData = data.itemData;
							if(itemData != null){
								itemData.m_sItemValueList.clear();
								if(itemData.m_OptionList.get(0) != null){
									itemData.m_sItemValueList.add(itemData.m_OptionList.get(0).m_sOptionValue);
								}
							}
						}

						notifyUpdataList();
					}
				});

				oSwitchOffBtn.setOnClickListener(new Button.OnClickListener() {
					@Override
					public void onClick(View v){
						m_bUpdate =true;
						if(wPos == 0){
							if(m_oContentsData.m_iStateFlag == 2){
								if(m_oContentsData.m_bLayering){
									m_oContentsData.m_bLayering = false;
									m_oContentsData.m_bDisplayItem = true;
								}
							}else{
								m_oContentsData.m_bDisplayItem = false;
							}
						}else if(wPos == 1){
							m_oContentsData.m_bDisplayItem = false;
						}else{
							DC_ContentsItemData itemData = data.itemData;
							if(itemData != null){
								itemData.m_sItemValueList.clear();
								itemData.m_sItemValueList.add("0");
							}
							setItemData(data.sectionItemListIndex, data.optionListIndex , itemData);
						}
						notifyUpdataList();
					}
				});


				if(data.itemData != null){
					if(data.itemData.m_nItemKind == ViewItemData.ITEM_TYPE_TITLE){
						SpannableString spannableString=new SpannableString(data.itemData.m_sItemText);
						spannableString.setSpan(
						new StyleSpan(android.graphics.Typeface.BOLD)
						          , 0
						          , spannableString.length()
						          , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						oTxtView.setText(spannableString);
//Chg 2011/10/31 Z01_h_yamada Start -->
//						oTxtView.setTextSize(getResources().getDimension(R.dimen.Common_Large_textSize));
//--------------------------------------------
						oTxtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.Common_Menu_Medium_textSize));
//Chg 2011/10/31 Z01_h_yamada End <--
//						oTxtView.setTextSize(getResources().getDimension(R.dimen.Common_Large_textSize));
					}else{
						oTxtView.setText(data.itemData.m_sItemText);
//Chg 2011/10/31 Z01_h_yamada Start -->
//						oTxtView.setTextSize(getResources().getDimension(R.dimen.Common_Medium_textSize));
//--------------------------------------------
						oTxtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.Common_Menu_Small_textSize));
//Chg 2011/10/31 Z01_h_yamada End <--
					}

					DC_ContentsItemData oContentsItemData;
					switch(data.itemData.m_nItemKind)
					{
					case ViewItemData.ITEM_TYPE_SWITCH:
						oSwitchOnBtn.setVisibility(Button.VISIBLE);
						oSwitchOffBtn.setVisibility(Button.VISIBLE);
						oComboGroupBtn.setVisibility(Button.INVISIBLE);
						m_oSeekBar.setVisibility(SeekBar.INVISIBLE);

						if(wPos == 0){
							if(m_oContentsData.m_iStateFlag == 2){
								if(m_oContentsData.m_bLayering){
									selectSwBtn(oSwitchOnBtn, oSwitchOffBtn, ON);
								}else{
									selectSwBtn(oSwitchOnBtn, oSwitchOffBtn, OFF);
								}
							}else{
								if(m_oContentsData.m_bDisplayItem){
									selectSwBtn(oSwitchOnBtn, oSwitchOffBtn, ON);
								}else{
									selectSwBtn(oSwitchOnBtn, oSwitchOffBtn, OFF);
								}
							}
						}else if(wPos == 1){
							if(m_oContentsData.m_iStateFlag == 2){
								if(m_oContentsData.m_bLayering){
									oTxtView.setText("");
									oSwitchOnBtn.setVisibility(Button.INVISIBLE);
									oSwitchOffBtn.setVisibility(Button.INVISIBLE);
								}else{
									if(m_oContentsData.m_bDisplayItem){
										selectSwBtn(oSwitchOnBtn, oSwitchOffBtn, ON);
									}else{
										selectSwBtn(oSwitchOnBtn, oSwitchOffBtn, OFF);
									}
								}
							}else{
								oTxtView.setText("");
								oSwitchOnBtn.setVisibility(Button.INVISIBLE);
								oSwitchOffBtn.setVisibility(Button.INVISIBLE);
							}
						}else{
							DC_ContentsItemData itemData = data.itemData;
							if(itemData != null){
								if(itemData.m_sItemValueList != null){
									if(!itemData.m_sItemValueList.contains("0")){
										selectSwBtn(oSwitchOnBtn, oSwitchOffBtn, ON);
									}else{
										selectSwBtn(oSwitchOnBtn, oSwitchOffBtn, OFF);
									}
								}else {
									if(itemData.m_sItemDefaultValue != null){
										if(!itemData.m_sItemDefaultValue.equals("0")){
											selectSwBtn(oSwitchOnBtn, oSwitchOffBtn, ON);
										}else{
											selectSwBtn(oSwitchOnBtn, oSwitchOffBtn, OFF);
										}
									}
								}
							}
						}
						break;
					case ViewItemData.ITEM_TYPE_SLIDER:
						m_oSeekBar.setVisibility(SeekBar.VISIBLE);
						break;
					case ViewItemData.ITEM_TYPE_SELECT:
					case ViewItemData.ITEM_TYPE_MULTI_SELECT:
						oSwitchOnBtn.setVisibility(Button.INVISIBLE);
						oSwitchOffBtn.setVisibility(Button.INVISIBLE);
						oComboGroupBtn.setVisibility(Button.VISIBLE);
						m_oSeekBar.setVisibility(SeekBar.INVISIBLE);
						oComboGroupBtn.setText("");
						oContentsItemData = data.itemData;
						if(oContentsItemData != null){
							if(oContentsItemData.m_nItemKind == ViewItemData.ITEM_TYPE_MULTI_SELECT){
								if(oContentsItemData.m_sItemValueList == null){
									oComboGroupBtn.setText(getResources().getString(R.string.dc_search_info_multi_nothing));
								}else{
									if(oContentsItemData.m_OptionList != null){
										String strBtnText = null;
										if(oContentsItemData.m_bAllSelect){
											oComboGroupBtn.setText(getResources().getString(R.string.dc_search_info_multi_all));
										}else{
											for(int j = 0; j < oContentsItemData.m_OptionList.size() ; j++ ){
												if(oContentsItemData.m_sItemValueList.contains(oContentsItemData.m_OptionList.get(j).m_sOptionValue)){
													if(strBtnText == null){
														strBtnText = new String();
														strBtnText = oContentsItemData.m_OptionList.get(j).m_sOptionText;
													}else{
														strBtnText = strBtnText + getResources().getString(R.string.dc_search_info_multi_other);
														break;
													}
												}
											}

											if(strBtnText != null){
												oComboGroupBtn.setText(strBtnText);
											}else{
												oComboGroupBtn.setText(getResources().getString(R.string.dc_search_info_multi_nothing));
											}
										}
									}else{
										oComboGroupBtn.setText(getResources().getString(R.string.dc_search_info_multi_select));
									}
								}
							}else{
								if(oContentsItemData.m_OptionList!= null){
									if(oContentsItemData.m_sItemValueList != null){
										for(int j = 0; j < oContentsItemData.m_OptionList.size() ; j++ ){
											if(oContentsItemData.m_sItemValueList.contains(oContentsItemData.m_OptionList.get(j).m_sOptionValue)){
												oComboGroupBtn.setText(oContentsItemData.m_OptionList.get(j).m_sOptionText);
												break;
											}
										}
									}
								}else{
									oComboGroupBtn.setText(getResources().getString(R.string.dc_search_info_multi_select));
								}
							}
						}
						oComboGroupBtn.setOnClickListener(new Button.OnClickListener() {
							@Override
							public void onClick(View v){
								showGroupView(data.sectionItemListIndex ,  data.optionListIndex);
							}
						});
						break;
					case ViewItemData.ITEM_TYPE_DATE:
					case ViewItemData.ITEM_TYPE_DATETIME:
					case ViewItemData.ITEM_TYPE_TIME:
						oSwitchOnBtn.setVisibility(Button.INVISIBLE);
						oSwitchOffBtn.setVisibility(Button.INVISIBLE);
						oComboGroupBtn.setVisibility(Button.INVISIBLE);
						m_oSeekBar.setVisibility(SeekBar.INVISIBLE);
						break;

					default:
						oSwitchOnBtn.setVisibility(Button.INVISIBLE);
						oSwitchOffBtn.setVisibility(Button.INVISIBLE);
						m_oSeekBar.setVisibility(SeekBar.INVISIBLE);
						oComboGroupBtn.setVisibility(Button.INVISIBLE);
						break;
					}
				}
			}
	    }
		return oView;
	}

	/**
	 * 指定したアイテムの値を取得する
	 * @param sectionItemListIndex  表示する情報のインデックス
	 * @param optionListIndex 選択されているオプションのインデックス
	 * @return 取得アイテムの値。なければnull。
	 */
	public DC_ContentsItemData getContentsItemDataValue(int sectionItemListIndex, int optionListIndex){
		if(sectionItemListIndex >= 0 && optionListIndex >= 0){
			Map<String , List<DC_ContentsItemData>> map = m_oContentsData.m_mapSectionItemList;
			if(map != null){
				Set<String> keySet = map.keySet();
		        String keyStr = (String)keySet.toArray()[sectionItemListIndex];
		        if(keyStr != null){
		        	ArrayList<DC_ContentsItemData> value = (ArrayList<DC_ContentsItemData>) map.get(keyStr);
		            if(value != null){
							return value.get(optionListIndex);
		        	}
		        }
			}
		}
		return null;
	}

	/**
	 * アイテムを設定する
	 * @param sectionItemListIndex  表示する情報のインデックス
	 * @param optionListIndex 選択されているオプションのインデックス
	 * @param itemData 設定するアイテム
	 */
	public void setItemData(int sectionItemListIndex, int optionListIndex , DC_ContentsItemData itemData)
	{
		if(sectionItemListIndex >= 0 && optionListIndex >= 0){
			String strKey = getContentsItemDataKey(sectionItemListIndex , optionListIndex);
			if(strKey != null){
				if(m_oContentsData.m_mapSectionItemList.get(strKey) != null){
					m_oContentsData.m_mapSectionItemList.get(strKey).get(optionListIndex).m_sItemValueList = itemData.m_sItemValueList;
				}
			}
		}
	}

	/**
	 * 指定されたコンテンツのキーを取得する
	 * @param sectionItemListIndex  表示する情報のインデックス
	 * @param optionListIndex 選択されているオプションのインデックス
	 * @return 取得したキー
	 */
	public String getContentsItemDataKey(int sectionItemListIndex, int optionListIndex){
		if(sectionItemListIndex >= 0 && optionListIndex >= 0){
			Map<String , List<DC_ContentsItemData>> map = m_oContentsData.m_mapSectionItemList;
			if(map != null){
				Set<String> keySet = map.keySet();
		        String keyStr = (String)keySet.toArray()[sectionItemListIndex];
		        return keyStr;
			}
		}
		return null;
	}

	/**
	 * グループ情報(select、multiselect)を表示する
	 * @param sectionItemListIndex  表示する情報のインデックス
	 * @param optionListIndex 選択されているオプションのインデックス
	 */
	public void showGroupView(int sectionItemListIndex, int optionListIndex){
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ詳細 グループ情報禁止 Start -->
		if(DrivingRegulation.CheckDrivingRegulation()){
			return;
		}
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ詳細 グループ情報禁止 End <--
		DC_ContentsItemData value = getContentsItemDataValue(sectionItemListIndex, optionListIndex);
		String keyStr = getContentsItemDataKey(sectionItemListIndex, optionListIndex);
		if(keyStr != null && value != null){
			Intent intent = new Intent();

			intent.putExtra(Constants.DC_INTENT_GROUP_PARENT_ITEM_EXTRA, m_oContentsData);
			intent.putExtra(Constants.DC_INTENT_GROUP_CONTENT_EXTRA, value);
			intent.putExtra(Constants.DC_INTENT_GROUP_PARENT_EXTRA, 0);
			intent.putExtra(Constants.DC_INTENT_GROUP_PARENT_INDEX_EXTRA, m_nDataIndex);
			intent.putExtra(Constants.DC_INTENT_GROUP_SECTION_KEY_EXTRA, keyStr);
			intent.putExtra(Constants.DC_INTENT_GROUP_SECTION_ITEM_EXTRA, optionListIndex);
			intent.setClass(this, DriConMenuGroupBaseActivity.class);
			NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_DRICON_SETTING_GROUP);
		}
	}

	/* (非 Javadoc)
	 * @see android.widget.SeekBar.OnSeekBarChangeListener#onProgressChanged(android.widget.SeekBar, int, boolean)
	 */
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
	}

	/* (非 Javadoc)
	 * @see android.widget.SeekBar.OnSeekBarChangeListener#onStartTrackingTouch(android.widget.SeekBar)
	 */
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	/* (非 Javadoc)
	 * @see android.widget.SeekBar.OnSeekBarChangeListener#onStopTrackingTouch(android.widget.SeekBar)
	 */
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.common.activity.MenuBaseActivity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

/*		if(requestCode == AppInfo.ID_ACTIVITY_DRICON_SETTING_GROUP){
			if(resultCode == Constants.DC_INTENT_GROUP_SET_DATA){
				DC_ContentsItemData itemData =  (DC_ContentsItemData) data.getSerializableExtra(Constants.DC_INTENT_GROUP_CONTENT_SET_DATA_EXTRA);
				String keyStr	= data.getStringExtra(Constants.DC_INTENT_GROUP_SECTION_KEY_EXTRA);
				int nIndex 		= data.getIntExtra(Constants.DC_INTENT_GROUP_SECTION_ITEM_EXTRA, -1);
				if(itemData == null){
					return;
				}
				Map<String , List<DC_ContentsItemData>> map = m_oContentsData.m_mapSectionItemList;
				if(map != null){
		            ArrayList<DC_ContentsItemData> value = (ArrayList<DC_ContentsItemData>) map.get(keyStr);
		            if(value != null){
		            	value.set(nIndex, itemData);
		            	m_oContentsData.m_mapSectionItemList.put(keyStr, value);
		            	m_oContentsDataList.remove(m_nDataIndex);
		            	m_oContentsDataList.add(m_nDataIndex ,m_oContentsData );
		            	updListView(m_nCount);
		            	notifyUpdataList();
		            }
				}
			}
		}
*/	}

	/**
	 * 表示アイテムを更新する
	 * @param nCount アイテム数
	 */
	private void initLstViewItem(int nCount) {
		if (m_oContentsData == null) {
			return;
		}
		if(m_lstViewItemData == null){
			m_lstViewItemData = new ArrayList<ViewItemData>();
		}else{
			m_lstViewItemData.clear();
		}
		if(m_oContentsData.m_iStateFlag == 2){
			ViewItemData data = new ViewItemData();
			data.itemData.m_sItemText = "常に地図上に表示";
			data.itemData.m_nItemKind = ViewItemData.ITEM_TYPE_SWITCH;
			ViewItemData data1 = new ViewItemData();
			data1.itemData.m_sItemText = "一覧に表示";
			data1.itemData.m_nItemKind = ViewItemData.ITEM_TYPE_SWITCH;
			m_lstViewItemData.add(data);
			m_lstViewItemData.add(data1);
		}else{
			ViewItemData data = new ViewItemData();
			data.itemData.m_sItemText = "一覧に表示";
			data.itemData.m_nItemKind = ViewItemData.ITEM_TYPE_SWITCH;
			m_lstViewItemData.add(data);
		}
		{
			ViewItemData data = new ViewItemData();
			data.itemData.m_nItemKind = ViewItemData.ITEM_TYPE_NOTHING;
			m_lstViewItemData.add(data);
		}

		for(int pos = 0; pos < nCount; pos++){
			ViewItemData data = getItemInfo(pos);
			m_lstViewItemData.add(data);
		}
	}

	/**
	 * 表示しているリスト情報が更新された場合に、リストを更新する
	 * @param nCount
	 *
	 */
	private void updListView(int nCount){
		try {
			m_oContentsData = m_oContentsDataList.get(m_nDataIndex).clone();
		} catch (CloneNotSupportedException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
		}
		initLstViewItem(nCount);
	}

	/**
	 * @author watanabe
	 * 表示データのタイプや表示内容を保持するためのクラス
	 *
	 */
	private static class ViewItemData{
		public final static int ITEM_TYPE_UNKNOWN 		= 0;
		public final static int ITEM_TYPE_SWITCH 		= 1;
		public final static int ITEM_TYPE_SELECT 		= 2;
		public final static int ITEM_TYPE_MULTI_SELECT 	= 3;
		public final static int ITEM_TYPE_SLIDER 		= 4;
		public final static int ITEM_TYPE_DATETIME 		= 5;
		public final static int ITEM_TYPE_DATE 			= 6;
		public final static int ITEM_TYPE_TIME 			= 7;
		public final static int ITEM_TYPE_TITLE 		= 8;
		public final static int ITEM_TYPE_NOTHING 		= 9;
		public DC_ContentsItemData 	itemData = new DC_ContentsItemData();
		public int					sectionItemListIndex 	= -1;
		public int					optionListIndex 		= -1;
	}

}
