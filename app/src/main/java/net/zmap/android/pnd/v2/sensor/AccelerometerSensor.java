package net.zmap.android.pnd.v2.sensor;

import android.hardware.Sensor;

public class AccelerometerSensor {
	private final static int NOISE_NUM_SAMPLES = 5;			///< ノイズ平滑化サンプル数
//Del 2011/04/04 Z01yoneya Start-->未使用のため
//	private final static int OFFSET_SAMPLING_INTERVAL = 3;	///< ゼロ補正オフセットサンプリング秒 (sec)
//	private final static int UPDATE_OFFSETS_INTERVAL = 1;	///< ゼロ補正オフセット更新間隔 (sec)
	//Del 2011/04/04 Z01yoneya End <--
	private final float NS2S = 1.0f / 1000000000.0f;

	public Sensor sensor;


	//加速度センサーのイベント生値は下記のとおりに格納される
	//[0] : 重力方向が正
	//[1] : 画面右方向が正
	//[2] : 画面奥方向が正
	//※XYZはまぎらわしいので変数に使わない
	public final static char ACC_DIR_0_DOWN =0;	//下		//Z
	public final static char ACC_DIR_1_RIGHT=1;	//右		//X
	public final static char ACC_DIR_2_AHEAD=2;	//前方		//Y

	public float rvalues[]		= new float[3];	///< 生の値 (m/s2)
	public float nrvalues[]		= new float[3];	///< ノイズ除去 (m/s2)
	public float acceleration[] = new float[3];	///< 回転前 (m/s2)
	public float accelerationT[]= new float[3];	///< 回転後 (m/s2)
	public float offsets[] 		= new float[3];	///< ゼロ補正オフセット
	public float dt;							///< サンプリング間隔

	// V2.5
	public float[]	delay_out = new float[3];  		///< ACC ノイズカット変数
	public float[]	Acc_lpf = new float[3];			///< LPF X,Y,Z
	public float[]	Acc_lpfadd = new float[3];		///< LPF加算値 X,Y,Z
	public float[]	Acc_Integ1 = new float[3];		/// IIR LPF 中間ﾊﾞｯﾌｧ１
	public float[]	Acc_Integ2 = new float[3];		/// IIR LPF 中間ﾊﾞｯﾌｧ2
	public long		Acctimestamp_Start = 0;			///< Acc Start タイムスタンプ
	public long		Acctimestamp_End = 0;			///< Acc End タイムスタンプ
	public long		Acc_1SectimeCnt = 0;			///< Acc 1秒経過カウンタ

	public long acc_t[]		= new long[201];		///< 送る値
	public float acc_d[][]	= new float[3][201];	///< 送る値

	protected float _timestamp = 0;

	protected CircularBuffer _noiseBuffer[] = new CircularBuffer[3];
//Del 2011/09/07 Z01yoneya Start -->加速度センサー値はゼロ補正しない
//	protected CircularBuffer _zeroBuffer[] = new CircularBuffer[3];
//Del 2011/09/07 Z01yoneya End <--

    protected Matrix3D _matrix = new Matrix3D();

	public AccelerometerSensor(Sensor accelerometer) {
		sensor = accelerometer;

		// ノイズ平滑化用
		_noiseBuffer[ACC_DIR_0_DOWN ] = new CircularBuffer();
		_noiseBuffer[ACC_DIR_1_RIGHT] = new CircularBuffer();
		_noiseBuffer[ACC_DIR_2_AHEAD] = new CircularBuffer();

//Del 2011/09/07 Z01yoneya Start -->加速度センサー値はゼロ補正しない
//		// ゼロ補正用
//		_zeroBuffer[ACC_DIR_0_DOWN ] = new CircularBuffer();
//		_zeroBuffer[ACC_DIR_1_RIGHT] = new CircularBuffer();
//		_zeroBuffer[ACC_DIR_2_AHEAD] = new CircularBuffer();
//Del 2011/09/07 Z01yoneya End <--

		_matrix.invert(0.0f, 0.0f, 0.0f);
	}

//Add 2011/09/07 Z01yoneya Start -->
	/*
	 * バッファデータクリア
	 */
	public void clearBuffer(){
	    _timestamp = 0;
	    if(_noiseBuffer[ACC_DIR_0_DOWN  ] != null){ _noiseBuffer[ACC_DIR_0_DOWN  ].clear(); }
        if(_noiseBuffer[ACC_DIR_1_RIGHT ] != null){ _noiseBuffer[ACC_DIR_1_RIGHT ].clear(); }
        if(_noiseBuffer[ACC_DIR_2_AHEAD ] != null){ _noiseBuffer[ACC_DIR_2_AHEAD ].clear(); }

//Del 2011/09/07 Z01yoneya Start -->加速度センサー値はゼロ補正しない
//       if(_zeroBuffer[ACC_DIR_0_DOWN  ] != null){ _zeroBuffer[ACC_DIR_0_DOWN  ].clear(); }
//       if(_zeroBuffer[ACC_DIR_1_RIGHT ] != null){ _zeroBuffer[ACC_DIR_1_RIGHT ].clear(); }
//       if(_zeroBuffer[ACC_DIR_2_AHEAD ] != null){ _zeroBuffer[ACC_DIR_2_AHEAD ].clear(); }
//Del 2011/09/07 Z01yoneya End <--
	}
//Add 2011/09/07 Z01yoneya End <--

	protected static long _prevTime = 0;

	public void zerofy() {
//Del 2011/03/18 Z01yoneya Start-->加速度センサー値はゼロ補正しない
//		long curTime = System.currentTimeMillis();
//		if (_prevTime == 0 || curTime - _prevTime > UPDATE_OFFSETS_INTERVAL * 1000) {
//			offsets[0] = _zeroBuffer[0].averageInSeconds(OFFSET_SAMPLING_INTERVAL);
//			offsets[1] = _zeroBuffer[1].averageInSeconds(OFFSET_SAMPLING_INTERVAL);
//			offsets[2] = _zeroBuffer[2].averageInSeconds(OFFSET_SAMPLING_INTERVAL);
//			Log.i("", String.format("zerofy offset( %f, %f, %f)", offsets[0], offsets[1], offsets[2]));
//			_prevTime = curTime;
//		}
//Del 2011/03/18 Z01yoneya End <--
	}

	public void setMatrix(Matrix3D matrix) {
		_matrix = matrix;
	}

	public void onSensorChanged(RawSensorEvent event) {
		long now = System.currentTimeMillis();

		// 生の値を保存
		rvalues[ACC_DIR_0_DOWN ] = event.values[ACC_DIR_0_DOWN ];
		rvalues[ACC_DIR_1_RIGHT] = event.values[ACC_DIR_1_RIGHT];
		rvalues[ACC_DIR_2_AHEAD] = event.values[ACC_DIR_2_AHEAD];

		// ノイズ除去
		_noiseBuffer[ACC_DIR_0_DOWN ].put(now, rvalues[ACC_DIR_0_DOWN ]);
		_noiseBuffer[ACC_DIR_1_RIGHT].put(now, rvalues[ACC_DIR_1_RIGHT]);
		_noiseBuffer[ACC_DIR_2_AHEAD].put(now, rvalues[ACC_DIR_2_AHEAD]);
		nrvalues[ACC_DIR_0_DOWN ] = _noiseBuffer[ACC_DIR_0_DOWN ].average(NOISE_NUM_SAMPLES);
		nrvalues[ACC_DIR_1_RIGHT] = _noiseBuffer[ACC_DIR_1_RIGHT].average(NOISE_NUM_SAMPLES);
		nrvalues[ACC_DIR_2_AHEAD] = _noiseBuffer[ACC_DIR_2_AHEAD].average(NOISE_NUM_SAMPLES);

		acceleration[ACC_DIR_0_DOWN ] = nrvalues[ACC_DIR_0_DOWN ];
		acceleration[ACC_DIR_1_RIGHT] = nrvalues[ACC_DIR_1_RIGHT];
		acceleration[ACC_DIR_2_AHEAD] = nrvalues[ACC_DIR_2_AHEAD];

//Chg 2011/03/18 Z01yoneya Start-->
//		// 回転
//		float[] tvalues = _matrix.convert(acceleration[0], acceleration[1], acceleration[2]);
//
//		// ゼロ補正用
//		_zeroBuffer[0].put(now, tvalues[0]);
//		_zeroBuffer[1].put(now, tvalues[1]);
//		_zeroBuffer[2].put(now, tvalues[2]);
//
//		accelerationT[0] = tvalues[0] - offsets[0] + SensorManager.GRAVITY_EARTH;
//		accelerationT[1] = tvalues[1] - offsets[1];
//		accelerationT[2] = tvalues[2] - offsets[2];
//------------------------------------------------------------------
		//加速度センサー値はゼロ補正しない

		// 回転
		float[] tvalues = _matrix.convert(acceleration[ACC_DIR_0_DOWN], acceleration[ACC_DIR_1_RIGHT], acceleration[ACC_DIR_2_AHEAD]);

		//回転後の値は、ゼロ補正しない
		accelerationT[ACC_DIR_0_DOWN ] = tvalues[ACC_DIR_0_DOWN ];
		accelerationT[ACC_DIR_1_RIGHT] = tvalues[ACC_DIR_1_RIGHT];
		accelerationT[ACC_DIR_2_AHEAD] = tvalues[ACC_DIR_2_AHEAD];
//Chg 2011/03/18 Z01yoneya End <--

		if (_timestamp != 0) {
			dt = (event.timestamp - _timestamp) * NS2S;
		}
		_timestamp = event.timestamp;
	}
}