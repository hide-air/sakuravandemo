package net.zmap.android.pnd.v2.vics;

import android.content.Intent;

/** 自動実行サービス用 Listener
 *
 */
public interface VicsListener
{
	/**
	 * データを成功に返した後、当該関数を呼出す。
	 *
	 * @param oIntent Intentデータオブジェクト
	 */
	public void onFinish(Intent oIntent);

	/**
	 * 途中でキャンセルした時、当該関数を呼出す。
	 *
	 * @param oIntent Intentデータオブジェクト
	 */
	public void onCancel(Intent oIntent);

	/**
	 * エラーが発生した時、当該関数を呼出す。
	 *
	 * @param oIntent Intentデータオブジェクト
	 */
	public void onError(Intent oIntent);

	/**
	 * 要求がタイムアウトした時、当該関数を呼出す。
	 *
	 * @param oIntent Intentデータオブジェクト
	 */
	public void onTimeOut(Intent oIntent);
}
