package net.zmap.android.pnd.v2.gps;

import net.zmap.android.pnd.v2.common.VPSettingManager;
import net.zmap.android.pnd.v2.maps.MapView;
import net.zmap.android.pnd.v2.common.view.NaviStateIcon;
// V2.5 GPS停止時の状態取得
import android.location.Location;
import android.location.LocationManager;


/*
 * GPS捕捉状態表示制御クラス
 */

public class LocationStatusController implements Runnable {
    /*
     * 自身のインスタンス
     */
    private static LocationStatusController singletonInstance = null;

    /*
     * GPS信頼度の判定モード
     */
    private static int   gpsReliabilityjudgeMode       = VPSettingManager.GPS_RELIABILITY_JUDGE_GPS_ACCURACY;

    /*
     * Accuracy(誤差円半径?メートル)しきい値
     *
     */
    private static float gpsJudgeAccuracyValue      = 10.0f;

    /*
     * 測位使用衛星のSNR平均値のしきい値
     *
     */
    private static float gpsJudgeSnrValue   = 25.0f;

    /*
     * 現在の衛星状態のSNR平均値
     */
    private static float currSnrAverage = Float.MAX_VALUE;

    /*
     * 現在の測位に活用する衛星数
     */
    private static int currUsedInFixGpsCount = 0;

    /*
     * 以前のGPS信頼度
     */
    private static int   prevGpsState = MapView.GPS_NO;

    /*
     * OnLocationChanged()(GPS座標イベント)が途絶えたと判断する時間
     */
    private static final long TIME_GPS_FIX_FAILED_MSEC = 1500;  //ミリ秒
    /*
     * 前回のOnLocationChanged()(GPS座標イベント)受信時刻
     */
    private static long timeReceivedOnLocationChanged = System.currentTimeMillis() - TIME_GPS_FIX_FAILED_MSEC;

    /*
     * OnLocationChanged()(衛星状態イベント)が途絶えたと判断する時間
     */
    private static final long TIME_GPS_STATUS_FAILED_MSEC = 3500;  //ミリ秒
    /*
     * 前回のOnUpdateGpsStatus()(衛星状態イベント)受信時刻
     */
    private static long timeReceivedOnUpdateGpsStatus = System.currentTimeMillis() - TIME_GPS_STATUS_FAILED_MSEC;

    /* V2.5 :: GPS停止時の状態取得 */
    private static MyLocationListener locationListener = new MyLocationListener();

    /*
     * GPS座標イベントが途絶えたと判断するスレッド
     */
    private static Thread statusCheckThread = null;

    /*
     * 初期化
     */
    public static LocationStatusController initialize()
    {
        if(singletonInstance != null){
            return singletonInstance;
        }
        singletonInstance = new LocationStatusController();
        statusCheckThread = new Thread(singletonInstance);
        statusCheckThread.start();
        return singletonInstance;
    }

    private static LocationStatusListner mListner = new LocationStatusListner(){
        @Override
        /*
         * GPS捕捉状態の表示更新
         *  @param usedInFixGpsCount
         *            測位使用GPSの数
         *  @param snrAverage
         *            測位使用GPSのSN比率の平均値
         */
        public void OnUpdateGpsStatus(int usedInFixGpsCount, float snrAverage)
        {
            //NaviLog.d("GPS", "LocationStatusController::OnUpdateGpsStatus() mode=" + gpsAccuracyjudgeMode + " count=" + usedInFixGpsCount + " snrAverage="+snrAverage);
            timeReceivedOnUpdateGpsStatus = System.currentTimeMillis();

            currUsedInFixGpsCount = usedInFixGpsCount;
            currSnrAverage = snrAverage;

            if(gpsReliabilityjudgeMode == VPSettingManager.GPS_RELIABILITY_JUDGE_GPS_COUNT){
                updateIconByGpsFixCount(usedInFixGpsCount, snrAverage);
            }
        }

        @Override
        public void OnGetLocationGpsAccuracy(boolean bValidAccuracy, float accuracy)
        {
            timeReceivedOnLocationChanged = System.currentTimeMillis();

            //NaviLog.d("GPS", "LocationStatusController::OnGetLocationGpsAccuracy() valid=" + bValidAccuracy +  " accuracy="+accuracy);

            if(gpsReliabilityjudgeMode == VPSettingManager.GPS_RELIABILITY_JUDGE_GPS_ACCURACY){
                updateIconByGpsAccuracy(bValidAccuracy, accuracy);
            }else if(gpsReliabilityjudgeMode == VPSettingManager.GPS_RELIABILITY_JUDGE_GPS_SNR){
                updateIconBySnr(currUsedInFixGpsCount, currSnrAverage );
            }
        }

    };

    /*
     * リスナーの参照を返す
     */
    public LocationStatusListner GetListner()
    {
        return mListner;
    }

    /*
     * GPS測位信頼度の判定方法を設定する
     */
    public void setGpsReliabilityJudgeMode(int mode)
    {
        //NaviLog.d("GPS", "LocationStatusController::setGpsReliabilityJudgeMode() mode=" + mode);
        gpsReliabilityjudgeMode       = mode;
    }
    /*
     * GPS測位信頼度をAccuracyで判定する時のしきい値を設定する
     */
    public void setGpsReliabilityJudgeAccuracyValue(float accuracyVal)
    {
        //NaviLog.d("GPS", "LocationStatusController::setGpsReliabilityJudgeAccuracyValue() accuracyVal=" + accuracyVal);
        gpsJudgeAccuracyValue      = accuracyVal;
    }
    /*
     * GPS測位信頼度をSNRで判定する時のしきい値を設定する
     */
    public void setGpsReliabilityJudgeSnrValue(float snrValue)
    {
        //NaviLog.d("GPS", "LocationStatusController::setGpsReliabilityJudgeSnrValue() snrValue=" + snrValue);
        gpsJudgeSnrValue   = snrValue;
    }

    /*
     * 衛星状態イベントを継続して受信中である
     */
    public static boolean IsGpsStatusReceive()
    {
        return prevGpsState != MapView.GPS_OFF;
    }

    /*
     * 座標イベントを継続して受信中である
     */
    public static boolean IsGpsLocationReceive()
    {
        return prevGpsState != MapView.GPS_OFF &&
                prevGpsState != MapView.GPS_NO;
    }

    /*
     * GPS状態を画面に反映する
     */
    private static synchronized void updateGPSIcon( int gpsState )
    {
        if(MapView.getInstance() == null) {
            return;
        }

        if(prevGpsState == gpsState){
            return;
        }

        MapView.getInstance().updateGPSIcon(gpsState);

        prevGpsState = gpsState;
    }

    /*
     * AccuracyモードのGPS状態判断
     */
    private static void updateIconByGpsAccuracy(boolean bValidAccuracy, float accuracy)
    {
        //Accuracyが有効な時だけ処理する
        if(bValidAccuracy == true){
            int gpsState = 0;
            if(accuracy > gpsJudgeAccuracyValue ){
                gpsState = MapView.GPS_BAD;
            }else{
                gpsState = MapView.GPS_NORMAL;
            }
            //NaviLog.d("GPS", "LocationStatusController::OnGetLocationGpsAccuracy() GPS_ACCURACY_JUDGE_GPS_ACCURACY valid=" + bValidAccuracy +  " accuracy="+accuracy);

            updateGPSIcon(gpsState);
        }else{
            //Accuracyが無効ならSNRで判定
            updateIconBySnr(currUsedInFixGpsCount, currSnrAverage );
        }
    }

    /*
     * SNRモードのGPS状態判断
     */
    private static void updateIconBySnr(int usedInFixGpsCount, float snrAverage)
    {
        int gpsState = 0;

        //SNRで判定する
        if(snrAverage < gpsJudgeSnrValue){
            gpsState = MapView.GPS_BAD;
        }else{
            gpsState = MapView.GPS_NORMAL;
        }

        updateGPSIcon(gpsState);
    }

    /*
     * GPS捕捉数モードのGPS状態判断
     */
    private static void updateIconByGpsFixCount(int usedInFixGpsCount, float snrAverage)
    {
        if(usedInFixGpsCount<0){
            return;
        }

        int gpsState = 0;
        //衛星数で判定する従来の処理
        if(usedInFixGpsCount == 0){
            gpsState = MapView.GPS_NO;
        }else if(usedInFixGpsCount > 0 && usedInFixGpsCount < 3){
            gpsState = MapView.GPS_BAD;
        }else{
            gpsState = MapView.GPS_NORMAL;
        }

        //NaviLog.d("GPS", "LocationStatusController::updateIconByGpsFixCount() GPS count=" + usedInFixGpsCount + " snrAverage="+snrAverage);

        updateGPSIcon(gpsState);
    }

    /*
     * GPS座標イベントが途絶えたか判定
     */
    private static boolean isGpsFixFailed()
    {
        boolean bRet =((System.currentTimeMillis() - timeReceivedOnLocationChanged )> TIME_GPS_FIX_FAILED_MSEC);
        //NaviLog.d("GPS", "LocationStatusController::isGpsFixFailed() isGpsFixFailed=" + bRet);
        return bRet;
    }

    /*
     * 衛星状態イベントが途絶えたか判定
     */
    private static boolean isGpsStatusFailed()
    {
        boolean bRet =((System.currentTimeMillis() - timeReceivedOnUpdateGpsStatus)> TIME_GPS_STATUS_FAILED_MSEC);
        //NaviLog.d("GPS", "LocationStatusController::isGpsStatusFailed() isGpsStatusFailed=" + bRet);
        return bRet;
    }

    /*
     * (非 Javadoc)
     * @see java.lang.Runnable#run()
     *
     * GPS座標イベント/衛星状態イベントが途絶えたか判定するスレッド
     */
    public void run() {
        Location oLoc = null;
        while(true){
            try{
                Thread.sleep(1000); //1秒スリープ
            }catch(InterruptedException e){
                //何もしない
                if (null != locationListener) {
                    locationListener = null;
                }
            }
            /* V2.5 GPS停止時の状態取得 */
            oLoc = new Location( LocationManager.GPS_PROVIDER );
            //NaviLog.v("wata", "LocationStatusController Loop ");
            if (null != locationListener) {
            	locationListener.set_stopGPSDatasave(oLoc);
            }

            if(isGpsStatusFailed()){
                updateGPSIcon(MapView.GPS_OFF);
            }else if(isGpsFixFailed()){
                updateGPSIcon(MapView.GPS_NO);
                //NaviLog.d("GPS", "LocationStatusController::run() isGpsFixFailed=true");
            }
        }
    }
}
