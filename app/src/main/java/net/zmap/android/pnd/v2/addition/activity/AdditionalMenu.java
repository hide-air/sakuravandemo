package net.zmap.android.pnd.v2.addition.activity;

import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.activity.MenuBaseActivity;
import net.zmap.android.pnd.v2.common.utils.DbHelper;
import net.zmap.android.pnd.v2.sakuracust.CommonInquiryOps;

public class AdditionalMenu extends MenuBaseActivity
	implements OnClickListener{
	private Button mBtnLoadCSV = null;
//	private Button mBtnResult = null;
	private Button mBtnSaveCSV = null;
// Add by CPJsunagawa '2015-07-08 軌跡保存　Start
//	private Button mBtnTrackClear = null;
//	private Button mBtnTrackEnd = null;
// Add by CPJsunagawa '2015-07-08 End
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		LayoutInflater oInflater = LayoutInflater.from(this);
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.additional_main, null);	
		
		// コントロールのインスタンス
		mBtnLoadCSV = (Button)oLayout.findViewById(R.id.importcsv);
//		mBtnResult = (Button)oLayout.findViewById(R.id.showresultofadrmatch);
		mBtnSaveCSV  = (Button)oLayout.findViewById(R.id.exportcsv);
// Add by CPJsunagawa '2015-07-08 軌跡保存　Start
//		mBtnTrackClear  = (Button)oLayout.findViewById(R.id.tracklogclear);
//		mBtnTrackEnd  = (Button)oLayout.findViewById(R.id.tracklogend);
// Add by CPJsunagawa '2015-07-08 End
		
		// イベント定義
		mBtnLoadCSV.setOnClickListener(this);
//		mBtnResult.setOnClickListener(this);
		mBtnSaveCSV.setOnClickListener(this);
// Add by CPJsunagawa '2015-07-08  軌跡保存　Start
//		mBtnTrackClear.setOnClickListener(this);
//		mBtnTrackEnd.setOnClickListener(this);
// Add by CPJsunagawa '2015-07-08 End
		
		setViewInWorkArea(oLayout);
	}
	
	protected Dialog onCreateDialog(int id) {
		return super.onCreateDialog(id);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.equals(mBtnLoadCSV))
		{
			// CSV取り込み
			setResult(AppInfo.ID_ACTIVITY_DRICON_IMPORTCSV);
			finish();
			//dismissDialog(id)
		}
/*		
		else if (v.equals(mBtnResult))
		{
			// アドレスマッチングの結果
			if (!DbHelper.isExistDB())
			{
				return;
			}
			
			setResult(AppInfo.ID_ACTIVITY_DRICON_SHOWMATCHRESULT);
			finish();
		}
*/
		else if (v.equals(mBtnSaveCSV))
		{
			// CSV出力
			setResult(AppInfo.ID_ACTIVITY_DRICON_EXPORTCSV);
			finish();
		}
/*
// Add by CPJsunagawa '2015-07-08 軌跡保存　Start
		else if (v.equals(mBtnTrackClear))
		{
			// 軌跡保存開始
			setResult(AppInfo.ID_ACTIVITY_TRACK_LOG_CLEAR);
			finish();
		}
// Add by CPJsunagawa '2015-07-08 End
 */
	}
}
