package net.zmap.android.pnd.v2.data;

import net.zmap.android.pnd.v2.common.Constants;


public class ZNE_SatelliteInfo {
    private long                lnTotalSatsInView;                  ///< 上空に見える衛星数
    private JNITwoLong          Point;                              ///< 座標
    private double              dAltitude;                          ///< 標高
    private ZNE_Satellite[]     Satellite = new ZNE_Satellite[Constants.DEF_NE_NUM_SATELLITE];      ///< 衛星状態

    public long getTotalSatsInView() {
        return lnTotalSatsInView;
    }
    public JNITwoLong getPoint() {
        return Point;
    }
    public double getAltitude() {
        return dAltitude;
    }
    public ZNE_Satellite[] getSatellite() {
        return Satellite;
    }

}
