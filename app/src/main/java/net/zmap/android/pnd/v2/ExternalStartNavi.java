package net.zmap.android.pnd.v2;

import java.util.List;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.GeoUri;
import net.zmap.android.pnd.v2.api.map.ZoomLevel;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.utils.GeoUtils;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.maps.MapActivity;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

public class ExternalStartNavi extends AbstractExternalAction {

    public ExternalStartNavi(MapActivity mapActivity, Intent intent) {
        super(mapActivity, intent);
    }

    @Override
    public void run() {
        super.run();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doExternalAction() {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "ExternalStartNavi::doExternalAction");
        Uri data = mIntent.getData();
        Bundle bundle = mIntent.getExtras();

        GeoUri geo = new GeoUri(data.toString());
        GeoPoint coordinate = GeoUtils.toTokyoDatum(geo.getLatitude(), geo.getLongitude());
        byte zoomLevel = (geo.hasZoom() ? (byte)ZoomLevel.convertFromGoogleMapsZoomLevel(geo.getZoom()) : (byte)0xFF);

        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(
                NaviRun.NE_NEActType_ShowMapNormal, NaviRun.NE_NETransType_AroundSearchPoint,
                zoomLevel, 1, coordinate.getLongitudeMs(), coordinate.getLatitudeMs(), 0);

        int condition = bundle.getInt(Constants.INTENT_NAVI_EXTRA_ROUTE_CONDITION);
        String goalName = (String)bundle.get(Constants.INTENT_NAVI_EXTRA_GOAL_NAME);
        List<String> viaNames = bundle.getStringArrayList(Constants.INTENT_NAVI_EXTRA_VIA_NAMES);
        List<Location> viaLocations = bundle.getParcelableArrayList(Constants.INTENT_NAVI_EXTRA_VIA_LOCATIONS);
        boolean routeGuidanceStarting = bundle.getBoolean(Constants.INTENT_NAVI_EXTRA_ROUTE_GUIDANCE_STARTING, true);

        mMapActivity.startGuideForService(condition,
                0,
                CommonLib.getRoutePoints(goalName, geo, viaNames, viaLocations),
                routeGuidanceStarting);
    }
}
