/**
 *  @file		DriConDetailInfoActivity
 *  @brief		ドライブコンテンツ 詳細情報表示用クラス
 *
 *  @attention
 *  @note		詳細表示用インターフェース処理
 *
 *  @author		Manabu Watanabe [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2011-09-01)
 *  @version	$Revision: $ by $Author: $
 *
 */

package net.zmap.android.pnd.v2.dricon.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.Constants;
//ADD 2013.08.08 M.Honma 走行規制 メッセージ表示とフラグ取得 Start -->
import net.zmap.android.pnd.v2.common.DrivingRegulation;
//ADD 2013.08.08 M.Honma 走行規制 メッセージ表示とフラグ取得 End <--
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.DC_POIInfoDetailData;

import java.util.ArrayList;

public class DriConDetailInfoActivity extends DriconMenuListBaseActivity
{
	private ArrayList<ViewItemData>					m_lstViewItemData;
	private DC_POIInfoDetailData					m_dcPoiInfoDtlData;

	private final int DC_DetailInfo_ViewType_Separater = 0;
	private final int DC_DetailInfo_ViewType_Basic = 1;
	private final int DC_DetailInfo_ViewType_Free = 2;


	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.view.DriconMenuListBaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent oIntent = getIntent();
		if(oIntent != null){
			try{
				m_dcPoiInfoDtlData  = (DC_POIInfoDetailData) oIntent.getSerializableExtra(Constants.DC_INTENT_DETAIL_INFO);
			}
			catch(Exception e)
			{
				NaviLog.d(NaviLog.PRINT_LOG_TAG," Error at bundle " + e.toString());
			}
		}

		// 親クラス (DriconMenuListBaseActivity) で処理
		{
			// タイトル
			super.setTitleValue(m_dcPoiInfoDtlData.m_POIInfo.m_sPOIName);

			// タイトル右側のアイコン
			super.setTitleIcon(m_dcPoiInfoDtlData.m_POIInfo.m_sIconPath);

			// 上記の設定で初期化
			initDriConMenuLayout();
		}

		// ドライブコンテンツ詳細の表示項目の設定
		initLstViewItem();


	}
	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.common.activity.MenuBaseActivity#goBack()
	 */
	public void goBack(){
		super.goBack();
		notifyUpdataList();
	}

	/* (非 Javadoc)
	 * @see net.zmap.android.pnd.v2.dricon.view.DriconMenuListBaseActivity#getViewList(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getViewList(int position, View oView, ViewGroup parent) {
		if(oView == null)
		{
			LayoutInflater oInflater = LayoutInflater.from(DriConDetailInfoActivity.this);
			m_oLayout = (LinearLayout)oInflater.inflate(R.layout.dricon_detail_info_item, null);
			oView = m_oLayout;
		} else {
			m_oLayout = (LinearLayout)oView;
		}

		// 電話/住所/URL 他
		LinearLayout oBasicView = (LinearLayout)m_oLayout.findViewById(R.id.dc_detail_info_item_basic);

		// セクション
		LinearLayout oFreeView = (LinearLayout)m_oLayout.findViewById(R.id.dc_detail_info_item_free);

		// セパレータ
		LinearLayout oSeparaterView = (LinearLayout)m_oLayout.findViewById(R.id.dc_detail_info_item_separater);

		ViewItemData viewItemData = m_lstViewItemData.get(position);
		switch (viewItemData.intViewType)
		{
		case DC_DetailInfo_ViewType_Basic:
			// 電話/住所/URL 他
			oBasicView.setVisibility(View.VISIBLE);
			oFreeView.setVisibility(View.GONE);
			oSeparaterView.setVisibility(View.GONE);

			makeViewListItem_Basic(viewItemData);

			break;

		case DC_DetailInfo_ViewType_Free:
			// セクション
			oBasicView.setVisibility(View.GONE);
			oFreeView.setVisibility(View.VISIBLE);
			oSeparaterView.setVisibility(View.GONE);

			makeViewListItem_Free(viewItemData);

			break;

		case DC_DetailInfo_ViewType_Separater:
			// セパレータ
			oBasicView.setVisibility(View.GONE);
			oFreeView.setVisibility(View.GONE);
			oSeparaterView.setVisibility(View.VISIBLE);

		}
		return oView;
	}

	/** 電話/住所/URL 他のレイアウト設定
	 *
	 * @param viewItemData 表示情報
	 */
	private void makeViewListItem_Basic(ViewItemData viewItemData) {
		// 電話/住所/URL 他

		// オブジェクト取得
		TextView oBasicTitle = (TextView)m_oLayout.findViewById(R.id.dc_detail_info_item_basic_title);
		TextView oBasicText = (TextView)m_oLayout.findViewById(R.id.dc_detail_info_item_basic_text);
		Button oBasicButton = (Button)m_oLayout.findViewById(R.id.dc_detail_info_item_basic_button);

		// タイトル
		if (viewItemData.bShowBasicTitle) {
			oBasicTitle.setVisibility(View.VISIBLE);
			oBasicTitle.setText(viewItemData.strBasicTitle);
		} else {
			oBasicTitle.setVisibility(View.GONE);
		}

		// 内容
		if (viewItemData.bShowBasicText) {
			oBasicText.setVisibility(View.VISIBLE);
			oBasicText.setText(viewItemData.strBasicText);
		} else {
			oBasicText.setVisibility(View.GONE);
		}

		// ボタン
		if (viewItemData.bShowBasicButton) {
			oBasicButton.setVisibility(View.VISIBLE);
			oBasicButton.setText(viewItemData.strBasicButton);
			oBasicButton.setOnClickListener(viewItemData.oOnClickListenerBasicButton);
		} else {
			oBasicButton.setVisibility(View.GONE);
		}
	}

	/** セクション別の情報のレイアウト作成
	 *
	 * @param viewItemData 表示情報
	 */
	private void makeViewListItem_Free(ViewItemData viewItemData) {
		// セクション別情報

		// オブジェクト取得
		TextView oFreeSection = (TextView)m_oLayout.findViewById(R.id.dc_detail_info_item_free_section);
		TextView oFreeLabel = (TextView)m_oLayout.findViewById(R.id.dc_detail_info_item_free_label);
		TextView oFreevalue = (TextView)m_oLayout.findViewById(R.id.dc_detail_info_item_free_value);

		// セクション
		if (viewItemData.bShowFreeSection) {
			oFreeSection.setVisibility(View.VISIBLE);
			oFreeSection.setText(viewItemData.strFreeSection);
		} else {
			oFreeSection.setVisibility(View.GONE);
		}

		// ラベル
		if (viewItemData.bShowFreeLabel) {
			oFreeLabel.setVisibility(View.VISIBLE);
			oFreeLabel.setText(viewItemData.strFreeLabel);
		} else {
			oFreeLabel.setVisibility(View.GONE);
		}

		// 内容
		if (viewItemData.bShowFreeValue) {
			oFreevalue.setVisibility(View.VISIBLE);
			oFreevalue.setText(viewItemData.strFreeValue);
		} else {
			oFreevalue.setVisibility(View.GONE);
		}
	}

	/** ドライブコンテンツ詳細の表示項目の設定
	 *
	 * ・項目の表示 / 非表示、表示文字列、ボタン押下時の処理内容
	 */
	private void initLstViewItem() {
		if (m_dcPoiInfoDtlData == null) {
			return;
		}

		m_lstViewItemData = new ArrayList<ViewItemData>();

		// ドライブコンテンツ詳細は、以下の順に表示する。

		// 詳細情報?
		if (m_dcPoiInfoDtlData.m_sDescription != null) {
			m_lstViewItemData.add(makeViewData_Desc(m_dcPoiInfoDtlData.m_sDescription));
		}

// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
		// WEBto
		if ((m_dcPoiInfoDtlData.m_sWebtoLabel != null) &&
			(m_dcPoiInfoDtlData.m_sWebtoUrl != null) &&
			(m_dcPoiInfoDtlData.m_sWebtoUrlText != null))
		{
			m_lstViewItemData.add(makeViewData_WebTo(
				m_dcPoiInfoDtlData.m_sWebtoLabel,
				m_dcPoiInfoDtlData.m_sWebtoUrlText,
				m_dcPoiInfoDtlData.m_sWebtoUrl
			));
		}
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END

		// 電話番号
		if (m_dcPoiInfoDtlData.m_sTellPhone != null)
		{
			m_lstViewItemData.add(makeViewData_Tel(m_dcPoiInfoDtlData.m_sTellPhone));
		}

		// 住所
		if (m_dcPoiInfoDtlData.m_sAddress != null)
		{
			m_lstViewItemData.add(makeViewData_Adrs(m_dcPoiInfoDtlData.m_sAddress));
		}

		// URL
		if (m_dcPoiInfoDtlData.m_sUrl != null) {
			m_lstViewItemData.add(makeViewData_Url(m_dcPoiInfoDtlData.m_sUrl, m_dcPoiInfoDtlData.m_sUrlText));
		}

		// 自由項目
		if (m_dcPoiInfoDtlData.m_oSectionInfo != null) {
			for (int i = 0; i < m_dcPoiInfoDtlData.m_oSectionInfo.size(); i++) {
				// セクション
				int sectionId = m_dcPoiInfoDtlData.m_oSectionInfo.get(i).sectionID;
				if (m_dcPoiInfoDtlData.m_oSectionInfo.get(i).textValue != null) {
					m_lstViewItemData.add(makeViewData_Sec(m_dcPoiInfoDtlData.m_oSectionInfo.get(i).textValue));
				}

				if (m_dcPoiInfoDtlData.m_oTextInfo != null) {
					for (int j = 0; j < m_dcPoiInfoDtlData.m_oTextInfo.size(); j++) {
						// テキスト
						if (m_dcPoiInfoDtlData.m_oTextInfo.get(j).sectionID == sectionId) {
							if (m_dcPoiInfoDtlData.m_oTextInfo.get(j).textLabel != null ||
									m_dcPoiInfoDtlData.m_oTextInfo.get(j).textValue != null) {
								m_lstViewItemData.add(makeViewData_Text(
										m_dcPoiInfoDtlData.m_oTextInfo.get(j).textLabel, m_dcPoiInfoDtlData.m_oTextInfo.get(j).textValue));
							}
						}
					}
				}
			}
		}

		super.m_nCount = m_lstViewItemData.size();
	}

// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
	/** Webtoの表示情報作成
	 *
	 * @param str WebToLabel
	 * @param str WebToボタンのテキスト
	 * @param str WebToボタンのURL
	 * @return ViewItemData 表示情報
	 */
	private ViewItemData makeViewData_WebTo(final String sLabel, final String sText, final String sUrl ) {
		ViewItemData viewItemData = new ViewItemData();

		viewItemData.intViewType = DC_DetailInfo_ViewType_Basic;

		viewItemData.strBasicText = sLabel;
		viewItemData.bShowBasicText = true;

		viewItemData.strBasicButton = sText;
		viewItemData.bShowBasicButton = true;
		viewItemData.oOnClickListenerBasicButton = new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				doActionView(sUrl);
			}
		};

		return viewItemData;
	}
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END

	/** 電話番号の表示情報作成
	 *
	 * @param str 電話番号文字列
	 * @return ViewItemData 表示情報
	 */
	private ViewItemData makeViewData_Tel(final String str) {
		ViewItemData viewItemData = new ViewItemData();

		viewItemData.intViewType = DC_DetailInfo_ViewType_Basic;

		viewItemData.strBasicTitle = getResources().getString(R.string.dc_detail_info_title_tel);
		viewItemData.bShowBasicTitle = true;

		viewItemData.strBasicText = str;
		viewItemData.bShowBasicText = true;

		viewItemData.strBasicButton = getResources().getString(R.string.dc_detail_info_btn_tel);
		viewItemData.bShowBasicButton = true;
		viewItemData.oOnClickListenerBasicButton = new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				doActionDial(str);
			}
		};

		return viewItemData;
	}

	/** 住所の表示情報作成
	 *
	 * @param str 住所文字列
	 * @return ViewItemData 表示情報
	 */
	private ViewItemData makeViewData_Adrs(String str) {
		ViewItemData viewItemData = new ViewItemData();

		viewItemData.intViewType = DC_DetailInfo_ViewType_Basic;

		viewItemData.strBasicTitle = getResources().getString(R.string.dc_detail_info_title_adrs);
		viewItemData.bShowBasicTitle = true;

		viewItemData.strBasicText = str;
		viewItemData.bShowBasicText = true;

		return viewItemData;
	}

	/** URL の表示情報作成
	 *
	 * @param str URL 文字列
	 * @return ViewItemData 表示情報
	 */
	private ViewItemData makeViewData_Url(final String url, String text) {
		ViewItemData viewItemData = new ViewItemData();

		viewItemData.intViewType = DC_DetailInfo_ViewType_Basic;

		if (text != null) {
			// URL の TEXT が存在する場合は、ボタンの文字列に設定
			viewItemData.strBasicButton = text;
		} else {
			viewItemData.strBasicButton = getResources().getString(R.string.dc_detail_info_btn_url);
		}

		viewItemData.bShowBasicButton = true;
		viewItemData.oOnClickListenerBasicButton = new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				doActionView(url);
			}
		};

		return viewItemData;
	}

	/** 詳細情報の表示情報作成
	 *
	 * @param str 詳細情報文字列
	 * @return ViewItemData 表示情報
	 */
	private ViewItemData makeViewData_Desc(String str) {
		ViewItemData viewItemData = new ViewItemData();

		viewItemData.intViewType = DC_DetailInfo_ViewType_Basic;

		viewItemData.strBasicText = str;
		viewItemData.bShowBasicText = true;

		return viewItemData;
	}

	/** セクション表示情報作成
	 *
	 * @param str セクション名
	 * @return ViewItemData 表示情報
	 */
	private ViewItemData makeViewData_Sec(String str) {
		ViewItemData viewItemData = new ViewItemData();

		viewItemData.intViewType = DC_DetailInfo_ViewType_Free;

		viewItemData.strFreeSection = str;
		viewItemData.bShowFreeSection = true;

		return viewItemData;
	}

	/** セクション配下のテキスト表示情報作成
	 *
	 * @param label ラベルテキスト
	 * @param value 値
	 * @return ViewItemData 表示情報
	 */
	private ViewItemData makeViewData_Text(String label, String value) {
		ViewItemData viewItemData = new ViewItemData();

		viewItemData.intViewType = DC_DetailInfo_ViewType_Free;

		viewItemData.strFreeLabel = label;
		viewItemData.bShowFreeLabel = true;

		viewItemData.strFreeValue = value;
		viewItemData.bShowFreeValue = true;

		return viewItemData;
	}

	/** 電話をかける
	 *
	 * @param str 電話番号
	 */
	private void doActionDial(String str) {
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ詳細 電話をかける禁止 Start -->
		if(DrivingRegulation.CheckDrivingRegulation()){
			return;
		}
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ詳細 電話をかける禁止 End <--
		Intent intent = new Intent(
				Intent.ACTION_DIAL, Uri.parse(getResources().getString(R.string.dc_detail_info_prefix_tel) + str));
		startActivity(intent);
	}

	/** ブラウザで開く
	 *
	 * @param str URL
	 */
	private void doActionView(String str) {
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ詳細 ブラウザで開く禁止 Start -->
		if(DrivingRegulation.CheckDrivingRegulation()){
			return;
		}
// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツ詳細 ブラウザで開く禁止 End <--
		try{
			Intent intent = new Intent(
					Intent.ACTION_VIEW, Uri.parse(str));
			startActivity(intent);
		}catch(Exception e){
			//NaviLog.d(NaviLog.PRINT_LOG_TAG,"[--DEBUG] url exception");
		}
	}


	/** ドライブコンテンツの詳細情報クラス
	 *
	 */
	static class ViewItemData{
		int intViewType;

		public String strBasicTitle;
		public String strBasicText;
		public String strBasicButton;
		public String strFreeSection;
		public String strFreeLabel;
		public String strFreeValue;

		public boolean bShowBasicTitle;
		public boolean bShowBasicText;
		public boolean bShowBasicButton;
		public boolean bShowFreeSection;
		public boolean bShowFreeLabel;
		public boolean bShowFreeValue;

		public Button.OnClickListener oOnClickListenerBasicButton;
	}

}