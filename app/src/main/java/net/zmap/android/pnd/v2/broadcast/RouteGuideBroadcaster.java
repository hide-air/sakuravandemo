package net.zmap.android.pnd.v2.broadcast;

import net.zmap.android.pnd.v2.api.NaviIntent;
import net.zmap.android.pnd.v2.api.event.RouteGuidance;
import net.zmap.android.pnd.v2.common.utils.GeoUtils;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.ZNE_RouteGuidance_t;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

public class RouteGuideBroadcaster {

    Context mBroadCaster = null;

    public RouteGuideBroadcaster(Context broadcaster) {
        mBroadCaster = broadcaster;
    }

    /**
     * ルート情報ブロードキャスト(ルート案内中の目的地、経由地、目標案内で呼ばれる)
     *
     */
    public void intentRouteGuide() {

        if (mBroadCaster == null) {
            return;
        }

        JNITwoLong myPosi = new JNITwoLong();
        JNITwoLong targetPos = null;
        String targetPosName = "";
        Location targetLocation = null;
        Location vehichelLocation = null;

        //案内情報取得
        ZNE_RouteGuidance_t stRouteGuidence = new ZNE_RouteGuidance_t();
        NaviRun.GetNaviRunObj().JNI_DG_GetGuidePointInfo(stRouteGuidence);

        //イベント種別取得
        int eventType = cnvEventCodetoGuideType(stRouteGuidence.getEventCode());
        if (eventType == RouteGuidance.TYPE_EVENT_NONE) {
            Log.e("RouteGuideBroadcaster", "intentRouteGuide: eventType is invalid.");
            return;
        }

        //イベント発生ロケーション取得(座標と現在日時)
        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(myPosi);
        vehichelLocation = GeoUtils.toWGS84Location(myPosi.getM_lLat(), myPosi.getM_lLong());
        if (vehichelLocation == null) {
            Log.e("RouteGuideBroadcaster", "intentRouteGuide: vehichelLocation is invalid.");
            return;
        }
        vehichelLocation.setTime(System.currentTimeMillis());

/*
        //目標名称取得(経由地、目的地の場合は経由地名称、目的地名称)
        if (eventType == RouteGuide.TYPE_EVENT_VIAGUIDE || eventType == RouteGuide.TYPE_EVENT_VIAGUIDE ) {
            ZNE_GuideRoutePoint NeRouteRequest = new ZNE_GuideRoutePoint();
            NaviRun.GetNaviRunObj().JNI_Java_GetGuideRoutePoint(NeRouteRequest);
            ZNE_RoutePoint[] guidePoint = NeRouteRequest.getStGuidePoint();
            if (guidePoint != null || guidePoint.length > 0) {
                if (eventType == RouteGuide.TYPE_EVENT_VIAGUIDE) {
                    //経由地の何番目か、判断する必要がある これだと先頭経由地の名前になる
                    targetPosName = guidePoint[Constants.NAVI_VIA_INDEX].getSzPointName();
                    targetPos = new JNITwoLong();
                    targetPos = guidePoint[Constants.NAVI_VIA_INDEX].getGuidePoint().getStPos();
                } else if (eventType == RouteGuide.TYPE_EVENT_VIAGUIDE) {
                    targetPosName = guidePoint[Constants.NAVI_DEST_INDEX].getSzPointName();
                    targetPos = new JNITwoLong();
                    targetPos = guidePoint[Constants.NAVI_DEST_INDEX].getGuidePoint().getStPos();
                }
            }
        } else {

*/
            targetPos = stRouteGuidence.getGuidePoint().getstPos();
            targetLocation = GeoUtils.toWGS84Location(targetPos.getM_lLat(), targetPos.getM_lLong());
            if (targetLocation == null) {
                Log.e("RouteGuideBroadcaster", "intentRouteGuide: targetLocation is invalid.");
                return;
            }

            JNIString targetAddress = new JNIString();
            NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(targetPos.getM_lLong(), targetPos.getM_lLat(), targetAddress);
            if (targetAddress.getM_StrAddressString() != null){
                targetPosName = targetAddress.getM_StrAddressString();
//                Log.i("RouteGuideBroadcaster", "targetPosName: " + targetPosName);
            }else{
//                Log.i("RouteGuideBroadcaster", "targetPosName: null");

            }
            if (targetPosName.length() == 0) {
                Log.i("RouteGuideBroadcaster", "intentRouteGuide: targetPosName is empty.");
                //続行!!
            }

/*        }
*/
        //ルート情報(API)作成
        RouteGuidance routeGuide = new RouteGuidance();
        if (eventType == RouteGuidance.TYPE_EVENT_CROSSGUIDE) {
            routeGuide.setCrossTargetInfo(eventType, vehichelLocation, cnvEventDetailCodetoTargetType(stRouteGuidence.getEventDetailCode()), targetPosName, targetLocation, stRouteGuidence.getDist(), stRouteGuidence.getTargetDirection());
        } else {
            routeGuide.setTargetInfo(eventType, vehichelLocation, targetPosName, targetLocation, stRouteGuidence.getDist());
        }

        //ルート情報(API)をブロードキャスト
        broadcastRouteGuide(routeGuide);

        return;

    }


    /**
     * イベント種別指定、ルート情報ブロードキャスト(案内開始、終了で呼ばれる)
     *
     * @param eventType
     *            イベント種別(routeGuide.TYPE_EVENT_STARTGUIDE/routeGuide.TYPE_EVENT_ENDGUIDE)
     */
    public void intentNavigation(int eventType) {

        if (mBroadCaster == null) {
            return;
        }

        JNITwoLong myPosi = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(myPosi);

        //イベント発生ロケーション取得
        Location vehichelLocation = GeoUtils.toWGS84Location(myPosi.getM_lLat(), myPosi.getM_lLong());
        if (vehichelLocation == null) {
            Log.e("RouteGuideBroadcaster", "intentNavigation: vehichelLocation is invalid.");
            return;
        }
        vehichelLocation.setTime(System.currentTimeMillis());

        //ルート情報(API)作成
        RouteGuidance routeGuide = new RouteGuidance();
        routeGuide.setGuideStatus(eventType, vehichelLocation);

        //ルート情報(API)をブロードキャスト
        broadcastRouteGuide(routeGuide);

        return;
    }

    /**
     * ルート案内情報 ブロードキャスト
     */
    protected void broadcastRouteGuide(RouteGuidance routeGuide) {

        Intent intent = new Intent();
        intent.setAction(NaviIntent.ACTION_GUIDE);

        Bundle bundle = new Bundle();
        bundle.putInt(NaviIntent.EXTRA_GUIDE_TYPE, routeGuide.getEventType());
        bundle.putParcelable(NaviIntent.EXTRA_LOCATION, routeGuide.getLocation());
        bundle.putInt(NaviIntent.EXTRA_TARGET_TYPE, routeGuide.getTargetType());
        bundle.putString(NaviIntent.EXTRA_TARGET_NAME,
                    routeGuide.getTargetName());
        bundle.putParcelable(NaviIntent.EXTRA_TARGET_LOCATION,
                    routeGuide.getTargetLocation());
        bundle.putFloat(NaviIntent.EXTRA_TARGET_DISTANCE,
                    routeGuide.getTargetDistance());
        bundle.putInt(NaviIntent.EXTRA_TARGET_DIRECTION,
                    routeGuide.getTargetDirection());

        intent.putExtras(bundle);
        mBroadCaster.sendBroadcast(intent);
    }

    /**
     * イベントコード → ルート案内種別コード 変換
     *
     * @param iEventDetailCode
     *            ZNE_RouteGuidance_tのイベントコード
     * @return API routeGuideクラスの案内種別コード
     */
    protected int cnvEventCodetoGuideType(int iEventCode) {

//      Log.d("NaviApiBroadcastSender", "cnvEventCodetoGuideType: " + Integer.toString(iEventCode));

        int eventType = RouteGuidance.TYPE_EVENT_NONE;
        switch (iEventCode) {
            case 5://目標案内
                eventType = RouteGuidance.TYPE_EVENT_CROSSGUIDE;
                break;
            case 6://経由地案内
                eventType = RouteGuidance.TYPE_EVENT_VIAGUIDE;
                break;
            case 7://目的地案内
                eventType = RouteGuidance.TYPE_EVENT_GOALGUIDE;
                break;
            case 8://リルート
                eventType = RouteGuidance.TYPE_EVENT_REROUTE;
                break;
            default:
                break;
        }
        return eventType;
    }

    /**
     * イベント詳細コード → 目標種別コード 変換
     *
     * @param iEventDetailCode
     *            ZNE_RouteGuidance_tのイベント詳細コード
     * @return API routeGuideクラスの目標種別コード
     */
    protected int cnvEventDetailCodetoTargetType(int iEventDetailCode) {
        int targeType = RouteGuidance.TYPE_TARGET_NONE;

        switch (iEventDetailCode) {
            case 1: //交差点
                targeType = RouteGuidance.TYPE_TARGET_CROSS;
                break;
            case 2: //分岐
                targeType = RouteGuidance.TYPE_TARGET_TILT;
                break;
            case 3: //合流
                targeType = RouteGuidance.TYPE_TARGET_JOINT;
                break;
            case 4: //高速入口
                targeType = RouteGuidance.TYPE_TARGET_ONHIGHWAY;
                break;
            case 5: //高速出口
                targeType = RouteGuidance.TYPE_TARGET_OFFHIGHWAY;
                break;
            case 6: //料金所
                targeType = RouteGuidance.TYPE_TARGET_TOLL;
                break;
            default:
                break;
        }
        return targeType;
    }

}
