package net.zmap.android.pnd.v2.api;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import net.zmap.android.pnd.v2.api.exception.NaviEngineException;
import net.zmap.android.pnd.v2.api.exception.NaviException;
import net.zmap.android.pnd.v2.api.exception.NaviIOException;
import net.zmap.android.pnd.v2.api.exception.NaviInvalidValueException;
import net.zmap.android.pnd.v2.api.exception.NaviNotFoundException;
import net.zmap.android.pnd.v2.api.exception.NaviOutOfRangeException;
import net.zmap.android.pnd.v2.api.exception.NaviRemoteException;
import net.zmap.android.pnd.v2.api.exception.NaviRequestFailureException;
import net.zmap.android.pnd.v2.api.exception.NaviServiceException;
import net.zmap.android.pnd.v2.api.exception.NaviUnknownException;

public class NaviResult implements Parcelable {

    /** 成功 */
    public static final int RESULT_OK                =  0;

    /** 失敗 */
    public static final int RESULT_NG                = -1;

    /** 引数エラー */
    public static final int RESULT_ILLEGAL_ARGUMENT  = -2;      // IllegalArgumentException (< RuntimeException)

    /** 処理するべきではないときに処理が呼ばれた */
    public static final int RESULT_ILLEGAL_STATE     = -3;      // IllegalStateException (< RuntimeException)

    /** メモリ不足 */
    public static final int RESULT_OUT_OF_MEMORY     = -4;      // OutOfMemoryError (< VirtualMachineError)

    /** null 値参照 (引数、返値など) */
    public static final int RESULT_NULL_POINTER      = -5;      // NullPointerException (< RuntimeException)

    /** 範囲外の値、添字オーバー、件数オーバー */
    public static final int RESULT_OUT_OF_RANGE      = -6;      // NaviOutOfRangeException (< NaviException)

    /** 不正な値、無効値 */
    public static final int RESULT_INVALID_VALUE     = -7;      // NaviInvalidValueException (< NaviException)

    /** データなし、ファイルなし */
    public static final int RESULT_NOT_FOUND         = -8;      // NaviNotFoundException (< NaviException)

    /** 計算失敗、処理失敗 */
    public static final int RESULT_REQUEST_FAILURE   = -9;      // NaviRequestFailureException (< NaviException)

    /** リモート呼び出し失敗 */
    public static final int RESULT_REMOTE_ERROR      = -10;     // NaviRemoteException (< NaviException)

    /** ファイル・メモリ入出力失敗 */
    public static final int RESULT_IO_ERROR          = -11;     // NaviIOException (< NaviException)

    /** エンジンモジュールでのエラー */
    public static final int RESULT_NAVI_ENGINE_ERROR = -12;     // NaviEngineException (< NaviException)

    /** サービス接続エラー */
    public static final int RESULT_SERVICE_ERROR     = -13;     // NaviServiceException (< NaviException)

    /** 分類外のエラー */
    public static final int RESULT_UNKNOWN_ERROR     = -99;     // NaviUnkwonException (< NaviException)


    private int             mResultCode              = RESULT_OK;
    private long            mEngineErrorCode         = 0;

    private String          mDescription             = "";

    private static final String OK_DESCRIPTION       = "";
    private static final String ERROR_DESCRIPTION    = "エラーが発生しました.";

    /**
     * コンストラクタ
     */
    public NaviResult() {
    }

    /**
     * コンストラクタ
     */
    private NaviResult(Parcel in) {
        readFromParcel(in);
    }

    /**
     * エラー内容設定
     *
     * @param resultCode リザルトコード (RESULT_*)
     * @param description 詳細メッセージ
     */
    public void setResult(int resultCode, String description) {
        setResultCode(resultCode);
        setDescription(description);
    }

    /**
     * 詳細メッセージの再設定
     *
     * @param resultCode リザルトコード (RESULT_*)
     */
    private void resetDescription(int resultCode) {
        if (resultCode == RESULT_OK) {
            mDescription = OK_DESCRIPTION;
        } else {
            if (mDescription.length() == 0) {
                mDescription = ERROR_DESCRIPTION;
            }
        }
    }

    /**
     * エラーコード設定
     *
     * @param resultCode リザルトコード (RESULT_*)
     */
    public void setResultCode(int resultCode) {
        mResultCode = resultCode;
        resetDescription(resultCode);
    }

    /**
     * エラーコード取得
     *
     * @return リザルトコード (RESULT_*)
     */
    public int getResultCode() {
        return mResultCode;
    }

    /**
     * ナビエンジンエラーコード設定
     *
     * @param engineErrorCode ナビエンジンエラーコード
     */
    public void setEngineErrorCode(long engineErrorCode) {
        setResultCode(RESULT_NAVI_ENGINE_ERROR);
        mEngineErrorCode = engineErrorCode;
    }

    /**
     * ナビエンジンエラー設定
     * @param engineErrorCode ナビエンジンエラーコード
     * @param description 詳細メッセージ
     */
    public void setEngineError(long engineErrorCode, String description) {
        String strJMsg;
        setResultCode(RESULT_NAVI_ENGINE_ERROR);
        setEngineErrorCode(engineErrorCode);
        strJMsg = description + ": 0x" + Integer.toHexString((int)engineErrorCode);
        setDescription(strJMsg);
        Log.e("NaviResult", strJMsg);
    }

    /**
     * ナビエンジンエラーコード
     *
     * @return ナビエンジンエラーコード
     */
    public long getEngineErrorCode() {
        return mEngineErrorCode;
    }

    /**
     * 詳細メッセージ設定
     *
     * @param description 詳細メッセージ
     */
    public void setDescription(String description) {
        mDescription = description;
    }

    /**
     * 詳細メッセージ取得
     *
     * @return 詳細メッセージ
     */
    public String getDescription() {
        return mDescription;
    }

    /**
     * エラーかチェック
     *
     * @return true:エラーである false:エラーでない
     */
    public boolean isError() {
        return (mResultCode < RESULT_OK) ? true : false;
    }

    /**
     * 成功かチェック
     *
     * @return true:成功した false:成功でない
     */
    public boolean isOK() {
        return (mResultCode == RESULT_OK) ? true : false;
    }

    /**
     * 例外発行の必要があれば発行する
     *
     * @throws IllegalArgumentException     引数エラー
     * @throws IllegalStateException        処理するべきではないときに処理が呼ばれた
     * @throws OutOfMemoryError             メモリ不足
     * @throws NullPointerException         null 値参照 (引数、返値など)
     * @throws NaviOutOfRangeException      範囲外の値、添字オーバー、件数オーバー
     * @throws NaviInvalidValueException    不正な値、無効値
     * @throws NaviNotFoundException        データなし、ファイルなし
     * @throws NaviRequestFailureException  計算失敗、処理失敗
     * @throws NaviRemoteException          リモート呼び出し失敗
     * @throws NaviIOException              ファイル・メモリ入出力失敗
     * @throws NaviServiceException         サービス接続エラー
     * @throws NaviUnknownException         分類外のエラー
     */
    public void doException() throws NaviException {
        if (isOK()) {
            return;
        }
        switch (mResultCode) {
        case RESULT_ILLEGAL_ARGUMENT:
            throw new IllegalArgumentException(mDescription);
        case RESULT_ILLEGAL_STATE:
            throw new IllegalStateException(mDescription);
        case RESULT_OUT_OF_MEMORY:
            throw new OutOfMemoryError(mDescription);
        case RESULT_NULL_POINTER:
            throw new NullPointerException(mDescription);
        case RESULT_OUT_OF_RANGE:
            throw new NaviOutOfRangeException(mDescription);
        case RESULT_INVALID_VALUE:
            throw new NaviInvalidValueException(mDescription);
        case RESULT_NOT_FOUND:
            throw new NaviNotFoundException(mDescription);
        case RESULT_REQUEST_FAILURE:
            throw new NaviRequestFailureException(mDescription);
        case RESULT_REMOTE_ERROR:
            throw new NaviRemoteException(mDescription);
        case RESULT_IO_ERROR:
            throw new NaviIOException(mDescription);
        case RESULT_SERVICE_ERROR:
            throw new NaviServiceException(mDescription);
        case RESULT_NAVI_ENGINE_ERROR:
            throw new NaviEngineException(mDescription);

        }
        throw new NaviUnknownException(mDescription);
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /* (非Javadoc)
     *
     */
    public void readFromParcel(Parcel in) {
        mResultCode = in.readInt();
        mEngineErrorCode = in.readLong();
        mDescription = in.readString();
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeInt(mResultCode);
        out.writeLong(mResultCode);
        out.writeString(mDescription);
    }

    /* (非Javadoc)
     *
     */
    public static final Parcelable.Creator<NaviResult> CREATOR = new Parcelable.Creator<NaviResult>() {
        public NaviResult createFromParcel(Parcel in) {
            return new NaviResult(in);
        }
        public NaviResult[] newArray(int size) {
            return new NaviResult[size];
        }
    };
}