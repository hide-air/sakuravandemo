package net.zmap.android.pnd.v2.common.services;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class FlightModeService {

	private FlightModeReceiver flightReceiver = null;
	private Context m_oContext = null;
	private FlightModeEventListener flightListener = null;


	public FlightModeService()
	{
		if(flightReceiver == null)
		{
			flightReceiver = new FlightModeReceiver();
//			IntentFilter intetFilter = new IntentFilter(
//					Intent.ACTION_AIRPLANE_MODE_CHANGED);
//			m_oContext.registerReceiver(flightReceiver, intetFilter);
		}
	}
	public void registerReceiver(Context oContext, FlightModeEventListener oListener){
        m_oContext = oContext;
        flightListener = oListener;

        m_oContext.registerReceiver(flightReceiver,
                new IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED));
    }

	public void release()
	{
		if(m_oContext != null && flightReceiver != null)
		{
			m_oContext.unregisterReceiver(flightReceiver);
		}
	}
	public class FlightModeReceiver extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context oContext, Intent oIntent)
		{
			if(flightListener != null)
			{
				flightListener.onFlightModeChange(oContext,oIntent);
			}
		}

	}
}
