package net.zmap.android.pnd.v2.maps;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.FloatMath;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.NaviIntent;
import net.zmap.android.pnd.v2.api.overlay.Icon;
import net.zmap.android.pnd.v2.api.overlay.OverlayItem;
import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.DrivingRegulation;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.utils.GeoUtils;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScreenAdapter;
import net.zmap.android.pnd.v2.common.view.NaviStateIcon;
import net.zmap.android.pnd.v2.data.JNIByte;
import net.zmap.android.pnd.v2.data.JNIDouble;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.Java_IconInfo;
import net.zmap.android.pnd.v2.data.MapInfo;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.ZNUI_ANGLE_DATA;
import net.zmap.android.pnd.v2.data.ZNUI_UI_MARK_DATA;
import net.zmap.android.pnd.v2.dricon.controller.DriverContentsCtrl;
import net.zmap.android.pnd.v2.overlay.DrawUserLayerManager;
import net.zmap.android.pnd.v2.sakuracust.DrawTrackActivity;
import net.zmap.android.pnd.v2.sakuracust.TrackIconEdit;
import net.zmap.android.pnd.v2.vics.VicsManager;

import java.nio.FloatBuffer;
import java.util.Timer;
import java.util.TimerTask;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
//import net.zmap.android.pnd.v2.common.activity.MainMenu;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
//import net.zmap.android.pnd.v2.data.DC_POIInfoShowList;

/**
 * @author wangdong
 *
 */
public class MapView extends RelativeLayout implements OnTouchListener {

//Del 2011/11/11 Z01_h_yamada Start -->
//    // XuYang add start #805
//    private BatteryIcon oBattery;
//    private SignalIcon imgSignal;
//    // XuYang add end #805
//Del 2011/11/11 Z01_h_yamada End <--
    /**
     * the instance of MapView
     */
    private static MapView    theObj;

    /**
     * LayoutInflater
     */
    private LayoutInflater    inflater;

    /**
     * Main layout, used for load Top View
     */
    private RelativeLayout    m_oMainLayout;

    /**
     * Extended View，reserved
     */
    private RelativeLayout    m_oExView;

    /**
     * TopView Layout
     */
    private RelativeLayout    m_oMainView;

// Add by CPJsunagawa '2015-07-28 Start
    public DrawTrackActivity DTActivity;
// Add by CPJsunagawa '2015-07-28 End
    
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応 START
//    private LinearLayout      m_oCurPosInfo;
//    private LinearLayout      m_oCurPosInfoPort;
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応  END

    /**
     * MP View
     */
    private GL2SurfaceView    mSurfaceView;
    private ViewGroup         mSurfaceHolder;
    private MapViewRenderer   mMapViewRenderer;

    /**
     * the instance of MapActivity
     */
    private MapActivity       mapActivity;

    /**
     * the instance of TopView
     */
    private MapTopView        m_oTopView;

//Add 2011/09/28 Z01_h_yamada Start -->
    private LinearLayout      mLeftInfoLayout;
//Add 2011/09/28 Z01_h_yamada End <--

    /**
     * ZoomIn Button
     */
    private Button            btnZoomIn;

    /**
     * ZoomOut Button
     */
    private Button            btnZoomOut;

    /**
     * Scale State
     */
    private Button            btnScale;

// Add by CPJsunagawa '2015-07-24 軌跡保存 Start
    /**
     * 軌跡保存
     */
//    private Button            btnTrackSaveStart;
    public Button             btnTrackSaveStart;
    private Button            btnTrackSaveEnd;
    private Button            btnTrackIconEdit;
    
	public static String mCurrentCourse = "";
// Add by CPJsunagawa '2015-07-24 軌跡保存 End

    /**
     * GPS state
     */
    private Button            btnGps;

    /**
     *
     */
    private Button            btnVics;

    private TextView          angleStatusView;
    private TextView          moveStatusView;
    private TextView          curveStatusView;

    /**
     * NaviRun instance.
     */
    private NaviRun           mNaviRun;

    private MotionEvent       mLastTouchEvent;

    /**
     * 交差点拡大図
     */
    private boolean           mJunctionVisible   = false;
    private int[]             mJunctionFbo       = new int[1];
    private int[]             mJunctionTexture   = new int[1];
    private int[]             mJunctionDepth     = new int[1];

    /** show cur map center address */
//Chg 2011/09/17 Z01yamaguchi Start -->
//	private Button mapCentreNameLand;
//	private Button mapCentreNamePort;
//--------------------------------------------
    private LinearLayout      mapCentreNameLand;
    private LinearLayout      mapCentreNamePort;
    private TextView          mapCentreNameTextLand;
    private TextView          mapCentreNameTextPort;
    private ImageView         mapShowDriConDtlImgLand;
    private ImageView         mapShowDriConDtlImgPort;

    private long              lnDriConPoiId;
    private boolean           isMapCentreNameClickable;

//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応 START
//    private ListView          mapCentreListViewLand;
//    private ListView          mapCentreListViewPort;
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応 START

//Chg 2011/09/17 Z01yamaguchi End <--

    /**
     * land mode time icon.
     */
//Del 2011/07/12 Z01thedoanh Start -->時間非表示
    //private TextView curTime_land;
//Del 2011/07/12 Z01thedoanh End <--

    /**
     * port mode time icon.
     */
//Del 2011/07/22 Z01thedoanh Start -->
    //private TextView curTime_port;
//Del 2011/07/22 Z01thedoanh End <--

//Add 2011/07/25 Z01thedoanh (自由解像度対応: Copy or Modify from MarketV1) Start -->
    // HVGA解像度
    public static final int   HVGA_WIDTH         = 480;
    public static final int   HVGA_HEIGHT        = 320;

    public static final float DEFAULT_DPI        = 240;

    public static final float FOV_OFFSET         = 1.5f;                      //defined Native

//    public static final long  EXTERNAL_POI_INDEX = -2L;
//    public static final long  FAVORITE_POI_INDEX = -1L;

//Add 2011/07/25 Z01thedoanh (自由解像度対応: Copy or Modify from MarketV1) End <--
    private MapListener       m_oMapListener;

    /** 地図中心アイコン */
    private ImageView         imgMapCenter;

    /** No Town Map info */
    private ImageView         imgDetailedMessage;

    /**
     * モード
     */
    private NaviStateIcon     naviStateIcon=null;

    /**
     * 方位表示
     */
    private Button            compass;
    private Bitmap            imgCompass;
    private Bitmap            imgCompass_on;
    private Bitmap            imgHeadupCompass;
    private Bitmap            imgHeadupCompass_on;

    private boolean           isTouch;
    private boolean           isHeadup;

    private JNITwoLong        coordinate         = new JNITwoLong();
    private JNIString         addressString      = new JNIString();

    private String            msPointString      = Constants.MAP_POINT_NAME;
    private long              Longitude          = 0;
    private long              Latitude           = 0;

    boolean                   bIsDrag            = false;

    boolean                   bIsTouched         = false;

    boolean                   myPosTouching      = false;
    /** 地図のスクロール判定 */
    private boolean           isMapScrolled      = false;

    /**
     * 地図のモード
     */
    private int               map_view_type      = Constants.COMMON_MAP_VIEW;

    /**
     * 切り替える前に、地図のモード
     */
    private int               pre_map_view_type  = map_view_type;

    /**
     * タップ判定のマージン
     */
    private static final int  DEF_CT_CLICKMARGIN = 32;

    /**
     * 地図スクロールのカウンタ
     */
    private int               iTime              = 0;
    /**
     * 地図スクローモード
     */
    final static int          NUI_MODE           = 11;

    private RelativeLayout   mWidgetParent;
    private Button           mTopWidget;
    private Button           mRightWidget;

//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応 START
//	Map<Long, ListItems> oMapPOIShowList = new TreeMap<Long, ListItems>();
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応  END

// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ピッチイン/アウト・回転・ムーブ禁止 Start -->
	/**
	 * 走行規制中の画面タッチフラグ
	 */
	private boolean bIsTouchedDR = false;
// ADD 2013.08.08 M.Honma 走行規制 ピッチイン/アウト・回転・ムーブ禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

    /**
     * メッセージ消去のtimer
     */
//Add 2011/10/16 Z01YUMISAKI Start -->
    private Timer             timerMessage       = null;
//Add 2011/10/16 Z01YUMISAKI End <--
    Icon mIcon = null;

	enum ContentType {
        CONTENT_NONE,
        CONTENT_STRING_ONLY,
        CONTENT_DRIVE_CONTENT,
        CONTENT_USER_FIGURE
    };
    private ContentType mContentType = ContentType.CONTENT_NONE;

//// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
//    static class ViewHolder {
//    	TextView PoiName;
//    	ImageView DetailIcon;
//    };
//// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END

//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応 START
//    private class ListItems {
//    	private long lPoiId = -1;
//    	private String oItemName = "";
//    	private boolean bItemNameVisible = false;
//    	private boolean bIconVisible = false;
//    	private ContentType eType = ContentType.CONTENT_NONE;
//
//    	ListItems(){ lPoiId = -1; oItemName = ""; bItemNameVisible = false; bIconVisible = false; eType = ContentType.CONTENT_NONE; };
//    	ListItems( long Poi, String ItemName, boolean NameVisible, boolean IconVisible, ContentType Type ){
//    		lPoiId = Poi;
//    		oItemName = ItemName;
//    		bItemNameVisible = NameVisible;
//    		bIconVisible = IconVisible;
//    		eType = Type;
//    	};
//    	public void setPoiId(long Poi) {
//    		this.lPoiId = Poi;
//    	}
//    	public long getPoiId() {
//    		return lPoiId;
//    	}
//    	public void setName(String ItemName) {
//    		this.oItemName = ItemName;
//    	}
//    	public String getName() {
//    		return oItemName;
//    	}
//    	public void setNameVisible(boolean NameVisible) {
//    		this.bItemNameVisible = NameVisible;
//    	}
//    	public int getNameVisible() {
//    		return bItemNameVisible ? View.VISIBLE : View.GONE;
//    	}
//    	public void setIconVisible(boolean IconVisible) {
//    		this.bIconVisible = IconVisible;
//    	}
//    	public int getIconVisible() {
//    		return bIconVisible ? View.VISIBLE : View.GONE;
//    	}
//    	public void setContentType(ContentType Type) {
//    		this.eType = Type;
//    	}
//    	public ContentType getContentType() {
//    		return eType;
//    	}
//    }
//// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
//    private class ListAdapter extends ArrayAdapter<ListItems>{
//    	private LayoutInflater mInflater;
//    	private ViewHolder holder;
//
//    	public ListAdapter(Context context, List<ListItems> objects) {
//    		super(context, 0, objects);
//    		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    	}
//
//    	public View getView(final int position, View convertView, ViewGroup parent) {
//    		View view = convertView;
//        	TextView PoiName;
//        	ImageView DetailIcon;
//
//        	if( view == null ){
//	    		if ( ScreenAdapter.isLandMode()) {
//	   				view = mInflater.inflate(R.layout.map_cur_pos_list_land, null);
//	   				PoiName = (TextView)view.findViewById(R.id.cur_pos_name_text_land);
//	   				DetailIcon = (ImageView)view.findViewById(R.id.cur_pos_name_img_land);
//	    		}
//	    		else{
//	   				view = mInflater.inflate(R.layout.map_cur_pos_list_port, null);
//	   				PoiName = (TextView)view.findViewById(R.id.cur_pos_name_text_port);
//	   				DetailIcon = (ImageView)view.findViewById(R.id.cur_pos_name_img_port);
//	    		}
//
//				PoiName.setFocusableInTouchMode(false);
//				PoiName.setFocusable(false);
//				DetailIcon.setFocusableInTouchMode(false);
//				DetailIcon.setFocusable(false);
//
//	    		holder = new ViewHolder();
//    			holder.PoiName = PoiName;
//    			holder.DetailIcon = DetailIcon;
//    			view.setTag( holder );
//    		}
//        	else{
//        		holder = (ViewHolder)view.getTag();
//        	}
//
//			final ListItems item = this.getItem(position);
//
//			if(item != null){
//				holder.PoiName.setText(item.getName());
//
//				holder.PoiName.setVisibility(item.getNameVisible());
//				holder.DetailIcon.setVisibility(item.getIconVisible());
//
//				holder.PoiName.setOnClickListener(new OnClickListener() {
//    				public void onClick(View v) {
//// ADD 2013.08.08 M.Honma 走行規制 POI禁止 Start -->
//                    	if(DrivingRegulation.CheckDrivingRegulation()){
//                    		return;
//                    	}
//// ADD 2013.08.08 M.Honma 走行規制 POI禁止 End <--
//    					if( item.getIconVisible() == View.VISIBLE ){
//	    					if( item.getContentType() == ContentType.CONTENT_DRIVE_CONTENT ){
//	    						showDCDetailInfo( item.getPoiId() );
//	    					}
//	    					else if( item.getContentType() == ContentType.CONTENT_USER_FIGURE){
//	    						openCenterContent();
//	    					}
//	    					else{
//	    						NaviLog.d(NaviLog.PRINT_LOG_TAG,"mPoiName ContentType is " + item.getContentType());
//	    					}
//	    				}
//    					else{
//    						NaviLog.d(NaviLog.PRINT_LOG_TAG,"mPoiName inVisible");
//    					}
//    				}
//    			});
//				holder.DetailIcon.setOnClickListener(new OnClickListener() {
//    				public void onClick(View v) {
//// ADD 2013.08.08 M.Honma 走行規制 POI禁止 Start -->
//                    	if(DrivingRegulation.CheckDrivingRegulation()){
//                    		return;
//                    	}
//// ADD 2013.08.08 M.Honma 走行規制 POI禁止 End <--
//    					if( item.getIconVisible() == View.VISIBLE ){
//	    					if( item.getContentType() == ContentType.CONTENT_DRIVE_CONTENT ){
//	    						showDCDetailInfo( item.getPoiId() );
//	    					}
//	    					else if( item.getContentType() == ContentType.CONTENT_USER_FIGURE){
//	    						openCenterContent();
//	    					}
//	    					else{
//	    						NaviLog.d(NaviLog.PRINT_LOG_TAG,"mDetailIcon ContentType is " + item.getContentType());
//	    					}
//	    				}
//    					else{
//    						NaviLog.d(NaviLog.PRINT_LOG_TAG,"mDetailIcon inVisible");
//    					}
//    				}
//    			});
//    		}
//
//    		return view;
//    	}
//    }
//// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応  END

    /**
     * 地図スクロールのメッセージの処理ハンドラ
     */
    private Handler           handler            = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    break;
                case 2:
                    if (iTime > 2) {
                        // Stop Smooth Scroll
                        if (!CommonLib.IsShowDoubleMap()) {
                            if (isMapScrolled) {
                                NaviRun.GetNaviRunObj().setSearchKind(Constants.SCROLL_MAP_KIND);
                            }
                            NaviRun.GetNaviRunObj().JNI_CT_UIC_StopSmoothScroll();
                            onMapScrollStop();
                        }
                    }
                    else {
                        NaviRun.GetNaviRunObj().setSearchKind(Constants.SCROLL_MAP_KIND);
                    }
                    iTime = 0;
                    JNITwoLong mapCentre = new JNITwoLong();
                    NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(mapCentre);
                    mapActivity.updateWeather(mapCentre.getM_lLong(), mapCentre.getM_lLat());
                    bIsTouched = true;
                    break;
                case 3:
                    if (iTime > 1) {
                        // Hide detail message
//Chg 2011/10/16 Z01YUMISAKI Start -->
//                        timer.cancel();
//------------------------------------------------------------------------------------------------------------------------------------
                        if (timerMessage != null) {
                            timerMessage.cancel();
                            timerMessage = null;
                        }
//Chg 2011/10/16 Z01YUMISAKI End <--
                        imgDetailedMessage.setVisibility(View.GONE);
                        imgDetailedMessage.setVisibility(View.INVISIBLE);
                        iTime = 0;
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };

    private MapView(Context context) {
        super(context);

        try {
            this.mapActivity = (MapActivity)context;
// Add by CPJsunagawa '2015-08-06 Start
            DTActivity = (DrawTrackActivity)context;
// Add by CPJsunagawa '2015-08-06 End
        } catch (Exception e) {
        }

//        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
//       // Log.d("test", "density=" + metrics.density);
//       // Log.d("test", "densityDpi=" + metrics.densityDpi);
//       // Log.d("test", "scaledDensity=" + metrics.scaledDensity);
//       // Log.d("test", "widthPixels=" + metrics.widthPixels);
//       // Log.d("test", "heightPixels=" + metrics.heightPixels);
//       // Log.d("test", "xDpi=" + metrics.xdpi);
//       // Log.d("test", "yDpi=" + metrics.ydpi);

        //init view
        inflater = LayoutInflater.from(context);
        RelativeLayout m_mainLayout = (RelativeLayout)inflater.inflate(R.layout.map_main, null);

        super.addView(m_mainLayout);

        //MapView
// Add by CPJsunagawa '13-12-25 Start
        mSurfaceView = createMapGLSurfaceView();
// Add by CPJsunagawa '13-12-25 End

        mSurfaceHolder = (ViewGroup) m_mainLayout.findViewById(R.id.map_view_holder);

// DEL.2013.11.22 N.Sasao 起動速度改善対応 START
		// この処理はonResumeで行うため行わないようにする
//        mSurfaceView = createMapGLSurfaceView();
//        mSurfaceView.setOnTouchListener(this);
//		  mSurfaceHolder.addView(mSurfaceView);
// DEL.2013.11.22 N.Sasao 起動速度改善対応  END

// Add by CPJsunagawa '13-12-25 Start
        mSurfaceView.setOnTouchListener(this);
        mSurfaceHolder.addView(mSurfaceView);
// Add by CPJsunagawa '13-12-25 End

        //
        m_oMainLayout = (RelativeLayout)m_mainLayout.findViewById(R.id.main_view);

//// MOD.2013.06.12 N.Sasao POI情報リスト表示対応 START
//        m_oCurPosInfo = (LinearLayout)m_mainLayout.findViewById(R.id.cur_pos_info);
//        m_oCurPosInfoPort = (LinearLayout)m_mainLayout.findViewById(R.id.cur_pos_info_port);

        //ExView Reserved
        m_oExView = (RelativeLayout)m_oMainLayout.findViewById(R.id.map_ex_view);

        //TopView Layout
        m_oMainView = (RelativeLayout)m_oMainLayout.findViewById(R.id.map_top_view);

        imgDetailedMessage = (ImageView)m_mainLayout.findViewById(R.id.map_no_detail);
        imgMapCenter = (ImageView)m_mainLayout.findViewById(R.id.map_center);

//Chg 2011/09/17 Z01yamaguchi Start -->
//		mapCentreNameLand = (Button) m_mainLayout.findViewById(R.id.cur_pos_name_land);
//		mapCentreNamePort = (Button) m_mainLayout.findViewById(R.id.cur_pos_name_port);
//--------------------------------------------
        mapCentreNameLand = (LinearLayout)m_oMainLayout.findViewById(R.id.cur_pos_name_land);
        mapCentreNamePort = (LinearLayout)m_oMainLayout.findViewById(R.id.cur_pos_name_port);
        mapCentreNameTextLand = (TextView)mapCentreNameLand.findViewById(R.id.cur_pos_name_text_land);
        mapCentreNameTextPort = (TextView)mapCentreNamePort.findViewById(R.id.cur_pos_name_text_port);
        mapShowDriConDtlImgLand = (ImageView)mapCentreNameLand.findViewById(R.id.cur_pos_name_img_land);
        mapShowDriConDtlImgPort = (ImageView)mapCentreNamePort.findViewById(R.id.cur_pos_name_img_port);
//Chg 2011/09/17 Z01yamaguchi End <--

        mapCentreNameLand.setVisibility(View.INVISIBLE);

//        mapCentreListViewLand = (ListView)m_oCurPosInfo.findViewById(R.id.cur_pos_info_list_land);
//
//        mapCentreListViewPort = (ListView)m_oCurPosInfoPort.findViewById(R.id.cur_pos_info_list_port);
//
//        mapCentreListViewLand.setScrollingCacheEnabled(false);
//        mapCentreListViewPort.setScrollingCacheEnabled(false);
//        mapCentreListViewLand.setVisibility(View.INVISIBLE);

//Add 2011/09/20 Z01yamaguchi Start -->
        // ドライブコンテンツ名称クリック時の動作
        OnClickListener onClickMapCentreName = new OnClickListener() {
            @Override
            public void onClick(View v) {
// ADD.2014.01.10 N.Sasao マルチ車載器連携地盤作成 START
            	if(DrivingRegulation.CheckDrivingRegulation()){
            		return;
            	}
// ADD.2014.01.10 N.Sasao マルチ車載器連携地盤作成  END
//                showDCDetailInfo();
                openCenterContent();
            }
        };

//        mapCentreNameLand.setOnClickListener(onClickMapCentreName);
//        mapCentreNamePort.setOnClickListener(onClickMapCentreName);
        mapShowDriConDtlImgLand.setOnClickListener(onClickMapCentreName);
        mapShowDriConDtlImgPort.setOnClickListener(onClickMapCentreName);
////Add 2011/09/20 Z01yamaguchi End <--
//// MOD.2013.06.12 N.Sasao POI情報リスト表示対応  END

//Del 2011/07/22 Z01thedoanh Start -->時間表示なし
        //curTime_land = (TextView) m_mainLayout.findViewById(R.id.curTime_land);
        //curTime_port = (TextView) m_mainLayout.findViewById(R.id.curTime_port);
//Del 2011/07/22 Z01thedoanh End <--

        initMapStateView(m_oMainLayout.findViewById(R.id.map_left));

        //init check screen size
        onSizeChange();

        mWidgetParent = m_mainLayout;
        mTopWidget = (Button)m_mainLayout.findViewById(R.id.top_dummy);
        mRightWidget = (Button)m_mainLayout.findViewById(R.id.right_dummy);
    }

    /**
     * hide View when map is scrolling.
     */
    protected void hideViewOnMapScroll() {
        if (m_oMainLayout != null) {
            m_oMainLayout.setVisibility(View.INVISIBLE);
        }
    }

	/**
	 * 地図のGLSurfaceViewを生成します。
	 *
	 * @return
	 */
	private GL2SurfaceView createMapGLSurfaceView() {
		GL2SurfaceView mapGLSurfaceView = new GL2SurfaceView(getContext());
//        mapGLSurfaceView.setLayoutParams(
 //       		new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        mMapViewRenderer = new MapViewRenderer();
        mMapViewRenderer.setAngle(0);
        mapGLSurfaceView.setRenderer(mMapViewRenderer);
        mapGLSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

//        mapGLSurfaceView.setOnTouchListener(mMapTouchListener);

        return mapGLSurfaceView;
	}

    /**
     * show View after map scrolled.
     */
    protected void showViewOnMapStop() {
        if (m_oMainLayout != null) {
            m_oMainLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Update mapEvent Handler
     *
     * @param activity
     *            MapEvent Hander.
     */
    private void updateMapEvent(MapActivity activity) {

        if (m_oTopView != null) {
            m_oTopView.setMapEvent(activity);
        }
//Del 2011/11/11 Z01_h_yamada Start -->
//        // XuYang add start #805
//        if (oBattery != null) {
//            if (this.mapActivity != activity) {
//                oBattery.release();
//                oBattery.update(activity);
//            }
//        }
//        if (imgSignal != null) {
//            if (this.mapActivity != activity) {
//                imgSignal.release();
//                imgSignal.update(activity);
//            }
//        }
//        // XuYang add end #805
//Del 2011/11/11 Z01_h_yamada End <--
        this.mapActivity = activity;
        mSurfaceView.requestRender();
    }

    /**
     * Get compass state
     *
     * @param void
     */
    private void GetCompassState() {

        isHeadup = mapActivity.isHeadupCompass();

    }

    /**
     * init View which layout parent left.
     *
     * @param oLayout
     */
    private void initMapStateView(View oLayout) {
        if (oLayout == null) {
            return;
        }
//Add 2011/11/22 Z01_h_yamada Start -->
        if (Constants.isDebug) {
//Add 2011/11/22 Z01_h_yamada End <--
            naviStateIcon = (NaviStateIcon)oLayout.findViewById(R.id.mode);

//Del 2011/06/28 Z01thedoanh Start --> 「車モード」・「歩行者モード」アイコンをクリックするとルート探索を行う機能を削除
//	    if(naviStateIcon != null){
//	        naviStateIcon.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (CommonLib.getNaviMode() != Constants.NE_NAVIMODE_BICYCLE) {
//                        if(isMapScrolled){
//                            mapActivity.calcRoute();
//                        }
//                    }
//                }
//            });
//	    }
//Del 2011/06/28 Z01thedoanh End <--
//Add 2011/09/16 Z01kkubo Start --> 「車モード」「歩行者モード」アイコン長押しで自車位置移動(デバッグ用)
            if (naviStateIcon != null) {
                naviStateIcon.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        // 自車位置の移動処理を呼ぶ
                        mapActivity.moveVehicle();
                        return true;
                    }
                });
            }
//Add 2011/09/16 Z01kkubo End <--
//Add 2011/11/22 Z01_h_yamada Start -->
        }
//Add 2011/11/22 Z01_h_yamada End <--

//Add 2011/09/28 Z01_h_yamada Start -->
        mLeftInfoLayout = (LinearLayout)oLayout.findViewById(R.id.mapleft_layout);
        if (mLeftInfoLayout != null) {
            // タッチ無反応領域
            mLeftInfoLayout.setOnTouchListener(this);
        }
//Add 2011/09/28 Z01_h_yamada End <--

        imgCompass = BitmapFactory.decodeResource(getResources(), R.drawable.compass_off);
        imgCompass_on = BitmapFactory.decodeResource(getResources(), R.drawable.compass_on);
        imgHeadupCompass = BitmapFactory.decodeResource(getResources(), R.drawable.compass_headup_off);
        imgHeadupCompass_on = BitmapFactory.decodeResource(getResources(), R.drawable.compass_headup_on);

        compass = (Button)oLayout.findViewById(R.id.compass);
        compass.setBackgroundDrawable(new BitmapDrawable() {
            @Override
            public void draw(Canvas canvas) {

                Bitmap bitmap = imgCompass;
                GetCompassState();

                if (isHeadup) {
                    if (isTouch) {
                        bitmap = imgHeadupCompass_on;
                    } else {
                        bitmap = imgHeadupCompass;
                    }
                } else {
                    if (isTouch) {
                        bitmap = imgCompass_on;
                    } else {
                        bitmap = imgCompass;
                    }
                }

                if (bitmap != null) {
//Chg 2011/12/14 Z01_h_yamada Start -->
////Chg 2011/10/19 Z01_h_yamada Start -->
////                	//Chg 2011/07/27 Z01thedoanh Start -->bitmap画像サイズ変更
//////                  int w = bitmap.getWidth();
//////                  int h = bitmap.getHeight();
////                  int newHeight = compass.getHeight(); //ボタンサイズ取得
////                  int newWidth = compass.getWidth(); //ボタンサイズ取得
////
////                  bitmap = CommonLib.ReSizeBmpImage(newHeight, newWidth, bitmap);
////                  if (CommonLib.angle % 360 == 0) {
////                      canvas.drawBitmap(bitmap, 0, 0, null);
////                  } else {
////                      //回転
////                      Matrix matrix = new Matrix();
////                      matrix.postRotate(CommonLib.angle);
////                      Bitmap b1 = Bitmap.createBitmap(bitmap, 0, 0, newWidth, newHeight, matrix, true);
////                      Bitmap b2 = Bitmap.createBitmap(b1, (b1.getWidth() - newWidth)/2, (b1.getHeight() - newHeight)/2, newWidth, newHeight);
////                      canvas.drawBitmap(b2, 0, 0, null);
//////Chg 2011/06/08 sawada Start -->
//////                    matrix.postRotate(CommonLib.angle, w / 2f, h / 2f);
//////                    canvas.drawBitmap(bitmap, matrix, null);
//////                      matrix.postRotate(CommonLib.angle);
//////                      Bitmap b1 = Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
//////                      Bitmap b2 = Bitmap.createBitmap(b1, (b1.getWidth() - w) / 2, (b1.getHeight() - h) / 2, w, h);
//////                      canvas.drawBitmap(b2, 0, 0, null);
//////Chg 2011/06/08 sawada End   <--
//////Chg 2011/07/27 Z01thedoanh End <--
////                  }
////--------------------------------------------
//                    int newHeight = compass.getHeight()-1; //コンパスサイズ取得
//                    int newWidth  = compass.getWidth()-1;  //  -1 は回転すると、1px増えて欠けてしまう場合があるので、その調整
//
//                    bitmap = CommonLib.ReSizeBmpImage(newHeight, newWidth, bitmap);
//                    if (CommonLib.angle % 360 == 0) {
//                        canvas.drawBitmap(bitmap, 0, 0, null);
//                    } else {
//                        //回転
//                        Matrix matrix = new Matrix();
//                        matrix.postRotate(CommonLib.angle);
//                        Bitmap b1 = Bitmap.createBitmap(bitmap, 0, 0, newWidth, newHeight, matrix, true);
//
//                        int x = (b1.getWidth() - newWidth) / 2;
//                        int y = (b1.getHeight() - newHeight) / 2;
//                        int w = newWidth+1;
//                        int h = newHeight+1;
//
//                        if (w >= b1.getWidth()) {
//                        	w = b1.getWidth();
//                        	x = 0;
//                        }
//                        if (h >= b1.getHeight()) {
//                        	h = b1.getHeight();
//                        	y = 0;
//                        }
//                        if (x + w > b1.getWidth()) {
//                        	x = b1.getWidth() -w;
//                        }
//                        if (x < 0) {
//                        	x = 0;
//                        }
//                        if (y + h > b1.getHeight()) {
//                        	y = b1.getHeight() -h;
//                        }
//                        if (y < 0) {
//                        	y = 0;
//                        }
//                        Bitmap b2 = Bitmap.createBitmap(b1, x, y, w, h);
//                    	canvas.drawBitmap(b2, 0, 0, null);
//                    }
////Chg 2011/10/19 Z01_h_yamada End <--
//--------------------------------------------

                    //  サイズの -1 指定は回転すると、1px増えて欠けてしまう場合があるので、その調整
                    Bitmap dstBitmap = CommonLib.rotateCommpas(bitmap, compass.getWidth() - 1, compass.getHeight() - 1, CommonLib.angle);
                    canvas.drawBitmap(dstBitmap, 0, 0, null);
//Chg 2011/12/14 Z01_h_yamada End <--
                }
            }
        });

        compass.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //NaviLog.d(NaviLog.PRINT_LOG_TAG,"----------->> onTouch Compass");//

                // 自動車・歩行者のモードを取得
                if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN) {
                    ZNUI_UI_MARK_DATA markData = new ZNUI_UI_MARK_DATA();
                    NaviRun.GetNaviRunObj().JNI_Java_GetDestinationGuide(markData);
                    ZNUI_ANGLE_DATA stAngle = markData.getAngle();
                    if (MotionEvent.ACTION_DOWN == event.getAction()) {
                        setPointAngle(null, true, stAngle);
                    } else if (MotionEvent.ACTION_UP == event.getAction()) {
                        setPointAngle(null, false, stAngle);
                        azimuthSwitch();
                    }
                } else {
                    Java_IconInfo iconInfo = new Java_IconInfo();
                    NaviRun.GetNaviRunObj().JNI_Java_GetCompass(iconInfo);
                    // 方位マークのビットマップを回転
                    if (MotionEvent.ACTION_DOWN == event.getAction()) {
                        setPointAngle(iconInfo, true, null);
                    } else if (MotionEvent.ACTION_UP == event.getAction()) {
                        setPointAngle(iconInfo, false, null);
                        azimuthSwitch();
                    }
                }
                return true;
            }
        });
/*
        compass.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                azimuthSwitch();

             // 自動車・歩行者のモードを取得
              if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN) {
                  ZNUI_UI_MARK_DATA markData = new ZNUI_UI_MARK_DATA();
                  NaviRun.GetNaviRunObj().JNI_Java_GetDestinationGuide(markData);
                  ZNUI_ANGLE_DATA stAngle = markData.getAngle();
                  setPointAngle(null, false, stAngle);
              } else {
                  Java_IconInfo iconInfo = new Java_IconInfo();
                  NaviRun.GetNaviRunObj().JNI_Java_GetCompass(iconInfo);
                  // 方位マークのビットマップを回転
                  setPointAngle(iconInfo, false, null);
              }
            }
        });
*/

        btnZoomIn = (Button)oLayout.findViewById(R.id.zoom_in);
        btnZoomIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                zoomIn();
            }
        });

        btnZoomOut = (Button)oLayout.findViewById(R.id.zoom_out);
        btnZoomOut.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                zoomOut();
            }
        });

        btnScale = (Button)oLayout.findViewById(R.id.scale_txt);
        btnGps = (Button)oLayout.findViewById(R.id.gps);
        btnVics = (Button)oLayout.findViewById(R.id.vics);
        if (btnVics != null) {
            btnVics.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
//Add 2011/10/01 Z01yamaguchi - VICS案内実装  Start -->
                    VicsManager vicsManager = VicsManager.getInstance();
                    if (vicsManager != null) {
                        vicsManager.requestVics();
                    }
                    NaviRun.GetNaviRunObj().JNI_NE_VICS_ReloadVicsFile();
//Add 2011/10/01 Z01yamaguchi - VICS案内実装  End <--
                }
            });
        }
        updateVicsIcon();

// Add by CPJsunagawa '2015-07-24 軌跡保存機能 Start
        btnTrackSaveStart = (Button)oLayout.findViewById(R.id.btn_TrackSaveStart);

        btnTrackSaveStart.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

            	// 保存するコース名の入力を促す
                //テキスト入力を受け付けるビューを作成します。
                final EditText editView = new EditText(mapActivity);
            	AlertDialog.Builder alertDlg = new AlertDialog.Builder(mapActivity);  // 2016/02/12 Yamamoto Del
            	alertDlg.setIcon(android.R.drawable.ic_dialog_info);
                alertDlg.setTitle("保存する軌跡データ名を入力してください");
                //setViewにてビューを設定します。
                alertDlg.setView(editView);
                alertDlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        System.out.println("DrawTrackActivity ExecuteTrackSaveCSV OK Click");
                        //入力した文字をトースト出力する
                        //Toast.makeText(ctx, editView.getText().toString(), Toast.LENGTH_LONG).show();
                        mCurrentCourse = editView.getText().toString();
                        System.out.println("DrawTrackActivity Dialog Input =>" + mCurrentCourse);

                        if (DrawTrackActivity.checkTrackCSVName(mapActivity, mCurrentCourse)) {  // 2016/02/12 Yamamoto Add

                            btnTrackSaveStart.setBackgroundColor(0xffff0000);
                            btnTrackSaveStart.setTextColor(0xffffffff);

// Add 20160212 Start
                            // ファイルのロックを解除するため、一旦VTプロセスを終わらせる  以下を実行すると、プロセスが応答しなくなる
                            //NaviRun.GetNaviRunObj().JNI_NE_OptionalVTFinalize();

                            // Test　以下を実行しても、軌跡は表示されず
                            NaviRun.GetNaviRunObj().JNI_NE_AgainOpenVehicleTrack();

                            //　###### アプリ起動直後であれば、確実に軌跡保存できる。よって、軌跡表示選択時に、アプリ起動時と ######
                            // ###### 同じ状態にすれば、できるはずである。 ######
// Add 20160212 End

                            // 大本の軌跡データファイルを削除する
                            NaviRun.GetNaviRunObj().JNI_NE_ClearVehicleTrack();

                            // 走行軌跡保存開始
                            NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(1);
//                            NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(1);
// Add 20160212 Start　　これを実行すると、軌跡保存時に軌跡が表示されなくなる
                            // 表示用フラグはOFFにする
//                            NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(0);
// Add 20160212 End

                            NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
                            // Add 20160112
                            //NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();

// Add 20160118 Start
                            // 軌跡保存中は、編集ボタンを無効化にする
                            btnTrackIconEdit.setEnabled(false);
                            System.out.println("MapView btnTrackIconEdit.setEnabled(false)");
// Add 20160118 End
                        }  // 2016/02/12 Yamamoto Add
                    }
                });
                alertDlg.setNegativeButton("キャンセル", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        mCurrentCourse = null;

                        System.out.println("MapView ExecuteTrackSaveCSV Cancel Click");

// Add 20160118 Start
                        // 軌跡保存がキャンセルされたら編集ボタンを有効に戻す
                        btnTrackIconEdit.setEnabled(true);
                        System.out.println("MapView btnTrackIconEdit.setEnabled(true)");
// Add 20160118 End

                        // ボタンを非選択状態にして処理を抜ける
                        btnTrackSaveStart.setBackgroundResource(R.drawable.btn_default);
                        btnTrackSaveStart.setTextColor(0xff000000);

                        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
                        //NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();
                    }
                });
                //alertDlg.show();
            	System.out.println("DrawTrackActivity ExecuteTrackSaveCSV alertDlg.create().show() Before");
                alertDlg.create().show();
            	
            }
        });

        btnTrackSaveEnd = (Button)oLayout.findViewById(R.id.btn_TrackSaveEnd);

        btnTrackSaveEnd.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

//            	Toast.makeText(this, "軌跡保存を開始します", Toast.LENGTH_LONG).show();
            	//DrawTrackActivity DTActivity = new DrawTrackActivity();
            	//int nRet = DTActivity.ExecuteTrackExportCSV(mapActivity);
            	int nRet = DrawTrackActivity.ExecuteTrackExportCSV(mapActivity, mCurrentCourse);
            	
                btnTrackSaveStart.setBackgroundResource(R.drawable.btn_default);
                btnTrackSaveStart.setTextColor(0xff000000);

// Add 20160118 Start
                // 軌跡保存が終了したら編集ボタンを有効に戻す
                btnTrackIconEdit.setEnabled(true);
            	System.out.println("MapView btnTrackIconEdit.setEnabled(true)");
//Add 20160118 End

            }
        });

        btnTrackIconEdit = (Button)oLayout.findViewById(R.id.btn_TrackIconEdit);

        btnTrackIconEdit.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
            	
            	int mSelectedIndex = 0;
                View mSelectedView = null;
            	
        		System.out.println("**** MapViewのTrackIconEdit onClick() Start ****");
        		
            	// 追加画面へ遷移する
                Intent intentAdd = new Intent();
                intentAdd.setClass(mapActivity, TrackIconEdit.class);
                NaviActivityStarter.startActivityForResult(mapActivity, intentAdd, AppInfo.ID_ACTIVITY_TRACK_ICON_EDIT);

            }
        });
// Add by CPJsunagawa '2015-07-24 軌跡保存機能 End

//Del 2011/11/11 Z01_h_yamada Start -->
//        // XuYang add start #805
//        oBattery = (BatteryIcon)oLayout.findViewById(R.id.imgBattery);
//        imgSignal = (SignalIcon)oLayout.findViewById(R.id.imgSignal);
//        // XuYang add end #805
//Del 2011/11/11 Z01_h_yamada End <--

        if (CommonLib.isDebuggable(mapActivity)) {
            angleStatusView = (TextView)oLayout.findViewById(R.id.angle_status);
            angleStatusView.setVisibility(View.VISIBLE);
            moveStatusView = (TextView)oLayout.findViewById(R.id.move_status);
            moveStatusView.setVisibility(View.VISIBLE);
            curveStatusView = (TextView)oLayout.findViewById(R.id.curve_status);
            curveStatusView.setVisibility(View.VISIBLE);
        }
    }

// Add 20160116 Start
	public void chgTrackStartButtonColor(int flag) {

 			if(flag == 1) {
 				btnTrackSaveStart.setBackgroundResource(R.drawable.btn_default);
 	            btnTrackSaveStart.setTextColor(0xff000000);
 	        	return;
 	        } else {
 	            btnTrackSaveStart.setBackgroundColor(0xffff0000);
 	            btnTrackSaveStart.setTextColor(0xffffffff);
 			}
     }
// Add 20160116 End

    public void setAngleStatus(String status) {
        if (angleStatusView != null) {
            angleStatusView.setText(status);
        }
    }

    public void setMoveStatus(String status) {
        if (moveStatusView != null) {
            moveStatusView.setText(status);
        }
    }

    public void setCurveStatus(String status) {
        if (curveStatusView != null) {
            curveStatusView.setText(status);
        }
    }

    /**
     * init funcation.<br>
     * this funcation must be called before MapView use.
     *
     * @param context
     *            Context Object
     */
    public static void init(Context context) {
        if (theObj == null) {
            theObj = new MapView(context);
        }
    }

    /**
     * get the instance of MapView.
     *
     * @return the instance of MapView
     */
    public static final MapView getInstance() {
        return theObj;
    }

    /**
     * Get the instance of MapTopView
     *
     * @return the instance of MapTopView
     */
    public final MapTopView getMapTopView() {
        return m_oTopView;
    }

    /**
     * Set the instance of MapTopView
     *
     * @param mapTopView
     *            the instance of MapTopView
     */
    public final void setMapTopView(MapTopView mapTopView) {
        m_oTopView = mapTopView;
    }

    /**
     * Add View to the Top layout.
     */
    public void addView(View child) {
        if (m_oMainView != null) {
            m_oMainView.addView(child);
        }
    }

    /**
     * add View to the top layout with the layoutParams.
     */
    public void addView(View child, RelativeLayout.LayoutParams params) {
        if (m_oMainView != null) {
            this.m_oMainView.addView(child, params);
        }
    }

    /**
     * delete view from top layout.
     */
    public void removeView(View child) {
        if (m_oMainView != null) {
            this.m_oMainView.removeView(child);
        }
    }

    /**
     * add View to the ex layout.
     */
    public void addExView(View child) {
        if (m_oExView != null) {
            this.m_oExView.addView(child);
        }
    }

    /**
     * add view to the Ex layout with the layoutParams.
     */
    public void addExView(View child, RelativeLayout.LayoutParams params) {
        if (m_oExView != null) {
            this.m_oExView.addView(child, params);
        }
    }

    /**
     * delete View from Ex layout.
     */
    public void removeExView(View child) {
        if (m_oExView != null) {
            this.m_oExView.removeView(child);
        }
    }

    /* (non-Javadoc)
     * @see android.view.View#onSizeChanged(int, int, int, int)
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (isEnableSizeChange()) {
            onSizeChange();
        }

        onNaviModeChange();
    }

    /**
     * Init NaviRun.
     */
    private void initNaviRun() {
        final long pixelFilter = 10;
        final long jumpNumber = 1;
//Chg 2011/10/12 Z01yoneya Start -->
//        mNaviRun = NaviRun.GetNaviRunObj();
//        mNaviRun.setDisplay(mSurfaceHolder, ScreenAdapter.getWidth(), ScreenAdapter.getHeight());
//        mNaviRun.JNI_NE_SetVehicleTrackParam(pixelFilter, jumpNumber);
////Chg 2011/07/25 Z01thedoanh (自由解像度対応: Copy or Modify from MarketV1) Start -->
//        Rect rect = new Rect();
//        getWindowVisibleDisplayFrame(rect);
//        //FullScreenではない場合は指定したサイズ設定
//        rect.right = ScreenAdapter.getWidth();
//        rect.bottom = ScreenAdapter.getHeight();
//        ScreenAdapter.setStatusBarHeight(rect.top);
//        int iWidth = ScreenAdapter.getWidth();
//        int iHeight = ScreenAdapter.getHeight() - ScreenAdapter.getStatusBarHeight();
//        int nGuideAreaSize = (int)(iHeight * Constants.GUIDE_AREA_ASPECT1); //画面２
//        //
//        //int     nMapFrontWide = (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_FrontWide)+0.5f);
//        //自己位置４：１にする
//        int nMapFrontWide = iHeight / 4;
//        int nGuideMapSize = iWidth - nGuideAreaSize;//画面１
//        // 案内情報の座標値取得
//        // left
//        int dispBrdLeft = (int)(getResources().getDimensionPixelOffset(R.dimen.Navi_Guide_DisplayBoard_mleft) + 0.5f);
//        // top
//        Bitmap guideBackBmp = BitmapFactory.decodeResource(getResources(), R.drawable.guide_back_ex);
//        float scale = (float)guideBackBmp.getHeight() / iHeight;
//        CommonLib.setGuideBackGroundScale(scale);
//        int dispBrdTop = (int)(Constants.GUIDE_CROSS_ZOOM_HEIGHT_OFFSET / scale) + 5;
//
//        // right
//        int dispBrdRight = nGuideAreaSize - (int)(getResources().getDimensionPixelOffset(R.dimen.Navi_Guide_DisplayBoard_mright) + 0.5f);
//        // bottom
//        int dispBrdBottom = ScreenAdapter.getHeight();
//        dispBrdLeft = 10;
//        dispBrdRight = nGuideAreaSize - 10;
//        CommonLib.setGuideAreaWidth(nGuideAreaSize);
//        CommonLib.setGuideAreaMarginLeft(dispBrdLeft);
//        CommonLib.setGuideAreaMarginRight(dispBrdRight);
//        CommonLib.setOrientation(ScreenAdapter.getOrientation());
//
//        //********************************
//        // 探索結果地図の表示サイズ算出用
//        //********************************
//        // 探索結果地図の座標値取得
//        // left
//        int routeCalcMapLeft = rect.left + (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mleft) + 0.5f);
//        // top
//        int routeCalcMapTop;
//        if (rect.top == 0) {
//            routeCalcMapTop = (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mtop) + 0.5f);
//        } else {
//            routeCalcMapTop = rect.top;
//        }
//        // right
//        int routeCalcMapRight = rect.right - (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mright) + 0.5f);
//        // bottom
//        int routeCalcMapBottom = (rect.bottom - rect.top) - (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mbottom) + 0.5f);
//------------------------------------------------------------------------------------------------------------------------------------
        int height = ScreenAdapter.getHeight();
        int width = ScreenAdapter.getWidth();
        //現在のナビエンジン初期化は縦画面起動時の場合、画面縦横サイズの整合性がとれていない
        //SCREEN_ONイベントでアプリ起動する場合、縦画面で起動することがある。
        //暫定対応として、縦画面の時は縦横を入れ替えてナビエンジンを初期化する
        if (height > width) {
            height = ScreenAdapter.getWidth();
            width = ScreenAdapter.getHeight();
        }

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            NaviLog.v(NaviLog.PRINT_LOG_TAG, "initialize screen landscape. (" + width + " ," + height + ")");
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            NaviLog.v(NaviLog.PRINT_LOG_TAG, "initialize screen portrait. (" + width + " ," + height + ")");
        }

        mNaviRun = NaviRun.GetNaviRunObj();
        mNaviRun.setDisplay(null, width, height);
        mNaviRun.JNI_NE_SetVehicleTrackParam(pixelFilter, jumpNumber);
//Chg 2011/07/25 Z01thedoanh (自由解像度対応: Copy or Modify from MarketV1) Start -->
        Rect rect = new Rect();
        getWindowVisibleDisplayFrame(rect);
        //FullScreenではない場合は指定したサイズ設定
        rect.right = width;
        rect.bottom = height;
        ScreenAdapter.setStatusBarHeight(rect.top);
        int iWidth = width;
        int iHeight = height - ScreenAdapter.getStatusBarHeight();
        int nGuideAreaSize = (int)(iHeight * Constants.GUIDE_AREA_ASPECT1); //画面２
        //
        //int     nMapFrontWide = (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_FrontWide)+0.5f);
        //自己位置４：１にする
        int nMapFrontWide = iHeight / 4;
        int nGuideMapSize = iWidth - nGuideAreaSize;//画面１
        // 案内情報の座標値取得
        // left
        int dispBrdLeft = (int)(getResources().getDimensionPixelOffset(R.dimen.Navi_Guide_DisplayBoard_mleft) + 0.5f);
        // top
        Bitmap guideBackBmp = BitmapFactory.decodeResource(getResources(), R.drawable.guide_back_ex);
        float scale = (float)guideBackBmp.getHeight() / iHeight;
        CommonLib.setGuideBackGroundScale(scale);
        int dispBrdTop = (int)(Constants.GUIDE_CROSS_ZOOM_HEIGHT_OFFSET / scale) + 5;

        // right
        int dispBrdRight = nGuideAreaSize - (int)(getResources().getDimensionPixelOffset(R.dimen.Navi_Guide_DisplayBoard_mright) + 0.5f);
        // bottom
        int dispBrdBottom = height;
        dispBrdLeft = 0;
        dispBrdRight = nGuideAreaSize;
        CommonLib.setGuideAreaWidth(nGuideAreaSize);
        CommonLib.setGuideAreaMarginLeft(dispBrdLeft);
        CommonLib.setGuideAreaMarginRight(dispBrdRight);
        CommonLib.setOrientation(ScreenAdapter.getOrientation());

        //********************************
        // 探索結果地図の表示サイズ算出用
        //********************************
        // 探索結果地図の座標値取得
        // left
        int routeCalcMapLeft = rect.left + (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mleft) + 0.5f);
        // top
        int routeCalcMapTop;
        if (rect.top == 0) {
            routeCalcMapTop = (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mtop) + 0.5f);
        } else {
            routeCalcMapTop = rect.top;
        }
        // right
        int routeCalcMapRight = rect.right - (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mright) + 0.5f);
        // bottom
        int routeCalcMapBottom = (rect.bottom - rect.top) - (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mbottom) + 0.5f);
//Chg 2011/10/12 Z01yoneya End <--

//Chg 2011/09/27 Z01yoneya Start -->
//        mNaviRun.init(rect.left, rect.top, rect.right, rect.bottom,
//                nGuideMapSize, nMapFrontWide, dispBrdLeft, dispBrdTop, dispBrdRight, dispBrdBottom
//                , isSmallMode(iWidth, dispBrdBottom), routeCalcMapLeft, routeCalcMapTop,
//                routeCalcMapRight, routeCalcMapBottom);
//------------------------------------------------------------------------------------------------------------------------------------

// DEL.2013.07.25 N.Sasao 高解像度HDPI⇒MDPI対応 START
// DEL.2013.07.25 N.Sasao 高解像度HDPI⇒MDPI対応  END
// MOD.2013.07.25 N.Sasao 高解像度HDPI⇒MDPI対応 START
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "rect(" + rect.left + "," + rect.top + "," + rect.right + "," + rect.bottom + ") width=" + iWidth + " height=" + iHeight);
        mNaviRun.init(((NaviApplication)mapActivity.getApplication()), rect.left, rect.top, rect.right, rect.bottom,
                nGuideMapSize, nMapFrontWide, dispBrdLeft, dispBrdTop, dispBrdRight, dispBrdBottom
                , isSmallMode(iWidth, dispBrdBottom), routeCalcMapLeft, routeCalcMapTop,
                routeCalcMapRight, routeCalcMapBottom);
// MOD.2013.07.25 N.Sasao 高解像度HDPI⇒MDPI対応  END
//Chg 2011/09/27 Z01yoneya End <--
//Chg 2011/07/25 Z01thedoanh (自由解像度対応: Copy or Modify from MarketV1) End <--
    }

//Add 2011/08/03 Z01thedoanh (自由解像度対応) Start -->
    public void setAreaShowCalcRoute(Boolean flagLanscape) {
        Rect rect = new Rect();
        int routeCalcMapTop = 0;
        int routeCalcMapLeft = 0;
        int routeCalcMapRight = 0;
        int routeCalcMapBottom = 0;
        getWindowVisibleDisplayFrame(rect);
//	    if(flagLanscape)
        {
            routeCalcMapTop = rect.top;
            routeCalcMapLeft = rect.left + (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mleft) + 0.5f);
            routeCalcMapRight = rect.right - (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mright) + 0.5f);
            routeCalcMapBottom = (rect.bottom - rect.top) - (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mbottom) + 0.5f);
        }
//	    else{
//	    	routeCalcMapTop    = rect.top;
//	        routeCalcMapLeft   = rect.left + (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mleft)+0.5f);
//	        routeCalcMapRight  = rect.right - (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mright)+0.5f);
//	        routeCalcMapBottom = (rect.bottom - rect.top) - (int)(getResources().getDimensionPixelOffset(R.dimen.MM_MAP_RouteCalcMap_mbottom)+0.5f);
//	    }
        NaviRun.GetNaviRunObj().JNI_NE_SetAreaShowCalcRoute(routeCalcMapLeft, routeCalcMapTop, routeCalcMapRight, routeCalcMapBottom,
                rect.left, rect.top, rect.right, rect.bottom);
    }

//Add 2011/08/03 Z01thedoanh (自由解像度対応) End <--

    /**
     * Update Mode Icon when the Mode changed.
     */
    public void onNaviModeChange() {
        if (naviStateIcon != null) {
            Resources resouces = getResources();
            Configuration config = resouces.getConfiguration();

            if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
// Cut 20160120 Start -- Cut Cancel
                naviStateIcon.updateStateIcon();
                naviStateIcon.setVisibility(View.VISIBLE);
// Cut 20160120 End
            } else {
                naviStateIcon.setVisibility(View.INVISIBLE);
            }
        }
//        Log.i("MapView", "onNaviModeChange");
        if (m_oTopView != null) {
            m_oTopView.UpdateNaviModeChange();
        }

        updateVicsIcon();
    }

    private void updateVicsIcon() {
//Chg 2011/10/10 Z01yoneya Start -->
//        //車モード以外では、非表示とする
//        if (btnVics != null) {
////Chg 2011/09/30 Z01yamaguchi Start -->
////            if (!CommonLib.isBIsExistsVicsData()) {
////--------------------------------------------
//            if (!((NaviApplication)mapActivity.getApplication()).getNaviAppDataPath().isExistVicsData()) {
////Chg 2011/09/30 Z01yamaguchi End <--
//
//                //VICSデータが無い時は、VICSアイコンを表示しない
//                btnVics.setVisibility(View.GONE);
////Add 2011/10/01 Z01yamaguchi - VICS案内実装  Start -->
//            } else {
//                JNIInt JIVicsUpdateInterval = new JNIInt();
//                NaviRun.GetNaviRunObj().JNI_NE_GetVicsInfo(JIVicsUpdateInterval);
//                if (JIVicsUpdateInterval.getM_iCommon() == Constants.VICSINFO_NONE) {
//                    //VICS の自動更新を行わないときは、VICSアイコンを表示しない
//                    btnVics.setVisibility(View.GONE);
////Add 2011/10/01 Z01yamaguchi - VICS案内実装  End <--
//                } else {
//                    if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_CAR) {
//                        btnVics.setVisibility(View.VISIBLE);
//                    } else {
//                        btnVics.setVisibility(View.INVISIBLE);
//                    }
//                }
////Add 2011/10/01 Z01yamaguchi - VICS案内実装  Start -->
//            }
////Add 2011/10/01 Z01yamaguchi - VICS案内実装  End <--
//        }
//------------------------------------------------------------------------------------------------------------------------------------
        //車モード以外では、非表示とする
        if (btnVics == null) {
            return;
        }

        try {
//Chg 2011/10/14 Z01_h_yamada Start -->
//            if (!((NaviApplication)mapActivity.getApplication()).getNaviAppDataPath().isExistVicsData()) {
//                //VICSデータが無い時は、VICSアイコンを表示しない
//                btnVics.setVisibility(View.GONE);
////Add 2011/10/01 Z01yamaguchi - VICS案内実装  Start -->
//            } else {
//                JNIInt JIVicsUpdateInterval = new JNIInt();
//                NaviRun.GetNaviRunObj().JNI_NE_GetVicsInfo(JIVicsUpdateInterval);
//                if (JIVicsUpdateInterval.getM_iCommon() == Constants.VICSINFO_NONE) {
//                    //VICS の自動更新を行わないときは、VICSアイコンを表示しない
//                    btnVics.setVisibility(View.GONE);
////Add 2011/10/01 Z01yamaguchi - VICS案内実装  End <--
//                } else {
//                    if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_CAR) {
//                        btnVics.setVisibility(View.VISIBLE);
//                    } else {
//                        btnVics.setVisibility(View.INVISIBLE);
//                    }
//                }
////Add 2011/10/01 Z01yamaguchi - VICS案内実装  Start -->
//            }
//Add 2011/10/01 Z01yamaguchi - VICS案内実装  End <--
//--------------------------------------------
            if (mapActivity != null) {
                NaviApplication ap = (NaviApplication)mapActivity.getApplication();
                if (ap != null) {
                    NaviAppDataPath app_path = ap.getNaviAppDataPath();
                    if (app_path != null) {
                        if (app_path.isExistVicsData()) {
                            JNIInt JIVicsUpdateInterval = new JNIInt();
                            NaviRun.GetNaviRunObj().JNI_NE_GetVicsInfo(JIVicsUpdateInterval);
                            if (JIVicsUpdateInterval.getM_iCommon() == Constants.VICSINFO_NONE) {
                                //VICS の自動更新を行わないときは、VICSアイコンを表示しない
                                btnVics.setVisibility(View.GONE);
                            } else {
                                if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_CAR) {
                                    btnVics.setVisibility(View.VISIBLE);
                                } else {
                                    btnVics.setVisibility(View.INVISIBLE);
                                }
                            }
                        } else {
                            //VICSデータが無い時は、VICSアイコンを表示しない
                            btnVics.setVisibility(View.GONE);
                        }
                    } else {
                        NaviLog.e(NaviLog.PRINT_LOG_TAG, "MapView::updateVicsIcon - app_path is null");
                    }
                } else {
                    NaviLog.e(NaviLog.PRINT_LOG_TAG, "MapView::updateVicsIcon - ap is null");
                }
            } else {
                NaviLog.e(NaviLog.PRINT_LOG_TAG, "MapView::updateVicsIcon - mapActivity is null");
            }

//Chg 2011/10/14 Z01_h_yamada End <--

        } catch (NullPointerException e) {
            return;
        }
//Chg 2011/10/10 Z01yoneya End <--
    }

    /**
     * Update Map show area.
     */
    public void updateSurfaceSize() {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapView::updateSurfaceSize");

        NaviRun.GetNaviRunObj().RefreshDisplay(null, ScreenAdapter.getWidth(), ScreenAdapter.getHeight());
    }

    public void onMapPosChange() {
//        updateMapCenereName();

        if (isMapScrolled) {
            showMapCentreIcon();
        } else {
            hideMapCentreIcon();
        }
    }

    /**
     * Get the Name or Address of Map centre.
     *
     * @return Name or Address of Map centre
     */
    public String getMapCentreName() {
        // if(mapCentreNameLand != null && mapCentreNameLand.getVisibility() == View.VISIBLE){
        // return mapCentreNameLand.getText().toString();
        // }
        // if(mapCentreNamePort != null && mapCentreNamePort.getVisibility() == View.VISIBLE){
        // return mapCentreNamePort.getText().toString();
        // }
        // return "";
        return msPointString;
    }

    /**
     * Update Name or Address of Map centre.
     *
     * @param addr
     *            Name or Address of Map centre
     */
    public void updateMapCentreName(String addr) {
        updateMapCentreName(false, addr);
    }

//Add 2011/09/19 Z01yamaguchi Start -->
    /**
     * Update Name or Address of Map centre
     *
     * @param isAuto
     *            is auto update.
     * @param addr
     *            Name or Address of Map centre
     */
    public void updateMapCentreName(boolean isAuto, String addr) {
        mContentType = ContentType.CONTENT_STRING_ONLY;
        mIcon = null;
        updateMapCentreName(isAuto, false, addr);
    }

//    /**
//     * Update Name of Map centre (if Map centre is Driver Contens)
//     */
//    public void updateDriConName(boolean isAuto, List<DC_POIInfoShowList> oList ) {
//        mContentType = ContentType.CONTENT_DRIVE_CONTENT;
//        mIcon = null;
//        updateMapCentreName(isAuto, true, oList);
//    }

    public void updateMapCenterContentByUserFig(long figureId) {
        mContentType = ContentType.CONTENT_USER_FIGURE;
        DrawUserLayerManager drawUserLayerMng = mapActivity.getNaviApplication().getDrawUserLayerManager();
        OverlayItem userFig = drawUserLayerMng.findUserFig(figureId);
        if(userFig != null && userFig instanceof Icon) {
            mIcon = (Icon)userFig;
            updateMapCentreName(false, mIcon.hasContent(), mIcon.getDescription());
        }
    }

//    /**
//     * Update Name of Map centre (if Map centre is Driver Contens)
//     */
//    public void updateDriConName(boolean isAuto, String PoiName, long PoiId) {
//        lnDriConPoiId = PoiId;
//        updateMapCentreName(isAuto, true, PoiName);
//    }

//    public void updateMapCentre
//Add 2011/09/19 Z01yamaguchi End <--

    /**
     * Update Name or Address of Map centre
     *
     * @param isAuto
     *            is auto update.
     * @param addr
     *            Name or Address of Map centre
     * @param dc_POIInfoDetalData
     *            : drive content detail data
     */
//Chg 2011/09/19 Z01yamaguchi Start -->
//  public void updateMapCenereName(boolean isAuto, String addr){
//--------------------------------------------
    public void updateMapCentreName(boolean isAuto, boolean isVisibleDCBtn, String addr) {
//Chg 2011/09/19 Z01yamaguchi End <--
// MOD.2013.06.12 N.Sasao POI情報リスト表示対応 START
        if (addr != null && addr.length() > 0) {
            boolean isSet = true;
            if (isAuto) {
                isSet = (msPointString == null)
                        || Constants.MAP_POINT_NAME.equals(msPointString);
            }

            if (isSet) {
//Chg 2011/09/17 Z01yamaguchi Start -->
//                if (mapCentreNameLand != null) {
//                    mapCentreNameLand.setText(addr);
//                }
//                if (mapCentreNamePort != null) {
//                    mapCentreNamePort.setText(addr);
//                }
//--------------------------------------------
                if (mapCentreNameTextLand != null) {
                    mapCentreNameTextLand.setText(addr);
                }
                if (mapCentreNameTextPort != null) {
                    mapCentreNameTextPort.setText(addr);
                }

                if (mapShowDriConDtlImgLand != null) {
                    if (isVisibleDCBtn) {
                        mapShowDriConDtlImgLand.setVisibility(View.VISIBLE);
                    } else {
                        mapShowDriConDtlImgLand.setVisibility(View.GONE);
                    }
                }

                if (mapShowDriConDtlImgPort != null) {
                    if (isVisibleDCBtn) {
                        mapShowDriConDtlImgPort.setVisibility(View.VISIBLE);
                    } else {
                        mapShowDriConDtlImgPort.setVisibility(View.GONE);
                    }
                }

                isMapCentreNameClickable = isVisibleDCBtn;

//            	ListItems item = null;
//
//            	long lPOIIndex;
//
//            	if( mContentType == ContentType.CONTENT_USER_FIGURE ){
//            		lPOIIndex = EXTERNAL_POI_INDEX;
//            	}
//            	else if( mContentType == ContentType.CONTENT_STRING_ONLY ) {
//            		lPOIIndex = FAVORITE_POI_INDEX;
//            	}
//            	else{
//            		NaviLog.e(NaviLog.PRINT_LOG_TAG, "updateMapCentreName abnormity value mContentType = " + mContentType );
//            		return;
//            	}
//
//                if (isVisibleDCBtn) {
//                	item = new ListItems( lPOIIndex, addr, true, true, mContentType );
//                }
//                else {
//                	item = new ListItems( lPOIIndex, addr, true, false, mContentType );
//                }
//
//            	if( item != null  ){
//	            	if( !oMapPOIShowList.containsValue(item) ){
//	            		oMapPOIShowList.put( lPOIIndex,item );
//                    }
//
//	            	List<ListItems> localPOIShowList = new ArrayList<ListItems>();
//	        		for( Entry<Long, ListItems> entry :  oMapPOIShowList.entrySet() ) {
//	        			ListItems value = (ListItems) entry.getValue();
//	        			localPOIShowList.add(value);
//                	}
//// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
//	            	ListAdapter adapter = new ListAdapter(mapActivity.getApplicationContext(), localPOIShowList);
//// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
//
//	            	if( mapCentreListViewLand != null ){
//	            		mapCentreListViewLand.setAdapter(adapter);
//                    }
//	            	if( mapCentreListViewPort != null ){
//	            		mapCentreListViewPort.setAdapter(adapter);
//	            	}
//
//Chg 2011/09/17 Z01yamaguchi End <--

                    showCurPosName();

                    msPointString = addr;
//            	}
            } else {
                mapCentreNameLand.setVisibility(View.INVISIBLE);
                mapCentreNamePort.setVisibility(View.INVISIBLE);
                msPointString = addr;

//// DEL.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
////            	mapCentreListViewLand.setVisibility(View.INVISIBLE);
////            	mapCentreListViewPort.setVisibility(View.INVISIBLE);
////            	clearListView(mapCentreListViewLand);
////            	clearListView(mapCentreListViewPort);
//
////            	NaviLog.e(NaviLog.PRINT_LOG_TAG, "updateMapCentreName clearListView isSet");
//
////                msPointString = addr;
//// DEL.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
//Add 2011/09/19 Z01yamaguchi Start -->
                mapShowDriConDtlImgLand.setVisibility(View.GONE);
                mapShowDriConDtlImgPort.setVisibility(View.GONE);

                isMapCentreNameClickable = false;
//Add 2011/09/19 Z01yamaguchi End <--
            }

        } else {
//        	clearListView(mapCentreListViewLand);
//        	clearListView(mapCentreListViewPort);

            msPointString = Constants.MAP_POINT_NAME;
        }

    }
//// MOD.2013.06.12 N.Sasao POI情報リスト表示対応  END
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応 START
//    public void updateMapCentreName(boolean isAuto, boolean isVisibleDCBtn, List<DC_POIInfoShowList> oList) {
//        if (oList != null && !oList.isEmpty() ) {
//            boolean isSet = true;
//            if (isAuto) {
//                isSet = (msPointString == null)
//                        || Constants.MAP_POINT_NAME.equals(msPointString);
//            }
//
//            if (isSet) {
//        		ListItems item;
//
//            	for( int nCnt = 0; nCnt < oList.size(); nCnt++ ){
//            		if (isVisibleDCBtn) {
//            			item = new ListItems( oList.get(nCnt).m_POIId, oList.get(nCnt).m_sPOIName, true, true, mContentType );
//            		}
//            		else{
//            			item = new ListItems( oList.get(nCnt).m_POIId, oList.get(nCnt).m_sPOIName, true, false, mContentType );
//            		}
//
//            		if( !oMapPOIShowList.containsKey(oList.get(nCnt).m_POIId) ){
//            			oMapPOIShowList.put(oList.get(nCnt).m_POIId, item);
//            		}
//            	}
//
//            	List<ListItems> localPOIShowList = new ArrayList<ListItems>();
//        		for( Entry<Long, ListItems> entry :  oMapPOIShowList.entrySet() ) {
//        			ListItems value = (ListItems) entry.getValue();
//        			localPOIShowList.add(value);
//        		}
//// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
//            	ListAdapter adapter = new ListAdapter(mapActivity.getApplicationContext(), localPOIShowList);
//// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
//
//            	if( mapCentreListViewLand != null ){
//                   	mapCentreListViewLand.setAdapter(adapter);
//            	}
//            	if( mapCentreListViewPort != null ){
//                   	mapCentreListViewPort.setAdapter(adapter);
//            	}
//
//                showCurPosName();
//                if( 0 < oList.size() ){
//                	msPointString = oList.get(0).m_sPOIName;
//                }
//            } else {
//// DEL.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
////            	mapCentreListViewLand.setVisibility(View.INVISIBLE);
////            	mapCentreListViewPort.setVisibility(View.INVISIBLE);
////            	clearListView(mapCentreListViewLand);
////            	clearListView(mapCentreListViewPort);
//
////            	NaviLog.e(NaviLog.PRINT_LOG_TAG, "updateMapCentreName clearListView isSet dricon");
//
////          	msPointString = Constants.MAP_POINT_NAME;
//
//
//// DEL.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
//            }
//
//        } else {
//        	clearListView(mapCentreListViewLand);
//        	clearListView(mapCentreListViewPort);
//
//            msPointString = Constants.MAP_POINT_NAME;
//        }
//
//    }
//// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
//    public void clearListView(){
//    	clearListView(mapCentreListViewLand);
//    	clearListView(mapCentreListViewPort);
//    }
//
//    public void clearListView( ListView v ){
//    	if(v != null){
//            msPointString = Constants.MAP_POINT_NAME;
//
//    		oMapPOIShowList.clear();
//// MOD.2013.06.20 N.Sasao ルート修正からのルート探索時に異常終了 START
//    		ArrayAdapter tempAdapter = (ArrayAdapter)v.getAdapter();
//    		if( tempAdapter != null && !tempAdapter.isEmpty()){
//    			tempAdapter.clear();
//    		}
//// MOD.2013.06.20 N.Sasao ルート修正からのルート探索時に異常終了  END
//    	}
//    }
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応  END
//// MOD.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
    /**
     * 交差点拡大図の表示・非表示を設定します。
     *
     * @param visible
     */
    public void setJunctionVisible(boolean visible) {
    	mJunctionVisible = visible;
    	if (mJunctionVisible) {
			mSurfaceView.requestRender();
    	}
    }

    public boolean isJunctionViewVisible() {
        return mJunctionVisible;
    }

    public static void requestRender()
    {
    	getInstance().mSurfaceView.requestRender();
    }

    public void getMapCentrePos() {
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(coordinate);
    }

    public void setScrollUi() {
        NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(1, 0);
        setMapScrolled(true);
        mapActivity.runOnUiThread( new Runnable(){
            @Override
            public void run() {
                showMapCentreIcon();
                if (CommonLib.isNaviMode()) {
                    CommonLib.setBIsDisplayGuideInfo(true);
                    if (null != m_oTopView.getGuideView()) {
                        m_oTopView.getGuideView().setGuideBackClickable(false);
                    }
                }
// DEL.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応 START
//            	clearListView(mapCentreListViewLand);
//            	clearListView(mapCentreListViewPort);
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応  END
//
                msPointString = Constants.MAP_POINT_NAME;
// DEL.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
                if (m_oMapListener != null) {
                    m_oMapListener.onMapMove();
                }

            }
        });
    }

    /**
     * Show Icon when map moved.
     */
    private void showMapCentreIcon() {
        if (imgMapCenter != null) {
            imgMapCenter.setVisibility(View.GONE);
            imgMapCenter.setVisibility(View.VISIBLE);
        }

        NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(1, 0);
    }

    /**
     * Hide the Icon in Map Centre.
     */
    public void hideMapCentreIcon() {
        if (imgMapCenter != null) {
            imgMapCenter.setVisibility(View.GONE);
            imgMapCenter.setVisibility(View.INVISIBLE);
        }

        NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getAction();

        if (v != this.mSurfaceView) {
            return true;
        }
        if (v == this.mSurfaceView) {

            if (MotionEvent.ACTION_DOWN == action) {

                myPosTouching = false;
                long lnDownX = (long)event.getX();
                long lnDownY = (long)event.getY();

                if (modifyCarPos(lnDownX, lnDownY, action)) {
                    NaviRun.GetNaviRunObj().JNI_NE_MP_SetClickVehicle(1);
                    myPosTouching = true;
                    mSurfaceView.requestRender();
                    return true;
                }
                bIsDrag = false;

//Add 2011/10/16 Z01YUMISAKI Start -->
                iTime = 0;

                if (timerMessage != null) {
                    timerMessage.cancel();
                    timerMessage = null;
                }
//Add 2011/10/16 Z01YUMISAKI End <--
                if (VISIBLE == imgDetailedMessage.getVisibility()) {
                    imgDetailedMessage.setVisibility(View.GONE);
                    imgDetailedMessage.setVisibility(View.INVISIBLE);
                }
            }
            else if(MotionEvent.ACTION_MOVE == action)
            {
            	if(myPosTouching)
            	{
                    long lnX = (long)event.getX();
                    long lnY = (long)event.getY();
                    int clicked_mypos = modifyCarPos(lnX, lnY, action)?1:0;
                    NaviRun.GetNaviRunObj().JNI_NE_MP_SetClickVehicle(clicked_mypos);
                    mSurfaceView.requestRender();
                    return true;
            	}
            	if(event.getPointerCount() == 1 )
            	{
                   	PointF current =new PointF(event.getX(), event.getY());
                   	PointF prev = previousTouchPoint(event.getPointerId(0));

            		if (modifyCarPos((int)current.x,(int)current.y, action))
            		{
            			return true;
            		}
                   	if(prev != null)
                   	{
                		if(!bIsDrag && Math.abs(current.x - prev.x) < 10 &&  Math.abs(current.y - prev.y) < 10)
                		{
                			return true;
                		}
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ピッチイン/アウト・回転・ムーブ禁止 Start -->
                    	if(DrivingRegulation.GetDrivingRegulationFlg())
                    	{
                    		bIsTouchedDR = true;
                    		return (true);
                    	}
// ADD 2013.08.08 M.Honma 走行規制 ピッチイン/アウト・回転・ムーブ禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                   		Point mapTouchPoint = eventPointToMapPoint(current);
                   		Point prevMapTouchPoint = eventPointToMapPoint(prev);
                   		PointF offset = new PointF(mapTouchPoint.x - prevMapTouchPoint.x,mapTouchPoint.y - prevMapTouchPoint.y);

                   		scrollToMapPointOffset(offset,new PointF(),1,0);
                   	}
            	}
            	else if(event.getPointerCount() == 2)
            	{
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ピッチイン/アウト・回転・ムーブ禁止 Start -->
                	if(DrivingRegulation.GetDrivingRegulationFlg())
                	{
                		bIsTouchedDR = true;
                		return (true);
                	}
// ADD 2013.08.08 M.Honma 走行規制 ピッチイン/アウト・回転・ムーブ禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                   	PointF current1 =new PointF(event.getX(0), event.getY(0));
                   	PointF prev1 = previousTouchPoint(event.getPointerId(0));
                   	PointF current2 =new PointF(event.getX(1), event.getY(1));
                   	PointF prev2 = previousTouchPoint(event.getPointerId(1));

                   	if(prev1 != null && prev2 != null)
                   	{
                   		Point mapTouchPoint1 = eventPointToMapPoint(current1);
                   		Point prevMapTouchPoint1 = eventPointToMapPoint(prev1);
                   		Point mapTouchPoint2 = eventPointToMapPoint(current2);
                   		Point prevMapTouchPoint2 = eventPointToMapPoint(prev2);

                   		PointF touchCenter = new PointF((mapTouchPoint1.x + mapTouchPoint2.x)/2,(mapTouchPoint1.y + mapTouchPoint2.y)/2);
                   		PointF prevTouchCenter = new PointF((float)(prevMapTouchPoint1.x + prevMapTouchPoint2.x)/2,(float)(prevMapTouchPoint1.y + prevMapTouchPoint2.y)/2);

                   		PointF offset = new PointF(touchCenter.x - prevTouchCenter.x,touchCenter.y - prevTouchCenter.y);

                   		float angle = 0;
                   		if(isHeadup)
                   		{
                   			angle = (float)Math.atan2(mapTouchPoint2.y - mapTouchPoint1.y, mapTouchPoint2.x - mapTouchPoint1.x);
                			angle -= (float)Math.atan2(prevMapTouchPoint2.y - prevMapTouchPoint1.y, prevMapTouchPoint2.x - prevMapTouchPoint1.x);
                			angle = -(float) (angle / Math.PI * 180);
                   		}

                   		MapMath.Vector2 touchDiff = new MapMath.Vector2(mapTouchPoint2.x - mapTouchPoint1.x,mapTouchPoint2.y - mapTouchPoint1.y);
                   		MapMath.Vector2 prevTouchDiff = new MapMath.Vector2(prevMapTouchPoint2.x - prevMapTouchPoint1.x,prevMapTouchPoint2.y - prevMapTouchPoint1.y);

                   		float scale = adjustZoomRatio(touchDiff.length() / prevTouchDiff.length());

                   		scrollToMapPointOffset(offset,touchCenter,scale,angle);

//// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない START
//                        if( null != DriverContentsCtrl.getController() ){
//                        	DriverContentsCtrl.getController().callScaleChange();
//                 		}
//// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  END
//// ADD.2013.07.02 N.Sasao POI情報リスト表示負荷低減3 START
//                        // ドライブコンテンツが非表示になるとき
//            			if( !NaviRun.GetNaviRunObj().JNI_JAVA_GetIsDriConShow() ){
//            				hideCurPosName();
//            			}
//// ADD.2013.07.02 N.Sasao POI情報リスト表示負荷低減3  END
                   	}
            	}

                isMapScrolled = true;
// DEL.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応 START
//            	clearListView(mapCentreListViewLand);
//            	clearListView(mapCentreListViewPort);
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応  END
//
                msPointString = Constants.MAP_POINT_NAME;
// DEL.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
                if (m_oMapListener != null) {
                    m_oMapListener.onMapMove();
                }

                showMapCentreIcon();

            	bIsDrag = true;
                isMapScrolled = true;

                hideViewOnMapScroll();
                mSurfaceView.requestRender();
            } else if (MotionEvent.ACTION_UP == action) {
                long lnUpX = (long)event.getX();
                long lnUpY = (long)event.getY();

            	if(myPosTouching)
            	{
            		modifyCarPos(lnUpX, lnUpY, action);
                    NaviRun.GetNaviRunObj().JNI_NE_MP_SetClickVehicle(0);
                    myPosTouching= false;
                    mSurfaceView.requestRender();
                    return true;
            	}
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ピッチイン/アウト・回転・ムーブ禁止 Start -->
// MOD.2013.10.08 N.Sasao ムーブ中に走行規制がかかると、画面描画が止まる。 START
            	if(bIsTouchedDR == true && !bIsDrag)
            	{
            		bIsTouchedDR = false;
            		return (true);
            	}
// MOD.2013.10.08 N.Sasao ムーブ中に走行規制がかかると、画面描画が止まる。  END
// ADD 2013.08.08 M.Honma 走行規制 ピッチイン/アウト・回転・ムーブ禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                if (!bIsDrag){
               	    PointF current = new PointF(event.getX(), event.getY());
                    Point mapTouchPoint = eventPointToMapPoint(current);
                    PointF offset = new PointF(-mapTouchPoint.x,-mapTouchPoint.y);

               	    scrollToMapPointOffset(offset,new PointF(),1,0);
// DEL.2013.06.25 N.Sasao POI情報リスト表示負荷低減1 START
                    msPointString = Constants.MAP_POINT_NAME;
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応 START
//                	clearListView(mapCentreListViewLand);
//                	clearListView(mapCentreListViewPort);
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応  END
// DEL.2013.06.25 N.Sasao POI情報リスト表示負荷低減1  END
                    if (m_oMapListener != null) {
                        m_oMapListener.onMapMove();
                    }
                }
                bIsDrag = false;
                NaviRun.GetNaviRunObj().setSearchKind(Constants.SCROLL_MAP_KIND);

                JNITwoLong mapCentre = new JNITwoLong();
                NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(mapCentre);
                onMapScrollStop();
                mapActivity.updateWeather(mapCentre.getM_lLong(), mapCentre.getM_lLat());
            }
        }

        mLastTouchEvent = MotionEvent.obtain(event);
        return true;
    }

	PointF previousTouchPoint(int PointerId)
	{
		if(mLastTouchEvent == null)
		{
			return null;
		}
		int index = mLastTouchEvent.findPointerIndex(PointerId);
		if(index == -1)
		{
			return null;
		}
		return new PointF(mLastTouchEvent.getX(index), mLastTouchEvent.getY(index));
	}
	public Point eventPointToMapPoint(PointF eventPoint) {
		PointF calcDownPoint = mMapViewRenderer.calc2DPoint(eventPoint);
		return new Point((int)calcDownPoint.x, (int)calcDownPoint.y);
	}
    /**
     * エンジンがサポートしている範囲内に調整したzoomRatioを返します。
     *
     * @param scale
     * @return
     */
    private float adjustZoomRatio(float zoomRatio) {
    	float adjustedZoomRatio = zoomRatio;

    	NaviRun naviRun = NaviRun.GetNaviRunObj();

        // 現在エンジンが保持している地図のレベル、倍率と角度を取得
        MapInfo mapInfo = new MapInfo();
        naviRun.JNI_CT_GetReqMapPosition(mapInfo);


        // 最小の地図のレベルでの倍率を計算
        JNIDouble minMapScale = new JNIDouble();
        naviRun.JNI_NE_CalcScale(mapInfo.getAppLevel(), mapInfo.getScale() * zoomRatio, MapInfo.MIN_APP_LEVEL, minMapScale);

        // 最大の地図のレベルでの倍率を計算
        JNIDouble maxMapScale = new JNIDouble();
        naviRun.JNI_NE_CalcScale(mapInfo.getAppLevel(), mapInfo.getScale() * zoomRatio, MapInfo.MAX_APP_LEVEL, maxMapScale);

        if (minMapScale.getM_dCommon() > 1.0) {
        	adjustedZoomRatio = zoomRatio / (float)minMapScale.getM_dCommon();
        } else if (maxMapScale.getM_dCommon() < 1.0){
        	adjustedZoomRatio = zoomRatio / (float)maxMapScale.getM_dCommon();
        } else {
        	adjustedZoomRatio = zoomRatio;
        }

    	return adjustedZoomRatio;
    }


	public void scrollToMapPointOffset(PointF offset,PointF pinchCenter,float scale,float angle)
	{
		float mRotateAngle = angle;
		float mZoomRatio = scale;

		android.graphics.Matrix rotation = new android.graphics.Matrix();
		android.graphics.Matrix drotation = new android.graphics.Matrix();

		rotation.setRotate(mRotateAngle);
		drotation.setRotate(-angle);

		PointF pinch_offset = new PointF(pinchCenter.x* (1 - scale),pinchCenter.y* (1 - scale));
		PointF rot_offset = new PointF(pinchCenter.x,pinchCenter.y);
		float[] transform_pinch = new float[]{pinchCenter.x,pinchCenter.y};
		drotation.mapPoints(transform_pinch);

	    rot_offset.x = pinchCenter.x - transform_pinch[0];
	    rot_offset.y = pinchCenter.y - transform_pinch[1];

	    offset.x += pinch_offset.x + rot_offset.x;
	    offset.y += pinch_offset.y + rot_offset.y;


		float[] transform_offset = new float[]{offset.x,offset.y};
		rotation.mapPoints(transform_offset);
		PointF add_offset = new PointF(transform_offset[0]* (1 / mZoomRatio),transform_offset[1]* (1 / mZoomRatio));

		int mMovePoint_x =(int)add_offset.x;
		int mMovePoint_y =(int)add_offset.y;

        NaviRun naviRun = NaviRun.GetNaviRunObj();


        PointF newCenter = new PointF(0,0) ;
        newCenter.x -= mMovePoint_x;
        newCenter.y += mMovePoint_y;

		JNILong lon = new JNILong();
		JNILong lat = new JNILong();
        naviRun.JNI_CT_MD_ConvWinPos2LonLat((int)newCenter.x,(int)newCenter.y,
        		lon, lat, (byte) 1);

        /*
         * 地図の縮尺・角度を計算
         */
        /* 現在エンジンが保持している地図のレベル、倍率と角度を取得 */
        MapInfo currentMapInfo = new MapInfo();
        naviRun.JNI_CT_GetReqMapPosition(currentMapInfo);


        double nextScale = currentMapInfo.getScale() * mZoomRatio;

        /* 適切な地図のレベルと倍率、角度を計算 */
        JNIByte newAppLevel = new JNIByte();
        JNIDouble newMapScale = new JNIDouble();
        naviRun.JNI_NE_CalcAppLevelScale(currentMapInfo.getAppLevel(), nextScale, newAppLevel, newMapScale);
        float mapAngle = (currentMapInfo.getMapAngle() - mRotateAngle);

        /* 画面を更新 */
        /*long result = */naviRun.JNI_NE_ActivateControl(2, 0, newAppLevel.getM_bScale(), newMapScale.getM_dCommon(),
        		lon.getLcount(), lat.getLcount(), mapAngle);
        MapInfo acceptedMapInfo = new MapInfo();
        naviRun.JNI_CT_GetReqMapPosition(acceptedMapInfo);


        mSurfaceView.requestRender();

//        mCompass.setAngle((int)-mapAngle);

        setMapScrolled(true);
//        setMapCentreIconVisible(true);

//        msPointString = Constants.MAP_POINT_NAME;
        if(m_oMapListener != null){
            m_oMapListener.onMapMove();
        }

        if(CommonLib.isNaviMode()){
            CommonLib.setBIsDisplayGuideInfo(true);
//            m_oTopView.getGuideView().setBackClickable();
        }

	}

    //add liutch 0422 bug 964
    public void updateMap() {
        JNITwoLong mapCentre = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(mapCentre);

        JNITwoLong selfCentre = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(selfCentre);

        if (m_oMapListener != null &&
                (mapCentre.getM_lLat() != selfCentre.getM_lLat() || mapCentre.getM_lLong() != selfCentre.getM_lLong())) {
            m_oMapListener.onMapMove();
        }
    }

    //end
    private boolean modifyCarPos(long x, long y, int iAction) {
        //現在地修正機能
        JNILong flag = new JNILong();
        NaviRun.GetNaviRunObj().JNI_NE_MP_PointContainInVehicle(x, y, flag);
        if (flag.getLcount() == 1) {
            if (MotionEvent.ACTION_UP == iAction) {
                NaviRun.GetNaviRunObj().JNI_VP_SwitchMapMatchingRoad();
            }
            return true;
        }
        return false;
    }

    private void onMapScrollStop() {
        showViewOnMapStop();
        showMapCentreIcon();
    }

    public View getSurfaceView() {
        return mSurfaceHolder;
//        return mSurfaceView;
    }

    private int mSurfaceViewEnabledFlag = -1;
    public void setSurfaceViewEnabled(boolean enabled) {
        if (mSurfaceViewEnabledFlag == -1) {
            if (mSurfaceView.isEnabled()) {
                mSurfaceViewEnabledFlag = 1;
            }
            else {
                mSurfaceViewEnabledFlag = 0;
            }
        }
        if ((mSurfaceViewEnabledFlag == 0 && enabled)
                || (mSurfaceViewEnabledFlag == 1 && !enabled)) {
            mSurfaceView.setEnabled(enabled);
            mSurfaceViewEnabledFlag = enabled ? 1 : 0;
        }
        
        if (mapActivity != null) mapActivity.ExecuteScatterIconWhenExpose();
    }

    /* (non-Javadoc)
     * @see android.view.ViewGroup#dispatchTrackballEvent(android.view.MotionEvent)
     */
    public boolean dispatchTrackballEvent(MotionEvent event) {
        this.mSurfaceView.onTrackballEvent(event);
        return super.dispatchTrackballEvent(event);
    }

    /*
     * トラックボールを処理して事件に転がります
     * (non-Javadoc)
     * @see android.view.View#onTrackballEvent(android.view.MotionEvent)
     */
    public boolean onTrackballEvent(MotionEvent event) {
        int action = event.getAction();
        if (action == MotionEvent.ACTION_MOVE) {


           	PointF current =new PointF(event.getX(), event.getY());
           	PointF prev = previousTouchPoint(event.getPointerId(0));

           	if(prev != null) {
                onScrollMap((int)prev.y * 20, (int)prev.x * 20, (int)current.y * 20, (int)current.x * 20);
            }

        } else if (action == MotionEvent.ACTION_UP) {
        } else {
            return false;
        }
        return true;
    }

    private void onScrollMap(long lnDownX, long lnDownY, long lnUpX, long lnUpY) {
        NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(1, 0);

        NaviRun.GetNaviRunObj().JNI_ct_uic_UserScroll(lnDownX, lnDownY, lnUpX, lnUpY);

        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
    }

    private String getScale() {
        long lflag = 1000;
        JNILong lnDistance = new JNILong();
        NaviRun.GetNaviRunObj().JNI_Java_GetScaleDistance(lnDistance);
        if (lnDistance.getLcount() <= 10) {
            btnZoomIn.setEnabled(false);
            //Liugang modify start For Redmine 1783
            btnZoomOut.setEnabled(true);
            //Liugang modify end For Redmine 1783
        } else if (lnDistance.getLcount() >= 100000) {
            btnZoomOut.setEnabled(false);
            //Liugang modify start For Redmine 1783
            btnZoomIn.setEnabled(true);
            //Liugang modify end For Redmine 1783
        } else {
            btnZoomOut.setEnabled(true);
            btnZoomIn.setEnabled(true);
        }
//Chg 2011/09/12 Z01nakajima (課題No.50対応) Start -->
        //  if(CommonLib.isBIsOnHighWay() && (lnDistance.getLcount() == 500))
//Liugang modify start For Redmine 1661
        if (CommonLib.isBIsOnHighWay() && (lnDistance.getLcount() == 100) && (!isMapScrolled))
//Liugang modify end For Redmine 1661
//Chg 2011/09/12 Z01nakajima (課題No.50対応) End <--
        {
            btnZoomIn.setEnabled(false);
        } else if (lnDistance.getLcount() > 10) {
            btnZoomIn.setEnabled(true);
        }
        if (lnDistance.getLcount() < lflag) {
            return lnDistance.getLcount() + "m";
        } else {
            return (lnDistance.getLcount()) / 1000 + "km";
        }
    }

    /**
     * 地図拡大
     */
    private void zoomOut() {

//// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1) START
//    	if( null != DriverContentsCtrl.getController() ){
//    		DriverContentsCtrl.getController().DrawCancelFromView();
//    	}
//// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1)  END

        NaviRun.GetNaviRunObj().JNI_ct_uic_MapLevelUp();
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
        imgDetailedMessage.setVisibility(View.GONE);
        imgDetailedMessage.setVisibility(View.INVISIBLE);
        btnScale.setText(this.getScale());

//// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない START
//        if( null != DriverContentsCtrl.getController() ){
//        	DriverContentsCtrl.getController().callScaleChange();
// 		}
//// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  END
//
//// ADD.2013.07.02 N.Sasao POI情報リスト表示負荷低減3 START
//        // ドライブコンテンツが非表示になるとき
//		if( !NaviRun.GetNaviRunObj().JNI_JAVA_GetIsDriConShow() ){
//			hideCurPosName();
//		}
//// ADD.2013.07.02 N.Sasao POI情報リスト表示負荷低減3  END

    }

    /**
     * 地図縮小
     */
    private void zoomIn() {
        boolean isExistTownMap;

//// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1) START
//    	if( null != DriverContentsCtrl.getController() ){
//    		DriverContentsCtrl.getController().DrawCancelFromView();
//    	}
//// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1)  END

        try {
            isExistTownMap = ((NaviApplication)mapActivity.getApplication()).getNaviAppDataPath().isExistTownMap();
        } catch (NullPointerException e) {
            isExistTownMap = false;
        }
        if (!isExistTownMap) {
            // Show message "No town map" for 5 seconds
            JNILong lnDistance = new JNILong();
            NaviRun.GetNaviRunObj().JNI_Java_GetScaleDistance(lnDistance);
            if (lnDistance.getLcount() <= 50) {
                imgDetailedMessage.setVisibility(View.GONE);
                imgDetailedMessage.setVisibility(View.VISIBLE);
//Chg 2011/10/16 Z01YUMISAKI Start -->
//                timer = new Timer();
//                timer.schedule(new TimerTask() {
//                public void run() {
//                    iTime++;
//                    Message message = new Message();
//                    message.what = 3;
//                    handler.sendMessage(message);
//                }
//            }, 0, 5000);
//------------------------------------------------------------------------------------------------------------------------------------
                if (timerMessage != null) {
                    timerMessage.cancel();
                    iTime = 0;
                }
                timerMessage = new Timer();
                timerMessage.schedule(new TimerTask() {
                    public void run() {
                        iTime++;
                        Message message = new Message();
                        message.what = 3;
                        handler.sendMessage(message);
                    }
                }, 0, 5000);
//Chg 2011/10/16 Z01YUMISAKI End <--
                btnScale.setText("50m");
                return;
            }
        }

// Mod -- 2011/10/08 -- H.Fukushima --- ↓ ----- MarketPND V2 No.228 -- Mapのレベルダウン実施後でも、同じレベルなら、詳細地図がないエリア
        JNIByte beforeMapLv = new JNIByte();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapLevel(beforeMapLv);

        NaviRun.GetNaviRunObj().JNI_ct_uic_MapLevelDown();
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();

        JNIByte afterMapLv = new JNIByte();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapLevel(afterMapLv);

        if (beforeMapLv.getM_bScale() == afterMapLv.getM_bScale()) {
// Mod -- 2011/10/08 -- H.Fukushima --- ↑ ----- MarketPND V2 No.228 -- Mapのレベルダウン実施後でも、同じレベルなら、詳細地図がないエリア
            JNILong IsDetailedMessage = new JNILong();
            NaviRun.GetNaviRunObj().JNI_java_GetShowDetailedMessage(IsDetailedMessage);
            if (Constants.MAP_DETAILED_MESSAGE == IsDetailedMessage.lcount) {
// ADD -- 2011/10/08 -- H.Fukushima --- ↓ ----- MarketPND V2 No.228
                imgDetailedMessage.setVisibility(View.GONE);
// ADD -- 2011/10/08 -- H.Fukushima --- ↑ ----- MarketPND V2 No.228
                imgDetailedMessage.setVisibility(View.VISIBLE);
//Chg 2011/10/16 Z01YUMISAKI Start -->
//                if (timer != null) {
//                    timer.cancel();
//                    iTime = 0;
//                }
//                timer = new Timer();
//                timer.schedule(new TimerTask() {
//                    public void run() {
//                        iTime++;
//                        Message message = new Message();
//                        message.what = 3;
//                        handler.sendMessage(message);
//                    }
//                }, 0, 5000);
//------------------------------------------------------------------------------------------------------------------------------------
                if (timerMessage != null) {
                    timerMessage.cancel();
                    iTime = 0;
                }
                timerMessage = new Timer();
                timerMessage.schedule(new TimerTask() {
                    public void run() {
                        iTime++;
                        Message message = new Message();
                        message.what = 3;
                        handler.sendMessage(message);
                    }
                }, 0, 5000);
//Chg 2011/10/16 Z01YUMISAKI End <--
            }
            btnScale.setText(this.getScale());
        }
//// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない START
//        else{
//    		if( null != DriverContentsCtrl.getController() ){
//        		DriverContentsCtrl.getController().callScaleChange();
//    		}
//        }
//// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  END

    }

    /**
     * Get current Map Mode.
     *
     * @return current Map Mode.
     */
    public final int getMap_view_flag() {
        return map_view_type;
    }

    /**
     * Set Map Mode.
     *
     * @param mapViewType
     *            Map Mode.
     */
    public final void setMap_view_flag(int mapViewType) {
        map_view_type = mapViewType;
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "--->> mapMode Changed = " + (pre_map_view_type != mapViewType) + ", pre=" + pre_map_view_type + ", " + mapViewType);
        if (mapViewType >= 0 && pre_map_view_type != mapViewType) {
            if (m_oMapListener != null) {
                m_oMapListener.onMapUIChange();
            }
        }

        //yangyang add start 20110413 Bug813
        if (mapViewType == Constants.COMMON_MAP_VIEW) {
            Constants.GenreFlag = false;
        }
        //yangyang add end 20110413 Bug813
        if (pre_map_view_type == Constants.FIVE_ROUTE_MAP_VIEW &&
                map_view_type != Constants.FIVE_ROUTE_MAP_VIEW) {
            CommonLib.set5RouteMode(false);
        }
        pre_map_view_type = mapViewType;
    }

    /**
     * Set Map Event.
     *
     * @param mOMapListener
     *            Map Event
     */
    public void setMapListener(MapListener mOMapListener) {
        m_oMapListener = mOMapListener;
    }

    public final JNITwoLong getCoordinate() {
        return coordinate;
    }

    public final JNIString getAddressString() {
        return addressString;
    }

    /**
     * Update Map Scale Text.
     */
    public void updateScale() {
        if (btnScale != null) {
            btnScale.setText(getScale());
        }
//        onNaviModeChange();        // 車マークの状態色更新 2013 7/9
    }

    public void onNaviStart() {
        isMapScrolled = false;
        hideMapCentreIcon();
    }

    public long getLongitude() {
        return Longitude;
    }

    public void setLongitude(long longitude) {
        Longitude = longitude;
    }

    public long getLatitude() {
        return Latitude;
    }

    public void setLatitude(long latitude) {
        Latitude = latitude;
    }

    private void onSizeChange() {

//Del 2011/07/22 Z01thedoanh Start -->
//        if(ScreenAdapter.isLandMode()){
////            mapCentreNameLand.setVisibility(View.VISIBLE);
////            mapCentreNamePort.setVisibility(View.INVISIBLE);
//        	curTime_land.setVisibility(View.VISIBLE);
//            curTime_port.setVisibility(View.INVISIBLE);
//
//            navi_icon_padding.setVisibility(View.VISIBLE);
//        }else{
////            mapCentreNameLand.setVisibility(View.INVISIBLE);
////            mapCentreNamePort.setVisibility(View.VISIBLE);
//        	curTime_land.setVisibility(View.INVISIBLE);
//            curTime_port.setVisibility(View.VISIBLE);
//
//            navi_icon_padding.setVisibility(View.GONE);
//        }
//Del 2011/07/22 Z01thedoanh End <--
    }

    public void onBackCarPos() {

//      isMapScrolled = false;

        if (m_oTopView != null) {
            m_oTopView.backCarPos();
        }
        updateScale();
        hideCurPosName();
    }

    private boolean isMapFirstReady = false;

    public void onMapDrawEnd() {
        if (!isMapFirstReady) {
            if (m_oMapListener != null) {
                m_oMapListener.onMapCreated();
            }
            isMapFirstReady = true;
        }

//        if (bIsTouched) {
//            broadcastCenterPosition();
//            bIsTouched = false;
//        }

//        if(isMapScrolled){
//            showMapCentreIcon();
//        }
        mSurfaceView.requestRender();
    }

    public boolean isEnableSizeChange() {
        if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_CAR) {
            // NaviLog.d(NaviLog.PRINT_LOG_TAG,"----test 01");
            return false;
        }
// ADD.2013.11.12 N.Sasao Widget表示時のレイアウト崩れ処理 START
        // アローモード & Widget表示モードの時は回転許可を出さない
        if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_BICYCLE) {
        	if( ((NaviApplication)mapActivity.getApplication()).isShowWidget() ){
        		return false;
        	}
        }
// ADD.2013.11.12 N.Sasao Widget表示時のレイアウト崩れ処理  END

        JNIInt settingInfo = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_Java_GetSettingInfo(settingInfo);
        boolean setting = (settingInfo.getM_iCommon() == 1);
        if (!setting) {
            return false;
        }

        boolean isEnable = false;
        switch (map_view_type) {
            case Constants.COMMON_MAP_VIEW:
            case Constants.OPEN_MAP_VIEW:
            case Constants.ROUTE_MAP_VIEW:
                // XuYang add start #1021
            case Constants.NAVI_MAP_VIEW:
                // XuYang add end #1021
            case Constants.ROUTE_MAP_VIEW_BIKE:
                isEnable = true;
                break;
            default:
                break;
        }
        // NaviLog.d(NaviLog.PRINT_LOG_TAG,"---- test 02: " + isEnable +", settingInfo=" + settingInfo.getM_iCommon());
        return isEnable && setting;
    }

    /**
     * 方位マークのビットマップを設定する。
     *
     * @param iconInfo
     *            方位マークの情報(角度、アイコン番号など)
     * @param bontouch
     *            タッチ.アイコンの描画
     * @param stAngle
     *            方位マークの情報(角度、アイコン番号など)
     */
    public void setPointAngle(Java_IconInfo iconInfo, boolean bontouch,
            ZNUI_ANGLE_DATA stAngle) {
        if (null != iconInfo && null == stAngle) {
            short sIconInfo = iconInfo.getSIconInfo();
            CommonLib.angle = 360 - sIconInfo;
            if (CommonLib.angle == 360) {
                CommonLib.angle = 0;
            }
        } else if (null != stAngle) {
            short sIconInfo = stAngle.getAngle();
            CommonLib.angle = 360 - sIconInfo;
            if (CommonLib.angle == 360) {
                CommonLib.angle = 0;
            }
        }

        isTouch = bontouch;
        compass.postInvalidate();
    }

    /**
     * ノースアップ・ヘッドアップを切替する
     */
    private void azimuthSwitch() {
        JNIInt intHeadingUp = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapHeadingup(intHeadingUp);
        if (intHeadingUp.getM_iCommon() == 0) {
            NaviRun.GetNaviRunObj().JNI_NE_SetMapHeadingup(Constants.HEADINGUP_ON);
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_SetMapHeadingup(Constants.HEADINGUP_OFF);
            CommonLib.angle = 0;
            setPointAngle(null, false, null);
        }
        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
    }

    /**
     * 地図　スクロールを判断する
     */
    public boolean isMapScrolled() {
        return isMapScrolled;
    }

    public void setMapScrolled(boolean isScroll) {
        isMapScrolled = isScroll;
    }

    /**
     * Show Map Centre Name/Address.
     */
    public void showCurPosName() {
//// ADD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2 START
//    	if( null != m_oTopView ){
//        	m_oTopView.setDriveContentsEnable(false);
//    	}
//
//    	if( null != DriverContentsCtrl.getController() ){
//    		DriverContentsCtrl.getController().mapChangeRestrictionStart();
//    	}
//// ADD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2  END
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応 START
        if (ScreenAdapter.isLandMode()) {
            mapCentreNameLand.setVisibility(View.VISIBLE);
            mapCentreNamePort.setVisibility(View.INVISIBLE);

//        	mapCentreListViewLand.setVisibility(View.VISIBLE);
//        	mapCentreListViewPort.setVisibility(View.INVISIBLE);
        } else {
            mapCentreNamePort.setVisibility(View.VISIBLE);
            mapCentreNameLand.setVisibility(View.INVISIBLE);

//        	mapCentreListViewPort.setVisibility(View.VISIBLE);
//        	mapCentreListViewLand.setVisibility(View.INVISIBLE);
        }
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応  END
    }

    /**
     * Hide Map Centre Name/Address.
     */
    public void hideCurPosName() {
        mapCentreNameLand.setVisibility(View.INVISIBLE);
        mapCentreNamePort.setVisibility(View.INVISIBLE);

//// MOD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2 START
//       if ( msPointString != null && !Constants.MAP_POINT_NAME.equals(msPointString) ){
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応 START
//	    	mapCentreListViewLand.setVisibility(View.INVISIBLE);
//	    	mapCentreListViewPort.setVisibility(View.INVISIBLE);
//
//	    	clearListView(mapCentreListViewLand);
//	    	clearListView(mapCentreListViewPort);
//// ADD.2013.06.12 N.Sasao POI情報リスト表示対応  END
	        msPointString = Constants.MAP_POINT_NAME;

//	    	if( null != DriverContentsCtrl.getController() ){
//	    		DriverContentsCtrl.getController().mapChangeRestrictionEnd();
//	    	}

//	    	if( null != m_oTopView ){
//	        	m_oTopView.setDriveContentsEnable(true);
//	    	}
//        }
    }
// MOD.2013.06.27 N.Sasao POI情報リスト表示負荷低減2  END
    /** save the Map Centre Name/Address */
    public void reSetPointString() {
        msPointString = Constants.MAP_POINT_NAME;
    }

    public void onDestory() {
//Del 2011/11/11 Z01_h_yamada Start -->
//        // XuYang add start 805
//        if (oBattery != null) {
//            oBattery.release();
//        }
//
//        if (imgSignal != null) {
//            imgSignal.release();
//        }
//        // XuYang add end 805
//Del 2011/11/11 Z01_h_yamada End <--

    }

    public static final int GPS_NO     = 0x1;
    public static final int GPS_BAD    = 0x2;
    public static final int GPS_NORMAL = 0x3;
    public static final int GPS_ACCESS = 0x4;
//Add 2011/09/01 Z01yoneya Start -->
    public static final int GPS_OFF    = 0x5; //GPSがOFFになっている。衛星情報が完全に途絶えて取れない。
//Add 2011/09/01 Z01yoneya End <--

    /**
     * GPS 受信状態表示を更新
     *
     * @param gps_state
     *            GPS 受信状態
     */
    public void updateGPSIcon(int gps_state) {
        int id = -1;
        switch (gps_state) {
            case GPS_NO:
                id = R.drawable.gps_no;
                break;
            case GPS_BAD:
                id = R.drawable.gps_bad;
                break;
            case GPS_NORMAL:
                id = R.drawable.gps_good;
                break;
            case GPS_ACCESS:
                id = R.drawable.gps_receive;
                break;
//Add 2011/09/01 Z01yoneya Start -->
            case GPS_OFF:
                id = R.drawable.gps_off;
                break;
//Add 2011/09/01 Z01yoneya End <--
            default:
                break;
        }
        if (id != -1) {
            final int resid = id;
            post(new Runnable() {
                @Override
                public void run() {
                    if (btnGps != null) {
                        btnGps.setBackgroundResource(resid);
                    }
                }
            });
        }
    }

    public static final int VICS_OFF    = 0x1;
    public static final int VICS_ON     = 0x2;
    public static final int VICS_SELECT = 0x3;

    /**
     * VICS状態を表示する
     *
     * @param vicsState
     *            VICS状態
     */
    public void updateVICSIcon(int vicsState) {
        int id = -1;
        switch (vicsState) {
            case VICS_OFF:
                id = R.drawable.gps_no;
                break;
            case VICS_ON:
                id = R.drawable.gps_bad;
                break;
            case VICS_SELECT:
                id = R.drawable.gps_good;
                break;
            default:
                break;
        }
        if (id != -1) {
            final int resid = id;
            post(new Runnable() {
                @Override
                public void run() {
                    if (btnVics != null) {
                        btnVics.setBackgroundResource(resid);
                    }
                }
            });
        }
    }

    /**
     * VICSの更新時間を表示する
     *
     * @param timeVICSの更新時間
     */
    public void setVicsTime(String time) {
        if (btnVics != null) {
            btnVics.setText(time);
        }
    }

    public void onResume(MapActivity mapActivity) {
    	mSurfaceHolder.removeAllViews();
    	mSurfaceView = createMapGLSurfaceView();
        mSurfaceView.setOnTouchListener(this);
    	mSurfaceHolder.addView(mSurfaceView);
        updateMapEvent(mapActivity);
        updateVicsIcon();
    }


	public void onPause() {
		/* AQUOS PHONE(SH-12C)、 Xperia arc(SO-01C)で、onPause後に、
		 * 何故か、１つ前のフレームの画像が表示されてしまうので、requestRender()
		 * を呼び出している */
		mSurfaceView.requestRender();
		mSurfaceView.onPause();
	}
//Add 2011/07/25 Z01thedoanh (自由解像度対応: Copy or Modify from MarketV1) Start -->
    private int isSmallMode(int width, int height) {
        // 解像度HVGAより小さい場合はスモールモード
        if (width < HVGA_WIDTH && height < HVGA_HEIGHT) {
            return 1;
        }
        return 0;
    }

//Add 2011/07/25 Z01thedoanh (自由解像度対応: Copy or Modify from MarketV1) End <--

//Add 2011/09/16 Z01yoneya Start -->
    /**
     * NaviRun初期化が完了しているか返す。trueなら初期化完了
     */
    public boolean isInitNaviRun() {
        return (mNaviRun != null);
    }

//Add 2011/09/16 Z01yoneya End <--

    /**
     * 地図操作イベントブロードキャスト
     */
    public void broadcastCenterPosition() {
        JNITwoLong lonLat = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(lonLat);

        GeoPoint point = GeoUtils.toWGS84(lonLat.getM_lLat(), lonLat.getM_lLong());

        Intent intent = new Intent();
        intent.setAction(NaviIntent.ACTION_MAP_CONTROL);

        Bundle bundle = new Bundle();
        bundle.putParcelable(NaviIntent.EXTRA_POINT, point);

        intent.putExtras(bundle);
        mapActivity.sendBroadcast(intent);
    }

    public RelativeLayout getWidgetParent() {
        return mWidgetParent;
    }

    public Button getTopWidget() {
        return mTopWidget;
    }

    public Button getRightWidget() {
        return mRightWidget;
    }

//Add 2011/09/20 Z01yamaguchi Start -->
//// MOD.2013.06.12 N.Sasao POI情報リスト表示対応  END
//    private void showDCDetailInfo( long poiID ) {
//// MOD.2013.06.21 N.Sasao お気に入り経由のPOI情報詳細表示時、プログレスが表示されない START
//    	DriverContentsCtrl.getController().sendEventCode(Constants.DC_DETAIL_POI, mapActivity, poiID);
//// MOD.2013.06.21 N.Sasao お気に入り経由のPOI情報詳細表示時、プログレスが表示されない  END
//    }
//// MOD.2013.06.12 N.Sasao POI情報リスト表示対応  END
//Add 2011/09/20 Z01yamaguchi End <--

    private void openCenterContent() {
        if (isMapCentreNameClickable) {
            if (mContentType == ContentType.CONTENT_USER_FIGURE) {
                if (mIcon != null && mIcon.hasContent()) {
                    mIcon.getContent().openContent(mapActivity);
                }
            }
            else if (mContentType == ContentType.CONTENT_DRIVE_CONTENT) {
                Long poiID = new Long(lnDriConPoiId);
                DriverContentsCtrl.getController().sendEventCode(Constants.DC_DETAIL_POI, poiID);
            }
        }
    }

    /**
     * 地図画面のOpenGLのレンダラー。
     */
    public class MapViewRenderer implements GLSurfaceView.Renderer {
    	private int mViewWidth;			/* 画面のサイズ */
    	private int mViewHeight;		/* 画面のサイズ */

    	private float mapXScale;
    	private float mapYScale;

    	private int[] mGlViewPort = new int[4];
		private float[] mGlViewMatrix = new float[16];
    	private float[] mGlProjectionMatrix = new float[16];

        private float mViewAngle = 0.f;	/* モデルを回転させる角度 */
        public static final int DEFAULT_TEXTURE_ID = 0;	/* OpenGLで予約テクスチャ名 */

        private TextureDrawer textureDrawer;

        public MapViewRenderer() {
        }

        public Point getMapSurfaceSize()
        {
            return new Point(mViewWidth,mViewHeight);
        }
        public PointF getDrawableMapSize()
        {
            //地図サイズ
            float mapWidth= mViewWidth;
            float mapHeight =mViewHeight;
            if(isBirdView())
            {
            	PointF leftTop=new PointF(mGlViewPort[0],mGlViewPort[1]);
                PointF rightTop=new PointF(mGlViewPort[2],mGlViewPort[1]);
                PointF leftCenter=new PointF(mGlViewPort[0],(float)mGlViewPort[3]/2);
                leftTop = calc2DPoint(leftTop);
                rightTop = calc2DPoint(rightTop);
                leftCenter = calc2DPoint(leftCenter);

                mapWidth = Math.abs(leftTop.x - rightTop.x);
                mapHeight = Math.abs(leftTop.y - leftCenter.y) * 2.0f;
            }
            mapWidth *= 1.2;
            mapHeight *= 1.2;
            return new PointF(mapWidth,mapHeight);
        }

    	/**
    	 * モデルを傾ける角度を設定します。バードビューにするための角度を設定します。
    	 *
    	 * @param angle 回転させる角度。{@link MapViewRenderer#PLAIN_VIEW_ANGLE}、
    	 * {@link MapViewRenderer#BIRD_VIEW_ANGLE}
    	 */
    	public void setAngle(float angle) {
    		mViewAngle = angle;
    	}

    	/**
    	 * バードビュー表示しているかどうかを返します。
    	 * @return
    	 */
    	public boolean isBirdView() {
    		return mViewAngle != 0;
    	}

    	/**
    	 * Windowの座標を2D表示時の座標に変換します。
    	 * 2D表示の場合は、同じ座標が返ります。
    	 * バードビュー表示の場合は、タッチされた場所を2Dで表示した場合の座標に変換して
    	 * 返します。
    	 * @param event
    	 */
    	public PointF calc2DPoint(PointF widgetPoint) {
    		/*
    		 * 最初に、タッチされたディスプレイ上の座標に対応するニアクリップ面とファクリップ面上での座標を求めます。
    		 * 両方の座標と通る直線とx-y平面の交点を求めます。
    		 * この交点が、地図上での座標になります。
    		 */
    		float[] nearPoint = unProject(widgetPoint.x, (-widgetPoint.y + mViewHeight), 0.0f);
    		float[] farPoint = unProject(widgetPoint.x, (-widgetPoint.y + mViewHeight), 1.0f);

    		float x = nearPoint[0] + (nearPoint[0] - farPoint[0])
    									* nearPoint[2] / (farPoint[2] - nearPoint[2]);
    		float y = nearPoint[1] + (nearPoint[1] - farPoint[1])
										* nearPoint[2] / (farPoint[2] - nearPoint[2]);
    		return new PointF(x/mapXScale,y/mapYScale);
    	}
    	/**
    	 * 2D表示の場合のJavaのwidgetの座標から、OpenGLでの座標に変換します。
    	 *
    	 * @param widgetPoint
    	 * @return
    	 */
    	public PointF widgetToGL(PointF widgetPoint) {
    		return new PointF(widgetPoint.x - (float)mViewWidth / 2,
    				-widgetPoint.y + (float)mViewHeight / 2);
    	}

    	/**
    	 * 2D表示の場合のOpenGLでの座標から、Javaのwidgetの座標に変換します。
    	 *
    	 * @param glPoint
    	 * @return
    	 */
    	public PointF glToWidget(PointF glPoint) {
    		return new PointF(glPoint.x + (float)mViewWidth/ 2,
    				-glPoint.y + (float)mViewHeight / 2);
    	}

    	/**
    	 * 地図のBitmap上での座標からOpenGLでの座標に変換します。
    	 * @param mapPoint
    	 * @return
    	 */
    	public PointF mapToGL(PointF mapPoint) {
    		return new PointF(mapPoint.x - (float)mViewWidth / 2,
    				-mapPoint.y + (float)mViewHeight / 2);
    	}

    	/**
    	 * 2D表示の場合のOpenGLでの座標から、地図のBitmap上の座標に変換します。
    	 * @param glPoint
    	 * @return
    	 */
    	public PointF glToMap(PointF glPoint) {
    		return new PointF(glPoint.x+ (float)mViewWidth / 2,
    				-glPoint.y+ (float)mViewHeight / 2);
    	}

	    /**
	     * 角度を計算します。
		 * @param a
		 * @param b
		 * @return 2つのベクトルの角度。aを基準にbの角度を時計回りで返します。単位は度です。(ラジアンではありません。) 理由は、OpenGLで回転に指定する
		 * のが、度単位だからです。
		 */
		public float calcAngle(PointF newVector, PointF oldVector) {
			/* 内積・外積の定義は以下の通り
			 * 内積: A ・ B = |A| * |B| * cosθ
			 * 外積: A × B = |A| * |B| * sinθ
			 * また、
			 * tanθ = sinθ/cosθ
			 * なので、
			 * tanθ = (A × B) / (A ・ B)
			 * となり、
			 * θ = Math.atan2(A × B, A ・ B)
			 * となる。
			 */
			float innerProduct = oldVector.x * newVector.x + oldVector.y * newVector.y;
			float outerProduct = oldVector.x * newVector.y - oldVector.y * newVector.x;
			float angle = (float)Math.atan2(outerProduct, innerProduct);

			return (float)(angle / Math.PI * 180);
		}

		/**
		 * 拡大率を計算します。
		 * @param newVector
		 * @param oldVector
		 * @return 2つのベクトルの大きさの比
		 */
		public float calcZoomRatio(PointF newVector, PointF oldVector) {
			float ratio = FloatMath.sqrt((float) Math.pow(Math.E,
					(Math.log(newVector.x * newVector.x + newVector.y * newVector.y)
							- Math.log(oldVector.x * oldVector.x + oldVector.y * oldVector.y))));
			return ratio;
		}

    	/**
    	 * GLU#gluUnProject()が使いにくいのでユーティリティ・メソッド。
    	 *
    	 * @param x
    	 * @param y
    	 * @param z
    	 * @return
    	 */
    	private float[] unProject(float x, float y, float z) {
    		float[] newCoord = new float[4];
    		float[] newPoint = new float[3];

    		GLU.gluUnProject(x, y, z,
    				mGlViewMatrix, 0, mGlProjectionMatrix, 0,
    				mGlViewPort, 0, newCoord, 0);

    		newPoint[0] = newCoord[0] / newCoord[3];
    		newPoint[1] = newCoord[1] / newCoord[3];
    		newPoint[2] = newCoord[2] / newCoord[3];

    		return newPoint;
    	}

//    	long samplingDrawTime;
 //   	static final int sampleCount = 12;
  //  	int samplingCount;
		public void onDrawFrame(GL10 gl) {
			clearView(gl);
			if(!isInitNaviRun())
			{
				return;
			}

			setCamera(gl);
//            long start = SystemClock.uptimeMillis();

			int renderSize[] = new int[]{mGlViewPort[0], mGlViewPort[1], (int)(mGlViewPort[2] / mapXScale), (int)(mGlViewPort[3] /  mapYScale)};

//			boolean touching = mTouchState.getClass() != MapTouchIni.class && mTouchState.getClass() != MapTouchNoOpereation.class;


			mNaviRun.JNI_CT_DrawMap(renderSize, 0,bIsDrag,true);

			setAlphaBlending(gl);

			// スクロール中は交差点拡大図を表示しない
			if(!isMapScrolled()){
				updateJunctionView(gl);
			}

			if(mNaviRun.JNI_CT_MapAnimating())
			{
				mSurfaceView.requestRender();
			}
         //   samplingCount++;
        //    samplingDrawTime += (end-start) /sampleCount;
         //   if(sampleCount < samplingCount)
         //   {
         //   	Log.d(LOG_TAG, "OnDrawFrame drawingTime ="+samplingDrawTime+"ms/f  = "+ (1000 / samplingDrawTime)+ "f/s");
        //    	samplingDrawTime=0;  /
         //   	samplingCount=0;
         //   }
		}
		private void drawJunction(GL10 gl) {

			createJunctionBackTexture();

			Rect frame;
			if (ScreenAdapter.isLandMode()) {
				// Landscape
				frame = new Rect(mViewWidth-mapActivity.getBackWidth(), 0, mViewWidth, mapActivity.getBackHeight());
			} else {
				// Portrait
				frame = new Rect(0, 0, mapActivity.getBackWidth(), mapActivity.getBackHeight());
			}

	        Bitmap guideBackBmp = BitmapFactory.decodeResource(getResources(), R.drawable.guide_back_ex);
	        float scale = (float)mViewHeight / guideBackBmp.getHeight();

	        int dispBrdLeft = frame.left;
	        // top
	        int dispBrdTop = frame.top + (int)(Constants.GUIDE_CROSS_ZOOM_HEIGHT_OFFSET * scale) - 5;

	        // right
	        int dispBrdRight =mViewWidth;
	        // bottom
	        int dispBrdBottom = frame.top + mViewHeight;

			Rect junctionMapRect = new Rect(dispBrdLeft,mViewHeight - dispBrdBottom,dispBrdRight,mViewHeight - dispBrdTop);

			GLES20.glViewport(junctionMapRect.left,junctionMapRect.top,junctionMapRect.width(),junctionMapRect.height());

			GLES20.glScissor(junctionMapRect.left,junctionMapRect.top,junctionMapRect.width(),junctionMapRect.height());
			GLES20.glEnable(GLES20.GL_SCISSOR_TEST);

			int junctionViewport[] = new int[]{
			    0,
			    0,
			    (int)(junctionMapRect.width() / mapXScale),(int)(junctionMapRect.height() / mapYScale)
			};

			mNaviRun.JNI_CT_DrawMagMap(junctionViewport);

			GLES20.glDisable(GLES20.GL_SCISSOR_TEST);


			textureDrawer.drawTexture(mJunctionTexture[0], frame);

            // draw cross name
            int crossDestX = mapActivity.getCrossNamePosX();
            int crossDestY = mViewHeight - mapActivity.getCrossNamePosY()- mapActivity.getCrossNameHeight();
            Bitmap crossBitmap = mapActivity.getCrossName();

            int textureID = createTextureFromBitmap(crossBitmap);
			textureDrawer.drawTexture(textureID, new Rect(crossDestX, crossDestY,crossDestX +  mapActivity.getCrossNameWidth(),crossDestY + mapActivity.getCrossNameHeight()));
			deleteTexture(textureID);

            // draw compass
            int compassDestX = ScreenAdapter.getWidth() - mapActivity.getResources().getDimensionPixelSize(R.dimen.NG_Cross_Compass_paddingRight);
            int compassDestY = (int)((Constants.GUIDE_CROSS_ZOOM_HEIGHT_OFFSET + 5) / mapActivity.getGuideHeightScale());

            compassDestY = mViewHeight- compassDestY - mapActivity.getCommpassBmpHeight();

            Bitmap commpassPixels = mapActivity.getCompassBMP();

            int compassTextureID = createTextureFromBitmap(commpassPixels);
			textureDrawer.drawTexture(compassTextureID, new Rect(compassDestX, compassDestY, compassDestX + mapActivity.getCommpassBmpWidth(),compassDestY + mapActivity.getCommpassBmpHeight()));
			deleteTexture(compassTextureID);

			GLES20.glViewport(0, 0, mViewWidth, mViewHeight);
		}
		private int createTextureFromBitmap(Bitmap bitmap)
		{
			int textureID[] = new int[1];
			GLES20.glGenTextures(1, textureID, 0);
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureID[0]);
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
					GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
					GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
					GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
					GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

			GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);

			return textureID[0];
		}
		private void deleteTexture(int texID)
		{
			int textureID[] = new int[1];
			textureID[0] = texID;
			GLES20.glDeleteTextures(1, textureID, 0);
		}
		private void createJunctionBackTexture()
		{
			Bitmap guideBackBitmap = mapActivity.getBackBmp();

			if (guideBackBitmap != null && mJunctionTexture[0]==0) {
				mJunctionTexture[0] = createTextureFromBitmap(guideBackBitmap);
			}
		}

		private void updateJunctionView(GL10 gl) {
			if(mJunctionVisible)
			{
				drawJunction(gl);
			}
		}
		/**
		 * ビューをクリアします。
		 *
		 * @param gl
		 */
		private void clearView(GL10 gl) {
			GLES20.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
			GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
		}

		/**
		 * アルファブレンディングの設定をします。
		 *
		 * @param gl
		 */
		private void setAlphaBlending(GL10 gl) {
			GLES20.glEnable(GLES20.GL_BLEND);
			GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
		}

		/**
		 * 視点に関する設定をします。
		 *
		 * @param gl
		 */
		private void setCamera(GL10 gl) {


		    float viewDistance = mViewHeight*FOV_OFFSET;

			Matrix.setIdentityM(mGlViewMatrix,0);
			Matrix.translateM(mGlViewMatrix, 0, 0, 0, -viewDistance);

		    if(mMapViewRenderer.isBirdView())
		    {
		    	mViewAngle = mNaviRun.JNI_CT_GetMapBirdviewAngle();
		    }
		    else
		    {
		    	mViewAngle=0.0f;
		    }
			Matrix.rotateM(mGlViewMatrix, 0,-mViewAngle, 1, 0, 0);

			Matrix.setIdentityM(mGlProjectionMatrix,0);
			Matrix.frustumM(mGlProjectionMatrix, 0,
					-mGlViewPort[2] / 2.0f / 2.0f / FOV_OFFSET, mGlViewPort[2] / 2.0f / 2.0f/ FOV_OFFSET,
					  -mGlViewPort[3] / 2.0f / 2.0f/ FOV_OFFSET, mGlViewPort[3] / 2.0f / 2.0f/ FOV_OFFSET,
					  mGlViewPort[3] * 0.5f, mGlViewPort[3] * 10.f);
		}

		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height) {

			/*
			 * 各種の幅と高さを計算
			 */
			mViewWidth = (int)((float)width);
			mViewHeight = (int)((float)height);

//			mapXScale = getResources().getDisplayMetrics().densityDpi / DEFAULT_DPI;
//			mapYScale = getResources().getDisplayMetrics().densityDpi / DEFAULT_DPI;
			mapXScale = 1;
			mapYScale = 1;

			GLES20.glViewport(0, 0, width, height);
			mGlViewPort = new int[]{0, 0, (int)(mViewWidth), (int)(mViewHeight)};

			if (mJunctionFbo[0] != 0) {
				GLES20.glDeleteFramebuffers(1, mJunctionFbo, 0);
				GLES20.glDeleteRenderbuffers(1, mJunctionDepth, 0);
				mJunctionFbo[0] = 0;
				mJunctionDepth[0] = 0;
			}
			if (mJunctionTexture[0] != 0) {
				GLES20.glDeleteTextures(1, mJunctionTexture, 0);
				mJunctionTexture[0] = 0;
			}
/*
			mHandler.post(new Runnable() {
				@Override
				public void run() {
				}
			});*/
		}

		@Override
		public void onSurfaceCreated(GL10 gl, EGLConfig config) {
			textureDrawer = new TextureDrawer();
			textureDrawer.setup();

			handler.post(new Runnable() {
				public void run() {
					NaviLog.d(NaviLog.PRINT_LOG_TAG, "MapView::surfaceCreated");
					if (mNaviRun == null) {
						initNaviRun();
						mapActivity.onInitMap();
					}

					mapActivity.onMapSizeChanged();

					updateScale();

					onNaviModeChange();
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
					// MapView::onResumeからの移植
					// 時間調整を行うとonResumeのタイミングでは、SurfaceViewが作成段階であり
					// その状態で下記JNIを呼ぶとエンジン側で異常終了してしまう。
					// SurfaceViewが作成された段階で、地図の更新処理が実行するように変更

					// 地図が描画状態なら行う
					if( !mNaviRun.isFirstDrawMap() ){
						// 走行状態を取得してマップ描画を更新する
					    boolean drivingReg = DrivingRegulation.GetDrivingRegulationFlg();
					    mNaviRun.JNI_NE_SetDrivingRegulation(drivingReg);
						NaviLog.v(NaviLog.PRINT_LOG_TAG, "MapView::onSurfaceCreated() JNI_NE_SetDrivingRegulation[" + drivingReg + "]");
					}
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

					((NaviApplication)mapActivity.getApplication()).setStarted(true);
				}
			});
		}

		class TextureDrawer
		{
			private int program;
			private int position;
			private int texcoords;
			private int sampler;


			FloatBuffer position_buffer;
			FloatBuffer texcoords_buffer;

			private final static String _vertexShader =
				    "attribute vec4 position;\n" +
				    "attribute vec2 texcoords;\n" +
				    "varying vec2 texcoord;\n" +
				    "void main() {\n" +
				    "  gl_Position = position;\n" +
				    "  texcoord = texcoords;\n" +
				    "}\n";

			private final static String _fragmentShader =
				    "precision mediump float;\n" +
				    "varying vec2 texcoord;\n" +
				    "uniform sampler2D sampler;\n" +
				    "void main() {\n" +
				    "      gl_FragColor = texture2D(sampler, texcoord);\n" +
				    "}\n";
		    private int loadShader(int shaderType, String source) {
		        int shader = GLES20.glCreateShader(shaderType);
		        if (shader != 0) {
		            GLES20.glShaderSource(shader, source);
		            GLES20.glCompileShader(shader);
		            int[] compiled = new int[1];
		            GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
		            if (compiled[0] == 0) {
		                GLES20.glDeleteShader(shader);
		                shader = 0;
		            }
		        }
		        return shader;
		    }

		    private int createProgram(String vertexSource, String fragmentSource) {
		        int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexSource);
		        if (vertexShader == 0) {
		            return 0;
		        }

		        int pixelShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentSource);
		        if (pixelShader == 0) {
	                GLES20.glDeleteShader(vertexShader);
		            return 0;
		        }

		        int program = GLES20.glCreateProgram();
		        if (program != 0) {
		            GLES20.glAttachShader(program, vertexShader);
		            GLES20.glAttachShader(program, pixelShader);
		            GLES20.glLinkProgram(program);
		            int[] linkStatus = new int[1];
		            GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
		            if (linkStatus[0] != GLES20.GL_TRUE) {
		                GLES20.glDeleteProgram(program);
		                program = 0;
		                GLES20.glDeleteShader(vertexShader);
		                GLES20.glDeleteShader(pixelShader);
		            }
		        }
		        return program;
		    }
			public void setup() {
				program = createProgram(_vertexShader,_fragmentShader);
				position = GLES20.glGetAttribLocation(program, "position");
				texcoords = GLES20.glGetAttribLocation(program, "texcoords");
				sampler = GLES20.glGetUniformLocation(program, "sampler");

				GLES20.glUseProgram(program);
				GLES20.glUniform1i(sampler, 0);
				GLES20.glUseProgram(0);

				MapMath.Vector2[] vertex = new MapMath.Vector2[] {
						new MapMath.Vector2(-1,-1),
						new MapMath.Vector2( 1,-1),
						new MapMath.Vector2(-1, 1),
						new MapMath.Vector2( 1, 1),
				};
				MapMath.Vector2[] texcoord = new MapMath.Vector2[] {
						new MapMath.Vector2(0, 1),
						new MapMath.Vector2(1, 1),
						new MapMath.Vector2(0, 0),
						new MapMath.Vector2(1, 0),
				};

				position_buffer = MapMath.arrayToFloatBuffer(vertex);
				texcoords_buffer = MapMath.arrayToFloatBuffer(texcoord);
		}

			public void destroy() {
				GLES20.glDeleteProgram(program); program = 0;
			}
			public boolean drawTexture(int textureID,Rect drawRect)
			{
				GLES20.glUseProgram(program);
				GLES20.glEnable(GLES20.GL_BLEND);
				GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

				GLES20.glViewport(drawRect.left, drawRect.top, drawRect.width(), drawRect.height());

				GLES20.glVertexAttribPointer(position, 2, GLES20.GL_FLOAT, false, 0, position_buffer);
				GLES20.glVertexAttribPointer(texcoords, 2, GLES20.GL_FLOAT, false, 0, texcoords_buffer);

				GLES20.glEnableVertexAttribArray(position);
				GLES20.glEnableVertexAttribArray(texcoords);
				GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureID);
				GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);

				GLES20.glDisableVertexAttribArray(position);
				GLES20.glDisableVertexAttribArray(texcoords);

				GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER,0);
				return true;
			}
		}

    }

}
