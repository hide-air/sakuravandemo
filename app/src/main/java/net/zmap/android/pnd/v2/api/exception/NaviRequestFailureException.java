package net.zmap.android.pnd.v2.api.exception;

/**
 * 計算失敗、処理失敗
 */
public class NaviRequestFailureException extends NaviException {
    private static final long serialVersionUID = 1L;

    /**
     * エラー詳細メッセージとして null を設定して NaviRequestFailureException を構築します。
     */
    public NaviRequestFailureException() {
        super();
    }

    /**
     * 詳細メッセージを指定して NaviRequestFailureException を構築します。
     *
     * @param message
     *            詳細メッセージ
     */
    public NaviRequestFailureException(String message) {
        super(message);
    }
}