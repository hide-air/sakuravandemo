/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNE_RouteGuidance_t.java
 * Description    ルート案内情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNE_RouteGuidance_t
{
    private int             iEventCode;
    private int             iEventDetailCode;
    private ZNE_RouteInfoGuidePoint_t   stGuidePoint;
    private float           iDist;
    private int             iTargetDirection;
    /**
    * Created on 2010/08/06
    * Title:       getEventCode
    * Description:   ルート案内イベント種別を取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getEventCode()
    {
        return iEventCode;
    }
    /**
    * Created on 2010/08/06
    * Title:       getEventDetailCode
    * Description:  進行方向種別を取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getEventDetailCode()
    {
        return iEventDetailCode;
    }
    /**
    * Created on 2010/08/06
    * Title:       getGuidePoint
    * Description:  目標地点情報を取得する
    * @param1  無し
    * @return       ZNE_RouteInfoGuidePoint_t

    * @version        1.0
    */
    public ZNE_RouteInfoGuidePoint_t getGuidePoint()
    {
        return stGuidePoint;
    }
    /**
    * Created on 2010/08/06
    * Title:       getDist
    * Description:  目標距離を取得する
    * @param1  無し
    * @return       float

    * @version        1.0
    */
    public float getDist()
    {
        return iDist;
    }
    /**
    * Created on 2010/08/06
    * Title:       getTargetDirection
    * Description:  進行方向種別を取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getTargetDirection()
    {
        return iTargetDirection;
    }
}
