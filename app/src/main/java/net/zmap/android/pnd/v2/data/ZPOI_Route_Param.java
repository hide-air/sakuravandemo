package net.zmap.android.pnd.v2.data;

public class ZPOI_Route_Param {
	public int eside;
	public long lnCount;
	public long ulnRange;

	public int getEside() {
		return eside;
	}
	public void setEside(int eside) {
		this.eside = eside;
	}
	public long getLnCount() {
		return lnCount;
	}
	public void setLnCount(long lnCount) {
		this.lnCount = lnCount;
	}
	public long getUlnRange() {
		return ulnRange;
	}
	public void setUlnRange(long ulnRange) {
		this.ulnRange = ulnRange;
	}
}
