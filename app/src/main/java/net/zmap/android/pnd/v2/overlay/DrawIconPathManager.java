package net.zmap.android.pnd.v2.overlay;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.utils.NaviLog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

//MOD.2013.09.19 N.Sasao 外部連携固定パス変更・後処理対応実装 START
/**
 * @author rseto
 *
 */
public class DrawIconPathManager {

    private static final String TAG = "DrawIconPathManager";
    private static final String EXT_PNG = ".png";

    private static HashMap<String, String> pathMap = new HashMap<String, String>();
// Mod by CPJsunagawa '13-12-25 Start
//    private String pngTempPath = null;
    private static String pngTempPath = "/sdcard/ItsmoNaviDrive/TMP/naviicon/";
// Mod by CPJsunagawa '13-12-25 Start
    private static long fileId = 0;

    /**
     * コンストラクタ
     *
     * @param naviTempPath
     *            オリジナルアイコンファイルパス
     */
	DrawIconPathManager( String naviTempPath ){
		pngTempPath = naviTempPath;
	}

    /**
     * アイコンファイル指定、PNGファイル取得
     *
     * @param orgIconFilePath
     *            オリジナルアイコンファイルパス
     * @return PNG形式に変換したアイコンファイルパス(エラーのときはnull)
     */
    public String getPngFilePath( String orgIconFilePath ) {

        if (orgIconFilePath == null){
            return null;
        }

        if (orgIconFilePath.endsWith(EXT_PNG)){
            //拡張子がpngのときは、画像ファイルを作成しない。このクラスでは管理しない。
            return orgIconFilePath;
        }

        String tmpIconFilePath = findPngFilePath(orgIconFilePath);
        if (tmpIconFilePath != null){
            File file = new File(tmpIconFilePath);
            if ( file.isFile() && file.exists() ) {
                Log.d(TAG,"##-- findPngFilePath." + file.toString());
                return tmpIconFilePath;
            }
        }

        return createBitmapFile( orgIconFilePath );
    }

    // パス指定 png画像ファイル検索(見つからないときnull返却)
    static private String findPngFilePath( String orgIconFilePath ) {
        String pngFilePath = null;
        pngFilePath = (String)pathMap.get(orgIconFilePath);
        return pngFilePath;
    }

    // 画像ファイル(png)作成 <格納先パス>/<orgIconFilePathのファイル名>_id.png
    private String createBitmapFile( String orgIconFilePath ) {

        if (orgIconFilePath == null){
            return null;
        }

        String pngIconFilePath = getIconFileName( orgIconFilePath );
        if (pngIconFilePath == null){
            return null;
        }

        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.ARGB_8888;

        Bitmap tmpBitmap = null;
        tmpBitmap = BitmapFactory.decodeFile( orgIconFilePath, opt );

        Bitmap bitmap = CommonLib.getTransparentBmp(tmpBitmap);

        try
        {
            //bitmapファイル作成
            File file = new File(pngIconFilePath);
            if ( file.isFile() && file.exists() ) {
                file.delete();
            }

            FileOutputStream fileOut;
            fileOut = new FileOutputStream( file );
            if ( bitmap.compress( Bitmap.CompressFormat.PNG, 100, fileOut ) )
            {
                fileOut.flush();
                fileOut.close();
                pngIconFilePath = file.getAbsolutePath();
                Log.d(TAG, "##-- pngIconPath: " + pngIconFilePath);
                pathMap.put(orgIconFilePath, pngIconFilePath);
            } else{
                return null;
            }
        }
        catch ( FileNotFoundException e )
        {
            NaviLog.e(TAG, "createBitmap FileNotFoundException " + e);
            return null;
        }
        catch ( IOException e )
        {
            NaviLog.e(TAG, "createBitmap IOException " + e);
            return null;
        }

        return pngIconFilePath;

    }

    // 画像ファイル名取得(エラーのときnull)
    private String getIconFileName( String orgIconFilePath ) {
        String iconFileName = null;

        if (orgIconFilePath == null){
            return null;
        }

        String iconTempDir = getIconTmpDir();
        if (iconTempDir == null){
            return null;
        }

        String fileNameId = String.format("_%d", fileId++);

        //ファイル名取得
        int startIndex = orgIconFilePath.lastIndexOf("/");
        int endIndex = orgIconFilePath.lastIndexOf(".");
        iconFileName = iconTempDir + orgIconFilePath.substring(startIndex+1, endIndex) + fileNameId + EXT_PNG;
        Log.d(TAG, "##-- createBitmapFile" + iconFileName);

        return iconFileName;
    }

    // アイコン格納先フォルダパス取得(存在しなければ作成、エラーのときnull)
    private String getIconTmpDir() {
        String iconTmpPath = pngTempPath;
        File dirs = new File(iconTmpPath);
        if (!dirs.exists()) {
            if (!dirs.mkdirs()){
                Log.e(TAG,"Failed to create directory." + dirs.getPath());
                return null;
            }
        }
        iconTmpPath = dirs.getPath() + "/";
        return iconTmpPath;
    }

    // 一時画像格納先フォルダ削除
    public static void deleteTmpPath( String deletePath ) {
        File dirs = new File(deletePath);
        if (dirs.exists()) {
            String[] files = dirs.list();   //ディレクトリにあるすべてのファイルを削除する
            for (int i = 0; i < files.length; i++) {
                File file = new File(dirs, files[i]);
                if (!file.delete()) {
                    Log.e(TAG,"Failed to delete file." + file.toString());
                    continue;
                }
                Log.d(TAG,"##-- file.delete()." + file.toString());
            }
            if (!dirs.delete()){
                Log.e(TAG,"Failed to delete directory." + dirs.toString());
            }
            Log.d(TAG,"##-- deleteTmpPath." + dirs.toString());
        }
    }
//MOD.2013.09.19 N.Sasao 外部連携固定パス変更・後処理対応実装  END
}
