/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        Market:V2.5
 * File           MyLocationListener.java
 * Description    GPS情報取得,生ログ作成 -> JNI -> DR/VP
 * 　　　　　　　　　or
 *                     生ログ走行 -> JNI -> DR/VP
 * Created on     2013/05/29
 *
********************************************************************
*/
package net.zmap.android.pnd.v2.gps;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.LoggerManager;
import net.zmap.android.pnd.v2.common.services.MonitorAdapter;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.GPSSensorData;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.gps.GPSStatusListener;

import net.zmap.android.pnd.v2.app.NaviAppDataPath;
//import net.zmap.android.pnd.v2.app.NaviAppStatus;
//import net.zmap.android.pnd.v2.notice.controller.NotificationCtrl;
import net.zmap.android.pnd.v2.sensor.SensorDataManager;

//import android.app.Activity;
//import android.app.ActivityManager;
//import android.content.Intent;

import java.io.File;
import java.util.Calendar;

public class MyLocationListener implements LocationListener
{
	// Exconfig読み出し対応
	private static NaviAppDataPath mNaviAppDataPath = null;
	private static int setend_flga = 0;

    private static final int GPS_Valid = 0;
    private static final int GPS_Invalid = 1;
    private static final int GPS_NonDevice = 2;

	// V2.5
    private static long gps_beftime = 0;   // GPS時間??
	private static int gps_chg_flag = 0;	/* センサデータ収集の同期合わせフラグ */
	private static int senserstop_gps = 0;						// senser からGPS転送停止要求
	private static int eventIN = 0;								// senser Stop要求後にGPSイベント発生
	private static double g_Latitude = 0.0;
	private static double g_Longitude = 0.0;
	private static float g_backBearing = 0.0f;				// 方位
	private static float g_Altitude = 0.0f;						// 標高
	private static float g_Accuracy = 0.0f;						// 総合的な精度の指針(Hdop,Vdopではない)
	private static int g__iFixCount = 0;								// 測位使用GPSの数
	private static float g__snrAverageUsedInFix = 0.0f;		// 測位使用GPSのSN比率の平均値
	private static float  g_Speed = 0.0f;

//Add 2011/08/29 Z01yoneya Start -->
    private static int VP_LocationCorrection = 0x00000000;  //NEUが中国で走行テストする時の座標調整フラグ
//Add 2011/08/29 Z01yoneya End <--

    public double lng = 0;
    public double lat = 0;
    public double speed = 0;
    public double altitude = 0;
    public short direction = 0;
//    private static Calendar c = Calendar.getInstance();
    private JNIInt loadMode = new JNIInt();
    GPSSensorData sensorData = new GPSSensorData();
    static GPSSensorData sensorNData = new GPSSensorData();
	private static boolean mSensorDisableGPS = false;// Add 2011/03/17 Z01ONOZAWA
//Add 2011/08/26 Z01yoneya Start -->
    private LocationStatusListner mLocationStatusListner = null;   //
//Add 2011/08/26 Z01yoneya End <--



    // 元パス設定
    public void init_MyLocation() {
    	// ここでは assets パスが取れない
    	setend_flga = 0;
    }

    /* (non-Javadoc)
     * @see android.location.LocationListener#onLocationChanged(android.location.Location)
     */
    @Override
    public void onLocationChanged(Location location) {
		if (location == null) {
//Chg 2011/08/26 Z01yoneya Start -->
//			if (MapView.getInstance() != null) {
//				MapView.getInstance().updateGPSIcon(MapView.GPS_NO);
//			}
//--------------------------------------------
		    if(mLocationStatusListner != null){
		        mLocationStatusListner.OnGetLocationGpsAccuracy(false, 0.0f);
		    }
//Chg 2011/08/26 Z01yoneya End <--
		} else {
            onGetLocation(location);

			if (Constants.VP_LoadMode_LocationManagerLOG != loadMode.getM_iCommon()) {
                //CommonLib.setBIsGpsStatus(true);
            }
//Add 2011/08/26 Z01yoneya Start -->
            if(mLocationStatusListner != null ){
                if(location.hasAccuracy()){
                    mLocationStatusListner.OnGetLocationGpsAccuracy(true, location.getAccuracy());
                }else{
                    mLocationStatusListner.OnGetLocationGpsAccuracy(false, 0.0f);
                }
            }
//Add 2011/08/26 Z01yoneya End <--
        }
		NaviLog.i(NaviLog.PRINT_LOG_TAG, "onLocationChanged is called!!!!!!!!!!!!!!!!!!!!!");
    }

    /* (non-Javadoc)
     * @see android.location.LocationListener#onProviderDisabled(java.lang.String)
     */
    @Override
    public void onProviderDisabled(String provider) {
    	NaviLog.w(NaviLog.PRINT_LOG_TAG, "The provider " + provider + " is disabled");
    }

    /* (non-Javadoc)
     * @see android.location.LocationListener#onProviderEnabled(java.lang.String)
     */
    @Override
    public void onProviderEnabled(String provider) {
    	NaviLog.w(NaviLog.PRINT_LOG_TAG, "The provider " + provider + " is enabled");
    }

    /* (non-Javadoc)
     * @see android.location.LocationListener#onStatusChanged(java.lang.String, int, android.os.Bundle)
     */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    	if (NaviRun.isNaviRunInitial()) {
	        if (status == 0) {
	            NaviRun.GetNaviRunObj().JNI_VP_NMEASetGPSStat(GPS_NonDevice);
	            NaviLog.w(NaviLog.PRINT_LOG_TAG, status + " is OUT OF SERVICE");
	        } else if (status == 1) {
	            NaviRun.GetNaviRunObj().JNI_VP_NMEASetGPSStat(GPS_Invalid);
	            NaviLog.w(NaviLog.PRINT_LOG_TAG, status + " is TEMPORARILY_UNAVAILABLE");
	        } else {
	            NaviRun.GetNaviRunObj().JNI_VP_NMEASetGPSStat(GPS_Valid);
	            NaviLog.w(NaviLog.PRINT_LOG_TAG, status + " is Available");
	        }
    	}
    }

	/* V2.5 センサからGPS受信状態を参照する */
	static public int get_GPSchg_flag(){
		return gps_chg_flag;
	}
	static public void set_GPSLocation(int senserflga){
		senserstop_gps = senserflga;
	}
	static public int get_GPSLocationEvent(){
		return eventIN;
	}
    /**
     * GPS情報をVPモジュールまで(へ)設けます
     * @param oLoc GPS位置の情報
     * (注意)トンネルなどに入った場合、このイベントは途切れます
     * 　　　　XperiaZでは20秒ほどで途切れる
     */
    public void onGetLocation(Location oLoc)
    {
		if (mSensorDisableGPS) return;
		if ( sensorData == null) return;

		//NaviLog.v("TestWata ","onGetLocation()....................GPS............flag"+setend_flga);
		// assets パスがあれば、Exconfig.iniを参照
		if((NaviRun.getassetspath() != null ) && (setend_flga == 0)) {
			setend_flga = 1;
		}
		// 起動後にパスが入る？？(やはり、入らない)
		//NaviLog.v("TestWata ","get user Path  = "+NaviRun.getuserpath());
		//NaviLog.v("TestWata ","get assets Path= "+NaviRun.getassetspath());
		// Exconfigの読み込みが完了したかチェック
		if(setend_flga > 0) {
			gps_chg_flag=0;
			//NaviApplication napp = new NaviApplication();
//			NaviLog.v("TestWata ","Ex FileName= "+NaviRun.getprofilename());
			if (NaviRun.getDRon()) {
				// 元パス設定完了
				if(setend_flga == 1) {
					//NaviLog.v("TestWata ","onGetLocation()....................1..................");
					// DR使用
					if (NaviRun.getoperation()) {
						// どのパスを使うか
						String wpath = NaviRun.getprofilepath();
						String wname = NaviRun.getprofilename();
						String setpath = "";
						String setname = "";
						if (wname.length() > 0) {
							setname = wname;
						}
						else {
							setname = "V250_Profile.txt";		// csrc側でエラーになる名前
						}
                        if (new File(NaviRun.getuserpath() + setname).exists())  {
                            setpath = NaviRun.getuserpath();
                        }
                        else {
                            setpath = NaviRun.getassetspath() + wpath;
                        }
//// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
//						NotificationCtrl noticeCtrl = NotificationCtrl.getController();
//				        if( noticeCtrl != null ){
//				        	boolean bResult = false;
//				        	// サーバーでDRファイル名が指定されている場合はそちらを使用する
//				        	if( noticeCtrl.getDrName() != null && noticeCtrl.getDrName().length() > 0 ){
//					        	setname = noticeCtrl.getDrName();
//					        	// 最終決定されたパスにダウンロードファイルをコピーする
//					        	bResult = noticeCtrl.downloadFileToCurrent(setpath, setname);
//					        	// コピーに成功 and ダウンロードしたパス違う場合はアプリ終了時に削除対象とする
//					        	if( bResult ){
//					        		NaviRun.GetNaviRunObj().addCurrentParamPath( setpath + setname );
//					        	}
//					        }
//				        }
//// ADD.2013.07.22 N.Sasao お知らせ機能実装  END
						if (NaviRun.isNaviRunInitial()) {
//							NaviLog.v("TestWata ","Send path="+setpath + " name="+setname);
							NaviRun.GetNaviRunObj().JNI_VP_SetDRprofilePath(setpath);				// パス
							NaviRun.GetNaviRunObj().JNI_VP_SetDRprofileFilename(setname);		// ファイル
							NaviRun.GetNaviRunObj().JNI_VP_Exconfig_Call();								// 読んで通知
							setend_flga = 2;
						}
					}
					else {
						// DRを使用しない場合
						setend_flga = -1;
					}
				}
				// Profile の取得完了待ち
				if(setend_flga == 2) {
					//NaviLog.v("TestWata ","onGetLocation()....................2..................");
					long	ret = NaviRun.GetNaviRunObj().JNI_VP_ProFile_Back();
					// 正常に読み込んだ
					if(ret == 1) {
						SensorDataManager.start_start();			// センサ起動
						setend_flga = -2;									// マイナスならばOK
//						NaviLog.v("TestWata ","onGetLocation()....................OK end..................");
					}
					else if(ret == 2){
						// 読み込み失敗 DRを使用しない場合
						setend_flga = -1;
//						NaviLog.v("TestWata ","onGetLocation()....................NG end..................");
					}
				}

			}

		}

        /*if (MapView.getInstance() != null) {
            MapView.getInstance().updateGPSIcon(MapView.GPS_ACCESS);
        }*/
		/* V2.5 senserからGPS転送停止要求あり */
		if(senserstop_gps != 0) {
			eventIN = 1;				// GPSイベント発生
			gps_beftime = 0;
			return;
		}
		eventIN = 0;

        //mNaviRun.JNI_VP_NMEASetGPSStat(GPS_Valid);
        JNITwoLong LonLat = null;
		// V2.5 DR判定した自車停止状態を取得
		// !=0 : Stop ,, = 0 Go or 未判定

		long stop_run = 0;
		if (NaviRun.isNaviRunInitial()) {
			stop_run = NaviRun.GetNaviRunObj().JNI_VP_DRStopGo_Status();
		}

		// 停止中は位置、方位は前回と同じにする（通常走行）
		if(stop_run == 0)
		{
        	LonLat = CommonLib.GetIntentLongitudeForRoute(oLoc);
			// VP処理に渡すTokyo測地系
        	//sensorData.latitude1000 = LonLat.getM_lLong();
        	//sensorData.longitude1000 =  LonLat.getM_lLat();
        	sensorData.longitude1000 = LonLat.getM_lLong();
        	sensorData.latitude1000 =  LonLat.getM_lLat();
        	//NEUが中国で走行テストするための特殊処理
        	if(VP_LocationCorrection == Constants.ON){
        		sensorData.latitude1000 = sensorData.latitude1000 + (-21675000) ;
            	sensorData.longitude1000 =  sensorData.longitude1000 + 58697000 ;
        	}
	        // DR処理に渡すWGS84測地系
	        sensorData.d_wlon = oLoc.getLongitude();
	        sensorData.d_wlat = oLoc.getLatitude();


        	if (oLoc.hasBearing()) {
        		float cw = oLoc.getBearing();
 	        	Double f2 = new Double(cw);	//LocationLisnerクラスの方位は北向き０度、時計回り正。
	        	sensorData.nDirDegCW = f2.shortValue();
	        	g_backBearing = cw;
        	}
	        // V2.5 :: 方位が無効(多分停止中)に0になるので、これで回避したつもり
	        else {
	        	sensorData.nDirDegCW = (short)g_backBearing;
	        }
        NaviLog.d("GPS", "onGetLocation::MyLocationListener() DirDegCW = " + sensorData.nDirDegCW);
		}
        altitude = oLoc.getAltitude();
        sensorData.fSpeedMPS = oLoc.getSpeed();
        sensorData.timestamp = System.currentTimeMillis();

        CommonLib.setSensorData(LonLat);
        CommonLib.setAltitude(altitude);

		/* 生ログ走行では、SensorData側で生ログファイルから読み出したNMEAデータを渡す */
		if(LoggerManager.isExistLOGNModeFile() == false)
		{
			String stime = null;
			String senddata = null;
	        int iwork;

	        iwork = gps_chg_flag;
	        iwork++;
			// flagの値は参照側で前回flagと違うことを認識できればよいので、256位でｶｳﾝﾀリセット
			if(iwork == 256) {
				// flag = 0の状態はGPS未受信を示すので、１に戻す
				iwork = 1;
			}
			stime = "$GTime,"+getSensorTime()+",gpscnt,"+iwork;

			 long aa = oLoc.getTime();
			if(gps_beftime == 0)
			{
				// イベント停止または起動後
				gps_beftime = aa;
				aa = 1000;						// 1000ms
			}
			else
			{
				aa = aa - gps_beftime;		// 差分
				gps_beftime += aa;
			}
			//ラスト位置用(WGS84測地系)
			//  生ログには、停止中でもそのまま記録する
			g_Latitude = oLoc.getLatitude();
			g_Longitude = oLoc.getLongitude();
			//g_Bearing = g_backBearing;
			g_Altitude = (float)oLoc.getAltitude();
			sensorData.fAltitude = g_Altitude;
			g_Accuracy = oLoc.getAccuracy();
			sensorData.fAccuracy = g_Accuracy;
			g__iFixCount = GPSStatusListener.get_iFixCount();
			sensorData.iFixCount = g__iFixCount;
			g__snrAverageUsedInFix = GPSStatusListener.get_snrAverageUsedInFix();
			sensorData.fsnrAverageUsedInFix = g__snrAverageUsedInFix;
			senddata = String.format("$GPSmove,%.6f",g_Latitude)
						       +String.format(",%.6f",g_Longitude)
						       +String.format(",%.1f",g_backBearing)			// 方位
						       +String.format(",%d",aa)							// 間隔[ms]
						       +String.format(",%.1f",g_Altitude)				// 標高[m]
						       +String.format(",%.1f",g_Accuracy)				// 総合的な精度の指針(Hdop,Vdopではない)
						       +String.format(",%d",g__iFixCount)				// 測位使用GPSの数
						       +String.format(",%.1f",g__snrAverageUsedInFix)	// 測位使用GPSのSN比率の平均値
						       +String.format(",%.1f",oLoc.getSpeed())+"\n";

	//NaviLog.v("wata", senddata);

		    // 実機用に生データをログする
		    LoggerManager.writeNMEA_InterNaviLogFile(stime+"\n" + senddata,iwork);
		    if (NaviRun.isNaviRunInitial()) {
		    	NaviRun.GetNaviRunObj().JNI_VP_GPSNMEA_String(stime); // 走行結果ログ用

		    	NaviRun.GetNaviRunObj().JNI_VP_NMEASetGPSStat(GPS_Valid);
		    	NaviRun.GetNaviRunObj().JNI_VP_NMEASetGPSSensorData(sensorData);
		    }
	        gps_chg_flag = iwork;
			MonitorAdapter monitor = MonitorAdapter.getInst();
			if (monitor != null) monitor.mTarget.cloneMonitorGPS(sensorData.timestamp, sensorData);
		}
    }

	// V2.5
    static public void GPS_DRLogSend(String stime,String sdata) {
    	if((stime == null) || (sdata == null)) {
    		// GPS受信なしを通知する
    		if (NaviRun.isNaviRunInitial()) {
    			NaviRun.GetNaviRunObj().JNI_VP_NMEASetGPSStat(GPS_Invalid);
    		}
    		//NaviLog.v("TestWata", "[MyLocation] GPS NULL Invalid .................");
    		return;
    	}
    	if((stime == "") || (sdata == "")) {
    		// GPS受信なしを通知する
    		if (NaviRun.isNaviRunInitial()) {
    			NaviRun.GetNaviRunObj().JNI_VP_NMEASetGPSStat(GPS_Invalid);
    		}
    		//NaviLog.v("TestWata", "[MyLocation] GPS Non Invalid .................");
    		return;
    	}
    	//NaviLog.v("TestWata", "[MyLocation] "+sdata);
    	//if(sdata.equals("$GPSmove")) {
    		// "$GPSmove,35.622677,139.778430,339.9,1000,43.0,5.0,11,25.1,5.2"
    		String[] datad = sdata.split(",", -1);
    		//NaviLog.v("TestWata", "[MyLocation] length ="+datad.length);
    		if(datad.length == 10)
    		{
    			// DR判定した自車停止状態を取得
    			// !=0 : Stop ,, = 0 Go or 未判定
    			long stop_run =0;
    			if (NaviRun.isNaviRunInitial()) {
    				stop_run = NaviRun.GetNaviRunObj().JNI_VP_DRStopGo_Status();
    			}

    			// 停止中は位置、方位は前回と同じにする（生ログ走行）
    			if(stop_run == 0)
    			{
	    			Location sLoc = new Location( LocationManager.GPS_PROVIDER );
	    			JNITwoLong LonLat = null;
	    			sLoc.setLatitude(Double.valueOf(datad[1]));
	    			sLoc.setLongitude(Double.valueOf(datad[2]));
	    			/* 世界測地系 --> 東京測地系の変換 */
	    	        LonLat = CommonLib.GetIntentLongitudeForRoute(sLoc);
	    	        //sensorNData.latitude1000 = LonLat.getM_lLong();
	    	        //sensorNData.longitude1000 =  LonLat.getM_lLat();
	    	        sensorNData.longitude1000 = LonLat.getM_lLong();
	    	        sensorNData.latitude1000 =  LonLat.getM_lLat();
	    	        // GPSそのまま(世界測地系)を追加
	    	        sensorNData.d_wlon = sLoc.getLongitude();
	    	        sensorNData.d_wlat = sLoc.getLatitude();

	    			if(datad[3] != null) {
	    				sensorNData.nDirDegCW = (short)Float.valueOf(datad[3]).floatValue();
	    			}
    			}

    			sensorNData.timestamp = System.currentTimeMillis();

    			// [4]:間隔[ms] 不要

    			if(datad[5] != null) {
    				sensorNData.fAltitude = Float.valueOf(datad[5]).floatValue();
    			}
    			if(datad[6] != null) {
    				sensorNData.fAccuracy = Float.valueOf(datad[6]).floatValue();
    			}
    			if(datad[7] != null) {
    				sensorNData.iFixCount = Integer.valueOf(datad[7]).intValue();
    			}
    			if(datad[8] != null) {
    				sensorNData.fsnrAverageUsedInFix = Float.valueOf(datad[8]).floatValue();
    			}
    			if(datad[9] != null) {
    				sensorNData.fSpeedMPS = Float.valueOf(datad[9]).floatValue();
    			}
    			if (NaviRun.isNaviRunInitial()) {
    				NaviRun.GetNaviRunObj().JNI_VP_GPSNMEA_String(stime); // 走行結果ログ用

    				NaviRun.GetNaviRunObj().JNI_VP_NMEASetGPSStat(GPS_Valid);
    				NaviRun.GetNaviRunObj().JNI_VP_NMEASetGPSSensorData(sensorNData);
    			}
    		}
    	//}
   }

	public void set_stopGPSDatasave(Location oLoc) {
		// 削除理由はラスト位置を使用する
		//g_Latitude = oLoc.getLatitude();
		//g_Longitude = oLoc.getLongitude();
		//g_Bearing = oLoc.getBearing();
		g_Altitude = (float)oLoc.getAltitude();
		g_Accuracy = oLoc.getAccuracy();
		g__iFixCount = GPSStatusListener.get_iFixCount();
		g__snrAverageUsedInFix = GPSStatusListener.get_snrAverageUsedInFix();
		g_Speed = oLoc.getSpeed();
	}
	static public double  get_gps_Latitude() {
		return g_Latitude;
	}
	static public double get_gps_Longitude() {
		return g_Longitude;
	}
	static public float  get_gps_Bearing() {
		return g_backBearing;							// 方位
	}
	static public float  get_gps_Altitude() {
		return g_Altitude;						// 標高
	}
	static public float  get_gps_Accuracy() {
		return g_Accuracy;						// 総合的な精度の指針(Hdop,Vdopではない)
	}
	static public int  get_gps_iFixCount() {
		return g__iFixCount;							// 測位使用GPSの数
	}
	static public float  get_gps_snrAverageUsedInFix() {
		return g__snrAverageUsedInFix;		// 測位使用GPSのSN比率の平均値
	}
	static public float   get_gps_Speed() {
		return g_Speed;
	}

	/* ●インタなび対応 */
	/* 時刻編集 */
    public String getSensorTime(){
    	String time = "";
    	final Calendar calendar = Calendar.getInstance();

        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);
        final int second = calendar.get(Calendar.SECOND);
        final int msecond = calendar.get(Calendar.MILLISECOND);

        time =  year + ",";

        if (month < 10){
        	time += "0" + (month + 1) + ",";
        }else{
        	time += (month + 1) + ",";
        }
        if (day < 10){
        	time += "0" + day + ",";
        }else{
        	time += day + ",";
        }
        if (hour < 10){
        	time += "0" + hour + ",";
        }else{
        	time += hour + ",";
        }
        if (minute < 10){
        	time += "0" + minute + ",";
        }else{
        	time += minute + ",";
        }
        if (second < 10){
        	time += "0" + second + ",";
        }else{
        	time += second + ",";
        }
        if (msecond < 100){
        	time += "0" + msecond;
        }else{
        	time += msecond;
        }
    //	time =  year + "," + (month + 1) + "," + day + ","+ hour + ","+ minute + "," + second+"."+ msecond+",";

    	return time;
    }

// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
	public void setSensorDisable(boolean flag) {
		mSensorDisableGPS = flag;
    }
// Add 2011/03/17 Z01ONOZAWA End   <--
//Add 2011/08/26 Z01yoneya Start -->
    public void registLocationStatusLisner(LocationStatusListner listner)
    {
        mLocationStatusListner = listner;
    }
//Add 2011/08/26 Z01yoneya End <--

}