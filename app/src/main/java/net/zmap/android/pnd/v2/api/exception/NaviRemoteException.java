package net.zmap.android.pnd.v2.api.exception;

/**
 * リモート呼び出し失敗
 */
public class NaviRemoteException extends NaviException {
    private static final long serialVersionUID = 1L;

    /**
     * エラー詳細メッセージとして null を設定して NaviRemoteException を構築します。
     */
    public NaviRemoteException() {
        super();
    }

    /**
     * 詳細メッセージを指定して NaviRemoteException を構築します。
     *
     * @param message
     *            詳細メッセージ
     */
    public NaviRemoteException(String message) {
        super(message);
    }
}