package net.zmap.android.pnd.v2.common.services;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class ShutDownService {

	private ShutDownReceiver shutDownReceiver = null;
	private Context m_oContext = null;
	private ShutDownListener carMountListener = null;


	public ShutDownService(Context oContext, ShutDownListener oListener)
	{
		m_oContext = oContext;
		carMountListener = oListener;

		if(m_oContext != null)
		{
		    shutDownReceiver = new ShutDownReceiver();
			IntentFilter intetFilter = new IntentFilter(
					"android.intent.action.ACTION_SHUTDOWN");
			m_oContext.registerReceiver(shutDownReceiver, intetFilter);
		}
	}

	public void release()
	{
		if(m_oContext != null && shutDownReceiver != null)
		{
			m_oContext.unregisterReceiver(shutDownReceiver);
		}
	}
	public class ShutDownReceiver extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context oContext, Intent oIntent)
		{
			if(carMountListener != null)
			{
				carMountListener.onShutDown(oIntent);
			}
		}

	}
}
