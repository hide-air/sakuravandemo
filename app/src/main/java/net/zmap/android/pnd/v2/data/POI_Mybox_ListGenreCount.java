/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           POI_Mybox_ListGenreCount.java
 * Description    MYBOX　お気に入り
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class POI_Mybox_ListGenreCount {
    private long m_lGenreCode;
    private short m_sRecordCount;
    /**
    * Created on 2010/08/06
    * Title:       getM_lGenreCode
    * Description:  ジャンルコードを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lGenreCode() {
        return m_lGenreCode;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lGenreCode
    * Description:  各ジャンルのレコード件数リストを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public short getM_sRecordCount() {
        return m_sRecordCount;
    }
}
