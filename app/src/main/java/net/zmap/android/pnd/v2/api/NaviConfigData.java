package net.zmap.android.pnd.v2.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;

public class NaviConfigData {
    private static final String NAVI_CONFIG_DATA_INI = "NaviConfigData.ini";

    private static final String DELIMITER = " = ";
    public static final String MAP_PATH   = "BaseMapPath";
    public static final String TOWN_PATH  = "TownMapPath";
    public static final String WALK_PATH  = "WalkDataPath";
    public static final String USER_PATH  = "UserDataPath";
    public static final String TMP_PATH   = "TmpDataPath";
    public static final String LOG_PATH   = "LogDataPath";

    private Context mContext = null;

    private String mMapPath  = null;
    private String mTownPath = null;
    private String mWalkPath = null;
    private String mUserPath = null;
    private String mTmpPath  = null;
    private String mLogPath  = null;

    public NaviConfigData(Context context) {
        mContext = context;
    }

    public void read() {
        InputStream is = null;
        BufferedReader reader = null;
        try {
            is = mContext.getAssets().open(NAVI_CONFIG_DATA_INI);
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

            String line = null;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith(MAP_PATH)) {
                    mMapPath = line.split(DELIMITER)[1];
                } else if (line.startsWith(TOWN_PATH)) {
                    mTownPath = line.split(DELIMITER)[1];
                } else if (line.startsWith(WALK_PATH)) {
                    mWalkPath = line.split(DELIMITER)[1];
                } else if (line.startsWith(USER_PATH)) {
                    mUserPath = line.split(DELIMITER)[1];
                } else if (line.startsWith(TMP_PATH)) {
                    mTmpPath = line.split(DELIMITER)[1];
                } else if (line.startsWith(LOG_PATH)) {
                    mLogPath = line.split(DELIMITER)[1];
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getPath(String pathType) {
        if (pathType.equals(MAP_PATH)) {
            return mMapPath;
        } else if (pathType.equals(TOWN_PATH)) {
            return mTownPath;
        } else if (pathType.equals(WALK_PATH)) {
            return mWalkPath;
        } else if (pathType.equals(USER_PATH)) {
            return mUserPath;
        } else if (pathType.equals(TMP_PATH)) {
            return mTmpPath;
        } else if (pathType.equals(LOG_PATH)) {
            return mLogPath;
        }
        return null;
    }
}