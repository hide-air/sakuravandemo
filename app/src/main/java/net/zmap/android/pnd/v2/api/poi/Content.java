package net.zmap.android.pnd.v2.api.poi;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.List;

/**
 * コンテンツ情報
 */
public final class Content implements Parcelable {

    private static final String TAG    = "Content";

    private String              mAction;
    private String              mUri;
    private String              mMimeType;
    private String              mPackageName;
    private String              mClassName;

    /**
     *  コンテンツ情報ビルダー
     */
    public static class Builder {
        private String       action;
        private String       uri;
        private String       mimeType;
        private String       packageName;
        private String       className;

        /**
         *  コンストラクタ
         */
        public Builder() {
        }

        /**
         * アクション文字列設定
         * @param action アクション文字列
         * @return　コンテンツ情報ビルダー
         */
        public Builder action(String action) {
            this.action = action;
            return this;
        }

        /**
         * URI文字列設定
         * @param uri URI文字列
         * @return コンテンツ情報ビルダー
         */
        public Builder uri(String uri) {
            this.uri = uri;
            return this;
        }

        /**
         * MIMEType文字列設定
         * @param mimeType MIMEType文字列
         * @return コンテンツ情報ビルダー
         */
        public Builder mimeType(String mimeType) {
            this.mimeType = mimeType;
            return this;
        }

        /**
         * パッケージ名称設定
         * @param packageName パッケージ名称
         * @return コンテンツ情報ビルダー
         */
        public Builder packageName(String packageName) {
            this.packageName = packageName;
            return this;
        }

        /**
         * クラス名称設定
         * @param className クラス名称
         * @return コンテンツ情報ビルダー
         */
        public Builder className(String className) {
            this.className = className;
            return this;
        }

        /**
         * コンテンツ情報生成
         * @return　コンテンツ情報
         */
        public Content build() {
            return new Content(this);
        }
    }

    private Content(Builder builder) {
        this.mAction = builder.action;
        this.mUri = builder.uri;
        this.mMimeType = builder.mimeType;
        this.mPackageName = builder.packageName;
        this.mClassName = builder.className;
    }

    /**
     * コンストラクタ
     */
    public Content(String title) {
        super();
    }

    private boolean isEmptyString(String str) {
        if (str != null && str.length() > 0) {
            return false;
        }
        return true;
    }

    /**
     * コンテンツ情報取得判定<br>
     * <br>
     * @return true : 情報あり    false : 情報なし
     */
    public boolean hasContent() {
        return (!isEmptyString(mAction) ||
                !isEmptyString(mUri) ||
                !isEmptyString(mMimeType) ||
                !isEmptyString(mPackageName) ||
                !isEmptyString(mClassName));
    }

    /**
     * コンテンツ情報　表示<br>
     * <br>
     * 所持しているコンテンツ情報をもとにコンテンツを表示します。<br>
     *
     * @param context
     *            コンテキスト
     */
    public void openContent(Context context) {

        Intent intent = null;
        if (!isEmptyString(mAction)) {
            intent = new Intent(mAction);
        } else {
            intent = new Intent(Intent.ACTION_VIEW);
        }

        if (!isEmptyString(mUri)) {
            intent.setData(Uri.parse(mUri));
        }

        if (!isEmptyString(mMimeType)) {
            intent.setType(mMimeType);
        }

        if (!isEmptyString(mPackageName) && !isEmptyString(mClassName)) {
            intent.setClassName(mPackageName, mClassName);
        }

        List<ResolveInfo> infoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (infoList != null && infoList.size() > 0) {
            context.startActivity(intent);
        } else {
            Log.w(TAG, "No matching activities(" + intent.toString() + ")");
        }
    }

    /**
     * アクション文字列設定<br>
     *
     * @param action
     *            アクション文字列
     */
    public void setAction(String action) {
        mAction = action;
    }

    /**
     * URI文字列設定<br>
     *
     * @param uri
     *            URI文字列
     */
    public void setUri(String uri) {
        mUri = uri;
    }

    /**
     * MIMEType文字列設定<br>
     *
     * @param mimeType
     *            MIMEType文字列
     */
    public void setMimeType(String mimeType) {
        mMimeType = mimeType;
    }

    /**
     * パッケージ名称設定
     *
     * @param packageName
     *            パッケージ名称
     */
    public void setPackageName(String packageName) {
        mPackageName = packageName;
    }

    /**
     * クラス名称設定
     *
     * @param className
     *            クラス名称
     */
    public void setClassName(String className) {
        mClassName = className;
    }

    protected Content(Parcel in) {
        mAction = in.readString();
        mUri = in.readString();
        mMimeType = in.readString();
        mPackageName = in.readString();
        mClassName = in.readString();
    }

    /* (非 Javadoc)
     * @see net.zmap.android.pnd.v2.agenthmi.api.data.NaviPoint#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mAction);
        out.writeString(mUri);
        out.writeString(mMimeType);
        out.writeString(mPackageName);
        out.writeString(mClassName);
    }

    /* (非javadoc)
     *
     */
    public static final Parcelable.Creator<Content> CREATOR = new Parcelable.Creator<Content>() {
        public Content createFromParcel(Parcel in) {
            return new Content(in);
        }

        public Content[] newArray(int size) {
            return new Content[size];
        }
    };

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }
}
