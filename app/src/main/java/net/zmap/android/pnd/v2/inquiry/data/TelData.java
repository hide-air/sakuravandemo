package net.zmap.android.pnd.v2.inquiry.data;

import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Tel_ListItem;
import net.zmap.android.pnd.v2.data.POI_ZipCode_ListItem;

public class TelData {

	private POI_Tel_ListItem[] m_Poi_Tel_Listitem = null;
	private POI_ZipCode_ListItem[] m_Poi_ZipCode_Listitem = null;
//	private String msPointName;

	public void GetTelSearchInfo(){
		byte bScale = 0;
		long Longitude = 0;
		long Latitude = 0;

		JNILong JNILRec = new JNILong();
		m_Poi_Tel_Listitem = new POI_Tel_ListItem[1];
		m_Poi_Tel_Listitem[0] = new POI_Tel_ListItem();

		NaviRun.GetNaviRunObj().JNI_NE_POITel_GetRecList(0, 1, m_Poi_Tel_Listitem, JNILRec);
//		msPointName = m_Poi_Tel_Listitem[0].getM_Name();

		JNIInt MapScale = new JNIInt();
		NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
		bScale = (byte) MapScale.getM_iCommon();

		Latitude = m_Poi_Tel_Listitem[0].getM_lLat();
		Longitude = m_Poi_Tel_Listitem[0].getM_lLon();
		NaviRun.GetNaviRunObj().JNI_NE_ActivateControl( 2, 2, bScale,1, Longitude, Latitude,0);
	}

	public void GetZipSearchInfo(){
		byte bScale = 0;
		long Longitude = 0;
		long Latitude = 0;

		JNILong JNILRec = new JNILong();
		m_Poi_ZipCode_Listitem = new POI_ZipCode_ListItem[1];
		m_Poi_ZipCode_Listitem[0] = new POI_ZipCode_ListItem();

		NaviRun.GetNaviRunObj().JNI_NE_POIZip_GetRecList(0, 1, m_Poi_ZipCode_Listitem, JNILRec);
//		msPointName = m_Poi_ZipCode_Listitem[0].getM_Name();

		JNIInt MapScale = new JNIInt();
		NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
		bScale = (byte) MapScale.getM_iCommon();

		Latitude = m_Poi_ZipCode_Listitem[0].getM_lLat();
		Longitude = m_Poi_ZipCode_Listitem[0].getM_lLon();
		NaviRun.GetNaviRunObj().JNI_NE_ActivateControl( 2, 2, bScale,1, Longitude, Latitude,0);
	}
}
