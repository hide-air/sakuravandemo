package net.zmap.android.pnd.v2.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidAlgorithmParameterException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.SecretKey;

import javax.crypto.KeyGenerator;

import org.w3c.dom.Element;

import net.zmap.android.pnd.v2.common.services.PreferencesService;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

public class CipherUtils {
	private static final String SECRET_IV_SRC = "zDcdRivEcoNteNts";
	private static final String KEY_PARENT = "private_key";
	private static final String KEY_DRICON = "dricon_key";

	private PreferencesService pref;
	private Key dri_key_pref = null;

	public CipherUtils(Context oContext){
		if( null != oContext ){
			pref = new PreferencesService( oContext, KEY_PARENT );

			String dri_key = pref.getString( KEY_DRICON, "");

			if( dri_key.length() <= 0 ){
				prefKeyUpdate();
			}
			else{
				// バイナリーデータにデコード(文字列をバイナリデータに変換)
				byte[] keyBytes = Base64.decode(dri_key, Base64.URL_SAFE | Base64.NO_WRAP);
				// キーの復元
				dri_key_pref = new SecretKeySpec(keyBytes, "AES");
			}
		}
	}

	public byte[] cipherEncrypt(byte[] buf){
		if( null == dri_key_pref ){
			return null;
		}

		try {
			return cipherEncrypt(buf, dri_key_pref, SECRET_IV_SRC);
	  	} catch (Exception e) {
	    	e.printStackTrace();
	  	}
	  	return null;
	}

	public byte[] cipherDecrypt(byte[] buf){
		if( null == dri_key_pref ){
			return null;
		}

		try {
			return cipherDecrypt(buf, dri_key_pref, SECRET_IV_SRC);
	  	} catch (Exception e) {
	    	e.printStackTrace();
	  	}
	  	return null;
	}
	// 暗号化
	// buf 暗号化するデータ
	// key 暗号化鍵
	// iv  IV(初期化ベクトル)
	private byte[] cipherEncrypt(byte[] buf, Key key, String iv){
		try {
			// 秘密鍵を構築します
	    	SecretKeySpec sksSpec = new SecretKeySpec(key.getEncoded(), "AES");

			// IV(初期化ベクトル)を構築します
	    	IvParameterSpec ivSpec = new IvParameterSpec(iv.getBytes());

			// 暗号化を行うアルゴリズム、モード、パディング方式を指定します
	    	Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

			// 初期化します
	    	cipher.init(Cipher.ENCRYPT_MODE, sksSpec, ivSpec);

			// 暗号化します
	    	return cipher.doFinal(buf);

	  	} catch (NoSuchAlgorithmException e) {
	    	e.printStackTrace();
	  	} catch (NoSuchPaddingException e) {
	    	e.printStackTrace();
	  	} catch (InvalidKeyException e) {
	    	e.printStackTrace();
	  	} catch (IllegalBlockSizeException e) {
	    	e.printStackTrace();
	  	} catch (BadPaddingException e) {
	    	e.printStackTrace();
	  	} catch (InvalidAlgorithmParameterException e) {
	    	e.printStackTrace();
	  	}
	  	return null;
	}

	// 複合化
	// buf 暗号化されたデータ
	// key 暗号化鍵
	// iv  IV(初期化ベクトル)
	private static byte[] cipherDecrypt(byte[] buf, Key key, String iv){
		try {
			// 秘密鍵を構築します
		    SecretKeySpec sksSpec = new SecretKeySpec(key.getEncoded(), "AES");

			// IV(初期化ベクトル)を構築します
		    IvParameterSpec ivSpec = new IvParameterSpec(iv.getBytes());

			// 暗号化を行うアルゴリズム、モード、パディング方式を指定します
		    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

			// 初期化します
		    cipher.init(Cipher.DECRYPT_MODE, sksSpec, ivSpec);

			// 複合化します
	    	return cipher.doFinal(buf);
	  	} catch (NoSuchAlgorithmException e) {
	    	e.printStackTrace();
	  	} catch (NoSuchPaddingException e) {
	    	e.printStackTrace();
	  	} catch (InvalidKeyException e) {
		    e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
	    	e.printStackTrace();
	  	} catch (BadPaddingException e) {
	    	e.printStackTrace();
	  	} catch (InvalidAlgorithmParameterException e) {
	    	e.printStackTrace();
	  	}
	  	return null;
	}

	// 暗号化アルゴリズムに応じた鍵を生成する
	private Key genKey() throws NoSuchAlgorithmException {

	  	KeyGenerator generator = KeyGenerator.getInstance("AES");
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
	  	// 乱数の発生源を作成します 指定できるのはSHA1PRNGのみ
	  	SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

	  	// 第一引数にキー長のbit数を指定します
	  	generator.init(128, random);
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END

	  	return generator.generateKey();
	}

	public void prefKeyUpdate(){
		if( null == pref ){
			return;
		}
		Key newKey = null;
		try {
			newKey = genKey();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		if( newKey != null ){
// ADD.2013.08.12 N.Sasao 初回暗号処理で必ず失敗する(作成した複合キーをメンバーに設定し忘れ) START
			// 復元キーを更新
			dri_key_pref = newKey;
// ADD.2013.08.12 N.Sasao 初回暗号処理で必ず失敗する(作成した複合キーをメンバーに設定し忘れ)  END
			//文字列のエンコード(バイナリデータを文字列に変換)
			String encode = Base64.encodeToString( newKey.getEncoded(), Base64.URL_SAFE | Base64.NO_WRAP );
			pref.setValue( KEY_DRICON, encode );
			pref.save();
		}
	}

	public boolean fileToEncrypt( String sFilePath ){
		byte[] input = null;
		byte[] output = null;

		input = loadStr(sFilePath);

		if( input == null ){
			return false;
		}

		byte[] contentsDc =cipherEncrypt(input);

		File deleteFile = new File(sFilePath);
		if( deleteFile != null && deleteFile.exists() ){
			deleteFile.delete();
		}

		output = saveStr(sFilePath, contentsDc);

		if( output == null ){
			return false;
		}

		return true;
	}

	public boolean fileToEncrypt( String sBeforePath, String sAfterPath, boolean bRemove ){
		byte[] input = null;
		byte[] output = null;

		input = loadStr(sBeforePath);

		if( input == null ){
			return false;
		}

		byte[] contentsDc =cipherEncrypt(input);

		output = saveStr(sAfterPath, contentsDc);

		if( output == null ){
			return false;
		}

		if( bRemove ){
			File deleteFile = new File( sBeforePath );
			if( deleteFile != null && deleteFile.exists() ){
				deleteFile.delete();
			}
		}
		return true;
	}

	public boolean fileToDecrypt( String sFilePath ){
		byte[] input = null;
		byte[] output = null;

		input = loadStr(sFilePath);

		if( input == null ){
			return false;
		}

		byte[] contentsDc =cipherDecrypt(input);

		File deleteFile = new File(sFilePath);
		if( deleteFile != null && deleteFile.exists() ){
			deleteFile.delete();
		}

		output = saveStr(sFilePath, contentsDc);

		if( output == null ){
			return false;
		}

		return true;
	}

	public boolean fileToDecrypt( String sBeforePath, String sAfterPath, boolean bRemove ){
		byte[] input = null;
		byte[] output = null;

		input = loadStr(sBeforePath);

		if( input == null ){
			return false;
		}

		byte[] contentsDc =cipherDecrypt(input);

		output = saveStr(sAfterPath, contentsDc);

		if( output == null ){
			return false;
		}

		if( bRemove ){
			File deleteFile = new File( sBeforePath );
			if( deleteFile != null && deleteFile.exists() ){
				deleteFile.delete();
			}
		}
		return true;
	}
	// Fileを指定して開く
	private byte[] loadStr(String inputPath){
		InputStream is = null;
		byte[] bytes = null;

		if( inputPath == null || inputPath.length() <= 0 ){
			return null;
		}

		try{
			File file = new File(inputPath);

			is = new FileInputStream(file);
			bytes = new byte[is.available()];
			is.read(bytes);

		} catch(FileNotFoundException e){
			// ここでエラーになることが前提のシーケンスもあるので、トレースは表示させない。
//			e.printStackTrace();
        } catch (IOException e) {
			e.printStackTrace();
		} finally {
			if( is != null ){
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		if( bytes == null ){
			return null;
		}
		else{
			return bytes;
		}
	}

	// Fileを指定して保存
	private byte[] saveStr(String outputFile, byte[] str){
		OutputStream os = null;
		boolean bSaveResult = false;

		if( outputFile == null || outputFile.length() <= 0 ){
			return null;
		}
		if( str == null || str.length <= 0 ){
			return null;
		}

		try{
			os = new FileOutputStream(outputFile);
			os.write(str);
			os.close();

			bSaveResult = true;
		} catch(FileNotFoundException e){
			e.printStackTrace();
        } catch (IOException e) {
			e.printStackTrace();
		} finally {
			if( os != null ){
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if( !bSaveResult ){
			return null;
		}
		else{
			return str;
		}
	}
}
