package net.zmap.android.pnd.v2.data;

import java.io.Serializable;

public class DC_SectionOptionData implements Serializable{
	private static final long serialVersionUID = -1367808929250977846L;
	public int		m_nParentID = 0;		//グループ化対応 親のID
	public int		m_nChildID = 0;			//グループ化対応 子のID
	public String 	m_sOptionText;			//オプションテキスト
	public String	m_sOptionValue;			//オプション値
}
