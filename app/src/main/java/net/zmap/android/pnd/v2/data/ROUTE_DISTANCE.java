/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ROUTE_DISTANCE.java
 * Description    �T�������ݒ�
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ROUTE_DISTANCE {

    private long lnRouteCount;
    private JNILong[] alnDistance = null;
    /**
    * Created on 2010/08/06
    * Title:       initalnDistance
    * Description:  関数を初期化します
    * @param1  int len 探索点数
    * @return       void

    * @version        1.0
    */
    public void initalnDistance(int len){
        alnDistance = new JNILong[len];
    }
    /**
    * Created on 2010/08/06
    * Title:       getRouteCount
    * Description:  探索点数を取得する
    * @param1   無し
    * @return       long

    * @version        1.0
    */
    public long getRouteCount() {
        return lnRouteCount;
    }
    /**
    * Created on 2010/08/06
    * Title:       getRouteCount
    * Description:  探索距離を取得する
    * @param1   無し
    * @return       JNILong[]

    * @version        1.0
    */
    public JNILong[] getalnDistance() {
        return alnDistance;
    }
}
