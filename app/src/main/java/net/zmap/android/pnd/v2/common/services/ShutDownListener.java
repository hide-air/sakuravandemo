package net.zmap.android.pnd.v2.common.services;

import android.content.Intent;

public interface ShutDownListener
{
	public void onShutDown(Intent oIntent);
}
