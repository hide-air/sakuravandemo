// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
package net.zmap.android.pnd.v2.common;

import net.zmap.android.pnd.v2.R;
// DEL 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
//import net.zmap.android.pnd.v2.common.services.ClarionControlService;
// DEL 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.services.DrivingRegulationServiceBase;
import net.zmap.android.pnd.v2.common.services.PreferencesService;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.maps.MapActivity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.view.Gravity;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.app.Activity;
import android.view.WindowManager;

/**
 *  @file		DrivingRegulation
 *  @brief		走行規制 状態管理および、規制メッセージの表示
 *
 *  @attention
 *  @note
 *
 *  @author		M.Honma
 *  @date		$Date:: 2013-08-28 00:00:00 +0900 #$ (Create at 2013-08-28)
 *  @version	$Revision: $ by $Author: $
 *
 */
public class DrivingRegulation {

	/**
     * ナビアプリのアクション名：走行規制下位サービス接続状態変化通知
     */
    public static final String ACTION_DRIVING_REGULATION_CONNECT 		= "net.zmap.android.pnd.v2.app.action.DRIVING_REGULATION_CONNECT";
	/**
     * ナビアプリのアクション名：走行規制下位サービス走行規制変化通知
     */
    public static final String ACTION_DRIVING_REGULATION_DRIVING 		= "net.zmap.android.pnd.v2.app.action.DRIVING_REGULATION_DRIVING";

    /**
     * エクストラ名：接続通知
     */
    public static final String EXTRA_CONNECT							= "net.zmap.android.pnd.v2.app.extra.EXTRA_CONNECT";

    /**
     * エクストラ名：走行中通知
     */
    public static final String EXTRA_DRIVING							= "net.zmap.android.pnd.v2.app.extra.EXTRA_DRIVING";

    /**
     * 公開定数：車載器連携種別
     */

    public static final int DEF_VALUE_GUIDE_ERROR 		= -1;						// 異常値
    public static final int DEF_VALUE_GUIDE_NONE 		= 0;						// 連携機能なし
    public static final int DEF_VALUE_GUIDE_CLARION 	= 1;						// クラリオン連携
    public static final int DEF_VALUE_GUIDE_OTHER 		= 10;						// その他連携(Dummy)

    private static final String KEY_PARENT 			= "driving_regulation";			// 車載器連携設定保存キー
    private static final String KEY_GUIDE 				= "kind";					// 車載器連携設定保存キー
    private static final String SMART_ACCESS_APP	= "Smart Access";

    private static int mRegulationKind = DEF_VALUE_GUIDE_NONE;						// 車載器連携種別
	private static boolean m_IsBind = false;										// 中間サービスのバインド状態
	private static boolean m_IsConnect = false;										// 中間サービスの接続状態
	private static boolean m_bDriving = false;
	private static Context m_Context = null;
	private static Activity m_Activity = null;
	private static Toast toast = null;

	/********************************************************************************/
	/* 新規車載器連携サービス追加時は、mValueCheckとmCooperativeCheckに種別を追加を */
	/********************************************************************************/
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// V2は外部API以外車載器連携をしない
//	private static List<Integer> mValueCheck			= new ArrayList<Integer>( 	// Preference設定値チェック用テーブル
//															Arrays.asList(	DEF_VALUE_GUIDE_NONE,
//																			DEF_VALUE_GUIDE_CLARION,
//																			DEF_VALUE_GUIDE_OTHER) );
//	private static List<Integer> mCooperativeCheck		= new ArrayList<Integer>( 	// 車載器連携種別チェック用テーブル
//															Arrays.asList(	DEF_VALUE_GUIDE_CLARION ) );
	private static List<Integer> mValueCheck			= new ArrayList<Integer>( 	// Preference設定値チェック用テーブル
															Arrays.asList(	DEF_VALUE_GUIDE_NONE,
																			DEF_VALUE_GUIDE_OTHER) );
	private static List<Integer> mCooperativeCheck		= new ArrayList<Integer>( 	// 車載器連携種別チェック用テーブル
																				);

// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
	private static DrivingRegulationServiceBase mDrivingRegulationService = null;			// 車載器連携するサービス

	/**
	 * 初期化処理。
	 * 「車載器連携」設定値を取得し、保存しておく。
	 * レシーバーの登録をする。
	 */
	public static void Init(Context context)
	{
		m_Context = context;
		boolean bCheckOK = false;

			// インストールされていれば、設定値の取得
	    PreferencesService pref = new PreferencesService( m_Context, KEY_PARENT );
		if( pref != null ){
			mRegulationKind = pref.getInt( KEY_GUIDE, DEF_VALUE_GUIDE_ERROR );

			for( int i = 0; i < mValueCheck.size(); i++ ){
				if( mValueCheck.get(i).equals(mRegulationKind) ){
					if( IsReadyEnv( mRegulationKind ) ){
						bCheckOK = true;
					}
				}
		}
		}

		if( !bCheckOK ){
			// 設定値が取れない場合、連携する環境が整ってない場合は、連携なしとして保存
			mRegulationKind = DEF_VALUE_GUIDE_NONE;
			SetDrivingRegulationGuide( mRegulationKind );
		}

		toast = Toast.makeText(m_Context, m_Context.getResources().getString(R.string.DrivingRegulationMsg), Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);

		if( m_Context != null ){
			IntentFilter intentFilter = new IntentFilter();
			intentFilter.addAction( ACTION_DRIVING_REGULATION_CONNECT );
			intentFilter.addAction( ACTION_DRIVING_REGULATION_DRIVING );

			m_Context.registerReceiver( mDrivingRegulationReceiver, intentFilter );
		}
	}
	/**
	 * 終了処理。
	 * レシーバーの登録を解除する。
	 */
	public static void Finalize(){
		if( m_Context != null ){
			m_Context.unregisterReceiver( mDrivingRegulationReceiver );
		}
	}

	/**
	 * 「車載器連携」設定値の取得。
	 * @return 車載器種別
	 */
	public static int GetDrivingRegulationGuide()
	{
		return (mRegulationKind);
	}

	/**
	 * 「車載器連携」設定値の保存。
	 * @param context コンテキスト
	 * @param value : 車載器種別
	 * @return true:成功, false: 失敗
	 */
	public static boolean SetDrivingRegulationGuide(int value)
	{
		boolean bCheckOK = false;
		for( int i = 0; i < mValueCheck.size(); i++ ){
			if( mValueCheck.get(i).equals(value) ){
				bCheckOK = true;
			}
		}

		if( bCheckOK ){
		// 設定値の保存
		PreferencesService pref = new PreferencesService(m_Context, KEY_PARENT);
		if( pref != null ){
			pref.setValue(KEY_GUIDE, value);
			pref.save();
				mRegulationKind = value;
				return true;
		}
		}
		return false;
	}

	public static void Bind(){
		Intent intent = null;
		ServiceConnection serviceConnection = null;

		if( m_Context == null ){
			NaviLog.e(NaviLog.PRINT_LOG_TAG, "Bind is referred before initialization. ");
			return;
		}

		// すでにバインド中 or 外部APIで走行規制権限取得中は何もしない
		if( m_IsBind || IsDrivingControlLock() ){
			return;
		}

		switch( mRegulationKind ){
			case DEF_VALUE_GUIDE_CLARION:
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
//				intent = new Intent(m_Context, ClarionControlService.class);
//				serviceConnection = clarionServiceConn;
				break;
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
/*********************************************************************/
/* 新規車載器連携サービス追加時は、intentとserviceConnectionの追加を */
/*********************************************************************/
			default:
				break;
		}

		if( intent != null && serviceConnection != null ){
			try
			{
				m_IsBind = m_Context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
			}
			catch(Exception e)
			{
				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
			}
		}
	}

	public static void UnBind() {
		if( m_Context == null ){
			NaviLog.e(NaviLog.PRINT_LOG_TAG, "UnBind is referred before initialization. ");
			return;
		}

		if( m_IsConnect ){
			try
			{
				mDrivingRegulationService.UnBind();
				m_Context.unbindService( clarionServiceConn );
			}
			catch(Exception e)
			{
				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
			}
			m_IsBind = false;
			m_IsConnect = false;
		}
	}

	public static boolean IsDriving() {
		if( !m_IsConnect || mDrivingRegulationService == null ){
			return false;
		}
		return mDrivingRegulationService.IsDriving();
	}

	public static boolean IsConnect() {
		if( !m_IsConnect || mDrivingRegulationService == null ){
			return false;
		}
		return mDrivingRegulationService.IsConnect();
	}

	/**
     * 全車載器連携を確認し、一つでも連携が出来るか確認する
     * @return true:連携あり false:連携なし
     */
    public static boolean IsAllCheckCooperativeEnv()
    {
    	boolean isReadyOK = false;

		if( m_Context == null ){
			NaviLog.e(NaviLog.PRINT_LOG_TAG, "IsAllCheckCooperativeEnv is referred before initialization. ");
			return isReadyOK;
		}

		for( int nCnt = 0; nCnt < mCooperativeCheck.size(); nCnt++ ){
			if( IsReadyEnv( mCooperativeCheck.get(nCnt) ) ){
				isReadyOK = true;
				break;
			}
		}
    	return isReadyOK;
	}

	/**
     * 連携種別毎に連携環境が整っているか確認する
     * @param nKind 連携種別
     * @return true:インストール済み false:未インストール
     */
    public static boolean IsReadyEnv( int nKind )
    {
    	boolean isReadyOK = false;

		if( m_Context == null ){
			NaviLog.e(NaviLog.PRINT_LOG_TAG, "IsReadyEnv is referred before initialization. ");
			return isReadyOK;
		}

    	switch( nKind ){
    		case DEF_VALUE_GUIDE_CLARION:
    			isReadyOK = CheckSmartAccessApp();
    			break;
/******************************************************************/
/* 新規車載器連携サービス追加時は、連携サービス毎のチェック追加を */
/******************************************************************/
    		case DEF_VALUE_GUIDE_NONE:
    		case DEF_VALUE_GUIDE_OTHER:
    			isReadyOK = true;
    			break;
    		default:
    			break;
    	}
    	return isReadyOK;
    }

    /**
     * アプリケーション「Smart Access」がインストールされているかチェックする
     * @return true:インストール済み false:未インストール
     */
    private static boolean CheckSmartAccessApp()
    {
    	boolean isFind = false;
    	try
    	{
	    	PackageManager manager = m_Context.getPackageManager();
	    	Intent intent = new Intent(Intent.ACTION_MAIN);
	    	intent.addCategory(Intent.CATEGORY_LAUNCHER);
	    	List<ResolveInfo> list = manager.queryIntentActivities(intent, 0);
	    	if(list != null) {
		    	for(ResolveInfo app : list) {
		        	if(app.loadLabel(manager).equals(SMART_ACCESS_APP)){
		        		isFind = true;
		        		break;
		        	}
		    	}
	    	}
    	}
    	catch(Exception e){
    		NaviLog.e(NaviLog.PRINT_LOG_TAG, "Error CheckSmartAccessApp.");
    	}
    	return (isFind);
    }

/***********************************************************/
/* 新規車載器連携サービス追加毎にServiceConnectionの追加を */
/***********************************************************/
	private static ServiceConnection clarionServiceConn = new ServiceConnection(){

		@Override
		public void onServiceConnected(ComponentName name, IBinder service){

// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
//			mDrivingRegulationService = ((ClarionControlService.ClarionControlBinder)service).getService();
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
			try
			{
				mDrivingRegulationService.Init(m_Context);
				mDrivingRegulationService.Bind();
				m_IsConnect = true;

			}
			catch(Exception e)
			{
				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name)
		{
			mDrivingRegulationService = null;
			m_IsConnect = false;
			m_IsBind = false;
		}
	};

	/**
	 * 走行規制判定
	 * @return true:走行規制する false:走行規制
	 */
	public static boolean GetDrivingRegulationFlg()
	{
		boolean bRet = false;

		// 車載器連携=する(コネクト済みの場合のみ実施)
		if(mDrivingRegulationService != null && mRegulationKind != DEF_VALUE_GUIDE_NONE)
		{
			// サービスから走行状態を取得
			bRet = mDrivingRegulationService.IsDriving();
		}
// ADD.2013.12.16 N.Sasao 外部I/F走行規制対応 START
		if( IsDrivingControlLock() ){
			// 外部API連携中は、外部指定の走行規制情報を返す
			bRet = m_bDriving;
		}
// ADD.2013.12.16 N.Sasao 外部I/F走行規制対応  END
		return(bRet);
	}
	/**
	 * 走行規制設定(外部API経由用)
	 * @return true:走行規制する false:走行規制
	 */
	public static void SetDrivingRegulationFlg( boolean bDriving )
	{
		if( IsDrivingControlLock() ){
			m_bDriving = bDriving;
		}
	}

	/**
	 * 走行規制の時、規制メッセージを表示する
	 * @return true:規制中 false:規制でない
	 */
    public static boolean CheckDrivingRegulation()
    {
    	boolean bRet = false;

		if( m_Context == null ){
			NaviLog.e(NaviLog.PRINT_LOG_TAG, "CheckDrivingRegulation is referred before initialization. ");
			return bRet;
		}

    	// 走行規制=する
		if(GetDrivingRegulationFlg())
		{
			// 規制メッセージの表示
			toast.show();
			bRet = true;
		}
		return(bRet);
    }

// ADD 2013.09.17 M.Honma 走行規制 SmartAccess監視 Start -->
    /**
	 * 走行状態を取得して保存する。
	 * MapActivityがForegroundの時はエンジンに通知して再描画する
     */
	private static void checkDrivingSts( boolean bDriving )
    {
		// MapActivityが表示されている
		MapActivity mapActivity = NaviRun.getMapActivity();
		if(mapActivity != null && mapActivity.IsShown())
    	{
			// 地図が描画状態なら行う
			if( !NaviRun.isFirstDrawMap() ){
				// 走行規制フラグを取得、通知して再描画する
			    boolean drivingReg = GetDrivingRegulationFlg();
			    NaviRun.GetNaviRunObj().JNI_NE_SetDrivingRegulation( bDriving );
				NaviLog.v(NaviLog.PRINT_LOG_TAG, "DrivingRegulationManager::checkDrivingSts() JNI_NE_SetDrivingRegulation[" + drivingReg + "]");
			}
		}
	}
	/**
     * 通知領域の表示非表示を切り替える
     * @return なし
     */
	private static void changeScreen( boolean bCon ){
		if( m_Activity != null ){
			int nCurrentFlags = m_Activity.getWindow().getAttributes().flags;
 			if(bCon){
 				// 該当Activityがフルスクリーンではないとき
 				if( (nCurrentFlags & WindowManager.LayoutParams.FLAG_FULLSCREEN) != WindowManager.LayoutParams.FLAG_FULLSCREEN ){
 					// 通知領域の非表示
 					m_Activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
 				}
	 		}
	 		else{
	 			// 該当Activityがフルスクリーンのとき
	 			if( (nCurrentFlags & WindowManager.LayoutParams.FLAG_FULLSCREEN) == WindowManager.LayoutParams.FLAG_FULLSCREEN ){
	 				// 通知領域の表示
	 				m_Activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		        	}
		    	}
	    	}
    	}
	/**
     * 通知領域表示を切り替えるアクティビティを保存する
     * このメソッドはBind前及びUnBindの後に実施すること
     * @return なし
     */
 	public static void updateScreenActivity(Activity activity)
    	{
 		m_Activity = activity;
    	changeScreen( IsConnect() );
		        	}
 	protected static BroadcastReceiver mDrivingRegulationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
/**********************************************************/
/* 新規車載器連携サービスで動作を変えたい場合は処理追加を */
/**********************************************************/
                if (action.equals( ACTION_DRIVING_REGULATION_CONNECT )) {
                    boolean bConnecting = intent.getBooleanExtra(EXTRA_CONNECT, false);
                    // フルスクリーンを戻す処理は連携に関係なく実施
                	if( GetDrivingRegulationGuide() == DEF_VALUE_GUIDE_CLARION || !bConnecting ){
                		changeScreen( bConnecting );
                	}
                }
                if (action.equals( ACTION_DRIVING_REGULATION_DRIVING )) {
                    boolean bDriving = intent.getBooleanExtra(EXTRA_DRIVING, false);
                	if( GetDrivingRegulationGuide() == DEF_VALUE_GUIDE_CLARION ){
                		checkDrivingSts( bDriving );
		    	}
	    	}
    	}
    	}
    };

    /**
     * 走行規制機能制限中か否か
     *
     * @return 結果(true：制御中, false：未制御)
     */
	public static boolean IsDrivingControlLock(){
		return NaviRun.GetNaviRunObj().IsDrivingControlLock();
    }

}
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
