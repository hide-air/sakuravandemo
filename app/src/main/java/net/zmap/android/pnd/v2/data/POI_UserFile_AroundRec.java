/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           POI_UserFile_AroundRec.java
 * Description    周辺検索用
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class POI_UserFile_AroundRec {
    private long m_lIconCode;
    private String m_DispName;
    private long m_lDate;
    private long m_lLon;
    private long m_lLat;
    private long m_lUserFlag;
    private long m_lImport;
    private long m_lRecIndex;
    private long m_lDistance;
    private long m_lGenreIndex;
    private short m_sTime;

    /**
    * Created on 2010/08/06
    * Title:       setM_DispName
    * Description:  表示用名称を設定する
    * @param1  無し
    * @return       void

    * @version        1.0
    */
    public void setM_DispName(String dispName) {
        m_DispName = dispName;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lIconCode
    * Description:  アイコンコードを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lIconCode() {
        return m_lIconCode;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_DispName
    * Description:  表示用名称を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getM_DispName() {
        return m_DispName;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_DispName
    * Description:  日付を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lDate() {
        return m_lDate;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lLon
    * Description:  経度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLon() {
        return m_lLon;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lLat
    * Description:  緯度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLat() {
        return m_lLat;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lUserFlag
    * Description:  ユーザ定義フラグを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lUserFlag() {
        return m_lUserFlag;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lImport
    * Description:  インポートフラグ（インポートから読み出しの時TRUE)を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lImport() {
        return m_lImport;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lRecIndex
    * Description:  ソート前のRecordIndexを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lRecIndex() {
        return m_lRecIndex;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lDistance
    * Description:  基準点からの距離を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lDistance() {
        return m_lDistance;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lGenreIndex
    * Description:  GenreIndexを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lGenreIndex() {
        return m_lGenreIndex;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_sTime
    * Description:  時刻を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public short getM_sTime() {
        return m_sTime;
    }
}
