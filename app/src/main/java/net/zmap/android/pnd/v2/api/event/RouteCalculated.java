package net.zmap.android.pnd.v2.api.event;

import net.zmap.android.pnd.v2.api.navigation.RemainingDistance;
import net.zmap.android.pnd.v2.api.navigation.RouteRequest;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * 探索要求パラメータ
 */
public final class RouteCalculated implements Parcelable {

    /** ルート探索リクエスト情報  */
    RouteRequest mRequest = new RouteRequest();

    /** 目的地までの案内情報 */
    RemainingDistance mRemainingDistance = new RemainingDistance();

    /**
     * コンストラクタ
     */
    public RouteCalculated() {
    }

    private RouteCalculated(Parcel in) {
        mRequest = in.readParcelable(RouteRequest.class.getClassLoader());
        mRemainingDistance = in.readParcelable(RemainingDistance.class.getClassLoader());
    }

    /**
     * ルート探索リクエスト情報 取得
     *
     * @return ルート探索リクエスト情報
     */
    public RouteRequest getRouteRequest() {
        return mRequest;
    }

    /**
     * ルート探索リクエスト情報 設定
     *
     * @param request ルート探索リクエスト情報
     */
    public void setRouteRequest(RouteRequest request) {
        mRequest = new RouteRequest( request );
    }

    /**
     * 目的地までの案内情報の取得
     *
     * @return 目的地までの案内情報
     */
    public RemainingDistance getRemainingDistance() {
        return mRemainingDistance;
    }

    /**
     * 目的地までの案内情報の設定
     *
     * @param remainingDistance
     *            目的地までの案内情報
     */
    public void setRemainingDistance(RemainingDistance remainingDistance) {
        mRemainingDistance = remainingDistance;
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(mRequest, flags);
        out.writeParcelable(mRemainingDistance, flags);
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /* (非 Javadoc)
     *
     */
    public static final Parcelable.Creator<RouteCalculated> CREATOR = new Parcelable.Creator<RouteCalculated>() {
        public RouteCalculated createFromParcel(Parcel in) {
            return new RouteCalculated(in);
        }
        public RouteCalculated[] newArray(int size) {
            return new RouteCalculated[size];
        }
    };
}