package net.zmap.android.pnd.v2.common;

import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.ZNE_GuideRoutePoint;
import net.zmap.android.pnd.v2.data.ZNE_RoutePoint;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/*
 * 案内再開ファイルクラスs
 *
 * このようなファイルができる
-----------------------------------------------
# 車/人モードの別
1
# 探索条件
1
# なし
NULL
# 現在位置 (緯度経度)
508688670,155142143
# 寄り道
NULL
# 経由地 1 情報 (緯度経度、名称、通過情報)
508703882,155146734,北海道 札幌市西区 八軒 7条西,1
# 経由地 2 情報 (緯度経度、名称、通過情報)
508741786,155157612,北海道 札幌市北区 新川 2条,1
# 経由地 3 情報 (緯度経度、名称、通過情報)
508891928,155024211,北海道 札幌市中央区 北2条西,0
# 経由地 4 情報 (緯度経度、名称、通過情報)
487813750,124880625,淀屋橋駅(1号御堂筋線),0
# 経由地 5 情報 (緯度経度、名称、通過情報)
NULL
# 目的地情報 (緯度経度、名称)
512471365,157538680,北海道 旭川市 神楽2条
-----------------------------------------------
 */
public class NaviGuideRestoreFile {
    /*
     * 案内再開ファイル作成
     */
    public void createNaviDataFile(boolean isViaPassed, NaviAppDataPath naviAppDataPath) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviGuideRestoreFile::createNaviDataFile()");

        ZNE_GuideRoutePoint RouteInfo = new ZNE_GuideRoutePoint();
        NaviRun.GetNaviRunObj().JNI_Java_GetGuideRoutePoint(RouteInfo);

        ZNE_RoutePoint[] guidePoint = RouteInfo.getStGuidePoint();
        if (guidePoint == null || guidePoint.length == 0) {
            return;
        }
        int size = guidePoint.length;

        FileWriter fw = null;
        try {
            File file = CommonLib.openFile(naviAppDataPath.getTmpDataPath(), NaviAppDataPath.NAVI_DATA_FILENAME);

            String newLine = "\r\n";
            String nullStr = "NULL";

            fw = new FileWriter(file);

//                      項目              内容
//          (1)車/人モードの別 モードを数値で表現
//          (2)現在位置                       緯度経度
//          (3)目的地情報        緯度経度、名称
//          (4)経由地情報        緯度経度、名称、通過情報
//          (5)探索条件         条件を数値で表現

            //車/人モードの別
            fw.write("# 車/人モードの別" + newLine);
//          fw.write(String.valueOf(RouteInfo.getICarManMode()));
            fw.write(String.valueOf(CommonLib.getNaviMode()));
            fw.write(newLine);

            //探索条件
            fw.write("# 探索条件" + newLine);
            JNIInt searchType = new JNIInt();
            if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_CAR) {
                NaviRun.GetNaviRunObj().JNI_NE_Drv_GetSectionSearchProperty(1, 8, searchType);
            } else {
                NaviRun.GetNaviRunObj().JNI_NE_Man_GetPointSearchProperty(8, searchType);
            }
            fw.write(String.valueOf(searchType.getM_iCommon()));

//            fw.write(String.valueOf(RouteInfo.getISearchType()));
            fw.write(newLine);

            StringBuffer strb = null;
            boolean hasPassed = true;
            int viaPos = 3;
            int startPos = 1;
            for (int i = 0; i < size; i++) {
                strb = new StringBuffer();
                if (guidePoint[i] != null) {
                    // 経由地 通過情報
                    /*
                    if(isViaPassed && hasPassed){
                        if(i >= viaPos && i < size - 1){
                            for(int j = 3;j< size - 1;j++){
                                if(guidePoint[j].getIPassed() == 0){
                                    guidePoint[j].setIPassed(1);
                                    hasPassed = false;
                                    break;
                                }
                            }
                        }
                    }
                    */
                    // add comment
                    if (i == 0) {
                        strb.append("# なし" + newLine);
                    } else if (i == startPos) {
                        strb.append("# 現在位置 (緯度経度)" + newLine);
                    } else if (i == 2) {
                        strb.append("# 寄り道" + newLine);
                    } else if (i >= viaPos && i < size - 1) {
                        strb.append("# 経由地 " + (i - viaPos + 1) + " 情報 (緯度経度、名称、通過情報)" + newLine);
                    } else if (i == size - 1) {
                        strb.append("# 目的地情報 (緯度経度、名称)" + newLine);
                    }

                    if (guidePoint[i].getStPos().getM_lLong() != 0 &&
                            guidePoint[i].getStPos().getM_lLat() != 0) {

                        strb.append((String.valueOf(guidePoint[i].getStPos().getM_lLong())));
                        strb.append(",");
                        strb.append((String.valueOf(guidePoint[i].getStPos().getM_lLat())));
                        //名称
                        if (i > startPos) {
                            strb.append(",");
                            strb.append(guidePoint[i].getSzPointName());
                        }

                        //経由地 通過情報
                        if (i >= viaPos && i < size - 1) {
                            strb.append(",");
                            strb.append(String.valueOf(guidePoint[i].getIPassed()));
                        }
                        //                  strb.append(",");
                        //                  strb.append(String.valueOf(guidePoint[i].getIPointType()));
                    } else {
                        strb.append(nullStr);
                    }
                } else {
                    strb.append(nullStr);
                }

                fw.write(strb.toString());
                fw.write(newLine);
                //NaviLog.d(NaviLog.PRINT_LOG_TAG,"write----" + i + "--[" + strb.toString()+ "]");
            }

            fw.flush();

        } catch (FileNotFoundException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
    				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
                fw = null;
            }
        }
    }

    /*
     * 案内再開データ取得
     */

    boolean bFileExist = false;
    protected int mCarManMode = 0;
    protected int mSearchType = 0;
    ZNE_RoutePoint[] mGuidePoint = null;

    /*
     * 案内再開ファイルが存在するか返す
     */
    public boolean isFileExist() {
        return bFileExist;
    }

    /*
     * ナビモード取得
     *
     * ※readNaviDataFileしてisFileExist()がtrueの時のみ有効
     */
    public int getCarManMode() {
        return mCarManMode;
    }

    /*
     * 探索条件
     *
     * ※readNaviDataFileしてisFileExist()がtrueの時のみ有効
     */
    public int getSearchType() {
        return mSearchType;
    }

    /*
     * ルート地点情報
     *
     * ※readNaviDataFileしてisFileExist()がtrueの時のみ有効
     */
    public ZNE_RoutePoint[] getRoutePoint() {
        return mGuidePoint;
    }

    /*
     * 案内再開ファイル読み込み
     *
     * 不正なファイルで読み込みに失敗した場合は
     * ファイルを削除する。
     *
     * @return true:成功 false:失敗(またはファイルが無い)
     *
     */
    public boolean readNaviDataFile(NaviAppDataPath naviAppDataPath) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviGuideRestoreFile::readNaviDataFile()");

        boolean bSuccesRead = false;
        bFileExist = false;

        if (naviAppDataPath == null) {
            return false;
        }
        String naviDataFile = naviAppDataPath.getNavigationDataFileFullPath();
        bFileExist = CommonLib.fileExists(naviDataFile);

        if (!bFileExist) {
            return false;
        }

        FileReader fr = null;
        BufferedReader bf = null;
        try {

            fr = new FileReader(naviDataFile);
            bf = new BufferedReader(fr);

            String naviType = null;
            String searchType = null;

            naviType = bf.readLine();

            //ナビモード
            if (naviType == null || !naviType.startsWith("#")) {
                throw new IOException();
            }
            naviType = bf.readLine();
            if (naviType == null) {
                throw new IOException();
            }

            //探索条件
            searchType = bf.readLine();
            if (searchType == null || !searchType.startsWith("#")) {
                throw new IOException();
            }
            searchType = bf.readLine();
            if (searchType == null) {
                throw new IOException();
            }

            //ナビモードの読み込み車・人・アロー以外は不正
            final int iCarManMode = Integer.valueOf(naviType);
            if (!Constants.isValidNaviMode(iCarManMode)) {
                throw new NumberFormatException();
            }
            mCarManMode = iCarManMode;

            int iSearchType = Integer.valueOf(searchType);
            mSearchType = iSearchType;

            ZNE_GuideRoutePoint RouteInfo = new ZNE_GuideRoutePoint();
            NaviRun.GetNaviRunObj().JNI_Java_GetGuideRoutePoint(RouteInfo);
            mGuidePoint = RouteInfo.getStGuidePoint();

            int size = mGuidePoint.length;
            JNITwoLong stPos = null;
            String strPointData = null;
            String[] strNaviData = null;
            for (int i = 0; i < size; i++) {

                strPointData = bf.readLine();
                if (strPointData == null || !strPointData.startsWith("#")) {
                    throw new IOException();
                }
                strPointData = bf.readLine();

                if (strPointData == null || strPointData.equalsIgnoreCase("NULL")) {
                    mGuidePoint[i] = null;
                } else {
                    strNaviData = strPointData.split(",");

                    mGuidePoint[i] = new ZNE_RoutePoint();
                    stPos = new JNITwoLong();

                    stPos.setM_lLong(Long.valueOf(strNaviData[0]));
                    stPos.setM_lLat(Long.valueOf(strNaviData[1]));

                    if (i > Constants.NAVI_START_INDEX) {
                        mGuidePoint[i].setSzPointName(strNaviData[2]);
                    }
                    if (i > Constants.NAVI_START_INDEX + 1 && i < Constants.NAVI_DEST_INDEX) {
                        int iPass = Integer.valueOf(strNaviData[3]);
                        mGuidePoint[i].setIPassed(iPass);
                    }

                    mGuidePoint[i].setStPos(stPos);
                }
            }
            bSuccesRead = true;
        } catch (IOException e) {
            bSuccesRead = false;
        } catch (NumberFormatException e) {
            bSuccesRead = false;
        } catch (NullPointerException e) {
            bSuccesRead = false;
        } catch (ArrayIndexOutOfBoundsException e) {
            bSuccesRead = false;
        }
        finally {
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bf != null) {
                try {
                    bf.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (!bSuccesRead) {
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviGuideRestoreFile::readNaviDataFile() failed");
            deleteNaviDataFile(naviAppDataPath);
            bFileExist = false;
        }
        return bFileExist;
    }

    /*
     * 案内再開ファイル削除
     */
    public void deleteNaviDataFile(NaviAppDataPath naviAppDataPath) {
        CommonLib.deleteFile(naviAppDataPath.getTmpDataPath(), NaviAppDataPath.NAVI_DATA_FILENAME);
    }

}
