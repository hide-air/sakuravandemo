package net.zmap.android.pnd.v2.api.map;

/**
 * ZoomLevel 定数定義クラス
 */
public final class ZoomLevel {
    /** 縮尺 100km */
    public static final int KM_100 = 13;
    /** 縮尺 50km */
    public static final int KM_50  = 12;
    /** 縮尺 20km */
    public static final int KM_20  = 11;
    /** 縮尺 10km */
    public static final int KM_10  = 10;
    /** 縮尺 5km */
    public static final int KM_5   = 9;
    /** 縮尺 2km */
    public static final int KM_2   = 8;
    /** 縮尺 1km */
    public static final int KM_1   = 7;
    /** 縮尺 500m */
    public static final int M_500  = 6;
    /** 縮尺 200m */
    public static final int M_200  = 5;
    /** 縮尺 100m */
    public static final int M_100  = 4;
    /** 縮尺 50m */
    public static final int M_50   = 3;
    /** 縮尺 40m */
    public static final int M_40   = 2;
    /** 縮尺 20m */
    public static final int M_20   = 1;
    /** 縮尺 10m */
    public static final int M_10   = 0;

    /**
     * 縮尺レベル妥当性チェック
     *
     * @param zoomLevel 縮尺レベル
     * @return true:妥当 false:不正
     */
    public static boolean validateRange(int zoomLevel) {
        if (zoomLevel != M_10 &&
            zoomLevel != M_20 &&
            zoomLevel != M_40 &&
            zoomLevel != M_50 &&
            zoomLevel != M_100 &&
            zoomLevel != M_200 &&
            zoomLevel != M_500 &&
            zoomLevel != KM_1 &&
            zoomLevel != KM_2 &&
            zoomLevel != KM_5 &&
            zoomLevel != KM_10 &&
            zoomLevel != KM_20 &&
            zoomLevel != KM_50 &&
            zoomLevel != KM_100) {
            return false;
        }
        return true;
    }

    /**
     * 任意スケールに近い縮尺レベルの取得
     *
     * @param scale 任意スケール
     * @return 縮尺レベル
     */
    public static int getNearestLevel(int scale) {
        if (scale < 20) {
            return M_10;
        } else if (20 <= scale && scale < 40) {
            return M_20;
        } else if (40 <= scale && scale < 50) {
            return M_40;
        } else if (50 <= scale && scale < 100) {
            return M_50;
        } else if (100 <= scale && scale < 200) {
            return M_100;
        } else if (200 <= scale && scale < 500) {
            return M_200;
        } else if (500 <= scale && scale < 1000) {
            return M_500;
        } else if (1000 <= scale && scale < 2000) {
            return KM_1;
        } else if (2000 <= scale && scale < 5000) {
            return KM_2;
        } else if (5000 <= scale && scale < 10000) {
            return KM_5;
        } else if (10000 <= scale && scale < 20000) {
            return KM_10;
        } else if (20000 <= scale && scale < 50000) {
            return KM_20;
        } else if (50000 <= scale && scale < 100000) {
            return KM_50;
        }
        return KM_100;
    }

    /* (非Javadoc)
     *
     */
    public static int convertFromGoogleMapsZoomLevel(int googleMapsZoomLevel) {
        switch ( googleMapsZoomLevel )
        {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return KM_100;
            case 7:
                return KM_50;
            case 8:
                return KM_20;
            case 9:
                return KM_10;
            case 10:
                return KM_5;
            case 11:
            case 12:
                return KM_2;
            case 13:
                return KM_1;
            case 14:
                return M_500;
            case 15:
                return M_200;
            case 16:
                return M_100;
            case 17:
                return M_50;
            case 18:
                return M_20;
            default:
                return M_10;
        }
    }
}