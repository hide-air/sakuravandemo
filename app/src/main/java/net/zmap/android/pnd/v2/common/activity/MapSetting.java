/**
 ********************************************************************
 * Copyright (c) 2010.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project        MarketV2
 * File           NaviDetailSetting.java
 * Description    ナビ詳細設定画面
 * Created on     2010/11/24
 *
 ********************************************************************
 */

package net.zmap.android.pnd.v2.common.activity;

// ADD.2013.07.23 N.Sasao 高解像度切り替え対応 START
import android.content.Context;
import android.view.View;
// ADD.2013.07.23 N.Sasao 高解像度切り替え対応  END
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.MapTypeListioner;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollBox;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.SwitchGroup;
import net.zmap.android.pnd.v2.common.view.SwitchGroupListener;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.sakuracust.DebugLogOutput;

/**
 * Created on 2010/12/15
 * <p>
 * Title: 地図設定画面
 * <p>
 * Description
 * <p>
 * author　XuYang version 1.0
 */
public class MapSetting extends MenuBaseActivity {
// ADD.2013.07.23 N.Sasao 高解像度切り替え対応 START
	Context mContext;
// ADD.2013.07.23 N.Sasao 高解像度切り替え対応  END
    LinearLayout oLayout;
    // アイコンサイズ
    SwitchGroup switchIconSize = null;
    // 文字サイズ
    SwitchGroup switchTextSize = null;
    // 一方通行表示
    SwitchGroup switchPassShow = null;
    // ヘディングアップ
    SwitchGroup switchHead = null;
    // フロントワイド
    SwitchGroup switchWide = null;
    // 走行軌跡表示
    SwitchGroup switchTrack = null;
    // 画面の自動回転
    SwitchGroup switchAutoBack = null;
    // 細街路侵入時の地図自動切換え
    SwitchGroup switchAutoChange = null;
    // 初期表示縮尺
    SwitchGroup switchScale = null;
    // 地図色設定
    SwitchGroup switchMapColor = null;
// ADD.2013.07.23 N.Sasao 高解像度切り替え対応 START
    // 解像度の考慮
    SwitchGroup switchGraphicMode = null;
// ADD.2013.07.23 N.Sasao 高解像度切り替え対応  END

    /**
     * 初期化処理
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//Del 2011/09/17 Z01_h_yamada Start -->
//		setMenuTitle(R.drawable.map_setting_title);
//Del 2011/09/17 Z01_h_yamada End <--

//		PageBox oPage = new PageBox(this);
//		oPage.setPadding(20, 10, 10, 10);
//
//		LayoutInflater oInflater = LayoutInflater.from(this);
//		// 画面を追加
//		setMenuTitle(R.drawable.map_setting_title);
//		oLayout = (LinearLayout)oInflater.inflate(R.layout.setting_map, null);
//		InitPage();
//        oPage.addPage(oLayout);
//
//        // page1
//        oLayout = (LinearLayout)oInflater.inflate(R.layout.setting_map1, null);
//        InitPage1();
//		oPage.addPage(oLayout);
//
//		ScrollTool oTool = new ScrollTool(this);
//		oTool.bindView(oPage);
//
//		setViewInOperArea(oTool);
//		setViewInWorkArea(oPage);
// ADD.2013.07.23 N.Sasao 高解像度切り替え対応 START
    	mContext = this;
// ADD.2013.07.23 N.Sasao 高解像度切り替え対応  END

        LayoutInflater oInflater = LayoutInflater.from(this);
        oLayout = (LinearLayout)oInflater.inflate(R.layout.map_setting, null);
        ScrollBox oBox = (ScrollBox)oLayout.findViewById(R.id.map_area);
        InitPage();
        InitPage1();

        ScrollTool oTool = new ScrollTool(this);
        oTool.bindView(oBox);

        setViewInOperArea(oTool);
        setViewInWorkArea(oLayout);
    }

    private void InitPage() {

        switchIconSize = (SwitchGroup)oLayout.findViewById(R.id.switchIconSize);
        if (switchIconSize != null) {
            switchIconSize.setListener(new SwitchGroupListener() {

                @Override
                public void onSelectChange(int wSelect)
                {
                    switch (wSelect) {
                        case 0:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapIconSize(
                                Constants.MapIconSize_Large);
                        break;
                    case 1:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapIconSize(
                                Constants.MapIconSize_Standard);
                        break;
                    case 2:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapIconSize(
                                Constants.MapIconSize_Small);
                        break;
                }
            }
            });
//            switchIconSize.getView(0).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapIconSize(
//                            Constants.MapIconSize_Large);
//                }
//
//            });
//            switchIconSize.getView(1).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapIconSize(
//                            Constants.MapIconSize_Standard);
//                }
//
//            });
//            switchIconSize.getView(2).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapIconSize(
//                            Constants.MapIconSize_Small);
//                }
//
//            });
            JNIInt JIIconSize = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_GetMapIconSize(JIIconSize);
            switch (JIIconSize.getM_iCommon()) {
                case 0:
                    switchIconSize.setSelected(2);
                    break;
                case 1:
                    switchIconSize.setSelected(1);
                    break;
                case 2:
                    switchIconSize.setSelected(0);
                    break;
            }
        }
        switchTextSize = (SwitchGroup)oLayout.findViewById(R.id.switchTextSize);
        if (switchTextSize != null) {
            switchTextSize.setListener(new SwitchGroupListener() {

                @Override
                public void onSelectChange(int wSelect)
                {
                    switch (wSelect) {
                        case 0:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapWordSize(
                                Constants.MapWordSize_Large);
                        break;
                    case 1:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapWordSize(
                                Constants.MapWordSize_Standard);
                        break;
                    case 2:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapWordSize(
                                Constants.MapWordSize_Small);
                        break;
                }
            }
            });
//            switchTextSize.getView(0).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapWordSize(
//                            Constants.MapWordSize_Large);
//                }
//
//            });
//            switchTextSize.getView(1).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapWordSize(
//                            Constants.MapWordSize_Standard);
//                }
//
//            });
//            switchTextSize.getView(2).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapWordSize(
//                            Constants.MapWordSize_Small);
//                }
//
//            });
            JNIInt JIWordSize = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_GetMapWordSize(JIWordSize);
            switch (JIWordSize.getM_iCommon()) {
                case 0:
                    switchTextSize.setSelected(2);
                    break;
                case 1:
                    switchTextSize.setSelected(1);
                    break;
                case 2:
                    switchTextSize.setSelected(0);
                    break;
            }
        }
        switchPassShow = (SwitchGroup)oLayout.findViewById(R.id.switchPassShow);
        if (switchPassShow != null) {
            switchPassShow.setListener(new SwitchGroupListener() {

                @Override
                public void onSelectChange(int wSelect)
                {
                    switch (wSelect) {
                        case 0:
                        NaviRun.GetNaviRunObj().JNI_NE_SetDispMapOneway(
                                Constants.ONEWAY_ON);
                        break;
                    case 1:
                        NaviRun.GetNaviRunObj().JNI_NE_SetDispMapOneway(
                                Constants.ONEWAY_OFF);
                        break;
                }
            }
            });
//            switchPassShow.getView(0).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetDispMapOneway(
//                            Constants.ONEWAY_ON);
//                }
//
//            });
//            switchPassShow.getView(1).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetDispMapOneway(
//                            Constants.ONEWAY_OFF);
//                }
//            });
            JNIInt JIOneway = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_GetDispMapOneway(JIOneway);
            if (JIOneway.getM_iCommon() == 0) {
                switchPassShow.setSelected(1);
            } else {
                switchPassShow.setSelected(0);
            }

        }
        switchHead = (SwitchGroup)oLayout.findViewById(R.id.switchHead);
        if (switchHead != null) {
            switchHead.setListener(new SwitchGroupListener() {

                @Override
                public void onSelectChange(int wSelect)
                {
                    switch (wSelect) {
                        case 0:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapHeadingup(
                                Constants.HEADINGUP_ON);
                        break;
                    case 1:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapHeadingup(
                                Constants.HEADINGUP_OFF);
                        break;
                }
            }
            });
//            switchHead.getView(0).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapHeadingup(
//                            Constants.HEADINGUP_ON);
//                }
//            });
//            switchHead.getView(1).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapHeadingup(
//                            Constants.HEADINGUP_OFF);
//                }
//            });
            JNIInt JIHeadingUp = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_GetMapHeadingup(JIHeadingUp);
            if (JIHeadingUp.getM_iCommon() == 0) {
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
                DebugLogOutput.put("RakuRaku-Log: JNI_NE_GetMapHeadingup->True");
//Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
                
                switchHead.setSelected(1);
            } else {
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
                DebugLogOutput.put("RakuRaku-Log: JNI_NE_GetMapHeadingup->False");
//Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
                switchHead.setSelected(0);
            }

        }
        switchWide = (SwitchGroup)oLayout.findViewById(R.id.switchWide);
        if (switchWide != null) {
            switchWide.setListener(new SwitchGroupListener() {

                @Override
                public void onSelectChange(int wSelect)
                {
                    switch (wSelect) {
                        case 0:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapFrontWide(
                                Constants.FRONT_ON);
                        break;
                    case 1:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapFrontWide(
                                Constants.FRONT_OFF);
                        break;
                }
            }
            });
//            switchWide.getView(0).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapFrontWide(
//                            Constants.FRONT_ON);
//                }
//            });
//            switchWide.getView(1).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapFrontWide(
//                            Constants.FRONT_OFF);
//                }
//            });
            JNIInt JIFrontWide = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_GetMapFrontWide(JIFrontWide);
            if (JIFrontWide.getM_iCommon() == 0) {
                switchWide.setSelected(1);
            } else {
                switchWide.setSelected(0);
            }
        }
    }

    private void InitPage1() {
        switchTrack = (SwitchGroup)oLayout.findViewById(R.id.switchTrack);
        if (switchTrack != null) {
            switchTrack.setListener(new SwitchGroupListener() {

                @Override
                public void onSelectChange(int wSelect)
                {
                    switch (wSelect) {
                        case 0:
                        NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(1);
                        break;
                    case 1:
                        NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(0);
                        break;
                }
            }
            });
//            switchTrack.getView(0).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(1);
//                }
//            });
//            switchTrack.getView(1).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetShowVehicleTrackFlag(0);
//                }
//            });
            JNILong showFlag = new JNILong();
            NaviRun.GetNaviRunObj()
                    .JNI_NE_GetShowVehicleTrackFlag(showFlag);
            if (showFlag.getLcount() == 0) {
                switchTrack.setSelected(1);
            } else {
                switchTrack.setSelected(0);
            }
// Add by CPJsunagawa '2015-07-20 Start  標準の走行軌跡をマスクする　Start
/*
            switchTrack.getView(0).setEnabled(false);
            switchTrack.getView(0).setBackgroundResource(R.drawable.btn_default_non);
            switchTrack.getView(1).setEnabled(false);
            switchTrack.getView(1).setBackgroundResource(R.drawable.btn_default_non);
*/
// Add by CPJsunagawa '2015-07-20 Start  標準の走行軌跡をマスクする　End
        }
        switchAutoBack = (SwitchGroup)oLayout.findViewById(R.id.switchAutoBack);
        if (switchAutoBack != null) {
            switchAutoBack.setListener(new SwitchGroupListener() {

                @Override
                public void onSelectChange(int wSelect)
                {
                    switch (wSelect) {
                        case 0:
                        NaviRun.GetNaviRunObj().JNI_Java_SetSettingInfo(1);
                        break;
                    case 1:
                        NaviRun.GetNaviRunObj().JNI_Java_SetSettingInfo(0);
                        break;
                }
            }
            });
//            switchAutoBack.getView(0).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_Java_SetSettingInfo(1);
//                    Log.v("xy", "JNI_Java_SetSettingInfo(1)" );
//                }
//            });
//            switchAutoBack.getView(1).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_Java_SetSettingInfo(0);
//                    Log.v("xy", "JNI_Java_SetSettingInfo(0)" );
//                }
//            });
            JNIInt SettingInfo = new JNIInt();
            // 徒歩モード、アローモードの場合
            if (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_BICYCLE
                    || CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN) {
                NaviRun.GetNaviRunObj().JNI_Java_GetSettingInfo(SettingInfo);
                switchAutoBack.getView(0).setEnabled(true);
                switchAutoBack.getView(1).setEnabled(true);
                if (SettingInfo.getM_iCommon() == 0) {
                    switchAutoBack.setSelected(1);

                } else {
                    switchAutoBack.setSelected(0);
                }
            } else {
                switchAutoBack.getView(0).setEnabled(false);
                switchAutoBack.getView(0).setBackgroundResource(R.drawable.btn_default_non);
                switchAutoBack.getView(1).setEnabled(false);
                switchAutoBack.getView(1).setBackgroundResource(R.drawable.btn_default_non);
            }
        }

        switchAutoChange = (SwitchGroup)oLayout.findViewById(R.id.switchAutoChange);
        if (switchAutoChange != null) {
            switchAutoChange.setListener(new SwitchGroupListener() {

                @Override
                public void onSelectChange(int wSelect)
                {
                    switch (wSelect) {
                        case 0:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMinorRoad(1);
                        break;
                    case 1:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMinorRoad(0);
                        break;
                }
            }
            });
//            switchAutoChange.getView(0).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMinorRoad(1);
//                }
//            });
//            switchAutoChange.getView(1).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMinorRoad(0);
//                }
//            });
            JNIInt MinorRoad = new JNIInt();
            NaviRun.GetNaviRunObj().JNI_NE_GetMinorRoad(MinorRoad);
            if (MinorRoad.getM_iCommon() == 0) {
                switchAutoChange.setSelected(1);
            } else {
                switchAutoChange.setSelected(0);
            }
        }
// ADD.2013.07.23 N.Sasao 高解像度切り替え対応 START
        // ExConfigで高解像度が有効であった場合、メニューを表示する
        switchGraphicMode = (SwitchGroup)oLayout.findViewById(R.id.switchGraphicMode);
// MOD.2013.07.25 N.Sasao 高解像度HDPI⇒MDPI対応 START
        if( NaviRun.GetNaviRunObj().getMap_HighResolution_OFF() || !NaviRun.GetNaviRunObj().isJudeValidGraphicMode() ){
        	LinearLayout otmpLayout = (LinearLayout) oLayout.findViewById(R.id.LinearGraphicMode);

        	if (otmpLayout != null) {
        		otmpLayout.setVisibility( View.GONE );
        	}
        }
        else{
	        if (switchGraphicMode != null) {
		        switchGraphicMode.setListener(new SwitchGroupListener() {
		            @Override
		            public void onSelectChange(int wSelect)
		            {
		            	float scaleDensity = NaviRun.GetNaviRunObj().getGraphicModeControlScale( false );
		                switch (wSelect) {
		                    case 0:
				            	scaleDensity = NaviRun.GetNaviRunObj().getGraphicModeControlScale( true );
		                    	NaviRun.GetNaviRunObj().JNI_NE_SetGraphicModeScale( scaleDensity );
		                    	NaviRun.GetNaviRunObj().setGraphicMode( true );
		                		break;
		                	case 1:

		                    	NaviRun.GetNaviRunObj().JNI_NE_SetGraphicModeScale( scaleDensity );
		                    	NaviRun.GetNaviRunObj().setGraphicMode( false );
		                    	break;
		            	}
		        	}
		        });
	            if ( NaviRun.GetNaviRunObj().getGraphicMode() ) {
	            	switchGraphicMode.setSelected(0);
	            } else {
	            	switchGraphicMode.setSelected(1);
	            }
	        }
        }
// ADD.2013.07.23 N.Sasao 高解像度切り替え対応  END
// MOD.2013.07.25 N.Sasao 高解像度HDPI⇒MDPI対応  END

        switchScale = (SwitchGroup)oLayout.findViewById(R.id.switchScale);
        if (switchScale != null) {
            switchScale.setListener(new SwitchGroupListener() {

                @Override
                public void onSelectChange(int wSelect)
                {
                    switch (wSelect) {
                        case 0:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapScale(
                                Constants.MAPSCALE_UPPERLEVEL);
                        break;
                    case 1:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapScale(
                                Constants.MAPSCALE_MIDLEVEL);
                        break;
                    case 2:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapScale(
                                Constants.MAPSCALE_LOWERLEVEL);
                        break;
                }
            }
            });
//            switchScale.getView(0).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapScale(
//                            Constants.MAPSCALE_UPPERLEVEL);
//                }
//            });
//            switchScale.getView(1).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapScale(
//                            Constants.MAPSCALE_MIDLEVEL);
//                }
//            });
//            switchScale.getView(2).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapScale(
//                            Constants.MAPSCALE_LOWERLEVEL);
//                }
//            });
            JNIInt MapScale = new JNIInt();
//Add 2011/06/30 Z01thedoanh Start -->
            //詳細地図データがないときは「初期表示縮尺の詳細ボタン」が無効される
//Chg 2011/09/26 Z01yoneya Start -->
//			if(!CommonLib.isBIsExistsDetailMapData()){
//------------------------------------------------------------------------------------------------------------------------------------
            boolean isExistTownMap;
            try {
                isExistTownMap = ((NaviApplication)getApplication()).getNaviAppDataPath().isExistTownMap();
            } catch (NullPointerException e) {
                isExistTownMap = false;
            }
            if (!isExistTownMap) {
//Chg 2011/09/26 Z01yoneya End <--
                switchScale.getView(2).setEnabled(false);
                switchScale.getView(2).setBackgroundResource(R.drawable.btn_default_non);
            }
//Add 2011/06/30 Z01thedoanh End <--

            NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
            switch (MapScale.getM_iCommon()) {
                case Constants.MAPSCALE_UPPERLEVEL:
                    switchScale.setSelected(0);
                    break;
                case Constants.MAPSCALE_MIDLEVEL:
                    switchScale.setSelected(1);
                    break;
                case Constants.MAPSCALE_LOWERLEVEL:
                    switchScale.setSelected(2);
                    break;
            }
        }

        switchMapColor = (SwitchGroup)oLayout.findViewById(R.id.switchMapColor);
        if (switchMapColor != null) {
            switchMapColor.setListener(new SwitchGroupListener() {

                @Override
                public void onSelectChange(int wSelect)
                {
                    switch (wSelect) {
                        case 0:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(
                                Constants.TGL_MAPCOLOR_DAY);
                        MapTypeListioner.isActive = false;
                        NaviRun.GetNaviRunObj().JNI_NE_SetMPMode(
                                Constants.MAPMODE_DAY_OR_NIGHT);
                        break;
                    case 1:
                        NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(
                                Constants.MAPCOLOR_NIGHT);
                        MapTypeListioner.isActive = false;
                        NaviRun.GetNaviRunObj().JNI_NE_SetMPMode(
                                    Constants.MAPMODE_DAY_OR_NIGHT);
                        break;
                    case 2:
                        MapTypeListioner.isActive = false;
                        NaviRun.GetNaviRunObj().JNI_NE_SetMPMode(Constants.MAPMODE_AUTO);
                        if (!CommonLib.isBMapColorAuto()) {
                            JNITwoLong Coordinate = new JNITwoLong();
                            NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);
                            boolean isDay = MapTypeListioner.getClockStsIsDay((int)Coordinate.getM_lLat(), (int)Coordinate.getM_lLong());
                            if (isDay) {
                                NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(
                                            Constants.TGL_MAPCOLOR_DAY);
                            } else {
                                NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(
                                            Constants.MAPCOLOR_NIGHT);
                            }
                            CommonLib.setBMapColorAuto(true);
                            Thread t = new Thread(new MapTypeListioner());
                            MapTypeListioner.isRuning = true;
                            MapTypeListioner.isActive = true;
                            t.start();
                        } else {
                            MapTypeListioner.isActive = true;
                        }
                        break;
                }
            }
            });
//            switchMapColor.getView(0).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(
//                            Constants.TGL_MAPCOLOR_DAY);
//                    MapTypeListioner.isActive = false;
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMPMode(
//                            Constants.MAPMODE_DAY_OR_NIGHT);
//                }
//            });
//            switchMapColor.getView(1).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(
//                            Constants.MAPCOLOR_NIGHT);
//                    MapTypeListioner.isActive = false;
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMPMode(
//                                Constants.MAPMODE_DAY_OR_NIGHT);
//                }
//            });
//            switchMapColor.getView(2).setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    MapTypeListioner.isActive = false;
//                    NaviRun.GetNaviRunObj().JNI_NE_SetMPMode(Constants.MAPMODE_AUTO);
//                    if (!CommonLib.isBMapColorAuto()){
//                        JNITwoLong Coordinate = new JNITwoLong();
//                        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);
//                        boolean isDay = MapTypeListioner.getClockStsIsDay((int)Coordinate.getM_lLat(),(int)Coordinate.getM_lLong());
//                        if (isDay) {
//                                NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(
//                                        Constants.TGL_MAPCOLOR_DAY);
//                        } else {
//                                NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(
//                                        Constants.MAPCOLOR_NIGHT);
//                        }
//                        CommonLib.setBMapColorAuto(true);
//                        Thread t = new Thread(new MapTypeListioner());
//                        MapTypeListioner.isRuning = true;
//                        MapTypeListioner.isActive = true;
//                        t.start();
//                    } else {
//                        MapTypeListioner.isActive = true;
//                    }
//                }
//            });
            JNILong MapMode = new JNILong();
            NaviRun.GetNaviRunObj().JNI_NE_GetMPMode(MapMode);
            if (MapMode.lcount != 0) {
                JNIInt JIDParaMode = new JNIInt();
                NaviRun.GetNaviRunObj().JNI_NE_GetMapDParaMode(JIDParaMode);
                switchMapColor.setSelected(JIDParaMode.getM_iCommon());
            } else {
                switchMapColor.setSelected(Constants.MAPCOLOR_AUTO);
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onClickGoBack();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected boolean onClickGoBack() {
        redrawMap();

        return super.onClickGoBack();
    }

//  XuYang start #926
    @Override
    protected boolean onClickGoMap() {
        redrawMap();

        return super.onClickGoMap();
    }

//     XuYang end #926
    /**
     * 地図は重さを更新してかきます
     */
    private void redrawMap() {
/*
        int ActiveType = 0;
        int TransType = 0;
        byte bMapLevel = 0;
        long Longitude = 0;
        long Latitude = 0;
        JNIByte bApplv = new JNIByte();
        JNITwoLong coordinate = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter( coordinate );
        NaviRun.GetNaviRunObj().JNI_NE_GetMapLevel( bApplv );
        bMapLevel = (byte)bApplv.getM_bScale();
        Longitude = coordinate.getM_lLong();
        Latitude = coordinate.getM_lLat();
        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl( ActiveType, TransType, bMapLevel, Longitude, Latitude );
*/
        NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();
    }

//Add 2011/11/14 Z01_h_yamada Start -->
    protected void onPause() {
    	NaviRun.GetNaviRunObj().JNI_saveConfig();
        super.onPause();
    }
//Add 2011/11/14 Z01_h_yamada End   <--

}
