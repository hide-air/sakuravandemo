package net.zmap.android.pnd.v2.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;

public class NaviStateIcon extends ImageView
{
	public NaviStateIcon(Context oContext)
	{
		super(oContext);
	}

	public NaviStateIcon(Context oContext,AttributeSet oSet)
	{
		super(oContext,oSet);
	}

	public void updateStateIcon()
	{
	    int navi_mode = CommonLib.getNaviCarMode();
	    int resId = R.drawable.menu_car;

        if (Constants.isDebug) {
            if(navi_mode == Constants.NE_NAVIMODE_YCAR){
                resId = R.drawable.menu_car_yellow;
            }
            if(navi_mode == Constants.NE_NAVIMODE_GCAR){
                resId = R.drawable.menu_car_green;
            }
            if(navi_mode == Constants.NE_NAVIMODE_RCAR){
                resId = R.drawable.menu_car_red;
            }
        }
	    if(navi_mode == Constants.NE_NAVIMODE_MAN){
	        resId = R.drawable.menu_walk;
	    }
	    if(navi_mode == Constants.NE_NAVIMODE_BICYCLE){
	        resId = R.drawable.menu_arrow;
	    }
		this.setBackgroundResource(resId);
	}
}
