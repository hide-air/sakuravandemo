//***************************;***************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.data.PoiBaseData;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Mybox_ListGenreName;
import net.zmap.android.pnd.v2.data.POI_UserFile_DataIF;
import net.zmap.android.pnd.v2.data.POI_UserFile_RecordIF;
import net.zmap.android.pnd.v2.inquiry.data.CommonMethd;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.inquiry.view.ButtonImg;
import net.zmap.android.pnd.v2.inquiry.view.ItemView;
import net.zmap.android.pnd.v2.maps.OpenMap;

import java.util.ArrayList;
/**
 * M-F5_お気に入り登録ｼﾞｬﾝﾙ選択
 *
 * */
public class FavoritesAddGenreInquiry extends InquiryBaseLoading {

	private JNITwoLong Coordinate = new JNITwoLong();
	private JNIString AddressString = new JNIString();

	private int recordCount = 50;
	public int KIND_OF_HOUSE = 7;
	private ArrayList<Integer> iconList = new ArrayList<Integer>();
	private ArrayList<Integer> btnIdList = new ArrayList<Integer>();
	private POI_Mybox_ListGenreName[] ListGenreName;
	private String strHomeFlag = "";

	private static final int[] imageList =
		{
		R.id.image_Button_Line_0101
		,R.id.image_Button_Line_0102
		,R.id.image_Button_Line_0103
		,R.id.image_Button_Line_0201
		,R.id.image_Button_Line_0202
		,R.id.image_Button_Line_0203
		,R.id.image_Button_Line_0301
		,R.id.image_Button_Line_0302
		,R.id.image_Button_Line_0303
		};
    /** 現在の接続リクエストのid */
    protected String m_sReqId = null;
    //<--

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		oIntent = getIntent();
		LayoutInflater oInflater = LayoutInflater.from(this);
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base, null);
		Button obtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
		obtn.setVisibility(Button.GONE);
		TextView otitle = (TextView) oLayout.findViewById(R.id.inquiry_title);
		otitle.setText(R.string.genre_choice_title);

		LinearLayout oGroupList = (LinearLayout)oLayout.findViewById(R.id.LinearLayout_list);
		ItemView oview = new ItemView(this,0,R.layout.inquiry_favorites_add);
		initGroupBtn(oview);
		oGroupList.addView(oview.getView(), new LinearLayout.LayoutParams(
		LinearLayout.LayoutParams.FILL_PARENT,
		LinearLayout.LayoutParams.FILL_PARENT));
		setViewInWorkArea(oLayout);

	}
	private void initGroupBtn(ItemView oView) {
		//地図中心のアドレス
		getCurrentAddress();
		//
		initRecordCount();
		initListData();
		setShowData(oView);
	}
	/**
	 * ButtonImg メソッド内で定義されているパディングのリテラル値
	 * @see ButtonImg
	 */
	private static final int iconW = 5;
	private void setShowData(ItemView oView) {
		Button btn = null;
		//該当POIのタイプを取得する
		int allCount = oIntent.getIntExtra(Constants.PARAMS_RECORD_COUNT, 0);
		int iconSize = getResources().getDimensionPixelSize(R.dimen.FAGI_Button_iconsize);
		Bitmap bitmap = null, scaled = null;
		ImageView image = null;
		int		paddingLeft = getResources().getDimensionPixelSize(R.dimen.FAGI_Button_paddingLeft)
				,paddingTop = getResources().getDimensionPixelSize(R.dimen.FAGI_Button_paddingTop)
				,paddingRight = getResources().getDimensionPixelSize(R.dimen.FAGI_Button_paddingRight)
				,PaddingBottom = getResources().getDimensionPixelSize(R.dimen.FAGI_Button_paddingBottom);
		RelativeLayout.LayoutParams layoutParams =
	              new RelativeLayout.LayoutParams( RelativeLayout.LayoutParams.WRAP_CONTENT,  RelativeLayout.LayoutParams.WRAP_CONTENT);
		layoutParams.setMargins(paddingLeft, paddingTop + iconW, paddingRight, PaddingBottom);

		int listSize = iconList.size();

		for (int i = 0 ; i < recordCount; i++) {
			btn = (Button) oView.findViewById(this,btnIdList.get(i));

			final int genrecode = (int) ListGenreName[i].getM_lGenreCode();
			final String genrename = ListGenreName[i].getM_GenreName();

			btn.setPadding( paddingLeft + iconSize + iconW, paddingTop, paddingRight,PaddingBottom);
			if (listSize > i) {
				bitmap =  BitmapFactory.decodeResource(getResources(),iconList.get(i));
				scaled = Bitmap.createScaledBitmap(bitmap, iconSize, iconSize, true);
				image = (ImageView)oView.findViewById(this, imageList[i]);
				image.setImageBitmap(scaled);
				image.setLayoutParams(layoutParams);
			}

			//レイアウト違うですから、取得した内容は全部表示する
			btn.setSingleLine(false);
			//自宅以外は最大500件
			if (recordCount-2 != i && allCount >= Constants.MAX_RECORD_COUNT) {
				btn.setEnabled(false);
			}
			else {
				btn.setEnabled(true);
			}
			btn.setOnClickListener(new OnClickListener(){


			@Override
			public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 お気に入り登録 ｼﾞｬﾝﾙ選択禁止 Start -->
				if(DrivingRegulation.CheckDrivingRegulation()){
					return;
				}
//// ADD 2013.08.08 M.Honma 走行規制 お気に入り登録 ｼﾞｬﾝﾙ選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

				oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, (genrecode-1));
				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, genrename);
				// xuyang modi start bug1750
				String addressName = getResources().getString(R.string.name_null);;
				if (AddressString.getM_StrAddressString() != null) {
				    addressName = AddressString.getM_StrAddressString();
				}
				oIntent.putExtra(Constants.FLAG_TITLE, addressName);

				oIntent.putExtra(Constants.FROM_FLAG_KEY, AppInfo.ID_ACTIVITY_FAVORITESADDGENREINQUIRY);
				if (genrecode == KIND_OF_HOUSE+1) {
					showDialog(Constants.DIALOG_ADD_HOME);
				} else {
				//自宅以外は最大500件
					oIntent.putExtra(Constants.PARAMS_BUTTON_NAME,
							FavoritesAddGenreInquiry.this.getResources().getString(R.string.btn_load));
					oIntent.setClass(FavoritesAddGenreInquiry.this, FavoritesModInquiry.class);
					NaviActivityStarter.startActivityForResult(((Activity)v.getContext()), oIntent,AppInfo.ID_ACTIVITY_FAVORITESMODINQUIRY);
				}
			}});
		}

	}

	protected void showWaitDialog(String sReqId,boolean bIsBack)
    {
        m_sReqId = sReqId;
    }
	protected void showErrorDialog()
    {
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent oIntent) {
		Intent intent = getIntent();
		int poi_type = intent.getIntExtra(AppInfo.FLAG_POI_TYPE, AppInfo.POI_NONE);
		if (poi_type == AppInfo.POI_SPOT) {
			if (resultCode == Activity.RESULT_OK) {
				PoiBaseData oData =  (PoiBaseData)intent.getSerializableExtra(AppInfo.POI_DATA);

                intent.putExtra(Constants.FLAG_FAVORITE_TO_MAP, true);
                NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
                OpenMap.moveMapTo(this, oData, intent, AppInfo.POI_SPOT, Constants.FAVORITE_ADD_REQUEST);
                this.createDialog(Constants.DIALOG_WAIT, -1);
			}

		}
		super.onActivityResult(requestCode, resultCode, intent);
	}
	private void getCurrentAddress() {

		NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(Coordinate);
		NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(Coordinate.getM_lLong(), Coordinate.getM_lLat(),
				AddressString);

	}

	/**
	 * ジャンル種別を取得する
	 *
	 * */
	private void initRecordCount() {
		JNILong getRec = new JNILong();
		NaviRun.GetNaviRunObj().JNI_NE_POIMybox_Clear();
		NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetGenreCount(getRec);
		recordCount = (int)getRec.lcount;

	}
	/**
	 * ジャンル名称を取得する
	 *
	 * */
	private void initListData() {
		ListGenreName = new POI_Mybox_ListGenreName[recordCount];

		for (int i = 0 ; i < recordCount; i++) {
			ListGenreName[i] = new POI_Mybox_ListGenreName();
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecName(i, ListGenreName[i]);

		}
		btnIdList.add(R.id.Button_Line_0101);
		btnIdList.add(R.id.Button_Line_0102);
		btnIdList.add(R.id.Button_Line_0103);
		btnIdList.add(R.id.Button_Line_0201);
		btnIdList.add(R.id.Button_Line_0202);
		btnIdList.add(R.id.Button_Line_0203);
		btnIdList.add(R.id.Button_Line_0301);
		btnIdList.add(R.id.Button_Line_0302);
		btnIdList.add(R.id.Button_Line_0303);

		iconList.add(R.drawable.icon_friend_44_44);
		iconList.add(R.drawable.icon_eat_44_44);
		iconList.add(R.drawable.icon_shopping_44_44);
		iconList.add(R.drawable.icon_interest_44_44);
		iconList.add(R.drawable.icon_sightseeing_44_44);
		iconList.add(R.drawable.icon_work_44_44);
		iconList.add(R.drawable.icon_other_44_44);
		iconList.add(R.drawable.icon_myhome_44_44);

	}
	@Override
	protected Dialog onCreateDialog(final int id) {
		if (id == Constants.DIALOG_ADD_HOME) {
			CustomDialog oDialog = new CustomDialog(this);
			oDialog.setTitle(R.string.dialog_myhome_save_title);
			oDialog.setMessage(R.string.dialog_myhome_save_msg);

			oDialog.addButton(this.getResources().getString(R.string.dialog_myhome_save_button1),new OnClickListener(){

				@Override
				public void onClick(View v) {
					removeDialog(Constants.DIALOG_ADD_HOME);
					oIntent.putExtra(Constants.PARAMS_MYHOME_FLAG, Constants.MYHOME_01);
					strHomeFlag = getResources().getString(R.string.dialog_myhome_save_button1);
					POI_UserFile_RecordIF[] m_Poi_UserFile_Listitem = new POI_UserFile_RecordIF[1];
					JNILong Rec = new JNILong();
			        NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecList(KIND_OF_HOUSE, 0, 1,
			                m_Poi_UserFile_Listitem, Rec);
			        if (Rec.lcount == 0 ) {
			        	saveMyHome();
			        } else {
			        	showDialog(Constants.DIALOG_HOME);
			        }
				}});
			oDialog.addButton(this.getResources().getString(R.string.dialog_myhome_save_button2),new OnClickListener(){

				@Override
				public void onClick(View v) {
					removeDialog(Constants.DIALOG_ADD_HOME);

					oIntent.putExtra(Constants.PARAMS_MYHOME_FLAG, Constants.MYHOME_02);
					strHomeFlag = getResources().getString(R.string.dialog_myhome_save_button2);
					POI_UserFile_RecordIF[] m_Poi_UserFile_Listitem = new POI_UserFile_RecordIF[1];
					JNILong Rec = new JNILong();
					NaviRun.GetNaviRunObj().JNI_NE_POIMybox_GetRecList(
									KIND_OF_HOUSE + 1, 0, 1,
			                m_Poi_UserFile_Listitem, Rec);
			        if (Rec.lcount == 0 ) {
			        	saveMyHome();
			        } else {
			        	showDialog(Constants.DIALOG_HOME2);
			        }

				}});
			oDialog.addButton(R.string.btn_cancel, new OnClickListener(){

				@Override
				public void onClick(View v) {
					removeDialog(Constants.DIALOG_ADD_HOME);

				}});
			return oDialog;

		} else if (id == Constants.DIALOG_HOME2 || id == Constants.DIALOG_HOME) {
			CustomDialog oDialog = new CustomDialog(this);
			oDialog.setTitle(R.string.dialog_home_title);

			strHomeFlag = oIntent.getStringExtra(Constants.PARAMS_MYHOME_FLAG);
			if (strHomeFlag.equals(Constants.MYHOME_01)) {
				strHomeFlag = this.getResources().getString(R.string.dialog_myhome_save_button1);
			} else {
				strHomeFlag = this.getResources().getString(R.string.dialog_myhome_save_button2);
			}
			String msg = CommonMethd.reSetMsg(strHomeFlag, this.getResources().getString(R.string.dialog_home_msg));
			oDialog.setMessage(msg);
			oDialog.addButton(this.getResources().getString(R.string.btn_ok),new OnClickListener(){

				@Override
				public void onClick(View v) {

					removeDialog(id);
					removeMyHome();
					saveMyHome();

				}





				});
			oDialog.addButton(R.string.btn_cancel, new OnClickListener(){

				@Override
				public void onClick(View v) {
					removeDialog(id);

				}});
			return oDialog;
		} else if (id == Constants.DIALOG_MAX_FAVORITES) {
			CustomDialog oDialog = new CustomDialog(this);
			oDialog.setTitle(R.string.favorite_login_over_load_title);
			oDialog.setMessage(R.string.max_favorites_msg);
			oDialog.addButton(R.string.btn_ok, new OnClickListener(){

				@Override
				public void onClick(View v) {
					removeDialog(id);

				}});
			return oDialog;
		}
		return super.onCreateDialog(id);
	}
	private void removeMyHome() {
		if (strHomeFlag.equals(getResources().getString(R.string.dialog_myhome_save_button2))) {
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_RemoveGenreAllRec(KIND_OF_HOUSE+1);
		} else {
			NaviRun.GetNaviRunObj().JNI_NE_POIMybox_RemoveGenreAllRec(KIND_OF_HOUSE);
		}

	}
	private void saveMyHome() {
		createDialog(Constants.DIALOG_WAIT, -1);
		String address = oIntent.getStringExtra(Constants.FLAG_TITLE);
		if (AppInfo.isEmpty(address)) {
			address = strHomeFlag;
		}
		// hangeng add start bug1696
		if (strHomeFlag.equals(getResources().getString(R.string.dialog_myhome_save_button1))) {
			address = getResources().getString(R.string.dialog_myhome_save_button1);
		} else if(strHomeFlag.equals(getResources().getString(R.string.dialog_myhome_save_button2))){
			address = getResources().getString(R.string.dialog_myhome_save_button2);
		}
		// hangeng add end bug1696
		POI_UserFile_RecordIF UserFile_RecordIF = initSaveRecord(address);
		POI_UserFile_DataIF Data = initSaveData(address);
		long lRet = 0;
		if (strHomeFlag.equals(getResources().getString(R.string.dialog_myhome_save_button2))) {
			lRet = NaviRun.GetNaviRunObj().JNI_NE_POIMybox_SetRec(KIND_OF_HOUSE+1, UserFile_RecordIF, 1, Data);
		} else {
			lRet = NaviRun.GetNaviRunObj().JNI_NE_POIMybox_SetRec(KIND_OF_HOUSE, UserFile_RecordIF, 1, Data);
		}
		if ( lRet != NaviRun.NaviEngine.NE_SUCCESS ) {
			closeWarningDialog();
			showDialog(Constants.DIALOG_FILE_WRITE_FAILED);
			return ;
		}

        POIData oData = new POIData();
        oData.m_sName = UserFile_RecordIF.getM_DispName();
        oData.m_sAddress = UserFile_RecordIF.getM_DispName();
        oData.m_wLong = UserFile_RecordIF.getM_lLon();
        oData.m_wLat = UserFile_RecordIF.getM_lLat();
        Intent intent = getIntent();
        if(intent == null){
            intent = new Intent();
        }
        intent.putExtra(Constants.FLAG_FAVORITE_TO_MAP, true);
        OpenMap.moveMapTo(FavoritesAddGenreInquiry.this,oData, intent, AppInfo.POI_LOCAL, Constants.FAVORITE_ADD_REQUEST);

	}

	/**
	 * 保存した対象を初期化する
	 * @param address アドレス
	 * @return POI_UserFile_DataIF データ対象
	 * */
	private POI_UserFile_DataIF initSaveData(String address) {
		POI_UserFile_DataIF Data = new POI_UserFile_DataIF();
		Data.m_sDataIndex = 0;
		Data.m_sDataSize = (short) address.length();
		Data.m_Data = address;
		return Data;
	}

	/**
	 * 保存した対象を初期化する
	 * @param address アドレス
	 * @return POI_UserFile_RecordIF レコード対象
	 * */
	private POI_UserFile_RecordIF initSaveRecord(String address) {
		POI_UserFile_RecordIF UserFile_RecordIF = new POI_UserFile_RecordIF();
		UserFile_RecordIF.m_DispName = address;
		UserFile_RecordIF.m_lDate = 0;
		UserFile_RecordIF.m_lLon = Coordinate.getM_lLong();
		UserFile_RecordIF.m_lLat = Coordinate.getM_lLat();
		UserFile_RecordIF.m_lUserFlag = 0;
		UserFile_RecordIF.m_lImport = 0;
		UserFile_RecordIF.m_lIconCode = 1;
		return UserFile_RecordIF;

	}
	@Override
	protected boolean onStartShowPage() throws Exception {
		return false;
	}
	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {
	}
}
