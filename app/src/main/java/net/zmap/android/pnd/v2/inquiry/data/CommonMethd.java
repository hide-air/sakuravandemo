//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************
package net.zmap.android.pnd.v2.inquiry.data;

import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.utils.NaviLog;

import java.io.UnsupportedEncodingException;
/**
 * 検索クラスの機能共通クラス
 *
 *
 * */
public class CommonMethd {


//	private String[] strFull = {"あ","い","う","え","お",
//			"か","き","く","け","こ",
//			"さ","し","す","せ","そ",
//			"た" ,"ち","つ","て","と",
//			"な","に","ぬ","め","の",
//			"は","ひ","ふ","へ","ほ",
//			"ま","み","む","め","も",
//			"や","ゆ","よ",
//			"ら","り","る","れ","ろ","わ","ん",
//			"ア","イ","ウ","エ","オ",
//			"カ","キ","ク","ケ","コ",
//			"サ","シ","ス","セ","ソ",
//			"タ","チ","ツ","テ","ト",
//			"ナ","ニ","ヌ","ネ","ノ",
//			"マ","ミ","ム","メ","モ",
//			"ハ","ヒ","フ","ヘ","ホ",
//			"や","ゆ","よ",
//			"ら","り","る","れ","ろ","わ","ん"};
//		private String strHalf = {"ｱｲｳｴｵあいうえお"}


	/**
	 * データを整理して、表示する
	 * @param pStr 分割したい
	 * */
	public static String reSetText(String pStr) {
		if (!AppInfo.isEmpty(pStr)) {
			if (pStr.length() > Constants.STRING_MAX_DISPLAY_COUNT) {
				String str = pStr;
				str = "..."
						+ str.substring(str.length()
								- Constants.STRING_MAX_DISPLAY_COUNT + 3, str
								.length());
				// poiTelListItem.setM_Name(str);
				pStr = str;
			}
			return pStr;
		} else {
			return "";
		}
	}

//	/**
//	 * データを整理して、表示する
//	 *
//	 * */
//	public static String reSetText(String pStr,int len) {
//		if (!AppInfo.isEmpty(pStr)) {
//			if (pStr.length() > len) {
//				String str = new String();
//				str = pStr;
//				str = "..."
//						+ str.substring(str.length()
//								- len + 3, str
//								.length());
//				// poiTelListItem.setM_Name(str);
//				pStr = str;
//			}
//			return pStr;
//		} else {
//			return "";
//		}
//	}
	public static String reSetMsg(String repValue1, String allString) {

		return allString.replaceFirst("%0", repValue1);
	}

	public static String reSetMsg(String repValue1, String repValue2,
			String allString) {

		allString = allString.replaceFirst("%1", repValue2);
		return allString.replaceFirst("%0", repValue1);
	}

	public static int reSetlen(String charList) {
		int oValueLen = charList.length();
		int rtnResult = 0;
		for (int i = 0; i < oValueLen; i++) {
			if (charList.substring(i, i + 1).getBytes().length > 1) {
				rtnResult++;
			}
			rtnResult++;
		}
		return rtnResult;
	}

	// 全角から半角までチェンジする
	public static String full2HalfChange(String QJstr)
			throws UnsupportedEncodingException {

		StringBuffer outStrBuf = new StringBuffer("");
		String Tstr = "";
		byte[] b = null;

		for (int i = 0; i < QJstr.length(); i++) {

			Tstr = QJstr.substring(i, i + 1);


			if (Tstr.equals("　")) {
				outStrBuf.append(" ");
				continue;
			}

			b = Tstr.getBytes("unicode");
			NaviLog.d(NaviLog.PRINT_LOG_TAG,"b===" + b.length);
			for (int k = 0 ; k < b.length; k++) {
				NaviLog.d(NaviLog.PRINT_LOG_TAG,"k===" + k + "====b[k]====" + b[k]);
			}
			if (b[3] == -1) {
				NaviLog.d(NaviLog.PRINT_LOG_TAG,"sdasfaja;jalskjf;akdjfa");
				b[2] = (byte) (b[2] + 32);
				b[3] = 0;

				outStrBuf.append(new String(b, "unicode"));
			} else {
				outStrBuf.append(Tstr);
			}

		} // end for.
		NaviLog.d(NaviLog.PRINT_LOG_TAG,"full2HalfChange------>"+outStrBuf.toString());
		return outStrBuf.toString();
	}

	//全角スペース：12288、半角スペース：32

	public static String ToDBC(String input) {
		char[] c = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			NaviLog.d(NaviLog.PRINT_LOG_TAG,"c[i]====" + c[i]);
			if (c[i] == 12288) {
				c[i] = (char) 32;
				continue;
			}
			if (c[i] > 65280 && c[i] < 65375)
				c[i] = (char) (c[i] - 65248);
		}
		return new String(c);
	}

	public static String ToSBC(String input) {
		// 半角から全角までチェンジする
		char[] c = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == 32) {
				c[i] = (char) 12288;
				continue;
			}
			if (c[i] < 127)
				c[i] = (char) (c[i] + 65248);
		}
		return new String(c);
	}

	// 半角から全角までチェンジする
	public static String half2Fullchange(String QJstr)
			throws UnsupportedEncodingException {
		StringBuffer outStrBuf = new StringBuffer("");
		String Tstr = "";
		byte[] b = null;

		for (int i = 0; i < QJstr.length(); i++) {

			Tstr = QJstr.substring(i, i + 1);
			if (Tstr.equals(" ")) {// 半角スペース
				outStrBuf.append(Tstr);
				continue;
			}

			b = Tstr.getBytes("unicode");
			NaviLog.d(NaviLog.PRINT_LOG_TAG,"b===" + b.length);
			for (int k = 0 ; k < b.length-1; k++) {
				NaviLog.d(NaviLog.PRINT_LOG_TAG,"k===" + k + "====b[k]====" + b[k]);
			}
			if (b[3] == 0) { //
				b[2] = (byte) (b[2] - 32);
				b[3] = -1;
				outStrBuf.append(new String(b, "unicode"));
			} else {
				outStrBuf.append(Tstr);
			}

			// if (b[3] != -1) {
			// b[2] = (byte)(b[2] - 32);
			// b[3] = -1;
			// outStrBuf.append( new String(b, "unicode") );
			// } else {
			// outStrBuf.append( Tstr );
			// }

		}

		return outStrBuf.toString();
	}
}
