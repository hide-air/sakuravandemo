/**
 ********************************************************************
 * COPYRIGHT
 ********************************************************************
 *
 * Project        Smart HMI
 * File           XmlUtis.java
 * Description    Xml解析ユーティリティクラス
 * Created on     2011/07/14
 * @author        itoh@cslab.co.jp
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.common.xml;


import net.zmap.android.pnd.v2.common.utils.NaviLog;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class XmlUtils {

    /**
     * writeDocument
     * XML Document ファイル書き込み。
     *
     * @param file
     * @param document
     * @param encode
     * @return
     */
    public static boolean writeDocument(File file, Document document, String encode) {
    	if(!file.exists()){
    		boolean result = file.getParentFile().mkdirs();
    		if (!result) {
                NaviLog.e(NaviLog.PRINT_LOG_TAG, "mkdirs error [" + file.getParentFile() + "]");
                return false;
    		}
    	}

    	Transformer transformer = null;
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            return false;
        }

        transformer.setOutputProperty( OutputKeys.INDENT, "yes");
        transformer.setOutputProperty( OutputKeys.ENCODING, encode);

        try {
            transformer.transform(new DOMSource(document), new StreamResult(file));
        } catch (TransformerException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            return false;
        }

        return true;
    }

    /**
     * writeDocument
     * XML Document ファイル書き込み
     *
     * @param file
     * @param document
     * @param encode
     * @return
     */
    public static boolean readDocument(File file, Document document, String encode) {

    	return true;
    }

    /**
     * FindFirstChildNode
     * Nodeの持つchildノードの中で、名称のキーを持つ最初のNodeを返す
     *
     * @param node :検索対象ノード
     * @param fidNodeName：検索名称
     * @return Node ：検索名称を持つノードリスト
     *                なければnull
     */
    public static Node FindFirstChildNode(Node node, String findNodeName) {

        Node retNode = null;
        NodeList nodes = node.getChildNodes();
        for (int i = 0;; i++) {
            Node childNode = nodes.item(i);
            if (childNode == null) {
                break;
            }

            if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                if (childNode.getNodeName().compareTo( findNodeName ) == 0) {

                    retNode = childNode;
                    break;
                }
            }
        }
        return retNode;
    }

    /**
     * FindFirstChildNode
     * Nodeの持つchildノードの中で、名称のキーを持つ最初のNodeを返す
     *
     * @param node :検索対象ノード
     * @param fidNodeName：検索名称
     * @return Node ：検索名称を持つノードリスト
     *                なければnull
     */
    public static Node FindFirstChildNode(Node node) {
        Node retNode = null;
        NodeList nodes = node.getChildNodes();
        for (int i = 0;; i++) {
            Node childNode = nodes.item(i);
            if (childNode == null) {
                break;
            }

            if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                retNode = childNode;
                break;
            }
        }
        return retNode;

    }


    /**
     * FindChildNodes
     * Nodeの持つchildノードの中で、名称のキーを持つNodeのリストを返す
     *
     * @param node :検索対象ノード
     * @param fidNodeName：検索名称
     * @return Node ：検索名称を持つノードリスト
     *                なければnull
     */
    public static List<Node> FindChildNodes(Node node, String findNodeName) {

    	if(node == null){
    		return null;
    	}
        List<Node> retNodes = null;
        NodeList nodes = node.getChildNodes();
        for (int i = 0;; i++) {
            Node childNode = nodes.item(i);
            if (childNode == null) {
                break;
            }

            if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                if (childNode.getNodeName().compareTo( findNodeName) == 0) {

                    if (retNodes == null) {
                        retNodes = new ArrayList<Node>();
                    }

                    retNodes.add(childNode);
                }
            }
        }
        return retNodes;
    }

    /**
     * getNodeValue
     * Elementから、指定されたキー文字列リストをたどった最初の値を返す
     *
     * @param root :検索対象エレメント
     * @param nodeStr：検索名称リスト
     * @return String ：指定されたキーの値
     *                なければnull
     */
    public static String getNodeValue(Element root , String[] nodeStr) {
        if (root == null) {
            return null;
        }
        Node node = null;
        try {
            if ((node = XmlUtils.FindFirstChildNode(root, nodeStr[0])) != null) {
	        	for(int i = 1; i < nodeStr.length ; i++){
	        		node = FindFirstChildNode(node , nodeStr[i]);
	        		if(node == null){
	        			return null;
	        		}
	        	}
            }
        } catch (NullPointerException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            return null;
        }
    	if  (node != null){
    		return node.getFirstChild().getNodeValue();
    	}
        return null;
    }

    /**
     * getNodeValue
     * Elementから、指定されたキー文字列リストをたどった最初の値を返す
     *
     * @param root :検索対象エレメント
     * @param nodeStr：検索名称リスト
     * @return String ：指定されたキーの値
     *                なければnull
     */
    public static Node getFirstNode(Element root , String[] nodeStr) {
        if (root == null) {
            return null;
        }
        Node node = null;
        try {
            if ((node = XmlUtils.FindFirstChildNode(root, nodeStr[0])) != null) {
	        	for(int i = 1; i < nodeStr.length ; i++){
	        		node = FindFirstChildNode(node , nodeStr[i]);
	        		if(node == null){
	        			return null;
	        		}
	        	}
            }
        } catch (NullPointerException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            return null;
        }

        if  (node != null){
    		return node;
    	}
        return null;
    }

    /**
     * getNodeValue
     * ノードから、指定されたキー文字列リストをたどった最初のノードを返す
     *
     * @param node :検索対象ノード
     * @param nodeStr：検索名称リスト
     * @return Node ：指定されたキーのノード
     *                なければnull
     */
    public static Node getNodeValue(Node node , String[] nodeStr) {
        if (node == null) {
            return null;
        }

        Node returnNode = node;
    	for(int i = 0; i < nodeStr.length ; i++){
    		returnNode = FindFirstChildNode(returnNode , nodeStr[i]);
    		if(returnNode == null){
    			return null;
    		}
    	}

        return returnNode;
    }

    public static void dbg_DispNode(Node node, String depth) {

        switch (node.getNodeType()) {
            case Node.ELEMENT_NODE:
            	NaviLog.e("XML", depth + ": name [" + node.getNodeName() + "] value [" + node.getNodeValue() + "]");
                break;
            default:
                break;
        }

    }

    public static void dbg_DispNodeFamily(Node current, String depth) {
        NodeList nodes = current.getChildNodes();
        for (int i = 0;; i++) {
            Node node = nodes.item(i);
            if (node == null) {
                break;
            }
            dbg_DispNode(node, depth);
            dbg_DispNodeFamily(node, depth + "-");
        }
    }
};