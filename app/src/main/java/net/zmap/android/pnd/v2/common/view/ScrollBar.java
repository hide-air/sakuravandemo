package net.zmap.android.pnd.v2.common.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import net.zmap.android.pnd.v2.R;

public class ScrollBar extends View
{
	private static final float MIN_BAR_HEIGHT = 10.0f;
	private int m_wMax = 0;
	private int m_wPos = 0;
	public ScrollBar(Context oContext)
	{
		super(oContext);

	}

	public ScrollBar(Context oContext,AttributeSet oSet)
	{
		super(oContext,oSet);
	}

	@Override
	protected void onDraw(Canvas oCanvas)
	{
		Paint oPaint = new Paint();
		int wX = getPaddingLeft();
		int wY = getPaddingTop();
		int wW = getWidth() - getPaddingLeft() - getPaddingRight();
		int wH = getHeight() - getPaddingTop() - getPaddingBottom();
		if(m_wMax > 1 && isEnabled())
		{
			float wBarHeight = wH*1.0f / m_wMax;

			boolean isMinHeight = false;
			if( wBarHeight<MIN_BAR_HEIGHT ){
				isMinHeight = true;
				wBarHeight = (wH - MIN_BAR_HEIGHT) / m_wMax;
			}
			float wBottom;

			if(m_wPos == m_wMax - 1)
			{
				wBottom = wY + wH;
			}
			else
			{
				wBottom = wY + wBarHeight * (m_wPos + 1)+(isMinHeight?MIN_BAR_HEIGHT:0);
			}

			oPaint.setStyle(Paint.Style.FILL);
			oPaint.setColor(getResources().getColor(R.color.scroll_tool_bg));
			oCanvas.drawRect(wX, wY, wX + wW,wY + wH, oPaint);

			oPaint.setColor(getResources().getColor(R.color.scroll_bar_in));
			oCanvas.drawRect(wX, wY + wBarHeight * m_wPos, wX + wW, wBottom, oPaint);

			oPaint.setStyle(Paint.Style.STROKE);
			oPaint.setColor(getResources().getColor(R.color.scroll_tool_out));
			oCanvas.drawRect(wX, wY, wX + wW - 1,wY + wH - 1, oPaint);
		}
		else
		{
			oPaint.setStyle(Paint.Style.FILL);
			oPaint.setColor(getResources().getColor(R.color.scroll_tool_dis_bg));
			oCanvas.drawRect(wX, wY, wX + wW,wY + wH, oPaint);

			oPaint.setStyle(Paint.Style.STROKE);
			oPaint.setColor(getResources().getColor(R.color.scroll_tool_out));
			oCanvas.drawRect(wX, wY, wX + wW - 1,wY + wH - 1, oPaint);
		}
		super.onDraw(oCanvas);
	}

	public void setMax(int wMax)
	{
		m_wMax = wMax;
		postInvalidate();
	}

	public void setPos(int wPos)
	{
		m_wPos = wPos;
		postInvalidate();
	}
}
