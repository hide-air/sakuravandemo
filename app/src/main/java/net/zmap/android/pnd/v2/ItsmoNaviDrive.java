package net.zmap.android.pnd.v2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.view.Window;
import android.widget.Toast;
import net.zmap.android.pnd.v2.app.NaviAppStatus;
import net.zmap.android.pnd.v2.app.control.NaviControllIntent;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.activity.BaseActivity;
import net.zmap.android.pnd.v2.common.activity.NotificationActivity;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.maps.OpenMap;
import net.zmap.android.pnd.v2.sakuracust.DebugLogOutput;

/*
 * メインアクティビティクラス
 *
 * このアクティビティ自身がスプラッシュ画面になる
 *
 */
public class ItsmoNaviDrive extends Activity {
// ADD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応 START
	private static final int DEF_FINISH_DELAYED = 300;
	private static Handler 	mHandler;
    private Dialog splashDialog = null;
// ADD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応  END

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        NaviLog.v(NaviLog.PRINT_LOG_TAG, "start navigation system.");
        super.onCreate(savedInstanceState);
// ADD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応 START
        initEventHandler();
// ADD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応  END

        //ナビアプリが起動済みなら起動しない
//Chg 2011/10/10 Z01yoneya Start -->
//        if (NaviAppStatus.isAlreadyLaunch()) {
//            finish();
//            return;
//        }
//
//        NaviAppStatus.setAppLaunchFlag(true);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "ItsmoNaviDrive::onCreate NaviAppIsStart=" + NaviAppStatus.isAppStart());

        Intent intent = getIntent();
        String action = intent.getAction();
        NaviLog.i(NaviLog.PRINT_LOG_TAG, "action = " + action);
        if(NaviControllIntent.ACTION_NAVI_APP_FINISH.equals(action)) {
            if (!NaviAppStatus.isAppRunning()) {
                NaviLog.i(NaviLog.PRINT_LOG_TAG, "app is not running.");
// MOD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応 START
                noticeFinish();
// MOD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応  END
                return;
            }
            if (NaviRun.isNaviRunInitial()) {
                NaviRun.GetNaviRunObj().finishApp();
            }
            NaviApplication app = ((NaviApplication)getApplication());
            app.stopNaviService();
// MOD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応 START
            noticeFinish();
// MOD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応  END
            return;
        }

        NaviApplication app = ((NaviApplication)getApplication());

        NaviLog.d(NaviLog.PRINT_LOG_TAG, "ItsmoNaviDrive::onCreate intent.action[" + action + "]");
        if (CommonLib.isExternalAction(intent)) {
            ((NaviApplication)getApplication()).enqueueExternalApiIntent(intent);
        }

        // ナビアプリが未起動
        if (!NaviAppStatus.isAppStart()) {
            NaviAppStatus.setAppStatus(NaviAppStatus.APP_STATE_STARTING);
        }
        // ナビアプリが起動途中
        else if (((NaviApplication)getApplication()).getBaseActivityStack().getActivityCount() == 0) {
            startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
// MOD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応 START
            noticeFinish();
// MOD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応  END
            return;
        }
        // ナビアプリが起動済み
        else {
            if (Constants.INTENT_VIEW_ACTION.equals(action)) {
                BaseActivity topActivity = ((NaviApplication)getApplication()).getBaseActivityStack().getTopActivity();
                if (topActivity instanceof MapActivity) {
                    ((MapActivity)topActivity).doExternalAction();
                }
            }
            else if (Constants.INTENT_NAVI_ACTION.equals(action)) {
                BaseActivity topActivity = ((NaviApplication)getApplication()).getBaseActivityStack().getTopActivity();
                if (topActivity instanceof MapActivity) {
                    OpenMap.locationBack((MapActivity)topActivity);
                    ((MapActivity)topActivity).goLocation();
                }
                CommonLib.goCarLocation(topActivity);
                MapActivity mapActivity = NaviRun.GetNaviRunObj().getMapActivity();
                mapActivity.doExternalAction();
            }
// MOD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応 START
            noticeFinish();
// MOD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応  END
            return;
        }
//Chg 2011/10/10 Z01yoneya End <--

//Del 2011/10/12 Z01yoneya Start -->
//        showSplash();
//Del 2011/10/12 Z01yoneya End <--

// MOD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応 START
        try {
            app.initializeNaviApp(this);
        } catch (NullPointerException e) {
            //ナビが起動できないので終了
        	noticeFinish();
            return;
        }
        // スプラッシュを表示する
        // styleによる常時スプラッシュ表示から、必要に応じてスプラッシュを表示するように変更
        showSplashDialog();
// MOD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応  END
        app.startNaviLaunchService(null);
// DEL.2013.11.20 N.Sasao 起動速度改善

// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
        DebugLogOutput.put("RakuRaku-Log Start");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end

// Add 2014/12/25 mitsuoka 特定ファイル作成 start-->
/*
        try {
        	FileOutputStream fos = openFileOutput("Debug_Log", MODE_PRIVATE);
        	OutputStreamWriter osw = new OutputStreamWriter(fos);
        	BufferedWriter writer = new BufferedWriter(osw);
        	writer.write("Debug Start");
        	writer.close();
        	//Toast.makeText(this, "Logファイルを作成しました", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
        	Toast.makeText(this, "Logファイルの作成に失敗しました\n" + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        try {
        	FileOutputStream fos = openFileOutput("XPiFK28R", MODE_PRIVATE);
        	OutputStreamWriter osw = new OutputStreamWriter(fos);
        	BufferedWriter writer = new BufferedWriter(osw);
        	writer.write("5E7xnDWQ");
        	writer.close();
        	Toast.makeText(this, "特定ファイルを作成しました", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
        	Toast.makeText(this, "特定ファイルの作成に失敗しました\n" + e.getMessage(), Toast.LENGTH_LONG).show();
        }
*/
// Add 2014/12/25 mitsuoka 特定ファイル作成 <--end

/*  2015-02-02  一次的にコメント  
// Add 2014/12/25 mitsuoka 特定ファイルチェック start-->
        try {
        	FileInputStream fis = openFileInput("XPiFK28R");
        	InputStreamReader isr = new InputStreamReader(fis);
        	BufferedReader reader = new BufferedReader(isr);
        	String text = reader.readLine();
        	
        	if (text.equals("5E7xnDWQ")) {
        		//Toast.makeText(this, "一致", Toast.LENGTH_LONG).show();
        	}
        	else {
        		//Toast.makeText(this, "不一致", Toast.LENGTH_LONG).show();
        		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            	Intent notificationIntent = new Intent(this, ItsmoNaviDrive.class);
            	PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
            	Notification notification = new NotificationCompat.Builder(this)
            		.setSmallIcon(R.drawable.ic_notification)    // アイコン
            		.setTicker("Lock")                      // 通知バーに表示する簡易メッセージ
            		.setWhen(System.currentTimeMillis())     // 時間
            		.setContentTitle(this.getString(R.string.notification_app_name))      // 展開メッセージのタイトル
            		.setContentText("アプリケーションがロックされています。")  // 展開メッセージの詳細メッセージ
            		.setContentIntent(contentIntent)         // PendingIntent
            		.build();
            	notificationManager.notify(1, notification);
            	
            	finish();
            	android.os.Process.killProcess(android.os.Process.myPid());
        	}
        } catch (FileNotFoundException e) {
        	//Toast.makeText(this, "不正利用のためアプリを起動できません", Toast.LENGTH_LONG).show();
        	NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        	Intent notificationIntent = new Intent(this, ItsmoNaviDrive.class);
        	PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        	Notification notification = new NotificationCompat.Builder(this)
        		.setSmallIcon(R.drawable.ic_notification)    // アイコン
        		.setTicker("Lock")                      // 通知バーに表示する簡易メッセージ
        		.setWhen(System.currentTimeMillis())     // 時間
        		.setContentTitle(this.getString(R.string.notification_app_name))      // 展開メッセージのタイトル
        		.setContentText("アプリケーションがロックされています。")  // 展開メッセージの詳細メッセージ
        		.setContentIntent(contentIntent)         // PendingIntent
        		.build();
        	notificationManager.notify(1, notification);
        	
        	finish();
        	android.os.Process.killProcess(android.os.Process.myPid());
        } catch (IOException e) {
        	//Toast.makeText(this, "予期せぬエラー", Toast.LENGTH_LONG).show();
        	finish();
        	android.os.Process.killProcess(android.os.Process.myPid());
        }
// Add 2014/12/25 mitsuoka 特定ファイルチェック <--end
  2015-02-02  一次的にコメント　　*/

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "ItsmoNaviDrive::onDestroy");

//Add 2012/02/22 Z01_h_yamada Start --> #3790
        if (NaviAppStatus.isAppFinishing() && CommonLib.getStopServiceFlag()) {
        	// 終了中に再起動された場合に、中途半端にインスタンスが再利用されて、おかしな状態となる場合があり、復帰できなくなる対策
        	// ２重起動検知で、ItsmoNaviDrive::finish後のdestoryで、プロセスの終了を要求する
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "ItsmoNaviDrive::onDestroy() kill process");
            ActivityManager manager = (ActivityManager)getSystemService("activity");
            if (manager != null) {
                manager.killBackgroundProcesses(getPackageName());
            }
        }
// ADD.2013.12.17 N.Sasao 起動時のメモリーリーク対応 START
        removeSplashDialog();
// ADD.2013.12.17 N.Sasao 起動時のメモリーリーク対応  END
       
//Add 2012/02/22 Z01_h_yamada End <--

//Del 2011/10/12 Z01yoneya Start -->
//        if (splashDialog != null) {
//            splashDialog.dismiss();
//            splashDialog = null;
//        }
//Del 2011/10/12 Z01yoneya End <--
    }

//Del 2011/10/12 Z01yoneya Start -->
//    private Dialog splashDialog;
//
//    /*
//     * スプラッシュ画面表示
//     */
//    public void showSplash() {
//        try {
//            if (splashDialog == null) {
//                //splash only
//                splashDialog = new Dialog(this, R.style.dialog_fullscreen);
//                Window oWindow = splashDialog.getWindow();
//                if (oWindow != null) {
//                    oWindow.setBackgroundDrawableResource(R.drawable.splash);
//                }
//            }
//            splashDialog.show();
//            NaviLog.d(NaviLog.PRINT_LOG_TAG, "ItsmoNaviDrive::showSplashDialog");
//
//        } catch (NullPointerException e) {
//            //なにもしない
//        }
//    }
//Del 2011/10/12 Z01yoneya End <--

// MOD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応 START
    /**
     * 自身終了処理
     */
    private void noticeFinish(){
    	mHandler.sendEmptyMessageDelayed(0, DEF_FINISH_DELAYED);
    }
	/**
	 * ハンドル作成
	 */
	private void initEventHandler(){
		mHandler = new Handler() {
            public void handleMessage(final Message msg) {
            	// onCreate内でのfinishはプロセスが終了しない。
            	// 終了トリガーがないため、メッセージディレイによって
            	// finishを実施する。
            	ItsmoNaviDrive.this.finish();
            }
		};
	}
// MOD.2013.12.17 N.Sasao 起動時のメモリーリーク対応 START
    /**
     * スプラッシュ表示処理
     */
	private void showSplashDialog() {
		if( splashDialog == null ){
	        //splash only
	        splashDialog = new Dialog(this, R.style.dialog_fullscreen);
	        Window oWindow = splashDialog.getWindow();
	        if (oWindow != null) {
	            oWindow.setBackgroundDrawableResource(R.drawable.splash);
	        }
	        splashDialog.show();
		}
    }

    /**
     * スプラッシュ削除処理
     */
	private void removeSplashDialog() {
        //splash only
        if( splashDialog != null ){
        	splashDialog.cancel();
        	splashDialog.dismiss();
        	splashDialog = null;
        }
    }
// MOD.2013.12.17 N.Sasao 起動時のメモリーリーク対応  END
// MOD.2013.12.11 N.Sasao 外部API(wakeup関連)経由後、アプリが正常終了しない対応  END
}