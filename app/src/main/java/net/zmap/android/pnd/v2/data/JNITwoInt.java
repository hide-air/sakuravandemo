/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           JNITwoInt.java
 * Description    2��long�^�̃f�[�^��class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class JNITwoInt {
    private int m_iSizeX;
    private int m_iSizeY;
    /**
    * Created on 2010/08/06
    * Title:       getM_iSizeX
    * Description:  第一個のデータを取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getM_iSizeX() {
        return m_iSizeX;
    }
    /**
    * Created on 2010/08/06
    * Title:       getM_iSizeX
    * Description:  第2のデータを取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getM_iSizeY() {
        return m_iSizeY;
    }
}
