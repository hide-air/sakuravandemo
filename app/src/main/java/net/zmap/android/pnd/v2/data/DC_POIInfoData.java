package net.zmap.android.pnd.v2.data;

import java.io.Serializable;

public class DC_POIInfoData implements Serializable {
	private static final long serialVersionUID = 229836352782008677L;

	public long				m_lMeshNum;				//メッシュ番号
	public String			m_sPOIName;				//POI名称
	public long 			m_lnLongitude1000;		//経度
	public long 			m_lnLatitude1000;		//緯度
	public long				m_POIId;				//POIのID
	public String			m_sIconPath;			//アイコンパス
	public int 				m_nX = 0;				//アイコンホットスポットX座標
	public int 				m_nY = 0;				//アイコンホットスポットY座標
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
	public String			m_sReqURL;				//POI詳細情報を取得するURL
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
}