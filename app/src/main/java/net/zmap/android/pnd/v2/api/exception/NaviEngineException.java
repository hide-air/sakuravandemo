package net.zmap.android.pnd.v2.api.exception;

/**
 * ナビエンジン (C モジュール) 関連例外
 */
public class NaviEngineException extends NaviException {
    private static final long serialVersionUID = 1L;

    /**
     * エラー詳細メッセージとして null を設定して NaviEngineException を構築します。
     */
    public NaviEngineException() {
        super();
    }

    /**
     * 詳細メッセージを指定して NaviEngineException を構築します。
     *
     * @param message
     *            詳細メッセージ
     */
    public NaviEngineException(String message) {
        super(message);
    }
}