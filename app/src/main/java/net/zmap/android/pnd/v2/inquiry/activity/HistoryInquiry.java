//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_UserFile_RecordIF;

/**
 * K-H0_履歴一覧
 * */
public class HistoryInquiry extends HistoryInquiryBase {

	/** カレント表示した内容 */
	private POI_UserFile_RecordIF[] m_Poi_UserFile_Listitem = null;
	private POI_UserFile_RecordIF[] m_Poi_Show_Listitem = null;


	@Override
	protected void getRecList(long RecIndex) {
		NaviRun.GetNaviRunObj().JNI_NE_POIHist_GetRecList(RecIndex, ONCE_GET_COUNT,
				m_Poi_UserFile_Listitem, Rec);
	}

	protected void initOnceGetList() {
		m_Poi_UserFile_Listitem = new POI_UserFile_RecordIF[ONCE_GET_COUNT];

		for (int i = 0; i < ONCE_GET_COUNT; i++) {
			m_Poi_UserFile_Listitem[i] = new POI_UserFile_RecordIF();
		}

	}

	protected void resetShowList(Long lRecIndex,JNILong Rec) {
		if (lRecIndex < ONCE_GET_COUNT) {
			int len = m_Poi_UserFile_Listitem.length;
			for (int i = 0 ; i < len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_UserFile_Listitem[i];
			}
		} else {
			if (lRecIndex >= ONCE_GET_COUNT*2) {
				for (int i = m_Poi_UserFile_Listitem.length-ONCE_GET_COUNT; i < ONCE_GET_COUNT*2 ; i++) {
					if (i <ONCE_GET_COUNT) {
						m_Poi_Show_Listitem[i] = m_Poi_Show_Listitem[i+ONCE_GET_COUNT];
					} else {
						m_Poi_Show_Listitem[i] = new POI_UserFile_RecordIF();
					}
				}
			}

			int len = (int)Rec.lcount;
			int j = 0;
			for (int i = ONCE_GET_COUNT ; i < ONCE_GET_COUNT + len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_UserFile_Listitem[j];
				j++;
			}
		}

	}



	protected void initCount() {

		//目的地
		NaviRun.GetNaviRunObj().JNI_NE_POIHist_GetRecCount(RecCount);
		showToast(this,RecCount);
		allRecordCount = initPageIndexAndCnt(RecCount);
	}

	/**
	 * メモリを申請する
	 * */
	protected void initResultObj() {

		initOnceGetList();
		m_Poi_Show_Listitem = new POI_UserFile_RecordIF[ONCE_GET_COUNT*2];
		for (int i = 0 ; i < ONCE_GET_COUNT*2 ; i++) {
			m_Poi_Show_Listitem[i] = new POI_UserFile_RecordIF();
		}
	}

	protected String getShowObjName(int wPos) {

		return m_Poi_Show_Listitem[wPos].m_DispName;
	}
	protected long getShowObjLon(int wPos) {

		return m_Poi_Show_Listitem[wPos].m_lLon;
	}
	protected long getShowObjLat(int wPos) {

		return m_Poi_Show_Listitem[wPos].m_lLat;
	}
	protected long getShowObjDate(int wPos) {

		return m_Poi_Show_Listitem[wPos].m_lDate;
	}
}
