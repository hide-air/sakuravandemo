package net.zmap.android.pnd.v2.common.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.activity.LocationInfo;
import net.zmap.android.pnd.v2.data.NaviRun;

import java.util.ArrayList;
import java.util.List;

public class CustomDialog extends Dialog {
    private Context m_oContext;
    private TextView m_oTitle = null;
    private TextView m_oMessage = null;
//	private LinearLayout m_oButtonArea = null;
    private List<Button> oButtons = new ArrayList<Button>();
    private int count = 0;
    private boolean isShowProGressBar = false;
    private ProgressBar progressBar;
    // XuYang add start 走行中の操作制限
    private boolean dialogCloseFlag = true;

    // XuYang add end 走行中の操作制限
    public CustomDialog(Context oContext) {
        super(oContext);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        m_oContext = oContext;

        setContentView(R.layout.base_dialog);
//Add 2011/07/25 Z01thedoanh Start -->
        if (CommonLib.getIsFullScreeen() == 0) {
            DisplayMetrics display = new DisplayMetrics();
            ((Activity)oContext).getWindowManager().getDefaultDisplay().getMetrics(display);

            WindowManager.LayoutParams params = getWindow().getAttributes();
            params.x = ((int)CommonLib.screenWidth / 2 - (int)display.widthPixels / 2);
            params.y = ((int)CommonLib.screenHeight / 2 - (int)display.heightPixels / 2);
            this.getWindow().setAttributes(params);
        }
//Add 2011/07/25 Z01thedoanh End <--

        m_oTitle = (TextView)findViewById(R.id.title);
        m_oMessage = (TextView)findViewById(R.id.context);
//		m_oButtonArea = (LinearLayout)findViewById(R.id.btnArea);
// 手書きメモ Start
        mContentView = (LinearLayout)findViewById(R.id.context_view);
// 手書きメモ End

        progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        oButtons.add((Button)findViewById(R.id.btn_1));
        oButtons.add((Button)findViewById(R.id.btn_2));
        oButtons.add((Button)findViewById(R.id.btn_3));
        oButtons.add((Button)findViewById(R.id.btn_4));
    }

    public void setTitle(String sTitle) {
        if (m_oTitle != null && sTitle != null) {
            m_oTitle.setText(sTitle);
        }
    }

    public void setTitle(int wId) {
        m_oTitle.setText(wId);
    }

    public void setMessage(String sMsg) {
        if (m_oMessage != null && sMsg != null) {
            m_oMessage.setText(sMsg);
        }
    }

    public void setMessage(String sMsg, int gravity) {
        if (m_oMessage != null && sMsg != null) {
            m_oMessage.setText(sMsg);
            m_oMessage.setGravity(gravity);
        }
    }

    public void setMessage(int wId) {
        m_oMessage.setText(wId);
    }

    public Button addButton(String sText, android.view.View.OnClickListener oListener) {
//		if(m_oButtonArea != null && sText != null && oListener != null)
//		{
        Button oButton = createButton();
        ;
        oButton.setText(sText);
        oButton.setOnClickListener(oListener);
//			m_oButtonArea.addView(oButton);
//		}
        return oButton;
    }

    public Button addButton(int wId, android.view.View.OnClickListener oListener) {
//		if(m_oButtonArea != null && oListener != null)
//		{
        Button oButton = createButton();
        ;
        oButton.setText(wId);
        oButton.setOnClickListener(oListener);
//			m_oButtonArea.addView(oButton);
//		}
        return oButton;
    }

    public Button addCancelButton(int wId) {
//		if(m_oButtonArea != null)
//		{
        Button oButton = createButton();
        oButton.setText(wId);
        oButton.setOnClickListener(new android.view.View.OnClickListener() {

            @Override
            public void onClick(View v)
                {
                    dismiss();
                }

        });
//			m_oButtonArea.addView(oButton);
//		}
        return oButton;
    }

    public Button addCancelButton(String text) {
//		if(m_oButtonArea != null)
//		{
        Button oButton = createButton();
        oButton.setText(text);
        oButton.setOnClickListener(new android.view.View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                dismiss();
            }

        });
//			m_oButtonArea.addView(oButton);
//		}
        return oButton;
    }

    private Button createButton() {
        Button oButton = oButtons.get(count);
        if (oButton == null) {
            oButton = new Button(getContext());
        }
        count++;
//	    if((count >=2) && (!ScreenAdapter.isLandMode())){
//            count ++;
//        }
        oButton.setVisibility(View.VISIBLE);

        return oButton;
    }

//	public void addButton(Button oButton)
//	{
//		if(m_oButtonArea != null && oButton != null)
//		{
//			m_oButtonArea.addView(oButton);
//		}
//	}

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_SEARCH) {
            return true;
        }
        // XuYang add start 走行中
//	    // 走行／停止
//		if (keyCode == KeyEvent.KEYCODE_R && event.getAction() == KeyEvent.ACTION_DOWN) {
//			Constants.RunStopflag = true;
//		}
//		if (keyCode == KeyEvent.KEYCODE_S && event.getAction() == KeyEvent.ACTION_DOWN) {
//			Constants.RunStopflag = false;
//		}

        // クレードル（車載ホルダ）
//	    if (keyCode == KeyEvent.KEYCODE_O && event.getAction() == KeyEvent.ACTION_DOWN) {
//            Constants.NaviOnOffflag = true;
//            SensorDataManager.onCarMountStatusChange(Constants.NaviOnOffflag);
//            int naviMode = CommonLib.getNaviMode();
//            dialogCloseFlag = true;
//            if(naviMode != Constants.NE_NAVIMODE_CAR) {
//                dialogCloseFlag = false;
//                showDialog();
//            }
//        }
//        if (keyCode == KeyEvent.KEYCODE_F && event.getAction() == KeyEvent.ACTION_DOWN) {
//            Constants.NaviOnOffflag = false;
//            SensorDataManager.onCarMountStatusChange(Constants.NaviOnOffflag);
//        }

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            if (dialogCloseFlag) {
                systemLimit();
            }
        }
        // XuYang add end 走行中
        return super.onKeyDown(keyCode, event);
    }

    private void showDialog() {
        final CustomDialog dialog = new CustomDialog(m_oContext);
        dialog.setTitle(R.string.txt_navi_mode_change_title);
        dialog.setMessage(R.string.mode_change_to_car_msg);
        dialog.addButton(R.string.btn_ok,
                new android.view.View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();
                        dialog.cancel();
                        // 案内中であるかどうかを判断する。
                        if (CommonLib.isNaviStart()) {
                            CommonLib.setBIsRouteDisplay(true);
                            NaviRun.GetNaviRunObj().JNI_Java_ct_ChangeNaviMode(Constants.NE_NAVIMODE_CAR);
                            NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
                        } else {
                            CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
                        }
                        systemLimit();
                    }
                });
        dialog.show();
    }

    // XuYang add start 走行中
    private void systemLimit() {
        if (CommonLib.isLimit()) {
            goLocationInfo();
        }
    }

    public void goLocationInfo() {
        Intent intent = new Intent();
        intent.setClass(m_oContext, LocationInfo.class);
        m_oContext.startActivity(intent);
    }

    // XuYang add end 走行中

    private Runnable updateThread = new Runnable() {
        @Override
        public void run() {

            if (progressBar != null) {
                if (isShowProGressBar) {
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }
            }
        }

    };

    public void setShowProGressBar(final boolean isShowProGressBar) {
        this.isShowProGressBar = isShowProGressBar;
        new Handler().post(updateThread);
    }
// 手書きメモ Start
    private LinearLayout mContentView;
    public void setContentView(View v) {
        m_oMessage.setVisibility(View.GONE);
        mContentView.setVisibility(View.VISIBLE);
        mContentView.removeAllViews();
        mContentView.addView(v);
    }

    public Button addButton(String sText, int textSize, android.view.View.OnClickListener oListener) {
		Button oButton = createButton();
        oButton.setText(sText);
        oButton.setOnClickListener(oListener);
        oButton.setTextSize(textSize);
        return oButton;
    }

    public void EnableTitleIcon(boolean flag) {
        View v = findViewById(R.id.icon);
        if(v == null) return; // fail safe
        if( flag ) {
            v.setVisibility(View.VISIBLE);
        } else {
            v.setVisibility(View.GONE);
        }
    }

// 手書きメモ End
}
