/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           GPSManager.java
 * Description    位置付けの機能の初期化、設置、スタートがおよび閉鎖する
 * Created on     2010/08/02
 *
********************************************************************
 */

package net.zmap.android.pnd.v2.gps;

//import com.Navigation.Public;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.services.MonitorAdapter;
import net.zmap.android.pnd.v2.common.services.MonitorListener;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.NaviRun;

import java.io.BufferedReader;
import java.io.IOException;



/**
 * Created on 2010/08/02
 * Title:       GPSManager
 * Description:  GPS位置を測定する管理Class
 * @version        1.0
 */

public class GPSManager implements Runnable
{
    public  static LocationManager locationManager;
    private static MyLocationListener locationListener = new MyLocationListener();
    private static GPSManager s_oGps;
    private static Thread s_oThread = null;
    public static GPSStatusListener statusListener = new GPSStatusListener();

//Add 2011/08/26 Z01yoneya Start -->
    //GPS精度状態の管理・制御クラス
    public static LocationStatusController  locationStatusController = null;
//Add 2011/08/26 Z01yoneya End <--

    /**
     * Created on 2010/08/02
     * Title:       GPSManager
     * Description:  初期化関数
     * @param  無し
     * @return      無し
     * @version        1.0
    */
    public GPSManager()
    {
        s_oGps = this;
    }

    /**
     * Created on 2010/08/02
     * Title:       init
     * Description:  位置付けの機能の初期化、設置、スタートに責任を負います
     * @param  Context 資源の対象
     * @param  reliabilityJudgeMode GPS精度を判断する方法モード
     * @param  reliabilityJudgeSnrValue GPS精度をSNRで判断する時のしきい値
     * @param  reliabilityJudgeAccuracyValue GPS精度をaccuracyで判断する時のしきい値
     * @param  gpsRequestMinTime GPS最小取得間隔
     * @return      無し
     * @version        1.0
    */
//Chg 2011/08/26 Z01yoneya Start -->
//    public static void init(Context oContext) {
//--------------------------------------------
    public static void init(Context oContext,
                            int reliabilityJudgeMode,
                            float reliabilityJudgeSnrValue,
                            float reliabilityJudgeAccuracyValue,
                            int gpsRequestMinTime){
//Chg 2011/08/26 Z01yoneya End <--
        s_oGps = new GPSManager();
        JNIInt intMode = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetNaviMode(intMode);

        // if((0x00000000 == Constants.LoadMode_GPS) && (0x00000002 !=
        // Constants.VP_LoadMode_LocationManagerLOG))
        if (
//Del 2011/08/29 Z01yoneya Start -->
//                (0x00000000 == Constants.LoadMode_GPS) &&
//Del 2011/08/29 Z01yoneya End <--
                (0x00000002 != Constants.VP_LoadMode_LocationManagerLOG) &&
                (Constants.OFF == Constants.VP_LoadMode_LoggerManager)) {
            locationManager = (LocationManager) oContext.getSystemService(Context.LOCATION_SERVICE);

            try {
                // NaviLog.d("GPSManager", "GPSManager Init!!!!!!!!!!!");

                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, gpsRequestMinTime, 0, locationListener);
            } catch (SecurityException ex1) {
            } catch (IllegalArgumentException ex2) {
            } catch (RuntimeException ex3) {
            }

            locationManager.addGpsStatusListener(statusListener);
        }
// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
		MonitorAdapter monitor = MonitorAdapter.getInst();
		if (monitor != null) monitor.registListener(MonitorAdapter.LISTENER_GPS, mListener);
// Add 2011/03/17 Z01ONOZAWA End   <--

//Add 2011/08/26 Z01yoneya Start -->
		locationStatusController = LocationStatusController.initialize();
		if(locationStatusController != null){
		    locationStatusController.setGpsReliabilityJudgeMode(reliabilityJudgeMode);
            locationStatusController.setGpsReliabilityJudgeSnrValue(reliabilityJudgeSnrValue);
		    locationStatusController.setGpsReliabilityJudgeAccuracyValue(reliabilityJudgeAccuracyValue);

   	        locationListener.registLocationStatusLisner(locationStatusController.GetListner());
   	        statusListener.registLocationStatusListner(locationStatusController.GetListner());
		}
//Add 2011/08/26 Z01yoneya End <--
		// V2.5 ProFile読み出し元パス
		locationListener.init_MyLocation();
    }
     /**
      * Created on 2010/08/02
      * Title:       start
      * Description:  スレッドは関数をスタートさせます
      * @param  無し
      * @return         無し
      * @version        1.0
     */
     public static void start()
     {
         s_oThread = new Thread(s_oGps);
         s_oThread.start();
     }
     /**
      * Created on 2010/08/02
      * Title:       gpsStop
      * Description:  位置付けの機能に対して閉鎖を行うことに責任を負います
      * @param  無し
      * @return         無し
      * @version        1.0
     */

     public static void gpsStop()
     {
        try {
            // Chg 2010/10/19 sawada Start -->
            // if((0x00000000 == Constants.LoadMode_GPS) && (0x00000002 !=
            // Constants.VP_LoadMode_LocationManagerLOG))
            if (
//Del 2011/08/29 Z01yoneya Start -->
//                    (0x00000000 == Constants.LoadMode_GPS) &&
//Del 2011/08/29 Z01yoneya End <--
                    (0x00000002 != Constants.VP_LoadMode_LocationManagerLOG) &&
                    (Constants.OFF == Constants.VP_LoadMode_LoggerManager))
            // Chg 2010/10/19 sawada End <--
            {
                if (null != locationManager) {
                    locationManager.removeUpdates(locationListener);
                    locationManager.removeGpsStatusListener(statusListener);
                    locationManager = null;
                }

                if (null != locationListener) {
                    locationListener = null;
                }

                if (null != statusListener) {
                    statusListener = null;
                }
            }

            if (s_oThread != null) {
                s_oThread.interrupt();
                s_oThread = null;
            }

            if (null != s_oGps) {
                s_oGps = null;
            }
        } catch (IllegalArgumentException ex) {
			locationManager = null;
			locationListener = null;
			statusListener = null;
			s_oThread = null;
			s_oGps = null;
        }
    }

     public static void vpLgStop()
     {
         if(s_oThread != null)
         {
                s_oThread.stop();
                s_oThread = null;
         }
     }

     public static boolean isVpLog()
     {
         if(s_oThread != null)
         {
             return true;
         }else{
             return false;
         }
     }

     /**
      * Created on 2010/08/02
      * Title:       run
      * Description:  スレッドは関数に運行
      * @param  無し
      * @return         無し
      * @version        1.0
     */
    @Override
    public void run() {
        Location oLoc = null;
//      JNIInt loadMode = new JNIInt();
//      NaviRun.GetNaviRunObj().JNI_VP_GetLoadMode(loadMode);

        while(true)
        {
            try {
                Thread.sleep(900);
            } catch (InterruptedException e) {
				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            }

            oLoc = new Location( LocationManager.GPS_PROVIDER );
            // このrunは動作していないので　LocationStatusController に移す
            // NaviLog.v("wata", "GPPSManager Loop ");
            //locationListener.set_stopGPSDatasave(oLoc);
            // VP Log
            if ( Constants.VP_LoadMode_LocationManagerLOG == 2 )
            {
                String str = CommonLib.getVpLogFileFormatName();
                if ("Ver 1".equals(str)) {
                    try {
                        BufferedReader bf = CommonLib.getBf();
                        if(bf == null){
                            break;
                        }
                        str = bf.readLine();
                        if (str != null) {

//                            2010-12-7  15:30:16,35.67527,139.76108,8.02,76.08,0.39,342.69

                            String []strGpsData = str.split(",");
                            if (strGpsData != null && (strGpsData.length > 6)) {
                                oLoc.setLatitude(Double.valueOf(strGpsData[1]));
                                oLoc.setLongitude(Double.valueOf(strGpsData[2]));
                                oLoc.setAccuracy(Float.valueOf(strGpsData[3]));
                                oLoc.setAltitude(Double.valueOf(strGpsData[4]));
                                oLoc.setSpeed(Float.valueOf(strGpsData[5]));
                                oLoc.setBearing(Float.valueOf(strGpsData[6]));


                                if (null != locationListener) {
                                    locationListener.onGetLocation(oLoc);
                                }
                            }
                        }
                    } catch (IOException e) {
        				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                    }
                }
            }
        }
    }

// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
	private final static MonitorListener mListener = new MonitorListener() {
		@Override
		public void onCommandReceived(int iCommand, String sOperand) {
			String[] arguments = sOperand.split(",");// sOperand="arg1,arg2,....."
			MonitorAdapter monitor = MonitorAdapter.getInst();

			switch (iCommand) {
			case MonitorAdapter.COMMAND_ENABLE:
			case MonitorAdapter.COMMAND_DISABLE:
				int i = 0;
				for (i = 0; i < arguments.length; ++i) {
					if (arguments[i].equalsIgnoreCase("GPS")) {
						if (iCommand == MonitorAdapter.COMMAND_DISABLE) {
							locationListener.setSensorDisable(true);
							statusListener.setSensorDisable(true);
						} else {
							locationListener.setSensorDisable(false);
							statusListener.setSensorDisable(false);
						}
						if (monitor != null) monitor.mTarget.mSensorDisableGPS = (iCommand == MonitorAdapter.COMMAND_DISABLE);
					}
				}
				break;
			default:
				break;
			}
		}
	};
// Add 2011/03/17 Z01ONOZAWA End   <--
}

