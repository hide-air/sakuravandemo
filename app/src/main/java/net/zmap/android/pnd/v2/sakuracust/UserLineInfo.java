package net.zmap.android.pnd.v2.sakuracust;

import android.graphics.Color;
import net.zmap.android.pnd.v2.api.ItsmoNaviDriveExternalApi;
import net.zmap.android.pnd.v2.api.exception.NaviException;
import net.zmap.android.pnd.v2.api.overlay.Icon;
import net.zmap.android.pnd.v2.api.overlay.Line;
import net.zmap.android.pnd.v2.api.overlay.LineDrawParam;
import net.zmap.android.pnd.v2.data.NaviRun;

/**
 * 1つのポリラインの情報を格納する。
 * @author cpjsuna
 *
 */
public class UserLineInfo {
	public int mID;				// ID
	public int mIconIndex;      // アイコン識別ID
    //public int mFigureKind;		// 図形ID
	//public long mFigureID;		// 図形ID
	public Line mFigureLine;        // 描画した線
	public LineDrawParam mLineDrawParam; // 線の描画属性
    public String mKeyComment;	// キーとなるコメント

	/**
	 * 
	 */
	public UserLineInfo() {
		// TODO 自動生成されたコンストラクター・スタブ
		
		// 線の描画属性
		mLineDrawParam = new LineDrawParam();
		mLineDrawParam.setLineWidth(9);
//		mLineDrawParam.setLineColor(0xFFFF0000);
		mLineDrawParam.setLineColor(Color.MAGENTA);
	}

	/**
	 * 頂点を1つ追加する。<br/>この際、描画も同時に行う。
	 * @param lat
	 * @param lon
	 */
	public void AddPoint(ItsmoNaviDriveExternalApi api, String layerId, double lat, double lon) throws NaviException
	{
		if (mFigureLine == null)
		{
			// 初描画の時は生成する。
			mFigureLine = new Line(mLineDrawParam);
		}
		else
		{
			// 追加の時は一旦消す。
			DeleteLineFigure();
		}
		
		// 頂点を追加する。
		mFigureLine.addPoint(lat, lon);
		
		// 描画し直す。
		if (mFigureLine.getNumberOfPoints() >= 2) api.drawLine(layerId, mFigureLine);
	}
	
	/**
	 * 最後の頂点を削除する。<br/>この際、描画も同時に行う。
	 * @param api
	 * @param layerId
	 * @throws NaviException
	 */
	public void DeleteLastPoint(ItsmoNaviDriveExternalApi api, String layerId) throws NaviException
	{
		if (mFigureLine == null) return;
		
		// 何はともあれまずは地図上から消す。
		DeleteLineFigure();
		
		if (mFigureLine.getNumberOfPoints() <= 1)
		{
			mFigureLine = null; // インスタンスの存在を消す。
		}
		else
		{
			Line newItem = new Line(mLineDrawParam);
			for (int i = 0; i <= mFigureLine.getNumberOfPoints() - 2; i++)
			{
				newItem.addPoint(mFigureLine.getPoint(i));
			}
			mFigureLine = newItem;
			
			// 2点以上であれば描画する
			if (mFigureLine.getNumberOfPoints() >= 2) api.drawLine(layerId, mFigureLine);
		}
	}
	
	private void DeleteLineFigure()
	{
		if (mFigureLine.getNumberOfPoints() >= 2) NaviRun.GetNaviRunObj().JNI_NE_DeleteUserFigure(mFigureLine.getParentLayer().getNELayerId(), mFigureLine.getNEID());
	}
}
