//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************
package net.zmap.android.pnd.v2.inquiry.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_ZipCode_ListItem;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.inquiry.view.KeyboardTelView;
import net.zmap.android.pnd.v2.inquiry.view.twoButtonView;
import net.zmap.android.pnd.v2.maps.OpenMap;

/**
 * K-Z0_郵便番号入力
 *
 * */

public class PostalCodeInquiry extends InquiryBaseLoading {


	/**
	 * 郵便番号の表示オブジェクト
	 * */
	private EditText oTitleValue = null;
	/** 検索ボタンのオブジェクト */
	private Button oSearchBtn = null;
	private Button oModBtn = null;
	public static PostalCodeInquiry instance;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
//Del 2011/10/31 Z01_h_yamada Start -->
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"PostalCodeInquiry onCreate do");
//Del 2011/10/31 Z01_h_yamada End <--
		Intent oIntent = getIntent();
//Del 2011/09/17 Z01_h_yamada Start -->
//		//タイトルを設定する
//		int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE, 0);
//		if (0!= titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--
		//まず、JNIインタフェースのクリア処理を行う
		NaviRun.GetNaviRunObj().JNI_NE_POIZip_Clear();
		twoButtonView oBtnView = new twoButtonView(this);

		setViewInOperArea(oBtnView.getView());
		initTwoBtn(oBtnView);

		int min = oIntent.getIntExtra(Constants.PARAMS_MIN_LIMITED, 0);
		int max = oIntent.getIntExtra(Constants.PARAMS_MAX_LIMITED, 0);
//Chg 2011/11/01 Z01_h_yamada Start -->
//		KeyboardTelView okey = new KeyboardTelView(this, oSearchBtn,oModBtn, min, max);
//--------------------------------------------
		KeyboardTelView okey = new KeyboardTelView(this, oModBtn, min, max);
        okey.setSearchListener(onSearchClickListener);
//Chg 2011/11/01 Z01_h_yamada End <--
//		initTwoBtn(okey);
//Chg 2011/11/01 Z01_h_yamada Start -->
//		LinearLayout phoneSearch = (LinearLayout) okey.findViewById(this, R.id.LinearLayout_phonebtn);
//		phoneSearch.setVisibility(Button.GONE);
//--------------------------------------------
        okey.setPhoneShow(false);
//Chg 2011/11/01 Z01_h_yamada End <--

		oTitleValue = KeyboardTelView.getoTitle();
		oTitleValue.setHint(R.string.postalcode_text_input);
		oTitleValue.setFilters(new InputFilter[]{new InputFilter.LengthFilter(max)});
		setViewInWorkArea(okey.getView());



	}
	public static PostalCodeInquiry getInstance() {

		return instance;
	}

	@Override
	protected void onResume() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onResume");
		//yangyang mod start Bug1044
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//		AppInfo.hiddenSoftInputMethod(this);
//Del 2011/09/08 Z01_h_yamada End <--
//		InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//		View view = this.getCurrentFocus();
//	    if (view != null){
//	        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//	    }
		//yangyang mod end Bug1044
	    super.onResume();
	}
//	@Override
//	protected void onStart() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStart");
//		InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//		View view = this.getCurrentFocus();
//	    if (view != null){
//	        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//	    }
//
//		super.onStart();
//	}


	@Override
	protected void onPause() {
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//		AppInfo.hiddenSoftInputMethod(this);
//Del 2011/09/08 Z01_h_yamada End <--
		super.onPause();
	}

	@Override
	protected void onStop()
	{
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//		AppInfo.hiddenSoftInputMethod(this);
//Del 2011/09/08 Z01_h_yamada End <--
		super.onStop();
	}
	@Override
	protected void search() {
		JNILong ListCount = new JNILong();
		//検索した内容によって、取得したレコード数を取得する
		NaviRun.GetNaviRunObj().JNI_NE_POIZip_GetRecCount(ListCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"dilaog before==" +ListCount.lcount);
		if(ListCount.lcount > 0)
		{
//Add 2012/02/07 katsuta Start --> #2698
//			NaviLog.d(NaviLog.PRINT_LOG_TAG, "PostalCodeInquiry itemClick ================== bIsListClicked: " + bIsListClicked);
   			if (bIsListClicked) {
   				return;
   			}
   			else {
   				bIsListClicked = true;
   			}
//Add 2012/02/07 katsuta End <--#2698
			//1件の場合、直接に地図画面へ遷移する
			if(ListCount.lcount == 1){// One List -> OpenMap
//				TelData oData = new TelData();
//
//				oData.GetZipSearchInfo();
//				NaviRun.GetNaviRunObj().setSearchKind(Constants.ZIPCODE_ONELIST_OPENMAP);
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"yyang setSearchKind=====");
				POI_ZipCode_ListItem[] m_Poi_ZipCode_Listitem = new POI_ZipCode_ListItem[(int) ListCount.lcount];
				for(int i = 0;i<ListCount.lcount;i++){
				    m_Poi_ZipCode_Listitem[i] = new POI_ZipCode_ListItem();
				}
				JNILong rec = new JNILong();
				NaviRun.GetNaviRunObj().JNI_NE_POIZip_GetRecList(0, ListCount.lcount, m_Poi_ZipCode_Listitem, rec);
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"yyang JNI_NE_POIZip_GetRecList=====");
//				long lon = m_Poi_Arround_Listitem[0].getM_lLon();
//				long lat = m_Poi_Arround_Listitem[0].getM_lLat();
//				String address = m_Poi_Arround_Listitem[0].getM_Name();
//				openMap(null, lon, lat, address);


				POIData ozData = new POIData();
				ozData.m_sName = m_Poi_ZipCode_Listitem[0].getM_Name();
				ozData.m_sAddress = m_Poi_ZipCode_Listitem[0].getM_Name();
				if (AppInfo.isEmpty(ozData.m_sName)) {
					ozData.m_sName = m_Poi_ZipCode_Listitem[0].getM_FullZipCode()+ this.getResources().getString(R.string.str_around);

				}

				ozData.m_wLong = m_Poi_ZipCode_Listitem[0].getM_lLon();
				ozData.m_wLat = m_Poi_ZipCode_Listitem[0].getM_lLat();
				// XuYang add start #1056
	            Intent oIntent = getIntent();
	            if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
	                CommonLib.setChangePosFromSearch(true);
	                CommonLib.setChangePos(false);
	            } else {
	                CommonLib.setChangePosFromSearch(false);
	                CommonLib.setChangePos(false);
	            }
	            // XuYang add end #1056
                OpenMap.moveMapTo(PostalCodeInquiry.this,ozData, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//        		AppInfo.hiddenSoftInputMethod(this);
//Del 2011/09/08 Z01_h_yamada End <--
                this.createDialog(Constants.DIALOG_WAIT, -1);
                NaviLog.d(NaviLog.PRINT_LOG_TAG,"PostalCodeInquiry click do");
				return;
			} else {		// More List
				//1件以上の場合、結果画面へ遷移する
				Intent oIntent = getIntent();
				oIntent.putExtra(Constants.FROM_WHICH_SEARCH_KEY, "PostalCode");
				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, oTitleValue.getText().toString());

				oIntent.setClass(this, PostalCodeResultInquiry.class);
//Chg 2011/09/23 Z01yoneya Start -->
//				startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_POSTALCODERESULTINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
				NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_POSTALCODERESULTINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
			}
		} else {
			//データがない場合、提示ダイアログを表示する
			createDialog(DIALOG_NODATA_ALARM,R.string.postalcode_dialog_title);

		}

	}

	/**
	 * 検索ボタンと修正ボタンの初期化
	 * @param oBtnView 検索ボタンと修正ボタンのレイアウト
	 *
	 * */
	private void initTwoBtn(twoButtonView oBtnView) {
		oModBtn = (Button) oBtnView.findViewById(this, R.id.Button_edit);
		oModBtn.setEnabled(false);
//		btnEdit.setOnClickListener(onEditClickListener);
//Chg 2011/11/01 Z01_h_yamada Start -->
//		oSearchBtn = (Button) oBtnView.findViewById(this, R.id.Button_search);
//		oSearchBtn.setEnabled(false);
////		oSearchBtn.setOnClickListener(onSearchClickListener);
//		oBtnView.setButtonListener(onEditClickListener, onSearchClickListener);
//--------------------------------------------
		oBtnView.setButtonListener(onEditClickListener);
//Chg 2011/11/01 Z01_h_yamada End <--
	}

	/**
	 * 修正ボタンの押下処理
	 *
	 * */
	OnClickListener onEditClickListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 郵便番号 削除 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 郵便番号 削除 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
			if (!AppInfo.isEmpty(oTitleValue.getText().toString())) {
				StringBuffer sb = new StringBuffer(oTitleValue.getText().toString());
				int selectionEnd = oTitleValue.getSelectionEnd();
				if (selectionEnd == 0) {
					sb.deleteCharAt(selectionEnd);
				} else {
					sb.deleteCharAt(selectionEnd-1);
				}
				oTitleValue.setText(sb.toString());
				if (selectionEnd == 0) {
					oTitleValue.setSelection(selectionEnd);
				} else {
					oTitleValue.setSelection(selectionEnd - 1);
				}
				if (AppInfo.isEmpty(oTitleValue.getText().toString())) {
					oTitleValue.setHint("");
				}

			}

		}

	};

	/**
	 * 検索ボタンの押下処理
	 *
	 * */
	OnClickListener onSearchClickListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 郵便番号 検索 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 郵便番号 検索 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
			Intent oIntent = getIntent();
			int flagKey = oIntent.getIntExtra(Constants.FROM_FLAG_KEY,-1);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"flagKey===" + flagKey);
			if (flagKey != -1 && (flagKey == AppInfo.ID_ACTIVITY_MAINMENU ||
					flagKey == AppInfo.ID_ACTIVITY_ROUTE_EDIT)) {
				//JNIに検索したいフリーワードを渡す
				NaviRun.GetNaviRunObj().JNI_NE_POIZip_SearchList(oTitleValue.getText().toString());
//				search();
			} else {
				oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, oTitleValue.getText().toString());
				setResult(RESULT_OK, oIntent);
				finish();
			}

		}


	};
//
//	@Override
//	protected Dialog onCreateDialog(int wId) {
//		if (wId == DIALOG_NODATA_ALARM) {
//			CustomDialog oDialog = new CustomDialog(this);
//			oDialog.setTitle(R.string.postalcode_dialog_title);
//			oDialog.setMessage(this.getResources().getString(R.string.postalcode_dialog_msg));
//			oDialog.addButton(this.getResources().getString(R.string.btn_ok),new OnClickListener(){
//
//				@Override
//				public void onClick(View v) {
//					removeDialog(DIALOG_NODATA_ALARM);
//
//				}});
//			return oDialog;
//		}
//		return super.onCreateDialog(wId);
//	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			NaviRun.GetNaviRunObj().JNI_NE_POIZip_Clear();
		}
		return super.onKeyDown(keyCode, event);
	}


	@Override
	protected boolean onClickGoMap() {
		NaviRun.GetNaviRunObj().JNI_NE_POIZip_Clear();
		return super.onClickGoMap();
	}

	@Override
	protected boolean onClickGoBack() {
		NaviRun.GetNaviRunObj().JNI_NE_POIZip_Clear();
		return super.onClickGoBack();
	}
	@Override
	protected boolean onStartShowPage() throws Exception {
		return false;
	}
	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {
	}
}
