//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Facility_ListItem;
import net.zmap.android.pnd.v2.inquiry.view.ButtonImg;

/**
 * K-N2_名称都道府県絞込み
 * */

public class KeyProvinceInquiry extends ProvinceInquiry {

	/** 表示した都道府県の内容 */
	private POI_Facility_ListItem[] m_Poi_Key_Listitem;
	/** JNIから、取得した全部都道府県内容 */
	private POI_Facility_ListItem[] m_Poi_Gnr_Listitem = new POI_Facility_ListItem[50];
//	private JNILong Index = new JNILong();
	/** 本クラスの対象 */
	private static KeyProvinceInquiry instance;
	@Override
	protected void setSearchValuetoJNI(short WideCode, short MiddleCode, short NarrowCode) {
		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_SetAddressKeyCode(WideCode, MiddleCode, NarrowCode);

//		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_SearchList(super.searchValue);
		//Constants.MAX_RECORD_COUNT 最大件数
		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_SearchRefine(Constants.MAX_RECORD_COUNT);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
	}

	/** 本クラスを取得する */
	public static KeyProvinceInquiry getInstance() {
		return instance;
	}


	@Override
	protected void getProvinceValue() {
		JNILong ListCut = new JNILong();
		//JNIの戻り値　取得した都道府県のレコード数
		JNILong MyCarPosi = new JNILong();
		//
		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetPrefuctureRecListCount(ListCut);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"ListCut===" + ListCut.lcount);
		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetMypositon(1, MyCarPosi);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy  MyCarPosi====" + MyCarPosi.lcount);
		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetPrefectureRecList(0, 50, m_Poi_Gnr_Listitem, Rec  );
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy  Rec=====" + Rec.lcount);
	}



	@Override
	protected int checkHasData(String text, int dataIndex) {
		int bResult = -1;
		if (null != m_Poi_Gnr_Listitem) {
			int len = m_Poi_Gnr_Listitem.length;
			for (int i = 0 ; i < len; i++) {
				if (null != m_Poi_Gnr_Listitem[i].getM_Name() &&
						!AppInfo.isEmpty(m_Poi_Gnr_Listitem[i].getM_Name()) &&
						m_Poi_Gnr_Listitem[i].getM_Name().equals(text)) {
					bResult = i;
					break;
				}
			}
			if (-1!= bResult) {
//				m_Poi_Key_Listitem[dataIndex] = m_Poi_Gnr_Listitem[bResult];
				m_Poi_Key_Listitem[dataIndex].setM_iOldIndex(bResult);
			}
		}
		return bResult;
	}

	@Override
	protected void initDataObj() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"initDataObj====getCountInBox===" + getCountInBox());

		m_Poi_Key_Listitem = new POI_Facility_ListItem[50];
		for (int i = 0 ; i < 50 ; i++) {
			m_Poi_Key_Listitem[i] = new POI_Facility_ListItem();
		}
		//全部都道府県の内容を取得する（ソートしない都道府県の内容）
		int len = m_Poi_Gnr_Listitem.length;
		for (int i = 0 ; i < len ; i++) {
			m_Poi_Gnr_Listitem[i] = new POI_Facility_ListItem();
		}
	}

	@Override
	protected void getCurrentPageShowData() {
//		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetRecCount(ListCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy 110f ListCount==" + ListCount.lcount);
//		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetRecList(RecIndex, getCountInBox()*2,
//				m_Poi_Key_Listitem, ListCount);// all list result
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy 112 ListCount==" + ListCount.lcount);
	}


	@Override
	protected void initShowProvinceValue(int startIndex, int count) {
		int j = 0;
		int dataCount = getCount()*2;
		//設定ファイルから取得した都道府県の名称をオブジェクトにインストールする
		for (int i = startIndex ; i < startIndex + count ; i++) {
			if (i < dataCount && i < province.length) {
				m_Poi_Key_Listitem[j].setM_Name(province[i]);
			} else {
				m_Poi_Key_Listitem[j].setM_Name("");
			}
			j++;
		}
	}

	@Override
	protected String getButtonValue(int dataIndex) {
		if (dataIndex < m_Poi_Key_Listitem.length && m_Poi_Key_Listitem[dataIndex] != null) {
			return (String) m_Poi_Key_Listitem[dataIndex].getM_Name();
		} else {
			return "";
		}
//		return (String) m_Poi_Key_Listitem[dataIndex].getM_Name();
	}

	@Override
	protected int getM_iOldIndex(int dataIndex) {

		return m_Poi_Key_Listitem[dataIndex].getM_iOldIndex();
	}

	@Override
	protected void onProvinceclick(int provinceIndex, View oView) {
		iProvinceId = m_Poi_Key_Listitem[provinceIndex].getM_iOldIndex();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"iProvinceId===" + iProvinceId);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"allRecordCount===" + allRecordCount);
		//0：全国　それ以外：別の都道府県
//		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_Refine(iProvinceId, 0);
//		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetRecCount(ListCount);
		if (null == ListCount){
			ListCount = new JNILong();
		}
		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetPrefucturePointCount((long)iProvinceId,ListCount);
		//都道府県の総件数
		allRecordCount = initPageIndexAndCnt(ListCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"223 province ListCount====" + ListCount.lcount);
		if (allRecordCount > 0) {
			if(allRecordCount > 40){
				goCityInquiry(oView,iProvinceId);
			} else {
				goResultInquiry(oView,iProvinceId);
			}
		}
	}



	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			NaviRun.GetNaviRunObj().JNI_NE_POIFacility_Clear();
			doBack();
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent oIntent) {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"requestCode=========" + requestCode);
//		if (resultCode == Activity.RESULT_CANCELED) {
//			if (requestCode == AppInfo.ID_ACITIVITY_KEYCITYINQUIRY) {
//				oBox.refresh();
//			}
//		}
//		this.setResult(resultCode, oIntent);
		super.onActivityResult(requestCode, resultCode, oIntent);
	}

	protected int getFirstShowIndex() {
//		NaviRun.GetNaviRunObj().JNI_NE_POI_Facility_GetMypositon(1, Index);//
		int iReturn = 0;
//		int pageIndex  = (int)Index.lcount/(getCountInBox()* 2);

		for (int i = 0 ; i < province.length ; i++ ) {
			if (province[i].equals(ms1Str)) {
				iReturn = i/2;

				if (iReturn > this.getCount()-5) {
					iReturn = this.getCount()-this.getCountInBox()+1;
				} else if (iReturn > 2) {
					//現在地が、北海道、青森県、岩手県、宮城県以外の場合
					iReturn -= 2;
				} else {
					iReturn = 0;
				}
//				int pageIndex  = i/(getCountInBox()* 2);
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"getFirstShowIndex pageIndex===" + pageIndex);
//				iReturn = getCountInBox() * pageIndex;
				break;
			}
		}
//		oBox.setM_wIndex(iReturn);
		return (int)iReturn ;
	}


	/**
	 * 市区町村画面へ遷移する
	 * @param oView レイアウト対象
	 * @param Pos 都道府県のIndex
	 * */
	private void goCityInquiry(View oView,int Pos) {
			ButtonImg obtn = (ButtonImg) oView;
			Intent oIntent = getIntent();
			String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
			if (-1 != title.indexOf(Constants.FLAG_TITLE)) {
				title = title.substring(0,title.indexOf(Constants.FLAG_TITLE));
			}
			title += Constants.FLAG_TITLE +	obtn.getText().toString();
			oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"click go city=====" + title);
			oIntent.putExtra(Constants.PARAMS_PROVINCE_ID, Pos);
			oIntent.putExtra(Constants.PARAMS_MYPOS_CITY_NAME, super.ms2Str);
			oIntent.putExtra(Constants.PARAMS_IS_SHOW_BUTTON, true);
			oIntent.setClass(KeyProvinceInquiry.this, KeyCityInquiry.class);
//Chg 2011/09/23 Z01yoneya Start -->
//			startActivityForResult(oIntent, AppInfo.ID_ACITIVITY_KEYCITYINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
			NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACITIVITY_KEYCITYINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
		}

	/**
	 * 結果画面へ遷移する
	 * @param oView レイアウト対象
	 * */
	private void goResultInquiry(View oView,int Pos) {
		Intent oIntent = getIntent();
		ButtonImg obtn = (ButtonImg) oView;
		oIntent.putExtra(Constants.FROM_WHICH_SEARCH_KEY, "AllCity");
		String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
		if (-1 != title.indexOf(Constants.FLAG_TITLE)) {
			title = title.substring(0,title.indexOf(Constants.FLAG_TITLE));
		}
		title += Constants.FLAG_TITLE +	obtn.getText().toString();
		oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
		oIntent.putExtra(Constants.PARAMS_PROVINCE_ID, Pos);
		oIntent.setClass(this, KeyResultInquiry.class);
//Chg 2011/09/23 Z01yoneya Start -->
//		startActivityForResult(oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
		NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACITIVITY_KEYRESULTINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
	}


	@Override
	protected boolean onClickGoMap() {

		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_Clear();
		return super.onClickGoMap();
	}

	@Override
	protected boolean onClickGoBack() {
		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_Clear();
		doBack();

		return super.onClickGoBack();
	}

	private void doBack() {
		Intent oIntent = getIntent();
		String title = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
		if (-1 != title.lastIndexOf(Constants.FLAG_TITLE)) {
			title = title.substring(0, title.lastIndexOf(Constants.FLAG_TITLE));
		}
		oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, title);
//		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_Clear();
		this.setResult(Activity.RESULT_CANCELED,getIntent());
		finish();

	}


}
