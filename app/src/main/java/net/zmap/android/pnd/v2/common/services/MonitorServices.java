package net.zmap.android.pnd.v2.common.services;

import net.zmap.android.pnd.v2.common.utils.NaviLog;
import android.content.Context;
import android.os.Handler;

public class MonitorServices {
    // Log
    private static final String TAG = "MonitorServices";
    private static final boolean D = true;// Output at true

    // Message types sent from the MonitorServer Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// Key names received from the MonitorServer Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

    // Member fields
    private final Handler mHandler;
    private int mState;

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device

    /**
     * Constructor. Prepares a new MonitorServices session.
     * @param context  The UI Activity Context
     * @param handler  A Handler to send messages back to the UI Activity
     */
    public MonitorServices(Context context, Handler handler) {
        mState = STATE_NONE;
        mHandler = handler;
    }

    /**
     * Set the current state of the server connection
     * @param state  An integer defining the current connection state
     */
    private synchronized void setState(int state) {
        if (D) NaviLog.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    /**
     * Return the current connection state. */
    public synchronized int getState() {
        return mState;
    }

    /**
     * Start the MonitorServices. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume() */
    public synchronized void start() {
        if (D) NaviLog.d(TAG, "start");

        setState(STATE_LISTEN);
    }

    /**
     * Stop all threads
     */
    public synchronized void stop() {
        if (D) NaviLog.d(TAG, "stop");
        setState(STATE_NONE);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     * @param out The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void write(byte[] out) {

    }
}
