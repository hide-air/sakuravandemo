package net.zmap.android.pnd.v2.sakuracust;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import net.zmap.android.pnd.v2.R;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

public class LogMonitorSampleActivity extends Activity {
	private final static String LOGDIR = Environment.getExternalStorageDirectory().getPath() + "/rakuraku_log/";
	private final static String SDFILE = LOGDIR+"log2.txt";

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate( savedInstanceState);
        setContentView( R.layout.map_main);

        try {
            // ���O�̃N���A
            Runtime.getRuntime().exec( "logcat -c");
        } catch ( Exception e) {
            // ��O����
        }

        // ���O�ǉ��p�̃X���b�h
        Thread logThread = new LogThread();
        logThread.start();

        try {
            ArrayList<String> commandLine = new ArrayList<String>();
            // �R�}���h�̍쐬
            commandLine.add( "logcat");
            commandLine.add( "-v");
            commandLine.add( "time");
            commandLine.add( "-s");
            commandLine.add( "tag:W");
            commandLine.add( "-ｆ");
            commandLine.add( "SDFILE");

            Process process = Runtime.getRuntime().exec( commandLine.toArray( new String[commandLine.size()]));
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader( process.getInputStream()), 1024);
            String line = bufferedReader.readLine();
            while ( line != null) {
                Log.i("LogMonitor", line);
            }

        } catch ( IOException e) {
            // ��O����
        }
    }
    
    private class LogThread extends Thread{
        private int cnt = 1;
        
        @Override
        public void run() {
             while( true){
                 Log.w( "tag", "message" + cnt++);
             }
        }
    }
}
