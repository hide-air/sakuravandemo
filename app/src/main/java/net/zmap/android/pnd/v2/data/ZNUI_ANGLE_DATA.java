/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNUI_ANGLE_DATA.java
 * Description    ANGLE_DATA情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNUI_ANGLE_DATA {
    private short           sAngle;         ///< 角度
    private int             iAngleNo;       ///< 角度番号
    private int             iReserved;      ///< 予備
    /**
    * Created on 2010/08/06
    * Title:       getAngle
    * Description:  角度を取得する
    * @param1   無し
    * @return       short

    * @version        1.0
    */
    public short getAngle() {
        return sAngle;
    }
    /**
    * Created on 2010/08/06
    * Title:       getAngleNo
    * Description:  角度番号を取得する
    * @param1   無し
    * @return       int

    * @version        1.0
    */
    public int getAngleNo() {
        return iAngleNo;
    }
    /**
    * Created on 2010/08/06
    * Title:       getReserved
    * Description:  予備を取得する
    * @param1   無し
    * @return       int

    * @version        1.0
    */
    public int getReserved() {
        return iReserved;
    }


}
