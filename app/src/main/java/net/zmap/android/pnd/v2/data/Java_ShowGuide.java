/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           Java_ShowGuide.java
 * Description    案内表示非表示設定
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class Java_ShowGuide {
    private int eGuideType = 0;
    private int eStatus = 0;

    /**
    * Created on 2010/08/06
    * Title:       getEGuideType
    * Description:  ガイドタイプを取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getEGuideType() {
        return eGuideType;
    }

    /**
    * Created on 2010/08/06
    * Title:       getEStatus
    * Description:  状態を取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getEStatus() {
        return eStatus;
    }

}
