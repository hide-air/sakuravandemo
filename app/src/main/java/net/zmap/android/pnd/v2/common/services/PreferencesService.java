package net.zmap.android.pnd.v2.common.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferencesService
{
	protected SharedPreferences m_oPreferences = null;

	protected Editor m_oEditData = null;

	public PreferencesService(Context oContext,String sKey)
	{
		if(oContext != null)
		{
			m_oPreferences = oContext.getSharedPreferences(sKey, Context.MODE_PRIVATE);
			m_oEditData = m_oPreferences.edit();
		}
	}

	public boolean isHasKey(String sKey)
	{
		if(m_oPreferences != null)
		{
			return m_oPreferences.contains(sKey);
		}
		return false;
	}

	public boolean getBoolean(String sKey, boolean defValue)
	{
		if(m_oPreferences != null)
		{
			return m_oPreferences.getBoolean(sKey, defValue);
		}
		return defValue;
	}

	public float getFloat(String sKey, float defValue)
	{
		if(m_oPreferences != null)
		{
			return m_oPreferences.getFloat(sKey, defValue);
		}
		return defValue;
	}

	public int getInt(String sKey, int defValue)
	{
		if(m_oPreferences != null)
		{
			return m_oPreferences.getInt(sKey, defValue);
		}
		return defValue;
	}

	public long getLong(String sKey, long defValue)
	{
		if(m_oPreferences != null)
		{
			return m_oPreferences.getLong(sKey, defValue);
		}
		return defValue;
	}

	public String getString(String sKey, String defValue)
	{
		if(m_oPreferences != null)
		{
			return m_oPreferences.getString(sKey, defValue);
		}
		return defValue;
	}

	public void setValue(String sKey, boolean defValue)
	{
		if(m_oEditData != null)
		{
			m_oEditData.putBoolean(sKey, defValue);
		}
	}

	public void setValue(String sKey, float defValue)
	{
		if(m_oEditData != null)
		{
			m_oEditData.putFloat(sKey, defValue);
		}
	}

	public void setValue(String sKey, int defValue)
	{
		if(m_oEditData != null)
		{
			m_oEditData.putInt(sKey, defValue);
		}
	}

	public void setValue(String sKey, long defValue)
	{
		if(m_oEditData != null)
		{
			m_oEditData.putLong(sKey, defValue);
		}
	}

	public void setValue(String sKey, String defValue)
	{
		if(m_oEditData != null)
		{
			m_oEditData.putString(sKey, defValue);
		}
	}

	public void clearAllValue()
	{
		if(m_oEditData != null)
		{
			m_oEditData.clear();
		}
	}

	public void save()
	{
		if(m_oEditData != null)
		{
			m_oEditData.commit();
		}
	}
}
