package net.zmap.android.pnd.v2.api.overlay;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 描画パラメータ
 */
public final class LineDrawParam implements Parcelable {

    protected final int DEF_MAX_PEN_WIDTH   = 100;
    protected final int DEF_MAX_FRAME_WIDTH = 25;
    protected final int DEF_MAX_DOT_SIZE    = 100;

    protected int       mLineWidth          = 1;
    protected int       mLineColor          = 0xFF000000;
    protected int       mLineFrameWidth     = 1;
    protected int       mLineFrameColor     = 0xFFFFFFFF;
// Add 2011/10/18 r.itoh Start -->
    protected int       mDotColor           = 0xFF555555;
    protected int       mDotSize            = 2;
// Add 2011/10/18 r.itoh End <--

    /**
     * ラインフレームサイズ取得
     *
     * @return ラインフレームサイズ
     */
    public int getLineFrameWidth() {
        return mLineFrameWidth;
    }

    /**
     * ラインフレームサイズ設定
     *
     * @param lineFrameWidth
     *            ラインフレームサイズ
     */
    public void setLineFrameWidth(int lineFrameWidth) {
        mLineFrameWidth = lineFrameWidth;
    }

    /**
     * ラインフレーム色取得
     *
     * @return ラインフレーム色
     */
    public int getLineFrameColor() {
        return mLineFrameColor;
    }

    /**
     * ラインフレーム色設定
     *
     * @param lineFrameColor
     *            ラインフレーム色
     */
    public void setLineFrameColor(int lineFrameColor) {
        mLineFrameColor = lineFrameColor;
    }

    /**
     * ラインサイズ取得
     *
     * @return ラインサイズ
     */
    public int getLineWidth() {
        return mLineWidth;
    }

    /**
     * ラインサイズ 設定
     *
     * @param lineWidth
     *            ラインサイズ
     */
    public void setLineWidth(int lineWidth) {
        mLineWidth = lineWidth;
    }

    /**
     * ライン色取得
     *
     * @return ライン色
     */
    public int getLineColor() {
        return mLineColor;
    }

    /**
     * ライン色設定
     *
     * @param lineColor
     *            ライン色
     */
    public void setLineColor(int lineColor) {
        mLineColor = lineColor;
    }

// Add 2011/10/18 r.itoh Start -->

    /**
     * ドットサイズ取得
     *
     * @return ラインサイズ
     */
    public int getDotSize() {
        return mDotSize;
    }

    /**
     * ドットサイズ 設定
     *
     * @param dotWidth
     *            ドットサイズ
     */
    public void setDotSize(int dotWidth) {
        mDotSize = dotWidth;
    }

    /**
     * ドット色取得
     *
     * @return ライン色
     */
    public int getDotColor() {
        return mDotColor;
    }

    /**
     * ドット色設定
     *
     * @param dotColor
     *            ドット色
     */
    public void setDotColor(int dotColor) {
        mDotColor = dotColor;
    }

// Add 2011/10/18 r.itoh End <--

    /**
     * 描画パラメータ有効判定
     *
     * @return true : 有効 / false : 無効
     */
    public boolean IsValidDrawParam() {
        return (IsValidLineWdith() && IsValidLineFrameWdith() && IsValidDotSize());
    }

    /**
     * ペンサイズ有効判定
     *
     * @return true : 有効 / false : 無効
     */
    public boolean IsValidLineWdith() {
        if (mLineWidth <= 0) {
            return false;
        }
        if (mLineWidth > DEF_MAX_PEN_WIDTH) {
            return false;
        }
        return true;
    }

    /**
     * ドットサイズ有効判定
     *
     * @return true : 有効 / false : 無効
     */
    public boolean IsValidDotSize() {
        if (mDotSize < 0) {
            return false;
        }
        if (mDotSize > DEF_MAX_DOT_SIZE) {
            return false;
        }
        return true;
    }

    /**
     * ペンフレームサイズ有効判定
     *
     * @return true : 有効 / false : 無効
     */
    public boolean IsValidLineFrameWdith() {
        if (mLineFrameWidth < 0) {
            return false;
        }
        if (mLineFrameWidth > DEF_MAX_FRAME_WIDTH) {
            return false;
        }
        return true;
    }

    /**
     * コンストラクタ
     */
    public LineDrawParam() {

    }

    private LineDrawParam(Parcel in) {
        mLineWidth = in.readInt();
        mLineColor = in.readInt();
        mLineFrameWidth = in.readInt();
        mLineFrameColor = in.readInt();
// Add 2011/10/18 r.itoh Start -->
        mDotSize = in.readInt();
        mDotColor = in.readInt();
// Add 2011/10/18 r.itoh End <--
    }

    /* (非 Javadoc)
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(mLineWidth);
        out.writeInt(mLineColor);
        out.writeInt(mLineFrameWidth);
        out.writeInt(mLineFrameColor);
// Add 2011/10/18 r.itoh Start -->
        out.writeInt(mDotSize);
        out.writeInt(mDotColor);
// Add 2011/10/18 r.itoh End <--
    }

    /* (非 Javadoc)
     * @see android.os.Parcelable#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /* (非 Javadoc)
     *
     */
    public static final Parcelable.Creator<LineDrawParam> CREATOR = new Parcelable.Creator<LineDrawParam>() {

        public LineDrawParam createFromParcel(Parcel in) {
            return new LineDrawParam(in);
        }

        public LineDrawParam[] newArray(int size) {
            return new LineDrawParam[size];
        }
    };
}