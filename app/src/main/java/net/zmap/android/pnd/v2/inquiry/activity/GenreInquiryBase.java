//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.ImageManager;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.data.JNIJumpKey;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIShort;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNIThreeShort;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Genre_ListItem;
import net.zmap.android.pnd.v2.data.ZLandmarkData_t;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.maps.OpenMap;

import java.util.ArrayList;
import java.util.List;
//yangyang mod waitDialog start
public class GenreInquiryBase extends InquiryBaseLoading {
//yangyang mod waitDialog end
    protected LayoutInflater oInflater;
    protected LinearLayout oLayout;
    protected LinearLayout oGroupList;

    protected ScrollList scrollList;
    protected ScrollTool oTool;
    /**検索条件を表示する*/
    protected TextView oTextTitle;
    /**検索条件*/
    protected String meTextName;

    protected String ms1Str = null;
    //yangyang del start Bug1105
//    protected String ms2Str = null;
//    protected String ms3Str = null;
  //yangyang del end Bug1105

    protected JNIShort jumpRecCount = new JNIShort();
//    protected String msName = null;
//    protected JNILong jumpRecIndex = new JNILong();
    protected JNILong getRec = new JNILong();
    protected JNILong treeIndex = new JNILong();
    protected JNIJumpKey[] jumpKey;

    protected POI_Genre_ListItem[] m_Poi_Gnr_Listitem = null;
    protected POI_Genre_ListItem[] m_Poi_Add_Data = null;
    protected List<POI_Genre_ListItem> genreList = new ArrayList<POI_Genre_ListItem>();

    protected int TEN_PAGE_COUNT = 50;
    protected final int ONE_PAGE_COUNT = 5;

    protected final int HAPPY = 0;
    protected final int SHOPPING = 1;
    protected final int EATING = 2;
    protected final int LIFE = 3;
    protected final int TRAVEL = 4;
    protected final int TRAFFIC = 5;

    /**大分類対応した中分類のスタートインディクス*/
    protected long btnIndex = 0;
    /**中分類にレコード数*/
    protected long lMiddleKindTotleCount = 0;
    protected long g_RecIndex = 0;
//    protected long numRecIndex = 0;
    /**リストにレコード総数*/
    protected long recCnt = 0;
    protected long nameSize;
    /**レコードに分類の個数*/
    protected long remCount;

    protected long lTreeRecIndex = 0;
    protected short wideCode;
    protected short middleCode;
    protected short narrowCode;

    protected int recIndexNum =0;
    /**大分類対応した中分類のIndex*/
    protected Long[] LRecIdx;
    /**データ総数*/
    protected JNILong recCount = new JNILong();
    protected int myProvinceId;
    protected Bitmap clickGenreBMP = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Constants.GenreFlag = true;
//Del 2011/09/17 Z01_h_yamada Start -->
//        setMenuTitle(R.drawable.genre_inquiry);
//Del 2011/09/17 Z01_h_yamada End <--

        initData();

        Intent oIntent = getIntent();
//Del 2011/09/17 Z01_h_yamada Start -->
//        int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE,0);
//        if (0 != titleId) {
//            this.setMenuTitle(titleId);
//        }
//Del 2011/09/17 Z01_h_yamada End <--

        oInflater = LayoutInflater.from(this);

        String sTitle = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
        if (sTitle == null) {
            meTextName = getString(R.string.genre_title);
        } else {
            meTextName = sTitle;
        }
    }

    private void initData(){

        Intent oIntent = getIntent();
        if(oIntent != null){
            treeIndex.lcount = oIntent.getLongExtra(Constants.PARAMS_TREERECINDEX, 0);
            lTreeRecIndex = oIntent.getLongExtra(Constants.PARAMS_LIST_INDEX, 0);
        }
        //yangyang add start 20110311
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"treeIndex.lcount===" + treeIndex.lcount);
        if (treeIndex.lcount == 4 || treeIndex.lcount == 5 ) {
        	TEN_PAGE_COUNT = ONCE_GET_COUNT;
        } else {
        	TEN_PAGE_COUNT = 50;
        }
        //yangyang add end 20110311

        JNITwoLong Coordinate = new JNITwoLong();
        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);

        JNIThreeShort KeyCode = new JNIThreeShort();
        NaviRun.GetNaviRunObj().JNI_NE_GetAddressCode(Coordinate.getM_lLong(), Coordinate.getM_lLat(), KeyCode);

        wideCode = KeyCode.getM_sWideCode();
        middleCode = KeyCode.getM_sMiddleCode();
        narrowCode = KeyCode.getM_sNarrowCode();

        NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);
        JNIString AddressString = new JNIString();
        NaviRun.GetNaviRunObj().JNI_NE_GetAddressString(Coordinate.getM_lLong(),Coordinate.getM_lLat(), AddressString);
        String address = AddressString.getM_StrAddressString();
        if(address != null){
            int iFirst = address.indexOf(" ");
          //yangyang del start Bug1105
//            int iSecond = address.lastIndexOf(" ");
          //yangyang del end Bug1105
            ms1Str =  address.substring(0, iFirst);
          //yangyang del start Bug1105
//            ms2Str =  address.substring(iFirst+1, iSecond);
//            ms3Str =  address.substring(iSecond+1, address.length());
          //yangyang del end Bug1105
        }

        initGenreList();

        initGenreItems();

        initGenreAddrData();
    }

    protected void initGenreList() {
        if (genreList == null) {
            genreList = new ArrayList<POI_Genre_ListItem>();
        } else {
            if (!genreList.isEmpty()) {
                genreList.clear();
            }
        }
    }
    protected void initGenreAddrData() {
        if(null == m_Poi_Add_Data)
        {
            m_Poi_Add_Data = new POI_Genre_ListItem[(int) TEN_PAGE_COUNT];
            for (int i = 0; i < TEN_PAGE_COUNT; i++) {
                m_Poi_Add_Data[i] = new POI_Genre_ListItem();
                m_Poi_Add_Data[i].setM_Name("");

            }
        }
    }

    protected void initGenreItems() {
//        if(null == m_Poi_Gnr_Listitem)
//        {
            m_Poi_Gnr_Listitem = new POI_Genre_ListItem[(int) TEN_PAGE_COUNT];
            for (int i = 0; i < TEN_PAGE_COUNT; i++) {
                m_Poi_Gnr_Listitem[i] = new POI_Genre_ListItem();
                m_Poi_Gnr_Listitem[i].setM_Name("");
            }
//        }
    }

    protected void onItemClick(int wPos){

        if(bListDealing){
            return;
        }
        int index = (int) (wPos);
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"GenreInquiryBase onItemClick wPos=" + wPos +", TreeIndex=" + treeIndex.lcount);

        if (index >= genreList.size())
        {
            return;
        }
        if (treeIndex.lcount <= 2) {
            setTitleText(index);
//            NaviLog.d(NaviLog.PRINT_LOG_TAG,"old lTreeRecIndex==" + (recIndexNum + index) + ",treeIndex==" + treeIndex.lcount);
            if (treeIndex.lcount < 2) {
            	lTreeRecIndex = recIndexNum + index;
            } else {
            	lTreeRecIndex = wPos;
            }
            NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetNextTreeIndex(lTreeRecIndex, treeIndex);
//            NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy onItemClick recIndexNum + index==" + (recIndexNum + index) + ",treeIndex==" + treeIndex);
            NaviRun.GetNaviRunObj().JNI_NE_POIGnr_SearchNextList(lTreeRecIndex);
            CommonLib.setBIsSearching(true);
//            NaviLog.d(NaviLog.PRINT_LOG_TAG,"new lTreeRecIndex==" + lTreeRecIndex + ",treeIndex==" + treeIndex.lcount);
            doSearch();
        }else{
//Add 2012/02/07 katsuta Start --> #2698
    		if (treeIndex.lcount == 5) { // 検索結果リスト表示
//    			NaviLog.d(NaviLog.PRINT_LOG_TAG, "GenreInquiryBase onItemClick ================== bIsListClicked: " + bIsListClicked);
    			if (bIsListClicked) {
    				return;
    			}
    			else {
    				bIsListClicked = true;
    			}
    		}
//Add 2012/02/07 katsuta End <-- #2698

            itemClick(wPos);
        }
    }

    /**
     * 押下した処理
     * @param wPos
     * @param intent
     */
    private void itemClick(int wPos) {

        POI_Genre_ListItem item = genreList.get(wPos);

        if (item.getM_iNextCategoryFlag() == 0) {

            POIData poi = new POIData();
            poi.m_wLong = item.getM_lLongitude();
            poi.m_wLat = item.getM_lLatitude();
            poi.m_sName = item.getM_Name();
            poi.m_sAddress = item.getM_Name();
         // XuYang add start #1056
            Intent oIntent = getIntent();
            if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
                CommonLib.setChangePosFromSearch(true);
                CommonLib.setChangePos(false);
            } else {
                CommonLib.setChangePosFromSearch(false);
                CommonLib.setChangePos(false);
            }
            // XuYang add end #1056
            OpenMap.moveMapTo(this, poi, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
            //yangyang add waitDialog start
            this.createDialog(Constants.DIALOG_WAIT, -1);
          //yangyang add waitDialog end
        }else{
            lTreeRecIndex = wPos /*+ NumRecIndex*/;

            setTitleText(wPos);

//            NaviLog.d(NaviLog.PRINT_LOG_TAG,"old lTreeRecIndex==" + lTreeRecIndex + ",treeIndex==" + treeIndex.lcount);
//            NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetNextTreeIndex(lTreeRecIndex, treeIndex);
////            NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy itemClick lTreeRecIndex==" + lTreeRecIndex + ",treeIndex==" + treeIndex.lcount);
//            NaviRun.GetNaviRunObj().JNI_NE_POIGnr_SearchNextList(lTreeRecIndex);
//            NaviLog.d(NaviLog.PRINT_LOG_TAG,"new lTreeRecIndex==" + lTreeRecIndex + ",treeIndex==" + treeIndex.lcount);

            doSearch();
        }
    }

	protected void setTitleText(int wPos) {
		if (genreList != null && genreList.size() > 0 && wPos >= 0
				&& wPos < genreList.size()) {
			StringBuffer str = new StringBuffer(genreList.get(wPos).getM_Name());
			// if (str.charAt(0) == '[') {
			// str.delete(0, 3);
			// }

			if (null == meTextName) {
				meTextName = str.toString();
			} else {
				meTextName = meTextName + Constants.FLAG_TITLE + str.toString();
			}
			// yangyang add waitDialog start
			if (treeIndex.lcount <= 2) {
				// yangyang add waitDialog end
				oTextTitle.setText(meTextName);
				// yangyang add waitDialog start
			}
			// yangyang add waitDialog start
		}
	}

    protected void doSearch(){
        searchJUMP();
        search();
    }

    @Override
    protected void search() {

//      NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy GenreBase case " + treeIndex.lcount);

      switch ((int) treeIndex.lcount) {
      case 0:
      case 1:
      case 2:
          break;
      case 3: // 住所の一レベル
      case 4:
      case 5:
      case 6:
          goNext();
          break;
      default:
          break;
      }

    }
    protected void goNext() {

    }

    @Override
    protected void searchJUMP() {
        long index = treeIndex.lcount;
        if (index ==0 || index == 4) {
            if(jumpKey == null){
                jumpKey = new JNIJumpKey[50];
                for (int i = 0; i < 50; i++) {
                    jumpKey[i] = new JNIJumpKey();
                }
            }

            NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetJumpList(jumpKey, jumpRecCount);
            initScrollGroupShowValue(jumpKey,jumpRecCount);
        }
    }

    protected void initScrollGroupShowValue(JNIJumpKey[] jumpKey2,
			JNIShort jumpRecCount2) {
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
           boolean isFinish = onClickGoBack();
           if(!isFinish){
               finish();
           }
           return true;
        }
        return super.onKeyDown(keyCode, event);
    }

//    private void onGoBack(){
//        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetPrevTreeIndex(treeIndex);
//        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_SearchPrevList();
//    }

    protected void clear() {
        // NaviRun.GetNaviRunObj().JNI_NE_POIGnrKey_Clear();
        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_Clear();
    }

//    @Override
//    protected boolean onClickGoMap() {
//        do {
//            onGoBack();
//        } while (treeIndex.lcount >= 2);
//
//        return super.onClickGoMap();
//    }

    @Override
    protected boolean onClickGoBack() {
        if (treeIndex.lcount >= 2) {
            setResult(Constants.RESULTCODE_BACK);
        }
        return super.onClickGoBack();
    }

    /**
     * Listを表示する
     * @param RecIndex
     * @param getListCount
     * @param b
     */
    protected void showList(long RecIndex, int getListCount, int flag) {
//        NumRecIndex = 0;
        recCnt = 0;
        getSearchList(RecIndex, flag);

        if (-1 != flag) {
        	scrollList.reset();
        }
    }

    protected boolean bListDealing = false;
    private String[] msListName = null;
    protected List<Bitmap> iconList = new ArrayList<Bitmap>();

    /**
     * Listを取得する
     * @param dataStartIndex
     * @param b
     */
    private void getSearchList(long dataStartIndex, int b){
        bListDealing = true;

        initGenreItems();
        //次のデータを取得し、元のデータはクリアしない
        if(dataStartIndex == 0){
            initGenreList();
        }

        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetRecList(dataStartIndex, TEN_PAGE_COUNT, m_Poi_Gnr_Listitem, getRec);

//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"---->> getSearchList size = " + getRec.lcount +", dataStartIndex=" + dataStartIndex);

        //GetLandMark
        if(treeIndex.lcount <= 2){
            int keyInfo = 0;
            ZLandmarkData_t image = null;
            if(iconList != null){
                iconList.clear();
            }else{
                iconList = new ArrayList<Bitmap>();
            }

            msListName = new String[(int) getRec.lcount];

            for (int i = 0; i < getRec.lcount; i++) {
                msListName[i] = m_Poi_Gnr_Listitem[i].getM_Name();
                keyInfo = (int)m_Poi_Gnr_Listitem[i].getM_lKeyInfo();

                image = new ZLandmarkData_t();
// Chg 2011/10/12 katsuta Start -->
//Chg 2011/08/16 Z01thedoanh Start -->
//                NaviRun.GetNaviRunObj().JNI_DAL_GetLandmarkData(keyInfo, Constants.Icon_Size, image);
//----------------------------------------------------------------------------
//Chg 2011/08/16 Z01thedoanh End <--
//                NaviRun.GetNaviRunObj().JNI_MP_OutSidePOIIcon_GetIconData(keyInfo, Constants.Icon_Size, image);
                if (Constants.useKiwiIconFlag) {
                	NaviRun.GetNaviRunObj().JNI_DAL_GetLandmarkData(keyInfo, Constants.Icon_Size, image);
                }
                else {
                	NaviRun.GetNaviRunObj().JNI_MP_OutSidePOIIcon_GetIconData(keyInfo, Constants.Icon_Size, image);
                }
// Chg 2011/10/12 katsuta End <--

//NaviLog.d(NaviLog.PRINT_LOG_TAG,"--------keyInfo----->[" + i +"] = " + keyInfo +", w=" + image.getSWidth() +", h=" +image.getSHeight() +", length=" + image.getSPixRGBA32().length);
                if(image.getSWidth() > 0 && image.getSHeight() > 0){
                    iconList.add(ImageManager.getImage(image.getSPixRGBA32(), (int)image.getSWidth(), (int)image.getSHeight()));
//                	int WIDTH = 16;
//                	int HEIGHT = 16;
//                	int[][] imgPixR = new int[HEIGHT][WIDTH];
//                	int[][] imgPixG = new int[HEIGHT][WIDTH];
//                	int[][] imgPixB = new int[HEIGHT][WIDTH];
//                    int[] pixels1=new int[WIDTH*HEIGHT];
//                	int j;
//                	//読み込むファイル名を指定
//                    FileInputStream fb;
//					try {
//						fb = new FileInputStream("/sdcard/icon_other_16_16.bmp");
//						//fb = new FileInputStream("/sdcard/test.bmp");
//	                    int hy;//読み込んだデータを格納
//	                    int count = 0;
//
//	                    //ファイルを1バイトずつ読み込み、読み込み終わったら終了
//						while ((hy = fb.read()) != -1) {
//							count++;
//							if(count == 54){
//								break;
//							}
//						}
//						count = 0;
//						int indexW = 0;
//						int indexH = 0;
//						while ((hy = fb.read()) != -1) {
//							indexW = (count/3)%WIDTH;
//							indexH = (count/3)/WIDTH;
//							if(indexH < HEIGHT){
//							if(count%3 == 0){
//								//
//								imgPixB[HEIGHT - 1 - indexH][indexW] = hy;
//								//imgPixB[indexW][indexH] = hy;
//							}else if(count%3 == 1){
//								imgPixG[HEIGHT - 1 - indexH][indexW] = hy;
//							}else{
//								imgPixR[HEIGHT - 1 - indexH][indexW] = hy;
//							}
//							}
//							count++;
//						}
//						fb.close();
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//
//					for (int i1=0;i1<HEIGHT;i1++) {
//				        for (j=0;j<WIDTH;j++){
//				        	pixels1[i1*WIDTH+j] = (0xff000000)|(imgPixR[i1][j]<<16) |(imgPixG[i1][j]<<8)| imgPixB[i1][j];
//				        }
//					}
//
//					iconList.add(ImageManager.getImage(pixels1, WIDTH, HEIGHT));
//                	 Bitmap tmp = BitmapFactory.decodeFile("/sdcard/icon_other_16_16.bmp");
//                     int width = tmp.getWidth();
//                     int height = tmp.getHeight();
//                     int[] pixels = new int[width * height];
//                     int c = tmp.getPixel(0, 0);
//                     // 0,0 のピクセルと同じ色のピクセルを透明化する．
//                     Bitmap bitmap = Bitmap.createBitmap(width,height,Bitmap.Config.ARGB_8888 );
//                     tmp.getPixels(pixels, 0, width, 0, 0, width, height);
////                     for (int y = 0; y < height; y++) {
////                       for (int x = 0; x < width; x++) {
////                         if( pixels[x + y * width]== c){ pixels[x + y * width] = 0; }
////                       }
////                     }
//                     bitmap.eraseColor(Color.argb(0, 0, 0, 0));
//                     bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
//                     NaviLog.d(NaviLog.PRINT_LOG_TAG,"START");
//                     for(int i1 = 0; i1 < width*8; i1++){
//                    	 byte	test = (byte) (pixels[i1]>>16);
//                    	 byte	test1 = (byte) ((pixels[i1]<<8)>>16);
//                    	 //if(test != 255)
//                     }
//                     iconList.add(ImageManager.getImage(pixels, width, height));

                }else{
                	//yangyang add start Bug468
                	if (treeIndex.lcount == 2 && i == 0) {
                		iconList.add(this.clickGenreBMP);
                		//yangyang add start Bug1086
                	} else {
                		//yangyang add end Bug1086
                	//yangyang add end Bug468
                    iconList.add(null);
                	}
                }
            }
        }

//        if (treeIndex.lcount == 3 || treeIndex.lcount == 4) {
//
//            JNILong jumpRecIndex = new JNILong();
//            for (int j = 0; j < jumpRecCount.getM_sJumpRecCount(); j++) {
//                nameSize = jumpKey[j].NameSize;
//                String msName = jumpKey[j].pucName;
//
//                if(msName != null){
//                    NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetJumpList2RecordIndex(nameSize, msName, jumpRecIndex);
//
//                }
//
////                if ((RecIndex <= jumpRecIndex.lcount)
////                        && ( jumpRecIndex.lcount < (RecIndex + getRec.lcount))) {
////                    int a = (int) (jumpRecIndex.lcount - RecIndex);
////                    m_Poi_Gnr_Listitem[a].setM_Name("[" + msName + "]" + m_Poi_Gnr_Listitem[a].getM_Name());
////                }
//                if (jumpRecIndex.lcount > (dataStartIndex + getRec.lcount)){
//                    break;
//                }
//            }
//        }
        // add data to list
        for (int i = 0; i < getRec.lcount; i++) {
            genreList.add(m_Poi_Gnr_Listitem[i]);
        }

        //大分類、中分類、小分類のレコード総数
        //yangyang mod start 20110420 Bug746
//        if ( b != 0 )
        if ( b != 0 && treeIndex.lcount != 2)
        //yangyang mod end 20110420 Bug746
        {
            recCnt = remCount;
        }
        else
        {
            recCnt = getRec.lcount;
        }

        g_RecIndex = dataStartIndex;
//        numRecIndex = g_RecIndex;
        bListDealing = false;
    }

    protected int iCarPosition = 0;               // 自車の位置
//    private boolean m_bCarFlg = false;

    private String[] province;

    protected void initAddrData(){

        if(province == null){
            province = this.getResources().getStringArray(R.array.inquiry_province);
        }

        if(province != null)
        {
            //全国
            m_Poi_Add_Data[0].setM_Name("");
            m_Poi_Add_Data[0].setM_bBtnSts(true);
            genreList.add(m_Poi_Add_Data[0]);
            //都道府県
            for(int i = 1;i<m_Poi_Add_Data.length;i++){
                int j = i - 1;
                //沖縄を表示するため「=」を追加する
                if(i <= province.length){
                    m_Poi_Add_Data[i].setM_Name(province[j]);
                }else{
                    m_Poi_Add_Data[i].setM_Name("");
                }
                genreList.add(m_Poi_Add_Data[i]);
            }

            for(int i = 0; i < TEN_PAGE_COUNT; i++){

                // ボタンの状態を設定する
                m_Poi_Add_Data[i].setM_bBtnSts(false);

                if((!AppInfo.isEmpty(m_Poi_Add_Data[i].getM_Name())) && m_Poi_Add_Data[i].getM_Name().equals(ms1Str)){
                    iCarPosition = i + 1;   //全てリストに自車の位置
//                    m_bCarFlg = true;
                    //NaviLog.d(NaviLog.PRINT_LOG_TAG,"-------------->>> iCarPosition=" + iCarPosition);
                }

                for(int j = 0; j < TEN_PAGE_COUNT; j++){
                    // 都道府県のデータをソートする
                    if ((!AppInfo.isEmpty(m_Poi_Add_Data[i].getM_Name()))
                            && m_Poi_Add_Data[i].getM_Name().equals(m_Poi_Gnr_Listitem[j].getM_Name())){//名称相等の場合
                        // 都道府県のコードを設定する
                        m_Poi_Add_Data[i] = m_Poi_Gnr_Listitem[j];
// Chg 2011/06/02 sawada Start -->
//                      m_Poi_Add_Data[i].setM_iOldIndex(j);
                        m_Poi_Add_Data[i].setM_iOldIndex(j + 1);
// Chg 2011/06/02 sawada End   <--

                        // 有効なボタンの状態を設定する
                        m_Poi_Add_Data[i].setM_bBtnSts(true);

                       // NaviLog.d(NaviLog.PRINT_LOG_TAG,"[" + i +"] =>"+ m_Poi_Add_Data[i].isM_bBtnSts());

                        // 次のデータ
                        break;
                    }
                }
            }
        }
    }

    /** ベースに移動する */
//    protected boolean check(String value, String[] checkGroupValue) {
//        boolean bReturn = false;
//        int len = checkGroupValue.length;
//        if (len > 0) {
//            for (int i = 0 ; i < len ; i++) {
//                if (checkGroupValue[i].equals(value.trim())) {
//                    bReturn = true;
//                    break;
//                }
//            }
//        }
//        return bReturn;
//    }

    protected int getItemCount(){
        int count = (int) recCount.lcount;
        showToast(this,recCount);
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ----count---------->>> " + count);
//Chg 2011/06/10 Z01thedoanh Start -->
//      int minCount = 5;
        int minCount = 0;
//Chg 2011/06/10 Z01thedoanh End <--
        int maxCount = (int) Constants.MAX_RECORD_COUNT;
        if(count < minCount){
            return minCount;
        }
        if(count > maxCount){

            return maxCount;
        }
        return count;
    }

    protected void setCarPos(int wPos, int maxCount){

//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy ----setCarPos---------->>> maxCount=" + maxCount +", carPos=" + wPos);

        int minPos = 3;
//        if(wPos < minPos || wPos > maxCount - minPos){
		if (wPos < minPos) {
            return;
        }

        int iLineIndex = wPos ;
        if (iLineIndex > maxCount - minPos) {
            iLineIndex = maxCount - ONE_PAGE_COUNT + 1;
		} else if (iLineIndex > minPos - 1) {
            //現在地が、北海道、青森県、岩手県、宮城県以外の場合
            iLineIndex -= minPos;
        }

        scrollTo(iLineIndex);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent oIntent) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"GenreInquiryBase onActivityResult resultCode====" + resultCode);
        if(resultCode == Constants.RESULTCODE_BACK){

            if(treeIndex.lcount >= 2){
                NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetPrevTreeIndex(treeIndex);

                NaviRun.GetNaviRunObj().JNI_NE_POIGnr_SearchPrevList();
//                CommonLib.setBIsSearching(true);

//                if (treeIndex.lcount == 2) {
//                    doSearch();
//                }
            }

            int x = meTextName.lastIndexOf(Constants.FLAG_TITLE);
            if (x > 0) {
                meTextName = meTextName.substring(0, x);
            }
            oTextTitle.setText(meTextName);
        }
        super.onActivityResult(requestCode, resultCode, oIntent);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if(id == DIALOG_NODATA_ALARM){
            return createDialog(id, R.string.genre_dialog_title);
        }
        return super.onCreateDialog(id);
    }

    protected void scrollTo(final int index) {
        scrollList.scroll(index);
//        scrollList.reset();
    }
private boolean bReturn = true;
	@Override
	protected boolean onStartShowPage() throws Exception {

		runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				bReturn = initWaitBackData();
			}
		});
		return bReturn;
	}

	protected boolean initWaitBackData() {
		return true;
	}

	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {
		if(bGetData)
		{
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage start");
//			resetDialog();
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage end");
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					resetDialog();
				}
			});
		}
	}
	protected void resetDialog() {
	}

	protected void setTreeIndex() {
//    	NaviLog.d(NaviLog.PRINT_LOG_TAG,"old lTreeRecIndex==" + lTreeRecIndex + ",treeIndex==" + treeIndex.lcount);
        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetNextTreeIndex(lTreeRecIndex, treeIndex);
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"Province click: lTreeRecIndex=" + lTreeRecIndex + ",treeIndex==" + treeIndex.lcount);
        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_SearchNextList(lTreeRecIndex);
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"new lTreeRecIndex==" + lTreeRecIndex + ",treeIndex==" + treeIndex.lcount);
	}
}
