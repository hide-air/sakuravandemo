package net.zmap.android.pnd.v2.common;
/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           MapTypeListioner.java
 * Description
 * Created on     2009/12/21
 *
 ********************************************************************
 */
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;

/**
 *
 * 地図色の監視
 * @author wangdong
 *
 */
public class MapTypeListioner implements Runnable {
    public static boolean isRuning = false;
    public static boolean isActive = false;
    private static SunClock sc = null;
    public static final int NE_MapDParaMode_Day = 0;
    public static final int NE_MapDParaMode_Night = 1;
    public static final int NE_MapDParaMode_Sunlight = 2;

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        JNITwoLong Coordinate = new JNITwoLong();
        JNIInt  MapMode = new JNIInt();
        if(sc == null)
        {
            sc = new SunClock(0, 0);
        }

        while (isRuning) {

            if(isActive)
            {
                NaviRun.GetNaviRunObj().JNI_NE_GetMyPosi(Coordinate);

                sc.recalc((int) Coordinate.getM_lLat(), (int) Coordinate
                        .getM_lLong());
                boolean isDay = sc.isDaylight();
                NaviRun.GetNaviRunObj().JNI_NE_GetMapDParaMode(MapMode);
                if (isDay) {
                        if(MapMode.getM_iCommon() != NE_MapDParaMode_Day)
                        {
                            NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(
                                    Constants.TGL_MAPCOLOR_DAY);
                            NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
                            CommonLib.setBIsAutoMap(true);
                        }

                } else {
                        if(MapMode.getM_iCommon() != NE_MapDParaMode_Night)
                        {
                            NaviRun.GetNaviRunObj().JNI_NE_SetMapDParaMode(
                                    Constants.MAPCOLOR_NIGHT);
                            NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
                            CommonLib.setBIsAutoMap(true);
                        }
                }
                if (CommonLib.isBIsAutoMap()) {
                    /*
                    JNITwoLong center = new JNITwoLong();
                    NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(center);
                    if(center.getM_lLong() != 0 && center.getM_lLat()!= 0)
                    {
                        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(0, 0,
                                (byte) 0xFF, center.getM_lLong(), center.getM_lLat());
                        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
                        CommonLib.setBIsAutoMap(false);
                    }
                    */
                    NaviRun.GetNaviRunObj().JNI_NE_ForceRedraw();
		    CommonLib.setBIsAutoMap(false);
                }
            }


            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            }
        }
    }

    public static boolean getClockStsIsDay(int lat, int lon)
    {
        if(sc == null)
        {
            sc = new SunClock(0, 0);
        }
        sc.recalc(lat,lon);
        boolean isDay = sc.isDaylight();
        return isDay;
    }

}
