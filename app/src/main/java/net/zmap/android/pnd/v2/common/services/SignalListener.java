package net.zmap.android.pnd.v2.common.services;

import android.telephony.ServiceState;
import android.telephony.SignalStrength;

public interface SignalListener
{
	public void onSignalChange(SignalStrength oSignalStrength);

	public void onServiceChanged(ServiceState serviceState);
}
