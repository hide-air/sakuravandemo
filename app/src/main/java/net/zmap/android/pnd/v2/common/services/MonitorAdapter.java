package net.zmap.android.pnd.v2.common.services;

import android.os.Handler;
import android.os.Message;

import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.GPSSensorData;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.sensor.AccelerometerSensor;
import net.zmap.android.pnd.v2.sensor.GyroscopeSensor;
import net.zmap.android.pnd.v2.sensor.OrientationSensor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

public class MonitorAdapter {
    // Log
    private static final String TAG = "MonitorAdapter";

    private static MonitorAdapter singletonInstance = null;

    private MonitorAdapter() {
    }

    public static MonitorAdapter getInst() {
        return singletonInstance;
    }

    // Listener type for monitor commands
    public static final int LISTENER_GPS = 1;
    public static final int LISTENER_SENSOR = 2;

    public static final int COMMAND_ENABLE = 100;
    public static final int COMMAND_DISABLE = 101;

    // Name of the connected device
//	private String mConnectedDeviceName = null;
    // Member object for the monitor services
//    private MonitorServices mService = null;
    // Member object for the command listener
    private MonitorListener mListenerGPS = null;
    private MonitorListener mListenerSensor = null;

    private static HashMap<String, Integer> mCommandHash = new HashMap<String, Integer>();

    // ファイル出力
    private static FileWriter mWriter = null;
    private static BufferedWriter mBufWriter = null;

    public static MonitorAdapter init() {
        if (Constants.VPLOG_OUTPUT_MONITOR == Constants.OFF) {
            // 機能を無効にする
            return null;
        }

        singletonInstance = new MonitorAdapter();

        // モニタ制御コマンドテーブル
        mCommandHash.put("enable", Integer.valueOf(COMMAND_ENABLE));
        mCommandHash.put("disable", Integer.valueOf(COMMAND_DISABLE));

        mSnapshot = singletonInstance.new TimetickHandler();

        return singletonInstance;
    }

    public void registListener(int kind, MonitorListener oListener) {
        switch (kind) {
            case LISTENER_GPS:
                mListenerGPS = oListener;
                break;
            case LISTENER_SENSOR:
                mListenerSensor = oListener;
                break;
            default:
                break;
        }
    }

//Chg 2011/09/26 Z01yoneya Start -->
//    public void connect() {
//        // ファイルに出力する
//			String []naviResFilePath = new String[10];
//			for(int i =0; i < 10; i++)
//			{
//				naviResFilePath[i] = new String();
//			}
//			naviResFilePath = CommonLib.getNaviResFilePath();
//
//			String[] logPath = naviResFilePath[6].split(" ");// LOG_PATH
//          SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
//          Date currentTime = new Date();
//          String filename = logPath[2] + "MSLog_" + formatter.format(currentTime) + ".txt";
//------------------------------------------------------------------------------------------------------------------------------------
    public void connect(String logPath) {
        // ファイルに出力する
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        Date currentTime = new Date();
        String filename = logPath + "MSLog_" + formatter.format(currentTime) + ".txt";
//Chg 2011/09/26 Z01yoneya End <--
        File file = new File(filename);
        boolean result = file.getParentFile().mkdirs();
        if (!result) {
            NaviLog.e(TAG, "mkdirs error[" + file.getParentFile() + "]");
            return;
        }
        try {
            mWriter = new FileWriter(file);
            mBufWriter = new BufferedWriter(mWriter);
        } catch (IOException e) {
            mWriter = null;
            mBufWriter = null;
        }
        file = null;

        if (mSnapshot != null) mSnapshot.sleep(1000);
    }

    public void close() {
//        if (mService != null) mService.stop();
        mSnapshot = null;

        if (mBufWriter != null) {
            try {
                mBufWriter.close();
            } catch (IOException e) {
            }
            mBufWriter = null;
        }
        if (mWriter != null) {
            try {
                mWriter.close();
            } catch (IOException e) {
            }
            mWriter = null;
        }
    }

    public void output(String strMessage) {
//        if (mService != null) {
//            if (mService.getState() == MonitorServices.STATE_CONNECTED) {
//                byte[] message = strMessage.getBytes();
//                mService.write(message);
//                NaviLog.e(TAG, "Send to MonitorHost");
//            }
//        }

        if (mBufWriter == null) {
            return;//出力できない
        }
        synchronized (mBufWriter) {
            try {
                mBufWriter.write(strMessage + "\r\n");
                mBufWriter.flush();
            } catch (IOException e) {
                ;
            }
        }
    }

    private void onReceived(String receivedMsg) {
        // 受信メッセージを解析し、各オブジェクトに通知する
        StringTokenizer commandLine = new StringTokenizer(receivedMsg, ":");// command:arg1,arg2,.....
        String command = null;
        String operand = null;
        if (commandLine.countTokens() > 0) {
            command = commandLine.nextToken();// コマンド名
            if (commandLine.hasMoreTokens()) {
                operand = commandLine.nextToken();// 引数
            } else {
                operand = "";
            }
            NaviLog.e(TAG, "[" + command + "] [" + operand + "]");

            if (!mCommandHash.containsKey(command.toLowerCase())) {
            	NaviLog.e(TAG, "Unknown Command [" + command + "] [" + operand + "]");
                return;
            }

            if (mListenerGPS != null) {
                mListenerGPS.onCommandReceived(mCommandHash.get(command.toLowerCase()).intValue(), operand);// 通知
            }
            if (mListenerSensor != null) {
                mListenerSensor.onCommandReceived(mCommandHash.get(command.toLowerCase()).intValue(), operand);// 通知
            }
        } else {
        	NaviLog.e(TAG, "Unknown Command [" + receivedMsg + "]");
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MonitorServices.MESSAGE_STATE_CHANGE:// 状態変化の通知
                    switch (msg.arg1) {
                        case MonitorServices.STATE_CONNECTED:
                      NaviLog.d(TAG, "Change State to CONNECTED");
                      break;
                  case MonitorServices.STATE_CONNECTING:
                      NaviLog.d(TAG, "Change State to CONNECTING");
                      break;
                  case MonitorServices.STATE_LISTEN:
                      NaviLog.d(TAG, "Change State to LISTEN");
                      break;
                  case MonitorServices.STATE_NONE:
                      NaviLog.d(TAG, "Change State to NONE");
                      break;
              }
              break;
          case MonitorServices.MESSAGE_WRITE:// 送信済みデータ
//				  byte[] writeBuf = (byte[]) msg.obj;
//				  String writeMessage = new String(writeBuf);
              break;
          case MonitorServices.MESSAGE_READ:// 受信データ
              byte[] readBuf = (byte[])msg.obj;
              String readMessage = new String(readBuf, 0, msg.arg1);
              // コマンド通知する(実行)
              onReceived(readMessage);
              break;
          case MonitorServices.MESSAGE_DEVICE_NAME:// 接続先デバイス名
              // save the connected device's name
//				  mConnectedDeviceName = msg.getData().getString(MonitorServices.DEVICE_NAME);
              break;
          case MonitorServices.MESSAGE_TOAST:// 通知メッセージ
// 				  Toast.makeText(getApplicationContext(), msg.getData().getString(MonitorServices.TOAST),
// 								 Toast.LENGTH_SHORT).show();
        	  NaviLog.i(TAG, "TOAST <" + msg.getData().getString(MonitorServices.TOAST) + ">");
              break;
      }
  }
    };

    public static class MonitorTarget {
        public long mTimestampSnap = 0;
        public long mSensorStatus = 0;
        public long mCarStatus = 0;

        public long mVPSpeed = 0;
        public boolean mVPSpeedAtGPS = true;

        public float[] mAccelerationDiff = new float[3];
        public float[] mAngularSpeedDiff = new float[3];

        public float[] mLastOrientation = new float[3];

        public boolean mSensorDisableAcc = false;
        public boolean mSensorDisableGyro = false;
        public boolean mSensorDisableOri = false;
        public boolean mSensorDisableMag = false;
        public boolean mSensorDisableGPS = false;

        public AccelerometerSensor mAccelerometer = null;
        public long mTimestampAcc = 0;
//		public OrientationSensor mOrientation = null;// mLastOrientationと同値
        public GyroscopeSensor mGyroscope = null;
        public long mTimestampGyro = 0;
        public OrientationSensor mOrientationHard = null;
        public long mTimestampOri = 0;
        public OrientationSensor mOrientationMagnetic = null;
        public long mTimestampMag = 0;
        public GPSSensorData mGpsSensorData = null;
        public long mTimestampGPS = 0;
        public long mTotalSatsInView = 0;
        public long mTimestampSat = 0;

        public void cloneMonitorAcc(long lTimestamp, AccelerometerSensor oAccelerometer) {
            mTimestampAcc = lTimestamp;
            mAccelerometer = oAccelerometer;
        }

        public void cloneMonitorGyro(long lTimestamp, GyroscopeSensor oGyroscope) {
            mTimestampGyro = lTimestamp;
            mGyroscope = oGyroscope;
        }

        public void cloneMonitorOri(long lTimestamp, OrientationSensor oOrientationHard) {
            mTimestampOri = lTimestamp;
            mOrientationHard = oOrientationHard;
        }

        public void cloneMonitorMag(long lTimestamp, OrientationSensor oOrientationMagnetic) {
            mTimestampMag = lTimestamp;
            mOrientationMagnetic = oOrientationMagnetic;
        }

        public void cloneMonitorGPS(long lTimestamp, GPSSensorData oGpsSensorData) {
            mTimestampGPS = lTimestamp;
            mGpsSensorData = oGpsSensorData;
        }

        public void cloneMonitorSat(long lTimestamp, long lTotalSatsInView) {
            mTimestampSat = lTimestamp;
            mTotalSatsInView = lTotalSatsInView;
        }
    }

    public MonitorTarget mTarget = new MonitorTarget();

    private class TimetickHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            this.sleep(1000);// 次の実行

            // MonitorHostへ転送
            mTarget.mTimestampSnap = System.currentTimeMillis();
            {
                JNIInt iKind = new JNIInt();
                mTarget.mVPSpeed = NaviRun.GetNaviRunObj().JNI_VP_GetCurrentSpeed(iKind);
                if (mTarget.mVPSpeed >= 0) {
                    mTarget.mVPSpeedAtGPS = iKind.getM_iCommon() == 1;
                }
            }

            String snapshot = String.format("Status,%d,%s,%04x,Ori,%.4f,%.4f,%.4f,Diff,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%s,%s,%s,%s,%s,%s\n",
                                      mTarget.mTimestampSnap,
                                      mTarget.mCarStatus == 2 ? "Stop" : mTarget.mCarStatus == 1 ? "Going" : "Unknown",
                                      mTarget.mSensorStatus,
                                      mTarget.mLastOrientation[0], mTarget.mLastOrientation[1], mTarget.mLastOrientation[2],
                                      mTarget.mAccelerationDiff[0], mTarget.mAccelerationDiff[1], mTarget.mAccelerationDiff[2],
                                      mTarget.mAngularSpeedDiff[0], mTarget.mAngularSpeedDiff[1], mTarget.mAngularSpeedDiff[2],
                                      mTarget.mSensorDisableGPS ? "---" : "GPS",
                                      mTarget.mSensorDisableAcc ? "---" : "Acc",
                                      mTarget.mSensorDisableGyro ? "---" : "Gyr",
                                      mTarget.mSensorDisableOri ? "---" : "Ori",
                                      mTarget.mSensorDisableMag ? "---" : "Mag",
                                      mTarget.mVPSpeed >= 0 ? String.format("%d Km/h(%s)", mTarget.mVPSpeed,
                                                                            mTarget.mVPSpeedAtGPS ? "GPS" : "ACC") : "--Km/h");
            snapshot += String.format("onSat,%d,Num,%d\n",
                                      mTarget.mTimestampSat,
                                      mTarget.mTotalSatsInView);
            if (mTarget.mGpsSensorData != null) {
                snapshot += String.format("onLoc,%d,GPS,%d,%d,%.4f,%d\n",
                                          mTarget.mTimestampGPS,
                                          mTarget.mGpsSensorData.latitude1000, mTarget.mGpsSensorData.longitude1000,
                                          mTarget.mGpsSensorData.fSpeedMPS, mTarget.mGpsSensorData.nDirDegCW);
            } else {
                snapshot += "onLoc\n";
            }
            if (mTarget.mAccelerometer != null) {
                snapshot += String.format("onAcc,%d,Acc,%.4f,%.4f,%.4f,Raw,%.4f,%.4f,%.4f,Ave,%.4f,%.4f,%.4f,Mat,%.4f,%.4f,%.4f,Off,%.4f,%.4f,%.4f\n",
                                          mTarget.mTimestampAcc,
                                          mTarget.mAccelerometer.accelerationT[0],
                                          mTarget.mAccelerometer.accelerationT[1],
                                          mTarget.mAccelerometer.accelerationT[2],
                                          mTarget.mAccelerometer.rvalues[0],
                                          mTarget.mAccelerometer.rvalues[1],
                                          mTarget.mAccelerometer.rvalues[2],
                                          mTarget.mAccelerometer.nrvalues[0],
                                          mTarget.mAccelerometer.nrvalues[1],
                                          mTarget.mAccelerometer.nrvalues[2],
                                          mTarget.mAccelerometer.acceleration[0],
                                          mTarget.mAccelerometer.acceleration[1],
                                          mTarget.mAccelerometer.acceleration[2],
                                          mTarget.mAccelerometer.offsets[0],
                                          mTarget.mAccelerometer.offsets[1],
                                          mTarget.mAccelerometer.offsets[2]);
            } else {
                snapshot += "onAcc\n";
            }
            if (mTarget.mGyroscope != null) {
                snapshot += String.format("onGyr,%d,Gyr,%.4f,%.4f,%.4f,Raw,%.4f,%.4f,%.4f,Ave,%.4f,%.4f,%.4f,Mat,%.4f,%.4f,%.4f,Off,%.4f,%.4f,%.4f\n",
                                          mTarget.mTimestampGyro,
                                          mTarget.mGyroscope.angularSpeedT[0],
                                          mTarget.mGyroscope.angularSpeedT[1],
                                          mTarget.mGyroscope.angularSpeedT[2],
                                          mTarget.mGyroscope.rvalues[0],
                                          mTarget.mGyroscope.rvalues[1],
                                          mTarget.mGyroscope.rvalues[2],
                                          mTarget.mGyroscope.nrvalues[0],
                                          mTarget.mGyroscope.nrvalues[1],
                                          mTarget.mGyroscope.nrvalues[2],
                                          mTarget.mGyroscope.angularSpeed[0],
                                          mTarget.mGyroscope.angularSpeed[1],
                                          mTarget.mGyroscope.angularSpeed[2],
                                          mTarget.mGyroscope.offsets[0],
                                          mTarget.mGyroscope.offsets[1],
                                          mTarget.mGyroscope.offsets[2]);
            } else {
                snapshot += "onGyr\n";
            }
            if (mTarget.mOrientationMagnetic != null) {
                snapshot += String.format("onOriM,%d,Ori,%.4f,%.4f,%.4f,Acc,%.4f,%.4f,%.4f,Mag,%.4f,%.4f,%.4f\n",
                                          mTarget.mTimestampMag,
                                          mTarget.mOrientationMagnetic.orientation[0],
                                          mTarget.mOrientationMagnetic.orientation[1],
                                          mTarget.mOrientationMagnetic.orientation[2],
                                          mTarget.mOrientationMagnetic.acceleration[0],
                                          mTarget.mOrientationMagnetic.acceleration[1],
                                          mTarget.mOrientationMagnetic.acceleration[2],
                                          mTarget.mOrientationMagnetic.magnetic[0],
                                          mTarget.mOrientationMagnetic.magnetic[1],
                                          mTarget.mOrientationMagnetic.magnetic[2]);
            } else {
                snapshot += "onOriM\n";
            }
            if (mTarget.mOrientationHard != null) {
                snapshot += String.format("onOriH,%d,Ori,%.4f,%.4f,%.4f\n",
                                          mTarget.mTimestampOri,
                                          mTarget.mOrientationHard.orientation[0],
                                          mTarget.mOrientationHard.orientation[1],
                                          mTarget.mOrientationHard.orientation[2]);
            } else {
                snapshot += "onOriH\n";
            }
            output(snapshot);
        }

        public void sleep(long delayMills) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMills);
        }
    }

    private static TimetickHandler mSnapshot = null;

}
