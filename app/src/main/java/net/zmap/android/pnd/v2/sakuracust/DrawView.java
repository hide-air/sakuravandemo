package net.zmap.android.pnd.v2.sakuracust;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.view.CustomDialog;


/**
 * 描画用ビュー
 * 
 * 他でもこのクラスを独立して使えそうなので、
 * TegakiMemoActivity.javaソースから分離独立
 * 
 */
class DrawView extends SurfaceView {
    // モードの指定
    public static final int COMMAND_MODE_FREEHAND = 0;  // フリーハンドモード（普段モード） 
    public static final int COMMAND_MODE_RECTANGLE = 1; // 矩形描画モード(枠線)
    public static final int COMMAND_MODE_RECTANGLE_FILL = 2; // 矩形描画モード(塗りつぶし)
    public static final int COMMAND_MODE_POLYLINE = 3;  // ポリライン描画モード 
    public static final int COMMAND_MODE_STRING = 4;    // 文字列（横書き）描画モード 
    
    // コマンドモード
    private int commandMode = COMMAND_MODE_FREEHAND;

    ArrayList<PathInfo> draw_list = new ArrayList<PathInfo>(); // 描画履歴
    private float mPosx = 0f;
    private float mPosy = 0f;
    
    // 現在描画中の情報
    private Path nowpath = null; // 現在描画中のパス
    private int mColor = 0xff000000; // black
    private int penWidth = 1; // 現在描画中の線幅
    private int mTextSize = 20;
    private Paint.Style mStyle = Paint.Style.STROKE;
    private ArrayList<FloatPoint> point_list = new ArrayList<FloatPoint>(); // 描画頂点情報(ポリライン用)

    // ファイル入出力対策
    private Bitmap bmp = null;
    private Bitmap lastLoadedBmp = null;
    private Canvas bmpCanvas;
    
    // 横書き文字列のエディットテキストの幅高さ保持用
    private int stringWidth;
    private int stringHeight;
    // 横書き文字のエディットテキストのパディング上、左保持用
    private int stringDefPaddingTop;
    private int stringDefPaddingLeft;
    
    // Paintは予め確保しておく
    Paint mPaint = null;
    
    // 文字列描画用
    EditText mText = null;
    
    
    // 文字列のキープ
    String mKeptLastString = null;
    
    // TegakiMemoActivity依存処理(カラーパレットを閉じる)用
    private TegakiMemoActivity mActivity;
    // DrawViewの親
    private ViewGroup mParent;
    
    
    public DrawView(Context context) {
        super(context);
        this.createPaint();
        mActivity = (TegakiMemoActivity)context;
    }

    protected void onSizeChanged (int w, int h, int oldw, int oldh)
    {
        super.onSizeChanged(w,h,oldw,oldh);
        createBitmap(w, h);
    }
    
    protected void onDraw(Canvas canvas) {
    	if (bmp == null) return;
    	
        canvas.drawBitmap(bmp, 0, 0, null);
        drawNowEditingToCanvas(canvas);
    }

    public boolean onTouchEvent(MotionEvent e){
        mActivity.showColorPalette(false); // カラーパレットを閉じる
        switch(e.getAction()){
            case MotionEvent.ACTION_DOWN: //最初のポイント
                ActWhenTapDown(e);
                break;
            case MotionEvent.ACTION_MOVE: //途中のポイント
                ActWhenDrag(e);
                break;
            case MotionEvent.ACTION_UP: //最後のポイント
                ActWhenTapUp(e);
                break;
            default:
                break;
        }
        return true;
    }

    public void deleteInternalObj() {
        if( bmp != null ) {
            bmp.recycle();
            bmp = null;
        }
        if(lastLoadedBmp != null ) {
            lastLoadedBmp.recycle();
            lastLoadedBmp = null;
        }
        draw_list.clear();
        draw_list = null;
        bmpCanvas = null;
        System.gc();
    }

    public void finalize() {
        if (bmp != null) bmp.recycle();
    }
    
    /**
     * カラーの設定
     * @param rgb
     */
    public void setColor(int rgb) {
        mColor = 0xff000000 | rgb;
        if( mText != null) { // 文字列編集中であればテキストカラーにも反映
            mText.setTextColor(mColor);
        }
    }
    
    /**
     * カラーの取得
     * @return
     */
    public int getColor() {
        return mColor;
    }

    /**
     * ペン幅の設定
     * @param width
     */
    public void setPenWidth(int width) {
        penWidth = width;
    }
    
    /**
     * ペン幅の取得
     * @return
     */
    public int getPenWidth(){return penWidth;}

    /**
     * テキストサイズの取得
     */
    public int getTextSize() {
        return mTextSize;
    }
    
    /**
     * テキストサイズの設定
     */
    public void setTextSize(int size) {
        mTextSize = size;
        if( mText != null ) {
            mText.setTextSize(mTextSize);
        }
    }

    // 編集中か？
    private boolean isNowEditing() {
        return (nowpath != null);
    }

    // ロードされているか？
    private boolean isLoaded() {
        return (lastLoadedBmp != null);
    }

    // コマンドモードの設定
    private void setCommandMode(int commandMode) {
        this.commandMode = commandMode;
    }

    // コマンドモードの取得
    public int getCommandMode(){return commandMode;}

    /**
     * コマンドモードを解除し、フリーハンドモードにします。
     */
    public void clearCommandMode() {
        mStyle = Paint.Style.STROKE;
        mPaint.setStyle(mStyle);
        setCommandMode(COMMAND_MODE_FREEHAND);
    }

    /**
     * クリアする
     */
    public void clear(){
        draw_list.clear();
        if (isLoaded()) lastLoadedBmp.recycle();
        lastLoadedBmp = null;
        refreshBitmap();
        invalidate();
    }

    /**
     * Undoを行う
     */
    public void undo() {
        int lastIndex = draw_list.size() - 1;
        if(lastIndex <0 ) {
            return; // fail safe
        }
        draw_list.remove(lastIndex);
        redrawToBitmapForUndo();
        invalidate();
    }

     /**
     * IDを埋め込んで、ファイルへ保存する
     * @param format ファイルフォーマット（CompressFormat.JPEG or CompressFormat.PNG）
     * @param filePath 出力先フルパス
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void storeImage(CompressFormat format, String filePath)
    throws FileNotFoundException, IOException {
        FileOutputStream out = new FileOutputStream(filePath);
        bmp.compress(format, 100, out);
        out.flush();
        out.close();
    }

    /**
     * ファイルをロードする
     * @param filePath ロード対象フルパス
     */
    public boolean loadImage(String filePath) {
        File fl = new File(filePath);
        boolean result = fl.exists();
        if (result) {
            draw_list.clear(); // 今までの記録はキャンセルする
            if (isLoaded()) lastLoadedBmp.recycle();
            lastLoadedBmp = BitmapFactory.decodeFile(filePath);
            
            Bitmap tmpBitmap = null; //回転後のビットマップ
            if(lastLoadedBmp.getWidth() < lastLoadedBmp.getHeight()) {
            	//横のサイズ < 縦のサイズの場合に回転を行う
            	Matrix mat = new Matrix();  
            	mat.postRotate(270); 
            	// 回転したビットマップを生成
            	tmpBitmap = Bitmap.createBitmap(lastLoadedBmp, 0, 0, lastLoadedBmp.getWidth(), lastLoadedBmp.getHeight(), mat, true);
            	lastLoadedBmp.recycle();
            	lastLoadedBmp = tmpBitmap;
            }
            
            copyFromLastLoaded();
//            invalidate();
        }
        return result;
    }

// Add 2015-02-02 ファイル削除 Start
    /**
     * ファイルを削除する
     * @param filePath ロード対象フルパス
     */
    public boolean deleteImage(String filePath) {
        File fl = new File(filePath);
        boolean result = fl.exists();
        if (result) {
            draw_list.clear(); // 今までの記録はキャンセルする
            if (isLoaded()) lastLoadedBmp.recycle();
            lastLoadedBmp = BitmapFactory.decodeFile(filePath);
            
            Bitmap tmpBitmap = null; //回転後のビットマップ
            if (fl.delete()) {
            	System.out.println("ファイルを削除しました");
            } else {
            	System.out.println("ファイルの削除に失敗しました");
            }
            //copyFromLastLoaded();
        }
        return result;
    }
 // Add 2015-02-02 ファイル削除 End

    // Paintの初期化
    private void createPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);  
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
    }

    // Bitmapの初期化
    private void createBitmap(int w, int h) {
         if(isLoaded()) {
            copyFromLastLoaded();
            return;
         }
         // ビットマップの生成
         if (bmp != null) bmp.recycle();
         //bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
         bmp = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
         
         bmpCanvas = new Canvas(bmp);
         
         // 予めビットマップに真っ白な矩形を描画しておく
         Paint pnt = new Paint();
         pnt.setColor(0xffffffff);
         pnt.setAlpha(0xFF);
         bmpCanvas.drawRect(0, 0, (float)w, (float)h, pnt);
    }

    // ビットマップを真っ更にする。
    private void refreshBitmap() {
        if (bmp == null) return;
        int w = bmp.getWidth(), h = bmp.getHeight();
        createBitmap(w, h);
    }

    // 描画リストから、改めてビットマップキャンバスに全描画する。
    private void redrawToBitmapForUndo() {
        MaintenanceBitmapForUndo();
        drawToSpecifiedCanvas(bmpCanvas);
    }

    private void MaintenanceBitmapForUndo() {
        if (isLoaded()) {
            copyFromLastLoaded();
        } else {
            refreshBitmap();
        }
    }

    // タップダウン時のアクション
    private void ActWhenTapDown(MotionEvent e) {
        mPosx = e.getX();
        mPosy = e.getY();
        
        switch (commandMode)
        {
        case COMMAND_MODE_FREEHAND:
        case COMMAND_MODE_RECTANGLE:
        case COMMAND_MODE_RECTANGLE_FILL:
            // フリーハンドモードと矩形描画モードの共通処理
            nowpath = new Path();
            nowpath.moveTo(mPosx, mPosy);
            break;
        case COMMAND_MODE_POLYLINE:
            if(point_list.size() <= 0) { // ポリライン描画開始?(リストのサイズで判断をする)
                nowpath = new Path();
                point_list.add( new FloatPoint(mPosx, mPosy) );
                nowpath.moveTo(mPosx, mPosy);
            }
            break;
        case COMMAND_MODE_STRING:
            if( mText != null ) {
                commitDrawText();
            }
            createEditText();
            break;
        }
    }
    
    // ドラッグ時のアクション
    private void ActWhenDrag(MotionEvent e) {
        switch (commandMode)
        {
        case COMMAND_MODE_FREEHAND:
            // フリーハンドモードの時は、無尽蔵に追加してゆく
            mPosx += (e.getX()-mPosx)/1.4;
            mPosy += (e.getY()-mPosy)/1.4;
            nowpath.lineTo(mPosx, mPosy);
            invalidate();
            break;
        case COMMAND_MODE_RECTANGLE:
        case COMMAND_MODE_RECTANGLE_FILL:
            // 矩形描画の時は、5点を計算する
            DrawRectangleRubberBand(e);
            break;
        case COMMAND_MODE_POLYLINE:
            DrawPolylineRubberBand(e);
            break;
        case COMMAND_MODE_STRING:
            LayoutEditText(e);
            break;
        }
    }
    
    // タップアップ時のアクション
    private void ActWhenTapUp(MotionEvent e) {
        switch (commandMode)
        {
        case COMMAND_MODE_FREEHAND:
            nowpath.lineTo(e.getX(), e.getY());
            CommonDecidedAction(false);
            break;
        case COMMAND_MODE_RECTANGLE:
        case COMMAND_MODE_RECTANGLE_FILL:
            CommonDecidedAction(commandMode == COMMAND_MODE_RECTANGLE_FILL);
            break;
        case COMMAND_MODE_POLYLINE:
            mPosx = e.getX(); // 座標点の移動のみを行う
            mPosy = e.getY();
            point_list.add( new FloatPoint(mPosx, mPosy) );
            break;

        case COMMAND_MODE_STRING:
            if( mText != null ) {
                if( mText.getWidth() <= mTextSize || mText.getHeight() <= mTextSize) { // 1文字が入らないサイズであったら処理をキャンセル
                    mParent.removeView(mText); // 幅 or 高さ0なら描画しない
                    mText = null;
                    drawStringSymbol();
                    return ;
                }
                // キャレットを表示し、ソフトキーボードを起動
                mText.requestFocus();
//                InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputMethodManager.showSoftInput(mText, InputMethodManager.SHOW_FORCED);
                showFrequentWords();
            }
            break;
        }
    }

    // ソフトキーボードボタンをタップした際の共通処理
    View.OnClickListener softKeyOCL = new View.OnClickListener() {
        public void onClick(View view) {
            // ソフトキーボードを表示させる。
            mActivity.mDlg.dismiss();
            mText.requestFocus();
//            InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
//            inputMethodManager.showSoftInput(mText, InputMethodManager.SHOW_FORCED);
        }
    };

    // 頻出単語リストを表示する。
    private void showFrequentWords() {
        
        // 頻出単語の表示準備
        mActivity.mDlg = new CustomDialog(this.mActivity);
        mActivity.mDlg.setTitle("単語を選択して下さい");
        mActivity.mDlg.EnableTitleIcon(false);  // タイトルのアイコンは消す
        
        // 単語が選択された際の挙動
        View.OnClickListener ocl = new View.OnClickListener() {
            public void onClick(View v) {
                // 単語の取得
                String word = (String)v.getTag();
                
                if (mText != null){
                    // 単語を反映する。
                    mText.setText(word);
                    commitDrawText();
                }
                mActivity.mDlg.dismiss();
            }
        };
        
        // アラートダイアログ内に収めるリニアレイアウトの生成
        LinearLayout ll = new LinearLayout(this.mActivity);
        ll.setOrientation(LinearLayout.VERTICAL);
        
        // 頻出単語ステップをリストアップする処理
        View layout;
        String[] words = this.mActivity.mWords;
        for (int i = 0; i <= words.length - 1; i++) {
            layout = getLayoutCtrlForList(words[i]);
            layout.setOnClickListener(ocl);
            layout.setTag(words[i]);
            ll.addView(layout);
        }
        
        // スクロールビューに載せる。
        ScrollView setView = new ScrollView(mActivity);
        setView.addView(ll);
        mActivity.mDlg.setContentView(setView);
        mActivity.mDlg.addButton(this.getResources().getString(R.string.btn_show_softkeyboard),18, softKeyOCL);
        mActivity.mDlg.addButton(this.getResources().getString(R.string.btn_cancel), new OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.mDlg.dismiss();
            }
        });
        mActivity.mDlg.show();
    }

    public View getLayoutCtrlForList(String caption) {
        View layout = mActivity.getLayoutInflater().inflate(R.layout.dialog_list, null);
        TextView tv = (TextView)layout.findViewById(R.id.text_list_caption);
        tv.setText(caption);
        
        return layout;
    }

    // 最後にロードしたビットマップを上書きコピーを行う
    private void copyFromLastLoaded() {
        if (isLoaded()) {
            if (bmp != null) bmp.recycle();
            //bmp = lastLoadedBmp.copy(Bitmap.Config.ARGB_8888, true);
            bmp = lastLoadedBmp.copy(Bitmap.Config.RGB_565, true);
            //BitmapFactory.Options options = new  BitmapFactory.Options();
            //options.inMutable = true;
            //bmp = BitmapFactory.decodeFile(loadFileName, options);
            bmpCanvas = new Canvas(bmp);

        }
    }

    // 最終インデックスのみ描画実行
    private void drawFromListOnlyLast(Canvas cv) {
        int lastIdx = draw_list.size() - 1;
        if (lastIdx >= 0)
        {
            drawFromListSpecifiedIndex(cv, lastIdx);
            invalidate();
        }
    }

    // パラメータのキャンバスに描画実行
    private void drawToSpecifiedCanvas(Canvas cv) {
        for (int i = 0; i < draw_list.size(); i++) {
            drawFromListSpecifiedIndex(cv, i);
        }
        
        drawNowEditingToCanvas(cv);
    }

    // 特定のインデックスに対して描画実行
    private void drawFromListSpecifiedIndex(Canvas cv, int index) {
        if( draw_list.get(index).path != null ) { // 通常描画
            mPaint.setStyle(draw_list.get(index).style);
            mPaint.setColor(draw_list.get(index).color);
            mPaint.setStrokeWidth(draw_list.get(index).width);
            cv.drawPath(draw_list.get(index).path, mPaint);
        }

        if( draw_list.get(index).textInfo != null ) { // Text
            TextInfo info = draw_list.get(index).textInfo;
            if (info.viewWidth >= 1 && info.viewHeight >= 1)
            {
                EditText tmpText = new EditText(mActivity);
                tmpText.setGravity(Gravity.LEFT | Gravity.TOP);
                tmpText.setTextColor(draw_list.get(index).color);
                tmpText.setTextSize(draw_list.get(index).textSize);
                tmpText.setBackgroundColor(0x00ffffff);
                //TextInfo info = draw_list.get(index).textInfo;
                tmpText.setText(info.text);
                // オフライン描画をするときには、view.measure(), view.layout(), view.invalidate() をこの順番かつ、適切な引数で先に呼び出すこと
                int measuredW = View.MeasureSpec.makeMeasureSpec(info.viewWidth,View.MeasureSpec.EXACTLY); 
                int measuredH = View.MeasureSpec.makeMeasureSpec(info.viewHeight,View.MeasureSpec.EXACTLY);
                tmpText.measure(measuredW, measuredH);
                tmpText.measure(measuredW, measuredH);
                tmpText.layout(0, 0, info.viewWidth, info.viewHeight);
                tmpText.invalidate();
                Bitmap lbmp = Bitmap.createBitmap(info.viewWidth, info.viewHeight,Bitmap.Config.ARGB_4444);
                Canvas tmpCanvas = new Canvas(lbmp); 
                tmpText.draw(tmpCanvas);
                cv.drawBitmap(lbmp, info.viewLeft, info.viewTop, null);
                lbmp.recycle();
                lbmp = null;
                tmpText = null;
            } else {
                mPaint.setStyle(Paint.Style.FILL); // 固定
                mPaint.setColor(draw_list.get(index).color); // mColor
                mPaint.setTextSize(draw_list.get(index).textSize); // mTextSize
                String setStr = draw_list.get(index).textInfo.text;
                
                bmpCanvas.drawText(setStr, 0, setStr.length(), 
                    info.viewLeft, info.viewTop, mPaint);
            }
        }
    }

    // 現在描画中のものは基本Viewのキャンバスにしか描かないこととする。
    private void drawNowEditingToCanvas(Canvas cv) {
        if(isNowEditing()){
            mPaint.setStyle(mStyle);
            mPaint.setColor(mColor); // 現在、ドラッグ中の線は現在の設定色で描画する
            mPaint.setStrokeWidth(penWidth);
            cv.drawPath(nowpath, mPaint);
        }
    }
    
    private void CommonDecidedAction(boolean isFill) {
        draw_list.add( new PathInfo(nowpath, mColor, penWidth, isFill) );
        nowpath = null;
        drawToBitmapOnlyLast();
    }

    // ビットマップに対して最終インデックスを描画する
    private void drawToBitmapOnlyLast() {
        drawFromListOnlyLast(bmpCanvas);
    }

    // ポリラインの描画
    private void DrawPolylineRubberBand(MotionEvent e) {
        nowpath.reset();
        nowpath.moveTo(point_list.get(0).x, point_list.get(0).y); // 一番最初の頂点に移動
        for(int i = 1, num = point_list.size(); i < num; i++) {
            FloatPoint point = point_list.get(i);
            nowpath.lineTo( point.x, point.y );
        }
        nowpath.lineTo(e.getX(), e.getY() ); // 現在ドラッグしている頂点
        invalidate();
    }

    // ポリラインの内容を確定
    void commitPolyline() {
        if( point_list.size() <= 0 ) {
            return ; // fail safe
        }
        CommonDecidedAction(false);
        point_list.clear();
    }

    // 矩形の描画
    private void DrawRectangleRubberBand(MotionEvent e) {
        float deltaX = e.getX() - mPosx, deltaY = e.getY() - mPosy;
        nowpath.reset();
        nowpath.moveTo(mPosx, mPosy);
        nowpath.lineTo(mPosx + deltaX, mPosy);
        nowpath.lineTo(mPosx + deltaX,mPosy + deltaY);
        nowpath.lineTo(mPosx, mPosy + deltaY);
        nowpath.close();
        invalidate();
    }

    // EditTextの位置変更
    private void LayoutEditText(MotionEvent e) {
        int baseX = (mPosx < e.getX())? (int)mPosx : (int)(e.getX());
        int baseY = (mPosy < e.getY())? (int)mPosy : (int)(e.getY());
        int width = (int)(e.getX() - mPosx);
        int height = (int)(e.getY() - mPosy);
        width = (width >=0 )? width : -width;
        height = (height >=0 )? height : -height;
        if( mText != null ) {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(width, height);
            params.leftMargin = baseX;
            params.topMargin = baseY;
            mText.setLayoutParams(params);
        }
    }

    // EditTextの内容を確定
    boolean commitDrawText() {
        if( mText == null ) {
            return false; // fail safe
        }
        else if( mText.getWidth() <= mTextSize || mText.getHeight() <= mTextSize ) {// 1文字が入らないサイズであったら処理をキャンセル
            mParent.removeView(mText); // 幅 or 高さ0なら描画しない
            mText = null;
            return false;
        }
        
        // 文字列がないときも処理しない
        String jtxt = mText.getText().toString();
        if ("".equals(jtxt)) {
            return false;
        }
        
        mText.setFocusable(false);
        mText.clearComposingText(); // 候補の確定
        // ソフトキーボードを隠す
        InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(mText.getWindowToken(), 0);
        // 描画処理
        mText.setBackgroundColor(0x00ffffff);
        mText.setDrawingCacheEnabled(true);
        mText.destroyDrawingCache();
        
        // キープする情報の取得
        mKeptLastString = mText.getText().toString();
        
        draw_list.add( new PathInfo(mKeptLastString, mColor,
                mTextSize, mText.getLeft(), mText.getTop(),
                mText.getWidth(), mText.getHeight(), commandMode) );
        
        // 描画したエディットテキストの幅と高さを保持する
        stringWidth = mText.getWidth();
        stringHeight = mText.getHeight();
        stringDefPaddingLeft = mText.getPaddingLeft();
        stringDefPaddingTop = mText.getPaddingTop();
        
        drawToBitmapOnlyLast();
        mParent.removeView(mText);
        mText = null;
        
        return true;
    }
    
    private void createEditText() {
        ClearEditText();
        mText = new EditText(mActivity);
        mText.setTextColor(mColor);
        mText.setGravity(Gravity.LEFT | Gravity.TOP);
        mText.setBackgroundColor(0x807f7f7f);
        mText.setTextSize(mTextSize);
/*
        mText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(mText, InputMethodManager.SHOW_FORCED);
                } else {
                    hideSoftKeyboard();
                }
            }
        });
*/
        mParent.addView(mText, 0, 0);

    }
    
    public void ClearEditText() {
        if (mText != null) {
            mParent.removeView(mText);
            mText = null;
        }
    }

    private void drawStringSymbol() {
        if (mKeptLastString != null) {
            // 横書きモードの場合、最後に範囲指定した幅と高さで描画する
            int width = 0;
            int height = 0;
            float marginX = 0.0f;
            float marginY = 0.0f;
            width = stringWidth;
            height = stringHeight;
            marginX = (float)stringDefPaddingLeft;
            marginY = (float)stringDefPaddingTop;
            draw_list.add( new PathInfo(mKeptLastString, mColor, mTextSize, mPosx - marginX, mPosy - marginY, width, height, commandMode) );
            drawToBitmapOnlyLast();
        }
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if( mText != null ) {
            inputMethodManager.hideSoftInputFromWindow(mText.getWindowToken(), 0);
        }
    }

    /**
     * 矩形描画モードにします。
     */
    public void startDrawRectangle() {
        mStyle = Paint.Style.STROKE;
        mPaint.setStyle(mStyle);
        setCommandMode(COMMAND_MODE_RECTANGLE);
    }
    
    /**
     * 矩形(塗りつぶし)描画モードにします。
     */
    public void startDrawRectangleFill() {
        mStyle = Paint.Style.FILL;
        mPaint.setStyle(mStyle);
        setCommandMode(COMMAND_MODE_RECTANGLE_FILL);
    }
    
    /**
     * ポリライン描画モードにします。
     */
    public void startDrawPolyline() {
        mStyle = Paint.Style.STROKE;
        mPaint.setStyle(mStyle);
        setCommandMode(COMMAND_MODE_POLYLINE);
    }

    /**
     * 文字列描画モードにします。
     */
    public void startDrawString(ViewGroup parent) {
        mParent = parent;
        mStyle = Paint.Style.STROKE;
        mPaint.setStyle(mStyle);
        setCommandMode(COMMAND_MODE_STRING);
    }

    // 各Pathの描画情報保存用
    private class PathInfo {
        Path path;
        TextInfo textInfo;
        int color;
        int width; // ペン幅 
        int textSize; // 文字列サイズ
        Paint.Style style = Paint.Style.STROKE;
        
        public PathInfo(Path path, int color, int width) {
            this.path = path;
            this.color = color;
            this.width = width;
            this.textInfo = null;
            this.textSize = 0;
        }
        
        public PathInfo(Path path, int color, int width, boolean isFill) {
            this.path = path;
            this.color = color;
            this.width = width;
            this.textInfo = null;
            this.textSize = 0;
            if( isFill ) {
                this.style = Paint.Style.FILL;
            }
        }
        
        public PathInfo(String text, int color, int textSize, float viewLeft, float viewTop, int viewWidth, int viewHeight, int mode) {
            TextInfo info = new TextInfo(text, viewLeft, viewTop, viewWidth, viewHeight);
            this.textInfo = info;
            this.color = color;
            this.textSize = textSize;
            this.path = null;
            this.width = 0;
        }
    }

    private class TextInfo {
        String text;
        float viewLeft; // Viewの位置
        float viewTop; // Viewの位置
        int viewWidth; // Viewの幅
        int viewHeight; // Viewの高さ
        
        public TextInfo(String text, float left, float top, int width, int height) {
            this.text = text;
            this.viewLeft = left;
            this.viewTop = top;
            this.viewWidth = width;
            this.viewHeight = height;
        }
    }

    private class FloatPoint {
        float x,y;
        
        public FloatPoint(float x, float y) {
            this.x = x;
            this.y = y;
        }
    }
}

