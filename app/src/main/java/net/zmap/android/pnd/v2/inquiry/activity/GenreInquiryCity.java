//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.data.JNIJumpKey;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIShort;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.inquiry.view.ItemView;

import java.util.ArrayList;
import java.util.List;

public class GenreInquiryCity extends GenreInquiryBase {

	//yangyang del start Bug617
//	private WhiteBackGroundTextView oGroupValue;
	//yangyang del end Bug617
	private CityList citylist;
	private String clickValue = "";
	private JNILong Index = new JNILong();
	private int allRecordCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		// yangyang mod waitDialog start
		startShowPage(NOTSHOWCANCELBTN);
		// // JNILong Idx = new JNILong();
		// // NaviRun.GetNaviRunObj().JNI_NE_POIGnr_AddrIndex(Idx);
		// NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetRecCount(recCount);
		//
		// // NaviLog.d(NaviLog.PRINT_LOG_TAG,"onCreate recCount.lcount===" +
		// recCount.lcount);
		// if(recCount.lcount == 0 || recCount.lcount == 1){
		// showDialog(DIALOG_NODATA_ALARM);
		//
		// }else{
		//
		// startShowPage(NOTSHOWCANCELBTN);
		//
		//
		// }
		// yangyang mod waitDialog end
	}

	// yangyang add waitDialog start
	@Override
	protected boolean initWaitBackData() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"city initWaitBackData start");

		setTreeIndex();
		NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetRecCount(recCount);

		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"onCreate recCount.lcount===" + recCount.lcount);
// Chg 2011/06/06 sawada Start -->
//		if (recCount.lcount == 0 || recCount.lcount == 1) {
		if (recCount.lcount == 0) {
// Chg 2011/06/06 sawada End   <--

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					showDialog(DIALOG_NODATA_ALARM);
				}
			});

			return false;
		} else {

			citylist = new CityList();
			searchJUMP();

//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"city initWaitBackData end");
		}
		return super.initWaitBackData();
	}

	@Override
	protected void resetDialog() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"city resetDialog start");
		initCityLayout();
		g_RecIndex = 0;
		showCityList(g_RecIndex);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"city resetDialog end");
	}

	// yangyang add waitDialog end
	private void initCityLayout() {
		oLayout = (LinearLayout) oInflater.inflate(R.layout.inquiry_base, null);

		Button obtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
		if (obtn != null) {

			obtn.setBackgroundResource(R.drawable.btn_default);
			obtn.setText(R.string.btn_all_city);
//Add 2011/11/01 Z01_h_yamada Start -->
			obtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.Common_Medium_textSize));
//Add 2011/11/01 Z01_h_yamada End <--

			obtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ 全市区町村選択禁止 Start -->
			    	if(DrivingRegulation.CheckDrivingRegulation()){
			    		return;
			    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ 全市区町村選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
					onItemClick(0);
				}
			});
		}

		oTextTitle = (TextView) oLayout.findViewById(R.id.inquiry_title);
		oTextTitle.setText(meTextName);

		oGroupList = (LinearLayout) oLayout
				.findViewById(R.id.LinearLayout_list);
		ItemView oview = new ItemView(this, 0, R.layout.inquiry_city_freescroll);

		//yangyang del start Bug617
//		oGroupValue = (WhiteBackGroundTextView) oview.findViewById(this,
//				R.id.TextView_GroupId);
		//yangyang del end Bug617
		// 初期値
		// oGroupValue.setText(R.string.kana_a);
		initGroupBtn(oview);

		scrollList = (ScrollList) oview.findViewById(this, R.id.scrollList);
		scrollList.setAdapter(citylist);
		scrollList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
//Del 2011/10/06 Z01_h_yamada Start -->
//		scrollList.setCountInPage(ONE_PAGE_COUNT);
//Del 2011/10/06 Z01_h_yamada End <--
//Del 2011/07/21 Z01thedoanh Start -->
		//yangyang add start  Bug617
		//scrollList.setPadding(5, 5, 4, 5);
		//yangyang add end  Bug617
//Del 2011/07/21 Z01thedoanh End <--
		oGroupList.addView(oview.getView(), new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT));

		oTool = new ScrollTool(this);
		oTool.bindView(scrollList);
		removeOperArea();
		setViewInOperArea(oTool);

		getWorkArea().removeAllViews();
		setViewInWorkArea(oLayout);
	}

	private Button btnA = null;
	private Button btnKa = null;
	private Button btnSa = null;
	private Button btnTa = null;
	private Button btnNa = null;
	private Button btnHa = null;
	private Button btnMa = null;
	private Button btnYa = null;
	private Button btnRa = null;
	private Button btnWa = null;
	// private Button curFocusBtn;

	private List<String> AKeyName = new ArrayList<String>();
	private List<String> WaKeyName = new ArrayList<String>();
	private List<String> KaKeyName = new ArrayList<String>();
	private List<String> SaKeyName = new ArrayList<String>();
	private List<String> TaKeyName = new ArrayList<String>();
	private List<String> NaKeyName = new ArrayList<String>();
	private List<String> HaKeyName = new ArrayList<String>();
	private List<String> MaKeyName = new ArrayList<String>();
	private List<String> YaKeyName = new ArrayList<String>();
	private List<String> RaKeyName = new ArrayList<String>();

//Add 2011/09/09 Z01_h_yamada Start -->
	private SpannableString makeGroupSpannableText(String groupName, float fontRate)
	{
        SpannableString spannable = new SpannableString(groupName);
        RelativeSizeSpan span = new RelativeSizeSpan(fontRate);
        spannable.setSpan(span, 1, 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannable;
	}
//Add 2011/09/09 Z01_h_yamada End <--


	private void initGroupBtn(ItemView oview) {
//Add 2011/09/09 Z01_h_yamada Start -->
        Resources oRes = getResources();
//Add 2011/09/09 Z01_h_yamada End <--

        btnA = (Button) oview.findViewById(this, R.id.Btn_Group_a);
//Add 2011/09/12 Z01_h_yamada Start -->
        btnA.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_a), Constants.KANA_FONT_RATE));
//Add 2011/09/12 Z01_h_yamada End <--
		btnA.setOnClickListener(OnGroupBtnListener);

		btnKa = (Button) oview.findViewById(this, R.id.Btn_Group_ka);
//Add 2011/09/12 Z01_h_yamada Start -->
        btnKa.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_ka), Constants.KANA_FONT_RATE));
//Add 2011/09/12 Z01_h_yamada End <--
		btnKa.setOnClickListener(OnGroupBtnListener);

		btnSa = (Button) oview.findViewById(this, R.id.Btn_Group_sa);
//Add 2011/09/12 Z01_h_yamada Start -->
        btnSa.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_sa), Constants.KANA_FONT_RATE));
//Add 2011/09/12 Z01_h_yamada End <--
		btnSa.setOnClickListener(OnGroupBtnListener);

		btnTa = (Button) oview.findViewById(this, R.id.Btn_Group_ta);
//Add 2011/09/12 Z01_h_yamada Start -->
        btnTa.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_ta), Constants.KANA_FONT_RATE));
//Add 2011/09/12 Z01_h_yamada End <--
		btnTa.setOnClickListener(OnGroupBtnListener);

		btnNa = (Button) oview.findViewById(this, R.id.Btn_Group_na);
//Add 2011/09/12 Z01_h_yamada Start -->
        btnNa.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_na), Constants.KANA_FONT_RATE));
//Add 2011/09/12 Z01_h_yamada End <--
		btnNa.setOnClickListener(OnGroupBtnListener);

		btnHa = (Button) oview.findViewById(this, R.id.Btn_Group_ha);
//Add 2011/09/12 Z01_h_yamada Start -->
        btnHa.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_ha), Constants.KANA_FONT_RATE));
//Add 2011/09/12 Z01_h_yamada End <--
		btnHa.setOnClickListener(OnGroupBtnListener);

		btnMa = (Button) oview.findViewById(this, R.id.Btn_Group_ma);
//Add 2011/09/12 Z01_h_yamada Start -->
        btnMa.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_ma), Constants.KANA_FONT_RATE));
//Add 2011/09/12 Z01_h_yamada End <--
		btnMa.setOnClickListener(OnGroupBtnListener);

		btnYa = (Button) oview.findViewById(this, R.id.Btn_Group_ya);
//Add 2011/09/12 Z01_h_yamada Start -->
        btnYa.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_ya), Constants.KANA_FONT_RATE));
//Add 2011/09/12 Z01_h_yamada End <--
		btnYa.setOnClickListener(OnGroupBtnListener);

		btnRa = (Button) oview.findViewById(this, R.id.Btn_Group_ra);
//Add 2011/09/12 Z01_h_yamada Start -->
        btnRa.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_ra), Constants.KANA_FONT_RATE));
//Add 2011/09/12 Z01_h_yamada End <--
		btnRa.setOnClickListener(OnGroupBtnListener);

		btnWa = (Button) oview.findViewById(this, R.id.Btn_Group_wa);
//Add 2011/09/12 Z01_h_yamada Start -->
        btnWa.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_wa), Constants.KANA_FONT_RATE));
//Add 2011/09/12 Z01_h_yamada End <--
		btnWa.setOnClickListener(OnGroupBtnListener);
	}

	private int iJumpCount = 0;

	private final OnClickListener OnGroupBtnListener = new OnClickListener() {

		@Override
		public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ 市町村 あかさたな禁止 Start -->
	    	if(DrivingRegulation.CheckDrivingRegulation()){
	    		return;
	    	}
//ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ 市町村 あかさたな禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

			paraBtnSetFocusFalse();

			oView.setSelected(true);
			// curFocusBtn = (Button) oView;

			int btnId = oView.getId();
			int kanaR = 0;

			List<String> list = null;

			switch (btnId) {
			case R.id.Btn_Group_a:
				kanaR = R.string.kana_a;
				list = AKeyName;
				break;
			case R.id.Btn_Group_ka:
				kanaR = R.string.kana_ka;
				list = KaKeyName;
				break;
			case R.id.Btn_Group_sa:
				kanaR = R.string.kana_sa;
				list = SaKeyName;
				break;
			case R.id.Btn_Group_ta:
				kanaR = R.string.kana_ta;
				list = TaKeyName;
				break;
			case R.id.Btn_Group_na:
				kanaR = R.string.kana_na;
				list = NaKeyName;
				break;
			case R.id.Btn_Group_ha:
				kanaR = R.string.kana_ha;
				list = HaKeyName;
				break;
			case R.id.Btn_Group_ma:
				kanaR = R.string.kana_ma;
				list = MaKeyName;
				break;
			case R.id.Btn_Group_ya:
				kanaR = R.string.kana_ya;
				list = YaKeyName;
				break;
			case R.id.Btn_Group_ra:
				kanaR = R.string.kana_ra;
				list = RaKeyName;
				break;
			case R.id.Btn_Group_wa:
				kanaR = R.string.kana_wa;
				list = WaKeyName;
				break;
			}

			setGroupValue(kanaR);
			// yangyang mod start 20100313
			//yangyang add start Bug617
			String checkValue = getResources().getString(kanaR);
			//yangyang add end Bug617
			// グループボタンのクリック事件ため、
			if (AppInfo.isEmpty(clickValue)
//					|| !clickValue.equals(oGroupValue.getText().toString())) {
					|| !clickValue.equals(checkValue)
					|| list == null) {

				iJumpCount = 0;
				//yangyang mod start  Bug617
//				clickValue = oGroupValue.getText().toString();
				clickValue = checkValue;
				//yangyang mod end  Bug617
			} else {
				iJumpCount++;
				iJumpCount = iJumpCount % list.size();
			}
			// if (curFocusBtn != oView) {
			// iJumpCount = 0;
			// } else {
			// iJumpCount ++;
			// iJumpCount %= list.size();
			// }
			// yangyang mod end 20100313
			if (list != null) {
				String msName = list.get(iJumpCount);
				long size = nameSize;
				onClickTb(size, msName);
			}
		}
	};

	private void setGroupValue(int kanaR) {
		//yangyang del start Bug617
//		oGroupValue.setText(kanaR);
		//yangyang del end Bug617
		setCurrentGroupStartShow();
	}

	protected void setCurrentGroupStartShow() {

	}

	/**
	 * 地域を表示
	 *
	 * @param RecIndex
	 */
	private void showCityList(long RecIndex) {

		// RecIndex = 0;
		// showList(RecIndex,(int) TEN_PAGE_COUNT, 0);

		int pos = 0;
		// for (int i = 0; i < getRec.lcount; i++) {
		// String name = genreList.get(i).getM_Name();
		// int a = name.lastIndexOf("]");
		// if(a > 0){
		// String str = name.substring(a+1, name.length());
		//
		// if(str.equals(ms1Str) || str.equals(ms2Str) || str.equals(ms3Str)){
		// pos = i;
		// break;
		// }
		// }else{
		// if(name.equals(ms1Str)
		// || name.equals(ms2Str)
		// || name.equals(ms3Str)){
		// pos = i;
		// break;
		// }
		// }
		// }

		getJump();

		NaviRun.GetNaviRunObj().JNI_NE_POIGnr_AddrIndex(Index);
		initCityText();
		pos = 0;
		// index[0] 全市区町村
		if (-1 != Index.lcount) {
			pos = (int) Index.lcount;

		}
		while (pos >= genreList.size()) {
			showList(genreList.size(), (int) TEN_PAGE_COUNT, -1);
		}

//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"showCityList do");
		initCount();
		setCarPos(pos, allRecordCount - 1);
		//yangyang del start Bug617
//		String groupId = getGroupValue(pos);
//		if (AppInfo.isEmpty(groupId)) {
//			oGroupValue.setText(getGroupValue(pos));
//		}
		//yangyang del end Bug617
	}

	// yangyang add start 20110413
	private void initCount() {
		allRecordCount = getItemCount();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"allRecordCount==" + allRecordCount);

	}

	// yangyang add end 20110413
	/**
	 * 初期化時、50音表示区を初期化する
	 *
	 * */
	private void initCityText() {

		int id = R.string.kana_a;

		if (btnA != null && btnA.isEnabled()) {

		} else if (btnKa != null && btnKa.isEnabled()) {
			id = R.string.kana_ka;
		} else if (btnSa != null && btnSa.isEnabled()) {
			id = R.string.kana_sa;
		} else if (btnTa != null && btnTa.isEnabled()) {
			id = R.string.kana_ta;
		} else if (btnNa != null && btnNa.isEnabled()) {
			id = R.string.kana_na;
		} else if (btnHa != null && btnHa.isEnabled()) {
			id = R.string.kana_ha;
		} else if (btnMa != null && btnMa.isEnabled()) {
			id = R.string.kana_ma;
		} else if (btnYa != null && btnYa.isEnabled()) {
			id = R.string.kana_ya;
		} else if (btnRa != null && btnRa.isEnabled()) {
			id = R.string.kana_ra;
		} else if (btnWa != null && btnWa.isEnabled()) {
			id = R.string.kana_wa;
		}

		setGroupValue(id);
	}

	/**
	 * ジャンプキーリスト取得
	 */
	private void getJump() {
		paraBtnSetEnableFalse();
		setNewValue();
		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"getJump >> "+jumpRecCount.getM_sJumpRecCount());

		for (int j = 0; j < jumpRecCount.getM_sJumpRecCount(); j++) {
			// System.out.println(jumpKey[j].pucName);
			if (/*
				 * JumpKey[j].pucName.equals(this.getResources().getString(R.string
				 * .btn_A_name)) ||
				 * JumpKey[j].pucName.equals(this.getResources()
				 * .getString(R.string.btn_I_name)) ||
				 * JumpKey[j].pucName.equals(
				 * this.getResources().getString(R.string.btn_U_name)) ||
				 * JumpKey
				 * [j].pucName.equals(this.getResources().getString(R.string
				 * .btn_E_name)) ||
				 * JumpKey[j].pucName.equals(this.getResources()
				 * .getString(R.string.btn_O_name))
				 */
					//yangyang mod start Bug617
//			check(jumpKey[j].pucName,
//					this.getResources().getStringArray(R.array.check_group_a))) {
			check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_a))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_i))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_u))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_e))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_o))) {
				//yangyang mod end Bug617

				AKeyName.add(jumpKey[j].pucName);
				btnA.setEnabled(true);

			} else if (/*
						 * JumpKey[j].pucName.equals(this.getResources().getString
						 * (R.string.btn_KA_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_KI_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_KU_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_KE_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_KO_name))
						 */
					//yangyang mod start Bug617
//			check(jumpKey[j].pucName,
//					this.getResources().getStringArray(R.array.check_group_ka))) {
			check(jumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ka))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_ki))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_ku))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_ke))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_ko))) {
				//yangyang mod end Bug617
				KaKeyName.add(jumpKey[j].pucName);
				btnKa.setEnabled(true);

			} else if (/*
						 * JumpKey[j].pucName.equals(this.getResources().getString
						 * (R.string.btn_SA_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_SHI_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_SU_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_SE_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_SO_name))
						 */
					//yangyang mod start Bug617
//			check(jumpKey[j].pucName,
//					this.getResources().getStringArray(R.array.check_group_sa))) {
			check(jumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_sa))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_si))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_su))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_se))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_so))) {
				//yangyang mod end Bug617
				SaKeyName.add(jumpKey[j].pucName);
				btnSa.setEnabled(true);

			} else if (/*
						 * JumpKey[j].pucName.equals(this.getResources().getString
						 * (R.string.btn_TA_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_CHI_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_TSU_name)) ||
						 * JumpKey[j].pucName.equals(
						 * this.getResources().getString(R.string.btn_TE_name))
						 * ||
						 * JumpKey[j].pucName.equals(this.getResources().getString
						 * (R.string.btn_TO_name))
						 */
					//yangyang mod start Bug617
//			check(jumpKey[j].pucName,
//					this.getResources().getStringArray(R.array.check_group_ta))) {
			check(jumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ta))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_ti))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_tu))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_te))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_to))) {
				//yangyang mod end Bug617
				TaKeyName.add(jumpKey[j].pucName);
				btnTa.setEnabled(true);

			} else if (/*
						 * JumpKey[j].pucName.equals(this.getResources().getString
						 * (R.string.btn_NA_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_NI_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_NU_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_NE_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_NO_name))
						 */
					//yangyang mod start Bug617
//			check(jumpKey[j].pucName,
//					this.getResources().getStringArray(R.array.check_group_na))) {
			check(jumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_na))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_ni))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_nu))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_ne))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_no))) {
				//yangyang mod end Bug617
				NaKeyName.add(jumpKey[j].pucName);
				btnNa.setEnabled(true);

			} else if (/*
						 * JumpKey[j].pucName.equals(this.getResources().getString
						 * (R.string.btn_HA_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_HI_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_FU_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_HE_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_HO_name))
						 */
					//yangyang mod start Bug617
//			check(jumpKey[j].pucName,
//					this.getResources().getStringArray(R.array.check_group_ha))) {
			check(jumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ha))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_hi))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_hu))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_he))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_ho))) {
				//yangyang mod end Bug617
				HaKeyName.add(jumpKey[j].pucName);
				btnHa.setEnabled(true);

			} else if (/*
						 * JumpKey[j].pucName.equals(this.getResources().getString
						 * (R.string.btn_MA_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_MI_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_MU_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_ME_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_MO_name))
						 */
					//yangyang mod start Bug617
//			check(jumpKey[j].pucName,
//					this.getResources().getStringArray(R.array.check_group_ma))) {
			check(jumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ma))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_mi))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_mu))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_me))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_mo))) {
				//yangyang mod end Bug617
				MaKeyName.add(jumpKey[j].pucName);
				btnMa.setEnabled(true);

			} else if (/*
						 * JumpKey[j].pucName.equals(this.getResources().getString
						 * (R.string.btn_YA_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_YU_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_YO_name))
						 */
					//yangyang mod start Bug617
//			check(jumpKey[j].pucName,
//					this.getResources().getStringArray(R.array.check_group_ya))) {
			check(jumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ya))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_yu))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_yo))) {
				//yangyang mod end Bug617
				YaKeyName.add(jumpKey[j].pucName);
				btnYa.setEnabled(true);

			} else if (/*
						 * JumpKey[j].pucName.equals(this.getResources().getString
						 * (R.string.btn_RA_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_RI_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_RU_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_RE_name)) ||
						 * JumpKey
						 * [j].pucName.equals(this.getResources().getString
						 * (R.string.btn_RO_name))
						 */
					//yangyang mod start Bug617
//			check(jumpKey[j].pucName,
//					this.getResources().getStringArray(R.array.check_group_ra))) {
			check(jumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ra))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_ri))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_ru))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_re))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_ro))) {
				//yangyang mod end Bug617
				RaKeyName.add(jumpKey[j].pucName);
				btnRa.setEnabled(true);

			} else if (/*
						 * JumpKey[j].pucName.equals(this.getResources().getString
						 * (R.string.btn_WA_name)) ||
						 * JumpKey[j].pucName.equals(this
						 * .getResources().getString(R.string.btn_WO_name))
						 */
//			check(jumpKey[j].pucName,
//					this.getResources().getStringArray(R.array.check_group_wa))) {
			check(jumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_wa))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_wo))
					|| check(jumpKey[j].pucName, this.getResources().getStringArray(R.array.check_group_nn))) {

				WaKeyName.add(jumpKey[j].pucName);
				btnWa.setEnabled(true);

			}
		}

	}

	/**
	 * Created on 2009/12/30 Title:paraBtnSetEnableFalse Description:
	 * Buttonを押下不可に設定する
	 *
	 * @param 無し
	 * @return void 無し
	 * @author: sun.dw
	 * @version 1.0
	 */
	private void paraBtnSetEnableFalse() {
		btnA.setEnabled(false);
		btnKa.setEnabled(false);
		btnSa.setEnabled(false);
		btnTa.setEnabled(false);
		btnNa.setEnabled(false);
		btnHa.setEnabled(false);
		btnMa.setEnabled(false);
		btnYa.setEnabled(false);
		btnRa.setEnabled(false);
		btnWa.setEnabled(false);
	}

	private void paraBtnSetFocusFalse() {
		btnA.setSelected(false);
		btnKa.setSelected(false);
		btnSa.setSelected(false);
		btnTa.setSelected(false);
		btnNa.setSelected(false);
		btnHa.setSelected(false);
		btnMa.setSelected(false);
		btnYa.setSelected(false);
		btnRa.setSelected(false);
		btnWa.setSelected(false);
	}

	/**
	 * 監視しているＥｖｅｎｔに対する処理
	 *
	 * @param size
	 * @param str
	 */
	private void onClickTb(long size, String str) {
		JNILong jumpRecIndex = new JNILong();
		NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetJumpList2RecordIndex(size,
				str, jumpRecIndex);
		long Idx = jumpRecIndex.lcount;
//		 NaviLog.d(NaviLog.PRINT_LOG_TAG,"----------->> click: size=" + size +", str=" +
//		 str +", Idx=" + Idx + ",getItemCount()=" + getItemCount());

		// yangyang mod start 20110313
		while (Idx >= genreList.size()) {
			g_RecIndex += TEN_PAGE_COUNT;
			showList(g_RecIndex, TEN_PAGE_COUNT, 0);
		}

		if (Idx > getItemCount() - ONE_PAGE_COUNT) {
			//yangyang bug617 start
			//最後Groupボタンを押下する場合、最後レコードは表示できないため、「-1」処理を削除する
//			Idx = getItemCount() - ONE_PAGE_COUNT-1;
			Idx = getItemCount() - ONE_PAGE_COUNT;
			//yangyang bug617 end
		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onClickTb do===" + (Idx - 1));
		scrollList.scroll((int) Idx - 1);

		// if (Idx >= genreList.size()) {
		// g_RecIndex += TEN_PAGE_COUNT;
		// showList(g_RecIndex, TEN_PAGE_COUNT, 0);
		// }else{
		// int iLineIndex = (int) (Idx / ONE_PAGE_COUNT * ONE_PAGE_COUNT);
		// scrollList.scroll(iLineIndex);
		// }
		// yangyang mod end 20110313
	}

	/**
	 * Created on 2009/12/30 Title:search Description: 検索レベルによって、当該Listを取得する
	 *
	 * @param 無し
	 * @return void 無し
	 * @author: sun.dw
	 * @version 1.0
	 */
	protected void goNext() {
		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"goNext case " + treeIndex.lcount);

		Intent intent = new Intent();
		Intent oIntent = getIntent();
		if (oIntent != null) {
			intent.putExtras(oIntent);
		}

		intent.putExtra(Constants.PARAMS_TREERECINDEX, treeIndex.lcount);
		intent.putExtra(Constants.PARAMS_SEARCH_KEY, meTextName);
		intent.putExtra(Constants.PARAMS_LIST_INDEX, lTreeRecIndex);
		intent.setClass(this, GenreInquiryResult.class);
//Chg 2011/09/23 Z01yoneya Start -->
//		startActivityForResult(intent, 0x5);
//------------------------------------------------------------------------------------------------------------------------------------
		NaviActivityStarter.startActivityForResult(this, intent, 0x5);
//Chg 2011/09/23 Z01yoneya End <--

	}

	/**
	 *
	 * Created on 2010/01/25 Title：setNewValue Description: 変数は空間を申請します
	 *
	 * @param 無し
	 * @return void 無し
	 * @version 1.0
	 */
	private void setNewValue() {
		if (null == AKeyName) {
			AKeyName = new ArrayList<String>();
		} else {
			AKeyName.clear();
		}

		if (null == WaKeyName) {
			WaKeyName = new ArrayList<String>();
		} else {
			WaKeyName.clear();
		}

		if (null == KaKeyName) {
			KaKeyName = new ArrayList<String>();
		} else {
			KaKeyName.clear();
		}

		if (null == SaKeyName) {
			SaKeyName = new ArrayList<String>();
		} else {
			SaKeyName.clear();
		}

		if (null == TaKeyName) {
			TaKeyName = new ArrayList<String>();
		} else {
			TaKeyName.clear();
		}

		if (null == NaKeyName) {
			NaKeyName = new ArrayList<String>();
		} else {
			NaKeyName.clear();
		}

		if (null == HaKeyName) {
			HaKeyName = new ArrayList<String>();
		} else {
			HaKeyName.clear();
		}

		if (null == MaKeyName) {
			MaKeyName = new ArrayList<String>();
		} else {
			MaKeyName.clear();
		}
		if (null == YaKeyName) {
			YaKeyName = new ArrayList<String>();
		} else {
			YaKeyName.clear();
		}

		if (null == RaKeyName) {
			RaKeyName = new ArrayList<String>();
		} else {
			RaKeyName.clear();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent oIntent) {
		// clear count
		if (resultCode == Constants.RESULTCODE_BACK) {
			iJumpCount = 0;
		}
		super.onActivityResult(requestCode, resultCode, oIntent);
	}

	private class CityList extends BaseAdapter {

		@Override
		public int getCount() {
			// 市区町村以外のレコード数
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"getCount do== allRecordCount=="
//					+ allRecordCount);
			// yangyang mod start 20110413
			// int iCount = getItemCount();
			int iCount = allRecordCount;
//Chg 2011/06/13 Z01thedoanh Start -->
			//if (iCount > 5) {
			if (iCount > 0) {
//Chg 2011/06/13 Z01thedoanh End <--
				iCount--;
			}
			return iCount;
			// yangyang mod start 20110413
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		// yangyang add start
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		// yangyang add end

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int wPos, View oView, ViewGroup parent) {
			//yangyang mod start Bug1032,Bug617
			LinearLayout oLayout = null;
			LinearLayout oLayoutBtn = null;
//	 		ButtonImg obtn = null;
	 		TextView oGroupValue = null;
	 		TextView obtn = null;
	 		ImageView oCarView = null;

//			if (oView == null) {
//				oLayout = new LinearLayout(GenreInquiryCity.this);
//				oLayout.setGravity(Gravity.CENTER_VERTICAL);
//				oLayout.setPadding(5, 0, 5, 0);
//				obtn = new ButtonImg(GenreInquiryCity.this);
//				obtn.setTextSize(26);
//				obtn.setId(R.id.btnOk);
//				//yangyang mod start Bug1032
//				obtn.setOneLine(obtn);
////				obtn.setSingleLine(true);
////				obtn.setEllipsize(TruncateAt.START);
//				obtn.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
//				//yangyang mod end Bug1032
//				//yangyang mod start Bug617
////				obtn.setBackgroundResource(R.drawable.btn_380_70_225);
//				obtn.setBackgroundResource(R.drawable.btn_428_70);
//				//yangyang mod end	 Bug617
//				oLayout.addView(obtn);
////				oView = obtn;
//				oView = oLayout;
//			} else {
//				// obtn = (ButtonImg)oView;
////				obtn = (ButtonImg) oView.findViewById(R.id.btnOk);
//				oLayout = (LinearLayout)oView;
//				obtn = (ButtonImg)oLayout.findViewById(R.id.btnOk);
//			}

			if(oView == null || !(oView instanceof LinearLayout))
			{
				LayoutInflater oInflater = LayoutInflater.from(GenreInquiryCity.this);
				oLayout = (LinearLayout)oInflater.inflate(R.layout.button_428_70, null);
//Del 2011/07/21 Z01thedoanh Start -->
				//oLayout.setPadding(5, 0, 5, 0);
//Del 2011/07/21 Z01thedoanh End <--
				oView = oLayout;
			} else {
				oLayout = (LinearLayout)oView;
			}
			oLayoutBtn = (LinearLayout) oLayout.findViewById(R.id.LinearLayout_button);
			oGroupValue = (TextView)oLayout.findViewById(R.id.TextView_groupShow);
			obtn =  (TextView)oLayout.findViewById(R.id.TextView_Value);
			oCarView =  (ImageView)oLayout.findViewById(R.id.ImageView_car);
			//yangyang mod end Bug1032,Bug617

//			if (scrollList.getIsScroll() && getGroupValue(wPos) != 0) {
			//yangyang del start Bug617
//			if (scrollList.getIsScroll() && AppInfo.isEmpty(getGroupValue(wPos))) {
//				oGroupValue.setText(getGroupValue(wPos));
//			}
			//yangyang del end Bug617
			// 0 全市区町村のデータ：0
			final int dataIndex = wPos + 1;
			if (wPos != 0 && dataIndex % TEN_PAGE_COUNT == 0
					&& dataIndex >= genreList.size()) {
				showList(dataIndex, (int) TEN_PAGE_COUNT, -1);
			}
			//yangyang add start Bug617
			String groupValue = getGroupValue(wPos);
			//yangyang add end Bug617
			if (dataIndex < genreList.size()) {

				//yangyang mod start Bug617
				if (!AppInfo.isEmpty(groupValue)) {
					oGroupValue.setText(Constants.LEFT_BRACKETS + groupValue + Constants.RIGHT_BRACKETS);
//				obtn.setText(Constants.LEFT_BRACKETS + groupValue + Constants.RIGHT_BRACKETS +
//						genreList.get(dataIndex).getM_Name());

				} else {
					oGroupValue.setText(Constants.SIX_SPACE);
//					obtn.setText(Constants.SIX_SPACE +
//							genreList.get(dataIndex).getM_Name());
				}
				obtn.setText(genreList.get(dataIndex).getM_Name());
				//yangyang mod end Bug617
				if (AppInfo.isEmpty(genreList.get(dataIndex).getM_Name())) {
					obtn.setEnabled(false);
				} else {
					obtn.setEnabled(true);
				}

				// if (myProvinceId == WideCode)
				{
					// if ((!AppInfo.isEmpty(ms2Str) &&
					// obtn.getText().toString().equals(ms2Str)) ||
					// (!AppInfo.isEmpty(ms3Str) &&
					// obtn.getText().toString().equals(ms3Str))) {
					if (Index.lcount == dataIndex) {
//						obtn = obtn.initBtn(obtn,
//								R.drawable.icon_current_normal, ButtonImg.RIGHT);
						oCarView.setBackgroundResource(R.drawable.icon_current_normal);
						oCarView.setVisibility(ImageView.VISIBLE);
					} else {
//						obtn = obtn.initBtn(obtn, -1, ButtonImg.RIGHT);
						oCarView.setVisibility(ImageView.GONE);
					}
				}
				// else {
				// obtn = obtn.initBtn(obtn,-1,obtn.RIGHT);
				// }
				//yangyang mod start Bug1032
				// setTextIcon(wPos, obtn);
//				obtn.setPadding(10, 12, 10, 10);
//				obtn.superPadding(5, 8, 10, 12);

				 //yangyang mod end Bug1032
				oLayoutBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ 市町村 選択禁止 Start -->
				    	if(DrivingRegulation.CheckDrivingRegulation()){
				    		return;
				    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ 市町村 選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
						onItemClick(dataIndex);
					}
				});
			} else {
				obtn.setText("");
				obtn.setEnabled(false);
			}

			return oView;
		}
	}

	ArrayList<String[]> listGroupValueIndex = new ArrayList<String[]>();

	private String getGroupValue(int startIndex) {
		String sResult = "";
//		int localIndex = 0;
		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"listGroupValueIndex.size()==" +
		// listGroupValueIndex.size());
		for (int i = 0; i < listGroupValueIndex.size(); i++) {
			String[] sValue = (String[]) listGroupValueIndex.get(i);
			int index = Integer.parseInt(sValue[0]);
			//yangyang mod start bug617
//			if (index <= startIndex) {
//				if (AppInfo.isEmpty(sResult)) {
//					sResult = sValue[1];
//					localIndex = index;
//				} else {
//					if (localIndex <= index) {
//						sResult = sValue[1];
//						localIndex = index;
//					}
//				}
//
//			}
			if (index - 1 == startIndex) {
				sResult = sValue[1];
//				scrollGroupIndex= index;
			}
			//yangyang mod end bug617
		}
		// NaviLog.d(NaviLog.PRINT_LOG_TAG,"sResult===" + sResult);
		return checkGroupBtnShow(sResult.trim());
	}

	@Override
	protected void initScrollGroupShowValue(JNIJumpKey[] jumpKey2,
			JNIShort jumpRecCount2) {

		for (int i = 0; i < jumpRecCount2.getM_sJumpRecCount(); i++) {
			String msName = jumpKey2[i].pucName;
			long NameSize = jumpKey2[i].NameSize;
			String[] sValue = new String[2];

			JNILong JumpRecIndex = new JNILong();
			NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetJumpList2RecordIndex(
					NameSize, msName, JumpRecIndex);
			sValue[0] = String.valueOf(JumpRecIndex.lcount);
			// NaviLog.d(NaviLog.PRINT_LOG_TAG,"hahah sValue[0]========sValue[1]====" +
			// sValue[0] + "======" + msName);
			sValue[1] = msName;
			listGroupValueIndex.add(sValue);

		}
	}
}
