/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           GPSStatusListener.java
 * Description    GPSを通して、衛星の情報を得ます
 * Created on     2010/08/02
 *
********************************************************************
*/
package net.zmap.android.pnd.v2.gps;


import android.location.GpsSatellite;
import android.location.GpsStatus;

import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.LoggerManager;
import net.zmap.android.pnd.v2.common.services.MonitorAdapter;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.ZVP_SatInfoList_t;
import net.zmap.android.pnd.v2.data.ZVP_SatInfo_t;
import net.zmap.android.pnd.v2.maps.MapView;

import java.util.Iterator;

public class GPSStatusListener implements GpsStatus.Listener
{
//Del 2011/09/01 Z01yoneya Start -->AndroidのGpsStatusクラスで定義されているので、ここで定義しない
//    private static final int GPS_EVENT_FIRST_FIX = 0x00000003;
//    private static final int GPS_EVENT_SATELLITE_STATUS = 0x00000004;
//    private static final int GPS_EVENT_STARTED = 0x00000001;
//    private static final int GPS_EVENT_STOPPED = 0x00000002;
//Del 2011/09/01 Z01yoneya End <--
//Del 2011/09/01 Z01yoneya Start -->とくに使われてないのでコメントアウトする
//    private JNIInt loadMode = new JNIInt();
//Del 2011/09/01 Z01yoneya End <--
	// V2.5 GPS状態収集
	public static int g_iFixCount = 0;
	public static float g_snrAverageUsedInFix = 0.0f;
    private GpsStatus mGpsStatus;
    private static ZVP_SatInfoList_t SatInfoList_t = new ZVP_SatInfoList_t();
    private static JNITwoLong LatLon = new JNITwoLong();
//    private LocationManager locationManager = GPSManager.locationManager;
//    private MyLocationListener listener = new MyLocationListener();
//    private GPSSensorData sensorData = new GPSSensorData();
    private static Iterable<GpsSatellite> allSatellites;
	private static boolean mSensorDisableGPS = false;// Add 2011/03/17 Z01ONOZAWA
	private static boolean bIsUploadVPLog = false;
	private static int gpsState = -1;
//Add 2011/08/26 Z01yoneya Start -->
    private LocationStatusListner mLocationStatusListner = null;   //
//Add 2011/08/26 Z01yoneya End <--

    /**
     * Created on 2010/08/02
     * Title:       onGpsStatusChanged
     * Description:  GPS衛星の情報が変える時、この関数を触発します
     * @param  event  事件ID
     * @return      無し
     * @version        1.0
    */
    @Override
    public void onGpsStatusChanged(int event) {
// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
		if (mSensorDisableGPS) return;
// Add 2011/03/17 Z01ONOZAWA End   <--
        mGpsStatus = GPSManager.locationManager.getGpsStatus(null);

         switch(event)
         {
         case GpsStatus.GPS_EVENT_FIRST_FIX:
             //NaviLog.d("GPS", "onGpsStatusChanged() GPS_EVENT_FIRST_FIX");
//Del 2011/09/01 Z01yoneya Start -->取得した値が未使用なのでコメントアウトする
//             mGpsStatus.getTimeToFirstFix();
//Del 2011/09/01 Z01yoneya End <--
             break;
         case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
             //NaviLog.d("GPS", "onGpsStatusChanged() GPS_EVENT_SATELLITE_STATUS");
        	 int satNum = 0;
             int iFixCount = 0;
        	 allSatellites = mGpsStatus.getSatellites();
        	 Iterator<GpsSatellite> it = allSatellites.iterator();
             while(it.hasNext())
             {
                 GpsSatellite gpsS = (GpsSatellite)it.next();
                 if(null == gpsS)
                 {
                     break;
                 }
                 if(gpsS.usedInFix()){
                	 iFixCount++;
                 }
                 satNum++;
             }

//Add 2011/08/26 Z01yoneya Start -->
             float snrAverageUsedInFix = 0.0f;
//Add 2011/08/26 Z01yoneya End <--
             SatInfoList_t.stVPSatInfo = new ZVP_SatInfo_t[satNum];

             Iterator<GpsSatellite> iterator = allSatellites.iterator();
             satNum = 0;
             while(iterator.hasNext())
             {
                 GpsSatellite gps = (GpsSatellite)iterator.next();
                 if(null == gps)
                 {
                     break;
                 }

                 SatInfoList_t.stVPSatInfo[satNum] = new ZVP_SatInfo_t();
                 SatInfoList_t.stVPSatInfo[satNum].wSatID = gps.getPrn();
                 SatInfoList_t.stVPSatInfo[satNum].wAzimuth =  gps.getAzimuth();
                 SatInfoList_t.stVPSatInfo[satNum].wElevation =  gps.getElevation();
                 SatInfoList_t.stVPSatInfo[satNum].wSNRate = gps.getSnr();
                 SatInfoList_t.stVPSatInfo[satNum].bEnable = gps.usedInFix();
//Add 2011/08/26 Z01yoneya Start -->
                 if(gps.usedInFix()){
                     snrAverageUsedInFix = snrAverageUsedInFix +  gps.getSnr();
                     //NaviLog.d("gps", "add snr=" + gps.getSnr());
                 }
//Add 2011/08/26 Z01yoneya End <--
                 satNum++;
             }
//Add 2011/08/26 Z01yoneya Start -->
             if(iFixCount != 0){
                 snrAverageUsedInFix =snrAverageUsedInFix / iFixCount;
             }
             //NaviLog.d("gps", "snr average=" + snrAverageUsedInFix);
//Add 2011/08/26 Z01yoneya End <--
             LatLon = CommonLib.getSensorData();
             // 2013.06.05 NullPointerException 対策
             if(LatLon != null) {
            	 SatInfoList_t.wTotalSatsInView = satNum;
            	 SatInfoList_t.lnLatitude1000 = LatLon.getM_lLat();
            	 SatInfoList_t.lnLongitude1000 = LatLon.getM_lLong();
            	 SatInfoList_t.dAltitude = CommonLib.getAltitude();

//Del 2011/09/01 Z01yoneya Start -->取得した値が未使用なのでコメントアウトする
//             NaviRun.GetNaviRunObj().JNI_VP_GetLoadMode(loadMode);
//Del 2011/09/01 Z01yoneya End <--
            	 CommonLib.setSatInfoList_t(SatInfoList_t);
             }
             //Fix bug 493

//Chg 2011/08/26 Z01yoneya Start -->
//            if(iFixCount == 0){
//                gpsState = MapView.GPS_NO;
//            }else if(iFixCount > 0 && iFixCount < 3){
//                gpsState = MapView.GPS_BAD;
//            }else{
//                gpsState = MapView.GPS_NORMAL;
//            }
//             NaviLog.e("GPS", "-----GPS_EVENT_SATELLITE_STATUS, usable gps count: "+iFixCount);
//             if((gpsState != -1) && (MapView.getInstance() != null) && !bIsUploadVPLog) {
//                 MapView.getInstance().updateGPSIcon(gpsState);
//             }
//             // end
//             // Add 2010/10/19 sawada Start -->
//             if (Constants.VPLOG_OUTPUT_LOGGER == Constants.ON) {
////Chg 2011/08/10 Z01yoneya Start -->
////                 LoggerManager.writeLog(mGpsStatus);
////--------------------------------------------
//                 LoggerManager.writeLog(mGpsStatus, System.currentTimeMillis());
////Chg 2011/08/10 Z01yoneya End <--
//
//             }
//--------------------------------------------
             if(mLocationStatusListner != null){
				// V2.5 GPS状態収集
            	g_iFixCount = iFixCount;
            	g_snrAverageUsedInFix = snrAverageUsedInFix;
                 mLocationStatusListner.OnUpdateGpsStatus(iFixCount, snrAverageUsedInFix);
             }

             if (Constants.VPLOG_OUTPUT_LOGGER == Constants.ON) {
                 LoggerManager.writeLog(mGpsStatus, System.currentTimeMillis());
             }

//Chg 2011/08/26 Z01yoneya End <--

             // Add 2010/10/19 sawada End   <--
// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
			 MonitorAdapter monitor = MonitorAdapter.getInst();
			 if (monitor != null) monitor.mTarget.cloneMonitorSat(System.currentTimeMillis(), SatInfoList_t.wTotalSatsInView);
// Add 2011/03/17 Z01ONOZAWA End   <--
             break;
         case GpsStatus.GPS_EVENT_STARTED:
             //NaviLog.d("GPS", "onGpsStatusChanged() GPS_EVENT_STARTED");
             break;
         case GpsStatus.GPS_EVENT_STOPPED:
             //NaviLog.d("GPS", "onGpsStatusChanged() GPS_EVENT_STOPPED");
//Chg 2011/08/26 Z01yoneya Start -->
//        	 if(MapView.getInstance() != null){
//        		 MapView.getInstance().updateGPSIcon(MapView.GPS_NO);
//        	 }
//--------------------------------------------
             if(mLocationStatusListner != null){
                 mLocationStatusListner.OnUpdateGpsStatus(0, 0.0f);
             }
//Chg 2011/08/26 Z01yoneya End <--
             break;
         default:
             break;
         }
    }

    /* V2.5 GPS状態参考 */
    public static int get_iFixCount() {
		return g_iFixCount;
	}
	public static float get_snrAverageUsedInFix() {
		return g_snrAverageUsedInFix;
	}

// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
	public void setSensorDisable(boolean flag) {
		mSensorDisableGPS = flag;
	}
// Add 2011/03/17 Z01ONOZAWA End   <--

// Add 2011/04/25 liufang Start -->493
	public static void setVPLogState(boolean bIsUpLoad){
		bIsUploadVPLog = bIsUpLoad;
//Chg 2011/09/06 Z01yoneya Start -->
//		if(bIsUploadVPLog){
//			if(MapView.getInstance() != null) {
//	            MapView.getInstance().updateGPSIcon(MapView.GPS_ACCESS);
//	        }
//		}else{
//			if(MapView.getInstance() != null) {
//	            MapView.getInstance().updateGPSIcon(gpsState);
//	        }
//		}
//------------------------------------------------------------------------------------------------------------------------------------
        MapView mapView = MapView.getInstance();
        if(mapView != null){
    		if(bIsUploadVPLog){
    	        mapView.updateGPSIcon(MapView.GPS_ACCESS);
    		}else{
    		    mapView.updateGPSIcon(gpsState);
    		}
        }
//Chg 2011/09/06 Z01yoneya End <--
        	}
// Add 2011/04/25 liufang End <--

//Add 2011/08/26 Z01yoneya Start -->
	public void registLocationStatusListner(LocationStatusListner listner)
	{
	    mLocationStatusListner = listner;
	}
//Add 2011/08/26 Z01yoneya End <--
}
