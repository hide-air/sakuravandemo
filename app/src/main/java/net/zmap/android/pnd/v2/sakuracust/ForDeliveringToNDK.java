package net.zmap.android.pnd.v2.sakuracust;

/**
 * 住所情報をNDKへ引き渡すためのインタフェイス<br>
 * このクラスは、C++側でNewObjectを行ってJavaに引き渡す
 * @author Norimasa
 *
 */
public class ForDeliveringToNDK {
	/** アイテム数（C++側の負担軽減のため） */
	public int count;
	
	/** 現在の住所レベル */
	public int mNowAdrLevel;
	
	/** 次階層が存在するか？ */
	int[]    mNextCategoly;
	/** 
	 * 住所コード<br>
	 * （JNI_NE_POIAddr_GetNextTreeIndexやJNI_NE_POIAddr_SearchNextListに引き渡す値） 
	 */
	int[]    mAdrCode;
	/** 住所名称文字数 */
	int[]    mAdrNameLen;
	/** 住所名称utf-8 */
	byte[][] mAdrName;
	/** 経度 */
	double[] mLongitude;
	/** 緯度 */
	double[] mLatitude;
	
	public ForDeliveringToNDK(){}
}
