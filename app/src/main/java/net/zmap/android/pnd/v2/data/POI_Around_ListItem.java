/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           POI_Around_ListItem.java
 * Description    取得周辺ジャンル検索リスト
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class POI_Around_ListItem {
    private long    NameSize;
    private String  pucName;
    private long    Distance;
    private long    Latitude;
    private long    Longitude;
    private long    ExistenceFlg;
    private long     kindeCode;

    public long getKindeCode() {
        return kindeCode;
    }
    public void setKindeCode(long kindeCode) {
        this.kindeCode = kindeCode;
    }
    /**
    * Created on 2010/08/06
    * Title:       getNameSize
    * Description:  表示名称サイズを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getNameSize() {
        return NameSize;
    }
    /**
    * Created on 2010/08/06
    * Title:       getPucName
    * Description:  表示名称を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getPucName() {
        return pucName;
    }
    /**
    * Created on 2010/08/06
    * Title:       setPucName
    * Description:  表示名称を設定する
    * @param1  無し
    * @return       void

    * @version        1.0
    */
    public void setPucName(String pucName) {
        this.pucName = pucName;
    }
    /**
    * Created on 2010/08/06
    * Title:       getDistance
    * Description:  距離を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getDistance() {
        return Distance;
    }
    /**
    * Created on 2010/08/06
    * Title:       getLatitude
    * Description:  経度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLatitude() {
        return Latitude;
    }
    /**
    * Created on 2010/08/06
    * Title:       getLongitude
    * Description:  緯度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getLongitude() {
        return Longitude;
    }
    /**
    * Created on 2010/08/06
    * Title:       getExistenceFlg
    * Description:  存在フラグ（大分類、中分類すべて返す為）を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getExistenceFlg() {
        return ExistenceFlg;
    }

}
