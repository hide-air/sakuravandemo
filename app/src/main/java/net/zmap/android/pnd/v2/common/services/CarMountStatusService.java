package net.zmap.android.pnd.v2.common.services;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class CarMountStatusService {

	private CarMountStatusEventReceiver mCarMountEventReceiver = null;
	private Context m_oContext = null;
	private CarMountStatusEventListener carMountListener = null;


	public CarMountStatusService(Context oContext, CarMountStatusEventListener oListener)
	{
		m_oContext = oContext;
		carMountListener = oListener;

	}

	public void release()
	{
		if(m_oContext != null && mCarMountEventReceiver != null)
		{
			m_oContext.unregisterReceiver(mCarMountEventReceiver);
		}
	}
	public class CarMountStatusEventReceiver extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context oContext, Intent oIntent)
		{
			if(carMountListener != null)
			{
				carMountListener.onCarMountEventChange(oIntent);
			}
		}

	}
}
