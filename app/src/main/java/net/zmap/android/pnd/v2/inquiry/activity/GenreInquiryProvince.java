//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Genre_ListItem;
import net.zmap.android.pnd.v2.inquiry.view.ButtonImg;

public class GenreInquiryProvince extends GenreInquiryBase{

    private ProvinceList provinceList;
    private int allRecordCount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        g_RecIndex = 0;
        initCount();


        if(recCount.lcount == 0){
            showDialog(DIALOG_NODATA_ALARM);

        }else{
        	//yangyang add waitDialog start
        	 startShowPage(NOTSHOWCANCELBTN);
        	//yangyang add waitDialog end
        }
    }

  private void initCount() {
	  NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetRecCount(recCount);
	  allRecordCount = (int)recCount.lcount;
	}

	//yangyang add waitDialog start
    @Override
	protected boolean initWaitBackData() {
    	 provinceList = new ProvinceList();
    	 NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetRecList(g_RecIndex, TEN_PAGE_COUNT, m_Poi_Gnr_Listitem, getRec);

         initAddrData();
//         scrollBox.reset();


//         scrollList.reset();
		return super.initWaitBackData();
	}


	@Override
	protected void resetDialog() {
		initProvinceLayout();
		showAddrFirstData(iCarPosition);

		super.resetDialog();
	}
	//yangyang add waitDialog end

	private void initProvinceLayout(){
        oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base_list_freescroll, null);

        oTextTitle = (TextView) oLayout.findViewById(R.id.inquiry_title);
        oTextTitle.setText(meTextName);

        //
        // //////////////////////
        Button allCountry = (Button) oLayout.findViewById(R.id.inquiry_btn);
        if(allCountry != null){
// Chg 2011/06/02 sawada Start -->
////            allCountry.setBackgroundResource(R.drawable.btn_assign);
//            allCountry.setBackgroundResource(R.drawable.btn_236_99_66);
//            allCountry.setText(R.string.btn_all_country);
//            allCountry.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    provinceItemClick(v, 0, 0);
//                }
//            });
//Chg 2011/07/21 Z01thedoanh Start -->
        	//allCountry.setVisibility(View.INVISIBLE);
        	allCountry.setVisibility(View.GONE);
//Chg 2011/07/21 Z01thedoanh End <--
// Chg 2011/06/02 sawada End   <--
        }

        scrollList = (ScrollList) oLayout.findViewById(R.id.scrollList);
        scrollList.setAdapter(provinceList);
        //yangyang mod start Bug793
        scrollList.setPadding(0, 5, 0, 5);
//        scrollList.setDividerHeight(6);
        scrollList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
        //yangyang mod start Bug793
//Del 2011/10/06 Z01_h_yamada Start -->
//        scrollList.setCountInPage(ONE_PAGE_COUNT);
//Del 2011/10/06 Z01_h_yamada End <--

        oTool = new ScrollTool(this);
        oTool.bindView(scrollList);
        removeOperArea();
        setViewInOperArea(oTool);


        getWorkArea().removeAllViews();
        setViewInWorkArea(oLayout);
    }

    /**
     * Created on   2009/12/30
     * Title:search
     * Description: 検索レベルによって、当該Listを取得する
     * @param  無し
     * @return void 無し
     * @author: sun.dw
     * @version 1.0
     */
    protected void goNext() {
//    	NaviLog.d(NaviLog.PRINT_LOG_TAG,"goNext===start" );
        Intent intent = new Intent();
        Intent oIntent = getIntent();
        if (oIntent != null) {
            intent.putExtras(oIntent);
        }
        intent.putExtra(Constants.PARAMS_TREERECINDEX, treeIndex.lcount);
        intent.putExtra(Constants.PARAMS_SEARCH_KEY, meTextName);
// Chg 2011/06/02 sawada Start -->
//      intent.putExtra(Constants.PARAMS_LIST_INDEX, lTreeRecIndex);
        intent.putExtra(Constants.PARAMS_LIST_INDEX, lTreeRecIndex - 1);
// Chg 2011/06/02 sawada End   <--
// Chg 2011/06/07 sawada Start -->
//      int requestCode = 0x4;
        JNILong nextTreeIndex = new JNILong();
        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetNextTreeIndex(lTreeRecIndex - 1, nextTreeIndex);
        int requestCode = (int)nextTreeIndex.lcount;
// Chg 2011/06/07 sawada End   <--
        //全国
// Chg 2011/06/07 sawada Start -->
//      if(lTreeRecIndex == 0){
        if(lTreeRecIndex == 0 || requestCode == 5){
// Chg 2011/06/07 sawada End   <--
            intent.setClass(this, GenreInquiryResult.class);
            requestCode = 0x5;
        }else{
            intent.setClass(this, GenreInquiryCity.class);
        }
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(intent, requestCode);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, intent, requestCode);
//Chg 2011/09/23 Z01yoneya End <--
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"goNext===end" );
    }


    /**
     * 最前の都道府県のページ自車マーク位置
     * @param iCarPos
     */
    private void showAddrFirstData(int iCarPos) {
        setCarPos(iCarPos / 2, getTotalCount());
    }

    private int getTotalCount(){
        //index[0] 全国
// Chg 2011/06/06 sawada Start -->
//    	NaviLog.d(NaviLog.PRINT_LOG_TAG,"allRecordCount==" + allRecordCount);
//      int count = allRecordCount/* - 1*/;
        int count = 47;
// Chg 2011/06/06 sawada End   <--
        return count / 2 + (count % 2 != 0 ? 1 : 0);

    }
    private void provinceItemClick(View view, int pos, int btnPos) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 施設/ｼﾞｬﾝﾙ 都道府県禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 施設/ｼﾞｬﾝﾙ 都道府県禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

//    	NaviLog.d(NaviLog.PRINT_LOG_TAG,"provinceItemClick start");
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"Province click: pos=" + pos +", button index=" + btnPos);

        if(pos > 0){
            setTitleText(btnPos);
        }
        //yangyang add start
		if (pos == 0 && btnPos == 0) {
			meTextName += Constants.FLAG_TITLE
					+ ((Button) view).getText().toString();
		}
		//yangyang add end
        lTreeRecIndex = pos;
        //yangyang del start
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"old lTreeRecIndex==" + lTreeRecIndex + ",treeIndex==" + treeIndex.lcount);
//        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetNextTreeIndex(lTreeRecIndex, treeIndex);
////        NaviLog.d(NaviLog.PRINT_LOG_TAG,"Province click: lTreeRecIndex=" + lTreeRecIndex + ",treeIndex==" + treeIndex.lcount);
//        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_SearchNextList(lTreeRecIndex);
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"new lTreeRecIndex==" + lTreeRecIndex + ",treeIndex==" + treeIndex.lcount);
      //yangyang del end
        doSearch();

        CommonLib.setBIsSearching(true);
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"provinceItemClick end");
    }

    private POI_Genre_ListItem getData(int wPos){
        if(wPos >=0 && m_Poi_Add_Data != null && wPos < m_Poi_Add_Data.length){
            return m_Poi_Add_Data[wPos];
        }
        return null;
    }

    private class ProvinceList extends BaseAdapter{

		@Override
		public int getCount() {
			return getTotalCount();
		}

		// yangyang add start
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		// yangyang add end
		@Override
		public Object getItem(int position) {
			return null;
		}

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int wPos, View oView, ViewGroup parent) {

            if (oView == null) {
                oView = oInflater.inflate(R.layout.inquiry_province, null);
            }

            ButtonImg obtnLeft = (ButtonImg)oView.findViewById(R.id.inquiry_line_left);
            ButtonImg obtnRight = (ButtonImg)oView.findViewById(R.id.inquiry_line_right);
          //yangyang mod start Bug1032
    		obtnLeft.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
    		obtnRight.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
    		//yangyang mod end Bug1032
            //Real Index in data
            final int dataIndex = wPos * 2 + 1; //(wPos % getCountInBox()) * 2 + pageIndex * getCountInBox();

//            NaviLog.d(NaviLog.PRINT_LOG_TAG,"Province index: "+ dataIndex + ", wPos=" + wPos);

            final POI_Genre_ListItem data_left = getData(dataIndex);
            final POI_Genre_ListItem data_right = getData(dataIndex + 1);

//            NaviLog.d(NaviLog.PRINT_LOG_TAG,"wPos=======data_left.isM_bBtnSts()====" + wPos + "==================" + data_left.isM_bBtnSts());

// Chg 2011/06/06 sawada Start -->
//            if( data_left!= null && data_left.isM_bBtnSts()) {
//                obtnLeft.setEnabled(true);
//                obtnLeft.setText(data_left.getM_Name());
//            }else{
//                obtnLeft.setEnabled(false);
//                obtnLeft.setText("");
//            }
            if (data_left != null) {
            	if (data_left.getM_Name().length() > 0) {
            		obtnLeft.setText(data_left.getM_Name());
            		obtnLeft.setEnabled(data_left.isM_bBtnSts());
            	} else {
            		obtnLeft.setText("");
            		obtnLeft.setEnabled(false);
            	}
            }
// Chg 2011/06/06 sawada End   <--

// Chg 2011/06/06 sawada Start -->
//            if(data_right != null && data_right.isM_bBtnSts()) {
//                obtnRight.setEnabled(true);
//                obtnRight.setText(data_right.getM_Name());
//            }else{
//                obtnRight.setText("");
//                obtnRight.setEnabled(false);
//            }
            if (data_right != null) {
            	if (data_right.getM_Name().length() > 0) {
            		obtnRight.setText(data_right.getM_Name());
            		obtnRight.setEnabled(data_right.isM_bBtnSts());
            	} else {
            		obtnRight.setText("");
            		obtnRight.setEnabled(false);
            	}
            }
// Chg 2011/06/06 sawada End   <--
//
            if (obtnLeft.getText().toString().trim().equals(ms1Str)) {
                myProvinceId = dataIndex;
                obtnLeft = obtnLeft.initBtn(obtnLeft, R.drawable.icon_current_normal, ButtonImg.RIGHT);
            } else {
                obtnLeft = obtnLeft.initBtn(obtnLeft, -1, ButtonImg.RIGHT);
            }
            //yangyang mod start Bug1032
//            obtnLeft.setPadding(10, 12, 10, 10);
            obtnLeft.superPadding(5, 8, 10, 12);
            //yangyang mod end Bug1032
            if (obtnRight.getText().toString().trim().equals(ms1Str)) {
                myProvinceId = dataIndex + 1;
                obtnRight = obtnRight.initBtn(obtnRight,R.drawable.icon_current_normal, ButtonImg.RIGHT);
            } else {
                obtnRight = obtnRight.initBtn(obtnRight, -1, ButtonImg.RIGHT);
            }
            //yangyang mod start Bug1032
//            obtnRight.setPadding(10, 12, 10, 10);
            obtnRight.superPadding(5, 8, 10, 12);
            //yangyang mod end Bug1032
            obtnLeft.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View oView) {
                    if(data_left != null){
                        provinceItemClick(oView, data_left.getM_iOldIndex(), dataIndex);
                    }
                }
            });
            obtnRight.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View oView) {
                    if(data_right != null){
                        provinceItemClick(oView, data_right.getM_iOldIndex(), dataIndex + 1);
                    }
                }
            });
            return oView;
        }
    }
}
