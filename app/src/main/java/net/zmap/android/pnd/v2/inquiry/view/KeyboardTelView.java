package net.zmap.android.pnd.v2.inquiry.view;


import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

public class KeyboardTelView extends ItemView {

//Del 2011/11/01 Z01_h_yamada Start -->
//	private static Activity m_Context = null;
//Del 2011/11/01 Z01_h_yamada End <--

	private static EditText oTitle = null;
	private static Button phoneSearch = null;
	private static Button m_oSearchBtn= null;
	private static Button m_oModBtn= null;
	private static int o_minSize = -1;
	private static int o_maxSize = -1;

	private Button btn_0 = null;
	private Button btn_1 = null;
	private Button btn_2 = null;
	private Button btn_3 = null;
	private Button btn_4 = null;
	private Button btn_5 = null;
	private Button btn_6 = null;
	private Button btn_7 = null;
	private Button btn_8 = null;
	private Button btn_9 = null;
	private static final OnClickListener OnClickLisTelNum = new OnClickListener(){

		@Override
		public void onClick(View v) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 郵便/電話番号 入力禁止 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 郵便/電話番号 入力禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
			int wId = v.getId();
			switch(wId) {
			case R.id.btn_0_tels:
				doChangeTitle("0");

				break;
			case R.id.btn_1_tels:
				doChangeTitle("1");
				break;
			case R.id.btn_2_tels:
				doChangeTitle("2");
				break;
			case R.id.btn_3_tels:
				doChangeTitle("3");
				break;
			case R.id.btn_4_tels:
				doChangeTitle("4");
				break;
			case R.id.btn_5_tels:
				doChangeTitle("5");
				break;
			case R.id.btn_6_tels:
				doChangeTitle("6");
				break;
			case R.id.btn_7_tels:
				doChangeTitle("7");
				break;
			case R.id.btn_8_tels:
				doChangeTitle("8");
				break;
			case R.id.btn_9_tels:
				doChangeTitle("9");
				break;
			}


		}



	};
	public static EditText getoTitle() {
		return oTitle;
	}

	public static void setoTitle(EditText pTitle) {
		oTitle = pTitle;
	}

	/**
	 * コンストラクタ
	 * */
	public KeyboardTelView(Activity oContext, int wId, int wResId) {
			super(oContext, wId, wResId);

			initTelView(oContext);
		}

//Chg 2011/11/01 Z01_h_yamada Start -->
//	/**
//	 * コンストラクタ
//	 * ソフトキーボードの初期化
//	 * @param oContext クラス対象
//	 * @param oBtn 長さによって、限定のボタン対象
//	 * @param minSize 最小長さ
//	 * @param maxSize 最大長さ
//	 * */
//	public KeyboardTelView(final Activity oContext,Button oBtn,int minSize ,int maxSize) {
//		super(oContext, 0, R.layout.keyboard_tel);
//		if (oBtn != null) {
//			m_oSearchBtn = oBtn;
//		} else {
//			m_oSearchBtn = (Button) findViewById(this, R.id.Button_search);
//		}
//
//		o_minSize = minSize;
//		o_maxSize = maxSize;
//		initTelView(oContext);
//	}
//	/**
//	 * コンストラクタ
//	 * ソフトキーボードの初期化
//	 * @param oContext クラス対象
//	 * @param oBtn 長さによって、限定のボタン対象
//	 * @param minSize 最小長さ
//	 * @param maxSize 最大長さ
//	 * */
//	public KeyboardTelView(final Activity oContext,Button oSearchBtn,Button oModBtn,int minSize ,int maxSize) {
//		super(oContext, 0, R.layout.keyboard_tel);
//		if (oSearchBtn != null) {
//			m_oSearchBtn = oSearchBtn;
//		} else {
//			m_oSearchBtn = (Button) findViewById(this, R.id.Button_search);
//		}
//		if (oModBtn != null) {
//			m_oModBtn = oModBtn;
//		} else {
//			m_oModBtn = (Button) findViewById(this, R.id.Button_edit);
//		}
//		o_minSize = minSize;
//		o_maxSize = maxSize;
//
//		initTelView(oContext);
//	}
//--------------------------------------------
	/**
	 * コンストラクタ
	 * ソフトキーボードの初期化
	 * @param oContext クラス対象
	 * @param oBtn 長さによって、限定のボタン対象
	 * @param minSize 最小長さ
	 * @param maxSize 最大長さ
	 * */
	public KeyboardTelView(final Activity oContext,Button oModBtn,int minSize ,int maxSize) {
		super(oContext, 0, R.layout.keyboard_tel);
		if (oModBtn != null) {
			m_oModBtn = oModBtn;
		} else {
			m_oModBtn = (Button) findViewById(this, R.id.Button_edit);
		}
		o_minSize = minSize;
		o_maxSize = maxSize;

		initTelView(oContext);
	}
//Chg 2011/11/01 Z01_h_yamada End <--


	private static boolean bselectionChangeFlag = false;
	/**
	 * タイトル部分の内容をリセットする
	 * @param string リセットの内容
	 *
	 * */
	public static void doChangeTitle(String string) {
		if (!AppInfo.isMatches(string)) {
			return;
		}
		StringBuffer sb  = new StringBuffer();

		int index = 0;
//		if (!isChangeTitle()) {
//
//			oTitle.setText( string );
//			sb.append(oTitle.getText().toString());
//		} else {

		index = oTitle.getSelectionEnd();
		sb.replace(0, sb.length(), oTitle.getText().toString());
		if (sb.length() < o_maxSize) {
			sb.insert(index, string);
		}
		oTitle.setText(sb.toString());
//		}
		if (sb.length() < o_maxSize) {
			oTitle.setSelection(index + 1);
			bselectionChangeFlag = false;
		} else {
			if (index +1 >= o_maxSize) {
				bselectionChangeFlag = false;
				oTitle.setSelection(o_maxSize);
			} else {
				if (!bselectionChangeFlag) {
					oTitle.setSelection(index + 1 );
					bselectionChangeFlag = true;
				} else {
					oTitle.setSelection(index);
				}
			}
		}

	}

	/**
	 * タイトル内容を取得する
	 * @return String タイトルの内容
	 *
	 * */
	public String getTitle() {
		return oTitle.getText().toString().trim();
	}

//	/**
//	 *
//	 *
//	 * */
//	public static boolean isChangeTitle() {
//		if (oTitle.getText().toString().equals(m_Context.getResources().getString(R.string.text_tel_input))) {
//			return false;
//		} else {
//			return true;
//		}
//	}

	/**
	 * 限定値の初期化
	 * @param minSize 最小サイズ
	 * @param maxSize 最大サイズ
	 * */
	public static void setLimited(int minSize,int maxSize) {
		o_minSize = minSize;
		o_maxSize = maxSize;
	}

	private void initTelView(Activity oContext) {
//Del 2011/11/01 Z01_h_yamada Start -->
//		m_Context = oContext;
//Del 2011/11/01 Z01_h_yamada End <--
		oTitle = (EditText) this.findViewById(oContext, R.id.txtTitle_tels);
//		oTitle.requestFocus();
//		oTitle.setOnClickListener(null);
		phoneSearch = (Button) this.findViewById(oContext, R.id.btn_phone_list);
//Add 2011/11/01 Z01_h_yamada Start -->
		m_oSearchBtn = (Button) this.findViewById(oContext, R.id.Button_search);
		m_oSearchBtn.setEnabled(false);
//Add 2011/11/01 Z01_h_yamada End <--
		//yangyang add start Bug1044
		oTitle.setOnKeyListener(new OnKeyListener (){

			@Override
			public boolean onKey(View arg0, int KeyId, KeyEvent event) {

				if (KeyId == KeyEvent.KEYCODE_DPAD_RIGHT) {
					if (oTitle.getSelectionEnd() >= oTitle.getText().length()) {
						return true;
					}
				}
				return false;
			}});
		oTitle.setOnTouchListener(new OnTouchListener (){

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				android.os.Handler handerBoard = new android.os.Handler();
				handerBoard.postDelayed(new Runnable() {

					@Override
					public void run() {

//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//						AppInfo.hiddenSoftInputMethod(m_oContext);
//Del 2011/09/08 Z01_h_yamada End <--

					}

				}, 500);
				return true;
			}});
		//yangyang add end Bug1044
		oTitle.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable oEdit) {
				oTitle.setHint("");

				//yangyang add start Bug1044
				if (m_oModBtn != null) {
					if (oEdit.length() > 0) {
						m_oModBtn.setEnabled(true);
					} else {
						m_oModBtn.setEnabled(false);
					}
				}
				//yangyang add end Bug1044
				if (-1 != o_minSize && o_maxSize != -1) {

					if (oEdit.toString().length()>= o_minSize && oEdit.toString().length()<= o_maxSize) {
						m_oSearchBtn.setEnabled(true);
					} else {
						m_oSearchBtn.setEnabled(false);
					}
				} else if (o_maxSize == -1) {
					if (oEdit.toString().length()>= o_minSize ) {
						m_oSearchBtn.setEnabled(true);
					} else {
						m_oSearchBtn.setEnabled(false);
					}
				} else if (-1 == o_minSize ) {
					if (oEdit.toString().length()<= o_maxSize) {
						m_oSearchBtn.setEnabled(true);
					} else {
						m_oSearchBtn.setEnabled(false);
					}
				} else {
					m_oSearchBtn.setEnabled(true);
				}
				//カーソルの設定する
//				int selection = oTitle.getSelectionEnd();
//				int textLen = oTitle.getText().toString().length();
//
//				if (selection >= textLen && textLen == o_maxSize) {
//					selection = textLen;
//					oTitle.setSelection(selection);
//				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {


			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {


			}});
		btn_0 = (Button) this.findViewById(oContext,R.id.btn_0_tels);

		btn_0.setOnClickListener(OnClickLisTelNum);


		btn_1 = (Button) this.findViewById(oContext,R.id.btn_1_tels);
		btn_1.setOnClickListener(OnClickLisTelNum);
		btn_2 = (Button) this.findViewById(oContext,R.id.btn_2_tels);
		btn_2.setOnClickListener(OnClickLisTelNum);
		btn_3 = (Button) this.findViewById(oContext,R.id.btn_3_tels);
		btn_3.setOnClickListener(OnClickLisTelNum);

		btn_4 = (Button) this.findViewById(oContext,R.id.btn_4_tels);
		btn_4.setOnClickListener(OnClickLisTelNum);
		btn_5 = (Button) this.findViewById(oContext,R.id.btn_5_tels);
		btn_5.setOnClickListener(OnClickLisTelNum);
		btn_6 = (Button) this.findViewById(oContext,R.id.btn_6_tels);
		btn_6.setOnClickListener(OnClickLisTelNum);

		btn_7 = (Button) this.findViewById(oContext,R.id.btn_7_tels);
		btn_7.setOnClickListener(OnClickLisTelNum);
		btn_8 = (Button) this.findViewById(oContext,R.id.btn_8_tels);
		btn_8.setOnClickListener(OnClickLisTelNum);
		btn_9 = (Button) this.findViewById(oContext,R.id.btn_9_tels);
		btn_9.setOnClickListener(OnClickLisTelNum);

	}

	/**
	 * タイトルのOnKeyListenerを設定する
	 * @param oListener タイトルのOnKeyListener
	 * */
	public void setEditListener(OnKeyListener oListener) {
		oTitle.setOnKeyListener(oListener);
	}


	public void setPhoneListener(OnClickListener oListener) {
		phoneSearch.setOnClickListener(oListener);
	}

//Add 2011/11/01 Z01_h_yamada Start -->
	public void setSearchListener(OnClickListener oListener) {
		if (m_oSearchBtn != null) {
			m_oSearchBtn.setOnClickListener(oListener);
		}
	}
//Add 2011/11/01 Z01_h_yamada End <--

	public void setPhoneBtnText(String str) {
		phoneSearch.setText(str);
	}
	public void setPhoneBtnText(int value) {
		phoneSearch.setText(value);
	}
	/**
	 * アドレス帳ボタンが表示かどうかを設定する
	 * @param bool true:アドレス帳を表示する false:アドレス帳が不表示する
	 * */
	public void setPhoneShow(boolean bool) {
		if (bool) {
			phoneSearch.setVisibility(Button.VISIBLE);
		} else {
			phoneSearch.setVisibility(Button.GONE);
		}
	}

	/**
	 * アドレス帳ボタンのバイクグランドを設定する
	 * @param drawableId バイクグランドのId
	 * */
	public void setPhoneBtnBackground (int drawableId) {
		phoneSearch.setBackgroundResource(drawableId);
	}

	/**
	 * アドレス帳ボタンのレイアウトを設定する
	 * @param textId アドレス帳ボタンの表示内容
	 * */
	public void setPhoneText (int textId) {
		phoneSearch.setText(textId);
//Del 2011/09/13 Z01_h_yamada Start -->
//		phoneSearch.setTextSize(20);
//Del 2011/09/13 Z01_h_yamada End <--
		phoneSearch.setPadding(5, 5, 5, 5);
		phoneSearch.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
	}

}
