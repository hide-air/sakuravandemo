/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNE_EventInfo.java
 * Description    DGイベント情報構造体(DGの案内ノードをユニークに表現する)
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNE_EventInfo {
    private long ulnInAbsLinkID = 0;
    private long ulnOutAbsLinkID = 0;
    private long ulnDistMeterFromSt = 0;
    /**
    * Created on 2010/08/06
    * Title:       getUlnInAbsLinkID
    * Description:  進入リンクIDを取得する
    * @param1   無し
    * @return       long

    * @version        1.0
    */
    public long getUlnInAbsLinkID() {
        return ulnInAbsLinkID;
    }
    /**
    * Created on 2010/08/06
    * Title:       getUlnOutAbsLinkID
    * Description:  退出リンクID
    * @param1   無し
    * @return       long

    * @version        1.0
    */
    public long getUlnOutAbsLinkID() {
        return ulnOutAbsLinkID;
    }
    /**
    * Created on 2010/08/06
    * Title:       getUlnDistMeterFromSt
    * Description:  出発地からの距離(m)
    * @param1   無し
    * @return       long

    * @version        1.0
    */
    public long getUlnDistMeterFromSt() {
        return ulnDistMeterFromSt;
    }
}
