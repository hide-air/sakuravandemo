package net.zmap.android.pnd.v2.sakuracust;

import java.util.Date;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TrackDBAdapter {
	static final String DATABASE_NAME = "track_data.db";
	static final int DATABASE_VERSION = 1;
	
	public static final String TABLE_NAME = "TrackData";
	public static final String COL_ID = "_id";
	public static final String COL_COURSE_NAME = "CourseName";
	public static final String COL_FILENAME = "FileName";
	public static final String COL_ROWID = "ROWID";
	
//    db.execSQL("CREATE TABLE TrackData  (_id INTEGER PRIMARY KEY, CourseName TEXT, SaveDir TEXT, FileName TEXT );");
	
	protected final Context context;
	protected DatabaseHelper dbHelper;
	protected SQLiteDatabase db;
	
	public TrackDBAdapter(Context context){
		this.context = context;
		dbHelper = new DatabaseHelper(this.context);
	}
	
	//
	// SQLiteOpenHelper
	//
	private static class DatabaseHelper extends SQLiteOpenHelper {
		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		
		@Override public void onCreate(SQLiteDatabase db) {
			db.execSQL(
				"CREATE TABLE " + TABLE_NAME + " ("
				+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ COL_COURSE_NAME + " TEXT NOT NULL,"
				+ COL_FILENAME + " TEXT NOT NULL);");
		}
		
		@Override public void onUpgrade(
			SQLiteDatabase db,
			int oldVersion,
			int newVersion) {
				db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
					onCreate(db);
			}
		}
	//
	// Adapter Methods
	//
	
	public TrackDBAdapter open() {
		db = dbHelper.getWritableDatabase();
		return this;
	}
	
	public void close(){
		dbHelper.close();
	}
	
	//
	// App Methods
	//
	public boolean deleteAllNotes(){
		return db.delete(TABLE_NAME, null, null) > 0;
	}
	
	public boolean deleteNote(int id){
		return db.delete(TABLE_NAME, COL_ID + "=" + id, null) > 0;
	}
	
	public Cursor getAllNotes(){
		return db.query(TABLE_NAME, null, null, null, null, null, null);
	}
	
	public void saveCourse(String course, String filename){
		Date dateNow = new Date ();
		ContentValues values = new ContentValues();
		values.put(COL_COURSE_NAME, course);
//		values.put(COL_FILENAME, dateNow.toLocaleString());
		values.put(COL_FILENAME, filename);
		db.insertOrThrow(TABLE_NAME, null, values);
	}
}
