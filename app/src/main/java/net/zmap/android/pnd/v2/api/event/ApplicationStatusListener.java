package net.zmap.android.pnd.v2.api.event;

/** アプリケーションステータスリスナインタフェース
*
*/
public interface ApplicationStatusListener {
    /** アプリケーションが起動したときに呼ばれるリスナ関数
     *
     */
    public void onStarted();

    /** アプリケーションが終了するときに呼ばれるリスナ関数
     *
     */
    public void onStopped();
}
