//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.data.NaviRun;

public class GenreInquiryResult extends GenreInquiryBase{

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
      //yangyang mod waitDialog start
        startShowPage(NOTSHOWCANCELBTN);

//        NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetRecCount(recCount);
//        NaviLog.d(NaviLog.PRINT_LOG_TAG,"onCreate recCount==" + recCount.lcount);
//        if(recCount.lcount == 0){
//            showDialog(DIALOG_NODATA_ALARM);
//
//        }else{
//
//        	startShowPage(NOTSHOWCANCELBTN);
//
////            resetShowList();
//        }
      //yangyang mod waitDialog end
    }


//    private void resetShowList() {
//
//	}


	private void initGenreResultLayout(){

        oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base_list_freescroll, null);

        oTextTitle = (TextView) oLayout.findViewById(R.id.inquiry_title);
        oTextTitle.setText(meTextName);

        // //////////////////////
        Button btn = (Button) oLayout.findViewById(R.id.inquiry_btn);
        if(btn != null){
            btn.setVisibility(View.GONE);
        }

        scrollList = (ScrollList) oLayout.findViewById(R.id.scrollList);
        scrollList.setAdapter(new GenreResultAdapter());
        //yangyang mod start Bug793
//        scrollList.setDividerHeight(6);
        scrollList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
      //yangyang mod end Bug793
        scrollList.setPadding(0, 5, 0, 5);

//Del 2011/10/06 Z01_h_yamada Start -->
//        scrollList.setCountInPage(ONE_PAGE_COUNT);
//Del 2011/10/06 Z01_h_yamada End <--

        oTool = new ScrollTool(this);
        oTool.bindView(scrollList);
        removeOperArea();
        setViewInOperArea(oTool);

        getWorkArea().removeAllViews();
        setViewInWorkArea(oLayout);
    }
	//yangyang add waitDialog start
    @Override
	protected boolean initWaitBackData() {
//    	NaviLog.d(NaviLog.PRINT_LOG_TAG,"result initWaitBackData start;");
    	setTreeIndex();

    	initCount();
    	if(recCount.lcount == 0){
    		runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					showDialog(DIALOG_NODATA_ALARM);
				}
			});
            return false;
        }else{
    	g_RecIndex = 0;
		showList(g_RecIndex, (int) TEN_PAGE_COUNT , -1);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"result initWaitBackData end;");
        }
    	return true;
	}


    private int allRecordCount = 0;
	private void initCount() {
		NaviRun.GetNaviRunObj().JNI_NE_POIGnr_GetRecCount(recCount);
		allRecordCount = getItemCount();
	}


	@Override
	protected void resetDialog() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"result resetDialog start;");
		initGenreResultLayout();
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"result resetDialog end;");

	}
	//yangyang add waitDialog end

	private class GenreResultAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			return allRecordCount;
		}

		// yangyang add start
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		// yangyang add end
		@Override
		public Object getItem(int position) {
			return null;
		}

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int wPos, View oView, ViewGroup parent) {


//          int wIndex = (int) (wPos - NumRecIndex);
          if(oView == null)
          {
              LayoutInflater inflater = LayoutInflater.from(GenreInquiryResult.this);
              LinearLayout oListLayout = (LinearLayout)inflater.inflate(R.layout.inquiry_distance_button_freescroll, null);
              oView = oListLayout;
          }

          LinearLayout oLayout = (LinearLayout) oView.findViewById(R.id.LinearLayout_distance);

          TextView oText = (TextView) oLayout.findViewById(R.id.TextView_value);

          boolean hasData = false;
          if ((!bListDealing) /*&& (NumRecIndex <= wPos) && ( wPos < (NumRecIndex + RecCnt))*/) {
//        	  NaviLog.d(NaviLog.PRINT_LOG_TAG,"------------>> genreList=" + genreList.size() +", count=" + getCount() + ", wPos=" + wPos);
        	  if (wPos != 0 && wPos % TEN_PAGE_COUNT == 0 && wPos >= genreList.size()) {
  				showList(wPos, (int) TEN_PAGE_COUNT, -1);
              }
              if(wPos < genreList.size()){
                  String text = genreList.get(wPos).getM_Name();
                  if (null != text && (!"".equalsIgnoreCase(text))) {
                      oLayout.setEnabled(true);
                      oText.setText(text);
                      oLayout.setOnClickListener(new OnClickListener() {
                          public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ 最終施設選択禁止 Start -->
      				    	if(DrivingRegulation.CheckDrivingRegulation()){
      				    		return;
      				    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ 最終施設選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                              onItemClick(wPos);
                          }
                      });

                      hasData = true;
                  }
              }
          }
          if(!hasData){
              oText.setText("");
              oLayout.setEnabled(false);
          }
          return oView;

        }
    }

}
