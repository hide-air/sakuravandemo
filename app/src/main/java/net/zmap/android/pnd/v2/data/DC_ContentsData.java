package net.zmap.android.pnd.v2.data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class DC_ContentsData implements Serializable, Cloneable{
	private static final long serialVersionUID = -4204639190466618432L;
	public final static int DISPLAY_STATE_DEFAULT 	= 0;
	public final int 		DISPLAY_STATECHECK 	= 1;
	public final static int DISPLAY_STATE_LAYER 	= 2;

    public String 	m_sContentsName;	//ドライブコンテンツ名
	public int		m_iStateFlag;		//0:チェックなし、1:チェック、2:重畳。参照用
	public String	m_sIconPath;		//アイコンパス
	public int		m_iContentsID;		//取得したコンテンツにIDを付ける
	public boolean	m_bCheckItem = false;	//チェック状態を保存
	public boolean	m_bLayering = false;	//重疊フラグの変更状態を保存
	public boolean	m_bDisplayItem = false;	//リストへの変更状態を保存
    public Map<String , List<DC_ContentsItemData>> m_mapSectionItemList;	//セクションリスト

    public DC_ContentsData clone() throws CloneNotSupportedException {
        return (DC_ContentsData) super.clone();
    }
}