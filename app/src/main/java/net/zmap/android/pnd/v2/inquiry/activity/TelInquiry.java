//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Tel_ListItem;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.inquiry.view.KeyboardTelView;
import net.zmap.android.pnd.v2.inquiry.view.twoButtonView;
import net.zmap.android.pnd.v2.maps.OpenMap;

/**
 * K-P0_電話番号入力
 *
 * */
public class TelInquiry extends InquiryBaseLoading {
    /**
     * 電話番号の表示オブジェクト
     * */
    private EditText oTitleValue = null;
    /** 検索ボタン対象 */
    private Button oSearchBtn = null;

    private Button oModBtn = null;

//	private static int DIALOG_NODATA_ALARM = 0x01;
//	/** NaviRunに使用した対象 */
    private static TelInquiry instance;

    private KeyboardTelView okey = null;

    /** 現在の接続リクエストのid */
    protected String m_sReqId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        oIntent = this.getIntent();
        instance = this;
        initDialog();
    }

    public static TelInquiry getInstance() {
        return instance;
    }

    private void initDialog() {
        Intent oIntent = getIntent();
//Del 2011/09/17 Z01_h_yamada Start -->
//		//タイトルを表示する
//		int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE, 0);
//		if (0!= titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--

        //まず、JNIインタフェースのクリア処理を行う
        NaviRun.GetNaviRunObj().JNI_NE_POITel_Clear();
        twoButtonView oBtnView = new twoButtonView(this);

        setViewInOperArea(oBtnView.getView());
//yangyang mod start Bug1044
        //修正、検索ボタンを初期化する
//		initTwoBtn(okey);
        initTwoBtn(oBtnView);
//		yangyang mod end Bug1044
        //最大長さと最小長さを取得する
        int min = oIntent.getIntExtra(Constants.PARAMS_MIN_LIMITED, -1);
        int max = oIntent.getIntExtra(Constants.PARAMS_MAX_LIMITED, -1);

//Chg 2011/11/01 Z01_h_yamada Start -->
//        //yangyang mod start Bug1044
//        okey = new KeyboardTelView(this, oSearchBtn, oModBtn, min, max);
//
//        //yangyang mod end Bug1044
//--------------------------------------------
        okey = new KeyboardTelView(this, oModBtn, min, max);
        okey.setSearchListener(onSearchClickListener);
//Chg 2011/11/01 Z01_h_yamada End <--

        //電話帳ボタンのクリック処理を初期化する

        if (oIntent.hasExtra(Constants.PARAMS_SEARCH_TYPE)) {
            int searchType = oIntent.getIntExtra(Constants.PARAMS_SEARCH_TYPE, 0);
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"searchType====" + searchType);
            if (searchType == TYPE_DEFAULT) {
                okey.setPhoneShow(false);
            } else if (searchType == TYPE_MYTEL) {
                okey.setPhoneShow(true);
                okey.setPhoneBtnBackground(R.drawable.btn_default);
                okey.setPhoneText(R.string.btn_myself_telphone);
                okey.setPhoneListener(onPhoneTelListener);

            } else {
                okey.setPhoneShow(true);
                okey.setPhoneBtnBackground(R.drawable.btn_default);
                okey.setPhoneText(R.string.btn_address_search);
                okey.setPhoneListener(onPhoneListener);
            }
        }
        oTitleValue = KeyboardTelView.getoTitle();
//		oTitleValue.requestFocus();
//		oTitleValue.setEnabled(false);
//		oTitleValue.setFocusable(false);

        int inthint = oIntent.getIntExtra(Constants.PARAMS_HINT_VALUE, R.string.text_tel_input);
        oTitleValue.setHint(inthint);
        oTitleValue.setFilters(new InputFilter[] {new InputFilter.LengthFilter(max)});
        String tel = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
        if (!AppInfo.isEmpty(tel)) {
            oTitleValue.setText(tel);
        }

        setViewInWorkArea(okey.getView());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent oIntent) {

    	if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppInfo.PICK_CONTACT) {
                //Androidの電話帳を表示する
                Uri contactData = oIntent.getData();
                Cursor c = managedQuery(contactData, null, null, null, null);
                if (c != null && c.moveToFirst()) {
                    String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"hasPhone====" + hasPhone);
                    //1の場合、PhoneNumがある、それ以外場合、PhoneNumがない
                    if (hasPhone.compareTo("1") == 0) {
//						Uri oPosUri = Uri.withAppendedPath(contactData, ContactsContract.Contacts.Data.CONTENT_DIRECTORY);
////						Uri phoneUri = ContactsContract.Contacts.CONTENT_URI;
//						Cursor cTel = managedQuery(oPosUri, null, null, null, null);
//						String tel = cTel.getString(cTel
//								.getColumnIndexOrThrow(Phone.NUMBER));
                        //クリックされたレコードのIDを取得する
                        int contactId = c.getInt(c.getColumnIndex(ContactsContract.Contacts._ID));
                        Cursor phones = getContentResolver().query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                        + " = " + contactId, null, null);
                        //クリックされた対象に属性がある場合
                        if (phones != null && phones.getCount() > 0) {
                            //一番目の電話番号を取得する
                            phones.moveToFirst();
                            //クリックされた対象の電話番号を取得する
                            String tel = phones
                                    .getString(phones
                                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
// MOD.2013.08.02 N.Sasao ハイフン含みの番号が切れて表示される START
                            tel = tel.replaceAll("-", "");
// MOD.2013.08.02 N.Sasao ハイフン含みの番号が切れて表示される  END
                            oTitleValue.setText(tel);
                            oTitleValue.setSelection(oTitleValue.getText().toString().length());
                        } else {
                            oTitleValue.setText("");
                        }

//Add 2011/10/20 Z01_h_yamada Start -->
                        if (phones != null) {
                        	phones.close();
                        }
//Add 2011/10/20 Z01_h_yamada End <--

                    } else {
                        oTitleValue.setText("");
                    }
                }
//Add 2011/10/20 Z01_h_yamada Start -->
                if (c != null) {
// DEL.2013.08.02 N.Sasao アドレス帳2回以上アクセス時の異常終了 START
                	// StaleDataException対策
                	// managedQueryはActivityがカーソルへの参照を保持していて必要なくなった時
                	//（onDestroy()が呼ばれた時）にクローズされるため、クローズしなくてよい。
                	// (getContentResolverはcloseが必要)
//                	c.close();
// DEL.2013.08.02 N.Sasao アドレス帳2回以上アクセス時の異常終了  END
                }
//Add 2011/10/20 Z01_h_yamada End <--
            }
        }
        super.onActivityResult(requestCode, resultCode, oIntent);
    }

    @Override
    protected void onResume() {
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//		AppInfo.hiddenSoftInputMethod(this);
//Del 2011/09/08 Z01_h_yamada End <--
        super.onResume();
    }

//	@Override
//	protected void onStart() {
//		InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
////		View view = this.getCurrentFocus();
////	    if (view != null){
////
////	    }
//
//		super.onStart();
//	}

    @Override
    protected void onPause() {
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//		AppInfo.hiddenSoftInputMethod(this);
//Del 2011/09/08 Z01_h_yamada End <--
        super.onPause();
    }

    @Override
    protected void onStop() {
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//		AppInfo.hiddenSoftInputMethod(this);
//Del 2011/09/08 Z01_h_yamada End <--
        super.onStop();
    }

    /**
     * 検索ボタンと修正ボタンの初期化
     *
     * @param oBtnView
     *            検索ボタンと修正ボタンのレイアウト対象
     *
     * */
//	yangyang add start Bug1044 仕様変更
//	private void initTwoBtn(KeyboardTelView oBtnView) {
    private void initTwoBtn(twoButtonView oBtnView) {
//		yangyang add end Bug1044 仕様変更
        oModBtn = (Button)oBtnView.findViewById(this, R.id.Button_edit);
        oModBtn.setEnabled(false);
//		btnEdit.setOnClickListener(onEditClickListener);
//Chg 2011/11/01 Z01_h_yamada Start -->
//        oSearchBtn = (Button)oBtnView.findViewById(this, R.id.Button_search);
//        Intent oIntent = getIntent();
//        int flagKey = oIntent.getIntExtra(Constants.FROM_FLAG_KEY, -1);
//        if (flagKey != -1 && flagKey != AppInfo.ID_ACTIVITY_MAINMENU &&
//                flagKey != AppInfo.ID_ACTIVITY_ROUTE_EDIT) {
//            String buttonName = oIntent.getStringExtra(Constants.PARAMS_BUTTON_NAME);
//            oSearchBtn.setText(buttonName);
//        }
//        oSearchBtn.setEnabled(false);
////		oSearchBtn.setOnClickListener(onSearchClickListener);
////		yangyang add start Bug1044 仕様変更
//        oBtnView.setButtonListener(onEditClickListener, onSearchClickListener);
////		yangyang add end Bug1044 仕様変更
//--------------------------------------------
        oBtnView.setButtonListener(onEditClickListener);
//Chg 2011/11/01 Z01_h_yamada End <--


    }

    /**
     * 電話帳ボタンの初期化
     * */
    OnClickListener onPhoneListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 電話番号 電話帳 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 電話番号 電話帳 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//			AppInfo.hiddenSoftInputMethod(TelInquiry.this);
//Del 2011/09/08 Z01_h_yamada End <--
//			createDialog(Constants.DIALOG_WAIT, -1);
            startShowPage(NOTSHOWCANCELBTN);

        }

    };

    private OnClickListener onPhoneTelListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 電話番号 電話帳 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 電話番号 電話帳 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"onPhoneTelListener");
            TelephonyManager oTele = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            String sNumber = oTele.getLine1Number();
            if (AppInfo.isEmpty(sNumber)) {
                oTitleValue.setText("");
            } else {
                Editable oEdit = Editable.Factory.getInstance().newEditable(sNumber);
                PhoneNumberUtils.formatJapaneseNumber(oEdit);
                oTitleValue.setText(oEdit.toString());
            }

        }

    };
    /**
     * 修正ボタンの押下事件
     *
     * */
    OnClickListener onEditClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 電話番号 削除 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 電話番号 削除 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
            if (!AppInfo.isEmpty(oTitleValue.getText().toString())) {
                StringBuffer sb = new StringBuffer(oTitleValue.getText().toString());
                int selectionEnd = oTitleValue.getSelectionEnd();
                if (selectionEnd == 0) {
                    sb.deleteCharAt(selectionEnd);
                } else {
                    sb.deleteCharAt(selectionEnd - 1);
                }
                oTitleValue.setText(sb.toString());
                if (selectionEnd == 0) {
                    oTitleValue.setSelection(selectionEnd);
                } else {
                    oTitleValue.setSelection(selectionEnd - 1);
                }
                if (AppInfo.isEmpty(oTitleValue.getText().toString())) {
                    oTitleValue.setHint("");
                }

            }

        }

    };

    private String getTitleValue() {
        return oTitleValue.getText().toString().replaceAll("-", "");
    }

    /**
     * 検索ボタンの押下事件
     *
     * */
    OnClickListener onSearchClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 電話番号 検索 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
// ADD 2013.08.08 M.Honma 走行規制 電話番号 検索 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
            Intent oIntent = getIntent();
            int flagKey = oIntent.getIntExtra(Constants.FROM_FLAG_KEY, -1);
            if (flagKey != -1 && (flagKey == AppInfo.ID_ACTIVITY_MAINMENU ||
                    flagKey == AppInfo.ID_ACTIVITY_ROUTE_EDIT)) {
                //次、JNIインタフェースで入力した内容を検索する
                NaviRun.GetNaviRunObj().JNI_NE_POITel_SearchList(getTitleValue());
//				search();
            }

        }

    };

    protected void showWaitDialog(String sReqId) {
        m_sReqId = sReqId;
    }

    @Override
    protected Dialog onCreateDialog(int id, Bundle args) {
        return super.onCreateDialog(id, args);
    }

    //<--
    @Override
    protected void search() {
        JNILong ListCount = new JNILong();

        //次、JNIインタフェースで検索した件数を取得する
        NaviRun.GetNaviRunObj().JNI_NE_POITel_GetRecCount(ListCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"ListCount.lcount===" + ListCount.lcount);
        if (ListCount.lcount > 0) {
            //1件の場合、直接に地図画面へ遷移する
            if (ListCount.lcount == 1) {// One List -> OpenMap
//Add 2012/02/07 katsuta Start --> #2698
//				NaviLog.d(NaviLog.PRINT_LOG_TAG, "TelInquiry onClick ================== bIsListClicked: " + bIsListClicked);
	   			if (bIsListClicked) {
	   				return;
	   			}
	   			else {
	   				bIsListClicked = true;
	   			}
//Add 2012/02/07 katsuta End <--#2698
//				TelData oData = new TelData();

//				oData.GetTelSearchInfo();
//				NaviRun.GetNaviRunObj().setSearchKind(Constants.TEL_ONELIST_OPENMAP);
                POI_Tel_ListItem[] m_Poi_Tel_Listitem = new POI_Tel_ListItem[(int)ListCount.lcount];
                for (int i = 0; i < ListCount.lcount; i++) {
                    m_Poi_Tel_Listitem[i] = new POI_Tel_ListItem();
                }
                JNILong rec = new JNILong();
                NaviRun.GetNaviRunObj().JNI_NE_POITel_GetRecList(0, 1, m_Poi_Tel_Listitem, rec);

//				long lon = m_Poi_Arround_Listitem[0].getM_lLon();
//				long lat = m_Poi_Arround_Listitem[0].getM_lLat();
//				String address = m_Poi_Arround_Listitem[0].getM_Name();
//				openMap(null, lon, lat, address);
                //Bug620対応
                //名称がない場合、検索結果画面へ遷移する　Bug487の仕様
                POIData opoiData = new POIData();
                opoiData.m_wLong = m_Poi_Tel_Listitem[0].getM_lLon();
                opoiData.m_wLat = m_Poi_Tel_Listitem[0].getM_lLat();
                if (m_Poi_Tel_Listitem[0].getM_Name() == null || "".equals(m_Poi_Tel_Listitem[0].getM_Name())) {
                    opoiData.m_sName = m_Poi_Tel_Listitem[0].getM_Tel() + this.getResources().getString(R.string.str_around);

//					Intent oIntent = getIntent();
//					oIntent.putExtra(Constants.FROM_WHICH_SEARCH_KEY, "Tel");
//					oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, oTitleValue.getText().toString());
//
//					oIntent.setClass(this, TelResultInquiry.class);
//					startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_TELRESULTINQUIRY);

                } else {
                    //	            POIData opoiData = new POIData();
                    opoiData.m_sName = m_Poi_Tel_Listitem[0].getM_Name() +
                            "(" + m_Poi_Tel_Listitem[0].getM_FullTel() + ")";
//					opoiData.m_sAddress = m_Poi_Tel_Listitem[0].getM_Name();
//					OpenMap.moveMapTo(TelInquiry.this, opoiData, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
//					return;

                }
                opoiData.m_sAddress = m_Poi_Tel_Listitem[0].getM_Name();
                // XuYang add start #1056
                if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
                    CommonLib.setChangePosFromSearch(true);
                    CommonLib.setChangePos(false);
                } else {
                    CommonLib.setChangePosFromSearch(false);
                    CommonLib.setChangePos(false);
                }
                // XuYang add end #1056
                OpenMap.moveMapTo(TelInquiry.this, opoiData, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
//				AppInfo.hiddenSoftInputMethod(this);
//Del 2011/09/08 Z01_h_yamada End <--
                this.createDialog(Constants.DIALOG_WAIT, -1);
                return;
            } else { // More List
                //1件以上の場合、Telの結果画面へ遷移する
//				Intent oIntent = getIntent();
                oIntent.putExtra(Constants.FROM_WHICH_SEARCH_KEY, "Tel");
                oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, getTitleValue());

                oIntent.setClass(this, TelResultInquiry.class);

//Chg 2011/09/23 Z01yoneya Start -->
//				startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_TELRESULTINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
                NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_TELRESULTINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
            }
        } else {
            //データがない場合、提示ダイアログを表示する

            createDialog(DIALOG_NODATA_ALARM, R.string.tel_dialog_title);

        }
//		Intent oIntent = getIntent();
//		oIntent.putExtra(Constants.FROM_WHICH_SEARCH_KEY, "Tel");
//		oIntent.putExtra(Constants.FROM_FLAG_KEY, oTitleValue.getText().toString());
//		oIntent.setClass(this, TelResultInquiry.class);

//		startActivityForResult(oIntent, Constants.REQUESTCODE_OPEN_MAP);

    }

//	@Override
//	protected Dialog onCreateDialog(int wId) {
//		if (wId == DIALOG_NODATA_ALARM) {
//			CustomDialog oDialog = new CustomDialog(this);
//			oDialog.setTitle(R.string.tel_dialog_title);
//			oDialog.setMessage(this.getResources().getString(R.string.tel_dialog_msg));
//			oDialog.addButton(this.getResources().getString(R.string.btn_ok),new OnClickListener(){
//
//				@Override
//				public void onClick(View v) {
//					removeDialog(DIALOG_NODATA_ALARM);
//
//				}});
//			return oDialog;
//		}
//		return super.onCreateDialog(wId);
//	}

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//			NaviRun.GetNaviRunObj().JNI_NE_POITel_Cancel();

            NaviRun.GetNaviRunObj().JNI_NE_POITel_Clear();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected boolean onClickGoMap() {
        NaviRun.GetNaviRunObj().JNI_NE_POITel_Clear();

        return super.onClickGoMap();
    }

    @Override
    protected boolean onClickGoBack() {
        NaviRun.GetNaviRunObj().JNI_NE_POITel_Clear();
        return super.onClickGoBack();
    }

    @Override
    protected boolean onStartShowPage() throws Exception {
        return true;
    }

    @Override
    protected void onFinishShowPage(boolean bGetData) throws Exception {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);

//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(intent, AppInfo.PICK_CONTACT);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, intent, AppInfo.PICK_CONTACT);
//Chg 2011/09/23 Z01yoneya End <--
    }
}
