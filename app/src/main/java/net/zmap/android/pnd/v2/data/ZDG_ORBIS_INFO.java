/**
 ********************************************************************
 * Copyright (c) 2009. ZDC Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZDG_ORBIS_INFO.java
 * Description    int Class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZDG_ORBIS_INFO {
	private String ulnID;					//ID
	private long lnPosLatMilliSec;		//緯度
	private long lnPosLonMilliSec;		//経度
	private boolean bHighway = false;

	public void setUlnID(String ulnID) {
		this.ulnID = ulnID;
	}

	public String getUlnID() {
		return ulnID;
	}
	public void setLnPosLatMilliSec(long lnPosLatMilliSec) {
		this.lnPosLatMilliSec = lnPosLatMilliSec;
	}
	public long getLnPosLatMilliSec() {
		return lnPosLatMilliSec;
	}
	public void setLnPosLonMilliSec(long lnPosLonMilliSec) {
		this.lnPosLonMilliSec = lnPosLonMilliSec;
	}
	public long getLnPosLonMilliSec() {
		return lnPosLonMilliSec;
	}
	public void setOnHighway(boolean bOnHighway) {
		this.bHighway = bOnHighway;
	}

	public boolean getOnHighway() {
		return bHighway;
	}
}
