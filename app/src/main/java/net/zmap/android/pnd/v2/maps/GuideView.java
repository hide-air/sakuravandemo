package net.zmap.android.pnd.v2.maps;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Bitmap.Config;
import android.graphics.Paint.FontMetrics;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.ImageManager;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScreenAdapter;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIString;
import net.zmap.android.pnd.v2.data.JNITwoInt;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.Java_Guide;
import net.zmap.android.pnd.v2.data.Java_IconInfo;
import net.zmap.android.pnd.v2.data.Java_KINDDATA;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.ZDGHighwayGuideInfo;
import net.zmap.android.pnd.v2.data.ZLandmarkData_t;
import net.zmap.android.pnd.v2.data.ZNE_LaneGuideInfo;
import net.zmap.android.pnd.v2.data.ZNUI_DESTINATION_DATA;
import net.zmap.android.pnd.v2.data.ZNUI_IMAGE;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * @author wangdong
 *
 */
public class GuideView{

    private boolean visible = true;
    private JNITwoLong lnIdx = null;
    private JNILong lnListNum = null;
    //private JNILong lnSts = null;
    private JNITwoInt showGuide = new JNITwoInt();

    private ZNE_LaneGuideInfo laneGuideInfo = null;

    private ZNUI_IMAGE image = null;
    private Java_KINDDATA kindData = null;
    //private ZNUI_DESTINATION_DATA destinationData = null;

//    private JNILong[] lnTotalDistance;
//    private Java_Guide pstGuide = null;
    private JNITwoInt pstSimpleGuideInfo = null;
    private JNILong plnRestDistance = null;
    private JNITwoInt pstShowGuide = null;


    private RelativeLayout layoutGuide;

    /**BackGround*/
    private ImageView guideBackground;

    private RelativeLayout back;

    // 【到着情報表示】 残距離・到着予想時刻をシステムで表示
    // 簡易案内
    private LinearLayout layoutArrivalForecastInfo;
    private LinearLayout simple_guide_info;
    private TextView txtDistance;
    private TextView txtUseTime;
//Chg 2011/09/18 Z01_h_yamada Start -->
//    private Icon imgGuideDir;
//    private Button btnNextDist;
//--------------------------------------------
    private ImageView imgGuideDir;
    private TextView textNextDist;
//Chg 2011/09/18 Z01_h_yamada End <--


    // 【レーンガイド表示】 車線情報

    /**
     * レーン情報
     */
    private LinearLayout laneInfo;

    /** ルート案内中のレーン情報レイアウト */
    private RelativeLayout layoutLaneInfo = null;

    /** レーン情報の距離 */
    private TextView txtLaneInfo = null;

    /**
     * 交差点の名レイアウト
     *
     */
    private LinearLayout layoutIntersectionInfo;
    /**交差点の名*/
    private TextView txtCrossName;

    /**IC表示*/
    //private GuideList guideList;
    private ICListAdapter oMyAdapter = null;
    private ListView lvICListView = null;
    private String[] m_szName = null;
    private String[] m_szTime = null;
    private String[] m_szDistance = null;
    private String[] m_szToll = null;
    private int[]    m_iResId = null;
    private ZLandmarkData_t[][]  m_iSAPAResId;
//Del 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//    private RelativeLayout layoutFirstNode;
//	private RelativeLayout layoutSecondNode;
//Del 2011/08/11 Z01thedoanh (自由解像度対応) End <--
    /** ルート案内中の高速情報レイアウト */
    private LinearLayout layoutAppoachIntersection = null;

    /**方向画像、拡大図*/
    private ImageView imgDisplayBoard = null;

    // ・JCTアイコン画像
    // ・JCT名称
    // ・方面名称
    /**JCTアイコン画像*/
    private ImageView imgFlagAppoachIntersection;
    /**JCT名称*/
    private TextView txtJCTName;
    /**方面名称*/
    private TextView txtSiteName;

    //リピート
    private Button imgRepeat = null;

//    /**  人モードの場合、方位マークのレイアウト */
//    private RelativeLayout guideGoalLayout = null;

    /**  方位マークのレイアウト */
    private ImageView layoutCompass = null;

//    /**
//     * 人モードの場合、方位マークのビットマップ
//     */
//    private static Bitmap[] IMG_MAP_GUIDE_DIRECTION;

    /**
     * レーン情報のビットマップ
     */
    private static Bitmap IMG_MAP_LANE_ARROW_ON_COMBI_BASE0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_ON_COMBI_STRAIGHT0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_ON_COMBI_LEFT0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFT0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFTBK0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_ON_COMBI_RIGHT0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHT0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHTBK0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_ON_UTURN = null;

    private static Bitmap IMG_MAP_LANE_ARROW_OFF_COMBI_BASE0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_OFF_COMBI_STRAIGHT0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_OFF_COMBI_LEFT0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFT0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFTBK0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_OFF_COMBI_RIGHT0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHT0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHTBK0 = null;
    private static Bitmap IMG_MAP_LANE_ARROW_OFF_UTURN = null;

    private static Bitmap IMG_MAP_LANE_MARK_ADD_LANE_LEFT0 = null;  //左レーン増加数
    private static Bitmap IMG_MAP_LANE_MARK_ADD_LANE_RIGHT0 = null; //右レーン増加数

    //  Modify by liugang at 10/07/15 For Bug opentask_23(7) Start
    private static Bitmap IMG_MAP_LANE_MARK_REDUCE_LANE_LEFT0 = null;   //左レーン減少数
    private static Bitmap IMG_MAP_LANE_MARK_REDUCE_LANE_RIGHT0 = null; //右レーン減少数
    //  Modify by liugang at 10/07/15 For Bug opentask_23(7) End

    private static Bitmap IMG_MAP_LANE_MARK_SEPARATOR_LANE0 = null;

//Del 2011/09/18 Z01_h_yamada Start -->
//    private static Bitmap img_wg_crossing = null;
//    private static Bitmap img_wg_bridge;
//Del 2011/09/18 Z01_h_yamada End <--

//    private Bitmap IMG_MAP_LANE_DISTANCE_BACK = null;

    private ImageManager imageManager = new ImageManager();
//    private static Bitmap bitmapIcon = null;
    private static Bitmap bitmapNaviUIImg = null;

    private MapActivity activity;
    private Resources resources;
    private LayoutInflater inflater;
    private Bitmap compass;
    private Bitmap HeadupCompass;


    final static int NUI_ICON_TOILET  = 0x00000001;
    final static int NUI_ICON_GAS  = 0x00000002;
    final static int NUI_ICON_RESTAURANT = 0x00000004;
    final static int NUI_ICON_SOUVENIR = 0x00000008;
    final int AST_KIND_CODE_TBL[][] = {{0x8814,NUI_ICON_TOILET},
                              {0x4081,NUI_ICON_GAS},
                              {0x4083,NUI_ICON_GAS},
                              {0x4084,NUI_ICON_GAS},
                              {0x4085,NUI_ICON_GAS},
                              {0x4086,NUI_ICON_GAS},
                              {0x4087,NUI_ICON_GAS},
                              {0x408A,NUI_ICON_GAS},
                              {0x408B,NUI_ICON_GAS},
                              {0x408B,NUI_ICON_GAS},
                              {0x408C,NUI_ICON_GAS},
                              {0x408D,NUI_ICON_GAS},
                              {0x4092,NUI_ICON_GAS},
                              {0x880C,NUI_ICON_RESTAURANT},
                              {0x8812,NUI_ICON_SOUVENIR}};

    public GuideView(MapActivity context){
        this.activity = context;
        this.resources = context.getResources();
        this.inflater = LayoutInflater.from(context);

//        if (null == bitmapIcon) {
//            bitmapIcon = imageManager.getBitmap(resources, R.drawable.common_icon);
//        }

// Del 2011/09/28 katsuta Start -->
//        if (null == bitmapNaviUIImg) {
//            bitmapNaviUIImg = imageManager.getBitmap(resources, R.drawable.navi_ui_img);
//        }
// Del 2011/09/28 katsuta End <--

        compass = BitmapFactory.decodeResource(resources, R.drawable.compass_off);
        HeadupCompass = BitmapFactory.decodeResource(resources, R.drawable.compass_headup_off);

//Del 2011/09/18 Z01_h_yamada Start -->
//        if (null == img_wg_crossing) {
//            img_wg_crossing = imageManager.getBitmap(resources, R.drawable.wg_crossing);
//        }
//
//        if (null == img_wg_bridge) {
//            img_wg_bridge = imageManager.getBitmap(resources, R.drawable.wg_bridge);
//        }
//Del 2011/09/18 Z01_h_yamada End <--



        initLaneImage();
//        initGuideDirectionBitmap();

        if(!isInit){
            initGuideLayout();
            isInit = true;
        }
    }
    private boolean isInit = false;

//    /**
//     * 人モードの場合、方位マークのビットマップを初期化する。
//     */
//    private void initGuideDirectionBitmap() {
//        if(IMG_MAP_GUIDE_DIRECTION == null)
//        {
//            IMG_MAP_GUIDE_DIRECTION = new Bitmap[10];
//
//            IMG_MAP_GUIDE_DIRECTION[0] = imageManager.getAlphaImage(bitmapIcon, 0, 100, 60, 60);
//            IMG_MAP_GUIDE_DIRECTION[1] = imageManager.getAlphaImage(bitmapIcon, 60, 100, 60, 60);
//            IMG_MAP_GUIDE_DIRECTION[2] = imageManager.getAlphaImage(bitmapIcon, 120, 100, 60, 60);
//            IMG_MAP_GUIDE_DIRECTION[3] = imageManager.getAlphaImage(bitmapIcon, 180, 100, 60, 60);
//            IMG_MAP_GUIDE_DIRECTION[4] = imageManager.getAlphaImage(bitmapIcon, 240, 100, 60, 60);
//            IMG_MAP_GUIDE_DIRECTION[5] = imageManager.getAlphaImage(bitmapIcon, 300, 100, 60, 60);
//            IMG_MAP_GUIDE_DIRECTION[6] = imageManager.getAlphaImage(bitmapIcon, 360, 100, 60, 60);
//            IMG_MAP_GUIDE_DIRECTION[7] = imageManager.getAlphaImage(bitmapIcon, 420, 100, 60, 60);
//            IMG_MAP_GUIDE_DIRECTION[8] = imageManager.getAlphaImage(bitmapIcon, 480, 100, 60, 60);
//            IMG_MAP_GUIDE_DIRECTION[9] = imageManager.getAlphaImage(bitmapIcon, 480, 160, 60, 60);
//        }
//    }


    /**
     * 人モードの場合、方位マークのビットマップを設定する。
     */
    public void setGuideDirectionAngle() {
        // int map_view_flag = MapView.getInstance().getMap_view_flag();
        // if (map_view_flag == Constants.NAVI_MAP_VIEW
        // || map_view_flag == Constants.SIMULATION_MAP_VIEW
        // || map_view_flag == Constants.ROUTE_MAP_VIEW_BIKE)
        // if (CommonLib.isNaviMode())
        // {
        // 自動車・歩行者のモードを取得
        // int naviMode = CommonLib.getNaviMode();
        // if ((naviMode == Constants.NE_NAVIMODE_MAN)
        // ||(naviMode == Constants.NE_NAVIMODE_MAN)) {
        // // レイアウトから方位マークのイメージを削除する
        // guideGoalLayout.removeAllViews();
        // // 方位マークの情報(角度、アイコン番号など)を取得
        // ZNUI_UI_MARK_DATA markData = new ZNUI_UI_MARK_DATA();
        // NaviRun.GetNaviRunObj().JNI_Java_GetDestinationGuide(markData);
        // JNITwoLong stPixInfo = markData.getPixInfo();
        // ZNUI_ANGLE_DATA stAngle = markData.getAngle();
        //
        // int angleNo = stAngle.getAngleNo();
        // short angle = stAngle.getAngle();
        // RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
        // RelativeLayout.LayoutParams.WRAP_CONTENT,
        // RelativeLayout.LayoutParams.WRAP_CONTENT);
        // Matrix matrix = new Matrix();
        // // 方位マークのビットマップを回転
        // matrix.postRotate(360 - angle, 0, 0);
        //
        // ImageView imgPoint = new ImageView(this.activity);
        // imgPoint.setImageBitmap(Bitmap.createBitmap(
        // IMG_MAP_GUIDE_DIRECTION[angleNo], 0, 0,
        // IMG_MAP_GUIDE_DIRECTION[angleNo].getWidth(),
        // IMG_MAP_GUIDE_DIRECTION[angleNo].getHeight(), matrix,
        // true));
        // // 方位マークの位置を設定
        // int x = (int) stPixInfo.getM_lLong();
        // int y = (int) stPixInfo.getM_lLat();
        // if (x == 0) {
        // x = 300;
        // } else {
        // x = x - 50;
        // }
        // if (y == 0) {
        // y = 200;
        // } else {
        // y = y - 45;
        // }
        // if (angle == 0 || angle == 180 || angle == 90 || angle == 270
        // || angle == 360) {
        // params.topMargin = y + 15;
        // params.leftMargin = x + 15;
        // } else {
        // params.topMargin = y + 5;
        // params.leftMargin = x + 5;
        // }
        // // 方位マークのレイアウトにビットマップを追加する
        // guideGoalLayout.addView(imgPoint, params);
        // setViewVisible(guideGoalLayout);
        // }
        // }
    }


    /**
     *
     * Created on 2010/01/11
     * Title：setExpandPointAngle
     * Description:方位マークのビットマップを設定する。
     * @param iconInfo 方位マークの情報(角度、アイコン番号など)
     * @return void 無し
     * @version 1.0
     */
    private void setExpandPointAngle(Java_IconInfo iconInfo) {
        int angle = 0;
        if (null != iconInfo) {
            short sIconInfo = iconInfo.getSIconInfo();
            angle = 360 - sIconInfo;
            if (angle % 360 == 0) {
                angle = 0;
            }

            if(activity.isHeadupCompass()){
            	layoutCompass.setImageBitmap(CommonLib.rotate(HeadupCompass, angle));
            }else{
                layoutCompass.setImageBitmap(CommonLib.rotate(compass, angle));
            }
          //yangyang add start Bug851
            //View invalid
           setViewInvisible(layoutCompass);
         //yangyang add start Bug851
        }
    }

    /**
     * 案内中地図（簡易案内・ルートガイド・分岐）
     */
    private void setGuideLayoutInvisibility() {
        setViewInvisible(layoutArrivalForecastInfo);

        setViewInvisible(imgDisplayBoard);

        setViewInvisible(layoutAppoachIntersection);
        setViewInvisible(layoutIntersectionInfo);

        setViewInvisible(layoutCompass);

        //guideList.hide();
        lvICListView.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//        layoutFirstNode.setVisibility(View.GONE);
//        layoutSecondNode.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) End <--

        setViewInvisible(laneInfo);

        setViewVisible(txtUseTime);
        setViewVisible(txtDistance);

        hideGuideBackGround();
        //setViewInvisible(guideBackground);
        changecalendarToRight();
        this.changePoiTitleToLong();
    }


    /**
     * ルート案内中のレイアウトを初期化する
     */
    private void initGuideLayout() {
//Add 2011/07/29 Z01thedoanh (自由解像度対応) Start -->
    	RelativeLayout.LayoutParams rParams;
    	LinearLayout.LayoutParams lParams;
        int mLeft = 10;
        int mRight = 10;
        int mTop = 10;
        int mBottom = 10;
    	int areaGuide = (int) CommonLib.getGuideAreaWidth();
    	int showGuideInfoWidth = areaGuide - (mLeft + mRight);
//Add 2011/07/29 Z01thedoanh (自由解像度対応) End <--
    	layoutGuide = (RelativeLayout) inflater.inflate(R.layout.navi_guide, null);
        back = (RelativeLayout)layoutGuide.findViewById(R.id.back);
//Add 2011/07/29 Z01thedoanh (自由解像度対応) Start -->
        rParams = new RelativeLayout.LayoutParams(areaGuide, -1);
        rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        back.setLayoutParams(rParams);
//Add 2011/07/29 Z01thedoanh (自由解像度対応) End <--

        guideBackground = (ImageView) layoutGuide.findViewById(R.id.guide_background);
        //到着情報表示
        layoutArrivalForecastInfo = (LinearLayout) layoutGuide.findViewById(R.id.navi_guide_dest_info);
        simple_guide_info = (LinearLayout) layoutArrivalForecastInfo.findViewById(R.id.simple_guide_info);

        txtDistance = (TextView) layoutArrivalForecastInfo.findViewById(R.id.dist_info);
        txtUseTime = (TextView) layoutArrivalForecastInfo.findViewById(R.id.dest_time_info);
//Chg 2011/09/18 Z01_h_yamada Start -->
//        imgGuideDir = (icon) layoutArrivalForecastInfo.findViewById(R.id.next_direction);
//        btnNextDist = (Button) layoutArrivalForecastInfo.findViewById(R.id.next_guide_dist);
//--------------------------------------------
        imgGuideDir = (ImageView) layoutArrivalForecastInfo.findViewById(R.id.next_direction);
        textNextDist = (TextView) layoutArrivalForecastInfo.findViewById(R.id.next_guide_dist);
        textNextDist.setText("");
//Chg 2011/09/18 Z01_h_yamada End <--

//Add 2011/07/29 Z01thedoanh (自由解像度対応) Start -->
        int btnWidth = (areaGuide - mLeft - 3*mRight)/3;
        int btnHeight = (btnWidth*70)/110;
//        lParams = new LinearLayout.LayoutParams(btnWidth, btnHeight);
//        lParams.leftMargin = mLeft;
//        btnNextDist.setLayoutParams(lParams);
//        lParams.height = btnHeight+2;
//        imgGuideDir.setLayoutParams(lParams);
//        lParams.height = btnHeight;
//        layoutDistTimeInfo.setLayoutParams(lParams);
//Add 2011/07/29 Z01thedoanh (自由解像度対応) End <--

        //レーン 情報
        laneInfo = (LinearLayout) layoutGuide.findViewById(R.id.lane_info);
        layoutLaneInfo = (RelativeLayout) laneInfo.findViewById(R.id.layoutLaneInfo);
        txtLaneInfo = (TextView) laneInfo.findViewById(R.id.txtLaneInfo);

//Add 2011/07/29 Z01thedoanh (自由解像度対応) Start -->
//        rParams = new RelativeLayout.LayoutParams(showGuideInfoWidth, (int) (51/CommonLib.getGuideBackGroundScale()));
//        rParams.topMargin = mTop;
//        rParams.leftMargin = mLeft;
//        rParams.addRule(RelativeLayout.BELOW, layoutArrivalForecastInfo.getId());
//        laneInfo.setLayoutParams(rParams);
//Add 2011/07/29 Z01thedoanh (自由解像度対応) End <--

        //交差点名称
        layoutIntersectionInfo = (LinearLayout) layoutGuide.findViewById(R.id.layout_cross_name);
        txtCrossName = (TextView) layoutIntersectionInfo.findViewById(R.id.cross_name);

//Add 2011/07/29 Z01thedoanh (自由解像度対応) Start -->
//        rParams = new RelativeLayout.LayoutParams(showGuideInfoWidth, -2);
//        rParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//        rParams.topMargin = mTop;
//        rParams.leftMargin = mLeft;
//        rParams.addRule(RelativeLayout.BELOW, laneInfo.getId());
//        layoutIntersectionInfo.setLayoutParams(rParams);
//Add 2011/07/29 Z01thedoanh (自由解像度対応) End <--

        layoutAppoachIntersection = (LinearLayout) layoutGuide.findViewById(R.id.high_way_info);
        imgFlagAppoachIntersection = (ImageView) layoutAppoachIntersection.findViewById(R.id.jct_icon);
        txtJCTName = (TextView)layoutAppoachIntersection.findViewById(R.id.jct_name);
        txtSiteName = (TextView)layoutAppoachIntersection.findViewById(R.id.dir_name);

//Add 2011/07/29 Z01thedoanh (自由解像度対応) Start -->
//        rParams = new RelativeLayout.LayoutParams(showGuideInfoWidth, showGuideInfoWidth*121/351);
//        rParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//        rParams.topMargin = mTop;
//        rParams.leftMargin = mLeft;
//        rParams.addRule(RelativeLayout.BELOW, layoutArrivalForecastInfo.getId());
//        layoutAppoachIntersection.setLayoutParams(rParams);
//Add 2011/07/29 Z01thedoanh (自由解像度対応) End <--


        imgDisplayBoard = (ImageView)layoutGuide.findViewById(R.id.imgDisplayBoard);
        layoutCompass = (ImageView)layoutGuide.findViewById(R.id.CompassBgCross);


        //Guide List
      //Del 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//        layoutFirstNode = (RelativeLayout) layoutGuide.findViewById(R.id.guide_list_first);
//    	layoutSecondNode = (RelativeLayout) layoutGuide.findViewById(R.id.guide_list_second);
////Add 2011/07/29 Z01thedoanh (自由解像度対応) Start -->
//        LinearLayout txtInfo1 = (LinearLayout)layoutFirstNode.findViewById(R.id.txt_info);
//        LinearLayout txtInfo2 = (LinearLayout)layoutSecondNode.findViewById(R.id.txt_info);
//        rParams = new RelativeLayout.LayoutParams((int) (Constants.NAVI_GUIDE_LIST_ITEM_TEXT_INFO_WIDTH/CommonLib.getGuideBackGroundScale()), -2);
//        txtInfo1.setLayoutParams(rParams);
//        txtInfo2.setLayoutParams(rParams);
//
//        LinearLayout layoutMainSAPAInfo1 = (LinearLayout)layoutFirstNode.findViewById(R.id.layoutMainSAPAInfo);
//		LinearLayout layoutMainSAPAInfo2 = (LinearLayout)layoutSecondNode.findViewById(R.id.layoutMainSAPAInfo);
//        rParams = new RelativeLayout.LayoutParams((int) (Constants.NAVI_GUIDE_LIST_ITEM_TEXT_INFO_WIDTH/CommonLib.getGuideBackGroundScale()), -2);
//        rParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//        layoutMainSAPAInfo1.setLayoutParams(rParams);
//        layoutMainSAPAInfo2.setLayoutParams(rParams);
//
//        LinearLayout imgKindOfFlag1 = (LinearLayout)layoutFirstNode.findViewById(R.id.imgKindOfFlag);
//		LinearLayout imgKindOfFlag2 = (LinearLayout)layoutSecondNode.findViewById(R.id.imgKindOfFlag);
//        rParams = new RelativeLayout.LayoutParams((int) (Constants.NAVI_GUIDE_LIST_ITEM_TEXT_INFO_WIDTH/CommonLib.getGuideBackGroundScale()), -2);
//        rParams.leftMargin = (int) (280/CommonLib.getGuideBackGroundScale())+ mLeft;
//        imgKindOfFlag1.setLayoutParams(rParams);
//        imgKindOfFlag2.setLayoutParams(rParams);
//
//    	RelativeLayout hightWayGuideListItem1 = (RelativeLayout) layoutFirstNode.findViewById(R.id.hightWayGuideListItem);
//    	RelativeLayout hightWayGuideListItem2 = (RelativeLayout) layoutSecondNode.findViewById(R.id.hightWayGuideListItem);
//        rParams = new RelativeLayout.LayoutParams(showGuideInfoWidth, showGuideInfoWidth*110/350);
//        int paddingTop = (int) (Constants.NAVI_GUIDE_LIST_ITEM_TEXT_INFO_POS*CommonLib.getGuideBackGroundScale()*ScreenAdapter.getScale());
//    	hightWayGuideListItem1.setPadding(0, paddingTop, 0, 0);
//    	hightWayGuideListItem2.setPadding(0, paddingTop, 0, 0);
//    	rParams.leftMargin = mLeft;
//    	rParams.bottomMargin = showGuideInfoWidth*110/350;
//        hightWayGuideListItem1.setLayoutParams(rParams);
//        rParams.bottomMargin = 0;
//        hightWayGuideListItem2.setLayoutParams(rParams);
////Add 2011/07/29 Z01thedoanh (自由解像度対応) End <--
//Del 2011/08/11 Z01thedoanh (自由解像度対応) End <--

        lvICListView = (ListView)layoutGuide.findViewById(R.id.navi_ic_list);
        lvICListView.setDivider(new ColorDrawable(0x00000000));
        lvICListView.setCacheColorHint(0x00000000);
//Add 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
        LinearLayout lvICListViewLL = (LinearLayout)back.findViewById(R.id.navi_ic_list_ll);
        int naviICListLayoutHeight = (int) ((areaGuide/Constants.GUIDE_AREA_ASPECT1) - btnHeight*1.5 - mTop - mBottom);
        rParams = new RelativeLayout.LayoutParams(showGuideInfoWidth, naviICListLayoutHeight );
        rParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lvICListViewLL.setLayoutParams(rParams);
        lvICListViewLL.setPadding(mLeft, 0, 0, mBottom);

        lParams = new LinearLayout.LayoutParams(-1, naviICListLayoutHeight - mBottom);
        lParams.leftMargin = mLeft;
        lParams.bottomMargin = mBottom;
        lvICListView.setLayoutParams(lParams);
//Add 2011/08/11 Z01thedoanh (自由解像度対応) End <--


//Add 2011/07/29 Z01thedoanh (自由解像度対応) End <--
//        layoutGuide.addView(lvICListView, rParams);

        //ScrollView layoutICList = (ScrollView)layoutGuide.findViewById(R.id.ic_list);
        //LinearLayout layoutGuidance = (LinearLayout) layoutGuide.findViewById(R.id.guide_list);
        //guideList = new GuideList(layoutICList, layoutGuidance);

        //


//Chg 2011/09/18 Z01_h_yamada Start -->
//        imgRepeat = (Button)layoutGuide.findViewById(R.id.img_repeat);
//--------------------------------------------
        imgRepeat = (Button)layoutArrivalForecastInfo.findViewById(R.id.img_repeat);
//Chg 2011/09/18 Z01_h_yamada End <--

        if(imgRepeat != null){
            imgRepeat.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(imgRepeat.getVisibility() == View.VISIBLE){
                        activity.doRepeat();
                    }
                }
            });
        }

//        guideGoalLayout = (RelativeLayout)layoutGuide.findViewById(R.id.guideGoalLayout);
        setGuideLayoutInvisibility();
    }


    /**
     * レーン情報のビットマップを初期化する
     */
    private void initLaneImage() {
// Chg 2011/09/17 katsuta Start -->
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_BASE0 == null){
//            IMG_MAP_LANE_ARROW_ON_COMBI_BASE0 = imageManager.getAlphaImage(bitmapNaviUIImg, 672, 832, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_STRAIGHT0 == null){
//            IMG_MAP_LANE_ARROW_ON_COMBI_STRAIGHT0 = imageManager.getAlphaImage(bitmapNaviUIImg, 704, 832, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_LEFT0 == null){
//            IMG_MAP_LANE_ARROW_ON_COMBI_LEFT0 = imageManager.getAlphaImage(bitmapNaviUIImg, 736, 832, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFT0 == null){
//            IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFT0 = imageManager.getAlphaImage(bitmapNaviUIImg, 768, 832, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFTBK0 == null){
//            IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFTBK0 = imageManager.getAlphaImage(bitmapNaviUIImg, 800, 832, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_RIGHT0 == null){
//            IMG_MAP_LANE_ARROW_ON_COMBI_RIGHT0 = imageManager.getAlphaImage(bitmapNaviUIImg, 832, 832, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHT0 == null){
//            IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHT0 = imageManager.getAlphaImage(bitmapNaviUIImg, 864, 832, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHTBK0 == null){
//            IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHTBK0 = imageManager.getAlphaImage(bitmapNaviUIImg, 896, 832, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_ON_UTURN == null){
//            IMG_MAP_LANE_ARROW_ON_UTURN = imageManager.getAlphaImage(bitmapNaviUIImg, 928, 832, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_BASE0 == null){
//            IMG_MAP_LANE_ARROW_OFF_COMBI_BASE0 = imageManager.getAlphaImage(bitmapNaviUIImg, 672, 876, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_STRAIGHT0 == null){
//            IMG_MAP_LANE_ARROW_OFF_COMBI_STRAIGHT0 = imageManager.getAlphaImage(bitmapNaviUIImg, 704, 876, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_LEFT0 == null){
//            IMG_MAP_LANE_ARROW_OFF_COMBI_LEFT0 = imageManager.getAlphaImage(bitmapNaviUIImg, 736, 876, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFT0 == null){
//            IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFT0 = imageManager.getAlphaImage(bitmapNaviUIImg, 768, 876, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFTBK0 == null){
//            IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFTBK0 = imageManager.getAlphaImage(bitmapNaviUIImg, 800, 876, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_RIGHT0 == null){
//            IMG_MAP_LANE_ARROW_OFF_COMBI_RIGHT0 = imageManager.getAlphaImage(bitmapNaviUIImg, 832, 876, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHT0 == null){
//            IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHT0 = imageManager.getAlphaImage(bitmapNaviUIImg, 864, 876, 32, 40);
//        }
//
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHTBK0 == null){
//            IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHTBK0 = imageManager.getAlphaImage(bitmapNaviUIImg, 896, 876, 32, 40);
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_UTURN == null){
//            IMG_MAP_LANE_ARROW_OFF_UTURN = imageManager.getAlphaImage(bitmapNaviUIImg, 928, 876, 32, 40);
//        }
//        if (IMG_MAP_LANE_MARK_SEPARATOR_LANE0 == null){
//            IMG_MAP_LANE_MARK_SEPARATOR_LANE0 = imageManager.getAlphaImage(bitmapNaviUIImg, 992, 832, 2, 40);
//        }
//        if (IMG_MAP_LANE_DISTANCE_BACK == null){
//            IMG_MAP_LANE_DISTANCE_BACK = imageManager.getAlphaImage(bitmapNaviUIImg, 672, 916, 68, 40);
//        }
//
//        if (IMG_MAP_LANE_MARK_REDUCE_LANE_LEFT0 == null) {
//            Bitmap bitmapLeft = imageManager.getBitmap(resources, R.drawable.left_reduce_lane_flag);
//            IMG_MAP_LANE_MARK_REDUCE_LANE_LEFT0 = imageManager.getAlphaImage(bitmapLeft, 0, 0, 32, 40);
//            bitmapLeft = null;
//        }
//        if (IMG_MAP_LANE_MARK_REDUCE_LANE_RIGHT0 == null) {
//            Bitmap bitmapRight = imageManager.getBitmap(resources, R.drawable.right_reduce_lane_flag);
//            IMG_MAP_LANE_MARK_REDUCE_LANE_RIGHT0 = imageManager.getAlphaImage(bitmapRight, 0, 0, 32, 40);
//            bitmapRight = null;
//        }
//
//        if (IMG_MAP_LANE_MARK_ADD_LANE_LEFT0 == null) {
//            Bitmap bitmapLeft = imageManager.getBitmap(resources, R.drawable.left_add_lane_flag);
//            IMG_MAP_LANE_MARK_ADD_LANE_LEFT0 = imageManager.getAlphaImage(bitmapLeft, 0, 0, 32, 40);
//            bitmapLeft = null;
//        }
//        if (IMG_MAP_LANE_MARK_ADD_LANE_RIGHT0 == null) {
//            Bitmap bitmapRight = imageManager.getBitmap(resources, R.drawable.right_add_lane_flag);
//            IMG_MAP_LANE_MARK_ADD_LANE_RIGHT0 = imageManager.getAlphaImage(bitmapRight, 0, 0, 32, 40);
//            bitmapRight = null;
//        }

//    	if (IMG_MAP_LANE_ARROW_ON_COMBI_BASE0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_00);
//            IMG_MAP_LANE_ARROW_ON_COMBI_BASE0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//      	}
//Chg 2011/10/12 Z01_h_yamada Start -->
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_STRAIGHT0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_02);
//            IMG_MAP_LANE_ARROW_ON_COMBI_STRAIGHT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_LEFT0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_03);
//            IMG_MAP_LANE_ARROW_ON_COMBI_LEFT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFT0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_04);
//            IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFTBK0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_05);
//            IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFTBK0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_RIGHT0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_06);
//            IMG_MAP_LANE_ARROW_ON_COMBI_RIGHT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHT0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_07);
//            IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHTBK0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_08);
//            IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHTBK0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_ON_UTURN == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_09);
//            IMG_MAP_LANE_ARROW_ON_UTURN = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_BASE0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_10);
//            IMG_MAP_LANE_ARROW_OFF_COMBI_BASE0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//
//
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_STRAIGHT0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_11);
//            IMG_MAP_LANE_ARROW_OFF_COMBI_STRAIGHT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_LEFT0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_12);
//            IMG_MAP_LANE_ARROW_OFF_COMBI_LEFT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFT0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_13);
//            IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFTBK0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_14);
//            IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFTBK0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_RIGHT0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_15);
//            IMG_MAP_LANE_ARROW_OFF_COMBI_RIGHT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHT0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_16);
//            IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//
//        if (IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHTBK0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_17);
//            IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHTBK0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_ARROW_OFF_UTURN == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_18);
//            IMG_MAP_LANE_ARROW_OFF_UTURN = imageManager.getAlphaImage(bitmapLane, 0, 0, 47, 78);
//            bitmapLane = null;
//        }
//        if (IMG_MAP_LANE_MARK_SEPARATOR_LANE0 == null){
//            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_19);
//            IMG_MAP_LANE_MARK_SEPARATOR_LANE0 = imageManager.getAlphaImage(bitmapLane, 0, 0, 2, 61);
//            bitmapLane = null;
//        }
////        if (IMG_MAP_LANE_DISTANCE_BACK == null){
////            IMG_MAP_LANE_DISTANCE_BACK = imageManager.getAlphaImage(bitmapNaviUIImg, 672, 916, 68, 40);
////        }
//
//        if (IMG_MAP_LANE_MARK_REDUCE_LANE_LEFT0 == null) {
//            Bitmap bitmapLeft = imageManager.getBitmap(resources, R.drawable.left_reduce_lane_flag);
//            IMG_MAP_LANE_MARK_REDUCE_LANE_LEFT0 = imageManager.getAlphaImage(bitmapLeft, 0, 0, 53, 78);
//            bitmapLeft = null;
//        }
//        if (IMG_MAP_LANE_MARK_REDUCE_LANE_RIGHT0 == null) {
//            Bitmap bitmapRight = imageManager.getBitmap(resources, R.drawable.right_reduce_lane_flag);
//            IMG_MAP_LANE_MARK_REDUCE_LANE_RIGHT0 = imageManager.getAlphaImage(bitmapRight, 0, 0, 53, 78);
//            bitmapRight = null;
//        }
//
//        if (IMG_MAP_LANE_MARK_ADD_LANE_LEFT0 == null) {
//            Bitmap bitmapLeft = imageManager.getBitmap(resources, R.drawable.left_add_lane_flag);
//            IMG_MAP_LANE_MARK_ADD_LANE_LEFT0 = imageManager.getAlphaImage(bitmapLeft, 0, 0, 53, 78);
//            bitmapLeft = null;
//        }
//        if (IMG_MAP_LANE_MARK_ADD_LANE_RIGHT0 == null) {
//            Bitmap bitmapRight = imageManager.getBitmap(resources, R.drawable.right_add_lane_flag);
//            IMG_MAP_LANE_MARK_ADD_LANE_RIGHT0 = imageManager.getAlphaImage(bitmapRight, 0, 0, 53, 78);
//            bitmapRight = null;
//        }
// Chg 2011/09/17 katsuta End <--
//--------------------------------------------
        if (IMG_MAP_LANE_ARROW_ON_COMBI_STRAIGHT0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_02);
            IMG_MAP_LANE_ARROW_ON_COMBI_STRAIGHT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_ON_COMBI_LEFT0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_03);
            IMG_MAP_LANE_ARROW_ON_COMBI_LEFT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFT0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_04);
            IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFTBK0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_05);
            IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFTBK0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_ON_COMBI_RIGHT0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_06);
            IMG_MAP_LANE_ARROW_ON_COMBI_RIGHT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHT0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_07);
            IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHTBK0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_08);
            IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHTBK0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_ON_UTURN == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_09);
            IMG_MAP_LANE_ARROW_ON_UTURN = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }

        if (IMG_MAP_LANE_ARROW_OFF_COMBI_BASE0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_10);
            IMG_MAP_LANE_ARROW_OFF_COMBI_BASE0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }


        if (IMG_MAP_LANE_ARROW_OFF_COMBI_STRAIGHT0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_11);
            IMG_MAP_LANE_ARROW_OFF_COMBI_STRAIGHT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_OFF_COMBI_LEFT0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_12);
            IMG_MAP_LANE_ARROW_OFF_COMBI_LEFT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFT0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_13);
            IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFTBK0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_14);
            IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFTBK0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_OFF_COMBI_RIGHT0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_15);
            IMG_MAP_LANE_ARROW_OFF_COMBI_RIGHT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHT0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_16);
            IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHT0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }

        if (IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHTBK0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_17);
            IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHTBK0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_ARROW_OFF_UTURN == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_18);
            IMG_MAP_LANE_ARROW_OFF_UTURN = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }
        if (IMG_MAP_LANE_MARK_SEPARATOR_LANE0 == null){
            Bitmap bitmapLane = imageManager.getBitmap(resources, R.drawable.navi_ui_img_19);
            IMG_MAP_LANE_MARK_SEPARATOR_LANE0 = imageManager.getAlphaImage(bitmapLane, 0, 0, bitmapLane.getWidth(), bitmapLane.getHeight());
            bitmapLane = null;
        }

        if (IMG_MAP_LANE_MARK_REDUCE_LANE_LEFT0 == null) {
            Bitmap bitmapLeft = imageManager.getBitmap(resources, R.drawable.left_reduce_lane_flag);
            IMG_MAP_LANE_MARK_REDUCE_LANE_LEFT0 = imageManager.getAlphaImage(bitmapLeft, 0, 0, bitmapLeft.getWidth(), bitmapLeft.getHeight());
            bitmapLeft = null;
        }
        if (IMG_MAP_LANE_MARK_REDUCE_LANE_RIGHT0 == null) {
            Bitmap bitmapRight = imageManager.getBitmap(resources, R.drawable.right_reduce_lane_flag);
            IMG_MAP_LANE_MARK_REDUCE_LANE_RIGHT0 = imageManager.getAlphaImage(bitmapRight, 0, 0, bitmapRight.getWidth(), bitmapRight.getHeight());
            bitmapRight = null;
        }

        if (IMG_MAP_LANE_MARK_ADD_LANE_LEFT0 == null) {
            Bitmap bitmapLeft = imageManager.getBitmap(resources, R.drawable.left_add_lane_flag);
            IMG_MAP_LANE_MARK_ADD_LANE_LEFT0 = imageManager.getAlphaImage(bitmapLeft, 0, 0, bitmapLeft.getWidth(), bitmapLeft.getHeight());
            bitmapLeft = null;
        }
        if (IMG_MAP_LANE_MARK_ADD_LANE_RIGHT0 == null) {
            Bitmap bitmapRight = imageManager.getBitmap(resources, R.drawable.right_add_lane_flag);
            IMG_MAP_LANE_MARK_ADD_LANE_RIGHT0 = imageManager.getAlphaImage(bitmapRight, 0, 0, bitmapRight.getWidth(), bitmapRight.getHeight());
            bitmapRight = null;
        }
//Chg 2011/10/12 Z01_h_yamada End <--



// Del 2011/09/28 katsuta Start -->
        //add liutch 0506 -->
//        CommonLib.recycleBitmap(bitmapNaviUIImg);
        //<--
// Del 2011/09/28 katsuta End <--
    }



    /**
     * 到着情報を表示する。
     */
    public void showArrivalForecastInfo(int iArrivalTime, ZNUI_DESTINATION_DATA destData) {
        int map_view_flag = MapView.getInstance().getMap_view_flag();
        int iHour = iArrivalTime / 60;
        int iMinute = iArrivalTime % 60;
        if (Constants.SIMULATION_MAP_VIEW == map_view_flag
                || Constants.NAVI_MAP_VIEW == map_view_flag) {

            if(isVisible()){
                setViewVisible(layoutGuide);
            }
            setViewVisible(layoutArrivalForecastInfo);

//            if(null == destinationData)
//            {
//                destinationData = new ZNUI_DESTINATION_DATA();
//            }
//
//            int naviMode = CommonLib.getNaviMode();
//            if(naviMode == Constants.NE_NAVIMODE_CAR){
//                NaviRun.GetNaviRunObj().JNI_Java_GetDestination(destinationData);
//            }else if(naviMode == Constants.NE_NAVIMODE_MAN){
//                NaviRun.GetNaviRunObj().JNI_WG_GetDestination(destinationData);
//            }

            if (destData != null) {
                //ZNUI_TIME time = destinationData.getStTime();
                //if (time != null) {
//                    txtDistance.setText(CommonLib.getFormatDistance(destinationData.getLnDistance()));
//Chg 2011/08/15 Z01_h_yamada Start -->
//              CommonLib.setDistenceText(txtDistance, CommonLib.getFormatDistance(destData.getLnDistance()), 16);
//--------------------------------------------
                CommonLib.setDistenceText(txtDistance, CommonLib.getFormatDistance(destData.getLnDistance()), (int)(txtDistance.getTextSize()*Constants.DISTANCE_UNIT_FONT_RATE+0.5f));
//Chg 2011/08/15 Z01_h_yamada End <--
                txtUseTime.setText(CommonLib.getFormatTime(iHour, iMinute));
                //}
            }
            Java_IconInfo iconInfo = new Java_IconInfo();
            NaviRun.GetNaviRunObj().JNI_Java_GetCompass(iconInfo);
            MapView.getInstance().setPointAngle(iconInfo, false, null);
        }else{
            hideArrivalForecastInfo();
            setViewInvisible(layoutGuide);
        }
    }



    /**
     * 到着情報を隠す。
     */
    public void hideArrivalForecastInfo() {

        setViewInvisible(layoutArrivalForecastInfo);
        //yangyang add start 20110512
        setViewInvisible(layoutGuide);
      //yangyang add end 20110512
    }

    /**
     * 交差点の名称を表示する
     */
    public void showIntersectionInfo() {
        if(CommonLib.isBIsShowGuide())
        {
            if(!MapView.getInstance().isMapScrolled())
            {
                //guideList.hide();
                lvICListView.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//                layoutFirstNode.setVisibility(View.GONE);
//                layoutSecondNode.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) End <--
                hideGuideBackGround();
            }
        }
        CommonLib.setBIsShowMagMap(true);
        JNIString strName = new JNIString();
        NaviRun.GetNaviRunObj().JNI_Java_GetName(strName);
        if(!MapView.getInstance().isMapScrolled())
        {
//            String name = strName.getM_StrAddressString();
//            if(name == null || name.length() == 0 ){
//            	name=activity.getResources().getString(R.string.intersection_name);
//            }

          //yangyang mod start Bug851
//          setViewVisible(layoutIntersectionInfo);
            setViewInvisible(layoutIntersectionInfo);
//          txtCrossName.setText(name);
//          setViewVisible(layoutCompass);
//          NaviRun.GetNaviRunObj().JNI_Java_GetMagMapCompass(iconInfo);
//          setExpandPointAngle(iconInfo);
          //yangyang mod end Bug851
          CommonLib.setBIsShowMagMapName(true);
          activity.onNaviViewShow(true);
          changecalendarToLeft();

           /* else
            {
                setViewInvisible(layoutIntersectionInfo);
                CommonLib.setBIsShowMagMapName(false);
            }*/
        }
        changecalendarToLeft();

        //add liutch bug1096 -->
        setGuideBackClickable(true);
        //<--
   }
    public void setGuideBackClickable(boolean Clickable){
        //add liutch bug1096 -->
        back.setClickable(Clickable);
        //<--
    }
//    /**
//     * 残距離バー描画
//     * @param pCnt
//     * @param isShow
//     */
//    public void calcRectDistanceBar(final int pCnt, final boolean isShow) {
//        if (isShow) {
////            int top = 0;
////            long limgHight = bitscrollGray.getHeight();//R.drawable.scrollyello
//            long restDestance = 0;
//            double dwCale = 0;
//
//            //Java_Guide java_guide = new Java_Guide();
//            //JNILong lnDistance = new JNILong();
//
//            if(null == pstGuide)
//            {
//                pstGuide = new Java_Guide();
//            }
//            NaviRun.GetNaviRunObj().JNI_Java_GetGuide(pstGuide);
//
//            if(null == lnSts)
//            {
//                lnSts = new JNILong();
//            }
//            NaviRun.GetNaviRunObj().JNI_Java_GetMagMapRestDistance(lnSts);
//
//            restDestance = lnSts.getLcount();
//            lnTotalDistance = pstGuide.getLnDistance();
//            if (isShow && pCnt == 1) {
//                TotalDistance = lnTotalDistance[3].getLcount();
//            }
//            //at 10/06/04 For RectDistanceBar Start
////            if (pCnt == 1) {
////                imgScrollyello.setImageResource(R.drawable.scrollyello);
////            } else {
////                imgScrollyello.setImageResource(R.drawable.scrollgray);
////            }
//
//            if (TotalDistance <= 0) {
//                TotalDistance = 1;
//            }
//            if (restDestance <= 0) {
//                restDestance = TotalDistance;
//            }
//            if (restDestance > TotalDistance) {
//                TotalDistance = restDestance;
//            }
//            double drestDestance = restDestance;
//            double dTotalDistance = TotalDistance;
//            dwCale = drestDestance / dTotalDistance;
//            java.text.DecimalFormat df = new java.text.DecimalFormat(
//                    "#.000");
//            df.format(dwCale);
////            rlScroll02.removeAllViews();
////            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
////                    RelativeLayout.LayoutParams.WRAP_CONTENT,
////                    RelativeLayout.LayoutParams.WRAP_CONTENT);
////            params.topMargin = 3;
////            params.leftMargin = 3;
////
////            if (dwCale != 0) {
////                top = (int) (limgHight * (dwCale));
////            } else {
////                top = (int) (limgHight * (1 - 0.99));
////            }
////            if (top == 0){
////                top = 1;
////            }
////            imgscrollgray.setImageBitmap(imageManager.getImage(
////                            bitscrollGray, 0, 0, bitscrollGray
////                                    .getWidth(), top));
////            rlScroll02.addView(imgscrollgray, params);
//        }
//    }
//
//
//    /**
//     * 残距離バー描画
//     * @param bshowJctBar 残距離
//     */
//    public void calcJctRectDistanceBar(boolean bshowJctBar) {
//        if(bshowJctBar)
//        {
////            int top = 0;
////            long limgHight = bitscrollGray.getHeight();
//            long restDestance = 0;
//            double dwCale = 0;
//
//            if(null == pstGuide)
//            {
//                pstGuide = new Java_Guide();
//            }
//            NaviRun.GetNaviRunObj().JNI_Java_GetGuide(pstGuide);
//
//            if(null == lnSts)
//            {
//                lnSts = new JNILong();
//            }
//            NaviRun.GetNaviRunObj().JNI_Java_GetJCTImagRestDistance(lnSts);
//
//            restDestance = lnSts.getLcount();
//            lnTotalDistance = pstGuide.getLnDistance();
//
//            TotalDistance = lnTotalDistance[1].getLcount();
//
//            if (TotalDistance <= 0) {
//                TotalDistance = 1;
//            }
//            if (restDestance <= 0) {
//                restDestance = TotalDistance;
//            }
//            if (restDestance > TotalDistance) {
//                TotalDistance = restDestance;
//            }
//            double drestDestance = restDestance;
//            double dTotalDistance = TotalDistance;
//            dwCale = drestDestance / dTotalDistance;
//            java.text.DecimalFormat df = new java.text.DecimalFormat(
//                    "#.000");
//            df.format(dwCale);
////            rlScroll02.removeAllViews();
////            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
////                    RelativeLayout.LayoutParams.WRAP_CONTENT,
////                    RelativeLayout.LayoutParams.WRAP_CONTENT);
////            params.topMargin = 3;
////            params.leftMargin = 3;
////
////            if (dwCale != 0) {
////                top = (int) (limgHight * (dwCale));
////            } else {
////                top = (int) (limgHight * (1 - 0.99));
////            }
////
////            if (top == 0){
////                top = 1;
////            }
////            imgscrollgray.setImageBitmap(imageManager.getImage(
////                            bitscrollGray, 0, 0, bitscrollGray
////                                    .getWidth(), top));
////            rlScroll02.addView(imgscrollgray, params);
////
////            if(!MapView.getInstance().isMapScrolled())
////            {
////                ScrollLayout.setVisibility(View.VISIBLE);
////            }
//            //For 112047 End
//        }
//    }
//
//
//    /**
//     * 残距離バーは表示しない
//     */
//    public void hidCalcRectDistanceBar() {/*
//        rlScroll02.removeAllViews();
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
//                RelativeLayout.LayoutParams.WRAP_CONTENT,
//                RelativeLayout.LayoutParams.WRAP_CONTENT);
//        params.topMargin = 5;
//        params.leftMargin = 5;
//        imgscrollgray.setImageBitmap(imageManager.getImage(
//                bitscrollGray, 0, 0, bitscrollGray.getWidth(),
//                bitscrollGray.getHeight()));
//        rlScroll02.addView(imgscrollgray, params);
//
//    */
//
//        if(!CommonLib.isBIsGuideEnd())
//        {
////            ScrollLayout.setVisibility(View.INVISIBLE);
//            setViewInvisible(layoutCompass);
//        }
//    }


    /**
     * 案内情報を隠れる
     */
    public void hideIntersectionInfo() {
      //add liutch bug1096 -->
    	setGuideBackClickable(false);
        //<--
        if(!CommonLib.isBIsGuideEnd())
        {
            setViewInvisible(layoutIntersectionInfo);
            setViewInvisible(layoutCompass);
            setViewInvisible(imgDisplayBoard);
            hideGuideBackGround();
            CommonLib.setBIsShowMagMap(false);
        }

        if(CommonLib.isBIsShowJct() || CommonLib.isBIsDirectionBoard())
        {
            if(!MapView.getInstance().isMapScrolled())
            {
                setViewVisible(imgDisplayBoard);
                showGuideBackGround();
                this.changecalendarToLeft();
            }
        }else{
            changecalendarToRight();
        }
    }

    /**
     * 地図の左を移動
     */
    public void changecalendarToLeft(){
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
//                70, 70);
//        params.leftMargin = 370;
//        params.topMargin = 400;
////        calendarBtn.setLayoutParams(params);
//        RelativeLayout.LayoutParams contentParams = new RelativeLayout.LayoutParams(
//                25, 25);
//        contentParams.leftMargin = 240;
//        contentParams.topMargin = 7;
//        imgPoiConetnt.setLayoutParams(contentParams);
    }


    /**
     * 地図の右を移動
     */
    public void changecalendarToRight(){
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
//                70, 70);
//        params.leftMargin = 740;
//        params.topMargin = 400;
////        calendarBtn.setLayoutParams(params);
//        RelativeLayout.LayoutParams contentParams = new RelativeLayout.LayoutParams(
//                25, 25);
//        contentParams.leftMargin = 400;
//        contentParams.topMargin = 7;
//        imgPoiConetnt.setLayoutParams(contentParams);
    }


    /**
     * POI.タイトルを変換する（長）
     */
    private void changePoiTitleToLong(){/*
        imgPoiTitle.setImageResource(R.drawable.poi02);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                400, 20);
        params.leftMargin = 30;
        params.topMargin = 10;
        txt_Poi.setLayoutParams(params);
    */}


    /**
     * POI.タイトルを変換する（小）
     */
    private void changePoiTitleToSmall(){/*
        imgPoiTitle.setImageResource(R.drawable.poi);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                220, 20);
        params.leftMargin = 30;
        params.topMargin = 10;
        txt_Poi.setLayoutParams(params);
    */}
    //For poititle end


    /**
     * シミュレーションの地図を表示する
     *
     * @param guideDir
     *            簡易案内の方向フラグ
     */
    private void setGuideDirection(int guideDir) {

        int dir = -1;

        int naviMode = CommonLib.getNaviMode();
        if(naviMode == Constants.NE_NAVIMODE_CAR){
            switch (guideDir) {
            case Constants.NUI_JCTARROW_STRAIGHT:
                dir = R.drawable.dir_178;
                break;
            case Constants.NUI_JCTARROW_INCRIGHT:
                dir = R.drawable.dir_181;
                break;
            case Constants.NUI_JCTARROW_INCLEFT:
                dir = R.drawable.dir_182;
                break;
            case Constants.NUI_JCTARROW_RIGHT:
                dir = R.drawable.dir_180;
                break;
            case Constants.NUI_JCTARROW_LEFT:
                dir = R.drawable.dir_183;
                break;
            case Constants.NUI_JCTARROW_INCRIGHTBK:
                dir = R.drawable.dir_185;
                break;
            case Constants.NUI_JCTARROW_INCLEFTBK:
                dir = R.drawable.dir_184;
                break;
            case Constants.NUI_JCTARROW_UTURN:
                dir = R.drawable.dir_179;
                break;
            case Constants.NUI_JCTARROW_NON:
                break;
            default:
                break;
            }
        }
        else if(naviMode == Constants.NE_NAVIMODE_MAN){

            switch (guideDir) {
            case Constants.NUI_WG_DIR_FORWARD: //直進矢印 (道なり)
                dir = R.drawable.dir_178;
                break;
            case Constants.NUI_WG_DIR_TILT_RIGHT: //前斜め右矢印
                dir = R.drawable.dir_181;
                break;
            case Constants.NUI_WG_DIR_TILT_LEFT: //前斜め左矢印
                dir = R.drawable.dir_182;
                break;
            case Constants.NUI_WG_DIR_TURN_RIGHT: //右矢印
                dir = R.drawable.dir_180;
                break;
            case Constants.NUI_WG_DIR_TURN_LEFT: //左矢印
                dir = R.drawable.dir_183;
                break;
            case Constants.NUI_WG_DIR_BACK_RIGHT://後ろ斜め右矢印
                dir = R.drawable.dir_185;
                break;
            case Constants.NUI_WG_DIR_BACK_LEFT: //後ろ斜め左矢印
                dir = R.drawable.dir_184;
                break;
            case Constants.NUI_WG_DIR_U_TURN: // U ターン矢印
                dir = R.drawable.dir_179;
                break;
            case Constants.NUI_WG_DIR_INVALID:  //無効 (簡易案内消去)
                break;
            default:
                break;
            }
        }

        if(dir != -1){
//Chg 2011/09/18 Z01_h_yamada Start -->
//            imgGuideDir.setBitmap(dir);
//--------------------------------------------
            imgGuideDir.setImageResource(dir);
//Chg 2011/09/18 Z01_h_yamada End <--
            setViewVisible(imgGuideDir);
        }else{
            setViewInvisible(imgGuideDir);
        }
    }

//    /**
//     * Created on 2010/01/25
//     * Title：setSimpleGuideKind
//     * Description:簡易案内の高速道路（IC・JCT・SA）種類を設定する
//     * @param1 simpleGuidePointKind 高速道路（IC・JCT・SA）の種類フラグ
//     * @param1 txtView 簡易案内レイアウトの種類TextView
//     * @return void 無し
//     * @version 1.0
//     */
//    private void setSimpleGuideKind(int simpleGuidePointKind) {
//        switch (simpleGuidePointKind) {
//        case Constants.NUI_KIND_TYPE_CROSS:
//            imgGuideDir.setText(Constants.EDG_SIMPLE_GUIDE_INTERSECTION);
//            break;
//        case Constants.NUI_KIND_TYPE_JCT:
//            imgGuideDir.setText(Constants.EDG_SIMPLE_GUIDE_JCT);
//            break;
//        case Constants.NUI_KIND_TYPE_IC:
//            imgGuideDir.setText(Constants.EDG_SIMPLE_GUIDE_IC);
//            break;
//        case Constants.NUI_KIND_TYPE_NON:
//            imgGuideDir.setText(Constants.EDG_SIMPLE_GUIDE_INVALID);
//            break;
//        default:
//            break;
//        }
//    }

    private void setWGSimpleGuideKind(int simpleGuidePointKind) {
//Chg 2011/09/18 Z01_h_yamada Start -->
//        switch (simpleGuidePointKind) {
//        case Constants.NUI_WG_LINK_PEDESTRIAN_CROSSING:
//            imgGuideDir.setIcon(img_wg_crossing);
//            break;
//        case Constants.NUI_WG_LINK_PEDESTRIAN_BRIDGE:
//            imgGuideDir.setIcon(img_wg_bridge);
//            break;
//        default:
//        	imgGuideDir.setIcon(null);
//            break;
//        }
//--------------------------------------------
        switch (simpleGuidePointKind) {
        case Constants.NUI_WG_LINK_PEDESTRIAN_CROSSING:
            imgGuideDir.setImageResource(R.drawable.wg_crossing);
            break;
        case Constants.NUI_WG_LINK_PEDESTRIAN_BRIDGE:
            imgGuideDir.setImageResource(R.drawable.wg_bridge);
            break;
        default:
           break;
        }
//Chg 2011/09/18 Z01_h_yamada End <--
    }

    /**
     * Created on 2010/01/25
     * Title：setKindMark
     * Description:分岐情報レイアウトに高速道路種類のイメージを設定する
     * @param1 type 高速道路（IC・JCT・SA）の種類フラグ
     * @param1 imgView 分岐情報レイアウトの高速道路種類のImageView
     * @return void 無し
     * @version 1.0
     */
    private void setKindMark(int type, ImageView imgView) {
        switch (type) {
        case Constants.NUI_KIND_TYPE_NON:
            break;
        case Constants.NUI_KIND_TYPE_IC:
            setViewVisible(imgView);
            imgView.setImageResource(R.drawable.ic_flag);
            break; // IC
        case Constants.NUI_KIND_TYPE_JCT:
            setViewVisible(imgView);
            imgView.setImageResource(R.drawable.jct_flag);
            break; // JCT
        case Constants.NUI_KIND_TYPE_SA:
            setViewVisible(imgView);
            imgView.setImageResource(R.drawable.sa_flag);
            break; // /< SA
        case Constants.NUI_KIND_TYPE_PA:
            setViewVisible(imgView);
            imgView.setImageResource(R.drawable.pa_flag);
            break; // PA
        case Constants.NUI_KIND_TYPE_TOLL:
            setViewVisible(imgView);
            imgView.setImageResource(R.drawable.toll_flag);
            break;
        case Constants.NUI_KIND_TYPE_CROSS:
        case Constants.NUI_KIND_TYPE_MAX:
        default:
            setViewInvisible(imgView);
            break;
        }
    }


//    /**
//     * 案内リストレイアウトの非表示状態を設定する
//     * @param layout　案内リストのレイアウト
//     */
//    private void setGuidanceListItemInvisible(View layout) {
//        if(!CommonLib.isBIsGuideEnd() && (layout != null))
//        {
//            setViewInvisible(layout);
//        }
//    }
//
//
//    /**
//     * 案内リストレイアウトの表示状態を設定する
//     * @param layout 案内リストのレイアウト
//     */
//    private void setGuidanceListItemVisible(View layout) {setViewVisible(layout);}


//    private StringBuilder buffer = new StringBuilder();

    /**
     * 案内リストを表示する
     */
    public void showGuidanceList() {
        if (null == lnIdx) {
            lnIdx = new JNITwoLong();
//            String config = CommonLib.getNaviResFilePath()[6];
//            String logPath = config.split(" ")[2];
//            LoggerManager.openLogFileForWrite(logPath);
        }

        if (null == lnListNum) {
            lnListNum = new JNILong();
        }

        /*if (null == lnSts) {
            lnSts = new JNILong();
        }*/

//        buffer = new StringBuilder();

        //NaviRun.GetNaviRunObj().JNI_Java_GetGuideSts(lnSts);
        //if (lnSts != null && lnSts.getLcount() == 0) {
        if(CommonLib.isBIsOnHighWay()){
//            buffer.append("---start---\n");
            NaviRun.GetNaviRunObj().JNI_DG_GetNextHighwayGuideInfoIdx(lnIdx);
            NaviRun.GetNaviRunObj().JNI_DG_GetHighwayGuideListNum(lnIdx.getM_lLong(), lnListNum);

            if (null == showGuide) {
            	showGuide = new JNITwoInt();
            }
            NaviRun.GetNaviRunObj().JNI_Java_GetShowGuide(showGuide);
            if (showGuide.getM_iSizeX() == Constants.NUI_GUIDE_HIGHWAYKUSI
                    && showGuide.getM_iSizeY() == Constants.NUI_STATE_SHOW) {
                CommonLib.setBIsShowGuide(true);
            }
            /*else{
            	CommonLib.setBIsShowGuide(false);
            }*/
//            buffer.append("---list count="+ lnListNum.getLcount() +"\n");

            //guideList.initView(activity, (int) lnListNum.getLcount());
            //guideList.scrollToIndex((int)(lnIdx.getM_lLat()));

            if (lnListNum.getLcount() > 0 && !MapView.getInstance().isMapScrolled()) {
                if (CommonLib.isBIsShowGuide()) {
                    if (CommonLib.isBIsShowJct() || CommonLib.isBIsDirectionBoard()) {
                        //guideList.hide();
                        lvICListView.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//                        layoutFirstNode.setVisibility(View.GONE);
//                        layoutSecondNode.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) End <--
                    } else {

                    	updateHighwayGuide(lnIdx.getM_lLong(), lnIdx.getM_lLat(), lnListNum.getLcount());
                        showGuideBackGround();
                        //guideList.show();
//Chg 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//                        if((lnListNum.getLcount() - lnIdx.getM_lLat()) < 3){
//----------------------------------------------------------------------------
                        if((lnListNum.getLcount() - lnIdx.getM_lLat()) < 1){
//Chg 2011/08/11 Z01thedoanh (自由解像度対応) End <--
                            lvICListView.setVisibility(View.GONE);
                        }else
                        {
                        	lvICListView.setVisibility(View.VISIBLE);
                        }
                        setViewInvisible(layoutIntersectionInfo);

                        setViewInvisible(layoutCompass);
                        setViewInvisible(layoutAppoachIntersection);
                        setViewInvisible(laneInfo);
                    }
                }
            }

//            buffer.append("---end---\n");
//            LoggerManager.writeLog(buffer.toString());
        }
    }

   public void setGuideListToCarPos(){
	   if(lvICListView != null && lnListNum != null && lnIdx != null){
		   if((lnListNum.getLcount()- lnIdx.getM_lLat()) > 0){
			   if(oMyAdapter != null){
 			       oMyAdapter.notifyDataSetChanged();
 	                lvICListView.setSelection(oMyAdapter.getCount() - 1);
 			   }
			   //lvICListView.setSelection((int)((lnListNum.getLcount()- lnIdx.getM_lLat() - 1)));
			   lvICListView.setStackFromBottom(true);
		   }
	   }
   }

   public void initGuideList(){
 	   int listIndex = 0;
       ZDGHighwayGuideInfo stDGHGInfo = null;

	   if (null == lnIdx) {
           lnIdx = new JNITwoLong();
       }
	   if (null == lnListNum) {
           lnListNum = new JNILong();
       }
       /*if (null == lnSts) {
           lnSts = new JNILong();
       }*/
       //NaviRun.GetNaviRunObj().JNI_Java_GetGuideSts(lnSts);
       //if (lnSts != null && lnSts.getLcount() == 0){
    	   NaviRun.GetNaviRunObj().JNI_DG_GetNextHighwayGuideInfoIdx(lnIdx);
           NaviRun.GetNaviRunObj().JNI_DG_GetHighwayGuideListNum(lnIdx.getM_lLong(), lnListNum);
           m_szName = new String[(int)(lnListNum.getLcount() - lnIdx.getM_lLat())];
           m_iResId = new int[(int)(lnListNum.getLcount() - lnIdx.getM_lLat())];
           m_szDistance = new String[(int)(lnListNum.getLcount() - lnIdx.getM_lLat())];
           m_szTime = new String[(int)(lnListNum.getLcount() - lnIdx.getM_lLat())];
           m_szToll = new String[(int)(lnListNum.getLcount() - lnIdx.getM_lLat())];
           m_iSAPAResId = new ZLandmarkData_t[(int)(lnListNum.getLcount() - lnIdx.getM_lLat())][Constants.SAPA_SHOW_ICON_COUNT];
           for(listIndex = (int)(lnListNum.getLcount() - 1); listIndex > lnIdx.getM_lLat() - 1; listIndex--){
        	   stDGHGInfo = new ZDGHighwayGuideInfo();
               NaviRun.GetNaviRunObj().JNI_DG_HighwayGuideInfo_new(lnIdx.getM_lLong(), listIndex, stDGHGInfo);
               if (stDGHGInfo.getUlnDistMeter() == 0
                       && stDGHGInfo.getUlnRestTimeMinute() == 0) {
                   break;
               }
               setGuidanceListItem(stDGHGInfo, (int)((lnListNum.getLcount() - 1)-listIndex));
               NaviRun.GetNaviRunObj().JNI_DG_HighwayGuideInfo_free(lnIdx.getM_lLong(), listIndex, stDGHGInfo);
           }
           oMyAdapter = new ICListAdapter(this.activity, m_iResId, m_szName, m_szTime, m_szDistance, m_szToll,m_iSAPAResId);
           lvICListView.setAdapter(oMyAdapter);
           lvICListView.setStackFromBottom(true);
           //lvICListView.setSelection((int)(m_iPosition));
       //}
   }
    /**
     * 案内リストのデータを更新する
     * @param1 lnGroupIdx 自己位置直近のグループ番号
     * @param2 lnListIdx 自己位置直近のインデックス番号
     * @param3 lnListNum インデックスの最大数
     * @return
     */
    public int updateHighwayGuide(long lnGroupIdx, long lnListIdx, long lnListNum)
    {
        int listIndex = 0;
        ZDGHighwayGuideInfo stDGHGInfo = null;


        m_szName = new String[(int)(lnListNum - lnListIdx)];
        m_iResId = new int[(int)(lnListNum - lnListIdx)];
        m_szDistance = new String[(int)(lnListNum - lnListIdx)];
        m_szTime = new String[(int)(lnListNum - lnListIdx)];
        m_szToll = new String[(int)(lnListNum - lnListIdx)];
        m_iSAPAResId = new ZLandmarkData_t[(int)(lnListNum - lnListIdx)][Constants.SAPA_SHOW_ICON_COUNT];

        //for (listIndex = (int) lnListIdx; listIndex < lnListNum; listIndex++) {
        for(listIndex = (int)(lnListNum - 1); listIndex > lnListIdx - 1; listIndex--){
            stDGHGInfo = new ZDGHighwayGuideInfo();
            NaviRun.GetNaviRunObj().JNI_DG_HighwayGuideInfo_new(lnGroupIdx, listIndex, stDGHGInfo);
            if (stDGHGInfo.getUlnDistMeter() == 0
                    && stDGHGInfo.getUlnRestTimeMinute() == 0) {
                break;
            }
            setGuidanceListItem(stDGHGInfo, (int)((lnListNum - 1)-listIndex));
//            buffer.append("---lnGroupIdx="  + lnGroupIdx +", lnListIdx=" + listIndex +", name=");

            //ICListItem item = guideList.getViewAtPos(((int)lnListNum - 1 - listIndex));
            //setGuidanceListItem(stDGHGInfo, item, 3);
            NaviRun.GetNaviRunObj().JNI_DG_HighwayGuideInfo_free(lnGroupIdx, listIndex, stDGHGInfo);
        }
//Chg 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//      if((lnListNum-lnListIdx) < 3 && (lnListNum-lnListIdx) > 0){
//    	setICListItem((int)(lnListNum-lnListIdx), m_iResId, m_szName, m_szTime, m_szDistance, m_szToll,m_iSAPAResId);
//    }else
//------------------------------------------------
        if((lnListNum-lnListIdx) > 0)
//Chg 2011/08/11 Z01thedoanh (自由解像度対応) End <--
        {
        	oMyAdapter.init(this.activity, m_iResId, m_szName, m_szTime, m_szDistance, m_szToll,m_iSAPAResId);
            oMyAdapter.notifyDataSetChanged();
        }
        //lvICListView.setSelection((int)(m_iPosition));
        return listIndex;
    }
//Del 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//    private boolean setICListItem(int iListCount,int[] iResId, String[] szNodeName, String[] szRestTime, String[] szDistance, String[] szToll, ZLandmarkData_t[][] stSAPAResId) {
//
//
//    	ImageView imgFlag1 = (ImageView)layoutFirstNode.findViewById(R.id.imgFlag);
//    	TextView txtTime1 = (TextView)layoutFirstNode.findViewById(R.id.txtTime);
//    	TextView txtKm1 = (TextView)layoutFirstNode.findViewById(R.id.txtKm);
//    	TextView txtName1 = (TextView)layoutFirstNode.findViewById(R.id.txtName);
//    	TextView txtMoney1 = (TextView)layoutFirstNode.findViewById(R.id.txtMoney);
//    	//LinearLayout layoutSAPAInfo1 = (LinearLayout)layoutFirstNode.findViewById(R.id.layoutSAPAInfo);
//	    ImageView imgSAPA1_1 = (ImageView)layoutFirstNode.findViewById(R.id.sa_1);
//	    ImageView imgSAPA2_1 = (ImageView)layoutFirstNode.findViewById(R.id.sa_2);
//	    ImageView imgSAPA3_1 = (ImageView)layoutFirstNode.findViewById(R.id.sa_3);
//	    ImageView imgSAPA4_1 = (ImageView)layoutFirstNode.findViewById(R.id.sa_4);
//	    ImageView imgSAPA5_1 = (ImageView)layoutFirstNode.findViewById(R.id.sa_5);
//	    ImageView imgSAPA6_1 = (ImageView)layoutFirstNode.findViewById(R.id.sa_6);
//	    ImageView imgSAPA7_1 = (ImageView)layoutFirstNode.findViewById(R.id.sa_7);
//	    ImageView imgSAPA8_1 = (ImageView)layoutFirstNode.findViewById(R.id.sa_8);
//	    ImageView imgFlag2 = (ImageView)layoutSecondNode.findViewById(R.id.imgFlag);
//    	TextView txtTime2 = (TextView)layoutSecondNode.findViewById(R.id.txtTime);
//    	TextView txtKm2 = (TextView)layoutSecondNode.findViewById(R.id.txtKm);
//    	TextView txtName2 = (TextView)layoutSecondNode.findViewById(R.id.txtName);
//    	TextView txtMoney2 = (TextView)layoutSecondNode.findViewById(R.id.txtMoney);
//	    ImageView imgSAPA1_2 = (ImageView)layoutSecondNode.findViewById(R.id.sa_1);
//	    ImageView imgSAPA2_2 = (ImageView)layoutSecondNode.findViewById(R.id.sa_2);
//	    ImageView imgSAPA3_2 = (ImageView)layoutSecondNode.findViewById(R.id.sa_3);
//	    ImageView imgSAPA4_2 = (ImageView)layoutSecondNode.findViewById(R.id.sa_4);
//	    ImageView imgSAPA5_2 = (ImageView)layoutSecondNode.findViewById(R.id.sa_5);
//	    ImageView imgSAPA6_2 = (ImageView)layoutSecondNode.findViewById(R.id.sa_6);
//	    ImageView imgSAPA7_2 = (ImageView)layoutSecondNode.findViewById(R.id.sa_7);
//	    ImageView imgSAPA8_2 = (ImageView)layoutSecondNode.findViewById(R.id.sa_8);
//
//	    if(iListCount == 2){
//	    	imgFlag2.setImageResource(m_iResId[1]);
//		    txtName2.setText(m_szName[1]);
//		    if(null != m_szDistance[1]){
//				CommonLib.setDistenceText(txtKm2, m_szDistance[1], 20);
//			}
//		    txtMoney2.setText(m_szToll[1]);
//		    txtTime2.setText(m_szTime[1]);
//		    setSAPAImage(m_iSAPAResId[1],imgSAPA1_2,imgSAPA2_2,imgSAPA3_2,imgSAPA4_2,imgSAPA5_2,imgSAPA6_2,imgSAPA7_2,imgSAPA8_2);
//		    imgFlag1.setImageResource(m_iResId[0]);
//		    txtName1.setText(m_szName[0]);
//		    if(null != m_szDistance[0]){
//				CommonLib.setDistenceText(txtKm1, m_szDistance[0], 20);
//			}
//		    txtMoney1.setText(m_szToll[0]);
//		    txtTime1.setText(m_szTime[0]);
//		    setSAPAImage(m_iSAPAResId[0],imgSAPA1_1,imgSAPA2_1,imgSAPA3_1,imgSAPA4_1,imgSAPA5_1,imgSAPA6_1,imgSAPA7_1,imgSAPA8_1);
//		    layoutSecondNode.setVisibility(View.VISIBLE);
//		    layoutFirstNode.setVisibility(View.VISIBLE);
//	    }else if(iListCount == 1){
//	    	layoutFirstNode.setVisibility(View.GONE);
//	    	imgFlag2.setImageResource(m_iResId[0]);
//		    txtName2.setText(m_szName[0]);
//		    if(null != m_szDistance[0]){
//				CommonLib.setDistenceText(txtKm2, m_szDistance[0], 20);
//			}
//		    txtMoney2.setText(m_szToll[0]);
//		    txtTime2.setText(m_szTime[0]);
//		    setSAPAImage(m_iSAPAResId[0],imgSAPA1_2,imgSAPA2_2,imgSAPA3_2,imgSAPA4_2,imgSAPA5_2,imgSAPA6_2,imgSAPA7_2,imgSAPA8_2);
//		    layoutSecondNode.setVisibility(View.VISIBLE);
//	    }
//        return true;
//    }
//
//    private void setSAPAImage(ZLandmarkData_t[] SAPAImg, ImageView img1, ImageView img2,ImageView img3,ImageView img4,ImageView img5,ImageView img6,ImageView img7,ImageView img8){
//    	for(int i = 0; i < SAPAImg.length; i++){
//            if(null != SAPAImg[i]){
//            	switch (i){
//		        case 0:
//		        	img1.setVisibility(View.VISIBLE);
//		            (img1).setImageBitmap(ImageManager.getImage(SAPAImg[i].getSPixRGBA32(), (int)SAPAImg[i].getSWidth(), (int)SAPAImg[i].getSHeight()));
//		            break;
//		        case 1:
//		        	img2.setVisibility(View.VISIBLE);
//		            (img2).setImageBitmap(ImageManager.getImage(SAPAImg[i].getSPixRGBA32(), (int)SAPAImg[i].getSWidth(), (int)SAPAImg[i].getSHeight()));
//		            break;
//		        case 2:
//		        	img3.setVisibility(View.VISIBLE);
//		            (img3).setImageBitmap(ImageManager.getImage(SAPAImg[i].getSPixRGBA32(), (int)SAPAImg[i].getSWidth(), (int)SAPAImg[i].getSHeight()));
//		            break;
//		        case 3:
//		        	img4.setVisibility(View.VISIBLE);
//		            (img4).setImageBitmap(ImageManager.getImage(SAPAImg[i].getSPixRGBA32(), (int)SAPAImg[i].getSWidth(), (int)SAPAImg[i].getSHeight()));
//		            break;
//		        case 4:
//		        	img5.setVisibility(View.VISIBLE);
//		            (img5).setImageBitmap(ImageManager.getImage(SAPAImg[i].getSPixRGBA32(), (int)SAPAImg[i].getSWidth(), (int)SAPAImg[i].getSHeight()));
//		            break;
//		        case 5:
//		        	img6.setVisibility(View.VISIBLE);
//		            (img6).setImageBitmap(ImageManager.getImage(SAPAImg[i].getSPixRGBA32(), (int)SAPAImg[i].getSWidth(), (int)SAPAImg[i].getSHeight()));
//		            break;
//		        case 6:
//		        	img7.setVisibility(View.VISIBLE);
//		            (img7).setImageBitmap(ImageManager.getImage(SAPAImg[i].getSPixRGBA32(), (int)SAPAImg[i].getSWidth(), (int)SAPAImg[i].getSHeight()));
//		            break;
//		        case 7:
//		        	img8.setVisibility(View.VISIBLE);
//		            (img8).setImageBitmap(ImageManager.getImage(SAPAImg[i].getSPixRGBA32(), (int)SAPAImg[i].getSWidth(), (int)SAPAImg[i].getSHeight()));
//		            break;
//		        default:
//		        	break;
//		        }
//            }else{
//            	switch(i){
//            	case 0:
//            		img1.setVisibility(View.GONE);
//            		break;
//            	case 1:
//            		img2.setVisibility(View.GONE);
//            		break;
//            	case 2:
//            		img3.setVisibility(View.GONE);
//            		break;
//            	case 3:
//            		img4.setVisibility(View.GONE);
//            		break;
//            	case 4:
//            		img5.setVisibility(View.GONE);
//            		break;
//            	case 5:
//            		img6.setVisibility(View.GONE);
//            		break;
//            	case 6:
//            		img7.setVisibility(View.GONE);
//            		break;
//            	case 7:
//            		img8.setVisibility(View.GONE);
//            		break;
//            	default:
//            		break;
//            	}
//            }
//		}
//    }
//Del 2011/08/11 Z01thedoanh (自由解像度対応) End <--
    /**
     * 案内リストのデータを設定する
	**/
    private void setGuidanceListItem(ZDGHighwayGuideInfo stDGHGInfo, int iListIndex){

    	 int kindCode = stDGHGInfo.getIKindCode();

         if ((kindCode & Constants.DG_HIGHWAY_GUIDE_IC) != 0) {
        	 m_iResId[iListIndex] = R.drawable.ic_flag;
         }

         if ((kindCode & Constants.DG_HIGHWAY_GUIDE_JCT) != 0) {
        	 m_iResId[iListIndex] = R.drawable.jct_flag;
         }

         if ((kindCode & Constants.DG_HIGHWAY_GUIDE_SA) != 0) {

        	 m_iResId[iListIndex] = R.drawable.sa_flag;
             setSAPAService(stDGHGInfo,iListIndex);
         }

         if ((kindCode & Constants.DG_HIGHWAY_GUIDE_PA) != 0) {

        	 m_iResId[iListIndex] = R.drawable.pa_flag;
             setSAPAService(stDGHGInfo,iListIndex);
         }
         if ((kindCode & Constants.DG_HIGHWAY_GUIDE_TOLLGATE) != 0) {

        	 m_iResId[iListIndex] = R.drawable.toll_flag;
             //setSAPAService(stDGHGInfo,iListIndex);
         }

         if (((kindCode & Constants.DG_HIGHWAY_GUIDE_START) != 0)
                 || ((kindCode & Constants.DG_HIGHWAY_GUIDE_VIA) != 0)
                 || ((kindCode & Constants.DG_HIGHWAY_GUIDE_GOAL) != 0)
                 || ((kindCode & Constants.DG_HIGHWAY_GUIDE_TOLLGATE) != 0)) {
             String strChargeYen = "--";
             long ChargeYen = stDGHGInfo.getUlnTollChargeYen();
             if (ChargeYen > 0) {
                 strChargeYen = "￥" + String.valueOf(ChargeYen);
             }
             m_szToll[iListIndex] = strChargeYen;
             //m_layoutSAPA[iListIndex].removeAllViews();
         }

         if(0 == stDGHGInfo.getUlnRestTimeMinute() && stDGHGInfo.getUlnDistMeter() != 0)
         {
             stDGHGInfo.setUlnRestTimeMinute(1);
         }
         m_szName[iListIndex] = stDGHGInfo.getSzName();
         m_szDistance[iListIndex] = CommonLib.getFormatGuideDistance(stDGHGInfo.getUlnDistMeter());
         m_szTime[iListIndex] = CommonLib.getFormatGuideMinute(stDGHGInfo.getUlnRestTimeMinute());
    }

    /**
     * SAPAのサービス内容の種別情報を設定する
     * @param listNum
     * @param stDGHGInfo
     * @return
     */
    private boolean setSAPAService(/*int listNum,*/ZDGHighwayGuideInfo stDGHGInfo/*, LinearLayout layoutSAPAInfo*/,int iListIndex) {
/*        List<Integer> punKindCord = new ArrayList<Integer> ();
        long ulnDrawCheck = 0;
        int unKindCnt = 0;*/
        int[] SAPAServiceCode = stDGHGInfo.getUnSAPAServiceCode();
        /*for (int unKindCount = 0; unKindCount < stDGHGInfo.getUnSAPAServiceCodeNum(); unKindCount++) {
            for (int unTblCount = 0; unTblCount < 15; unTblCount++) {
                if (SAPAServiceCode[unKindCount] == AST_KIND_CODE_TBL[unTblCount][0]
                        && (ulnDrawCheck & AST_KIND_CODE_TBL[unTblCount][1]) == 0) {
                    if (unKindCnt >= Constants.SAPA_SHOW_ICON_COUNT) {
                        break;
                    }
                    punKindCord.add(AST_KIND_CODE_TBL[unTblCount][0]);
                    unKindCnt++;
                    ulnDrawCheck |= AST_KIND_CODE_TBL[unTblCount][1];
                }
            }
        }*/
        //layoutSAPAInfo.removeAllViews();
        if( SAPAServiceCode.length>Constants.SAPA_SHOW_ICON_COUNT ){
            sortData(SAPAServiceCode);
         }
        // アイコン表示の個数を取得
        for (int i = 0; i < SAPAServiceCode.length; i++ ){
            if (i < Constants.SAPA_SHOW_ICON_COUNT) {
            	setSAPA(SAPAServiceCode[i], iListIndex, i);
            }
        }
        return true;
    }

    private void swap(int[] data, int i, int j) {
        int temp = data[i];
           data[i] = data[j];
           data[j] = temp;
       }

   private void sortData(int[] data){
       if( data!=null && data.length>0 ){
           for( int i=1;i<data.length;i++ ){
               for( int j=i;(j>0&& getPriority(data[j])<getPriority(data[j-1]));j-- ){
                   swap(data,j,j-1);
               }
           }
       }
   }

   private int getPriority(int serviceCode){
       int priority = 0x00;
       switch(serviceCode){
       case Constants.NUI_ICON_TOILET1: //0x8814
           priority = 0x10;
           break; // IC
       case Constants.NUI_ICON_TOILET2: //0x881F
           priority = 0x20;
           break; // IC
       case Constants.NUI_ICON_TOILET3: //0x880F
           priority = 0x20;
           break; // IC
       case Constants.NUI_ICON_RESTAURANT1://ox880c
       case Constants.NUI_ICON_RESTAURANT2://0x880E
       case Constants.NUI_ICON_RESTAURANT3://0x8807
           priority = 0x40;
           break;
       case Constants.NUI_ICON_GAS1:
           priority = 0x32;
           break;
       case Constants.NUI_ICON_GAS2:
           priority = 0x38;
           break;
       case Constants.NUI_ICON_GAS3:
           priority = 0x35;
           break;
       case Constants.NUI_ICON_GAS4:
           priority = 0x31;
           break;
       case Constants.NUI_ICON_GAS5:
           priority = 0x34;
           break;
       case Constants.NUI_ICON_GAS6:
           priority = 0x36;
           break;
       case Constants.NUI_ICON_GAS7:
           priority = 0x39;
           break;
       case Constants.NUI_ICON_GAS8:
           priority = 0x37;
           break;
       case Constants.NUI_ICON_GAS9:
           priority = 0x33;
           break;
       case Constants.NUI_ICON_GAS10:
       case Constants.NUI_ICON_GAS11:
       case Constants.NUI_ICON_GAS12:
       case Constants.NUI_ICON_GAS13:
           priority = 0x3a;
           break;
       case Constants.NUI_STORE1:
           priority = 0x5c;
           break;
       case Constants.NUI_STORE2:
           priority = 0x55;
           break;
       case Constants.NUI_STORE3:
           priority = 0x54;
           break;
       case Constants.NUI_STORE4:
           priority = 0x59;
           break;
       case Constants.NUI_STORE5:
           priority = 0x53;
           break;
       case Constants.NUI_STORE6:
           priority = 0x52;
           break;
       case Constants.NUI_STORE7:
           priority = 0x58;
           break;
       case Constants.NUI_STORE8:
           priority = 0x56;
           break;
       case Constants.NUI_STORE9:
           priority = 0x51;
           break;
       case Constants.NUI_STORE10:
           priority = 0x5d;
           break;
       case Constants.NUI_STORE11:
           priority = 0x5b;
           break;
       case Constants.NUI_STORE12:
           priority = 0x5a;
           break;
       case Constants.NUI_STORE13:
           priority = 0x57;
           break;
       case Constants.NUI_STORE14:
       case Constants.NUI_STORE15:
       case Constants.NUI_STORE16:
       case Constants.NUI_STORE17:
       case Constants.NUI_STORE18:
       case Constants.NUI_STORE19:
       case Constants.NUI_STORE20:
           priority = 0x5d;
           break;
       case Constants.NUI_SHOP21:
       case Constants.NUI_SHOP22:
       case Constants.NUI_SHOP23:
           priority = 0x50;
           break;
       case Constants.NUI_REST1:
           priority = 0x62;
           break;
       case Constants.NUI_REST2:
           priority = 0x65;
           break;
       case Constants.NUI_REST3:
           priority = 0x63;
           break;
       case Constants.NUI_REST4:
           priority = 0x64;
           break;
       case Constants.NUI_REST5:
           priority = 0x61;
           break;
       case Constants.NUI_TRAFFIC1:
           priority = 0x71;
           break;
       case Constants.NUI_TRAFFIC2:
           priority = 0x72;
           break;
       case Constants.NUI_COMMUNCATION1:
           priority = 0x81;
           break;
       case Constants.NUI_COMMUNCATION2:
           priority = 0x82;
           break;
       case Constants.NUI_MONEY1:
           priority = 0x92;
           break;
       case Constants.NUI_MONEY2:
           priority = 0x91;
           break;
       case Constants.NUI_FACILITY1:
           priority = 0xa1;
           break;
       case Constants.NUI_FACILITY2:
           priority = 0xa3;
           break;
       case Constants.NUI_FACILITY3:
           priority = 0xa2;
           break;
       case Constants.NUI_MAIL:
           priority = 0xb1;
           break;
       case Constants.NUI_IC:
           priority = 0xb1;
           break;
       default:
           priority = 0xff;
           break;
       }
       return priority;
   }
//   private ZLandmarkData_t sapaImage = new ZLandmarkData_t();
    private void setSAPA(int ServiceCode/*,LinearLayout layoutSAPAInfo*/,int iListIndex, int iIconIndex) {
        //mod liutch 0503 bug 1046 -->
        ZLandmarkData_t sapaImage = new ZLandmarkData_t();
// Chg 2011/10/12 katsuta Start -->
//Chg 2011/08/16 Z01thedoanh Start -->
//    	NaviRun.GetNaviRunObj().JNI_DAL_GetLandmarkData(ServiceCode, Constants.Icon_Size, sapaImage);
//----------------------------------------------------------------------------
//    	NaviRun.GetNaviRunObj().JNI_MP_OutSidePOIIcon_GetIconData(ServiceCode, Constants.Icon_Size, sapaImage);
//Chg 2011/08/16 Z01thedoanh End <--
        if (Constants.useKiwiIconFlag){
        	NaviRun.GetNaviRunObj().JNI_DAL_GetLandmarkData(ServiceCode, Constants.Icon_Size, sapaImage);
        }
        else {
        	NaviRun.GetNaviRunObj().JNI_MP_OutSidePOIIcon_GetIconData(ServiceCode, Constants.Icon_Size, sapaImage);
        }
// Chg 2011/10/12 katsuta End <--

    	if(sapaImage.getSWidth() > 0 && sapaImage.getSHeight() > 0)
    	{
    		m_iSAPAResId[iListIndex][iIconIndex] = sapaImage;
    	}
    	else
    	{
    	m_iSAPAResId[iListIndex][iIconIndex] = null;
    	}

    	sapaImage = null;
    	//<--
    }

    /*private void addSaPaIcon(LinearLayout parent, int resId){

        ImageView imgDistance = null;
        if( resId != -1){
            imgDistance = new ImageView(this.activity);
            Bitmap image = imageManager.getBitmap(resources, resId);
            imgDistance.setImageBitmap(image);
        }

        addSaPaIcon(parent, imgDistance);
    }

    private void addSaPaIcon(LinearLayout parent, View child){
        if(parent != null && child != null){
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT );
            params.rightMargin = 5;
            parent.addView(child, params);
        }
    }*/


//    /**
//     * 案内リストのボタンを押す場合、案内リストのデータを更新する
//     * @param firstIndex 自己位置直近のインデックス番号
//     */
//    private void refreshHighwayGuide(long firstIndex) {/*
//        long lnGroupIdx = guideListGroupIndex;
//        JNILong lnListNum = new JNILong();
//        NaviRun.GetNaviRunObj().JNI_DG_GetHighwayGuideListNum(lnGroupIdx, lnListNum);
//
//        this.updateHighwayGuide(lnGroupIdx, firstIndex, lnListNum.getLcount());
//    */}


    /**
     * 方面看板を表示する
     */
    public void showImageDirectionBoard() {

        this.hideIntersectionInfo();

        CommonLib.setBIsDirectionBoard(true);

        if(null == image)
        {
            image = new ZNUI_IMAGE();
        }

        boolean hasImageGuide = false;
        NaviRun.GetNaviRunObj().JNI_Java_GetGuidePicture(image);
        if(image != null && (image.getSWidth() > 0 && image.getSHeight() > 0)){
            Bitmap bitmap = ImageManager.getImage(image.getSPixRGBA32(), image.getSWidth(), image.getSHeight());
            imgDisplayBoard.setImageBitmap(bitmap);
            hasImageGuide = true;
        }else{
            setViewInvisible(imgDisplayBoard);
        }
        JNIString strName = new JNIString();
        NaviRun.GetNaviRunObj().JNI_Java_GetName(strName);
        String strCrossName = strName.getM_StrAddressString();
        if(strCrossName == null || strCrossName.length() == 0 ){
        	strCrossName=activity.getResources().getString(R.string.intersection_name);
        }
        if (strCrossName.length() > 0 && !MapView.getInstance().isMapScrolled() ) {
            txtCrossName.setText(strCrossName);
            setViewVisible(layoutIntersectionInfo);
        }
        if(hasImageGuide && !MapView.getInstance().isMapScrolled())
        {
            if (isShowGuideList()) {
                //guideList.hide();
                lvICListView.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//                layoutFirstNode.setVisibility(View.GONE);
//                layoutSecondNode.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) End <--
            }
            setViewVisible(imgDisplayBoard);
            this.changecalendarToLeft();
            if (!isBackgroundVisible()) {
                showGuideBackGround();
            }
        }
    }


    /**
     * 高速画像案内を表示する
     */
    public void showImageGuide() {

        if(CommonLib.isBIsApproachHighway())
        {
            setViewInvisible(layoutAppoachIntersection);
            CommonLib.setBIsAppoachIntersection(false);
        }
        this.hideIntersectionInfo();
        Java_Guide guide = new Java_Guide();
        NaviRun.GetNaviRunObj().JNI_Java_GetGuide(guide);
        CommonLib.setBIsShowJct(true);

        if(null == image)
        {
            image = new ZNUI_IMAGE();
        }

        boolean hasImage = false;
        NaviRun.GetNaviRunObj().JNI_Java_GetGuidePicture(image);
        if(image != null && (image.getSWidth() > 0 && image.getSHeight() > 0)){
            Bitmap bitmap = ImageManager.getImage(image.getSPixRGBA32(), image.getSWidth(), image.getSHeight());
//            imgDisplayBoard.setImageBitmap(null);
            imgDisplayBoard.setImageBitmap(bitmap);
            hasImage = true;
        }else{
            setViewInvisible(imgDisplayBoard);
        }

     // XuYang add start #1020
//      if(null == kindData)
//      {
      // XuYang add end #1020
          kindData = new Java_KINDDATA();
      // XuYang add start #1020
//      }
      // XuYang add end #1020
        NaviRun.GetNaviRunObj().JNI_Java_GetKindInfo(kindData);
        String strKindName = kindData.getSKindName();
        String strDirectionName = kindData.getSDirectionName();

        //For GuideInfo isn't synchronisation Start
        if ((strKindName != null && strKindName.length() > 0)
                || (strDirectionName != null && strDirectionName.length() > 0)) {
            if((kindData.getIGuideType() != Constants.NUI_GUIDE_CROSS) && (kindData.getIKindType() != Constants.NUI_KIND_TYPE_CROSS)){
                CommonLib.setBIsAppoachIntersection(true);
                setKindMark(kindData.getIKindType(), imgFlagAppoachIntersection);
                txtJCTName.setText(strKindName);
                txtSiteName.setText(strDirectionName);

                //setViewVisible(layoutAppoachIntersection);
                layoutLaneInfo.removeAllViews();
                setViewInvisible(laneInfo);
                setViewInvisible(layoutIntersectionInfo);
            }
        }

        if(hasImage && !MapView.getInstance().isMapScrolled())
        {
            if (CommonLib.isBIsAppoachIntersection()) {
                setViewVisible(layoutAppoachIntersection);
            }

            if (isShowGuideList()) {
                //guideList.hide();
                lvICListView.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//                layoutFirstNode.setVisibility(View.GONE);
//                layoutSecondNode.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) End <--
            }

            setViewVisible(imgDisplayBoard);

            this.changecalendarToLeft();

            if (!isBackgroundVisible()) {
                showGuideBackGround();
            }
        }
    }


    /**
     * 高速画像案内を隠す
     */
    public void hideImageGuide() {

      //add liutch bug1096 -->
    	setGuideBackClickable(false);
        //<--
        /*if (CommonLib.isBIsApproachHighway()) {
            setViewInvisible(layoutAppoachIntersection);
            CommonLib.setBIsShowJct(false);
            CommonLib.setBIsAppoachIntersection(false);

            CommonLib.setBIsApproachHighway(false);

            return;
        }*/
        if (!CommonLib.isBIsGuideEnd()) {
            setViewInvisible(imgDisplayBoard);
            //setViewInvisible(guideBackground);
            setViewInvisible(layoutAppoachIntersection);
            if(!CommonLib.isBIsShowGuide()){
            	hideGuideBackGround();
            }
            if(false == CommonLib.isBIsShowMagMap()){
            	setViewInvisible(layoutIntersectionInfo);
            }
        }
        CommonLib.setBIsDirectionBoard(false);
        CommonLib.setBIsShowJct(false);
        //at 10/04/07 For GuideInfo isn't synchronisation Start
        if (CommonLib.isBIsAppoachIntersection()) {
            CommonLib.setBIsAppoachIntersection(false);
        }
        // For GuideInfo isn't synchronisation End
        if (CommonLib.isBIsShowGuide()){
            if(!MapView.getInstance().isMapScrolled()){
                //guideList.show();
            	//showGuidanceList();
            	if(lvICListView != null && lnListNum != null && lnIdx != null){
//Chg 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//            		if((lnListNum.getLcount() - lnIdx.getM_lLat()) == 2){
//        			layoutSecondNode.setVisibility(View.VISIBLE);
//        		    layoutFirstNode.setVisibility(View.VISIBLE);
//                }else if((lnListNum.getLcount() - lnIdx.getM_lLat()) == 1){
//                	layoutSecondNode.setVisibility(View.VISIBLE);
//                }else
//----------------------------------------------------------------------------
                    if((lnListNum.getLcount() - lnIdx.getM_lLat()) > 0)
//Chg 2011/08/11 Z01thedoanh (自由解像度対応) End <--
                    {
                    	//oMyAdapter.notifyDataSetChanged();
                    	//NaviLog.d("Test","---1--hideImageGuide, Last Position is "+lvICListView.getLastVisiblePosition());
                    	lvICListView.setVisibility(View.VISIBLE);
                    	/*if(lvICListView.isStackFromBottom()){
                    		lvICListView.setSelection((int)lnListNum.getLcount());
                    		lvICListView.setStackFromBottom(true);
                    		NaviLog.d("Test","---2--hideImageGuide, Last Position is "+lvICListView.getLastVisiblePosition());
                    	}*/
                    }
            	}
                changecalendarToLeft();
            }
            else{
                if(CommonLib.isBIsShowMagMapName()){
                    changecalendarToLeft();
                }else{
                    changecalendarToRight();
                }
            }
        }
        else {
            if (CommonLib.isBIsShowMagMapName()) {
                changecalendarToLeft();
            } else {
                changecalendarToRight();
            }
        }
    }

    /**
     * 方面看板を隠す
     */
    public void hideDisplayBoard()
    {
      //add liutch bug1096 -->
		setGuideBackClickable(false);
        //<--
        if(!CommonLib.isBIsGuideEnd())
        {
            setViewInvisible(imgDisplayBoard);
            hideGuideBackGround();
            setViewInvisible(layoutAppoachIntersection);
        }
    }


    /**
     * 簡易案内対象変化の内容を更新する
     */
    public void updateSimpleGuideInfo() {
        /*if(null == pstGuide)
        {
            pstGuide = new Java_Guide();
        }*/

        if(null == pstSimpleGuideInfo)
        {
            pstSimpleGuideInfo = new JNITwoInt();
        }

        if(null == plnRestDistance)
        {
            plnRestDistance = new JNILong();
        }

        if(null == pstShowGuide)
        {
            pstShowGuide = new JNITwoInt();
        }

        int map_view_flag = MapView.getInstance().getMap_view_flag();

        //NaviRun.GetNaviRunObj().JNI_Java_GetGuide(pstGuide);

        //案内ポイントでの矢印向き、案内ポイントまでの距離を取得する
        int naviMode = CommonLib.getNaviMode();
        if(naviMode == Constants.NE_NAVIMODE_CAR){
            NaviRun.GetNaviRunObj().JNI_Java_GetJCTDirectionInfo(pstSimpleGuideInfo);
        }else if(naviMode == Constants.NE_NAVIMODE_MAN){
            NaviRun.GetNaviRunObj().JNI_WG_GetJctDirectionInfo(pstSimpleGuideInfo);
        }

        int eJctArrow = pstSimpleGuideInfo.getM_iSizeX();
        int eKindType = pstSimpleGuideInfo.getM_iSizeY();

        //残距離
        if(naviMode == Constants.NE_NAVIMODE_CAR){
            NaviRun.GetNaviRunObj().JNI_Java_GetRestDistance(plnRestDistance);
        }else if(naviMode == Constants.NE_NAVIMODE_MAN){
            NaviRun.GetNaviRunObj().JNI_WG_GetRestDistance(plnRestDistance);
        }

        long lDistance = plnRestDistance.getLcount();
        if(lDistance < 0){
            lDistance = 0;
        }
//Chg 2011/09/18 Z01_h_yamada Start -->
// Chg 2011/08/25 katsuta Start -->
// 5km以上先の"先"をとる(#1444)
//      btnNextDist.setText(CommonLib.getFormatDistance(lDistance) +"先");
//      if (lDistance >= 5000) {
//	     	btnNextDist.setText(CommonLib.getFormatDistance(lDistance));
//      }
//      else {
//         	btnNextDist.setText(CommonLib.getFormatDistance(lDistance) +"先");
//      }
// Chg 2011/08/25 katsuta End <--
//--------------------------------------------
        // 5km以上先の"先"をとる(#1444)
		if (lDistance >= 5000) {
			textNextDist.setText(CommonLib.getFormatDistance(lDistance));
		} else {
			textNextDist.setText(CommonLib.getFormatDistance(lDistance) +"先");
		}
//Chg 2011/09/18 Z01_h_yamada End <--


        NaviRun.GetNaviRunObj().JNI_Java_GetShowGuide(pstShowGuide);
        // if (pstShowGuide.getM_iSizeX() == Constants.NUI_GUIDE_SIMPLE
        // && pstShowGuide.getM_iSizeY() == Constants.NUI_STATE_SHOW) {
        if (pstShowGuide.getM_iSizeX() == Constants.NUI_GUIDE_SIMPLE) {
            if (pstShowGuide.getM_iSizeY() == Constants.NUI_STATE_SHOW) {
//                setViewVisible(layoutArrivalForecastInfo);
                setViewVisible(simple_guide_info);

                setGuideDirection(eJctArrow);
//                setSimpleGuideKind(eKindType);

                if (naviMode == Constants.NE_NAVIMODE_MAN) {
                    setWGSimpleGuideKind(eKindType);
                }

                if (CommonLib.isBIsGuideEnd()) {
//Chg 2011/09/18 Z01_h_yamada Start -->
//                    setViewInvisible(btnNextDist);
//--------------------------------------------
                    setViewInvisible(textNextDist);
//Chg 2011/09/18 Z01_h_yamada End <--
                    this.changePoiTitleToLong();
                } else {
                    if (map_view_flag == Constants.NAVI_MAP_VIEW
                            || map_view_flag == Constants.SIMULATION_MAP_VIEW) {
//Chg 2011/09/18 Z01_h_yamada Start -->
//                        setViewVisible(btnNextDist);
//--------------------------------------------
                        setViewVisible(textNextDist);
//Chg 2011/09/18 Z01_h_yamada End <--
                    }
                    this.changePoiTitleToSmall();
                }
            }else if(pstShowGuide.getM_iSizeY() == Constants.NUI_STATE_HIDE){
//                setViewInvisible(layoutArrivalForecastInfo);
                setViewInvisible(simple_guide_info);
            }
        }
        // xuyang modi start #2063
        if(lDistance <= 0 ){
        	setViewInvisible(simple_guide_info);
        }
        // xuyang modi end #2063
    }

    /**
     * 案内リストを隠す
     */
    public void hideGuideList() {

        if(!CommonLib.isBIsGuideEnd())
        {
//            setViewInvisible(layoutGuidance);
            //guideList.hide();
            lvICListView.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//            layoutFirstNode.setVisibility(View.GONE);
//            layoutSecondNode.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) End <--
            if(false == isVisible(imgDisplayBoard)){
            	hideGuideBackGround();
            }
            changecalendarToRight();
            CommonLib.setBIsShowGuide(false);
        }
    }


    /**
     * レーン情報の距離を更新します
     */
    public void updateLaneDis() {
        JNILong lnDistance = new JNILong();
        NaviRun.GetNaviRunObj().JNI_Java_GetLaneRestDistance(lnDistance);

    	if(Constants.LONG_MAX == lnDistance.lcount)
    	{
    		return;
    	}

        if(lnDistance.lcount <= 0)
        {
            if(!CommonLib.isBIsGuideEnd())
            {
                layoutLaneInfo.removeAllViews();
                setViewInvisible(laneInfo);
//                txtLaneInfo.setVisibility(View.INVISIBLE);
//                layoutLaneInfo.setVisibility(View.INVISIBLE);
            }
        }else{
            txtLaneInfo.setText(CommonLib.getFormatDistance(lnDistance.getLcount()));
            //setViewVisible(laneInfo);
        }
    }


    /**
     * レーン情報を更新する
     */
    public void updateLaneInfo() {

    	if(CommonLib.isBIsShowGuide()){
    		return;
    	}
        if(null == showGuide)
        {
        	showGuide = new JNITwoInt();
        }

        NaviRun.GetNaviRunObj().JNI_Java_GetShowGuide(showGuide);
        if(null == laneGuideInfo)
        {
            laneGuideInfo = new ZNE_LaneGuideInfo();
        }

        NaviRun.GetNaviRunObj().JNI_Java_GetLaneGuideInfo(laneGuideInfo);
        if (Constants.NUI_STATE_SHOW == showGuide.getM_iSizeY()&&
                Constants.NUI_GUIDE_LANE == showGuide.getM_iSizeX() ) {
            if(!CommonLib.isBIsGuideEnd())
            {
                layoutLaneInfo.removeAllViews();
            }
            layoutLaneInfo.removeAllViews();
			setViewVisible(laneInfo);
//        	setViewVisible(layoutLaneInfo);
//        	txtLaneInfo.setVisibility(View.VISIBLE);
        	showLaneInfoPic(laneGuideInfo);
        } else if(Constants.NUI_STATE_HIDE == showGuide.getM_iSizeY()&&
                Constants.NUI_GUIDE_LANE == showGuide.getM_iSizeX() ){
            if(!CommonLib.isBIsGuideEnd())
            {
                layoutLaneInfo.removeAllViews();
                setViewInvisible(laneInfo);
//                txtLaneInfo.setVisibility(View.INVISIBLE);
//                layoutLaneInfo.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * レーン情報のImageViewのID
     */
    private int imgId = 0;
    /**
     * レーン情報の方向フラッグ
     */
    private int[] laneDir = {
            Constants.DEF_LANE_ARROW_BIT_FORWARD,
            Constants.DEF_LANE_ARROW_BIT_TILT_RIGHT,
            Constants.DEF_LANE_ARROW_BIT_TILT_LEFT,
            Constants.DEF_LANE_ARROW_BIT_TURN_RIGHT,
            Constants.DEF_LANE_ARROW_BIT_TURN_LEFT,
            Constants.DEF_LANE_ARROW_BIT_BACK_RIGHT,
            Constants.DEF_LANE_ARROW_BIT_BACK_LEFT,
            Constants.DEF_LANE_ARROW_BIT_U_TURN };

    static final int MAX_LANE_COUNT = 8;
//Del 2011/10/12 Z01_h_yamada Start -->
//    final int SEPARATOR_WIDTH = 4;
//Del 2011/10/12 Z01_h_yamada End <--
    int MaxLaneWidth = 0;
//Del 2011/10/18 Z01_h_yamada Start -->
//    int OffsetLanePosLeft = 0;
//Del 2011/10/18 Z01_h_yamada End <--
    int LeftLaneChangeCount = 0;
//Del 2011/10/12 Z01_h_yamada Start -->
//    int RightLaneChangeCount = 0;
//Del 2011/10/12 Z01_h_yamada End <--
//Add 2011/10/14 Z01_h_yamada Start -->
    private int layoutLaneInfoMaxWidth = 0;
    private int LaneFrameInfoWidth = 0;
//Add 2011/10/14 Z01_h_yamada End <--

    /**
     * レーン情報を表示する
     * @param laneGuideInfo レーン情報
     */
    private void showLaneInfoPic(ZNE_LaneGuideInfo laneGuideInfo) {
        int ucEnterLaneNum = laneGuideInfo.getCEnterLaneNum();      //交差点進入レーン数(0-16)
        int ucLeftAddLaneNum = laneGuideInfo.getCLeftAddLaneNum();  //左レーン増加数
        int ucRightAddLaneNum = laneGuideInfo.getCRightAddLaneNum();//右レーン増加数

        int ucLeftReduceLaneNum = laneGuideInfo.getCLeftReduceLaneNum();    //左レーン減少数
        int ucRightReduceLaneNum = laneGuideInfo.getCRightReduceLaneNum();//右レーン減少数

        imgId = 0;
        int laneCnt = 0;

//Chg 2011/10/12 Z01_h_yamada Start -->
//        MaxLaneWidth      = layoutLaneInfo.getWidth()/MAX_LANE_COUNT -SEPARATOR_WIDTH;
//        OffsetLanePosLeft = (layoutLaneInfo.getWidth() -(ucEnterLaneNum *MaxLaneWidth)) /2;
//        LeftLaneChangeCount = 0;
//        RightLaneChangeCount = 0;
//--------------------------------------------
//Chg 2011/10/14 Z01_h_yamada Start -->
//        MaxLaneWidth      = layoutLaneInfo.getWidth()/MAX_LANE_COUNT -IMG_MAP_LANE_MARK_SEPARATOR_LANE0.getWidth();
//        OffsetLanePosLeft = (layoutLaneInfo.getWidth() -(ucEnterLaneNum *MaxLaneWidth)) /2;
//        LeftLaneChangeCount = 0;
//--------------------------------------------

        if ( layoutLaneInfoMaxWidth == 0 ) {
        	layoutLaneInfoMaxWidth = layoutLaneInfo.getWidth();
        	LaneFrameInfoWidth     = laneInfo.getWidth();
        }
        MaxLaneWidth      = layoutLaneInfoMaxWidth/MAX_LANE_COUNT -IMG_MAP_LANE_MARK_SEPARATOR_LANE0.getWidth();

        ViewGroup.LayoutParams params = laneInfo.getLayoutParams();
        params.width = LaneFrameInfoWidth - ((MAX_LANE_COUNT -ucEnterLaneNum) *MaxLaneWidth);
//        params.width = (ucEnterLaneNum *MaxLaneWidth) + txtLaneInfo.getWidth();
        laneInfo.setLayoutParams(params);
//        Log.i("test", "showLaneInfoPic ucEnterLaneNum=" + ucEnterLaneNum + " layoutLaneInfoMaxWidth=" + layoutLaneInfoMaxWidth + " LaneFrameInfoWidth=" + LaneFrameInfoWidth + " MaxLaneWidth=" + MaxLaneWidth + " params.width=" + params.width);

        LeftLaneChangeCount = 0;
//Chg 2011/10/14 Z01_h_yamada End <--


//        NaviLog.d("showLaneInfoPic","layoutLaneInfo.getWidth()="+ layoutLaneInfo.getWidth()+" layoutLaneInfo.getHeight()="+ layoutLaneInfo.getHeight()+" nMaxLaneWidth="+nMaxLaneWidth +" ucEnterLaneNum=" + ucEnterLaneNum);

        for (int ucArrowFlagCount = 0; ucArrowFlagCount < ucEnterLaneNum; ucArrowFlagCount++) {
            boolean bIsExitLane = (laneGuideInfo.getSExitLaneFlag() >> ((Constants.KIWI_DIRECTIONGUIDE_LANE_MAX_NUM - 1) - ucArrowFlagCount) & 0x0001) > 0;
            int ucArrowFlag = laneGuideInfo.getAucArrowFlag()[ucArrowFlagCount];


            if (isExistUTurn(ucArrowFlag)) {
                if (!isExistFowardOrSide(ucArrowFlag)) {
                    //Uターン表示
                    nui_ShowLaneItem(ucArrowFlag,
                            Constants.DEF_LANE_ARROW_BIT_U_TURN, bIsExitLane,
                            laneGuideInfo.getCExitGuideDir(), laneCnt, false, true);
                } else {
                    if (!bIsExitLane) {
                    		//Uターンと別の矢印が混在していた場合、通らないUターンは表示しない
                            nui_ShowCombiLaneItem(ucArrowFlag & ~Constants.DEF_LANE_ARROW_BIT_U_TURN,
                                    bIsExitLane, laneGuideInfo.getCExitGuideDir(), laneCnt, false, false);
                    } else {
                        if ((laneGuideInfo.getCExitGuideDir() & Constants.DEF_LANE_ARROW_BIT_U_TURN) > 0) {
                            //Uターン表示
                        	nui_ShowLaneItem(ucArrowFlag,
                                    Constants.DEF_LANE_ARROW_BIT_U_TURN,
                                    bIsExitLane, laneGuideInfo.getCExitGuideDir(), laneCnt, false, true);
                        } else {
                           //Uターンと別の矢印が混在していた場合、通らないUターンは表示しない
                           nui_ShowCombiLaneItem(ucArrowFlag & ~Constants.DEF_LANE_ARROW_BIT_U_TURN,
                                    bIsExitLane, laneGuideInfo.getCExitGuideDir(), laneCnt, false, false);
                        }
                    }
                }
            } else {//正常の場合
                    nui_ShowCombiLaneItem(ucArrowFlag,
                    		bIsExitLane, laneGuideInfo.getCExitGuideDir(), laneCnt, false, false);
            }
            //左レーン増加数
            if (ucLeftAddLaneNum > 0
                    && (ucArrowFlagCount < ucLeftAddLaneNum)) {
                nui_ShowCombiLaneItem(ucArrowFlag,
                		bIsExitLane, laneGuideInfo.getCExitGuideDir(), laneCnt, true, false);
            }
            //右レーン増加数
            if (ucRightAddLaneNum > 0
                    && (ucArrowFlagCount >= ucEnterLaneNum
                            - ucRightAddLaneNum)) {
                nui_ShowCombiLaneItem(ucArrowFlag,
                		bIsExitLane, laneGuideInfo.getCExitGuideDir(), laneCnt, false, true);
            }
            //左レーン減少数
            if(ucLeftReduceLaneNum > 0
                    && (ucArrowFlagCount < ucLeftReduceLaneNum)){
                nui_ShowReduceLaneItem(ucArrowFlag,
                        Constants.DEF_LANE_ARROW_BIT_FORWARD, bIsExitLane,
                        laneGuideInfo.getCExitGuideDir(), laneCnt, true, false);
            }

            //右レーン減少数
            if(ucRightReduceLaneNum > 0
                    && (ucArrowFlagCount >= ucEnterLaneNum
                            - ucRightReduceLaneNum)){
                nui_ShowReduceLaneItem(ucArrowFlag,
                        Constants.DEF_LANE_ARROW_BIT_FORWARD, bIsExitLane,
                        laneGuideInfo.getCExitGuideDir(), laneCnt, false, true);
            }
            laneCnt++;
        }

//Add 2011/10/18 Z01_h_yamada Start -->
        if (laneCnt > 0) {
            // 右端のレーン点線
            ImageView imgSeparator = new ImageView(this.activity);
            imgSeparator.setImageBitmap(IMG_MAP_LANE_MARK_SEPARATOR_LANE0);
            RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
            		IMG_MAP_LANE_MARK_SEPARATOR_LANE0.getWidth(),
            		RelativeLayout.LayoutParams.FILL_PARENT);
            oParams.addRule(RelativeLayout.RIGHT_OF, imgId);
            layoutLaneInfo.addView(imgSeparator, oParams);
        }
//Add 2011/10/18 Z01_h_yamada End <--

//        if (imgId > 0) {
//            ImageView imgDistance = new ImageView(this.activity);
//            imgDistance.setImageBitmap(IMG_MAP_LANE_DISTANCE_BACK);
//            RelativeLayout.LayoutParams oParams4 = new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
//            oParams4.addRule(RelativeLayout.RIGHT_OF, imgId);
//            layoutLaneInfo.addView(imgDistance, oParams4);
//        }

        setViewVisible(laneInfo);
        setViewVisible(layoutLaneInfo);
    }


    /**
     * レーン情報のビットマップを初期化する
     */
    void destroyLaneImage() {

// Del 2011/09/28 katsuta Start -->
//        CommonLib.recycleBitmap(bitmapNaviUIImg);
//        bitmapNaviUIImg = null;
// Del 2011/09/28 katsuta End <--

        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_BASE0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_STRAIGHT0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_LEFT0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFT0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFTBK0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_RIGHT0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHT0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHTBK0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_ON_UTURN);

        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_BASE0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_STRAIGHT0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_LEFT0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFT0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFTBK0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_RIGHT0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHT0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHTBK0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_ARROW_OFF_UTURN);

        CommonLib.recycleBitmap(IMG_MAP_LANE_MARK_ADD_LANE_LEFT0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_MARK_ADD_LANE_RIGHT0);

        CommonLib.recycleBitmap(IMG_MAP_LANE_MARK_REDUCE_LANE_LEFT0);
        CommonLib.recycleBitmap(IMG_MAP_LANE_MARK_REDUCE_LANE_RIGHT0);

        CommonLib.recycleBitmap(IMG_MAP_LANE_MARK_SEPARATOR_LANE0);

//        CommonLib.recycleBitmap(IMG_MAP_LANE_DISTANCE_BACK);

        CommonLib.recycleBitmap(compass);
        CommonLib.recycleBitmap(HeadupCompass);

//Del 2011/09/18 Z01_h_yamada Start -->
//        CommonLib.recycleBitmap(img_wg_bridge);
//        CommonLib.recycleBitmap(img_wg_crossing);
//Del 2011/09/18 Z01_h_yamada End <--
}
//Del 2011/09/29 Z01_h_yamada Start -->
//    /**
//     * レーン情報を表示する
//     * @param bIsExitLane
//     */
//    private void nui_ShowLaneBase(boolean bIsExitLane) {
//        if (bIsExitLane) {
//            ImageView imgFoward = new ImageView(this.activity);
//            RelativeLayout.LayoutParams oParams1 = new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
//            imgFoward.setImageBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_BASE0);
//            oParams1.addRule(RelativeLayout.RIGHT_OF, imgId - 1);
//            layoutLaneInfo.addView(imgFoward, oParams1);
//        } else {
//            ImageView imgFoward = new ImageView(this.activity);
//            RelativeLayout.LayoutParams oParams1 = new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
//            imgFoward.setImageBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_BASE0);
//            oParams1.addRule(RelativeLayout.RIGHT_OF, imgId - 1);
//            layoutLaneInfo.addView(imgFoward, oParams1);
//        }
//
//    }
//Del 2011/09/29 Z01_h_yamada End <--


    /**
     * レーン案内アイテムを表示する。
     * @param ucLaneArrowFlag レーン方向
     * @param ucItemArrowDir カレントレーン方向
     * @param bIsExitLane  エグジットフラグ
     * @param ucExitGuideDir エグジットレーン方向
     * @param laneCnt レーンの数
     * @param isLeftAddLane レーンの左増加数
     * @param isRightAddLane レーンの右増加数
     */
    private void nui_ShowLaneItem(int ucLaneArrowFlag, int ucItemArrowDir,
            boolean bIsExitLane, int ucExitGuideDir, int laneCnt,
            boolean isLeftAddLane, boolean isRightAddLane) {

//		NaviLog.d("nui_ShowLaneItem","ucLaneArrowFlag:"+ucLaneArrowFlag);
//		NaviLog.d("nui_ShowLaneItem","ucItemArrowDir:"+ucItemArrowDir);
//		NaviLog.d("nui_ShowLaneItem","bIsExitLane:"+bIsExitLane);
//		NaviLog.d("nui_ShowLaneItem","ucExitGuideDir:"+ucExitGuideDir);

        if ((ucLaneArrowFlag & ucItemArrowDir) > 0) {
            ImageView imgLane = new ImageView(this.activity);
            imgLane.setScaleType(ImageView.ScaleType.FIT_CENTER);
            RelativeLayout.LayoutParams oParams2 = new RelativeLayout.LayoutParams(
            		MaxLaneWidth,
            		RelativeLayout.LayoutParams.FILL_PARENT);
//            RelativeLayout.LayoutParams oParams2 = new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
//Del 2011/10/18 Z01_h_yamada Start -->
//            if ( laneCnt == 0) {
//            	oParams2.leftMargin = (int) OffsetLanePosLeft;
//            }
//Del 2011/10/18 Z01_h_yamada End <--
            // xuyang add start #1775
            if (isRightAddLane) {
                imgId++;
            }
            // xuyang add end #1775
            oParams2.addRule(RelativeLayout.RIGHT_OF, imgId - 1);
            if (bIsExitLane && ((ucItemArrowDir & ucExitGuideDir) > 0)) {
                switch (ucItemArrowDir) {
                case Constants.DEF_LANE_ARROW_BIT_FORWARD:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_STRAIGHT0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_TILT_RIGHT:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHT0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_TILT_LEFT:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFT0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_TURN_RIGHT:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_RIGHT0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_TURN_LEFT:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_LEFT0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_BACK_RIGHT:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_INCRIGHTBK0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_BACK_LEFT:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_ON_COMBI_INCLEFTBK0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_U_TURN:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_ON_UTURN);
                default:
                    break;
                }

            } else {
                switch (ucItemArrowDir) {
                case Constants.DEF_LANE_ARROW_BIT_FORWARD:
                	imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_STRAIGHT0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_TILT_RIGHT:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHT0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_TILT_LEFT:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFT0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_TURN_RIGHT:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_RIGHT0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_TURN_LEFT:
            		imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_LEFT0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_BACK_RIGHT:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_INCRIGHTBK0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_BACK_LEFT:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_INCLEFTBK0);
                    break;
                case Constants.DEF_LANE_ARROW_BIT_U_TURN:
                    imgLane.setImageBitmap(IMG_MAP_LANE_ARROW_OFF_UTURN);
                default:
                    break;
                }
            }
            layoutLaneInfo.addView(imgLane, oParams2);
        }

    }

    /**
     *レーン案内アイテムを表示する
     * @param ucLaneArrowFlag   レーン方向
     * @param bIsExitLane        エグジットフラグ
     * @param ucExitGuideDir     エグジットレーン方向
     * @param laneCnt            レーンの数
     * @param isLeftAddLane      レーンの左増加数
     * @param isRightAddLane   レーンの右増加数
     */
    private void nui_ShowCombiLaneItem(int ucLaneArrowFlag,
            boolean bIsExitLane, int ucExitGuideDir, int laneCnt,
            boolean isLeftAddLane, boolean isRightAddLane) {

        if (isLeftAddLane) {
            ImageView imgLeftAddLane = new ImageView(this.activity);
            imgLeftAddLane.setImageBitmap(IMG_MAP_LANE_MARK_ADD_LANE_LEFT0);
//            RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            imgLeftAddLane.setScaleType(ImageView.ScaleType.FIT_END);
            RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
            		MaxLaneWidth,
            		RelativeLayout.LayoutParams.FILL_PARENT);
//Del 2011/10/18 Z01_h_yamada Start -->
//            if ( LeftLaneChangeCount == 0) {
//            	oParams.leftMargin = (int) OffsetLanePosLeft;
//            }
//Del 2011/10/18 Z01_h_yamada End <--
            oParams.bottomMargin = 3;
            //Liugang modify start For Redmine 1659
            oParams.addRule(RelativeLayout.RIGHT_OF, LeftLaneChangeCount);
//            oParams.addRule(RelativeLayout.LEFT_OF, 0);
            //Liugang modify end  For Redmine 1659
            layoutLaneInfo.addView(imgLeftAddLane, oParams);
            LeftLaneChangeCount++;

            return;
        }
        if (isRightAddLane) {
            ImageView imgRightAddLane = new ImageView(this.activity);
            imgRightAddLane.setImageBitmap(IMG_MAP_LANE_MARK_ADD_LANE_RIGHT0);
            imgRightAddLane.setScaleType(ImageView.ScaleType.FIT_END);
            RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
            		MaxLaneWidth,
            		RelativeLayout.LayoutParams.FILL_PARENT);

            oParams.bottomMargin = 3;
//            RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
 //                   RelativeLayout.LayoutParams.WRAP_CONTENT,
  //                  RelativeLayout.LayoutParams.WRAP_CONTENT);
//Chg 2011/10/12 Z01_h_yamada Start -->
//            oParams.addRule(RelativeLayout.RIGHT_OF, imgId - RightLaneChangeCount -1);
//            layoutLaneInfo.addView(imgRightAddLane, oParams);
//            RightLaneChangeCount++;
//--------------------------------------------
           	oParams.addRule(RelativeLayout.RIGHT_OF, imgId -1);
            layoutLaneInfo.addView(imgRightAddLane, oParams);
//Chg 2011/10/12 Z01_h_yamada End <--

            return;
        }

        ImageView imgFoward = new ImageView(this.activity);
        imgFoward.setId(++imgId);
//        //curImgId=imgId;
        imgFoward.setScaleType(ImageView.ScaleType.FIT_CENTER);
        RelativeLayout.LayoutParams oParams1 = new RelativeLayout.LayoutParams(
        		MaxLaneWidth,
        		RelativeLayout.LayoutParams.FILL_PARENT);
//        RelativeLayout.LayoutParams oParams1 = new RelativeLayout.LayoutParams(
//                RelativeLayout.LayoutParams.WRAP_CONTENT,
//                RelativeLayout.LayoutParams.WRAP_CONTENT);
        imgFoward.setImageBitmap(IMG_MAP_LANE_ARROW_OFF_COMBI_BASE0);
//Del 2011/10/18 Z01_h_yamada Start -->
//        if ( laneCnt == 0) {
//        	oParams1.leftMargin = (int) OffsetLanePosLeft;
//        }
//Del 2011/10/18 Z01_h_yamada End <--

        oParams1.addRule(RelativeLayout.RIGHT_OF, imgId - 1);
        layoutLaneInfo.addView(imgFoward, oParams1);

//      nui_ShowLaneBase(bIsExitLane);
//		NaviLog.d("Test","nui_ShowCombiLaneItem -> count:"+laneDir.length);

        // OFFの矢印描画
        for (int i = 0; i < laneDir.length; i++) {
// Add 2011/09/23 katsuta Start -->
            if ((ucLaneArrowFlag & laneDir[i]) == 0) {
            	continue;
            }

            if (bIsExitLane && ((laneDir[i] & ucExitGuideDir) > 0)) {
            	continue;
            }

//    		NaviLog.d("nui_ShowCombiLaneItem off","ucLaneArrowFlag:"+ucLaneArrowFlag);
//    		NaviLog.d("nui_ShowCombiLaneItem off","bIsExitLane:"+bIsExitLane);
//    		NaviLog.d("nui_ShowCombiLaneItem off","laneDir:"+laneDir[i]);
//    		NaviLog.d("nui_ShowCombiLaneItem off","ucExitGuideDir:"+ucExitGuideDir);

// Add 2011/09/23 katsuta End <--
            nui_ShowLaneItem(ucLaneArrowFlag, laneDir[i], bIsExitLane,
                    ucExitGuideDir, laneCnt, isLeftAddLane, isRightAddLane);
        }
// Add 2011/09/23 katsuta Start -->
        // ONの矢印描画
//		NaviLog.d("nui_ShowCombiLaneItem on","laneDir.length:"+laneDir.length);
        for (int i = 0; i < laneDir.length; i++) {

            if (!bIsExitLane){
            	continue;
            }

            if	((laneDir[i] & ucExitGuideDir) == 0) {
            	continue;
            }
//    		NaviLog.d("nui_ShowCombiLaneItem on","ucLaneArrowFlag:"+ucLaneArrowFlag);
//    		NaviLog.d("nui_ShowCombiLaneItem on","bIsExitLane:"+bIsExitLane);
//    		NaviLog.d("nui_ShowCombiLaneItem on","laneDir:"+laneDir[i]);
//    		NaviLog.d("nui_ShowCombiLaneItem on","ucExitGuideDir:"+ucExitGuideDir);
            nui_ShowLaneItem(ucLaneArrowFlag, laneDir[i], bIsExitLane,
                    ucExitGuideDir, laneCnt, isLeftAddLane, isRightAddLane);
        }
// Add 2011/09/23 katsuta End <--

        if (laneCnt > 0) {
            ImageView imgSeparator = new ImageView(this.activity);
            imgSeparator.setImageBitmap(IMG_MAP_LANE_MARK_SEPARATOR_LANE0);
//Chg 2011/10/12 Z01_h_yamada Start -->
//            RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
//            		SEPARATOR_WIDTH,
//            		RelativeLayout.LayoutParams.FILL_PARENT);
//--------------------------------------------
            RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
            		IMG_MAP_LANE_MARK_SEPARATOR_LANE0.getWidth(),
            		RelativeLayout.LayoutParams.FILL_PARENT);
//Chg 2011/10/12 Z01_h_yamada End <--

//            RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
//Del 2011/10/18 Z01_h_yamada Start -->
//            if ( laneCnt == 0) {
//            	oParams.leftMargin = (int) OffsetLanePosLeft;
//            }
//Del 2011/10/18 Z01_h_yamada End <--

            oParams.addRule(RelativeLayout.RIGHT_OF, imgId - 1);
            layoutLaneInfo.addView(imgSeparator, oParams);
        }
    }

    /**
     * レーン案内アイテムを表示する
     * @param ucLaneArrowFlag   レーン方向
     * @param ucItemArrowDir    カレントレーン方向
     * @param bIsExitLane       エグジットフラグ
     * @param ucExitGuideDir    エグジットレーン方向
     * @param laneCnt          レーンの数
     * @param isLeftReduceLane  レーンの左減少数
     * @param isRightReduceLaneNum  レーンの右減少数
     */
    private void nui_ShowReduceLaneItem(int ucLaneArrowFlag, int ucItemArrowDir,
            boolean bIsExitLane, int ucExitGuideDir, int laneCnt,
            boolean isLeftReduceLane, boolean isRightReduceLaneNum) {

        if (isLeftReduceLane) {
            ImageView imgLeftAddLane = new ImageView(this.activity);
            imgLeftAddLane.setImageBitmap(IMG_MAP_LANE_MARK_REDUCE_LANE_LEFT0);
            imgLeftAddLane.setScaleType(ImageView.ScaleType.FIT_START);
            RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
            		MaxLaneWidth,
            		RelativeLayout.LayoutParams.FILL_PARENT);
//            RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
//Del 2011/10/18 Z01_h_yamada Start -->
//            if ( laneCnt == 0) {
//            	oParams.leftMargin = (int) OffsetLanePosLeft;
//            }
//Del 2011/10/18 Z01_h_yamada End <--

            oParams.addRule(RelativeLayout.RIGHT_OF, imgId - 1);
            layoutLaneInfo.addView(imgLeftAddLane, oParams);
            return;
        }
        if (isRightReduceLaneNum) {
            ImageView imgRightAddLane = new ImageView(this.activity);
            imgRightAddLane.setImageBitmap(IMG_MAP_LANE_MARK_REDUCE_LANE_RIGHT0);
            imgRightAddLane.setScaleType(ImageView.ScaleType.FIT_START);
            RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
            		MaxLaneWidth,
            		RelativeLayout.LayoutParams.FILL_PARENT);
//            RelativeLayout.LayoutParams oParams = new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
//Del 2011/10/18 Z01_h_yamada Start -->
//            if ( laneCnt == 0) {
//            	oParams.leftMargin = (int) OffsetLanePosLeft;
//            }
//Del 2011/10/18 Z01_h_yamada End <--

            oParams.addRule(RelativeLayout.RIGHT_OF, imgId - 1);
            layoutLaneInfo.addView(imgRightAddLane, oParams);
            return;
        }
    }


    /**
     * レーン情報のU-turnを判じる
     * @param ucArrowFlag レーン情報フラグ
     * @return レーン情報がU-turnの場合、trueを返す
     */
    private boolean isExistUTurn(int ucArrowFlag) {
        boolean blResult = false;
        if ((ucArrowFlag & Constants.DEF_LANE_ARROW_BIT_U_TURN) > 0) {
            blResult = true;
        }
        return blResult;
    }


    /**
     * レーン情報のエグジット状態を判じる
     * @param ucArrowFlag レーン情報のフラグ
     * @return レーン情報がエグジットの場合、trueを返す
     */
    private boolean isExistFowardOrSide(int ucArrowFlag) {
        boolean blResult = false;
        if ((ucArrowFlag & 0xfe) > 0) {
            blResult = true;
        }

        return blResult;
    }


    /**
     * 案内を終了する
     */
    public void guideEnd() {
        if(CommonLib.isBIsOnHighWay())
        {
            CommonLib.setBIsOnHighWay(false);
        }
        CommonLib.setBIsGuideEnd(true);

        CommonLib.setBIsShowGuide(false);
        CommonLib.setBIsDirectionBoard(false);
        CommonLib.setBIsShowJct(false);
        CommonLib.setBIsShowMagMap(false);
        CommonLib.setBIsShowMagMapName(false);
        CommonLib.setBIsAppoachIntersection(false);

        hideGuideInfomation();
        hiddenRepeatImageButton();
        hideArrivalForecastInfo();

//Del 2011/09/18 Z01_h_yamada Start -->
//        if(imgGuideDir != null){
//            imgGuideDir.setIcon(null);
//            imgGuideDir.setText(null);
//        }
//Del 2011/09/18 Z01_h_yamada End <--


        /*if(lvICListView != null){
            //guideList.clear();
            lvICListView.removeAllViews();;
        }*/
    }

    /**
     * 現在地を中心に移動する
     */
    public void onShowLocusMap() {

        if (CommonLib.isBIsDirectionBoard()) {
            setViewVisible(layoutIntersectionInfo);
            setViewVisible(imgDisplayBoard);
            showGuideBackGround();
            changecalendarToLeft();
        } else if (CommonLib.isBIsShowMagMap()) {
            //yangyang mod start Bug851
            //View invalid
            if (CommonLib.isBIsShowMagMapName()) {
                //yangyang mod start Bug851
//                setViewVisible(layoutIntersectionInfo);
                setViewInvisible(layoutIntersectionInfo);
              //yangyang mod end Bug851
            }
//            Java_IconInfo iconInfo = new Java_IconInfo();
//            setViewVisible(layoutCompass);
//
//            NaviRun.GetNaviRunObj().JNI_Java_GetMagMapCompass(iconInfo);
//            setExpandPointAngle(iconInfo);
            setViewInvisible(layoutCompass);
            //yangyang mod end Bug851
//Add 2011/10/06 Z01_h_yamada Start -->
            setGuideBackClickable(true);
//Add 2011/10/06 Z01_h_yamada End <--
//Add 2011/10/10 Z01kkubo Start --> 課題No.190関連
//    拡大図表示状態で現在地押下すると右下部ボタンが出てしまう不具合を改修
            this.activity.onNaviViewShow(true);
//Add 2011/10/10 Z01kkubo End <--
        } else if (CommonLib.isBIsShowJct()) {
            setViewVisible(imgDisplayBoard);
            showGuideBackGround();
            changecalendarToLeft();
            if (CommonLib.isBIsAppoachIntersection()) {
                setViewVisible(layoutAppoachIntersection);
            }

        } else if (CommonLib.isBIsShowGuide()) {
            //guideList.show();
        	//lvICListView.setVisibility(View.VISIBLE);
        	showGuidanceList();
            showGuideBackGround();
            if(oMyAdapter != null){
            	lvICListView.setSelection(oMyAdapter.getCount() - 1);
            }
            lvICListView.setStackFromBottom(true);
            /*if(lvICListView != null && lnListNum != null && lnIdx != null){
        		if((lnListNum.getLcount() - lnIdx.getM_lLat()) == 2){
        			layoutSecondNode.setVisibility(View.VISIBLE);
        		    layoutFirstNode.setVisibility(View.VISIBLE);
                }else if((lnListNum.getLcount() - lnIdx.getM_lLat()) == 1){
                	layoutSecondNode.setVisibility(View.VISIBLE);
                }else if((lnListNum.getLcount() - lnIdx.getM_lLat()) > 2){
                	//oMyAdapter.notifyDataSetChanged();
                	if(oMyAdapter != null){
      			       oMyAdapter.notifyDataSetChanged();
      			   }
                	lvICListView.setSelection((int)lnListNum.getLcount());
                	lvICListView.setVisibility(View.VISIBLE);
                }
        	}*/
            //showGuideBackGround();
            changecalendarToLeft();
        }
    }

    /**
     * 案内終了,Guide情報を隠れる
     */
    public void hideGuideInfo()
    {
        setViewInvisible(imgDisplayBoard);
        hideGuideBackGround();
        changecalendarToRight();
        setViewInvisible(layoutAppoachIntersection);
        setViewInvisible(layoutIntersectionInfo);
        setViewInvisible(layoutCompass);
        lvICListView.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//        layoutFirstNode.setVisibility(View.GONE);
//        layoutSecondNode.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) End <--
        //guideList.hide();
    }


    /**
     * 案内終了,Guide情報を隠れる
     */
    public void hideGuideInfomation()
    {
        setGuideLayoutInvisibility();
        layoutLaneInfo.removeAllViews();
        setViewInvisible(laneInfo);

//        clearManGoalView();
    }


    /**
     * 案内終了,Guide情報を隠れる
     */
    public void hideGuideInfomationForGuideEnd()
    {
        setGuideLayoutInvisibility();

        layoutLaneInfo.removeAllViews();
        setViewInvisible(laneInfo);

//        clearManGoalView();

        MapView.getInstance().setMap_view_flag(Constants.COMMON_MAP_VIEW);
    }


//    private void clearManGoalView(){
//        if(guideGoalLayout != null){
//            guideGoalLayout.removeAllViews();
//        }
//    }
//    /**
//     * 人モードの場合、方位マークのビットマップを隠れる
//     */
//    public void hideGoalLayout()
//    {
//        clearManGoalView();
//        setViewVisible(guideGoalLayout);
//
//    }


    /**
     * シミュレーション案内開始
     */
    public void onStartDemoGuide(int iArrivalTime, ZNUI_DESTINATION_DATA destData) {
        CommonLib.setBIsDemo(true);
        this.showArrivalForecastInfo(iArrivalTime, destData);
    }


    /**
     * 案内終了
     */
    public void onStopGuidance() {
        setGuideLayoutInvisibility();
        layoutLaneInfo.removeAllViews();
//        clearManGoalView();
    }


    // リピート
    public void showRepeatImage() {
        setViewVisible(imgRepeat);
    }

    public void hiddenRepeatImageButton() {
        setViewInvisible(imgRepeat);
    }

    public View getView(){
        return layoutGuide;
    }

//    @Override
//    public void onClick(View v) {
//        if(v == layoutPre){
//            JNITwoLong lnIdx = new JNITwoLong();
//            JNILong lnListNum = new JNILong();
//
//            NaviRun.GetNaviRunObj().JNI_DG_GetNextHighwayGuideInfoIdx(lnIdx);
//            NaviRun.GetNaviRunObj().JNI_DG_GetHighwayGuideListNum(lnIdx.getM_lLong(),lnListNum);
//
//            if(guideListFirstIndex <= lnIdx.getM_lLat())
//            {
//                guideListFirstIndex = lnIdx.getM_lLat();
//            }
//            guideListTotalCount = lnListNum.getLcount();
//            guideListGroupIndex = lnIdx.getM_lLong();
//
//            if (guideListFirstIndex + 3 > guideListTotalCount) {
//                refreshHighwayGuide(guideListFirstIndex);
//            } else {
//                refreshHighwayGuide(guideListFirstIndex + 1);
//            }
//
//            guideListFirstIndex += 1;
//        }else if(v == layoutNext)
//        {
//            JNITwoLong lnIdx = new JNITwoLong();
//            JNILong lnListNum = new JNILong();
//
//            NaviRun.GetNaviRunObj().JNI_DG_GetNextHighwayGuideInfoIdx(lnIdx);
//            NaviRun.GetNaviRunObj().JNI_DG_GetHighwayGuideListNum(lnIdx.getM_lLong(),
//                        lnListNum);
//            if(isCilckGuideUp && (guideListFirstIndex != lnIdx.getM_lLat()))
//            {
//                guideListTotalCount = lnListNum.getLcount();
//                guideListFirstIndex -= 1;
//                guideListGroupIndex = lnIdx.getM_lLong();
//
//                if (guideListFirstIndex != 0) {
//                    refreshHighwayGuide(guideListFirstIndex - 1);
//                } else {
//                    refreshHighwayGuide(guideListFirstIndex);
//                }
//            }
//        }
//    }

    public void onMapMove() {
//        int naviMode = CommonLib.getNaviMode();
//        if(naviMode == Constants.NE_NAVIMODE_CAR)
        {
            //add liutch bug1096 -->
        	setGuideBackClickable(false);
            //<--

            setViewInvisible(imgDisplayBoard);
            setViewInvisible(guideBackground);

            changecalendarToRight();
            setViewInvisible(layoutAppoachIntersection);

            setViewInvisible(layoutIntersectionInfo);

            setViewInvisible(layoutCompass);
            //guideList.hide();
            lvICListView.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//            layoutFirstNode.setVisibility(View.GONE);
//            layoutSecondNode.setVisibility(View.GONE);
//Del 2011/08/11 Z01thedoanh (自由解像度対応) End <--
        }
    }

    private void showGuideBackGround(){
      //add liutch bug1096 -->
    	setGuideBackClickable(true);
        //<--
        setViewVisible(guideBackground);
        this.activity.onNaviViewShow(true);
		CommonLib.setIsShowDoubleMap(true);
    }
    private void hideGuideBackGround(){
      //add liutch bug1096 -->
        if( !CommonLib.isBIsShowMagMapName() ){
        	setGuideBackClickable(false);
        }
        //<--
        setViewInvisible(guideBackground);
        this.activity.onNaviViewShow(false);
		CommonLib.setIsShowDoubleMap(false);
    }

//    private void setViewGone(View view){
//        if(view != null){
//            view.setVisibility(View.GONE);
//        }
//    }
    private void setViewVisible(View view){
        if (view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }
    private void setViewInvisible(View view){
        if(view != null){
            view.setVisibility(View.INVISIBLE);
        }
    }

    public boolean isBackgroundVisible() {
        return isVisible(guideBackground);
    }

    private boolean isVisible(View view){
        if(view  == null){
            return false;
        }
        return view.getVisibility() == View.VISIBLE;
    }

    private boolean isShowGuideList(){
//Chg 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//    	return (lvICListView.getVisibility()== View.VISIBLE || layoutFirstNode.getVisibility()== View.VISIBLE
//    			|| layoutSecondNode.getVisibility()== View.VISIBLE);
//------------------------------------------------
    	return (lvICListView.getVisibility()== View.VISIBLE);
//Chg 2011/08/11 Z01thedoanh (自由解像度対応) End <--
        //return guideList.isShow();
    }


    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
    private Bitmap backBmp = null;
    public Bitmap getBackBmp(){
    	if( backBmp==null )
    	{
    	//Add 2011/07/27 Z01thedoanh (自由解像度対応) Start -->
    	Bitmap orgBackBmp = null;
    	orgBackBmp = BitmapFactory.decodeResource(activity.getResources(), R.drawable.guide_back_ex);
    	float newHeight = ScreenAdapter.getHeight()-ScreenAdapter.getStatusBarHeight();
    	float newWidth = newHeight*Constants.GUIDE_AREA_ASPECT1;
    	fGuideHeightScale = orgBackBmp.getHeight()/newHeight;
        backBmp = CommonLib.ReSizeBmpImage(newHeight, newWidth, orgBackBmp);
    	}
        return backBmp;
    }
//Add 2011/07/27 Z01thedoanh (自由解像度対応) Start -->
    private float fGuideHeightScale = 1;

    public float getGuideHeightScale() {
        return fGuideHeightScale;
    }

//Add 2011/07/27 Z01thedoanh (自由解像度対応) End <--

    public int getBackWidth() {
        return backBmp.getWidth();
    }
    public int getBackHeight() {
        return backBmp.getHeight();
    }

    private Bitmap backGroundBmp = null;
    public Bitmap getCrossName(String crossName) {

//Chg 2011/10/01 Z01_h_yamada Start -->
//        if( backGroundBmp==null ){
//        	//Add 2011/07/27 Z01thedoanh (自由解像度対応) Start -->
//        	        	Bitmap orgBackGroundBmp = null;
//        	        	orgBackGroundBmp = BitmapFactory.decodeResource(activity.getResources(),R.drawable.txt_cross_name_back);
//        	        	backGroundBmp = CommonLib.ReSizeBmpImage(orgBackGroundBmp.getHeight()/fGuideHeightScale, backWidth - 2*CommonLib.getGuideAreaMarginLeft(), orgBackGroundBmp);
//        	//Add 2011/07/27 Z01thedoanh (自由解像度対応) End <--
//        	            backPix = new int[backGroundBmp.getWidth() * backGroundBmp.getHeight()];
//        	        }
//--------------------------------------------
        if( backGroundBmp==null ){
        	Bitmap orgBackGroundBmp = null;
//Chg 2011/10/07 Z01_h_yamada Start -->
//			orgBackGroundBmp = BitmapFactory.decodeResource(activity.getResources(),R.drawable.txt_cross_name_back);
//--------------------------------------------
   	    	orgBackGroundBmp = BitmapFactory.decodeResource(activity.getResources(),R.drawable.txt_cross_name_back_normal);
//Chg 2011/10/07 Z01_h_yamada End <--
        	backGroundBmp = CommonLib.ReSizeBmpImage(layoutIntersectionInfo.getHeight(), layoutIntersectionInfo.getWidth(), orgBackGroundBmp);
        }
//Chg 2011/10/01 Z01_h_yamada End <--

        crossWidth = backGroundBmp.getWidth();
        crossHeight = backGroundBmp.getHeight();

//Chg 2012/02/09 Z01_h_yamada Start -->
//        Bitmap bmpNew = Bitmap.createBitmap(backGroundBmp.getWidth(), backGroundBmp.getHeight(), Bitmap.Config.ARGB_8888);
//--------------------------------------------
        Bitmap bmpNew;
        try {
        	bmpNew = Bitmap.createBitmap(backGroundBmp.getWidth(), backGroundBmp.getHeight(), Bitmap.Config.ARGB_8888);
        } catch ( OutOfMemoryError e ) {
        	NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        	java.lang.System.gc();
        	// 2回目は try-catch せず、2回目も失敗した時は諦める
            try {
            	bmpNew = Bitmap.createBitmap(backGroundBmp.getWidth(), backGroundBmp.getHeight(), Bitmap.Config.ARGB_8888);
            } catch( OutOfMemoryError ex ) {
            	bmpNew = backGroundBmp;
            }
        }
//Chg 2012/02/09 Z01_h_yamada End <--


        Canvas c = new Canvas(bmpNew);
        Paint paint = new Paint();

//Chg 2011/09/25 Z01_h_yamada Start -->
//        paint.setColor(Color.BLACK);
//--------------------------------------------
        paint.setColor(Color.WHITE);
//Chg 2011/09/25 Z01_h_yamada End <--
//Chg 2011/07/28 Z01thedoanh (自由解像度対応) Start -->
        //paint.setTextSize(30);
// Chg 2011/10/11 Z01kkubo Start --> 課題No.173改修反映
        //paint.setTextSize(activity.getResources().getDimension(R.dimen.Common_Medium_textSize));
        //----------------------------------------------------------------------------------------------------
        // 方面看板などとフォントサイズを合わせるため、MediumからLargeに変更する
        paint.setTextSize(activity.getResources().getDimension(R.dimen.Common_HugeHuge_textSize));
// Chg 2011/10/11 Z01kkubo End <--
//Chg 2011/07/28 Z01thedoanh (自由解像度対応) End <--
        paint.setTypeface(Typeface.DEFAULT_BOLD);
// Add 2011/06/13 sawada Start -->
        paint.setAntiAlias(true);
// Add 2011/06/13 sawada End   <--

        c.drawBitmap(backGroundBmp, 0, 0, paint);

//Chg 2011/10/07 Z01_h_yamada Start -->
//        int fontwidth = 0;
//        if( crossName == null || crossName.length() == 0 ){
//        	crossName = activity.getResources().getString(R.string.intersection_name);
//        }
//      fontwidth = (int) (paint.getTextSize()*crossName.length());
//      c.drawText(crossName, backGroundBmp.getWidth()/2-fontwidth/2, backGroundBmp.getHeight()/2+10, paint);
//--------------------------------------------
        if( crossName == null || crossName.length() == 0 ){
        	crossName = activity.getResources().getString(R.string.intersection_name);
        }

        FontMetrics fontMetrics = paint.getFontMetrics();
        float centerX = backGroundBmp.getWidth() / 2.0f;
        float centerY = backGroundBmp.getHeight() / 2.0f;
		float textWidth = paint.measureText( crossName );

// Add 2011/10/11 Z01kkubo Start --> 課題No.173改修反映
		if (crossName != null && crossName.length() > 0) {
			int fontwidth = 0;
			fontwidth = (int) (paint.getTextSize() * crossName.length());
			// 交差点名称が表示する背景BMP幅より大きい
			if (fontwidth > backGroundBmp.getWidth()) {
				int cutNum = ((fontwidth - (int)(backGroundBmp.getWidth())) / (int)(paint.getTextSize())) + 2;
				crossName = crossName.substring(0, crossName.length()-cutNum) + "...";
				// テキストBMP幅の再調整
				textWidth = paint.measureText( crossName );
			}
		}
// Add 2011/10/11 Z01kkubo End <--

		float baseX = centerX - textWidth / 2;
		float baseY = centerY - (fontMetrics.ascent + fontMetrics.descent) / 2;

		c.drawText( crossName, baseX, baseY, paint);
//Chg 2011/10/07 Z01_h_yamada End <--

        return bmpNew;
    }

    private int crossWidth;
    private int crossHeight;

    public int getCrossNameWidth() {
        //Bitmap backGroundBmp = BitmapFactory.decodeResource(activity.getResources(),R.drawable.txt_cross_name_back);
        //return backGroundBmp.getWidth();
        return crossWidth;
    }
    public int getCrossNameHeight() {
        //Bitmap backGroundBmp = BitmapFactory.decodeResource(activity.getResources(),R.drawable.txt_cross_name_back);
        //return backGroundBmp.getHeight();
        return crossHeight;
    }
//Add 2011/10/01 Z01_h_yamada Start -->
    public int getCrossNamePosX() {
        return back.getLeft() +layoutIntersectionInfo.getLeft();
    }
    public int getCrossNamePosY() {
        return back.getTop() +layoutIntersectionInfo.getTop();
    }
//Add 2011/10/01 Z01_h_yamada End <--

    public  Java_IconInfo getIconInfo() {
        Java_IconInfo iconInfo = new Java_IconInfo();
        NaviRun.GetNaviRunObj().JNI_Java_GetMagMapCompass(iconInfo);
//        setExpandPointAngle(iconInfo);
        return iconInfo;
    }

    /*
    public int[] getCompassBMP(Java_IconInfo iconInfo) {

        Bitmap bmpNew = Bitmap.createBitmap(getCompassBmpWidth(), getCompassBmpHeight(), Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(bmpNew);

        int angle = 0;
        if (null != iconInfo) {
            short sIconInfo = iconInfo.getSIconInfo();
            angle = 360 - sIconInfo;
            if (angle % 360 == 0) {
                angle = 0;
            }
        }

        if(activity.isHeadupCompass()){
            c.drawBitmap(CommonLib.rotate(HeadupCompass, angle), 0, 0, null);
            c.drawBitmap(HeadupCompass, 0, 0, null);
        }else{
            c.drawBitmap(CommonLib.rotate(compass, angle), 0, 0, null);
            c.drawBitmap(compass, 0, 0, null);
        }

        c.save(Canvas.ALL_SAVE_FLAG);
        c.restore();

        int[] pix = new int[getCompassBmpWidth() * getCompassBmpHeight()];
        bmpNew.getPixels(pix, 0, getCompassBmpWidth(), 0, 0, getCompassBmpWidth(), getCompassBmpHeight());

        writeToBMP(HeadupCompass,"HeadupCompass.bmp");
        writeToBMP(bmpNew,"bmpNew.bmp");
        return pix;
   }
   */
    private Bitmap compassBmp = null;
    public Bitmap getCompassBMP(Java_IconInfo iconInfo) {
        int angle = 0;
        short sIconInfo = iconInfo.getSIconInfo();
        angle = 360 - sIconInfo;
        if (angle % 360 == 0) {
            angle = 0;
        }
//Chg 2011/12/14 Z01_h_yamada Start -->
////Chg 2011/09/21 Z01_h_yamada Start -->
////        compassBmp = null;
////        if(activity.isHeadupCompass()){
////        	compassSrcBmp = CommonLib.rotate(HeadupCompass, angle);
////        }else{
////        	compassSrcBmp = CommonLib.rotate(compass, angle);
////        }
////--------------------------------------------
//        Bitmap compassSrcBmp = null;
//
//        compassBmp = null;
//        if(activity.isHeadupCompass()){
//        	compassSrcBmp = CommonLib.rotate(HeadupCompass, angle);
//        }else{
//        	compassSrcBmp = CommonLib.rotate(compass, angle);
//        }
//        int compassSize = activity.getResources().getDimensionPixelSize(R.dimen.ML_CompassButton_Lheight);
//        compassBmp = CommonLib.ReSizeBmpImage( compassSize, compassSize, compassSrcBmp );
//        compassSrcBmp = null;
////Chg 2011/09/21 Z01_h_yamada End <--
//--------------------------------------------
        Bitmap compassSrcBmp = null;

        compassBmp = null;
        if(activity.isHeadupCompass()){
        	compassSrcBmp = HeadupCompass;
        }else{
        	compassSrcBmp = compass;
        }
        int compassSize = activity.getResources().getDimensionPixelSize(R.dimen.ML_CompassButton_Lheight);
        compassBmp = CommonLib.rotateCommpas(compassSrcBmp, compassSize, compassSize, angle);
        compassSrcBmp = null;
//Chg 2011/12/14 Z01_h_yamada End <--

        tmpBmpWidth = compassBmp.getWidth();
        tmpBmpHeight = compassBmp.getHeight();
        return compassBmp;
   }

/*
    public int getCompassBmpWidth() {
        Bitmap backGroundBmp = BitmapFactory.decodeResource(activity.getResources(),R.drawable.compass_headup_off);
        return backGroundBmp.getWidth();
    }
    public int getCompassBmpHeight() {
        Bitmap backGroundBmp = BitmapFactory.decodeResource(activity.getResources(),R.drawable.compass_headup_off);
        return backGroundBmp.getHeight();
    }
*/

    private Bitmap getAngleCompassBmp(Java_IconInfo iconInfo) {
        int angle = 0;
        Bitmap oBmp = null;
        if (null != iconInfo) {
            short sIconInfo = iconInfo.getSIconInfo();
            angle = 360 - sIconInfo;
            if (angle % 360 == 0) {
                angle = 0;
            }
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"angle========="  + angle);
            if(activity.isHeadupCompass()){
                if (HeadupCompass == null) {
                    NaviLog.d(NaviLog.PRINT_LOG_TAG,"HeadupCompass is null");
                } else {
                    NaviLog.d(NaviLog.PRINT_LOG_TAG,"HeadupCompass is not null");
                }
             oBmp = CommonLib.rotate(HeadupCompass, angle);
            }else{
                if (compass == null) {
                    NaviLog.d(NaviLog.PRINT_LOG_TAG,"compass is null");
                } else {
                    NaviLog.d(NaviLog.PRINT_LOG_TAG,"compass is not null");
                }
             oBmp = CommonLib.rotate(compass, angle);
            }
        }
        if (HeadupCompass != null) {
            writeToBMP(HeadupCompass,"HeadupCompass.bmp");
        }
        if (compass != null) {
            writeToBMP(compass,"compass.bmp");
        }
        if (oBmp != null) {
            writeToBMP(oBmp,"new.bmp");
        }
        return oBmp;
    }
    FileOutputStream bitmapWtriter;
    public int iIndex = 0;
    private void writeToBMP(Bitmap b,String bmpfileName) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"-------------------->>>>>>" + activity.getFilesDir().getPath());
        if(iIndex >= 1)
        {
            return;
        }
//      activity.getFilesDir().listFiles()
        //File bitmapFile = new File(activity.getFilesDir().getPath(), bmpfileName+ iIndex+".bmp");
File bitmapFile = new File("//sdcard//zdcpnd//TMP//" + bmpfileName);
        // File bitmapFilenew = new File(this.getFilesDir().getPath(),"b.bmp");

        try {
            bitmapWtriter = new FileOutputStream(bitmapFile);
        // bitmapWtriternew = new FileOutputStream(bitmapFilenew);
        } catch (FileNotFoundException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }
        // // if (bitmapWtriter == null) {
        // // NaviLog.d(NaviLog.PRINT_LOG_TAG,"cccccccccc");
        // // } else {
        // // NaviLog.d(NaviLog.PRINT_LOG_TAG,"dddddddddd");
        // // }
        if (b.compress(Bitmap.CompressFormat.PNG, 100, bitmapWtriter)) {
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"writeToBMP!");
            iIndex++;
        } else {
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"writeToBMP not");
        }
        // if(bnew.compress(Bitmap.CompressFormat.PNG, 100, bitmapWtriternew)) {
        // NaviLog.d("TAG","保存文件成功!new");
        // }
        }


    private int tmpBmpWidth;
    private int tmpBmpHeight;

    public int getCompassBmpWidth() {
//      if (activity.isHeadupCompass()) {
    //      return HeadupCompass.getWidth();
        //} else {
            //return compass.getWidth();
        //}
        return tmpBmpWidth;
    }
    public int getCompassBmpHeight() {
//      if (activity.isHeadupCompass()) {
//          return HeadupCompass.getHeight();
//      } else {
//          return compass.getHeight();
        //}
        return tmpBmpHeight;
    }
}
