/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           Java_KINDDATA.java
 * Description    種別情報設定
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class Java_KINDDATA {
    private int iGuideType;
    private int iKindType;
    private String SKindName;
    private String SDirectionName;

    /**
    * Created on 2010/08/06
    * Title:       getIGuideType
    * Description:  Guideタイプを取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getIGuideType() {
        return iGuideType;
    }
    /**
    * Created on 2010/08/06
    * Title:       getIKindType
    * Description:  種別タイプを取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getIKindType() {
        return iKindType;
    }
    /**
    * Created on 2010/08/06
    * Title:       getSKindName
    * Description:  種別名称
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getSKindName() {
        return SKindName;
    }
    /**
    * Created on 2010/08/06
    * Title:       getSKindName
    * Description:  方面名称
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getSDirectionName() {
        return SDirectionName;
    }

}
