package net.zmap.android.pnd.v2.sensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

public class RawSensorEvent {
	public int accuracy;
	public Sensor sensor;
	public long timestamp;
	public final float[] values = new float[3];

	public RawSensorEvent() {
	}

	public RawSensorEvent(SensorEvent event) {
		accuracy = event.accuracy;
		sensor = event.sensor;
		timestamp = event.timestamp;
		values[0] = event.values[0];
		values[1] = event.values[1];
		values[2] = event.values[2];
	}
}