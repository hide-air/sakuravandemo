package net.zmap.android.pnd.v2.common.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.app.DialogFragment;
//import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.content.Context;
import android.content.DialogInterface;

/**
 * リストダイアログ
 * リスト項目を表示してラジオボタンによる選択を可能とする
 * @author misao.m
 *
 */
public class ListDialogFragment extends DialogFragment {
	private DialogInterface.OnClickListener checkedChangeListener = null;
	private DialogInterface.OnClickListener positiveClickListener = null;
	private DialogInterface.OnClickListener negativeClickListener = null;
	private DialogInterface.OnClickListener deleteClickListener = null;
	
	private String title = "";
	private String[] items = null;
	private int itemNum = -1; 
	
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setIcon(android.R.drawable.ic_menu_more);
		
		if (!TextUtils.isEmpty(this.title)) {
			builder.setTitle(this.title);
		}
		
		if (this.items.length > 0) {
			this.itemNum = 0;
			
			builder.setSingleChoiceItems(this.items, 0, this.checkedChangeListener);
		}
		
		if (deleteClickListener != null)
			builder.setNeutralButton("Delete", deleteClickListener);
		if (this.positiveClickListener != null)
			builder.setPositiveButton("OK", this.positiveClickListener);
		if (this.negativeClickListener != null)
			builder.setNegativeButton("Cancel", this.negativeClickListener);
		
        // Create the AlertDialog object and return it
        return builder.create();
    }
	
	@Override
    public void onDestroy() {
		this.checkedChangeListener = null;
		this.positiveClickListener = null;
		this.negativeClickListener = null;
		this.deleteClickListener = null;
    	super.onDestroy();
    }
	
	/**
	 * タイトルを設定する
	 * @param title タイトル
	 */
	public void setTitle(String title) {
		if (!TextUtils.isEmpty(title)) this.title = title;
		return ;
	}
	
	/**
	 * タイトルを設定する
	 * @param context コンテキスト
	 * @param id タイトルが設定されたリソースID
	 */
	public void setTitle(Context context, int id) {
		if (id != 0) {
			String title = context.getResources().getString(id);
			this.setTitle(title);
		}
		return ;
	}
	
	/**
	 * リスト項目を設定する
	 * @param items リスト項目
	 */
	public void setItems(String[] items) {
		if (items.length > 0) this.items = items;
		return ;
	}
	
	/**
	 * リスト項目を設定する
	 * @param context コンテキスト
	 * @param id リスト項目が設定されたリソースID
	 */
	public void setItems(Context context, int id) {
		if (id != 0) {
			String[] items = context.getResources().getStringArray(id);
			this.setItems(items);
		}
		return ;
	}
	
	/**
	 * 項目選択リスナー
	 * @param listener 項目選択リスナー
	 */
	public void setOnCheckedChangeListener(DialogInterface.OnClickListener listener) {
		this.checkedChangeListener = listener;
		return ;
	}
	
	/**
	 * ポジティブクリックリスナーを設定する
	 * @param listener ポジティブクリックリスナー
	 */
	public void setOnPositiveClickListener(DialogInterface.OnClickListener listener) {
		this.positiveClickListener = listener;
		return ;
	}
	
	/**
	 * ネガティブクリックリスナーを設定する
	 * @param listener ネガティブクリックリスナー
	 */
	public void setOnNegativeClickListener(DialogInterface.OnClickListener listener) {
		this.negativeClickListener = listener;
		return ;
	}
	
	/**
	 * デリートクリックリスナーを設定する
	 * @param listener デリートクリックリスナー
	 */
	public void setOnDeleteClickListener(DialogInterface.OnClickListener listener) {
		this.deleteClickListener = listener;
		return ;
	}
}
