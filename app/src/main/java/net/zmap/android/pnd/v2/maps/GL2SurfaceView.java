/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.zmap.android.pnd.v2.maps;
/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.util.AttributeSet;

import net.zmap.android.pnd.v2.data.NaviRun;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

/**
 * A simple GLSurfaceView sub-class that demonstrate how to perform
 * OpenGL ES 2.0 rendering into a GL Surface. Note the following important
 * details:
 *
 * - The class must use a custom context factory to enable 2.0 rendering.
 *   See ContextFactory class definition below.
 *
 * - The class must use a custom EGLConfigChooser to be able to select
 *   an EGLConfig that supports 2.0. This is done by providing a config
 *   specification to eglChooseConfig() that has the attribute
 *   EGL10.ELG_RENDERABLE_TYPE containing the EGL_OPENGL_ES2_BIT flag
 *   set. See ConfigChooser class definition below.
 *
 * - The class must select the surface's format, then choose an EGLConfig
 *   that matches it exactly (with regards to red/green/blue/alpha channels
 *   bit depths). Failure to do so would result in an EGL_BAD_MATCH error.
 */
class GL2SurfaceView extends GLSurfaceView {
	public GL2SurfaceView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	    init();
	}
    public GL2SurfaceView(Context context) {
        super(context);
        init();
    }

    private void init() {
//        this.getHolder().setFormat(PixelFormat.TRANSLUCENT);
    	setDebugFlags(DEBUG_CHECK_GL_ERROR | DEBUG_LOG_GL_CALLS);
        setEGLContextFactory(new ContextFactory());
        setEGLConfigChooser(new MultisampleConfigChooser());
    }

    private static class ContextFactory implements GLSurfaceView.EGLContextFactory {
        private static int EGL_CONTEXT_CLIENT_VERSION = 0x3098;
        public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig) {
            int[] attrib_list = {EGL_CONTEXT_CLIENT_VERSION, 2, EGL10.EGL_NONE };
            EGLContext context = egl.eglCreateContext(display, eglConfig, EGL10.EGL_NO_CONTEXT, attrib_list);
            return context;
        }
        public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context) {
    		NaviRun.GetNaviRunObj().JNI_CT_GLResourceClear();
            egl.eglDestroyContext(display, context);
        }
    }

    private static class MultisampleConfigChooser implements GLSurfaceView.EGLConfigChooser {
    	public MultisampleConfigChooser() {
        }

        private static int EGL_OPENGL_ES2_BIT = 4;
// MOD.2013.11.19 N.Sasao 機種依存描画不具合対応(移植) START
        //Kyocera URBANO PROGRESSOはMSAA + DepthBufferで描画が乱れるため、depthなしの専用Configを使用する
        public EGLConfig chooseConfigForKYY04(EGL10 egl, EGLDisplay display) {
            int[] configSpec  =
                    {
                            EGL10.EGL_RED_SIZE, 4,
                            EGL10.EGL_GREEN_SIZE, 4,
                            EGL10.EGL_BLUE_SIZE, 4,
                            EGL10.EGL_DEPTH_SIZE, 16,
                            EGL10.EGL_SAMPLE_BUFFERS, 1,
                            EGL10.EGL_SAMPLES, 4,
                            EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
                            EGL10.EGL_NONE
                    };
            /* Get the number of minimally matching EGL configurations
             */
            int[] num_config = new int[1];
            egl.eglChooseConfig(display, configSpec, null, 0, num_config);

            int numConfigs = num_config[0];
        
            /* Allocate then read the array of minimally matching EGL configs
             */
            EGLConfig[] configs = new EGLConfig[numConfigs];
            egl.eglChooseConfig(display, configSpec, configs, numConfigs, num_config);

            /* Now return the "best" one
             */
            return chooseConfig(egl, display, configs);
        }
//ItsmoNaviDriveはバードビューが無いので深度バッファ不要・・・なんだけど、無効にすると正常に描画されない端末あり。
        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
            if(Build.PRODUCT.compareTo("KYY04") == 0)
            {
                return chooseConfigForKYY04(egl,display);
            }

        	int[] configSpec  =
                {
                    EGL10.EGL_RED_SIZE, 4,
                    EGL10.EGL_GREEN_SIZE, 4,
                    EGL10.EGL_BLUE_SIZE, 4,
                    EGL10.EGL_DEPTH_SIZE, 16,
                    EGL10.EGL_SAMPLE_BUFFERS, 1,
                    EGL10.EGL_SAMPLES, 4,
                    EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
                    EGL10.EGL_NONE
                };
            /* Get the number of minimally matching EGL configurations
             */
            int[] num_config = new int[1];
            egl.eglChooseConfig(display, configSpec, null, 0, num_config);

            int numConfigs = num_config[0];

            if (numConfigs <= 0) {

                //for tegra spec
                final int EGL_COVERAGE_BUFFERS_NV = 0x30E0;
                final int EGL_COVERAGE_SAMPLES_NV = 0x30E1;
                configSpec =
                    new int[]{
                        EGL10.EGL_RED_SIZE, 4,
                        EGL10.EGL_GREEN_SIZE, 4,
                        EGL10.EGL_BLUE_SIZE,4,
                        EGL10.EGL_DEPTH_SIZE, 16,
                        EGL_COVERAGE_BUFFERS_NV, 1,
                        EGL_COVERAGE_SAMPLES_NV, 2,
                        EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
                        EGL10.EGL_NONE
                    };
                //NVIDIA専用 Coverage sampleはMSAAより品質が劣る
                egl.eglChooseConfig(display, configSpec, null, 0, num_config);
                numConfigs = num_config[0];

                if (numConfigs <= 0) {
                    configSpec =
                        new int[]{
                            EGL10.EGL_RED_SIZE, 4,
                            EGL10.EGL_GREEN_SIZE, 4,
                            EGL10.EGL_BLUE_SIZE,4,
                            EGL10.EGL_DEPTH_SIZE, 16,
                            EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
                            EGL10.EGL_NONE
                        };
                    egl.eglChooseConfig(display, configSpec, null, 0, num_config);
                    numConfigs = num_config[0];

                    if (numConfigs <= 0) {
                    	throw new IllegalArgumentException("No configs match configSpec");
                    }
                }
            }
// MOD.2013.11.19 N.Sasao 機種依存描画不具合対応(移植)  END
            /* Allocate then read the array of minimally matching EGL configs
             */
            EGLConfig[] configs = new EGLConfig[numConfigs];
            egl.eglChooseConfig(display, configSpec, configs, numConfigs, num_config);

            /* Now return the "best" one
             */
            return chooseConfig(egl, display, configs);
        }


        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display,
                EGLConfig[] configs) {
        	return configs[0];
        }
    }
}