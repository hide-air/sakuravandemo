package net.zmap.android.pnd.v2.inquiry.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;

public class InputText extends ItemView {

	public static String KEY = "key";
	public static String TEL = "tel";
	public String FAVORITES = "favorites";
	private String RECORD_ZERO_COUNT = "0件";
	private JNILong ListCount = new JNILong();
	private int minLimited = 0;
	private int maxLimited = 0;
	private boolean btnEnable = false;
	private static  EditText oText = null;
	private TextView oCountView = null;
//	private boolean runflag = true;
	public TextView getoCountView() {
		return oCountView;
	}

	public void setoCountView(TextView oCountView) {
		this.oCountView = oCountView;
	}

	private static Activity m_oContext = null;
	private Button o_Button = null;
	public Button getO_Button() {
		return o_Button;
	}

	public void setO_Button(Button oButton) {
		o_Button = oButton;
	}

	private String keyType = "";
	private boolean doEnterFlag = false;
	/**false:Add  true : delete*/
	private boolean flag_AddAndDel = false;
	private boolean textChangeFlag = false;

	public InputText(Activity oContext, int wId,Button obtn) {
		super(oContext, wId, R.layout.input_text);
		m_oContext = oContext;
		if (obtn == null) {
			o_Button = (Button)super.findViewById(oContext, R.id.Button_search);
		} else {
			o_Button = obtn;
		}
		init(oContext);

	}


	public InputText(final Activity oContext,Button obtn) {
		super(oContext, 0, R.layout.input_text);
		m_oContext = oContext;
		if (obtn == null) {
			o_Button = (Button)super.findViewById(oContext, R.id.Button_search);
		} else {
			o_Button = obtn;
		}
		init(oContext);
	}

	public int getMinLimited() {
		return minLimited;
	}
	public int getMaxLimited() {
		return maxLimited;
	}
	public void setMinLimited(int minLimited,Button btn) {
		this.minLimited = minLimited;
		o_Button = btn;
	}
	public void setMaxLimited(int maxLimited,Button btn) {
		this.maxLimited = maxLimited;
		o_Button = btn;
	}
	public static boolean isChangeTitle(String str) {
		if (str.equals(m_oContext.getResources().getString(R.string.text_key_input))) {
			return false;
		} else {
			return true;
		}
	}

//	private int changeCount = 0;
//	Thread getCountThread = null;
	private String beforeStr = "";
	private void init(final Context oContext) {

//		getCountThread = new Thread(this);
		oText = (EditText)this.findViewById(oContext, R.id.txtTitle_freeword);
		oText.requestFocus();
		oCountView = (TextView) this.findViewById(oContext, R.id.text_record_count);
		oText.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable oEdit) {
//				System.out.println(getMinLimited());
				checkMinLimited(oEdit.toString().length());

//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"--------->" + oText.getImeActionId() + "---------->" + oText.getInputType());
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"getMaxLimited====" + getMaxLimited());
				if (getMaxLimited() != 0) {
					if (oEdit.toString().length() <= getMaxLimited() && oEdit.toString().length() >= getMinLimited()) {
						o_Button.setEnabled(true);
					} else {
						o_Button.setEnabled(false);
					}
				}

				if (keyType.equals(KEY)) {
					String editString = Html.toHtml(oEdit);
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"editString====" + editString);
					if (!editString.contains("<u>")) {
						String strKana= AppInfo.getKanaList(oEdit.toString());
						if (strKana.length() != oEdit.toString().length()) {
							oText.setText(strKana);
						}
						if (AppInfo.isEmpty(strKana)) {
							oCountView.setText(RECORD_ZERO_COUNT);
						} else {
							//件数を取得する
							getListCount(oEdit.toString());
						}
					}
				} else if (keyType.equals(FAVORITES)) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"textChangeFlag==" + textChangeFlag);
					if (textChangeFlag) {
						textChangeFlag = true;

						o_Button.setEnabled(true);
						checkMinLimited(oEdit.toString().length());
					} else {
						o_Button.setEnabled(false);
					}
				}
			}

			private void checkMinLimited(int recordLen) {
				if (recordLen >= getMinLimited()) {
					o_Button.setEnabled(true);
				} else {
					if (o_Button.isEnabled()) {
						oText.setHint("");
					}
					o_Button.setEnabled(false);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"beforeTextChanged====s=" + s);
				//お気に入り登録名称編集画面で未編集時に[登録]ボタンがグレーダウンしない

				if (keyType.equals(FAVORITES)) {
					beforeStr = s.toString();
				}
			}


			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				setFlag_AddAndDel(false);
				if (count > 1 ||!s.toString().equals(beforeStr)) {
					textChangeFlag = true;
				}
			}});

		oText.setOnEditorActionListener(new OnEditorActionListener(){

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {

//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"onEditorAction======================" + event.getKeyCode());
				if (!doEnterFlag &&keyType.equals(KEY)) {

					getListCount(getText());
				}

				return true;
			}});
	}


	public boolean isFlag_AddAndDel() {
		return flag_AddAndDel;
	}


	public void setFlag_AddAndDel(boolean flagAddAndDel) {
		flag_AddAndDel = flagAddAndDel;
	}


	public boolean isBtnEnable() {
		return btnEnable;
	}


	public void setBtnEnable(boolean btnEnable) {
		this.btnEnable = btnEnable;
	}

	public String getText(){
		return oText.getText().toString();
	}
	public EditText getTextObj(){
		return oText;
	}


	public void setType(String string) {
		keyType = string;

	}
	public int getRecordCount () {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"getRecordCount");
//		if (null == ListCount || ListCount.lcount == 0) {
////			NaviLog.d(NaviLog.PRINT_LOG_TAG,"null == ListCount");
//
//		}
//		getListCount(getText());
		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_SearchList(getText());
		NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetMatchCount(ListCount);
		setListCount(ListCount);
		return (int)ListCount.lcount;
	}

	private boolean doSearchFlag = false;
	private void getListCount(final String pStr) {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"179 pStr InputText====" + pStr);

		doEnterFlag = true;
		o_Button.setEnabled(false);
		if (!doSearchFlag) {
			m_oContext.showDialog(Constants.DIALOG_WAIT);
			android.os.Handler hander = new android.os.Handler();
			hander.postDelayed(new Runnable() {

				@Override
				public void run() {

						doSearchFlag = true;
						NaviRun.GetNaviRunObj().JNI_NE_POIFacility_SearchList(pStr);
						NaviRun.GetNaviRunObj().JNI_NE_POIFacility_GetMatchCount(ListCount);
						setListCount(ListCount);
						doSearchFlag = false;

						if (m_oWarningDialog != null) {
							m_oWarningDialog.cancel();
						}
//					}

				}},2000);
			android.os.Handler handerBoard = new android.os.Handler();
			handerBoard.postDelayed(new Runnable() {

				@Override
				public void run() {
					InputMethodManager imm = (InputMethodManager) m_oContext.getSystemService(INPUT_METHOD_SERVICE);
					if(m_oContext.getWindow().getCurrentFocus()!=null) {
//						imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
						imm.showSoftInput(m_oContext.getWindow().getCurrentFocus(), 0);
					}
				}

			}, 3000);
		}
	}

	public void setListCount(JNILong pListCount) {
		ListCount = pListCount;
		int recodeCount = (int)ListCount.lcount;
		oCountView.setText("" + recodeCount + "件");
		doEnterFlag = false;
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"setListCount do");
		//3000件以上の場合、検索ボタンは使用不可とする
		if (recodeCount > 3000 || recodeCount == 0) {
			o_Button.setEnabled(false);
		} else {
			o_Button.setEnabled(true);
		}
	}

	private Dialog m_oWarningDialog = null;
//	private static Thread waitThread = null;

	public void putWaitDialog(Dialog oWarningDialog) {
		m_oWarningDialog = oWarningDialog;
	}
}
