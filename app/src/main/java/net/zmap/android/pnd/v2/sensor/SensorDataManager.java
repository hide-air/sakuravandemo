/**
 *  @file		SensorDataManager.java
 *  @brief
 *
 *  @attention
 *  @note
 *
 *  @author		Yuki Sawada [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2010-10-13)
 *  @version	$Revision: $ by $Author: $
 */

package net.zmap.android.pnd.v2.sensor;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;					// V2.5 端末情報

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.gps.GPSStatusListener;
import net.zmap.android.pnd.v2.gps.MyLocationListener;
import net.zmap.android.pnd.v2.maps.MapView;

import net.zmap.android.pnd.v2.common.LoggerManager;
import net.zmap.android.pnd.v2.common.services.MonitorAdapter;
import net.zmap.android.pnd.v2.common.services.MonitorListener;
import net.zmap.android.pnd.v2.data.AccSensorData;
import net.zmap.android.pnd.v2.data.GyroSensorData;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.OriSensorData;
import java.util.List;

public class SensorDataManager implements SensorEventListener {
    private static SensorDataManager mSingleton;
    private static SensorManager mSensorManager;

    private static AccelerometerSensor mAccelerometer;
    private static OrientationSensor mOrientation;
    private static GyroscopeSensor mGyroscope;

// Add 2011/03/10 Z01ONOZAWA Start --> 内部情報ログ出力
    private static OrientationSensor mOrientationHard = null;
    private static OrientationSensor mOrientationMagnetic = null;
    private static MonitorAdapter mMonitor;

    private static boolean mExConfigDisableAG = true; // ExConfig.ini読み込みまで停止
    private static boolean back_bNeedSettingDevice = true;
    private static boolean mSensorDisableAcc = false; // センサー無効
    private static boolean mSensorDisableGyro = false; // センサー無効
    private static boolean mSensorDisableOri = false; // センサー無効
    private static boolean mSensorDisableMag = false; // センサー無効
// Add 2011/03/10 Z01ONOZAWA End   <--

// Add 2011/02/22 sawada Start -->
    private float[] mLastOrientation = new float[3]; //傾き補正角度(度)
//Add 2011/04/25 Z01yoneya Start -->
    //傾き補正にジャイロピッチ分を足しこむ用
    private static float[] mGyroAngleDegGainTotal = new float[3]; //傾き補正を求めた後のジャイロ変化量積算(度)
//Add 2011/05/16 Z01yoneya Start -->
    private static final float mGyroAngleDegGainMax = 10.0f; //ピッチ変化分は上限10度とする。センサーが返す角度変化量がおかしくなるため。
//Add 2011/05/16 Z01yoneya End <--
    private static long mGyroGainPassTimeMSec; //前回の変化量取得からの経過時間
    private static int mSamplingNum = 80; //傾き補正積算用の平滑化サンプル数
//Add 2011/04/25 Z01yoneya End <--

//Chg 2011/03/18 Z01yoneya Start-->
//    private static CircularBuffer[] mPastAcceleration = new CircularBuffer[3];
//    private static CircularBuffer[] mPastAngularSpeed = new CircularBuffer[3];
//--------------------------------------------------------------------------------
    private static CircularBuffer3f mPastAcceleration = null;
    private static CircularBuffer3f mPastAngularSpeed = null;
//Chg 2011/03/18 Z01yoneya End <--

//    private Matrix3D mMatrix = new Matrix3D();
//    private final float DEG2RAD = (float)(Math.PI / 180.0);
// Add 2011/02/22 sawada End   <--

// Add 2011/02/23 sawada Start -->
    final static long CAR_STATUS_UNKNOWN = -1;
    final static long CAR_STATUS_GOING = 1;
    final static long CAR_STATUS_STOPPING = 2;
// Add 2011/02/23 sawada End   <--

// Add 2011/02/28 sawada Start -->
    final static long SENSOR_STATUS_ORIENTATION_NOT_SET = 0;
    final static long SENSOR_STATUS_YAW_SET = (1 << 0);
    final static long SENSOR_STATUS_PITCH_SET = (1 << 1);
    final static long SENSOR_STATUS_ROLL_SET = (1 << 2);
// Add 2011/02/28 sawada End   <--

// Add 2011/02/28 sawada Start -->
    private static long mSensorStatus = SENSOR_STATUS_ORIENTATION_NOT_SET;
// Add 2011/02/28 sawada End   <--
// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
    private static long mCarStatus = CAR_STATUS_UNKNOWN;
// Add 2011/03/17 Z01ONOZAWA End   <--

	// V.25
	private static float 	ACC_H1filter_coef = 0.1f;		///< ACC hpf係数(JNIより更新)
	private static float 	ACC_IIRLPFfilter_coef = 0.12f;	///< ACC lpf係数(JNIより更新)
	private static int      Acc_GPS_flag = 0;				// GPS受信状態フラグ
	//private static int      Gyro_GPS_flag = 0;			// GPS受信状態フラグ
	private static int     GPSStopNflag = 0;				// GPS停止時にセンサ内で同期するフラグ
	private static int 	ACC_Gyrosend_flag = 0;				// JNI転送指示フラグ 1:ACC 2:Gyro
	private static int 	start_stop_flag = 0;						// センサアシストあり、なしの抑制
	/* 生データログ用 */
	private static String[]	Acc_sdmp = new String[201];		// Acc 生データ文字列(1秒分)
	private static String[]	Gyro_sdmp = new String[201];	// Gyro 生データ文字列(1秒分)
	private static int      Acc_dmpcnt = 0;					// 1秒分の文字列カウンタ
	private static int      Gyro_dmpcnt = 0;				// 1秒分の文字列カウンタ
	/* 生データログ走行 */
	private static int		DRN_runFlag = 0;				// 1:生データログ走行
	private static long		DRN_CuntTimems = 0;				// 生ログ加算タイマ
	private static long		DRN_runTimems = 0;				// 生ログ走行の周期時間[ms]
	//private static long		DRN_GPSTimems = 0;			// GPS間隔[ms]
	private static String   DRN_GPSTimesenddata = null;		// 生ログ用のGPS データ
	private static String   DRN_senddata = null;			// 生ログ用のGPS データ
	private static int     On1 = 0;
	private static int     Acc_Tcnt = 0;
	private static int     Gyro_Tcnt = 0;
	private static int 	gps_cntflag = 0;

//Add 2011/03/18 Z01yoneya Start-->
//Chg 2011/04/21 Z01yoneya Start -->
//	final static long SENSOR_DATA_SAMPLING_TIME_SEC = 1;	//センサー値を判定に使う時間間隔(秒)
//--------------------------------------------
    final static long SENSOR_DATA_SAMPLING_TIME_SEC = 3; //センサー値を判定に使う時間間隔(秒)
//Chg 2011/04/21 Z01yoneya End <--

    final static long SENSOR_DATA_SAMPLING_MIN_NUM = 4; //センサー値を判定に使う時間間隔あたりのデータ数(
    final static float NEAR_GRAVITY_DIFF = (float)(SensorManager.GRAVITY_EARTH * 0.03); //加速度ベクトルの大きさの1G近似範囲(3%)
//Del 2011/09/25 Z01YUMISAKI Start -->
//    final static float ACCEL_STOP_VALUE_MAX =   0.1f;   //停止中と判断する加速度のしきい値(走行ログより)
//Del 2011/09/25 Z01YUMISAKI End <--

//Chg 2011/09/25 Z01YUMISAKI Start -->
////Chg 2011/04/21 Z01yoneya Start -->
////   	final static float ACCEL_STOP_DIFF_MAX	=	0.2f;	//停止中と判断する加速度値の前回との差(机上では0.1だったが修正、要資料)
////   	final static float GYRO_STOP_DIFF_MAX	=	2.0f;	//停止中と判断するジャイロ値の前回との差
////--------------------------------------------
//   	final static float ACCEL_STOP_DIFF_MAX	=	0.3f;	//停止中と判断する加速度値の前回との差(机上では0.1だったが修正、要資料)
//   	final static float GYRO_STOP_DIFF_MAX	=	2.0f;	//停止中と判断するジャイロ値の前回との差
//   	final static float GYRO_STOP_FOR_CALC_ROTATE_MATRIX	=	0.1f;//加速度から傾き角度を求めるための最低ライン
//Chg 2011/04/21 Z01yoneya End <--
//------------------------------------------------------------------------------------------------------------------------------------
    final static float STANDSTILL_ACCEL_DIFF_FLUCTUATION = 0.3f; //静止中の加速度センサー出力変化許容値
    final static float STANDSTILL_GYRO_DIFF_FLUCTUATION = 2.0f; //静止中のジャイロスコープ出力変化許容値
    final static float STANDSTILL_GYRO_ABS_FLUCTUATION = 0.1f; //静止中のジャイロスコープ出力変動許容絶対値
    final static float CARSTOP_ACCEL_DIFF_FLUCTUATION = 0.4f; //停車中の加速度センサー出力変化許容値
    final static float CARSTOP_GYRO_DIFF_FLUCTUATION = 2.0f; //停車中のジャイロスコープ出力変化許容値
    final static float CARSTOP_GYRO_ABS_FLUCTUATION = 0.4f; //停車中のジャイロスコープ出力変動許容絶対値
//Chg 2011/09/25 Z01YUMISAKI End <--

//Add 2011/03/18 Z01yoneya End <--
// Add 2011/05/10 Z01ONOZAWA Start --> 速度が理論値より大きな値を示すので、調整してみる
    private static float inaccurately_ratio = 0.0f;
// Add 2011/05/10 Z01ONOZAWA End   <--

//Add 2011/09/08 Z01yoneya Start -->
//    private static final int SENSOR_NAVI_OFF = 0;
//    private static final int SENSOR_NAVI_ON = 1;

//Add 2011/09/08 Z01yoneya End <--

    public SensorDataManager() {
        mSingleton = this;
    }

    /*
     * 初期化処理
     */
    public static void init(Activity activity) {
        mSingleton = new SensorDataManager();

        mSensorManager = (SensorManager)activity.getSystemService(Context.SENSOR_SERVICE);

        Sensor accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mAccelerometer = new AccelerometerSensor(accelerometer);

        Sensor gyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mGyroscope = new GyroscopeSensor(gyroscope);

        mOrientation = new OrientationSensor(OrientationSensor.FROM_ACCELEROMETER);
//
// Add 2011/03/10 Z01ONOZAWA Start --> 内部情報ログ出力
        mMonitor = MonitorAdapter.getInst();
        if (mMonitor != null) {
            mMonitor.registListener(MonitorAdapter.LISTENER_SENSOR, mListener);

            Sensor orientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
            mOrientationHard = new OrientationSensor(OrientationSensor.FROM_ORIENTATION, orientation);

            Sensor magnetic = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            mOrientationMagnetic = new OrientationSensor(OrientationSensor.FROM_ACCELEROMETER_AND_MAGNETIC_FIELD, magnetic);

        }
// Add 2011/03/10 Z01ONOZAWA End   <--
//Add 2011/04/25 Z01yoneya Start -->
        mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_0_AZ] = 0.0f;
        mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH] = 0.0f;
        mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_2_ROLL] = 0.0f;
        mGyroGainPassTimeMSec = 0;
//Add 2011/04/25 Z01yoneya End <--

        mPastAcceleration = new CircularBuffer3f();
        mPastAngularSpeed = new CircularBuffer3f();
        if (mAccelerometer != null) {
        	mAccelerometer.Acc_1SectimeCnt = 0;
        }
        if (mGyroscope != null) {
        	mGyroscope.Gyro_1SectimeCnt = 0;
        }
        NoiseCut_IIRLPF_Rst();
        LoggerManager.Navi_setlogpath();

//Add 2011/09/09 Z01yoneya Start -->
        clear();
//Add 2011/09/09 Z01yoneya End <--
    }

//Add 2011/09/06 Z01yoneya Start -->
    /*
     * 終了処理
     */
    public static void destroy() {
    	//NaviLog.v("TestWata ","destroy().......................Senser...............");
        stopSensorReceive();
    }

//Add 2011/09/06 Z01yoneya End <--
    // V2.5 ではProfileをサーバー送られくるデータを使う場合があるので、それまでセンサ停止にする
    // その解除をこのstart_start()で行う
    public static void start_start() {
    	start_stop_flag = 0;
    	mExConfigDisableAG = false;
    	start(back_bNeedSettingDevice);
    }
//Chg 2011/09/06 Z01yoneya Start -->
//    public static void start() {
//------------------------------------------------------------------------------------------------------------------------------------
    /**
     * センサー処理を開始する
     *
     * @param bNeedSettingDevice
     *            　ハードウェアのデバイスリスナーの設定要否フラグ
     */
    public static void start(boolean bNeedSettingDevice) {
    	int n = 0;
    	if(start_stop_flag != 0) return;

    	// 起動時センサアシストしないでは falseになる
    	back_bNeedSettingDevice = bNeedSettingDevice;
//   if( bNeedSettingDevice)
//  NaviLog.v("TestWata ","start().......................TRUE...............");
//   else
//  NaviLog.v("TestWata ","start()......................FALSE................");
    	if(mExConfigDisableAG) return;
    	start_stop_flag = 1;
    	List<Sensor> m_sensorlist;
        // V2.5ではセンサは常に動作、メニュー設定はVP内で使う、使わない判断とする
        //エンジンへの設定
        //if (NaviRun.isNaviRunInitial()) {
        //    NaviRun obj = NaviRun.GetNaviRunObj();
        //    if (obj != null) {
        //        obj.JNI_VP_SetEnableSensorNaviFlag(SENSOR_NAVI_ON);
        //    }
        //}

        //デバイスリスナーの設定
        //PNDではログ走行とGPS/センサー走行の２つのモードがある
        //ログ走行ではリスナー登録不要。GPS/センサー走行の時だけセンサー受信を開始する
        if (bNeedSettingDevice) {
            startSensorReceive();
        }
    	// V2.5
        if(On1 == 0)
        {
        	On1 = 1;
        	for(int i=0;i<201;i++) {
        		Acc_sdmp[i] = "";							///< NULL初期化
        	}
        	for(int i=0;i<201;i++) {
        		Gyro_sdmp[i] = "";							///< NULL初期化
        	}
	        /* DR生データログファイルをｵｰﾌﾟﾝする */
	        /* 生ログ走行のReadもしくは生ログ作成のWriteでOpenする */
	        LoggerManager.openRW_NaviLogFile();
        	if(LoggerManager.isExistLOGNModeFile() == true) {
        		DRN_runFlag = 1;		/* 生ログ走行ON */
        	}
	        else if(LoggerManager.DRN_Get_WriteFlag() == 1) {
	        	/* ｾﾝｻスペックをﾛｸﾞ先頭に書き込む */
	        	/* Gyro/ACC ｾﾝｻ */
	        	m_sensorlist =  mSensorManager.getSensorList(Sensor.TYPE_ALL);
	        	if(!m_sensorlist.isEmpty()){
	        		// 端末情報(DRn どのバージョンで作られたログか残す印)
	        		Acc_sdmp[0] = String.format("Dev_tty(DR1):: BOARD=%s, BRAND=%s, PRODUCT=%s",Build.BOARD,Build.BRAND,Build.PRODUCT);
	        		n=1;
	        		float maxrange;
	        		float power;
	        		int resolution;
	        		//int mindelay;
	        		Sensor snsr;
	        		String buf;
	        		for(int i=0;i<m_sensorlist.size();i++) {
	        			buf="";
	        			snsr = m_sensorlist.get(i);
//	        			NaviLog.v("TestWata", "getType="+snsr.getType());
	        			if(snsr.getType() == Sensor.TYPE_GYROSCOPE) {
	        				buf = "Dev_Gyro::";
	        			}
	        			if(snsr.getType() == Sensor.TYPE_ACCELEROMETER) {
	        				buf = "Dev_Acc ::";
	        			}
	        			if(!buf.equals("")) {
	        				/* Gyro/ACC最大ﾚﾝｼﾞを取得 */
	        				maxrange = snsr.getMaximumRange();
	        				/* Gyro/ACC電流[mA]を取得 */
	        				power = snsr.getPower();
	        				/* Gyro/ACC解像度を取得 (1/nn で表示)*/
	        				resolution = (int)(1.0/snsr.getResolution());
	        				/* Gyro/ACC最小ディレイを取得 */
	        				//mindelay = 20;
	        				Acc_sdmp[n] = String.format("%s MaxRange=%.1f,Power=%.1f[mA],Resolution=1/%d",buf,maxrange,power,resolution);
	        				n++;
	        			}
	        		}
	        	}
	        	/* 実機用に生データをログする */
	    		if(n!=0) {
	    			LoggerManager.writeAcc_InterNaviLogFile(Acc_sdmp,0);
	            	for(int i=0;i<201;i++) {
	            		Acc_sdmp[i] = "";							///< NULL初期化
	            	}
	    		}
	        }
        }

    }

    /*
     * センサー受信開始
     */
    private static void startSensorReceive() {

//        NaviLog.d("", "startSensorReceive()");

//Chg 2011/09/06 Z01yoneya End <--

        if (mAccelerometer != null) {
            mSensorManager.registerListener(mSingleton, mAccelerometer.sensor, SensorManager.SENSOR_DELAY_GAME);
        }
// Del 2011/02/22 sawada Start -->
//		if (mOrientation != null) {
//			mSensorManager.registerListener(mSingleton, mOrientation, SensorManager.SENSOR_DELAY_NORMAL);
//		}
// Del 2011/02/22 sawada End   <--
        if (mGyroscope != null) {
            mSensorManager.registerListener(mSingleton, mGyroscope.sensor, SensorManager.SENSOR_DELAY_GAME);
        }

// Add 2011/03/10 Z01ONOZAWA Start --> 内部情報ログ出力
        if ((mOrientationHard != null) && (mOrientationHard.sensor != null)) {
            mSensorManager.registerListener(mSingleton, mOrientationHard.sensor, SensorManager.SENSOR_DELAY_NORMAL);
        }

        if ((mOrientationMagnetic != null) && (mOrientationMagnetic.sensor != null)) {
            mSensorManager.registerListener(mSingleton, mOrientationMagnetic.sensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
// Add 2011/03/10 Z01ONOZAWA End   <--

    }

//Chg 2011/09/06 Z01yoneya Start -->
//    public static void stop() {
//------------------------------------------------------------------------------------------------------------------------------------
    /**
     * センサー処理を停止する
     *
     * @param bNeedSettingDevice
     *            　ハードウェアのデバイスリスナーの設定要否フラグ
     */
    public static void stop(boolean bNeedSettingDevice) {
    	start_stop_flag = 0;		// このstopはv2.5ではセンサアシストしないでもここにこない
    	// V2.5 メニュー反映は"SenserNavigationManager"より戻す
        //if (NaviRun.isNaviRunInitial()) {
        //    NaviRun obj = NaviRun.GetNaviRunObj();
        //    if (obj != null) {
        //        obj.JNI_VP_SetEnableSensorNaviFlag(SENSOR_NAVI_OFF);
        //    }
        //}

        if (bNeedSettingDevice) {
            stopSensorReceive();
        }

        clear();
    }

    /*
     * センサー受信終了
     */
    private static void stopSensorReceive() {
//Chg 2011/09/06 Z01yoneya End <--
//        NaviLog.d("", "stopSensorReceive()");
        mSensorManager.unregisterListener(mSingleton);
        /* V2.5 DR生データログファイルをｸﾛｰｽﾞする */
        LoggerManager.close_NaviLogFile();
    }

//Add 2011/09/09 Z01yoneya Start -->
    //システム設定の自律航法有効・無効フラグを取得する
    public static boolean getSystemSettingEnableSensorFlag() {
        if (NaviRun.isNaviRunInitial()) {
            NaviRun obj = NaviRun.GetNaviRunObj();
            if (obj != null) {
//                NaviLog.d("", "getSystemSettingEnableSensorFlag()");
                return obj.JNI_VP_GetEnableSensorNaviFlag() != 0;
            }
        }
        return false;
    }

//Add 2011/09/09 Z01yoneya End <--

//Add 2011/09/06 Z01yoneya Start -->
    /*
     * センサーバッファ情報や補正情報をクリアする。
     */
    synchronized private static void clear() {
        if (mPastAngularSpeed == null) {
            mPastAngularSpeed = new CircularBuffer3f();
        } else {
            mPastAngularSpeed.clear();
        }

        if (mPastAcceleration == null) {
            mPastAcceleration = new CircularBuffer3f();
        } else {
            mPastAcceleration.clear();
        }

        mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_0_AZ] = 0.0f;
        mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH] = 0.0f;
        mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_2_ROLL] = 0.0f;
        mGyroGainPassTimeMSec = 0;

        if (mAccelerometer != null) {
            mAccelerometer.clearBuffer();
        }
        if (mGyroscope != null) {
            mGyroscope.clearBuffer();
        }
        if (mOrientation != null) {
            mOrientation.clearBuffer();
        }
        if (mOrientationHard != null) {
            mOrientationHard.clearBuffer();
        }
        if (mOrientationMagnetic != null) {
            mOrientationMagnetic.clearBuffer();
        }

        //補正情報クリア
        mSensorStatus = SENSOR_STATUS_ORIENTATION_NOT_SET;

//        mLastOrientation[0] = 0.0f;
//        mLastOrientation[1] = 0.0f;
//        mLastOrientation[2] = 0.0f;
    }

//Add 2011/09/06 Z01yoneya End <--

// Add 2011/03/29 Z01ONOZAWA Start --> クレードルON/OFFによるセンサー制御
    public static void onCarMountStatusChange(boolean bCarMount) {
        if (bCarMount) {// クレードルON
//			JNI_VP_EnableSensor(Constants.ON);
            mSensorDisableAcc = false;
            mSensorDisableGyro = false;
            mSensorDisableOri = false;
            mSensorDisableMag = false;
        } else {// クレードルOFF
//			JNI_VP_EnableSensor(Constants.OFF);
            mSensorDisableAcc = true;
            mSensorDisableGyro = true;
            mSensorDisableOri = true;
            mSensorDisableMag = true;
        }
    }

// Add 2011/03/29 Z01ONOZAWA End   <--
// Add 2011/02/25 sawada Start -->
    private static boolean mPrevGoing = false;
    private static boolean mPrevStopping = false;

// Add 2011/02/25 sawada End   <--

//Add 2011/09/06 Z01yoneya Start -->
    /*
     * ハードウェアが自律航法のためのセンサーを持つか返す。
     * trueならセンサー有り。init()関数以降に正しい値を返します。
     */
    public static boolean isExistSensor() {
        return (mGyroscope.sensor != null) && (mAccelerometer.sensor != null);
    }

//Add 2011/09/06 Z01yoneya End <--

//Add 2011/04/26 Z01yoneya Start -->
//傾き補正角度計算関数
    private void updateDeviceRotateAngle(RawSensorEvent event, boolean bUpdateYaw) {
        mOrientation.onSensorChanged(event);
        if (bUpdateYaw == true) {
//	   		Log.i("", String.format("yoneya angle onAcc isCarStopping ave num=%d roll=%.2f, pitch=%.2f yaw=%.2f inputAcc(%3.2f, %3.2f, %3.2f)",
//			       					mPastAcceleration.getDataNum(SENSOR_DATA_SAMPLING_TIME_SEC),
//			       					mOrientation.orientation[2],
//			       					mOrientation.orientation[1],
//			       					mOrientation.orientation[0],
//			       					event.values[0],
//			       					event.values[1],
//			       					event.values[2]));
        }

        if (bUpdateYaw) {
            mLastOrientation[0] = mOrientation.orientation[0];
        } else {
            mLastOrientation[0] = 0.0f;
            mLastOrientation[1] = mOrientation.orientation[1];
            mLastOrientation[2] = mOrientation.orientation[2];
        }

//Add 2011/04/25 Z01yoneya Start -->
        mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_0_AZ] = 0.0f;
        mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH] = 0.0f;
        mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_2_ROLL] = 0.0f;
//Add 2011/04/25 Z01yoneya End <--

        // 傾きが変わったので回転行列を作る
        //mMatrix.invert(mLastOrientation[0] * DEG2RAD
        //              , mLastOrientation[2] * DEG2RAD
        //              , mLastOrientation[1] * DEG2RAD);

        //mAccelerometer.setMatrix(mMatrix);
        //mGyroscope.setMatrix(mMatrix);

        if (bUpdateYaw) {
            mSensorStatus |= SENSOR_STATUS_YAW_SET;
        }
        mSensorStatus |= SENSOR_STATUS_PITCH_SET;
        mSensorStatus |= SENSOR_STATUS_ROLL_SET;
    }

//Add 2011/04/26 Z01yoneya End <--

//Add 2011/04/13 Z01yoneya Start-->
    //センサー内
    //加速度センサーのイベント生値は下記のとおりに格納される
    //[0] : 重力方向が正
    //[1] : 画面右方向が正
    //[2] : 画面奥方向が正

    //ジャイロセンサーのイベント生値は下記のとおりに格納される
    //[0] : azimath(左回転が正）
    //[1] : pitch(奥に倒すと正)
    //[2] : roll(左に傾けると正）

    //VP内部
    //加速度センサー値は下記の通り
    //X :  画面右方向が正
    //Y :  画面奥方向が正
    //Z :  重力方向が正

    //ジャイロセンサー値は下記の通り
    //X :  pitch(奥に倒すと正)
    //Y :  roll(左に傾けると正）
    //Z :  azimath(左回転が正）
//Add 2011/04/13 Z01yoneya End <--

    // ACC 生データ ノイズカット処理
    // in  [0]:X軸 [1]:Y軸 [2]:Z軸
    private void NoiseCut_ACC(float in[],float out[]) {
    	float   hpf_out;
    	int cnt;
    	// Hpf:ハイパスフィルタ (Lowノイズを除去している)
    	for(cnt=0;cnt<3;cnt++)
    	{
    		hpf_out = in[cnt] * ACC_H1filter_coef + mAccelerometer.delay_out[cnt] * (1.0f - ACC_H1filter_coef);
    		mAccelerometer.delay_out[cnt] = hpf_out;
    		out[cnt] = hpf_out;
    	}
    }
    // ACC IIR LPF 中間ﾊﾞｯﾌｧリセット
    private static void NoiseCut_IIRLPF_Rst() {
    	int cnt;
    	// LPF用中間ﾊﾞｯﾌｧリセット
    	for(cnt=0;cnt<3;cnt++)
    	{
    		mAccelerometer.Acc_Integ1[cnt] = 0.0f;
    		mAccelerometer.Acc_Integ2[cnt] = 0.0f;
    	}
    }
    // ACC IIR LPF
    private void NoiseCut_IIRLPF(float in[],float out[]) {
    	int cnt;
    	// LPF
    	for(cnt=0;cnt<3;cnt++)
    	{
    		mAccelerometer.Acc_Integ2[cnt] += ((mAccelerometer.Acc_Integ1[cnt] - mAccelerometer.Acc_Integ2[cnt]) * ACC_IIRLPFfilter_coef);
    		mAccelerometer.Acc_Integ1[cnt] += ((in[cnt] - mAccelerometer.Acc_Integ1[cnt]) * ACC_IIRLPFfilter_coef);
    		out[cnt] = mAccelerometer.Acc_Integ2[cnt];
    	}
    }


    public void onAccelSensorChanged(RawSensorEvent event) {
    	//double acc_gravity;
		//float fAccCut[] = new float[3];		///< Acc一時用(ノイズカット後)
		float facc[] = new float[3];				///< 生Acc
		//float fxacc2,fyacc2,fzacc2;
       // int gps_flag = MyLocationListener.get_GPSchg_flag();
        //int send_flag = 0;
    	long twork=0;			///< タイムスタンプ経過ms

    	// GPS停止中
    	if(GPSStopNflag == 1) {
    			// GPS停止中にイベント発生あり
    			if (MyLocationListener.get_GPSLocationEvent() == 1) {
    				// GPS転送停止を解除
    				MyLocationListener.set_GPSLocation(0);
    			}
    	}
    	// GPS更新カウンタ取得
    	gps_cntflag =  MyLocationListener.get_GPSchg_flag();

    	// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
        if (mSensorDisableAcc) return;
// Add 2011/03/17 Z01ONOZAWA End   <--
// Add 2011/02/24 sawada Start -->
        mAccelerometer.onSensorChanged(event);

        AccSensorData data = new AccSensorData();
// Add 2011/02/28 sawada Start -->
        mSensorStatus = SENSOR_STATUS_PITCH_SET;
        mSensorStatus |= SENSOR_STATUS_ROLL_SET;

        data.status = mSensorStatus;
// Add 2011/02/28 sawada End   <--
        data.accuracy = (short)event.accuracy;
// Chg 2011/03/01 sawada Start -->
//    	data.timestamp = event.timestamp;
        data.timestamp = System.currentTimeMillis();
// Chg 2011/03/01 sawada End   <--

        //ログにセンサー値のまま出力する
        data.acceleroX = event.values[AccelerometerSensor.ACC_DIR_0_DOWN];
        data.acceleroY = event.values[AccelerometerSensor.ACC_DIR_1_RIGHT];
        data.acceleroZ = event.values[AccelerometerSensor.ACC_DIR_2_AHEAD];
        //LoggerManager.writeLog(data);

    	/* 生ログ走行 */
        if((DRN_runFlag & 0x0001) == 1) {
    		if(DRN_runTimems == 0) {
    			DRN_runTimems = LoggerManager.DRN_ReadFile();	/* 1ブロックﾃﾞｰﾀを生ﾃﾞｰﾀファイルから読込む */
    			//NaviLog.v("TestWata", "[Sensor] ReadFile() ---Time="+DRN_runTimems);
    			if(DRN_runTimems == 0) {
    				DRN_runFlag |= 0x0002;			/* wait　*/
    			}
    			else {
    				DRN_CuntTimems = 0; 			/* 加算カウントクリア */
    				DRN_runFlag = 1;				/* wait解除 */
    			}
    			/* -1が戻った場合、そのままになり、走行停止になる */
    		}
        }
    	///< 開始ms設定
    	mAccelerometer.Acctimestamp_Start = System.currentTimeMillis();
    	if(mAccelerometer.Acc_1SectimeCnt == 0)
    	{
    		Acc_dmpcnt = 0;										///< 生データログ文字配列カウンタ
    		Acc_sdmp[Acc_dmpcnt] = "";							///< NULL初期化
    		if(mAccelerometer.Acctimestamp_End != 0)			///< 前回タイムスタンプあり
    		{
    			twork = mAccelerometer.Acctimestamp_Start - mAccelerometer.Acctimestamp_End;
    		}
    	}
    	else {
    		twork = mAccelerometer.Acctimestamp_Start - mAccelerometer.Acctimestamp_End;
    	}
		DRN_CuntTimems += twork;									/* 生ログ用 */
    	mAccelerometer.Acctimestamp_End = mAccelerometer.Acctimestamp_Start;

        /* 生ログ走行しない場合 */
        if(DRN_runFlag == 0) {
	        facc[0] = data.acceleroX;
	        facc[1] = data.acceleroY;
	        facc[2] = data.acceleroZ;
/*
			// Lpf:ローパスフィルタ
			NoiseCut_IIRLPF(facc, mAccelerometer.Acc_lpf );
			// Hpf:ハイパスフィルタ
			NoiseCut_ACC(mAccelerometer.Acc_lpf,fAccCut);
			fxacc2 = fAccCut[0] * fAccCut[0];
			fyacc2 = fAccCut[1] * fAccCut[1];
			fzacc2 = fAccCut[2] * fAccCut[2];
	        acc_gravity = Math.sqrt(fxacc2+fyacc2+fzacc2);
 NaviLog.d("wata ","acc_gravity,"+acc_gravity +String.format(",%02d,%d,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f",Acc_dmpcnt,twork,facc[0],facc[1],facc[2]
 ,mAccelerometer.Acc_lpf[0],mAccelerometer.Acc_lpf[1],mAccelerometer.Acc_lpf[2],fAccCut[0],fAccCut[1],fAccCut[2]));
 **/
			/* 実機用に生データをログする */
			if(twork != 0)
        	{
				mAccelerometer.Acc_1SectimeCnt += twork;					///< 時間差を加算する
        		//DRN_GPSTimems += twork;										///< GPS停止判断用
        		if(LoggerManager.DRN_Get_WriteFlag() == 1)
				{
					Acc_sdmp[Acc_dmpcnt] = String.format("%02d,A,%d,%.6f,%.6f,%.6f",Acc_dmpcnt,twork,facc[0],facc[1],facc[2]);
					mAccelerometer.acc_t[Acc_dmpcnt] = twork;
					mAccelerometer.acc_d[0][Acc_dmpcnt] = facc[0];	//重力方向が正
					mAccelerometer.acc_d[1][Acc_dmpcnt] = facc[1];	//画面右方向が正
					mAccelerometer.acc_d[2][Acc_dmpcnt] = facc[2];	//画面奥方向が正

					if(Acc_dmpcnt < 198) /* CarInfo分１つ空ける */
					{
						Acc_dmpcnt++;
					}
					Acc_sdmp[Acc_dmpcnt] = "";							///< NULL初期化
				}
        	}
			/* GPS受信が開始されれば、GPS flagの変化でデータ転送を行う */
			/* ACC周期は、500ms */
			//NaviLog.d("TestWata ","cnt="+Acc_GPS_flag +" cntflag="+gps_cntflag);
			if((Acc_GPS_flag != gps_cntflag) &&  (ACC_Gyrosend_flag  == 0)){
				ACC_Gyrosend_flag = 1;
				GPSStopNflag = 0;
				//DRN_GPSTimems = 0;
			}
			else if(gps_cntflag == 0){
	       		if(mAccelerometer.Acc_1SectimeCnt > 1000) {
	       			ACC_Gyrosend_flag = 1;
	       		}
			}
			else {
				/* GPS onLocationChanged 停止 */
				/* 最初の停止は時間を+300ms見る */
	       		if((GPSStopNflag == 0) && (Acc_dmpcnt > 60) && (mAccelerometer.Acc_1SectimeCnt > 1300))  {
	       			// GPS転送停止要求
	       			MyLocationListener.set_GPSLocation(1);
	       			ACC_Gyrosend_flag = 1;
	       		}
	       		if((GPSStopNflag == 1) && (mAccelerometer.Acc_1SectimeCnt > 1000))  {
	       			ACC_Gyrosend_flag = 1;
	       		}
	       		if(ACC_Gyrosend_flag == 1) {
	       			String senddata = null;
					senddata = String.format("$GPSstop,%.6f",MyLocationListener.get_gps_Latitude())
						       +String.format(",%.6f",MyLocationListener.get_gps_Longitude())
						       +String.format(",%.1f",MyLocationListener.get_gps_Bearing())     // 方位
						        +String.format(",%d",mAccelerometer.Acc_1SectimeCnt)     // 間隔[ms]
						       +String.format(",%.1f",MyLocationListener.get_gps_Altitude())    // 標高
						       +String.format(",%.1f",MyLocationListener.get_gps_Accuracy()) // 総合的な精度の指針(Hdop,Vdopではない)
						       +String.format(",%d",MyLocationListener.get_gps_iFixCount())  // 測位使用GPSの数
						       +String.format(",%.1f",MyLocationListener.get_gps_snrAverageUsedInFix())  // 測位使用GPSのSN比率の平均値
						       +String.format(",%.1f",MyLocationListener.get_gps_Speed())+"\n";
					//NaviLog.v("wata", senddata);
			        // 実機用に生データをログする
			        LoggerManager.writeNMEA_InterNaviLogFile(senddata,gps_cntflag);
	       			//DRN_GPSTimems = 0;
	       			//GPSStopNflag = 2;  // Gyroに通知
	       			GPSStopNflag = 1;  // GPS Stop継続

	       		}
			}

        } else if((DRN_runFlag & 0x00FF) == 1) {
        	/* 生ログ走行  */
        	/* ログ周期時間経過(ACCタイマ使用) */
        	if(DRN_runTimems < DRN_CuntTimems) {
        		/* GPS NMEAを送る */
        		/* 生ログからGPS NMEAを取得する */
        		DRN_GPSTimesenddata = LoggerManager.DRN_Get_GPSTime();
        		DRN_senddata = LoggerManager.DRN_Get_GPS();
        		//MyLocation から送る（3.GPS受信ありの場合のみ）
        		MyLocationListener.GPS_DRLogSend(DRN_GPSTimesenddata,DRN_senddata);
       		// V2.5 では次の３パターンとなる
        		// 1.GPS受信なし(起動後GPSを受信しない状態)
        		//   NULL が戻ります
        		// 2.GPS受信なし(起動後、一度GPS受信して、その後GPS受信が途絶えた場合)
        		//   トンネルなどではGPSイベントが入らないと "$GPSstop, ..."が戻ります
        		// 3.GPS受信あり
        		//  "$GPSmove, ..." が戻る
        		// "$GPSmove,35.622677,139.778430,339.9,1000,43.0,5.0,11,25.1,5.2"

        		//NaviLog.v("TestWata", "[Sensor] DRN_Get_GPS() ---NMEA="+DRN_senddata);
        		// ここでDRへ渡すとファイルアクセス分のタイミングずれが起きるのでACCへ移動する
        		/* ACCデータ */
        		int gcnt =  LoggerManager.DRN_Get_AccCnt();
        		//NaviLog.v("TestWata", "[Sensor] DRN_Get_AccCnt() ="+gcnt);
        		if(gcnt > 100) {
        			gcnt = 100;
        		}
        		for(int icnt = 0; icnt <gcnt;icnt++) {
                	//生ログセンサー値を設定する
        			mAccelerometer.acc_d[0][icnt] = LoggerManager.DRN_Get_AccX(icnt);
        			mAccelerometer.acc_d[1][icnt] = LoggerManager.DRN_Get_AccY(icnt);
        			mAccelerometer.acc_d[2][icnt] = LoggerManager.DRN_Get_AccZ(icnt);
            		mAccelerometer.acc_t[icnt] = LoggerManager.DRN_Get_AccnmTime(icnt);
            		/* 1ｲﾍﾞﾝﾄ分の加速度ﾃﾞｰﾀ処理を行う */
            		//Acc_1Proc(facc,twork);
        		}
        		Acc_dmpcnt = gcnt;
        		LoggerManager.DRN_GetEnd_Acc();		/* 取得完了 */
        		/* Gyroデータ */
        		gcnt =  LoggerManager.DRN_Get_GyroCnt();
        		//NaviLog.v("TestWata", "[Sensor] DRN_Get_GyroCnt() ="+gcnt);
        		if(gcnt > 100) {
        			gcnt = 100;
        		}
        		for(int icnt = 0; icnt < gcnt;icnt++) {
        			//生ログセンサー値を設定する
        			mGyroscope.gyro_d[0][icnt] = LoggerManager.DRN_Get_GyroX(icnt);
        			mGyroscope.gyro_d[1][icnt] = LoggerManager.DRN_Get_GyroY(icnt);
        			mGyroscope.gyro_d[2][icnt] = LoggerManager.DRN_Get_GyroZ(icnt);
        			mGyroscope.gyro_t[icnt] = LoggerManager.DRN_Get_GyronmTime(icnt);
        			/* 1ｲﾍﾞﾝﾄ分の積分、統計 */
        			//Gyro_1Proc(fXYZ,twork);
        		}
        		Gyro_dmpcnt = gcnt;
        		LoggerManager.DRN_GetEnd_Gyro();		/* 取得完了 */

        		//NaviLog.v("TestWata", "[Sensor] DRN_Get_AccCnt() ---Cnt="+LoggerManager.DRN_Get_AccCnt());
        		DRN_runFlag |= 0x010;		/* 完了の印 */
        		DRN_runTimems = 0;
        		DRN_CuntTimems = 0;					/* 次の１ブロックデータ読込み */
        		ACC_Gyrosend_flag = 1;
        	}
        }
        /* ACC転送 */
    	if(ACC_Gyrosend_flag == 1)
    	{
    		if(Acc_dmpcnt > 0 ) {
	    		twork = mAccelerometer.Acc_1SectimeCnt;		/* 生ログ用に保管 */

	        	/* 実機用に生データをログする */
	        	LoggerManager.writeAcc_InterNaviLogFile(Acc_sdmp,gps_cntflag);
				mAccelerometer.Acc_1SectimeCnt = 0;

				//setCarGoingStatus();
		        if (NaviRun.isNaviRunInitial()) {
		            data.status = SENSOR_STATUS_PITCH_SET | SENSOR_STATUS_ROLL_SET;
		            data.accuracy = (short)event.accuracy;
		        	if(Acc_dmpcnt > 100) {
		        		Acc_dmpcnt = 100;
		        	}
		        	//NaviLog.v("TestWata ","ACC Data JNI ====== Start ========cnt="+Acc_dmpcnt);
		        	for(int i = 0; i < Acc_dmpcnt; i++ )
		        	{
		                data.timestamp = mAccelerometer.acc_t[i];
		                data.acceleroX = mAccelerometer.acc_d[0][i];
		                data.acceleroY = mAccelerometer.acc_d[1][i];
		                data.acceleroZ = mAccelerometer.acc_d[2][i];
		                //NaviLog.d("wata ","cnt = "+i);
		                NaviRun.GetNaviRunObj().JNI_VP_SetAcceleroSensorData(data);
		        	}
		        }
	   			Acc_Tcnt++;
	   			if(Acc_Tcnt > 255) {
	   				Acc_Tcnt = 1;
	   			}
				setCarGoingStatus();
				ACC_Gyrosend_flag = 2;
    		}
    		else {
    			mAccelerometer.Acc_1SectimeCnt = 0;
    			ACC_Gyrosend_flag = 0;
    			mAccelerometer.Acctimestamp_End = System.currentTimeMillis();
    			mGyroscope.Gyro_1SectimeCnt  = 0;
    			mGyroscope.Gyrotimestamp_End = mAccelerometer.Acctimestamp_End;
    		}
    	}

        //VPに渡すときはX=左右方向,Y=前後方向,Z=重力方向の加速度として与える
        //data.acceleroX = mAccelerometer.accelerationT[AccelerometerSensor.ACC_DIR_1_RIGHT]; //画面右方向が正
        //data.acceleroY = mAccelerometer.accelerationT[AccelerometerSensor.ACC_DIR_2_AHEAD]; //画面奥方向が正
        //data.acceleroZ = mAccelerometer.accelerationT[AccelerometerSensor.ACC_DIR_0_DOWN]; //重力方向が正
//    	float accelLength = (float)Math.sqrt(Math.pow((double)data.acceleroX, 2.0) +
//    										 Math.pow((double)data.acceleroY, 2.0) +
//    										 Math.pow((double)data.acceleroZ, 2.0) );
//    	Log.i("", String.format("yoneya acc %6.3f, %6.3f, %6.3f length=%6.3f", data.acceleroX, data.acceleroY, data.acceleroZ, accelLength));

// Add 2011/05/10 Z01ONOZAWA Start --> 速度が理論値より大きな値を示すので、調整してみる
  /********
        if (data.acceleroY > 0.0) {
            inaccurately_ratio = 1.0f;
        } else {
            inaccurately_ratio = 0.75f;
        }
        data.acceleroY *= inaccurately_ratio;
// Add 2011/05/10 Z01ONOZAWA End   <--

        long now = System.currentTimeMillis();
        mPastAcceleration.put(now, mAccelerometer.nrvalues[AccelerometerSensor.ACC_DIR_0_DOWN],
                                    mAccelerometer.nrvalues[AccelerometerSensor.ACC_DIR_1_RIGHT],
                                    mAccelerometer.nrvalues[AccelerometerSensor.ACC_DIR_2_AHEAD]);

//Chg 2011/03/18 Z01yoneya Start-->
//		// 回転前の加速度よりピッチ・ロールがわかる
//    	if (isCarStopping()) {
//---------------------------------------------------------
//Chg 2011/09/25 Z01YUMISAKI Start -->
//        boolean isCarStop =isCarStopping();
//------------------------------------------------------------------------------------------------------------------------------------
        boolean isCarStop = isCarStandstill();
//Chg 2011/09/25 Z01YUMISAKI End <--
        // 回転前の加速度よりピッチ・ロールがわかる
        if (isCarStop) {
//Chg 2011/03/18 Z01yoneya End <--
//Chg 2011/04/26 Z01yoneya Start -->
//// Add 2011/02/25 sawada Start -->
//if (mPrevGoing) {
//// Add 2011/02/25 sawada End   <--
//	mAccelerometer.zerofy();
//
////Chg 2011/04/21 Z01yoneya Start -->
////	    		event.values[0] = mAccelerometer.acceleration[0];
////	    		event.values[1] = mAccelerometer.acceleration[1];
////	    		event.values[2] = mAccelerometer.acceleration[2];
////
////	    		mOrientation.onSensorChanged(event);
////	       		Log.i("", String.format("yoneya onAcc isCarStopping raw num=%d roll=%.2f, pitch=%.2f",
////       					mPastAcceleration.getDataNum(SENSOR_DATA_SAMPLING_TIME_SEC),
////       					mOrientation.orientation[1],
////       					mOrientation.orientation[2]));
////--------------------------------------------
//	//isCarStop判定した時の平均値で傾き角度を求める。
//	float [] accAve = mPastAcceleration.getAverageInSeconds(SENSOR_DATA_SAMPLING_TIME_SEC);
//	event.values[0] = accAve[AccelerometerSensor.ACC_DIR_0_DOWN		];	//重力方向が正
//	event.values[1] = accAve[AccelerometerSensor.ACC_DIR_1_RIGHT	];	//画面右方向が正
//	event.values[2] = accAve[AccelerometerSensor.ACC_DIR_2_AHEAD	];	//画面奥方向が正
//
//	mOrientation.onSensorChanged(event);
//	Log.i("", String.format("yoneya onAcc isCarStopping ave num=%d roll=%.2f, pitch=%.2f",
//			mPastAcceleration.getDataNum(SENSOR_DATA_SAMPLING_TIME_SEC),
//			mOrientation.orientation[2],
//			mOrientation.orientation[1]));
////Chg 2011/04/21 Z01yoneya End <--
//
//	mLastOrientation[1] = mOrientation.orientation[1];
//	mLastOrientation[2] = mOrientation.orientation[2];
////	    		float [] gyro1sec = mPastAngularSpeed.getAverageInSeconds(1);
////	       		Log.i("", String.format("yoneya onAcc gyro ave %f %f %f",
////	       				gyro1sec[GyroscopeSensor.GYRO_DIR_0_AZ		],
////	       				gyro1sec[GyroscopeSensor.GYRO_DIR_1_PITCH	],
////	       				gyro1sec[GyroscopeSensor.GYRO_DIR_2_ROLL	]));
////	       		Log.i("", String.format("yoneya onAcc gyro row %f %f %f",
////	       				mGyroscope.angularSpeed[GyroscopeSensor.GYRO_DIR_0_AZ		],
////	       				mGyroscope.angularSpeed[GyroscopeSensor.GYRO_DIR_1_PITCH	],
////	       				mGyroscope.angularSpeed[GyroscopeSensor.GYRO_DIR_2_ROLL	]));
////Add 2011/04/25 Z01yoneya Start -->
//	mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_0_AZ	] = 0.0f;
//	mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH	] = 0.0f;
//	mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_2_ROLL	] = 0.0f;
////Add 2011/04/25 Z01yoneya End <--
//	// 傾きが変わったので回転行列を作る
//// Chg 2011/02/25 sawada Start -->
////	     		mMatrix.invert(0.0f, mLastOrientation[2] * DEG2RAD, mLastOrientation[1] * DEG2RAD);
//	mMatrix.invert(mLastOrientation[0] * DEG2RAD
//			,mLastOrientation[2] * DEG2RAD
//			,mLastOrientation[1] * DEG2RAD);
//// Chg 2011/02/25 sawada End   <--
//	mAccelerometer.setMatrix(mMatrix);
//	mGyroscope.setMatrix(mMatrix);
//// Add 2011/02/25 sawada Start -->
//// Add 2011/02/28 sawada Start -->
//	mSensorStatus |= SENSOR_STATUS_PITCH_SET;
//	mSensorStatus |= SENSOR_STATUS_ROLL_SET;
//	//Log.i("", String.format("mSensorStatus:%d", mSensorStatus));
//// Add 2011/02/28 sawada End   <--
//--------------------------------------------
            if (mPrevGoing) {
                mAccelerometer.zerofy();
                event.values[0] = mAccelerometer._noiseBuffer[AccelerometerSensor.ACC_DIR_0_DOWN].average(mSamplingNum); //重力方向が正
                event.values[1] = mAccelerometer._noiseBuffer[AccelerometerSensor.ACC_DIR_1_RIGHT].average(mSamplingNum); //画面右方向が正
                event.values[2] = mAccelerometer._noiseBuffer[AccelerometerSensor.ACC_DIR_2_AHEAD].average(mSamplingNum); //画面奥方向が正
//    			float [] accAve = mPastAcceleration.getAverageInSeconds(SENSOR_DATA_SAMPLING_TIME_SEC);
//    			event.values[0] = accAve[AccelerometerSensor.ACC_DIR_0_DOWN		];	//重力方向が正
//    			event.values[1] = accAve[AccelerometerSensor.ACC_DIR_1_RIGHT	];	//画面右方向が正
//    			event.values[2] = accAve[AccelerometerSensor.ACC_DIR_2_AHEAD	];	//画面奥方向が正

                updateDeviceRotateAngle(event, false);
//Chg 2011/04/26 Z01yoneya End <--
            }
            mPrevStopping = true;
            mPrevGoing = false;
// Add 2011/02/25 sawada End   <--
        }

        if (!isCarStop) {
// Add 2011/02/25 sawada Start -->
            if (mPrevStopping && (mSensorStatus & (SENSOR_STATUS_PITCH_SET | SENSOR_STATUS_ROLL_SET)) != 0) {
// Add 2011/02/25 sawada End   <--
                // 回転後の加速度よりヨーがわかる
                event.values[0] = mAccelerometer.accelerationT[0];
                event.values[1] = mAccelerometer.accelerationT[1];
                event.values[2] = mAccelerometer.accelerationT[2];
                mOrientation.onSensorChanged(event);
// Chg 2011/03/03 sawada Start -->
//				mLastOrientation[0] = mOrientation.orientation[0];
                // [暫定] ヨー評価不足につき 0 固定とする
                mLastOrientation[0] = 0.0f;
// Chg 2011/03/03 sawada End   <--
// Add 2011/02/25 sawada Start -->
// Add 2011/03/02 sawada Start -->
                mMatrix.invert(mLastOrientation[0] * DEG2RAD
                             , mLastOrientation[2] * DEG2RAD
                             , mLastOrientation[1] * DEG2RAD);
                mAccelerometer.setMatrix(mMatrix);
                mGyroscope.setMatrix(mMatrix);
// Add 2011/03/02 sawada End   <--
// Add 2011/02/28 sawada Start -->
                mSensorStatus |= SENSOR_STATUS_YAW_SET;
                //Log.i("", String.format("mSensorStatus:%d", mSensorStatus));
// Add 2011/02/28 sawada End   <--
            }
            mPrevGoing = true;
            mPrevStopping = false;
// Add 2011/02/25 sawada End   <--
        }
// Add 2011/02/25 sawada Start -->
        setCarGoingStatus();
// Add 2011/02/25 sawada End   <--
//Chg 2011/04/25 Z01yoneya Start -->
//Add 2011/04/20 Z01yoneya Start -->
//	    data.yaw	= mLastOrientation[0];
//	    data.roll	= mLastOrientation[2];
//	    data.pitch	= mLastOrientation[1];
//Add 2011/04/20 Z01yoneya End <--
//--------------------------------------------
    ****/

        //傾き補正にピッチを足しこむ修正
        //data.yaw = mLastOrientation[0];
        //data.roll = mLastOrientation[2] + mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_2_ROLL];
        //data.pitch = mLastOrientation[1] + mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH];
//Chg 2011/04/25 Z01yoneya End <--
// Add 2011/02/24 sawada End   <--
//Chg 2011/05/11 Z01thedoanh Start -->
        //NaviRunObj == nullの時、JNI関数呼び出さない（終了時に対策）
        //if (NaviRun.isNaviRunInitial()) {
        //    NaviRun.GetNaviRunObj().JNI_VP_SetAcceleroSensorData(data);
        //}
//Chg 2011/05/11 Z01thedoanh End <--

// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
        cloneToMonitor();
        if (mMonitor != null) mMonitor.mTarget.cloneMonitorAcc(data.timestamp, mAccelerometer);
// Add 2011/03/17 Z01ONOZAWA End   <--
// Add 2011/02/24 sawada Start <--
        //使っていない
        //onOrientSensorChanged(event);
// Add 2011/02/24 sawada End   <--
    }

    public void onOrientSensorChanged(RawSensorEvent event) {
// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
        if (mSensorDisableOri) return;
// Add 2011/03/17 Z01ONOZAWA End   <--
// Add 2011/02/24 sawada Start -->
        OriSensorData data = new OriSensorData();

// Add 2011/02/28 sawada Start -->
        data.status = mSensorStatus;
// Add 2011/02/28 sawada End   <--
        data.accuracy = (short)event.accuracy;
// Chg 2011/03/01 sawada Start -->
//    	data.timestamp = event.timestamp;
        data.timestamp = System.currentTimeMillis();
// Chg 2011/03/01 sawada End   <--
        data.azimuth = mLastOrientation[0];
        data.pitch = mLastOrientation[1];
        data.roll = mLastOrientation[2];
//    	Log.i("", String.format("[O] %f, %f, %f", data.azimuth, data.pitch, data.roll));
// Add 2011/02/24 sawada End   <--

//Chg 2011/05/11 Z01thedoanh Start -->
        //NaviRunObj == nullの時、JNI関数呼び出さない（終了時に対策）
        if (NaviRun.isNaviRunInitial()) {
            NaviRun.GetNaviRunObj().JNI_VP_SetOrientSensorData(data);
        }
//Add 2011/05/11 Z01thedoanh End <--

// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
        cloneToMonitor();
// Add 2011/03/17 Z01ONOZAWA End   <--
    }

    public void onGyroSensorChanged(RawSensorEvent event) {
    	long twork;			///< タイムスタンプ経過ms
		float fXYZ[] = new float[3];			///< Gyro一時用

// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
        if (mSensorDisableGyro) return;
// Add 2011/03/17 Z01ONOZAWA End   <--
// Add 2011/02/24 sawada Start -->
        mGyroscope.onSensorChanged(event);

        GyroSensorData data = new GyroSensorData();

        mSensorStatus = SENSOR_STATUS_PITCH_SET;
        mSensorStatus |= (SENSOR_STATUS_ROLL_SET | SENSOR_STATUS_YAW_SET);

// Add 2011/02/28 sawada Start -->
        data.status = mSensorStatus;
// Add 2011/02/28 sawada End   <--
        data.accuracy = (short)event.accuracy;
// Chg 2011/03/01 sawada Start -->
//    	data.timestamp = event.timestamp;
        data.timestamp = System.currentTimeMillis();

        //ログにセンサー値のまま出力する
        data.gyroAccelX = event.values[GyroscopeSensor.GYRO_DIR_0_AZ];
        data.gyroAccelY = event.values[GyroscopeSensor.GYRO_DIR_1_PITCH];
        data.gyroAccelZ = event.values[GyroscopeSensor.GYRO_DIR_2_ROLL];
        //LoggerManager.writeLog(data);
// Chg 2011/03/01 sawada End   <--
    	//ログにセンサー値のまま出力する
    	fXYZ[0] = event.values[GyroscopeSensor.GYRO_DIR_0_AZ];
    	fXYZ[1] = event.values[GyroscopeSensor.GYRO_DIR_1_PITCH];
    	fXYZ[2] = event.values[GyroscopeSensor.GYRO_DIR_2_ROLL];

    	twork = 0;
    	///< 開始ms設定
		if(LoggerManager.DRN_Get_WriteFlag() == 1)	{
	    	mGyroscope.Gyrotimestamp_Start = System.currentTimeMillis();
	    	if(mGyroscope.Gyro_1SectimeCnt == 0)
	    	{
	    		Gyro_dmpcnt = 0;									///< 生データログ文字配列カウンタ
	    		Gyro_sdmp[Gyro_dmpcnt] = "";						///< NULL初期化
	    		if(mGyroscope.Gyrotimestamp_End != 0)				///< 前回タイムスタンプあり
	    		{
	    			twork = mGyroscope.Gyrotimestamp_Start - mGyroscope.Gyrotimestamp_End;
	    		}
	    	}
	    	else
	    	{
	    		twork = mGyroscope.Gyrotimestamp_Start - mGyroscope.Gyrotimestamp_End;
	    	}
			mGyroscope.Gyrotimestamp_End = mGyroscope.Gyrotimestamp_Start;
	        //int gps_flag = MyLocationListener.get_GPSchg_flag();
	        //int send_flag = 0;
	       // NaviLog.d("wata ","Gyro cnt="+Gyro_GPS_flag +" "+gps_cntflag);
			//* Gyro積分値を求める */
			if(twork != 0)								///< 前回タイム差がある
			{
				mGyroscope.Gyro_1SectimeCnt += twork;					///< 時間差を加算する
				Gyro_sdmp[Gyro_dmpcnt] = String.format("%02d,G,%d,%.6f,%.6f,%.6f",Gyro_dmpcnt,twork,fXYZ[0],fXYZ[1],fXYZ[2]);

				mGyroscope.gyro_t[Gyro_dmpcnt] = twork;
				mGyroscope.gyro_d[0][Gyro_dmpcnt] = fXYZ[0];	//azimath(=yaw,左回転が正）
				mGyroscope.gyro_d[1][Gyro_dmpcnt] = fXYZ[1];	//pitch(奥に倒すと正)
				mGyroscope.gyro_d[2][Gyro_dmpcnt] = fXYZ[2];	//roll(左に傾けると正）
				if(Gyro_dmpcnt < 199)
				{
					Gyro_dmpcnt++;										///< 生データログ文字配列カウンタ
				}
				Gyro_sdmp[Gyro_dmpcnt] = "";							///< NULL初期化
			}
    	}
    	/* GPS受信が開始されない状態は、内部ｶｳﾝﾀが1sを超えていた場合、データ転送を行う */
/*** ACCでsend_flagを操作する
    	if(gps_cntflag == 0) {
       		if(mGyroscope.Gyro_1SectimeCnt > 1000) {
       			send_flag = 1;
       		}
    	} else {
    		// GPS受信が開始されれば、GPS flagの変化でデータ転送を行う
    		if(Gyro_GPS_flag != gps_cntflag) {
    			GPSStopNflag = 0;
    			send_flag = 1;
    		}
    		// GPS停止をACCが認識
    		else if(GPSStopNflag  == 2) {
    			GPSStopNflag = 1;
    			send_flag = 1;
    		}
    	}
  *****/
		/* Gyro転送 */
    	if(ACC_Gyrosend_flag == 2)
    	{
			LoggerManager.writeGyro_InterNaviLogFile(Gyro_sdmp,gps_cntflag);
			//Gyro_GPS_flag = gps_cntflag;					/* GPS更新状態保存 */
			mGyroscope.Gyro_1SectimeCnt = 0;

	        if (NaviRun.isNaviRunInitial()) {
	        	data.status = SENSOR_STATUS_PITCH_SET | SENSOR_STATUS_ROLL_SET | SENSOR_STATUS_YAW_SET;
	        	if(Gyro_dmpcnt > 100) {
	        		Gyro_dmpcnt = 100;
	        	}
	        	for(int i = 0; i < Gyro_dmpcnt; i++ )
	        	{
		        	data.timestamp = mGyroscope.gyro_t[i];
		        	data.gyroAccelX = mGyroscope.gyro_d[0][i];
		       		data.gyroAccelY = mGyroscope.gyro_d[1][i];
		        	data.gyroAccelZ = mGyroscope.gyro_d[2][i];
		            NaviRun.GetNaviRunObj().JNI_VP_SetGyroSensorData(data);
	        	}
	        }
	        Acc_GPS_flag = gps_cntflag;					/* GPS更新状態保存 */
			Gyro_dmpcnt = 0;								///< 生データログ文字配列カウンタ
			Acc_dmpcnt = 0;									///< 生データログ文字配列カウンタ
			ACC_Gyrosend_flag = 0;
   			Gyro_Tcnt++;
   			if(Gyro_Tcnt > 255) {
   				Gyro_Tcnt = 1;
   			}
   			if (NaviRun.isNaviRunInitial()) {
   				NaviRun.GetNaviRunObj().JNI_VP_SetEventTcnt(Acc_Tcnt,Gyro_Tcnt);
   			}

   			// DR状態仮Iconに表示する(CT_GetNaviMode()に移す 6/26)
   			// これは、DR状態を車Iconに色付き表示しています。デバッグ用
   	// ★CalledFromWrongThreadExceptionが発生
   	// 車マークの状態色更新は、updateScaleで行う 2013 07/09
   	//		MapView mapView = MapView.getInstance();
   			//nt DR_sts = (int)NaviRun.GetNaviRunObj().JNI_VP_DR_Status();
   			//NaviLog.v("TestWata", "[Sensor] Disp Mode ="+DR_sts);
   	//		if(mapView != null){
   	//			mapView.onNaviModeChange();
   	//		}
    	}

// Chg 2011/02/25 sawada Start -->
//    	data.gyroAccelX = mGyroscope.angularSpeedT[0];
//   	data.gyroAccelY = mGyroscope.angularSpeedT[1];
//    	data.gyroAccelZ = mGyroscope.angularSpeedT[2];
        //data.gyroAccelX = mGyroscope.angularSpeedT[GyroscopeSensor.GYRO_DIR_1_PITCH]; //pitch(奥に倒すと正)
        //data.gyroAccelY = mGyroscope.angularSpeedT[GyroscopeSensor.GYRO_DIR_2_ROLL]; //roll(左に傾けると正）
        //data.gyroAccelZ = mGyroscope.angularSpeedT[GyroscopeSensor.GYRO_DIR_0_AZ]; //azimath(=yaw,左回転が正）
//    	Log.i("", String.format("yoneya gyro %6.3f, %6.3f, %6.3f", data.gyroAccelX, data.gyroAccelY, data.gyroAccelZ));
//    	Log.i("", String.format("[G] %f, %f, %f", data.gyroAccelX, data.gyroAccelY, data.gyroAccelZ));
// Chg 2011/02/25 sawada End   <--

/*****
        // ストップ・ゴー判定用
        long now = System.currentTimeMillis();
        mPastAngularSpeed.put(now, mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_0_AZ],
                                    mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_1_PITCH],
                                    mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_2_ROLL]);

//    	Log.i("", String.format("yoneya onGyro isCarStopping acc_num(%d) gyr_num(%d)", mPastAcceleration.getDataNum(SENSOR_DATA_SAMPLING_TIME_SEC), mPastAngularSpeed.getDataNum(SENSOR_DATA_SAMPLING_TIME_SEC)));
//Chg 2011/09/25 Z01YUMISAKI Start -->
//    	if (isCarStopping()) {
//------------------------------------------------------------------------------------------------------------------------------------
        if (isCarStandstill()) {
//Chg 2011/09/25 Z01YUMISAKI End <--
            mGyroscope.zerofy();
//    		Log.i("", String.format("yoneya onGyro isCarStopping zerofy()"));
//Add 2011/04/25 Z01yoneya Start -->
            mGyroGainPassTimeMSec = data.timestamp; //最初の1回目はGyro変化量を無視する。時間の制御が難しいため
            mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_0_AZ] = 0.0f;
            mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH] = 0.0f;
            mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_2_ROLL] = 0.0f;
//Add 2011/04/25 Z01yoneya End <--
        }
//Add 2011/04/25 Z01yoneya Start -->
        else {
            if (mGyroGainPassTimeMSec != 0) {
                float passTimeSec = (data.timestamp - mGyroGainPassTimeMSec) * 0.001f; //経過時間(秒)

                //ジャイロ変化量にあわせて、傾き補正角度を変更する
                //加速度センサーの傾き補正角度のみ変更する
                //ジャイロを一緒に補正すると、ジャイロのGainTotalのつじつまがあわなくなるため
                if ((mSensorStatus & (SENSOR_STATUS_PITCH_SET | SENSOR_STATUS_ROLL_SET)) != 0) {
                    //ヨー回転は常に0(ヨー方向は傾き補正をしてないため)
                    float[] gyroSampleAve = new float[3];
                    gyroSampleAve[GyroscopeSensor.GYRO_DIR_0_AZ] = mGyroscope._noiseBuffer[GyroscopeSensor.GYRO_DIR_0_AZ].average(mSamplingNum);
                    gyroSampleAve[GyroscopeSensor.GYRO_DIR_1_PITCH] = mGyroscope._noiseBuffer[GyroscopeSensor.GYRO_DIR_1_PITCH].average(mSamplingNum);
                    gyroSampleAve[GyroscopeSensor.GYRO_DIR_2_ROLL] = mGyroscope._noiseBuffer[GyroscopeSensor.GYRO_DIR_2_ROLL].average(mSamplingNum);

//        			float [] gyroSampleAve = mPastAngularSpeed.getAverageInSeconds(SENSOR_DATA_SAMPLING_TIME_SEC);
//    				Log.i("", String.format("yoneya angle onGyro gyroSampleAve %10f %10f %10f passTimeSec(%f)",
//    						gyroSampleAve[GyroscopeSensor.GYRO_DIR_0_AZ		],
//    						gyroSampleAve[GyroscopeSensor.GYRO_DIR_1_PITCH	],
//    						gyroSampleAve[GyroscopeSensor.GYRO_DIR_2_ROLL	],passTimeSec));
                    //ピッチのみ足しこむ
                    //mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_0_AZ	] = mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_0_AZ	] + (gyroSampleAve[GyroscopeSensor.GYRO_DIR_0_AZ	] * passTimeSec);
                    mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH] = mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH] + (gyroSampleAve[GyroscopeSensor.GYRO_DIR_1_PITCH] * passTimeSec);
//            		mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_2_ROLL	] = mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_2_ROLL	] + (gyroSampleAve[GyroscopeSensor.GYRO_DIR_2_ROLL	] * passTimeSec);
//Add 2011/05/16 Z01yoneya Start -->
                    if (mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH] > mGyroAngleDegGainMax) {
                        mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH] = mGyroAngleDegGainMax;
                    } else if (mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH] < -mGyroAngleDegGainMax) {
                        mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH] = -mGyroAngleDegGainMax;
                    }
//Add 2011/05/16 Z01yoneya End <--
                    mMatrix.invert(mLastOrientation[0] * DEG2RAD
                             , (mLastOrientation[2] + mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_2_ROLL]) * DEG2RAD
                             , (mLastOrientation[1] + mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH]) * DEG2RAD);
                    mAccelerometer.setMatrix(mMatrix);
                    mGyroscope.setMatrix(mMatrix);
//            		Log.i("", String.format("yoneya angle onGyro setMatrix(roll=%10f pitch=%10f gainRoll=%10f gainPitch=%10f"
//            								, mLastOrientation[2]+mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_2_ROLL]
//            								, mLastOrientation[1]+mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH]
//            								, mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_2_ROLL]
//            								, mGyroAngleDegGainTotal[GyroscopeSensor.GYRO_DIR_1_PITCH]));
                }
            }
            mGyroGainPassTimeMSec = data.timestamp;
        }
//Add 2011/04/25 Z01yoneya End <--
***/

// Add 2011/02/25 sawada Start -->
        //setCarGoingStatus();
// Add 2011/02/25 sawada End   <--
// Add 2011/02/24 sawada End   <--
//Chg 2011/05/11 Z01thedoanh Start -->
        //NaviRunObj == nullの時、JNI関数呼び出さない（終了時に対策）
        //if (NaviRun.isNaviRunInitial()) {
        //    NaviRun.GetNaviRunObj().JNI_VP_SetGyroSensorData(data);
        //}
//Chg 2011/05/11 Z01thedoanh End <--

// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
        cloneToMonitor();
        if (mMonitor != null) mMonitor.mTarget.cloneMonitorGyro(data.timestamp, mGyroscope);
// Add 2011/03/17 Z01ONOZAWA End   <--
    }

// Add 2011/02/23 sawada Start -->
    public void setCarGoingStatus() {
        long status = CAR_STATUS_UNKNOWN;
        //Log.i("SensorDataManager", String.format("isCarStopping:%b isCarGoing:%b", isCarStopping(), isCarGoing()));
        if (isCarStopping()) {
            status = CAR_STATUS_STOPPING;
        } else if (isCarGoing()) {
            status = CAR_STATUS_GOING;
        }
//Chg 2011/05/11 Z01thedoanh Start -->
        //NaviRunObj == nullの時、JNI関数呼び出さない（終了時に対策）
        if (NaviRun.isNaviRunInitial()) {
            NaviRun.GetNaviRunObj().JNI_VP_SetCarGoingStatus(status);
        }
//Chg 2011/05/11 Z01thedoanh End <--

// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
        mCarStatus = status;
// Add 2011/03/17 Z01ONOZAWA End   <--
    }

// Add 2011/02/23 sawada End   <--

    @Override
    synchronized public void onSensorChanged(SensorEvent event) {
        RawSensorEvent revent = new RawSensorEvent(event);
        if (event.sensor == mAccelerometer.sensor) {
            onAccelSensorChanged(revent);
// Add 2011/03/10 Z01ONOZAWA Start --> 内部情報ログ出力
            // 加速度センサーと地磁気センサーから傾きを求めるためイベントを送る
            if ((mOrientationMagnetic != null) && (mOrientationMagnetic.sensor != null)) {
                onMagSensorChangedToLog(revent);
            }
// Add 2011/03/10 Z01ONOZAWA End   <--
        } else if (event.sensor == mGyroscope.sensor) {
            onGyroSensorChanged(revent);
        }
// Add 2011/03/10 Z01ONOZAWA Start --> 内部情報ログ出力
        else if (event.sensor == mOrientationHard.sensor) {
            onOriSensorChangedToLog(revent);
        } else if (event.sensor == mOrientationMagnetic.sensor) {
            onMagSensorChangedToLog(revent);
        }
// Add 2011/03/10 Z01ONOZAWA End   <--
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
// Del 2011/02/22 sawada Start -->
//    	NaviLog.d("Sensor", "onAccuracyChanged() is called");
// Del 2011/02/22 sawada End   <--
    }

//Add 2011/09/25 Z01YUMISAKI Start -->
    /**
     * 静止判定
     *
     * @return 静止状態である
     */
    public boolean isCarStandstill() {
        //1秒間にセンサーデータが4つ以上たまってない場合は、停止判定しない。
        //エミュレータ(処理付加が高い場合も?)では、たまらない場合がある。
        //1秒間隔判定でセンサー200msecを想定して個とする。
        if (mPastAcceleration.getDataNum(SENSOR_DATA_SAMPLING_TIME_SEC) < SENSOR_DATA_SAMPLING_MIN_NUM) {
            return false;
        }

        float[] accelerationDiff = mPastAcceleration.getMinMaxDiffAbsInSeconds(SENSOR_DATA_SAMPLING_TIME_SEC);
        float[] angularSpeedDiff = mPastAngularSpeed.getMinMaxDiffAbsInSeconds(SENSOR_DATA_SAMPLING_TIME_SEC);
        if ((Math.abs(mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_0_AZ]) >= STANDSTILL_GYRO_ABS_FLUCTUATION)
                || (Math.abs(mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_1_PITCH]) >= STANDSTILL_GYRO_ABS_FLUCTUATION)
                || (Math.abs(mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_2_ROLL]) >= STANDSTILL_GYRO_ABS_FLUCTUATION)
                || (accelerationDiff[AccelerometerSensor.ACC_DIR_0_DOWN] >= STANDSTILL_ACCEL_DIFF_FLUCTUATION)
                || (accelerationDiff[AccelerometerSensor.ACC_DIR_1_RIGHT] >= STANDSTILL_ACCEL_DIFF_FLUCTUATION)
                || (accelerationDiff[AccelerometerSensor.ACC_DIR_2_AHEAD] >= STANDSTILL_ACCEL_DIFF_FLUCTUATION)
                || (angularSpeedDiff[GyroscopeSensor.GYRO_DIR_0_AZ] >= STANDSTILL_GYRO_DIFF_FLUCTUATION)
                || (angularSpeedDiff[GyroscopeSensor.GYRO_DIR_1_PITCH] >= STANDSTILL_GYRO_DIFF_FLUCTUATION)
                || (angularSpeedDiff[GyroscopeSensor.GYRO_DIR_2_ROLL] >= STANDSTILL_GYRO_DIFF_FLUCTUATION)) {
            return false;
        }

        return true;
    }

//Add 2011/09/25 Z01YUMISAKI End <--

// Add 2011/02/22 sawada Start -->
//Chg 2011/09/25 Z01YUMISAKI Start -->
//    public boolean isCarStopping() {
////Add 2011/04/13 Z01yoneya Start-->
////    	//回転補正ができたあとは、加速度の絶対値がしきい値以下に停車判定を行う
////    	if(isEnablePitchRollRotate()){
////	    	//回転補正後の加速度がしきい値より大きい場合は、停車してない
////			if(Math.abs(mAccelerometer.accelerationT[AccelerometerSensor.ACC_DIR_2_AHEAD	]) >= ACCEL_STOP_VALUE_MAX){
////				return false;
////			}
////    	}
////Add 2011/04/13 Z01yoneya End <--
//
//    	//1秒間にセンサーデータが4つ以上たまってない場合は、停止判定しない。
//    	//エミュレータ(処理付加が高い場合も?)では、たまらない場合がある。
//    	//1秒間隔判定でセンサー200msecを想定して個とする。
//    	if(mPastAcceleration.getDataNum(SENSOR_DATA_SAMPLING_TIME_SEC)<SENSOR_DATA_SAMPLING_MIN_NUM){
//    		return false;
//    	}
////    	if(mPastAngularSpeed.getDataNum(SENSOR_DATA_SAMPLING_TIME_SEC)<SENSOR_DATA_SAMPLING_MIN_NUM){
////    		return false;
////    	}
//    	float[] accelerationDiff;
//    	float[] angularSpeedDiff;
//
////    	float accelLength = (float)Math.sqrt(Math.pow((double)mAccelerometer.accelerationT[1], 2.0) +	//X
////											 Math.pow((double)mAccelerometer.accelerationT[2], 2.0) +	//Y
////											 Math.pow((double)mAccelerometer.accelerationT[0], 2.0) );	//Z
//
//    	accelerationDiff = mPastAcceleration.getMinMaxDiffAbsInSeconds(SENSOR_DATA_SAMPLING_TIME_SEC);
//		angularSpeedDiff = mPastAngularSpeed.getMinMaxDiffAbsInSeconds(SENSOR_DATA_SAMPLING_TIME_SEC);
////    	Log.i("", String.format("yoneya isCar diff acc(%6.2f, %6.2f, %6.2f) gyr(%6.2f, %6.2f, %6.2f) currGyr(%6.2f, %6.2f, %6.2f) len(%6.2f)"
////    								, accelerationDiff[0]
////    			                    , accelerationDiff[1]
////    			                    , accelerationDiff[2]
////    			                    , angularSpeedDiff[0]
////    			                    , angularSpeedDiff[1]
////    			                    , angularSpeedDiff[2]
////    			       	            , mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_0_AZ		]
////    			       	            , mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_1_PITCH	]
////    			       	            , mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_2_ROLL	]
////    			                    , accelLength));
//
//		if(mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_0_AZ		] >= GYRO_STOP_FOR_CALC_ROTATE_MATRIX){return false;}
//		if(mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_1_PITCH		] >= GYRO_STOP_FOR_CALC_ROTATE_MATRIX){return false;}
//		if(mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_2_ROLL		] >= GYRO_STOP_FOR_CALC_ROTATE_MATRIX){return false;}
//
////		Log.i("", String.format("yoneya isCar diff %6.2f, %6.2f, %6.2f %6.2f, %6.2f, %6.2f", accelerationDiff[0], accelerationDiff[1], accelerationDiff[2], angularSpeedDiff[0], angularSpeedDiff[1], angularSpeedDiff[2]));
//		if(accelerationDiff[AccelerometerSensor.ACC_DIR_0_DOWN		] >= ACCEL_STOP_DIFF_MAX){return false;}
//		if(accelerationDiff[AccelerometerSensor.ACC_DIR_1_RIGHT		] >= ACCEL_STOP_DIFF_MAX){return false;}
//		if(accelerationDiff[AccelerometerSensor.ACC_DIR_2_AHEAD		] >= ACCEL_STOP_DIFF_MAX){return false;}
//
//		if(angularSpeedDiff[GyroscopeSensor.GYRO_DIR_0_AZ		] >= GYRO_STOP_DIFF_MAX){return false;}
//		if(angularSpeedDiff[GyroscopeSensor.GYRO_DIR_1_PITCH	] >= GYRO_STOP_DIFF_MAX){return false;}
//		if(angularSpeedDiff[GyroscopeSensor.GYRO_DIR_2_ROLL		] >= GYRO_STOP_DIFF_MAX){return false;}
//
//
//    	return true;
//    }
//------------------------------------------------------------------------------------------------------------------------------------
    /**
     * 停車判定
     *
     * @return 停車状態である
     */
    public boolean isCarStopping() {
        //1秒間にセンサーデータが4つ以上たまってない場合は、停止判定しない。
        //エミュレータ(処理付加が高い場合も?)では、たまらない場合がある。
        //1秒間隔判定でセンサー200msecを想定して個とする。
        if (mPastAcceleration.getDataNum(SENSOR_DATA_SAMPLING_TIME_SEC) < SENSOR_DATA_SAMPLING_MIN_NUM) {
            return false;
        }
        float[] accelerationDiff;
        float[] angularSpeedDiff;
        accelerationDiff = mPastAcceleration.getMinMaxDiffAbsInSeconds(SENSOR_DATA_SAMPLING_TIME_SEC);
        angularSpeedDiff = mPastAngularSpeed.getMinMaxDiffAbsInSeconds(SENSOR_DATA_SAMPLING_TIME_SEC);
        if ((Math.abs(mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_0_AZ]) >= CARSTOP_GYRO_ABS_FLUCTUATION)
                || (Math.abs(mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_1_PITCH]) >= CARSTOP_GYRO_ABS_FLUCTUATION)
                || (Math.abs(mGyroscope.nrvalues[GyroscopeSensor.GYRO_DIR_2_ROLL]) >= CARSTOP_GYRO_ABS_FLUCTUATION)
                || (accelerationDiff[AccelerometerSensor.ACC_DIR_0_DOWN] >= CARSTOP_ACCEL_DIFF_FLUCTUATION)
                || (accelerationDiff[AccelerometerSensor.ACC_DIR_1_RIGHT] >= CARSTOP_ACCEL_DIFF_FLUCTUATION)
                || (accelerationDiff[AccelerometerSensor.ACC_DIR_2_AHEAD] >= CARSTOP_ACCEL_DIFF_FLUCTUATION)
                || (angularSpeedDiff[GyroscopeSensor.GYRO_DIR_0_AZ] >= CARSTOP_GYRO_DIFF_FLUCTUATION)
                || (angularSpeedDiff[GyroscopeSensor.GYRO_DIR_1_PITCH] >= CARSTOP_GYRO_DIFF_FLUCTUATION)
                || (angularSpeedDiff[GyroscopeSensor.GYRO_DIR_2_ROLL] >= CARSTOP_GYRO_DIFF_FLUCTUATION)) {
            return false;
        }

        return true;
    }

//Chg 2011/09/25 Z01YUMISAKI End <--

    public boolean isCarGoing() {
        return !isCarStopping();
    }

// Add 2011/02/22 sawada End   <--
// Add 2011/03/10 Z01ONOZAWA Start --> 内部情報ログ出力
    public void onOriSensorChangedToLog(RawSensorEvent event) {
// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
        if (mSensorDisableOri) return;
// Add 2011/03/17 Z01ONOZAWA End   <--
        mOrientationHard.onSensorChanged(event);
        // 傾きセンサーをログ出力
// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
        cloneToMonitor();
        if (mMonitor != null) mMonitor.mTarget.cloneMonitorOri(mOrientationHard.timestamp, mOrientationHard);
// Add 2011/03/17 Z01ONOZAWA End   <--
    }

    public void onMagSensorChangedToLog(RawSensorEvent event) {
// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
        if (mSensorDisableMag) return;
// Add 2011/03/17 Z01ONOZAWA End   <--
        mOrientationMagnetic.onSensorChanged(event);
        // 傾きセンサーをログ出力
        if (mOrientationMagnetic.stateOrient == OrientationSensor.SET_MAGNETIC_FIELD) {
            // 傾きセンサーをログ出力→処理なし
        }
// Add 2011/03/17 Z01ONOZAWA Start --> モニタ機能対応
        cloneToMonitor();
        if (mMonitor != null) mMonitor.mTarget.cloneMonitorMag(mOrientationMagnetic.timestamp, mOrientationMagnetic);
// Add 2011/03/17 Z01ONOZAWA End   <--
    }

    private final static MonitorListener mListener = new MonitorListener() {
        @Override
        public void onCommandReceived(int iCommand, String sOperand) {
            String[] arguments = sOperand.split(",");// sOperand="arg1,arg2,....."
            MonitorAdapter monitor = MonitorAdapter.getInst();

            switch (iCommand) {
                case MonitorAdapter.COMMAND_ENABLE:
            case MonitorAdapter.COMMAND_DISABLE:
                int i = 0;
                for (i = 0; i < arguments.length; ++i) {
                    if (arguments[i].equalsIgnoreCase("ACC")) {
                        if (iCommand == MonitorAdapter.COMMAND_DISABLE) {
                            mSensorDisableAcc = true;
                        } else {
                            mSensorDisableAcc = false;
                        }
                        if (monitor != null) monitor.mTarget.mSensorDisableAcc = mSensorDisableAcc;
                    } else if (arguments[i].equalsIgnoreCase("GYRO")) {
                        if (iCommand == MonitorAdapter.COMMAND_DISABLE) {
                            mSensorDisableGyro = true;
                        } else {
                            mSensorDisableGyro = false;
                        }
                        if (monitor != null) monitor.mTarget.mSensorDisableGyro = mSensorDisableGyro;
                    } else if (arguments[i].equalsIgnoreCase("ORI")) {
                        if (iCommand == MonitorAdapter.COMMAND_DISABLE) {
                            mSensorDisableOri = true;
                        } else {
                            mSensorDisableOri = false;
                        }
                        if (monitor != null) monitor.mTarget.mSensorDisableOri = mSensorDisableOri;
                    } else if (arguments[i].equalsIgnoreCase("MAG")) {
                        if (iCommand == MonitorAdapter.COMMAND_DISABLE) {
                            mSensorDisableMag = true;
                        } else {
                            mSensorDisableMag = false;
                        }
                        if (monitor != null) monitor.mTarget.mSensorDisableMag = mSensorDisableMag;
                    }
                }
                break;
            default:
                break;
        }
    }
    };

    private void cloneToMonitor() {
        if (mMonitor == null) return;

        mMonitor.mTarget.mSensorStatus = mSensorStatus;
        mMonitor.mTarget.mCarStatus = mCarStatus;

        mMonitor.mTarget.mLastOrientation = mLastOrientation.clone();

        mMonitor.mTarget.mSensorDisableAcc = mSensorDisableAcc;
        mMonitor.mTarget.mSensorDisableGyro = mSensorDisableGyro;
        mMonitor.mTarget.mSensorDisableOri = mSensorDisableOri;
        mMonitor.mTarget.mSensorDisableMag = mSensorDisableMag;

        mMonitor.mTarget.mAccelerationDiff = mPastAcceleration.getMinMaxDiffAbsInSeconds(SENSOR_DATA_SAMPLING_TIME_SEC);

        mMonitor.mTarget.mAngularSpeedDiff = mPastAngularSpeed.getMinMaxDiffAbsInSeconds(SENSOR_DATA_SAMPLING_TIME_SEC);
    }

// Add 2011/03/10 Z01ONOZAWA End   <--

}
