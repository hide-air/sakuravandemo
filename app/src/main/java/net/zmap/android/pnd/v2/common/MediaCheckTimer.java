package net.zmap.android.pnd.v2.common;

/**
 *  @file       MediaCheckTimer.java
 *  @brief
 *
 *  @attention
 *  @note
 *
 *  @author     Hideyuki Yamada [Z01]
 *  @date       $Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2010-10-13)
 *  @version    $Revision: $ by $Author: $
 */


import java.util.Timer;
import java.util.TimerTask;

import android.os.StatFs;
import android.content.Context;

import net.zmap.android.pnd.v2.app.control.NaviControllIntent;
import net.zmap.android.pnd.v2.common.utils.NaviLog;

public class MediaCheckTimer {

	private Context 		mContext = null;

    private Timer   		mTimer   = null;
    private static final long  	MEGABYTE = 1024*1024;
    private long		  	mMediaCheckSize = 30 * MEGABYTE;		// 30MByte

    private String 			mNaviAppRootPath = null; //ナビアプリデータのルートフォルダ

    public MediaCheckTimer(Context context, String NaviAppRootPath) {
    	mContext = context;
        mNaviAppRootPath = NaviAppRootPath;
    }

	/** サイズチェック
	*
	* @param delay  タスクが実行される前のミリ秒単位の遅延
	* @param period 連続するタスク実行のミリ秒単位の時間
	**/
    public boolean isMediaSizeOK() {

        if ( mNaviAppRootPath != null ) {
// MOD.2013/03/25 N.Sasao Android4.2 以降 マルチユーザー対応 START
        	StatFs statFs = null;
        	try{
        		statFs = new StatFs(mNaviAppRootPath);
        	}catch(IllegalArgumentException e){
        		/* パスが不正な場合は、上位にエラーを返す */
            	NaviLog.e("ItsmoNaviDrive", "MediaCheckTimer::isMediaSizeOK IllegalArgumentException msg = " + e.toString() );
        		return false;
        	}
// MOD.2013/03/25 N.Sasao Android4.2 以降 マルチユーザー対応  END

//Chg 2011/11/21 Z01_h_yamada Start -->
//        	long disksize = statFs.getBlockSize() * statFs.getAvailableBlocks();
//        	NaviLog.i("ItsmoNaviDrive", "MediaCheckTimer::isMediaSizeOK disksize=" + disksize + " byte. ( " + disksize/MEGABYTE + " MByte)");
//--------------------------------------------
        	long disksize = (long)statFs.getBlockSize() * (long)statFs.getAvailableBlocks();
        	NaviLog.d("ItsmoNaviDrive", "MediaCheckTimer::isMediaSizeOK disksize=" + disksize + " byte. ( " + disksize/MEGABYTE + " MByte)");
//Chg 2011/11/21 Z01_h_yamada End <--

        	if ( disksize < mMediaCheckSize ) {
            	NaviLog.w("ItsmoNaviDrive", "disk size is insufficient. " + disksize + " byte.");
        		return false;
         	}
        }
        return true;
    }

	/** チェックするサイズを設定
	*
	* @param size  チェックするサイズ
	**/
    public void setMediaCheckSize( long size ) {

    	mMediaCheckSize = size;
    }

	/** サイズチェックタイマー開始
	*
	* @param delay  タスクが実行される前のミリ秒単位の遅延
	* @param period 連続するタスク実行のミリ秒単位の時間
	**/
    public void start( long delay, long period ) {

    	NaviLog.i("ItsmoNaviDrive", "MediaCheckTimer::start delay=" + delay + " period=" + period);
    	if ( mTimer == null )
    	{
	    	mTimer = new Timer();
	    	mTimer.schedule( new TimerTask() {
				@Override
				public void run() {
		        	if ( !isMediaSizeOK() ) {
		        		NaviControllIntent.sendMediaSizeInsufficient(mContext);
		        	}
				}
			}, delay, period );
    	}
    }

	/** サイズチェックタイマー終了
	**/
    public void stop() {

    	NaviLog.i("ItsmoNaviDrive", "MediaCheckTimer::stop");
		if ( mTimer != null ) {
			mTimer.cancel();
			mTimer = null;
		}
    }

}