package net.zmap.android.pnd.v2.sakuracust;

import net.zmap.android.pnd.v2.common.utils.DbHelper;
import android.app.Dialog;

//import java.io.UnsupportedEncodingException;

/**
 * さくらヴァン向けいつもNAVIのデータ取り込みのファサードクラス
 * @author User
 *
 */
public class DataCtrlFacade {
	private static String sjiscsn = "SJIS";
	
	public static final int SUCCEED = 1;
	public static final int FAIL = 0;
	

	/** 失敗 */
	public static final int  FAILED_AM         = 0;

	/** 地方（未選択）*/
	public static final int  CHIHO_LEV_AM      = 1;

	/** 都道府県     */
	public static final int  AREA_LEV_AM       = 2;

	/** 特別行政界   */
	public static final int  SPEC_LEV_AM       = 3;

	/** 市区町村     */
	public static final int  CITY_LEV_AM       = 4;

	/** 大字         */
	public static final int  OAZA_LEV_AM       = 5;

	/** 字丁目       */
	public static final int  AZA_LEV_AM        = 6;

	/** 街区         */
	public static final int  STRT_LEV_AM       = 7;

	/** 地番・戸番   */
	public static final int  EARTH_LEV_AM      = 8;

	/** 属性(世帯主) 若しくはいつもNAVIに於ける枝番 */
	public static final int  ATTR_LEV_AM       = 9;
	
	private static Dialog mWaitDlg;
	
    static {
//*    	
        try {
        	// NBや佐川、NGK等から流用した
        	// ネイティブ処理の取り込み
            System.loadLibrary("SakuraData");
        } catch (Exception ex) {
            ex.printStackTrace();
        } catch(ExceptionInInitializerError errori){
        	 errori.getCause().printStackTrace();
        }
//*/        
    }
    
	/**
	 * CSV取り込みの実行
	 * @param csvFileName CSVファイル名
	 * @param dbFileName  SQLiteファイル名
	 * @return 成功/失敗
	 */
	private static final int ExecuteImportCSV(
		String csvFileName,
		String dbFileName) throws DataCtrlException
	{
		//int result = FAIL;
/*		
		try
		{
			byte[] csvFNByte = csvFileName.getBytes(sjiscsn);
			byte[] dbFNByte = dbFileName.getBytes(sjiscsn);
			
			//byte[] csvFNByteNull = new byte[]
			
			result = ExecuteImportCSVNtv(csvFNByte, dbFNByte);
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
*/
		byte[] csvFNByte = getByteWithLastNull(csvFileName.getBytes());
		byte[] dbFNByte = getByteWithLastNull(dbFileName.getBytes());
				
		return ExecuteImportCSVNtv(csvFNByte, dbFNByte);
	}
	
	/**
	 * CSV取り込みを裏スレッドで回す
	 * @param waitDlg
	 */
	public static final void ExecuteImportCSVOnAnotherThread(
			Dialog waitDlg)
	{
		ImportCSVOnAnotherThread t = new ImportCSVOnAnotherThread(waitDlg);
		t.start();
	}
	
	/**
	 * CSV取り込みの裏スレッド
	 * @author User
	 *
	 */
	private static class ImportCSVOnAnotherThread extends Thread
	{
		private Dialog mWaitDlg;
		//private DataCtrlException mException;
		
		ImportCSVOnAnotherThread(Dialog waitDlg)
		{
			mWaitDlg = waitDlg;
			//mException = null;
		}
		
		public void run()
		{
			try
			{
				ExecuteImportCSV(DbHelper.INPORTCSV_NAME, DbHelper.DATABASE_NAME);
			}
			catch (DataCtrlException e)
			{
				e.printStackTrace();
			}
			mWaitDlg.dismiss();
		}
	}
	
	/**
	 * CSV出力の実行
	 * @param csvFileName CSVファイル名
	 * @param dbFileName  SQLiteファイル名
	 * @return 成功/失敗
	 */
	private static final int ExecuteExportCSV(
			String csvFileName,
			String dbFileName) throws DataCtrlException
	{
/*		
		int result = FAIL;
		try
		{
			byte[] csvFNByte = csvFileName.getBytes(sjiscsn);
			byte[] dbFNByte = dbFileName.getBytes(sjiscsn);
			result = ExecuteExportCSVNtv(csvFNByte, dbFNByte);
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		return result;
*/		
		byte[] csvFNByte = getByteWithLastNull(csvFileName.getBytes());
		byte[] dbFNByte = getByteWithLastNull(dbFileName.getBytes());
		return ExecuteExportCSVNtv(csvFNByte, dbFNByte);
	}
	
	/**
	 * CSV出力を裏スレッドで回す
	 * @param waitDlg
	 */
	public static final void ExecuteExportCSVOnAnotherThread(
			Dialog waitDlg)
	{
		ExportCSVOnAnotherThread t = new ExportCSVOnAnotherThread(waitDlg);
		t.start();
	}
	
	/**
	 * CSV出力の裏スレッド
	 * @author User
	 *
	 */
	private static class ExportCSVOnAnotherThread extends Thread
	{
		private Dialog mWaitDlg;
		ExportCSVOnAnotherThread(Dialog wd)
		{
			mWaitDlg = wd;
		}
		
		public void run()
		{
			try
			{
				ExecuteExportCSV(DbHelper.EXPORTCSV_NAME, DbHelper.DATABASE_NAME);
			}
			catch (DataCtrlException e)
			{
				e.printStackTrace();
			}
			mWaitDlg.dismiss();
		}
	}
	
	private static byte[] getByteWithLastNull(byte[] src)
	{
		byte[] result = new byte[src.length + 1];
		for (int i = 0; i <= src.length - 1; i++)
		{
			result[i] = src[i];
		}
		result[src.length] = '\0';
		return result;
	}
	
	//private static Thread importThread = new Thread();
	
	private static native int ExecuteImportCSVNtv(
			byte[] csvFileName, byte[] dbFileName) throws DataCtrlException;
	
	private static native int ExecuteExportCSVNtv(
			byte[] csvFileName, byte[] dbFileName) throws DataCtrlException;
}
