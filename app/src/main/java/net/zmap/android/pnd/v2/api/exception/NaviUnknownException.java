package net.zmap.android.pnd.v2.api.exception;

/**
 * 分類外のエラー
 */
public class NaviUnknownException extends NaviException {
    private static final long serialVersionUID = 1L;

    /**
     * エラー詳細メッセージとして null を設定して NaviUnknownException を構築します。
     */
    public NaviUnknownException() {
        super();
    }

    /**
     * 詳細メッセージを指定して NaviUnknownException を構築します。
     *
     * @param message
     *            詳細メッセージ
     */
    public NaviUnknownException(String message) {
        super(message);
    }
}