package net.zmap.android.pnd.v2.common.view;

import android.content.Context;
import android.graphics.Paint.FontMetrics;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

public class FitTextView extends TextView {

	private float 		 defaultSize = 12;
	private static float MIN_TEXT_SIZE = 6;

	private String 		 src_text;
	private int 		 src_viewWidth = 0;
	private int			 src_viewHeight = 0;

	public FitTextView(Context context) {
	    super(context);
	    defaultSize = getTextSize();
	}

	public FitTextView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	    defaultSize = getTextSize();
	}

	public FitTextView(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
	    defaultSize = getTextSize();
	}

	@Override
	public void setTextSize(float size) {
//	  	Log.i("test", "FitTextView setTextSize size=" +size);
	    super.setTextSize(size);
	    defaultSize = getTextSize();
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//	  	Log.i("test", "FitTextView onSizeChanged w=" + w + " h=" + h);
	    super.onSizeChanged(w, h, oldw, oldh);
	    fitTextInView();
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
//	  	Log.i("test", "FitTextView setText text=" +text);
 	    super.setText(text, type);
		fitTextInView();
	}

	public void fitTextInView() {
	    int paddingW = getPaddingLeft() + getPaddingRight();
	    int paddingH = getPaddingTop() + getPaddingBottom();
	    int viewWidth = getWidth() - paddingW;
	    int viewHeight = getHeight() - paddingH;

	    if (viewWidth <= 0 || viewHeight <= 0) {
	    	return;
	    }

	    if (src_viewWidth == viewWidth && src_viewHeight == viewHeight && src_text.compareTo( getText().toString()) == 0) {
	    	return;
	    }

	    setTextSize(TypedValue.COMPLEX_UNIT_PX, defaultSize);

	    int textWidth = getTextWidth();
	    int textHeight = getTextHeight();

	    float textSize = getTextSize();
	    while ((textWidth >= viewWidth ||textHeight >= viewHeight)
	    		&& textSize > MIN_TEXT_SIZE) {

	        setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize-1);
	        textWidth = getTextWidth();
	        textHeight = getTextHeight();

	        textSize = getTextSize();
	    }

//	  	Log.i("test", "FitTextView finish!! textSize=" + getTextSize());

	  	src_viewWidth  = viewWidth;
	  	src_viewHeight = viewHeight;
	  	src_text = getText().toString();
	}

	private int getTextWidth() {
	    return (int)Math.ceil(getPaint().measureText(getText().toString()));
	}

	private int getTextHeight() {
	    FontMetrics fontMetrics = getPaint().getFontMetrics();
	    return (int)Math.ceil(Math.abs( fontMetrics.top - fontMetrics.bottom));
	}
}
