/*------------------------------------------------------------------------------
 * Copyright(C) 2011 ZenrinDataCom Co.,LTD. All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of ZenrinDataCom Co.,LTD.;
 * the contents of this file is not to be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior written
 * permission of ZenrinDataCom Co.,LTD.
 *------------------------------------------------------------------------------*/

package net.zmap.android.pnd.v2.maps;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import android.util.FloatMath;

/**
 * 地図関連で数学計算をさせるためのユーティリティクラス。
 *
 * @id $Id: MapMath.java 4449 2012-01-13 10:52:49Z kojima-k $
 */
public class MapMath {

	/**
	 * 引数よりも大きな2の累乗値の中で最小の値を取得します。
	 *
	 * @param
	 */
	public static int nextPOT(int x) {
	    x = x - 1;
	    x = x | (x >> 1);
	    x = x | (x >> 2);
	    x = x | (x >> 4);
	    x = x | (x >> 8);
	    x = x | (x >>16);

	    return x + 1;
	}

	public static boolean isPowerOfTwo(int x) {
		return (x & (x - 1)) == 0;
	}

	static public class Vector2
	{
		private float x;
		private float y;

		public float x(){return x;}
		public float y(){return y;}

		public Vector2()
		{
			x = 0;
			y = 0;
		}
		public Vector2(float X,float Y)
		{
			set(X,Y);
		}

		public Vector2(Vector2 vec)
		{
			set(vec);
		}
		public Vector2 set(float X,float Y)
		{
			x = X;
			y = Y;
			return this;
		}
		public Vector2 set(Vector2 vec)
		{
			x = vec.x;
			y = vec.y;
			return this;
		}

		public final Vector2 add(Vector2 vec)
		{
			x += vec.x;
			y += vec.y;
			return this;
		}
		public final Vector2 add(float X,float Y)
		{
			x += X;
			y += Y;
			return this;
		}

		public final Vector2 subtract(Vector2 vec)
		{
			x -= vec.x;
			y -= vec.y;
			return this;
		}
		public final Vector2 subtract(float X,float Y)
		{
			x -= X;
			y -= Y;
			return this;
		}
		public final Vector2 multiply(float scaler)
		{
			x *= scaler;
			y *= scaler;
			return this;
		}
		public final Vector2 divide(float scaler)
		{
			x /= scaler;
			y /= scaler;
			return this;
		}

		public final Vector2 normalized()
		{
			divide(length());
			return this;
		}

		public final float length2()
		{
			return x * x + y * y;
		}
		public final float length()
		{
			return FloatMath.sqrt(length2());
		}
	}

	static public class Vector3
	{
		private float x;
		private float y;
		private float z;

		public float x(){return x;}
		public float y(){return y;}
		public float z(){return z;}

		public Vector3()
		{
			x = 0;
			y = 0;
			z = 0;
		}
		public Vector3(float X,float Y,float Z)
		{
			set(X,Y,Z);
		}

		public Vector3(Vector3 vec)
		{
			set(vec);
		}
		public Vector3 set(float X,float Y,float Z)
		{
			x = X;
			y = Y;
			z = Z;
			return this;
		}
		public Vector3 set(Vector3 vec)
		{
			x = vec.x;
			y = vec.y;
			z = vec.z;
			return this;
		}

		public final Vector3 add(Vector3 vec)
		{
			x += vec.x;
			y += vec.y;
			z += vec.z;
			return this;
		}
		public final Vector3 add(float X,float Y,float Z)
		{
			x += X;
			y += Y;
			z += Z;
			return this;
		}

		public final Vector3 subtract(Vector3 vec)
		{
			x -= vec.x;
			y -= vec.y;
			z -= vec.z;
			return this;
		}
		public final Vector3 subtract(float X,float Y,float Z)
		{
			x -= X;
			y -= Y;
			z -= Z;
			return this;
		}
		public final Vector3 multiply(float scaler)
		{
			x *= scaler;
			y *= scaler;
			z *= scaler;
			return this;
		}
		public final Vector3 divide(float scaler)
		{
			x /= scaler;
			y /= scaler;
			z /= scaler;
			return this;
		}

		public final Vector3 normalized()
		{
			divide(length());
			return this;
		}

		public final float length2()
		{
			return x * x + y * y + z * z;
		}
		public final float length()
		{
			return FloatMath.sqrt(length2());
		}
	}

	private static FloatBuffer createNativeOrderDirectFloatBuffer(int capacity)
	{
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4 * capacity);
		byteBuffer.order(ByteOrder.nativeOrder());

		return byteBuffer.asFloatBuffer();
	}
	public static ByteBuffer arrayToByteBuffer(byte[] array)
	{
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(array.length);
		for(byte n:array)
		{
			byteBuffer.put(n);
		}
		byteBuffer.position(0);
		return byteBuffer;
	}

	public static FloatBuffer arrayToFloatBuffer(Vector2[] array)
	{
		FloatBuffer buffer = createNativeOrderDirectFloatBuffer(array.length * 2);
		for(Vector2 vec:array)
		{
			buffer.put(vec.x());
			buffer.put(vec.y());
		}
		buffer.position(0);
		return buffer;
	}
	public static FloatBuffer arrayToFloatBuffer(Vector3[] array)
	{
		FloatBuffer buffer = createNativeOrderDirectFloatBuffer(array.length * 3);
		for(Vector3 vec:array)
		{
			buffer.put(vec.x());
			buffer.put(vec.y());
			buffer.put(vec.z());
		}
		buffer.position(0);
		return buffer;
	}
	public static FloatBuffer arrayToFloatBuffer(float[] array)
	{
		FloatBuffer buffer = createNativeOrderDirectFloatBuffer(array.length);
		for(float data:array)
		{
			buffer.put(data);
		}
		buffer.position(0);
		return buffer;
	}

}
