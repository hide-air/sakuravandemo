package net.zmap.android.pnd.v2.common;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.exception.NaviInvalidValueException;
import net.zmap.android.pnd.v2.api.overlay.Icon;
import net.zmap.android.pnd.v2.common.utils.DbHelper;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
//import android.app.Activity;
//import android.content.Context;
//import android.location.Location;


// ナビ位置情報以外を格納するクラス
public class DeliveryInfo {
	public int mMyIndex;            // 自分自身のインデックス
	public int mMyButtonPos;        // 自分自身のボタンの位置
    public int mCarNo;				// PK
    public int mDeliveryOrder;      // 配達順
    public String mStartDate;		// 指定開始年月日
    public String mEndDate;			// 指定終了年月日
    public int mCustID;				// 顧客ID
    public String mCustCode;		// 顧客コード
    public String mCustmer;			// 世帯主名称
    public String mTelNo;			// 電話番号
    public String mAddress;			// 住所（都道府県）
    public String mAddress2;		// 住所（市区町村）
    public String mAddress3;		// 住所（大字以降）
    public int mMatchLebel;			// マッチングレベル
    public int mChangeFlag;			// 変更フラグ
    public int mHouseFigure;		// 家屋形状
    public int mHousehold;			// 世帯構成
    public int mSex;				// 世帯構成
    public int mGeneration;			// 年代
    public String mContract;		// 顧客契約状況
    public String mSeiyakuDate;		// 成約年月日
    public String mTorihikiDate;	// 取引開始年月日
    public String mKaiyakuDate;		// 解約年月日
    public String mKaiyakuReason;	// 解約理由
    public String mKaishuKind;		// 回収種別
    public String mCollectingMemo;	// 集金コメント
    public int mCustPoint;			// ポイント
    public String mDeliCourse;		// 配達コース
    public int mDeliOrder;			// 設定配達順
    public String mDeliComment;		// 配達コメント
    public String mMapNo;			// 図番
    public String mBoxPlace;		// BOX位置
    public String mContactingMemo;	// 連絡メモ
    public String mBirthDay;		// 誕生日
    public String mProduct;			// 商品名称
    public String mProductRyaku;	// 商品略称
    public int mProductCnt;			// 配達数量
    public GeoPoint mPoint;			// 位置情報
    public Icon mMapIcon;           // 地図上のアイコン
	public int mDeliveryStatus;	// 配達結果
// カメラ連携等 Start
    public int mPhotoNum; // 撮影画像ファイル数
    public static final String PHOTO_PATH = "Picture/Camera/"; // 撮影したファイルの相対パス
    public String mPhotoPath; // ItsmoNaviDriveからの相対パス
    public String[] mPhotos; // 撮影画像ファイルのパス
    public static final int PHOTO_MAX_NUM = 5; // 最大撮影画像ファイル数
    public static final String MEMO_PATH = "Picture/tegaki/"; // 手書きメモファイルの相対パス
    public String mHandWrittenMemoPath; // ItsmoNaviDriveからの相対パス
    public String mHandWrittenMemo; // 手書きメモ ファイル名
// カメラ連携等 End
	
	public void reflectToIcon()
	{
		if (mMatchLebel >= 2)
		{
// Mod by M.Suna '14-05-05
//			String iconPath = DbHelper.ICON_PATH + mMatchLebel + ".png";
//			String iconPath = DbHelper.ICON_PATH + (mMyIndex + 1) + ".png";
//			String iconPath = DbHelper.ICON_PATH + (mDeliveryOrder + 1) + ".png";
//			String iconPath = DbHelper.ICON_PATH + (mDeliOrder + 1) + ".png";
		    String iconPath;
			//if (mDeliveryOrder <= 200)
		    if (mMyIndex <= 199)
			{	
			    //iconPath = DbHelper.ICON_PATH + (mDeliveryOrder) + ".png";
		    	iconPath = DbHelper.ICON_PATH + (mMyIndex + 1) + ".png";
			} else {
				iconPath = DbHelper.ICON_PATH + "200over.png";
			}
			//JNITwoLong tky = new JNITwoLong();
			/*
			JNITwoLong wgs = new JNITwoLong();
			NaviRun.JNI_LIB_dtmConvByDegree(
					//Constants.DTAUM_INDEX_TOKYO, Constants.CONVERT_FROM_WGS84,
					//mPoint.getLongitudeMs(), mPoint.getLatitudeMs(), tky);
					Constants.CONVERT_FROM_WGS84, Constants.DTAUM_INDEX_TOKYO,
					mPoint.getLongitudeMs(), mPoint.getLatitudeMs(), wgs);
			*/
			//JNITwoLong tky = new JNITwoLong();
			//tky.setM_lLat(mPoint.getLatitudeMs());
			//tky.setM_lLong(mPoint.getLongitudeMs());
			//CommonLib.c
			//Location wgs = CommonLib.ConvertDBCoordinateSystemToWGS(tky);
			//GeoPoint wgsPnt = new GeoPoint(wgs.getLatitude(), wgs.getLongitude());
			
			// 実は何故かLongitude（経度）とLatitude（緯度）が逆になっている。バグのようだ。
			//GeoPoint tkyPnt = new GeoPoint(tky.getM_lLong(), tky.getM_lLat());
			//GeoPoint wgsPnt = new GeoPoint(wgs.getM_lLong(), wgs.getM_lLat());
			if (mMapIcon == null)
			{
				//Icon.Builder iconBuilder = new Icon.Builder(wgsPnt, iconPath, mCustmer);
				Icon.Builder iconBuilder = new Icon.Builder(mPoint, iconPath, mCustmer);
				//Icon.Builder iconBuilder = new Icon.Builder(tkyPnt, iconPath, mCustmer);
				mMapIcon = iconBuilder.build();
		    }
			else
			{
				try
				{
					//mMapIcon.setPoint(wgsPnt);
					mMapIcon.setPoint(mPoint);
					//mMapIcon.setPoint(tkyPnt);
				}
				catch (NaviInvalidValueException e)
				{
					e.printStackTrace();
				}
				mMapIcon.setUrl(iconPath);
			}
		}
	}
	
	/** 未配達 */
	public static final int DELIVERY_STATUS_NOTDELIVERED = 0;
	/** 配達済み */
	public static final int DELIVERY_STATUS_DELIVERED = 1;
	/** 配達中止 */
	public static final int DELIVERY_STATUS_ABORTED = 2;
	/** 保留 */
	public static final int DELIVERY_STATUS_RESERVED = 3;
}

/*  
public class DeliveryInfo {
    public int mCarNo;
    public int mCustID;
    public int mCustCode;
    public String mCustmer;
    public String mCustTel;
    public String mAddress;
    public String mAddress2;
    public String mAddress3;
    public int mMatchLebel;
    public int mModFlag;
    public String mProduct;
    public int mProductCnt;
    public String mLastDate;
    public int mClaim;
    public GeoPoint mPoint;
}
*/