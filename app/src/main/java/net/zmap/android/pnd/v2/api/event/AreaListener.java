package net.zmap.android.pnd.v2.api.event;

import android.location.Location;


/**
 * 行政界移動リスナインタフェース
 *
 * @see Area
 */
public interface AreaListener {
    /**
     * 行政界が変化したときに呼ばれるリスナ関数 (車モードのみ)
     *
     * @param location
     * @param from
     * @param to
     */
    public void onChanged(Location location, Area from, Area to);
}
