package net.zmap.android.pnd.v2.api.event;

import net.zmap.android.pnd.v2.api.event.VehiclePositionEx;

/**
 * 自車位置更新リスナインタフェース
 */
public interface VehiclePositionExListener {
    /**
     * 自車位置が更新されたときに呼び出されるリスナ (車モードのみ)<br>
     * ルートシミュレーション中などで画面が更新されている場合でも、
     * シミュレーション中の自車位置ではなく、実際の自車位置を返す
     * @param location 自車位置情報
     */
    public void onChanged(VehiclePositionEx vehiclePositionEx);
}
