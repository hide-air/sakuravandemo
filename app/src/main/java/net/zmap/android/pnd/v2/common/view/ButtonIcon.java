package net.zmap.android.pnd.v2.common.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.Button;

public class ButtonIcon extends Button
{

	private Bitmap m_oIcon = null;

	private int m_wPaddingLeft = 0;
	private int m_wPaddingRight = 0;
	private int m_wPaddingTop = 0;
	private int m_wPaddingBottom = 0;

	private int m_wIconW = 0;
	private int m_wIconH = 0;
	private int m_wIconSpace = 0;

	public ButtonIcon(Context context)
	{
		super(context);
	}

	public ButtonIcon(Context context,AttributeSet oSet)
	{
		super(context,oSet);
	}

	public void setIcon(Bitmap oBitmap)
	{
		m_oIcon = oBitmap;
	}

	public void setIcon(int wId)
	{
		m_oIcon = BitmapFactory.decodeResource(getResources(), wId);
	}

	public void setPadding(int wL,int wT,int wR,int wB)
	{
		m_wPaddingLeft = wL;
		m_wPaddingTop = wT;
		m_wPaddingRight = wR;
		m_wPaddingBottom = wB;
		resetIconSize(getWidth(), getHeight());
	}

	public void setIconSpace(int wSpace)
	{
		m_wIconSpace = wSpace;
		resetIconSize(getWidth(), getHeight());
	}

	@Override
	protected void onDraw(Canvas oCanvas)
	{
		if(m_oIcon != null)
		{
			int wY = (getHeight() - m_wIconH)/2;
			Rect oOldRect = new Rect(0, 0, m_oIcon.getWidth(), m_oIcon.getHeight());
			Rect oNewRect = new Rect(m_wPaddingLeft, wY,m_wPaddingLeft + m_wIconW,wY + m_wIconH);
			oCanvas.drawBitmap(m_oIcon, oOldRect, oNewRect, null);
		}
		super.onDraw(oCanvas);
	}

	@Override
	protected void onSizeChanged(int wW, int wH, int oldw, int oldh)
	{
		super.onSizeChanged(wW, wH, oldw, oldh);
		resetIconSize(wW, wH);
	}

	private void resetIconSize(int wW,int wH)
	{
		if(m_oIcon != null)
		{
			m_wIconH = wH - getPaddingTop() - getPaddingBottom();
			m_wIconW = m_wIconH * m_oIcon.getWidth() / m_oIcon.getHeight();
			super.setPadding(m_wPaddingLeft + m_wIconW + m_wIconSpace,
					m_wPaddingTop, m_wPaddingRight, m_wPaddingBottom);
		}
	}



}
