package net.zmap.android.pnd.v2.api.overlay;

import android.os.Parcel;
import android.os.Parcelable;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.exception.NaviInvalidValueException;
import net.zmap.android.pnd.v2.api.util.GeoCheckUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * ライン情報
 */
public final class Line extends OverlayItem {

    /*　ライン描画パラメータ */
    private LineDrawParam  mDrawParam;

    /*　ライン座標点列 */
    private List<GeoPoint> mPath = new ArrayList<GeoPoint>();

    /**
     * コンストラクタ
     * @param drawParam ライン描画パラメータ
     */
    public Line(LineDrawParam drawParam) {
        super();
        mDrawParam = drawParam;
    }

    /**
     * 描画パラメータ取得<br>
     *
     * @return　描画パラメータ
     */
    public LineDrawParam getDrawParam() {
        return mDrawParam;
    }

    /**
     * 描画パラメータ設定<br>
     *
     * @param drawParam
     *            描画パラメータ
     */
    public void setDrawParam(LineDrawParam drawParam) {
        mDrawParam = drawParam;
    }

    /**
     * ライン座標点列取得
     *
     * @return　ライン座標点列
     */
    public List<GeoPoint> getPath() {
        return mPath;
    }

    /**
     * ライン座標点列数取得
     * @return ライン座標点列数
     */
    public int getNumberOfPoints() {
        return mPath.size();
    }

    /**
     * インデックス指定ライン座標点取得
     *
     * @param index
     *            インデックス
     * @return ライン座標点
     */
    public GeoPoint getPoint( int index ) {
        return mPath.get(index);
    }


    /**
     * ライン座標点列設定
     *
     * @param path
     *            ライン座標点列
     * @throws NaviInvalidValueException 不正な値、無効値
     */
    public void setPath(List<GeoPoint> path) throws NaviInvalidValueException {
        for (GeoPoint point : path) {
            if (!validateRangeLocation(point)) {
                throw new NaviInvalidValueException();
            }
        }
        mPath = path;
    }

    /**
     * ライン座標点追加
     *
     * @param point
     *            ライン座標点
     * @throws NaviInvalidValueException 不正な値、無効値
     */
    public void addPoint(GeoPoint point) throws NaviInvalidValueException {
        if (!validateRangeLocation(point)) {
            throw new NaviInvalidValueException();
        }
        mPath.add(new GeoPoint(point));
    }

    /**
     * ライン座標点追加
     *
     * @param latitude
     *            経度
     * @param longitude
     *            緯度
     */
    public void addPoint(double latitude, double longitude) {
        mPath.add(new GeoPoint(latitude, longitude));
    }

    private Line(Parcel in) {
        super(in);
        mDrawParam = in.readParcelable(LineDrawParam.class.getClassLoader());
        mPath = in.createTypedArrayList(GeoPoint.CREATOR);
    }

    /* (非 Javadoc)
     * @see net.zmap.android.pnd.v2.agenthmi.api.data.NaviPoint#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeParcelable(mDrawParam, flags);
        out.writeTypedList(mPath);
    }

    /* (非 Javadoc)
     *
     */
    public static final Parcelable.Creator<Line> CREATOR = new Parcelable.Creator<Line>() {
        public Line createFromParcel(Parcel in) {
            return new Line(in);
        }

        public Line[] newArray(int size) {
            return new Line[size];
        }
    };

    /*
     * (非 Javadoc)
     * @see net.zmap.android.pnd.v2.api.overlay.OverlayItem#describeContents()
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * ポイント座標点列 空判定
     *
     * @return true : 空 / false : 空ではない
     */
    public boolean isEmptyLocations() {
        if (mPath == null || mPath.size() == 0 ) {
            return true;
        }
        return false;
    }

    /**
     * ポイント座標点列 有効範囲内チェック
     *
     * @return true : 範囲内 / false : 範囲外
     */
    private boolean validateRangeLocation(GeoPoint point) {
        if (point == null) {
            return false;
        }
        return GeoCheckUtils.inBoundMapWGS(point);
    }
}
