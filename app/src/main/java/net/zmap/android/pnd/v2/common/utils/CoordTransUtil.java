// 2012/02/29 ���W�n�ϊ����s��Utility

package net.zmap.android.pnd.v2.common.utils;

import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;


public class CoordTransUtil  {
    
    // WGS���W�n�̓x->msec�ϊ�
    public static long[] degreeToMsec(float fLatitude, float fLongitude) {
        long[] msec = new long[2];
        msec[0] = (long)(fLatitude * 100000  ) * 36; // �ܓx(WGS�n�~���b)
        msec[1] = (long)(fLongitude * 100000 ) * 36; // �o�x(WGS�n�~���b)
        return msec;
    }
    
    // WGS���W�n(msec) -> ��{���n�n(msec)
    public static long[] wgs2tky(long latitude, long longitude) {
        long[] tky = new long[2];
/*
        tky[0] = (long)(latitude + 0.000106960 * latitude - 0.000017467 * longitude - 0.0046020 * 3600 * 1000);
        tky[1] = (long)(longitude + 0.000046047 * latitude + 0.000083049 * longitude - 0.0100410 * 3600 * 1000);
*/
        JNITwoLong lonLat = new JNITwoLong();
            NaviRun.JNI_LIB_dtmConvByDegree(Constants.DTAUM_INDEX_TOKYO,
                        Constants.CONVERT_FROM_WGS84,
                        longitude,
                        latitude,
                        lonLat);
            tky[0] = lonLat.getM_lLong();
            tky[1] = lonLat.getM_lLat();
        return tky;
    }
    
    // ��{���n�n -> WGS���W�n(msec)
    public static long[] tky2wgs(long latitude, long longitude) {
        long[] wgs = new long[2];
/*
        wgs[0] = (long)(latitude - 0.000106950 * latitude + 0.000017464 * longitude + 0.0046017 * 3600 * 1000);
        wgs[1] = (long)(longitude - 0.000046038 * latitude - 0.000083043 * longitude - 0.0100400 * 3600 * 1000);
*/
        JNITwoLong lonLat = new JNITwoLong();
            NaviRun.JNI_LIB_dtmConvByDegree(Constants.CONVERT_FROM_WGS84,
                        Constants.DTAUM_INDEX_TOKYO,
                        longitude,
                        latitude,
                        lonLat);
            wgs[0] = lonLat.getM_lLong();
            wgs[1] = lonLat.getM_lLat();
        return wgs;
    }
}
