package net.zmap.android.pnd.v2.maps;

import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.text.TextUtils.TruncateAt;
import android.view.Display;
import android.view.WindowManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.api.ItsmoNaviDriveExternalApi;
import net.zmap.android.pnd.v2.api.exception.NaviException;
import net.zmap.android.pnd.v2.api.map.ZoomLevel;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.DeliveryInfo;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.utils.ScreenAdapter;
import net.zmap.android.pnd.v2.common.view.NaviStateIcon;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.sakuracust.DebugLogOutput;
import net.zmap.android.pnd.v2.sakuracust.UserLineInfo;
//import net.zmap.android.pnd.v2.dricon.controller.DriverContentsCtrl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangdong
 *
 */
public class MapTopView {

    private List<View> visible = new ArrayList<View>();
    private List<View> invisible = new ArrayList<View>();

    /** Right Layout */
    private LinearLayout moreInfo;
    private List<Button> route5List = new ArrayList<Button>();
    private LinearLayout layoutRoute5;

    /** デモ開始 */
    private Button naviDemoStart;
    /** 案内開始 */
    private Button naviStart;
    /** Menu */
    private Button btnMainMenu;
    /** 現在地 */
    private Button btnLocation;

//    private Button btnDriverContentsMenu;
    /** 表示切替 */
//    private Button btnShow;
//Add 2011/12/14 Z01_h_yamada Start -->
	LinearLayout mTopLeftGrayLayout;
//	LinearLayout mTopLeftGrayLayout2;
	LinearLayout mTopRightGrayLayout;
	LinearLayout mTopRightGrayLayout2;
//Add 2011/12/14 Z01_h_yamada End <--

 // 2013/10/17 M.Suna Start -->
/*    
    private Button btnShowResidentialMap; // 住宅地図表示
    private Button btnShowResidentialMapR; // 住宅地図表示(右)
    //private Button btnTapOrderSettingR; // タップ順設定
    private Button btnTapOrderNAVI; // タップ順NAVI
    private Button btnTapOrderSetting; // タップ順設定
*/    
// 2013/10/17 M.Suna End <--

    /** 周辺 */
    private Button btnSurround;
    private Button btnSurroundMore;
// Add by CPJsunagawa '13-12-25 Start
    /** 注意事項 */
    private Button btnCaution;
    /** リスト表示 */
    private Button btnShowList;
// Add by CPJsunagawa '13-12-25 End
    /** 目的地 */
    private Button btnDestInquiry;
    /** 自宅 */
    private Button btnHome;
    /** もどる */
    private Button btnBack;
    /** アロー終了 */
    private Button btnBikeExit;
    /** 取り消す */
    private Button btnRouteCancel;
//Add 2011/09/17 Z01_h_yamada Start -->
    /** モード */
    private NaviStateIcon naviStateIcon=null;
//Add 2011/09/17 Z01_h_yamada End <--
//Add 2011/09/05 Z01_h_yamada Start -->
    /** デモ終了 */
    private Button btnDemoExit;
//Add 2011/09/05 Z01_h_yamada End <--
    /** 案内終了 */
    private Button btnGuideExit;
    /** ﾙｰﾄ編集 */
    private Button btnRouteEdit;
// Add by CPJsunagawa '2015-07-31 Start
    /** アイコン編集 */
    private Button btnIconEdit;
// Add by CPJsunagawa '2015-07-31 End

    /** ここに行く */
    private Button navi_here;
    /** お気に入り */
    private Button btnFavorite;
    /** 地図更新 */
//    private Button mapUpdate;
    /** 5ルート */
    private Button route_5;

    /** 全ルートの距離 */
    private TextView txtRouteDistance = null;
    /** 全ルートの料金 */
    private TextView txtRouteUseTime = null;
    /** 到着時刻 */
    private TextView txtcoin = null;

// Add by CPJsunagawa '13-12-25 Start
    /** 現在マッチング中の住所と顧客名 */
    private TextView matCustName = null;
    private TextView matAdr = null;
// Add by CPJsunagawa '13-12-25 End

    private LayoutInflater inflater;

    private boolean isShowMode = true;

    private MapActivity activity;
    private MapView mapView;
    private RelativeLayout[] oLayout = new RelativeLayout[10];

    private int[] layout_land = new int[] {
            R.layout.map_top_land,// normal_map
            R.layout.map_top_land_search,// open_map
            R.layout.map_top_land_route,// route_map
            R.layout.map_top_land_navi,// navi_map
            R.layout.map_top_land_navi,// simulation_navi_map
            R.layout.map_top_land_route,// five_route_map
            R.layout.map_top_land_route_bike, // アローモード画面
    };
    private int[] layout_port = new int[] {
            R.layout.map_top_port,// normal_map
            R.layout.map_top_port_search,// open_map
            R.layout.map_top_port_route,// route_map
            // XuYang modi start #1022
            R.layout.map_top_port_navi_map,// navi_map
            // XuYang modi end #1022
            -1,// simulation_navi_map
            R.layout.map_top_land_route,// five_route_map
            R.layout.map_top_port_route_bike, // アローモード画面
    };

    private int[] layout_moreInfo = new int[] {
            R.layout.map_more_info_right, // normal_map
            R.layout.map_more_info_right, // open_map
            R.layout.map_more_info_right_route, // route_map
            -1,// navi_map
            -1,// simulation_navi_map
            R.layout.map_more_info_right_route_5,// five_route_map
            // XuYang modi start #1029
            -1, // アローモード画面
    // XuYang modi end #1029
    };
    private RelativeLayout m_oTopView;

    private GuideView guideView;
    private boolean isConfigChanged = true;
    private int curMainLayoutId = -1;
    private int curMoreLayoutId = -1;

    private int curMode = -1;
    private int preMode = -1;

    private boolean isMapMove = false;

    public MapTopView(MapActivity context, MapView mapView) {
        setMapEvent(context);
        this.mapView = mapView;

        m_oTopView = new RelativeLayout(context);
        inflater = LayoutInflater.from(context);
    }

    /**
     * 現在のモードによると、View初期化
     */
    private void init() {
        //
        if (curMode >= 0) {
            CommonLib.clearViewInLayout(oLayout[curMode]);
            CommonLib.clearViewInLayout(moreInfo);
        }

        if (curMainLayoutId != -1) {
            initBottomView();
        }

//Del 2011/12/15 Z01_h_yamada Start -->
//        initOtherView();
//Del 2011/12/15 Z01_h_yamada End <--

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.FILL_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        m_oTopView.addView(oLayout[curMode], params);

//Add 2011/12/15 Z01_h_yamada Start -->
        initOtherView();
//Add 2011/12/15 Z01_h_yamada End <--

    }

    /**
     * Change mode flag before mode changed.
     */
    private void initConfigChange() {

        final int index = mapView.getMap_view_flag();
        int resId = (!ScreenAdapter.isLandMode() && mapView.isEnableSizeChange()) ? layout_port[index] : layout_land[index];
        int moreId = layout_moreInfo[index];

// MOD.2013.02.13 N.Sasao Bug #9269 地点地図画面でボタン位置ズレ対応 START
        isConfigChanged = (curMainLayoutId != resId) || (curMoreLayoutId != moreId) || (ScreenAdapter.isCurrentSizeChange());
// MOD.2013.02.13 N.Sasao Bug #9269 地点地図画面でボタン位置ズレ対応  END

        if (isConfigChanged) {
            curMainLayoutId = resId;
            curMoreLayoutId = moreId;

            preMode = curMode;
            curMode = index;
        }

        if (mapView != null) {
        	mapView.onNaviModeChange();
        }
    }

    /**
     * Update/init Main View (Location Page Button layout)
     */
    private void initBottomView() {
        if (curMode >= 0) {
            m_oTopView.removeView(oLayout[curMode]);
        }
        oLayout[curMode] = (RelativeLayout)inflater.inflate(curMainLayoutId, null);
        initView(oLayout[curMode]);
    }

    /**
     * init view children.
     *
     * @param layout
     *            view layout
     */
    private void initView(RelativeLayout layout) {
        if (layout != null) {
            //Log.i("MapTopView", "initView");
            // Menu
            btnMainMenu = (Button)layout.findViewById(R.id.main_menu);
            if (btnMainMenu != null) {
//Chg 2011/09/05 Z01_h_yamada Start -->
//	            if (curMode == Constants.SIMULATION_MAP_VIEW) {
//	            	btnMainMenu.setEnabled(false);
//	            } else {
//	                btnMainMenu.setEnabled(true);
//	            }
//--------------------------------------------
                if (curMode == Constants.SIMULATION_MAP_VIEW) {
                    btnMainMenu.setVisibility(View.GONE);
                } else {
                    btnMainMenu.setVisibility(View.VISIBLE);
                }
//Chg 2011/09/05 Z01_h_yamada End <--

                btnMainMenu.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 メニュー禁止 Start -->
                    	if(DrivingRegulation.CheckDrivingRegulation()){
                    		return;
                    	}
// ADD 2013.08.08 M.Honma 走行規制 メニュー禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                        activity.goMainMenu();
                    }
                });
            }

            // 現在地
            btnLocation = (Button)layout.findViewById(R.id.location);
            if (btnLocation != null) {
                btnLocation.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
// Add by CPJsunagawa '13-12-25 Start
                    	if (activity.IsManualMatching())
                    	{
                    		try
                    		{
                    			activity.setPosAsNowGPS();
                    		}
                    		catch (NaviException e)
                    		{
                    			e.printStackTrace();
                    		}
                    		return;
                    	}
// Add by CPJsunagawa '13-12-25 End
                        if (mapView.isMapScrolled()) {
                            OpenMap.locationBack(activity);
                            activity.goLocation();
                        } else if (null != guideView) {
                            guideView.setGuideListToCarPos();
                        }
                    }
                });
            }

////Add 2011/09/22 Z01thedoanh Start -->
//            btnDriverContentsMenu = (Button)layout.findViewById(R.id.main_drivecontents);
//            if (btnDriverContentsMenu != null) {
//            	if(DriverContentsCtrl.getController() != null){
//	            	setDriveContentsEnable(DriverContentsCtrl.getController().isGetContents());
//            	}else{
//	            	setDriveContentsEnable(false);
//            	}
//                btnDriverContentsMenu.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツメニュー禁止 Start -->
//                    	if(DrivingRegulation.CheckDrivingRegulation()){
//                    		return;
//                    	}
//// ADD 2013.08.08 M.Honma 走行規制 ドライブコンテンツメニュー禁止 End <--
//                        setDriverContentsMenuClickable(false);
//                        DriverContentsCtrl.getController().sendEventCode(Constants.DC_SHOW_LIST_MENU, activity);
//                    }
//                });
//            }
////Add 2011/09/22 Z01thedoanh End <--

            // 表示切替
            /*btnShow = (Button) layout.findViewById(R.id.show_mode);
            if (btnShow != null) {
                btnShow.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isMapMove
                                && (curMode == Constants.COMMON_MAP_VIEW || curMode == Constants.ROUTE_MAP_VIEW_BIKE)) {

                        }else{
                            isShowMode = !isShowMode;
                        }
                        setShowHide();
                    }
                });
            }*/

// Add by CPJsunagawa '13-12-25 Start
/*
            // 注意事項
            btnCaution = (Button)layout.findViewById(R.id.caution);
            if( btnCaution != null) {
                btnCaution.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        activity.showCaution();
                    }
                });
            }
*/

// added by furusawa Start
            // リスト表示・非表示
            View tmpv = layout.findViewById(R.id.showList);
            //btnShowList = (Button)layout.findViewById(R.id.showList);
            btnShowList = (Button)tmpv;
            
            if( btnShowList != null) {
                System.out.println("activity.getShowFlag() = " + activity.getShowFlag() );
                NaviApplication app = (NaviApplication)activity.getApplication();
                //if (app.getInfoListSize() == 0)
                if (!app.isExistInfoList())
                {
                	btnShowList.setVisibility(View.GONE);
                }
                else
                {
                	btnShowList.setVisibility(View.VISIBLE);
	                if( activity.getShowFlag() ) {
	                    btnShowList.setText("リスト\n非表示");
	                } else {
	                    btnShowList.setText("リスト\n表示");
	                }
	                
	                btnShowList.setOnClickListener(new OnClickListener() {
	                    public void onClick(View v) {
	                        Button btn = (Button)v;
	                        boolean flag;
	                        if( "リスト\n非表示".equals(btn.getText().toString()) ) {
	                            btn.setText("リスト\n表示");
	                            flag = false;
	                        } else {
	                            btn.setText("リスト\n非表示");
	                            flag = true;
	                        }
	                        activity.showList(flag, false, false);
	                        mapView.updateMap();
	                    }
	                });
                }
            }
// added by furusawa End
            
            // マッチング中の顧客名
            tmpv = layout.findViewById(R.id.displayCust);
            matCustName = (TextView)tmpv;
// Add by CPJsunagawa '2015-05-13 Start
        	if (matCustName != null)
            {
	            matCustName.setSingleLine(true);
	            matCustName.setEllipsize(TruncateAt.START);
            }
// Add by CPJsunagawa '2015-05-13 End

            // マッチング中の住所
            tmpv = layout.findViewById(R.id.displayAdr);
            matAdr = (TextView)tmpv;
// Add by CPJsunagawa '2015-05-13 Start
            if (matAdr != null)
            {
	            matAdr.setSingleLine(true);
	            matAdr.setEllipsize(TruncateAt.START);
            }
// Add by CPJsunagawa '2015-05-13 End

// 2013/10/17 y_matsumoto Start -->  これはj不要？
            // 住宅地図表示
      /*      
            btnShowResidentialMap = (Button)layout.findViewById(R.id.showResidentialMap);
            if (btnShowResidentialMap != null) {
                btnShowResidentialMap.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.showResidentialMap();
                    }
                });
            }
           */ 
/*            
            // タップ順NAVI
            btnTapOrderNAVI = (Button)layout.findViewById(R.id.tap_order_navi);
            if (btnTapOrderNAVI != null) {
                btnTapOrderNAVI.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.goTapOrderNAVI();
                    }
                });
            }
            // タップ順設定
            btnTapOrderSetting = (Button)layout.findViewById(R.id.tap_order_setting);
            if (btnTapOrderSetting != null) {
                btnTapOrderSetting.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.goTapOrderSetting();
                    }
                });
            }
*/            
// 2013/10/17 y_matsumoto End <--
// Add by CPJsunagawa '13-12-25 End
            // 周辺
            btnSurround = (Button)layout.findViewById(R.id.surround);
            if (btnSurround != null) {
                btnSurround.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 周辺禁止 Start -->
                    	if(DrivingRegulation.CheckDrivingRegulation()){
                    		return;
                    	}
// ADD 2013.08.08 M.Honma 走行規制 周辺禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                        activity.goSurroundInquiry(false);
                    }
                });
            }

            // 目的地
            btnDestInquiry = (Button)layout.findViewById(R.id.dest);
            if (btnDestInquiry != null) {
                btnDestInquiry.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地禁止 Start -->
                    	if(DrivingRegulation.CheckDrivingRegulation()){
                    		return;
                    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                        activity.goDestInquiry();
                    }
                });
            }

            // 自宅
            btnHome = (Button)layout.findViewById(R.id.home);
            if (btnHome != null) {
//Chg 2011/09/05 Z01_h_yamada Start -->
//            	if (curMode == Constants.SIMULATION_MAP_VIEW) {
//                	btnHome.setEnabled(false);
//                } else {
//                    btnHome.setEnabled(true);
//                }
//--------------------------------------------
                if (curMode == Constants.SIMULATION_MAP_VIEW) {
                    btnHome.setVisibility(View.GONE);
                } else {
                    btnHome.setVisibility(View.VISIBLE);
                }
//Chg 2011/09/05 Z01_h_yamada End <--

                btnHome.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 自宅禁止 Start -->
                    	if(DrivingRegulation.CheckDrivingRegulation()){
                    		return;
                    	}
// ADD 2013.08.08 M.Honma 走行規制 自宅禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                        activity.onClickHome();
                    }
                });
            }

            // もどる
            btnBack = (Button)layout.findViewById(R.id.modoru);
            if (btnBack != null) {
                btnBack.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.goBack();
                    }
                });
            }

            // アロー終了
            btnBikeExit = (Button)layout.findViewById(R.id.bike_exit);
            if (btnBikeExit != null) {
                btnBikeExit.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.clearBicycleNaviData();
                    }
                });
            }

            // 取り消す
            btnRouteCancel = (Button)layout.findViewById(R.id.route_cancel);
            if (btnRouteCancel != null) {
                btnRouteCancel.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.onClickRouteCancel();
                    }
                });
// Add by CPJsunagawa 2015-05-13 Start
                NaviApplication na = activity.getNaviApplication();
                if (na.getMatchigMode() >= 1)
                {
                	// 文字列を「戻る」に差し替える
                	btnRouteCancel.setText(R.string.maptop_tap_order_return);
                }
             }
// Add by CPJsunagawa 2015-05-13 End

//Add 2011/09/17 Z01_h_yamada Start -->
            // モード
            naviStateIcon = (NaviStateIcon)layout.findViewById(R.id.mode);
            if (naviStateIcon != null) {
//Add 2011/11/22 Z01_h_yamada Start -->
                if (Constants.isDebug) {
//Add 2011/11/22 Z01_h_yamada End <--
	            	//「車モード」「歩行者モード」アイコン長押しで自車位置移動(デバッグ用)
	                naviStateIcon.setOnLongClickListener(new View.OnLongClickListener() {
	                    @Override
	                    public boolean onLongClick(View v) {
	                        // 自車位置の移動処理を呼ぶ
	                        activity.moveVehicle();
	                        return true;
	                    }
	                });
//Add 2011/11/22 Z01_h_yamada Start -->
                }
//Add 2011/11/22 Z01_h_yamada End <--

                UpdateNaviModeChange();
            }
//Add 2011/09/17 Z01_h_yamada End <--

            // デモ開始
            naviDemoStart = (Button)layout.findViewById(R.id.navi_demo_start);
            if (naviDemoStart != null) {
                if (!NaviRun.GetNaviRunObj().getShowSimulationButton()) {
                    setViewInvisible(naviDemoStart);
                }
                else {
                    int naviMode = CommonLib.getNaviMode();
                    if (naviMode == Constants.NE_NAVIMODE_MAN) {
                        setViewInvisible(naviDemoStart);
                    } else {
                        setViewVisible(naviDemoStart);
                    }
                    if (is5RouteMode()) {
                       setViewDisable(naviDemoStart);
                    }
                }

                naviDemoStart.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 デモ開始禁止 Start -->
                    	if(DrivingRegulation.CheckDrivingRegulation()){
                    		return;
                    	}
// ADD 2013.08.08 M.Honma 走行規制 デモ開始禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                        mapView.setSurfaceViewEnabled(false);
                        guideView.setVisible(true);
// Add by CPJsunagawa '13-12-25 Start
                        activity.hideList();
// Add by CPJsunagawa '13-12-25 End
                        setViewVisible(guideView.getView());
                        activity.startDemoGuide();
                    }
                });
            }

//Add 2011/09/05 Z01_h_yamada Start -->
            // デモ終了
            btnDemoExit = (Button)layout.findViewById(R.id.demo_navi_exit);
            if (btnDemoExit != null) {
                if (curMode == Constants.SIMULATION_MAP_VIEW) {
                    btnDemoExit.setVisibility(View.VISIBLE);
                } else {
                    btnDemoExit.setVisibility(View.GONE);
                }
                btnDemoExit.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        activity.showGuideEnd();
                    }
                });
            }
//Add 2011/09/05 Z01_h_yamada End <--

            // 案内終了
            btnGuideExit = (Button)layout.findViewById(R.id.navi_exit);
            if (btnGuideExit != null) {
//Chg 2011/09/05 Z01_h_yamada Start -->
//              if (curMode == Constants.SIMULATION_MAP_VIEW) {
//                  btnGuideExit.setText(R.string.demo_navi_exit);
//                  btnGuideExit.setBackgroundResource(R.drawable.btn_demo_navi_exit);
//              } else {
//                  btnGuideExit.setText(R.string.navi_exit);
//                  btnGuideExit.setBackgroundResource(R.drawable.btn_navi_exit);
//              }
//--------------------------------------------
                if (curMode == Constants.SIMULATION_MAP_VIEW) {
                    btnGuideExit.setVisibility(View.GONE);
                } else {
                    btnGuideExit.setVisibility(View.VISIBLE);
                }
//Chg 2011/09/05 Z01_h_yamada End <--

                btnGuideExit.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        activity.showGuideEnd();
                    }
                });
            }

            // ﾙｰﾄ編集
            btnRouteEdit = (Button)layout.findViewById(R.id.route_edit);
            if (btnRouteEdit != null) {
//Chg 2011/09/05 Z01_h_yamada Start -->
//              if (curMode == Constants.SIMULATION_MAP_VIEW) {
//                  btnRouteEdit.setEnabled(false);
//              } else {
//                  btnRouteEdit.setEnabled(true);
//              }
//--------------------------------------------
                if (curMode == Constants.SIMULATION_MAP_VIEW) {
                    btnRouteEdit.setVisibility(View.GONE);
                } else {
                    btnRouteEdit.setVisibility(View.VISIBLE);
                }
//Chg 2011/09/05 Z01_h_yamada End <--

                btnRouteEdit.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ルート編集禁止 Start -->
                    	if(DrivingRegulation.CheckDrivingRegulation()){
                    		return;
                    	}
// ADD 2013.08.08 M.Honma 走行規制 ルート編集禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                        activity.goRouteMain();
                    }
                });
            }

// Cut 20160118 以下の処理を通っていない 
/*
// Add by CPJsunagawa '2015-07-31 Start
            // 軌跡クリア、アイコン登録
            btnIconEdit = (Button)layout.findViewById(R.id.btn_TrackIconEdit);
            if (btnIconEdit != null) {
            	
        		System.out.println("**** MapTopViewのbtnIconEditがNULL以外 ****");
                btnIconEdit.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    	
                		System.out.println("**** MapTopViewのTrackIconEdit onClick() Start ****");

                		if(DrivingRegulation.CheckDrivingRegulation()){
                    		return;
                    	}
                        activity.goRouteMain();
                    }
                });
            }
// Add by CPJsunagawa '2015-07-31 End
*/
// Cut 20160118 以下の処理を通っていない 

//Add 2011/12/14 Z01_h_yamada Start -->
          	mTopLeftGrayLayout = (LinearLayout)layout.findViewById(R.id.maptopleft_layout);
            if (mTopLeftGrayLayout != null) {
            	// タッチ無反応領域
            	mTopLeftGrayLayout.setOnClickListener(new OnClickListener() {
            		@Override
            		public void onClick(View view) {
            		}
            	});
            }

//          	mTopLeftGrayLayout2 = (LinearLayout)layout.findViewById(R.id.maptopleft_layout2);
//            if (mTopLeftGrayLayout2 != null) {
//            	// タッチ無反応領域
//            	mTopLeftGrayLayout2.setOnClickListener(new OnClickListener() {
//            		@Override
//            		public void onClick(View view) {
//            		}
//            	});
//            }

            mTopRightGrayLayout = (LinearLayout)layout.findViewById(R.id.maptopright_layout);
            if (mTopRightGrayLayout != null) {
            	// タッチ無反応領域
            	mTopRightGrayLayout.setOnClickListener(new OnClickListener() {
            		@Override
            		public void onClick(View view) {
            		}
            	});
            }

            mTopRightGrayLayout2 = (LinearLayout)layout.findViewById(R.id.maptopright_layout2);
            if (mTopRightGrayLayout2 != null) {
            	// タッチ無反応領域
            	mTopRightGrayLayout2.setOnClickListener(new OnClickListener() {
            		@Override
            		public void onClick(View view) {
            		}
            	});
            }
//Add 2011/12/14 Z01_h_yamada End <--

            switch (curMode) {
                case Constants.COMMON_MAP_VIEW:
                    addVisible(btnSurround);
                    addVisible(btnDestInquiry);
                    addVisible(btnHome);
//Add 2011/12/14 Z01_h_yamada Start -->
                    addVisible(mTopRightGrayLayout);
                    addVisible(mTopRightGrayLayout2);
//Add 2011/12/14 Z01_h_yamada End <--
                    break;
                case Constants.NAVI_MAP_VIEW:
                    addVisible(btnHome);
                    addVisible(btnRouteEdit);
                    addVisible(btnGuideExit);
//Add 2011/12/14 Z01_h_yamada Start -->
                    addVisible(mTopRightGrayLayout);
                    addVisible(mTopRightGrayLayout2);
//Add 2011/12/14 Z01_h_yamada End <--
                    break;
                case Constants.SIMULATION_MAP_VIEW:
//                addVisible(btnHome);
//                addVisible(btnRouteEdit);
//                addVisible(btnGuideExit);
//                break;
                case Constants.ROUTE_MAP_VIEW:
                case Constants.OPEN_MAP_VIEW:
                case Constants.FIVE_ROUTE_MAP_VIEW:
                    break;
                case Constants.ROUTE_MAP_VIEW_BIKE:
                    addVisible(btnBikeExit);
                    addVisible(btnSurround);
                    addVisible(btnHome);
//Add 2011/12/14 Z01_h_yamada Start -->
                    addVisible(mTopRightGrayLayout);
                    addVisible(mTopRightGrayLayout2);
//Add 2011/12/14 Z01_h_yamada End <--
                    break;
                default:
                    break;
            }
// Add by CPJsunagawa '13-12-25 Start
            // タップ順Naviの「前へ」と「次へ」ボタン
            //Button btnBackward = (Button)layout.findViewById(R.id.route_backward);
            //Button btnForward = (Button)layout.findViewById(R.id.route_forward);
            //Button btnCount = (Button)layout.findViewById(R.id.deliv_count);
/*            
            if( isTapOrderNavi && btnBackward !=null && btnForward != null
                && CommonLib.Start_Demo_Guide_FLAG != 1) { // タップ順ルート表示中で、デモ中でない場合
                NaviApplication app = (NaviApplication)activity.getApplication();
                // 「前へ」ボタン
                btnBackward.setVisibility(View.VISIBLE);
                btnBackward.setEnabled(app.isEnableBackwardBtn() );
                btnBackward.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        backwardTapList();
                    }
                });
                // [次へ」ボタン
                btnForward.setVisibility(View.VISIBLE);
                btnForward.setEnabled(app.isEnableForwardBtn() );
                btnForward.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        forwardTapList();
                    }
                });*/
                // 数表示
/*                
                btnCount.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// 保留を取得する。
						
						NaviApplication app = (NaviApplication)activity.getApplication();
						int residx = app.getFirstReservedIndex();
						if (residx == -1)
						{
							// メッセージ
						}
						else
						{
							// そこへ飛ばす
							activity.setListAsIndex(residx);
						}
					}
				});
*/
                //reflectDeliveredCount(layout, true);
            //}
            showSakuraDeliveryControl(layout);
// Add by CPJsunagawa '13-12-25 End
            
        }
    }


    /**
     * init other View layout.
     */
    private void initOtherView() {
        initMoreInfo();
        init5Route();
    }

    /**
     * init right layout.
     */
    private void initMoreInfo() {

        if (layout_moreInfo[curMode] == -1) {
            return;
        }

        RelativeLayout.LayoutParams params = null;

        if (ScreenAdapter.isLandMode()) {
            params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.FILL_PARENT);
        } else {
            params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
//                    238);
            params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        }

        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        // More Info
        moreInfo = (LinearLayout)inflater.inflate(layout_moreInfo[curMode], null);
        //５ルートのボタンの下に「デモ開始」のボタンがいるため、
        //@layout/map_more_info_right_routeのみタッチイベントを抑制するため、
        //タッチイベント抑制の処理を入れた後にviewをaddする処理をしている
        if(layout_moreInfo[curMode] == R.layout.map_more_info_right_route_5) {
        	LinearLayout moreInfoRightRoot = (LinearLayout)inflater.inflate(R.layout.map_more_info_right_route, null);
        	moreInfoRightRoot.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
				}
			});
        	moreInfo.addView(moreInfoRightRoot
    				,new LinearLayout.LayoutParams(
        				RelativeLayout.LayoutParams.WRAP_CONTENT,
        				RelativeLayout.LayoutParams.FILL_PARENT));
        } else {
	        //地図画面タッチ時の右フレーム内でのボタン以外の領域のタッチ動作を抑制
	        moreInfo.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
				}
			});
        }
        CommonLib.clearViewInLayout(moreInfo);
        m_oTopView.addView(moreInfo, params);

        switch (curMode) {
            case Constants.ROUTE_MAP_VIEW:
            case Constants.FIVE_ROUTE_MAP_VIEW:
                setViewVisible(moreInfo);
                break;
            case Constants.OPEN_MAP_VIEW:
                addVisible(moreInfo);
                break;
            case Constants.COMMON_MAP_VIEW:
            case Constants.ROUTE_MAP_VIEW_BIKE:
                setViewInvisible(moreInfo);
                break;
            default:
                break;
        }

        // ここに行く
        navi_here = (Button)moreInfo.findViewById(R.id.navi_here);
        if (navi_here != null) {

            // 地点の追加
            if (CommonLib.isChangePosFromSearch()) {
//Chg 2011/09/17 Z01_h_yamada Start -->
//                navi_here.setBackgroundResource(R.drawable.btn_pos_add);
//--------------------------------------------
                navi_here.setText(R.string.maptop_point_add);
//Chg 2011/09/17 Z01_h_yamada End <--
            }

            // 地点の変更
            else if (CommonLib.isChangePos()) {
//Chg 2011/09/17 Z01_h_yamada Start -->
//              navi_here.setBackgroundResource(R.drawable.btn_pos_change);
//--------------------------------------------
                navi_here.setText(R.string.maptop_point_change);
//Chg 2011/09/17 Z01_h_yamada End <--
            }

// Add by CPJsunagawa '2015-07-08 Start
            // アイコン登録
            else if (CommonLib.isRegIcon()) {
                navi_here.setText(R.string.maptop_regist_icon);
            	System.out.println("***　ボタン名称を「アイコン登録」に変更　***");
            }

        	// テキスト入力
            else if (CommonLib.isInputText()) {
                navi_here.setText(R.string.maptop_Input_text);
            }
// Add by CPJsunagawa '2015-07-08 End

// Add by CPJsunagawa '2015-08-15 Start
            // アイコン移動
            else if (CommonLib.isMoveIcon()) {
                navi_here.setText(R.string.maptop_move_icon);
            	System.out.println("***　ボタン名称を「アイコン移動」に変更　***");
            }
        	
            // アイコン削除
            else if (CommonLib.isDeleteIcon()) {
                navi_here.setText(R.string.maptop_delete_icon);
            	System.out.println("***　ボタン名称を「アイコン削除」に変更　***");
            }
        	
            // ライン描画
            else if (CommonLib.isDrawLine()) {
                //navi_here.setText(R.string.maptop_draw_line);
            	Intent mapActInt = activity.getIntent();
            	int nowIdx = mapActInt.getIntExtra(Constants.INTENT_EXTRA_DRAW_LINE, -1);
            	//if (nowIdx <= -1) {return;}
            	
            	navi_here.setText("始点確定");
            	System.out.println("***　ボタン名称を「ライン描画」に変更　***");
            }
// Add by CPJsunagawa '2015-08-15 End

// Add by CPJsunagawa '2015-07-08 Start
/*
        	if (CommonLib.isRegIcon() || CommonLib.isInputText()) {
                navi_here.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(DrivingRegulation.CheckDrivingRegulation()){
                     	    return;
                     	}
                        executeUpDateMap(POS_CHANGE_UPDATE);
                     }
            });
*/
//Add by CPJsunagawa '2015-07-08 End

            // ここに行く（文言変更時）
//            if (CommonLib.isChangePos() || CommonLib.isChangePosFromSearch()) {
            if (CommonLib.isChangePos() || CommonLib.isChangePosFromSearch() || CommonLib.isRegIcon() || CommonLib.isMoveIcon() || CommonLib.isDeleteIcon() || CommonLib.isDrawLine() || CommonLib.isInputText()) {
            	System.out.println("***　地点変更、もしくはアイコン登録か？　***");
                navi_here.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    	System.out.println("***　「地点変更」もしくは「アイコン登録」ボタンクリック！　***");
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 地点の追加禁止 Start -->
                    	if(DrivingRegulation.CheckDrivingRegulation()){
                    		return;
                    	}
// ADD 2013.08.08 M.Honma 走行規制 地点の追加禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

// Add by CPJsunagawa '2015-07-08 Start
                    	//executeUpDateMap(POS_CHANGE_UPDATE);
                    	if(CommonLib.isRegIcon()) {
                        	System.out.println("***　アイコン登録クリック　***");
                    		
                        	registFigure(ID_REGIST_ICON);
                    	} else if(CommonLib.isMoveIcon()) {
                        	System.out.println("***　アイコン移動クリック　***");
                    		
                        	registFigure(ID_MOVE_ICON);
                    	} else if(CommonLib.isDeleteIcon()) {
                        	System.out.println("***　アイコン削除クリック　***");
                    		
                        	registFigure(ID_DELETE_ICON);
                    	} else if(CommonLib.isDrawLine()) {
                        	System.out.println("***　ライン描画クリック　***");
                    		
                        	// これは、ここではやらない。
                        	//registFigure(ID_DRAW_LINE);

                        	activity.drawLine(false);
                    	} else if(CommonLib.isInputText()) {
                        	System.out.println("***　テキスト入力クリック　***");

                    		registFigure(ID_INPUT_TEXT);
                    	} else {
                        	System.out.println("***　地点変更クリック　***");
                        	executeUpDateMap(POS_CHANGE_UPDATE);
                    	}
// Add by CPJsunagawa '2015-07-08 End

                    }
                });
                
                // ラインの時は、ロングタップのイベントを入れる。
                if (CommonLib.isDrawLine())
                {
                	navi_here.setOnLongClickListener(new View.OnLongClickListener() {
						
						@Override
						public boolean onLongClick(View v) {
							// ロングタップで確定させる。
							if (!CommonLib.isDrawLine()) return false;
							
							activity.drawLine(true);
							return true;
						}
					});
                }
            } else {
                // ここに行く（純粋なここへ行く）
                navi_here.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

// Add by CPJsunagawa '13-12-25 Start
                    	// リストをクリアする。
                    	NaviApplication na = (NaviApplication)activity.getApplication();
// ここでは行わない
/*                    	
                    	na.initInfoList();
                    	na.initProductList();
                    	na.initRouteList();
                    	na.clearCloseUpPoint();
                    	na.setNowRoutingCustomerIndex(keptDelivInfo);
*/
                    	if(keptDelivInfo != null) {
                    		na.setNowRoutingCustomerIndex(keptDelivInfo);
                    	} else {
                    		na.clearNowRoutingCustomerIndex();
                    	}
// Add by CPJsunagawa '13-12-25 End

// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ここに行く禁止 Start -->
                    	if(DrivingRegulation.CheckDrivingRegulation()){
                    		return;
                    	}
// ADD 2013.08.08 M.Honma 走行規制 ここに行く禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
//                        m_bCancel = true;
                        executeUpDateMap(GOTO_UPDATE);
                    }
                });
            }
        }

        // お気に入り
        btnFavorite = (Button)moreInfo.findViewById(R.id.favorite);
        if (btnFavorite != null) {
            btnFavorite.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 お気に入り禁止 Start -->
                	if(DrivingRegulation.CheckDrivingRegulation()){
                		return;
                	}
// ADD 2013.08.08 M.Honma 走行規制 お気に入り禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                    activity.goFavourite();
                }
            });
        }
        // 周辺
        btnSurroundMore = (Button)moreInfo.findViewById(R.id.surround);
        if (btnSurroundMore != null) {
//            if (CommonLib.isChangePosFromSearch() || CommonLib.isChangePos()) {
//                btnSurroundMore.setEnabled(false);
//            } else {
//                btnSurroundMore.setEnabled(true);
//            }

            btnSurroundMore.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 周辺禁止 Start -->
                	if(DrivingRegulation.CheckDrivingRegulation()){
                		return;
                	}
// ADD 2013.08.08 M.Honma 走行規制 周辺禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                    activity.goSurroundInquiry(CommonLib.isChangePosFromSearch() || CommonLib.isChangePos());
                	// xuyang add start bug1764
//Chg 2011/11/10 Z01_h_yamada Start -->
//                  if (mapView.getMap_view_flag() != Constants.COMMON_MAP_VIEW) {
//--------------------------------------------
                    if (mapView.getMap_view_flag() != Constants.COMMON_MAP_VIEW  && mapView.getMap_view_flag() != Constants.OPEN_MAP_VIEW) {
//Chg 2011/11/10 Z01_h_yamada End <--

                     // xuyang add end bug1764
                    	if (!(CommonLib.isChangePosFromSearch() || CommonLib.isChangePos())) {
                    	    OpenMap.locationBack(activity);
                    	}
                     // xuyang add start bug1764
                	}
                 // xuyang add end bug1764
                }
            });
        }

        // ﾙｰﾄ編集
        btnRouteEdit = (Button)moreInfo.findViewById(R.id.route_edit);
        if (btnRouteEdit != null) {
            if (is5RouteMode()) {
                setViewDisable(btnRouteEdit);
            }
            btnRouteEdit.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ルート編集禁止 Start -->
                	if(DrivingRegulation.CheckDrivingRegulation()){
                		return;
                	}
// ADD 2013.08.08 M.Honma 走行規制 ルート編集禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                    activity.goRouteEdit();
                }
            });
        }
        // 5ルート
        route_5 = (Button)moreInfo.findViewById(R.id.five_route);
        if (route_5 != null) {
            if (is5RouteMode()) {
                setViewDisable(route_5);
            }
            // 徒歩モードの場合、あるいは経由地がある時は非表示
// Mod by CPJsunagawa '13-12-25 Start
//            if (isHide5Route() || is5RouteMode()) {
            if (isHide5Route() || is5RouteMode() || activity.isExistInfoList()) {
// Mod by CPJsunagawa '13-12-25 End
                setViewInvisible(route_5);
            } else {
                setViewVisible(route_5);
            }
            route_5.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 5ルート禁止 Start -->
                	if(DrivingRegulation.CheckDrivingRegulation()){
                		return;
                	}
// ADD 2013.08.08 M.Honma 走行規制 5ルート禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                    activity.navi5Route();
                }
            });
        }

        // 案内開始
        naviStart = (Button)moreInfo.findViewById(R.id.navi_start);
        if (naviStart != null) {
            if (is5RouteMode()) {
                setViewDisable(naviStart);
            } else {
                setViewEnable(naviStart);
            }
// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
	        DebugLogOutput.put("RakuRaku-Log: 案内開始（MapTopView: 1）");
// Add 2015-05-28 CPJsunagawa SDカードにログ出力 end
            naviStart.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
// Add by CPJsunagawa '13-12-25 Start
                	activity.hideList();
                	enableTapOrderNaviMode(true);
// Add by CPJsunagawa '13-12-25 End
                	activity.navigationStart(1);
// Add 2015-05-28 CPJsunagawa SDカードにログ出力 start
	        DebugLogOutput.put("RakuRaku-Log: 案内開始（MapTopView: 2）");
// Add 2015-05-28 CPJsunagawa SDカードにログ出力 end
                }
            });
        }

        // Route Info
        if (curMode == Constants.ROUTE_MAP_VIEW
                || curMode == Constants.FIVE_ROUTE_MAP_VIEW) {
            txtRouteDistance = (TextView)moreInfo.findViewById(R.id.dist_info);
            txtcoin = (TextView)moreInfo.findViewById(R.id.spend_info);
            txtRouteUseTime = (TextView)moreInfo.findViewById(R.id.dest_time_info);
        }
// Add by CPJsunagawa '13-12-25 Start
/* 
        // 住宅地図表示(右メニュー)
        btnShowResidentialMapR = (Button)moreInfo.findViewById(R.id.showResidentialMapR);
        if (btnShowResidentialMapR != null) {
            if( mapView.getMap_view_flag() == Constants.OPEN_MAP_VIEW) {
                btnShowResidentialMapR.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.showResidentialMap();
                    }
                });
            } else { // 下と右に表示されてしまうので、右は隠す
                btnShowResidentialMapR.setVisibility(View.GONE);
            }
        }
*/
/*        
        // タップ順設定(右メニュー)
        btnTapOrderSettingR = (Button)moreInfo.findViewById(R.id.tap_order_settingR);
        if (btnTapOrderSettingR != null) {
            if( mapView.getMap_view_flag() == Constants.OPEN_MAP_VIEW) {
                btnTapOrderSettingR.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.goTapOrderSetting();
                    }
                });
            } else { // 下と右に表示されてしまうので、右は隠す
                btnTapOrderSettingR.setVisibility(View.GONE);
            }
        }
*/
/*        
        // タップ順Naviの「前へ」と「次へ」ボタン
        Button btnBackward = (Button)moreInfo.findViewById(R.id.route_backward);
        Button btnForward = (Button)moreInfo.findViewById(R.id.route_forward);
        if( isTapOrderNavi && btnBackward !=null && btnForward != null) { // タップ順ルート表示中
            NaviApplication app = (NaviApplication)activity.getApplication();
            // 「前へ」ボタン
            btnBackward.setVisibility(View.VISIBLE);
            btnBackward.setEnabled(app.isEnableBackwardBtn() );
            btnBackward.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        backwardTapList();
                    }
                });
            // [次へ」ボタン
            btnForward.setVisibility(View.VISIBLE);
            btnForward.setEnabled(app.isEnableForwardBtn() );
            btnForward.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        forwardTapList();
                    }
                });
            // 数表示
            reflectDeliveredCount(moreInfo, true);
        }
*/
        // Nexus7 2012対応
        WindowManager wm = (WindowManager)activity.getSystemService(Context.WINDOW_SERVICE);
        Display disp = wm.getDefaultDisplay();
        int windowWidth = disp.getWidth();
        if( "Nexus 7".equals(android.os.Build.MODEL) && windowWidth == 1280) {
            System.out.println("Nexus7 2012 model");
            isNexus7_2012 = true;

        }
        // 共通関数化
        showSakuraDeliveryControl(moreInfo);
// Add by CPJsunagawa '13-12-25 End

    }

    private void executeUpDateMap(final int updateType) {
        if (updateType == MAP_UPDATE) {

        } else {
            undoUpdate(updateType);
        }
    }

    public static final int MAP_UPDATE = 0;
    public static final int GOTO_UPDATE = 1;
    public static final int POS_CHANGE_UPDATE = 2;

    private void undoUpdate(int updateType) {
        switch (updateType) {
            case MAP_UPDATE:
                break;
            case GOTO_UPDATE:
                calcRoute();
                break;
            case POS_CHANGE_UPDATE:
                activity.goBackRouteEdit();
                break;
        }
    }

// Add by CPJsunagawa '2015-07-08 Start
    public static final int ID_REGIST_ICON = 1;
    public static final int ID_MOVE_ICON = 6;
    public static final int ID_DELETE_ICON = 3;
    public static final int ID_DRAW_LINE = 4;
    public static final int ID_INPUT_TEXT = 5;

    private void registFigure(int figureType) {
    	System.out.println("***　Into registFigure　Type => " + figureType);
        switch (figureType) {
            case ID_REGIST_ICON:
        		// アイコン登録
            	System.out.println("***　Before goRegistIcon");
                activity.goRegistIcon();
                activity.finish();
                break;
            case ID_MOVE_ICON:
        		// アイコン移動
            	System.out.println("***　Before goMoveIcon");
                activity.goMoveIcon();
                activity.finish();
                break;
            case ID_DELETE_ICON:
        		// アイコン削除
            	System.out.println("***　Before goDeleteIcon");
                //activity.goDeleteIcon();
                break;
/*            case ID_DRAW_LINE:
        		// ライン描画
            	System.out.println("***　Before goDrawLine");
                //activity.goDrawLine();
                break;
*/
            case ID_INPUT_TEXT:
        		// テキスト入力
            	System.out.println("***　Before goInputText");
//                activity.goInputText();
                break;
            case POS_CHANGE_UPDATE:
                activity.goBackRouteEdit();
                break;
        }
    }
// Add by CPJsunagawa '2015-07-08 End

    /**
     * Is hidden 5Route Button
     *
     * @return if hidden return true, else return false;
     */
    private boolean isHide5Route() {
        return (CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN) || CommonLib.routeHasVia();
    }

    /**
     * Route calc.
     */
    private void calcRoute() {
        activity.calcRoute();
    }

    /**
     * 5Route Button id
     */
    private final int[] route_5_id = new int[] {R.id.route_1, R.id.route_2, R.id.route_3, R.id.route_4, R.id.route_5};

    /**
     * init 5Route layout.
     */
    private void init5Route() {
        // 5 Route Info
        if (curMode != Constants.FIVE_ROUTE_MAP_VIEW) {
            return;
        }

        if (route5List != null && route5List.size() > 0) {
            route5List.clear();
        }
        layoutRoute5 = (LinearLayout)moreInfo.findViewById(R.id.layout_route_5);

        for (int i = 0; i < Constants.ROUTE_5_TYPE.length; i++) {
            final Button route_i = (Button)moreInfo.findViewById(route_5_id[i]);
            route5List.add(route_i);
            final int index = i;
            if (route_i != null) {
                route_i.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 5ルート(個別選択)禁止 Start -->
                    	if(DrivingRegulation.CheckDrivingRegulation()){
                    		return;
                    	}
// ADD 2013.08.08 M.Honma 走行規制 5ルート(個別選択)禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                        activity.onShow5Route(Constants.ROUTE_5_TYPE[index]);
                        for (Button btn : route5List) {
                            if (btn != null) {
                                btn.setEnabled(!(btn == route_i));
                            }
                        }

                        setViewEnable(naviDemoStart);
                        setViewEnable(naviStart);
                        setViewEnable(btnRouteEdit);
                    }
                });
            }
        }
    }

    /**
     * get the instance of GuideView
     *
     * @return the instance of GuideView
     */
    public GuideView getGuideView() {
        return guideView;
    }

    /**
     * init GuideView
     *
     * @param guideView
     *            GuideView instance.
     */
    public void initGuideInfo(GuideView guideView) {
        //mod liutch 0517 bug1095 -->
//Chg 2012/02/23 Z01_h_yamada Start --> #3839
//        if (!CommonLib.isNaviMode()) {
//--------------------------------------------
        if (!CommonLib.isNaviMode() || this.guideView == null) {
//Chg 2012/02/23 Z01_h_yamada End <--
            this.guideView = guideView;
            if (guideView != null) {
                View view = guideView.getView();
                CommonLib.clearViewInLayout(view);
                m_oTopView.addView(view);
            }
        }
        //<--
    }

    /**
     * get cur View
     *
     * @return View which is displayed now.
     */
    public RelativeLayout getView() {
        return m_oTopView;
    }

    /**
     * event when MP is moved.
     */
    public void onMapMove() {
        // XuYang mode start #1029
//        if (curMode == Constants.COMMON_MAP_VIEW
//                  || curMode == Constants.ROUTE_MAP_VIEW_BIKE) {
        if (curMode == Constants.COMMON_MAP_VIEW) {

            // XuYang mode start #1029
//            isVisibleShow = false;
//            isShowMode = false;
            isMapMove = true;

            hide(visible);
            show(invisible);

            setViewVisible(moreInfo);
//Chg 2011/09/17 Z01_h_yamada Start -->
//          setViewInvisible(btnMainMenu);
//--------------------------------------------
            btnMainMenu.setVisibility(View.GONE);
//Chg 2011/09/17 Z01_h_yamada End <--

            AdjustGrayLeyout();
        }
    }

    private void show(List<View> visible) {
        for (View view : visible) {
            setViewVisible(view);
        }
    }

    private void hide(List<View> invisible) {
        for (View view : invisible) {
            setViewInvisible(view);
        }
    }

    private void clearView(List<View> visible) {
        if (visible == null || visible.size() == 0) {
            return;
        }
        for (View view : visible) {
            CommonLib.clearViewInLayout(view);
        }
        visible.clear();
    }

    public void AdjustGrayLeyout() {

    	AdjustGrayLeyout( mTopLeftGrayLayout );
//    	AdjustGrayLeyout( mTopLeftGrayLayout2 );
    	AdjustGrayLeyout( mTopRightGrayLayout );
    	AdjustGrayLeyout( mTopRightGrayLayout2 );
    }

    private void AdjustGrayLeyout( LinearLayout layout ) {

    	if ( layout != null && View.VISIBLE == layout.getVisibility() ) {
    		ViewGroup.LayoutParams params = layout.getLayoutParams();
    		params.width = getChildlenWidth( layout );
    		if ( params.width > 0 ) {
    			layout.setLayoutParams(params);
    		}
    	}
    }

    private int getChildlenWidth( LinearLayout layout ) {

    	int width = 0;
    	int count = 0;
    	for ( int  i = 0; i < layout.getChildCount(); ++i ) {
    		if ( View.VISIBLE == layout.getChildAt(i).getVisibility() ) {
    			ViewGroup.LayoutParams params = layout.getChildAt(i).getLayoutParams();
    			if (params.width > 0) {
    				width += params.width;
    				count++;
    			}
    		}
    	}
    	if (count > 0) {
    		// 無反応領域側マージン
    		width += (int)(activity.getResources().getDimensionPixelSize(R.dimen.MT_LmarginGray));

    		// 画面端マージン
    		width += (int)(activity.getResources().getDimensionPixelSize(R.dimen.Common_Location_margin));

    		// ボタン間マージン
			width += ((int)(activity.getResources().getDimensionPixelSize(R.dimen.MT_LmarginRight)) *(count-1));
    	}
    	return width;
    }

    public void onConfigChanged() {
        initConfigChange();
        if (isConfigChanged) {
            if (preMode >= 0) {
                CommonLib.clearViewInLayout(oLayout[preMode]);
            }

            hide(visible);
            hide(invisible);

            clearView(visible);
            clearView(invisible);

            init();

            if (curMode == preMode) {
                if (curMode == Constants.COMMON_MAP_VIEW
                        || curMode == Constants.ROUTE_MAP_VIEW_BIKE) {

                    if (isMapMove) {
                        hide(visible);
                        show(invisible);

//Chg 2011/09/17 Z01_h_yamada Start -->
//                      setViewInvisible(btnMainMenu);
//--------------------------------------------
                        btnMainMenu.setVisibility(View.GONE);
//Chg 2011/09/17 Z01_h_yamada End <--
                        setViewVisible(moreInfo);

                    } else {
                        if (isShowMode) {
                        } else {
                            hide(visible);
                            show(invisible);
                        }
                    }

                } else {
                    if (isShowMode) {//
                    } else {//some view hidden
                            //yangyang add start Bug1021
                            //BackGroundが表示し、あるいはMagMapが表示する場合
                        if (getGuideView().isBackgroundVisible() || CommonLib.isBIsShowMagMap()) {
                            //yangyang add end Bug1021
                            hide(visible);
                            //yangyang add start Bug1021
                        }
                        //yangyang add end Bug1021
                        show(invisible);
                    }
                }
            } else {
                reset();
            }

            AdjustGrayLeyout();
        }
    }

    private boolean is5RouteMode() {
        return Constants.FIVE_ROUTE_MAP_VIEW == curMode;
    }

//    private boolean m_bCancel = false;

    public void setMapEvent(MapActivity activity) {
        this.activity = activity;
    }

    private void addVisible(View view) {
        setViewVisible(view);
        visible.add(view);
    }

    /**
     * 到着情報を更新する。
     *
     * @param dist
     *            全ルートの距離
     * @param toll
     *            料金
     * @param time
     *            到着時刻
     */
    public void showRouteMenu(String dist, String toll, String time) {
        setText(txtRouteDistance, dist);
        setText(txtcoin, toll);
        setText(txtRouteUseTime, time);
    }

// Add by CPJsunagawa '13-12-25 Start
    public void setNowMatchingCustomer(DeliveryInfo delivInfo)
    {
    	if (matCustName != null)
    	{
    		LinearLayout ll = (LinearLayout)oLayout[curMode].findViewById(R.id.display_now_name);
    		if (ll != null)
    		{
    			ll.setVisibility(View.VISIBLE);
// Mod by CPJsunagawa '14-06-18  配達先名称の前に、配達順を入れたい
//	        	setText(matCustName, delivInfo.mCustmer);
	        	setText(matCustName, "【" + delivInfo.mDeliOrder + "】　" + delivInfo.mCustmer);
	        	setText(matAdr, delivInfo.mAddress);
    		}
    	}
    }
    
    public void showMapTopRightForList()
    {
    	View view = oLayout[curMode].findViewById(R.id.maptopright_layout_forlist);
    	if (view != null) view.setVisibility(View.VISIBLE);
    }
// Add by CPJsunagawa '13-12-25 End

    private void setText(TextView view, String text) {
        if (view != null) {
            if (text != null) {
                view.setText(text);
            }
        }
    }

    private void setViewVisible(View view) {
        if (view != null) {
            view.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
        }
    }

    private void setViewInvisible(View view) {
        if (view != null) {
//Add 2011/09/08 Z01_h_yamada Start --> AndroidOS2.3.3 から一部のUIパーツが再描画されない場合がある不具合の修正
            view.setVisibility(View.GONE);
//Add 2011/09/08 Z01_h_yamada End <--
        	
// Cut by CPJsunagawa '13-12-25 Start
            //view.setVisibility(View.INVISIBLE);
// Cut by CPJsunagawa '13-12-25 Start
        }
    }

    private void setViewDisable(View view) {
        if (view != null) {
            view.setEnabled(false);
            try {
                ViewGroup v = (ViewGroup)view;
                for (int i = 0; i < v.getChildCount(); i++) {
                    setViewDisable(v.getChildAt(i));
                }
            } catch (Exception e) {
            }
        }
    }

    private void setViewEnable(View view) {
        if (view != null) {
            view.setEnabled(true);
            try {
                ViewGroup v = (ViewGroup)view;
                for (int i = 0; i < v.getChildCount(); i++) {
                    setViewEnable(v.getChildAt(i));
                }
            } catch (Exception e) {
            }
        }
    }

    public void UpdateNaviModeChange() {
        if (naviStateIcon != null) {
        	naviStateIcon.updateStateIcon();
            naviStateIcon.setVisibility(View.VISIBLE);
        }
    }

    //   private boolean isMapScroll = false;
    /**
     * 現在地を戻る
     */
    public void backCarPos() {
        preMode = -1;

        if (curMode == Constants.SIMULATION_MAP_VIEW
                || curMode == Constants.NAVI_MAP_VIEW) {
            //Navi-Demo-Mode Not need to change.
        } else if (curMode == Constants.COMMON_MAP_VIEW
                || curMode == Constants.ROUTE_MAP_VIEW_BIKE) {

            hide(invisible);

            if (isShowMode) {
                show(visible);
            }

            setViewInvisible(moreInfo);
            setViewVisible(btnMainMenu);

        } else {
            hide(invisible);
            show(visible);
        }

        AdjustGrayLeyout();
        isMapMove = false;
//        isShowMode = true;
    }

    void reset() {
        isMapMove = false;
        isShowMode = true;
    }

    public void onSysLimit() {
        if (CommonLib.isLimit()) {
            setViewDisable(btnMainMenu);
            setViewDisable(btnDestInquiry);
            setViewDisable(btnFavorite);
//            setViewDisable(btnShow);
            setViewDisable(btnSurround);
            setViewDisable(btnSurroundMore);
            setViewDisable(navi_here);
            setViewDisable(btnRouteEdit);
            setViewDisable(route_5);
            setViewDisable(btnMainMenu);
            setViewDisable(btnMainMenu);
            setViewDisable(btnMainMenu);

            setViewDisable(layoutRoute5);
        } else {
            setViewEnable(btnMainMenu);
            setViewEnable(btnDestInquiry);
            setViewEnable(btnFavorite);
//            setViewEnable(btnShow);
            setViewEnable(btnSurround);
            setViewEnable(btnSurroundMore);
            setViewEnable(navi_here);
            setViewEnable(btnRouteEdit);
            setViewEnable(route_5);
            setViewEnable(btnMainMenu);
            setViewEnable(btnMainMenu);
            setViewEnable(btnMainMenu);

            setViewEnable(layoutRoute5);
        }
    }

    private boolean isShowGuideViewFlag() {
        if (guideView != null) {
            return guideView.isVisible();
        }
        return false;
    }

    public void onHideNaviView() {
        if (isShowGuideViewFlag()) {
            show(visible);
        }
    }

    public void onShowNaviView() {
        if (isShowGuideViewFlag()) {
            hide(visible);
        }
    }

    public boolean isShowMenu() {
        if (btnMainMenu != null) {
            return (btnMainMenu.getVisibility() == View.VISIBLE) && btnMainMenu.isEnabled();
        }
        return false;
    }

// Add by CPJsunagawa '13-12-25 Start
    public void reflectDeliveredCount()
    {
    	reflectDeliveredCount(true);
    }
    
    public void reflectDeliveredCount(boolean isShow)
    {
    	reflectDeliveredCount(null, isShow);
    }
    
    // 
    private ViewGroup mLastVGForDelivCnt;
    public void reflectDeliveredCount(ViewGroup rl, boolean isShow)
    {
    	ViewGroup trueRL = rl;
    	if (rl == null)
    	{
    		if (mLastVGForDelivCnt != null)
    		{
    			trueRL = mLastVGForDelivCnt;
    		}
    		else
    		{
	    		if (curMode <= -1) trueRL = null;
	    		else trueRL = oLayout[curMode];
    		}
    	}
    	else
    	{
    		mLastVGForDelivCnt = rl;
    	}
    	
    	if (trueRL != null && trueRL.getVisibility() == View.VISIBLE)
    	{
	    	Button btn = (Button)trueRL.findViewById(R.id.deliv_count);
	    	if (btn != null)
	    	{
	    		btn.setVisibility(isShow ? View.VISIBLE : View.GONE);
		    	if (isShow)
		    	{
			    	NaviApplication app = (NaviApplication)activity.getApplication();
			    	String disp = app.getCountCaption();
			    	if (disp != null && !"".equals(disp))
			    	{
			    		btn.setText(disp);
			    	}
			    	else
			    	{
			    		btn.setVisibility(View.GONE);
			    	}
		    	}
	    	}
    	}
    }

// 2013/10/17 M.Suna Start -->
    private boolean isTapOrderNavi = false;
    
    public void enableTapOrderNaviMode(boolean flg) {
        isTapOrderNavi = flg;
    }
    
    public void backwardTapList() {
        NaviApplication app = (NaviApplication)activity.getApplication();
        app.decrementRoutePage();
        
        int pos = app.getTapRouteStartPos();
        
/*        
        pos -= 6;
        if( pos < 0) {
            pos = 0;
        }
        app.setTapRouteStartPos(pos);
*/        
        //activity.determineTapOrderRoute(pos);
        activity.determineVehicleRoute(pos);
    }
    
/*    
    public void showRightBar()
    {
    	moreInfo = (LinearLayout)inflater.inflate(layout_moreInfo[2], null);
    	if (moreInfo != null) moreInfo.setVisibility(View.VISIBLE);
    }
    
    public void hideRightBar()
    {
    	moreInfo = (LinearLayout)inflater.inflate(layout_moreInfo[2], null);
    	if (moreInfo != null) moreInfo.setVisibility(View.GONE);
    }
*/    
    
    public void forwardTapList() {
        NaviApplication app = (NaviApplication)activity.getApplication();
        app.incrementRoutePage();
        int pos = app.getTapRouteStartPos();
/*        
        pos += 6;
        //if( pos >= app.getTapOrderListSize() ) { // 到達しないはずであるがfail safe
        if( pos >= app.getRouteListSize() ) { // 到達しないはずであるがfail safe
            return; // 現在が最後尾の範囲なので何もしない
        }
        app.setTapRouteStartPos(pos);
*/        
        //activity.determineTapOrderRoute(pos);
        activity.determineVehicleRoute(pos);
    }
// 2013/10/17 M.Suna End <--
    
    public void setDetailButton(DeliveryInfo info, LinearLayout dispTV)
    {
    	showSakuraDeliveryControl(null, info, dispTV);
    }
    
    private void showSakuraDeliveryControl(ViewGroup rl)
    {
    	showSakuraDeliveryControl(rl, null, null);
    }

    ViewGroup keptRL;
    DeliveryInfo keptDelivInfo;
    LinearLayout keptDispTV;
    boolean isNexus7_2012 = false;
    final int NEXUS7_2012_BTN_HEIGHT = 70;
    /**
     *
     * @param rl　ViewGroup
     * @param selectedID 選択されたID
     * @param dispTV ボタンのリニアレイアウト
     */
    private void showSakuraDeliveryControl(ViewGroup rl, DeliveryInfo info, LinearLayout dispTV)
    {
    	if (rl != null) keptRL = rl;
    	keptDelivInfo = info;
    	boolean isShowDetail = (keptDelivInfo != null);
    	keptDispTV = dispTV;
    	isShowDetail = isShowDetail && (keptDispTV != null);
    	
        // タップ順Naviの「前へ」と「次へ」ボタン
        Button btnBackward = (Button)keptRL.findViewById(R.id.route_backward);
        // Nexus7 2012の場合には、高さを変更する
        if(isNexus7_2012 && btnBackward != null ) {
            btnBackward.getLayoutParams().height = NEXUS7_2012_BTN_HEIGHT;
            btnBackward.requestLayout();
        }
        Button btnForward = (Button)keptRL.findViewById(R.id.route_forward);
        if(isNexus7_2012 && btnForward != null ) {
            btnForward.getLayoutParams().height = NEXUS7_2012_BTN_HEIGHT;
            btnForward.requestLayout();
        }
        View btn = keptRL.findViewById(R.id.route_edit);
        if(isNexus7_2012 && btn != null ) {
            btn.getLayoutParams().height = NEXUS7_2012_BTN_HEIGHT;
            btn.requestLayout();
        }
        btn = keptRL.findViewById(R.id.deliv_count);
        if(isNexus7_2012 && btn != null ) {
            btn.getLayoutParams().height = NEXUS7_2012_BTN_HEIGHT;
            btn.requestLayout();
        }
        btn = keptRL.findViewById(R.id.navi_start);
        if(isNexus7_2012 && btn != null ) {
            btn.getLayoutParams().height = NEXUS7_2012_BTN_HEIGHT;
            btn.requestLayout();
        }

        Button btnDetail = (Button)keptRL.findViewById(R.id.show_detail);
        // Nexus7 2012対応
        if(isNexus7_2012 && btnDetail != null ) {
            btnDetail.getLayoutParams().height = NEXUS7_2012_BTN_HEIGHT;
            btnDetail.requestLayout();
        }
        Button btnMatch = (Button)keptRL.findViewById(R.id.manual_matching);
        if(isNexus7_2012 && btnMatch != null ) {
            btnMatch.getLayoutParams().height = NEXUS7_2012_BTN_HEIGHT;
            btnMatch.requestLayout();
            // マッチングは2行になってしまう為サイズ変更
            btnMatch.setTextSize(28.0f);
        }
        Button btnReturn = (Button)keptRL.findViewById(R.id.route_return);
        if(isNexus7_2012 && btnReturn != null ) {
            btnReturn.getLayoutParams().height = NEXUS7_2012_BTN_HEIGHT;
            btnReturn.requestLayout();
        }
        
        if( isTapOrderNavi && btnBackward !=null && btnForward != null) { // タップ順ルート表示中
            NaviApplication app = (NaviApplication)activity.getApplication();
            
            // ボタン表示の制御
            btnBackward.setVisibility(isShowDetail ? View.GONE : View.VISIBLE);
            btnForward.setVisibility(isShowDetail ? View.GONE : View.VISIBLE);
            btnDetail.setVisibility(isShowDetail ? View.VISIBLE : View.GONE);
            btnMatch.setVisibility(isShowDetail ? View.VISIBLE : View.GONE);
            btnReturn.setVisibility(isShowDetail ? View.VISIBLE : View.GONE);
            
            if (isShowDetail)
            {
            	// 「顧客詳細」ボタン
            	btnDetail.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    	activity.showInfoDialog(keptDelivInfo, keptDispTV);
                    }
                });
            	// 「マッチング」ボタン
            	btnMatch.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    	activity.executeManualMatching(keptDelivInfo, null);
                    }
                });
            	
            	// 「戻る」ボタン
            	btnReturn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    	showSakuraDeliveryControl(keptRL);
                    }
                });
            }
            else
            {
	            // 「前へ」ボタン
	            btnBackward.setEnabled(app.isEnableBackwardBtn() );
	            btnBackward.setOnClickListener(new OnClickListener() {
	                    @Override
	                    public void onClick(View v) {
	                        backwardTapList();
	                    }
	                });
	            
	            // [次へ」ボタン
	            btnForward.setEnabled(app.isEnableForwardBtn() );
	            btnForward.setOnClickListener(new OnClickListener() {
	                    @Override
	                    public void onClick(View v) {
	                        forwardTapList();
	                    }
	                });
	        }
            
            // 数表示
            reflectDeliveredCount(keptRL, !isShowDetail);
        }
    }
// Add by CPJsunagawa '13-12-25 End

//    public void setDriveContentsEnable(boolean bEnable){
//    	if(bEnable){
//    		setViewEnable(btnDriverContentsMenu);
//    	}else{
//    		setViewDisable(btnDriverContentsMenu);
//
//    	}
//  	}
//
//    public void setDriverContentsMenuClickable(boolean flag) {
//        if (btnDriverContentsMenu == null) {
//            return;
//        }
//        btnDriverContentsMenu.setClickable(flag);
//    }
}
