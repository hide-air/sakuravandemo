package net.zmap.android.pnd.v2.common.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;


import java.io.FileOutputStream;
import java.util.ArrayList;

public class PhotoCanvas extends FrameLayout
{
    Context mCtx = null;
    private PhotoView mPhoto = null;
    private CanvasView mCanvas = null;
    private boolean mIsEraser = false;
    private View mPenWidthView = null, mPenColorView = null; // 選択された感を出す為に使用
    private int mPenWidth = 0;
    private int mPenColor = 0;
// 2015-04-20 Add Start
    private boolean mBackKey = false;
// 2015-04-20 Add End
//    String mPath = null;
    public String mPath = null;
    private int mOrgBmpW = 0, mOrgBmpH = 0;

	public PhotoCanvas(Context context) {
        super(context);
        mCtx = context;
        mPhoto = new PhotoView(context);
        addView(mPhoto, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
	}

	public PhotoCanvas(Context context,AttributeSet oSet) {
		super(context,oSet);
        mCtx = context;
        mPhoto = new PhotoView(context);
        addView(mPhoto, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
	}

    public void initView(String path, int viewW, int viewH) {
        mPath = path;
        // 写真の設定
        Bitmap bmp = BitmapFactory.decodeFile(path);
        int requiredW = 0;
        int requiredH = 0;
        mOrgBmpW = bmp.getWidth();
        mOrgBmpH = bmp.getHeight();
        double aspectRatio = (double)mOrgBmpW / mOrgBmpH;
        requiredW = viewW;
        requiredH = (int)(viewW / aspectRatio);
        if( requiredH > viewH ) { // 高さが超えてしまう場合は、再調整
            double rate = (double)viewH / requiredH;
            requiredW = (int)(requiredW * rate);
            requiredH = (int)(requiredH * rate);
        }
        Bitmap bmp2  = Bitmap.createScaledBitmap(bmp, requiredW, requiredH, false);
        bmp.recycle();
        mPhoto.setBitmap(bmp2, viewW, viewH);
        // キャンバスの設定
        if( mCanvas != null ) {
            removeView(mCanvas);
            mCanvas = null;
        }
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(mPhoto.w, mPhoto.h);
        params.setMargins(mPhoto.x, mPhoto.y, 0, 0);
        mCanvas = new CanvasView(mCtx);
        addView(mCanvas, params);
    }

    public void clearPhotoObj() {
        if( mPhoto != null ) {
            mPhoto.finish();
        }
        if( mCanvas != null ) {
            mCanvas.finish();
        }
        System.gc();
    }

    public void save() {
        // オリジナルbmpのCanvasを作成する
        Bitmap bmp = Bitmap.createBitmap(mOrgBmpW, mOrgBmpH, Bitmap.Config.ARGB_8888);
        Canvas cv = new Canvas(bmp);
        // 写真用のbmp
        Bitmap bmp2 = BitmapFactory.decodeFile(mPath);
        cv.drawBitmap(bmp2, 0, 0, null);
        bmp2.recycle();
        bmp2 = null;
        // キャンバスのbmpを拡大
        bmp2  = Bitmap.createScaledBitmap(mCanvas.mBmp, mOrgBmpW, mOrgBmpH, false);
        cv.drawBitmap(bmp2, 0, 0, null);
        bmp2.recycle();
        bmp2 = null;
        try {
             FileOutputStream out = new FileOutputStream(mPath);
            bmp.compress(CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
System.out.println("save after");
        } catch( Exception e) {
            e.printStackTrace();
        }
    }

    public void setPenWidth(View v, int width) {
        if(mPenWidthView != null) {
            mPenWidthView.setBackgroundColor(0x00000000); // 透明
        }
        mPenWidthView = v;
        v.setBackgroundColor(0xff40B3DD); // 選択された感を出す為に色を変える
        mPenWidth = width;
        if( mCanvas != null ) {
            mCanvas.setPenWidth(mPenWidth);
        }
    }

    public void setPenColor(View v, int color) {
        if(mPenColorView != null) {
            mPenColorView.setBackgroundColor(0x00000000); // 透明
        }
        mPenColorView = v;
        v.setBackgroundColor(0xff40B3DD); // 選択された感を出す為に色を変える
        mPenColor = color;
        if( mCanvas != null ) {
            mCanvas.setPenColor(mPenColor);
        }
    }

    public void clear() {
        if( mCanvas != null ) {
            mCanvas.clear();
        }
    }
// Add 2015-02-10 Start
    public void delete() {
        if( mCanvas != null ) {
            mCanvas.delete();
        }
    }
// Add 2015-02-10 End

    public void undo() {
        if( mCanvas != null ) {
            mCanvas.undo();
        }
    }

    public void setEraserMode(View v) {
        mIsEraser = !mIsEraser;
        if(mIsEraser) {
            v.setBackgroundColor(0xff40B3DD); // 選択された感を出す為に色を変える
        } else {
            v.setBackgroundColor(0x00000000); // 透明
        }
        if( mCanvas != null ) {
            mCanvas.setEraserMode(mIsEraser);
        }
    }

    public void resetEraserMode(View v) {
        mIsEraser = true;
        v.setBackgroundColor(0x00000000); // 透明
        if( mCanvas != null ) {
            mCanvas.setEraserMode(mIsEraser);
        }
    }


 // Add 2015-04-02 BACKキー対応 Start
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (mapView != null && keyCode == KeyEvent.KEYCODE_BACK) {
//        if (keyCode == KeyEvent.KEYCODE_BACK ) {
// 2015-04-20 Mod
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
        	
//        	Toast.makeText(this, "BACKキーではなく、戻るボタンを使用してください", Toast.LENGTH_SHORT).show();
            // BACKキーを連続して押しても初回のみメッセージを出力
        	if(mBackKey == false) {
        		Toast.makeText(mCanvas.getContext(), "前の画面に戻る場合は左上の「戻る」ボタンを押してください", Toast.LENGTH_SHORT).show();
        	}
        	mBackKey = true;
        	return false;
        }
        return super.onKeyDown(keyCode, event);
    }
// Add 2015-04-02 BACKキー対応 End
    
    
    class PhotoView extends View {
        Bitmap mBmp;
        public int x = 0, y = 0, w = 0, h = 0, viewW = 0, viewH = 0;

        public PhotoView(Context context) {
    		super(context);
    	}

    	public PhotoView(Context context,AttributeSet oSet) {
    		super(context,oSet);
    	}

        protected void finalize() {
            if(mBmp != null ) { 
                mBmp.recycle();
            }
        }

        public void setBitmap(Bitmap bmp, int width, int height) {
            if(mBmp != null) {
                mBmp.recycle();
            }
            mBmp = bmp;
            viewW = width;
            viewH = height;
            if( mBmp != null) {
                // 中心に位置するように、描画位置を求める
                w = mBmp.getWidth();
                h = mBmp.getHeight();
// Mod 2015-03-27 Test M.Suna
//                if( viewW - w > 0) {
                if( viewW - w >= 0) {
                    x = (int)((viewW - w)/2);
                }
// Mod 2015-03-27 Test M.Suna
//                if( viewH - h > 0) {
                if( viewH - h >= 0) {
                    y = (int)((viewH - h)/2);
                }
            }
        }

    	@Override
    	protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            Paint paint = new Paint();
            paint.setAntiAlias(true);
    //        canvas.drawCircle(40.5f, 40.5f, 20.0f, paint);
            if( mBmp != null ) {
                canvas.drawBitmap(mBmp,x,y, paint);
            }
    	}

        // 内部オブジェクトを削除
        public void finish() {
            if(mBmp != null ) { 
                mBmp.recycle();
                mBmp = null;
            }
        }
    }

    class CanvasView extends View {
        final int mEraserPenWidth = 36;
        Bitmap mBmp;
        Paint mPaint = null;
        public int x = 0, y = 0, w = 0, h = 0, viewW = 0, viewH = 0;
        private float mPosx = 0f;
        private float mPosy = 0f;
        ArrayList<PathInfo> draw_list = new ArrayList<PathInfo>(); // 描画履歴
        private int mColor = 0xff000000; // black
        private int mPenWidth = 6; // 現在描画中の線幅
        private Canvas bmpCanvas;
        // 現在描画中の情報
        private Path nowpath = null; // 現在描画中のパス
        private boolean mIsEraserMode = false;

        public CanvasView(Context context) {
    		super(context);
            createPaint();
    	}

    	public CanvasView(Context context,AttributeSet oSet) {
    		super(context,oSet);
            createPaint();
    	}

        protected void finalize() {
            if(mBmp != null ) { 
                mBmp.recycle();
            }
        }

        protected void onSizeChanged (int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w,h,oldw,oldh);
            createBitmap(w, h);
        }

        public boolean onTouchEvent(MotionEvent e){
            switch(e.getAction()){
                case MotionEvent.ACTION_DOWN: //最初のポイント
                    ActWhenTapDown(e);
                    break;
                case MotionEvent.ACTION_MOVE: //途中のポイント
                    ActWhenDrag(e);
                    break;
                case MotionEvent.ACTION_UP: //最後のポイント
                    ActWhenTapUp(e);
                    break;
                default:
                    break;
            }
            return true;
        }

        protected void onDraw(Canvas canvas) {
        	if (mBmp == null) return;
        	
            canvas.drawBitmap(mBmp, 0, 0, null);
            drawNowEditingToCanvas(canvas);
        }

        // 内部オブジェクトを削除
        public void finish() {
            if(mBmp != null ) { 
                mBmp.recycle();
                mBmp = null;
            }
            draw_list.clear();
        }

        /**
         * カラーの設定
         * @param rgb
         */
        public void setPenColor(int rgba) {
            mColor = rgba;
        }

        /**
         * ペン幅の設定
         * @param width
         */
        public void setPenWidth(int width) {
            mPenWidth = width;
        }

        /**
         * クリアする
         */
        public void clear(){
            draw_list.clear();
            refreshBitmap();
            invalidate();
        }
// Add 2015-02-10 Start
        /**
         * 削除する
         */
        public void delete(){
//            draw_list.remove(index);
            draw_list.clear();
            refreshBitmap();
            invalidate();
        }
// Add 2015-02-10 End

        /**
         * Undoを行う
         */
        public void undo() {
            int lastIndex = draw_list.size() - 1;
            if(lastIndex <0 ) {
                return; // fail safe
            }
            draw_list.remove(lastIndex);
            redrawToBitmapForUndo();
            invalidate();
        }

        // 消しゴムモードを切り替える
        public void setEraserMode(boolean isEraser) {
            mIsEraserMode = isEraser;
            if(mIsEraserMode) {
                mPaint.setColor(0x80ffffff); // todo
                mPaint.setStrokeWidth(mEraserPenWidth);
//                mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));//SRC));
//                mPaint.setARGB(0, 0, 0, 0);
            } else {
                mPaint.setColor(mColor);
                mPaint.setStrokeWidth(mPenWidth);
//                mPaint.setXfermode(null);
            }
        }

        // Paintの初期化
        private void createPaint() {
            mPaint = new Paint();
            mPaint.setAntiAlias(true);  
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mPaint.setStrokeJoin(Paint.Join.ROUND);
        }

        // タップダウン時のアクション
        private void ActWhenTapDown(MotionEvent e) {
            mPosx = e.getX();
            mPosy = e.getY();
            mPaint.setXfermode(null);
            nowpath = new Path();
            nowpath.moveTo(mPosx, mPosy);
        }

        // ドラッグ時のアクション
        private void ActWhenDrag(MotionEvent e) {
            mPosx += (e.getX()-mPosx)/1.4;
            mPosy += (e.getY()-mPosy)/1.4;
            nowpath.lineTo(mPosx, mPosy);
            invalidate();
        }

        // タップアップ時のアクション
        private void ActWhenTapUp(MotionEvent e) {
            nowpath.lineTo(e.getX(), e.getY());
            if(mIsEraserMode) { // 消しゴム
                draw_list.add( new PathInfo(nowpath, 0, mEraserPenWidth, true) );
            } else { // 通常
                draw_list.add( new PathInfo(nowpath, mColor, mPenWidth) );
            }
            nowpath = null;
            int lastIdx = draw_list.size() - 1;
            if (lastIdx >= 0) {
                mPaint.setColor(draw_list.get(lastIdx).color);
                mPaint.setStrokeWidth(draw_list.get(lastIdx).width);
                if(mIsEraserMode) {
                    mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));//SRC));
                } else {
                    mPaint.setXfermode(null);
                }
                bmpCanvas.drawPath(draw_list.get(lastIdx).path, mPaint);
                if(mIsEraserMode) {
                    mPaint.setXfermode(null); // 元に戻す
                }
                invalidate();
            }
        }

        // Bitmapの初期化
        private void createBitmap(int w, int h) {
             // ビットマップの生成
             if (mBmp != null) mBmp.recycle();
             mBmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
             bmpCanvas = new Canvas(mBmp);
             
             // ビットマップに透明な矩形を描画しておく
             Paint pnt = new Paint();
             pnt.setColor(0);
             bmpCanvas.drawRect(0, 0, (float)w, (float)h, pnt);
        }

        // ビットマップを透明で初期化する。
        private void refreshBitmap() {
            if (mBmp == null) return;
            int w = mBmp.getWidth(), h = mBmp.getHeight();
            createBitmap(w, h);
        }

        // 現在描画中のものは基本Viewのキャンバスにしか描かないこととする。
        private void drawNowEditingToCanvas(Canvas cv) {
            if(nowpath != null){
                if(mIsEraserMode) { // 消しゴム
                    mPaint.setColor(0x80ffffff); // 途中経過は透明で書くことはできないので、半透明の白で描画する
                    mPaint.setStrokeWidth(mEraserPenWidth);
//                    mPaint.setXfermode(null);//new PorterDuffXfermode(PorterDuff.Mode.CLEAR));//SRC));
//                    mPaint.setARGB(0, 0, 0, 0);
                } else {
                    mPaint.setColor(mColor); // 現在、ドラッグ中の線は現在の設定色で描画する
                    mPaint.setStrokeWidth(mPenWidth);
//                    mPaint.setXfermode(null);
                }
                cv.drawPath(nowpath, mPaint);
            }
        }

        // 描画リストから、改めてビットマップキャンバスに全描画する。
        private void redrawToBitmapForUndo() {
            refreshBitmap();
            for (int i = 0; i < draw_list.size(); i++) {
                mPaint.setColor(draw_list.get(i).color);
                mPaint.setStrokeWidth(draw_list.get(i).width);
                if( draw_list.get(i).isEraser) {
                    mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                } else {
                    mPaint.setXfermode(null);
                }
                bmpCanvas.drawPath(draw_list.get(i).path, mPaint);
            }
            mPaint.setXfermode(null);//戻す
        }

    }

    class PathInfo {
        Path path;
        int color;
        int width; // ペン幅
        boolean isEraser;
        
        public PathInfo(Path path, int color, int width) {
            this.path = path;
            this.color = color;
            this.width = width;
            this.isEraser = false;
        }

        public PathInfo(Path path, int color, int width, boolean isEraser) {
            this.path = path;
            this.color = color;
            this.width = width;
            this.isEraser = true;
        }
    }
}
