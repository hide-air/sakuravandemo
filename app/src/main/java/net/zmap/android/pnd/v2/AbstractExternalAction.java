package net.zmap.android.pnd.v2;

import net.zmap.android.pnd.v2.maps.MapActivity;
import android.content.Intent;

public abstract class AbstractExternalAction implements Runnable {
    protected MapActivity   mMapActivity;
    protected Intent        mIntent;
    
    protected AbstractExternalAction(MapActivity mapActivity, Intent intent) {
        mMapActivity = mapActivity;
        mIntent = intent;
    }

    @Override
    public void run() {
        doExternalAction();
    }

    protected abstract void doExternalAction();
}
