//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.Constants;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.ScrollBoxAdapter;
import net.zmap.android.pnd.v2.data.JNIJumpKey;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNIShort;
import net.zmap.android.pnd.v2.inquiry.view.ItemView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CityInquiry extends InquiryBaseLoading implements ScrollBoxAdapter {


	protected List<String> AKeyName = new ArrayList<String>();
	protected List<String> WaKeyName = new ArrayList<String>();
	protected List<String> KaKeyName = new ArrayList<String>();
	protected List<String> SaKeyName = new ArrayList<String>();
	protected List<String> TaKeyName = new ArrayList<String>();
	protected List<String> NaKeyName = new ArrayList<String>();
	protected List<String> HaKeyName = new ArrayList<String>();
	protected List<String> MaKeyName = new ArrayList<String>();
	protected List<String> YaKeyName = new ArrayList<String>();
	protected List<String> RaKeyName = new ArrayList<String>();
	protected boolean showa = false;
	protected boolean showka = false;
	protected boolean showsa = false;
	protected boolean showta = false;
	protected boolean showna = false;
	protected boolean showha = false;

	protected 	boolean showma = false;
	protected boolean showya = false;
	protected boolean showra = false;
	protected boolean showwa = false;

//	private Button btnA = null;
//    private Button btnKa = null;
//    private Button btnSa = null;
//    private Button btnTa = null;
//    private Button btnNa = null;
//    private Button btnHa = null;
//    private Button btnMa = null;
//    private Button btnYa = null;
//    private Button btnRa = null;
//    private Button btnWa = null;
	protected long RecIndex = 0;
//	private int pageIndex = 0;
	protected JNILong ListCount;
	/** スクロールできる総件数 */
	protected int allRecordCount = 0;

	//yangyang del start Bug617
//	protected WhiteBackGroundTextView oGroupValue = null;
	//yangyang del end Bug617
	protected int iProvinceId = 0;
	private int myProvinceId = 0;
//	private String myCityName = "";
//	private String myCityNextName = "";
	protected ScrollList oList = null;
	protected boolean back = false;

	protected String msName;
	protected long NameSize ;
	/** 自車の市区町村のIndex */
	protected JNILong Index = new JNILong();
	private ItemView oview = null;
	protected boolean isGroupBtnClick = false;
//	private boolean onlyRunOne = false;
	protected HashMap<Integer, Integer> hashIndex = new HashMap<Integer, Integer>();
	private int clickIndex = 0;
	private String clickValue = "";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		startShowPage(NOTSHOWCANCELBTN);


	}
	protected void initCityBtn(Button obtn) {
	}
	public  void initDialog() {
		Intent oIntent = getIntent();
//Del 2011/09/17 Z01_h_yamada Start -->
//		int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE,0);
//		if (0!= titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--
//		isGroupBtnClick= false;
//		iProvinceId = oIntent.getShortExtra(Constants.PARAMS_WIDECODE, (short) 0);
//		myProvinceId = oIntent.getIntExtra(Constants.PARAMS_MYPOS_PROVINCE, 0) + 1;
//		myCityName = oIntent.getStringExtra(Constants.PARAMS_MYPOS_CITY_NAME);
//		myCityNextName = oIntent.getStringExtra(Constants.PARAMS_MYPOS_CITYNEXT_NAME);
		//レイアウトを初期化する
		LayoutInflater oInflater = LayoutInflater.from(this);
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base, null);
//
		TextView oText = (TextView)oLayout.findViewById(R.id.inquiry_title);
		String sTitle = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
		if(sTitle == null)
		{
			oText.setText("");
		}
		else
		{
// Add by CPJsunagawa '13-12-25 Start
        	if (oIntent.hasExtra(Constants.ADDRESS_SEARCH_DB_ADR))
        	{
        		sTitle += " （" + oIntent.getStringExtra(Constants.ADDRESS_SEARCH_DB_ADR) + "）";
        	}
// Add by CPJsunagawa '13-12-25 End
			oText.setText(sTitle);
		}
//
		//右側のレイアウトを追加する
		LinearLayout oGroupList = (LinearLayout)oLayout.findViewById(R.id.LinearLayout_list);
		oview = new ItemView(this,0,R.layout.inquiry_city_freescroll);
		//yangyang del start Bug617
//		LinearLayout list = (LinearLayout)oInflater.inflate(R.layout.inquiry_base_city, null);
//		oGroupValue = (WhiteBackGroundTextView)oview.findViewById(this, R.id.TextView_GroupId);

		//初期値
//		oGroupValue.setText(R.string.kana_a);
		//yangyang del end Bug617
		initGroupBtn(oview);

		//市区町村のとき、確定ボタンが不表示とする、大字のとき、確定ボタンが表示とする
		Button oBtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
		initCityBtn(oBtn);
		if (oIntent.getBooleanExtra(Constants.PARAMS_IS_SHOW_BUTTON, false)) {
			oBtn.setOnClickListener(OnAllCityListener);
			oBtn.setVisibility(Button.VISIBLE);

		} else {
			oBtn.setVisibility(Button.GONE);
		}

		///////////////////////////////
//		initCount();
//
//		//
//		initJumpList();
		initGroupButton();
//		getCurrentPageShowData(true);
		int iLineIndex = getFirstShowIndex();
		if (ONCE_GET_COUNT != 0) {
			getRecordIndex = iLineIndex/ONCE_GET_COUNT;
		}
		RecIndex= getRecordIndex*ONCE_GET_COUNT;
		getCurrentPageShowData(false);
		//yangyang del start Bug617
//		if (-1 != Index.lcount) {
//			oGroupValue.setText(getGroupValue((int)Index.lcount));
//		} else {
//			oGroupValue.setText(getGroupValue(iLineIndex));
//		}
		//yangyang del end Bug617
//		oBox = (ScrollBox)oview.findViewById(this,R.id.scrollBox);
//		oBox.setAdapter(this);
		oList = (ScrollList)oview.findViewById(this,R.id.scrollList);
//Del 2011/10/06 Z01_h_yamada Start -->
//		oList.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
		oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
		oList.setAdapter(listAdapter);
//Del 2011/07/21 Z01thedoanh Start -->
		//yangyang add start  Bug617
		//oList.setPadding(5, 5, 4, 5);
		//yangyang add end  Bug617
//Del 2011/07/21 Z01thedoanh End <--
		oGroupList.addView(oview.getView(), new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT));

		ScrollTool oTool = new ScrollTool(this);
//		oTool.bindView(oBox);
		oTool.bindView(oList);

		setViewInOperArea(oTool);
		setViewInWorkArea(oLayout);
		oList.scroll(iLineIndex);
		//yangyang del start Bug617
//		oGroupValue.setText(getGroupValue(iLineIndex));
		//yangyang del end Bug617

//		oList.onScrollStateChanged(view, scrollState).setOnScrollListener(this);
	}
	private ListAdapter listAdapter = new BaseAdapter (){

		@Override
		public int getCount() {
			int iCount = CityInquiry.this.getCount();

			if (iCount < 5) {
				iCount = 5;
			}
			return iCount;
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}
		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int wPos, View oView, ViewGroup parent) {
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"getview start wPos==========" + wPos);
			return CityInquiry.this.getView(wPos, oView);
		}

	};
//Add 2011/09/09 Z01_h_yamada Start -->
	private SpannableString makeGroupSpannableText(String groupName, float fontRate)
	{
        SpannableString spannable = new SpannableString(groupName);
        RelativeSizeSpan span = new RelativeSizeSpan(fontRate);
        spannable.setSpan(span, 1, 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannable;
	}
//Add 2011/09/09 Z01_h_yamada End <--

	private void initGroupBtn(ItemView oview) {
//Add 2011/09/09 Z01_h_yamada Start -->
        Resources oRes = getResources();
//Add 2011/09/09 Z01_h_yamada End <--

        Button btn = (Button) oview.findViewById(this, R.id.Btn_Group_a);
//Add 2011/09/09 Z01_h_yamada Start -->
        btn.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_a), Constants.KANA_FONT_RATE));
//Add 2011/09/09 Z01_h_yamada End <--
		btn.setOnClickListener(OnGroupBtnListener);

		btn = (Button) oview.findViewById(this, R.id.Btn_Group_ka);
//Add 2011/09/09 Z01_h_yamada Start -->
        btn.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_ka), Constants.KANA_FONT_RATE));
//Add 2011/09/09 Z01_h_yamada End <--
		btn.setOnClickListener(OnGroupBtnListener);

		btn = (Button) oview.findViewById(this, R.id.Btn_Group_sa);
//Add 2011/09/09 Z01_h_yamada Start -->
        btn.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_sa), Constants.KANA_FONT_RATE));
//Add 2011/09/09 Z01_h_yamada End <--
		btn.setOnClickListener(OnGroupBtnListener);

		btn = (Button) oview.findViewById(this, R.id.Btn_Group_ta);
//Add 2011/09/09 Z01_h_yamada Start -->
        btn.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_ta), Constants.KANA_FONT_RATE));
//Add 2011/09/09 Z01_h_yamada End <--
		btn.setOnClickListener(OnGroupBtnListener);

		btn = (Button) oview.findViewById(this, R.id.Btn_Group_na);
//Add 2011/09/09 Z01_h_yamada Start -->
        btn.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_na), Constants.KANA_FONT_RATE));
//Add 2011/09/09 Z01_h_yamada End <--
		btn.setOnClickListener(OnGroupBtnListener);

		btn = (Button) oview.findViewById(this, R.id.Btn_Group_ha);
//Add 2011/09/09 Z01_h_yamada Start -->
        btn.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_ha), Constants.KANA_FONT_RATE));
//Add 2011/09/09 Z01_h_yamada End <--
		btn.setOnClickListener(OnGroupBtnListener);

		btn = (Button) oview.findViewById(this, R.id.Btn_Group_ma);
//Add 2011/09/09 Z01_h_yamada Start -->
        btn.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_ma), Constants.KANA_FONT_RATE));
//Add 2011/09/09 Z01_h_yamada End <--
		btn.setOnClickListener(OnGroupBtnListener);

		btn = (Button) oview.findViewById(this, R.id.Btn_Group_ya);
//Add 2011/09/09 Z01_h_yamada Start -->
        btn.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_ya), Constants.KANA_FONT_RATE));
//Add 2011/09/09 Z01_h_yamada End <--
		btn.setOnClickListener(OnGroupBtnListener);

		btn = (Button) oview.findViewById(this, R.id.Btn_Group_ra);
//Add 2011/09/09 Z01_h_yamada Start -->
        btn.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_ra), Constants.KANA_FONT_RATE));
//Add 2011/09/09 Z01_h_yamada End <--
		btn.setOnClickListener(OnGroupBtnListener);

		btn = (Button) oview.findViewById(this, R.id.Btn_Group_wa);
//Add 2011/09/09 Z01_h_yamada Start -->
        btn.setText(makeGroupSpannableText(oRes.getString(R.string.kana_group_wa), Constants.KANA_FONT_RATE));
//Add 2011/09/09 Z01_h_yamada End <--
		btn.setOnClickListener(OnGroupBtnListener);

	}

	@Override
	public int getCount() {

		return allRecordCount;
//		return 50;
	}

	@Override
	public int getCountInBox() {
		return 5;
	}

//	private int iDoCount = 0;
//	private boolean bFirstDo = false;
	public int getRecordIndex = 0;
	int wOldPos = 0;

	@Override
	public View getView(final int wPos, View oView) {
		//yangyang mod start Bug617
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"yy getview do wPos===" + wPos);
//		if (iDoCount == 0) {
//			hashIndex.clear();
//		}

//		hashIndex.put(wPos, (iDoCount-1));

		LinearLayout oLayout = null;
		LinearLayout oLayoutBtn = null;
// 		ButtonImg obtn = null;
 		TextView oGroupValue = null;
 		TextView obtn = null;
 		ImageView oCarView = null;
//		if(oView == null)
//		{
//			oLayout = new LinearLayout(this);
//			oLayout.setGravity(Gravity.CENTER_VERTICAL);
//			oLayout.setPadding(5, 0, 5, 0);
//			obtn = new ButtonImg(this);
//			obtn.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
//			obtn.setId(R.id.btnOk);
//			obtn.setTextSize(26);
//			//yangyang mod start Bug617
//			obtn.setBackgroundResource(R.drawable.btn_428_70);
//			//yangyang mod end Bug617
//			//前から「...」とする
//			obtn.setOneLine(obtn);
////			obtn.setSingleLine(true);
////			obtn.setEllipsize(TruncateAt.START);
//			oLayout.addView(obtn);
//
//			oView = oLayout;
//		}
//		else
//		{
//			oLayout = (LinearLayout)oView;
//			obtn = (ButtonImg)oLayout.findViewById(R.id.btnOk);
//		}
		if(oView == null || !(oView instanceof LinearLayout))
		{
			LayoutInflater oInflater = LayoutInflater.from(this);
			oLayout = (LinearLayout)oInflater.inflate(R.layout.button_428_70, null);
//Del 2011/07/21 Z01thedoanh Start -->
			//oLayout.setPadding(5, 0, 5, 0);
//Del 2011/07/21 Z01thedoanh End <--

			oView = oLayout;
		} else {
			oLayout = (LinearLayout)oView;
		}
		oLayoutBtn = (LinearLayout) oLayout.findViewById(R.id.LinearLayout_button);
		oGroupValue = (TextView)oLayout.findViewById(R.id.TextView_groupShow);
		obtn =  (TextView)oLayout.findViewById(R.id.TextView_Value);
		oCarView =  (ImageView)oLayout.findViewById(R.id.ImageView_car);




		// 下にスクロールの場合
		if (wOldPos <= wPos) {
			if (wPos < allRecordCount
					&& ONCE_GET_COUNT != 0
					&& (wPos % ONCE_GET_COUNT == 0 ||
							(getRecordIndex != wPos / ONCE_GET_COUNT))) {
				// 上にスクロール場合
				RecIndex = wPos;
				getCurrentPageShowData(false);
			}
		} else {
			/*総件数76件、最後ページであるし、上にスクロール時、
			 * 70あるいは６９のデータを表示するとき、グレーボタンを表示しないように
			 */
			if (getRecordIndex != wPos / ONCE_GET_COUNT) {
				RecIndex = wPos;
				getCurrentPageShowData(false);
			}
		}
		//yangyang add start Bug617
		String groupValue = getGroupValue(wPos);
		//yangyang add end Bug617

		//yangyang del start Bug617
//		if (oList.getIsScroll()) {
//			oGroupValue.setText(getGroupValue(wPos));
//		}
		//yangyang del end Bug617
//		}
		if (wPos < allRecordCount) {
			//yangyang mod start Bug617
			if (!AppInfo.isEmpty(groupValue)) {
				oGroupValue.setText(Constants.LEFT_BRACKETS + groupValue + Constants.RIGHT_BRACKETS);
//			obtn.setText(Constants.LEFT_BRACKETS + groupValue + Constants.RIGHT_BRACKETS +
//					getButtonValue(wPos));
			} else {
				oGroupValue.setText(Constants.SIX_SPACE);
//				obtn.setText(Constants.SIX_SPACE +
//						getButtonValue(wPos));
			}
			obtn.setText(getButtonValue(wPos));
			//yangyang mod end Bug617
//			obtn.setText(getButtonValue(iDoCount-1));
			if (AppInfo.isEmpty(getButtonValue(wPos))) {
				oLayoutBtn.setEnabled(false);
			} else {
				oLayoutBtn.setEnabled(true);
			}
		} else {
			obtn.setText("");
			oLayoutBtn.setEnabled(false);
		}

		if (myProvinceId == iProvinceId) {
			boolean checkMyCar = false;
			if (getIntent().hasExtra(Constants.FROM_WHICH_SEARCH_KEY)) {
				if (Index.lcount == (wPos + 1)) {
					checkMyCar = true;
				}
			} else {
				if (Index.lcount == wPos) {
					checkMyCar = true;
				}
			}

			if (checkMyCar) {
//				obtn = obtn.initBtn(obtn,R.drawable.icon_current_normal,ButtonImg.RIGHT);
				oCarView.setBackgroundResource(R.drawable.icon_current_normal);
				oCarView.setVisibility(ImageView.VISIBLE);
			} else {
//				obtn = obtn.initBtn(obtn,-1,ButtonImg.RIGHT);
				oCarView.setVisibility(ImageView.GONE);
			}
		} else {
//			obtn = obtn.initBtn(obtn,-1,ButtonImg.RIGHT);
			oCarView.setVisibility(ImageView.GONE);
		}
		//yangyang mod start Bug1032
//		obtn.setOneLine(obtn);
//		obtn.superPadding(5, 8, 10, 12);
		//yangyang mod end Bug1032
		oLayoutBtn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 住所 市町村選択禁止 Start -->
				if(DrivingRegulation.CheckDrivingRegulation()){
					return;
				}
// ADD 2013.08.08 M.Honma 走行規制 住所 市町村選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
				wOldPos = 0;
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"vvvv onclik========" + iIndex);
//Add 2012/02/07 katsuta Start --> #2698
//				NaviLog.d(NaviLog.PRINT_LOG_TAG, "CityInquiry onClick ================== bIsListClicked: " + bIsListClicked);
	   			if (bIsListClicked) {
	   				return;
	   			}
	   			else {
	   				bIsListClicked = true;
	   			}
//Add 2012/02/07 katsuta End <--#2698
				goResultInquiry(oView,wPos);


			}


			} );

		//上にスクロールの場合
		if (wOldPos > wPos) {
			if (wPos < allRecordCount && ONCE_GET_COUNT != 0
					&& wPos >=ONCE_GET_COUNT  &&
					(wPos % ONCE_GET_COUNT == 0 ||
							(getRecordIndex != wPos / ONCE_GET_COUNT))) {
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"getButtonValue wOldPos > wPos  3==== wPos====" + wPos +
//						",==getRecordIndex==" + getRecordIndex + ",==(wPos / ONCE_GET_COUNT)==" + (wPos / ONCE_GET_COUNT));
//				getRecordIndex = wPos/ONCE_GET_COUNT;
				//上にスクロール場合

				RecIndex = wOldPos-ONCE_GET_COUNT;

				getCurrentPageShowData(false);
			}
		}
		//yangyang mod end Bug617
		wOldPos = wPos;
		return oView;
	}
	/**
	 * 市区町村名称前にグループの名
	 * @param wPos 判断のIndex
	 * */

	protected String getGroupValue(int wPos) {
		return "";
	}

	protected int getFirstShowIndex() {

		return 0;
	}
	protected void initJumpList() {

	}

	protected String getButtonValue(int wPos) {
		return null;
	}

	protected void initDataObj() {

	}

	/**
	 * カレント画面に表示した全市区町村の内容を取得する
	 * @param bool 初期化　true
	 *             スクロール　false
	 * */
	protected void getCurrentPageShowData(boolean bool) {

	}

	protected void initCount() {
	}

	/**
	 * layout中にGroupの内容をリセットする
	 * */
	private void setGroupValue(int kanaR) {
		//yangyang del start Bug617
//		oGroupValue.setText(kanaR);
		//yangyang del end Bug617
		String checkValue = this.getResources().getString(kanaR);
		//グループボタンのクリック事件ため、
		//yangyang mod start Bug617
//		if (AppInfo.isEmpty(clickValue) || !clickValue.equals(oGroupValue.getText().toString())) {
		if (AppInfo.isEmpty(clickValue) || !clickValue.equals(checkValue)) {
			//yangyang mdo end Bug617
			clickIndex = 0;
//			clickValue = oGroupValue.getText().toString();
			clickValue = checkValue;
		} else {
			clickIndex++;
			clickIndex = clickIndex % list.size();
		}
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"617 clickIndex==========" + clickIndex);

	}

	protected  void setCurrentGroupStartShow() {

	}
	public int resetIndex(int wPos) {
		if (wPos >= ONCE_GET_COUNT*2) {
			int pageIndex  = wPos/ONCE_GET_COUNT;
			wPos = wPos-ONCE_GET_COUNT*pageIndex+ONCE_GET_COUNT;
		}
		return wPos;
	}
	private OnClickListener OnAllCityListener = new OnClickListener(){

		@Override
		public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 住所 市町村選択での確定禁止 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
//ADD 2013.08.08 M.Honma 走行規制 住所 市町村選択での確定禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
//Add 2012/02/07 katsuta Start --> #2698
//			NaviLog.d(NaviLog.PRINT_LOG_TAG, "CityInquiry onClick ================== bIsListClicked: " + bIsListClicked);
   			if (bIsListClicked) {
   				return;
   			}
   			else {
   				bIsListClicked = true;
   			}
//Add 2012/02/07 katsuta End <--#2698
   			if (oView instanceof Button) {
   	            Button obtn = (Button) oView;
   	            doAllCityClick(obtn);
   			}
		}
	};

	protected void doAllCityClick(Button btn) {

	}
	protected void goResultInquiry(View oView,int wPos) {

	}
	List<String> list = null ;

	private final OnClickListener OnGroupBtnListener = new OnClickListener(){

		@Override
		public void onClick(View oView) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 住所 あかさたな～禁止 Start -->
			if(DrivingRegulation.CheckDrivingRegulation()){
				return;
			}
//ADD 2013.08.08 M.Honma 走行規制 住所 あかさたな～禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
			int btnId = oView.getId();
//			initGroupBtn(btnId);
			initGroupButton();
			oView.setSelected(true);
			int kanaR = 0;
			switch(btnId) {
			case R.id.Btn_Group_a:
				kanaR = R.string.kana_a;
				list = AKeyName;
				break;
			case R.id.Btn_Group_ka:
				kanaR = R.string.kana_ka;
				list = KaKeyName;
				break;
			case R.id.Btn_Group_sa:
				kanaR = R.string.kana_sa;
				list = SaKeyName;
				break;
			case R.id.Btn_Group_ta:
				kanaR = R.string.kana_ta;
				list = TaKeyName;
				break;
			case R.id.Btn_Group_na:
				kanaR = R.string.kana_na;
				list = NaKeyName;
				break;
			case R.id.Btn_Group_ha:
				kanaR = R.string.kana_ha;
				list = HaKeyName;
				break;
			case R.id.Btn_Group_ma:
				kanaR = R.string.kana_ma;
				list = MaKeyName;
				break;
			case R.id.Btn_Group_ya:
				kanaR = R.string.kana_ya;
				list = YaKeyName;
				break;
			case R.id.Btn_Group_ra:
				kanaR = R.string.kana_ra;
				list = RaKeyName;
				break;
			case R.id.Btn_Group_wa:
				kanaR = R.string.kana_wa;
				list = WaKeyName;
				break;

			}
			setGroupValue(kanaR);
			msName = list.get(clickIndex);
//			long size = NameSize;
			setCurrentGroupStartShow();

		}



		};

//	/**
//	 * 500件を表示するため、ページIndexと表示できる総件数を初期化する
//	 *
//	 * @author yangyang
//	 *
//	 * */
//	public void initPageIndexAndCnt() {
////		NaviLog.d(NaviLog.PRINT_LOG_TAG,"371  ListCount.lcount====" + ListCount.lcount);
//		if (ListCount.lcount >= 500) {
//			allRecordCount = 500;
//		} else {
//			allRecordCount = (int) ListCount.lcount;
//		}
//
//	}

	/**
	 * ボタンが使用できるかどうかをチェックする
	 * @param JumpKey JNIから取得した50音の内容
	 * @param JumpRecCount JNIから取得したレコード数
	 * @param isGroupBtnClick グループボタンの処理であるかどうかのフラグ
	 *
	 * */
	//yangyang mod start Bug617
	public void checkGroupBtnShow( JNIJumpKey[] JumpKey, JNIShort JumpRecCount,boolean isGroupBtnClick) {
		//yangyang mod end Bug617
//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"JumpRecCount.getM_sJumpRecCount()====" + JumpRecCount.getM_sJumpRecCount());
		 for (int j = 0; j < JumpRecCount.getM_sJumpRecCount(); j++) {


           if (/*JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_A_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_I_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_U_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_E_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_O_name))*/
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_a))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_i))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_u))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_e))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_o))) {



                   AKeyName.add(JumpKey[j].pucName);
                   showa= true;


           } else if (/*JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_KA_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_KI_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_KU_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_KE_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_KO_name))*/
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ka))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ki))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ku))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ke))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ko))) {

               KaKeyName.add(JumpKey[j].pucName);
               showka= true;

           } else if (/*JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_SA_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_SHI_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_SU_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_SE_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_SO_name))*/
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_sa))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_si))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_su))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_se))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_so))) {

               SaKeyName.add(JumpKey[j].pucName);
               showsa= true;

           } else if (/*JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_TA_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_CHI_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_TSU_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_TE_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_TO_name))*/
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ta))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ti))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_tu))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_te))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_to))) {

               TaKeyName.add(JumpKey[j].pucName);
               showta= true;

           } else if (/*JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_NA_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_NI_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_NU_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_NE_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_NO_name))*/
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_na))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ni))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_nu))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ne))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_no))) {

               NaKeyName.add(JumpKey[j].pucName);
               showna= true;

           } else if (/*JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_HA_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_HI_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_FU_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_HE_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_HO_name))*/
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ha))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_hi))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_hu))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_he))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ho))) {

               HaKeyName.add(JumpKey[j].pucName);
               showha= true;

           } else if (/*JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_MA_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_MI_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_MU_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_ME_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_MO_name))*/
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ma))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_mi))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_mu))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_me))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_mo))) {

               MaKeyName.add(JumpKey[j].pucName);
               showma= true;

           } else if (/*JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_YA_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_YU_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_YO_name))*/
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ya))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_yu))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_yo))) {

               YaKeyName.add(JumpKey[j].pucName);
//	               btnYa.setEnabled(true);
               showya= true;

           } else if (/*JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_RA_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_RI_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_RU_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_RE_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_RO_name))*/
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ra))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ri))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ru))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_re))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_ro))) {
               RaKeyName.add(JumpKey[j].pucName);
               showra= true;

           } else if (/*JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_WA_name))
                   || JumpKey[j].pucName.equals(this.getResources().getString(R.string.btn_WO_name))*/
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_wa))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_wo))||
                   check(JumpKey[j].pucName,this.getResources().getStringArray(R.array.check_group_nn))) {

               WaKeyName.add(JumpKey[j].pucName);
               showwa= true;

           }
       }

	}



	public void initGroupButton() {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"initGroupButton do");
		Button btn = null;

		if (showa) {
			btn = (Button) oview.findViewById(this,R.id.Btn_Group_a);
			btn.setEnabled(true);
			btn.setSelected(false);
		}
		if (showka){
			btn = (Button) oview.findViewById(this,R.id.Btn_Group_ka);
			btn.setEnabled(true);
			btn.setSelected(false);
		}
		if (showsa) {
			btn = (Button) oview.findViewById(this,R.id.Btn_Group_sa);
			btn.setEnabled(true);
			btn.setSelected(false);
		}
		if (showta) {
			btn = (Button) oview.findViewById(this,R.id.Btn_Group_ta);
			btn.setEnabled(true);
			btn.setSelected(false);
		}
		if (showna) {
			btn = (Button) oview.findViewById(this,R.id.Btn_Group_na);
			btn.setEnabled(true);
			btn.setSelected(false);
		}
		if (showha) {
			btn = (Button) oview.findViewById(this,R.id.Btn_Group_ha);
			btn.setEnabled(true);
			btn.setSelected(false);
		}
		if (showma) {
			btn = (Button) oview.findViewById(this,R.id.Btn_Group_ma);
			btn.setEnabled(true);
			btn.setSelected(false);
		}
		if (showya) {
			btn = (Button) oview.findViewById(this,R.id.Btn_Group_ya);
			btn.setEnabled(true);
			btn.setSelected(false);
		}
		if (showra) {
			btn = (Button) oview.findViewById(this,R.id.Btn_Group_ra);
			btn.setEnabled(true);
			btn.setSelected(false);
		}
		if (showwa) {
			btn = (Button) oview.findViewById(this,R.id.Btn_Group_wa);
			btn.setEnabled(true);
			btn.setSelected(false);

		}

//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"initGroupButton end");
	}
	@Override
	protected boolean onStartShowPage() throws Exception {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"onStartShowPage start");
		Intent oIntent = getIntent();

		isGroupBtnClick= false;
		iProvinceId = oIntent.getShortExtra(Constants.PARAMS_WIDECODE, (short) 0);
		myProvinceId = oIntent.getIntExtra(Constants.PARAMS_MYPOS_PROVINCE, 0) + 1;

//
		initDataObj();
		initCount();
		initJumpList();
//		getFirstShowIndex();
		return true;
	}
	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {
		if(bGetData)
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage end");
					initDialog();
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage end");
				}



			});
		}

	}

//	@Override
//	public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
//
//
//	}
//
//	@Override
//	public void onScrollStateChanged(AbsListView view, int scrollState) {
//		if (scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
//			iDoCount = 0;
//		}
//
//	}
}
