/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           Java_IconInfo.java
 * Description    アイコン情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class Java_IconInfo {
    private short sCalcAngle;
    private int cAngleNo;
    private short sIconInfo;
    /**
    * Created on 2010/08/06
    * Title:       getSIconInfo
    * Description:  角度を取得する
    * @param1  無し
    * @return       short

    * @version        1.0
    */
    public short getSIconInfo() {
        return sIconInfo;
    }

    /**
    * Created on 2010/08/06
    * Title:       getSCalcAngle
    * Description:  丸めた角度（0〜40）を取得する
    * @param1  無し
    * @return       short

    * @version        1.0
    */
    public short getSCalcAngle() {
        return sCalcAngle;
    }

    /**
    * Created on 2010/08/06
    * Title:       getCAngleNo
    * Description:  角度の方位（0〜7）を取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getCAngleNo() {
        return cAngleNo;
    }
}
