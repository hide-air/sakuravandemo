package net.zmap.android.pnd.v2.data;


public class DC_MapIconData {
	public String 	m_sID;				//アイコンID
	public String 	m_sURL;				//アイコンURL
	public String 	m_sIconPath;		//アイコンファイルパス
	public Integer 	m_nX = 0;			//アイコンホットスポットX座標
	public Integer 	m_nY = 0;			//アイコンホットスポットY座標
}
