package net.zmap.android.pnd.v2.maps;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.ImageManager;
import net.zmap.android.pnd.v2.data.ZLandmarkData_t;

public class ICListAdapter extends BaseAdapter{

	private LayoutInflater m_oInflater;
	private int[] m_iResId = null;
	private String[] m_szNodeName = null;
	private String[] m_szRestTime = null;
	private String[] m_szDistance = null;
	private String[] m_szToll = null;
	private ZLandmarkData_t[][] m_stSAPAImage;

	public ICListAdapter(Context context, int[] iResId, String[] szNodeName, String[] szRestTime, String[] szDistance, String[] szToll, ZLandmarkData_t[][] stSAPAResId)
	{
//Add 2011/08/16 Z01thedoanh Start -->
		this.m_oInflater = LayoutInflater.from(context);
//Add 2011/08/16 Z01thedoanh End <--
		init(context, iResId, szNodeName, szRestTime, szDistance, szToll, stSAPAResId);
	}

    public void init(Context oContext, int[] iResId, String[] szNodeName, String[] szRestTime, String[] szDistance, String[] szToll, ZLandmarkData_t[][] stSAPAResId)
    {
//Del 2011/08/16 Z01thedoanh Start -->
    	//this.m_oInflater = LayoutInflater.from(oContext);
//Del 2011/08/16 Z01thedoanh End <--
    	m_iResId = iResId;
    	m_szNodeName = szNodeName;
    	m_szRestTime = szRestTime;
    	m_szDistance = szDistance;
    	m_szToll = szToll;
    	m_stSAPAImage = stSAPAResId;
    }

	@Override
	public int getCount() {
        if(null != m_szDistance){
        	return m_szDistance.length;
        }
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"---getView, position is "+position+"  converView is "+convertView);
		ViewHolder holder;
		if(null == convertView){
			convertView = m_oInflater.inflate(R.layout.navi_guide_list_item, null);

//Del 2011/09/25 Z01_h_yamada Start -->
////Add 2011/07/29 Z01thedoanh (自由解像度対応) Start -->
//			LinearLayout txtInfo = (LinearLayout)convertView.findViewById(R.id.txt_info);
//			LinearLayout layoutMainSAPAInfo = (LinearLayout)convertView.findViewById(R.id.layoutMainSAPAInfo);
//			LinearLayout imgKindOfFlag = (LinearLayout)convertView.findViewById(R.id.imgKindOfFlag);
//	        RelativeLayout.LayoutParams rParams = new RelativeLayout.LayoutParams((int) (Constants.NAVI_GUIDE_LIST_ITEM_TEXT_INFO_WIDTH/CommonLib.getGuideBackGroundScale()), -2);
//	        txtInfo.setLayoutParams(rParams);
//	        rParams = new RelativeLayout.LayoutParams((int) (Constants.NAVI_GUIDE_LIST_ITEM_TEXT_INFO_WIDTH/CommonLib.getGuideBackGroundScale()), -2);
//	        rParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//	        layoutMainSAPAInfo.setLayoutParams(rParams);
//	        rParams = new RelativeLayout.LayoutParams((int) (Constants.NAVI_GUIDE_LIST_ITEM_TEXT_INFO_WIDTH/CommonLib.getGuideBackGroundScale()), -2);
//	        rParams.leftMargin = (int) (280/CommonLib.getGuideBackGroundScale());
//	        imgKindOfFlag.setLayoutParams(rParams);
////Add 2011/07/29 Z01thedoanh (自由解像度対応) End <--
//Del 2011/09/25 Z01_h_yamada End <--


			holder = new ViewHolder();
            holder.imgFlag = (ImageView)convertView.findViewById(R.id.imgFlag);
            holder.layoutSAPAInfo = (LinearLayout)convertView.findViewById(R.id.layoutSAPAInfo);
            holder.imgSAPA1 = (ImageView)convertView.findViewById(R.id.sa_1);
            holder.imgSAPA2 = (ImageView)convertView.findViewById(R.id.sa_2);
            holder.imgSAPA3 = (ImageView)convertView.findViewById(R.id.sa_3);
            holder.imgSAPA4 = (ImageView)convertView.findViewById(R.id.sa_4);
            holder.imgSAPA5 = (ImageView)convertView.findViewById(R.id.sa_5);
            holder.imgSAPA6 = (ImageView)convertView.findViewById(R.id.sa_6);
            holder.imgSAPA7 = (ImageView)convertView.findViewById(R.id.sa_7);
            holder.imgSAPA8 = (ImageView)convertView.findViewById(R.id.sa_8);
            holder.txtKm = (TextView) convertView.findViewById(R.id.txtKm);
            holder.txtMoney = (TextView) convertView.findViewById(R.id.txtMoney);
            holder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            holder.txtTime = (TextView) convertView.findViewById(R.id.txtTime);

            convertView.setTag(holder);
		}else{
			holder = (ViewHolder)convertView.getTag();
			//return convertView;
		}
		/*if(0==m_iResId[position]){
			holder.imgFlag.setVisibility(View.INVISIBLE);
		}else{*/
			holder.imgFlag.setImageResource(m_iResId[position]);
		//}

		holder.txtName.setText(m_szNodeName[position]);
		if(null != m_szDistance[position]){
//Chg 2011/09/25 Z01_h_yamada Start -->
//			//Chg 2011/08/15 Z01_h_yamada Start -->
////			CommonLib.setDistenceText(holder.txtKm, m_szDistance[position], 20);
////--------------------------------------------
//			CommonLib.setDistenceText(holder.txtKm, m_szDistance[position], (int)(holder.txtKm.getTextSize()*Constants.DISTANCE_UNIT_FONT_RATE+0.5f));
////Chg 2011/08/15 Z01_h_yamada End <--
//--------------------------------------------
			// 自動文字サイズ調整のため、先に一回TEXTを設定する
			// これをしないと、文字サイズがパラつく
			holder.txtKm.setText(m_szDistance[position]);
			CommonLib.setDistenceText(holder.txtKm, m_szDistance[position], (int)(holder.txtKm.getTextSize()*Constants.DISTANCE_UNIT_FONT_RATE+0.5f));
//Chg 2011/09/25 Z01_h_yamada End <--
		}

        //if(null != m_szToll[position]){
        	holder.txtMoney.setText(m_szToll[position]);
        //}
        //if(null != m_szRestTime[position]){
        	holder.txtTime.setText(m_szRestTime[position]);
        //}
		//holder.layoutSAPAInfo.removeAllViews();


		if(null != m_stSAPAImage[position]){
			for(int i = 0; i < m_stSAPAImage[position].length; i++){
                if(null != m_stSAPAImage[position][i]){
                	switch (i){
    		        case 0:
    		        	holder.imgSAPA1.setVisibility(View.VISIBLE);
    		            (holder.imgSAPA1).setImageBitmap(ImageManager.getImage(m_stSAPAImage[position][i].getSPixRGBA32(), (int)m_stSAPAImage[position][i].getSWidth(), (int)m_stSAPAImage[position][i].getSHeight()));
    		            break;
    		        case 1:
    		        	holder.imgSAPA2.setVisibility(View.VISIBLE);
    		            (holder.imgSAPA2).setImageBitmap(ImageManager.getImage(m_stSAPAImage[position][i].getSPixRGBA32(), (int)m_stSAPAImage[position][i].getSWidth(), (int)m_stSAPAImage[position][i].getSHeight()));
    		            break;
    		        case 2:
    		        	holder.imgSAPA3.setVisibility(View.VISIBLE);
    		            (holder.imgSAPA3).setImageBitmap(ImageManager.getImage(m_stSAPAImage[position][i].getSPixRGBA32(), (int)m_stSAPAImage[position][i].getSWidth(), (int)m_stSAPAImage[position][i].getSHeight()));
    		            break;
    		        case 3:
    		        	holder.imgSAPA4.setVisibility(View.VISIBLE);
    		            (holder.imgSAPA4).setImageBitmap(ImageManager.getImage(m_stSAPAImage[position][i].getSPixRGBA32(), (int)m_stSAPAImage[position][i].getSWidth(), (int)m_stSAPAImage[position][i].getSHeight()));
    		            break;
    		        case 4:
    		        	holder.imgSAPA5.setVisibility(View.VISIBLE);
    		            (holder.imgSAPA5).setImageBitmap(ImageManager.getImage(m_stSAPAImage[position][i].getSPixRGBA32(), (int)m_stSAPAImage[position][i].getSWidth(), (int)m_stSAPAImage[position][i].getSHeight()));
    		            break;
    		        case 5:
    		        	holder.imgSAPA6.setVisibility(View.VISIBLE);
    		            (holder.imgSAPA6).setImageBitmap(ImageManager.getImage(m_stSAPAImage[position][i].getSPixRGBA32(), (int)m_stSAPAImage[position][i].getSWidth(), (int)m_stSAPAImage[position][i].getSHeight()));
    		            break;
    		        case 6:
    		        	holder.imgSAPA7.setVisibility(View.VISIBLE);
    		            (holder.imgSAPA7).setImageBitmap(ImageManager.getImage(m_stSAPAImage[position][i].getSPixRGBA32(), (int)m_stSAPAImage[position][i].getSWidth(), (int)m_stSAPAImage[position][i].getSHeight()));
    		            break;
    		        case 7:
    		        	holder.imgSAPA8.setVisibility(View.VISIBLE);
    		            (holder.imgSAPA8).setImageBitmap(ImageManager.getImage(m_stSAPAImage[position][i].getSPixRGBA32(), (int)m_stSAPAImage[position][i].getSWidth(), (int)m_stSAPAImage[position][i].getSHeight()));
    		            break;
    		        default:
    		        	break;
    		        }
                }else{
                	switch(i){
                	case 0:
                		holder.imgSAPA1.setVisibility(View.GONE);
                		break;
                	case 1:
                		holder.imgSAPA2.setVisibility(View.GONE);
                		break;
                	case 2:
                		holder.imgSAPA3.setVisibility(View.GONE);
                		break;
                	case 3:
                		holder.imgSAPA4.setVisibility(View.GONE);
                		break;
                	case 4:
                		holder.imgSAPA5.setVisibility(View.GONE);
                		break;
                	case 5:
                		holder.imgSAPA6.setVisibility(View.GONE);
                		break;
                	case 6:
                		holder.imgSAPA7.setVisibility(View.GONE);
                		break;
                	case 7:
                		holder.imgSAPA8.setVisibility(View.GONE);
                		break;
                	default:
                		break;
                	}
                }
			}
		}
		return convertView;
	}

	static class ViewHolder {
		/**
	     * SA/PA案内
	     */
	    LinearLayout layoutSAPAInfo;
	    ImageView imgSAPA1;
	    ImageView imgSAPA2;
	    ImageView imgSAPA3;
	    ImageView imgSAPA4;
	    ImageView imgSAPA5;
	    ImageView imgSAPA6;
	    ImageView imgSAPA7;
	    ImageView imgSAPA8;
	    /**
	     * 高速道路料金
	     */
	    TextView txtMoney;

	    /**
	     * Name
	     */
	    TextView txtName;

	    /**
	     * 距離
	     */
	    TextView txtKm;
	    /**
	     * 時間
	     */
	    TextView txtTime;
	    /**
	     * JCT/IC Icon
	     */
	    ImageView imgFlag;

	}

}