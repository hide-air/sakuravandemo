package net.zmap.android.pnd.v2.engine;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import net.zmap.android.pnd.v2.data.NaviRun;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;

public class PlatformWrapper {

	public final static int HORIZONTAL_TB = 0;
	public final static int VERTICAL_RL = 1;
	public final static int VERTICAL_LR = 2;
// ADD.2013.09.09 N.Sasao 名称座布団描画 再調整 START
	public final static int CORRECTION_HORIZONTAL_TEXTURE_XSIZE = 6;
	public final static int CORRECTION_HORIZONTAL_TEXTURE_YSIZE = CORRECTION_HORIZONTAL_TEXTURE_XSIZE / 2;
	public final static int CORRECTION_VERTICAL_TEXTURE_YSIZE = 6;
	public final static int CORRECTION_VERTICAL_TEXTURE_XSIZE = CORRECTION_VERTICAL_TEXTURE_YSIZE / 2;
	public final static int CORRECTION_FRAME_SIZE = 2;
// ADD.2013.09.09 N.Sasao 名称座布団描画 再調整  END

	static private int roundup2(int value) {
		int i = 1;
		for (;;) {
			if (i >= value) {
				return i;
			}
			i <<= 1;
		}
	}

	static public class GLTextureInfo
	{
		GLTextureInfo(int texID,int w,int h)
		{
			textureID = texID;
			width = w;
			height = h;
		}
		public int textureID;
		public int width;
		public int height;
	}
	static public class DrawTextSizeInfo
	{
		DrawTextSizeInfo(int w,int h,int baseline_h)
		{
			baselineHeight = baseline_h;
			width = w;
			height = h;
		}
		public int baselineHeight;
		public int width;
		public int height;
	}

	public static Context appContext = null;
	public static void init(Context applicationContext)
	{
		appContext = applicationContext;
	}

	public static DrawTextSizeInfo CalcDrawTextSize(String text,float fontSize,boolean bold,int writing_mode)
	{
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setTextSize(fontSize);
		if (bold == false) {
			paint.setTypeface(Typeface.DEFAULT);
		} else {
			paint.setTypeface(Typeface.DEFAULT_BOLD);
		}
		return CalcDrawTextSize(text,paint,writing_mode);
	}

	public static DrawTextSizeInfo CalcDrawTextSize(String text,Paint paint,int writing_mode)
	{
		if(writing_mode == HORIZONTAL_TB)
		{
			return CalcDrawTextSizeHorizontal(text,paint);
		}
		else if(writing_mode == VERTICAL_RL || writing_mode == VERTICAL_LR)
		{
			return CalcDrawTextSizeVertical(text,paint);
		}

		return new DrawTextSizeInfo( 0,0,0);
	}
	public static DrawTextSizeInfo CalcDrawTextSizeHorizontal(String text,Paint paint)
	{
		float width = 0.0f;

		String[] lines = text.split("\\r");

		for(String line:lines)
		{
			width = Math.max(CalcDrawHorizontal1LineTextWidth(line,paint),width);
		}
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整 START
		float fScale = 1.0f;
		if( NaviRun.GetNaviRunObj().isJudeValidGraphicMode() ){
			fScale = NaviRun.GetNaviRunObj().getGraphicModeControlScale( true );
		}
		int nCorrectTextureXSize = (int)(CORRECTION_HORIZONTAL_TEXTURE_XSIZE * fScale);
		int nCorrectTextureYSize = (int)(CORRECTION_HORIZONTAL_TEXTURE_YSIZE * fScale);
		return new DrawTextSizeInfo( (int) (width+nCorrectTextureXSize),(int) (paint.getFontSpacing() * lines.length) + nCorrectTextureYSize,paint.getFontMetricsInt().leading);
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整  END
	}
	public static DrawTextSizeInfo CalcDrawTextSizeVertical(String text,Paint paint)
	{
		float height = 0.0f;

		String[] lines = text.split("\\r");


		for(String line:lines)
		{
			height = Math.max(CalcDrawVertical1LineTextHeight(line,paint),height);
		}

// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整 START
		float fScale = 1.0f;
		if( NaviRun.GetNaviRunObj().isJudeValidGraphicMode() ){
			fScale = NaviRun.GetNaviRunObj().getGraphicModeControlScale( true );
		}
		int nCorrectTextureXSize = (int)(CORRECTION_VERTICAL_TEXTURE_XSIZE * fScale);
		int nCorrectTextureYSize = (int)(CORRECTION_VERTICAL_TEXTURE_YSIZE * fScale);
		return new DrawTextSizeInfo( (int) (paint.getFontSpacing()* lines.length) + nCorrectTextureXSize,(int) (height +nCorrectTextureYSize),paint.getFontMetricsInt().leading);
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整  END
	}

	public static float CalcDrawVertical1LineTextHeight(String text,Paint paint)
	{
		FontMetrics fm = paint.getFontMetrics();
		float charHeight = paint.getFontSpacing() + (fm.top - fm.ascent) - (fm.bottom - fm.descent);
		float height = charHeight * text.length();

		return height + fm.bottom;
	}
	public static float CalcDrawHorizontal1LineTextWidth(String text,Paint paint)
	{
		float width = 0.0f;
		float[] widths = new float[text.length()];
		int num = paint.getTextWidths(text, widths);
		for (int i = 0; i < num; i++)
		{
			width += widths[i];
		}
		return width;
	}



	public static void drawTextVerticalRL(Canvas target,String text,float startOffsetX,float startOffsetY,int targetWidth,int targetHeight,Paint paint)
	{
		int lineCount=0;
		float lineWidth = paint.getFontSpacing();
		FontMetrics fm = paint.getFontMetrics();
		float charHeight = paint.getFontSpacing() + (fm.top - fm.ascent) - (fm.bottom - fm.descent);

		float OriginX = targetWidth - lineWidth -startOffsetX;
		float OriginY = startOffsetY;


		String[] lines = text.split("\\r");

		for(String line:lines)
		{
			int textPos = 0;
			//サロゲートペア無視してます.
			for(int i=0;i<line.length();i++)
			{
				String drawText = line.substring(i, i+1);

				float charWidth =0;
				float[] widths = new float[drawText.length()];
				int num = paint.getTextWidths(drawText, widths);
				for (int w = 0; w < num; w++)
				{
					charWidth += widths[w];
				}
				float offsetX = (lineWidth - charWidth)/2;


				target.drawText(drawText, OriginX - (lineWidth * lineCount) + offsetX, OriginY + textPos * charHeight, paint);
				textPos +=1;
			}
			lineCount +=1;
		}

	}
	public static void drawTextHorizontal(Canvas target,String text,float startOffsetX,float startOffsetY,int targetWidth,int targetHeight,Paint paint)
	{
		int lineCount=0;
		float lineHeight = paint.getFontSpacing();

		String[] lines = text.split("\\r");

		for(String line:lines)
		{
			target.drawText(line, startOffsetX, startOffsetY + lineHeight * lineCount, paint);
			lineCount +=1;
		}
	}


	/**
	 * 文字列のテクスチャを作成します.
	 *
	 * @return OpenGL Texture ID
	 */
	public static GLTextureInfo CreateFontTexture(String text,int color,int hemmingColor,float fontSize,boolean bold,boolean fillRect,int[] backImagePixels,int backImageWidth,int backImageHeight,float backImageScale,float edgeSize,int writing_mode)
	{
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setTextSize(fontSize);
		if (bold == false) {
			paint.setTypeface(Typeface.DEFAULT);
		} else {
			paint.setTypeface(Typeface.DEFAULT_BOLD);
		}
		Canvas canvas = new Canvas();

		DrawTextSizeInfo sizeInfo = CalcDrawTextSize(text,paint,writing_mode);

		float edge_size = edgeSize;
		sizeInfo.width += edge_size*2;
		sizeInfo.height += edge_size*2;

		Point size = new Point(sizeInfo.width,sizeInfo.height);
		Point adjustPos = new Point(0,0);

		Bitmap backBitmap = null;

		if(backImagePixels != null)
		{
			size.x = (int) (backImageWidth * backImageScale);
			size.y = (int) (backImageHeight * backImageScale);

			adjustPos = new Point((int)((size.x - sizeInfo.width)*0.5),(int)((size.y - sizeInfo.height)*0.5));
			backBitmap = Bitmap.createBitmap(backImageWidth,backImageHeight,Bitmap.Config.ARGB_8888);
			backBitmap.setPixels(backImagePixels, 0,backImageWidth, 0, 0, backImageWidth, backImageHeight);
		}

		int twidth = roundup2(size.x);
		int theight = roundup2(size.y);

		Bitmap textBitmap = Bitmap.createBitmap(twidth,theight,Bitmap.Config.ARGB_8888);

		textBitmap.eraseColor(Color.TRANSPARENT);
		canvas.setBitmap(textBitmap);
		canvas.translate(0, theight);
		canvas.scale(1, -1);

		if(backBitmap != null)
		{
			Rect srcRect = new Rect();
			Rect dstRect = new Rect();
			srcRect.set(0, 0, backBitmap.getWidth(), backBitmap.getHeight());
			dstRect.set(0, 0, size.x,size.y);
			canvas.drawBitmap(backBitmap, srcRect, dstRect, null);
			backBitmap.recycle();
			backBitmap=null;
		}

		float fScale = 1.0f;
		if( NaviRun.GetNaviRunObj().isJudeValidGraphicMode() ){
			fScale = NaviRun.GetNaviRunObj().getGraphicModeControlScale( true );
		}
		int nCorrectFrameSize = (int)(CORRECTION_FRAME_SIZE * fScale);

		if (hemmingColor != Color.TRANSPARENT) {
			if(fillRect)
			{
				Rect rect = new Rect();
				rect.set(0, 0, size.x,size.y);
// MOD.2013.07.30 N.Sasao 名称座布団描画  背景色判定無効 START
//				if(hemmingColor == 0xFFFFFFFF)
//				{
					paint.setColor(color);
					canvas.drawRect(rect, paint);
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整 START
					rect.set(nCorrectFrameSize, nCorrectFrameSize, size.x-nCorrectFrameSize,size.y-nCorrectFrameSize);
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整  END
//				}
// MOD.2013.07.30 N.Sasao 名称座布団描画  背景色判定無効  END
				paint.setColor(hemmingColor);
				canvas.drawRect(rect, paint);
			}
// MOD.2013.07.30 N.Sasao 名称座布団描画  地図名称 描画オフセット微調整 START
			else
			{
				paint.setColor(hemmingColor);
				paint.setStyle(Paint.Style.STROKE);
				paint.setStrokeWidth(edge_size * 2);
				if(HORIZONTAL_TB == writing_mode)
				{
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整 START
					int nCorrectionTextPoint_x = nCorrectFrameSize + 1;
					int nCorrectionTextPoint_y = nCorrectFrameSize;
					drawTextHorizontal(canvas,text,edge_size + adjustPos.x + nCorrectionTextPoint_x, -paint.ascent() + edge_size+ adjustPos.y + nCorrectionTextPoint_y,size.x,size.y, paint);
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整  END
				}
				else if(VERTICAL_RL == writing_mode)
				{
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整 START
					int nCorrectionTextPoint_x = nCorrectFrameSize;
					int nCorrectionTextPoint_y = nCorrectFrameSize + 2;
					drawTextVerticalRL(canvas,text,edge_size + adjustPos.x + nCorrectionTextPoint_x, -paint.ascent() + edge_size+ adjustPos.y +nCorrectionTextPoint_y,size.x,size.y, paint);
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整  END
				}
				else
				{
					assert(false);

				}
			}
		}

		paint.setColor(color);
		paint.setStyle(Paint.Style.FILL);
		paint.setStrokeWidth(0.0f);
		if(HORIZONTAL_TB == writing_mode)
		{
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整 START
			int nCorrectionTextPoint_x = nCorrectFrameSize + 1;
			int nCorrectionTextPoint_y = nCorrectFrameSize;
			drawTextHorizontal(canvas,text,edge_size + adjustPos.x +nCorrectionTextPoint_x, -paint.ascent() + edge_size+ adjustPos.y+nCorrectionTextPoint_y,size.x,size.y, paint);
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整  END
		}
		else if(VERTICAL_RL == writing_mode)
		{
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整 START
			int nCorrectionTextPoint_x = nCorrectFrameSize;
			int nCorrectionTextPoint_y = nCorrectFrameSize + 2;
			drawTextVerticalRL(canvas,text,edge_size + adjustPos.x + nCorrectionTextPoint_x, -paint.ascent() + edge_size+ adjustPos.y +nCorrectionTextPoint_y,size.x,size.y, paint);
// MOD.2013.09.09 N.Sasao 名称座布団描画 再調整  END
		}
// MOD.2013.07.30 N.Sasao 名称座布団描画  地図名称 描画オフセット微調整  END
		else
		{
			assert(false);
		}

		int[] textures = new int[1];
		GLES20.glGenTextures(1, textures, 0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0]);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, textBitmap, 0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);

		textBitmap.recycle();

		return new GLTextureInfo(textures[0],twidth,theight);
	}

	private static byte[] getBytes(InputStream is) {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		OutputStream os = new BufferedOutputStream(b);
		int c;
		try {
			while ((c = is.read()) != -1) {
				os.write(c);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				try {
					os.flush();
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return b.toByteArray();
	}
	public static byte[] GetAssetFileData(String assetFileName)
	{

		try {
			AssetManager assetManager = appContext.getAssets();
			InputStream fileStream = assetManager.open(assetFileName);
			return getBytes(fileStream);
		} catch (IOException e) {
	        Log.w("naviengine", assetFileName +":file can not read");
			return null;
		}
	}
}
