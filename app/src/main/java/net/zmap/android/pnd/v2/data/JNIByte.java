/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           JNIByte.java
 * Description    Byte Class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class JNIByte {
    private byte m_bScale;

    /**
    * Created on 2010/08/06
    * Title:       getM_bScale
    * Description:  byteの値を取得する
    * @param1  無し
    * @return       byte

    * @version        1.0
    */
    public byte getM_bScale() {
        return m_bScale;
    }

}
