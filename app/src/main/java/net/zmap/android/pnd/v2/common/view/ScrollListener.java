package net.zmap.android.pnd.v2.common.view;

public interface ScrollListener
{
	public void moveUp();
	public void moveDown();
	public boolean isHasNext();
	public boolean isHasPrevious();
	public int getMaxShow();
	public int getCount();
	public int getPos();
	public void setScrollToolListener(ScrollToolListener oListener);
}
