package net.zmap.android.pnd.v2.api.exception;

/**
 * ナビサービス例外ベースクラス
 */
public class NaviException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * エラー詳細メッセージとして null を設定して NaviException を構築します。
     */
    public NaviException() {
        super();
    }

    /**
     * 詳細メッセージを指定して NaviException を構築します。
     *
     * @param message
     *            詳細メッセージ
     */
    public NaviException(String message) {
        super(message);
    }
}