package net.zmap.android.pnd.v2.api.exception;

/**
 * 範囲外の値、添字オーバー、件数オーバー
 */
public class NaviOutOfRangeException extends NaviException {
    private static final long serialVersionUID = 1L;

    /**
     * エラー詳細メッセージとして null を設定して NaviOutOfRangeException を構築します。
     */
    public NaviOutOfRangeException() {
        super();
    }

    /**
     * 詳細メッセージを指定して NaviOutOfRangeException を構築します。
     *
     * @param message
     *            詳細メッセージ
     */
    public NaviOutOfRangeException(String message) {
        super(message);
    }
}