package net.zmap.android.pnd.v2.api;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import net.zmap.android.pnd.v2.api.event.ApplicationStatusListener;
import net.zmap.android.pnd.v2.api.event.Area;
import net.zmap.android.pnd.v2.api.event.AreaListener;
import net.zmap.android.pnd.v2.api.event.IApplicationStatusListener;
import net.zmap.android.pnd.v2.api.event.IAreaListener;
import net.zmap.android.pnd.v2.api.event.IRoadListener;
import net.zmap.android.pnd.v2.api.event.IRouteCalcListener;
import net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener;
import net.zmap.android.pnd.v2.api.event.IVehiclePositionListener;
import net.zmap.android.pnd.v2.api.event.Road;
import net.zmap.android.pnd.v2.api.event.RoadListener;
import net.zmap.android.pnd.v2.api.event.RouteCalcListener;
import net.zmap.android.pnd.v2.api.event.RouteCalculated;
import net.zmap.android.pnd.v2.api.event.RouteGuidance;
import net.zmap.android.pnd.v2.api.event.RouteGuidanceListener;
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応 START
import net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener;
import net.zmap.android.pnd.v2.api.event.VehiclePositionEx;
import net.zmap.android.pnd.v2.api.event.VehiclePositionExListener;
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応  END
import net.zmap.android.pnd.v2.api.event.VehiclePositionListener;
import net.zmap.android.pnd.v2.api.exception.NaviEngineException;
import net.zmap.android.pnd.v2.api.exception.NaviException;
import net.zmap.android.pnd.v2.api.exception.NaviNotFoundException;
import net.zmap.android.pnd.v2.api.exception.NaviRemoteException;
import net.zmap.android.pnd.v2.api.exception.NaviTimeoutException;
import net.zmap.android.pnd.v2.api.map.DayNightMode;
import net.zmap.android.pnd.v2.api.map.ZoomLevel;
import net.zmap.android.pnd.v2.api.navigation.RemainingDistance;
import net.zmap.android.pnd.v2.api.navigation.RoutePoint;
import net.zmap.android.pnd.v2.api.navigation.RouteRequest;
import net.zmap.android.pnd.v2.api.overlay.Icon;
import net.zmap.android.pnd.v2.api.overlay.Line;
import net.zmap.android.pnd.v2.api.poi.FavoritesCategory;
import net.zmap.android.pnd.v2.sakuracust.DebugLogOutput;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/** APIクラス
 *
 */
public final class ItsmoNaviDriveExternalApi extends AbstractExternalApi {

    private static final String                    TAG                           = "ItsmoNaviDriveExternalApi";

    private static final ItsmoNaviDriveExternalApi mInstance                     = new ItsmoNaviDriveExternalApi();

    private List<VehiclePositionListener>          mVehiclePositionListeners     = new ArrayList<VehiclePositionListener>();
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応 START
    private List<VehiclePositionExListener>        mVehiclePositionExListeners   = new ArrayList<VehiclePositionExListener>();
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応  END
    private List<RoadListener>                     mRoadListeners                = new ArrayList<RoadListener>();

    private List<AreaListener>                     mAreaListeners                = new ArrayList<AreaListener>();

//    private List<MapControlListener>               mMapControlListeners          = new ArrayList<MapControlListener>();

    private List<RouteGuidanceListener>            mRouteGuidanceListeners       = new ArrayList<RouteGuidanceListener>();

    private List<RouteCalcListener>                mRouteCalcListeners           = new ArrayList<RouteCalcListener>();

    private List<ApplicationStatusListener>        mApplicationStatusListeners   = new ArrayList<ApplicationStatusListener>();

    private INaviService                           mNaviService                  = null;

    private static final String                    SERVICE_CNAME                 = "net.zmap.android.pnd.v2.NaviService";

    private ApplicationStatusListener              mMyApplicationStatusListener  = new ApplicationStatusListener() {
        @Override
        public void onStopped() {
        }
        @Override
        public void onStarted() {
            dequeueAllApis();
            unregisterApplicationStatusListener(mMyApplicationStatusListener);
        }
    };

    /**
     * コンストラクタ
     */
    private ItsmoNaviDriveExternalApi() {
        super(SERVICE_CNAME);
    }

    /**
     * ナビサービスのインスタンスを取得します。 (シングルトン)<br />
     * <br />
     *
     * ナビサービス (<code>NaviApi</code>) が提供するすべての API は [関連項目] の通りです。
     *
     * @see #openMap(boolean)
     * @see #openMap(GeoPoint, int, boolean)
     * @see #setCenter(GeoPoint)
     * @see #getCenter()
     * @see #setZoom(int)
     * @see #getZoom()
     * @see #setDayNightMode(int)
     *
     * @see #drawIcon(String, Icon)
     * @see #drawLine(String, Line)
     * @see #removeLayer(String)
     * @see #removeAllLayers()
     *
     * @see #getRemainingDistance()
     *
     * @see #calculateRoute(RouteRequest, boolean)
     * @see #startRouteGuidance()
     * @see #stopRouteGuidance()
     * @see #pauseRouteGuidance()
     * @see #resumeRouteGuidance()
     *
     * @see #addFavorites(RoutePoint, int)
     *
     * @see #registerVehiclePositionListener(VehiclePositionListener)
     * @see #registerRoadListener(RoadListener)
     * @see #registerAreaListener(AreaListener)
     * @see #registerRouteGuidanceListener(RouteGuidanceListener)
     * @see #registerRouteCalcListener(RouteCalcListener)
     * @see #registerApplicationStatusListener(ApplicationStatusListener)
     * @see #registerVehiclePositionExListener(VehiclePositionExListener)
     * @see #unregisterVehiclePositionListener(VehiclePositionListener)
     * @see #unregisterRoadListener(RoadListener)
     * @see #unregisterAreaListener(AreaListener)
     * @see #unregisterRouteGuidanceListener(RouteGuidanceListener)
     * @see #unregisterRouteCalcListener(RouteCalcListener)
     * @see #unregisterApplicationStatusListener(ApplicationStatusListener)
     * @see #unregisterVehiclePositionExListener(VehiclePositionExListener)
     */
    public static ItsmoNaviDriveExternalApi getInstance() {
        return mInstance;
    }

    /**
     * インタフェース名称を取得します。
     */
    @Override
    protected String getInterfaceName() {
        return INaviService.class.getName();
    }

    /**
     * サービスバインドのためのアクション名を取得します。
     */
    @Override
    protected String getServiceAction() {
        return NaviIntent.ACTION_NAVI_SERVICE;
    }

    /**
     * {@link bindService()} する場合のフラグを返却します。<br />
     * <br />
     */
    @Override
    protected int getBindServiceFlags() {
        return Context.BIND_AUTO_CREATE;
    }

    /**
     * サービスへの接続時に呼ばれるコールバックメソッド。
     */
    @Override
    protected void onApiServiceConnected(ComponentName name, IBinder service) {
        Log.d(TAG, "onApiServiceConnected("+name.toShortString()+", "+service.toString()+")");
        mNaviService = INaviService.Stub.asInterface(service);
        try {
            if (mNaviService.isStarted()) {
                dequeueAllApis();
            } else {
                registerApplicationStatusListener(mMyApplicationStatusListener);
            }
        } catch (RemoteException e) {
            Log.e(TAG, "Error when checking application status.", e);
        }
    }

    /**
     * サービスからの切断時に呼ばれるコールバックメソッド。
     */
    @Override
    protected void onApiServiceDisconnected(ComponentName name) {
        Log.d(TAG, "onApiServiceDisconnected("+name.toString()+")");
        mNaviService = null;
    }

    /**
     * ナビサービスインタフェースを取得します。
     */
    private INaviService getNaviService() {
        if (mNaviService == null) {
            throw new IllegalStateException();
        }
        return mNaviService;
    }

    /**
     * 地図を開きます (自動起動対応)。<br />
     * <br />
     *
     * [アプリがすでに起動している場合の挙動]
     * <ul>
     * <li>処理完了後、メソッドが返ります。</li>
     * </ul>
     * <br />
     * [アプリ未起動時の挙動]
     * <ul>
     * <li>ナビの起動確認後、未起動ならナビを起動してから API を発行します。</li>
     * <li>このとき、ナビの起動チェック後にメソッドから返ります。ナビ起動、サービス接続、API 発行は非同期に行われます。</li>
     * <li>API の終了タイミングの検知、および例外のキャッチはできません。</li>
     * </ul>
     *
     * @param forced
     *            現在地表示フラグ（アプリが起動済みのときに有効。true：現在地を中心とした地図画面を表示する、false：状態を維持したままナビを前面に移動させる）
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     *
     */
    public void openMap(final boolean forced) throws NaviException {
        Log.d(TAG, "openMap()");
        if (isNaviAppReady()) {
            wakeupNaviApp();
            _openMap(forced);
        } else {
            runOnNaviApp(new Runnable() {
                @Override
                public void run() {
                    try {
                        _openMap(forced);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void _openMap(boolean forced) throws NaviException {
        Log.d(TAG, "_openMap()");
        NaviResult result = new NaviResult();

        try {
            getNaviService()._openMap(result, forced);
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "openMap() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "openMap() failed, service is dead?");
        }

        result.doException();
    }

    /**
     * 経緯度と縮尺を設定して地図を開きます (自動起動対応)。<br />
     * <br />
     *
     * [アプリがすでに起動している場合の挙動]
     * <ul>
     * <li>処理完了後、メソッドが返ります。</li>
     * </ul>
     * <br />
     * [アプリ未起動時の挙動]
     * <ul>
     * <li>ナビの起動確認後、未起動ならナビを起動してから API を発行します。</li>
     * <li>このとき、ナビの起動チェック後にメソッドから返ります。ナビ起動、サービス接続、API 発行は非同期に行われます。</li>
     * <li>API の終了タイミングの検知、および例外のキャッチはできません。</li>
     * </ul>
     *
     * @param point
     *            地図中心座標 (緯度・経度、世界測地系) を設定します。
     * @param zoomLevel
     *            縮尺を指定します。
     * @param forced
     *            地図画面表示フラグ（アプリが起動済みのときに有効。true：地図画面を表示する、false：地図画面を表示しない）
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     *
     * @see ZoomLevel#KM_100
     * @see ZoomLevel#KM_50
     * @see ZoomLevel#KM_20
     * @see ZoomLevel#KM_10
     * @see ZoomLevel#KM_5
     * @see ZoomLevel#KM_2
     * @see ZoomLevel#KM_1
     * @see ZoomLevel#M_500
     * @see ZoomLevel#M_200
     * @see ZoomLevel#M_100
     * @see ZoomLevel#M_50
     * @see ZoomLevel#M_40
     * @see ZoomLevel#M_20
     * @see ZoomLevel#M_10
     */
    public void openMap(final GeoPoint point, final int zoomLevel, final boolean forced) throws NaviException {
        Log.d(TAG, "openMap(" + (point != null ? "(" + point.getLatitude() + ", " + point.getLongitude() + ")" : "null") + "), " + zoomLevel+")");
        if (isNaviAppReady()) {
            wakeupNaviApp();
            _openMap(point, zoomLevel, forced);
        } else {
            runOnNaviApp(new Runnable() {
                @Override
                public void run() {
                    try {
                        _openMap(point, zoomLevel, forced);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void _openMap(final GeoPoint point, final int zoomLevel, final boolean forced) throws NaviException {
        Log.d(TAG, "_openMap(" + (point != null ? "(" + point.getLatitude() + ", " + point.getLongitude() + ")" : "null") + "), " + zoomLevel+", " + forced + ")");
        NaviResult result = new NaviResult();

        try {
//            wakeupNaviApp();
            getNaviService().openMap(result, point, zoomLevel, forced);
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "openMap() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "openMap() failed, service is dead?");
        }

        result.doException();
    }

    /**
     * 地図中心座標を設定します。
     *
     * @param point
     *            地図中心座標 (緯度・経度、世界測地系) を設定します。
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     */
    public void setCenter(final GeoPoint point) throws NaviException {
        NaviResult result = new NaviResult();

        try {
            getNaviService().setCenter(result, point);
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "setLocation() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "setLocation() failed, service is dead?");
        }

        result.doException();
    }

    /**
     * 地図縮尺を設定します。
     *
     * @param zoomLevel
     *            縮尺を指定します。 (指定可能なレベルは関連項目を参照)
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     *
     * @see ZoomLevel#KM_100
     * @see ZoomLevel#KM_50
     * @see ZoomLevel#KM_20
     * @see ZoomLevel#KM_10
     * @see ZoomLevel#KM_5
     * @see ZoomLevel#KM_2
     * @see ZoomLevel#KM_1
     * @see ZoomLevel#M_500
     * @see ZoomLevel#M_200
     * @see ZoomLevel#M_100
     * @see ZoomLevel#M_50
     * @see ZoomLevel#M_40
     * @see ZoomLevel#M_20
     * @see ZoomLevel#M_10
     */
    public void setZoom(int zoomLevel) throws NaviException {
        NaviResult result = new NaviResult();

        try {
            getNaviService().setZoom(result, zoomLevel);
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "setZoomLevel() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "setZoomLevel() failed, service is dead?");
        }

        result.doException();
    }

    /**
     * 地図中心座標を取得します。<br />
     * <br />
     *
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     * @return 地図中心座標 (緯度・経度、世界測地系)
     */
    public GeoPoint getCenter() throws NaviException {
        NaviResult result = new NaviResult();

        System.out.println("Into getCenter");

        GeoPoint point = null;
        try {
            point = getNaviService().getCenter(result);
            System.out.println("Into getCenter: getNaviService().getCenter(result)");
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "getLocation() raised RemoteException");
            System.out.println("Into getCenter: getLocation() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "getLocation() failed, service is dead?");
            System.out.println("Into getCenter: getLocation() failed, service is dead?");
        }

        result.doException();

        return point;
    }

    /**
     * 地図縮尺を取得します。<br />
     * <br />
     *
     * ・10m を除く任意スケールはそれを超えない縮尺レベルを返します。 (例: 350m の場合、{@link ZoomLevel#M_200})<br />
     *
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     * @return 縮尺 (関連項目を参照)
     *
     * @see ZoomLevel#KM_100
     * @see ZoomLevel#KM_50
     * @see ZoomLevel#KM_20
     * @see ZoomLevel#KM_10
     * @see ZoomLevel#KM_5
     * @see ZoomLevel#KM_2
     * @see ZoomLevel#KM_1
     * @see ZoomLevel#M_500
     * @see ZoomLevel#M_200
     * @see ZoomLevel#M_100
     * @see ZoomLevel#M_50
     * @see ZoomLevel#M_40
     * @see ZoomLevel#M_20
     * @see ZoomLevel#M_10
     */
    public int getZoom() throws NaviException {
        NaviResult result = new NaviResult();

        int scale = ZoomLevel.KM_100;
        try {
            scale = getNaviService().getZoom(result);
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "getZoomLevel() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "getZoomLevel() failed, service is dead?");
        }

        result.doException();

        return scale;
    }

    /**
     * 昼・夜モードを設定します。
     *
     * [アプリがすでに起動している場合の挙動]
     * <ul>
     * <li>処理完了後、メソッドが返ります。</li>
     * </ul>
     * <br />
     * [アプリ未起動時の挙動]
     * <ul>
     * <li>ナビの起動確認後、未起動ならナビを起動してから API を発行します。</li>
     * <li>このとき、ナビの起動チェック後にメソッドから返ります。ナビ起動、サービス接続、API 発行は非同期に行われます。</li>
     * <li>API の終了タイミングの検知、および例外のキャッチはできません。</li>
     * </ul>
     *
     * @param mode
     *            昼か夜を設定します。 (設定可能な値は関連項目を参照)
     * @throws IllegalArgumentException
     *             引数がエラーの場合にスローします。
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     *
     * @see DayNightMode#DAY
     * @see DayNightMode#NIGHT
     * @see DayNightMode#AUTO
     */
    public void setDayNightMode(int mode) throws NaviException {
        NaviResult result = new NaviResult();

        try {
            getNaviService().setDayNightMode(result, mode);
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "setDayNightMode() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "setDayNightMode() failed, service is dead?");
        }

        result.doException();

        return;
    }

    /**
     * 指定した地点にユーザーアイコンを描画します。 (外部起動対応)。<br />
     * <br />
     *
     * <ul>
     * <li>任意の地点に、指定した画像ファイルを描画します。</li>
     * <li>形状は指定したユーザレイヤに登録されます。</li>
     * <li>地図中心座標、縮尺の変更は行いません。必要に応じて以下の API を {@link #drawIcon(String, Icon)} の後にコールしてください。
     * <ul>
     * <li>縮尺の変更 : {@link #setZoom(int)}</li>
     * </ul>
     * </li>
     * <li>アイコン上に中心点を移動した場合アイコンに関連付けされた情報をポップアップ表示します。</li>
     * <li>コンテンツ (URL など）情報が存在する場合、ポップアップ情報をクリックすることで、コンテンツに応じた情報を表示します。</li>
     * <li>アイコン描画時のホットスポットはアイコン中心とします。</li>
     * <li>指定可能な画像ファイル形式は PNG (32bit, 24bit, 8bit, 4bit, 8bit グレイスケール), JPEG, GIF, BMP です。</li>
     * <li>アプリ終了時、ユーザー形状は削除されます。</li>
     * <li>アプリ起動中、画像ファイルを書き換えないようにしてください。</li>
     * </ul>
     *
     * [アプリがすでに起動している場合の挙動]
     * <ul>
     * <li>処理完了後、メソッドが返ります。</li>
     * </ul>
     * <br />
     * [アプリ未起動時の挙動]
     * <ul>
     * <li>ナビの起動確認後、未起動ならナビを起動してから API を発行します。</li>
     * <li>このとき、ナビの起動チェック後にメソッドから返ります。ナビ起動、サービス接続、API 発行は非同期に行われます。</li>
     * <li>API の終了タイミングの検知、および例外のキャッチはできません。</li>
     * </ul>
     *
     * @param layerId
     *            レイヤ識別子
     * @param icon
     *            アイコン情報 ({@link Icon})
     *
     * @throws IllegalArgumentException
     *             引数エラー時にスローします。
     * @throws NaviEngineException
     *             ナビエンジンエラー時にスローします。
     *             (画像ファイルが存在しない、フォーマット以外の画像ファイル指定した)
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     */
    public void drawIcon(final String layerId, final Icon icon) throws NaviException {
        if (isNaviAppReady()) {
            _drawIcon(layerId, icon);
        } else {
            runOnNaviApp(new Runnable() {
                @Override
                public void run() {
                    try {
                        _drawIcon(layerId, icon);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void _drawIcon(String layerId, Icon icon) throws NaviException {
        NaviResult result = new NaviResult();

        try {
            getNaviService().drawIcon(result, layerId, icon);
        } catch (RemoteException e) {
            result.setResultCode(NaviResult.RESULT_REMOTE_ERROR);
        }

        result.doException();

        return;
    }

    /**
     * 指定した座標点列でライン形状を描画します。 (外部起動対応)。<br />
     * <br />
     *
     * <ul>
     * <li>形状は指定したユーザレイヤに登録されます。</li>
     * <li>地図中心座標、縮尺の変更は行いません。必要に応じて以下の API を {@link #drawLine(String, Line)} の後にコールしてください。
     * <ul>
     * <li>縮尺の変更 : {@link #setZoom(int)}</li>
     * </ul>
     * </li>
     * <li>常時呼び出し可能です。</li>
     * <li>指定した座標点列に指定した描画パラメータでライン形状を描画します。 (形状点位置に円形状を描画します）
     * <li>
     * <li>ライン形状はポップアップ情報表示は対応しません。</li>
     * <li>アプリ終了時、ユーザー形状は削除されます。</li>
     * </ul>
     *
     * [アプリがすでに起動している場合の挙動]
     * <ul>
     * <li>処理完了後、メソッドが返ります。</li>
     * </ul>
     * <br />
     * [アプリ未起動時の挙動]
     * <ul>
     * <li>ナビの起動確認後、未起動ならナビを起動してから API を発行します。</li>
     * <li>このとき、ナビの起動チェック後にメソッドから返ります。ナビ起動、サービス接続、API 発行は非同期に行われます。</li>
     * <li>API の終了タイミングの検知、および例外のキャッチはできません。</li>
     * </ul>
     *
     * @param layerId
     *            レイヤ識別子
     * @param line
     *            ユーザライン情報 ({@link Line})
     *
     * @throws IllegalArgumentException
     *             引数エラー時にスローします。
     * @throws NaviEngineException
     *             ナビエンジンエラー時にスローします。
     *             (画像ファイルが存在しない、フォーマット以外の画像ファイル指定した)
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     */
    public void drawLine(final String layerId, final Line line) throws NaviException {
        if (isNaviAppReady()) {
            _drawLine(layerId, line);
        } else {
            runOnNaviApp(new Runnable() {
                @Override
                public void run() {
                    try {
                        _drawLine(layerId, line);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void _drawLine(String layerId, Line line) throws NaviException {
        NaviResult result = new NaviResult();

        try {
            getNaviService().drawLine(result, layerId, line);
        } catch (RemoteException e) {
            result.setResultCode(NaviResult.RESULT_REMOTE_ERROR);
        }

        result.doException();

        return;
    }

    /**
     * 指定したユーザレイヤを消去します。<br />
     * <br />
     *
     * 下記の関数で登録した形状を削除します :
     * <ul>
     * <li>{@link #drawIcon(String,Icon)}</li>
     * <li>{@link #drawLine(String,Line)}</li>
     * </ul>
     *
     * @throws NaviEngineException
     *             ナビエンジンエラー時にスローします。
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     */
    public void removeLayer(String layerId) throws NaviException {
        NaviResult result = new NaviResult();

        try {
            getNaviService().removeLayer(result, layerId);
        } catch (RemoteException e) {
            result.setResultCode(NaviResult.RESULT_REMOTE_ERROR);
        }

        result.doException();

        return;
    }

    /**
     * 指定したユーザレイヤを全て消去します。<br>
     * <br>
     *
     *
     * 指定したユーザレイヤを全て消去します。<br>
     * 下記の関数で登録した形状を削除します。
     * <ul>
     * <li>{@link #drawIcon(String,Icon)}</li>
     * <li>{@link #drawLine(String,Line)}</li>
     * </ul>
     *
     * @throws NaviEngineException
     *             ナビエンジンエラー時にスローします。
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     */
    public void removeAllLayers() throws NaviException {
        NaviResult result = new NaviResult();

        try {
            getNaviService().removeAllLayers(result);
        } catch (RemoteException e) {
            result.setResultCode(NaviResult.RESULT_REMOTE_ERROR);
        }

        result.doException();

        return;
    }

    /**
     * 目的地までの案内情報を取得します。<br />
     * <br />
     *
     * ・目的地までの残距離、残時間が含まれます。<br />
     * ・ルートが探索されていない状態では <code>IllegalStateException</code> を発行します。<br />
     *
     * @return 目的地までの案内情報 ({@link RemainingDistance})
     *
     * @throws IllegalStateException
     *             機能が提供できない状態の場合にスローします。
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     */
    public RemainingDistance getRemainingDistance() throws NaviException {
        NaviResult result = new NaviResult();
        RemainingDistance remainingDistance = null;

        try {
            remainingDistance = getNaviService().getRemainingDistance(result);
        } catch (RemoteException e) {
            result.setResultCode(NaviResult.RESULT_REMOTE_ERROR);
        }

        result.doException();

        return remainingDistance;

    }

    /**
     * ルート探索条件を指定して案内を開始します (外部起動対応)。<br />
     * <br />
     * ルート案内開始フラグ がfalseのとき、ルート探索を行います。<br />
     * ルート案内開始フラグ がtrueのとき、ルート探索を行い案内を開始します。<br />
     *
     * ・ルート案内可能な状態は以下の通り :<br />
     * <br />
     * <ol>
     * <li>[ナビアプリ未起動時]<br />
     * ナビアプリを起動し、ルート案内を開始します。
     * <li>[地図表示時]<br />
     * ルート案内を開始します。
     * <li>[ルート表示・案内・デモ中]<br />
     * ルート取り消し・案内・デモ終了し、ルート案内を開始します。
     * </ol>
     * <br />
     * ・上記以外の画面では、例外 <code>IllegalStateException</code> を返します。<br />
     * ・[制限事項] PND 設定画面の規制条件は、ルート探索条件で置き換えられます。<br />
     * ・[制限事項] オープンマップ状態 (目的地検索後、地点表示のバルーンが出ている状態) は、
     * 例外 IllegalStateException となる。<br />
     * ・[制限事項] 2 画面状態 (交差点拡大図、IC、分岐、方面看板表示) のとき、
     * API を発行すると PND が動作不安定になることがあるので、なるべく呼ばないこと。<br />
     * <br />
     * [アプリがすでに起動している場合の挙動]
     * <ul>
     * <li>処理完了後、メソッドが返ります。</li>
     * </ul>
     * <br />
     * [アプリ未起動時の挙動]
     * <ul>
     * <li>ナビの起動確認後、未起動ならナビを起動してから API を発行します。</li>
     * <li>このとき、ナビの起動チェック後にメソッドから返ります。ナビ起動、サービス接続、API 発行は非同期に行われます。</li>
     * <li>API の終了タイミングの検知、および例外のキャッチはできません。</li>
     * </ul>
     *
     * @param routeRequest
     *            ルート探索条件
     * @param routeGuidanceStarting
     *            ルート案内開始フラグ (false:ルート探索のみ、true:ルート探索および案内開始)
     *
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             無操作地図及び案内中でない場合にスローします。
     * @throws NaviNotFoundException
     *             ルート ID が見つからない場合にスローします。
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     */
    public void calculateRoute(final RouteRequest routeRequest, final boolean routeGuidanceStarting) throws NaviException {
        if (isNaviAppReady()) {
            _calculateRoute(routeRequest, routeGuidanceStarting);
        } else {
            runOnNaviApp(new Runnable() {
                @Override
                public void run() {
                    try {
                        _calculateRoute(routeRequest, routeGuidanceStarting);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void _calculateRoute(final RouteRequest routeRequest, final boolean routeGuidanceStarting) throws NaviException {
        NaviResult result = new NaviResult();

        try {
            wakeupNaviApp();
            getNaviService().calculateRoute(result, routeRequest, routeGuidanceStarting);
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "startGuide() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "startGuide() failed, service is dead?");
        }

        result.doException();
    }

    /**
     * 案内を開始します。<br />
     * <br />
     *
     * ・案内開始可能な状態は以下の通り :<br />
     * <br />
     * <ol>
     * <li>[ルート表示中]<br />
     * 案内を開始します。
     * </ol>
     * <br />
     * ・上記以外の画面では、例外 <code>IllegalStateException</code> を返します。<br />
     *
     * @throws IllegalStateException
     *             ルート表示または案内中でない場合にスローします。
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     */
    public void startRouteGuidance() throws NaviException {
        if (isNaviAppReady()) {
            _startRouteGuidance();
        }
    }
    private void _startRouteGuidance() throws NaviException {
        NaviResult result = new NaviResult();

        try {
            getNaviService().startRouteGuidance(result);
            wakeupNaviApp();
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "startGuide() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "startGuide() failed, service is dead?");
        }

        result.doException();
    }

    /**
     * 案内中の案内を終了します。<br />
     * <br />
     *
     * ・案内終了可能な状態は以下の通り :<br />
     * <br />
     * <ol>
     * <li>[ルート表示・案内・デモ中]<br />
     * ルート取り消し・案内・デモ終了し、無操作地図画面に遷移します。
     * </ol>
     * <br />
     * ・上記以外の画面では、例外 <code>IllegalStateException</code> を返します。
     * ・[制限事項] オープンマップ状態 (目的地検索後、地点表示のバルーンが出ている状態) は、
     * 例外 <code>IllegalStateException</code> となる。<br />
     * ・[制限事項] 2 画面状態 (交差点拡大図、IC、分岐、方面看板表示) のとき、
     * API を発行すると PND が動作不安定になることがあるので、なるべく呼ばないこと。<br />
     *
     * @throws IllegalStateException
     *             ルート表示または案内中でない場合にスローします。
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     */
    public void stopRouteGuidance() throws NaviException {
        if (isNaviAppReady()) {
            _stopRouteGuidance();
        }
    }
    private void _stopRouteGuidance() throws NaviException {
        NaviResult result = new NaviResult();

        try {
            getNaviService().stopRouteGuidance(result);
            wakeupNaviApp();
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "endGuide() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "endGuide() failed, service is dead?");
        }

        result.doException();
    }

    /**
     * お気に入りを登録します。<br />
     * <br />
     *
     * ・<code>NaviPoint</code> の地点名称は、SJIS で最大 80 バイトとします。<br />
     * ・<code>NaviPoint</code> の経緯度範囲は、経度 [-180, 180] 、緯度 [-90, 90] とします。<br />
     * ・カテゴリ識別 ID (<code>categoryId</code>) は以下の値を指定します。 :<br />
     * <br />
     * <ul>
     * <li>{@link FavoritesCategory#FRIEND} (友人・親戚)</li>
     * <li>{@link FavoritesCategory#EATING} (食事)</li>
     * <li>{@link FavoritesCategory#SHOPPING} (買い物)</li>
     * <li>{@link FavoritesCategory#HOBBY} (趣味)</li>
     * <li>{@link FavoritesCategory#SIGHTSEEING} (観光)</li>
     * <li>{@link FavoritesCategory#WORK} (仕事)</li>
     * <li>{@link FavoritesCategory#OTHERS} (その他)</li>
     * <li>{@link FavoritesCategory#HOME_1} (自宅 1)</li>
     * <li>{@link FavoritesCategory#HOME_2} (自宅 2)</li>
     * </ul>
     * <br />
     * ・登録件数最大数は以下の通り :<br />
     * <ul>
     * <li>[友人・親戚～その他] は合計で 500 件まで (ちょうど 500 件を含む)。</li>
     * <li>[自宅 1] [自宅 2] は、上記 500 件によらず 1 件ずつ(上書き登録しません)。</li>
     * </ul>
     * ・最大件数超過時、またはすでに登録済み (自宅) の場合は、例外 <code>NaviOutOfRangeException</code> を返します。<br />
     * ・[制限事項] PND のお気に入り機能と同時に登録を行った場合の動作は保証しません。
     * (見た目上、登録に成功しているが、データは追加されていない状態となる)<br />
     *
     * @param point
     *            お気に入り地点情報 (世界測地系度単位と名称)
     * @param categoryId
     *            お気に入り登録先カテゴリ
     *
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     */
    public void addFavorites(RoutePoint point, int categoryId) throws NaviException {
        NaviResult result = new NaviResult();

        try {
            getNaviService().addFavorites(result, point, categoryId);
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "addFavoritePoint() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "addFavoritePoint() failed, service is dead?");
        }

        result.doException();
    }

// Add 2011/11/02 sawada Start -->
    private IVehiclePositionListener mVehiclePositionListener = new IVehiclePositionListener.Stub() {
        @Override
        public void onChanged(final Location location) throws RemoteException {
            for (final VehiclePositionListener listener : mVehiclePositionListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onChanged(location);
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onChanged(location);
                    }
                });
            }
        }
    };

// Add 2011/11/02 sawada End <--

    /**
     * 自車位置更新イベントリスナを登録します。<br />
     * <br />
     *
     * 情報を取得する場合は <code>VehiclePositionListener</code> を implements し、 {@link VehiclePositionListener#onChanged(Location)} で受け取ります。<br />
     *
     * @see #unregisterVehiclePositionListener(VehiclePositionListener)
     *
     * @param listener
     *            自車位置更新イベントイベントリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void registerVehiclePositionListener(VehiclePositionListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (!mVehiclePositionListeners.contains(listener)) {
            if (mVehiclePositionListeners.size() == 0) {
// Chg 2011/11/02 sawada Start -->
//              IntentFilter filter = new IntentFilter(NaviIntent.ACTION_VEHICLE_POSITION);
//              mContext.registerReceiver(mVehiclePositionBroadcastReceiver, filter);
                try {
                    getNaviService().registerVehiclePositionListener(mVehiclePositionListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
// Chg 2011/11/02 sawada End <--
            }
            mVehiclePositionListeners.add(listener);
        }
    }

    /**
     * 自車位置更新イベントリスナを解除します。<br />
     * <br />
     *
     * @see #registerVehiclePositionListener(VehiclePositionListener)
     *
     * @param listener
     *            自車位置更新イベントイベントリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void unregisterVehiclePositionListener(VehiclePositionListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (mVehiclePositionListeners.contains(listener)) {
            mVehiclePositionListeners.remove(listener);
            if (mVehiclePositionListeners.size() == 0) {
// Chg 2011/11/02 sawada Start -->
//              mContext.unregisterReceiver(mVehiclePositionBroadcastReceiver);
                try {
                    getNaviService().unregisterVehiclePositionListener(mVehiclePositionListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
// Chg 2011/11/02 sawada End <--
            }
        }
    }

// Del 2011/11/02 sawada Start -->
//  /**
//   * 自車位置更新イベントブロードキャストレシーバ
//   */
//  protected class VehiclePositionBroadcastReceiver extends BroadcastReceiver {
//      @Override
//      public void onReceive(Context context, Intent intent) {
//          String action = intent.getAction();
//          if (action != null) {
//              if (action.equals(NaviIntent.ACTION_VEHICLE_POSITION)) {
//                  Location location = (Location)intent.getParcelableExtra(NaviIntent.EXTRA_LOCATION);
//                  for (VehiclePositionListener listener : mVehiclePositionListeners) {
//                      listener.onVehiclePositionChanged(location);
//                  }
//              }
//          }
//      }
//  }
// Del 2011/11/02 sawada End <--

// Add 2011/11/02 sawada Start -->
    private IRoadListener mRoadListener = new IRoadListener.Stub() {
        @Override
        public void onChanged(final Location location, final Road from, final Road to) throws RemoteException {
            for (final RoadListener listener : mRoadListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onChanged(location, from, to);
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onChanged(location, from, to);
                    }
                });
            }
        }
    };
// Add 2011/11/02 sawada End <--

    /**
     * 道路種別変更イベントリスナを登録します。<br />
     * <br />
     *
     * 情報を取得する場合は <code>RoadListener</code> を implements し、 {@link RoadListener#onChanged(Location, Road, Road)} で受け取ります。<br />
     *
     * @see #unregisterRoadListener(RoadListener)
     *
     * @param listener
     *            道路種別変更イベントイベントリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void registerRoadListener(RoadListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (!mRoadListeners.contains(listener)) {
            if (mRoadListeners.size() == 0) {
// Chg 2011/11/07 sawada Start -->
//              IntentFilter filter = new IntentFilter(NaviIntent.ACTION_ROAD_MOVED);
//              mContext.registerReceiver(mRoadMovedBroadcastReceiver, filter);
                try {
                    getNaviService().registerRoadListener(mRoadListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
// Chg 2011/11/07 sawada End <--
            }
            mRoadListeners.add(listener);
        }
    }

    /**
     * 道路種別変更イベントリスナを解除します。<br />
     * <br />
     *
     * @see #registerRoadListener(RoadListener)
     *
     * @param listener
     *            道路種別変更イベントイベントリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void unregisterRoadListener(RoadListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (mRoadListeners.contains(listener)) {
            mRoadListeners.remove(listener);
            if (mRoadListeners.size() == 0) {
// Chg 2011/11/07 sawada Start -->
//              mContext.unregisterReceiver(mRoadMovedBroadcastReceiver);
                try {
                    getNaviService().unregisterRoadListener(mRoadListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
// Chg 2011/11/07 sawada End <--
            }
        }
    }

// Del 2011/11/07 sawada Start -->
//    /**
//     * 道路種別変更イベントブロードキャストレシーバ
//     */
//    protected class RoadMovedBroadcastReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            if (action != null) {
//                if (action.equals(NaviIntent.ACTION_ROAD_MOVED)) {
//                    Location location = (Location)intent.getParcelableExtra(NaviIntent.EXTRA_LOCATION);
//                    int roadFrom = intent.getIntExtra(NaviIntent.EXTRA_ROAD_FROM, -1);
//                    int roadTo = intent.getIntExtra(NaviIntent.EXTRA_ROAD_TO, -1);
//                    RoadMoved moved = new RoadMoved(location, roadFrom, roadTo);
//                    for (RoadListener listener : mRoadListeners) {
//                        listener.onRoadMoved(moved);
//                    }
//                }
//            }
//        }
//    }
// Del 2011/11/07 sawada End <--

// Add 2011/11/02 sawada Start -->
    private IAreaListener mAreaListener = new IAreaListener.Stub() {
        @Override
        public void onChanged(final Location location, final Area from, final Area to) throws RemoteException {
            for (final AreaListener listener : mAreaListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onChanged(location, from, to);
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onChanged(location, from, to);
                    }
                });
            }
        }
    };
// Add 2011/11/02 sawada End <--

    /**
     * 行政界移動イベントリスナを登録します。
     *
     * 情報を取得する場合は <code>AreaListener</code> を implements し、 {@link AreaListener#onChanged(Location, Area, Area)} で受け取ります。<br />
     *
     * @see #unregisterAreaListener(AreaListener)
     *
     * @param listener
     *            行政界移動イベントイベントリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void registerAreaListener(AreaListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (!mAreaListeners.contains(listener)) {
            if (mAreaListeners.size() == 0) {
// Chg 2011/11/07 sawada Start -->
//              IntentFilter filter = new IntentFilter(NaviIntent.ACTION_AREA_MOVED);
//              mContext.registerReceiver(mAreaBroadcastReceiver, filter);
                try {
                    getNaviService().registerAreaListener(mAreaListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
// Chg 2011/11/07 sawada End <--
            }
            mAreaListeners.add(listener);
        }
    }

    /**
     * 行政界移動イベントリスナを解除します。<br />
     * <br />
     *
     * @see #registerAreaListener(AreaListener)
     *
     * @param listener
     *            行政界移動イベントイベントリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void unregisterAreaListener(AreaListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (mAreaListeners.contains(listener)) {
            mAreaListeners.remove(listener);
            if (mAreaListeners.size() == 0) {
// Del 2011/11/07 sawada Start -->
//              mContext.unregisterReceiver(mAreaBroadcastReceiver);
                try {
                    getNaviService().unregisterAreaListener(mAreaListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
// Del 2011/11/07 sawada End <--
            }
        }
    }

// Del 2011/11/07 sawada Start -->
//    /**
//     * 行政界移動イベントブロードキャストレシーバ
//     */
//    protected class AreaBroadcastReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            if (action != null) {
//                if (action.equals(NaviIntent.ACTION_AREA_MOVED)) {
//                    Location location = (Location)intent.getParcelableExtra(NaviIntent.EXTRA_LOCATION);
//                    List<String> adminFrom = intent.getStringArrayListExtra(NaviIntent.EXTRA_ADMIN_FROM);
//                    List<String> adminTo = intent.getStringArrayListExtra(NaviIntent.EXTRA_ADMIN_TO);
//                    Area moved = new Area(location, adminFrom, adminTo);
//                    for (AreaListener listener : mAreaListeners) {
//                        listener.onChanged(moved);
//                    }
//                }
//            }
//        }
//    }
// Del 2011/11/07 sawada End <--

//// Add 2011/11/02 sawada Start -->
//    private IMapControlListener mMapControlListener = new IMapControlListener.Stub() {
//        @Override
//        public void onMoved(final GeoPoint point) throws RemoteException {
//            for (final MapControlListener listener : mMapControlListeners) {
////                ((Activity)mContext).runOnUiThread(new Runnable() {
////                    @Override
////                    public void run() {
////                        listener.onMoved(point);
////                    }
////                });
//                mHandler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onMoved(point);
//                    }
//                });
//            }
//        }
//    };
//// Add 2011/11/02 sawada End <--
//
//    /**
//     * 表示地図操作イベントリスナを登録します。<br />
//     * <br />
//     *
//     * 情報を取得する場合は <code>MapControlListener</code> を implements し、 {@link MapControlListener#onMoved(GeoPoint)} で受け取ります。<br />
//     *
//     * @see #unregisterMapControlListener(MapControlListener)
//     *
//     * @param listener
//     *            表示地図操作イベントリスナ
//     * @throws IllegalArgumentException
//     *             引数が不正な場合にスローします。
//     * @throws IllegalStateException
//     *             コンテキストが無効な場合にスローします。
//     */
//    public void registerMapControlListener(MapControlListener listener) {
//        if (listener == null) {
//            throw new IllegalArgumentException();
//        }
//        if (mContext == null) {
//            throw new IllegalStateException();
//        }
//        if (!mMapControlListeners.contains(listener)) {
//            if (mMapControlListeners.size() == 0) {
//// Chg 2011/11/07 sawada Start -->
////              IntentFilter filter = new IntentFilter(NaviIntent.ACTION_MAP_CONTROL);
////              mContext.registerReceiver(mMapControlBroadcastReceiver, filter);
//                try {
//                    getNaviService().registerMapControlListener(mMapControlListener);
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }
//// Chg 2011/11/07 sawada End <--
//            }
//            mMapControlListeners.add(listener);
//        }
//    }
//
//    /**
//     * 表示地図操作イベントリスナを解除します。<br />
//     * <br />
//     *
//     * @see #registerMapControlListener(MapControlListener)
//     *
//     * @param listener
//     *            表示地図操作イベントリスナ
//     * @throws IllegalArgumentException
//     *             引数が不正な場合にスローします。
//     * @throws IllegalStateException
//     *             コンテキストが無効な場合にスローします。
//     */
//    public void unregisterMapControlListener(MapControlListener listener) {
//        if (listener == null) {
//            throw new IllegalArgumentException();
//        }
//        if (mContext == null) {
//            throw new IllegalStateException();
//        }
//        if (mMapControlListeners.contains(listener)) {
//            mMapControlListeners.remove(listener);
//            if (mMapControlListeners.size() == 0) {
//// Chg 2011/11/07 sawada Start -->
////              mContext.unregisterReceiver(mMapControlBroadcastReceiver);
//                try {
//                    getNaviService().unregisterMapControlListener(mMapControlListener);
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }
//// Chg 2011/11/07 sawada End <--
//            }
//        }
//    }

// Del 2011/11/07 sawada Start -->
//    /**
//     * 表示地図操作イベントブロードキャストレシーバ
//     */
//    class MapControlBroadcastReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            if (action != null) {
//                if (action.equals(NaviIntent.ACTION_MAP_CONTROL)) {
//                    Location location = (Location)intent.getParcelableExtra(NaviIntent.EXTRA_LOCATION);
//                    for (MapControlListener listener : mMapControlListeners) {
//                        listener.onMapControlled(location);
//                    }
//                }
//            }
//        }
//    }
// Del 2011/11/07 sawada End <--

// Add 2011/11/02 sawada Start -->
    private IRouteGuidanceListener mRouteGuidanceListener = new IRouteGuidanceListener.Stub() {
        @Override
        public void onStarted(final RouteGuidance routeGuidance) throws RemoteException {
            for (final RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onStarted(routeGuidance);
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onStarted(routeGuidance);
                    }
                });
            }
        }

        @Override
        public void onStopped(final RouteGuidance routeGuidance) throws RemoteException {
            for (final RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onStopped(routeGuidance);
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onStopped(routeGuidance);
                    }
                });
            }
        }

        @Override
        public void onPaused(final RouteGuidance routeGuidance) throws RemoteException {
            for (final RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onPaused(routeGuidance);
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onPaused(routeGuidance);
                    }
                });
            }
        }

        @Override
        public void onResumed(final RouteGuidance routeGuidance) throws RemoteException {
            for (final RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onResumed(routeGuidance);
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onResumed(routeGuidance);
                    }
                });
            }
        }

        @Override
        public void onNearToCrossroad(final RouteGuidance routeGuidance) throws RemoteException {
            for (final RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onNearToCrossroad(routeGuidance);
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onNearToCrossroad(routeGuidance);
                    }
                });
            }
        }

        @Override
        public void onNearToWaypoint(final RouteGuidance routeGuidance) throws RemoteException {
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
            DebugLogOutput.put("RakuRaku-Log: onNearToWaypoint Start");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
            for (final RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onNearToWaypoint(routeGuidance);
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onNearToWaypoint(routeGuidance);
                    }
                });
            }
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
            DebugLogOutput.put("RakuRaku-Log: onNearToWaypoint End");
//Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
        }

        @Override
        public void onNearToDestination(final RouteGuidance routeGuidance) throws RemoteException {
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
            DebugLogOutput.put("RakuRaku-Log: onNearToDestination Start");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
            for (final RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onNearToDestination(routeGuidance);
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onNearToDestination(routeGuidance);
                    }
                });
            }
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 start
            DebugLogOutput.put("RakuRaku-Log: onNearToDestination End");
// Add 2015-05-20 CPJsunagawa SDカードにログ出力 end
        }

        @Override
        public void onRerouted(final RouteGuidance routeGuidance) throws RemoteException {
            for (final RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onRerouted(routeGuidance);
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onRerouted(routeGuidance);
                    }
                });
            }
        }
    };
// Add 2011/11/02 sawada End <--

    /**
     * ルート案内イベントリスナを登録します。<br />
     *
     * 情報を取得する場合は <code>RouteGuidanceListener</code> を implements し、 {@link RouteGuidanceListener#onStarted(RouteGuidance routeGuidance)}、{@link RouteGuidanceListener#onStopped(RouteGuidance routeGuidance)}、{@link RouteGuidanceListener#onPaused(RouteGuidance routeGuidance)}、{@link RouteGuidanceListener#onResumed(RouteGuidance routeGuidance)}、{@link RouteGuidanceListener#onNearToCrossroad(RouteGuidance routeGuidance)}、{@link RouteGuidanceListener#onNearToWaypoint(RouteGuidance routeGuidance)}、{@link RouteGuidanceListener#onNearToDestination(RouteGuidance routeGuidance)}、{@link RouteGuidanceListener#onRerouted(RouteGuidance routeGuidance)} で受け取ります。<br />
     *
     * @see #unregisterRouteGuidanceListener(RouteGuidanceListener)
     *
     * @param listener
     *            ルート案内イベントリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void registerRouteGuidanceListener(RouteGuidanceListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (!mRouteGuidanceListeners.contains(listener)) {
            if (mRouteGuidanceListeners.size() == 0) {
// Chg 2011/11/02 sawada Start -->
//              IntentFilter filter = new IntentFilter(NaviIntent.ACTION_GUIDE);
//              mContext.registerReceiver(mRouteGuidanceBroadcastReceiver, filter);
                try {
                    getNaviService().registerRouteGuidanceListener(mRouteGuidanceListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
// Chg 2011/11/02 sawada End <--
            }
            mRouteGuidanceListeners.add(listener);
        }
    }

    /**
     * ルート案内イベントリスナを解除します。<br />
     * <br />
     *
     * @see #registerRouteGuidanceListener(RouteGuidanceListener)
     *
     * @param listener
     *            ルート案内イベントリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void unregisterRouteGuidanceListener(RouteGuidanceListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (mRouteGuidanceListeners.contains(listener)) {
            mRouteGuidanceListeners.remove(listener);
            if (mRouteGuidanceListeners.size() == 0) {
// Chg 2011/11/02 sawada Start -->
//              mContext.unregisterReceiver(mRouteGuidanceBroadcastReceiver);
                try {
                    getNaviService().unregisterRouteGuidanceListener(mRouteGuidanceListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
// Chg 2011/11/02 sawada End <--
            }
        }
    }

// Del 2011/11/02 sawada Start -->
//    /**
//     * ルート案内イベントブロードキャストレシーバ
//     */
//    class RouteGuidanceBroadcastReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            if (action != null) {
//                if (action.equals(NaviIntent.ACTION_GUIDE)) {
//                    int eventType = intent.getIntExtra(NaviIntent.EXTRA_GUIDE_TYPE, RouteGuidance.TYPE_EVENT_NONE);
//                    Location location = (Location)intent.getParcelableExtra(NaviIntent.EXTRA_LOCATION);
//                    int targetType = intent.getIntExtra(NaviIntent.EXTRA_TARGET_TYPE, RouteGuidance.TYPE_TARGET_NONE);
//                    String targetName = intent.getStringExtra(NaviIntent.EXTRA_TARGET_NAME);
//                    Location targetLocation = (Location)intent.getParcelableExtra(NaviIntent.EXTRA_TARGET_LOCATION);
//                    float targetDistance = intent.getFloatExtra(NaviIntent.EXTRA_TARGET_DISTANCE, 0.0F);
//                    int targetDirection = intent.getIntExtra(NaviIntent.EXTRA_TARGET_DIRECTION, RouteGuidance.TYPE_DIRECTION_NONE);
//                    RouteGuidance routeGuidance = new RouteGuidance(eventType, location, targetType, targetName, targetLocation, targetDistance, targetDirection);
//
//                    switch (eventType) {
//                    case RouteGuidance.TYPE_EVENT_STARTGUIDE:  // 案内開始
//                        for (RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                            listener.onStartGuide(routeGuidance);
//                        }
//                        break;
//                    case RouteGuidance.TYPE_EVENT_ENDGUIDE:    // 案内終了
//                        for (RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                            listener.onEndGuide(routeGuidance);
//                        }
//                        break;
//                    case RouteGuidance.TYPE_EVENT_PAUSEGUIDE:  // 案内停止
//                        for (RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                            listener.onPauseGuide(routeGuidance);
//                        }
//                        break;
//                    case RouteGuidance.TYPE_EVENT_RESUMEGUIDE: // 案内再開
//                        for (RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                            listener.onResumeGuide(routeGuidance);
//                        }
//                        break;
//                    case RouteGuidance.TYPE_EVENT_CROSSGUIDE:  // 目標案内
//                        for (RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                            listener.onCrossGuide(routeGuidance);
//                        }
//                        break;
//                    case RouteGuidance.TYPE_EVENT_VIAGUIDE:    // 経由地案内
//                        for (RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                            listener.onViaGuide(routeGuidance);
//                        }
//                        break;
//                    case RouteGuidance.TYPE_EVENT_GOALGUIDE:   // 目的地案内
//                        for (RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                            listener.onGoalGuide(routeGuidance);
//                        }
//                        break;
//                    case RouteGuidance.TYPE_EVENT_REROUTE:     // リルート
//                        for (RouteGuidanceListener listener : mRouteGuidanceListeners) {
//                            listener.onReRoute(routeGuidance);
//                        }
//                        break;
//                    default:
//                        break;
//                    }
//                }
//            }
//        }
//    }
// Del 2011/11/02 sawada End <--

// Add 2011/11/02 sawada Start -->
    private IRouteCalcListener mRouteCalcListener = new IRouteCalcListener.Stub() {
        @Override
        public void onRouteCalculated(final RouteCalculated routeCalculated) throws RemoteException {
            for (final RouteCalcListener listener : mRouteCalcListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onRouteCalculated(routeCalculated);
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onRouteCalculated(routeCalculated);
                    }
                });
            }
        }
    };
// Add 2011/11/02 sawada End <--

    /**
     * ルート探索イベントリスナを登録します。<br />
     * <br />
     *
     * 情報を取得する場合は <code>RouteCalcListener</code> を implements し、 {@link RouteCalcListener#onRouteCalculated(RouteCalculated)} で受け取ります。<br />
     *
     * @see #unregisterRouteCalcListener(RouteCalcListener)
     *
     * @param listener
     *            ルート探索イベントリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void registerRouteCalcListener(RouteCalcListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (!mRouteCalcListeners.contains(listener)) {
            if (mRouteCalcListeners.size() == 0) {
// Chg 2011/11/07 sawada Start -->
//              IntentFilter filter = new IntentFilter(NaviIntent.ACTION_ROUTE_CALCULATED);
//              mContext.registerReceiver(mRouteCalcBroadcastReceiver, filter);
                try {
                    getNaviService().registerRouteCalcListener(mRouteCalcListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
// Chg 2011/11/07 sawada End <--
            }
            mRouteCalcListeners.add(listener);
        }
    }

    /**
     * ルート探索イベントリスナを解除します。<br />
     * <br />
     *
     * @see #registerRouteCalcListener(RouteCalcListener)
     *
     * @param listener
     *            ルート探索イベントイベントリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void unregisterRouteCalcListener(RouteCalcListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (mRouteCalcListeners.contains(listener)) {
            mRouteCalcListeners.remove(listener);
            if (mRouteCalcListeners.size() == 0) {
// Chg 2011/11/07 sawada Start -->
//              mContext.unregisterReceiver(mRouteCalcBroadcastReceiver);
                try {
                    getNaviService().unregisterRouteCalcListener(mRouteCalcListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
// Chg 2011/11/07 sawada End <--
            }
        }
    }

// Del 2011/11/07 sawada Start -->
//    /**
//     * ルート探索イベントブロードキャストレシーバ
//     */
//    class RouteCalcBroadcastReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            if (action != null) {
//                if (action.equals(NaviIntent.ACTION_ROUTE_CALCULATED)) {
//                    RouteCalculated routeCalculated = (RouteCalculated)intent.getParcelableExtra(NaviIntent.EXTRA_ROUTE_CALCULATED);
//                    for (RouteCalcListener listener : mRouteCalcListeners) {
//                        listener.onRouteCalculated(routeCalculated);
//                    }
//                }
//            }
//        }
//    }
// Del 2011/11/07 sawada End <--

    private IApplicationStatusListener mApplicationStatusListener = new IApplicationStatusListener.Stub() {
        @Override
        public void onStarted() throws RemoteException {
            for (final ApplicationStatusListener listener : mApplicationStatusListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onStarted();
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onStarted();
                    }
                });
            }
        }
        @Override
        public void onStopped() throws RemoteException {
            for (final ApplicationStatusListener listener : mApplicationStatusListeners) {
//                ((Activity)mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        listener.onStopped();
//                    }
//                });
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onStopped();
                    }
                });
            }
        }
    };

    /**
     * アプリケーションステータスリスナを登録します。<br />
     *
     * @see #unregisterApplicationStatusListener(ApplicationStatusListener)
     *
     * @param listener
     *            アプリケーションステータスリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void registerApplicationStatusListener(ApplicationStatusListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (!mApplicationStatusListeners.contains(listener)) {
            if (mApplicationStatusListeners.size() == 0) {
                try {
                    getNaviService().registerApplicationStatusListener(mApplicationStatusListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            mApplicationStatusListeners.add(listener);
        }
    }

    /**
     * アプリケーションステータスリスナを解除します。<br />
     * <br />
     *
     * @see #registerApplicationStatusListener(ApplicationStatusListener)
     *
     * @param listener
     *            アプリケーションステータスリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void unregisterApplicationStatusListener(ApplicationStatusListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (mApplicationStatusListeners.contains(listener)) {
            mApplicationStatusListeners.remove(listener);
            if (mApplicationStatusListeners.size() == 0) {
                try {
                    getNaviService().unregisterApplicationStatusListener(mApplicationStatusListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応 START
    private IVehiclePositionExListener mVehiclePositionExListener = new IVehiclePositionExListener.Stub() {
        @Override
        public void onChanged(final VehiclePositionEx vehiclePositionEx) throws RemoteException {
            for (final VehiclePositionExListener listener : mVehiclePositionExListeners) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onChanged(vehiclePositionEx);
                    }
                });
            }
        }
    };

    /**
     * 自車位置更新イベントリスナ(拡張版)を登録します。<br />
     * <br />
     *
     * 情報を取得する場合は <code>VehiclePositionExListener</code> を implements し、 {@link VehiclePositionExListener#onChanged(VehiclePositionEx)} で受け取ります。<br />
     *
     * @see #unregisterVehiclePositionExListener(VehiclePositionExListener)
     *vehiclePositionExvehiclePositionExvehiclePositionExvehiclePositionExvehiclePositionExvehiclePositionExvehiclePositionExvehiclePositionExvehiclePositionExvehiclePositionExvehiclePositionEx","mLocation = new Location(LocationManager.GPS_PROVIDER);
        mLocation.setLatitude(0.0);
        mLocation.setLongitude(0.0);mLocation = new Location(LocationManager.GPS_PROVIDER);
        mLocation.setLatitude(0.0);
        mLocation.setLongitude(0.0); 自車位置更新イベントイベントリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void registerVehiclePositionExListener(VehiclePositionExListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (!mVehiclePositionExListeners.contains(listener)) {
            if (mVehiclePositionExListeners.size() == 0) {
                try {
					getNaviService().registerVehiclePositionExListener(mVehiclePositionExListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            mVehiclePositionExListeners.add(listener);
        }
    }

    /**
     	* 自車位置更新イベントリスナ(拡張版)を解除します。<br />
     * <br />
     *
     * @see #registerVehiclePositionExListener(VehiclePositionExListener)
     *
     * @param listener
     *            自車位置更新イベントイベントリスナ
     * @throws IllegalArgumentException
     *             引数が不正な場合にスローします。
     * @throws IllegalStateException
     *             コンテキストが無効な場合にスローします。
     */
    public void unregisterVehiclePositionExListener(VehiclePositionExListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
        if (mContext == null) {
            throw new IllegalStateException();
        }
        if (mVehiclePositionExListeners.contains(listener)) {
            mVehiclePositionExListeners.remove(listener);
            if (mVehiclePositionExListeners.size() == 0) {
                try {
                    getNaviService().unregisterVehiclePositionExListener(mVehiclePositionExListener);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }
// ADD.2013.05.15 N.Sasao 自車情報取得拡張対応  END
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
    /**
     * 走行規制機能の制御許可を申請します。<br />
     * <br />
     *
     * ・許可申請したプロセスIDに対して走行規制機能の制御許可を申請します。<br />
     *   別プロセスにより走行規制機能を使用中は制御許可をしません。<br />
     *
     * @see #requestDrivingControlLock( int nKey )
     *
     * @param nKey
     *            アプリの識別キー
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     * @return 申請許可(true：許可, false：拒否)
     *
    */
    public boolean requestDrivingControlLock( int nKey ) throws NaviException {
        NaviResult result = new NaviResult();

        boolean bIsLock = false;
        try {
            bIsLock = getNaviService().requestDrivingControlLock( nKey );
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "requestDrivingControlLock() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "requestDrivingControlLock() failed, service is dead?");
        }

        result.doException();

        return bIsLock;
    }
    /**
     * 走行規制機能の制御権を解放します。<br />
     * <br />
     *
     * ・制御中のプロセスIDに対して、制御権を解放します。<br />
     *   制御許可を行ったプロセスは、この機能で制御権を解放する必要があります<br />
     *
     * @see #requestDrivingControlUnLock( int nKey )
     *
     * @param nKey
     *            アプリの識別キー
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     * @return 結果(true：成功, false：失敗)
     *
    */
    public boolean requestDrivingControlUnLock( int nKey ) throws NaviException {
        NaviResult result = new NaviResult();

        boolean bIsUnLock = false;
        try {
            bIsUnLock = getNaviService().requestDrivingControlUnLock( nKey );
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "requestDrivingControlUnLock() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "requestDrivingControlUnLock() failed, service is dead?");
        }

        result.doException();

        return bIsUnLock;
    }
    /**
     * 走行規制を指示します。<br />
     * <br />
     *
     * ・走行規制を指示します。<br />
     *   予め走行規制許可を取得しておく必要があります。<br />
     *
     * @see #requestDrivingControl( int nKey, boolean bControl )
     *
     * @param nKey
     *            アプリの識別キー
     * @param bControl
     *            true：走行規制あり, false：走行規制なし
     * @throws NaviRemoteException
     *             リモート呼び出しのエラー時にスローします。
     * @throws NaviException
     *             ナビ例外のベースクラス
     * @return 結果(true：成功, false：失敗)
     *
    */
    public boolean requestDrivingControl( int nKey, boolean bControl ) throws NaviException {
        NaviResult result = new NaviResult();

        boolean bIsControlResult = false;
        try {
            bIsControlResult = getNaviService().requestDrivingControl( nKey, bControl );
        } catch (RemoteException e) {
            result.setResult(NaviResult.RESULT_REMOTE_ERROR, "requestDrivingControl() raised RemoteException");
        } catch (Exception e) {
            result.setResult(NaviResult.RESULT_ILLEGAL_STATE, "requestDrivingControl() failed, service is dead?");
        }

        result.doException();

        return bIsControlResult;
    }
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

    /** startActivity() のためのパッケージ名 */
    protected static final String NAVI_PACKAGE_NAME         = "net.zmap.android.pnd.v2";
    /** ナビサービス名 */
    protected static final String NAVI_SERVICE_NAME         = NAVI_PACKAGE_NAME + ".NaviService";
    /** ナビスタータサービス名 */
    protected static final String NAVI_STARTER_SERVICE_NAME = NAVI_PACKAGE_NAME + ".api.NaviStarterService";
    /** startActivity() のためのクラス名 */
    protected static final String NAVI_CLASS_NAME           = NAVI_PACKAGE_NAME + ".ItsmoNaviDrive";

    /** サービス起動後に処理を継続するためのハンドラ */
    protected Handler             mHandler                  = new Handler();
    /** アプリ起動直前からバインド直後まで */
    protected boolean             mIsNaviAppStarting        = false;
    /** タイムアウト監視用 */
    protected int                 mTimeoutTicks             = 0;

    /** API キュー */
    protected Queue<Runnable>     mApiQueue                 = new LinkedList<Runnable>();

    /**
     * ナビアプリが起動中か確認します。
     */
    public boolean isNaviAppRunning() {
        ActivityManager activityManager = (ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningAppProcessInfo> apps = activityManager.getRunningAppProcesses();
        for (RunningAppProcessInfo info : apps) {
            try {
                if (info.processName.equals(NAVI_PACKAGE_NAME) && getNaviService().isNaviAppRunning()) {
                    return true;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    /**
     * ナビアプリが起動しサービスに接続可能な状態か確認します。
     */
    public boolean isNaviAppReady() {
//        return isNaviAppRunning() && (mNaviService != null);
        try {
            return (mNaviService != null) && mNaviService.isStarted();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * ナビアプリが準備中 (起動しているがサービスに未接続) か確認します。
     */
    public boolean isNaviAppPreparing() {
        return mIsNaviAppStarting && (mNaviService == null);
    }

    /**
     * NaviStarterService が起動中か確認します。
     *
     * @return true:起動中 false:未起動
     */
    public boolean isNaviStarterServiceRunning() {
        ActivityManager activityManager = (ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
        for (RunningServiceInfo info : services) {
            if (info.service.getClassName().equals(NAVI_STARTER_SERVICE_NAME)) {
                return true;
            }
        }
        return false;
    }

    /**
     * ナビを全面に出す.
     */
    public void wakeupNaviApp() {
// MOD.2013.12.11 N.Sasao ナビ前面時のちらつき対応 START
    	boolean bFrontNavi = false;
    	String topActivityPkg = null;

    	if( mContext != null ){
	    	ActivityManager am =
	    			(ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE);
	    	try{
		    	List<RunningTaskInfo> runTask = am.getRunningTasks(5);
		    	if( runTask != null && !runTask.isEmpty() ){
		    		topActivityPkg = runTask.get(0).topActivity.getPackageName();
		    	}
	    	}
	    	catch( SecurityException e ){
	    		Log.i(TAG, "ItsmoNavi recommends that your app permits GET_TASK.");
	    	}
    	}

    	if( topActivityPkg != null && topActivityPkg.length() > 0 ){
	    	if( topActivityPkg.equals(NAVI_PACKAGE_NAME) ){
	        	bFrontNavi = true;
	    	}
    	}

    	// いつもNaviが前面にない or API使用側にGET_TASKの権限がない時はナビを全面に出す
    	if( !bFrontNavi ){
            Intent intent = new Intent();
            intent.setClassName(NAVI_PACKAGE_NAME, NAVI_CLASS_NAME);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, -1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarm = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
            alarm.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), 1, pendingIntent);
            alarm.cancel(pendingIntent);
    	}
// MOD.2013.12.11 N.Sasao ナビ前面時のちらつき対応  END
    }

    /**
     * ナビを起動する.
     */
    protected synchronized void startNavi() throws NaviException {
        final int WAIT_MILLIS = 500;
        final int WAIT_MAX = 60;

        mIsNaviAppStarting = true;

        // ナビが準備完了になるまで待つためのサービスを起動
        Log.d(TAG, "*** NaviStarterService starting...");
        Intent serviceIntent = new Intent(mContext, NaviStarterService.class);
        mContext.startService(serviceIntent);

        // ナビを起動
        Log.d(TAG, "*** ItsmoNaviPnd starting...");
        Intent intent = new Intent();
        intent.setClassName(NAVI_PACKAGE_NAME, NAVI_CLASS_NAME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(NaviIntent.EXTRA_INVOKED_BY_AGENT, true);
        try {
            mContext.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            mContext.stopService(serviceIntent);
            throw new NaviNotFoundException("ItsmoNaviPnd not found, install it...");
        }

        // いつもナビが大きすぎてレシーバを含むアクティビティを殺してしまうようだ
        // 回避策としてダミーサービス終了待ちを監視する :-(
        mTimeoutTicks = 0;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "*** NaviStarterService waiting...");
                if (isNaviStarterServiceRunning()) {
                    // 30 秒なっても起動しなかったらあきらめる
                    if (++mTimeoutTicks > WAIT_MAX) {
                        try {
                            throw new NaviTimeoutException("NaviStarterService not responding...");
                        } catch (NaviTimeoutException e) {
                            e.printStackTrace();
                        }
                        return;
                    }
                    mHandler.postDelayed(this, WAIT_MILLIS);
                } else {
                    Log.d(TAG, "*** ItsmoNaviPnd ready!");
                    mContext.bindService(new Intent(getServiceAction()), mConnection, getBindServiceFlags());
                    mIsNaviAppStarting = false;
                }
            }
        }, WAIT_MILLIS);

        // APIを使用するアプリのAndroidManifestにServiceの記述をしないといけないのは問題なので
        // とりあえずこの方法で検証し問題があれば他の方法を検討する
//        BroadcastReceiver receiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                Log.d(TAG, "onReceive()");
//                Log.d(TAG, "*** ItsmoNaviPnd ready!");
//                mContext.bindService(new Intent(getServiceAction()), mConnection, 0);
//                mIsNaviAppStarting = false;
//                mContext.unregisterReceiver(this);
//            }
//        };
//        mContext.registerReceiver(receiver, new IntentFilter(NaviIntent.ACTION_NAVI_START));
    }

    /**
     * API キューに Runnable を追加する.
     */
    protected synchronized void enqueueApi(Runnable r) {
        mApiQueue.offer(r);
    }

    /**
     * API キューに入っている Runnable をすべて処理する.
     */
    protected synchronized void dequeueAllApis() {
        while (true) {
            Runnable r = mApiQueue.poll();
            if (r == null) {
                break;
            }
            Log.d(TAG, "*** dequeueAllApis: " + r.toString());
            r.run();
        }
    }

    /**
     * ナビサービスの準備を済ませて Runnable を実行するラッパー.
     */
    protected synchronized void runOnNaviApp(Runnable r) throws NaviException {
        if (r == null) {
            throw new NullPointerException();
        }
        if (isNaviAppPreparing()) {
            Log.d(TAG, "*** enqueueApi");
            enqueueApi(r);
        } else {
            Log.d(TAG, "*** startNavi, enqueueApi");
            startNavi();
            enqueueApi(r);
        }
    }

}
