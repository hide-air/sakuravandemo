package net.zmap.android.pnd.v2.route.data;
/**
 ********************************************************************
 * Copyright (c) 2010.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project
 * File           DeleteBean.java
 * Description    無し
 * Created on     2010/12/04
 *
 ********************************************************************
 */
public class DeleteBean {
    private int index = 0;
    private String text = null;
	/**
	 * Created on 2010/12/04
	 * Description: 索引値を取得する
	 * @param: 無し
	 * @return： int
	 * @author:XuYang
	 */
	public int getIndex() {
		return index;
	}
	/**
	 * Created on 2010/12/04
	 * Description: 索引値を設定する
	 * @param: int
	 * @return：無し
	 * @author:XuYang
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	/**
	 * Created on 2010/12/04
	 * Description: テキストを取得する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public String getText() {
		return text;
	}
	/**
	 * Created on 2010/12/04
	 * Description: テキストを設定する
	 * @param: String
	 * @return： 無し
	 * @author:XuYang
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * Created on 2010/12/04
	 * Description: 時間を取得する
	 * @param: 無し
	 * @return： String
	 * @author:XuYang
	 */
	public String getTime() {
		return text;
	}
	/**
	 * Created on 2010/12/04
	 * Description: 時間を設定する
	 * @param: String
	 * @return： 無し
	 * @author:XuYang
	 */
	public void setTime(String text) {
		this.text = text;
	}

}
