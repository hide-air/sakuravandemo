/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           Constants.java
 * Description    定数の定義
 * Created on     2010/08/10
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.common;

public class Constants {

//Add 2011/11/22 Z01_h_yamada Start -->
    public final static boolean	isDebug = false;	// 開発者デバッグON/OFF
//Add 2011/11/22 Z01_h_yamada End <--

    public static final int Output_UsedTime_Calc = 0x00000000;
    /**
     * RESULT CODE
     */
    public final static short RESULTCODE_FINISH_ALL = 10;
//Chg 2011/09/21 Z01yoneya Start -->
//    public final static short RESULTCODE_FINISH_EXCEPT_MAPVIEW = 11;
//------------------------------------------------------------------------------------------------------------------------------------
    public final static short RESULTCODE_GOBACK_TO_ROUTE_EDIT = 11; //ルート編集画面まで戻る(までアクティビティを終了する)
//Chg 2011/09/21 Z01yoneya End <--
    public final static short RESULTCODE_ROUTE_CALCULATE = 12;
    public final static short RESULTCODE_GUIDE_END = 13;
    public final static short RESULTCODE_SIMULATION = 14;
    public final static short RESULTCODE_BACK = 15;
    public final static short RESULTCODE_FINISH_EXCEPT_ROUTE = 16;
    public final static short RESULTCODE_FAVORITE_REGISTER = 17;
    public final static short RESULTCODE_FINISH_FROM_COMMON = 18;
    public final static short RESULTCODE_ROUTE_DETERMINATION = 19;
    public final static short RESULTCODE_ROUTE_LIST = 20;
    public final static short RESULTCODE_SYSTEM_SET_CLEAN_DATA = 21;
    public final static short RESULT_REROUTE = 22;
    public final static short RESULT_CHANGE_NAVI_MODE_MANUAL = 23;
// Add by CPJsunagawa '13-12-15 Start
	public final static short RESULTCODE_VEHICLE_ROUTE_DETERMINATION = 24;
    //public final static short RESULTCODE_SHOW_LIST = 25;
    public final static short RESULTCODE_CHECK_PLACE = 26;
// Add by CPJsunagawa '13-12-15 End
// Add by CPJsunagawa '2015-07-08 Start
    public final static short RESULTCODE_REGIST_ICON = 27;
    public final static short RESULTCODE_INPUT_TEXT = 28;
// Add by CPJsunagawa '2015-07-08 End

    //yangyang add start bug813
    public final static String PARAMS_ROUTE_MOD_FLAG = "params_route_mod_flag";
    //yangyang add end bug813

    // xuyang add start 走行中の操作制限20101117
    public static boolean RunStopflag = false;
    public static boolean UserModeflag = false;
    public static boolean NaviOnOffflag = false;
    public static final int DIALOG_CHANGE_MODE_CAR_MENU = 0x100;
    // xuyang add end 走行中の操作制限20101117

    /** モード */
    public final static int NE_NAVIMODE_NONE = 0;
    /** 車モード(青) */
    public final static int NE_NAVIMODE_CAR = 1;
    /** 車モード(黄 DR学習中) */
    public final static int NE_NAVIMODE_YCAR = 4;
    /** 車モード(緑 DR走行可能) */
    public final static int NE_NAVIMODE_GCAR = 5;
    /** 車モード(赤 DR走行停止) */
    public final static int NE_NAVIMODE_RCAR = 6;
    /** 徒歩モード */
    public final static int NE_NAVIMODE_MAN = 2;
    /** アローモード */
    public final static int NE_NAVIMODE_BICYCLE = 3;

    //ナビモードが正当な値か判定
    public static boolean isValidNaviMode(int naviMode) {
        switch (naviMode) {
            case Constants.NE_NAVIMODE_CAR:
            case Constants.NE_NAVIMODE_YCAR:
            case Constants.NE_NAVIMODE_GCAR:
            case Constants.NE_NAVIMODE_RCAR:
            case Constants.NE_NAVIMODE_MAN:
            case Constants.NE_NAVIMODE_BICYCLE:
                return true;
            default:
                return false;
        }
    }

    public final static String FLAG_NAVIMODE = "flag_navi_mode";
//    public final static String FLAG_REROUTE = "flag_reroute";

//    /**
//     *
//     */
//    public final static int TRUE = 1;
//    public final static int FALSE = 1;

    /**
     *
     */
    public final static int DEF_NE_MAX_LANE_NUM = 8;
    public final static int KIWI_DIRECTIONGUIDE_LANE_MAX_NUM = 16; ///< KIWI-DB
    public final static int MAX_OF_HISTORY_LIST = 100;
    /**
     *
     */
    public final static int DEF_LANE_ARROW_BIT_FORWARD = 0x80;//
    public final static int DEF_LANE_ARROW_BIT_TILT_RIGHT = 0x40;//
    public final static int DEF_LANE_ARROW_BIT_TILT_LEFT = 0x20;//
    public final static int DEF_LANE_ARROW_BIT_TURN_RIGHT = 0x10;//
    public final static int DEF_LANE_ARROW_BIT_TURN_LEFT = 0x08;//
    public final static int DEF_LANE_ARROW_BIT_BACK_RIGHT = 0x04;//
    public final static int DEF_LANE_ARROW_BIT_BACK_LEFT = 0x02;//
    public final static int DEF_LANE_ARROW_BIT_U_TURN = 0x01;//

    public final static int NUI_ITM_IMG_MAPGUIDE_LANE_ARROW_ON_COMBI_BASE0 = 19;
    public final static int NUI_ITM_IMG_MAPGUIDE_LANE_ARROW_OFF_COMBI_BASE0 = 91;
    public final static int NUI_MODE_MAP_GUIDE = 3;
    /**
     *
     */
    public final static long DG_SUCCESS = 0x00000000;
    /**
     *
     */
    public final static int DG_HIGHWAY_GUIDE_START = 0;
    public final static int DG_HIGHWAY_GUIDE_VIA = 1;
    public final static int DG_HIGHWAY_GUIDE_GOAL = 1 << 1;
    public final static int DG_HIGHWAY_GUIDE_IC = 1 << 2; // /< IC
    public final static int DG_HIGHWAY_GUIDE_JCT = 1 << 3; // /< JCT
    public final static int DG_HIGHWAY_GUIDE_TOLLGATE = 1 << 4;
    public final static int DG_HIGHWAY_GUIDE_SA = 1 << 5; // /< SA
    public final static int DG_HIGHWAY_GUIDE_PA = 1 << 6; // /< PA

    /**
     *
     */
    public final static long CT_DualGuide_Nothing = 0x00000000;
    public final static long CT_DualGuide_DirectBoard = 0x00000001;
    public final static long CT_DualGuide_HighEntry = 0x00000002;
    public final static long CT_DualGuide_MagMap = 0x00000004;
    public final static long CT_DualGuide_JCTBMP = 0x00000008;
    public final static long CT_DualGuide_Real3D = 0x00000010;
    public final static long CT_DualGuide_GuideList = 0x00000020;
    public final static long CT_DualGuide_FastSatellite = 0x00000040;
    public final static long CT_DualGuide_PrepareMagMap = 0x00010000;

    public final static String SETTING_GPS_BUTTON_FLAG = "gps_measurement_information";
    /**
     *
     */
    public final static String MAP_VIEW_FLAG_KEY = "MAP_VIEW_FLAG_KEY";
    public final static int COMMON_MAP_VIEW = 0;
    public final static int OPEN_MAP_VIEW = 1;
    public final static int ROUTE_MAP_VIEW = 2;
    public final static int NAVI_MAP_VIEW = 3;
    public final static int SIMULATION_MAP_VIEW = 4;
    public final static int FIVE_ROUTE_MAP_VIEW = 5;
    public final static int ROUTE_MAP_VIEW_BIKE = 6;

    /**
     *
     */
    public final static int NUI_JCTARROW_STRAIGHT = 1; //
    public final static int NUI_JCTARROW_INCRIGHT = 6; //
    public final static int NUI_JCTARROW_INCLEFT = 3; //
    public final static int NUI_JCTARROW_RIGHT = 5; //
    public final static int NUI_JCTARROW_LEFT = 2; //
    public final static int NUI_JCTARROW_INCRIGHTBK = 7; //
    public final static int NUI_JCTARROW_INCLEFTBK = 4; //
    public final static int NUI_JCTARROW_UTURN = 8; //
    public final static int NUI_JCTARROW_NON = 0;

    /**
     *
     */
    public final static int NUI_KIND_TYPE_NON = 0;
    public final static int NUI_KIND_TYPE_IC = 1; // /< IC
    public final static int NUI_KIND_TYPE_JCT = 2; // /< JCT
    public final static int NUI_KIND_TYPE_SA = 3; // /< SA
    public final static int NUI_KIND_TYPE_PA = 4; // /< PA
    public final static int NUI_KIND_TYPE_TOLL = 5; //
    public final static int NUI_KIND_TYPE_CROSS = 6; //
    public final static int NUI_KIND_TYPE_MAX = 7; //

    /**
     *
     */
    public final static String EDG_SIMPLE_GUIDE_INTERSECTION = ""; //
    public final static String EDG_SIMPLE_GUIDE_JCT = "JCT"; //
    public final static String EDG_SIMPLE_GUIDE_IC = "IC"; //
    public final static String EDG_SIMPLE_GUIDE_INVALID = ""; //

    /**
     *
     */
    public final static long NUI_GUIDE_NON = 0x00000001; //
    public final static long NUI_GUIDE_JCTBITMAP = 0x00000002; //
    public final static long NUI_GUIDE_FREEWAYIN = 0x00000004; //
    public final static long NUI_GUIDE_DIRECTION = 0x00000008; //
    public final static long NUI_GUIDE_3DCROSS = 0x00000010; // 3D
    public final static long NUI_GUIDE_CROSS = 0x00000020; //
    public final static long NUI_GUIDE_SIMPLE = 0x00000040; //
    public final static long NUI_GUIDE_HIGHWAYKUSI = 0x00000080; //
    public final static long NUI_GUIDE_LANE = 0x00000100; //

    // Add 2011/01/15 sawada Start -->
    public final static int NUI_WG_DIR_FORWARD = 0; ///< 直進矢印 (道なり)
    public final static int NUI_WG_DIR_TILT_RIGHT = 1; ///< 前斜め右矢印
    public final static int NUI_WG_DIR_TILT_LEFT = 2; ///< 前斜め左矢印
    public final static int NUI_WG_DIR_TURN_RIGHT = 3; ///< 右矢印
    public final static int NUI_WG_DIR_TURN_LEFT = 4; ///< 左矢印
    public final static int NUI_WG_DIR_BACK_RIGHT = 5; ///< 後ろ斜め右矢印
    public final static int NUI_WG_DIR_BACK_LEFT = 6; ///< 後ろ斜め左矢印
    public final static int NUI_WG_DIR_U_TURN = 7; ///< U ターン矢印
    public final static int NUI_WG_DIR_INVALID = -1; ///< 無効 (簡易案内消去)

    public final static int NUI_WG_LINK_PEDESTRIAN_CROSSING = 1; ///< 横断歩道
    public final static int NUI_WG_LINK_PEDESTRIAN_BRIDGE = 3; ///< 歩道橋
    public final static int NUI_WG_LINK_NONE = -1; ///< なし
    // Add 2011/01/15 sawada End   <--

    /**
     *
     */
    public final static long NUI_STATE_ENABLE = 0x00000001; //
    public final static long NUI_STATE_DISABLE = 0x00000002; //

    public final static long NUI_STATE_SHOW = 0x00000001;//
    public final static long NUI_STATE_HIDE = 0x00000002; //

    public final static String FROM_FLAG_KEY = "from";
    public final static String ISAROUND_FLAG_KEY = "ard";
    public final static String POINT_NAME = "PointName";
    public final static String MAP_POINT_NAME = "MapPointName";
    public final static String FROM_WHICH_SEARCH_KEY = "FROM_WHICH_SEARCH_KEY";
    public final static String FROM_ROUTE_FLAG_KEY = "from_ROUTE";
    public final static String ROUTE_TYPE_KEY = "ROUTE_TYPE";
    public final static String ROUTE_ROUTE_NAME_KEY = "ROUTE_NAME_EDIT_KEY";

// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
    public final static String NOTICE_TYPE_KEY = "NOTICE_TYPE";
// ADD.2013.07.22 N.Sasao お知らせ機能実装  END

//    public final static String FROM_INTENT = "intent";
    //For startNaviFromIntent
    public final static String FROM_NAVISATRT_INTENT = "naviStartIntent";
    public final static String FROM_SEARCH = "search";
    public final static String FROM_AROUND_SEARCH = "around";
    public final static String FROM_OTHER_SEARCH = "other";
    public final static String FROM_HOME = "home";
    public final static String FROM_ROUTE_NEW = "0";
    public final static String FROM_ROUTE_EDIT = "1";
    public final static String FROM_NOWROUTE_EDIT = "2";
// Add by CPJsunagawa '2015-07-08 Start
    public final static String FROM_REGIST_ICON = "3";
    public final static String FROM_MOVE_ICON = "4";
    public final static String FROM_DELETE_ICON = "5";
    public final static String FROM_DRAW_LINE = "6";
    public final static String FROM_INPUT_TEXT = "7";
// Add by CPJsunagawa '2015-07-08 End
    public final static String FROM_ROUTE_ADD = "ROUTE_ADD";
    public final static String FROM_ROUTE_CHANGE = "ROUTE_CHANGE";

    public final static String ROUTE_TYPE_NEW = "btnNewRoute";
    public final static String ROUTE_TYPE_EDIT = "btnEditRoute";
    public final static String ROUTE_TYPE_NOW = "btnNowRoute";
    public final static String ROUTE_TYPE_List = "btnListRoute";
    public final static String ROUTE_NODE_INFO_KEY = "ROUTE_NODE_INFO_KEY";
// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
	public final static String NOTICE_TYPE_STATING 	= "NOTICE_STATING";
	public final static String NOTICE_TYPE_SETTING 	= "NOTICE_SETTING";
	public final static String NOTICE_TYPE_RESULT 	= "NOTICE_RESULT";
// ADD.2013.07.22 N.Sasao お知らせ機能実装  END
    //yangyang add
    public final static String PARAMS_PROVINCE_ID = "params_province_id";
    public final static String PARAMS_MYPOS_PROVINCE = "params_mypos_province";
    public final static String PARAMS_MYPOS_CITY_NAME = "params_mypos_city_name";
    public final static String PARAMS_MYPOS_CITYNEXT_NAME = "params_mypos_citynext_name";

    /**
     *
     */
    public static final int GUI_GUIDESTAT_STANDBY = 0; //
    public static final int GUI_GUIDESTAT_ACTION = 1; //
    public static final int GUI_GUIDESTAT_SIMURATION = 2; //
    public static final int GUI_GUIDESTAT_PAUSE = 3; //
    /**
     *
     */
    public static final int GUI_ROUTEMODE_CAR = 0;
    public static final int GUI_ROUTEMODE_MAN = 1;
    /**
     *
     */
    public final static int MSG_CANCEL_ROUTE = 0;
    public final static int MSG_ROUTE_SEARCH_FINISH = 1;

    /***/
    public static final int SCROLL_MAP_KIND = 12;

    /** 検索->地点地図 */
    public static final int LOCAL_INQUIRY_REQUEST = 21;
    /** メニュー ->現在地 */
    public static final int LOCATION_MAP_REQUEST = 22;
    /** 操作地図->現在地 */
    public static final int LOCATION_MAP_2_REQUEST = 24;
    /** 登録地点地図 */
    public static final int FAVORITE_ADD_REQUEST = 25;

    /**
     * Dialog type
     * for common dialog id start from 0x70.
     */
    private final static int DIALOG_BASE_ID = 0x30;

    public final static int DIALOG_EXIT_APP = DIALOG_BASE_ID + 0x1;
    public final static int DIALOG_EXIT_NAVIGATION = DIALOG_BASE_ID + 0x2;
    public final static int DIALOG_EXIT_SIMULATE_NAVI = DIALOG_BASE_ID + 0x3;

    public final static int DIALOG_NO_WALKER_DATA = DIALOG_BASE_ID + 0x4;
    public final static int DIALOG_NO_DETAIL_MAP_DATA = DIALOG_BASE_ID + 0x5;
    /*
     * 自宅
     */
    public final static int DIALOG_NO_HOME = DIALOG_BASE_ID + 0x6;
    public final static int DIALOG_LOGIN_HOME = DIALOG_BASE_ID + 0x7;
    public final static int DIALOG_MARK_LOGIN2 = DIALOG_BASE_ID + 0x8;
    public final static int DIALOG_HOUSE_BE2 = DIALOG_BASE_ID + 0x9;
    public final static int DIALOG_HOME2 = DIALOG_BASE_ID + 0x10;
    public final static int DIALOG_HOME = DIALOG_BASE_ID + 0x11;

    public final static int DIALOG_NOW_MAIL_FAIL = DIALOG_BASE_ID + 0x13;

//    /**Lite Version*/
//    public final static int DIALOG_LITE_VERSION = DIALOG_BASE_ID + 0x14;

    //yangyang add
    public final static int DIALOG_DELETE_FAVORITESGENRE = DIALOG_BASE_ID + 0x15;
    public final static int DIALOG_DELETE_FAVORITES = DIALOG_BASE_ID + 0x16;
    public final static int DIALOG_LOAD_FAVORITES = DIALOG_BASE_ID + 0x17;
    public final static int DIALOG_ADD_HOME = DIALOG_BASE_ID + 0x18;

    public final static int DIALOG_MARK_FULL = DIALOG_BASE_ID + 0x19;
    public final static int DIALOG_HOUSE_BE = DIALOG_BASE_ID + 0x1A;
    public final static int DIALOG_ROUTE_FAILURE = DIALOG_BASE_ID + 0x1B;
    public final static int DIALOG_DELETE_HISTORY = DIALOG_BASE_ID + 0x1C;
    public final static int DIALOG_DELETE_MYBOX = DIALOG_BASE_ID + 0x1D;
    public final static int DIALOG_MAX_FAVORITES = DIALOG_BASE_ID + 0x1E;
    public final static int DIALOG_WAIT = DIALOG_BASE_ID + 0x1F;
//Add 2011/11/15 Z01_h_yamada Start -->
    public final static int DIALOG_MEDIA_SIZE_INSUFFICIENT = DIALOG_BASE_ID + 0x20;
//Add 2011/11/15 Z01_h_yamada End <--
//Add 2011/12/14 Z01_h_yamada Start -->
    public final static int DIALOG_FILE_WRITE_FAILED = DIALOG_BASE_ID + 0x21;
//Add 2011/12/14 Z01_h_yamada End <--
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
    public final static int DIALOG_DURING_EXTERNAL_CONTROL = DIALOG_BASE_ID + 0x22;
    public final static int DIALOG_NOT_ALREADY_ENVIRONMENT = DIALOG_BASE_ID + 0x23;
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
    public final static int ROUTE_CALC_STATUS = 0;
    public final static int MAP_DETAILED_MESSAGE = 1;

    /**
     *
     */
    public final static short NUI_ANGLE_NO_0 = 0;
    public final static short NUI_ANGLE_NO_5 = 1; ///< 5
    public final static short NUI_ANGLE_NO_10 = 2; ///< 10
    public final static short NUI_ANGLE_NO_15 = 3; ///< 15
    public final static short NUI_ANGLE_NO_20 = 4; ///< 20
    public final static short NUI_ANGLE_NO_25 = 5; ///< 25
    public final static short NUI_ANGLE_NO_30 = 6; ///< 30
    public final static short NUI_ANGLE_NO_35 = 7; ///< 35
    public final static short NUI_ANGLE_NO_40 = 8; ///< 40
    public final static short NUI_ANGLE_NO_45 = 9; ///< 45
    public final static short NUI_ANGLE_NO_MAX = 10; ///<

    public static int gs_iMapScale = 1;
    public static int gs_iOPSound = 1;

    public static final int MapIconSize_Small = 0; //
    public static final int MapIconSize_Standard = 1; //
    public static final int MapIconSize_Large = 2; //

    public static final int MapWordSize_Small = 0; //
    public static final int MapWordSize_Standard = 1; //
    public static final int MapWordSize_Large = 2; //

    public static final int ONEWAY_OFF = 0; // /< OFF
    public static final int ONEWAY_ON = 1; // /< ON

    public static final int HEADINGUP_OFF = 0; // /< OFF
    public static final int HEADINGUP_ON = 1; // /< ON

    public static final int FRONT_OFF = 0; // /< OFF
    public static final int FRONT_ON = 1; // /< ON

    public static final int TGL_MAPCOLOR_DAY = 0; //
    public static final int MAPCOLOR_NIGHT = 1; //
    public static final int MAPCOLOR_AUTO = 2; //

    // For MapColorAuto
    public static final int MAPMODE_AUTO = 0; //
    public static final int MAPMODE_DAY_OR_NIGHT = 1; //

    public static final int MAPSCALE_UPPERLEVEL = 6; //
    public static final int MAPSCALE_MIDLEVEL = 4; //
    public static final int MAPSCALE_LOWERLEVEL = 2; //
    public static final int DEF_NE_NUM_SATELLITE = 12;
    public static final int DEF_RADRA_W = 296;
    public static final int DEF_RADRA_H = 296;
    public static final int DEF_RADRA_X = 0; //122;
    public static final int DEF_RADRA_Y = 0; //35;
//Del 2011/09/30 Z01_h_yamada Start -->
//    public static final int DEF_ICON_W = 30;
//    public static final int DEF_ICON_H = 30;
//Del 2011/09/30 Z01_h_yamada End <--
    public static final int DEF_RECEIVE_DIS = 180;
    public static final int DEF_RCEIVECOUNT_X = 0; //425;
    public static final int DEF_RCEIVECOUNT_Y = 0; //37;
    public static final int SETTING_POPDIALOG_CARTYPE = 1;
    public static final int SETTING_POPDIALOG_CARSRHTYPE = 2;
    public static final int SETTING_POPDIALOG_MANSRCTYPE = 3;
    public static final int SETTING_POPDIALOG_ICONTYPE = 4;
    public static final int SETTING_POPDIALOG_WORDTYPE = 5;
    public static final int SETTING_POPDIALOG_SCALETYPE = 6;
    public static final int SETTING_POPDIALOG_MAPTYPE = 7;
    public static final int SETTING_POPDIALOG_GUIDE_VOICE = 8;
    public static final int FROM_INTENT_INIT = -1;
    public static final int OPEN_MAP_FROM_INTENT = 1;
    public static final int ROUTE_DISPLAY_FROM_INTENT = 2;
    public static final int NAVI_START_FROM_INTENT = 3;

    public static final int DTAUM_INDEX_TOKYO = 1;
    public static final int CONVERT_FROM_WGS84 = 2;

    public static final String INTENT_ZOOM_LEVEL = "IntentZoomLevel";
    public static final String INTENT_LONGITUDE = "IntentLongitude";
    public static final String INTENT_LATITUDE = "IntentLatitude";

// Add by CPJsunagawa '13-12-15 Start
    // 顧客ID
    public static final String INTENT_CUST_ID = "IntentCustID";
    
    // 座標に変化なし
    public static final String INTENT_NO_CHANGE_COORD = "IntentNoChangeCoord";
// Add by CPJsunagawa '13-12-15 End

//    public static final String MAP_LONGITUDE = "map_longitude";
//    public static final String MAP_LATITUDE = "map_latitude";

    public static final String INTENT_NAVISTART = "net.zmap.android.pnd.v2.intent.action.APP_BEGIN";
    public static final String INTENT_NAVISTART_EXTRA = "net.zmap.android.intent.extra.START";
    public static final String INTENT_NAVIEND = "net.zmap.android.pnd.v2.intent.action.APP_END";
    public static final String INTENT_NAVIEND_EXTRA = "net.zmap.android.intent.extra.END";

    public static final String AREA_MOVED_INTENT_ACTION_EXTRA = "net.zmap.android.pnd.v2.intent.action.AREA_MOVED";
    public static final String PRE_AREATYPE_EXTRA_FROM = "net.zmap.android.pnd.v2.intent.extra.ADMIN_FROM";
    public static final String CUR_AREATYPE_EXTRA_FROM = "net.zmap.android.pnd.v2.intent.extra.ADMIN_TO";

    public static final String INTENT_VEHICLEINFO_EXTRA = "net.zmap.android.pnd.v2.intent.extra.LOCATION";
    public static final String ROADTYPE_EXTRA_FROM = "net.zmap.android.pnd.v2.intent.extra.ROAD_FROM";
    public static final String ROADTYPE_EXTRA_TO = "net.zmap.android.pnd.v2.intent.extra.ROAD_TO";
    public static final String ROADTYPE_INTENT_ACTION_EXTRA = "net.zmap.android.pnd.v2.intent.action.ROAD_MOVED";

    public static final String ROUTESEARCHED_INTENT_ACTION_EXTRA = "net.zmap.android.pnd.v2.intent.action.ROUTE_SEARCHED";
    public static final String INTENT_ROUTESEARCHED_SEARCHTYPE_EXTRA = "net.zmap.android.pnd.v2.intent.extra.ROUTE_CONDITION";
    public static final String INTENT_ROUTESEARCHED_STARTNAME_EXTRA = "net.zmap.android.pnd.v2.intent.extra.START_NAME";
    public static final String INTENT_ROUTESEARCHED_STARTLOCATION_EXTRA = "net.zmap.android.pnd.v2.intent.extra.START_LOCATION";
    public static final String INTENT_ROUTESEARCHED_GOALNAME_EXTRA = "net.zmap.android.pnd.v2.intent.extra.GOAL_NAME";
    public static final String INTENT_ROUTESEARCHED_GOALLOCATION_EXTRA = "net.zmap.android.pnd.v2.intent.extra.GOAL_LOCATION";
    public static final String INTENT_ROUTESEARCHED_VIANAME_EXTRA = "net.zmap.android.pnd.v2.intent.extra.VIA_NAMES";
    public static final String INTENT_ROUTESEARCHED_VIALOCATION_EXTRA = "net.zmap.android.pnd.v2.intent.extra.VIA_LOCATIONS";

    public static final String ROUTEGUIDE_INTENT_ACTION_EXTRA = "net.zmap.android.pnd.v2.intent.action.GUIDE";
    public static final String INTENT_ROUTEGUIDE_TYPE_EXTRA = "net.zmap.android.pnd.v2.intent.extra.GUIDE_TYPE";
    public static final String INTENT_ROUTETARGET_TYPE_EXTRA = "net.zmap.android.pnd.v2.intent.extra.TARGET_TYPE";
    public static final String INTENT_ROUTEGUIDE_TARGETNAME_EXTRA = "net.zmap.android.pnd.v2.intent.extra.TARGET_NAME";
    public static final String INTENT_ROUTEGUIDE_TARGETLOCATION_EXTRA = "net.zmap.android.pnd.v2.intent.extra.TARGET_LOCATION";
    public static final String INTENT_ROUTEGUIDE_DISTANCE_EXTRA = "net.zmap.android.pnd.v2.intent.extra.TARGET_DISTANCE";
    public static final String INTENT_ROUTEGUIDE_DIRECTION_EXTRA = "net.zmap.android.pnd.v2.intent.extra.TARGET_DIRECTION";

    public static final String INTENT_VIEW_ACTION = "android.intent.action.VIEW";
    public static final String INTENT_NAVI_ACTION = "net.zmap.android.pnd.v2.intent.action.NAVI";
    public static final String INTENT_NAVI_EXTRA_ROUTE_CONDITION = "nnet.zmap.android.pnd.v2.intent.extra.ROUTE_CONDITION";
    public static final String INTENT_NAVI_EXTRA_GOAL_NAME = "net.zmap.android.pnd.v2.intent.extra.GOAL_NAME";
    public static final String INTENT_NAVI_EXTRA_VIA_NAMES = "net.zmap.android.pnd.v2.intent.extra.VIA_NAMES";
    public static final String INTENT_NAVI_EXTRA_VIA_LOCATIONS = "net.zmap.android.pnd.v2.intent.extra.VIA_LOCATIONS";
    public static final String INTENT_NAVI_EXTRA_ROUTE_GUIDANCE_STARTING = "net.zmap.android.pnd.v2.intent.extra.ROUTE_GUIDANCE_STARTING";

    public static final String FAVORITE_INTENT_ACTION_EXTRA = "net.zmap.android.pnd.v2.intent.action.FAVORITE_SAVED";
    public static final String INTENT_FAVORITE_CATEGORY_EXTRA = "net.zmap.android.pnd.v2.intent.extra.FAVORITE_CATEGORY";
    public static final String INTENT_FAVORITE_NAME_EXTRA = "net.zmap.android.pnd.v2.intent.extra.FAVORITE_NAME";
    public static final String INTENT_FAVORITE_LOCATION_EXTRA = "net.zmap.android.pnd.v2.intent.extra.FAVORITE_LOCATION";

    public static final String INTENT_DOWNLOAD_APP_AUTHENTICATION = "net.zmap.android.pnd.v2.downloader.action.AUTHENTICATION";

    // xuyang add end 外部機器接続

    // xuyang add start いまココメール
    public final static short REQUESTCODE_NOW_MAIL = 24;
    // xuyang add end いまココメール

    /**
     * INTENT BROADCAST
     */
    public static final int NAVI_START_INTENT_BROADCAST = 1;
    public static final int NAVI_END_INTENT_BROADCAST = 2;

    public static final int START_NAVIGATION_INTENT_BROADCAST = 1;
    public static final int END_NAVIGATION_INTENT_BROADCAST = 2;
    public static final int SUSPEND_NAVIGATION_INTENT_BROADCAST = 3;
    public static final int RESUME_NAVIGATION_INTENT_BROADCAST = 4;

    public static final int STRING_MAX_DISPLAY_COUNT = 20;
    public static final long MAX_RECORD_COUNT = 500;
    public static final long NOT_RECORD_COUNT = 0;

    /*
     * SA PA
     */
    public final static int NUI_ICON_TOILET1 = 0x8814;
    public final static int NUI_ICON_TOILET2 = 0x881F;
    public final static int NUI_ICON_TOILET3 = 0x880F;

    public final static int NUI_ICON_GAS1 = 0x4081;
    public final static int NUI_ICON_GAS2 = 0x4083;
    public final static int NUI_ICON_GAS3 = 0x4084;
    public final static int NUI_ICON_GAS4 = 0x4085;
    public final static int NUI_ICON_GAS5 = 0x4086;
    public final static int NUI_ICON_GAS6 = 0x4087;
    public final static int NUI_ICON_GAS7 = 0x408A;
    public final static int NUI_ICON_GAS8 = 0x408B;
    public final static int NUI_ICON_GAS9 = 0x4092;
    public final static int NUI_ICON_GAS10 = 0x408C;
    public final static int NUI_ICON_GAS11 = 0x408D;
    public final static int NUI_ICON_GAS12 = 0x4097;
    public final static int NUI_ICON_GAS13 = 0x4099;
    //飲食
    public final static int NUI_ICON_RESTAURANT1 = 0x880C;//レストラン
    public final static int NUI_ICON_RESTAURANT2 = 0x880E;//軽食
    public final static int NUI_ICON_RESTAURANT3 = 0x8807;//コーヒー
    //コンビニ
    public final static int NUI_STORE1 = 0x208B;
    public final static int NUI_STORE2 = 0x208C;
    public final static int NUI_STORE3 = 0x208E;
    public final static int NUI_STORE4 = 0x2094;
    public final static int NUI_STORE5 = 0x2095;
    public final static int NUI_STORE6 = 0x209B;
    public final static int NUI_STORE7 = 0x209E;
    public final static int NUI_STORE8 = 0x20A0;
    public final static int NUI_STORE9 = 0x20A4;
    public final static int NUI_STORE10 = 0x20B0;
    public final static int NUI_STORE11 = 0x20B1;
    public final static int NUI_STORE12 = 0x20B3;
    public final static int NUI_STORE13 = 0x20C9;
    public final static int NUI_STORE14 = 0x20D3;
    public final static int NUI_STORE15 = 0x20D4;
    public final static int NUI_STORE16 = 0x20D5;
    public final static int NUI_STORE17 = 0x20D6;
    public final static int NUI_STORE18 = 0x20D7;
    public final static int NUI_STORE19 = 0x2080;
    public final static int NUI_STORE20 = 0x20CA;
    //買物
    public final static int NUI_SHOP21 = 0x8812;
    public final static int NUI_SHOP22 = 0x8808;
    public final static int NUI_SHOP23 = 0x8811;
    //休憩施設
    public final static int NUI_REST1 = 0x880D;
    public final static int NUI_REST2 = 0x880A;
    public final static int NUI_REST3 = 0x8803;
    public final static int NUI_REST4 = 0x881C;
    public final static int NUI_REST5 = 0x8810;
    //交通情報
    public final static int NUI_TRAFFIC1 = 0x8802;
    public final static int NUI_TRAFFIC2 = 0x8809;
    //連絡手段
    public final static int NUI_COMMUNCATION1 = 0x8816;
    public final static int NUI_COMMUNCATION2 = 0x8801;
    //お金
    public final static int NUI_MONEY1 = 0x881E;
    public final static int NUI_MONEY2 = 0x881D;
    //コイン施設
    public final static int NUI_FACILITY1 = 0x8804;
    public final static int NUI_FACILITY2 = 0x8806;
    public final static int NUI_FACILITY3 = 0x8805;
    //郵便ポスト
    public final static int NUI_MAIL = 0x880B;
    //スマートIC
    public final static int NUI_IC = 0x8940;

    public final static int LONG_MAX = 2147483647;
//    /*
//     * SearchProperty
//     */
//    public static final String CAR_MODE = "0";
//    public static final String MAN_MODE = "1";
//
//    public static final String SEARCHSTATE_RECOMMEND = "4";
//    public static final String SEARCHSTATE_HIGHWAY = "1";
//    public static final String SEARCHSTATE_NORMAL = "2";

//    public static final String MANNAVISEARCHPROPERTY_DEFAULT = "0";
//    public static final String MANNAVISEARCHPROPERTY_ROOFED = "1";
//    public static final String MANNAVISEARCHPROPERTY_EASINESS = "2";
//    public static final String MANNAVISEARCHPROPERTY_HISPEED = "3";
//Del 2011/10/03 Z01yoneya Start -->
//    public static boolean ROUTE_EXIST = false;
//Del 2011/10/03 Z01yoneya End <--
    //button click
//    public static final int ROUTE_DELETE = 37;
//    public static final int IS_OPEN_MAP = 36;
//    public static boolean CALL_BACK = false;
//    public static boolean OPEN_MAP = false;
//    public static boolean BACK_OPEN_MAP = false;
    //button click end
    public static final int SURROUNDING = 35;
    public static boolean IBSURROUNDING = false;
    public static boolean MENU_SURROUNDING = false;
    public static boolean MENU_SEARCH = false;
    public static boolean FROM_ROUTE_CAL = false;
    public static String DIALOG_FROM = null;
    public static final String DIALOG_FROM_MAP = "dialog is from Map";
    public static final String DIALOG_FROM_ROUTE_SAVE = "dialog is from Route";
    public static boolean FROM_POIFAVORITEREGISTERVIEW = false;
    public static boolean isFavoriteRegister = false;
    public static long Favorite_LAT = 0;
    public static long Favorite_LONG = 0;

    public static final int OFF = 0;
    public static final int ON = 1;

    //ItsmoNavi-PND製品版は下記のようにする。絶対変更しないこと。
    //GPS：有効
    //加速度・ジャイロセンサー：有効
    //ログ再生、ログ出力：全部無効
    //
    //※変更する場合はVPSetting.iniを使用すること
    public static int LoadMode_LOG = ON; //ほとんど意味が無いが、ソースで参照している部分があるので残す。ログ出力のONでは無い！意味がわからないのでONのままにしておく。
    public static int LoadMode_SENSOR = ON; //加速度センサー・ジャイロセンサーを有効・無効フラグ。SensorManagerを起動する。
    public static int VP_LoadMode_LocationManagerLOG = 0x00000000; //NEUが作成したVPログによるログ再生の有効・無効フラグ
    public static int VP_LoadMode_LoggerManager = OFF; //Logger.txtによるログ再生の有効・無効フラグ
    public static int VPLOG_OUTPUT = 0x00000000; //NEUが作成したVPログの出力フラグ
    public static int VPLOG_OUTPUT_LOGGER = OFF; //Logger.txt(GPS/センサーのログ)の出力フラグ
    public static int VPLOG_OUTPUT_MONITOR = OFF; //bluetoothを利用したセンサーモニタ機能の有効無効フラグ

// Add 2011/03/23 sawada Start -->
    public static int USE_ELECTRIC_COMPASS = OFF;
// Add 2011/03/23 sawada End   <--

//    public static boolean ISFROMARROUND = false;

    // For DragMap Start
    public static long SCREEN_CENTER_X = 400;
    public static long SCREEN_CENTER_Y = 225;
    //For DragMap End

    //  For arround search button Start
    public final static String NOT_FROM_AROUND_SEARCH = "not from around search";
    public static boolean AROUND_SEARCH_ROUTE_FLAG = false;
    // For arroundsearchbutton End

    public final static int NE_GuideStat_Standby = 0;
    public final static int NE_GuideStat_Action = 1;
    public final static int NE_GuideStat_Simulation = 2;
    public final static int NE_GuideStat_Pause = 3;
    public final static int VP_SUCCESS = 1;
    public final static int VP_FAILURE = 0;
    //DG Voice volume Ctrl default,Max,Min
    public final static int DG_VoiceVolume_Default = 80; //voluem = (max-min)*80/100.0+min ;
    public final static int DG_VoiceVolume_Max = 2; //200%
    public final static int DG_VoiceVolume_Min = 1; //0%

    // xuyang add start 5route
    public final static int DG_ROUTE_SEARCH_TYPE_NONE = 0;
    public final static int DG_ROUTE_SEARCH_TYPE_COMMEND = 0x1 << 1; ///< 高速
    public final static int DG_ROUTE_SEARCH_TYPE_GENERAL = 0x1 << 2;///< 一般
    public final static int DG_ROUTE_SEARCH_TYPE_DISTANCE = 0x1 << 3;// 距離
    public final static int DG_ROUTE_SEARCH_TYPE_WIDTH = 0x1 << 4;// 道幅
    public final static int DG_ROUTE_SEARCH_TYPE_ANOTHER = 0x1 << 5; // 別ルート
    // xuyang add end 5route

    /** SA.PA アイコン表示の個数 */
    public final static int SAPA_SHOW_ICON_COUNT = 8;

    // 1: 24 * 24
    public static final int Icon_Size = 1;

//    // ジャンル検索結果から、地点地図に遷移するフラグ
//    public final static String GENRE_OPENMAP_FLAG = "GenreToOpenMap";

    public final static String ROUTE_FLAG_KEY = "routeFlag";
    public final static String FROM_ROUTE_SEARCH = "routeFlag";
    public final static String FROM_NAVI_SEARCH = "navi";
    //add by yangyang start
    public final static String PARAMS_HISTORY_FROM_MAINMENU = "params_history_from_mainmenu";
    public final static String PARAMS_SEARCH_KEY = "params_search_key";
    public final static String PARAMS_GENRE_INDEX = "params_genre_index";
    public final static String PARAMS_LIST_INDEX = "params_list_index";
    public final static String PARAMS_MYHOME_FLAG = "params_myhome_flag";
    public final static String MYHOME_01 = "myhome_01";
    public final static String MYHOME_02 = "myhome_02";
    public final static String PARAMS_TREERECINDEX = "params_treerecindex";
    public final static String PARAMS_WIDECODE = "params_widecode";
    public final static String PARAMS_MIDDLECODE = "params_middlecode";
    public final static String PARAMS_NARROWCODE = "params_narrowcode";
    public final static String PARAMS_IS_SHOW_BUTTON = "params_is_show_button";
    public final static String PARAMS_CLICK_LON = "params_click_lon";
    public final static String PARAMS_CLICK_LAT = "params_click_lat";
    public final static String PARAMS_CLICK_NAME = "params_click_name";

    public final static String PARAMS_MIN_LIMITED = "params_min_limited";
    public final static String PARAMS_MAX_LIMITED = "params_max_limited";
//Del 2011/09/17 Z01_h_yamada Start -->
//    public final static String PARAMS_TITLE = "params_title";
//Del 2011/09/17 Z01_h_yamada End <--
//Add 2011/09/17 Z01_h_yamada Start -->
    public final static String PARAMS_FAVORITES_EDIT = "params_favorites_edit";
//Add 2011/09/17 Z01_h_yamada End <--
    public final static String PARAMS_BUTTON_NAME = "params_button_name";
    public final static String PARAMS_SEARCH_TYPE = "params_search_type";
    public final static String PARAMS_HINT_VALUE = "params_hint_value";
    public final static String PARAMS_RECORD_COUNT = "params_record_count";
    public final static String PARAMS_MYHOME_RECORD_COUNT = "params_myhome_record_count";
    public final static String FLAG_BACK_CAR_POS = "car_location";
    public final static String FLAG_FAVORITE_TO_MAP = "favorite_map";
    public final static String PARAMS_AV_FLAG = "params_av_flag";

    public final static String PARAMS_IS_CHANGE_NAVIMODE = "params_is_change_navimode";

// Add by CPJsunagawa '13-12-15 Start
    // マッチングモードを埋めるためのIntentのエクストラ
    public final static String INTENT_EXTRA_MATCHING_MODE = "matchingMode";
// Add by CPJsunagawa '13-12-15 End
    
    public final static String INTENT_EXTRA_SELECT_ICON = "selectIcon";
    public final static String INTENT_EXTRA_MOVE_ICON = "moveIcon";
    public final static String INTENT_EXTRA_DELETE_ICON = "deleteIcon";
    public final static String INTENT_EXTRA_DRAW_LINE = "drawLine";
    public final static String INTENT_EXTRA_DRAW_LINE_ALL_CANCEL = "allCancel";

    public final static String FLAG_MENU_TYPE = "menu_type";
    public final static int MENU_NORMAL = 0x1;
    public final static int MENU_NAVI = 0x2;

    //data from ExConfig.ini
    public static String mail_title;
    public static String mail_Content;
    public static int times = 1000;
    public static int flag;
    public static int GET_JNI_COUNT;
    public static int vp_speed;

    //Navi Point index data
    public final static int NAVI_DEST_INDEX = 8;
    public final static int NAVI_START_INDEX = 1;
    public final static int NAVI_VIA_INDEX = 3;

    /** 自宅1 */
    public final static int NE_HOME_1 = 7;

    /** 自宅2 */
    public final static int NE_HOME_2 = 8;

    public final static String FLAG_TITLE = ">";

    public final static int[] ROUTE_5_TYPE = new int[] {13, 14, 16, 15, 17};

    /** 歩行者探索の最大距離について */
// Mod -- 2011/10/15 -- H.Fukushima --- ↓ ----- MarketVer2 No.104 探索範囲を広げる
//    public final static int MAX_WALK_ROUTE_DIST = 10 * 1000;
    public final static int MAX_WALK_ROUTE_DIST = 15 * 1000;
// Mod -- 2011/10/15 -- H.Fukushima --- ↑ ----- MarketVer2 No.104 探索範囲を広げる

//Del 2011/11/11 Z01_h_yamada Start -->
//    // 自宅探索エラー（S-E2）の距離は、車：10m以下、歩行者：3m以下、アロー：3m以下でエラーとする
//    public final static int NAVI_HOME_CAR_ROUTE_MIN_DIST = 10;
//    public final static int NAVI_HOME_WALK_BIKE_ROUTE_MIN_DIST = 3;
//Del 2011/11/11 Z01_h_yamada End <--

    /** 8K-char-buffer */
    public final static int _8K_CHAR_BUFFER_SIZE = 16384; //8 * 1024 * 2;

    public final static String AddressString = "AddressString";
    public final static String NotFromSearch = "not from search";

    //  _MULTI_ROUTE_CALC
    public final static int DEF_ROUTE_CALCULATE_TYPE_NONE = 0;// (0)
    public final static int DEF_ROUTE_CALCULATE_TYPE_MAIN = 1;//  (1)
    public final static int DEF_ROUTE_CALCULATE_TYPE_MULTI = 2;// (2)
//yangyang add start
    public final static String AROUND_CONFIG_PATH = "/assets/NaviAroundConfigData.ini";
    public final static String AROUND_CONFIG_TAG_CAR = "[CAR]";
    public final static String AROUND_CONFIG_TAG_MAN = "[MAN]";
    public final static String AROUND_CONFIG_CONTENTS_KEY = "Name";
    public final static String AROUND_CONFIG_SPLIT_VALUE = " ";

    public final static String STRING_YEN = "円";
    public static boolean doMoveNextPage = false;

    public static boolean GenreFlag = false;
    public static boolean informationBarShow = false;
    public static String LEFT_BRACKETS = "[";
    public static String RIGHT_BRACKETS = "]";
    public static String SIX_SPACE = "      ";
    //yangyang add end
//Add 2011/07/27 Z01thedoanh (自由解像度対応) Start -->
    public final static int GUIDE_CROSS_ZOOM_HEIGHT_OFFSET = 212;
    public final static int NAVI_GUIDE_LIST_ITEM_TEXT_INFO_POS = 35;
    public final static int NAVI_GUIDE_LIST_ITEM_TEXT_INFO_WIDTH = 260;
    public final static float GUIDE_AREA_ASPECT1 = (float)0.80625;
    public final static float GUIDE_AREA_ASPECT2 = (float)0.625;
//Add 2011/07/27 Z01thedoanh (自由解像度対応) End <--

//Add 2011/08/15 Z01_h_yamada Start -->
    public final static float DISTANCE_UNIT_FONT_RATE = 0.8f; // 距離単位のフォント表示サイズ割合
//Add 2011/08/15 Z01_h_yamada End <--
//Add 2011/08/26 Z01thedoanh Start -->
    public static final int DC_SHOW_LIST_MENU = 0x1;
    public static final int DC_SHOW_SETTING_MENU = 0x2;
    public static final int DC_DRAW_ICON = 0x3;
    public static final int DC_DETAIL_POI = 0x4;
    public static final int DC_HOMEPAGE_ACCESS = 0x5;
    public static final int DC_AROUND_SEARCH = 0x6;
    public static final int DC_CHANGES_SETTING = 0x7;
    public static final int DC_UPDATE_DATA = 0x8;
    public static final int DC_MAP_CHANGED = 0x9;
    public static final int DC_WAIT_DIALOG_TIMEOUT = 0xA;
    public static final int DC_ORBIS_DATA_CHANGED = 0xB;
    public static final int DC_CANCEL = 0xC;
    public static final int DC_GET_CONTENTS= 0xD;
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1) START
    public static final int DC_FORCE_REDRAW= 0xE;
// ADD.2013.02.25 N.Sasao Bug #9920 地図スクロール及び縮尺変更が滑らかではない(1)  END
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応 START
    public static final int DC_POI_DETAIL_DATA= 0xF;
// ADD.2013.04.01 N.Sasao POI詳細情報取得タイミング変更対応  END
// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない  START
    public static final int DC_CHANGES_SCALE= 0x10;
// ADD.2013.04.12 N.Sasao ユーザー選択したコンテンツ数が既定以上の場合描画しない   END
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ) START
    public static final int DC_DATA_CLEAR = 0x11;
// MOD.2013.07.08 N.Sasao ドライブコンテンツリストキャッシュ対応2(アプリケーション領域へ)  END
    public static final int DC_INTENT_GROUP_SET_DATA = 0x100;
    public static final int DC_INTENT_CONTENTS_SET_DATA = 0x101;
    public static final String DC_INTENT_DETAI_SETTING_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.detail_setting";
    public static final String DC_INTENT_DATA_SETTING_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.data_setting";
    public static final String DC_INTENT_DATA_SETTING_INDEX_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.data_setting_index";
    public static final String DC_INTENT_DATA_SETTING_LEVEL_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.data_setting_level";
    public static final String DC_INTENT_GROUP_CONTENT_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.group.content";
    public static final String DC_INTENT_GROUP_SECTION_KEY_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.group._SECTION_KEY";
    public static final String DC_INTENT_GROUP_SECTION_ITEM_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.group._SECTION_ITEM";
    public static final String DC_INTENT_GROUP_PARENT_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.group.parent";
    public static final String DC_INTENT_GROUP_PARENT_ITEM_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.group.parent.item";
    public static final String DC_INTENT_GROUP_PARENT_INDEX_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.group.parent.index";
    public static final String DC_INTENT_GROUP_CHILD_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.group.child";
    public static final String DC_INTENT_GROUP_CONTENT_SET_DATA_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.group.content_set_data";
    public static final String DC_INTENT_DIALOG_TO_MENU_EXTRA = "net.zmap.android.pnd.v2.intent.extra.dc.dialog_to_menu";

//Add 2011/08/26 Z01thedoanh End <--
//Add 2011/09/19 Z01yamaguchi Start -->
    public static final String DC_INTENT_DETAIL_INFO = "net.zmap.android.pnd.v2.intent.extra.dc.detail_info";
//Add 2011/09/19 Z01yamaguchi End <--

//Add 2011/09/09 Z01_h_yamada Start -->
    public final static float KANA_FONT_RATE = 0.7f; // かなグループの行文字のフォント割合
//Add 2011/09/09 Z01_h_yamada End <--

//Add 2011/09/30 Z01yamaguchi Start -->
    public final static int VICSINFO_5 = 0; // Vics 更新間隔 : 5分
    public final static int VICSINFO_10 = 1; // Vics 更新間隔 : 10分
    public final static int VICSINFO_15 = 2; // Vics 更新間隔 : 15分
    public final static int VICSINFO_NONE = 3; // Vics 更新間隔 : オフ
//Add 2011/09/30 Z01yamaguchi End <--

// Add 2011/10/12 katsuta Start -->
    public final static boolean	useKiwiIconFlag = true;	// KIWIのアイコンを使用する場合 true
// Add 2011/10/12 katsuta End <--

	public static final byte MAPSCALE_DEFAULTLEVEL_MAIN = 4; // メイン地図スケールのデフォルト

// Add by CPJsunagawa '13-12-15 Start
    // 住所検索時、「住所からマニュアルマッチング」である旨をIntentのExtraに入れて、
    // それがそうであるかどうかはhasExtraで判定する。
    public static final String ADDRESS_SEARCH_FOR_MATCH = "AddressSearchForMatch";
    
// Add by CPJsunagawa '2015-07-08 Start
    public static final String REGIST_ICON = "RegistIcon";
    public static final String INPUT_TEXT = "InputText";
    public static final String SELECT_ICON_INDEX = "SelectIconIndex";
// Add by CPJsunagawa '2015-07-08 End

	// マッチ中の、大本の住所を格納する
    public static final String ADDRESS_SEARCH_DB_ADR = "AddressSearchDBAdr";
	
    // resultCodeがConstants.RESULTCODE_VEHICLE_ROUTE_DETERMINATIONで、Intentにこれらが入っていたとき、
    // 該当インデックスのBLを変更し、DBにも反映する。
    public static final String CHANGE_COORD_LIST_INDEX = "ChangeCoordListIndex";
    //public static final String CHANGE_COORD_LONGITUDE = "ChangeCoordLongitude";
    //public static final String CHANGE_COORD_LATITUDE = "ChangeCoordLatitude";
    
    // らくらくNAVI用 接近時のクローズアップの閾値
    public static final double CLOSE_UP_DISTANCE = 100.0;
    
    // らくらくNAVI用アイコンレイヤ
    public static final String SAKURA_ICON_ID = "SakuraIcon";
// Add by CPJsunagawa '13-12-15 End

	
	public static final String PREFIX_VOICE_FILE = "ZVS_NaviVoice_";

// ADD.2013.07.22 N.Sasao お知らせ機能実装 START
    public static final int NC_INFORMATION 		= 0x0001;
    public static final int NC_MAP_SETTING 		= 0x0002;
    public static final int NC_MAP_ICON		 	= 0x0004;
    public static final int NC_DR_SETTING 		= 0x0008;
    public static final int NC_RE_INFORMATION 	= 0x0010;
    public static final int NC_USER_CONFIRM 	= 0x0011;
// ADD.2013.08.21 N.Sasao お知らせ追加機能(描パラダウンロード)実装 START
    public static final int NC_EXT_PARAM0		= 0x0012;
    public static final int NC_EXT_PARAM1		= 0x0014;
// ADD.2013.09.12 N.Sasao お知らせ機能フェールセーフ不具合対応 START
    public static final int NC_INFORMATION_FAILED_SAFE = 0x8000;
// ADD.2013.09.12 N.Sasao お知らせ機能フェールセーフ不具合対応  END
// ADD.2013.08.21 N.Sasao お知らせ追加機能(描パラダウンロード)実装  END

    public static final int NC_RESULT_OK 		= 0x1;
    public static final int NC_RESULT_NG 		= 0x0;
// ADD.2013.07.22 N.Sasao お知らせ機能実装  END

}
