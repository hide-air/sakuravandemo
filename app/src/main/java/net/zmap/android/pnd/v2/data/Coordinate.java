/*------------------------------------------------------------------------------
 * Copyright(C) 2011 ZenrinDataCom Co.,LTD. All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of ZenrinDataCom Co.,LTD.;
 * the contents of this file is not to be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior written
 * permission of ZenrinDataCom Co.,LTD.
 *------------------------------------------------------------------------------*/

package net.zmap.android.pnd.v2.data;

/**
 * 緯度経度情報を保持するクラスです。
 *
 * @id $Id: Coordinate.java 4571 2012-01-20 07:52:53Z horie-t $
 */
public class Coordinate {
	/** 緯度 */
	private long mLongitude;
	/** 経度 */
	private long mLatitude;

	public Coordinate()
	{
		mLongitude = 0;
		mLatitude = 0;
	}
	public Coordinate(long Longitude,long Latitude)
	{
		mLongitude = Longitude;
		mLatitude = Latitude;
	}
	/**
	 * 緯度を返します。
	 *
	 * @return longitude
	 */
	public long getLongitude() {
		return mLongitude;
	}

	/**
	 * 緯度を設定します。
	 *
	 * @param longitude セットする mLongitude
	 */
	public void setLongitude(long longitude) {
		mLongitude = longitude;
	}

	/**
	 * 経度を返します。
	 *
	 * @return latitude
	 */
	public long getLatitude() {
		return mLatitude;
	}

	/**
	 * 経度を設定します。
	 *
	 * @param latitude セットする mLatitude
	 */
	public void setLatitude(long latitude) {
		mLatitude = latitude;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Coordinate) {
			Coordinate other = (Coordinate) object;
			return mLongitude == other.getLongitude()
				|| mLatitude == other.getLatitude();
		} else {
			return false;
		}
	}

	@Override
    public int hashCode() {
	    final int multiplier = 31;
	    int result = 1;
	    result = multiplier * result + (int)(mLongitude ^ (mLongitude >>> 32));
        result = multiplier * result + (int)(mLatitude  ^ (mLatitude  >>> 32));

        return result;
    }
}
