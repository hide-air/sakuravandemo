
package net.zmap.android.pnd.v2.common.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.LinearLayout;

import net.zmap.android.pnd.v2.R;


/**
 * メッセージとOK、キャンセル(デフォルトは非表示)ボタンを表示するダイアログ
 */
public class MsgDialog extends Dialog {

    private Button mbtnOk;
    private Button mbtnCancel;
    private TextView tvMsg;
    // OKボタン押下時のListener
    private OnMsgBtnListener mMsgBtnListener = null;

    /**
     * メッセージダイアログ表示
     * 
     * @param context コンテキスト
     * @param msg ダイアログに表示するメッセージ
     * @param useCancel キャンセルボタンを使用するか否か
     */
    public MsgDialog(Context context, String msg, boolean useCancel) {
        super(context);

        // ダイアログレイアウト
        LinearLayout dlgLayout = (LinearLayout) LayoutInflater.from(context).inflate(
                R.layout.dlg_ok, null);

        // ボタン周りの初期化
        InitComponent(dlgLayout, useCancel);

        // メッセージの設定
        tvMsg = (TextView) dlgLayout.findViewById(R.id.tvMsg);
        tvMsg.setText(msg);

        // タイトル非表示
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        // モーダル状態セット
        this.setCanceledOnTouchOutside(false);

        // レイアウトセット
        this.setContentView(dlgLayout);
    }

    /**
     * メッセージダイアログ表示
     * 
     * @param context コンテキスト
     * @param msg ダイアログに表示するメッセージ
     * @param useCancel キャンセルボタンを使用するか否か
     * @param disableEnter Enterキー入力による、OKボタン押下を無効にするか否か
     */
    public MsgDialog(Context context, String msg, boolean useCancel, boolean disableEnter) {
        this(context, msg, useCancel);
        if (disableEnter) { // Enter無効化
            mbtnOk.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (KeyEvent.KEYCODE_ENTER == keyCode) {
                        if (KeyEvent.ACTION_UP == event.getAction()) {
                            return true;
                        }
                    }
                    return false;
                }
            });
        }
    }

    /**
     * ボタン周りの初期化
     * 
     * @param dlgLayout ダイアログレイアウト
     * @param useCancel キャンセルボタンを使用するか否か
     */
    private void InitComponent(LinearLayout dlgLayout, boolean useCancel) {

        // OKボタンイベントセット
        mbtnOk = (Button) dlgLayout.findViewById(R.id.btnOK);
        mbtnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (mMsgBtnListener != null) {
                    mMsgBtnListener.onPressedMsgOK();
                }
                MsgDialog.this.dismiss();
            }
        });
        if (useCancel) {
            mbtnCancel = (Button) dlgLayout.findViewById(R.id.btnCancel);
            mbtnCancel.setVisibility(View.VISIBLE);
            // キャンセルボタンイベントセット
            mbtnCancel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    MsgDialog.this.dismiss();
                }
            });
        }
    }

    /**
     * メッセージのフォントサイズを変更する
     * 
     * @param size 設定するフォントのサイズ
     */
    public void setMsgFontSize(float size) {
        tvMsg.setTextSize(size);
    }

    /**
     * デフォルト時、"OK"が表示されるボタンのラベルを変更する
     * 
     * @param str 設定するボタンのラベル
     */
    public void setButtonLabel(String str) {
        mbtnOk.setText(str);
    }

    /**
     * デフォルト時、"キャンセル"が表示されるボタンのラベルを変更する
     * 
     * @param str 設定するボタンのラベル
     */
    public void setCancelButtonLabel(String str) {
        mbtnCancel.setText(str);
    }

    /**
     * ボタン押下時のListenerを設定する
     * 
     * @param listener 設定するListener
     */
    public void setListener(OnMsgBtnListener listener) {
        mMsgBtnListener = listener;
    }

    /**
     * ボタン押下時のListener用Interface
     */
    public interface OnMsgBtnListener {
        public void onPressedMsgOK();
    }

}
