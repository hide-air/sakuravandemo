/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZGRL_SIZE.java
 * Description    long Class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZGRL_SIZE {
	private short	unWidth;				///< 幅
	private short	unHigth;				///< 高さ
	/**
	 * <p>Description:[...]</p>
	 * @param unWidth The unWidth to set.
	 */
	public void setUnWidth(short unWidth) {
		this.unWidth = unWidth;
	}
	/**
	 * <p>Description:[...]</p>
	 * @return short unWidth.
	 */
	public short getUnWidth() {
		return unWidth;
	}
	/**
	 * <p>Description:[...]</p>
	 * @param unHigth The unHigth to set.
	 */
	public void setUnHigth(short unHigth) {
		this.unHigth = unHigth;
	}
	/**
	 * <p>Description:[...]</p>
	 * @return short unHigth.
	 */
	public short getUnHigth() {
		return unHigth;
	}
}