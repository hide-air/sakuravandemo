package net.zmap.android.pnd.v2.api.util;

import android.location.Location;
import android.location.LocationManager;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.data.JNITwoLong;

/**
 * 座標変換クラス
 */
public class GeoUtils {

    /** 地図緯度方向上限 54.666666666666666666666666666667 */
    private static final long MAP_TOP            = 196800000;
    /** 地図緯度方向下限 120 */
    private static final long MAP_BOTTOM         = 43200000;
    /** 地図経度方向下限 104 */
    private static final long MAP_LEFT           = 374400000;
    /** 地図経度方向上限 168 */
    private static final long MAP_RIGHT          = 604800000;

    private static final int  DATUM_INDEX_WGS84  = 0;
    private static final int  DATUM_INDEX_TOKYO  = 1;

    private static final int  CONVERT_TO_WGS84   = 1;
    private static final int  CONVERT_FROM_WGS84 = 2;

    /**
     * 指定座標が地図範囲内にあるか
     *
     * @param latitude
     *            緯度(東京測地系) (ミリ秒)
     * @param longitude
     *            経度(東京測地系) (ミリ秒)
     * @return true: 範囲内、false: 範囲外
     */
    public static boolean inBoundMapTKY(long latitude, long longitude) {
        return latitude <= MAP_TOP && latitude >= MAP_BOTTOM && longitude >= MAP_LEFT && longitude <= MAP_RIGHT;
    }

    /**
     * 指定座標が地図範囲内にあるか
     *
     * @param locate
     *            経緯度(世界測地系)
     * @return true: 範囲内 /false: 範囲外
     */
    public static boolean inBoundMapWGS(Location locate) {
        return inBoundMapWGS(locate.getLatitude(), locate.getLongitude());
    }

    /**
     * 指定座標が地図範囲内にあるか
     *
     * @param coordinate
     *            経緯度(世界測地系)
     * @return true: 範囲内 /false: 範囲外
     */
    public static boolean inBoundMapWGS(GeoPoint coordinate) {
        return inBoundMapWGS(coordinate.getLatitude(), coordinate.getLongitude());
    }

    /**
     * 指定座標が地図範囲内にあるか
     *
     * @param latitude
     *            緯度(世界測地系)
     * @param longitude
     *            経度(世界測地系)
     * @return true: 範囲内 /false: 範囲外
     */
    public static boolean inBoundMapWGS(double latitude, double longitude) {
        return latitude <= 90.0 && latitude >= -90.0 &&
                longitude >= -180.0 && longitude <= 180.0;
    }

    /**
     * 度分秒→ミリ秒変換
     *
     * @param d
     *            度
     * @param m
     *            分
     * @param s
     *            秒
     * @return ミリ秒
     */
    public static long toMillis(int d, int m, int s) {
        return (d * 60 * 60 * 1000) + (m * 60 * 1000) + s * 1000;
    }

    /**
     * 東京測地系座標に変換
     *
     * @param latitude
     *            緯度 (度)
     * @param longitude
     *            経度 (度)
     * @return 座標
     */
    public static GeoPoint toTokyoDatum(double latitude, double longitude) {
        JNITwoLong coordinates = new JNITwoLong();
        long latMillis = GeoPoint.toMilliseconds(latitude);
        long lonMillis = GeoPoint.toMilliseconds(longitude);
        dtmConvByDegree(DATUM_INDEX_TOKYO, CONVERT_FROM_WGS84, lonMillis, latMillis, coordinates);
        return new GeoPoint(coordinates.getM_lLat(), coordinates.getM_lLong());
    }

    /**
     * 東京測地系座標に変換
     *
     * @param location
     *            座標
     * @return 座標
     */
    public static GeoPoint toTokyoDatum(Location location) {
        return toTokyoDatum(location.getLatitude(), location.getLongitude());
    }

    /**
     * 東京測地系座標に変換
     *
     * @param coordinate
     *            座標
     * @return 座標
     */
    public static GeoPoint toTokyoDatum(GeoPoint coordinate) {
        return toTokyoDatum(coordinate.getLatitude(), coordinate.getLongitude());
    }

    /**
     * 世界測地系座標に変換
     *
     * @param latitude
     *            緯度 (ミリ秒)
     * @param longitude
     *            経度 (ミリ秒)
     * @return 座標 (度)
     */
    public static GeoPoint toWGS84(long latitude, long longitude) {
        JNITwoLong coordinates = new JNITwoLong();
        dtmConvByDegree(DATUM_INDEX_TOKYO, CONVERT_TO_WGS84, longitude, latitude, coordinates);
        return new GeoPoint(coordinates.getM_lLat(), coordinates.getM_lLong());
    }

    /**
     * 世界測地系座標に変換
     *
     * @param latitude
     *            緯度 (ミリ秒)
     * @param longitude
     *            経度 (ミリ秒)
     * @return 座標 (Location)
     */
    public static Location toWGS84Location(long latitude, long longitude) {
        GeoPoint coordinates = toWGS84(latitude, longitude);
        Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setLatitude(coordinates.getLatitude());
        location.setLongitude(coordinates.getLongitude());
        return location;
    }

    /**
     * 世界測地系座標に変換
     *
     * @param latitude
     *            緯度 (ミリ秒)
     * @param longitude
     *            経度 (ミリ秒)
     * @return 座標 (Location)
     */
    public static GeoPoint toWGS84Location(JNITwoLong TKYLocate) {
        return toWGS84(TKYLocate.getM_lLat(), TKYLocate.getM_lLong());
    }

    /**
     * 東京測地系座標に変換
     *
     * @param coordinate
     *            座標
     * @return 座標 (ミリ秒)
     */
    public static JNITwoLong toTokyoJniTwoLong(GeoPoint coordinate) {
        GeoPoint out = toTokyoDatum(coordinate);
        JNITwoLong locateJNI = new JNITwoLong();
        locateJNI.setM_lLong(out.getLongitudeMs());
        locateJNI.setM_lLat(out.getLatitudeMs());
        return locateJNI;
    }

    static {
        System.loadLibrary("GeoUtils");
    }

    public static native long dtmConvByDegree(int datumType, int convertType, long lon, long lat, JNITwoLong coor);
}