package net.zmap.android.pnd.v2.inquiry.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
//MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollBox;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.inquiry.view.ButtonImg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 周辺検索メニュー
 *
 * @author XuYang
 *
 */
public class AroundSearchMainMenuActivity extends InquiryBase {
	// 徒歩の場合
	private static String[]manItem;
	private static String[]manItemCode;
	private static String[] manPointCode;
	// 車の場合
    private static String[]carItem;
    private static String[]carItemCode;
    private static String[]carPointCode;

//Add 2011/09/15 Z01_h_yamada Start -->
	private final int[] around_genre_menu_car = new int[]{
			R.string.local_search_menu_car_gas,		//ガソリンスタンド
			R.string.local_search_menu_car_store, 	//コンビニ1
			R.string.local_search_menu_car_ic, 		//IC・SA・PA
			R.string.local_search_menu_car_family,	//ファミレス3
			R.string.local_search_menu_car_bank,	//銀行4
			R.string.local_search_menu_car_cafe,	//カフェ・飲食ﾞ5
			R.string.local_search_menu_car_fev,		//お気に入り6
			R.string.local_search_menu_car_history,	//履歴7
			R.string.local_search_menu_car_etc_poi 	//その他の施設8
    };
	private final int[] around_small_icon_car = new int[]{
	        R.drawable.around_small_icon_gas,
	        R.drawable.around_small_icon_store,
	        R.drawable.around_small_icon_sa,
	        R.drawable.around_small_icon_family_rest,
	        R.drawable.around_small_icon_bank,
	        R.drawable.around_small_icon_fast_food,
	    	R.drawable.favorites_icon_40_40,
	    	R.drawable.history_icon_40_40,
	    	R.drawable.genre_icon_40_40
	};

	private final int[] around_genre_menu_man = new int[]{
			R.string.local_search_menu_man_store,	//コンビニ0
			R.string.local_search_menu_man_fastfood,//ファーストフード1
			R.string.local_search_menu_man_family, 	//ファミレス2
			R.string.local_search_menu_man_bank,	//銀行3
			R.string.local_search_menu_man_super,	//スーパー4
			R.string.local_search_menu_man_hotel,	//宿泊施設5
			R.string.local_search_menu_man_fev,		//お気に入り
			R.string.local_search_menu_man_history,	//履歴
			R.string.local_search_menu_man_etc_poi 	//その他の施設
    };
	private final int[] around_small_icon_man = new int[]{
	        R.drawable.around_small_icon_store,
	        R.drawable.around_small_icon_fast_food,
	        R.drawable.around_small_icon_family_rest,
	        R.drawable.around_small_icon_bank,
	        R.drawable.around_small_icon_super,
	        R.drawable.around_small_icon_hotel,
	    	R.drawable.favorites_icon_40_40,
	    	R.drawable.history_icon_40_40,
	    	R.drawable.genre_icon_40_40
	};

	private final int[]around_menu_id = new int[]{
            R.id.mainmenu_1,
            R.id.mainmenu_2,
            R.id.mainmenu_3,
            R.id.mainmenu_4,
            R.id.mainmenu_5,
            R.id.mainmenu_6,
            R.id.mainmenu_favorites,
            R.id.mainmenu_history,
            R.id.mainmenu_genre,
    };
	private final int[]around_menu_image_id = new int[]{
            R.id.image_mainmenu_1,
            R.id.image_mainmenu_2,
            R.id.image_mainmenu_3,
            R.id.image_mainmenu_4,
            R.id.image_mainmenu_5,
            R.id.image_mainmenu_6,
            R.id.image_mainmenu_favorites,
            R.id.image_mainmenu_history,
            R.id.image_mainmenu_genre,
    };
//Add 2011/09/15 Z01_h_yamada End <--


    LinearLayout oLayout;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 設定ファイルにショートカット名称を取得
		getShortCutNameFromFile();

//Add 2011/11/23 Z01_h_yamada Start -->
	 	JNITwoLong myPosi = new JNITwoLong();
		NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(myPosi);
		CommonLib.setMapCenterInfo(myPosi);
//Add 2011/11/23 Z01_h_yamada End <--

		LayoutInflater oInflater = LayoutInflater.from(this);
	    oLayout = (LinearLayout)oInflater.inflate(R.layout.local_search_main_menu, null);
		ScrollBox oBox = (ScrollBox)oLayout.findViewById(R.id.aroundSearchArea);
		initLayout01();
		ScrollTool oTool = new ScrollTool(this);
		oTool.bindView(oBox);

		setViewInOperArea(oTool);
		setViewInWorkArea(oLayout);
	}
    /**
     * 元の実装が、m_wPaddingLeft + m_wIconW + m_wIconSpace と<br>
     * {ButtonImg} 内、及びコンストラクタでリテラル指定になっていたのでソレに準じる
     */
    private static final int IMAGE_BUTTON_PADDING_LEFT = 10 + 5;
    private static final int IMAGE_BUTTON_PADDING_TOP = 10;
    private static final int IMAGE_BUTTON_PADDING_RIGHT = 10;
    private static final int IMAGE_BUTTON_PADDING_BUTTOM = 30;

	/**
	 * １ページの初期化処理
	 * @param oLayout レイアウト
	 */
	private void initLayout01() {
		int iconSize = getResources().getDimensionPixelSize(R.dimen.SearchMenu_Button_iconsize);
	    // 可変メニュー
		Bitmap bitmap = null, scaled = null;
		Button btn = null;
		ImageView image = null;
		int length = around_menu_id.length;
       	for (int i = 0; i < length; i++) {
       	    btn = (Button) oLayout.findViewById(around_menu_id[i]);

            if(btn != null){
                String indexName = "";
                String middleCode = "";
                String pointCode = "";

            	if(CommonLib.getNaviMode() == Constants.NE_NAVIMODE_BICYCLE
            		|| CommonLib.getNaviMode() == Constants.NE_NAVIMODE_MAN){
                    // 徒歩モード、アローモードの場合
            		if (i < manItem.length) {
                  		indexName = manItem[i];
                		middleCode = manItemCode[i];
                		pointCode = manPointCode[i];
              		} else {
 					    indexName = "";
                        middleCode = "";
					    pointCode = "";
              		}
                    btn.setText(getResources().getString(around_genre_menu_man[i]));
                   	btn.setPadding(IMAGE_BUTTON_PADDING_LEFT + iconSize
                			,IMAGE_BUTTON_PADDING_TOP
                			,IMAGE_BUTTON_PADDING_RIGHT
                			,IMAGE_BUTTON_PADDING_BUTTOM);
                   	bitmap =  BitmapFactory.decodeResource(getResources(),around_small_icon_man[i]);

            	} else {
                    // 車モード
            		if (i < carItem.length) {
	            		indexName = carItem[i];
	            		middleCode = carItemCode[i];
	            		pointCode = carPointCode[i];
            		} else {
                        indexName = "";
                        middleCode = "";
                        pointCode = "";
            		}
           			btn.setText(getResources().getString(around_genre_menu_car[i]));
                   	btn.setPadding(IMAGE_BUTTON_PADDING_LEFT + iconSize
                			,IMAGE_BUTTON_PADDING_TOP
                			,IMAGE_BUTTON_PADDING_RIGHT
                			,IMAGE_BUTTON_PADDING_BUTTOM);
                   	bitmap =  BitmapFactory.decodeResource(getResources(),around_small_icon_car[i]);
            	}
            	image = (ImageView)oLayout.findViewById(around_menu_image_id[i]);
            	scaled = Bitmap.createScaledBitmap(bitmap, iconSize, iconSize, true);
            	image.setImageBitmap(scaled);

            	final int resId = around_menu_id[i];
            	final String genreName = indexName;
            	final String genreCode = middleCode;
            	final String pointKey = pointCode;
                btn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
						clickItem(resId, genreName, genreCode, pointKey);
                    }
                });
            }
        }
	}

//Chg 2011/09/15 Z01_h_yamada End <--

	private void clickItem(int resId, String indexName, String genreCode, String pointCode) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 周辺検索画面 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 周辺検索画面 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
		switch(resId) {
		case R.id.mainmenu_favorites:
			// お気に入り
			goFavoritesGenreSearch();
			break;
		case R.id.mainmenu_history:
			// 履歴
			goHistorySearch();
			break;
		case R.id.mainmenu_genre:
			// その他の施設
			goOtherGenreSearch();
			break;
		default:
			// ショートカットの場合
			CommonLib.setShortcutName(indexName);
			CommonLib.setStrShortcutCode(genreCode);
			if (!AppInfo.isEmpty(pointCode)) {
				CommonLib.setStrShortPointCode(pointCode);
			}
			goGenreSearch();
			break;

		}
	}

	/**
	 * 周辺のお気にいり画面に遷移する
	 */
	private void goFavoritesGenreSearch() {
		Intent oIntent = new Intent(getIntent());
		oIntent.setClass(this, AroundFavoritesGenreInquiry.class);
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
		NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_AROUND_FAVORITES_GENRE);
	}
	/**
	 * 周辺の履歴画面に遷移する
	 */
	private void goHistorySearch() {
		Intent oIntent = new Intent(getIntent());
	    oIntent.setClass(this, HistoryInquiryAround.class);
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_HISTORYINQUIRY_AROUND);

	}

	/**
	 * 周辺のその他の施設 K-J0_ジャンル大分類画面に遷移する
	 */
	private void goOtherGenreSearch() {
		Intent oIntent = new Intent(getIntent());
		oIntent.setClass(this, OtherAroundGenreInquiry.class);
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_LOCAL_OTHER);
	}

	/**
	 * 周辺のK-J5_ジャンル検索結果画面に遷移する
	 */
	private void goGenreSearch() {
		Intent oIntent = new Intent(getIntent());
	    oIntent.setClass(this, AroundGenreInquiry.class);

        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_GENREINQUIRY);
	}

	/**
	 * 設定ファイルにショートカットの名称（車と徒歩モード）を取得
	 */
	private void getShortCutNameFromFile(){
    	if(manItem != null && carItem != null){
    		return;
    	}
    	InputStream iStream = null;
    	BufferedReader br = null;
    	try {
            iStream = this.getClass().getResourceAsStream(Constants.AROUND_CONFIG_PATH);
            if(iStream != null){
            	br = new BufferedReader(new InputStreamReader(iStream));
            	while(true){
            		String line = br.readLine();

            		if(line == null){
            			break;
            		}
            		// 車のショートカット名称を取得
            		if(Constants.AROUND_CONFIG_TAG_CAR.equalsIgnoreCase(line)){
            			NaviLog.d(NaviLog.PRINT_LOG_TAG,"111111111");
            			StringBuffer buffer = new StringBuffer();
            			StringBuffer buffer_Middle = new StringBuffer();
            			//yangyang add start QA49 20110422
            			StringBuffer buffer_Point = new StringBuffer();
            			//yangyang add end QA49 20110422
    					line = br.readLine();
            			while(line!= null && line.startsWith(Constants.AROUND_CONFIG_CONTENTS_KEY)){

            				String[] s = line.split(Constants.AROUND_CONFIG_SPLIT_VALUE);
            				if(s != null && s.length > 2){
            					buffer.append(s[2]);
            					buffer.append(Constants.AROUND_CONFIG_SPLIT_VALUE);
            				}
            				if(s != null && s.length > 5){
            					buffer_Middle.append(s[5]);
            					buffer_Middle.append(Constants.AROUND_CONFIG_SPLIT_VALUE);
            				}
            				//yangyang add start QA49 20110422
            				if(s != null && s.length > 8){
            					buffer_Point.append(s[8]);
            					buffer_Point.append(Constants.AROUND_CONFIG_SPLIT_VALUE);
            				}
            				//yangyang add end QA49 20110422
            				line = br.readLine();
            			}
            			carItem = buffer.toString().trim().split(Constants.AROUND_CONFIG_SPLIT_VALUE);
            			carItemCode = buffer_Middle.toString().trim().split(Constants.AROUND_CONFIG_SPLIT_VALUE);
            			//yangyang add start QA49 20110422
            			carPointCode = buffer_Point.toString().trim().split(Constants.AROUND_CONFIG_SPLIT_VALUE);
            			//yangyang add end QA49 20110422
            			buffer_Middle = null;
            			buffer = null;
            		}
            		// 徒歩のショートカット名称を取得
            		if(Constants.AROUND_CONFIG_TAG_MAN.equalsIgnoreCase(line)){
            			StringBuffer buffer = new StringBuffer();
            			StringBuffer buffer_key = new StringBuffer();
            			//yangyang add start QA49 20110422
            			StringBuffer buffer_Point = new StringBuffer();
            			//yangyang add end QA49 20110422
            			line = br.readLine();
            			while(line!= null && line.startsWith(Constants.AROUND_CONFIG_CONTENTS_KEY)){
            				String[] s = line.split(Constants.AROUND_CONFIG_SPLIT_VALUE);
            				if(s != null && s.length > 2){
            					buffer.append(s[2]);
            					buffer.append(Constants.AROUND_CONFIG_SPLIT_VALUE);
            				}
            				if(s != null && s.length > 5){
            					buffer_key.append(s[5]);
            					buffer_key.append(Constants.AROUND_CONFIG_SPLIT_VALUE);
            				}
            				//yangyang add start QA49 20110422
            				if(s != null && s.length > 8){
            					buffer_Point.append(s[8]);
            					buffer_Point.append(Constants.AROUND_CONFIG_SPLIT_VALUE);
            				}
            				//yangyang add end QA49 20110422
            				line = br.readLine();
            			}
            			manItem = buffer.toString().trim().split(Constants.AROUND_CONFIG_SPLIT_VALUE);
            			manItemCode = buffer_key.toString().trim().split(Constants.AROUND_CONFIG_SPLIT_VALUE);
            			//yangyang add start QA49 20110422
            			manPointCode = buffer_Point.toString().trim().split(Constants.AROUND_CONFIG_SPLIT_VALUE);
            			//yangyang add end QA49 20110422
            			buffer_key = null;
            			buffer = null;
            		}
            	}
            }

        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }
    	finally {
            try {
                if (br != null) {
            	br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (iStream != null) {
            	iStream.close();
            }

        } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
