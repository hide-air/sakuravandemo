package net.zmap.android.pnd.v2.common.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

//Add 2012/03/23 Z01t_nakajima(No.414) Start --> ref 4146
//Add 2012/03/23 Z01t_nakajima(No.414) End <-- ref 4146
import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.common.view.WaitDialog;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.maps.MapView;
import net.zmap.android.pnd.v2.maps.OpenMap;

public class NaviMode extends MenuBaseActivity implements OnClickListener {

    private static final int DIALOG_CHANGE_NAVI_MODE = 0x1;
    private static final int DIALOG_WALK_ROUTE_TOO_LONG = 0x0;
    //hangeng add start bug1773
    private static final int DIALOG_ROUTE_FAILURE_MANMODE = 0x9;
    private MapView mapView;
	private boolean isManMode = false;
	//hangeng add end bug1773
//Add 2012/03/23 Z01t_nakajima(No.414) Start --> ref 4146
    private WaitDialog wDialog;
//Add 2012/03/23 Z01t_nakajima(No.414) End <-- ref 4146
    private Button naviCar;
    private Button naviWalk;
    private Button naviBike;
    private int newNaviMode;

//Add 2012/04/12 Z01hirama Start --> #4223
    private boolean mDoEndBicycleNavi = false;
//Add 2012/04/12 Z01hirama End <-- #4223


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//Del 2011/09/17 Z01_h_yamada Start -->
//        setMenuTitle(R.drawable.navi_mode_change);
//Del 2011/09/17 Z01_h_yamada End <--

        LayoutInflater inflater = LayoutInflater.from(this);
        LinearLayout layout = (LinearLayout)inflater.inflate(R.layout.navi_mode_change, null);

        naviCar = (Button)layout.findViewById(R.id.navi_car);
        naviWalk = (Button)layout.findViewById(R.id.navi_walk);
        naviBike = (Button)layout.findViewById(R.id.navi_bike);
//Add 2011/08/31 Z01yoneya Start -->
        //歩行者データが無い時は、歩行者ボタンを表示しない
//Chg 2011/09/26 Z01yoneya Start -->
//        if (!CommonLib.isBIsExistsWalkerData()) {
//------------------------------------------------------------------------------------------------------------------------------------
        boolean isExistWalkData;
        try {
            isExistWalkData = ((NaviApplication)getApplication()).getNaviAppDataPath().isExistWalkData();
        } catch (NullPointerException e) {
            isExistWalkData = false;
        }
        if (!isExistWalkData) {
//Chg 2011/09/26 Z01yoneya End <--
            if (naviWalk != null) {
                naviWalk.setVisibility(View.GONE);
                naviWalk = null;
            }
        }
//Add 2011/08/31 Z01yoneya End <--
        updateView();
        setViewInWorkArea(layout);
    }

    @Override
    public void onClick(View v) {
        if (v.isSelected()) {
            return;
        }

        if (v == naviCar) {
            newNaviMode = Constants.NE_NAVIMODE_CAR;
        }
        if (v == naviWalk) {
            newNaviMode = Constants.NE_NAVIMODE_MAN;
        }
        if (v == naviBike) {
            newNaviMode = Constants.NE_NAVIMODE_BICYCLE;
        }

        if (CommonLib.isNaviStart()) {
            showDialog(DIALOG_CHANGE_NAVI_MODE);
        } else {
            chageNaviMode();
            updateView();
//Del 2011/09/09 Z01_h_yamada Start -->
//            updateNaviStateIcon();
//Del 2011/09/09 Z01_h_yamada End <--
        }
    }

    private void updateView() {
        int naviMode = CommonLib.getNaviMode();

        if (naviCar != null) {
            naviCar.setSelected(naviMode == Constants.NE_NAVIMODE_CAR);
            naviCar.setClickable(!naviCar.isSelected());
            naviCar.setOnClickListener(this);
        }
        if (naviWalk != null) {
            naviWalk.setSelected(naviMode == Constants.NE_NAVIMODE_MAN);
            naviWalk.setClickable(!naviWalk.isSelected());
            naviWalk.setOnClickListener(this);
        }
        if (naviBike != null) {
            naviBike.setSelected(naviMode == Constants.NE_NAVIMODE_BICYCLE);
            naviBike.setClickable(!naviBike.isSelected());
            naviBike.setOnClickListener(this);
        }
    }

    private void onReRoute() {
        int naviMode = CommonLib.getNaviMode();
        //hangeng add start bug1773
    	NaviRun.setManMode(isManMode);
    	NaviRun.setNaviModeEventHandler(this);
    	//hangeng add end bug1773
        if (newNaviMode == Constants.NE_NAVIMODE_MAN) {
//Chg 2011/09/26 Z01yoneya Start -->
//            if(CommonLib.isBIsExistsWalkerData()){
//------------------------------------------------------------------------------------------------------------------------------------
            boolean isExistWalkData;
            try {
                isExistWalkData = ((NaviApplication)getApplication()).getNaviAppDataPath().isExistWalkData();
            } catch (NullPointerException e) {
                isExistWalkData = false;
            }
            if (isExistWalkData) {
//Chg 2011/09/26 Z01yoneya End <--
                long routeDist = CommonLib.calcRouteDistance();
                if (routeDist > Constants.MAX_WALK_ROUTE_DIST) {
                    showDialog(DIALOG_WALK_ROUTE_TOO_LONG);
                    return;
                }
            } else {
                showDialog(Constants.DIALOG_NO_WALKER_DATA);
                return;
            }
        }
//Add 2012/03/23 Z01t_nakajima(No.414) Start --> ref 4146
// Chg -- 2012/04/09 -- Z01H.Fukushima --- Start ----- refs 3818
//      // 歩行者モードに切り替えるときだけクルクルを出す
//    if(newNaviMode == Constants.NE_NAVIMODE_MAN){
		// 車・歩行者モードに切り替えるときクルクルを出す
		if(newNaviMode != Constants.NE_NAVIMODE_BICYCLE){
//Chg -- 2012/04/09 -- Z01H.Fukushima --- End ----- refs 3818
            showRouteSearchWaitingDialog();
        }
//Add 2012/03/23 Z01t_nakajima(No.414) End <-- ref 4146
        if (naviMode == Constants.NE_NAVIMODE_CAR
                || (naviMode == Constants.NE_NAVIMODE_MAN && newNaviMode == Constants.NE_NAVIMODE_BICYCLE)) {
            //CommonLib.guideEnd(false);
            CommonLib.setBIsRouteDisplay(true);
            NaviRun.GetNaviRunObj().JNI_Java_ct_ChangeNaviMode(newNaviMode);
            NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();

            Intent oIntent = getIntent();
            oIntent.putExtra(Constants.PARAMS_IS_CHANGE_NAVIMODE, true);

        } else {
            CommonLib.setBIsRouteDisplay(true);
            //yangyang add start Bug1080
            NaviRun.GetNaviRunObj().JNI_Java_ct_ChangeNaviMode(newNaviMode);
            NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
            //yangyang add end Bug1080
            Intent intent = new Intent();
            intent.putExtra(Constants.FLAG_NAVIMODE, newNaviMode);
            //setResult(Constants.RESULT_CHANGE_NAVI_MODE_MANUAL, intent);
            //finish();
        }
        updateView();
//Del 2011/09/09 Z01_h_yamada Start -->
//        updateNaviStateIcon();
        //Del 2011/09/09 Z01_h_yamada End <--
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        final CustomDialog oDialog = new CustomDialog(this);
        boolean hasDialog = true;
        Resources oRes = getResources();

        switch (id) {
            case DIALOG_CHANGE_NAVI_MODE:
                oDialog.setTitle(R.string.txt_navi_mode_change_title);
                oDialog.setMessage(getMessage());
                oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {
                    @Override
                    public void onClick(View v) {
//Chg 2012/03/17 Z01_h_yamada(No.401) Start --> ref 3818
//                		//hangeng add start bug1773
//						if (newNaviMode == Constants.NE_NAVIMODE_MAN) {
//							isManMode = true;
//						}
//						//hangeng add end bug1773
//                        if (!CommonLib.bIsModeChanging) {
//                            removeDialog(DIALOG_CHANGE_NAVI_MODE);
//                            synchronized (NaviRun.GetNaviRunObj()) {
//                                NaviRun.setRouteCalculateFinished(false);
//                            }
//                            onReRoute();
//                            CommonLib.bIsModeChanging = true;
//                        }
//--------------------------------------------
                        removeDialog(DIALOG_CHANGE_NAVI_MODE);

                        synchronized (NaviRun.GetNaviRunObj()) {
                        	if (!CommonLib.bIsModeChanging) {
          						if (newNaviMode == Constants.NE_NAVIMODE_MAN) {
        							isManMode = true;
        						}
	                            NaviRun.setRouteCalculateFinished(false);
	                            onReRoute();
	                            CommonLib.bIsModeChanging = true;
                        	}
                        }
//Chg 2012/03/17 Z01_h_yamada(No.401) End <-- ref 3818
                     }
                });
                oDialog.addCancelButton(R.string.btn_cancel);
                break;
            case DIALOG_WALK_ROUTE_TOO_LONG:
                oDialog.setTitle(R.string.walk_search_failed_title);
                oDialog.setMessage(R.string.walk_search_failed);
                oDialog.addButton(R.string.btn_ok,
                        new OnClickListener() {
                            public void onClick(View v) {
                                oDialog.dismiss();
                                removeDialog(DIALOG_WALK_ROUTE_TOO_LONG);
                                routeCalc();
                                //CommonLib.setNaviMode(CommonLib.getNaviMode());
                                //goMap();
                            }
                        });
//Chg 2012/04/26 Z01hirama Start --> #4417
//                oDialog.addButton(R.string.btn_cancel,
//                        new OnClickListener() {
//                            public void onClick(View v) {
//                                removeDialog(DIALOG_WALK_ROUTE_TOO_LONG);
//                                CommonLib.setNaviMode(CommonLib.getNaviMode());
////Add 2012/04/13 Z01hirama Start --> #4266
//                                CommonLib.bIsModeChanging = false;
////Add 2012/04/13 Z01hirama End <-- #4266
//                                //Redmine 1730 modify start
//                                //goMap();
//                                //Redmine 1730 modify end
//                            }
//                        });
//------------------------------------------------------------------------------------------------------------------------------------
                oDialog.addButton(R.string.btn_cancel, new WalkRootTooLongDialogCancelListnere());
                oDialog.setOnCancelListener(new WalkRootTooLongDialogCancelListnere());
//Chg 2012/04/26 Z01hirama End <-- #4417
                break;
            case Constants.DIALOG_NO_WALKER_DATA:
                oDialog.setTitle(R.string.dialog_no_walkerdata_title);
                oDialog.setMessage(R.string.dialog_no_walkerdata);
                oDialog.addButton(R.string.btn_ok, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        removeDialog(Constants.DIALOG_NO_WALKER_DATA);
                    }
                });
                break;
            // XuYang add start 走行中の操作制限
            case Constants.DIALOG_CHANGE_MODE_CAR_MENU:
                oDialog.setTitle(R.string.txt_navi_mode_change_title);
                oDialog.setMessage(R.string.mode_change_to_car_msg);
                oDialog.addButton(R.string.btn_ok,
                        new OnClickListener() {
                            public void onClick(View v) {
                                oDialog.dismiss();
                                removeDialog(Constants.DIALOG_CHANGE_MODE_CAR_MENU);
                                // 案内中であるかどうかを判断する。
                                if (CommonLib.isNaviStart()) {
                                    CommonLib.setBIsRouteDisplay(true);
                                    NaviRun.GetNaviRunObj().JNI_Java_ct_ChangeNaviMode(Constants.NE_NAVIMODE_CAR);
                                    NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
                                    goMap();
                                } else {
                                    CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
                                    // モード状態を更新
                                    updateView();
//Del 2011/09/09 Z01_h_yamada Start -->
//                                updateNaviStateIcon();
//Del 2011/09/09 Z01_h_yamada End <--
                                    systemLimit();
                                }
                            }
                        });
                break;
        	//hangeng add start bug1773
          	case DIALOG_ROUTE_FAILURE_MANMODE:
          		NaviLog.d(NaviLog.PRINT_LOG_TAG,"come DIALOG_ROUTE_FAILURE_MANMODE");
          		oDialog.setTitle(R.string.route_failure_title);
          		oDialog.setMessage(R.string.route_failure_message);

//Chg 2012/04/26 Z01hirama Start --> #4417
//	          	oDialog.addButton(R.string.btn_ok,
//	            	new OnClickListener() {
//	                	public void onClick(View v) {
//	                    	mapView = MapView.getInstance();
//	                      	CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
//	                        updateView();
//
//	                        if(0 == NaviRun.GetNaviRunObj().JNI_ct_uic_ShowRestoreRoute()){
//	                        	mapView.onNaviModeChange();
//	                        	CommonLib.setIsCalcFromRouteEdit(false);
//	                        }else{
//	                        	CommonLib.setNaviStart(false);
//	                        	CommonLib.setHasRoute(false);
//	                        	CommonLib.setBIsRouteDisplay(false);
//	                        	if(true == CommonLib.getIsCalcFromRouteEdit()){
//	                          		CommonLib.setIsCalcFromRouteEdit(false);
//	                          		NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);
//	                                //showLocusMap();
//	                                NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
//	                                mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
//	                          	}
//	                        }
//	                        NaviRun.GetNaviRunObj().JNI_NE_MP_SetOffRoute(0);
//	                        NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
////Add 2012/04/12 Z01hirama Start --> #4223
//                            if(mDoEndBicycleNavi) {
//                            	// このケースでは車にモード変更済みの為、画面位置のみ更新する.
//                            	MapActivity mapActivity = NaviRun.getMapActivity();
//                                if (mapView.isMapScrolled()) {
//                                    OpenMap.locationBack(mapActivity);
//                                }
//                            	mDoEndBicycleNavi = false;
//                            }
////Add 2012/04/12 Z01hirama End <-- #4223
//	                        oDialog.dismiss();
//	                    }
//	                });
//------------------------------------------------------------------------------------------------------------------------------------
	          	oDialog.addButton(R.string.btn_ok, new RouteFailureDialogCancelListener(oDialog));
	          	oDialog.setOnCancelListener(new RouteFailureDialogCancelListener(oDialog));
//Chg 2012/04/26 Z01hirama End <-- #4417

	          		break;
          	//hangeng add end bug1773
            // XuYang add end 走行中の操作制限
            default:
                hasDialog = false;
                break;
        }
        return hasDialog ? oDialog : super.onCreateDialog(id);
    }

    private String getMessage() {
        Resources oRes = getResources();
        StringBuilder sb = new StringBuilder();

        //変更前モードで案内中
        int str1 = R.string.txt_navi_mode_change_1;
        int str2 = R.string.txt_navi_mode_change_2_def;
        int str3 = R.string.txt_navi_mode_change_end;

        int naviMode = CommonLib.getNaviMode();

        if (newNaviMode == Constants.NE_NAVIMODE_BICYCLE) {
            str2 = R.string.txt_navi_mode_change_1_car_bike;
        }

        sb.append(oRes.getString(str1));
        sb.append("\n");
        sb.append(oRes.getString(str2));
        sb.append("\n");
        sb.append(oRes.getString(str3));

        // 車載取付時メッセージ
        if ((naviMode == Constants.NE_NAVIMODE_MAN || naviMode == Constants.NE_NAVIMODE_BICYCLE)
                && (newNaviMode == Constants.NE_NAVIMODE_CAR)) {
//            msg = R.string.txt_navi_mode_change_ex_to_car;
        }
        return sb.toString();
    }

    private void chageNaviMode() {
        CommonLib.setNaviMode(newNaviMode);
    }

    private void routeCalc() {
        JNIInt GuideState = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_Java_GetGuideState(GuideState);
        if (GuideState.getM_iCommon() != 0) {
            NaviRun.GetNaviRunObj().JNI_NE_StopGuidance();
        }
        CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
        int NE_NEActType_RouteCalculate = 5;
        int NE_NETransType_NormalMap = 0;
        byte bScale = 0;
        long lLongitude = 0;
        long lLatitude = 0;
        NaviRun.GetNaviRunObj().JNI_NE_ActivateControl(
                NE_NEActType_RouteCalculate, NE_NETransType_NormalMap, bScale,0,
                lLongitude, lLatitude,0);
        NaviRun.GetNaviRunObj().JNI_CT_ExposeMap();
        NaviRun.GetNaviRunObj().JNI_ct_ExposeMap();
        CommonLib.setIsStartCalculateRoute(true);
        this.setResult(Constants.RESULTCODE_ROUTE_DETERMINATION);
        this.finish();
    }
	//hangeng add start bug1773
	public void showSearchRouteFailureDialog() {
		showDialog(DIALOG_ROUTE_FAILURE_MANMODE);
		CommonLib.set5RouteMode(false);
//Add 2012/04/10 Z01hirama Start --> #4223
        if(MapView.getInstance().getMap_view_flag() == Constants.ROUTE_MAP_VIEW_BIKE)
        {
        	mDoEndBicycleNavi = true;
        }
//Add 2012/04/12 Z01hirama End <-- #4223
	}
	//hangeng add start bug1773

//Add 2012/03/23 Z01t_nakajima(No.414) Start --> ref 4146
    private void showRouteSearchWaitingDialog() {
        wDialog = new WaitDialog(this);
        wDialog.setShowProGressBar(true);
        wDialog.setCancelable(false);
        wDialog.show();
    }

    public void removeRouteSearchWaitingDialog() {
         wDialog.dismiss();
    }
//Add 2012/03/23 Z01t_nakajima(No.414) End <-- ref 4146
//Add 2012/04/26 Z01hirama Start --> #4417
    /**
     * 徒歩ルート距離オーバーダイアログキャンセルリスナー
     */
    private class WalkRootTooLongDialogCancelListnere implements OnClickListener,
    															 OnCancelListener
	 {
		@Override
		public void onCancel(DialogInterface dialog) {
			BeforeCloseDialogProc();
		}

		@Override
		public void onClick(View v) {
            removeDialog(DIALOG_WALK_ROUTE_TOO_LONG);
			BeforeCloseDialogProc();
		}


		private void BeforeCloseDialogProc() {
            CommonLib.setNaviMode(CommonLib.getNaviMode());
//Add 2012/04/13 Z01hirama Start --> #4266
            CommonLib.bIsModeChanging = false;
//Add 2012/04/13 Z01hirama End <-- #4266
            //Redmine 1730 modify start
            //goMap();
            //Redmine 1730 modify end
		}
	 }


    /**
     * ルート探索失敗ダイアログのリスナークラス
     */
    private class RouteFailureDialogCancelListener implements OnClickListener,
    														  OnCancelListener
    {
		CustomDialog mDialog = null;

		public RouteFailureDialogCancelListener(CustomDialog dialog) {
			mDialog = dialog;
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			BeforeCloseDialogProc();
		}

		@Override
		public void onClick(View v) {
			BeforeCloseDialogProc();
            mDialog.dismiss();
		}

    	private void BeforeCloseDialogProc() {
        	mapView = MapView.getInstance();
          	CommonLib.setNaviMode(Constants.NE_NAVIMODE_CAR);
            updateView();

            if(0 == NaviRun.GetNaviRunObj().JNI_ct_uic_ShowRestoreRoute()){
            	mapView.onNaviModeChange();
            	CommonLib.setIsCalcFromRouteEdit(false);
            }else{
            	CommonLib.setNaviStart(false);
            	CommonLib.setHasRoute(false);
            	CommonLib.setBIsRouteDisplay(false);
            	if(true == CommonLib.getIsCalcFromRouteEdit()){
              		CommonLib.setIsCalcFromRouteEdit(false);
              		NaviRun.GetNaviRunObj().JNI_NE_MP_SetDirectLineIsDisplayed(0, 0);
                    //showLocusMap();
                    NaviRun.GetNaviRunObj().JNI_ct_uic_MoveMapMyPosi();
                    mapView.setMap_view_flag(Constants.COMMON_MAP_VIEW);
              	}
            }
            NaviRun.GetNaviRunObj().JNI_NE_MP_SetOffRoute(0);
            NaviRun.GetNaviRunObj().JNI_CT_ReqRedraw();
//Add 2012/04/12 Z01hirama Start --> #4223
            if(mDoEndBicycleNavi) {
            	// このケースでは車にモード変更済みの為、画面位置のみ更新する.
            	MapActivity mapActivity = NaviRun.getMapActivity();
                if (mapView.isMapScrolled()) {
                    OpenMap.locationBack(mapActivity);
                }
            	mDoEndBicycleNavi = false;
            }
//Add 2012/04/12 Z01hirama End <-- #4223
    	}
    }
//Add 2012/04/26 Z01hirama End <-- #4417
}
