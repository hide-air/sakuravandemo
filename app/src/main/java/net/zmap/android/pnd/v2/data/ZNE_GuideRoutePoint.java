/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNE_RouteInfo_t.java
 * Description    ルート情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNE_GuideRoutePoint
{
    private ZNE_RoutePoint[]    stGuidePoint = new ZNE_RoutePoint[8] ;
    private int         iSearchType ;       // 検索条件
    private int         iCarManMode ;       // 人車モード
    /**
     * <p>Description:[...]</p>
     * @param iSearchType The iSearchType to set.
     */
    public void setISearchType(int iSearchType) {
        this.iSearchType = iSearchType;
    }
    /**
     * <p>Description:[...]</p>
     * @return int iSearchType.
     */
    public int getISearchType() {
        return iSearchType;
    }
    /**
     * <p>Description:[...]</p>
     * @param iCarManMode The iCarManMode to set.
     */
    public void setICarManMode(int iCarManMode) {
        this.iCarManMode = iCarManMode;
    }
    /**
     * <p>Description:[...]</p>
     * @return int iCarManMode.
     */
    public int getICarManMode() {
        return iCarManMode;
    }
    /**
     * <p>Description:[...]</p>
     * @param stGuidePoint The stGuidePoint to set.
     */
    public void setStGuidePoint(ZNE_RoutePoint[] stGuidePoint) {
        this.stGuidePoint = stGuidePoint;
    }
    /**
     * <p>Description:[...]</p>
     * @return ZNE_RoutePoint[] stGuidePoint.
     */
    public ZNE_RoutePoint[] getStGuidePoint() {
        return stGuidePoint;
    }
}
