package net.zmap.android.pnd.v2.common;

/**
 *  @file       LoggerManager.java
 *  @brief
 *
 *  @attention
 *  @note
 *
 *  @author     Yuki Sawada [Z01]
 *  @date       $Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2010-10-13)
 *  @version    $Revision: $ by $Author: $
 */

import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;

import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.data.AccSensorData;
import net.zmap.android.pnd.v2.data.GyroSensorData;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.OriSensorData;
import net.zmap.android.pnd.v2.data.ZVP_SatInfoList_t;
import net.zmap.android.pnd.v2.data.ZVP_SatInfo_t;
import net.zmap.android.pnd.v2.gps.MyLocationListener;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.sensor.RawSensorEvent;
import net.zmap.android.pnd.v2.sensor.SensorDataManager;
import net.zmap.android.pnd.v2.app.NaviAppDataPath;			// V2.5 ログパス

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;


public class LoggerManager implements Runnable {
// Chg 2011/03/25 Z01ONOZAWA Start -->
//    private static final String LOG_BASEDIR =  "/sdcard/zdcpnd/log/";
    private static String LOG_BASEDIR = null;
    private static String LOG_BASEWRITEDIR = null;			/* V2.5 log書込みパス */
    private static String LOG_BASEREADDIR = null;			/* V2.5 log読込みパス */

// Chg 2011/03/25 Z01ONOZAWA End   <--
    private static final String LOG_FILENAME = "Logger.txt";
    private static final String LOG_FILENAME_DATED = "Logger_%04d%02d%02d_%02d%02d%02d.txt";
    private static final String LOG_WRITEFILENAME_DATED = "DRLog_%04d_%02d%02d%02d%02d%02dn.txt";
//Chg 2011/08/10 Z01yoneya Start -->
//    private static final int LOG_VALUES_CNT = 78;
//--------------------------------------------
    private static final int LOG_VALUES_CNT = 82;
//Chg 2011/08/10 Z01yoneya End <--
    private static String[] mLogValues = new String[LOG_VALUES_CNT];

    private static LoggerManager mSingleton = new LoggerManager();
    private static Thread mThread;
// Chg 2011/03/30 Z01ONOZAWA Start --> ログファイルが存在しない場合、何もしない
//    private static FileReader mReader;
    private static FileReader mReader = null;
// Chg 2011/03/30 Z01ONOZAWA End   <--
    private static BufferedReader mBufReader;
    private static FileWriter mWriter;
    private static BufferedWriter mBufWriter;

    private static MyLocationListener mLocationListener = new MyLocationListener();
    private static SensorDataManager mSensorData = new SensorDataManager();
    private static ZVP_SatInfoList_t mSatInfoList = new ZVP_SatInfoList_t();
    private static JNITwoLong mLatLon = new JNITwoLong();
    private static JNIInt mLoadMode = new JNIInt();

//Add 2011/11/02 Z01_h_yamada Start -->
    private static boolean mIsLoggerStart = false;
//Add 2011/11/02 Z01_h_yamada End <--

    private static long mPrevMillis = 0;

//Add 2011/10/20 Z01YUMISAKI Start -->
    private static final String FIELD_STRING_INVALID = new String("INVALID");
//Add 2011/10/20 Z01YUMISAKI End <--

//Add 2011/04/12 Z01yoneya Start-->
    //Logger.txtのバージョンヘッダ情報
//Chg 2011/08/10 Z01yoneya Start -->GPS精度など追加のためver2へアップデート
//    private static final int WRITE_FILE_VERSION= 1;	//出力のバージョン
//--------------------------------------------
    private static final int WRITE_FILE_VERSION = 2; //出力のバージョン
//Chg 2011/08/10 Z01yoneya End <--
    private static final String FILE_VERSION_STRING = "VP_LOGGER_VER";
    private static int LOAD_LOG_FILE_VERSION = 0; //読み込みファイルのバージョン。ヘッダがない場合ver0
//Add 2011/04/12 Z01yoneya End <--

    private LoggerManager() {
    }

//Chg 2011/09/26 Z01yoneya Start -->
//    public static void init(Context context) {
//        mSingleton = new LoggerManager();
//
//// Add 2011/03/25 Z01ONOZAWA Start --> ログパス名を取得
////		String []naviResFilePath = new String[10];
////		for(int i =0; i < 10; i++)
////		{
////			naviResFilePath[i] = new String();
////		}
//        String[] naviResFilePath = CommonLib.getNaviResFilePath();
//
//        String[] logPath = naviResFilePath[6].split(" ");// LOG_PATH
//        LOG_BASEDIR = logPath[2];
//// Add 2011/03/25 Z01ONOZAWA End   <--
//    }
//------------------------------------------------------------------------------------------------------------------------------------
    public static void init(String logPath) {
        LOG_BASEDIR = logPath;
// Add 2011/03/25 Z01ONOZAWA End   <--
    }

//Chg 2011/09/26 Z01yoneya End <--

    public static int openLogFile() {

        try {
//            String fileName = LOG_BASEDIR + LOG_FILENAME;
//            File file = new File(fileName);
//
//            if(!file.exists()){
//                file.createNewFile();
//            }
// Chg 2011/03/30 Z01ONOZAWA Start --> ログファイルが存在しない場合、何もしない
//            File file = CommonLib.openFile(LOG_BASEDIR, LOG_FILENAME);
            File file = new File(LOG_BASEDIR + LOG_FILENAME);
// Chg 2011/03/30 Z01ONOZAWA End   <--

            mReader = new FileReader(file);
            mBufReader = new BufferedReader(mReader, Constants._8K_CHAR_BUFFER_SIZE);
//Add 2011/04/12 Z01yoneya Start-->
            if (mBufReader != null) {
                String line = mBufReader.readLine();
                if (line != null) {
                    if (line.startsWith(FILE_VERSION_STRING)) {
//Chg 2011/08/10 Z01yoneya Start -->
//						LOAD_LOG_FILE_VERSION = 1;
//--------------------------------------------
                        LOAD_LOG_FILE_VERSION = Integer.valueOf(line.split("=")[1]);
//Chg 2011/08/10 Z01yoneya End <--

                    } else {
                        //１行読み捨てる
                    }
                }
            }
//Add 2011/04/12 Z01yoneya End <--

// Add 2011/03/30 Z01ONOZAWA Start --> ログファイルが存在しない場合、何もしない
        } catch (FileNotFoundException e) {
            mReader = null;
// Add 2011/03/30 Z01ONOZAWA End   <--
        } catch (IOException e) {
            return Constants.VP_FAILURE;
        }
        return Constants.VP_SUCCESS;
    }

    public static void closeLogFile() {
    	NaviLog.d(NaviLog.PRINT_LOG_TAG, "LoggerManager::closeLogFile");
        try {
            if (mBufReader != null) {
                mBufReader.close();
//Add 2011/11/17 Z01_h_yamada Start -->
                mBufReader = null;
//Add 2011/11/17 Z01_h_yamada End <--
            }
            if (mReader != null) {
                mReader.close();
//Add 2011/11/17 Z01_h_yamada Start -->
                mReader = null;
//Add 2011/11/17 Z01_h_yamada End <--
            }
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }
    }

    public static void start() {
// Add 2011/03/30 Z01ONOZAWA Start --> ログファイルが存在しない場合、何もしない
        if (mReader == null) return;
// Add 2011/03/30 Z01ONOZAWA End   <--
//Add 2011/11/02 Z01_h_yamada Start -->
        mIsLoggerStart = true;
//Add 2011/11/02 Z01_h_yamada End <--
        mThread = new Thread(mSingleton);
        mThread.start();
    }

    public static void stop() {
//Add 2011/11/02 Z01_h_yamada Start -->
    	mIsLoggerStart = false;
//Add 2011/11/02 Z01_h_yamada End <--
       if (mThread != null) {
            mThread.interrupt();
            mThread = null;
        }
    }

    protected int sleep(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            return Constants.VP_FAILURE;
        }
        return Constants.VP_SUCCESS;
    }

    private static final int TYPE_UNKNOWN = -1;
    private static final int TYPE_ACCELINFO = 0;
    private static final int TYPE_ORIENTINFO = 1;
    private static final int TYPE_GYROINFO = 2;
    private static final int TYPE_LATLONINFO = 3;
    private static final int TYPE_SATINFO = 4;

    protected class LogInfo {
        public int type;
        public long passTime;

        public LogInfo() {
            type = TYPE_UNKNOWN;
        }
    };

    protected class AccelInfo extends LogInfo {
        public long timestamp;
        public short accuracy;
        public float accelX;
        public float accelY;
        public float accelZ;

        public AccelInfo() {
            type = TYPE_ACCELINFO;
        }

        public AccelInfo(String[] values) {
            super();
//Chg 2011/10/19 Z01_h_yamada Start -->
//            passTime = Long.valueOf(values[0]);
//            timestamp = Long.valueOf(values[1]);
//            accuracy = Short.valueOf(values[2]);
//            accelX = Float.valueOf(values[3]);
//            accelY = Float.valueOf(values[4]);
//            accelZ = Float.valueOf(values[5]);
//--------------------------------------------
            if ( 6 <= values.length ) {
	            passTime = Long.valueOf(values[0]);
	            timestamp = Long.valueOf(values[1]);
	            accuracy = Short.valueOf(values[2]);
	            accelX = Float.valueOf(values[3]);
	            accelY = Float.valueOf(values[4]);
	            accelZ = Float.valueOf(values[5]);
            } else {
            	NaviLog.e("ItumoNaviPND", "AccelInfo::AccelInfo values length error. " + values.length);
            }
//Chg 2011/10/19 Z01_h_yamada End <--
        }
    };

    protected class OrientInfo extends LogInfo {
        public long timestamp;
        public short accuracy;
        public float azimuth;
        public float pitch;
        public float roll;

        public OrientInfo() {
            type = TYPE_ORIENTINFO;
        }

        public OrientInfo(String[] values) {
            super();
//Chg 2011/10/19 Z01_h_yamada Start -->
//            passTime = Long.valueOf(values[0]);
//            timestamp = Long.valueOf(values[1]);
//            accuracy = Short.valueOf(values[6]);
//            azimuth = Float.valueOf(values[7]);
//            pitch = Float.valueOf(values[8]);
//            roll = Float.valueOf(values[9]);
//--------------------------------------------
            if ( 10 <= values.length ) {
	            passTime = Long.valueOf(values[0]);
	            timestamp = Long.valueOf(values[1]);
	            accuracy = Short.valueOf(values[6]);
	            azimuth = Float.valueOf(values[7]);
	            pitch = Float.valueOf(values[8]);
	            roll = Float.valueOf(values[9]);
            } else {
            	NaviLog.e("ItumoNaviPND", "OrientInfo::OrientInfo values length error. " + values.length);
            }
//Chg 2011/10/19 Z01_h_yamada End <--
        }
    };

    protected class GyroInfo extends LogInfo {
        public long timestamp;
        public short accuracy;
        public float accelX;
        public float accelY;
        public float accelZ;

        public GyroInfo() {
            type = TYPE_ORIENTINFO;
        }

        public GyroInfo(String[] values) {
            super();
//Chg 2011/10/19 Z01_h_yamada Start -->
//            passTime = Long.valueOf(values[0]);
//            timestamp = Long.valueOf(values[1]);
//            accuracy = Short.valueOf(values[10]);
//            accelX = Float.valueOf(values[11]);
//            accelY = Float.valueOf(values[12]);
//            accelZ = Float.valueOf(values[13]);
//--------------------------------------------
            if ( 14 <= values.length ) {
	            passTime = Long.valueOf(values[0]);
	            timestamp = Long.valueOf(values[1]);
	            accuracy = Short.valueOf(values[10]);
	            accelX = Float.valueOf(values[11]);
	            accelY = Float.valueOf(values[12]);
	            accelZ = Float.valueOf(values[13]);
            } else {
            	NaviLog.e("ItumoNaviPND", "GyroInfo::GyroInfo values length error. " + values.length);
            }
//Chg 2011/10/19 Z01_h_yamada End <--
        }
    };

    protected class LatLonInfo extends LogInfo {
        public long lat;
        public long lon;
        public float dir;
        public float speed;
//Add 2011/08/10 Z01yoneya Start -->
        public long timestamp; //utc,ミリ秒
        public float accuracy;
        public float altitude;
        public String provider;
        public long gpsFixTime; //utc,ミリ秒
//Add 2011/08/10 Z01yoneya End <--
//Add 2011/10/20 Z01YUMISAKI Start -->
        public boolean hasDir;
//Add 2011/10/20 Z01YUMISAKI End <--
        public LatLonInfo() {
            type = TYPE_LATLONINFO;
        }

        public LatLonInfo(String[] values) {
            super();
//Chg 2011/08/10 Z01yoneya Start -->
//            passTime = Long.valueOf(values[0]);
//            lat = Long.valueOf(values[14]);
//            lon = Long.valueOf(values[15]);
//            dir = Float.valueOf(values[16]);
////Chg 2011/04/12 Z01yoneya Start-->
//////// Add 2011/01/20 Z01ONOZAWA Start --> GPS(Location)と単位を合わせる
//////          speed = Float.valueOf(values[17]);
////            speed = Float.valueOf(values[17]) * 1000.0f;// Km/s -> m/s
//////Add 2011/01/20 Z01ONOZAWA End   <--
////---------------------------------------
//            speed = Float.valueOf(values[17]);
//            switch(LOAD_LOG_FILE_VERSION){
//            case 0:	speed = speed * 1000.0f;			break;// Km/s -> m/s
//            case 1:	speed = speed * 1000.0f / 3600.0f;	break;// Km/h -> m/s
//            }
////Chg 2011/04/12 Z01yoneya End <--
//--------------------------------------------
            if (LOAD_LOG_FILE_VERSION <= 1) {
                passTime = Long.valueOf(values[0]);
                lat = Long.valueOf(values[14]);
                lon = Long.valueOf(values[15]);
//Chg 2011/10/20 Z01YUMISAKI Start -->
//                dir = Float.valueOf(values[16]);
//------------------------------------------------------------------------------------------------------------------------------------
                if (values[16].trim().equalsIgnoreCase(FIELD_STRING_INVALID)) {
                	hasDir = false;
                	dir = 0;
                }
                else {
                	hasDir = true;
                	dir = Float.valueOf(values[16]);
                }
//Chg 2011/10/20 Z01YUMISAKI End <--

                speed = Float.valueOf(values[17]);
                switch (LOAD_LOG_FILE_VERSION) {
                    case 0:
                        speed = speed * 1000.0f;
                        break;// Km/s -> m/s
                    case 1:
                        speed = speed * 1000.0f / 3600.0f;
                        break;// Km/h -> m/s
                }
            } else if (LOAD_LOG_FILE_VERSION == 2) {
                passTime = Long.valueOf(values[0]);
                timestamp = Long.valueOf(values[1]);
                accuracy = Float.valueOf(values[14]);
                lat = Long.valueOf(values[15]);
                lon = Long.valueOf(values[16]);
//Chg 2011/10/20 Z01YUMISAKI Start -->
//                dir = Float.valueOf(values[17]);
//------------------------------------------------------------------------------------------------------------------------------------
                if (values[17].trim().equalsIgnoreCase(FIELD_STRING_INVALID)) {
                	hasDir = false;
                	dir = 0;
                }
                else {
                	hasDir = true;
                	dir = Float.valueOf(values[17]);
                }
//Chg 2011/10/20 Z01YUMISAKI End <--

                speed = Float.valueOf(values[18]);
                speed = speed * 1000.0f / 3600.0f; // Km/h -> m/s
                altitude = Float.valueOf(values[19]);
                provider = values[20];
                gpsFixTime = Long.valueOf(values[21]);
            }
//Add 2011/10/20 Z01YUMISAKI Start -->
//            if (hasDir && 0 == speed && 0 == dir) {
//            	NaviLog.d("GPS", "LatLonInfo::LatLonInfo() INVALIDATE bearing while both speed and bearing are zero");
//            	hasDir = false;
//            }
//            NaviLog.d("GPS", "LatLonInfo::LatLonInfo() hasDir = " + hasDir + " dir = " + dir);
//Add 2011/10/20 Z01YUMISAKI End <--
//Chg 2011/08/10 Z01yoneya End <--
        }
    };

    protected class SatInfoItem {
        public short satID;
        public short azimuth;
        public short elevation;
        public short snRate;
        public boolean enable;
    }

    protected class SatInfo extends LogInfo {
        protected Vector<SatInfoItem> list = new Vector<SatInfoItem>(12);

        public SatInfo() {
            type = TYPE_SATINFO;
        }

        public SatInfo(String[] values) {
            super();
            passTime = Long.valueOf(values[0]);
            for (int i = 0; i < 12; i++) {
//Chg 2011/08/10 Z01yoneya Start -->
//                if (values[i * 5 + 18].length() > 0) {
//                    SatInfoItem item = new SatInfoItem();
//                    item.satID = Short.valueOf(values[i * 5 + 18]);
//                    item.azimuth = Short.valueOf(values[i * 5 + 19]);
//                    item.elevation = Short.valueOf(values[i * 5 + 20]);
//                    item.snRate = Short.valueOf(values[i * 5 + 21]);
//// Chg 2011/03/01 sawada Start -->
////                  item.enable = (values[i * 5 + 22] == "1") ? true : false;
//                    item.enable = values[i * 5 + 22].equals("1");
//// Chg 2011/03/01 sawada End   <--
//                    list.add(item);
//                }
//--------------------------------------------
                int satInfoValuesIdx = 18;
                if (LOAD_LOG_FILE_VERSION == 2) {
                    satInfoValuesIdx = 22;
                }
                if (values[i * 5 + satInfoValuesIdx].length() > 0) {
                    SatInfoItem item = new SatInfoItem();
                    item.satID = Short.valueOf(values[i * 5 + satInfoValuesIdx]);
                    satInfoValuesIdx++;
                    item.azimuth = Short.valueOf(values[i * 5 + satInfoValuesIdx]);
                    satInfoValuesIdx++;
                    item.elevation = Short.valueOf(values[i * 5 + satInfoValuesIdx]);
                    satInfoValuesIdx++;
                    item.snRate = Short.valueOf(values[i * 5 + satInfoValuesIdx]);
                    satInfoValuesIdx++;
                    item.enable = values[i * 5 + satInfoValuesIdx].equals("1");
                    list.add(item);
                }

//Chg 2011/08/10 Z01yoneya End <--
            }
        }

        public int getSatCount() {
            return list.size();
        }

        public SatInfoItem getSatAt(int location) {
            return list.get(location);
        }
    }

//Chg 2011/08/10 Z01yoneya Start -->
//    protected int getLogType(String[] values) {
//        if (values[2].length() > 0) {
//            // accelerometer
//            return TYPE_ACCELINFO;
//        } else if (values[6].length() > 0) {
//            // orientation
//            return TYPE_ORIENTINFO;
//        } else if (values[10].length() > 0) {
//            // gyroscope
//            return TYPE_GYROINFO;
//        } else if (values[14].length() > 0) {
//            // latitude/longitude
//            return TYPE_LATLONINFO;
//        } else if (values[18].length() > 0) {
//            // satellite
//            return TYPE_SATINFO;
//        }
//        return TYPE_UNKNOWN;
//    }
//--------------------------------------------
    protected int getLogType(String[] values) {
        int satInfoStartIdx = 18;
        if (LOAD_LOG_FILE_VERSION == 2) {
            satInfoStartIdx = 22;
        }

        if(satInfoStartIdx < values.length){
	        if (values[2].length() > 0) {
	            // accelerometer
	            return TYPE_ACCELINFO;
	        } else if (values[6].length() > 0) {
	            // orientation
	            return TYPE_ORIENTINFO;
	        } else if (values[10].length() > 0) {
	            // gyroscope
	            return TYPE_GYROINFO;
	        } else if (values[14].length() > 0) {
	            // latitude/longitude
	            return TYPE_LATLONINFO;
	        } else if (values[satInfoStartIdx].length() > 0) {
	            // satellite
	            return TYPE_SATINFO;
	        }
        }
        return TYPE_UNKNOWN;
    }

//Chg 2011/08/10 Z01yoneya End <--

    protected void parseLine(String line, long prevTime) {
        mLogValues = line.split(",", LOG_VALUES_CNT);
        if (mLogValues == null) {
            return;
        }

        switch (getLogType(mLogValues)) {
            case TYPE_ACCELINFO: {
                AccelInfo info = new AccelInfo(mLogValues);
                long doneTime = System.currentTimeMillis() - prevTime;
                sleep(Math.max(info.passTime - doneTime, 0));

// Chg 2011/02/24 sawada Start -->
//              AccSensorData data = new AccSensorData();
//              data.accuracy = info.accuracy;
//// Chg 2011/01/07 Z01ONOZAWA Start -->
////			data.timestamp = info.timestamp;
//				data.timestamp = System.currentTimeMillis();
//// Chg 2011/01/07 Z01ONOZAWA End   <--
//              data.acceleroX = info.accelX;
//              data.acceleroY = info.accelY;
//              data.acceleroZ = info.accelZ;
//              mSensorData.onAccelSensorChanged(data);
                RawSensorEvent revent = new RawSensorEvent();
                revent.accuracy = info.accuracy;
                revent.sensor = null;
                revent.timestamp = System.currentTimeMillis();
                ;
                revent.values[0] = info.accelX;
                revent.values[1] = info.accelY;
                revent.values[2] = info.accelZ;
                mSensorData.onAccelSensorChanged(revent);
// Chg 2011/02/24 sawada End   <--
            }
                break;

            case TYPE_ORIENTINFO: {
// Del 2011/01/17 Z01ONOZAWA Start  --> 傾きセンサーは使用しない
//              OrientInfo info = new OrientInfo(mLogValues);
//              long doneTime = System.currentTimeMillis() - prevTime;
//              sleep(Math.max(info.passTime - doneTime, 0));
//
//              OriSensorData data = new OriSensorData();
//              data.accuracy = info.accuracy;
//// Chg 2011/01/07 Z01ONOZAWA Start -->
////               data.timestamp = info.timestamp;
//           data.timestamp = System.currentTimeMillis();
//// Chg 2011/01/07 Z01ONOZAWA End   <--
//              data.azimuth = info.azimuth;
//              data.pitch = info.pitch;
//              data.roll = info.roll;
//              mSensorData.onOrientSensorChanged(data);
//Del 2011/01/17 Z01ONOZAWA End   <--
            }
                break;

            case TYPE_GYROINFO: {
                GyroInfo info = new GyroInfo(mLogValues);
                long doneTime = System.currentTimeMillis() - prevTime;
                sleep(Math.max(info.passTime - doneTime, 0));

// Chg 2011/02/24 sawada Start -->
//              GyroSensorData data = new GyroSensorData();
//              data.accuracy = info.accuracy;
//// Chg 2011/01/07 Z01ONOZAWA Start -->
////			data.timestamp = info.timestamp;
//				data.timestamp = System.currentTimeMillis();
//// Chg 2011/01/07 Z01ONOZAWA End   <--
//              data.gyroAccelX = info.accelX;
//              data.gyroAccelY = info.accelY;
//              data.gyroAccelZ = info.accelZ;
//              mSensorData.onGyroSensorChanged(data);
                RawSensorEvent revent = new RawSensorEvent();
                revent.accuracy = info.accuracy;
                revent.sensor = null;
                revent.timestamp = System.currentTimeMillis();
                ;
                revent.values[0] = info.accelX;
                revent.values[1] = info.accelY;
                revent.values[2] = info.accelZ;
                mSensorData.onGyroSensorChanged(revent);
// Chg 2011/02/24 sawada End   <--
            }
                break;

            case TYPE_LATLONINFO: {
                LatLonInfo info = new LatLonInfo(mLogValues);
                long doneTime = System.currentTimeMillis() - prevTime;
                sleep(Math.max(info.passTime - doneTime, 0));

                Location location = new Location(LocationManager.GPS_PROVIDER);
                location.setLongitude((double)(info.lon / 3600000.0));
                location.setLatitude((double)(info.lat / 3600000.0));
//Chg 2011/08/10 Z01yoneya Start -->
//                location.setAltitude(0);
//                location.setAccuracy(0);
//--------------------------------------------
                location.setAltitude(info.altitude);
                location.setAccuracy(info.accuracy);
                location.setProvider(info.provider);
                location.setTime(info.gpsFixTime);
//Chg 2011/08/10 Z01yoneya End <--
                location.setSpeed(info.speed);
//Chg 2011/10/20 Z01YUMISAKI Start -->
//                location.setBearing(info.dir);
//------------------------------------------------------------------------------------------------------------------------------------
                if (info.hasDir) {
                	location.setBearing(info.dir);
                }
                else {
                	location.removeBearing();
                }
//Chg 2011/10/20 Z01YUMISAKI End <--
                mLocationListener.onGetLocation(location);
            }
                break;

            case TYPE_SATINFO: {
                SatInfo info = new SatInfo(mLogValues);
                long doneTime = System.currentTimeMillis() - prevTime;
                sleep(Math.max(info.passTime - doneTime, 0));

                int satCnt = info.getSatCount();
                mSatInfoList.stVPSatInfo = new ZVP_SatInfo_t[satCnt];

                for (int i = 0; i < satCnt; i++) {
                    SatInfoItem item = info.getSatAt(i);
                    mSatInfoList.stVPSatInfo[i] = new ZVP_SatInfo_t();
                    mSatInfoList.stVPSatInfo[i].wSatID = item.satID;
                    mSatInfoList.stVPSatInfo[i].wAzimuth = item.azimuth;
                    mSatInfoList.stVPSatInfo[i].wElevation = item.elevation;
                    mSatInfoList.stVPSatInfo[i].wSNRate = item.snRate;
                    mSatInfoList.stVPSatInfo[i].bEnable = item.enable;
                }

                mLatLon = CommonLib.getSensorData();
                mSatInfoList.wTotalSatsInView = satCnt;
                mSatInfoList.lnLatitude1000 = mLatLon.getM_lLat();
                mSatInfoList.lnLongitude1000 = mLatLon.getM_lLong();
                mSatInfoList.dAltitude = CommonLib.getAltitude();
//Chg 2011/05/11 Z01thedoanh Start -->
                //NaviRunObj == nullの時、JNI関数呼び出さない（終了時に対策）
                if (NaviRun.isNaviRunInitial()) {
                    NaviRun.GetNaviRunObj().JNI_VP_GetLoadMode(mLoadMode);
                }
//Chg 2011/05/11 Z01thedoanh End <--

                CommonLib.setSatInfoList_t(mSatInfoList);
            }
                break;
        }
    }

    @Override
    public void run() {
        try {
//Chg 2011/11/02 Z01_h_yamada Start -->
//          while (true) {
//--------------------------------------------
            while (mIsLoggerStart) {
//Chg 2011/11/02 Z01_h_yamada End <--
                String line = null;

//Add 2011/03/21 Z01yoneya Start-->
                if (mBufReader != null) {
//Add 2011/03/21 Z01yoneya End <--
                    long prevTime = System.currentTimeMillis();
                    line = mBufReader.readLine();
                   if (line != null) {
                        isLogLoad = true;
                        parseLine(line, prevTime);
                    } else {
                        closeLogFile();
//Chg 2011/05/13 Z01thedoanh Start -->
                        if (VPSettingManager.VP_Enable_VPSensorLog_ListPlay == 0) {
                            //いままでどおりのVPLogger.txt再生
                            openLogFile();
                        }
                        if (VPSettingManager.VP_Enable_VPSensorLog_ListPlay == 1) {
                            //VPLogger.txtをリストで再生する場合
//Chg 2011/06/22 Z01thedoanh Start -->
//							BaseActivity act = (BaseActivity) BaseActivity.getBaseActivity();
//					        ActivityManager manager = (ActivityManager) act.getSystemService("activity");
//
//					        //sendIntent2アプリ（ツール）が機械に存在が必要
//					        Intent intentUPDstart = new Intent();
//					        intentUPDstart.setClassName("jp.co.cslab", "jp.co.cslab.sendIntent2");
//					        intentUPDstart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//					        act.startActivity(intentUPDstart);
//					        Log.i("[DEBUG]", "START other Application");
//					        act.finish();
//					        ((BaseActivity) act).killMainThread();
//					        break;
                            MapActivity.onExitAppFromOutSide();
//Chg 2011/06/22 Z01thedoanh End <--
                        }
//Chg 2011/05/13 Z01thedoanh End <--

                    }
//Del 2011/03/25 Z01yoneya Start-->
//					Thread.sleep(100);
//Del 2011/03/25 Z01yoneya End <--
//Add 2011/03/21 Z01yoneya Start-->
                }
//Add 2011/03/21 Z01yoneya End <--
            }
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
//Del 2011/03/25 Z01yoneya Start-->
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//Del 2011/03/25 Z01yoneya End <--
        } finally {
            closeLogFile();
            isLogLoad = false;
        }
    }

    protected static String getLogName() {
        Calendar c = Calendar.getInstance();

        return String.format(LOG_FILENAME_DATED, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DATE),
                             c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), c.get(Calendar.SECOND));
    }

    public static int openLogFileForWrite(String baseDir) {
//        String fileName = baseDir + getLogName();
//        File file = new File(fileName);
//        file.getParentFile().mkdirs();
        try {
            File file = CommonLib.openFile(baseDir, getLogName());
            mWriter = new FileWriter(file);
            mBufWriter = new BufferedWriter(mWriter);
//Add 2011/04/12 Z01yoneya Start-->
            //1行目にバージョンヘッダをつける
            if (mBufWriter != null) {
                StringBuffer log = new StringBuffer(LOG_BUFSIZE);
                log.append(FILE_VERSION_STRING);
                log.append("=");
                log.append(WRITE_FILE_VERSION);
                log.append("\r\n");
                mBufWriter.write(log.toString());
            }
//Add 2011/04/12 Z01yoneya End <--

        } catch (IOException e) {
            mWriter = null;
            mBufWriter = null;
            return Constants.VP_FAILURE;
        }
        return Constants.VP_SUCCESS;
    }

    public static void closeLogFileForWrite() {
        try {
            if (mBufWriter != null) {
                mBufWriter.close();
//Add 2011/11/17 Z01_h_yamada Start -->
                mBufWriter = null;
//Add 2011/11/17 Z01_h_yamada End <--
            }
            if (mWriter != null) {
                mWriter.close();
//Add 2011/11/17 Z01_h_yamada Start -->
                mWriter = null;
//Add 2011/11/17 Z01_h_yamada End <--
            }
        } catch (IOException e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        }
    }

    private final static int LOG_BUFSIZE = 256;
    private final static String NO_ACCELINFO = ",,,";
    private final static String NO_ORIENTINFO = ",,,";
    private final static String NO_GYROINFO = ",,,";
//Chg 2011/08/10 Z01yoneya Start -->LogVer=2
//    private final static String NO_LATLONINFO = ",,,";
//--------------------------------------------
    private final static String NO_LATLONINFO = ",,,,,,,";
//Chg 2011/08/10 Z01yoneya End <--
    private final static String NO_SATINFO = ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,";

    public static int writeLog(AccSensorData data) {
        if (mBufWriter == null) {
            return Constants.VP_SUCCESS;
        }
        synchronized (mBufWriter) {
            long millis = System.currentTimeMillis();
            long elapsed = (mPrevMillis > 0) ? (millis - mPrevMillis) : 0;

            StringBuffer log = new StringBuffer(LOG_BUFSIZE);
            log.append(elapsed);
            log.append(",");
            log.append(data.timestamp);
            log.append(",");
            log.append(data.accuracy);
            log.append(",");
            log.append(data.acceleroX);
            log.append(",");
            log.append(data.acceleroY);
            log.append(",");
            log.append(data.acceleroZ);
            log.append(",");
            log.append(NO_ORIENTINFO);
            log.append(",");
            log.append(NO_GYROINFO);
            log.append(",");
            log.append(NO_LATLONINFO);
            log.append(",");
            log.append(NO_SATINFO);
            log.append("\r\n");
            try {
                mBufWriter.write(log.toString());
            } catch (IOException e) {
                return Constants.VP_FAILURE;
            }
            mPrevMillis = millis;
        }
        return Constants.VP_SUCCESS;
    }

    public static int writeLog(GyroSensorData data) {
        if (mBufWriter == null) {
            return Constants.VP_SUCCESS;
        }
        synchronized (mBufWriter) {
            long millis = System.currentTimeMillis();
            long elapsed = (mPrevMillis > 0) ? (millis - mPrevMillis) : 0;

            StringBuffer log = new StringBuffer(LOG_BUFSIZE);
            log.append(elapsed);
            log.append(",");
            log.append(data.timestamp);
            log.append(",");
            log.append(NO_ACCELINFO);
            log.append(",");
            log.append(NO_ORIENTINFO);
            log.append(",");
            log.append(data.accuracy);
            log.append(",");
            log.append(data.gyroAccelX);
            log.append(",");
            log.append(data.gyroAccelY);
            log.append(",");
            log.append(data.gyroAccelZ);
            log.append(",");
            log.append(NO_LATLONINFO);
            log.append(",");
            log.append(NO_SATINFO);
            log.append("\r\n");
            try {
                mBufWriter.write(log.toString());
            } catch (IOException e) {
                return Constants.VP_FAILURE;
            }
            mPrevMillis = millis;
        }
        return Constants.VP_SUCCESS;
    }

    public static int writeLog(OriSensorData data) {
        if (mBufWriter == null) {
            return Constants.VP_SUCCESS;
        }
        synchronized (mBufWriter) {
            long millis = System.currentTimeMillis();
            long elapsed = (mPrevMillis > 0) ? (millis - mPrevMillis) : 0;

            StringBuffer log = new StringBuffer(LOG_BUFSIZE);
            log.append(elapsed);
            log.append(",");
            log.append(data.timestamp);
            log.append(",");
            log.append(NO_ACCELINFO);
            log.append(",");
            log.append(data.accuracy);
            log.append(",");
            log.append(data.azimuth);
            log.append(",");
            log.append(data.pitch);
            log.append(",");
            log.append(data.roll);
            log.append(",");
            log.append(NO_GYROINFO);
            log.append(",");
            log.append(NO_LATLONINFO);
            log.append(",");
            log.append(NO_SATINFO);
            log.append("\r\n");
            try {
                mBufWriter.write(log.toString());
            } catch (IOException e) {
                return Constants.VP_FAILURE;
            }
            mPrevMillis = millis;
        }
        return Constants.VP_SUCCESS;
    }

    public static void writeLog(String log) {
        if (mBufWriter == null) {
            return;
        }

        try {
            mBufWriter.write(log);
        } catch (IOException e) {
            return;
        }

    }

//Chg 2011/08/10 Z01yoneya Start -->
//    public static int writeLog(Location location) {
//--------------------------------------------
    public static int writeLog(Location location, long timeStamp) {
//Chg 2011/08/10 Z01yoneya End <--

        if (mBufWriter == null) {
            return Constants.VP_SUCCESS;
        }
        synchronized (mBufWriter) {
            // convert to 1/1000 sec
            long lon = (long)(location.getLongitude() * 3600000);
            long lat = (long)(location.getLatitude() * 3600000);

//Chg 2011/04/12 Z01yoneya Start-->
//            // convert to km/s
//            float speed = location.getSpeed() / 1000;
//---------------------------------------
            //LOG_FILE_VERSIONが1以降はkm/hで入出力する
            // m/s -> km/h
            float speed = location.getSpeed() * 3600 * 0.001f;
//Chg 2011/04/12 Z01yoneya End <--

            // direction
//Add 2011/10/20 Z01YUMISAKI Start -->
            boolean hasBearing = location.hasBearing();
//Add 2011/10/20 Z01YUMISAKI End <--
            float bearing = location.getBearing();
//Add 2011/08/10 Z01yoneya Start -->
            float accuracy = location.getAccuracy(); //GPS精度(m)
            float altitude = (float)location.getAltitude(); //高度(m)
            String provider = location.getProvider(); //GPS計算提供(GPS or 基地局)
            long gps_fix_time_utc = location.getTime(); //GPS座標確定時刻(ミリ秒、1970年基準)
//Add 2011/08/10 Z01yoneya End <--
            long millis = System.currentTimeMillis();
            long elapsed = (mPrevMillis > 0) ? (millis - mPrevMillis) : 0;

            StringBuffer log = new StringBuffer(LOG_BUFSIZE);
            log.append(elapsed);
//Chg 2011/08/10 Z01yoneya Start -->取得時刻を出力
//            log.append(",,");
//--------------------------------------------
            log.append(",");
            log.append(timeStamp);
            log.append(",");
//Chg 2011/08/10 Z01yoneya End <--
            log.append(NO_ACCELINFO);
            log.append(",");
            log.append(NO_ORIENTINFO);
            log.append(",");
            log.append(NO_GYROINFO);
            log.append(",");
//Add 2011/08/10 Z01yoneya Start -->LogVer=2
            log.append(accuracy);
            log.append(",");
//Add 2011/08/10 Z01yoneya End <--
            log.append(lat);
            log.append(",");
            log.append(lon);
            log.append(",");
//Chg 2011/10/20 Z01YUMISAKI Start -->
//            log.append(bearing);
//------------------------------------------------------------------------------------------------------------------------------------
            if (hasBearing) {
            	log.append(bearing);
            }
            else {
            	log.append(FIELD_STRING_INVALID);
            }
//Chg 2011/10/20 Z01YUMISAKI End <--

            log.append(",");
            log.append(speed);
            log.append(",");
//Add 2011/08/10 Z01yoneya Start -->LogVer=2
            log.append(altitude);
            log.append(",");
            log.append(provider);
            log.append(",");
            log.append(gps_fix_time_utc);
            log.append(",");
//Add 2011/08/10 Z01yoneya End <--
            log.append(NO_SATINFO);
            log.append("\r\n");
            try {
                mBufWriter.write(log.toString());
            } catch (IOException e) {
                return Constants.VP_FAILURE;
            }
            mPrevMillis = millis;
        }
        return Constants.VP_SUCCESS;
    }

//Chg 2011/08/10 Z01yoneya Start -->
//    public static int writeLog(GpsStatus gpsStatus) {
//--------------------------------------------
    public static int writeLog(GpsStatus gpsStatus, long timeStamp) {
//Chg 2011/08/10 Z01yoneya End <--

        if (mBufWriter == null) {
            return Constants.VP_SUCCESS;
        }
        synchronized (mBufWriter) {
            long millis = System.currentTimeMillis();
            long elapsed = (mPrevMillis > 0) ? (millis - mPrevMillis) : 0;

            StringBuffer log = new StringBuffer(LOG_BUFSIZE);
            log.append(elapsed);
//Chg 2011/08/10 Z01yoneya Start -->取得時刻を出力
//          log.append(",,");
//--------------------------------------------
            log.append(",");
            log.append(timeStamp);
            log.append(",");
//Chg 2011/08/10 Z01yoneya End <--
            log.append(NO_ACCELINFO);
            log.append(",");
            log.append(NO_ORIENTINFO);
            log.append(",");
            log.append(NO_GYROINFO);
            log.append(",");
            log.append(NO_LATLONINFO);
//Del 2011/08/10 Z01yoneya Start -->whileループ文の先頭でカンマ出力するから不要
//            log.append(",");
//Del 2011/08/10 Z01yoneya End <--

            Iterable<GpsSatellite> allSatellites = gpsStatus.getSatellites();
            Iterator<GpsSatellite> it = allSatellites.iterator();
            int satCnt = 0;

            while (it.hasNext()) {
                GpsSatellite sat = (GpsSatellite)it.next();
                if (sat == null) {
                    break;
                }
                log.append(",");
                log.append(sat.getPrn());
                log.append(",");
                log.append((short)sat.getAzimuth());
                log.append(",");
                log.append((short)sat.getElevation());
                log.append(",");
                log.append((short)sat.getSnr());
                log.append(",");
                log.append((sat.usedInFix() == true) ? 1 : 0);

                satCnt++;
            }
            for (int i = satCnt; i < 12; i++) {
                log.append(",,,,,");
            }
            log.append("\r\n");
            try {
                mBufWriter.write(log.toString());
            } catch (IOException e) {
                return Constants.VP_FAILURE;
            }
            mPrevMillis = millis;
        }
        return Constants.VP_SUCCESS;
    }

    private static boolean isLogLoad = false;

    public static boolean isLoggerLoad() {
        return isLogLoad;
    }


    ///////////////////////////////////////////////////////////
    // ●インタなび対応（GPS NMEA,ACC,Gyro,車パルの生データをログ）
    // Java側の生ログ作成
    ///////////////////////////////////////////////////////////
    /*
     *  VP/DRのログ優先（バリエーション）について説明
     *  ログファイルは２つ存在します。一つはVPログ走行用の"DRlog.txt"、もう一つは
     *  DRログ走行用の"DRlogN.txt"（生ログ）があります。
     *  前者("DRlog.txt")は、VPマッチング評価用です（Nativeで作成）
     *  後者("DRlogN.txt")は、ｾﾝｻ評価用です(javaで作成)
     *  優先はログ走行を優先します。(\DRlogフォルダにログファイルが存在する)
     *  1)"DRlog.txt"のログ走行
     *  2)"DRlogN.txt"の生ログ走行("DRlog_xx.txt"の作成)
     *  3)通常走行("DRlog_xx.txt","DRlog_xxn.txt"の作成)
     *
     *  生ログ走行は、DRデバッグ用の機能として搭載しています。ドキュメントはありません
     */
       /* 書込み用の変数 */
       private static FileWriter interNavi_Writer = null;
       private static BufferedWriter interNavi_BufWriter;
       private static String[]	saveAcc_sdmp = new String[200];		// Acc 生データ文字列(1秒分)
       private static String[]	saveGyro_sdmp = new String[200];	// Gyro 生データ文字列(1秒分)
       private static String[]	saveNMEA_sdmp = new String[2] ;		// GPS NMEA 生データ文字列(1秒分)
       private static int	wsetflag = 0;							// 生データ収集状態
       private static int	writeflag = 0;							// 生データWrite状態
       /* 詠出し用の変数 */
       private static FileReader interNavi_Reader = null;
       private static BufferedReader interNavi_BufReader;
       private static int	readstats = -1;							// 生データread状態
       private static int	readGet_stats = 0;						// 生データ Get状態
       private static int	readGet2_stats = 0;						// 生データ Get状態
       															// 0:空、Readする,0x000F Read済み
       private static int	GPS_RecvFlga = 0;						// GPSを一度受信している
       private static String ReadLinedata = null;					// ファイル読込み１行ﾃﾞｰﾀ
       private static int	readGet_GPSstats = 0;					// 生データ GPSGet状態
       private static int	read_ACCCnt = 0;						// 生データ ACC行数
       private static int	read_GyroCnt = 0;						// 生データ Gyro行数
       /* Get関数で渡すﾃﾞｰﾀ */
       private static String GPS_sensdata = "";					// MyNmeaListenerが出力するGPS文字ﾃﾞｰﾀ
       private static String GPS_Timesensdata = "";
       private static long	readGet_AccTotalTime = 0;				// ACC ﾄｰﾀﾙ時間[ms]
       private static long[]  read_AccmnTime = new long[200];		// ACC 周期時間[ms]
       private static float[] read_AccX = new float[200];			// ACC Xﾃﾞｰﾀ
       private static float[] read_AccY = new float[200];			// ACC Yﾃﾞｰﾀ
       private static float[] read_AccZ = new float[200];			// ACC Zﾃﾞｰﾀ
       private static long	readGet_GyroTotalTime = 0;				// Gyro ﾄｰﾀﾙ時間[ms]
       private static long[]  read_GyromnTime = new long[200];		// Gyro 周期時間[ms]
       private static float[] read_GyroX = new float[200];			// Gyro Xﾃﾞｰﾀ
       private static float[] read_GyroY = new float[200];			// Gyro Yﾃﾞｰﾀ
       private static float[] read_GyroZ = new float[200];			// Gyro Zﾃﾞｰﾀ
       private static long	readGet_CarTotalTime = 0;				// Car ﾄｰﾀﾙ時間[ms]
       private static int	read_CarInfobit = -1;					// Car 車両情報bit
       private static int	read_CardiffPulseCount = 0;				// Car 差分車パルス
       private static int	read_CarPulseCount = 0;					// Car 車パルス
       private static int	read_DataPass = 0;						// 読み飛ばしデータ
       private static int	read_Datagpscntflag = 0;				// gpscnt付きログデータか 1:はい
       private static int	funtoro_flag = -1;						// 1:生ログはFUNTORO端末で作成されている

       public static void Navi_setlogpath() {

           LOG_BASEWRITEDIR = NaviAppDataPath.getLogFilePath();
           /* Write: log\日付ﾌｫﾙﾀﾞ\DRLog_xxxxn.txt */
           LOG_BASEREADDIR = NaviAppDataPath.getDRLogFilePath();
           /* Read:  DRlog\DRLogN.txt */

           //NaviLog.v("TestWata", "WRITEpath="+LOG_BASEWRITEDIR+" READpath="+LOG_BASEREADDIR);
       }
   	/* DR生ログファイル名(Write)作成 */
       protected static String interNavi_getWriteLogName() {
            Calendar c = Calendar.getInstance();

            return String.format(LOG_WRITEFILENAME_DATED, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DATE),
                                 c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), c.get(Calendar.SECOND));
       }

   	// 生ログ走行ファイルの存在を確認する用
   	public static boolean isExistLOGNModeFile(){
   		return NaviAppDataPath.isgetDRLogFile();
   	}

   	//------------------------------------------------------
   	/* 生ログ走行状態を戻す */
       public static int DRN_ReadFileStats() {
       	return readstats;
       }
       /* 生ﾃﾞｰﾀファイルRead */
       private static void RDN_readline() {
   		try {
   			ReadLinedata = interNavi_BufReader.readLine();
   			read_DataPass++;
   			//NaviLog.v("TestWata", "DRN RDN_readline() =>"+ReadLinedata);
   		} catch (IOException e) {
//   			NaviLog.v("TestWata", "RDN_readline() Read Error");
   			ReadLinedata = null;
   		}
//   		} finally {
//   			NaviLog.v("TestWata", "RDN_readline() Read finally");
//   			ReadLinedata = null;
//   		}
       }
       static boolean isInteger(String num) {
       	try {
       		/* parseIntを使う場合、必ず try-cacth を付けること */
       		int n = Integer.parseInt(num);
       		n = 0;
       		return true;
       	} catch (NumberFormatException e) {
       		return false;
       	}
       }

       /* GPSﾃﾞｰﾀを収集する */
       public static int RDN_GPSData(String tdata) {
       	int iret = 0;
   		if(tdata.equals("$GTime"))
   		{
   			//NaviLog.v("TestWata", "DRN RDN_GPSData() "+ReadLinedata);
   			/* NMEAが続けてログされている場合は、後のNMEAが使われる */
            GPS_Timesensdata = ReadLinedata + "\n";
   			readGet_GPSstats |= 0x0001;
   			RDN_readline();			/* １行読み込む */
   		}
   		else if(tdata.equals("$GPSmove"))
   		{
   			//NaviLog.v("TestWata", "DRN RDN_GPSData() $GPSmove ");
   			GPS_sensdata = ReadLinedata + "\n";
   			readGet_GPSstats |= 0x0002;
   			//readGet_stats = 0;			// GPSを頭にする
   			RDN_readline();			/* １行読み込む */
   		}
   		else if(tdata.equals("$GPSstop"))
   		{
   			//NaviLog.v("TestWata", "DRN RDN_GPSData() $GPSstop ");
   			GPS_Timesensdata = "";
   			GPS_sensdata = ReadLinedata + "\n";
   			readGet_GPSstats |= 0x0003;
   			//readGet_stats = 0;			// GPSを頭にする
   			RDN_readline();			/* １行読み込む */
   		}
   		else {
   			/* GPS以外のﾃﾞｰﾀなので */
   			iret = 1;
   		}
       	return iret;
       }
       /* ACCﾃﾞｰﾀを収集する */
       public static int RDN_ACCData(String tdata) {
       	int iret = 0;
   		if(tdata.equals("A"))
   		{
   			//NaviLog.v("TestWata", "DRN RDN_ACCData() ACC "+":"+tdata+"::"+ReadLinedata+"cnt="+read_ACCCnt);
   			String[] accd = ReadLinedata.split(",", -1);
   			if(accd.length == 6) {
   				//NaviLog.v("TestWata", "DRN RDN_ACCData() Data="+accd[2]+","+accd[3]);
   				/* <先頭カラムは、No> */
   				/* <2カラムは、ACC種別 */
   				/* <3カラムは、周期間隔[ms]> */
   				read_AccmnTime[read_ACCCnt] = 1;
   				if(accd[2] != null) {
   					read_AccmnTime[read_ACCCnt] = Long.valueOf(accd[2]).longValue();
   				}
   				readGet_AccTotalTime = readGet_AccTotalTime + read_AccmnTime[read_ACCCnt];
   				/* <4カラムは、X軸> */
   				read_AccX[read_ACCCnt] = 0.0f;
   				if(accd[3] != null) {
   					read_AccX[read_ACCCnt] = Float.valueOf(accd[3]).floatValue();
   				}
   				/* <5カラムは、Y軸> */
   				read_AccY[read_ACCCnt] = 0.0f;
   				if(accd[4] != null) {
   					read_AccY[read_ACCCnt] = Float.valueOf(accd[4]).floatValue();
   				}
   				/* <6カラムは、Z軸> */
   				read_AccZ[read_ACCCnt] = 0.0f;
   				if(accd[5] != null) {
   					read_AccZ[read_ACCCnt] = Float.valueOf(accd[5]).floatValue();
   				}
   				read_ACCCnt++;
   				if(read_ACCCnt>=200)
   					read_ACCCnt = 199;
   				RDN_readline();			/* １行読み込む */
   			}
   			else {
   				/* 中途半端ﾃﾞｰﾀなので */
   				iret = 1;
   			}
   		}
   		else {
   			/* ACC以外のﾃﾞｰﾀなので */
   			iret = 1;
   		}
       	return iret;
       }
       /* Gyroﾃﾞｰﾀを収集する */
       public static int RDN_GyroData(String tdata) {
       	int iret = 0;
   		if(tdata.equals("G"))
   		{
   			//NaviLog.v("TestWata", "DRN RDN_GyroData() Gyro "+ReadLinedata+"cnt="+read_GyroCnt);
   			String[] accd = ReadLinedata.split(",", -1);
   			if(accd.length == 6) {
   				/* <先頭カラムは、No> */
   				/* <2カラムは、Gyro種別 */
   				/* <3カラムは、周期間隔[ms]> */
   				read_GyromnTime[read_GyroCnt] = 1;
   				if(accd[2] != null) {
   					read_GyromnTime[read_GyroCnt] = Long.valueOf(accd[2]).longValue();
   				}
   				readGet_GyroTotalTime = readGet_GyroTotalTime + read_GyromnTime[read_GyroCnt];
   				/* <4カラムは、X軸> */
   				read_GyroX[read_GyroCnt] = 0.0f;
   				if(accd[3] != null) {
   					read_GyroX[read_GyroCnt] = Float.valueOf(accd[3]).floatValue();
   				}
   				/* <5カラムは、Y軸> */
   				read_GyroY[read_GyroCnt] = 0.0f;
   				if(accd[4] != null) {
   					read_GyroY[read_GyroCnt] = Float.valueOf(accd[4]).floatValue();
   				}
   				/* <6カラムは、Z軸> */
   				read_GyroZ[read_GyroCnt] = 0.0f;
   				if(accd[5] != null) {
   					read_GyroZ[read_GyroCnt] = Float.valueOf(accd[5]).floatValue();
   				}
   				read_GyroCnt++;
   				if(read_GyroCnt>=200)
   					read_GyroCnt = 199;
   				RDN_readline();			/* １行読み込む */
   			}
   			else {
   				/* 中途半端ﾃﾞｰﾀなので */
   				iret = 1;
   			}
   		}
   		else {
   			/* ACC以外のﾃﾞｰﾀなので */
   			iret = 1;
   		}
       	return iret;
       }
       /* Carﾃﾞｰﾀを収集する */
       public static int RDN_CarData(String tdata) {
       	int iret = 0;
   		if(tdata.equals("C"))
   		{
   			//NaviLog.v("TestWata", "DRN RDN_CarData() Car "+ReadLinedata);
   			String[] card = ReadLinedata.split(",", -1);
   			if(card.length == 8) {
   				/* <先頭カラムは、周期間隔[ms]> */
   				readGet_CarTotalTime = 1000;
   				if(card[0] != null) {
   					readGet_CarTotalTime = Long.valueOf(card[0]).longValue();
   				}
   				/* <2カラムは、Gyro種別 */
   				/* <3カラムは、"CarInfoBit"> */
   				/* <4カラムは、車両情報(16進文字　->int変換)> */
   				read_CarInfobit = 0;
   				if(card[3] != null) {
   					read_CarInfobit = Integer.decode(card[3]);
   				}
   				/* <5カラムは、"PulseCount"> */
   				/* <6カラムは、車パルス数> 未使用 */
   				read_CarPulseCount = Integer.valueOf(card[5]).intValue();
   				/* <7カラムは、差分車パルス数> */
   				read_CardiffPulseCount = 0;
   				if(card[6] != null) {
   					read_CardiffPulseCount = Integer.valueOf(card[6]).intValue();
   				}
   				RDN_readline();			/* １行読み込む */
   				/* Car情報は１行しかない */
   			}
   			else {
   				/* 中途半端ﾃﾞｰﾀなので */
   				iret = 1;
   			}
   		}
   		else {
   			/* Car以外のﾃﾞｰﾀなので */
   			iret = 1;
   		}
       	return iret;
       }

       /* 1ブロックﾃﾞｰﾀを生ﾃﾞｰﾀファイルから読込む */
       /* 1ブロックとは、GPS,ACC,Gyro,Carの一周期分のﾃﾞｰﾀ */
       /* 但し、GPS,Carはない場合がある */
       /* 戻り値は、１ブロックの周期時間[ms]を戻す */
       public static long DRN_ReadFile() {
       	/* 生ログ走行 */
       	if(readstats == 1)
       	{
       		/* Readしていない */
       		if(readGet_stats == 0)
       		{
       			//NaviLog.v("TestWata", "DRN DRN_ReadFile() Start --"+ReadLinedata);
       			GPS_Timesensdata = "";					/* GetGPSﾃﾞｰﾀクリア */
       			GPS_sensdata = "";							/* GetGPSﾃﾞｰﾀクリア */
       			readGet_GPSstats = 0;						/* GPS収集状態クリア */
       			readGet2_stats = 0;
       		    read_ACCCnt = 0;							// 生データ ACC行数
       		    read_GyroCnt = 0;							// 生データ Gyro行数
       		    readGet_AccTotalTime = 0;					// ACC ﾄｰﾀﾙ時間[ms]
       		    readGet_GyroTotalTime = 0;					// Gyro ﾄｰﾀﾙ時間[ms]
       		    readGet_CarTotalTime = 0;					// Car ﾄｰﾀﾙ時間[ms]
       			/* 前回に先頭行を読込んでいない(一番最初？) */
       			if (ReadLinedata == null)
       			{
       				/* readGet_stats : 0x0001 :GPS フラグ */
       				/*               : 0x0002 :Acc フラグ */
       				/*               : 0x0004 :Gyro フラグ */
       				/*               : 0x0008 :Car フラグ */
       				RDN_readline();			/* １行読み込む */
       			}

       			if(ReadLinedata == null)
       			{
       				//NaviLog.v("TestWata", "DRN DRN_ReadFile() NULL close ");
   					close_NaviLogFile();	/* 一旦ｸﾛｰｽﾞする */
   					openRW_NaviLogFile();	/* 同じファイルを繰り返す */
       			}
       			else
       			{
       				boolean loopf = true;
       				long	end_flag = 0;
       				int		igpscnt = 0;	/* 読出しgpscntｶｳﾝﾀ値 */
       				int		Gps_gpscnt = 0;
       				int		Acc_gpscnt = 0;
       				int		Gyro_gpscnt = 0;
       				//NaviLog.v("TestWata", "DRN DRN_ReadFile() Loop Start ");

       				/* 生ログデータには、ACCの後にCarが来る（あれば）以外の規則はありません */
       				/* GPSがあれば、GPSを最初に読み出すようにしています */
       				while(loopf)
       				{
       					/* 同期ｶｳﾝﾀの取得 */
                       	if((ReadLinedata.length() > 6) && (read_Datagpscntflag != 0)) { /* =までの文字数 */
                       		if(ReadLinedata.substring(0,6).equals("gpscnt")) {
                       			String[] num = ReadLinedata.split("=");
                       			igpscnt = Integer.valueOf(num[1]);
                       			//NaviLog.v("TestWata", "gpscnt="+igpscnt);
                       			// gpscntでデータ区切り判断
                       			if(((readGet2_stats & 0x0001) != 0) && ((readGet_stats & 0x0001) == 0)) {
                       				readGet_stats |= 0x0001;			/* GPSﾃﾞｰﾀRead完了 */
                       			}
                       			if(((readGet2_stats & 0x0002) != 0) && ((readGet_stats & 0x0002) == 0)) {
                       				readGet_stats |= 0x0002;			/* GPSﾃﾞｰﾀRead完了 */
                       			}
                       			if(((readGet2_stats & 0x0004) != 0) && ((readGet_stats & 0x0004) == 0)) {
                       				readGet_stats |= 0x0004;			/* GPSﾃﾞｰﾀRead完了 */
                       			}
               					/* ACC,GyroもしくはGPS揃っていれば、1ブロック完了 */
           						// 起動時にGPS受信がない場合
           						if((igpscnt == 0) &&((readGet_stats & 0x0006) == 0x0006)){
           							end_flag = 1000;	/* 1000ms */
           							//NaviLog.v("TestWata", "DRN DRN_ReadFile() RET Time ="+end_flag+"Sts="+readGet_stats);
           							loopf = false;
           							break;
           						} else if((igpscnt != 0) &&((readGet_stats & 0x0007) == 0x0007)) {
           							// V2.5では"GPSstop"が入る。但しgpscntは更新していない値
           							end_flag = 1000;	/* 1000ms */
           							//NaviLog.v("TestWata", "DRN DRN_ReadFile() RET Time ="+end_flag+"Sts="+readGet_stats+"gpscnt="+Gps_gpscnt);
           							loopf = false;
           							break;
           						}
                       			RDN_readline();			/* １行読み込む */
                       		}
                       	}

       					String[] fruit = ReadLinedata.split(",", -1);
       					read_DataPass = 0;							/* 読み飛ばしフラグON */
       					/* 最初の行ﾃﾞｰﾀよりﾃﾞｰﾀ種別を振り分ける */
       					/* GPSﾃﾞｰﾀを収集する */
       					/* GPSが重なっている場合は、最後優先 */
       					if(((readGet_stats & 0x0001) == 0) && (loopf == true) ) {
       					//if(((readGet_GPSstats & 0x0003) != 0x0003) && (loopf == true) ) {
       						if(RDN_GPSData(fruit[0]) == 1) {
       							/* 収集ﾃﾞｰﾀがあれば、完了フラグを設定する */
       							if(readGet_GPSstats == 0x0003) {
      									//readGet_stats |= 0x0001;			/* GPSﾃﾞｰﾀRead完了 */
      									Gps_gpscnt = igpscnt;				/* 同期ｶｳﾝﾀ */
      									GPS_RecvFlga = 1;					/* 一度受けた印 */
          								//NaviLog.v("TestWata", "DRN DRN_ReadFile() GPS End..ST="+readGet_stats);
          								readGet_GPSstats = 0;
       							}
       						}
       						else {
       							readGet2_stats |= 0x0001;			/* GPSﾃﾞｰﾀReadした */
       						}
           					if(ReadLinedata == null) {
           						/* Readﾃﾞｰﾀない、もしくは異常 */
           						//NaviLog.v("TestWata", "DRN DRN_ReadFile(1) ReadLinedata == null");
           						close_NaviLogFile();	/* 一旦ｸﾛｰｽﾞする */
           						openRW_NaviLogFile();	/* 同じファイルを繰り返す */
           						loopf = false;
           					}
       					}
       					/* ACCﾃﾞｰﾀを収集する */
       					if(((readGet_stats & 0x0002) == 0) && (loopf == true)) {
       						/* １カラム目は数字であること */
       						if(isInteger(fruit[0])) {
       							if(RDN_ACCData(fruit[1]) == 1) {
       								/* 収集ﾃﾞｰﾀがあれば、完了フラグを設定する */
       								if(read_ACCCnt != 0) {
       									//readGet_stats |= 0x0002;			/* ACCﾃﾞｰﾀRead完了 */
       									Acc_gpscnt = igpscnt;				/* 同期ｶｳﾝﾀ */
       									//NaviLog.v("TestWata", "DRN DRN_ReadFile() AccEnd..ST="+readGet_stats);
       								}
       							}
       							else {
       								readGet2_stats |= 0x0002;			/* ACCﾃﾞｰﾀReadした */
       							}
        						if(ReadLinedata == null) {
        							/* Readﾃﾞｰﾀない、もしくは異常 */
        							//NaviLog.v("TestWata", "DRN DRN_ReadFile(2) ReadLinedata == null");
        							close_NaviLogFile();	/* 一旦ｸﾛｰｽﾞする */
        							openRW_NaviLogFile();	/* 同じファイルを繰り返す */
        							loopf = false;
        						}
       						}
           					if(ReadLinedata == null) {
           						/* Readﾃﾞｰﾀない、もしくは異常 */
           						//NaviLog.v("TestWata", "DRN DRN_ReadFile(3) ReadLinedata == null");
           						close_NaviLogFile();	/* 一旦ｸﾛｰｽﾞする */
           						openRW_NaviLogFile();	/* 同じファイルを繰り返す */
           						loopf = false;
           					}
       					}
       					/* Gyroﾃﾞｰﾀを収集する */
       					if(((readGet_stats & 0x0004) == 0) && (loopf == true)) {
          					/* １カラム目は数字であること */
          					if(isInteger(fruit[0])) {
       							if(RDN_GyroData(fruit[1]) == 1) {
       								/* 収集ﾃﾞｰﾀがあれば、完了フラグを設定する */
       								if(read_GyroCnt != 0) {
       									//readGet_stats |= 0x0004;			/* GyroﾃﾞｰﾀRead完了 */
       									Gyro_gpscnt = igpscnt;				/* 同期ｶｳﾝﾀ */
       									//NaviLog.v("TestWata", "DRN DRN_ReadFile() GyroEnd..ST="+readGet_stats);
       								}
       							}
       							else {
       								readGet2_stats |= 0x0004;			/* GPSﾃﾞｰﾀReadした */
       							}
       							if(ReadLinedata == null) {
       								/* Readﾃﾞｰﾀない、もしくは異常 */
       								//NaviLog.v("TestWata", "DRN DRN_ReadFile(4) ReadLinedata == null");
       								close_NaviLogFile();	/* 一旦ｸﾛｰｽﾞする */
       								openRW_NaviLogFile();	/* 同じファイルを繰り返す */
       								loopf = false;
       							}
           					}
       					}

       					if((read_DataPass == 0) && (loopf == true)) {
       						/* 次のデータ読出しを行っていない */
       						/* このLineデータはどのデータにも属さない読み飛ばしデータ */
       						//NaviLog.v("TestWata", "DRN SkipData = "+ReadLinedata);
       						RDN_readline();			/* １行読み込む */
           					if(ReadLinedata == null) {
           						/* Readﾃﾞｰﾀない、もしくは異常 */
           						//NaviLog.v("TestWata", "DRN DRN_ReadFile(5) ReadLinedata == null");
           						close_NaviLogFile();	/* 一旦ｸﾛｰｽﾞする */
           						openRW_NaviLogFile();	/* 同じファイルを繰り返す */
           						loopf = false;
           					}
       					}
       				}
       				//NaviLog.v("TestWata", "DRN DRN_ReadFile() Loop End Ret="+end_flag);
       				return end_flag;
       			}
       			/* Read失敗した場合、戻りは０です。これは、地図更新を止める目的です */
       		}
       		/* 転送タイミング待ち */
       		return 0;
       	}
       	return -1;			/* 生ログ走行ではない */
       }
       /* GET関数は転送時間になってから呼び出してください */
       public static String DRN_Get_GPS() {
       	if((readGet_stats & 0x0001) != 0) {
       		readGet_stats = readGet_stats & 0xFFFE;
       		return GPS_sensdata;
       	} else {
           	return null;
       	}
       }
       public static String DRN_Get_GPSTime() {
          	if((readGet_stats & 0x0001) != 0) {
          		return GPS_Timesensdata;
          	} else {
              	return null;
          	}
          }
       /* ACC 1ブロックデータ数取得 */
       public static int DRN_Get_AccCnt() {
       	return read_ACCCnt;
       }
       /* ACC 1ブロック 周期時間[ms]取得 */
       public static long DRN_Get_AccnmTime(int index) {
       	return read_AccmnTime[index];
       }
       /* ACC XYZデータ数取得 */
       public static float DRN_Get_AccX(int index) {
       	return read_AccX[index];
       }
       public static float DRN_Get_AccY(int index) {
       	return read_AccY[index];
       }
       public static float DRN_Get_AccZ(int index) {
       	return read_AccZ[index];
       }

       /* Gyro 1ブロックデータ数取得 */
       public static int DRN_Get_GyroCnt() {
       	return read_GyroCnt;
       }
       /* Gyro 1ブロック 周期時間[ms]取得 */
       public static long DRN_Get_GyronmTime(int index) {
       	return read_GyromnTime[index];
       }
       /* Gyro XYZデータ数取得 */
       public static float DRN_Get_GyroX(int index) {
       	return read_GyroX[index];
       }
       public static float DRN_Get_GyroY(int index) {
       	return read_GyroY[index];
       }
       public static float DRN_Get_GyroZ(int index) {
       	return read_GyroZ[index];
       }

       /* データが複数あるので、完了を通知してもらう */
       public static void DRN_GetEnd_Acc() {
   		readGet_stats = readGet_stats & 0xFFFD;
       }
       public static void DRN_GetEnd_Gyro() {
       	readGet_stats = readGet_stats & 0xFFFB;
       }
       public static void DRN_GetEnd_Car() {
       	readGet_stats = readGet_stats & 0xFFF7;
       }

       /* 車パル取得状態 */
       public static int DRN_Get_CarStats() {
       	//NaviLog.v("TestWata", "DRN DRN_Get_CarStats() readGet_stats="+readGet_stats);    	/* 生ログ走行 */
       	if((readGet_stats & 0x0008) != 0) {
       		return 1;
       	} else {
       		/* 生ログ走行では、bitﾃﾞｰﾀ -1の状態をDRに戻す */
       		if(readstats == 1) {
       			return 1;
       		}
       		return 0;
       	}
       }
       public static long DRN_Get_CarTotalTime() {
       		/* 周期間隔[ms]> */
       	return readGet_CarTotalTime ;
       }
       public static int DRN_Get_CarInfobit() {
       	/* 車両情報(int)> */
       	return read_CarInfobit ;
       }
       public static int DRN_Get_CardiffPulseCount() {
       	/* 差分車パルス数 */
       	return read_CardiffPulseCount;
       }

       public static int DRN_Get_CarPulseCount() {
       	/* 積算パルス数 */
       	return read_CarPulseCount;
       }

       public static int DRN_Get_VT_Flag() {
       	/* 生ログファイルは、FUNTORO端末で作成している場合 1を戻す、異なる端末では -1を戻す */
       	return funtoro_flag;
       }

       /* Java でformat編集関数を使うと処理負荷があるので、生ログ書込みする／しないを通知する */
       public static int DRN_Get_WriteFlag() {
       	return writeflag;
       }

       /* DR生ログファイルRead/Write Open<MapActivity:起動時Call> */
       public static int openRW_NaviLogFile() {
       	boolean DRN_Readflag = false;
// ADD.2013.10.16 N.Sasao DRのログ出力強制カット START
       	boolean DR_Log_Flag = false;
// ADD.2013.10.16 N.Sasao DRのログ出力強制カット  END
       	/* 生ログファイルのログ走行指定あり */
       	if(isExistLOGNModeFile() == true)
       	{
       		/* 生ログファイル(DRLogN.txt)パス取得 */
//       		NaviLog.v("TestWata", "DRN isExistLOGNModeFile() TRUE file="+"/DRLogN.txt");

       		try {
   				//NaviLog.v("TestWata", "DRN OPEN Dir ="+LOG_BASEREADDIR);
   				//NaviLog.v("TestWata", "DRN OPEN File ="+"DRLogN.txt");
   				File file = CommonLib.openFile(LOG_BASEREADDIR, "DRLogN.txt");
   				interNavi_Reader = new FileReader(file);
   				interNavi_BufReader = new BufferedReader(interNavi_Reader);
   				//1行目にバージョンヘッダをつける
   				if (interNavi_BufReader != null) {
   					/* 生ログファイル確認と不要な行を読み飛ばし */
   	                String line = interNavi_BufReader.readLine();
   	                //NaviLog.v("TestWata", "DRN Get!!! "+line);
   					/* ファイル先頭は"[DR Data]" */
   					/* 2,3行目は、ｾﾝｻ特性情報     */
   	                if (line != null) {
   	                    if ((line.startsWith("[DR Data]")) || (line.startsWith("[DRF Data]"))) {
   	                    	if(line.startsWith("[DRF Data]")) {
   	                    		funtoro_flag = 1;
   	                    	}
   	                    	/* gpscnt=xxxがあるか */
   	                    	line = interNavi_BufReader.readLine();
   	                    	//NaviLog.v("TestWata", "DRN Get!!! "+line);
   	    	                if (line != null) {
   		                    	if(line.length() > 6) { /* =までの文字数 */
   		                    		if(line.substring(0,6).equals("gpscnt")) {
   		                    			// 生ログ走行時用 生ログ先頭に戻った場合、クリアする
   		                    			NaviRun.GetNaviRunObj().JNI_VP_clear_MMData();
   		                    			String[] num = line.split("=");
   		                    			int iNum = Integer.valueOf(num[1]);
   		                    			//NaviLog.v("TestWata", "gpscnt="+iNum);
   		                    			/* デバイス情報を読み飛ばす */
   		                    			while(true)
   		                    			{
   		                    				line = interNavi_BufReader.readLine();
   		                    				//NaviLog.v("TestWata", "Top DRN Get!!! "+line);
   		                    				if (line != null) {
   		                    					if(line.length() > 6) { /* =までの文字数 */
   		                    						if(line.substring(0,6).equals("gpscnt")) {
   		                    							break;
   		                    						}
   		                    					}
   	   		                    				else {
   	   		                    					break;
   	   		                    				}

   		                    				}
   		                    				else {
   		                    					break;
   		                    				}
   		                    			}
   		                    			read_Datagpscntflag = 1;		/* gpscnt付きログデータ */
   		                    		}
   	   	   	    	                //else {
   	   	   	    	                //	NaviLog.v("TestWata", "Not gpscntline ");
   	   	   	    	                //}
   		                    	}
   	   	    	                //else {
   	   	    	                //	NaviLog.v("TestWata", "Not 6over = "+line.length());
   	   	    	                //}
  	    	                }
   	    	                //else {
   	    	               // 	NaviLog.v("TestWata", "line = NULL!  or No't DR File !!! ");
   	    	               // }

   	                    	//NaviLog.v("TestWata", "DRN Get!!! "+line);
   	                        //次の行読み捨てる
   	                    	//line = interNavi_BufReader.readLine();
   	                    	//NaviLog.v("TestWata", "DRN Get!!! "+line);
   	                    	line = null;
   	                    	readGet_stats = 0;
   	                    	readstats = 1;
   	                    }
   	                }
                   	DRN_Readflag = true;
                   	/* 生ログファイルとは違ったﾌｧｲﾙを"DRlogN.txt"とした場合でも、通常動作は行わない */
   				}
   			} catch (IOException e) {
   				interNavi_Reader = null;
   				interNavi_BufReader = null;
//   				NaviLog.v("TestWata", "DRLogn Read Open Error");
   			}
      		/* Write用変数はクリアしておく */
   			interNavi_Writer = null;
   			interNavi_BufWriter = null;
   			writeflag = 0;
       	}
// MOD.2013.10.16 N.Sasao DRのログ出力強制カット START
       	/* 生ログ走行しない、できない場合は、生ログ作成する */
       	if(DRN_Readflag == false && DR_Log_Flag == true)
       	{
// MOD.2013.10.16 N.Sasao DRのログ出力強制カット  END
       		/* POSログする指定ﾌｧｲﾙがある & Log走行モードでない場合 */
       		/* 開発者メニューを起動後、変更してもこれは対応しない         */
       		// #6418 開発者メニューカスタマイズ対応
       			try {
       				File file = CommonLib.openFile(LOG_BASEWRITEDIR, interNavi_getWriteLogName());
       				interNavi_Writer = new FileWriter(file);
       				interNavi_BufWriter = new BufferedWriter(interNavi_Writer);
       				//1行目にバージョンヘッダをつける
       				if (interNavi_BufWriter != null) {
       					StringBuffer log = new StringBuffer(LOG_BUFSIZE);

       			       	/* 非FUNTORO端末 */
       			        log.append("[DR Data]");

       					//log.append("\r\n");
       					log.append("\n");
       					interNavi_BufWriter.write(log.toString());
       					writeflag = 1;
       				}
       				wsetflag = 0;
       			} catch (IOException e) {
       				interNavi_Writer = null;
       				interNavi_BufWriter = null;
       				writeflag = 0;
//       				NaviLog.v("TestWata", "DRLogn Write Open Error");
       				return Constants.VP_FAILURE;
       			}

       	}
       	return Constants.VP_SUCCESS;
       }

       /* 生ログファイルに書き込む */
       public static void write_DRData(int loop,String buf[]) {
       	int cnt = 0;

   			/* データコピー */
   			if ((interNavi_BufWriter != null) && (writeflag != 0)) {
   				for(cnt = 0; cnt < loop;cnt++)
   				{
   					/* NULL行あり */
   					if(buf[cnt] == null || buf[cnt].length() <= 0)
   					{
   						break;
   					}
   		    		try {
   		    			interNavi_BufWriter.write(buf[cnt]);
   		    		} catch (IOException e) {
   		    			interNavi_Writer = null;
   		    			writeflag = 0;
//   		    			NaviLog.v("TestWata", "DRLogn Write Error");
   		    			break;
   		    		}
   				}
   			}

       }

       /* ACC生ログファイルWrite */
       public static void writeAcc_InterNaviLogFile(String buf[],int gps_flag) {
       	int cnt = 0;

   		/* データコピー */
       	saveAcc_sdmp[0] = "gpscnt="+gps_flag+"\n";
   		for(cnt = 1; cnt < 200;cnt++)
   		{
   			/* NULL行あり */
   			if(buf[cnt-1] == null || buf[cnt-1].length() <= 0)
   			{
   				break;
   			}
   			saveAcc_sdmp[cnt] = buf[cnt-1]+"\n";
   			if(cnt < 199)
   			{
   				saveAcc_sdmp[cnt+1] = "";
   			}
   		}
   		wsetflag |= 0x01;	/* ACC データコピー完了 */
   		write_DRData(200,saveAcc_sdmp);
       }
       /* Gyro生ログファイルWrite */
       public static void writeGyro_InterNaviLogFile(String buf[],int gps_flag) {
       	int cnt = 0;

   		/* データコピー */
       	saveGyro_sdmp[0] = "gpscnt="+gps_flag+"\n";
   		for(cnt = 1; cnt < 200;cnt++)
   		{
   			/* NULL行あり */
   			if(buf[cnt-1] == null || buf[cnt-1].length() <= 0)
   			{
   				break;
   			}
   			saveGyro_sdmp[cnt] = buf[cnt-1]+"\n";
   			if(cnt < 199)
   			{
   				saveGyro_sdmp[cnt+1] = "";
   			}
   		}
   		wsetflag |= 0x02;	/* Gyro データコピー完了 */
   		write_DRData(200,saveGyro_sdmp);
       }

       /* GPS NMEA 生ログファイルWrite */
       public static void writeNMEA_InterNaviLogFile(String buf,int gps_flag) {
   		/* データコピー */
       	saveNMEA_sdmp[0] = "gpscnt="+gps_flag+"\n";
       	/* 既に改行付き */
   		saveNMEA_sdmp[1] = buf;
   		wsetflag |= 0x04;	/* NMEA データコピー完了 */
   		write_DRData(2,saveNMEA_sdmp);
       }

        /* DR生ログファイルClose<MapActivity:終了時Call> */
        public static void close_NaviLogFile() {
            try {
                if (interNavi_BufReader != null) {
               	 interNavi_BufReader.close();
               	 interNavi_BufReader = null;
                }
                if (interNavi_Reader != null) {
               	 interNavi_Reader.close();
               	 interNavi_Reader = null;
                }
                if (interNavi_BufWriter != null) {
               	 interNavi_BufWriter.close();
               	 interNavi_BufWriter = null;
                }
                if (interNavi_Writer != null) {
               	 interNavi_Writer.close();
               	 interNavi_Writer = null;
                }
            } catch (IOException e) {
//    			NaviLog.v("TestWata", "DRLogn Close Error");
            }
            writeflag = 0;
        }
}