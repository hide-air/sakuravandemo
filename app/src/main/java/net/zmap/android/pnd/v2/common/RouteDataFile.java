package net.zmap.android.pnd.v2.common;

import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.route.data.RouteData;
import net.zmap.android.pnd.v2.route.data.RouteNodeData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/*
 * ルート情報ファイル("RouteData.xml")クラス
 *
 * 拡張子はxmlだが、一般的なパーサは使わずクラス内で解釈する。
 * ルート名称にはXML禁止文字を使えるようにするため。
 *
 */
public class RouteDataFile {

    private static final String TAG_ROUTE_DATA_START = "<ROUTE_DATA>";
    private static final String TAG_ROUTE_DATA_END = "</ROUTE_DATA>";
    private static final String TAG_Route_START = "<Route";
    private static final String TAG_Route_END = "</Route>";
    private static final String TAG_Node = "<Node";

    /*
     * ファイル出力
     */
    public boolean writeData(String filePath, List<RouteData> routedataList) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "RouteDataFile::writeData()");
        File file = new File(filePath);
        if (file.exists()) {
            boolean result = file.delete();
            if (!result) {
                NaviLog.i(NaviLog.PRINT_LOG_TAG, "file delete error[" + filePath + "]");
            }
        }
        List<String> lineList = new ArrayList<String>();

        lineList.add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\r\n");
        lineList.add("<ROUTE_DATA>" + "\r\n");
        for (int i = 0; i < routedataList.size(); i++) {
            RouteData routeData = routedataList.get(i);
            lineList.add("<Route Date=\"" + routeData.getRouteDate() + "\" Mode=\"" + routeData.getRouteMode().toString() + "\" Name=\"" + routeData.getRouteName() + "\">" + "\r\n");
            String strSearchProperty = routeData.getSearchProperty().toString();
            List<RouteNodeData> nodeList = routeData.getRouteNodeDataList();
            for (int j = 0; j < nodeList.size(); j++) {
                RouteNodeData node = nodeList.get(j);

                lineList.add("<Node Lon=\"" + node.getLon() + "\" Lat=\"" + node.getLat()
                        + "\" Name=\"" + node.getName() + "\" Status=\"" + strSearchProperty + "\"/>" + "\r\n");
            }
            lineList.add("</Route>" + "\r\n");
        }
        lineList.add("</ROUTE_DATA>");

        return writeRouteDataFile(filePath, lineList);
    }

    /*
     * 属性ごとに文字列を区切る
     */
    private List<String> splitXmlTagAttrilbStr(String line, List<String> attribStrList) throws IOException {
        List<String> values = new ArrayList<String>();
        for (int i = 0; i < attribStrList.size(); i++) {
            int idx = line.indexOf(attribStrList.get(i));
            if (idx == -1) {
                throw new IOException();
            }
            if (i == 0) {
                continue;
            }
            values.add(line.substring(0, idx));
            line = line.substring(idx);
        }
        values.add(line);

        //"xxx"で囲んだ中身を取り出す
        List<String> attribValueList = new ArrayList<String>();
        for (int i = 0; i < values.size(); i++) {
            String attrib = values.get(i);
            int idxSt = attrib.indexOf("\"");
            int idxEd = attrib.lastIndexOf("\"");
// Mod -- 2011/10/10 -- H.Fukushima --- ↓ ----- MarketPND V2 課題No.235
//            if (idxSt == -1 || idxEd == -1) {
            if (idxSt == -1 || idxEd == -1 || (idxSt==idxEd)) {
// Mod -- 2011/10/10 -- H.Fukushima --- ↑ ----- MarketPND V2 課題No.235
                throw new IOException();
            }
            attribValueList.add(attrib.substring(idxSt + 1, idxEd));
        }

        return attribValueList;
    }

    /*
     * ファイルを読み込んでデータをメンバに展開する
     *
     * @return ルート情報リスト
     */
    public List<RouteData> readData(String filePath) {
        List<RouteData> routeDataList = new ArrayList<RouteData>();
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "RouteDataFile::readData()");
        List<String> fileLines = null;
        try {
            fileLines = readFile(filePath);
        } catch (IOException e) {
            return routeDataList;
        }

        if (fileLines == null || fileLines.isEmpty()) {
            return routeDataList;
        }

        boolean bSuccessReadData = false;
        boolean bIsInsideRouteDataTag = false;
        RouteData routeData = null;

        try {
            for (int i = 0; i < fileLines.size(); i++) {
                String currLine = fileLines.get(i);
//                NaviLog.d(NaviLog.PRINT_LOG_TAG, currLine);

                if (!bIsInsideRouteDataTag) {
                    if (currLine.indexOf(TAG_ROUTE_DATA_START) == 0) {
                        bIsInsideRouteDataTag = true;
                        bSuccessReadData = true;
                    }
                    continue;
                }

                if (currLine.indexOf(TAG_ROUTE_DATA_END) == 0) {
                    bIsInsideRouteDataTag = false;
                    break;
                }

                if (currLine.indexOf(TAG_Route_START) == 0) {
                    routeData = new RouteData();
                    List<String> attribList = new ArrayList<String>();
                    attribList.add("Date=\"");
                    attribList.add("Mode=\"");
                    attribList.add("Name=\"");
                    List<String> values = splitXmlTagAttrilbStr(currLine, attribList);
                    if (attribList.size() != values.size()) {
                        throw new IOException();
                    }

                    String strDate = values.get(0);
                    if (strDate.length() == 0) {
                        throw new IOException();
                    }
                    String strMode = values.get(1);
                    if (strMode.length() == 0) {
                        throw new IOException();
                    }

                    String strName = values.get(2);
                    if (strName.length() == 0) {
                        throw new IOException();
                    }

                    routeData.setRouteDate(strDate);
                    routeData.setRouteName(strName);
                    int mode = Integer.parseInt(strMode);
                    routeData.setRouteMode(mode);

                } else if (currLine.indexOf(TAG_Route_END) == 0) {
                    routeDataList.add(routeData);

                } else if (currLine.indexOf(TAG_Node) == 0) {
                    List<String> attribList = new ArrayList<String>();
                    attribList.add("Lon=\"");
                    attribList.add("Lat=\"");
                    attribList.add("Name=\"");
                    attribList.add("Status=\"");
                    List<String> values = splitXmlTagAttrilbStr(currLine, attribList);
                    if (attribList.size() != values.size()) {
                        throw new IOException();
                    }

                    String strLon = values.get(0);
                    if (strLon.length() == 0) {
                        throw new IOException();
                    }
                    String strLat = values.get(1);
                    if (strLat.length() == 0) {
                        throw new IOException();
                    }
                    String strName = values.get(2);
                    if (strName.length() == 0) {
                        throw new IOException();
                    }
                    String strStatus = values.get(3);
                    if (strStatus.length() == 0) {
                        throw new IOException();
                    }
                    if (routeData != null) {
                        routeData.addRouteNode(Integer.parseInt(strLat), Integer.parseInt(strLon), strName, Integer.parseInt(strStatus));
                    }
                }

            }
        } catch (IOException e) {
            bSuccessReadData = false;
        } catch (NumberFormatException e) {
            bSuccessReadData = false;
        }

        //タグを閉じてない場合はファイルが破損しているのでエラーとする
        if (bIsInsideRouteDataTag) {
            bSuccessReadData = false;
        }

        //読み込めないファイルは削除する
        if (!bSuccessReadData) {
            File file = new File(filePath);
            boolean result = file.delete();
            if (!result) {
                NaviLog.i(NaviLog.PRINT_LOG_TAG, "file delete error[" + filePath + "]");
            }
            routeDataList = new ArrayList<RouteData>();
        }
        return routeDataList;
    }

    /*
     * ファイルから指定日付のルート情報を削除する
     */
    public void deleteRoute(String filePath, String delRouteDate) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "RouteDataFile::deleteRoute()");

        List<RouteData> routeDataList = readData(filePath);

        //編集するデータは削除
        for (int i = 0; i < routeDataList.size(); i++) {
            RouteData routeData = routeDataList.get(i);
            if (delRouteDate != null && routeData.getRouteDate().equals(delRouteDate)) {
                routeDataList.remove(i);
                break;
            }
        }
        writeData(filePath, routeDataList);
    }

    /*
     * 保存日付からルート情報を取得する
     *
     * 見つからない場合は例外を返す
     */
    public RouteData getRouteData(List<RouteData> routeDataList, String saveDate) throws IllegalStateException {
        if (routeDataList == null) {
            throw new IllegalStateException();
        }

        for (int i = 0; i < routeDataList.size(); i++) {
            RouteData data = routeDataList.get(i);
            if (saveDate != null && data.getRouteDate().equals(saveDate)) {
                return data;
            }
        }
        throw new IllegalStateException();
    }

    /*
     * ファイルを読み込んで文字列リストを返す
     */
    private List<String> readFile(String filePath) throws IOException {
        BufferedReader bufferedReader = null;
        boolean bSuccessRead = false;
        List<String> lineList = new ArrayList<String>();
        try {

            File file = new File(filePath);
            InputStreamReader read = new InputStreamReader(new FileInputStream(file), "UTF-8");
            bufferedReader = new BufferedReader(read);
            String strLine = null;

            strLine = bufferedReader.readLine();
            while (null != strLine) {
                lineList.add(strLine);
                strLine = bufferedReader.readLine();
            }
            read.close();
            bSuccessRead = true;
        } catch (FileNotFoundException e) {
            bSuccessRead = false;
        } catch (UnsupportedEncodingException e) {
            bSuccessRead = false;
        } catch (NullPointerException e) {
            bSuccessRead = false;
        } catch (IOException e) {
            bSuccessRead = false;
        } catch (Exception e) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
            bSuccessRead = false;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
    				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
                }
            }
        }

        if (!bSuccessRead) {
            throw new IOException();
        }
        return lineList;
    }

    /*
     * ルート情報をファイルに保存する
     */
    public boolean writeRouteDataFile(String filePath, List<String> pDataList) {
        NaviLog.d(NaviLog.PRINT_LOG_TAG, "RouteDataFile::writeRouteDataFile()");

        boolean bWriteSuccess;
        File filename;
        RandomAccessFile randomAccessFile = null;
        filename = new File(filePath);
        String strTemp;
        try {
            randomAccessFile = new RandomAccessFile(filename, "rw");
            for (int i = 0; i < pDataList.size(); i++) {
//Del 2011/12/15 Z01_h_yamada Start -->
//                try {
//Del 2011/12/15 Z01_h_yamada End <--
                    strTemp = pDataList.get(i);
                    randomAccessFile.write(strTemp.getBytes("UTF-8"));
//Del 2011/12/15 Z01_h_yamada Start -->
//            } catch (IOException e) {
//				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
//            }
//Del 2011/12/15 Z01_h_yamada End <--
            }
            bWriteSuccess = true;
        } catch (IOException e1) {
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e1);
            bWriteSuccess = false;
        } finally {
            if (randomAccessFile != null) {
                try {
                    randomAccessFile.close();
                } catch (IOException e2) {
    				NaviLog.e(NaviLog.PRINT_LOG_TAG, e2);
                }
            }
        }
        return bWriteSuccess;
    }
}
