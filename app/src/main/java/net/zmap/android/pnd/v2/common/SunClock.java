package net.zmap.android.pnd.v2.common;

/**
 * @file SunClock.java
 * @author Kazuaki Oshiba <k_ooshiba@zenrin-datacom.net>
 */

import java.util.Calendar;

/**
 * 日の出日の入り計算（概算）クラス<br>
 *<code>
 *  SunClock sc = new SunClock(lat, lon);<br>
 *  sc.recalc(lat, lon);<br>
 *  if (sc.isDaylight()) { <br>
 *      // daymode <br>
 *  } else { <br>
 *   // nightmode <br>
 *  } <br>
 * </code>
 */
public class SunClock {

    /**
     * コンストラクタ
     * @param msLat    緯度（ミリ秒）
     * @param msLon    経度（ミリ秒）
     */
    public SunClock(int msLat, int msLon) {
        recalc(msLat, msLon);
    }

    /**
     * 日中かどうかを判定
     * @return 日中の場合は true を返す。
     */
    public boolean isDaylight() {
        Calendar now = Calendar.getInstance();
        return isDaylight(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), now.get(Calendar.SECOND));
    }

    /**
     * 日中かどうかを判定
     * @param hour  時(0-23)
     * @param min   分(0-59)
     * @param sec   秒(0-59)
     * @return 日中の場合は true を返す。
     */
    boolean isDaylight(int hour, int min, int sec) {
        int min_of_day = min + hour * 60;
        if (min_of_day < (m_sunrise - m_twilight / 2)) {
            return false;
        }
        if (min_of_day > (m_sunset + m_twilight / 2)) {
            return false;
        }
        return true;
    }

    /**
     * 判定条件を更新して再計算
     * @param msLat    緯度（ミリ秒）
     * @param msLon    経度（ミリ秒）
     */
    public void recalc(int msLat, int msLon) {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;
        int day = now.get(Calendar.DAY_OF_MONTH);
        recalc(msLat, msLon, year, month, day);
    }

    /**
     * 判定条件を更新して再計算
     * @param mslat    緯度（ミリ秒）
     * @param mslon    経度（ミリ秒）
     * @param year     年（西暦）
     * @param month    月（1-）
     * @param day      日（1-）
     */
    void recalc(int msLat, int msLon, int year, int month, int day)
    {
        double lat, lon;
        double tzone, date;
        double obliq, alpha, delta, equation, ha, hb, twx;
        double settm, riset;
        double L, g, lambda, LL;
        Calendar now = Calendar.getInstance();

        lat = (double)msLat / (60*60*1000);
        lon = (double)msLon / (60*60*1000);
        tzone = now.get(Calendar.ZONE_OFFSET) / (double)(60*60*1000);

        date = get_days_since_2000(year, month, day, 12);

        // 太陽の平均経度(mean longitude) を求める
        L = normalize_angle(280.461 * M_RADS + .9856474 * M_RADS * date);
        // 太陽の平均近点角(mean anomaly) を求める
        g = normalize_angle(357.528 * M_RADS + .9856003 * M_RADS * date);
        // 黄経(ecliptic longitude) を返す
        lambda = normalize_angle(L + 1.915 * M_RADS * Math.sin(g) + .02 * M_RADS * Math.sin(2 * g));

        obliq = 23.439 * M_RADS - .0000004 * M_RADS * date;

        alpha = Math.atan2(Math.cos(obliq) * Math.sin(lambda), Math.cos(lambda));
        delta = Math.asin(Math.sin(obliq) * Math.sin(lambda));

        LL = L - alpha;
        if (L < M_PI) {
            LL += 2.0 * M_PI;
        }
        equation = 1440.0 * (1.0 - LL / M_PI / 2.0);
        ha = calc_hourangle(lat, delta);
        hb = calc_twilight_hourangle(lat, delta);

        riset = 12.0 - 12.0 * ha / M_PI + tzone - lon / 15.0 + equation / 60.0;
        settm = 12.0 + 12.0 * ha / M_PI + tzone - lon / 15.0 + equation / 60.0;
        if (riset > 24.0) {
            riset -= 24.0;
        }
        if (settm > 24.0) {
            settm -= 24.0;
        }
        twx = 12.0 * (hb - ha) / M_PI;

        m_sunrise  = (int)(riset * 60);
        m_sunset   = (int)(settm * 60);
        m_twilight = (int)(twx * 60);
    }

    /**
     * 2000年1月1日0時からの経過日数を返す
     */
    private static double get_days_since_2000(int y, int m, int d, int h) {
        Calendar baseday = Calendar.getInstance();
        Calendar theday = Calendar.getInstance();
        baseday.set(2000, 0, 1);
        theday.set(y, m - 1, d, h, 0, 0);

        long diff = theday.getTimeInMillis() - baseday.getTimeInMillis();
        return diff / 1000.0 / 60.0 / 60.0 / 24.0;
    }

    /**
     * 角度を正規化する
     */
    private static double normalize_angle(double x) {
        double b = 0.5 * x / M_PI;
        double a = 2.0 * M_PI * (b - (long)(b));
        if (a < 0) {
            a = 2.0 * M_PI + a;
        }
        return a;
    }

    /**
     * 時角(hourangle) を計算
     */
    private static double calc_hourangle(double lat, double declin) {
        double fo, dfo;

        dfo = M_RADS * (0.5 * M_SUN_DIA + M_AIR_REFR);
        if (lat < 0.0) {
            dfo = -dfo;
        }
        fo = Math.tan(declin + dfo) * Math.tan(lat * M_RADS);
        if (fo > 0.99999) {
            fo = 1.0;
        }
        fo = Math.asin(fo) + M_PI / 2.0;
        return fo;
    }

    /**
     * 薄明(twilight)の時角(hourangle) を計算
     */
    private static double calc_twilight_hourangle(double lat, double declin) {
        double fi, df1;

        df1 = M_RADS * 6.0;
        if (lat < 0.0) {
            df1 = -df1;
        }
        fi = Math.tan(declin + df1) * Math.tan(lat * M_RADS);
        if (fi > 0.99999) {
            fi = 1.0;
        }
        fi = Math.asin(fi) + M_PI / 2.0;
        return fi;
    }

    private final static double M_PI = Math.PI;
    private final static double M_RADS = M_PI / 180.0;
    private final static double M_SUN_DIA = 0.53f;
    private final static double M_AIR_REFR = 0.34f / 60.0f;

    private int m_sunrise;  // 日の出（0時からの経過分）
    private int m_sunset;   // 日の入り（0時からの経過分）
    private int m_twilight; // 薄明時間（分）
}

