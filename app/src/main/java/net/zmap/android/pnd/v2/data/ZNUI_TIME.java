/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZNUI_TIME.java
 * Description    時計情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZNUI_TIME {
    private int nHouer = 0;
    private int nMinute = 0;
    /**
    * Created on 2010/08/06
    * Title:       getNHouer
    * Description:  時間を取得する
    * @param1  無し
    * @return       String
    * @author         int
    * @version        1.0
    */
    public int getNHouer() {
        return nHouer;
    }
    /**
    * Created on 2010/08/06
    * Title:       getNMinute
    * Description:  分を取得する
    * @param1  無し
    * @return       String
    * @author         int
    * @version        1.0
    */
    public int getNMinute() {
        return nMinute;
    }
}
