package net.zmap.android.pnd.v2.data;

import java.util.Map;

public class DC_ContentsDetailData {
	public String 	m_sURL;				//検索URL
	public String 	m_sContentsID;		//コンテンツID
    public Map<String , DC_MapIconData> m_mapIconList;	//アイコンリスト
}
