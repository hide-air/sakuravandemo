package net.zmap.android.pnd.v2.widget;

import android.content.Context;
import android.graphics.PointF;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * Widgetの場所とID値を保持します。
 */
public class WidgetSaver {

    /** Widget の場所定義 */
    enum WidgetLocation {
        RIGHT,
        TOP
    }

    /** インスタンス */
    private static WidgetSaver               mInstance;

    /** WidgetのIDを保存するファイル名 */
    private static final String              WIDGET_ID_FILE_NAME   = "widgetid.txt";

    /** Widgetのサイズを保存するファイル名 */
    private static final String              WIDGET_SIZE_FILE_NAME = "widgetsize.txt";

    /** コンテキスト */
    private Context                          mContext;

    /** IDを保持するテーブル */
    private HashMap<WidgetLocation, Integer> mIdTable;

    /** サイズを保持するテーブル */
    private HashMap<WidgetLocation, PointF>  mSizeTable;

    /**
     * インスタンスを取得します。
     *
     * @param context
     * @return
     */
    public static WidgetSaver getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new WidgetSaver(context);
        }
        return mInstance;
    }

    /**
     * コンストラクタ
     *
     * @param context
     */
    public WidgetSaver(Context context) {
        mContext = context;
        mIdTable = new HashMap<WidgetLocation, Integer>();
        mSizeTable = new HashMap<WidgetLocation, PointF>();
    }

    /**
     * ファイルより読み込みます。
     */
    public void load() {
        loadWidgetId();
        loadWidgetSize();
    }

    /**
     * WidgetのIdを読み込みます。
     */
    public void loadWidgetId() {
        FileInputStream is = null;
        try {
            is = mContext.openFileInput(WIDGET_ID_FILE_NAME);
        } catch (FileNotFoundException nffe) {
            /* ファイルが存在しない場合はそのまま、返却 */
            Log.w("WidgetSaver.loadWidgetId", "File not found : " + WIDGET_ID_FILE_NAME);
            return;
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        try {
            String line = null;
            int counter = 0;
            while ((line = reader.readLine()) != null) {
                processWidgetIdLine(line, counter++);
            }

        } catch (IOException ioe) {
            Log.e("WidgetSaver.loadWidgetId", "Failed to read " + WIDGET_ID_FILE_NAME);

        } finally {
            try {
                reader.close();
                is.close();
            } catch (IOException ioe) {
            }
        }
    }

    /**
     * Widgetのサイズを読み込みます。
     */
    public void loadWidgetSize() {
        FileInputStream is = null;
        try {
            is = mContext.openFileInput(WIDGET_SIZE_FILE_NAME);
        } catch (FileNotFoundException nffe) {
            /* ファイルが存在しない場合はそのまま、返却 */
            Log.w("WidgetSaver.loadWidgetSize", "File not found : " + WIDGET_SIZE_FILE_NAME);
            return;
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        try {
            String line = null;
            int counter = 0;
            while ((line = reader.readLine()) != null) {
                processWidgetSizeLine(line, counter++);
            }

        } catch (IOException ioe) {
            Log.e("WidgetSaver.loadWidgetSize", "Failed to read " + WIDGET_SIZE_FILE_NAME);

        } finally {
            try {
                reader.close();
                is.close();
            } catch (IOException ioe) {
            }
        }

    }

    /**
     * 1行文字列をパースし、テーブルに読み込みます。
     * key = value 形式、key は WidgetLocationでの定義名、valueはWidgetのId
     *
     * @param line
     *            行文字列
     * @param counter
     *            行番号
     */
    private void processWidgetIdLine(String line, int counter) {
        String[] elements = line.split("=");
        if (elements.length != 2) {
            Log.w("WidgetSaver.processLine", "Line : " + counter + ", Violating key=value restriction ignored.");
            return;
        }

        WidgetLocation location = null;
        Integer id = null;

        try {
            location = WidgetLocation.valueOf(elements[0].trim());
        } catch (IllegalArgumentException iae) {
            Log.w("WidgetSaver.processLine", "Line : " + counter + ", Failed to recognize : " + elements[0] + ", ignored.");
            return;
        }

        try {
            id = Integer.parseInt(elements[1].trim());
        } catch (NumberFormatException nfe) {
            Log.w("WidgetSaver.processLine", "Line : " + counter + ", Failed to prase integer : " + elements[1] + ", ignored.");
        }

        mIdTable.put(location, id);
        Log.d("WidgetSaver.processLine", "mIdTable.put(" + location + ", " + id + ")");
    }

    /**
     * 1行文字列をパースし、テーブルに読み込みます。
     * key = value 形式、key は WidgetLocationでの定義名、valueはカンマ区切りのWidgetのサイズ
     *
     * @param line
     *            行文字列
     * @param counter
     *            行番号
     */
    private void processWidgetSizeLine(String line, int counter) {
        String[] elements = line.split("=");
        if (elements.length != 2) {
            Log.w("WidgetSaver.processLine", "Line : " + counter + ", Violating key=value restriction ignored.");
            return;
        }

        WidgetLocation location = null;

        try {
            location = WidgetLocation.valueOf(elements[0].trim());
        } catch (IllegalArgumentException iae) {
            Log.w("WidgetSaver.processLine", "Line : " + counter + ", Failed to recognize : " + elements[0] + ", ignored.");
            return;
        }

        String[] wh = elements[1].trim().split(",");
        if (wh.length != 2) {
            Log.w("WidgetSaver.processWidgetSizeLine", "Line : " + counter + ", Violationg value,value restriction ignored.");
        }

        float width, height;

        try {
            width = Float.parseFloat(wh[0].trim());
        } catch (NumberFormatException nfe) {
            Log.w("WidgetSaver.processLine", "Line : " + counter + ", Failed to prase integer : " + elements[1] + ", ignored.");
            return;
        }

        try {
            height = Float.parseFloat(wh[1].trim());
        } catch (NumberFormatException nfe) {
            Log.w("WidgetSaver.processLine", "Line : " + counter + ", Failed to prase integer : " + elements[1] + ", ignored.");
            return;
        }

        mSizeTable.put(location, new PointF(width, height));
        Log.d("WidgetSaver.processLine", "mSizeTable.put(" + location + ", new PointF(" + width + ", " + height + "))");
    }

    /**
     * ファイルへ書き込みます。
     */
    public void save() {
        saveWidgetId();
        saveWidgetSize();
    }

    /**
     * WidgetのIdを書きこみます。
     */
    public void saveWidgetId() {
        FileOutputStream os = null;
        try {
            os = mContext.openFileOutput(WIDGET_ID_FILE_NAME, Context.MODE_WORLD_READABLE | Context.MODE_WORLD_WRITEABLE);
        } catch (IOException ioe) {
            return;
        }

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new OutputStreamWriter(os));
            for (Entry<WidgetLocation, Integer> entry : mIdTable.entrySet()) {
                String line = String.format("%s=%d", entry.getKey().toString(), entry.getValue());
                Log.d("WidgetSaver", "saveWidgetId line[" + line + "]");
                writer.println(line);
            }
            writer.flush();
        } finally {
            try {
                writer.close();
                os.close();
            } catch (IOException ioe) {
            }
        }
    }

    /**
     * Widgetのサイズを書きこみます。
     */
    public void saveWidgetSize() {
        FileOutputStream os = null;
        try {
            os = mContext.openFileOutput(WIDGET_SIZE_FILE_NAME, Context.MODE_WORLD_READABLE | Context.MODE_WORLD_WRITEABLE);
        } catch (IOException ioe) {
            return;
        }

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new OutputStreamWriter(os));
            for (Entry<WidgetLocation, PointF> entry : mSizeTable.entrySet()) {
                PointF size = entry.getValue();
                String line = String.format("%s=%02f,%02f", entry.getKey().toString(), size.x, size.y);
                Log.d("WidgetSaver", "saveWidgetSize line[" + line + "]");
                writer.println(line);
            }
            writer.flush();
        } finally {
            try {
                writer.close();
                os.close();
            } catch (IOException ioe) {
            }
        }

    }

    /**
     * WidgetのIdを設定します。
     *
     * @param location
     *            Widget の場所
     * @param id
     *            WidgetのId
     */
    public void setWidget(WidgetLocation location, int id) {
        mIdTable.put(location, id);
        Log.d("WidgetSaver.setWidget", "mIdTable.put(" + location + ", " + id + ")");
        save();
    }

    /**
     * WidgetのIdを取得します。
     *
     * @param location
     *            Widget の場所
     * @return Widget のId、存在しない場合は -1
     */
    public int getWidget(WidgetLocation location) {
        Integer id = mIdTable.get(location);
        if (id == null) {
            return -1;
        }

        return id.intValue();
    }

    /**
     * Widgetのサイズを設定します。
     *
     * @param location
     *            Widget の場所
     * @param size
     *            サイズ
     */
    public void setWidgetSize(WidgetLocation location, PointF size) {
        mSizeTable.put(location, size);
        Log.d("WidgetSaver.setWidgetSize", "mSizeTable.put(" + location + ", PointF(" + size.x + ", " + size.y + "))");
    }

    /**
     * Widgetのサイズを取得します。
     *
     * @param location
     *            Widgetの場所
     * @return サイズ
     */
    public PointF getWidgetSize(WidgetLocation location) {
        PointF size = mSizeTable.get(location);
        return size;
    }

    /**
     * 保存内容をクリアします。
     */
    public void clear() {
        mIdTable.clear();
        mSizeTable.clear();
        save();
    }
}