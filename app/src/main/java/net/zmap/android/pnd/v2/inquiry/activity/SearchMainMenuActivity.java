//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************
package net.zmap.android.pnd.v2.inquiry.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import net.zmap.android.pnd.v2.R;
// Add by CPJsunagawa '15-05-13 Start
import net.zmap.android.pnd.v2.NaviApplication;
// Add by CPJsunagawa '15-05-13 End
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.utils.ScrollBox;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.inquiry.view.ButtonImg;
import net.zmap.android.pnd.v2.inquiry.view.InputText;
import net.zmap.android.pnd.v2.maps.OpenMap;

/**
 * K-T0_目的地検索メニュー
 *
 * */
public class SearchMainMenuActivity extends InquiryBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater oInflater = LayoutInflater.from(this);
        // 画面を追加
        LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.search_mainmenu_01, null);
        ScrollBox oBox = (ScrollBox)oLayout.findViewById(R.id.search_main_menu);

/* 元々あった古澤さんコード　Start
        // added by furusawa
        ButtonImg btn = (ButtonImg)oLayout.findViewById(R.id.btn_vehicle_search);
        btn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                goVehicleSearch();
            }
        });
        btn = (ButtonImg)oLayout.findViewById(R.id.btn_delivery_search);
        btn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //goVehicleSearch();
            	goAddressSearch();
            }
        });
        // end of added code
 元々あった古澤さんコード　End　*/

// Add by CPJsunagawa '13-12-25 Start
    	// 配達コースボタン
        ButtonImg btn = (ButtonImg)oLayout.findViewById(R.id.btn_vehicle_search);
        //btn = (ButtonImg)oLayout.findViewById(R.id.btn_vehicle_search);
        btn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                goVehicleSearch();
            }
        });

        // 配達先ボタン
        btn = (ButtonImg)oLayout.findViewById(R.id.btn_delivery_search);
        btn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //goVehicleSearch();
// Add by CPJsunagawa '15-05-13 Start
            	clearList();
// Add by CPJsunagawa '15-05-13 End
            	goAddressSearch();
            }
        });
/*
// 2013/10/17 M.Suna Start -->
        // NOSAI向け処理の初期化
        initLayout_for_nosai(oLayout);
// 2013/10/17 M.Suna End <--
*/
// Add by CPJsunagawa '13-12-25 End

    	//50音順検索と住所検索の初期化
        initLayout01(oLayout);

        //施設、ジャンル、郵便番号、電話番号、周辺、先程の地図　検索処理の初期化
        initLayout02(oLayout);

        ScrollTool oTool = new ScrollTool(this);
        oTool.bindView(oBox);

        setViewInOperArea(oTool);
        setViewInWorkArea(oLayout);
    }

    /**
     * 施設、ジャンル、郵便番号、電話番号、周辺、先程の地図　検索処理の初期化
     *
     * @param oLayout
     *            レイアウト対象
     *
     * */

    private void initLayout02(LinearLayout oLayout) {
        Button btn = settingImageButton(oLayout, R.id.btn_genre_search, R.drawable.genre_icon_40_40, R.id.image_btn_genre_search);
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //K-J0_ジャンル大分類
                goGenreSearch();

            }

        });

        btn = settingImageButton(oLayout, R.id.btn_postalcode_search, R.drawable.postalcode_icon_40_40, R.id.image_btn_postcode_search);
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //K-Z0_郵便番号入力
                goPostalCodeSearch();
            }

        });
        btn = settingImageButton(oLayout, R.id.btn_telphone_search, R.drawable.tel_icon_40_40, R.id.image_btn_telephone_search);
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //K-P0 電話番号入力
                goTelphoneSearch();

            }

        });
        btn = settingImageButton(oLayout, R.id.btn_around_search, R.drawable.around_icon_40_40, R.id.image_btn_arround_search);
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //K-T1 周辺検索メニュー
                goAroundSearch();

            }

        });
        btn = settingImageButton(oLayout, R.id.btn_mapsearch_search, R.drawable.mapsearch_icon_40_40, R.id.image_btn_mapsearch_search);
//Chg 2011/09/13 Z01_h_yamada End <--
        Intent oIntent = getIntent();
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            btn.setVisibility(Button.VISIBLE);
            btn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ルート編集・地図から探す禁止 Start -->
            		if(DrivingRegulation.CheckDrivingRegulation()){
            			return;
            		}
// ADD 2013.08.08 M.Honma 走行規制 ルート編集・地図から探す禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
                    //M-M3_地点地図
                    POIData poi = new POIData();
                    // XuYang add start #1056
                    CommonLib.setChangePosFromSearch(true);
                    CommonLib.setChangePos(false);
                    // XuYang add end #1056
                    OpenMap.moveMapTo(SearchMainMenuActivity.this, poi, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
                }

            });
        } else {
            LinearLayout layoutMapSearch = (LinearLayout)oLayout.findViewById(R.id.LinearLayout_mapsearch);
            layoutMapSearch.setVisibility(LinearLayout.GONE);
        }

    }

    /**
     * 50音順検索と住所検索の初期化
     *
     * @param oLayout
     *            レイアウト対象
     *
     * */
    private void initLayout01(LinearLayout oLayout) {
        // 50音検索
    	Button btn = settingImageButton(oLayout, R.id.btn_freeword_search, R.drawable.search_icon_40_40, R.id.image_btn_freeword_search);
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                //K-N0 名称入力
                goFreewordSearch();
            }

        });

        // 住所検索
        btn = settingImageButton(oLayout, R.id.btn_address_search, R.drawable.address_icon_40_40, R.id.image_btn_address_search);
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                //K-A0 都道府県
                goAddressSearch();
            }

        });

        // 履歴検索
        btn = settingImageButton(oLayout, R.id.btn_history_search, R.drawable.history_icon_40_40, R.id.image_btn_history_search);
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                //K-H0 履歴検索
                goHistorySearch();
            }

        });
        // お気に入り検索
        btn = settingImageButton(oLayout, R.id.btn_bookmark_search, R.drawable.favorites_icon_40_40,R.id.image_btn_bookmark_search);
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                //K-F0 お気に入り一覧
                goFavoritesGenreSearch();
            }

        });
    }

    /**
     * 元の実装が、m_wPaddingLeft + m_wIconW + m_wIconSpace と<br>
     * {ButtonImg} 内、及びコンストラクタでリテラル指定になっていたのでソレに準じる
     */
    private static final int IMAGE_BUTTON_PADDING_LEFT = 10 + 5;
    private static final int IMAGE_BUTTON_PADDING_TOP = 10;
    private static final int IMAGE_BUTTON_PADDING_RIGHT = 10;
    private static final int IMAGE_BUTTON_PADDING_BUTTOM = 30;

    /**
     * 検索メニューの各種ボタン、及びボタン上のアイコンPaddingなどを設定する。
     * @param layout 定義先のレイアウトObject
     * @param buttonId ボタンのレイアウトID
     * @param drawableIconId 設定するアイコンのID
     * @param imageViewId ボタン上のImageViewのレイアウトID
     * @return
     *
     * @see ButtonImg#resetIconSize
     */
    private Button settingImageButton(LinearLayout layout, int buttonId, int drawableIconId, int imageViewId) {
    	int iconSize = getResources().getDimensionPixelSize(R.dimen.SearchMenu_Button_iconsize);
    	// ボタンのラベルのPaddingを設定
    	Button btn = (Button)layout.findViewById(buttonId);
    	btn.setPadding(IMAGE_BUTTON_PADDING_LEFT + iconSize
    			,IMAGE_BUTTON_PADDING_TOP
    			,IMAGE_BUTTON_PADDING_RIGHT
    			,IMAGE_BUTTON_PADDING_BUTTOM);
        Bitmap bitmap =  BitmapFactory.decodeResource(getResources(),drawableIconId);
        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, iconSize, iconSize, true);
    	ImageView image = (ImageView)layout.findViewById(imageViewId);
    	image.setImageBitmap(scaled);

    	return btn;
    }
    /**
     * K-N0 名称入力へ遷移する
     *
     * */
    private void goFreewordSearch() {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・50音禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・50音禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        Intent oIntent = new Intent(getIntent());
        oIntent.setClass(this, KeyInquiry.class);
        // XuYang add start #1056
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        // XuYang add end #1056
        oIntent.putExtra(Constants.PARAMS_MIN_LIMITED, 2);
        oIntent.putExtra(Constants.PARAMS_MAX_LIMITED, 40);
//Del 2011/09/17 Z01_h_yamada Start -->
//	    oIntent.putExtra(Constants.PARAMS_TITLE, R.drawable.menu_keysearch);
//Del 2011/09/17 Z01_h_yamada End <--
        oIntent.putExtra(Constants.FROM_NAVI_SEARCH, InputText.KEY);
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_KEYINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_KEYINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--

    }

    /**
     * K-A0 都道府県へ遷移する
     *
     * */
    private void goAddressSearch() {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・住所禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・住所禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        Intent oIntent = new Intent(getIntent());
        oIntent.setClass(this, AddressProvinceInquiry.class);
        // XuYang add start #1056
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        // XuYang add end #1056
//		NaviRun.GetNaviRunObj().setiActivityId(AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY);
        NaviRun.GetNaviRunObj().setSearchKind(AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY);
        oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, this.getResources().getString(R.string.address_title));
//Del 2011/09/17 Z01_h_yamada Start -->
//		oIntent.putExtra(Constants.PARAMS_TITLE, R.drawable.menu_address);
//Del 2011/09/17 Z01_h_yamada End <--
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
    }

    /**
     * K-F0 お気に入り一覧へ遷移する
     * */
    private void goFavoritesGenreSearch() {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・お気に入り禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・お気に入り禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        Intent oIntent = new Intent(getIntent());
        oIntent.setClass(this, FavoritesGenreInquiry.class);
        // XuYang add start #1056
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        // XuYang add end #1056
//Del 2011/09/17 Z01_h_yamada Start -->
//		oIntent.putExtra(Constants.PARAMS_TITLE, R.drawable.menu_bookmark);
//Del 2011/09/17 Z01_h_yamada End <--
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_FAVORITESGENREINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_FAVORITESGENREINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--

    }

    /**
     * K-J0_ジャンル大分類へ遷移する
     * */
    private void goGenreSearch() {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・施設/ｼﾞｬﾝﾙ禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        Intent oIntent = new Intent(getIntent());
        // XuYang add start #1056
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        // XuYang add end #1056
        oIntent.setClass(this, GenreInquiryKind.class);
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_GENREINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_GENREINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
    }

    /**
     * K-Z0 郵便番号入力へ遷移する
     *
     * */
    private void goPostalCodeSearch() {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・郵便番号禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・郵便番号禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        Intent oIntent = new Intent(getIntent());
        oIntent.setClass(this, PostalCodeInquiry.class);
        // XuYang add start #1056
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        // XuYang add end #1056
        oIntent.putExtra(Constants.PARAMS_MIN_LIMITED, 7);
        oIntent.putExtra(Constants.PARAMS_MAX_LIMITED, 7);
//Del 2011/09/17 Z01_h_yamada Start -->
//	    oIntent.putExtra(Constants.PARAMS_TITLE, R.drawable.menu_postalcode);
//Del 2011/09/17 Z01_h_yamada End <--
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_POSTALCODEINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_POSTALCODEINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--
    }

    /**
     * K-P0 電話番号入力へ遷移する
     * */
    private void goTelphoneSearch() {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・電話番号禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・電話番号禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        Intent oIntent = new Intent(getIntent());
        oIntent.setClass(this, TelInquiry.class);
        // XuYang add start #1056
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        // XuYang add end #1056
//Del 2011/09/17 Z01_h_yamada Start -->
//	    oIntent.putExtra(Constants.PARAMS_TITLE, R.drawable.menu_phone);
//Del 2011/09/17 Z01_h_yamada End <--
        oIntent.putExtra(Constants.PARAMS_MIN_LIMITED, 8);
        oIntent.putExtra(Constants.PARAMS_MAX_LIMITED, 12);
        oIntent.putExtra(Constants.PARAMS_SEARCH_TYPE, TYPE_TELNUMS);
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_TELINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_TELINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--

    }

    /**
     * K-T1 周辺検索メニューへ遷移する
     *
     * */
    private void goAroundSearch() {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・周辺禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・周辺禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        // XuYang add start
        Intent oIntent = new Intent();
        CommonLib.setAroundFlg(false);
        oIntent.setClass(this, AroundSearchMainMenuActivity.class);
        // XuYang add start #1056
        Intent oldIntent = getIntent();
        if (null != oldIntent && oldIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        // XuYang add end #1056
        oIntent.putExtra(Constants.FROM_FLAG_KEY, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_LOCAL_MAINMENU);
//Chg 2011/09/23 Z01yoneya End <--
        // XuYang add end
    }

    /**
     * K-H0 履歴検索へ遷移する
     * */
    private void goHistorySearch() {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 目的地・履歴禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 目的地・履歴禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        Intent oIntent = new Intent(getIntent());
        oIntent.setClass(this, HistoryInquiry.class);
        // XuYang add start #1056
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        // XuYang add end #1056
//Del 2011/09/17 Z01_h_yamada Start -->
//	    oIntent.putExtra(Constants.PARAMS_TITLE, R.drawable.title_history_search);
//Del 2011/09/17 Z01_h_yamada End <--
//Chg 2011/09/23 Z01yoneya Start -->
//        startActivityForResult(oIntent, AppInfo.ID_ACITIVITY_HISTORYINQUIRY);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACITIVITY_HISTORYINQUIRY);
//Chg 2011/09/23 Z01yoneya End <--

    }

/*
// Add by CPJsunagawa '13-10-17  Start -->
    /**
     * NOSAI向け検索画面にへ遷移する
     * *
    private void goNosaiSearch(int nosai_jigyo_code) {
        Intent oIntent = new Intent(getIntent());
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        oIntent.setClass(this, NosaiInquiry.class);
        oIntent.putExtra("Jigyo", nosai_jigyo_code); // Jigyoコード
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_NOSAI_INQUIRY);
    }
// Add by CPJsunagawa '13-10-17  End
*/

/*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent oIntent) {
        if (resultCode == RESULT_CANCELED) {
            if (requestCode == AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY) {
                JNILong Treeindex = new JNILong();
                hashSelectedPoi.clear();
                NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetPrevTreeIndex(Treeindex);
                NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchPrevList();
                NaviRun.GetNaviRunObj().JNI_NE_POIAddrKey_Clear();
                NaviRun.GetNaviRunObj().JNI_NE_POIAddr_Clear();
            }
        }
        super.onActivityResult(requestCode, resultCode, oIntent);
    }
*/

// Add by CPJsunagawa '13-12-25 Start
    private void goVehicleSearch() {
        Intent oIntent = new Intent(getIntent());
        oIntent.setClass(this, VehicleInquiry.class);
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            oIntent.putExtra(Constants.ROUTE_FLAG_KEY, Constants.FROM_ROUTE_ADD);
        }
        NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_VEHICLE_INQUIRY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent oIntent) {
        if (resultCode == RESULT_CANCELED) {
            if (requestCode == AppInfo.ID_ACTIVITY_ADDRESSPROVINCEINQUIRY) {
                JNILong Treeindex = new JNILong();
                hashSelectedPoi.clear();
                NaviRun.GetNaviRunObj().JNI_NE_POIAddr_GetPrevTreeIndex(Treeindex);
                NaviRun.GetNaviRunObj().JNI_NE_POIAddr_SearchPrevList();
                NaviRun.GetNaviRunObj().JNI_NE_POIAddrKey_Clear();
                NaviRun.GetNaviRunObj().JNI_NE_POIAddr_Clear();
            }
        }else if(resultCode == Constants.RESULTCODE_VEHICLE_ROUTE_DETERMINATION) {
            System.out.println("RESULTCODE_VEHICLE_ROUTE_DETERMINATION");
            this.setResult(Constants.RESULTCODE_VEHICLE_ROUTE_DETERMINATION);
            //this.setResult(Constants.RESULTCODE_CHECK_PLACE);
            this.finish();
        }
        else if(resultCode == Constants.RESULTCODE_CHECK_PLACE) {
            System.out.println("RESULTCODE_CHECK_PLACE");
            this.setResult(Constants.RESULTCODE_CHECK_PLACE);
            //this.setResult(Constants.RESULTCODE_CHECK_PLACE);
            this.finish();
        }
// Add by CPJsunagawa '2015-07-08 Start
        else if(resultCode == Constants.RESULTCODE_REGIST_ICON) {
            System.out.println("RESULTCODE_REGIST_ICON");
            this.setResult(Constants.RESULTCODE_REGIST_ICON);
            this.finish();
        }
        else if(resultCode == Constants.RESULTCODE_INPUT_TEXT) {
            System.out.println("RESULTCODE_INPUT_TEXT");
            this.setResult(Constants.RESULTCODE_INPUT_TEXT);
            this.finish();
        }
// Add by CPJsunagawa '2015-07-08 End

    	super.onActivityResult(requestCode, resultCode, oIntent);
    }
// Add by CPJsunagawa '13-12-25 End

// Add by CPJsunagawa '15-05-13 Start
    private void clearList()
    {
    	NaviApplication app = (NaviApplication)getApplication();
    	app.allClearList();
    }
// Add by CPJsunagawa '15-05-13 End
}
