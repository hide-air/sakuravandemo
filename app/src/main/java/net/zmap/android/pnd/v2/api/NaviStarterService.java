package net.zmap.android.pnd.v2.api;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

public class NaviStarterService extends Service {

    protected static final String TAG = "NaviStarterService";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate()");
        IntentFilter filter = new IntentFilter(NaviIntent.ACTION_NAVI_START);
        registerReceiver(mNaviStartReceiver, filter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        unregisterReceiver(mNaviStartReceiver);
        super.onDestroy();
    }

    protected BroadcastReceiver mNaviStartReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive()");
            stopSelf();
        }
    };
}
