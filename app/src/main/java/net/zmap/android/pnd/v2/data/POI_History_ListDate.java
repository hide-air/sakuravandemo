/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           POI_History_FilePath.java
 * Description    日付リスト
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class POI_History_ListDate {
    private long m_lDate;

    /**
    * Created on 2010/08/06
    * Title:       getM_lDate
    * Description:  日付リストを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lDate() {
        return m_lDate;
    }
}
