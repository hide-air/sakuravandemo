package net.zmap.android.pnd.v2.common.utils;

public interface ScrollControlListener
{
	public void update();
	public void update(int wPos,float wAreaHeight,int wHeight);
}
