package net.zmap.android.pnd.v2.common;

/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ImageManager.java
 * Description   　  無し
 * Created on     2009/12/30
 *
 ********************************************************************
 */
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;


/**
 * イメージの関連メソッド
 * @author wangdong
 *
 */
public class ImageManager {

    private final static int ALPHA_COLOR_VALUE = 0x000F0000;
    private final static int COLOR_VALUE = 0x00080000;
    /**
     * Created on 2009/12/30
     * Description: Bitmapを取得する
     *
     * @param Resources resources
     * @param drawableId drawableId
     * @return Bitmap

     */
    public Bitmap getBitmap(Resources resources, int drawableId){
        Bitmap bitmap = BitmapFactory.decodeResource(resources, drawableId);
        return bitmap;
    }
    /**
     * Created on 2009/12/30
     * Description: Bitmapに変換する
     *
     * @param Bitmap  bitmap
     * @param int x
     * @param int y
     * @param int width
     * @param int height
     * @return Bitmap

     */
    public Bitmap getAlphaImage(Bitmap bitmap, int x, int y, int width, int height) {
        int[] argb = new int[width * height];
        if(false == bitmap.isRecycled()){
        bitmap.getPixels(argb, 0, width, x, y, width, height);
        }
        for (int i = 0; i < argb.length; i++) {
            if ((argb[i] & ALPHA_COLOR_VALUE) == COLOR_VALUE) {
                argb[i] = argb[i] & ALPHA_COLOR_VALUE;
            }
        }

//Chg 2012/02/09 Z01_h_yamada Start -->
//      return Bitmap.createBitmap(argb, width, height,
//		 Config.ARGB_8888);
//--------------------------------------------
        Bitmap out;

        try {
        	out = Bitmap.createBitmap(argb, width, height, Config.ARGB_8888);
        } catch ( OutOfMemoryError e ) {
        	NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        	java.lang.System.gc();
        	// 2回目は try-catch せず、2回目も失敗した時は諦める
            try {
            	out = Bitmap.createBitmap(argb, width, height, Config.ARGB_8888);
            } catch( OutOfMemoryError ex ) {
            	return bitmap;
            }
        }

        return out;
//Chg 2012/02/09 Z01_h_yamada End <--
    }
//Del 2012/02/09 Z01_h_yamada Start -->
//    /**
//     * Created on 2009/12/30
//     * Description: Bitmapに変換する
//     *
//     * @param Bitmap  bitmap
//     * @param int x
//     * @param int y
//     * @param int width
//     * @param int height
//     * @return Bitmap
//
//     */
//    public Bitmap getImage(Bitmap bitmap, int x, int y, int width, int height) {
//        int[] argb = new int[width * height];
//        if(false == bitmap.isRecycled()){
//        bitmap.getPixels(argb, 0, width, x, y, width, height);
//        }
//
//        return Bitmap.createBitmap(argb, width, height,
//                Config.ARGB_8888);
//    }
//Del 2012/02/09 Z01_h_yamada End <--
    /**
     * Created on 2009/12/30
     * Description: Bitmapを取得する
     *
     * @param int[] argb
     * @param int width
     * @param int height
     * @return Bitmap

     */
    public static Bitmap getImage(int[] argb, int width, int height){
//Chg 2012/02/09 Z01_h_yamada Start -->
//        return Bitmap.createBitmap(argb, width, height,
//                Config.ARGB_8888);
//--------------------------------------------
        Bitmap out;

        try {
        	out = Bitmap.createBitmap(argb, width, height, Config.ARGB_8888);
        } catch ( OutOfMemoryError e ) {
        	NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
        	java.lang.System.gc();
        	// 2回目は try-catch せず、2回目も失敗した時は諦める
            try {
            	out = Bitmap.createBitmap(argb, width, height, Config.ARGB_8888);
            } catch( OutOfMemoryError ex ) {
            	return null;
            }
        }

        return out;
//Chg 2012/02/09 Z01_h_yamada End <--

    }
}
