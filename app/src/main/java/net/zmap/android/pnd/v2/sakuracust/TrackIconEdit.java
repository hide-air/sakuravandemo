package net.zmap.android.pnd.v2.sakuracust;

import java.io.File;
import java.util.List;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.app.NaviAppDataPath;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.activity.MenuBaseActivity;
import net.zmap.android.pnd.v2.common.utils.StorageUtil;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.inquiry.activity.VehicleInquiry;
import net.zmap.android.pnd.v2.maps.MapActivity;
import net.zmap.android.pnd.v2.sakuracust.DrawTrackActivity.DbHelper;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;

public class TrackIconEdit extends MenuBaseActivity {

    // 図形種別
    public static final int FIGURE_KIND_ICON = 1;
    public static final int FIGURE_KIND_LINE = 2;

    // アイコン編集オペレーション
    public static final int PRESSED_NEW_ICON = 101;
    public static final int PRESSED_MOVE_ICON = 102;
    public static final int PRESSED_DELETE_ICON = 103;
    public static final int PRESSED_DRAW_LINE = 104;
    public static final int PRESSED_SELECT_TRACK = 105;
    public static final int PRESSED_CLEAR_TRACK = 106;
    public static final int PRESSED_EDIT_TRACK = 107;  // 2016/01/07 Yamamoto Add

    private Context mCtx;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setContentView(R.layout.track_icon_edit);
        
    	System.out.println("***　TrackIconEdit onCreate　***");
        
        LayoutInflater oInflater = LayoutInflater.from(this);
        // 画面を追加
        LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.track_icon_edit, null);

        // 軌跡表示ボタン
        View btnDisp = oLayout.findViewById(R.id.track_disp);
        btnDisp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	System.out.println("***　軌跡表示 onCreate　***");

            	// 軌跡の表示切換え
                JNILong showFlag = new JNILong();
                NaviRun.GetNaviRunObj().JNI_NE_GetShowOnlyVehicleTrackFlag(showFlag);

                // 2016/01/07 Yamamoto Mod Start
                /*
                if (showFlag.getLcount() == 0) {
                	NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(1);
                } else {
                	NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(0);
                }
                */
                if (showFlag.getLcount() != 0) {
                    NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(0);
                }
                // 2016/01/07 Yamamoto Mod End

            	System.out.println("***　JNI_NE_SetShowOnlyVehicleTrackFlag Exec　***");

            	finish();
            }});
        
// Add 20151206 Start
    	// 軌跡データ選択ボタン
        View btnSelect = oLayout.findViewById(R.id.track_select);
        btnSelect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	System.out.println("***　軌跡データ選択 onCreate　***");

                setResult(PRESSED_SELECT_TRACK, new Intent());
                finish();

/*
            	// 軌跡データの選択
//            	TrackSelectActivity.show
        		System.out.println("Intentで、TrackSelectActivity呼び出し前");
                Intent intent = new Intent(TrackIconEdit.this, TrackSelectActivity.class);
                Bundle bundle = new Bundle();
//                bundle.putString("path", file.getPath());
                intent.putExtras(bundle);
        		System.out.println("TegakiMemoActivityに、path => " + bundle + " を渡す");
                startActivityForResult(intent, AppInfo.ID_ACTIVITY_TEGAKI_MEMO);

            	JNILong showFlag = new JNILong();
                NaviRun.GetNaviRunObj().JNI_NE_GetShowOnlyVehicleTrackFlag(showFlag);
                if (showFlag.getLcount() == 0) {
                	NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(1);
                } else {
                	NaviRun.GetNaviRunObj().JNI_NE_SetShowOnlyVehicleTrackFlag(0);
                }
            	System.out.println("***　JNI_NE_SetShowOnlyVehicleTrackFlag Exec　***");

            	finish();
*/
            }});
// Add 20151206 End

// 2016/01/07 Yamamoto Add Start
        // 軌跡データ名称変更ボタン
        View btnEdit = oLayout.findViewById(R.id.track_edit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("***　軌跡データ名称変更 onCreate　***");
                setResult(PRESSED_EDIT_TRACK, new Intent());
                finish();
            }});
// 2016/01/07 Yamamoto Add End

        // 軌跡クリアボタン
        View btnClear = oLayout.findViewById(R.id.track_clear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                System.out.println("***　軌跡データ消去 onCreate　***");
// Add 20151216 Start
                setResult(PRESSED_CLEAR_TRACK, new Intent());
                finish();
// Add 20151216 End

            }});
        // アイコン登録ボタン
        View btnIconReg = oLayout.findViewById(R.id.registicon);
        btnIconReg.setOnClickListener(new View.OnClickListener() {
        	@Override
            public void onClick(View v) {
            	System.out.println("***　TrackIconEdit onClick: PRESSED_NEW_ICON　***");
// Add by CPJsunagawa '2015-07-08 Start
                CommonLib.setRegIcon(true);
                CommonLib.setMoveIcon(false);
                CommonLib.setDeleteIcon(false);
                CommonLib.setDrawLine(false);
                CommonLib.setInputText(false);
// Add by CPJsunagawa '2015-07-08 End

                setResult(PRESSED_NEW_ICON, new Intent());
                finish();
			}
        }
        );
        // アイコン移動ボタン
        View btnMove = oLayout.findViewById(R.id.moveicon);
        btnMove.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	System.out.println("***　TrackIconEdit onClick: PRESSED_MOVE_ICON　***");
                CommonLib.setRegIcon(false);
                CommonLib.setMoveIcon(true);
                CommonLib.setDeleteIcon(false);
                CommonLib.setDrawLine(false);
                CommonLib.setInputText(false);

                setResult(PRESSED_MOVE_ICON, new Intent());
                finish();
			}
        }
        );
        // アイコン削除ボタン
        View btnDelete = oLayout.findViewById(R.id.deleteicon);
        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            	System.out.println("***　TrackIconEdit onClick: PRESSED_DELETE_ICON　***");
                CommonLib.setRegIcon(false);
                CommonLib.setMoveIcon(false);
                CommonLib.setDeleteIcon(true);
                CommonLib.setDrawLine(false);
                CommonLib.setInputText(false);

                setResult(PRESSED_DELETE_ICON, new Intent());
                finish();
			}
        }
        );

        // ライン描画ボタン
        View btnDrawLine = oLayout.findViewById(R.id.drawUserLine);
        btnDrawLine.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            	System.out.println("***　TrackIconEdit onClick: PRESSED_DRAW_LINE　***");
                CommonLib.setRegIcon(false);
                CommonLib.setMoveIcon(false);
                CommonLib.setDeleteIcon(false);
                CommonLib.setDrawLine(true);
                CommonLib.setInputText(false);

                setResult(PRESSED_DRAW_LINE, new Intent());
                finish();
			}
        }
        );
        
        // レイアウトを表示する
        setViewInWorkArea(oLayout);
    }

    protected void onResume() {
        super.onResume();
//        showColorPalette(false);
    }

    /**
	 * 入力された図形情報をDBに登録する
	 * @param 
	 * @param 
	 * @return 成功/失敗
	 */
	public static final int ExecuteRegistFigureData(Context ctx, int nIconIndex, int nKind, double lat1,double lon1, double lat2, double lon2, String szIconName)
	{
		String strSQL = null;

		System.out.println("TrackIconEdit ExecuteRegistFigureData ");
		System.out.println("TrackIconEdit ExecuteRegistFigureData Param nIconIndex => " + nKind);
		System.out.println("TrackIconEdit ExecuteRegistFigureData Param nKind => " + nKind);
		System.out.println("TrackIconEdit ExecuteRegistFigureData Param lon1 => " + lon1);
		System.out.println("TrackIconEdit ExecuteRegistFigureData Param lat1 => " + lat1);
		System.out.println("TrackIconEdit ExecuteRegistFigureData Param lon2 => " + lon2);
		System.out.println("TrackIconEdit ExecuteRegistFigureData Param lat2 => " + lat2);
		System.out.println("TrackIconEdit ExecuteRegistFigureData Param szIconName => " + szIconName);

        // 選択したコースがDBに存在するか
        DbHelper helper = new DbHelper(ctx);
        SQLiteDatabase db = helper.getWritableDatabase();

        // DB新規登録
        int nID = 0;
//       	strSQL = "INSERT INTO FigureData VALUES(" + nID + ", "  + nKind + ", " + lon1 + ", " + lat1 + ", " + lon2 + ", " + lat2 + ", '" + szIconName + "');";
       	strSQL = "INSERT INTO FigureData (nIconIndex, nFigureKind, lat1, lon1, lat2, lon2, strIconFile) VALUES(" + nIconIndex + ", " + nKind + ", " + lat1 + ", " + lon1 + ", " + lat2 + ", " + lon2 + ", '" + szIconName + "');";
       	System.out.println("TrackIconEdit ExecuteRegistFigureData strSQL => " + strSQL);

       	db.execSQL(strSQL);
       	
       	String lastIDSQL = "SELECT MAX(_id) FROM FigureData";
       	Cursor maxID = db.rawQuery(lastIDSQL, null);
       	maxID.moveToFirst();
       	nID = maxID.getInt(0);
       	maxID.close();
       	db.close();
       	helper.close();

        System.out.println("TrackIconEdit ExecuteRegistFigureData End");
        return nID;
	}
    
    /**
	 * 入力された図形情報をDBに更新する（アイコン移動）
	 * @param 
	 * @param 
	 * @return 成功/失敗
	 */
	public static final int ExecuteUpdateFigureData(Context ctx, int nID, double lat1, double lon1, double lat2, double lon2)
	{

		System.out.println("TrackIconEdit ExecuteUpdateFigureData ");
		System.out.println("TrackIconEdit ExecuteUpdateFigureData Param nID => " + nID);
		System.out.println("TrackIconEdit ExecuteUpdateFigureData Param lon1 => " + lon1);
		System.out.println("TrackIconEdit ExecuteUpdateFigureData Param lat1 => " + lat1);
		System.out.println("TrackIconEdit ExecuteUpdateFigureData Param lon2 => " + lon2);
		System.out.println("TrackIconEdit ExecuteUpdateFigureData Param lan2 => " +  lat2);
//		System.out.println("TrackIconEdit ExecuteUpdateFigureData Param szIconName => " + szIconName);

        // 選択したコースがDBに存在するか
        DbHelper helper = new DbHelper(ctx);
        SQLiteDatabase db = helper.getWritableDatabase();

        // DB更新（アイコン移動）
 		StringBuffer sb = new StringBuffer();
 		sb.append("UPDATE FigureData SET lat1 = ");
 		sb.append(lat1);
 		sb.append(",lon1 = ");
 		sb.append(lon1);
 		sb.append(" WHERE _id = ");
 		sb.append(nID);
        
       	System.out.println("TrackIconEdit ExecuteUpdateFigureData strSQL => " + sb.toString());

       	db.execSQL(sb.toString());
       	db.close();
       	helper.close();

        System.out.println("TrackIconEdit ExecuteUpdateFigureData End");
        return 0;
	}
    
    /**
	 * 入力された図形情報を削除する（アイコン削除）
	 * @param 
	 * @param 
	 * @return 成功/失敗
	 */
	public final static int ExecuteDeleteFigureData(Context ctx, int nID)
	{

		System.out.println("TrackIconEdit ExecuteDeleteFigureData ");
		System.out.println("TrackIconEdit ExecuteDeleteFigureData Param nID => " + nID);

        DbHelper helper = new DbHelper(ctx);
        SQLiteDatabase db = helper.getWritableDatabase();

        // DB更新（アイコン削除）
 		StringBuffer sb = new StringBuffer();
 		sb.append("DELETE FROM FigureData WHERE _id = ");
 		sb.append(nID);
        
       	System.out.println("TrackIconEdit ExecuteDeleteFigureData strSQL => " + sb.toString());

       	db.execSQL(sb.toString());
       	db.close();
       	helper.close();

        System.out.println("TrackIconEdit ExecuteDeleteFigureData End");
        return 0;
	}
    
    /**
	 * 地図上に任意のラインを描画する
	 * @param 
	 * @param 
	 * @return 成功/失敗
	 */
/*
	public final static int ExecuteDrawUserLine(Context ctx, int nID)
	{
		System.out.println("TrackIconEdit ExecuteDrawUserLine ");
		System.out.println("TrackIconEdit ExecuteDrawUserLine Param nID => " + nID);

        DbHelper helper = new DbHelper(ctx);
        SQLiteDatabase db = helper.getWritableDatabase();

        // DB更新（アイコン削除）
 		StringBuffer sb = new StringBuffer();
 		sb.append("DELETE FROM FigureData WHERE = _id = ");
 		sb.append(nID);
        
       	System.out.println("TrackIconEdit ExecuteDrawUserLine strSQL => " + sb.toString());

       	db.execSQL(sb.toString());
       	db.close();
       	helper.close();

        System.out.println("TrackIconEdit ExecuteDrawUserLine End");
        return 0;
	}
*/
	public final static int ExecuteDrawUserLine(Context ctx, int nKind, List<GeoPoint> points, String keyComment)
	{
		System.out.println("TrackIconEdit ExecuteDrawUserLine ");
		//System.out.println("TrackIconEdit ExecuteDrawUserLine Param nID => " + nID);
		
		// まずはアイコンと同じ処理を行い、最新のIDを取得する。
		int nID = ExecuteRegistFigureData(ctx, 0, nKind, points.get(0).getLatitude(), points.get(0).getLongitude(),
				points.get(points.size() - 1).getLatitude(), points.get(points.size() - 1).getLongitude(), keyComment);

        // 頂点テーブルに格納する。
        // 選択したコースがDBに存在するか
        DbHelper helper = new DbHelper(ctx);
        SQLiteDatabase db = helper.getWritableDatabase();

        for (int i = 0; i <= points.size() - 1; i++)
        {
        	String oneSQL = "INSERT INTO FigureCoord VALUES(" + nID + "," + i + "," + points.get(i).getLatitude() + "," + points.get(i).getLongitude() + ")";
        	db.execSQL(oneSQL);
        }

        db.close();
        System.out.println("TrackIconEdit ExecuteDrawUserLine End");
        return nID;
	}
	
// Add by CPJsunagawa '2015-08-06 Start
    /**
     * 軌跡保存用DB用Helper
     */
    public static class DbHelper extends SQLiteOpenHelper {
    	
    	public final static String navidatapath = StorageUtil.getSdStoragePath(NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME) +
             "/" + NaviAppDataPath.NAVI_APP_ROOT_DIR_NAME;
        public final static String DATABASE_NAME = navidatapath + "/figure_data.db";

        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        	
            System.out.println("TrackIconEdit DbHelperのonCreate DB作成！");
        	
            // DBが存在しない時にのみ呼ばれる
            db.execSQL("CREATE TABLE FigureData  (_id INTEGER PRIMARY KEY, nIconIndex INTEGER, nFigureKind INTEGER, Lat1 REAL, Lon1 REAL, Lat2 REAL, Lon2 REAL, strIconFile TEXT );");
            db.execSQL("CREATE TABLE FigureCoord (_id INTEGER, _order INTEGER, Lat1 REAL, Lon1 REAL, PRIMARY KEY(_id, _order));");

            // デフォルト値をInsert
/*
            db.execSQL("INSERT INTO FrequencyWords VALUES(1, '見込み');");
            db.execSQL("INSERT INTO FrequencyWords VALUES(2, '継続');");
            db.execSQL("INSERT INTO FrequencyWords VALUES(3, '解約');");
*/
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // DBの変更があれば、ここで行う
        }
        
        // DBに登録されているレコード数を取得する
        public int getFigureDataCount(SQLiteDatabase db) {
            Cursor cur = db.rawQuery("SELECT * FROM FigureData", null);
            if( cur == null ) return 0;
            int num = cur.getCount();
            cur.close();
            return num;
        }

/*
        // DBに登録されているコース名を取得する
        public String[] getFigureKind(SQLiteDatabase db) {
            Cursor cur = db.rawQuery("SELECT nFigureKind FROM FigureData", new int[] {});
            if( cur == null ) return null;
            int num = cur.getCount();
            String courseName[] = new String[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	courseName[i] = cur.getString(0);
                cur.moveToNext();
            }
            cur.close();
            return courseName;
        }
*/
        
        // DBに登録されているインデックスを取得する
        public int[] getTFigureDataIndex(SQLiteDatabase db) {
        	String  szSql = "SELECT `_id` FROM FigureData";
            Cursor cur = db.rawQuery(szSql, null);
            if( cur == null ) return null;
            int num = cur.getCount();
            int nIndex[] = new int[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	nIndex[i] = cur.getInt(0);
                cur.moveToNext();
            }
            cur.close();
            return nIndex;
        }

        // DBに登録されている図形種別を取得する
        public int[] getTFigureDataFigureKind(SQLiteDatabase db) {
        	String  szSql = "SELECT `nFigureKind` FROM FigureData";
            Cursor cur = db.rawQuery(szSql, null);
            if( cur == null ) return null;
            int num = cur.getCount();
            int nIndex[] = new int[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	nIndex[i] = cur.getInt(0);
                cur.moveToNext();
            }
            cur.close();
            return nIndex;
        }

        // DBに登録されているアイコンファイル名を取得する
        public String[] getFigureIconFileName(SQLiteDatabase db) {
            Cursor cur = db.rawQuery("SELECT strIconFile FROM FigureData", new String[] {});
            if( cur == null ) return null;
            int num = cur.getCount();
            String FileName[] = new String[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	FileName[i] = cur.getString(0);
                cur.moveToNext();
            }
            cur.close();
            return FileName;
        }
/*
        // DBに登録されているアイコン配置位置を取得する
        public GeoPoint[] getFigureIconPlace(SQLiteDatabase db) {
        	
        	GeoPoint gPoint = new GeoPoint();
        	String  szSql = "SELECT `lat1` FROM FigureData";
            Cursor cur = db.rawQuery(szSql, null);
            if( cur == null ) return null;
            int num = cur.getCount();
            int nIndex[] = new int[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	// 登録されている位置情報（lat1、lon1）を変数に格納
            	nIndex[i] = cur.getInt(0);
                cur.moveToNext();
            }
            cur.close();
            return gPoint[];

            Cursor cur = db.rawQuery("SELECT lat1 FROM FigureData", new double[] {});
            if( cur == null ) return null;
            int num = cur.getCount();
            String FileName[] = new String[num];
            cur.moveToFirst();
            for(int i = 0; i < num; i++ ) {
            	FileName[i] = cur.getString(0);
                cur.moveToNext();
            }
            cur.close();
            return FileName;
        }
*/
    }
// Add by CPJsunagawa '2015-08-06 End
}
