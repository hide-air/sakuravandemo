/**
 *  @file		AccSensorData.java
 *  @brief		加速度センサー通知データ
 *
 *  @attention
 *  @note
 *
 *  @author		Yuki Sawada [Z01]
 *  @date		$Date:: 2010-10-13 00:00:00 +0900 #$ (Create at 2010-10-13)
 *  @version	$Revision: $ by $Author: $
 *
 */

package net.zmap.android.pnd.v2.data;

public class AccSensorData {
	public int		accuracy;		///< 精度
	public long		timestamp;		///< タイムスタンプ
	public float	acceleroX;		///< X軸方向の加速度
	public float	acceleroY;		///< Y軸方向の加速度
	public float	acceleroZ;		///< Z軸方向の加速度
// Add 2011/02/28 sawada Start -->
    public long		status;			///< センサ状態 (SENSOR_STATUS_*)
// Add 2011/02/28 sawada End   <--
//Add 2011/04/20 Z01yoneya Start -->
    public float	pitch;			///< 傾き補正角度 pitch
    public float	roll;			///< 傾き補正角度 roll
    public float	yaw;			///< 傾き補正角度 yaw
//Add 2011/04/20 Z01yoneya End <--
	public int getAccuracy() {
		return accuracy;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public float getAcceleroX() {
		return acceleroX;
	}
	public float getAcceleroY() {
		return acceleroY;
	}
	public float getAcceleroZ() {
		return acceleroZ;
	}
// Add 2011/02/28 sawada Start -->
	public long getStatus() {
		return status;
	}
// Add 2011/02/28 sawada End   <--
//Add 2011/04/20 Z01yoneya Start -->
	public float getPitch() {
		return pitch;
	}
	public float getRoll() {
		return roll;
	}
	public float getYaw() {
		return yaw;
	}
//Add 2011/04/20 Z01yoneya End <--

}