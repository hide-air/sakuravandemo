package net.zmap.android.pnd.v2.sakuracust;

import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.exception.NaviInvalidValueException;
import net.zmap.android.pnd.v2.api.overlay.Icon;
import net.zmap.android.pnd.v2.common.utils.DbHelper;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;



// 軌跡情報を格納するクラス
public class TrackDataInfo {

	public int mID;				// ID
    public String mCourseName;	// コース名
    public String mSaveDir;		// 保存先フォルダ名
    public String mTrackFile;	// 保存ファイル名
    public int mRowID;			// ROWID
    
	public int mMaxDataCnt;		// データ保持レコード数

	// インデックスから、該当の軌跡ファイル名を返却する
    public  String getCourseNameByIndex(int index){
    	
    	
    	return null;
    	
    }

    // 軌跡ファイル名から該当のインデックスを返却する
    public  int getIndexByCourseName(String szFileName){
    	
    	
    	return 0;
    	
    }
}

