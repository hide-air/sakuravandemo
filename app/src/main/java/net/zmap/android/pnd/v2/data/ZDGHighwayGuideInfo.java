/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZDGHighwayGuideInfo.java
 * Description    高速道路案内施設情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZDGHighwayGuideInfo {
    private String szName = null;
    private int iKindCode = 0;
    private short unSAPAServiceCodeNum = 0;
    private int[] unSAPAServiceCode = null;
    //private short unSAPATextSizeByte = 0;
    //private String pszSAPAText = null;
    private long ulnDistMeter = 0;
    private long ulnTollChargeYen = 0;
    private long ulnRestTimeMinute = 0;
    /**
    * Created on 2010/08/06
    * Title:       initUnSAPAServiceCode
    * Description:  関数を初期化します
    * @param1   int len SAPAのテキスト情報サイズ
    * @return       void

    * @version        1.0
    */
    public void initUnSAPAServiceCode(int len){
        unSAPAServiceCode = new int[len];
    }
    /**
    * Created on 2010/08/06
    * Title:       getSzName
    * Description:  施設・地名名称を取得する
    * @param1   無し
    * @return       String

    * @version        1.0
    */
    public String getSzName() {
        return szName;
    }
    /**
    * Created on 2010/08/06
    * Title:       getIKindCode
    * Description:  施設種別を取得する
    * @param1   無し
    * @return       int

    * @version        1.0
    */
    public int getIKindCode() {
        return iKindCode;
    }
    /**
    * Created on 2010/08/06
    * Title:       getUnSAPAServiceCodeNum
    * Description:  SAPAのサービス施設数(種別がSA/PAの時のみ有効)を取得する
    * @param1   無し
    * @return       int

    * @version        1.0
    */
    public short getUnSAPAServiceCodeNum() {
        return unSAPAServiceCodeNum;
    }
    /**
    * Created on 2010/08/06
    * Title:       getUnSAPAServiceCode
    * Description:  サービス内容の種別コードを取得する
    * @param1   無し
    * @return       int[]

    * @version        1.0
    */
    public int[] getUnSAPAServiceCode() {
        return unSAPAServiceCode;
    }
    /**
    * Created on 2010/08/06
    * Title:       getUnSAPATextSizeByte
    * Description:  SAPAのテキスト情報サイズを取得する
    * @param1   無し
    * @return       short

    * @version        1.0
    */
    /*public short getUnSAPATextSizeByte() {
        return unSAPATextSizeByte;
    }*/
    /**
    * Created on 2010/08/06
    * Title:       getPszSAPAText
    * Description:   SAPAのテキスト情報を取得する
    * @param1   無し
    * @return       String

    * @version        1.0
    */
    /*public String getPszSAPAText() {
        return pszSAPAText;
    }*/
    /**
    * Created on 2010/08/06
    * Title:       getUlnDistMeter
    * Description:   自車位置からの距離(m)を取得する
    * @param1   無し
    * @return       long

    * @version        1.0
    */
    public long getUlnDistMeter() {
        return ulnDistMeter;
    }
    /**
    * Created on 2010/08/06
    * Title:       getUlnTollChargeYen
    * Description:   料金(種別が料金所のとき有効)を取得する
    * @param1   無し
    * @return       long

    * @version        1.0
    */
    public long getUlnTollChargeYen() {
        return ulnTollChargeYen;
    }
    /**
    * Created on 2010/08/06
    * Title:       getUlnRestTimeMinute
    * Description:   串の地点までの時間(分)を取得する
    * @param1   無し
    * @return       long

    * @version        1.0
    */
    public long getUlnRestTimeMinute() {
        return ulnRestTimeMinute;
    }
    /**
    * Created on 2010/08/06
    * Title:       setUlnRestTimeMinute
    * Description:   串の地点までの時間(分)を設定する
    * @param1   long ulnRestTimeMinute 串の地点までの時間(分)
    * @return       long

    * @version        1.0
    */
    public void setUlnRestTimeMinute(long ulnRestTimeMinute) {
        this.ulnRestTimeMinute = ulnRestTimeMinute;
    }
}
