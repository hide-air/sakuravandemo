package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.ImageManager;
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.JNITwoLong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Around_ListItem;
import net.zmap.android.pnd.v2.data.ZLandmarkData_t;
import net.zmap.android.pnd.v2.data.ZPOI_Route_ListItem;
import net.zmap.android.pnd.v2.data.ZPOI_Route_Param;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.inquiry.view.ItemView;
import net.zmap.android.pnd.v2.maps.OpenMap;

import java.util.ArrayList;
import java.util.List;

/**
 * 周辺検索のその他の施設（K-J0_ジャンル大分類）
 *
 * @author XuYang
 *
 */
public class OtherAroundGenreInquiry extends InquiryBaseLoading {

    private static OtherAroundGenreInquiry instance;
    private ScrollTool oTool = null;
    protected ScrollList scroll = null;
    private TextView oText = null;
    private Button[] menu_button = new Button[6];
    private int DIALOG_NO_SEARCH_DATA = 1;
    private final int[] menu_id = new int[] {R.id.btn_amusement_120_59,
            R.id.btn_shopping_120_59, R.id.btn_eating_120_59,
            R.id.btn_life_120_59, R.id.btn_journey_120_59,
            R.id.btn_traffic_120_59};

    private final int[] titleName = new int[] {R.string.genre_amusement_name,
            R.string.genre_shopping_name, R.string.genre_eating_name,
            R.string.genre_life_name, R.string.genre_travel_name,
            R.string.genre_traffic_name};

    private JNITwoLong myPosi = new JNITwoLong();
    private POI_Around_ListItem[] poi_Around_Listitem = null;
    private ZPOI_Route_ListItem[] poi_Route_Listitem = null;
    private int SearchCacheNum = 0;
    private int iNextTreeIndex = 0;
    private JNILong RecCount = new JNILong();
    private long RecIndexBtn = 0;
    private final int HAPPY = 0;
    private final int SHOPPING = 1;
    private final int EATING = 2;
    private final int LIFE = 3;
    private final int TRAVEL = 4;
    private final int TRAFFIC = 5;
    private final int[] RecIndexBtnArray = new int[] {HAPPY, SHOPPING, EATING,
            LIFE, TRAVEL, TRAFFIC};
    private long RecIndex = 0;
    private long RecCnt = 0;
    private JNILong GetCount;
    private boolean bListDealing = false;
    private String msPointName = "";
    private String ardString = "";
//    private LinearLayout layGroup = null;
    private List<POI_Around_ListItem> ardList = new ArrayList<POI_Around_ListItem>();
    private List<ZPOI_Route_ListItem> ardRouteList = new ArrayList<ZPOI_Route_ListItem>();
    private boolean mbAroundFlg = false; // 沿いルート周辺と施設周辺のフラグ
    private int MAX_LIST_COUNT = 0;
    public boolean otherType = false;
//Add 2011/06/09 Z01thedoanh Start -->
    private final long INIT_LAYOUT_BIGSHOW = 0; //最初の大分類はグレーボタン数を表示
//Add 2011/06/09 Z01thedoanh End <--


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        SearchCacheNum = ONCE_GET_COUNT;
        // 沿いルート周辺と施設周辺のフラグを取得
        mbAroundFlg = CommonLib.isAroundFlg();
//Del 2011/09/17 Z01_h_yamada Start -->
//        this.setMenuTitle(R.drawable.menu_genre);
//Del 2011/09/17 Z01_h_yamada End <--
        instance = this;
        CommonLib.isAround = false;
        LayoutInflater oInflater = LayoutInflater.from(this);
        // modify by wangdong 20110222
        LinearLayout oLayout = (LinearLayout)oInflater.inflate(
                R.layout.inquiry_base, null);
        Button obtn = (Button)oLayout.findViewById(R.id.inquiry_btn);
        obtn.setVisibility(Button.GONE);

        oText = (TextView)oLayout.findViewById(R.id.inquiry_title);
        oText.setText(R.string.genre_title);

        ItemView oview = new ItemView(this, 0, R.layout.inquiry_genre_freescroll);
        initGroupButton(oview);
        LinearLayout oGroupList = (LinearLayout)oLayout
                .findViewById(R.id.LinearLayout_list);
        oGroupList.addView(oview.getView(), new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT));

        initRightView(oview);
        setViewInWorkArea(oLayout);
        initData();
    }

//Del 2011/08/17 Z01thedoanh Start -->
//    static public OtherAroundGenreInquiry GetGenreSrchGlobalRef() {
//        return instance;
//    }
//Del 2011/08/17 Z01thedoanh End <--

    /**
     * 画面のデータの初期化処理
     */
    protected void initData() {
        if (mbAroundFlg) {
            // 沿い周辺ジャンル検索の検索初期化
//            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_Cancel();
            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_Clear();
            ZPOI_Route_Param stRoutData = new ZPOI_Route_Param();

            // 沿い周辺検索の方向【デフォルト：両方】
            stRoutData.setEside(3);

            // 沿い周辺検索の幅【デフォルト：500】
            stRoutData.setUlnRange(500);

            // 沿い周辺検索の件数
// Chg 2011/10/03 katsuta Start -->
//			stRoutData.setLnCount(40);
			stRoutData.setLnCount(MAX_ROUTE_POINUM);
// Chg 2011/10/03 katsuta End <--


            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SetParam(stRoutData);
            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SetRoute();
            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchList();
            // 沿いルート周辺
            if (poi_Route_Listitem == null) {
                poi_Route_Listitem = new ZPOI_Route_ListItem[(int)SearchCacheNum];
                for (int i = 0; i < SearchCacheNum; i++) {
                    poi_Route_Listitem[i] = new ZPOI_Route_ListItem();
                }
            }
        } else {
            // データのclear
            NaviRun.GetNaviRunObj().JNI_NE_POI_Around_Clear();
            //yangyang mod start Bug774
//            NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SetDistance(10000);
            NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SetDistance(6000);
            //yangyang mod end Bug774
            NaviRun.GetNaviRunObj().JNI_NE_GetMapCenter(myPosi);
            NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SetPosition(
                    myPosi.getM_lLong(), myPosi.getM_lLat());
            NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SearchList();
            if (poi_Around_Listitem == null) {
                poi_Around_Listitem = new POI_Around_ListItem[(int)SearchCacheNum];
                for (int i = 0; i < SearchCacheNum; i++) {
                    poi_Around_Listitem[i] = new POI_Around_ListItem();
                }
            }
        }
//Chg 2011/08/17 Z01thedoanh Start -->
        initShowData();
//Chg 2011/08/17 Z01thedoanh End <--
    }

    /**
     * スクロールバーの初期化
     *
     * @param oview
     */
    protected void initRightView(ItemView oview) {
        scroll = (ScrollList)oview.findViewById(this, R.id.scrollList);
        scroll.setAdapter(new BaseAdapter() {

            @Override
            public int getCount() {
                if (MAX_LIST_COUNT == 0) {
                    if (mbAroundFlg) {
                        return ardRouteList.size();
                    } else {
                        return ardList.size();
                    }
                }
                return MAX_LIST_COUNT;
//
            }

            // yangyang add start
            @Override
            public boolean isEnabled(int position) {
                return false;
            }

            @Override
            public boolean areAllItemsEnabled() {
                return false;
            }

            // yangyang add end

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return otherAroundGetView(position, convertView);
            }

        });
//Del 2011/10/06 Z01_h_yamada Start -->
//        scroll.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
        scroll.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
        //yangyang add start 20110423 Bug1060
        scroll.setPadding(5, 5, 2, 5);
        //yangyang add start 20110423 Bug1060
        oTool = new ScrollTool(this);
        oTool.bindView(scroll);
        setViewInOperArea(oTool);
    }

    private int clickBtn = -1;

    /**
     * グループボタンの初期化処理
     *
     * @param oview
     */
    private void initGroupButton(ItemView oview) {
        for (int i = 0; i < menu_id.length; i++) {
            final Button btn = (Button)oview.findViewById(this, menu_id[i]);
            final int index = i;
            //menu_button[i] = btn;
            btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
                    // TODO Auto-generated method stub
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・その他施設 ｼﾞｬﾝﾙ選択禁止 Start -->
        			if(DrivingRegulation.CheckDrivingRegulation()){
        				return;
        			}
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・その他施設 ｼﾞｬﾝﾙ選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

                    for (int k = 0; k < menu_button.length; k++) {
                        if (index != k) {
                            menu_button[k].setSelected(false);
                        } else {
                            clickBtn = k;
                            menu_button[k].setSelected(true);
                        }
                    }
//                    startShowPage(NOTSHOWCANCELBTN);
                    itemClick(clickBtn);
//Chg 2011/06/10 Z01thedoanh Start -->
                    //コメントアウトを外す
                    Resources oRes = getResources();
                    String title = oRes.getString(titleName[clickBtn]);
                    oText.setText(title);
                    ardString = title;
                    scroll.reset();
                    scroll.scroll(0);
//Chg 2011/06/10 Z01thedoanh End <--

                }
            });
            menu_button[i] = btn;
        }
    }

    /**
     * 周辺のデータをクリックする処理
     *
     * @param Idx
     *            周辺の種類
     */
    private boolean click(int Idx) {
        // RecIndex = Idx;
        if (iNextTreeIndex != 0) {
            if (iNextTreeIndex == 2) {
                if (mbAroundFlg) {// 沿いルート周辺検索
                    NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetPrevTreeIndex(
                            RecCount);
                    NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchPrevList();
                    iNextTreeIndex = (int)RecCount.lcount;
                } else {// 施設周辺検索
                    NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetPrevTreeIndex(
                            RecCount);
                    NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SearchPrevList();
                    iNextTreeIndex = (int)RecCount.lcount;
                }
            }
            if (mbAroundFlg) {// 沿いルート周辺検索
                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchPrevList();
            } else {// 施設周辺検索
                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SearchPrevList();
            }
        }
        if (mbAroundFlg) {
            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetNextTreeIndex(Idx,
                    RecCount);
            iNextTreeIndex = (int)RecCount.lcount;
            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchNextList(Idx);
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetNextTreeIndex(Idx,
                    RecCount);
            iNextTreeIndex = (int)RecCount.lcount;
            NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SearchNextList(Idx);
        }
//Chg 2011/08/17 Z01thedoanh Start -->
        return initShowData();
        //return true;
//Chg 2011/08/17 Z01thedoanh End <--
    }

    /**
     * 周辺のデータをクリックする処理、タイトルの設定
     *
     * @param index
     *            周辺の種類
     */
    private boolean itemClick(int index) {
        RecIndexBtn = RecIndexBtnArray[index];

        boolean bReturn = click(index);

        return bReturn;
    }

//    public static OtherAroundGenreInquiry getInstance() {
//        return instance;
//    }

    /**
     * 表示したデータを取得
     */
    public View otherAroundGetView(int pos, View view) {
        LinearLayout oLayout;
        if (view == null || !(view instanceof LinearLayout)) {
            LayoutInflater inflater;
            inflater = LayoutInflater.from(this);

            // ボタンを追加
            oLayout = (LinearLayout)inflater.inflate(
                    R.layout.inquiry_distance_button_freescroll, null);

            view = oLayout;
        } else {
            oLayout = (LinearLayout)view;
        }
        final int iPos = pos;

        LinearLayout oButton = (LinearLayout)oLayout
                .findViewById(R.id.LinearLayout_distance);
        oButton.setBackgroundResource(R.drawable.btn_default);
//        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
//                480, 70);
//        oButton.setLayoutParams(param);

        ImageView txtIcon = (ImageView)oLayout.findViewById(R.id.icon);
        TextView txtCon = (TextView)oLayout.findViewById(R.id.TextView_value);
        TextView txtDis = (TextView)oLayout.findViewById(R.id.TextView_distance);

        if (mbAroundFlg) {
            // 沿いルート周辺

            if (pos >= ardRouteList.size()) {
                if (pos % SearchCacheNum == 0) {
                    reGetListRouteData(pos);
                }
            }

            if (!"".equals(ardRouteList.get(pos).getPucName())) {
                oButton.setEnabled(true);
                oButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
                        // TODO Auto-generated method stub
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・その他施設 沿いルート時ｼﾞｬﾝﾙ詳細選択禁止 Start -->
            			if(DrivingRegulation.CheckDrivingRegulation()){
            				return;
            			}
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・その他施設 沿いルート時ｼﾞｬﾝﾙ詳細選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                        onItemRouteClick(iPos);
                    }
                });
                txtCon.setText(ardRouteList.get(pos).getPucName());

                txtDis.setVisibility(View.GONE);
                txtIcon.setVisibility(View.VISIBLE);
                // txtIcon.setBackgroundResource(R.drawable.cg_flag);
                ZLandmarkData_t image = new ZLandmarkData_t();
                int kindeCode = (int)ardRouteList.get(pos).getKindeCode();
                Bitmap iconCode = null;
                if (kindeCode != 0) {
// Chg 20yy/mm/dd katsuta Start -->
//Chg 2011/08/16 Z01thedoanh Start -->
//                        NaviRun.GetNaviRunObj().JNI_DAL_GetLandmarkData(kindeCode, Constants.Icon_Size, image);
//----------------------------------------------------------------------------
//                    NaviRun.GetNaviRunObj().JNI_MP_OutSidePOIIcon_GetIconData(kindeCode, Constants.Icon_Size, image);
//Chg 2011/08/16 Z01thedoanh End <--
                	if (Constants.useKiwiIconFlag) {
                		NaviRun.GetNaviRunObj().JNI_DAL_GetLandmarkData(kindeCode, Constants.Icon_Size, image);
                	}
                	else {
                		NaviRun.GetNaviRunObj().JNI_MP_OutSidePOIIcon_GetIconData(kindeCode, Constants.Icon_Size, image);
                	}
// Chg 2011/10/12 katsuta End <--

                    if (image.getSWidth() > 0 && image.getSHeight() > 0) {
                        iconCode = ImageManager.getImage(image.getSPixRGBA32(), (int)image.getSWidth(), (int)image.getSHeight());
                    }
                }
                if (iconCode != null) {
                    txtIcon.setBackgroundDrawable(new BitmapDrawable(iconCode));
                    //txtIcon.setBackgroundResource(0);
                } else {
                    txtIcon.setBackgroundResource(0);
                }

            } else {
                oButton.setEnabled(false);
                txtCon.setText("");
                txtIcon.setBackgroundResource(0);
                txtDis.setText("");
            }

        } else {
            if (pos >= ardList.size()) {
                if (pos % SearchCacheNum == 0) {
                    reGetListAroundData(pos);
                }
            }
            if (!"".equals(ardList.get(pos).getPucName())) {
                oButton.setEnabled(true);
                oButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
                        // TODO Auto-generated method stub
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・その他施設 周辺時ｼﾞｬﾝﾙ詳細選択禁止 Start -->
            			if(DrivingRegulation.CheckDrivingRegulation()){
            				return;
            			}
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・その他施設 周辺時ｼﾞｬﾝﾙ詳細選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                        onItemClick(iPos);
                    }
                });
                txtCon.setText(ardList.get(pos).getPucName());

                txtDis.setVisibility(View.GONE);
                txtIcon.setVisibility(View.VISIBLE);
                ZLandmarkData_t image = new ZLandmarkData_t();
                int kindeCode = (int)ardList.get(pos).getKindeCode();
                Bitmap iconCode = null;
                if (kindeCode != 0) {
// Chg 2011/10/12 katsuta Start -->
//Chg 2011/08/16 Z01thedoanh Start -->
//                    NaviRun.GetNaviRunObj().JNI_DAL_GetLandmarkData(kindeCode, Constants.Icon_Size, image);
//----------------------------------------------------------------------------
//                    NaviRun.GetNaviRunObj().JNI_MP_OutSidePOIIcon_GetIconData(kindeCode, Constants.Icon_Size, image);
//Chg 2011/08/16 Z01thedoanh End <--
                	if (Constants.useKiwiIconFlag){
                		NaviRun.GetNaviRunObj().JNI_DAL_GetLandmarkData(kindeCode, Constants.Icon_Size, image);
                	}
                	else {
                		NaviRun.GetNaviRunObj().JNI_MP_OutSidePOIIcon_GetIconData(kindeCode, Constants.Icon_Size, image);
                	}
// Chg 2011/10/12 katsuta End <--

                    if (image.getSWidth() > 0 && image.getSHeight() > 0) {
                        iconCode = ImageManager.getImage(image.getSPixRGBA32(), (int)image.getSWidth(), (int)image.getSHeight());
                    }
                }
                if (iconCode != null) {
                    txtIcon.setBackgroundDrawable(new BitmapDrawable(iconCode));
                    //txtIcon.setBackgroundResource(0);
                } else {
                    txtIcon.setBackgroundResource(0);
                }

            } else {
                oButton.setEnabled(false);
                txtCon.setText("");
                txtIcon.setBackgroundResource(0);
                txtDis.setText("");
            }

        }
        return view;
    }

    /**
     * 周辺検索の場合、データを検索
     *
     * @param wPos
     *            データのindex
     */
    private void onItemClick(int wPos) {
        do {
            if (bListDealing == true) break;
            if (RecIndex > wPos || wPos > (RecIndex + RecCnt)) {
                break;
            }
            int index = (int)(wPos - RecIndex);
            if (index >= ardList.size()) {
                break;
            }
            if (iNextTreeIndex >= 2) {
                msPointName = ardList.get(index).getPucName();
                long Longitude = 0;
                long Latitude = 0;
                JNIInt MapScale = new JNIInt();
                NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
                Latitude = ardList.get(index).getLatitude();
                Longitude = ardList.get(index).getLongitude();

                POIData oData = new POIData();
                oData.m_sName = msPointName;
                oData.m_sAddress = msPointName;
                oData.m_wLong = Longitude;
                oData.m_wLat = Latitude;
                OpenMap.moveMapTo(OtherAroundGenreInquiry.this, oData,
                        getIntent(), AppInfo.POI_LOCAL,
                        Constants.LOCAL_INQUIRY_REQUEST);
                this.createDialog(Constants.DIALOG_WAIT, -1);

            } else {

                JNILong JNIRecCnt = new JNILong();
                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetNextTreeIndex(index,
                        JNIRecCnt);
                if (JNIRecCnt.lcount != 0) {
                    NaviRun.GetNaviRunObj()
                            .JNI_NE_POIArnd_SearchNextList(index);

                    /*iNextTreeIndex = (int) JNIRecCnt.lcount;
                    ardString = ardString + ">"
                            + ardList.get(index).getPucName();
                    oText.setText(ardString);
                    search();*/
                    Intent oIntent = getIntent();
                    oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, RecIndexBtn);
                    oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, ardString + ">" + ardList.get(index).getPucName());
                    oIntent.setClass(this,
                            OtherAroundGenreInquiryResult.class);
//Chg 2011/09/23 Z01yoneya Start -->
//                    this.startActivityForResult(
//                                    oIntent,
//                                    AppInfo.ID_ACTIVITY_AROUND_FAVORITES_TEMP);
//------------------------------------------------------------------------------------------------------------------------------------
                    NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_AROUND_FAVORITES_TEMP);
//Chg 2011/09/23 Z01yoneya End <--
                } else {
                    this.showDialog(DIALOG_NO_SEARCH_DATA);
                }
            }
        } while (false);
    }

    /**
     * 沿いルート周辺検索の場合、データを検索
     *
     * @param wPos
     *            データのindex
     */
    private void onItemRouteClick(int wPos) {
        do {
            if (bListDealing == true) break;
            if (RecIndex > wPos || wPos > (RecIndex + RecCnt)) {
                break;
            }
            int index = (int)(wPos - RecIndex);
            if (index >= ardRouteList.size()) {
                break;
            }
            if (iNextTreeIndex >= 2) {
                msPointName = ardRouteList.get(index).getPucName();
                long Longitude = 0;
                long Latitude = 0;
                JNIInt MapScale = new JNIInt();
                NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
                Latitude = ardRouteList.get(index).getLatitude();
                Longitude = ardRouteList.get(index).getLongitude();

                POIData oData = new POIData();
                oData.m_sName = msPointName;
                oData.m_sAddress = msPointName;
                oData.m_wLong = Longitude;
                oData.m_wLat = Latitude;
                //yangyang mod start Bug813
//                Intent oIntent = getIntent();
//                oIntent.setClass(this, RouteEdit.class);
//                oIntent.putExtra(AppInfo.POI_DATA, oData);
//                oIntent.putExtra(Constants.PARAMS_ROUTE_MOD_FLAG, true);
//                oIntent.putExtra(Constants.ROUTE_TYPE_KEY,Constants.ROUTE_TYPE_NOW);
//                this.startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);

                CommonLib.routeSearchFlag = true;
                CommonLib.setChangePosFromSearch(true);
                CommonLib.setChangePos(false);
                OpenMap.moveMapTo(OtherAroundGenreInquiry.this, oData,
                        getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
                //yangyang mod end Bug813
                this.createDialog(Constants.DIALOG_WAIT, -1);
            } else {

                JNILong JNIRecCnt = new JNILong();
                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetNextTreeIndex(index,
                        JNIRecCnt);
                if (JNIRecCnt.lcount != 0) {
                    NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchNextList(
                            index);
                    Intent oIntent = getIntent();
                    oIntent.putExtra(Constants.PARAMS_GENRE_INDEX, RecIndexBtn);
                    oIntent.putExtra(Constants.PARAMS_SEARCH_KEY, ardString + Constants.FLAG_TITLE + ardRouteList.get(index).getPucName());
                    oIntent.setClass(this,
                            OtherAroundGenreInquiryResult.class);
//Chg 2011/09/23 Z01yoneya Start -->
//                    this.startActivityForResult(
//                                    oIntent,
//                                    AppInfo.ID_ACTIVITY_AROUND_FAVORITES_TEMP);
//------------------------------------------------------------------------------------------------------------------------------------
                    NaviActivityStarter.startActivityForResult(this, oIntent, AppInfo.ID_ACTIVITY_AROUND_FAVORITES_TEMP);
//Chg 2011/09/23 Z01yoneya End <--
                    /*iNextTreeIndex = (int) JNIRecCnt.lcount;
                    ardString = ardString + ">"
                            + ardRouteList.get(index).getPucName();
                    oText.setText(ardString);
                    search();*/
                } else {
                    this.showDialog(DIALOG_NO_SEARCH_DATA);
                }
            }
        } while (false);
    }

    /**
     * 検索レベルによって、当該Listを取得する
     */
//Chg 2011/08/17 Z01thedoanh Start -->
    private boolean initShowData() {
        boolean bReturn = true;
        switch (iNextTreeIndex) {
            case 0:
                bReturn = showBigKind();
                break;
            case 1:
                bReturn = showMiddleKind();
                break;
            case 2:
                bReturn = showList();
                break;
            default:
                // this.showDialog(Constants.DIALOG_NO_GNR);
                break;
        }
        return bReturn;
    }

//Chg 2011/08/17 Z01thedoanh End <--

    /**
     * 大分類データを取得
     */
    private boolean showBigKind() {
        POI_Around_ListItem aroundBigListitem = null;
        ZPOI_Route_ListItem aroundBigListitem1 = null;
        RecCount = new JNILong();
        if (mbAroundFlg) {// 娌裤亜銉兗銉堝懆杈烘绱�
            if (ardRouteList == null) {
                ardRouteList = new ArrayList<ZPOI_Route_ListItem>();
            } else {
                if (!ardRouteList.isEmpty()) ardRouteList.clear();
            }

            aroundBigListitem1 = new ZPOI_Route_ListItem();
//Chg 2011/06/09 Z01thedoanh Start -->
            //for (int i = 0; i < 5; i++) {
            for (int i = 0; i < INIT_LAYOUT_BIGSHOW; i++) {
//Chg 2011/06/09 Z01thedoanh End <--
                aroundBigListitem1.setPucName("");
                ardRouteList.add(aroundBigListitem1);
            }
        } else {
            if (ardList == null) {
                ardList = new ArrayList<POI_Around_ListItem>();
            } else {
                if (!ardList.isEmpty()) ardList.clear();
            }

            aroundBigListitem = new POI_Around_ListItem();
//Chg 2011/06/09 Z01thedoanh Start -->
            //for (int i = 0; i < 5; i++) {
            for (int i = 0; i < INIT_LAYOUT_BIGSHOW; i++) {
//Chg 2011/06/09 Z01thedoanh End <--
                aroundBigListitem.setPucName("");
                ardList.add(aroundBigListitem);
            }
        }
        RecIndex = 0;
        RecCnt = 5;

        GetCount = new JNILong();
        if (mbAroundFlg) {// 沿いルート周辺検索
            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetRecCount(RecCount);

            if (RecCount.lcount != 0) {
                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetRecList(0,
                        RecCount.lcount, poi_Route_Listitem, GetCount);
                //yangyang del start
//                scroll.reset();
                //yangyang del end
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //showDialog(DIALOG_NO_SEARCH_DATA);
                    }
                });
                return false;
            }
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetRecCount(RecCount);
            if (RecCount.lcount != 0) {
                if (poi_Around_Listitem == null) {
                    poi_Around_Listitem = new POI_Around_ListItem[(int)RecCount.lcount];
                    for (int i = 0; i < RecCount.lcount; i++) {
                        poi_Around_Listitem[i] = new POI_Around_ListItem();
                    }
                }
                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetRecList(0,
                        RecCount.lcount, poi_Around_Listitem, GetCount);
                //yangyang del start
//              scroll.reset();
                //yangyang del end
            } else {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //showDialog(DIALOG_NO_SEARCH_DATA);
                    }
                });
                return false;
            }
        }
        return true;
    }

    /**
     * 検索したデータを存在しない場合、ダイアログを表示
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        final CustomDialog oDialog = new CustomDialog(this);
        Resources oRes = getResources();
        if (id == DIALOG_NO_SEARCH_DATA) {
            oDialog.setTitle(oRes
                    .getString(R.string.around_search_no_data_title));
            oDialog.setMessage(oRes.getString(R.string.tel_dialog_msg));

            oDialog.addButton(oRes.getString(R.string.btn_ok),
                    new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }

                    });
//Add 2012/04/26 Z01hirama Start --> #4417
            oDialog.setOnCancelListener(new OnCancelListener() {

	        	@Override
				public void onCancel(DialogInterface dialog) {
					finish();
				}
			});
//Add 2012/04/26 Z01hirama End <-- #4417
            return oDialog;
        }

        // XuYang add start 走行中の操作制限
        return super.onCreateDialog(id);
        // XuYang add end 走行中の操作制限
    }

    /**
     * 中分類のデータを取得
     */
    private boolean showMiddleKind() {

        if (mbAroundFlg) { // 沿いルート周辺検索
            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetRecCount(RecCount);
            if (ardRouteList == null) {
                ardRouteList = new ArrayList<ZPOI_Route_ListItem>();
            } else {
                if (!ardRouteList.isEmpty()) ardRouteList.clear();
            }
        } else { // 施設周辺検索

            NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetRecCount(RecCount);
            if (ardList == null) {
                ardList = new ArrayList<POI_Around_ListItem>();
            } else {
                if (!ardList.isEmpty()) ardList.clear();
            }
        }
        showToast(this, RecCount);
        MAX_LIST_COUNT = initPageIndexAndCnt(RecCount);
        if (RecCount.lcount > 0) {
            RecIndex = 0;
            RecCnt = 0;
            getSearchList(RecIndex);
            //yangyang del start
//            scroll.reset();
            //yangyang del end
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialog(DIALOG_NO_SEARCH_DATA);
                }
            });
            return false;

        }
        return true;
    }

    /**
     * 検索したデータのリストを取得
     *
     * @param lRecIndex
     *            index
     */
    protected void getSearchList(long lRecIndex) {
        bListDealing = true;
        if (mbAroundFlg) { // 沿いルート周辺検索
            if (ardRouteList == null) {
                ardRouteList = new ArrayList<ZPOI_Route_ListItem>();
            }
            else if (!ardRouteList.isEmpty()) {
                ardRouteList.clear();
            }
            long recCount = RecCount.lcount;
            if (RecCount.lcount > SearchCacheNum) {
                recCount = SearchCacheNum;
            }
//Add 2011/05/26 Z01thedoanh Start -->
            if (poi_Route_Listitem == null) {
                poi_Route_Listitem = new ZPOI_Route_ListItem[(int)recCount];
                for (int i = 0; i < recCount; i++) {
                    poi_Route_Listitem[i] = new ZPOI_Route_ListItem();
                }
            }
//Add 2011/05/26 Z01thedoanh End <--

            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetRecList(lRecIndex,
                    recCount, poi_Route_Listitem, GetCount);
            for (int i = 0; i < GetCount.lcount; i++) {
                ardRouteList.add(poi_Route_Listitem[i]);
            }
            RecCnt = GetCount.lcount;
            RecIndex = lRecIndex;
            bListDealing = false;

        } else { // 施設周辺検索
            if (ardList == null) {
                ardList = new ArrayList<POI_Around_ListItem>();
            }
            else if (!ardList.isEmpty()) {
                ardList.clear();
            }

            long recCount = RecCount.lcount;
            if (RecCount.lcount > SearchCacheNum) {
                recCount = SearchCacheNum;
            }
//Add 2011/05/26 Z01thedoanh Start -->
            if (poi_Around_Listitem == null) {
                poi_Around_Listitem = new POI_Around_ListItem[(int)recCount];
                for (int i = 0; i < recCount; i++) {
                    poi_Around_Listitem[i] = new POI_Around_ListItem();
                }
            }
//Add 2011/05/26 Z01thedoanh End <--

            NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetRecList(lRecIndex,
                    recCount, poi_Around_Listitem, GetCount);

            for (int i = 0; i < GetCount.lcount; i++) {
                ardList.add(poi_Around_Listitem[i]);
            }
            RecCnt = GetCount.lcount;
            RecIndex = lRecIndex;
            bListDealing = false;
        }
    }

    /**
     * 検索したデータを表示
     */
    private boolean showList() {
        JNILong RecCountTmp = new JNILong();
        if (mbAroundFlg) { // 沿いルート周辺検索
            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetRecCount(RecCountTmp);
            if (ardRouteList == null) {
                ardRouteList = new ArrayList<ZPOI_Route_ListItem>();
            } else {
                if (!ardRouteList.isEmpty()) ardRouteList.clear();
            }
        } else { // 施設周辺検索
            NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetRecCount(RecCountTmp);
            if (ardList == null) {
                ardList = new ArrayList<POI_Around_ListItem>();
            } else {
                if (!ardList.isEmpty()) ardList.clear();
            }
        }

        if (RecCountTmp.lcount > 0) {

            RecIndex = 0;
            RecCnt = 0;
            getSearchList(RecIndex);
            //yangyang del start
//            scroll.reset();
            //yangyang del end
        }
        if (RecCountTmp.lcount <= 0) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialog(DIALOG_NO_SEARCH_DATA);
                }

            });
            return false;

        }
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see net.zmap.android.pnd.v2.common.activity.MenuBaseActivity#goBack()
     */
//    /**
//     * 画面に戻るの処理
//     */
//    @Override
//    protected void goBack() {
//        initBack();
//    }
//
//    /**
//     * 戻るの処理
//     */
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
////        if (initBack() == true) {
////            return true;
////        }
//        return super.onKeyDown(keyCode, event);
//    }
//
//    /**
//     * 戻る後で画面の初期化処理
//     *
//     * @return　true/false
//     */
//
//    private boolean initBack() {
//        if (iNextTreeIndex == 2) {
//            if (mbAroundFlg) {// 沿いルート周辺検索
//                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetPrevTreeIndex(
//                        RecCount);
//                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchPrevList();
//                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetPrevTreeIndex(
//                        RecCount);
//                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchPrevList();
//                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetNextTreeIndex(
//                        RecIndexBtn, RecCount);
//                iNextTreeIndex = (int) RecCount.lcount;
//                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchNextList(
//                        RecIndexBtn);
//
//            } else {// 施設周辺検索
//                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetPrevTreeIndex(
//                        RecCount);
//                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SearchPrevList();
//                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetPrevTreeIndex(
//                        RecCount);
//                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SearchPrevList();
//                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetNextTreeIndex(
//                        RecIndexBtn, RecCount);
//                iNextTreeIndex = (int) RecCount.lcount;
//                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SearchNextList(
//                        RecIndexBtn);
//            }
//            search();
//            int x = ardString.lastIndexOf(">");
//            if (x > 0) {
//                ardString = ardString.substring(0, x);
//                oText.setText(ardString);
//            } else {
//                oText.setText(ardString);
//            }
//            return true;
//        } else {
//            super.goBack();
//            return false;
//        }
//    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent oIntent) {
        super.onActivityResult(requestCode, resultCode, oIntent);
        if (requestCode == AppInfo.ID_ACTIVITY_AROUND_FAVORITES_TEMP) {
            if (mbAroundFlg) {// 沿いルート周辺検索
                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetPrevTreeIndex(
                        RecCount);
                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchPrevList();
                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetPrevTreeIndex(
                        RecCount);
                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchPrevList();
                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetNextTreeIndex(
                        RecIndexBtn, RecCount);
                iNextTreeIndex = (int)RecCount.lcount;
                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_SearchNextList(
                        RecIndexBtn);
            } else {// 施設周辺検索
                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetPrevTreeIndex(
                        RecCount);
                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SearchPrevList();
                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetPrevTreeIndex(
                        RecCount);
                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SearchPrevList();
                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetNextTreeIndex(
                        RecIndexBtn, RecCount);
                iNextTreeIndex = (int)RecCount.lcount;
                NaviRun.GetNaviRunObj().JNI_NE_POIArnd_SearchNextList(
                        RecIndexBtn);
            }

            //yangyang del start 20110423 Bug1060
//            initShowData();
            //yangyang del end 20110423 Bug1060
            int x = ardString.lastIndexOf(">");
            if (x > 0) {
                ardString = ardString.substring(0, x);
                oText.setText(ardString);
            } else {
                oText.setText(ardString);
            }
        }
        //yangyang add start Bug1060 20110423
        if (resultCode == Constants.RESULTCODE_BACK) {
            if (mbAroundFlg) {
                NaviRun.GetNaviRunObj().JNI_NE_POIRoute_Clear();
            } else {
                NaviRun.GetNaviRunObj().JNI_NE_POI_Around_Clear();
            }
            this.setResult(resultCode, oIntent);
            finish();
        }
        //yangyang add end Bug1060 20110423

    }

    public void reGetListRouteData(long lRecIndex) {
        JNILong getCnt = new JNILong();
        poi_Route_Listitem = new ZPOI_Route_ListItem[(int)SearchCacheNum];
        for (int i = 0; i < SearchCacheNum; i++) {
            poi_Route_Listitem[i] = new ZPOI_Route_ListItem();
        }
        NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetRecList(lRecIndex,
                SearchCacheNum, poi_Route_Listitem, getCnt);
        for (int i = 0; i < getCnt.lcount; i++) {
            ardRouteList.add(poi_Route_Listitem[i]);
        }
    }

    public void reGetListAroundData(long lRecIndex) {
        JNILong getAroundCnt = new JNILong();
        poi_Around_Listitem = new POI_Around_ListItem[(int)SearchCacheNum];
        for (int i = 0; i < SearchCacheNum; i++) {
            poi_Around_Listitem[i] = new POI_Around_ListItem();
        }

        NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetRecList(lRecIndex,
                SearchCacheNum, poi_Around_Listitem, getAroundCnt);
        for (int i = 0; i < getAroundCnt.lcount; i++) {
            ardList.add(poi_Around_Listitem[i]);
        }
    }

    @Override
    protected boolean onStartShowPage() throws Exception {

        return itemClick(clickBtn);
    }

    @Override
    protected void onFinishShowPage(boolean bGetData) throws Exception {
        if (bGetData) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//Chg 2011/06/10 Z01thedoanh Start -->
                    //コメントアウトを外す
                    Resources oRes = getResources();
                    String title = oRes.getString(titleName[clickBtn]);
                    oText.setText(title);
                    ardString = title;
                    scroll.reset();
                    scroll.scroll(0);
//Chg 2011/06/10 Z01thedoanh End <--
                }

            });
        }

    }

    //yangyang add start 20110423
    @Override
    protected boolean onClickGoBack() {
        if (mbAroundFlg) {
            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_Clear();
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_POI_Around_Clear();
        }
//		NaviRun.GetNaviRunObj().JNI_NE_POIArnd_Cancel();

        return super.onClickGoBack();
    }

    @Override
    protected boolean onClickGoMap() {
        if (mbAroundFlg) {
            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_Clear();
        } else {
            NaviRun.GetNaviRunObj().JNI_NE_POI_Around_Clear();
        }
//		NaviRun.GetNaviRunObj().JNI_NE_POIArnd_Cancel();
        return super.onClickGoMap();
    }
    //yangyang add end 20110423
//Del 2011/08/17 Z01thedoanh Start -->
////Add 2011/05/25 Z01thedoanh Start -->
//	//EVENTCODE返した時,search()関数を行う
//	@Override
//	protected void search(){
//		switch (iNextTreeIndex) {
//		case 0:
//			showBigKind();
//			break;
//		case 1:
//			showMiddleKind();
//			if(!bListDealing){
//				showResultScrollList();
//			}
//
//			break;
//		case 2:
//			showList();
//			break;
//		default:
//			this.showDialog(DIALOG_NO_SEARCH_DATA);
//			break;
//		}
//	}
//	public void showResultScrollList(){
//        Resources oRes = getResources();
//        String title = oRes.getString(titleName[clickBtn]);
//        oText.setText(title);
//        ardString = title;
//		scroll.reset();
//		scroll.scroll(0);
//	}
////Add 2011/05/25 Z01thedoanh End <--
//Del 2011/08/17 Z01thedoanh End <--

}
