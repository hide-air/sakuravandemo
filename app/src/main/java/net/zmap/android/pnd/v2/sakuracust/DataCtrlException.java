package net.zmap.android.pnd.v2.sakuracust;

/*
 * さくらヴァンデータIO関係の例外
 */
public class DataCtrlException extends Exception {
	public DataCtrlException(String msg)
	{
		super(msg);
	}
}
