/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           JNIInt.java
 * Description    int Class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class JNIInt {
    private int m_iCommon;

    /**
    * Created on 2010/08/06
    * Title:       getM_fCommon
    * Description:  intの値を取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getM_iCommon() {
        return m_iCommon;
    }

}
