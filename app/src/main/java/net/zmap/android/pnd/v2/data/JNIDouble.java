/*------------------------------------------------------------------------------
 * Copyright(C) 2011 ZenrinDataCom Co.,LTD. All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of ZenrinDataCom Co.,LTD.;
 * the contents of this file is not to be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior written
 * permission of ZenrinDataCom Co.,LTD.
 *------------------------------------------------------------------------------*/
/**
 ********************************************************************
 *
 * Project
 * File           JNIByte.java
 * Description    Float Class
 * Created on     2011/11/27
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

/**
 * JNIとdoubleのデータをやり取りするためのクラス
 *
 * @id $Id: JNIDouble.java 4449 2012-01-13 10:52:49Z kojima-k $
 */
public class JNIDouble {
    private double m_dCommon;

    /**
    * Created on 2011/11/27
    * Title:       getM_dCommon
    * Description:  doubleの値を取得する
    * @param1  無し
    * @return       double
    * @version        1.0
    */
    public double getM_dCommon() {
        return m_dCommon;
    }

    /**
     * Created on 2011/11/27
     * Title:       setM_dCommon
     * Description:  doubleの値を設定する
     * @param	dCommon
     * @return       無
     * @version        1.0
     */
    public void setM_dCommon(double dCommon) {
    	m_dCommon = dCommon;
    }
}
