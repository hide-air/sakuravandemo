package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.data.JNIInt;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Around_ListItem;
import net.zmap.android.pnd.v2.data.ZPOI_Route_ListItem;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.maps.OpenMap;

import java.util.ArrayList;
import java.util.List;

/**
 * 周辺検索のその他の施設（K-J5_ジャンル検索結果）
 *
 * @author XuYang
 *
 */
public class OtherAroundGenreInquiryResult extends InquiryBaseLoading{

    protected ScrollList scroll = null;
    private ScrollTool oTool = null;
    private TextView oText = null;
    private boolean mbAroundFlg = false; // 沿いルート周辺と施設周辺のフラグ
    private List<ZPOI_Route_ListItem> ardRouteList = new ArrayList<ZPOI_Route_ListItem>();
    private List<POI_Around_ListItem> ardList = new ArrayList<POI_Around_ListItem>();
    private JNILong RecCount = new JNILong();
    private long RecIndex = 0;
//    private long RecCnt = 0;
    private int DIALOG_NO_SEARCH_DATA = 1;
//    private boolean bListDealing = false;
    private  int SearchCacheNum = 0;
    private POI_Around_ListItem[] poi_Around_Listitem = null;
    private ZPOI_Route_ListItem[] poi_Route_Listitem = null;
//    private long RecIndexBtn = 0;
    private int MAX_LIST_COUNT = 0;
    private String msPointName = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startShowPage(NOTSHOWCANCELBTN);

    }

    private void initDialog() {
    	// 沿いルート周辺と施設周辺のフラグを取得

//Del 2011/09/17 Z01_h_yamada Start -->
//        this.setMenuTitle(R.drawable.menu_genre);
//Del 2011/09/17 Z01_h_yamada End <--

        LayoutInflater oInflater = LayoutInflater.from(this);
        LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base_list_freescroll, null);
        Button obtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
        obtn.setVisibility(Button.GONE);

        oText = (TextView)oLayout.findViewById(R.id.inquiry_title);

        // データを初期化
        Intent preIntent = getIntent();
//        RecIndexBtn = preIntent.getLongExtra(Constants.PARAMS_GENRE_INDEX, -1);
        oText.setText(preIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY));


        scroll = (ScrollList)oLayout.findViewById(R.id.scrollList);
        scroll.setAdapter(new BaseAdapter(){

            @Override
            public int getCount() {
                return MAX_LIST_COUNT;
//                if(mbAroundFlg){    //沿いルート周辺検索
//                    return ardRouteList.size();
//                } else {
//                    return ardList.size();
//                }
            }

			// yangyang add start
			@Override
			public boolean isEnabled(int position) {
				return false;
			}

			@Override
			public boolean areAllItemsEnabled() {
				return false;
			}

			// yangyang add end

            @Override
            public Object getItem(int arg0) {
                return null;
            }

            @Override
            public long getItemId(int arg0) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return otherAroundGetView(position, convertView);
            }

        });
//Del 2011/10/06 Z01_h_yamada Start -->
//        scroll.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
        scroll.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
        scroll.setPadding(0, 5, 0, 5);
        oTool = new ScrollTool(this);
        oTool.bindView(scroll);
        setViewInOperArea(oTool);
        setViewInWorkArea(oLayout);

	}

	private boolean initData() {
        return showList();
    }
    /**
     * 検索したデータを表示
     */
    private boolean showList() {
    	NaviLog.d(NaviLog.PRINT_LOG_TAG,"showList start");
        if (mbAroundFlg) { // 沿いルート周辺検索

            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetRecCount(RecCount);
            if (ardRouteList == null) {
                ardRouteList = new ArrayList<ZPOI_Route_ListItem>();
            } else {
                if (!ardRouteList.isEmpty())
                    ardRouteList.clear();
            }
        } else { // 施設周辺検索
            NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetRecCount(RecCount);
            if (ardList == null) {
                ardList = new ArrayList<POI_Around_ListItem>();
            } else {
                if (!ardList.isEmpty())
                    ardList.clear();
            }
        }
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"RecCount=====" + RecCount.lcount);
        showToast(this,RecCount);
        MAX_LIST_COUNT = initPageIndexAndCnt(RecCount);
        if (RecCount.lcount > 0) {

            RecIndex = 0;
//            RecCnt = 0;
            getSearchList(RecIndex);
        }
        if (RecCount.lcount <= 0) {

            runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					NaviLog.d(NaviLog.PRINT_LOG_TAG,"dialog show");
					 showDialog(DIALOG_NO_SEARCH_DATA);
				}

			});
            return false;
        }
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"showList end");
        return true;
    }
    /**
     * 検索したデータのリストを取得
     *
     * @param lRecIndex
     *            index
     */
    protected void getSearchList(long lRecIndex) {
NaviLog.d(NaviLog.PRINT_LOG_TAG,"getSearchList start");
//        bListDealing = true;
        JNILong GetCount = new JNILong();;
        if (mbAroundFlg) { // 沿いルート周辺検索
            if (ardRouteList == null) {
                ardRouteList = new ArrayList<ZPOI_Route_ListItem>();
            }
            else if (!ardRouteList.isEmpty()) {
                ardRouteList.clear();
            }
            long recCount = RecCount.lcount;
            if (RecCount.lcount > SearchCacheNum) {
                recCount = SearchCacheNum;
            }
         // 沿いルート周辺
            if (poi_Route_Listitem == null) {
                poi_Route_Listitem = new ZPOI_Route_ListItem[(int) SearchCacheNum];
                for (int i = 0; i < SearchCacheNum; i++) {
                    poi_Route_Listitem[i] = new ZPOI_Route_ListItem();
                }
            }

            NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POIRoute_GetRecList start lRecIndex==" + lRecIndex + ",recCount==" + recCount);
            NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetRecList(lRecIndex,
                    recCount, poi_Route_Listitem, GetCount);
            NaviLog.d(NaviLog.PRINT_LOG_TAG,"JNI_NE_POIRoute_GetRecList end poi_Route_Listitem==" + poi_Route_Listitem.length + ",GetCount==" + GetCount.lcount);
            for (int i = 0; i < GetCount.lcount; i++) {
                ardRouteList.add(poi_Route_Listitem[i]);
            }
//            RecCnt = GetCount.lcount;
            RecIndex = lRecIndex;
//            bListDealing = false;

        } else { // 施設周辺検索
            if (ardList == null) {
                ardList = new ArrayList<POI_Around_ListItem>();
            }
            else if (!ardList.isEmpty()) {
                ardList.clear();
            }


            long recCount = RecCount.lcount;
            if (RecCount.lcount > SearchCacheNum) {
                recCount = SearchCacheNum;
            }

            if (poi_Around_Listitem == null) {
                poi_Around_Listitem = new POI_Around_ListItem[(int) SearchCacheNum];
                for (int i = 0; i < SearchCacheNum; i++) {
                    poi_Around_Listitem[i] = new POI_Around_ListItem();
                }
            }
            NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetRecList(lRecIndex,
                    recCount, poi_Around_Listitem, GetCount);

            for (int i = 0; i < GetCount.lcount; i++) {
                ardList.add(poi_Around_Listitem[i]);
            }
//            RecCnt = GetCount.lcount;
            RecIndex = lRecIndex;
//            bListDealing = false;
        }
        NaviLog.d(NaviLog.PRINT_LOG_TAG,"getSearchList end");
    }

    /**
     * 検索したデータを存在しない場合、ダイアログを表示
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        final CustomDialog oDialog = new CustomDialog(this);
        Resources oRes = getResources();
        if (id == DIALOG_NO_SEARCH_DATA) {
            oDialog.setTitle(oRes
                    .getString(R.string.around_search_no_data_title));
            oDialog.setMessage(oRes.getString(R.string.tel_dialog_msg));
            //yangyang add start 20110423 Bug1060
            oDialog.setCancelable(true);
            //yangyang add end 20110423 Bug1060
//Chg 2012/04/26 Z01hirama Start --> #4417
//            oDialog.addButton(oRes.getString(R.string.btn_ok),
//                    new OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                        	//yangyang mod start 20110423 Bug1060
////                            finish();
//                        	removeDialog(DIALOG_NO_SEARCH_DATA);
//                        	oDialog.cancel();
//                        	oDialog.dismiss();
//                        	setResult(Constants.RESULTCODE_BACK, getIntent());
//                        	finish();
//                        	//yangyang mod end 20110423 Bug1060
//
//                        }
//
//                    });
//------------------------------------------------------------------------------------------------------------------------------------
            oDialog.addButton(oRes.getString(R.string.btn_ok), new NoSearchDataDialogCancelListener(oDialog));
            oDialog.setOnCancelListener(new NoSearchDataDialogCancelListener(oDialog));
//Chg 2012/04/26 Z01hirama End <-- #4417
            return oDialog;
        }
        // XuYang add start 走行中の操作制限
        return super.onCreateDialog(id);
        // XuYang add end 走行中の操作制限

    }
    /**
     * 表示したデータを取得
     */
    public View otherAroundGetView(int pos, View view) {
        LinearLayout oLayout;
        if (view == null || !(view instanceof LinearLayout)) {
            LayoutInflater inflater;
            inflater = LayoutInflater.from(this);

            // ボタンを追加
            oLayout = (LinearLayout) inflater.inflate(
                    R.layout.inquiry_distance_button_freescroll, null);

            view = oLayout;
        } else {
            oLayout = (LinearLayout) view;
        }
        final int iPos = pos;

        LinearLayout oButton = (LinearLayout)oLayout.findViewById(R.id.LinearLayout_distance);
        TextView txtCon = (TextView)oLayout.findViewById(R.id.TextView_value);
        TextView txtDis = (TextView)oLayout.findViewById(R.id.TextView_distance);

        if (mbAroundFlg) {
            // 沿いルート周辺
            if (pos >= ardRouteList.size()) {
                if (pos % SearchCacheNum == 0) {
                    reGetListRouteData(pos);
                }
            }
            if (!"".equals(ardRouteList.get(pos).getPucName())) {
                oButton.setEnabled(true);
                oButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
                        // TODO Auto-generated method stub
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・その他施設 沿いルート時最終施設選択禁止 Start -->
            			if(DrivingRegulation.CheckDrivingRegulation()){
            				return;
            			}
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・その他施設 沿いルート時最終施設選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                        onItemRouteClick(iPos);
                    }
                });
                txtCon.setText(ardRouteList.get(pos).getPucName());
                txtDis.setVisibility(View.VISIBLE);

                // 距離
                long dis = ardRouteList.get(pos).getDistance();
                String s = String.valueOf(dis);
                txtDis.setText("(" + s + "m" + ")");
                if (dis >= 1000) {
                    double distance = (double) dis / (double) 1000;
                    int t = (int) (distance * 10);
                    distance = (double) t / (double) 10;
                    s = String.valueOf(distance);
                    txtDis.setText("(" + s + "km" + ")");
                }

            } else {
                oButton.setEnabled(false);
                txtCon.setText("");
                txtDis.setText("");
            }

        } else {
            if (pos >= ardList.size()) {
                if (pos % SearchCacheNum == 0) {
                    reGetListAroundData(pos);
                }
            }

            if (!"".equals(ardList.get(pos).getPucName())) {
                oButton.setEnabled(true);
                oButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
                        // TODO Auto-generated method stub
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・その他施設結果 選択禁止 Start -->
            			if(DrivingRegulation.CheckDrivingRegulation()){
            				return;
            			}
// ADD 2013.08.08 M.Honma 走行規制 周辺検索・その他施設結果 選択禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
                        onItemClick(iPos);
                    }
                });
                txtCon.setText(ardList.get(pos).getPucName());
                // 娯楽,買物...等6ボタンを表示するかどうかを判断する

                txtDis.setVisibility(View.VISIBLE);
                // 距離
                long dis = ardList.get(pos).getDistance();
                String s = String.valueOf(dis);
                txtDis.setText("(" + s + "m" + ")");
                if (dis >= 1000) {
                    double distance = (double) dis / (double) 1000;
                    int t = (int) (distance * 10);
                    distance = (double) t / (double) 10;
                    s = String.valueOf(distance);
                    txtDis.setText("(" + s + "km" + ")");
                }

            } else {
                oButton.setEnabled(false);
                txtCon.setText("");
                txtDis.setText("");
            }

        }
        return view;
    }
    private void onItemClick(int wPos) {
//Add 2012/02/07 katsuta Start --> #2698
//		NaviLog.d(NaviLog.PRINT_LOG_TAG, "OtherAroundGenreInquiryResult onItemClick ================== bIsListClicked: " + bIsListClicked);
			if (bIsListClicked) {
				return;
			}
			else {
				bIsListClicked = true;
			}
//Add 2012/02/07 katsuta End <--#2698
        int index = wPos;
        msPointName = ardList.get(index).getPucName();
        long Longitude = 0;
        long Latitude = 0;
        JNIInt MapScale = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
        Latitude = ardList.get(index).getLatitude();
        Longitude = ardList.get(index).getLongitude();

        POIData oData = new POIData();
        oData.m_sName = msPointName;
        oData.m_sAddress = msPointName;
        oData.m_wLong = Longitude;
        oData.m_wLat = Latitude;
     // XuYang add start #1056
        Intent oIntent = getIntent();
        if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
            CommonLib.setChangePosFromSearch(true);
            CommonLib.setChangePos(false);
        } else {
            CommonLib.setChangePosFromSearch(false);
            CommonLib.setChangePos(false);
        }
        // XuYang add end #1056
        OpenMap.moveMapTo(this, oData,
                getIntent(), AppInfo.POI_LOCAL,
                Constants.LOCAL_INQUIRY_REQUEST);
        this.createDialog(Constants.DIALOG_WAIT, -1);
    }
    /**
     * 沿いルート周辺検索の場合、データを検索
     *
     * @param wPos
     *            データのindex
     */
    private void onItemRouteClick(int wPos) {

//Add 2012/02/07 katsuta Start --> #2698
//		NaviLog.d(NaviLog.PRINT_LOG_TAG, "OtherAroundGenreInquiryResult onItemRouteClick ================== bIsListClicked: " + bIsListClicked);
			if (bIsListClicked) {
				return;
			}
			else {
				bIsListClicked = true;
			}
//Add 2012/02/07 katsuta End <--#2698
    	this.createDialog(Constants.DIALOG_WAIT, -1);

    	int index = wPos;

        msPointName = ardRouteList.get(index).getPucName();
        long Longitude = 0;
        long Latitude = 0;
        JNIInt MapScale = new JNIInt();
        NaviRun.GetNaviRunObj().JNI_NE_GetMapScale(MapScale);
        Latitude = ardRouteList.get(index).getLatitude();
        Longitude = ardRouteList.get(index).getLongitude();

        POIData oData = new POIData();
        oData.m_sName = msPointName;
        oData.m_sAddress = msPointName;
        oData.m_wLong = Longitude;
        oData.m_wLat = Latitude;
      //yangyang mod start Bug813
//        Intent oIntent = getIntent();
//        oIntent.setClass(this, RouteEdit.class);
//        oIntent.putExtra(AppInfo.POI_DATA, oData);
//        oIntent.putExtra(Constants.PARAMS_ROUTE_MOD_FLAG, true);
//        oIntent.putExtra(Constants.ROUTE_TYPE_KEY,Constants.ROUTE_TYPE_NOW);
//        this.startActivityForResult(oIntent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);

        CommonLib.routeSearchFlag = true;
        CommonLib.setChangePosFromSearch(true);
        CommonLib.setChangePos(false);
        OpenMap.moveMapTo(this, oData,
        		getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);
      //yangyang mod end Bug813

    }
    public void reGetListRouteData(long lRecIndex) {
        JNILong getCnt = new JNILong();
        poi_Route_Listitem = new ZPOI_Route_ListItem[(int) SearchCacheNum];
        for (int i = 0; i < SearchCacheNum; i++) {
            poi_Route_Listitem[i] = new ZPOI_Route_ListItem();
        }
        NaviRun.GetNaviRunObj().JNI_NE_POIRoute_GetRecList(lRecIndex,
                SearchCacheNum, poi_Route_Listitem, getCnt);
        for (int i = 0; i < getCnt.lcount; i++) {
            ardRouteList.add(poi_Route_Listitem[i]);
        }
    }
    public void reGetListAroundData(long lRecIndex) {
        JNILong getAroundCnt = new JNILong();
        poi_Around_Listitem = new POI_Around_ListItem[(int) SearchCacheNum];
        for (int i = 0; i < SearchCacheNum; i++) {
            poi_Around_Listitem[i] = new POI_Around_ListItem();
        }



        NaviRun.GetNaviRunObj().JNI_NE_POIArnd_GetRecList(lRecIndex,
                SearchCacheNum, poi_Around_Listitem, getAroundCnt);
        for (int i = 0; i < getAroundCnt.lcount; i++) {
            ardList.add(poi_Around_Listitem[i]);
        }
    }

	@Override
	protected boolean onStartShowPage() throws Exception {
		SearchCacheNum = ONCE_GET_COUNT;
		CommonLib.isAround = true;
        mbAroundFlg = CommonLib.isAroundFlg();
        initData();
		return true;
	}

	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {

		if(bGetData)
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage start");
					initDialog();
					NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage end");
				}



			});
		}

	}
	//yangyang add start 20110423

	@Override
	protected boolean onClickGoMap() {
		NaviLog.d(NaviLog.PRINT_LOG_TAG,"mbAroundFlg===" + mbAroundFlg);
		if (mbAroundFlg) {
			NaviRun.GetNaviRunObj().JNI_NE_POIRoute_Clear();
		} else {
//		NaviRun.GetNaviRunObj().JNI_NE_POIArnd_Cancel();
			NaviRun.GetNaviRunObj().JNI_NE_POI_Around_Clear();
		}
		return super.onClickGoMap();
	}
	//yangyang add end 20110423


//Add 2012/04/26 Z01hirama Start --> #4417
	private class NoSearchDataDialogCancelListener implements OnClickListener,
															  OnCancelListener
	{
		CustomDialog mDialog = null;

		public NoSearchDataDialogCancelListener(CustomDialog dialog) {
			mDialog = dialog;
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			BeforeCloseDialogProc();
		}

		@Override
		public void onClick(View v) {
        	removeDialog(DIALOG_NO_SEARCH_DATA);
        	mDialog.cancel();
        	mDialog.dismiss();
			BeforeCloseDialogProc();
		}

		private void BeforeCloseDialogProc() {
        	//yangyang mod start 20110423 Bug1060
//            finish();
        	setResult(Constants.RESULTCODE_BACK, getIntent());
        	finish();
        	//yangyang mod end 20110423 Bug1060
		}
	}
//Add 2012/04/26 Z01hirama End <-- #4417
}
