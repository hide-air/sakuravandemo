/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           POI_Address_ListItem.java
 * Description    取得レコードリスト
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;


public class POI_Address_ListItem {
    private long m_lAreaNumber;
    private long m_lNameSize;
    private String m_Name;
    private long m_lLongitude;
    private long m_lLatitude;
    private int m_iNextCategoryFlag;    //　次レベルのデータリストがあるかとうか？
    private int m_iOldIndex;            // POIからのオフセット
    private boolean m_bBtnSts;              // 都道府県のデータ
    /**
    * Created on 2010/08/06
    * Title:       setM_Name
    * Description:  表示名称を設定する
    * @param1  無し
    * @return       void

    * @version        1.0
    */
    public void setM_Name(String name) {
        m_Name = name;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lAreaNumber
    * Description:  広域インデックスを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lAreaNumber() {
        return m_lAreaNumber;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lNameSize
    * Description:  表示名称サイズを取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lNameSize() {
        return m_lNameSize;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_Name
    * Description:  表示名称を取得する
    * @param1  無し
    * @return       String

    * @version        1.0
    */
    public String getM_Name() {
        return m_Name;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lLongitude
    * Description:  経度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLongitude() {
        return m_lLongitude;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lLatitude
    * Description:  緯度を取得する
    * @param1  無し
    * @return       long

    * @version        1.0
    */
    public long getM_lLatitude() {
        return m_lLatitude;
    }

    /**
    * Created on 2010/08/06
    * Title:       getM_lLatitude
    * Description:  次カテゴリへのオフセットフラグ(TRUE : データ有，FALSE : データ無)を取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getM_iNextCategoryFlag() {
        return m_iNextCategoryFlag;
    }

    /**
     * <p>Description:[...]</p>
     * @param m_iOldIndex The m_iOldIndex to set.
     */
    public void setM_iOldIndex(int m_iOldIndex) {
        this.m_iOldIndex = m_iOldIndex;
    }

    /**
     * <p>Description:[...]</p>
     * @return int m_iOldIndex.
     */
    public int getM_iOldIndex() {
        return m_iOldIndex;
    }

    /**
     * <p>Description:[...]</p>
     * @param m_bBtnSts The m_bBtnSts to set.
     */
    public void setM_bBtnSts(boolean m_bBtnSts) {
        this.m_bBtnSts = m_bBtnSts;
    }

    /**
     * <p>Description:[...]</p>
     * @return bool m_bBtnSts.
     */
    public boolean isM_bBtnSts() {
        return m_bBtnSts;
    }
}
