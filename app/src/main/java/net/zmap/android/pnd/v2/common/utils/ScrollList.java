package net.zmap.android.pnd.v2.common.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.Rect;
import android.view.View;
import android.util.AttributeSet;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.DrivingRegulation;
import android.view.MotionEvent;

public class ScrollList extends ListView implements ScrollListener,		OnScrollListener {
	private int m_wCountInPage = 0;
//	private int m_wPos = 0;
	private boolean m_bIsScroll = false;
	private ScrollControlListener m_oListener = null;

	public ScrollList(Context context) {
		super(context);
		init();
	}

	public ScrollList(Context oContext, AttributeSet oSet) {
		super(oContext, oSet);
		init();
	}

	public void reset() {
		BaseAdapter oAdapter = (BaseAdapter) getAdapter();
		oAdapter.notifyDataSetChanged();
	}

	public int getCountInPage() {
		return m_wCountInPage;
	}


	@Override
	protected void onLayout(boolean changed, int left, int top, int right,int bottom)
	{

		super.onLayout(changed, left, top, right, bottom);

		int screenHeight = 0;
		int totalHeight = 0;

		m_wCountInPage = 0;
		m_bIsScroll = false;

		if ( getItemHeight() > 0 ) {
			screenHeight = getHeight() - getPaddingTop() - getPaddingBottom();
			totalHeight  = getCount() *(getItemHeight() +getDividerHeight()) -getDividerHeight();
			if ( totalHeight <= screenHeight ) {
				// 画面内にすべて収まる
				m_wCountInPage = getCount();
				m_bIsScroll = false;
			} else {
				m_wCountInPage = (int)getShowItemCount();
				m_bIsScroll = true;
			}
		}
		if (m_oListener != null) {
			m_oListener.update();
		}

//		NaviLog.d("ScrollList", "onLayout() " + " screenHeight=" + screenHeight + " totalHeight=" + totalHeight + " m_wCountInPage=" + m_wCountInPage + " m_bIsScroll=" + m_bIsScroll);
	}

	private void init() {

		setOnScrollListener(this);

		setDivider(new Drawable() {

			@Override
			public void draw(Canvas arg0) {
			}

			@Override
			public int getOpacity() {
				return 0;
			}

			@Override
			public void setAlpha(int arg0) {
			}

			@Override
			public void setColorFilter(ColorFilter arg0) {
			}

		});
		setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
		setCacheColorHint(0x00000000);
		setVerticalScrollBarEnabled(false);

	}

//	private boolean isOverOnePage() {
//		return (m_bIsScroll && m_wCountInPage +1 == getCount());
//	}

	@Override
	public boolean isHasNextPage() {
		if (getLastVisiblePosition() == (getCount() - 1)) {
//			NaviLog.d("ScrollList", "isHasNextPage() isOverOnePage=" + isOverOnePage() + " m_wPos=" +  m_wPos);
//Chg 2012/04/04 hirama Start -->  refs 4016
//			if (isOverOnePage() && m_wPos <= 0) {
//				return true;
//			}
			boolean isLastitemTrue = true;
			int maxitemoffset = 0;
			int childcnt = getChildCount();
			View lastitemview = null;
			for(int index=0; index < childcnt; index++)
			{
				Rect visiblerect = new Rect();
				getChildAt(index).getGlobalVisibleRect(visiblerect);

				int childposition = getPositionForView(getChildAt(index));
				if(childposition == getLastVisiblePosition())
				{
					lastitemview = getChildAt(index);
					if(maxitemoffset < visiblerect.top && ((visiblerect.top - maxitemoffset) >= visiblerect.height()))
					{
						maxitemoffset = visiblerect.top;
					}
					else
					{
						isLastitemTrue = false;
					}
				}

				if(maxitemoffset < visiblerect.top)
				{
					if(lastitemview != null)
					{
						lastitemview = null;
						// 実際は最後のアイテムがちゃんとリストの最後に配置されていない場合がある為.
						// 最後のアイテムであることが正しいかをフラグでも管理する.
						isLastitemTrue = false;
					}

					maxitemoffset = visiblerect.top;
				}

				if(!isLastitemTrue)
				{	// ListViewのAPIの返却値が不正で最後のアイテムが一番下でない場合はスクロール可.
					return true;
				}
			}


			if(lastitemview != null)
			{
				Rect lastvisiblerect = new Rect();
				lastitemview.getGlobalVisibleRect(lastvisiblerect);
				if(lastitemview.getHeight() > lastvisiblerect.height())
				{
					return true;
				}
			}
//Chg 2012/04/04 Z01hirama End <--  refs 4016
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean isHasPreviousPage() {
		if (getFirstVisiblePosition() == 0) {
//			NaviLog.d("ScrollList", "isHasPreviousPage() isOverOnePage=" + isOverOnePage() + " m_wPos=" +  m_wPos);
//Del 2012/04/05 Z01hirama Start --> refs 4016
//			if (isOverOnePage() && m_wPos > 0) {
//				return true;
//			}
//Del 2012/04/05 Z01hirama End <-- refs 4016
//Add 2012/04/05 Z01hirama Start --> refs 4016
			boolean isFirstitemTrue = true;
			Rect listrect = new Rect();
			getGlobalVisibleRect(listrect);
			int minimamitemoffset = listrect.bottom;
			int childcnt = getChildCount();
			View firstitemview = null;
			for(int index=0; index < childcnt; index++)
			{
				Rect visiblerect = new Rect();
				getChildAt(index).getGlobalVisibleRect(visiblerect);
				int childposition = getPositionForView(getChildAt(index));
				if(childposition == getFirstVisiblePosition())
				{
					firstitemview = getChildAt(index);
					if(minimamitemoffset > visiblerect.top && 0 <= visiblerect.top)
					{
						minimamitemoffset = visiblerect.top;
					}
					else
					{
						isFirstitemTrue = false;
					}
				}

				if(minimamitemoffset > visiblerect.top && 0 <= visiblerect.top)
				{
					if(firstitemview != null)
					{
						firstitemview = null;
						// 実際は最初のアイテムが表示領域をはみだして配置されている場合がある為.
						// 表示されている最初のアイテムであることが正しいかをフラグでも管理する.
						isFirstitemTrue = false;
					}

					minimamitemoffset = visiblerect.top;
				}

				if(!isFirstitemTrue)
				{	// ListViewのAPIの返却値が不正で最後のアイテムが一番下でない場合はスクロール可.
					return true;
				}
			}


			if(firstitemview != null)
			{
				Rect lastvisiblerect = new Rect();
				firstitemview.getGlobalVisibleRect(lastvisiblerect);
				if(firstitemview.getHeight() > lastvisiblerect.height())
				{
					return true;
				}
			}
//Add 2012/04/05 Z01hirama End <-- refs 4016
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void moveNextPage() {

//		m_wPos++;
		int posIndex = getLastVisiblePosition() + m_wCountInPage;

		final int maxIndex = getAdapter().getCount()-1;
		int toIndex = posIndex;
		if (posIndex > maxIndex) {
			toIndex = maxIndex;
		} else if (posIndex >= m_wCountInPage *2 ) {
			toIndex--;
		}

		if (toIndex == 0) {
			if (getLastVisiblePosition() > 1 ) {
				smoothScrollToPosition(1);
			}
			this.scroll(toIndex);

		} else if (toIndex == maxIndex) {
			if (getLastVisiblePosition() < maxIndex ) {
				smoothScrollToPosition(maxIndex-1);
			}
			this.scroll(toIndex);
		} else {
		    smoothScrollToPosition(toIndex);
		}

//		NaviLog.d("ScrollList", "moveNextPage() toIndex=" + toIndex + " getLastVisiblePosition()=" +  getLastVisiblePosition() + " m_wCountInPage=" + m_wCountInPage + " getItemHeight()=" + getItemHeight());
	}


	@Override
	public void movePreviousPage() {

//		m_wPos--;
		int posIndex = getFirstVisiblePosition() - m_wCountInPage;

		int toIndex = posIndex;
		final int maxIndex = getAdapter().getCount()-1;
		if (posIndex <= 0) {
			toIndex = 0;
		}else{
			toIndex++;
		}

		if (toIndex == 0) {
			if (getFirstVisiblePosition() > 1 ) {
				smoothScrollToPosition(1);
			}
			this.scroll(toIndex);

		} else if (toIndex == maxIndex) {
			if (getLastVisiblePosition() < maxIndex ) {
				smoothScrollToPosition(maxIndex-1);
			}
			this.scroll(toIndex);
		} else {
		    smoothScrollToPosition(toIndex);
		}

//NaviLog.d("ScrollList", "movePreviousPage() toIndex=" + toIndex + " getLastVisiblePosition()=" +  getLastVisiblePosition() + " m_wCountInPage=" + m_wCountInPage + " getItemHeight()=" + getItemHeight());
	}

	@Override
	public void setControlListener(ScrollControlListener oListener) {
		m_oListener = oListener;
	}


	@Override
	public void onScroll(AbsListView arg0, int wIndex, int wPage, int wCount) {

		if (m_oListener != null) {
//NaviLog.d("ScrollList", "onScroll() wIndex=" + wIndex + " wPage=" + wPage + " wCount=" + wCount + " getHeight()=" +getHeight());

			if ( getItemHeight() > 0) {

				float page = getShowItemCount();
				if ( page > getItemCount() ) {
					page = getItemCount();
				}
//				NaviLog.d("ScrollList", "onScroll  wIndex=" + wIndex + " page=" + page +" wCount=" + wCount);

				m_oListener.update(wIndex, page, wCount);
			}
		}


	}

	private float getShowItemCount() {

		int i, totalHeight = 0;
		int layoutHeight = getHeight() - getPaddingTop() - getPaddingBottom();
		float showCount = 0;

		for ( i = 0; i < getItemCount(); i++ )
		{
			int H = 0;
			if ( i == 0 ) {
				H = getItemHeight();
			} else {
				H = getItemHeight() +getDividerHeight();
			}
			if ( totalHeight + H <= layoutHeight ) {
				totalHeight += H;
				showCount = i +1;
			} else {
				showCount += (float)(layoutHeight -totalHeight) /(float)H;
				break;
			}
		}

		return showCount;
	}


	private int getItemCount() {
		return getAdapter().getCount();
	}

	private int getItemHeight() {
		if (0 < this.getChildCount()) {
			return this.getChildAt(0).getHeight();
		}
		return 0;
	}


	public int m_oScrollState = 0;

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		m_oScrollState = scrollState;
	}

	public void scroll(int wIndex) {
		setSelection(wIndex);
		m_bIsScroll = false;
	}

	public boolean IsScroll() {
		return m_bIsScroll;
	}
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ListViewスクロール禁止 Start -->
	@Override
	public boolean onInterceptTouchEvent(MotionEvent event)
	{
    	if(DrivingRegulation.GetDrivingRegulationFlg())
    	{
			return false;
    	}
		else
		{
			return super.onInterceptTouchEvent(event);
		}
	}
//ADD 2013.08.08 M.Honma 走行規制 ListViewスクロール禁止 End <--
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
}
