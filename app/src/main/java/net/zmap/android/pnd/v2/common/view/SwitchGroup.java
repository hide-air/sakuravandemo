package net.zmap.android.pnd.v2.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.services.SystemService;
import net.zmap.android.pnd.v2.common.utils.ScreenAdapter;

import java.util.Vector;

public class SwitchGroup extends LinearLayout
{
// CHG 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 Start -->
	/*
	private int[] m_wResId = null;
	private int[] m_wText = null;
	private SwitchGroupListener m_oListener = null;
	private Vector<ToggleButton> m_ovList = new Vector<ToggleButton>();
	private int flgBtnWidth = 0;
	*/
	protected int[] m_wResId = null;
	protected int[] m_wText = null;
	protected SwitchGroupListener m_oListener = null;
	protected Vector<ToggleButton> m_ovList = new Vector<ToggleButton>();
	protected int flgBtnWidth = 0;
// CHG 2013.08.08 M.Honma 走行規制 ルート編集 「推奨～別ルート」禁止 End <--
	public SwitchGroup(Context oContext)
	{
		super(oContext);
//		this.setOrientation(VERTICAL);
	}

	public SwitchGroup(Context oContext, AttributeSet oSet)
	{

		super(oContext, oSet);
		// 間隔フラグを取得（0：横の時間隔を追加：1：縦の時間隔を追加；2：間隔がない）
		String btnWidth = oSet.getAttributeValue(null, "switch_5width");
		if (btnWidth != null) {
			int strBtnWidth = SystemService.toInt(oSet.getAttributeValue(null, "switch_5width"));
			if (strBtnWidth != 0) {
				flgBtnWidth = strBtnWidth;
			}
		}
		// ボタン横表示のフラグを取得
		String strOri = oSet.getAttributeValue(null, "switch_orientation");
		if (strOri != null) {
		    this.setOrientation(VERTICAL);
		} else {
			this.setOrientation(HORIZONTAL);
		}

		int wSize = SystemService.toInt(oSet.getAttributeValue(null, "switch_size"));
		if(wSize <= 1)
		{
			wSize = 2;
		}

		String sValue;
		m_wResId = new int[wSize];
		m_wText = new int[wSize];
		for(int i = 0;i < m_wResId.length;++i)
		{
			sValue = oSet.getAttributeValue(null, "style" + (i + 1));
			if(sValue != null)
			{
				m_wResId[i] = getResources().getIdentifier(sValue,
						"drawable", getContext().getPackageName());
			}
			else
			{
				m_wResId[i] = -1;
			}

			sValue = oSet.getAttributeValue(null, "text" + (i + 1));
			if(sValue != null)
			{
				m_wText[i] = getResources().getIdentifier(sValue,
						"string", getContext().getPackageName());
			}
			else
			{
				m_wText[i] = -1;
			}
		}
		setSize(wSize);

	}

	public void setSize(int wSize)
	{
		if(wSize > 0)
		{
			removeAllViews();

			ToggleButton oButton;
//Chg 2011/08/11 Z01thedoanh (自由解像度対応) Start -->
//			DisplayMetrics dm = getResources().getDisplayMetrics();
//			int intMargin = (int) dm.density * 5;
//------------------------------------------------
			int intMargin = (int) ScreenAdapter.getScale() * 5;
//Chg 2011/08/11 Z01thedoanh (自由解像度対応) End <--

			LinearLayout.LayoutParams oParams = new LinearLayout.LayoutParams(
					intMargin, LinearLayout.LayoutParams.FILL_PARENT);
			LinearLayout.LayoutParams oParams1 = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT, intMargin);

			for(int i = 0;i < wSize;++i)
			{
				final int wIndex = i;
				oButton = new ToggleButton(getContext());
				if(m_wResId != null && m_wResId[i] != -1)
				{
					oButton.setBackgroundResource(m_wResId[i]);
				}
				oButton.setOnCheckedChangeListener(new OnCheckedChangeListener(){

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked)
					{
						if(isChecked)
						{
							setSelected(wIndex);
							buttonView.setTextColor(getResources().getColor(R.color.switch_on));
						}
						else
						{
							buttonView.setTextColor(getResources().getColor(R.color.switch_off));
						}
					}

				});
				oButton.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View oView)
					{
					    if (oView instanceof ToggleButton) {
	                        ToggleButton oButton = (ToggleButton)oView;
	                        if(!oButton.isChecked())
	                        {
	                            oButton.setChecked(true);
	                        }
					    }
					}

				});
				if(m_wText != null && m_wText[i] != -1)
				{
					oButton.setText(getResources().getString(m_wText[i]));
					oButton.setTextOff(getResources().getString(m_wText[i]));
					oButton.setTextOn(getResources().getString(m_wText[i]));
				}
				else
				{
					oButton.setText("");
					oButton.setTextOff("");
					oButton.setTextOn("");
				}
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
					oButton.setPadding(0, 0, 0, 0);
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
				oButton.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimension(R.dimen.Common_Huge_textSize));
				oButton.postInvalidate();
//Chg 2011/08/15 Z01_h_yamada Start -->
//				addView(oButton);
//--------------------------------------------
				addView(oButton, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT, 1));
//Chg 2011/08/15 Z01_h_yamada End <--
				m_ovList.addElement(oButton);
				if (flgBtnWidth == 0) {
					// ボタン横表示の時、
					if(i != wSize - 1)
					{
						LinearLayout oLayout = new LinearLayout(getContext());
						addView(oLayout,oParams);
					}
				} else if (flgBtnWidth == 1) {
					// ボタン縦表示
					if(i != wSize - 1)
					{
						LinearLayout oLayout = new LinearLayout(getContext());
						addView(oLayout,oParams1);
					}
				}
			}
		}
	}

	public void setSelected(int wSelect)
	{
		if(wSelect >= 0 && wSelect < m_ovList.size())
		{
			ToggleButton oButton;
			for(int i = 0;i < m_ovList.size();++i)
			{
				oButton = m_ovList.elementAt(i);
				if(wSelect == i)
				{
					oButton.setChecked(true);
					oButton.setTextColor(getResources().getColor(R.color.switch_on));
					if(m_oListener != null)
					{
						m_oListener.onSelectChange(i);
					}
				}
				else
				{
					oButton.setChecked(false);
                    oButton.setTextColor(getResources().getColor(R.color.switch_off));
				}
			}
		}
	}

	public int getSelected()
	{
		int wIndex = -1;
		ToggleButton oButton;
		for(int i = 0;i < m_ovList.size();++i)
		{
			oButton = m_ovList.elementAt(i);
			if(oButton.isChecked())
			{
				wIndex = i;
				break;
			}
		}
		return wIndex;
	}

	public void setListener(SwitchGroupListener oListener)
	{
		m_oListener = oListener;
	}

	public ToggleButton getView(int wIndex)
	{
		ToggleButton oButton = null;
		if(wIndex >= 0 && wIndex < m_ovList.size())
		{
			oButton = m_ovList.elementAt(wIndex);
		}
		return oButton;
	}
}
