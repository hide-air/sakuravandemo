/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project		MarketV2
 * File			SystemService.java
 * Description	当該クラスは共通のメソッドをいくつか提供する。
 * Created on	2010/11/10
 * Author		kexin
 *
********************************************************************
 */
package net.zmap.android.pnd.v2.common.services;

import android.app.Activity;
import android.content.Context;
import android.view.Window;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import net.zmap.android.pnd.v2.common.utils.NaviLog;

/**
 * 当該クラスは共通のメソッドをいくつか提供する。
 */
public class SystemService
{

	/** バージョン比較エラーの場合のflag */
	public static final short COMPARE_ERROR = -1;

	/** バージョン比較で、同じの場合のflag */
	public static final short COMPARE_EQUAL = 0;

	/** バージョン比較で、前者が後者より大きい場合のflag */
	public static final short COMPARE_GREATER = 1;

	/** バージョン比較で、前者が後者より小さい場合のflag */
	public static final short COMPARE_LESS = 2;

	/**
	 * バージョン比較
	 *
	 * @param sVer1 比較しようとする一つ目のバージョンのバージョン番号
	 * @param sVer2 比較しようとする二つ目のバージョンのバージョン番号
	 *
	 * @return 比較結果であり、有効値がCOMPARE_ERROR，COMPARE_EQUAL，COMPARE_GREATER，
COMPARE_LESSである。
	 */
	public static int compareVersion(String sVer1,String sVer2)
	{
		int wRe = COMPARE_ERROR;
		if(sVer1 != null && sVer2 != null)
		{
			if(!sVer1.equals(sVer2))
			{
				try
				{
					int wNum1 = 0;
					int wNum2 = 0;
					int wBegin1 = 0;
					int wBegin2 = 0;
					int wIndex1 = sVer1.indexOf('.');
					int wIndex2 = sVer2.indexOf('.');
					sVer1 = sVer1 + ".";
					sVer2 = sVer2 + ".";
					while(wIndex1 != -1 || wIndex2 != -1)
					{
						wNum1 = Integer.parseInt(sVer1.substring
(wBegin1,wIndex1));
						wNum2 = Integer.parseInt(sVer2.substring
(wBegin2,wIndex2));
						wBegin1 = wIndex1 + 1;
						wBegin2 = wIndex2 + 1;
						wIndex1 = sVer1.indexOf('.', wBegin1);
						wIndex2 = sVer2.indexOf('.', wBegin2);
						if(wNum1 > wNum2)
						{
							wRe = COMPARE_GREATER;
							break;
						}
						else if(wNum1 < wNum2)
						{
							wRe = COMPARE_LESS;
							break;
						}
					}

					if(wRe == COMPARE_ERROR)
					{
						if(wIndex1 == -1 && wIndex2 != -1)
						{
							wRe = COMPARE_LESS;
						}
						else if(wIndex1 != -1 && wIndex2 == -1)
						{
							wRe = COMPARE_GREATER;
						}
					}
				}
				catch(Exception e)
				{
					NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
				}
			}
			else
			{
				wRe = COMPARE_EQUAL;
			}
		}
		return wRe;
	}

	/**
	 * Activityを全画面モードに設定する
	 *
	 * @param oActivity Activityの操作ハンドル
	 */
	public static void setFullScreen(Activity oActivity)
	{
		oActivity.requestWindowFeature(Window.FEATURE_NO_TITLE);
//Del 2011/07/11 Z01thedoanh Start -->
//		oWindow.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//		WindowManager.LayoutParams.FLAG_FULLSCREEN);
//Del 2011/07/11 Z01thedoanh End <--
	}

	/**
	 * 文字列を強制的にintタイプのデータに変換する。
	 *
	 * @param sStr 文字列データ
	 *
	 * @return 変換後のデータである。エラーが発生した場合、デフォルト値の0を返す。
	 */
	public static int toInt(String sStr)
	{
		int wRe = 0;
		if(sStr != null && sStr.length() > 0)
		{
			try
			{
				wRe = Integer.parseInt(sStr);
			}
			catch(Exception e)
			{
				NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
			}
		}
		return wRe;
	}

	/**
	 * 時間をフォーマットする。
	 *
	 * @return フォーマット後の時間文字列
	 */
	public static String getDate()
	{
		Calendar oCalendar = Calendar.getInstance();
        SimpleDateFormat oFormat = new SimpleDateFormat("yyyyMMddHHmm");
        return oFormat.format(oCalendar.getTime());
	}

	/**
	 * 時間をフォーマットする。
	 *
	 * @param sFormat 出力フォーマット
	 *
	 * @return フォーマット後の時間文字列
	 */
	public static String getDate(String sFormat)
	{
		Calendar oCalendar = Calendar.getInstance();
        SimpleDateFormat oFormat = new SimpleDateFormat(sFormat);
        return oFormat.format(oCalendar.getTime());
	}

	/**
	 * 提示情報を表示する。
	 *
	 * @param oContext Contextの操作ハンドル
	 * @param sText 提示する内容
	 */
	public static void showMessage(Context oContext,String sText)
	{
		if(oContext != null && sText != null)
		{
			Toast oToast = Toast.makeText(oContext, sText, Toast.LENGTH_SHORT);
			oToast.show();
		}
	}

	/**
	 * 提示情報を表示する。
	 *
	 * @param oContext Contextの操作ハンドル
	 * @param wId 内容を提示しようとするリソースid番号
	 */
	public static void showMessage(Context oContext,int wId)
	{
		if(oContext != null)
		{
			Toast oToast = Toast.makeText(oContext, wId, Toast.LENGTH_SHORT);
			oToast.show();
		}
	}
}
