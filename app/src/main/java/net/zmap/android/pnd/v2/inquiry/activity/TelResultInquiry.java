//******************************************************************
//Copyright (C) 2010 ZDC Co,Ltd. All Rights Reserved.
//
//-----------------------------------------------------------------------
//システム名： MarketV2
//-----------------------------------------------------------------------
//Version　 Author　　 Date　　　 Note
//1.0.0     yangyang　 2010/12/31     新規
//******************************************************************

package net.zmap.android.pnd.v2.inquiry.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
import net.zmap.android.pnd.v2.common.activity.InquiryBaseLoading;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
import net.zmap.android.pnd.v2.common.utils.ScrollList;
import net.zmap.android.pnd.v2.common.utils.ScrollTool;
import net.zmap.android.pnd.v2.common.view.ScrollBoxAdapter;
import net.zmap.android.pnd.v2.data.JNILong;
import net.zmap.android.pnd.v2.data.NaviRun;
import net.zmap.android.pnd.v2.data.POI_Tel_ListItem;
import net.zmap.android.pnd.v2.inquiry.data.POIData;
import net.zmap.android.pnd.v2.maps.OpenMap;

/**
 * 電話番号検索結果画面
 * */
public class TelResultInquiry extends InquiryBaseLoading implements ScrollBoxAdapter{


	/** データを取得するとき、開始のインディクス */
	private long RecIndex = 0;
//	private long oldRecIndex = 0;
//	/** ページのインディクス */
//	private int pageIndex = 0;

	/** スクロールできる総件数 */
	private int allRecordCount = 0;
	/** JNIの出力引数 */

	private JNILong Rec = new JNILong();
	/** JNIから取得したデータ */
	private POI_Tel_ListItem[] m_Poi_Tel_Listitem = null;
	/** 表示した内容 */
	private POI_Tel_ListItem[] m_Poi_Show_Listitem = null;
	private static TelResultInquiry instance;

	private int getRecordIndex = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		oIntent = this.getIntent();
		instance = this;
		startShowPage(NOTSHOWCANCELBTN);


//		initDialog();

	}

	public static TelResultInquiry getInstance() {
		return instance;
	}


	private void initDialog() {
		Intent oIntent = getIntent();
//Del 2011/09/17 Z01_h_yamada Start -->
//		//タイトルを設定する
//		int titleId = oIntent.getIntExtra(Constants.PARAMS_TITLE, 0);
//		if (0!= titleId) {
//			this.setMenuTitle(titleId);
//		}
//Del 2011/09/17 Z01_h_yamada End <--

		//画面を初期化する
		LayoutInflater oInflater = LayoutInflater.from(this);
		LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.inquiry_base_list_freescroll, null);
		Button obtn = (Button) oLayout.findViewById(R.id.inquiry_btn);
		obtn.setVisibility(Button.GONE);
		TextView oText = (TextView)oLayout.findViewById(R.id.inquiry_title);
		//検索内容を初期化する
		String sTitle = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
		if(sTitle == null)
		{
			oText.setText(R.string.tel_inquiry_title);
		}
		else
		{
			oText.setText(this.getResources().getString(R.string.tel_inquiry_title) + sTitle);
		}

//		final ScrollBox oBox = (ScrollBox)oLayout.findViewById(R.id.scrollBox);
//		oBox.setAdapter(this);
		ScrollList oList = (ScrollList)oLayout.findViewById(R.id.scrollList);
//Del 2011/10/06 Z01_h_yamada Start -->
//		oList.setCountInPage(5);
//Del 2011/10/06 Z01_h_yamada End <--
		oList.setDividerHeight((int)getResources().getDimensionPixelSize(R.dimen.List_dividerheight));
		oList.setAdapter(listAdapter);

		ScrollTool oTool = new ScrollTool(this);
//		oTool.bindView(oBox);
		oTool.bindView(oList);
		setViewInOperArea(oTool);

		this.setViewInWorkArea(oLayout);

	}


	private void initCount() {
		JNILong ListCount = new JNILong();
		NaviRun.GetNaviRunObj().JNI_NE_POITel_GetRecCount(ListCount);
		showToast(this,ListCount);
		allRecordCount = initPageIndexAndCnt(ListCount);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"allRecordCount====" + allRecordCount);
	}


	@Override
	protected void search() {
		getRecordIndex = (int)RecIndex / ONCE_GET_COUNT;
		initOnceGetList();
		NaviRun.GetNaviRunObj().JNI_NE_POITel_GetRecList(RecIndex,ONCE_GET_COUNT, m_Poi_Tel_Listitem, Rec);
		resetShowList(RecIndex,Rec);
	}


	private void initOnceGetList() {
		m_Poi_Tel_Listitem = new POI_Tel_ListItem[ONCE_GET_COUNT];
		for (int i = 0 ; i < ONCE_GET_COUNT ; i++) {
			m_Poi_Tel_Listitem[i] = new POI_Tel_ListItem();
		}

	}


	private void initDataObj() {

		initOnceGetList();
		m_Poi_Show_Listitem = new POI_Tel_ListItem[ONCE_GET_COUNT*2];
		for (int i = 0 ; i < ONCE_GET_COUNT*2 ; i++) {
			m_Poi_Show_Listitem[i] = new POI_Tel_ListItem();
		}

	}
	private ListAdapter listAdapter = new BaseAdapter (){

		@Override
		public int getCount() {
			int iCount = TelResultInquiry.this.getCount();
			if (iCount <5) {
				return 5;
			} else {
				return iCount;
			}

		}

		// yangyang add start
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		// yangyang add end

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int wPos, View oView, ViewGroup parent) {

//			NaviLog.d(NaviLog.PRINT_LOG_TAG,"getView wPos--------------->" + wPos);
			return TelResultInquiry.this.getView(wPos, oView);
		}

	};
	@Override
	public int getCount() {
//		JNILong ListCount = new JNILong();
//		NaviRun.GetNaviRunObj().JNI_NE_POITel_GetRecCount(ListCount);
//		int len = initPageIndexAndCnt(ListCount);
		return allRecordCount;
	}

	@Override
	public int getCountInBox() {
		return 5;
	}
//	private int getRecordIndex = 0;
	int wOldPos = 0;
	@Override
	public View getView(final int wPos, View oView) {
		LinearLayout oLayout = null;

		Button obtn = null;
		if(oView == null || !(oView instanceof LinearLayout))
		{
			oLayout = new LinearLayout(this);
			oLayout.setGravity(Gravity.CENTER_VERTICAL);
//Add 2011/09/30 Z01_h_yamada Start -->
			LinearLayout.LayoutParams oParams = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT,
					(int)getResources().getDimension(R.dimen.TR_Button_Lheight));
//Add 2011/09/30 Z01_h_yamada End <--
//			oLayout.setPadding(5, 5, 5, 5);
			obtn = new Button(this);
			obtn.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
			obtn.setId(R.id.btnOk);
//Chg 2011/09/30 Z01_h_yamada Start -->
//			obtn.setTextSize(26);
//--------------------------------------------
			obtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.Common_Medium_textSize));
//Chg 2011/09/30 Z01_h_yamada End <--
			obtn.setBackgroundResource(R.drawable.btn_default);
			//前から「...」とする
			obtn.setSingleLine(true);
			obtn.setEllipsize(TruncateAt.START);
			oLayout.addView(obtn, oParams);
			oView = oLayout;
		}
		else
		{
//			if (wPos%getCountInBox() == 0) {
//				//ページIndexを取得する
//				pageIndex = wPos/getCountInBox();
//				RecIndex = getCountInBox() * pageIndex;
//			}
			oLayout = (LinearLayout)oView;
			obtn = (Button)oLayout.findViewById(R.id.btnOk);
//			obtn = (Button)oView;
		}


		//下にスクロールの場合
		if (wOldPos <= wPos) {
			if (wPos < getCount() && ONCE_GET_COUNT != 0
					&& (wPos % ONCE_GET_COUNT == 0 ||
							(getRecordIndex != wPos / ONCE_GET_COUNT))) {
				//上にスクロール場合
				RecIndex = wPos;
				search();
			}
		}else {
			/*総件数76件、最後ページであるし、上にスクロール時、
			 * 70あるいは６９のデータを表示するとき、グレーボタンを表示しないように
			 */
			if (getRecordIndex != wPos / ONCE_GET_COUNT) {
				RecIndex = wPos;
				search();
			}
		}


		if (wPos < this.getCount()) {
			if (getShowObj(wPos).getM_Name() == null || "".equals(getShowObj(wPos).getM_Name())) {
				obtn.setText( getShowObj(wPos).getM_FullTel() + this.getResources().getString(R.string.str_around));
//				NaviLog.d(NaviLog.PRINT_LOG_TAG,"getShowObj(wPos).getM_Tel()===" + getShowObj(wPos).getM_FullTel());
			} else {
				obtn.setText(getShowObj(wPos).getM_Name() + "(" + getShowObj(wPos).getM_FullTel() + ")");
			}
			obtn.setTextColor(getResources().getColor(R.color.text));
			obtn.setEnabled(true);
		} else {
			obtn.setText("");
			obtn.setEnabled(false);
		}
		obtn.setPadding(10, 10, 10, 10);
		obtn.setOnClickListener(new OnClickListener(){

	        @Override
	        public void onClick(View v) {
//Add 2012/02/07 katsuta Start --> #2698
//				NaviLog.d(NaviLog.PRINT_LOG_TAG, "TelResultInquiry onClick ================== bIsListClicked: " + bIsListClicked);
	   			if (bIsListClicked) {
	   				return;
	   			}
	   			else {
	   				bIsListClicked = true;
	   			}
//Add 2012/02/07 katsuta End <--#2698

	        	RecIndex = wPos/ONCE_GET_COUNT*ONCE_GET_COUNT;
	        	search();
	            POI_Tel_ListItem poi = getShowObj(wPos);
	            POIData oData = new POIData();
	            if (poi.getM_Name() == null || "".equals(poi.getM_Name())) {
	            	oData.m_sName = poi.getM_FullTel() +
	            	TelResultInquiry.this.getResources().getString(R.string.str_around);
	            } else {
	            	oData.m_sName = poi.getM_Name() + "(" + poi.getM_FullTel() + ")";
	            }
//Chg 2011/11/18 Z01_h_yamada Start -->
//	            if (getCount() != 1) {
//	            	oData.m_sAddress = poi.getM_Name();
//	            }
//---------------------------------------
            	oData.m_sAddress = oData.m_sName;
//Chg 2011/11/18 Z01_h_yamada End <--
                oData.m_wLong = poi.getM_lLon();
                oData.m_wLat = poi.getM_lLat();

//                NaviLog.d(NaviLog.PRINT_LOG_TAG,"oData.m_wLong===oData.m_wLat===" + oData.m_wLong + "," + oData.m_wLat);
             // XuYang add start #1056
                if (null != oIntent && oIntent.hasExtra(Constants.ROUTE_FLAG_KEY)) {
                    CommonLib.setChangePosFromSearch(true);
                    CommonLib.setChangePos(false);
                } else {
                    CommonLib.setChangePosFromSearch(false);
                    CommonLib.setChangePos(false);
                }
                // XuYang add end #1056
                OpenMap.moveMapTo(TelResultInquiry.this, oData, getIntent(), AppInfo.POI_LOCAL, Constants.LOCAL_INQUIRY_REQUEST);

                TelResultInquiry.this.createDialog(Constants.DIALOG_WAIT, -1);
	        }
	    });

		//上にスクロールの場合
		if (wOldPos > wPos) {
			if (wPos < getCount() && ONCE_GET_COUNT != 0
					&& wPos >= ONCE_GET_COUNT && (wPos % ONCE_GET_COUNT == 0 ||
							(getRecordIndex != wPos / ONCE_GET_COUNT))) {
	//			getRecordIndex = wPos/ONCE_GET_COUNT;
				//上にスクロール場合

				RecIndex = (wOldPos-ONCE_GET_COUNT)/ONCE_GET_COUNT*ONCE_GET_COUNT;

				search();
			}
		}
		wOldPos = wPos;

		return oView;
	}

	/**
	 * Index対応したレコードを取得する
	 * @param wPos フリースクロールリストに対応レコードのIndex
	 * @return POI_Tel_ListItem 戻り値：取得したレコード対象
	 *
	 * */
	private POI_Tel_ListItem getShowObj(int wPos) {

		int showIndex = getShowIndex(wPos);
//		NaviLog.d(NaviLog.PRINT_LOG_TAG,"showIndex=========" + showIndex);
		return m_Poi_Show_Listitem[showIndex];
	}

	/**
	 * フリースクロールリストのIndex対応したレコードのIndexを取得する
	 * 例えば：wPosはONCE_GET_COUNT*2以内の場合、
	   * 　　　　　直接にwPosを戻す
	 * ONCE_GET_COUNT*2を超える場合、
	 * 対応したIndexはONCE_GET_COUNT～ONCE_GET_COUNT*2である
	 * @param wPos フリースクロールリストに対応レコードのIndex
	 * @return int 取得したレコードのIndex
	 *
	 * */
	private int getShowIndex(int wPos) {
		if (wPos >= ONCE_GET_COUNT*2) {
			int pageIndex  = wPos/ONCE_GET_COUNT;
			wPos = wPos-ONCE_GET_COUNT*pageIndex+ONCE_GET_COUNT;
		}
		return wPos;
	}

	/**
	 * 取得したデータを表示した対象に整理する
	 * @param lRecIndex スタートのIndex
	 * @param Rec 実際に取得したレコード数
	 *
	 * */
	private void resetShowList(Long lRecIndex,JNILong Rec) {
		if (lRecIndex < ONCE_GET_COUNT) {
			int len = m_Poi_Tel_Listitem.length;
			for (int i = 0 ; i < len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_Tel_Listitem[i];
			}
		} else {
			if (lRecIndex >= ONCE_GET_COUNT*2) {
				for (int i = m_Poi_Tel_Listitem.length-ONCE_GET_COUNT; i < ONCE_GET_COUNT*2 ; i++) {
//					NaviLog.d(NaviLog.PRINT_LOG_TAG,"resetShowList i===" + i);
					if (i <ONCE_GET_COUNT) {
						m_Poi_Show_Listitem[i] = m_Poi_Show_Listitem[i+ONCE_GET_COUNT];
					} else {
						m_Poi_Show_Listitem[i] = new POI_Tel_ListItem();
					}
				}
			}

			int len = (int)Rec.lcount;
			int j = 0;
			for (int i = ONCE_GET_COUNT ; i < ONCE_GET_COUNT + len ; i++) {
				m_Poi_Show_Listitem[i] = m_Poi_Tel_Listitem[j];
//				System.out.println(m_Poi_Show_Listitem[i].getM_Name());
				j++;
			}
		}
	}

	@Override
	protected boolean onClickGoBack() {
		return super.onClickGoBack();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			Intent oIntent = getIntent();
//			String tel = oIntent.getStringExtra(Constants.PARAMS_SEARCH_KEY);
//			NaviRun.GetNaviRunObj().JNI_NE_POITel_Cancel();
			this.setResult(Activity.RESULT_CANCELED, oIntent);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected boolean onClickGoMap() {
		NaviRun.GetNaviRunObj().JNI_NE_POITel_Clear();
		return super.onClickGoMap();
	}

	@Override
	protected boolean onStartShowPage() throws Exception {
		initDataObj();
		initCount();
		RecIndex=0;
		search();
		return true;
	}

	@Override
	protected void onFinishShowPage(boolean bGetData) throws Exception {
		if(bGetData)
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage end");
					initDialog();
					NaviLog.d(NaviLog.PRINT_LOG_TAG,"onFinishShowPage end");
				}
			});
		}
	}
}
