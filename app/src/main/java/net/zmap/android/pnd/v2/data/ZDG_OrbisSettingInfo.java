/**
 ********************************************************************
 * Copyright (c) 2009. ZDC Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project        MarketV2
 * File           ZDG_ORBIS_INFO.java
 * Description    int Class
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class ZDG_OrbisSettingInfo {
    private short unGeneralOrbisGuideDist;  ///<一般道ORBIS発声距離
    private short unHighwayOrbisGuideDist;  ///<高速道ORBIS発声距離
    private short unGeneralOrbisGuideAngle; ///<一般道ORBIS発声Angle
    private short unHighwayOrbisGuideAngle; ///<高速道ORBIS発声Angle
    public void setUnGeneralOrbisGuideDist(short unGeneralOrbisGuideDist) {
        this.unGeneralOrbisGuideDist = unGeneralOrbisGuideDist;
    }
    public short getUnGeneralOrbisGuideDist() {
        return unGeneralOrbisGuideDist;
    }
    public void setUnHighwayOrbisGuideDist(short unHighwayOrbisGuideDist) {
        this.unHighwayOrbisGuideDist = unHighwayOrbisGuideDist;
    }
    public short getUnHighwayOrbisGuideDist() {
        return unHighwayOrbisGuideDist;
    }
    public void setUnGeneralOrbisGuideAngle(short unGeneralOrbisGuideAngle) {
        this.unGeneralOrbisGuideAngle = unGeneralOrbisGuideAngle;
    }
    public short getUnGeneralOrbisGuideAngle() {
        return unGeneralOrbisGuideAngle;
    }
    public void setUnHighwayOrbisGuideAngle(short unHighwayOrbisGuideAngle) {
        this.unHighwayOrbisGuideAngle = unHighwayOrbisGuideAngle;
    }
    public short getUnHighwayOrbisGuideAngle() {
        return unHighwayOrbisGuideAngle;
    }

}
