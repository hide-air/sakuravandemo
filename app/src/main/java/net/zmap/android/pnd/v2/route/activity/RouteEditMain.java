/**
 ********************************************************************
 * Copyright (c) 2010.  Corporation.  All Rights Reserved.
 ********************************************************************
 *
 * Project        MarketV2
 * File           SettingMain.java
 * Description    設定/情報メイン画面
 * Created on     2010/11/24
 *
 ********************************************************************
 */

package net.zmap.android.pnd.v2.route.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import net.zmap.android.pnd.v2.NaviApplication;
import net.zmap.android.pnd.v2.R;
import net.zmap.android.pnd.v2.common.AppInfo;
import net.zmap.android.pnd.v2.common.CommonLib;
import net.zmap.android.pnd.v2.common.Constants;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
import net.zmap.android.pnd.v2.common.DrivingRegulation;
// MOD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
import net.zmap.android.pnd.v2.common.NaviActivityStarter;
import net.zmap.android.pnd.v2.common.RouteDataFile;
import net.zmap.android.pnd.v2.common.activity.MenuBaseActivity;
import net.zmap.android.pnd.v2.common.view.CustomDialog;
import net.zmap.android.pnd.v2.route.data.RouteData;

import java.util.List;

/**
 * Created on 2010/11/29
 * <p>
 * Title: ルート編集メイン画面
 * <p>
 * Description
 * <p>
 * author　XuYang version 1.0
 */
public class RouteEditMain extends MenuBaseActivity {
    private Button butNowRoute = null;
    private Button butNewRoute = null;
    private Button butSaveRoute = null;
    private Button butGuideEnd = null;
    private int DIALOG_GUIDE_END = 1;

    /**
     * 初期化処理
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater oInflater = LayoutInflater.from(this);

//Del 2011/09/17 Z01_h_yamada Start -->
//		// 画面を追加
//		setMenuTitle(R.drawable.title_route_list);
//Del 2011/09/17 Z01_h_yamada End <--
        LinearLayout oLayout = (LinearLayout)oInflater.inflate(R.layout.route_edit_main, null);

        // 現在のルートを編集
        butNowRoute = (Button)oLayout.findViewById(R.id.butNowRoute);
        butNowRoute.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                goNowRoute();
            }
        });

        // 新規ルート設定
        butNewRoute = (Button)oLayout.findViewById(R.id.butNewRoute);
        butNewRoute.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                goNewRoute();
            }
        });
        // 保存ルート一覧
        butSaveRoute = (Button)oLayout.findViewById(R.id.butSaveRoute);
        butSaveRoute.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                goSaveRoute();
            }
        });

        // 案内終了
        butGuideEnd = (Button)oLayout.findViewById(R.id.butGuideEnd);
        butGuideEnd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                goGuideEnd();
            }
        });
        initStatus();
        setViewInWorkArea(oLayout);
    }

    /**
     * 初期化の状態を設定
     */
    private void initStatus() {
        // 現在のルートが存在するかどうかを判断する
        if (CommonLib.hasRoute()) {
            butNowRoute.setEnabled(true);
        } else {
            butNowRoute.setEnabled(false);
        }
        // 保存のルートが存在するかどうかを判断する
//Chg 2011/09/26 Z01yoneya Start -->
//        boolean isExistRoute = CommonLib.XmlRead();
//------------------------------------------------------------------------------------------------------------------------------------
        boolean isExistRoute;
        try {
            RouteDataFile routeDataFile = new RouteDataFile();
            List<RouteData> routeDataList = routeDataFile.readData(((NaviApplication)getApplication()).getNaviAppDataPath().getRouteDataXmlFileFullPath());
            isExistRoute = !routeDataList.isEmpty();
        } catch (NullPointerException e) {
            isExistRoute = false;
        }
//Chg 2011/09/26 Z01yoneya End <--
        if (isExistRoute) {
            butSaveRoute.setEnabled(true);
        } else {
            butSaveRoute.setEnabled(false);
        }
        // 案内するかどうかを判断する
        if (CommonLib.isNaviStart()) {
            butGuideEnd.setEnabled(true);
        } else {
            butGuideEnd.setEnabled(false);
        }
    }

    /**
     * 現在ルート画面に遷移する処理
     */
    private void goNowRoute() {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ルート編集 現在のルートを編集禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 ルート編集 現在のルートを編集禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        Intent intent = new Intent();
        intent.setClass(this, RouteEdit.class);
        // #875 XuYang add start
//		intent.putExtra(Constants.AddressString, (String) getIntent().getExtras().get(Constants.AddressString));
        // #875 XuYang add end
        intent.putExtra(Constants.ROUTE_TYPE_KEY, Constants.ROUTE_TYPE_NOW);
//Chg 2011/09/23 Z01yoneya Start -->
//        this.startActivityForResult(intent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
//Chg 2011/09/23 Z01yoneya End <--
    }

    /**
     * 新規ルート画面に遷移する場合
     */
    private void goNewRoute() {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ルート編集 新規ルート設定禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 ルート編集 新規ルート設定禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        Intent intent = new Intent();
        intent.setClass(this, RouteEdit.class);
        intent.putExtra(Constants.ROUTE_TYPE_KEY, Constants.ROUTE_TYPE_NEW);
//Chg 2011/09/23 Z01yoneya Start -->
//        this.startActivityForResult(intent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_ROUTE_EDIT);
//Chg 2011/09/23 Z01yoneya End <--
    }

    /**
     * 保存ルート画面に遷移する処理
     */
    private void goSaveRoute() {
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成 START
// ADD 2013.08.08 M.Honma 走行規制 ルート編集 保存ルート一覧禁止 Start -->
    	if(DrivingRegulation.CheckDrivingRegulation()){
    		return;
    	}
// ADD 2013.08.08 M.Honma 走行規制 ルート編集 保存ルート一覧禁止 End <--
// MOD 2014.01.09 N.Sasao マルチ車載器連携地盤作成  END
        Intent intent = new Intent();
        intent.setClass(this, SaveRouteList.class);
//Chg 2011/09/23 Z01yoneya Start -->
//        this.startActivityForResult(intent, AppInfo.ID_ACTIVITY_ROUTE_SAVE);
//------------------------------------------------------------------------------------------------------------------------------------
        NaviActivityStarter.startActivityForResult(this, intent, AppInfo.ID_ACTIVITY_ROUTE_SAVE);
//Chg 2011/09/23 Z01yoneya End <--
    }

    /**
     * 案内終了処理
     */
    private void goGuideEnd() {
        this.showDialog(DIALOG_GUIDE_END);
    }

    /**
     * ダイアログ処理
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        final CustomDialog oDialog = new CustomDialog(this);
        if (id == DIALOG_GUIDE_END) {
            Resources oRes = getResources();
            oDialog.setTitle(oRes.getString(R.string.txt_navi_exit_title));
            oDialog.setMessage(oRes.getString(R.string.txt_navi_exit));
            oDialog.addButton(oRes.getString(R.string.btn_ok), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // 案内終了の処理
//Chg 2011/09/26 Z01yoneya Start -->
//                    CommonLib.guideEnd(true);
//------------------------------------------------------------------------------------------------------------------------------------
                    CommonLib.guideEnd(true, ((NaviApplication)getApplication()).getNaviAppDataPath());
//Chg 2011/09/26 Z01yoneya End <--
                    oDialog.dismiss();
                    removeDialog(DIALOG_GUIDE_END);
                    initStatus();
                }
            });

            oDialog.addButton(oRes.getString(R.string.btn_cancel), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    oDialog.dismiss();
                    removeDialog(DIALOG_GUIDE_END);
                }
            });
        }
        // XuYang add start 走行中の操作制限
        else {
            return super.onCreateDialog(id);
        }
        // XuYang add end 走行中の操作制限
        return oDialog;
    }

    /**
     * 戻るの処理
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.RESULTCODE_ROUTE_DETERMINATION) {
            this.setResult(Constants.RESULTCODE_ROUTE_DETERMINATION);
            this.finish();
        }
//Add 2011/09/21 Z01yoneya Start -->
        super.onActivityResult(requestCode, resultCode, data);
//Add 2011/09/21 Z01yoneya End <--
    }

    /**
     * 更新初期化の状態
     */
    @Override
    protected void onResume() {
        initStatus();
        super.onResume();
    }
}
