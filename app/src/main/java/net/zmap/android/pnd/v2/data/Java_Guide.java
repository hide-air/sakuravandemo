/**
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           Java_Guide.java
 * Description    案内情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class Java_Guide {
    int iGuideType;
    JNILong[] lnArrDistance = new JNILong[9];
    int iStatus;
    /**
    * Created on 2010/08/06
    * Title:       getIGuideType
    * Description:  案内タイプを取得する
    * @param1  無し
    * @return       int

    * @version        1.0
    */
    public int getIGuideType() {
        return iGuideType;
    }
    /**
    * Created on 2010/08/06
    * Title:       getLnDistance
    * Description:  目標座標を取得する
    * @param1  無し
    * @return       JNILong[]

    * @version        1.0
    */
    public JNILong[]  getLnDistance() {
        return lnArrDistance;
    }
    /**
    * Created on 2010/08/06
    * Title:       getLnDistance
    * Description:  案内状態を取得する
    * @param1  無し
    * @return       JNILong[]

    * @version        1.0
    */
    public int getIStatus() {
        return iStatus;
    }
}
