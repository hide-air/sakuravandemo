package net.zmap.android.pnd.v2.common;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppInfo {

	public final static int ID_ACTIVITY = 0x01;

	/**Splash*/
	public final static int ID_ACTIVITY_LOGO = ID_ACTIVITY + 0x01;

	/**Map*/
	public final static int ID_ACTIVITY_MAP = ID_ACTIVITY + 0x2;
	public final static int ID_ACTIVITY_OPEN_MAP = ID_ACTIVITY + 0x3;
	public final static int ID_ACTIVITY_ROUTE_MAP = ID_ACTIVITY + 0x4;
	public final static int ID_ACTIVITY_NAVI_MAP = ID_ACTIVITY + 0x5;

	/**menu*/
	public final static int ID_ACTIVITY_MENU_NORMAL = ID_ACTIVITY + 0x7;
	public final static int ID_ACTIVITY_MENU_NAVI = ID_ACTIVITY + 0x8;
	public final static int ID_ACTIVITY_NAVI_MODE_CHANGE = ID_ACTIVITY + 0x9;

	/**Inquiry*/
	public final static int ID_ACTIVITY_INQUIRY = ID_ACTIVITY + 0x10;
	public final static int ID_ACTIVITY_TELINQUIRY = ID_ACTIVITY + 0x11;
	public final static int ID_ACTIVITY_MAINMENU = ID_ACTIVITY + 0x13;
	public final static int ID_ACTIVITY_KEYINQUIRY = ID_ACTIVITY + 0x14;
	public final static int ID_ACITIVITY_KEYPROVINCEINQUIRY = ID_ACTIVITY + 0x15;
	public final static int ID_ACITIVITY_KEYCITYINQUIRY =  ID_ACTIVITY + 0x16;
	public final static int ID_ACITIVITY_KEYRESULTINQUIRY =  ID_ACTIVITY + 0x17;
	public final static int ID_ACITIVITY_HISTORYINQUIRY =  ID_ACTIVITY + 0x18;
	public final static int ID_ACTIVITY_TELRESULTINQUIRY =  ID_ACTIVITY + 0x19;
	public final static int ID_ACTIVITY_FAVORITESGENREINQUIRY = ID_ACTIVITY + 0x20;
	public final static int ID_ACTIVITY_FAVORITESINQUIRY = ID_ACTIVITY + 0x21;
	public final static int ID_ACTIVITY_FAVORITESGENREMENUINQUIRY = ID_ACTIVITY + 0x22;
	public final static int ID_ACTIVITY_FAVORITESMENUINQUIRY = ID_ACTIVITY + 0x23;
	public final static int ID_ACTIVITY_FAVORITESMODINQUIRY = ID_ACTIVITY + 0x24;
	public final static int ID_ACTIVITY_FAVORITESADDGENREINQUIRY =  ID_ACTIVITY + 0x25;
	public final static int ID_ACTIVITY_ADDRESSPROVINCEINQUIRY =   ID_ACTIVITY + 0x26;
	public final static int ID_ACTIVITY_ADDRESSCITYINQUIRY =   ID_ACTIVITY + 0x27;
	public final static int ID_ACTIVITY_POSTALCODEINQUIRY = ID_ACTIVITY + 0x28;
	public final static int ID_ACTIVITY_POSTALCODERESULTINQUIRY = ID_ACTIVITY + 0x29;
	public final static int ID_ACTIVITY_ADDRESSRESULTINQUIRY = ID_ACTIVITY + 0x30;
	public final static int ID_ACTIVITY_GENREINQUIRY = ID_ACTIVITY + 0x31;

	// XuYang add start 周辺検索
	public final static int ID_ACTIVITY_LOCAL_MAINMENU = ID_ACTIVITY + 0x32;
	public final static int ID_ACTIVITY_LOCAL_OTHER = ID_ACTIVITY + 0x33;
	public final static int ID_ACTIVITY_AROUND_FAVORITES_GENRE = ID_ACTIVITY + 0x34;
	public final static int ID_ACTIVITY_AROUND_FAVORITES_TEMP = ID_ACTIVITY + 0x35;
	public final static int ID_ACTIVITY_AROUND_OTHER_TEMP = ID_ACTIVITY + 0x36;
	// XuYang add end 周辺検索

    public final static int ID_ACTIVITY_VEHICLE_INQUIRY = ID_ACTIVITY + 0x37;
	// 2013/10/17 M.Suna Start -->
    public final static int ID_ACTIVITY_NOSAI_INQUIRY = ID_ACTIVITY + 0x38;
    public final static int ID_ACTIVITY_NOSAI_TAP_ORDER_LIST = ID_ACTIVITY + 0x39;
    public final static int ID_ACTIVITY_NOSAI_MAP_VIEWER = ID_ACTIVITY + 0x40;
    // 2013/10/17 M.Suna End <--

	/**Setting*/
	public final static int ID_ACTIVITY_SETTING = ID_ACTIVITY + 0x50;
	public final static int ID_ACTIVITY_SETTING_NAVI = ID_ACTIVITY + 0x51;
//Add 2011/09/13 Z01thedoanh Start -->
	public final static int ID_ACTIVITY_DRICON_SETTING_MAIN = ID_ACTIVITY + 0x49;
	public final static int ID_ACTIVITY_DRICON_SETTING_DETAIL = ID_ACTIVITY + 0x48;
	public final static int ID_ACTIVITY_DRICON_SETTING_CONDITION = ID_ACTIVITY + 0x47;
	public final static int ID_ACTIVITY_DRICON_SETTING_GROUP	 = ID_ACTIVITY + 0x46;
//Add 2011/09/13 Z01thedoanh End <--
//Add 2011/09/20 Z01yamaguchi Start -->
	public final static int ID_ACTIVITY_DRICON_DETAIL_INFO = ID_ACTIVITY + 0x45;
//Add 2011/09/20 Z01yamaguchi End <--

// 2013/10/17 M.Suna Start -->
	// ここに、いつもNAVI独自で定義する、UIの定数を追加した。
    public final static int ID_ACTIVITY_DRICON_IMPORTCSV = ID_ACTIVITY + 0x100;
    public final static int ID_ACTIVITY_DRICON_EXPORTCSV = ID_ACTIVITY + 0x101;
    public final static int ID_ACTIVITY_DRICON_SHOWMATCHRESULT = ID_ACTIVITY + 0x102;
// Add by CPJsunagawa '2015-07-08 軌跡保存　Start
    public final static int ID_ACTIVITY_TRACK_LOG_START = ID_ACTIVITY + 0x103;
    public final static int ID_ACTIVITY_TRACK_LOG_END = ID_ACTIVITY + 0x104;
    public final static int ID_ACTIVITY_TRACK_LOG_CLEAR = ID_ACTIVITY + 0x105;
    public final static int ID_ACTIVITY_TRACK_ICON_EDIT = ID_ACTIVITY + 0x106;
    public final static int ID_ACTIVITY_REGIST_ICON = ID_ACTIVITY + 0x107;
    public final static int ID_ACTIVITY_INPUT_TEXT = ID_ACTIVITY + 0x108;
// Add by CPJsunagawa '2015-08-15 Start
	public final static int ID_ACTIVITY_MOVE_ICON = ID_ACTIVITY + 0x109;
    public final static int ID_ACTIVITY_DELETE_ICON = ID_ACTIVITY + 0x110;
    public final static int ID_ACTIVITY_DRAW_LINE = ID_ACTIVITY + 0x111;
// Add by CPJsunagawa '2015-08-15 End
// Add by CPJsunagawa '2015-07-08 End
    
    // マッチングモード 常にこれらの値を使う。
    public final static int ID_ACTIVITY_DRICON_MATCHFROMMAP = ID_ACTIVITY + 0x200;
    public final static int ID_ACTIVITY_DRICON_MATCHFROMADR = ID_ACTIVITY + 0x201;
    public final static int ID_ACTIVITY_DRICON_MATCHCONF = ID_ACTIVITY + 0x202;
// 2013/10/17 M.Suna End

	/**GPS*/
	public final static int ID_ACTIVITY_GPS = ID_ACTIVITY + 0x53;
	/**Setting Map*/
	public final static int ID_ACTIVITY_SETTING_MAP = ID_ACTIVITY + 0x54;

	/**Route*/
	public final static int ID_ACTIVITY_ROUTE = ID_ACTIVITY + 0x55;
	public final static int ID_ACTIVITY_ROUTE_EDIT = ID_ACTIVITY + 0x56;
	public final static int ID_ACTIVITY_ROUTE_SAVE = ID_ACTIVITY + 0x57;
	public final static int ID_ACTIVITY_ROUTE_ADD = ID_ACTIVITY + 0x58;
	public final static int ID_ACTIVITY_ROUTE_NAME_EDIT = ID_ACTIVITY + 0x59;
	public final static int ID_ACTIVITY_ROUTE_MODIFY = ID_ACTIVITY + 0x60;
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
	/**DrivingRegulation*/
	public final static int ID_ACTIVITY_DRIVING_REGULATION = ID_ACTIVITY + 0x61;
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END
	/**いまここメール*/
	public final static int ID_ACTIVITY_MAIL = ID_ACTIVITY + 0x81;

	public final static int PICK_CONTACT = ID_ACTIVITY + 0x21B;
	public final static int ID_ACTIVITY_HISTORYINQUIRY_AROUND= ID_ACTIVITY + 0x21C;
// カメラ連携 & 手書きメモ対応 Start
    public final static int ID_ACTIVITY_IMAGE_CAPTURE = ID_ACTIVITY + 0x300;
    public final static int ID_ACTIVITY_TEGAKI_MEMO = ID_ACTIVITY + 0x301;
    public final static int ID_ACTIVITY_PHOTO_TYPE_LIST_DIALOG = ID_ACTIVITY + 0x302;

    public final static String FLAG_POI_TYPE = "poi_type";
    public final static int POI_NONE = 0x03;
    public final static int POI_LOCAL = 0x00;
    public final static int POI_SPOT = 0x02;

    public final static String POI_DATA = "poi_data";

//Del 2011/09/09 Z01_h_yamada Start -->
//	public static int[] imgOne = new int[]{R.drawable.btn_tel_1_select,R.drawable.btn_tel_1_press,R.drawable.btn_tel_1_normal,R.drawable.btn_tel_1_normal};
//	public static int[] imgTwo = new int[]{R.drawable.btn_tel_2_select,R.drawable.btn_tel_2_press,R.drawable.btn_tel_2_normal,R.drawable.btn_tel_2_normal};
//	public static int[] imgThree = new int[]{R.drawable.btn_tel_3_select,R.drawable.btn_tel_3_press,R.drawable.btn_tel_3_normal,R.drawable.btn_tel_3_normal};
//	public static int[] imgFour = new int[]{R.drawable.btn_tel_4_select,R.drawable.btn_tel_4_press,R.drawable.btn_tel_4_normal,R.drawable.btn_tel_4_normal};
//	public static int[] imgFive = new int[]{R.drawable.btn_tel_5_select,R.drawable.btn_tel_5_press,R.drawable.btn_tel_5_normal,R.drawable.btn_tel_5_normal};
//	public static int[] imgSix = new int[]{R.drawable.btn_tel_6_select,R.drawable.btn_tel_6_press,R.drawable.btn_tel_6_normal,R.drawable.btn_tel_6_normal};
//	public static int[] imgSeven = new int[]{R.drawable.btn_tel_7_select,R.drawable.btn_tel_7_press,R.drawable.btn_tel_7_normal,R.drawable.btn_tel_7_normal};
//	public static int[] imgEight = new int[]{R.drawable.btn_tel_8_select,R.drawable.btn_tel_8_press,R.drawable.btn_tel_8_normal,R.drawable.btn_tel_8_normal};
//	public static int[] imgNine = new int[]{R.drawable.btn_tel_9_select,R.drawable.btn_tel_9_press,R.drawable.btn_tel_9_normal,R.drawable.btn_tel_9_normal};
//	public static int[] imgZero = new int[]{R.drawable.btn_tel_0_select,R.drawable.btn_tel_0_press,R.drawable.btn_tel_0_normal,R.drawable.btn_tel_0_normal};
//Del 2011/09/09 Z01_h_yamada End <--



//Del 2011/09/08 Z01_h_yamada Start <-- ソフトウェアキーボードが一瞬表示される不具合の修正
	public static void hiddenSoftInputMethod(Activity oActivity){
		InputMethodManager ime = (InputMethodManager) oActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
		if(ime.isActive()){
			if(oActivity.getWindow().getCurrentFocus()!=null)
			  {
				  ime.hideSoftInputFromWindow(oActivity.getWindow().getCurrentFocus().getWindowToken(), 0);
			  }
//			  else
//			  {
//				  ime.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);
//			  }
		}
	}
//Del 2011/09/08 Z01_h_yamada End <--


	public static boolean isEmpty(String sStr)
	{
		if(sStr == null || sStr.length() == 0)return true;
		else return false;
	}

	public static boolean isMatches(String string ) {

		Pattern p = Pattern.compile("[0-9]+");
		Matcher m = p.matcher(string);

		return m.matches();
	}
	public static boolean isEnglish(String string) {
		Pattern p = Pattern.compile("[a-zA-Z]+");
		Matcher m = p.matcher(string);

		return m.matches();
	}
	public static boolean isInitFinish = false;
	public static String[] KANA_LIST = null;


	public static String getKanaList(String str) {

		char[] charlist = str.toCharArray();
        int length = charlist.length;
        int kanalistLength = KANA_LIST.length;
        StringBuilder boolResult = new StringBuilder(length);
        char checkValue = '\0';

		for (int i = 0;i < length; i++) {
			checkValue = charlist[i];
			for (int j = 0 ; j < kanalistLength; j++) {
				if (KANA_LIST[j].equals(String.valueOf(checkValue))) {
					boolResult.append(String.valueOf(checkValue));
					break;
				}
			}

		}

		return boolResult.toString();
	}
	public static boolean isKana(String str) {

		boolean boolResult = false;
		ArrayList<Boolean> isReal = new ArrayList<Boolean>();
		char[] charlist = str.toCharArray();
		for (int i = 0;i < charlist.length; i++) {
			char checkValue = charlist[i];
			if (!isEnglish(String.valueOf(checkValue))) {
				for (int j = 0 ; j < KANA_LIST.length; j++) {
					if (KANA_LIST[j].equals(String.valueOf(checkValue))) {
						isReal.add(true);
						break;
					}
				}
			} else {
				isReal.add(true);
			}

		}
		if (isReal.size() == charlist.length){
			boolResult = true;
		} else {
			boolResult = false;
		}
		return boolResult;
	}

	public static long m_lLat = 0;
	public static long m_lLon = 0;
}
