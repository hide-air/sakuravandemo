package net.zmap.android.pnd.v2.sakuracust;

import net.zmap.android.pnd.v2.common.data.PoiBaseData;
import net.zmap.android.pnd.v2.inquiry.data.POIData;

/**
 * さくらのマニュアルマッチング用POIData
 * @author Norimasa
 *
 */
public class SakuraCustPOIData extends POIData {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	
	/** 自分自身のインデックス */
	public int m_MyIndex;

	/**
	 * 顧客ID
	 */
	public int m_CustID;
	
	/**
	 * 指定したPoiBaseDataは自分か？
	 * @param pbd PoiBaseData
	 * @return これがtrueとなった際、m_wLatとm_wLongに値を差し替え、DBを更新する。
	 */
	public static boolean isMe(PoiBaseData pbd)
	{
		return pbd instanceof SakuraCustPOIData;
	}
	
	/**
	 * 指定したインスタンスを自分のインスタンスにキャストする。但し、インスタンスが自分でなければnullを返却する
	 * @param pbd
	 * @return
	 */
	public static SakuraCustPOIData castMe(PoiBaseData pbd)
	{
		return isMe(pbd) ? (SakuraCustPOIData) pbd : null;
	}
}
