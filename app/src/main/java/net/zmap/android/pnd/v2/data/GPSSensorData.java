/*
 ********************************************************************
 * COPYRIGHT ZENRIN DataCom CO., LTD
 ********************************************************************
 *
 * Project        MarketV2
 * File           GPSSensorData.java
 * Description    GPS衛星情報
 * Created on     2010/08/06
 *
 ********************************************************************
 */
package net.zmap.android.pnd.v2.data;

public class GPSSensorData {
//Add 2010/11/08 Z01ONOZAWA Start -->
		public long		timestamp;		///< タイムスタンプ
		public long getTimestamp() {
			return timestamp;
		}
//Add 2010/11/08 Z01ONOZAWA End   <--
		public long longitude1000;				// 東京測地系
		public long latitude1000;
		public short nDirDegCW;					//時計方向(ClockWise)をCWとつける。反時計回り(CounterClockWise)はCCW
		public double	d_wlon;					///< V2.5 :: GPS世界測地系
		public double	d_wlat;
		public float	fAltitude;				///< V2.5 :: 標高[m]
		public float	fAccuracy;				///< V2.5 :: 総合的な精度の指針(Hdop,Vdopではない) [horizontalAccuracy]
		public float	fsnrAverageUsedInFix; 	///< V2.5 :: 測位使用GPSのSN比率の平均値
		public float fSpeedMPS;					//meter/sec
		public int		iFixCount;				///< V2.5 :: 測位使用GPSの数

		/**
		 *  経度を取得する
		 * @return
		 */
		public long getlongitude1000() {
			return longitude1000;
		}

		/**
		 * 緯度を取得する
		 * @return
		 */
		public long getlatitude1000() {
			return latitude1000;
		}

		/**
		 * 方向を取得する
		 * @return
		 */
		public short getNDireDegCW() {
			return nDirDegCW;
		}

		public double getd_wlon() {
			return d_wlon;
		}
		public double getd_wlat() {
			return d_wlat;
		}

        /**
         * 標高[m]を取得する
         * @return
         */
        public float getfAltitude() {
            return fAltitude;
        }

        /**
         * 総合的な精度の指針(Hdop,Vdopではない)を取得する
         * @return
         */
        public float getfAccuracy() {
            return fAccuracy;
        }

        /**
         * 測位使用GPSのSN比率の平均値を取得する
         * @return
         */
        public float getfsnrAverageUsedInFix() {
            return fsnrAverageUsedInFix;
        }

		/**
		 * スピードを取得する
		 * @return スピード
		 */
		public float getSpeedMPS() {
			return fSpeedMPS;
		}

		/**
		 * 測位使用GPSの数を取得する
		 * @return
		 */
		public int getiFixCount() {
			return iFixCount;
		}
}
