package net.zmap.android.pnd.v2.common.services;

import android.content.Intent;

public interface BatteryListener
{
	public void onBatteryChange(Intent oIntent);
}
