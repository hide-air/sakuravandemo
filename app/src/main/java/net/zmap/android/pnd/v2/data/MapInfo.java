/*------------------------------------------------------------------------------
 * Copyright(C) 2011 ZenrinDataCom Co.,LTD. All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of ZenrinDataCom Co.,LTD.;
 * the contents of this file is not to be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior written
 * permission of ZenrinDataCom Co.,LTD.
 *------------------------------------------------------------------------------*/

package net.zmap.android.pnd.v2.data;

/**
 * エンジン側で保持している表示中の地図の情報です。
 * 基本的にCTTypes.hのZCT_CtrInfoに合わせています。
 *
 * @id $Id: MapInfo.java 7247 2012-08-29 05:14:54Z shiraki-m $
 */
public class MapInfo {
	/** 地図のレベル */
	private byte mAppLevel;
	/** 地図レベルでの標準の大きさからの拡大率 */
	private double mScale;
	/** 地図中心の経緯度 */
	private Coordinate mMapPoint;
	/** 自己位置の経緯度 */
	private Coordinate mMyPosition;
	/** 地図の回転角度(北向きを0度に時計回りで度単位で指定) */
	private float mMapAngle;
	/** 自己位置の角度 */
	private float mMyPositionAngle;
	/** ??? 調べていません。ZCT_CtrInfo.bVehicleStatusと同じ */
	private byte mVehicleStatus;

	/** AppLevelの最小値 */
	public static final byte MIN_APP_LEVEL = 0;

	/** AppLevelの最大値 */
	public static final byte MAX_APP_LEVEL = 13;


	/**
	 * 地図のレベルを返します。
	 *
	 * @return mAppLevel
	 */
	public byte getAppLevel() {
		return mAppLevel;
	}

	/**
	 * 地図のレベルを設定します。
	 *
	 * @param appLevel セットする mAppLevel
	 */
	public void setAppLevel(byte appLevel) {
		mAppLevel = appLevel;
	}

	/**
	 * 地図レベルでの標準の大きさからの拡大率を返します。
	 *
	 * @return mScale
	 */
	public double getScale() {
		return mScale;
	}

	/**
	 * 地図レベルでの標準の大きさからの拡大率を設定します。
	 *
	 * @param scale セットする mScale
	 */
	public void setScale(double scale) {
		mScale = scale;
	}

	/**
	 * 地図中心の経緯度を返します。
	 *
	 * @return mMapPoint
	 */
	public Coordinate getMapPoint() {
		return mMapPoint;
	}

	/**
	 * 地図中心の経緯度を設定します。
	 *
	 * @param mapPoint セットする mMapPoint
	 */
	public void setMapPoint(Coordinate mapPoint) {
		mMapPoint = mapPoint;
	}

	/**
	 * 自己位置の経緯度を返します。
	 *
	 * @return mMyPosition
	 */
	public Coordinate getMyPosition() {
		return mMyPosition;
	}

	/**
	 * 自己位置の経緯度を設定します。
	 *
	 * @param myPosition セットする mMyPosition
	 */
	public void setMyPosition(Coordinate myPosition) {
		mMyPosition = myPosition;
	}

	/**
	 * 地図の回転角度(北向きを0度に時計回りで度単位で指定)を返します。
	 *
	 * @return mMapAngle
	 */
	public float getMapAngle() {
		return mMapAngle;
	}

	/**
	 * 地図の回転角度(北向きを0度に時計回りで度単位で指定)を設定します。
	 *
	 * @param mapAngle セットする mMapAngle
	 */
	public void setMapAngle(short mapAngle) {
		mMapAngle = mapAngle;
	}

	/**
	 * 自己位置の角度を返します。
	 *
	 * @return mMyPositionAngle
	 */
	public float getMyPositionAngle() {
		return mMyPositionAngle;
	}

	/**
	 * 自己位置の角度を設定します。
	 *
	 * @param myPositionAngle セットする mMyPositionAngle
	 */
	public void setMyPositionAngle(short myPositionAngle) {
		mMyPositionAngle = myPositionAngle;
	}

	/**
	 * @return mVehicleStatus
	 */
	public byte getVehicleStatus() {
		return mVehicleStatus;
	}

	/**
	 * @param vehicleStatus セットする mVehicleStatus
	 */
	public void setmVehicleStatus(byte vehicleStatus) {
		mVehicleStatus = vehicleStatus;
	}

	/**
	 * 同じ地図画面かどうかを判定します。自己位置に関する情報は判定材料に含まれません。
	 *
	 * @param other
	 * @return
	 */
	public boolean isSameMap(MapInfo other) {
		return mMapPoint.equals(other.getMapPoint())
			&& mAppLevel == other.mAppLevel
			&& mScale == other.mScale
			&& mMapAngle == other.mMapAngle;
	}

	@Override
	public String toString() {
		return "Lon: " + mMapPoint.getLongitude()
			+ ". Lat: " + mMapPoint.getLatitude()
			+ ". AppLevel: " + mAppLevel
			+ ". Scale: " + mScale
			+ ". Angle: " + mMapAngle;
	}
}
