package net.zmap.android.pnd.v2.sensor;


public class CircularBuffer {

	public class Data {
		public long timestamp;
		public float value;

		public Data(long timestamp, float value) {
			this.timestamp = timestamp;
			this.value = value;
		}
	};

	// 200ms 間隔なら 200 秒ぶん、
	//  60ms 間隔なら  60 秒ぶん貯められる
	private final static int DEFAULT_CAPACITY = 1000;

	private int _capacity = DEFAULT_CAPACITY;
	private Data _list[];
	private int _index = 0;

	public CircularBuffer() {
		_list = new Data[_capacity];
//Chg 2011/09/06 Z01yoneya Start -->
//		for (int i = 0; i < _capacity; i++) {
//			_list[i] = new Data(-1, 0);
//		}
//------------------------------------------------------------------------------------------------------------------------------------
		clear();
//Chg 2011/09/06 Z01yoneya End <--
	}

	protected int next(int index) {
		return (index + 1) % _capacity;
	}

	protected int prev(int index) {
		return (--index < 0) ? (_capacity - 1) : index;
	}

	public void put(long timestamp, float value) {
		synchronized (this) {
			Data data = new Data(timestamp, value);
			_list[_index] = data;
			_index = next(_index);
		}
	}

	public Data get(int index) {
		synchronized (this) {
			return _list[index];
		}
	}

//Add 2011/09/06 Z01yoneya Start -->
	public void clear(){
        synchronized (this) {
            _index = 0;
            for (int i = 0; i < _capacity; i++) {
                _list[i] = new Data(-1, 0);
            }
        }
    }
//Add 2011/09/06 Z01yoneya End <--
//Add 2011/03/18 Z01yoneya Start-->
	//指定時間内に入るデータ数を返す
	public long getDataNum(long seconds){
		synchronized (this) {
			long dataNum=0;
			int ptr = prev(_index);
			Data head = get(ptr);
			long startTime = head.timestamp;
			long prevTime = startTime;
			long elapsed = 0;

			while (true) {
				Data data = get(ptr);
				// 値未設定 or キュー一周
				if (data.timestamp == -1 ||
					data.timestamp > startTime) {
					break;
				}
				// 追加するには昔すぎるデータ]
				elapsed += (prevTime - data.timestamp);
				if (elapsed >= seconds * 1000) {
					break;
				}
				dataNum = dataNum + 1;
				prevTime = data.timestamp;
				ptr = prev(ptr);
			}
			//Log.i("", String.format("getDataNum %d", dataNum));
			return dataNum;
		}
	}
//Add 2011/03/18 Z01yoneya End <--

	public float minInSeconds(long seconds) {
		synchronized (this) {
			int ptr = prev(_index);
			Data head = get(ptr);
			long startTime = head.timestamp;
			long prevTime = startTime;
			float min = head.value;
			long elapsed = 0;

			while (true) {
				Data data = get(ptr);
				// 値未設定 or キュー一周
				if (data.timestamp == -1 ||
					data.timestamp > startTime) {
					break;
				}
				// 追加するには昔すぎるデータ]
				elapsed += (prevTime - data.timestamp);
				if (elapsed >= seconds * 1000) {
					break;
				}
//				Log.i("", String.format("%d: %f", elapsed, data.value));
				min = Math.min(min, data.value);
				prevTime = data.timestamp;
				ptr = prev(ptr);
			}
			return min;
		}
	}

	public float maxInSeconds(long seconds) {
		synchronized (this) {
			int ptr = prev(_index);
			Data head = get(ptr);
			long startTime = head.timestamp;
			long prevTime = startTime;
			float max = head.value;
			long elapsed = 0;

			while (true) {
				Data data = get(ptr);
				// 値未設定 or キュー一周
				if (data.timestamp == -1 ||
					data.timestamp > startTime) {
					break;
				}
				// 追加するには昔すぎるデータ
				elapsed += (prevTime - data.timestamp);
				if (elapsed >= seconds * 1000) {
					break;
				}
//				Log.i("", String.format("%d: %f", elapsed, data.value));
				max = Math.max(max, data.value);
				prevTime = data.timestamp;
				ptr = prev(ptr);
			}
			return max;
		}
	}

	public float averageInSeconds(long seconds) {
		synchronized (this) {
			int ptr = prev(_index);
			Data head = get(ptr);
			long startTime = head.timestamp;
			long prevTime = startTime;
			float sum = 0;
			int dataCount = 0;
			long elapsed = 0;

			while (true) {
				Data data = get(ptr);
				// 値未設定 or キュー一周
				if (data.timestamp == -1 ||
					data.timestamp > startTime) {
					break;
				}
				// 追加するには昔すぎるデータ
				elapsed += (prevTime - data.timestamp);
				if (elapsed >= seconds * 1000) {
					break;
				}
//				Log.i("", String.format("%d: %f", elapsed, data.value));
				sum += data.value;
				dataCount++;
				prevTime = data.timestamp;
				ptr = prev(ptr);
			}
//Add 2011/09/21 Z01yoneya Start -->
            if (dataCount == 0) {
                return 0.0f;
            }
//Add 2011/09/21 Z01yoneya End <--
			return sum / dataCount;
		}
	}

	public float average(int recent) {
		synchronized (this) {
			int ptr = prev(_index);
			Data head = get(ptr);
			long startTime = head.timestamp;
			float sum = 0;
			int dataCount = 0;

			while (dataCount < recent) {
				Data data = get(ptr);
				// 値未設定 or キュー一周
				if (data.timestamp == -1 ||
					data.timestamp > startTime) {
					break;
				}
				sum += data.value;
				dataCount++;
				ptr = prev(ptr);
			}
//Add 2011/09/21 Z01yoneya Start -->
            if (dataCount == 0) {
                return 0.0f;
            }
//Add 2011/09/21 Z01yoneya End <--
			return sum / dataCount;
		}
	}
}