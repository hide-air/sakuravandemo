package net.zmap.android.pnd.v2.api.exception;

/**
 * データなし、ファイルなし
 */
public class NaviNotFoundException extends NaviException {
    private static final long serialVersionUID = 1L;

    /**
     * エラー詳細メッセージとして null を設定して NaviNotFoundException を構築します。
     */
    public NaviNotFoundException() {
        super();
    }

    /**
     * 詳細メッセージを指定して NaviNotFoundException を構築します。
     *
     * @param message
     *            詳細メッセージ
     */
    public NaviNotFoundException(String message) {
        super(message);
    }
}