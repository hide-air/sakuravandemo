package net.zmap.android.pnd.v2.api.event;

import android.location.Location;
import android.location.LocationManager;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * 自車位置情報拡張クラス
 *
 * @see IVehiclePositionListener
 */
public final class VehiclePositionEx implements Parcelable {
    /**
     * メンバ変数
     */
    /** イベント発生ロケーション (世界測地と現在日時) */
    private Location        mLocation;
    /** 高速道路上有無 */
    private boolean[] mIsBIsOnHighWay = new boolean[1];

    /**
     * コンストラクタ
     */
    public VehiclePositionEx() {
        mLocation = new Location(LocationManager.GPS_PROVIDER);
        mLocation.setLatitude(0.0);
        mLocation.setLongitude(0.0);
        mIsBIsOnHighWay[0] = false;
    }

    /**
     * コンストラクタ(自車位置情報指定)
     * @param location
     *            イベント発生ロケーション (世界測地座標と現在日時)
     * @param isBIsOnHighWay
     *            高速道路上有無
     */
    public VehiclePositionEx(Location location, boolean isBIsOnHighWay) {
        setLocation(location);
    	setBIsOnHighWay(isBIsOnHighWay);
    }

    private VehiclePositionEx(Parcel in) {
        readFromPercel(in);
    }

    //---------------
    // getter
    //---------------
    /**
     * イベント発生ロケーション取得
     *
     * @return イベント発生ロケーション (世界測地座標と現在日時)
     */
    public Location getLocation() {
        return mLocation;
    }

    /**
     * 高速道路上か否かを取得<br>
     *
     * @return true: 高速道路上, false: 高速道路上ではない
     */
    public boolean getBIsOnHighWay() {
        return mIsBIsOnHighWay[0];
    }

    //---------------
    // setter
    //---------------
    /**
     * ロケーション設定
     *
     * @param location
     *            イベント発生ロケーション (世界測地座標と現在日時)
     */
    public void setLocation(Location location) {
        mLocation = location;
    }

    /**
     * ロケーション設定
     *
     * @param isBIsOnHighWay
     *            高速道路上有無
     */
    public void setBIsOnHighWay(boolean isBIsOnHighWay) {
        mIsBIsOnHighWay[0] = isBIsOnHighWay;
    }

    /*
     * (非 Javadoc)
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(mLocation, flags);
        out.writeBooleanArray(mIsBIsOnHighWay);
    }

    private void readFromPercel(Parcel in) {
        mLocation = in.readParcelable(Location.class.getClassLoader());
        in.readBooleanArray(mIsBIsOnHighWay);
    }

    /* (非 Javadoc)
     *
     */
    public static final Parcelable.Creator<VehiclePositionEx> CREATOR = new Parcelable.Creator<VehiclePositionEx>() {
        public VehiclePositionEx createFromParcel(Parcel in) {
            return new VehiclePositionEx(in);
        }
        public VehiclePositionEx[] newArray(int size) {
            return new VehiclePositionEx[size];
        }
    };

	@Override
	public int describeContents() {
		return 0;
	}
}