package net.zmap.android.pnd.v2.app.control;

import android.content.Context;
import android.content.Intent;

import net.zmap.android.pnd.v2.ItsmoNaviDrive;
//import net.zmap.android.pnd.v2.app.launch.DownloadMapActivity;
import net.zmap.android.pnd.v2.common.utils.NaviLog;
//import net.zmap.android.pnd.v2.downloader.common.ExtraName;

/**
 *
 * PNDアプリ制御クラス
 *
 */
public class NaviAppController {
    Context mContext = null;

    public NaviAppController(Context context) {
        mContext = context;
    }

//Add 2011/10/12 Z01yoneya Start -->
    private ScreenSwitchBroadcastReceiver mScreenSwitchBroadcastReceiver = null;

    public void onCreate() {
        mScreenSwitchBroadcastReceiver = new ScreenSwitchBroadcastReceiver(this);
    }

    public void onDestroy() {
        try {
//            mScreenSwitchBroadcastReceiver.unreg(mContext.getApplicationContext());
            mScreenSwitchBroadcastReceiver = null;
        } catch (NullPointerException e) {
            mScreenSwitchBroadcastReceiver = null;
        }
    }

//Add 2011/10/12 Z01yoneya End <--

    /*
     * PNDアプリ起動
     *
     * @param int[] idList ダウンロードコンテンツIDリスト
     *                      nullの場合は、インテントに含めない
     *                      IDリストが有効の場合は、メインアクティビティからの地図ダウンロード起動インテントにIDリストを含める
     */
    public void startNaviApp(int[] idList) {
        try {
            NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviAppController::startNaviApp");
            Intent intent = new Intent(mContext, ItsmoNaviDrive.class);
////Add 2011/10/05 Z01yoneya Start -->
//            if (idList != null && idList.length > 0) {
//                intent.putExtra(ExtraName.DL_CONTENTS, idList);
//            }
////Add 2011/10/05 Z01yoneya End <--
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //外部からのアプリ起動なのでNaviActivityStarterは使用しない
            mContext.startActivity(intent);
        } catch (NullPointerException e) {
            //起動できなかった時は特に何もしない
//            e.printStackTrace();
        }
    }

    /*
     * PNDアプリ終了
     */
    public void finishNaviApp() {
        try {
            Intent intent = new Intent();
            intent.setAction(NaviControllIntent.ACTION_NAVI_APP_FINISH);
            mContext.sendBroadcast(intent);
        } catch (NullPointerException e) {
            //終了できなかった時は特に何もしない
//          e.printStackTrace();
        }
    }

//    /*
//     * 地図ダウンロードアクティビティ起動
//     *
//     * @param int[] idList ダウンロード処理(DownloadActivity)から開始する場合、idListを設定する。
//     *                      nullの場合は認証処理から行う。
//     */
//    public void startMapDownload(int[] idList) {
//        try {
//            Intent intent = new Intent(mContext, DownloadMapActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////Add 2011/10/05 Z01yoneya Start -->
//            if (idList != null && idList.length > 0) {
//                intent.putExtra(ExtraName.DL_CONTENTS, idList);
//            }
////Add 2011/10/05 Z01yoneya End <--
//            //地図ダウンロードアクティビティはナビアプリとは別扱い
//            //なのでNaviActivityStarterは使用しない
//            mContext.startActivity(intent);
//        } catch (NullPointerException e) {
//            //起動できなかった時は特に何もしない
////            e.printStackTrace();
//        }
//    }

    /*
     * 地図アクティビティ起動
     */
    public void startMapActivity() {
// MOD.2013.11.20 N.Sasao 起動速度改善 START
        try {

// Marge by CPJsunagawa '14-07-04 Start
            final int MIN_SPLASH_SHOW_TIME = 1000; //従来のメインアクティビティからの地図アクティビティ起動のdelay相当
            Thread.sleep(MIN_SPLASH_SHOW_TIME);
// Marge by CPJsunagawa '14-07-04 End

        	NaviLog.d(NaviLog.PRINT_LOG_TAG, "NaviAppController::startMapActivity");

            Intent intent = new Intent();
            intent.setAction(NaviControllIntent.ACTION_MAP_ACTIVITY_START);
            mContext.sendBroadcast(intent);
//Add 2011/10/12 Z01yoneya Start -->
            //地図アクティビティを作成してからSCREEN_ON/OFFブロードキャストを登録する
//            mScreenSwitchBroadcastReceiver.reg(mContext);
//Add 2011/10/12 Z01yoneya End <--
// MOD.2013.11.20 N.Sasao 起動速度改善  END
// Marge by CPJsunagawa '14-07-04 Start
        } catch (InterruptedException e) {
            //とくに何もしない
			NaviLog.e(NaviLog.PRINT_LOG_TAG, e);
// Marge by CPJsunagawa '14-07-04 End
        } catch (NullPointerException e) {
            //起動できなかった時は特に何もしない
//            e.printStackTrace();
        }
    }
}
