package net.zmap.android.pnd.v2.api.event;

import net.zmap.android.pnd.v2.api.event.VehiclePositionEx;

/**
 * 自車位置更新リスナインタフェース (aidl)
 */
interface IVehiclePositionExListener {

    /**
     * 自車位置が更新されたときに呼び出されるリスナ (自車位置情報拡張版)
     */
    void onChanged(in VehiclePositionEx vehiclePositionEx);
}