package net.zmap.android.pnd.v2.api.event;

import android.location.Location;
import net.zmap.android.pnd.v2.api.event.Road;

/**
 * 道路種別変更リスナインタフェース (aidl)
 */
interface IRoadListener {
    /**
     * 道路種別が変化したときに呼ばれるリスナ関数 (車モードのみ)
     */
    void onChanged(in Location location, in Road from, in Road to);
}