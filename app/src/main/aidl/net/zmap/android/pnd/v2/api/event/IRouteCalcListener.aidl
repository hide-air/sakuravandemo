package net.zmap.android.pnd.v2.api.event;

import android.location.Location;
import net.zmap.android.pnd.v2.api.event.RouteCalculated;

/**
 * ルート探索イベントリスナインタフェース (aidl)
 */
interface IRouteCalcListener {
    /**
     * ルート探索完了時に呼び出されるリスナ関数 (車モードのみ)
     */
    void onRouteCalculated(in RouteCalculated routeCalculated);
}