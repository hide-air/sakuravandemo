package net.zmap.android.pnd.v2.api.event;

interface IApplicationStatusListener {
    void onStarted();
    void onStopped();
}