package net.zmap.android.pnd.v2.api.event;

import android.location.Location;
import net.zmap.android.pnd.v2.api.event.Area;

/**
 * 行政界移動リスナインタフェース (aidl)
 */
interface IAreaListener {
    /**
     * 行政界が変化したときに呼ばれるリスナ関数 (車モードのみ)
     */
    void onChanged(in Location location, in Area from, in Area to);
}