package net.zmap.android.pnd.v2.api;

import net.zmap.android.pnd.v2.api.event.IApplicationStatusListener;
import net.zmap.android.pnd.v2.api.event.IAreaListener;
import net.zmap.android.pnd.v2.api.event.IRoadListener;
import net.zmap.android.pnd.v2.api.event.IRouteCalcListener;
import net.zmap.android.pnd.v2.api.event.IRouteGuidanceListener;
import net.zmap.android.pnd.v2.api.event.IVehiclePositionListener;
import net.zmap.android.pnd.v2.api.GeoPoint;
import net.zmap.android.pnd.v2.api.navigation.RemainingDistance;
import net.zmap.android.pnd.v2.api.navigation.RoutePoint;
import net.zmap.android.pnd.v2.api.navigation.RouteRequest;
import net.zmap.android.pnd.v2.api.NaviResult;
import net.zmap.android.pnd.v2.api.overlay.Icon;
import net.zmap.android.pnd.v2.api.overlay.Line;
import net.zmap.android.pnd.v2.api.event.IVehiclePositionExListener;

import java.util.List;

interface INaviService {

    /**
     * 地図を表示
     */
    void _openMap(out NaviResult res, in boolean forced);

    /**
     * 経緯度・縮尺を指定して地図を表示
     */
	void openMap(out NaviResult res, in GeoPoint point, in int zoomLevel, in boolean forced);

    /**
     * 地図中心座標の取得
     */
	GeoPoint getCenter(out NaviResult res);

    /**
     * 地図中心座標の設定
     */
	void setCenter(out NaviResult res, in GeoPoint point);

    /**
     * 地図縮尺の取得
     */
	int getZoom(out NaviResult res);

	/**
     * 地図縮尺の設定
     */
	void setZoom(out NaviResult res, in int zoomLevel);

    /**
     * 昼・夜モードの設定
     */
	void setDayNightMode(out NaviResult res,in int mode);

    /**
     * 指定した地点にユーザーアイコンを描画する.
     * 形状は指定したユーザレイヤに登録される.
     */
    void drawIcon( out NaviResult res, in String layerId, in Icon icon );

    /**
     * 指定した地点にライン形状を描画する.
     * 形状は指定したユーザレイヤに登録される.
     */
    void drawLine( out NaviResult res, in String layerId, in Line line );

    /**
     * 指定したユーザレイヤを消去する.
     */
    void removeLayer( out NaviResult res, in String layerId );

    /**
     * 全てのユーザレイヤを消去する.
     */
    void removeAllLayers( out NaviResult res );

    /**
     * 目的地までの案内情報(残距離、残時間..)を取得する
     */
    RemainingDistance getRemainingDistance(out NaviResult res);

    /**
     * ルート ID を指定して案内を開始する
     */
    void calculateRoute(out NaviResult res, in RouteRequest routeRequest, in boolean routeGuidanceStarting);

    /**
     * 案内を開始する
     */
    void startRouteGuidance(out NaviResult res);

    /**
     * 案内を終了する
     */
    void stopRouteGuidance(out NaviResult res);

    /**
     * お気に入り登録
     */
    void addFavorites(out NaviResult res,in RoutePoint point, in int categoryId);

    /**
     * 起動確認
     */
    boolean isNaviAppRunning();

    /**
     * 自車位置更新イベントリスナの登録
     */
 	void registerVehiclePositionListener(in IVehiclePositionListener listener);

    /**
     * 自車位置更新イベントリスナの解除
     */
 	void unregisterVehiclePositionListener(in IVehiclePositionListener listener);

    /**
     * 道路種別変更イベントリスナの登録
     */
 	void registerRoadListener(in IRoadListener listener);

    /**
     * 道路種別変更イベントリスナの解除
     */
 	void unregisterRoadListener(in IRoadListener listener);

    /**
     * 行政界移動イベントリスナの登録
     */
 	void registerAreaListener(in IAreaListener listener);

    /**
     * 行政界移動イベントリスナの解除
     */
 	void unregisterAreaListener(in IAreaListener listener);

    /**
     * ルート案内イベントリスナの登録
     */
 	void registerRouteGuidanceListener(in IRouteGuidanceListener listener);

    /**
     * ルート案内イベントリスナの解除
     */
 	void unregisterRouteGuidanceListener(in IRouteGuidanceListener listener);

    /**
     * ルート探索イベントリスナの登録
     */
 	void registerRouteCalcListener(in IRouteCalcListener listener);

    /**
     * ルート探索イベントリスナの解除
     */
 	void unregisterRouteCalcListener(in IRouteCalcListener listener);

    /**
     *
     */
    boolean isStarted();

    /**
     * アプリケーションステータスリスナの登録
     */
    void registerApplicationStatusListener(in IApplicationStatusListener listener);

    /**
     * アプリケーションステータスリスナの解除
     */
    void unregisterApplicationStatusListener(in IApplicationStatusListener listener);

    /**
     * 自車位置更新イベントリスナの登録(拡張版)
     */
 	void registerVehiclePositionExListener(in IVehiclePositionExListener listener);

    /**
     * 自車位置更新イベントリスナの解除(拡張版)
     */
 	void unregisterVehiclePositionExListener(in IVehiclePositionExListener listener);

// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成 START
	/**
     * 走行規制機能の制御許可申請
     */
    boolean requestDrivingControlLock( int nKey );

	/**
     * 走行規制機能の制御解放
     */
    boolean requestDrivingControlUnLock( int nKey );

	/**
     * 走行規制指示
     */
    boolean requestDrivingControl( int nKey, boolean bControl );
// ADD 2014.01.07 N.Sasao マルチ車載器連携地盤作成  END

}
