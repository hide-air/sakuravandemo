package net.zmap.android.pnd.v2.api.event;

import android.location.Location;
import net.zmap.android.pnd.v2.api.event.RouteGuidance;

/**
 * ルート案内リスナインターフェース (aidl)
 */
interface IRouteGuidanceListener {

    /**
     * 案内を開始したときに呼び出されるリスナ関数
     */
    void onStarted(in RouteGuidance routeGuidance);

    /**
     * 案内を終了したときに呼び出されるリスナ関数
     */
    void onStopped(in RouteGuidance routeGuidance);

    /**
     * 案内を一時停止したときに呼び出されるリスナ関数
     */
	void onPaused(in RouteGuidance routeGuidance);

    /**
     * 一時停止した再開したときに呼び出されるリスナ関数
     */
    void onResumed(in RouteGuidance routeGuidance);

    /**
     * ルート案内中、目標案内 (交差点) に接近して、音声案内したときに呼び出されるリスナ関数
     */
    void onNearToCrossroad(in RouteGuidance routeGuidance);

    /**
     * ルート案内中、経由地に接近して、音声案内したときに呼び出されるリスナ関数
     */
    void onNearToWaypoint(in RouteGuidance routeGuidance);

    /**
     * ルート案内中、目的地に接近して、音声案内したときに呼び出されるリスナ関数
     */
    void onNearToDestination(in RouteGuidance routeGuidance);

    /**
     * ルート案内中、リルート後に案内を開始したときに呼び出されるリスナ関数
     */
    void onRerouted(in RouteGuidance routeGuidance);
}