package net.zmap.android.pnd.v2.api.event;

import android.location.Location;

/**
 * 自車位置更新リスナインタフェース (aidl)
 */
interface IVehiclePositionListener {

    /**
     * 自車位置が更新されたときに呼び出されるリスナ (車モードのみ)
     */
    void onChanged(in Location location);
}