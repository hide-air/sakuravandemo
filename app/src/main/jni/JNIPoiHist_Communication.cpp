/********************************************************************************
* File Name	:	 JNIPoiArnd_Communication.cpp
* Created	:	2009/10/12
* Author	:	xiayx
* Model		:
* Description	:
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "JNIPoiHist_Communication.h"
#include "NaviEngineIF.h"
#include "NaviEngineCtrl.h"
#include "NaviEngine/CT/CTUICommand.h"
#include "NETypes.h"
#include "windows.h"
#include "NaviEngine/POI/POIStruct.h"
#include "jni/JNI_Common.h"
#include "CTICU.h"
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define LOG_TAG "NavigationJNI_NaviRun"
#define JNI_DEBUG

#include "debugPrint.h"

typedef unsigned char byte;

/*********************************************************************
*	Function Name	: JNI_NE_POIHist_Reset         	             *
*	Description	:                                            *
*	Date		: 09/11/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHist_Reset
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POIHist_Reset();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHist_Clear        	             *
*	Description	:                                            *
*	Date		: 09/11/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHist_Clear
  (JNIEnv *env, jclass cls,jobject object)
{
	ZPOI_History_FilePath Path;
	jlong Ret = (jlong)NE_POIHist_Clear(&Path);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_History_FilePath");


	//Get variable's ID in JNI
	jfieldID FilePath = env->GetFieldID(clazz,"m_stRet","S");
	if(FilePath)
	{
		//Set the variable's value  to Java
		env->SetShortField(object,FilePath,(jshort)Path.stRet);
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIHist_Reset GetFieldID Path Failed");
	}

	return Ret;

}

/*********************************************************************
*	Function Name	: JNI_NE_POIHist_GetRecCount        	      *
*	Description	:                                            *
*	Date		: 09/11/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHist_GetRecCount
  (JNIEnv *env, jclass cls,jobject object)
{
	long RecCount;

	jlong Ret = (jlong)NE_POIHist_GetRecCount(&RecCount);

	//LOGDp("[DEBUG] JNI_NE_POIHist_GetRecCount RecCount:%d!!!!!!!!!!!!",RecCount);
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jRecCount = env->GetFieldID(clazz,"lcount","J");
		if(jRecCount)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,jRecCount,(long)RecCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIHist_GetRecCount GetFieldID RecCount Failed");
		}
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHist_RemoveRec        	      *
*	Description	:                                            *
*	Date		: 09/11/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHist_RemoveRec
  (JNIEnv *env, jclass cls,jlong RecordIndex)
{
	//LOGDp("[DEBUG] JNI_NE_POIHist_RemoveRec Begin");
	return (jlong)NE_POIHist_RemoveRec((long)RecordIndex);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHist_GetRecList        	      *
*	Description	:                                            *
*	Date		: 09/11/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHist_GetRecList
  (JNIEnv *env, jclass clz,jlong Index,jlong RecCount,jobjectArray args,jobject object)
{
	ZPOI_UserFile_RecordIF *pstRecs;
	long GetRecCount = 0;

	jlong Ret = (jlong)NE_POIHist_GetRecList((long)Index,(long)RecCount,&pstRecs,&GetRecCount);


	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}

	//LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList Sucessful");

	if(cls)
	{
		//Get variable's ID in JNI
		//LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList GetLongField Count Begin");
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		//LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList GetLongField Count End");
		if(Count)
		{
			//Set the variable's value  to Java
			//LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList SetLongField Count Begin");
			env->SetLongField(object,Count,(jlong)GetRecCount);
			//LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList SetLongField Count End");
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList FindClass  Failed");
	}

	//LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList Middle");
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_UserFile_RecordIF");
#if 0
	if(clazz)
	{
		LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList FindClass Sucessful!!!!!!!!!!!!!!");
	}
#endif
	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < GetRecCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			jfieldID IconCode = env->GetFieldID(clazz,"m_lIconCode","J");
			if(IconCode)
			{
				env->SetLongField(obj,IconCode,pstRecs[i].dwIconCode);
				jfieldID DispName = env->GetFieldID(clazz,"m_DispName","Ljava/lang/String;");
				if(DispName)
				{
					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].szDispName));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].szDispName), (jbyte*)pstRecs[i].szDispName);

					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						env->SetObjectField(obj,DispName,jsName);
					}

					if(NULL != bytes)
					{
						env->DeleteLocalRef( bytes);
						bytes = NULL;
					}
					if(NULL != encoding)
					{
						env->DeleteLocalRef( encoding);
						encoding = NULL;
					}
					if(NULL != jsName)
					{
						env->DeleteLocalRef( jsName);
						jsName = NULL;
					}

					jfieldID Date = env->GetFieldID(clazz,"m_lDate","J");
					if(Date)
					{
						env->SetLongField(obj,Date,pstRecs[i].dwDate);
						jfieldID Lon = env->GetFieldID(clazz,"m_lLon","J");
						if(Lon)
						{
							env->SetLongField(obj,Lon,pstRecs[i].dwLon);
							jfieldID Lat = env->GetFieldID(clazz,"m_lLat","J");
							if(Lat)
							{
								env->SetLongField(obj,Lat,pstRecs[i].dwLat);
								jfieldID UserFlag = env->GetFieldID(clazz,"m_lUserFlag","J");
								if(UserFlag)
								{
									env->SetLongField(obj,UserFlag,pstRecs[i].wUserFlag);
									jfieldID Import = env->GetFieldID(clazz,"m_lImport","J");
									if (Import)
									{
										env->SetLongField(obj,Import,pstRecs[i].bImport);
									}
									else
									{
										LOGDp("[DEBUG] JNICALL JNI_NE_POIHist_GetRecList GetFieldID Import Failed");
									}
								}
								else
								{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIHist_GetRecList GetFieldID UserFlag Failed");
								}
							}
							else
							{
								LOGDp("[DEBUG] JNICALL JNI_NE_POIHist_GetRecList GetFieldID Lat Failed");
							}
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POIHist_GetRecList GetFieldID Lon Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList GetFieldID Date Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList GetFieldID DispName Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList GetFieldID IconCode Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POIHist_GetRecList FindClass Failed!");
		}

		if(NULL != obj)
		{
			env->DeleteLocalRef( obj);
			obj = NULL;
		}
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHist_GetListDate        	      *
*	Description	:                                            *
*	Date		: 09/11/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHist_GetListDate
  (JNIEnv *env, jclass clv,jlong FromDate,jlong ToDate,jobject object,jobjectArray args)
{
	ZPOI_History_ListDate *pstListDate;
	long ListCount;

	jlong Ret = (jlong)NE_POIHist_GetListDate(FromDate,ToDate,&ListCount,&pstListDate);

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//LOGDp("[DEBUG] JNI_NE_POIHist_GetListDate Sucessful");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)ListCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIHist_GetListDate GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIHist_GetListDate FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_History_ListDate");
#if 0
	if(clazz)
	{
		LOGDp("[DEBUG] JNI_NE_POIHist_GetListDate FindClass Sucessful!!!!!!!!!!!!!!");
	}
#endif
	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < ListCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIHist_GetListDate loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			jfieldID Date = env->GetFieldID(clazz,"m_lDate","J");
			if(Date)
			{
				env->SetLongField(obj,Date,pstListDate[i].dwDate);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIHist_GetListDate GetFieldID Date  Failed");
			}
		}

		if(NULL != obj)
		{
			env->DeleteLocalRef( obj);
			obj = NULL;
		}
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHist_GetDateRecCount       	      *
*	Description	:                                            *
*	Date		: 09/11/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHist_GetDateRecCount
  (JNIEnv *env, jclass clz,jobject object,jint DateCount,jobject obj)
{
	unsigned long Date = 0;
	long RecCount = 0;

	jlong Ret = (jlong)NE_POIHist_GetDateRecCount(&Date,(unsigned int)DateCount,&RecCount);

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//LOGDp("[DEBUG] JNI_NE_POIHist_GetListDate Sucessful");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID DateCount = env->GetFieldID(cls,"lcount","J");
		if(DateCount)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,DateCount,(jlong)Date);

			jfieldID jRecCount = env->GetFieldID(cls,"lcount","J");
			if(RecCount)
			{
				env->SetLongField(obj,jRecCount,(jlong)RecCount);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIHist_GetDateRecCount GetLongField RecCount Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIHist_GetDateRecCount GetLongField DateCount Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIHist_GetDateRecCount FindClass  Failed");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHist_GetDateRec      	      *
*	Description	:                                            *
*	Date		: 09/11/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHist_GetDateRec
  (JNIEnv *env, jclass clv,jlong Index,jlong RecCount,jobject object,jint DateCount,jobjectArray args,jobject obj)
{
	unsigned long Date = 0;
	long GetRecCount = 0;
	ZPOI_UserFile_RecordIF *pstRecs;

	jlong Ret = (jlong)NE_POIHist_GetDateRec((long)Index,(long)RecCount,&Date,(unsigned short)DateCount,&pstRecs,&GetRecCount);

	//jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNIShort");
	jclass clz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	jclass strClass = env->FindClass("java/lang/String");
	jmethodID ctorID = 0;
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}

	//LOGDp("[DEBUG] JNI_NE_POIHist_GetListDate Sucessful");

	if(clz)
	{
		//Get variable's ID in JNI
		jfieldID jDate = env->GetFieldID(clz,"lcount","J");
		if(jDate)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,jDate,(jlong)Date);

		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIHist_GetDateRec GetLongField Date Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIHist_GetDateRecCount FindClass  Failed");
	}
	if(clz)
	{
		jfieldID jRecCount = env->GetFieldID(clz,"lcount","J");
		if(RecCount)
		{
			env->SetLongField(obj,jRecCount,(jlong)RecCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIHist_GetDateRec GetLongField GetRecCount Failed");
		}
	}

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_UserFile_RecordIF");
#if 0
	if(clazz)
	{
		LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList FindClass Sucessful!!!!!!!!!!!!!!");
	}
#endif
	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < RecCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			jfieldID IconCode = env->GetFieldID(clazz,"m_lIconCode","J");
			if(IconCode)
			{
				env->SetLongField(obj,IconCode,pstRecs[i].dwIconCode);
				jfieldID DispName = env->GetFieldID(clazz,"m_DispName","Ljava/lang/String;");
				if(DispName)
				{
					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].szDispName));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].szDispName), (jbyte*)pstRecs[i].szDispName);
					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						env->SetObjectField(obj,DispName,jsName);
					}

					if(NULL != bytes)
					{
						env->DeleteLocalRef( bytes);
						bytes  = NULL;
					}
					if(NULL != encoding)
					{
						env->DeleteLocalRef( encoding);
						encoding  = NULL;
					}
					if(NULL != jsName)
					{
						env->DeleteLocalRef( jsName);
						jsName  = NULL;
					}

					jfieldID Date = env->GetFieldID(clazz,"m_lDate","J");
					if(Date)
					{
						env->SetLongField(obj,Date,pstRecs[i].dwDate);
						jfieldID Lon = env->GetFieldID(clazz,"m_lLon","J");
						if(Lon)
						{
							env->SetLongField(obj,Lon,pstRecs[i].dwLon);
							jfieldID Lat = env->GetFieldID(clazz,"m_lLat","J");
							if(Lat)
							{
								env->SetLongField(obj,Lat,pstRecs[i].dwLat);
								jfieldID UserFlag = env->GetFieldID(clazz,"m_lUserFlag","J");
								if(UserFlag)
								{
									env->SetLongField(obj,UserFlag,pstRecs[i].wUserFlag);
									jfieldID Import = env->GetFieldID(clazz,"m_lImport","J");
									if (Import)
									{
										env->SetLongField(obj,Import,pstRecs[i].bImport);
									}
									else
									{
										LOGDp("[DEBUG] JNICALL JNI_NE_POIHist_GetRecList GetFieldID Import Failed");
									}
								}
								else
								{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIHist_GetRecList GetFieldID UserFlag Failed");
								}
							}
							else
							{
								LOGDp("[DEBUG] JNICALL JNI_NE_POIHist_GetRecList GetFieldID Lat Failed");
							}
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POIHist_GetRecList GetFieldID Lon Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList GetFieldID Date Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList GetFieldID DispName Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIHist_GetRecList GetFieldID IconCode Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POIHist_GetRecList FindClass Failed!");
		}

		if(NULL != obj)
		{
			env->DeleteLocalRef( obj);
			obj  = NULL;
		}
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHist_RemoveItemDate      	      *
*	Description	:                                            *
*	Date		: 09/11/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHist_RemoveItemDate
  (JNIEnv *env, jclass clz,jobject object,jint DateCount,jlong Index)
{
	unsigned long Date = 0;
	jlong Ret = (jlong)NE_POIHist_RemoveItemDate(&Date,(int)DateCount,(long)Index);

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//LOGDp("[DEBUG] JNI_NE_POIHist_GetListDate Sucessful");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID DateCount = env->GetFieldID(cls,"lcount","J");
		if(DateCount)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,DateCount,(jlong)Date);
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIHist_GetDateRecCount FindClass  Failed");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHist_SetRec          	      *
*	Description	:                                            *
*	Date		: 09/11/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHist_SetRec(JNIEnv *env, jclass cls,jobject object,jlong DataCount,jobject obj)
{
	ZPOI_UserFile_RecordIF Record;
	ZPOI_UserFile_DataIF Data;
	jclass jcls = env->GetObjectClass(object);
	jclass jclz = env->GetObjectClass(obj);

	if (jcls)
	{
		jfieldID jfid1 = env->GetFieldID( jcls, "m_lIconCode", "J" );
		if (jfid1)
		{
			jlong value1 = env->GetLongField(object,jfid1);
			Record.dwIconCode = (long)value1;
		}

		jfieldID jfid2 = env->GetFieldID( jcls, "m_DispName", "Ljava/lang/String;");
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応 START
		if (jfid2)
		{
			jstring jstr = (jstring)env->GetObjectField(object,jfid2);
			jsize alen = 0;
			jbyte* ba = NULL;

		    jclass clsstring = env->FindClass(CLASS_CommonLib);
		    jmethodID mid = env->GetStaticMethodID(clsstring, "getToSJIS", "(Ljava/lang/String;)[B");
			jbyteArray barr= (jbyteArray)env->CallStaticObjectMethod(clsstring, mid, jstr);
			if( barr != NULL ){
				alen = env->GetArrayLength(barr);
		    	ba = env->GetByteArrayElements(barr, JNI_FALSE);

				if( alen > 0 ){
					// CID#10260 無意味な一時領域の確保を取りやめ直接目的領域に文字列を転写する
		   			memcpy(Record.szDispName, ba, sizeof(char)*(alen+1));
		   			Record.szDispName[alen] = '\0';	// jniの仕様がわからないので念のため
				}
		   		env->ReleaseByteArrayElements(barr, ba, 0);
		 	}
		}
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応  END

		jfieldID jfid3 = env->GetFieldID( jcls, "m_lDate", "J" );
		if (jfid3)
		{
			jlong value3 = env->GetLongField(object,jfid3);
			Record.dwDate = (long)value3;
		}

		jfieldID jfid4 = env->GetFieldID( jcls, "m_lLon", "J" );
		if (jfid4)
		{
			jlong value4 = env->GetLongField(object,jfid4);
			Record.dwLon = (long)value4;
		}

		jfieldID jfid5 = env->GetFieldID( jcls, "m_lLat", "J" );
		if (jfid5)
		{
			jlong value5 = env->GetLongField(object,jfid5);
			Record.dwLat = (long)value5;
		}

		jfieldID jfid6 = env->GetFieldID( jcls, "m_lUserFlag", "J" );
		if (jfid6)
		{
			jlong value6 = env->GetLongField(object,jfid6);
			Record.wUserFlag = (long)value6;
		}

		jfieldID jfid7 = env->GetFieldID( jcls, "m_lUserFlag", "J" );
		if (jfid7)
		{
			jlong value7 = env->GetLongField(object,jfid7);
			Record.bImport = (long)value7;
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIHist_SetRec FindClass  Failed");
	}

	if (jclz)
	{

		jfieldID jfid1 = env->GetFieldID( jclz, "m_sDataIndex", "S" );
		if (jfid1)
		{
			jlong value1 = env->GetShortField(obj,jfid1);
			Data.wDataIndex = (long)value1;
		}

		jfieldID jfid2 = env->GetFieldID( jclz, "m_sDataSize", "S" );
		if (jfid2)
		{
			jlong value2 = env->GetShortField(obj,jfid2);
			Data.wDataSize = (long)value2;
		}

		jfieldID jfid3 = env->GetFieldID( jclz, "m_Data", "Ljava/lang/String;");
		if (jfid3)
		{
		    jstring jstr = (jstring)env->GetObjectField(obj,jfid3);
		    char* rtn = NULL;

		    jclass clsstring = env->FindClass("java/lang/String");
		    jstring strencode = env->NewStringUTF("ShiftJIS");
		    jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
		    jbyteArray barr= (jbyteArray)env->CallObjectMethod(jstr, mid, strencode);
		    jsize alen = env->GetArrayLength(barr);
		    jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);
		    if (alen > 0)
		   {
			 rtn = new char[alen+1];
			 memset(rtn, 0, sizeof(char)*(alen+1));
			 memcpy(rtn, ba, sizeof(char)*(alen+1));
			 rtn[alen] = '\0';
		   }

			Data.pcData = rtn;
			env->ReleaseByteArrayElements(barr, ba, 0);
			env->DeleteLocalRef(strencode);
		 	if(rtn)
		      {
				delete[] rtn;
			      rtn = NULL;
		      }
		}
		//LOGDp("[DEBUG] JNI_NE_POIHist_SetRec Data.pcData=%s",Data.pcData);
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIHist_SetRec FindClass  Failed");
	}
	//LOGDp("[DEBUG] !DataCount=%d",(long)DataCount);
	jlong Ret = (jlong)NE_POIHist_SetRec(&Record,(long)DataCount,&Data);

	return Ret;
}

// Add 2011/05/10 yinrongji Start

/*********************************************************************
*	Function Name	: JNI_NE_POIHistory_SetDistance                 *
*	Description	:                                            *
*	Date		: 11/05/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: yinrongji	                             *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHistory_SetDistance
  (JNIEnv *env, jclass cls,jlong lnDistance)
{
	jlong Ret = NE_ERR_POI;
	if(lnDistance > 0){

		Ret = (jlong)NE_POIHistory_SetDistance((long)lnDistance);
	}else{
		LOGDp("[DEBUG] NE_POIHistory_SetDistance lnDistance < 0");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHistory_GetAroundList                 *
*	Description	:                                            *
*	Date		: 11/05/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: yinrongji	                             *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHistory_GetAroundList
  (JNIEnv *env, jclass cls, jlong Longitude, jlong Latitude)
{
	return (jlong)NE_POIHistory_GetAroundList((long)Longitude,(long)Latitude);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHistory_GetAroundRecCount        *
*	Description	:                                            *
*	Date		: 11/05/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: yinrongji	                             *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHistory_GetAroundRecCount
  (JNIEnv *env, jclass cls,jobject object)
{
		ZPOI_Mybox_ListGenreCount RecCount;

		jlong lRet = (jlong)NE_POIHistory_GetAroundRecCount(&RecCount);

		//LOGDp("[DEBUG] JNI_NE_POIMybox_GetGenreCount Listconut:%d,lRet:%d!!!!!!!!!!!!",RecCount,lRet);

/*
		//Fing Java's Class
		jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Mybox_ListGenreCount");

		//jlong lCount = (*env)->GetLongField(env,object,Count);

		//LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundRecCount Sucessful");

		if(clazz)
		{
			//Get variable's ID in JNI
			jfieldID GenreCode = env->GetFieldID(clazz,"m_lGenreCode","J");
			if(GenreCode)
			{
				//Set the variable's value  to Java
				env->SetLongField(object,GenreCode,RecCount.dwGenreCode);
				jfieldID RecordCount = env->GetFieldID(clazz,"m_sRecordCount","S");
				if (RecordCount)
				{
					env->SetShortField(object,RecordCount,(jshort)RecCount.wRecordCount);
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundRecCount GetLongField RecordCount Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundRecCount GetShortField GenreCode Failed");
			}
		}

		else
		{
			LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundRecCount FindClass  Failed");
		}
*/
		jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

		//LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundRecCount Sucessful");

		if(clazz)
		{
			//Get variable's ID in JNI
			jfieldID Count = env->GetFieldID(clazz,"lcount","J");
			if(Count)
			{
				//Set the variable's value  to Java
				env->SetLongField(object,Count,(jlong)RecCount.wRecordCount);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundRecCount GetLongField Count Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundRecCount FindClass  Failed");
		}
		return lRet;

}

/*********************************************************************
*	Function Name	: JNI_NE_POIHistory_GetAroundListItem        *
*	Description	:                                            *
*	Date		: 11/05/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: yinrongji	                             *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHistory_GetAroundListItem
  (JNIEnv *env, jclass clz,jlong lnStartindex,jlong lnListcount,jobjectArray args,jobject object)
{
	ZPOI_UserFile_AroundRec *pstRecs;
	long GetRecCount = 0;

	jlong Ret = (jlong)NE_POIHistory_GetAroundListItem((long)lnStartindex,(long)lnListcount,&pstRecs,&GetRecCount);

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}

	//LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundListItem Sucessful");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)GetRecCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundListItem GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundListItem FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_UserFile_AroundRec");



	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < GetRecCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundListItem loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			jfieldID IconCode = env->GetFieldID(clazz,"m_lIconCode","J");
			if(IconCode)
			{
				env->SetLongField(obj,IconCode,pstRecs[i].dwIconCode);
				jfieldID DispName = env->GetFieldID(clazz,"m_DispName","Ljava/lang/String;");
				if(DispName)
				{

					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].szDispName));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].szDispName), (jbyte*)pstRecs[i].szDispName);

					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						env->SetObjectField(obj,DispName,jsName);
					}

					if(NULL != bytes)
					{
						env->DeleteLocalRef( bytes);
						bytes = NULL;
					}
					if(NULL != encoding)
					{
						env->DeleteLocalRef( encoding);
						encoding = NULL;
					}
					if(NULL != jsName)
					{
						env->DeleteLocalRef( jsName);
						jsName = NULL;
					}

					jfieldID Date = env->GetFieldID(clazz,"m_lDate","J");
					if(Date)
					{
						env->SetLongField(obj,Date,pstRecs[i].dwDate);
						jfieldID Lon = env->GetFieldID(clazz,"m_lLon","J");
						if(Lon)
						{
							env->SetLongField(obj,Lon,pstRecs[i].dwLon);
							jfieldID Lat = env->GetFieldID(clazz,"m_lLat","J");
							if(Lat)
							{
								env->SetLongField(obj,Lat,pstRecs[i].dwLat);
								jfieldID UserFlag = env->GetFieldID(clazz,"m_lUserFlag","J");
								if(UserFlag)
								{
									env->SetLongField(obj,UserFlag,pstRecs[i].wUserFlag);
									jfieldID Import = env->GetFieldID(clazz,"m_lImport","J");
									if (Import)
									{
										env->SetLongField(obj,Import,pstRecs[i].bImport);
										jfieldID Time = env->GetFieldID(clazz,"m_sTime","S");
										if (Time)
										{
											env->SetShortField(obj,Time,pstRecs[i].wTime);
											jfieldID RecIndex = env->GetFieldID(clazz,"m_lRecIndex","J");
											if (RecIndex)
											{
												env->SetLongField(obj,RecIndex,pstRecs[i].dwRecIndex);
												jfieldID Distance = env->GetFieldID(clazz,"m_lDistance","J");
												if (Distance)
												{
													env->SetLongField(obj,Distance,pstRecs[i].lnDistance);
													jfieldID GenreIndex = env->GetFieldID(clazz,"m_lGenreIndex","J");
													if (GenreIndex)
													{
														env->SetLongField(obj,GenreIndex,pstRecs[i].lnGenreIndex);
													}
													else
													{
														LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetAroundListItem GetFieldID GenreIndex Failed");
													}
												}
												else
												{
													LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetAroundListItem GetFieldID Distance Failed");
												}
											}
											else
											{
												LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetAroundListItem GetFieldID RecIndex Failed");
											}
										}
										else
										{
											LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetAroundListItem GetFieldID Time Failed");
										}
									}
									else
									{
										LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetAroundListItem GetFieldID Import Failed");
									}
								}
								else
								{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetAroundListItem GetFieldID UserFlag Failed");
								}
							}
							else
							{
								LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetAroundListItem GetFieldID Lat Failed");
							}
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetAroundListItem GetFieldID Lon Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundListItem GetFieldID Date Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundListItem GetFieldID DispName Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundListItem GetFieldID IconCode Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POIHistory_GetAroundListItem FindClass Failed!");
		}

		if(NULL != obj)
		{
			env->DeleteLocalRef( obj);
			obj = NULL;
		}
	}

	return Ret;
}


/*********************************************************************
*	Function Name	: JNI_NE_POIHistory_SetRouteParam            *
*	Description	:                                            *
*	Date		: 11/05/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: yinrongji	                             *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHistory_SetRouteParam
  (JNIEnv *env, jclass cls,jobject object)
{
	//LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_SetParam begin!!!!!!!!!");
	ZPOI_Route_Param pstRoutData;

	LOGDp("[DEBUG] JNI_NE_POIHistory_SetParam Begin!!!!!!!!!!!");
	jclass jcls = env->GetObjectClass(object);

	if (jcls)
	{

		jfieldID jfid1 = env->GetFieldID( jcls, "eside", "I" );
		if (jfid1)
		{
			jint value1 = env->GetIntField(object,jfid1);
			pstRoutData.eSide= (EPOI_ROUTE_SIDE)value1;
		}

		jfieldID jfid2 = env->GetFieldID( jcls, "lnCount", "J" );
		if (jfid2)
		{
			jlong value2 = env->GetLongField(object,jfid2);
			pstRoutData.lnCount = (long)value2;
		}

		jfieldID jfid3 = env->GetFieldID( jcls, "ulnRange", "J" );
		if (jfid3)
		{
			jlong value3 = env->GetLongField(object,jfid3);
			pstRoutData.ulnRange = (unsigned long)value3;

		}

	}
	return (jlong)NE_POIHistory_SetRouteParam(&pstRoutData);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHistory_GetRouteList             *
*	Description	:                                            *
*	Date		: 11/05/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: yinrongji	                             *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHistory_SetRoute
  (JNIEnv *env, jclass cls,jint iForward)
{
	//LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_SetRoute begin!!!!!!!!!");

	return (jlong)NE_POIHistory_SetRoute((BOOL)iForward);
}


/*********************************************************************
*	Function Name	: JNI_NE_POIHistory_GetRouteList             *
*	Description	:                                            *
*	Date		: 11/05/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: yinrongji	                             *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHistory_GetRouteList
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POIHistory_GetRouteList();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHistory_GetRouteRecCount         *
*	Description	:                                            *
*	Date		: 11/05/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: yinrongji	                             *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHistory_GetRouteRecCount
  (JNIEnv *env, jclass cls, jobject object)
{
	ZPOI_Mybox_ListGenreCount RecCount;

	jlong lRet = (jlong)NE_POIHistory_GetRouteRecCount(&RecCount);

	//LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteRecCount Listconut:%d,lRet:%d!!!!!!!!!!!!",RecCount,lRet);

/*
	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Mybox_ListGenreCount");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID GenreCode = env->GetFieldID(clazz,"m_lGenreCode","J");
		if(GenreCode)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,GenreCode,RecCount.dwGenreCode);
			jfieldID RecordCount = env->GetFieldID(clazz,"m_sRecordCount","S");
			if (RecordCount)
			{
				env->SetShortField(object,RecordCount,(jshort)RecCount.wRecordCount);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteRecCount GetLongField RecordCount Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteRecCount GetShortField GenreCode Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteRecCount FindClass  Failed");
	}
*/
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//LOGDp("[DEBUG] JNI_NE_POIHistory_GetAroundRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)RecCount.wRecordCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteRecCount GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteRecCount FindClass  Failed");
	}
	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIHistory_GetRouteListItem         *
*	Description	:                                            *
*	Date		: 11/05/10                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: yinrongji	                             *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIHistory_GetRouteListItem
  (JNIEnv *env, jclass clz,jlong lnStartindex,jlong lnListcount,jobjectArray args,jobject object)
{
	ZPOI_UserFile_RouteRec *pstRecs;
	long GetRecCount = 0;

	jlong Ret = (jlong)NE_POIHistory_GetRouteListItem((long)lnStartindex,(long)lnListcount,&pstRecs,&GetRecCount);

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}

	//LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteListItem Sucessful");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)GetRecCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteListItem GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteListItem FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_UserFile_RouteRec");



	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < GetRecCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteListItem loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			jfieldID IconCode = env->GetFieldID(clazz,"m_lIconCode","J");
			if(IconCode)
			{
				env->SetLongField(obj,IconCode,pstRecs[i].dwIconCode);
				jfieldID DispName = env->GetFieldID(clazz,"m_DispName","Ljava/lang/String;");
				if(DispName)
				{

					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].szDispName));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].szDispName), (jbyte*)pstRecs[i].szDispName);

					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						env->SetObjectField(obj,DispName,jsName);
					}

					jfieldID Date = env->GetFieldID(clazz,"m_lDate","J");
					if(Date)
					{
						env->SetLongField(obj,Date,pstRecs[i].dwDate);
						jfieldID Lon = env->GetFieldID(clazz,"m_lLon","J");
						if(Lon)
						{
							env->SetLongField(obj,Lon,pstRecs[i].dwLon);
							jfieldID Lat = env->GetFieldID(clazz,"m_lLat","J");
							if(Lat)
							{
								env->SetLongField(obj,Lat,pstRecs[i].dwLat);
								jfieldID UserFlag = env->GetFieldID(clazz,"m_lUserFlag","J");
								if(UserFlag)
								{
									env->SetLongField(obj,UserFlag,pstRecs[i].wUserFlag);
									jfieldID Import = env->GetFieldID(clazz,"m_lImport","J");
									if (Import)
									{
										env->SetLongField(obj,Import,pstRecs[i].bImport);
										jfieldID Time = env->GetFieldID(clazz,"m_sTime","S");
										if (Time)
										{
											env->SetShortField(obj,Time,pstRecs[i].wTime);
											jfieldID RecIndex = env->GetFieldID(clazz,"m_lRecIndex","J");
											if (RecIndex)
											{
												env->SetLongField(obj,RecIndex,pstRecs[i].dwRecIndex);
												jfieldID Distance = env->GetFieldID(clazz,"m_lDistance","J");
												if (Distance)
												{
													env->SetLongField(obj,Distance,pstRecs[i].lnDistance);
													jfieldID GenreIndex = env->GetFieldID(clazz,"m_lGenreIndex","J");
													if (GenreIndex)
													{
														env->SetLongField(obj,GenreIndex,pstRecs[i].lnGenreIndex);
													}
													else
													{
														LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetRouteListItem GetFieldID GenreIndex Failed");
													}
												}
												else
												{
													LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetRouteListItem GetFieldID Distance Failed");
												}
											}
											else
											{
												LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetRouteListItem GetFieldID RecIndex Failed");
											}
										}
										else
										{
											LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetRouteListItem GetFieldID Time Failed");
										}
									}
									else
									{
										LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetRouteListItem GetFieldID Import Failed");
									}
								}
								else
								{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetRouteListItem GetFieldID UserFlag Failed");
								}
							}
							else
							{
								LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetRouteListItem GetFieldID Lat Failed");
							}
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POIHistory_GetRouteListItem GetFieldID Lon Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteListItem GetFieldID Date Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteListItem GetFieldID DispName Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIHistory_GetRouteListItem GetFieldID IconCode Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POIHistory_GetRouteListItem FindClass Failed!");
		}
	}

	return Ret;
}
// Add 2011/05/10 yinrongji End
