/********************************************************************************
* File Name	:	 JNIPoiFacility_Communication.cpp
* Created	:	2009/10/26
* Author	:	xiayx
* Model		:
* Description	:
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "JNIPoiArnd_Communication.h"
#include "NaviEngineIF.h"
#include "NaviEngineCtrl.h"
#include "NaviEngine/CT/CTUICommand.h"
#include "NETypes.h"
#include "windows.h"
#include "NaviEngine/POI/POIStruct.h"
#include "jni/JNI_Common.h"
#include "CTICU.h"
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define LOG_TAG "NavigationJNI_NaviRun"
#define JNI_DEBUG

#include "debugPrint.h"

/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_Cancel         	     *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jint JNICALL JNI_NE_POIArnd_Cancel
  (JNIEnv *env, jclass a)
{
	//LOGDp("[DEBUG] JNICALL JNI_NE_POIArnd_Cancel begin!!!!!!!!!");

	return (jint)NE_POIArnd_Cancel();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_SetPosition         	     *
*	Description	:                                            *
*	Date		: 09/12/23                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIArnd_SetPosition
  (JNIEnv *env, jclass a,jlong lon,jlong lat)
{
	return (jlong)NE_POIArnd_SetPosition((long)lon,(long)lat);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_SetDistance         	     *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIArnd_SetDistance
  (JNIEnv *env, jclass a, jlong iMode)
{
	unsigned long mode = iMode;
	//LOGDp("[DEBUG] NE_POIArnd_SetPosition !!!!!!!!!");
	//NE_POIArnd_SetPosition(503167985,128441598);
	return (jlong)NE_POIArnd_SetDistance(mode);
	//return 0;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_SearchList         	     *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIArnd_SearchList
  (JNIEnv *env, jclass a)
{
	//LOGDp("[DEBUG] JNICALL JNI_NE_POIArnd_SearchList begin!!!!!!!!!!");
	return (jlong)NE_POIArnd_SearchList();
}

/*********************************************************************
*	Function Name	: JNI_NE_POI_Around_Clear         	     *
*	Description	:                                            *
*	Date		: 11/01/27                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: wangmeng	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Around_Clear
  (JNIEnv *env, jclass a)
{
	//LOGDp("[DEBUG] JNICALL JNI_NE_POI_Around_Clear begin!!!!!!!!!!");
	return (jlong)NE_POI_Around_Clear();
}
/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_GetRecCount        	     *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIArnd_GetRecCount
  (JNIEnv * env, jclass a, jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount begin!!!!!!!!!!!!");

	long Listconut = 0;
	jlong lRet = (jlong)NE_POIArnd_GetRecCount(&Listconut);

	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount Listconut:%d,lRet:%d!!!!!!!!!!!!",Listconut,lRet);
	jlong listconut = Listconut;
	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount SetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_GetNextTreeIndex            *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIArnd_GetNextTreeIndex
  (JNIEnv * env, jclass a, jlong RecIndex, jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetNextTreeIndex begin!!!!!!!!!!!!!!!!!!!!!");
	unsigned long Treeindex = 0;

	jlong lRet = (jlong)NE_POIArnd_GetNextTreeIndex(RecIndex,&Treeindex);

	jlong treeindex = Treeindex;
	LOGDp("[DEBUG]treeindex is : treeindex = %d", treeindex);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID TreeIndex = env->GetFieldID(clazz,"lcount","J");
		if(TreeIndex)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,TreeIndex,treeindex);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIArnd_GetNextTreeIndex SetLongField TreeIndex Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetNextTreeIndex FindClass  Failed");
	}

	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetNextTreeIndex Sucessful");

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_SearchNextList              *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIArnd_SearchNextList
  (JNIEnv *env, jclass a, jlong RecIndex)
{
	unsigned long Recindex = RecIndex;
	//LOGDp("[DEBUG] JNI_NE_POIArnd_SearchNextList begin");
	return (jlong)NE_POIArnd_SearchNextList(Recindex);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_GetPrevTreeIndex             *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIArnd_GetPrevTreeIndex
  (JNIEnv *env, jclass a, jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetPrevTreeIndex begin");
	unsigned long treeindex = 0;

	//jlong lRet =
	(jlong)NE_POIArnd_GetPrevTreeIndex(&treeindex);

	LOGDp("[DEBUG]treeindex is : treeindex = %d", treeindex);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID TreeIndex = env->GetFieldID(clazz,"lcount","J");
		if(TreeIndex)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,TreeIndex,treeindex);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIArnd_GetPrevTreeIndex SetLongField TreeIndex Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetPrevTreeIndex FindClass  Failed");
	}
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetPrevTreeIndex Sucessful");

	return 0;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_SearchPrevList              *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIArnd_SearchPrevList
  (JNIEnv *env, jclass a)
{
	return (jlong)NE_POIArnd_SearchPrevList();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_GetRecList                  *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIArnd_GetRecList
  (JNIEnv *env, jclass a, jlong RecIndex, jlong RecCount, jobjectArray args, jobject object)
  {
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList begin");
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList RecIndex:%d,RecCount:%d!!!!!!!!!!!!",RecIndex,RecCount);
	long Count;
	long Index = RecIndex;
	long RecNum = RecCount;
	ZPOI_Around_ListItem* pstRecs;

	//jlong Ret =
	(jlong)NE_POIArnd_GetRecList(Index,RecNum,&pstRecs, &Count);

	jlong listconut = Count;
	//Fing Java's Class
	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList Sucessful");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount SetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Around_ListItem");

#if 0
	if(clazz)
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList FindClass Sucessful!!!!!!!!!!!!!!");
	}
#endif
	int i;
	//char recName[512] = {0};
	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}else
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList for 7-error strClass NULL");
	}

	for( i = 0; i < Count; i++ )
	{
		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			jfieldID NameSize = env->GetFieldID(clazz,"NameSize","J");
			if(NameSize)
			{
// Chg 2011/05/23 katsuta Start -->
//				env->SetLongField(obj,NameSize,pstRecs[i].nameSize);
				env->SetLongField(obj,NameSize,pstRecs[i].lnNameSize);
// Chg 2011/05/23 katsuta End <--
				jfieldID Pucname = env->GetFieldID(clazz,"pucName","Ljava/lang/String;");
				if(Pucname)
				{
// Chg 2011/05/23 katsuta Start -->
//					int lenjis = strlen((char*)pstRecs[i].pcName);
					int lenjis = strlen((char*)pstRecs[i].pucName);
// Chg 2011/05/23 katsuta End <--
					jbyteArray bytes = env->NewByteArray(lenjis);
					if(bytes != NULL)
					{
// Chg 2011/05/23 katsuta Start -->
//						env->SetByteArrayRegion(bytes, 0, lenjis, (jbyte*)pstRecs[i].pcName);
						env->SetByteArrayRegion(bytes, 0, lenjis, (jbyte*)pstRecs[i].pucName);
// Chg 2011/05/23 katsuta End <--

					}else{
						LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList for 8 error bytes==NULL");
					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
								env->SetObjectField(obj,Pucname,jsName);
					}else
					{
						LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList error888 jsName== NULL ");
					}

					if(NULL != bytes)
					{
						env->DeleteLocalRef( bytes);
						bytes = NULL;
					}
					if(NULL != encoding)
					{
						env->DeleteLocalRef( encoding);
						encoding = NULL;
					}
					if(NULL != jsName)
					{
						env->DeleteLocalRef( jsName);
						jsName = NULL;
					}
					//delete []jname;
					jfieldID Distance = env->GetFieldID(clazz,"Distance","J");
					if(Distance)
					{
						env->SetLongField(obj,Distance,pstRecs[i].lnDistance);
						jfieldID Latitude = env->GetFieldID(clazz,"Latitude","J");
						if(Latitude)
						{
							env->SetLongField(obj,Latitude,pstRecs[i].ulnLatitude);
							jfieldID Longitude = env->GetFieldID(clazz,"Longitude","J");
							if(Longitude)
							{
								env->SetLongField(obj,Longitude,pstRecs[i].ulnLongitude);
								jfieldID Existenceflg = env->GetFieldID(clazz,"ExistenceFlg","J");
								if(Existenceflg)
								{
// Chg 2011/05/23 katsuta Start -->
//									env->SetLongField(obj,Existenceflg,pstRecs[i].prefCode);
									env->SetLongField(obj,Existenceflg,pstRecs[i].lnExistenceFlg);
// Chg 2011/05/23 katsuta End <--
								}
								else
								{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIArnd_GetRecList GetFieldID Existenceflg Failed");
								}

								jfieldID jKindCode = env->GetFieldID(clazz,"kindeCode","J");

								if(jKindCode){
									env->SetLongField(obj, jKindCode, pstRecs[i].lnKindCode);
								}
								else{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIArnd_GetRecList GetFieldID kindCode Failed");
								}
							}
							else
							{
								LOGDp("[DEBUG] JNICALL JNI_NE_POIArnd_GetRecList GetFieldID Longitude Failed");
							}
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POIArnd_GetRecList GetFieldID Latitude Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList GetFieldID Distance Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList GetFieldID Pucname Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList GetFieldID NameSize Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POIArnd_GetRecList FindClass Failed!");
		}

		if(NULL != obj)
		{
			env->DeleteLocalRef( obj);
			obj = NULL;
		}
	}
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList end");

	return 0;
}
/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_MiddleList_Search                               *
*	Description	:                                                                                       *
*	Date		: 09/10/12                                                                         *
*	Parameter	:                                                                                       *
*	Return Code	: jlong                                                                                *
*	Author		: xiayx	                                                                           *
*  -------------------------------------------------------------------*
* Revision History	                                                                                        *
* No	Date		Revised by		Description                                              *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIArnd_MiddleList_Search
  (JNIEnv *env, jclass a,jlong skey, jlong lPointKey)
{
	return (jlong)NE_POIArnd_MiddleList_Search((unsigned short)skey, (unsigned short)lPointKey);
}
