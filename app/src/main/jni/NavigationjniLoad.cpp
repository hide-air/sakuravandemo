/* //device/libs/android_runtime/android_media_MtvPlayer.cpp */

//#define LOG_NDEBUG 0
#define LOG_TAG "NavigationJNI"
#define NELEM(x) ((int) (sizeof(x) / sizeof((x)[0])))

#include "utils/Log.h"

#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>

#include <jni.h>


#include "JNIMap_Communication.h"
#include "JNIPoiAddr_Communication.h"
#include "JNIPoiFacility_Communication.h"
#include "JNIPoiGnr_Communication.h"
#include "JNIPoiHist_Communication.h"
#include "JNIPoiMyBox_Communication.h"
#include "JNIPoiTel_Communication.h"
#include "JNIPoiZip_Communication.h"
#include "JNIPoiArnd_Communication.h"
#include "JNIPoiRoute_Communication.h"
#include "JNIRoute_Communication.h"
#include "JNIGuide_Communication.h"
#include "JNISet_Communication.h"
#include "JNIMap_DriCon_Communication.h"

//#include "NavigationMouseEventCommon.h"
#include "NavigationSurfaceMgrCommon.h"
#include "NavigationSurfaceMgr.h"

#include <pthread.h>
#include "semaphore.h"
#include "ZDCPND_Dev.h"
#include "JNI_Common.h"
#include "windowsMacroDef.h"
#include "CTTypes.h"
#include "JetGlyph.h"
#include "CTMapDisplay.h"

// ADD.2013.05.10 N.Sasao エンジン共通化対応 START
#include "GRL_Common.h"
#include "DALIF.h"
// ADD.2013.05.10 N.Sasao エンジン共通化対応  END

#include "debugPrint.h"
// ----------------------------------------------------------------------------

struct fields_t
{
    jfieldID    context;
    jmethodID   post_event;

	jmethodID   draw_mag_name;		// Add by qilj for bug 851
};

static fields_t fields;

extern "C" JavaVM* gJavaVM = NULL;

jclass gClazz = NULL;

//static jobject gInterfaceObject;
static  jobject     gRefObject;

static pthread_mutex_t	g_postMutex;
static int iInitialed = 0;

// ADD.2013.05.10 N.Sasao エンジン共通化対応 START
static ZGRL_DRAW_TEXT_SIZE_INFO stDrawTextSizeInfo;
static ZGRL_FONT_TEXTURE_INFO stFontTextureInfo;
static std::vector<char> clAssetFileInfo;
// ADD.2013.05.10 N.Sasao エンジン共通化対応  END

// ----------------------------------------------------------------------------
//static pthread_t g_pThreadHandle = 0;

// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
static int
navi_set_display( JNIEnv *env, jobject thiz, int w, int h)
{
	setDisplay(w, h);

	return 0;
}

static int
navi_init_run( JNIEnv *env, jobject thiz, int w, int h)
{

		setDisplay(w, h);

		initNaviSurfaceMgr();

    	return 0;
}

static int navi_init( JNIEnv *env, jobject thiz, jobject weak_this, int w, int h, float scaleDensity)
{
	pthread_mutexattr_t attr;
	pthread_mutexattr_init (&attr);
	pthread_mutexattr_settype (&attr, PTHREAD_MUTEX_RECURSIVE_NP);

	jclass NaviClass = env->FindClass("net/zmap/android/pnd/v2/data/NaviRun");
	gClazz = (jclass) env->NewGlobalRef(NaviClass);
    gRefObject = env->NewGlobalRef(weak_this);
	pthread_mutex_init (&g_postMutex,&attr);
	pthread_mutexattr_destroy(&attr);

	NE_SetScaleDensity(scaleDensity);

	setDisplay(w, h);

        iInitialed = 1;
        return 0;
}




static void navi_release(JNIEnv *env, jobject thiz)
{
      env->DeleteGlobalRef(gRefObject);

	pthread_mutex_destroy(&g_postMutex);
	return ;
}

// ----------------------------------------------------------------------------

static JNINativeMethod gMethods[] =
{

    {"native_init",              "(Ljava/lang/Object;IIF)I",   (void *)navi_init            },
    {"native_release",           "()V",                     (void *)navi_release         },
    {"native_navi_run",   "(II)I",  (void *)navi_init_run  },
    {"navi_set_display",   "(II)I",  (void *)navi_set_display  },

	{"JNI_NE_POI_Facility_SearchRefine",   "(J)J",  (void *)JNI_NE_POI_Facility_SearchRefine },
	{"JNI_NE_POI_Facility_GetPrefuctureRecListCount",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POI_Facility_GetPrefuctureRecListCount },
	{"JNI_NE_POI_Facility_GetPrefectureRecList",   "(JJ[Lnet/zmap/android/pnd/v2/data/POI_Facility_ListItem;Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_POI_Facility_GetPrefectureRecList },

    {"JNI_NE_POIArnd_Cancel",   "()I",  (void *)JNI_NE_POIArnd_Cancel  },
    {"JNI_NE_POIArnd_SetDistance",   "(J)J",  (void *)JNI_NE_POIArnd_SetDistance  },
    {"JNI_NE_POIArnd_SearchList",   "()J",  (void *)JNI_NE_POIArnd_SearchList  },
    {"JNI_NE_POIArnd_GetRecCount",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIArnd_GetRecCount  },
    {"JNI_NE_POIArnd_GetNextTreeIndex",   "(JLnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIArnd_GetNextTreeIndex  },
    {"JNI_NE_POIArnd_SearchNextList",   "(J)J",  (void *)JNI_NE_POIArnd_SearchNextList  },
    {"JNI_NE_POIArnd_GetPrevTreeIndex",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIArnd_GetPrevTreeIndex  },
    {"JNI_NE_POIArnd_SearchPrevList",   "()J",  (void *)JNI_NE_POIArnd_SearchPrevList  },
    {"JNI_NE_POIArnd_GetRecList",   "(JJ[Lnet/zmap/android/pnd/v2/data/POI_Around_ListItem;Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_POIArnd_GetRecList },
    {"JNI_NE_GetMapHeadingup",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetMapHeadingup  },
    {"JNI_NE_SetMapHeadingup",   "(I)J",  (void *)JNI_NE_SetMapHeadingup  },
    {"JNI_NE_GetMapCenter",   "(Lnet/zmap/android/pnd/v2/data/JNITwoLong;)J",  (void *)JNI_NE_GetMapCenter  },
    {"JNI_LIB_dtmConvByDegree",   "(IIJJLnet/zmap/android/pnd/v2/data/JNITwoLong;)J",  (void *)JNI_LIB_dtmConvByDegree  },
    {"JNI_NE_GetMapLevel",   "(Lnet/zmap/android/pnd/v2/data/JNIByte;)J",  (void *)JNI_NE_GetMapLevel  },
    {"JNI_NE_GetMapLevelScale",   "(Lnet/zmap/android/pnd/v2/data/JNIByte;Lnet/zmap/android/pnd/v2/data/JNIDouble;)J",  (void *)JNI_NE_GetMapLevelScale  },
	{"JNI_CT_GetMapPosition", "(Lnet/zmap/android/pnd/v2/data/MapInfo;)J", (void *)JNI_CT_GetMapPosition},
	{"JNI_CT_GetReqMapPosition", "(Lnet/zmap/android/pnd/v2/data/MapInfo;)J", (void *)JNI_CT_GetReqMapPosition},
    {"JNI_NE_CalcAppLevelScale",   "(BDLnet/zmap/android/pnd/v2/data/JNIByte;Lnet/zmap/android/pnd/v2/data/JNIDouble;)J",  (void *)JNI_NE_CalcAppLevelScale  },
    {"JNI_NE_CalcScale",   "(BDBLnet/zmap/android/pnd/v2/data/JNIDouble;)J",  (void *)JNI_NE_CalcScale  },
    {"JNI_NE_ExposeMap",   "()J",  (void *)JNI_NE_ExposeMap  },
    {"JNI_NE_GetAddressString",   "(JJLnet/zmap/android/pnd/v2/data/JNIString;)J",  (void *)JNI_NE_GetAddressString  },
    {"JNI_NE_GetAddressCode",   "(JJLnet/zmap/android/pnd/v2/data/JNIThreeShort;)J",  (void *)JNI_NE_GetAddressCode  },
    {"JNI_NE_GetMyPosi",   "(Lnet/zmap/android/pnd/v2/data/JNITwoLong;)J",  (void *)JNI_NE_GetMyPosi },
    {"JNI_NE_GetMyPosiEx",   "(Lnet/zmap/android/pnd/v2/data/JNITwoLong;Lnet/zmap/android/pnd/v2/data/JNIShort;)J",  (void *)JNI_NE_GetMyPosiEx },
    {"JNI_NE_AjustMyPosi",   "(JJ)J",  (void *)JNI_NE_AjustMyPosi },
    {"JNI_NE_AjustMyPosiEx",   "(JJSZ)J",  (void *)JNI_NE_AjustMyPosiEx },
    {"JNI_NE_GetNaviMode",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetNaviMode },
    {"JNI_NE_GetNaviCarMode",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetNaviCarMode },
    {"JNI_NE_SetNaviMode",   "(I)J",  (void *)JNI_NE_SetNaviMode },
    {"JNI_NE_ActivateControl",   "(IIBDJJF)J",  (void *)JNI_NE_ActivateControl },
    {"JNI_CT_UIC_RouteFailerProc",   "()J",  (void *)JNI_CT_UIC_RouteFailerProc },
    {"JNI_ct_uic_UserScroll",   "(JJJJ)J",  (void *)JNI_ct_uic_UserScroll },
    {"JNI_ct_uic_MoveMapMyPosi",   "()J",  (void *)JNI_ct_uic_MoveMapMyPosi },
    {"JNI_ct_uic_SetMapLevel",   "(B)J",  (void *)JNI_ct_uic_SetMapLevel },
    {"JNI_ct_uic_MapLevelDown",   "()J",  (void *)JNI_ct_uic_MapLevelDown },
    {"JNI_ct_uic_MapLevelUp",   "()J",  (void *)JNI_ct_uic_MapLevelUp },
    {"JNI_CT_UIC_SetNUIModeBeforeSmoothScroll",   "(I)J",  (void *)JNI_CT_UIC_SetNUIModeBeforeSmoothScroll },

      {"JNI_CT_UIC_SmoothScrollMap",   "()J",  (void *)JNI_CT_UIC_SmoothScrollMap },
      {"JNI_CT_UIC_StopSmoothScroll",   "()J",  (void *)JNI_CT_UIC_StopSmoothScroll },
      {"JNI_NE_ForceRedraw",   "()J",  (void *)JNI_NE_ForceRedraw },
      {"JNI_ct_OnMPCommand",   "(JJ)J",  (void *)JNI_ct_OnMPCommand },
      {"JNI_ct_OnNUICommand",   "(JJ)J",  (void *)JNI_ct_OnNUICommand },
      {"JNI_CT_ReqRedraw",   "()J",  (void *)JNI_CT_ReqRedraw },
// Chg 2012/03/17 Z01_sawada(No.334) Start --> ref 2697
//    {"JNI_ct_uic_RouteCalculate",   "()J",  (void *)JNI_ct_uic_RouteCalculate },
      {"JNI_ct_uic_RouteCalculate",   "(ILjava/lang/String;)J",  (void *)JNI_ct_uic_RouteCalculate },
// Chg 2012/03/17 Z01_sawada(No.334) End <-- ref 2697
      {"JNI_ct_uic_5RouteCalculate",   "()J",  (void *)JNI_ct_uic_5RouteCalculate },
      {"JNI_ct_uic_RouteView",   "(I)J",  (void *)JNI_ct_uic_RouteView },
      {"JNI_ct_OnDGCommand",   "(JJ)J",  (void *)JNI_ct_OnDGCommand },
      {"JNI_ct_OnWGCommand",   "(JJ)J",  (void *)JNI_ct_OnWGCommand },
      {"JNI_NE_POITel_Clear",   "()J",  (void *)JNI_NE_POITel_Clear },
      {"JNI_NE_POITel_SearchList",   "(Ljava/lang/String;)J",  (void *)JNI_NE_POITel_SearchList },
      {"JNI_NE_POITel_GetRecCount",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POITel_GetRecCount },
      {"JNI_NE_POITel_GetRecList",   "(JJ[Lnet/zmap/android/pnd/v2/data/POI_Tel_ListItem;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POITel_GetRecList },
      {"JNI_NE_POITel_Cancel",   "()J",  (void *)JNI_NE_POITel_Cancel },

      {"JNI_NE_POIZip_Clear",   "()J",  (void *)JNI_NE_POIZip_Clear },
      {"JNI_NE_POIZip_SearchList",   "(Ljava/lang/String;)J",  (void *)JNI_NE_POIZip_SearchList },
      {"JNI_NE_POIZip_GetRecCount",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIZip_GetRecCount },
      {"JNI_NE_POIZip_GetRecList",   "(JJ[Lnet/zmap/android/pnd/v2/data/POI_ZipCode_ListItem;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIZip_GetRecList },
      {"JNI_NE_POIZip_Cancel",   "()J",  (void *)JNI_NE_POIZip_Cancel },

      {"JNI_NE_POIFacility_Clear",   "()J",  (void *)JNI_NE_POIFacility_Clear },
      {"JNI_NE_POIFacility_SearchList",   "(Ljava/lang/String;)J",  (void *)JNI_NE_POIFacility_SearchList },
      {"JNI_NE_POIFacility_GetRecCount",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIFacility_GetRecCount },
      {"JNI_NE_POIFacility_GetMatchCount",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIFacility_GetMatchCount },
      {"JNI_NE_POIFacility_GetRecList",   "(JJ[Lnet/zmap/android/pnd/v2/data/POI_Facility_ListItem;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIFacility_GetRecList },
      {"JNI_NE_POIFacility_Cancel",   "()J",  (void *)JNI_NE_POIFacility_Cancel },

      {"JNI_NE_POI_Facility_GetPointList",   "(JJ)J",  (void *)JNI_NE_POI_Facility_GetPointList },
      {"JNI_NE_POI_Facility_GetPointListCount",   "(JLnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POI_Facility_GetPointListCount },
      {"JNI_NE_POI_Facility_GetPointRecList",   "(JJJ[Lnet/zmap/android/pnd/v2/data/POI_Facility_ListItem;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POI_Facility_GetPointRecList },

      {"JNI_NE_POIGnr_Clear",   "()J",  (void *)JNI_NE_POIGnr_Clear },
      {"JNI_NE_POIGnr_SearchNextList",   "(J)J",  (void *)JNI_NE_POIGnr_SearchNextList },
      {"JNI_NE_POIGnr_SearchPrevList",   "()J",  (void *)JNI_NE_POIGnr_SearchPrevList },
      {"JNI_NE_POIGnr_GetNextTreeIndex",   "(JLnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIGnr_GetNextTreeIndex },
      {"JNI_NE_POIGnr_GetPrevTreeIndex",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIGnr_GetPrevTreeIndex },
      {"JNI_NE_POIGnr_GetJumpList",   "([Lnet/zmap/android/pnd/v2/data/JNIJumpKey;Lnet/zmap/android/pnd/v2/data/JNIShort;)J",  (void *)JNI_NE_POIGnr_GetJumpList },
      {"JNI_NE_POIGnr_GetJumpList2RecordIndex",   "(JLjava/lang/String;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIGnr_GetJumpList2RecordIndex },
      {"JNI_NE_POIGnr_GetProgressRate",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIGnr_GetProgressRate },
      {"JNI_NE_POIGnr_GetRecCount",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIGnr_GetRecCount },
      {"JNI_NE_POIGnr_GetRecList",   "(JJ[Lnet/zmap/android/pnd/v2/data/POI_Genre_ListItem;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIGnr_GetRecList },
      {"JNI_NE_POIGnr_SetAddrKey",   "(SSS)J",  (void *)JNI_NE_POIGnr_SetAddrKey },
      {"JNI_NE_POIGnr_Abort",   "()J",  (void *)JNI_NE_POIGnr_Abort },
      {"JNI_NE_POIGnr_AddrIndex",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIGnr_AddrIndex },

      {"JNI_NE_POIAddrKey_Clear",   "()J",  (void *)JNI_NE_POIAddrKey_Clear },
      {"JNI_NE_POIAddr_Clear",   "()J",  (void *)JNI_NE_POIAddr_Clear },
      {"JNI_NE_POIAddr_SearchNextList",   "(J)J",  (void *)JNI_NE_POIAddr_SearchNextList },
      {"JNI_NE_POIAddr_SearchPrevList",   "()J",  (void *)JNI_NE_POIAddr_SearchPrevList },
      {"JNI_NE_POIAddr_GetNextTreeIndex",   "(JLnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIAddr_GetNextTreeIndex },
      {"JNI_NE_POIAddr_GetPrevTreeIndex",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIAddr_GetPrevTreeIndex },
      {"JNI_NE_POIAddr_GetJumpList",   "([Lnet/zmap/android/pnd/v2/data/JNIJumpKey;Lnet/zmap/android/pnd/v2/data/JNIShort;)J",  (void *)JNI_NE_POIAddr_GetJumpList },
      {"JNI_NE_POIAddr_GetJumpList2RecordIndex",   "(JLjava/lang/String;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIAddr_GetJumpList2RecordIndex },
      {"JNI_NE_POIAddr_GetProgressRate",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIAddr_GetProgressRate },
      {"JNI_NE_POIAddr_GetRecCount",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIAddr_GetRecCount },
      {"JNI_NE_POIAddr_GetRecList",   "(JJ[Lnet/zmap/android/pnd/v2/data/POI_Address_ListItem;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIAddr_GetRecList },
      {"JNI_NE_POIAddr_SetAddrKey",   "(SSS)J",  (void *)JNI_NE_POIAddr_SetAddrKey },
      {"JNI_NE_POIAddr_Abort",   "()J",  (void *)JNI_NE_POIAddr_Abort },
      {"JNI_NE_POIAddr_AddrIndex",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIAddr_AddrIndex },

      {"JNI_NE_POIHist_Reset",   "()J",  (void *)JNI_NE_POIHist_Reset },
      {"JNI_NE_POIHist_Clear",   "(Lnet/zmap/android/pnd/v2/data/POI_History_FilePath;)J",  (void *)JNI_NE_POIHist_Clear },
      {"JNI_NE_POIHist_GetRecCount",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIHist_GetRecCount },
      {"JNI_NE_POIHist_RemoveRec",   "(J)J",  (void *)JNI_NE_POIHist_RemoveRec },
      {"JNI_NE_POIHist_GetRecList",   "(JJ[Lnet/zmap/android/pnd/v2/data/POI_UserFile_RecordIF;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIHist_GetRecList},
      {"JNI_NE_POIHist_GetListDate",   "(JJLnet/zmap/android/pnd/v2/data/JNILong;[Lnet/zmap/android/pnd/v2/data/POI_History_ListDate;)J",  (void *)JNI_NE_POIHist_GetListDate },
      {"JNI_NE_POIHist_GetDateRecCount",   "(Lnet/zmap/android/pnd/v2/data/JNILong;ILnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIHist_GetDateRecCount },
      {"JNI_NE_POIHist_GetDateRec",   "(JJLnet/zmap/android/pnd/v2/data/JNILong;I[Lnet/zmap/android/pnd/v2/data/POI_UserFile_RecordIF;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIHist_GetDateRec },
      {"JNI_NE_POIHist_RemoveItemDate",   "(Lnet/zmap/android/pnd/v2/data/JNILong;IJ)J",  (void *)JNI_NE_POIHist_RemoveItemDate },
      {"JNI_NE_POIHist_SetRec",   "(Lnet/zmap/android/pnd/v2/data/POI_UserFile_RecordIF;JLnet/zmap/android/pnd/v2/data/POI_UserFile_DataIF;)J",  (void *)JNI_NE_POIHist_SetRec },

      {"JNI_NE_POIMybox_Reset",   "()J",  (void *)JNI_NE_POIMybox_Reset },
      {"JNI_NE_POIMybox_Clear",   "()J",  (void *)JNI_NE_POIMybox_Clear },
      {"JNI_NE_POIMybox_GetGenreCount",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIMybox_GetGenreCount },
      {"JNI_NE_POIMybox_RemoveRec",   "(JJ)J",  (void *)JNI_NE_POIMybox_RemoveRec },
      {"JNI_NE_POIMybox_GetRecList",   "(JJJ[Lnet/zmap/android/pnd/v2/data/POI_UserFile_RecordIF;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIMybox_GetRecList },
      {"JNI_NE_POIMybox_GetRecName",   "(JLnet/zmap/android/pnd/v2/data/POI_Mybox_ListGenreName;)J",  (void *)JNI_NE_POIMybox_GetRecName },
      {"JNI_NE_POIMybox_GetAroundRecCount",   "(ILnet/zmap/android/pnd/v2/data/POI_Mybox_ListGenreCount;)J",  (void *)JNI_NE_POIMybox_GetAroundRecCount },
      {"JNI_NE_POIMybox_GetRecCount",   "(JLnet/zmap/android/pnd/v2/data/POI_Mybox_ListGenreCount;)J",  (void *)JNI_NE_POIMybox_GetRecCount },
      {"JNI_NE_POIMybox_RemoveGenreAllRec",   "(J)J",  (void *)JNI_NE_POIMybox_RemoveGenreAllRec },
      {"JNI_NE_POIMybox_SetRec",   "(JLnet/zmap/android/pnd/v2/data/POI_UserFile_RecordIF;JLnet/zmap/android/pnd/v2/data/POI_UserFile_DataIF;)J",  (void *)JNI_NE_POIMybox_SetRec },
      {"JNI_NE_POIMybox_GetAroundListItem",   "(JJJ[Lnet/zmap/android/pnd/v2/data/POI_UserFile_AroundRec;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIMybox_GetAroundListItem },
      {"JNI_NE_POIMybox_GetAroundList",   "(JJ)J",  (void *)JNI_NE_POIMybox_GetAroundList },

      {"JNI_ct_uic_StartGuide",   "(J)J",  (void *)JNI_ct_uic_StartGuide },
      {"JNI_ct_uic_RouteCalcCancel",   "()J",  (void *)JNI_ct_uic_RouteCalcCancel },
      {"JNI_ct_uic_RouteCalcAbort",   "(I)J",  (void *)JNI_ct_uic_RouteCalcAbort },
      {"JNI_ct_uic_StopGuide",   "()J",  (void *)JNI_ct_uic_StopGuide },
      {"JNI_ct_uic_SuspendGuide",   "()J",  (void *)JNI_ct_uic_SuspendGuide },
      {"JNI_ct_uic_ResumeGuide",   "()J",  (void *)JNI_ct_uic_ResumeGuide },
      {"JNI_NE_StopGuidance",   "()J",  (void *)JNI_NE_StopGuidance },
      {"JNI_ct_uic_RefreshHighwayGuide",   "()J",  (void *)JNI_ct_uic_RefreshHighwayGuide },
      {"JNI_CT_UIC_SetScreenCoordinate",   "(JJ)J",  (void *)JNI_CT_UIC_SetScreenCoordinate },

      {"JNI_NE_SetOPSound",   "(I)J",  (void *)JNI_NE_SetOPSound  },
      {"JNI_NE_GetOPSound",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetOPSound  },
      {"JNI_NE_SetGuideVoice",   "(I)J",  (void *)JNI_NE_SetGuideVoice  },
      {"JNI_NE_GetGuideVoice",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetGuideVoice  },
      {"JNI_NE_IsExternalGPS",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_IsExternalGPS  },
      {"JNI_NE_GetGPSPortNo",   "(Lnet/zmap/android/pnd/v2/data/JNIString;)J",  (void *)JNI_NE_GetGPSPortNo  },
      {"JNI_NE_UpdateGPSPortNo",   "(Ljava/lang/String;)J",  (void *)JNI_NE_UpdateGPSPortNo  },
      {"JNI_NE_GetDBDiskTitle",   "(Lnet/zmap/android/pnd/v2/data/JNIString;)J",  (void *)JNI_NE_GetDBDiskTitle  },
      {"JNI_NE_SetExternalGPS",   "(I)J",  (void *)JNI_NE_SetExternalGPS  },
      {"JNI_NE_Drv_GetCarType",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_Drv_GetCarType },
      {"JNI_NE_Drv_SetCarType",   "(I)J",  (void *)JNI_NE_Drv_SetCarType  },
      {"JNI_NE_Drv_GetDefaultSearchProperty",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_Drv_GetDefaultSearchProperty},
      {"JNI_NE_Drv_SetDefaultSearchProperty",   "(I)J",  (void *)JNI_NE_Drv_SetDefaultSearchProperty  },
      {"JNI_NE_Drv_GetComplexProperty",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_Drv_GetComplexProperty },
      {"JNI_NE_Drv_SetComplexProperty",   "(I)J",  (void *)JNI_NE_Drv_SetComplexProperty  },
//Add 2011/09/30 Z01yamaguchi Start -->
      {"JNI_NE_GetVicsInfo",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetVicsInfo },
      {"JNI_NE_SetVicsInfo",   "(I)J",  (void *)JNI_NE_SetVicsInfo  },
//Add 2011/09/30 Z01yamaguchi End <--
      {"JNI_NE_Man_GetDefaultSearchProperty",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_Man_GetDefaultSearchProperty },
      {"JNI_NE_Man_SetDefaultSearchProperty",   "(I)J",  (void *)JNI_NE_Man_SetDefaultSearchProperty  },
      {"JNI_NE_GetMapFrontWide",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetMapFrontWide  },
      {"JNI_NE_SetMapFrontWide",   "(I)J",  (void *)JNI_NE_SetMapFrontWide  },
      {"JNI_NE_GetMapIconSize",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetMapIconSize  },
      {"JNI_NE_SetMapIconSize",   "(I)J",  (void *)JNI_NE_SetMapIconSize  },
      {"JNI_NE_GetMapWordSize",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetMapWordSize  },
      {"JNI_NE_SetMapWordSize",   "(I)J",  (void *)JNI_NE_SetMapWordSize  },
      {"JNI_NE_GetDispMapOneway",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetDispMapOneway  },
      {"JNI_NE_SetDispMapOneway",   "(I)J",  (void *)JNI_NE_SetDispMapOneway  },
      {"JNI_NE_GetMapDParaMode",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetMapDParaMode  },
      {"JNI_NE_SetMapDParaMode",   "(I)J",  (void *)JNI_NE_SetMapDParaMode  },
      {"JNI_NE_SetMapScale",   "(I)J",  (void *)JNI_NE_SetMapScale  },
      {"JNI_DG_startDemoDrive",   "(I)J",  (void *)JNI_DG_startDemoDrive  },
       {"JNI_DG_endDemoDrive",   "(I)J",  (void *)JNI_DG_endDemoDrive  },
      {"JNI_NE_GetMapScale",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetMapScale  },
      {"JNI_NE_SetMPMode",   "(J)J",  (void *)JNI_NE_SetMPMode  },
      {"JNI_NE_GetMPMode",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_GetMPMode  },

      {"JNI_Java_GetDestination",   "(Lnet/zmap/android/pnd/v2/data/ZNUI_DESTINATION_DATA;)J",  (void *)JNI_Java_GetDestination },
      {"JNI_Java_GetGuideRoutePoint",   "(Lnet/zmap/android/pnd/v2/data/ZNE_GuideRoutePoint;)J",  (void *)JNI_Java_GetGuideRoutePoint },
      {"JNI_DG_GetNextHighwayGuideInfoIdx",   "(Lnet/zmap/android/pnd/v2/data/JNITwoLong;)J",  (void *)JNI_DG_GetNextHighwayGuideInfoIdx },
      {"JNI_DG_GetHighwayGuideListNum",   "(JLnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_DG_GetHighwayGuideListNum },
      {"JNI_DG_HighwayGuideInfo_new",   "(JJLnet/zmap/android/pnd/v2/data/ZDGHighwayGuideInfo;)J",  (void *)JNI_DG_HighwayGuideInfo_new },
      {"JNI_DG_HighwayGuideInfo_free",   "(JJLnet/zmap/android/pnd/v2/data/ZDGHighwayGuideInfo;)J",  (void *)JNI_DG_HighwayGuideInfo_free },
      {"JNI_Java_GetGuideSts",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_Java_GetGuideSts },
      {"JNI_Java_GetSimpleGuideAt",   "(Lnet/zmap/android/pnd/v2/data/JNIThreeLong;)J",  (void *)JNI_Java_GetSimpleGuideAt },
      {"JNI_DG_GetNextGuidancePointInfo",   "(Lnet/zmap/android/pnd/v2/data/JNIThreeLong;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_DG_GetNextGuidancePointInfo },
      {"JNI_DG_IsHighwayGuideReady",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_DG_IsHighwayGuideReady },
      {"JNI_DG_IsVehicleOnHighway",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_DG_IsVehicleOnHighway },
      {"JNI_Java_GetGuide",   "(Lnet/zmap/android/pnd/v2/data/Java_Guide;)J",  (void *)JNI_Java_GetGuide },
      {"JNI_Java_GetJCTDirectionInfo",   "(Lnet/zmap/android/pnd/v2/data/JNITwoInt;)J",  (void *)JNI_Java_GetJCTDirectionInfo },
      {"JNI_Java_GetRestDistance",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_Java_GetRestDistance },
      {"JNI_Java_GetShowGuide",   "(Lnet/zmap/android/pnd/v2/data/JNITwoInt;)J",  (void *)JNI_Java_GetShowGuide },
      {"JNI_Java_GetLaneGuideInfo",   "(Lnet/zmap/android/pnd/v2/data/ZNE_LaneGuideInfo;)J",  (void *)JNI_Java_GetLaneGuideInfo },
      {"JNI_Java_GetEventInfo",   "(Lnet/zmap/android/pnd/v2/data/JNIThreeLong;)J",  (void *)JNI_Java_GetEventInfo },
      {"JNI_Java_GetCompass",   "(Lnet/zmap/android/pnd/v2/data/Java_IconInfo;)J",  (void *)JNI_Java_GetCompass },
      {"JNI_Java_GetLaneRestDist",   "(Lnet/zmap/android/pnd/v2/data/Java_Lane;)J",  (void *)JNI_Java_GetLaneRestDist },
      {"JNI_Java_GetGuidePicture",   "(Lnet/zmap/android/pnd/v2/data/ZNUI_IMAGE;)J",  (void *)JNI_Java_GetGuidePicture },
      {"JNI_DAL_GetLandmarkData",   "(IILnet/zmap/android/pnd/v2/data/ZLandmarkData_t;)J",  (void *)JNI_DAL_GetLandmarkData },
      {"JNI_Java_GetKindInfo",   "(Lnet/zmap/android/pnd/v2/data/Java_KINDDATA;)J",  (void *)JNI_Java_GetKindInfo },
      {"JNI_Java_ct_SwitchNaviMode",   "()J",  (void *)JNI_Java_ct_SwitchNaviMode },
      {"JNI_Java_ChangeSettingMode",   "(II)J",  (void *)JNI_Java_ChangeSettingMode },
      {"JNI_Java_GetGuideState",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_Java_GetGuideState },
      {"JNI_Java_GetNaviMode",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_Java_GetNaviMode },
      {"JNI_Java_GetShowRoute",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_Java_GetShowRoute },
      {"JNI_NE_GetSatelliteInfo",   "(Lnet/zmap/android/pnd/v2/data/ZNE_SatelliteInfo;)J",  (void *)JNI_NE_GetSatelliteInfo },
      {"JNI_CMN_cos",   "(SLnet/zmap/android/pnd/v2/data/JNIFloat;)J",  (void *)JNI_CMN_cos },
      {"JNI_CMN_sin",   "(SLnet/zmap/android/pnd/v2/data/JNIFloat;)J",  (void *)JNI_CMN_sin },
      {"JNI_DG_getVehiclePosInfo",   "(Lnet/zmap/android/pnd/v2/data/ZNE_VehicleInfo_t;)J",  (void *)JNI_DG_getVehiclePosInfo },
      {"JNI_DG_GetRouteInfo",   "(Lnet/zmap/android/pnd/v2/data/ZNE_RouteInfo_t;)J",  (void *)JNI_DG_GetRouteInfo },
      {"JNI_DG_GetGuidePointInfo",   "(Lnet/zmap/android/pnd/v2/data/ZNE_RouteGuidance_t;)J",  (void *)JNI_DG_GetGuidePointInfo },
      {"JNI_NE_GetRoutePoint",   "(ILnet/zmap/android/pnd/v2/data/JNITwoLong;Lnet/zmap/android/pnd/v2/data/JNIString;Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetRoutePoint },
      {"JNI_NE_SetRoutePoint",   "(IJJLjava/lang/String;I)J",  (void *)JNI_NE_SetRoutePoint },
      {"JNI_Java_GetDestinationGuide",   "(Lnet/zmap/android/pnd/v2/data/ZNUI_UI_MARK_DATA;)J",  (void *)JNI_Java_GetDestinationGuide },
      {"JNI_Java_GetSetDestinationGuide",   "(Lnet/zmap/android/pnd/v2/data/ZNUI_UI_MARK_DATA;)J",  (void *)JNI_Java_GetSetDestinationGuide },
      {"JNI_Java_GetShowDestinationGuide",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_Java_GetShowDestinationGuide },
      {"JNI_Java_GetShowInformation",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_Java_GetShowInformation },
      {"JNI_Java_GetInformationInfo",   "(Lnet/zmap/android/pnd/v2/data/JNIString;)J",  (void *)JNI_Java_GetInformationInfo },
      {"JNI_CT_ICU_ReqRefleshUI",   "(I)J",  (void *)JNI_CT_ICU_ReqRefleshUI },
      {"JNI_CT_ICU_ReqRefleshMapUI",   "(I)J",  (void *)JNI_CT_ICU_ReqRefleshMapUI },
      {"JNI_Java_GetScaleDistance",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_Java_GetScaleDistance },
      {"JNI_Java_GetName",   "(Lnet/zmap/android/pnd/v2/data/JNIString;)J",  (void *)JNI_Java_GetName },
      {"JNI_CT_ICU_SetDistination",   "(JJ)J",  (void *)JNI_CT_ICU_SetDistination },
      {"JNI_NE_Drv_SetSectionSearchProperty",   "(III)J",  (void *)JNI_NE_Drv_SetSectionSearchProperty },
      {"JNI_NE_Man_SetPointSearchProperty",   "(II)J",  (void *)JNI_NE_Man_SetPointSearchProperty },
      {"JNI_ct_ExposeMap",   "()J",  (void *)JNI_ct_ExposeMap },
      {"JNI_CT_ExposeMap",   "()J",  (void *)JNI_CT_ExposeMap },
      {"JNI_CT_Navi_StartCalculate",   "(I)J",  (void *)JNI_CT_Navi_StartCalculate },
      {"JNI_NE_ClearCalculate",   "()J",  (void *)JNI_NE_ClearCalculate },
      {"JNI_NE_DeleteRoutePoint",   "(I)J",  (void *)JNI_NE_DeleteRoutePoint },
      {"JNI_CT_Man_WG_getRouteInfo",   "(Lnet/zmap/android/pnd/v2/data/ZNE_RouteInfo_t;)J",  (void *)JNI_CT_Man_WG_getRouteInfo },
      {"JNI_Java_GetLaneRestDistance",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_Java_GetLaneRestDistance },
      {"JNI_Java_GetMagMapRestDistance",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_Java_GetMagMapRestDistance },
      {"JNI_Java_GetJCTImagRestDistance",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_Java_GetJCTImagRestDistance },
      {"JNI_java_GetRouteCalcDistance",   "(Lnet/zmap/android/pnd/v2/data/ROUTE_DISTANCE;)J",  (void *)JNI_java_GetRouteCalcDistance },
      {"JNI_DG_GetCalculateStat",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_DG_GetCalculateStat },
      {"JNI_java_GetShowDetailedMessage",   "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_java_GetShowDetailedMessage },
      {"JNI_Java_GetMagMapCompass",   "(Lnet/zmap/android/pnd/v2/data/Java_IconInfo;)J",  (void *)JNI_Java_GetMagMapCompass },
//Chg 2011/07/25 Z01thedoanh (自由解像度対応: Copy or Modify from MarketV1) Start -->
// MOD.2013.08.22 N.Sasao エンジン共通化対応2 START
      //{"JNI_InitInstance",   "()J",  (void *)JNI_InitInstance },
//@@MOD-START BB-0003 2012/10/19 Y.Hayashida
// MOD.2013.05.10 N.Sasao エンジン共通化対応 START
//      {"JNI_InitInstance",   "(IIIIIIIIIIIIIIILjava/lang/String;)J",  (void *)JNI_InitInstance },
      {"JNI_InitInstance",   "(IIIIIIIIIIIIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)J",  (void *)JNI_InitInstance },
//      {"JNI_InitInstance",   "(IIIIIIIIIIIIIII)J",  (void *)JNI_InitInstance },
// MOD.2013.05.10 N.Sasao エンジン共通化対応  END
//@@MOD-END BB-0003 2012/10/19 Y.Hayashida
// MOD.2013.08.22 N.Sasao エンジン共通化対応2  END
      {"JNI_NE_SetAreaShowCalcRoute",   "(IIIIIIII)V",  (void *)JNI_NE_SetAreaShowCalcRoute },
//Chg 2011/07/25 Z01thedoanh (自由解像度対応: Copy or Modify from MarketV1) End <--
      {"JNI_NE_Finalize",   "()J",  (void *)JNI_NE_Finalize },
      {"JNI_saveConfig",   "()J",  (void *)JNI_saveConfig },
      {"JNI_NE_POIArnd_SetPosition",   "(JJ)J",  (void *)JNI_NE_POIArnd_SetPosition },
      {"JNI_VP_NMEASetGPSStat",   "(I)J",  (void *)JNI_VP_NMEASetGPSStat },
      {"JNI_VP_NMEASetGPSSensorData",   "(Lnet/zmap/android/pnd/v2/data/GPSSensorData;)J",  (void *)JNI_VP_NMEASetGPSSensorData },
      {"JNI_VP_NMEASetSatellitesInfo",   "(Lnet/zmap/android/pnd/v2/data/ZVP_SatInfoList_t;[Lnet/zmap/android/pnd/v2/data/ZVP_SatInfo_t;I)J",  (void *)JNI_VP_NMEASetSatellitesInfo },
      {"JNI_VP_GetLoadMode",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_VP_GetLoadMode },
       {"JNI_VP_IsOutputLog",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_VP_IsOutputLog },
       {"JNI_CT_MD_ConvWinPos2LonLat",   "(JJLnet/zmap/android/pnd/v2/data/JNILong;Lnet/zmap/android/pnd/v2/data/JNILong;B)J",  (void *)JNI_CT_MD_ConvWinPos2LonLat },
       {"JNI_CT_MD_ConvLonLat2WinPos",   "(JJLnet/zmap/android/pnd/v2/data/JNILong;Lnet/zmap/android/pnd/v2/data/JNILong;B)J",  (void *)JNI_CT_MD_ConvLonLat2WinPos },
       {"JNI_CT_DrawMap",   "([IFZZ)J",  (void *)JNI_CT_DrawMap },
       {"JNI_CT_DrawMagMap",   "([I)J",  (void *)JNI_CT_DrawMagMap },
       {"JNI_CT_MapAnimating",   "()Z",  (void *)JNI_CT_MapAnimating },
       {"JNI_CT_GetMapBirdviewAngle",   "()F",  (void *)JNI_CT_GetMapBirdviewAngle },
		{"JNI_CT_GLResourceClear",   "()J",  (void *)JNI_CT_GLResourceClear },
#ifdef DRAW_POILOG
	  {"JNI_MP_DM_SetCSVInfo",   "(JJ)J",  (void *)JNI_MP_DM_SetCSVInfo },
#endif
//Add 2011/08/16 Z01thedoanh Start -->
	  {"JNI_MP_OutSidePOIIcon_GetIconData",   "(IILnet/zmap/android/pnd/v2/data/ZLandmarkData_t;)J",  (void *)JNI_MP_OutSidePOIIcon_GetIconData },
//Add 2011/08/16 Z01thedoanh End <--
 	  {"JNI_Java_GetShowPOIIconInformation",  "(Lnet/zmap/android/pnd/v2/data/PoiIconInfo;)J", (void *)JNI_Java_GetShowPOIIconInformation},
//Add 2011/09/19 Z01yamaguchi Start -->
// MOD.2013.06.12 N.Sasao POI情報リスト表示対応 START
      {"JNI_JAVA_GetDriConPOIIconInformationState",  "(Lnet/zmap/android/pnd/v2/data/JNIInt;Lnet/zmap/android/pnd/v2/data/JNIInt;)J", (void *)JNI_JAVA_GetDriConPOIIconInformationState},
      {"JNI_JAVA_GetDriConPOIIconInformation",  "(I[Lnet/zmap/android/pnd/v2/data/DC_POIInfoShowList;)J", (void *)JNI_JAVA_GetDriConPOIIconInformation},
// ADD.2013.07.02 N.Sasao POI情報リスト表示負荷低減3 START
      {"JNI_JAVA_GetIsDriConShow",	 "()Z",  (void *)JNI_JAVA_GetIsDriConShow },
// ADD.2013.07.02 N.Sasao POI情報リスト表示負荷低減3  END
// MOD.2013.06.12 N.Sasao POI情報リスト表示対応  END
//Add 2011/09/19 Z01yamaguchi End <--
      {"JNI_NE_GetGuideStat",  "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J", (void *)JNI_NE_GetGuideStat},
       {"JNI_CT_Drv_GetVehiclePosition",   "(Lnet/zmap/android/pnd/v2/data/JNITwoLong;)J",  (void *)JNI_CT_Drv_GetVehiclePosition },
// Add 2011/07/12 sawada [AgentHMI] Start -->
      {"JNI_CT_Drv_GetVehiclePosition2",   "(Lnet/zmap/android/pnd/v2/data/JNITwoLong;Lnet/zmap/android/pnd/v2/data/JNILong;Lnet/zmap/android/pnd/v2/data/JNIShort;)J",  (void *)JNI_CT_Drv_GetVehiclePosition2 },
// Add 2011/07/12 sawada [AgentHMI] End   <--
       {"JNI_Set_OutPutUsedtime_Ctrl",   "(J)J",  (void *)JNI_Set_OutPutUsedtime_Ctrl  },
      {"JNI_SetFilePathByType",  "(JLjava/lang/String;)J", (void *)JNI_SetFilePathByType},
//	  {"JNI_MapRotate",  "(J)V", (void *)JNI_MapRotate},
//	  {"JNI_SetVolume",  "(JJ)V", (void *)JNI_SetVolume},
	  {"JNI_SetVolumeScale",  "(J)V", (void *)JNI_SetVolumeScale},
	 {"JNI_GetVolumeScale",  "(Lnet/zmap/android/pnd/v2/data/JNILong;)V", (void *)JNI_GetVolumeScale},
	{"JNI_SetVolumeCtrl",  "(JJJ)V", (void *)JNI_SetVolumeCtrl},
	{"JNI_NE_POIRoute_SetRoute",  "()J", (void *)JNI_NE_POIRoute_SetRoute},
	{"JNI_NE_POIRoute_SetParam",  "(Lnet/zmap/android/pnd/v2/data/ZPOI_Route_Param;)J", (void *)JNI_NE_POIRoute_SetParam},
	{"JNI_NE_POIRouteMybox_SetRoute",  "(I)J", (void *)JNI_NE_POIRouteMybox_SetRoute},
	{"JNI_NE_POIRouteMybox_SetParam",  "(Lnet/zmap/android/pnd/v2/data/ZPOI_Route_Param;)J", (void *)JNI_NE_POIRouteMybox_SetParam},
	{"JNI_NE_POIRoute_SearchList",  "()J", (void *)JNI_NE_POIRoute_SearchList},
	{"JNI_NE_POIRoute_GetRecCount",  "(Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_POIRoute_GetRecCount},
	{"JNI_NE_POIRoute_GetNextTreeIndex",  "(JLnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_POIRoute_GetNextTreeIndex},
	{"JNI_NE_POIRoute_SearchNextList",  "(J)J", (void *)JNI_NE_POIRoute_SearchNextList},
	{"JNI_NE_POIRoute_GetPrevTreeIndex",  "(Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_POIRoute_GetPrevTreeIndex},
	{"JNI_NE_POIRoute_SearchPrevList",  "()J", (void *)JNI_NE_POIRoute_SearchPrevList},
	{"JNI_NE_POIRoute_Cancel",  "()J", (void *)JNI_NE_POIRoute_Cancel},
	{"JNI_NE_POIRoute_GetRecList",  "(JJ[Lnet/zmap/android/pnd/v2/data/ZPOI_Route_ListItem;Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_POIRoute_GetRecList},
	{"JNI_NE_POI_Facility_GetMypositon",  "(JLnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_POI_Facility_GetMypositon},
	{"JNI_NE_POI_Facility_SetAddressKeyCode",  "(SSS)J", (void *)JNI_NE_POI_Facility_SetAddressKeyCode},
	{"JNI_NE_SetShowVehicleTrackFlag",  "(J)J", (void *)JNI_NE_SetShowVehicleTrackFlag},
	{"JNI_NE_GetShowVehicleTrackFlag",  "(Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_GetShowVehicleTrackFlag},
	{"JNI_NE_ClearVehicleTrack",  "()J", (void *)JNI_NE_ClearVehicleTrack},
// Add 20151204 Start
	{"JNI_NE_SaveVehicleTrack",  "()J", (void *)JNI_NE_SaveVehicleTrack},
	{"JNI_NE_AgainOpenVehicleTrack",  "()J", (void *)JNI_NE_AgainOpenVehicleTrack},
	{"JNI_NE_RestartOpenVehicleTrack",  "(Ljava/lang/String;)J", (void *)JNI_NE_RestartOpenVehicleTrack},//ファイルパス設定
	{"JNI_NE_OptionalVTFinalize",  "()J", (void *)JNI_NE_OptionalVTFinalize},
	{"JNI_NE_VT_Initialize_Cust",  "(Ljava/lang/String;)J", (void *)JNI_NE_VT_Initialize_Cust},
// Add 20151204 End
	{"JNI_NE_SetVehicleTrackParam",  "(JJ)J", (void *)JNI_NE_SetVehicleTrackParam},
// Add by CPJsunagawa '2015-07-26 Start
	{"JNI_NE_SetVehicleTrackCourse",  "(Ljava/lang/String;)J", (void *)JNI_NE_SetVehicleTrackCourse},//コース名称設定
// Add by CPJsunagawa '2015-07-26 End
// Add by CPJsunagawa '2015-08-03 Start
	{"JNI_NE_SetShowOnlyVehicleTrackFlag",  "(J)J", (void *)JNI_NE_SetShowOnlyVehicleTrackFlag},
	{"JNI_NE_GetShowOnlyVehicleTrackFlag",  "(Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_GetShowOnlyVehicleTrackFlag},
// Add by CPJsunagawa '2015-08-03 End
	{"JNI_DG_RepeatVoice",  "()J", (void *)JNI_DG_RepeatVoice},
	{"JNI_Java_GetVicsBtnSts",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_Java_GetVicsBtnSts },
	{"JNI_ct_SwitchVicsMode",  "()J", (void *)JNI_ct_SwitchVicsMode},
       {"JNI_DG_DecideRoute",   "(I)J",  (void *)JNI_DG_DecideRoute },
    {"JNI_NE_MP_SetGuideLineIsDisplayed",  "(JJ)J", (void *)JNI_NE_MP_SetGuideLineIsDisplayed},
    {"JNI_NE_MP_DestinationPoint",  "(JJ)J", (void *)JNI_NE_MP_DestinationPoint},
	{"JNI_NE_POI_Facility_GetMunicipalityRecListCount",  "(JLnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_POI_Facility_GetMunicipalityRecListCount},
    {"JNI_NE_POI_Facility_GetMunicipalityRecList",   "(JJ[Lnet/zmap/android/pnd/v2/data/POI_Facility_ListItem;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POI_Facility_GetMunicipalityRecList },
	// Add 2010/10/12 Z01ONOZAWA Start -->
      {"JNI_VP_SetAcceleroSensorData",   "(Lnet/zmap/android/pnd/v2/data/AccSensorData;)J",  (void *)JNI_VP_SetAcceleroSensorData },
      {"JNI_VP_SetGyroSensorData",   "(Lnet/zmap/android/pnd/v2/data/GyroSensorData;)J",  (void *)JNI_VP_SetGyroSensorData },
      {"JNI_VP_SetOrientSensorData",   "(Lnet/zmap/android/pnd/v2/data/OriSensorData;)J",  (void *)JNI_VP_SetOrientSensorData },
// Add 2010/10/12 Z01ONOZAWA End   <--
// Add 2011/02/23 sawada Start -->
    {"JNI_VP_SetCarGoingStatus", "(J)J",  (void *)JNI_VP_SetCarGoingStatus },
// Add 2011/02/23 sawada End   <--
//Add 2011/09/08 Z01yoneya Start -->
    {"JNI_VP_SetEnableSensorNaviFlag", "(I)J",  (void *)JNI_VP_SetEnableSensorNaviFlag },
    {"JNI_VP_GetEnableSensorNaviFlag", "()I",  (void *)JNI_VP_GetEnableSensorNaviFlag },
//Add 2011/09/08 Z01yoneya End <--
	{"JNI_VP_SwitchMapMatchingRoad",  "()J", (void *)JNI_VP_SwitchMapMatchingRoad},//自車の位置を修正
	{"JNI_VP_GetMovingStatus", "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_VP_GetMovingStatus },//停止中、走行中のステータス（Status）を取得する
	{"JNI_NE_POI_Mybox_ChangeData",  "(JJLjava/lang/String;I)J", (void *)JNI_NE_POI_Mybox_ChangeData},//Myboxの名称を変更する
      {"JNI_NE_SetMinorRoad",   "(I)J",  (void *)JNI_NE_SetMinorRoad  },//細街路の設定
      {"JNI_NE_GetMinorRoad",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetMinorRoad  },//細街路の設定状態取得
      {"JNI_NE_SetTollGuide",   "(I)J",  (void *)JNI_NE_SetTollGuide  },//料金所の発音設定状態設定する
      {"JNI_NE_GetTollGuide",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetTollGuide  },//料金所の発音設定状態取得
      {"JNI_NE_SetOrbisGuide",   "(I)J",  (void *)JNI_NE_SetOrbisGuide  },//Orbisの設定状態設定
      {"JNI_NE_GetOrbisGuide",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_GetOrbisGuide  },//Orbisの設定状態取得
      	{"JNI_DG_ClearOrbisData",  "()J", (void *)JNI_DG_ClearOrbisData},//Orbisのデータを削除する
	{"JNI_DG_UpdateOrbisData",  "(J[Lnet/zmap/android/pnd/v2/data/ZDG_ORBIS_INFO;)J", (void *)JNI_DG_UpdateOrbisData},//Orbisのデータを更新する
	{"JNI_DG_SetOrbisSetting",  "(Lnet/zmap/android/pnd/v2/data/ZDG_OrbisSettingInfo;)J", (void *)JNI_DG_SetOrbisSetting},///Orbisのデータを設定する
	{"JNI_Java_SetDriveMode",   "(I)J",  (void *)JNI_Java_SetDriveMode  },//運転手と助手席の状態設定する
	{"JNI_Java_GetDriveMode",  "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J", (void *)JNI_Java_GetDriveMode},//運転手と助手席の状態取得する
	{"JNI_NE_Drv_GetSectionSearchProperty",   "(IILnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_Drv_GetSectionSearchProperty  },//車モードの区間探索条件取得
	{"JNI_NE_Man_GetPointSearchProperty",   "(ILnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_NE_Man_GetPointSearchProperty  },//人モードの区間探索条件取得
	{"JNI_ct_uic_ShowRestoreRoute",   "()J",  (void *)JNI_ct_uic_ShowRestoreRoute },
	{"JNI_Java_SetSettingInfo",   "(I)J",  (void *)JNI_Java_SetSettingInfo  },//横縦屏を変更する
	{"JNI_Java_GetSettingInfo",   "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",  (void *)JNI_Java_GetSettingInfo  },//横縦屏を変更する
	{"JNI_CT_SCM_ChangeSettingInfo",   "(J[Lnet/zmap/android/pnd/v2/data/ZGRL_SIZE;)J",  (void *)JNI_CT_SCM_ChangeSettingInfo  },//横縦屏を変更する
	{"JNI_Lib_calcDistancePointToPoint",   "(JJJJLnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_Lib_calcDistancePointToPoint},//両点距離を計算する
	{"JNI_NE_POI_Facility_Refine",   "(JJ)J",  (void *)JNI_NE_POI_Facility_Refine },
	{"JNI_NE_POI_Facility_SyllabaricSearch",  "()J", (void *)JNI_NE_POI_Facility_SyllabaricSearch},
	{"JNI_NE_POI_Facility_AroundSearch",   "(JJJ)J",  (void *)JNI_NE_POI_Facility_AroundSearch },
	{"JNI_NE_POI_Facility_GetJumpList",   "([Lnet/zmap/android/pnd/v2/data/JNIJumpKey;Lnet/zmap/android/pnd/v2/data/JNIShort;)J",  (void *)JNI_NE_POI_Facility_GetJumpList },
    {"JNI_NE_POI_Facility_GetJumpKey2RecordNumber",   "(JLjava/lang/String;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POI_Facility_GetJumpKey2RecordNumber },
    {"JNI_ct_OnBGCommand",   "(JJ)J",  (void *)JNI_ct_OnBGCommand },
    {"JNI_Java_ct_ChangeNaviMode",   "(I)J",  (void *)JNI_Java_ct_ChangeNaviMode },
    {"JNI_Java_SetAppLevel",   "(I)J",  (void *)JNI_Java_SetAppLevel },
    {"JNI_NE_POI_Facility_GetPrefucturePointCount",   "(JLnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POI_Facility_GetPrefucturePointCount },//レコード件数を取得
    {"JNI_NE_POIMybox_GetRouteList",   "()J",  (void *)JNI_NE_POIMybox_GetRouteList },//ルート検索結果を取得する
    {"JNI_NE_POIMybox_GetRouteRecCount", "(JLnet/zmap/android/pnd/v2/data/POI_Mybox_ListGenreCount;)J",  (void *)JNI_NE_POIMybox_GetRouteRecCount },//沿い周辺検索リスト数を取得する
    {"JNI_NE_POIMybox_GetRouteListItem",   "(JJJ[Lnet/zmap/android/pnd/v2/data/POI_UserFile_RouteRec;Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_POIMybox_GetRouteListItem },//沿い周辺検索リストを取得する
    {"JNI_NE_POIArnd_MiddleList_Search",   "(JJ)J",  (void *)JNI_NE_POIArnd_MiddleList_Search },//中分類の検索
    // Add 2011/01/15 sawada Start -->
    // 歩行者案内向け
    { "JNI_WG_IsOnRoute", "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J",(void *)JNI_WG_IsOnRoute },
    { "JNI_WG_GetJctDirectionInfo", "(Lnet/zmap/android/pnd/v2/data/JNITwoInt;)J",(void *)JNI_WG_GetJctDirectionInfo },
    { "JNI_WG_GetRestDistance", "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_WG_GetRestDistance },
    { "JNI_WG_GetDestination", "(Lnet/zmap/android/pnd/v2/data/ZNUI_DESTINATION_DATA;)J", (void *)JNI_WG_GetDestination },
    { "JNI_WG_RepeatVoice", "()J",  (void *)JNI_WG_RepeatVoice },
// Add 2011/03/22 sawada Start -->
	// 電子コンパス向け
	{ "JNI_WG_SetCompassInfo", "(JJ)J", (void *)JNI_WG_SetCompassInfo },
// Add 2011/03/22 sawada End   <--
    // Add 2011/01/15 sawada End   <--
    {"JNI_NE_POI_Around_Clear",   "()J",  (void *)JNI_NE_POI_Around_Clear  },
    {"JNI_NE_MP_SetDirectLineIsDisplayed",  "(JJ)J", (void *)JNI_NE_MP_SetDirectLineIsDisplayed},
    {"JNI_NE_MP_PointContainInVehicle",  "(JJLnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_MP_PointContainInVehicle},
    {"JNI_NE_POIRoute_MiddleList_Search",   "(JJ)J",  (void *)JNI_NE_POIRoute_MiddleList_Search},
    {"JNI_NE_POIRoute_Clear",   "()J",  (void *)JNI_NE_POIRoute_Clear },
    {"JNI_NE_MP_SetOffRoute",   "(I)V",  (void *)JNI_NE_MP_SetOffRoute },
// Add 2011/04/08 add by libin for Changing Vehicle Status when Click Vehicle
    {"JNI_NE_MP_SetClickVehicle",   "(I)V",  (void *)JNI_NE_MP_SetClickVehicle },

    {"JNI_NE_Drv_PlayChangeVolume",   "()V",  (void *)JNI_NE_Drv_PlayChangeVolume },
// Chg 2011/03/28 sawada Start -->
//  {"JNI_UPD_IsNeedUpd", "(JJLnet/zmap/android/pnd/v2/data/JNIInt;Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_UPD_IsNeedUpd },
//  {"JNI_UPD_DelUpdFiles", "()J", (void *)JNI_UPD_DelUpdFiles },
//    {"JNI_UPD_IsNeedUpd", "(JJLnet/zmap/android/pnd/v2/data/JNIInt;Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_UPD_IsNeedUpd },
//	{"JNI_UPD_IsNeedReUpd", "(Lnet/zmap/android/pnd/v2/data/JNIInt;Lnet/zmap/android/pnd/v2/data/JNIInt;)J", (void *)JNI_UPD_IsNeedReUpd },
//    {"JNI_UPD_DelUpdFiles", "()J", (void *)JNI_UPD_DelUpdFiles },
//	{"JNI_UPD_TermAreaInfo", "()V", (void *)JNI_UPD_TermAreaInfo },
//	{"JNI_UPD_IsNeedAreaUpd","(JLnet/zmap/android/pnd/v2/data/JNIInt;Lnet/zmap/android/pnd/v2/data/JNILong;Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *) JNI_UPD_IsNeedAreaUpd },
//	{"JNI_UPD_GetAreaNames", "(Ljava/lang/String;[Lnet/zmap/android/pnd/v2/data/AreaName;)J",  (void *)JNI_UPD_GetAreaNames },
// Chg 2011/03/28 sawada End   <--
// Add 2011/03/29 Z01ONOZAWA Start -->
	{"JNI_VP_EnableSensor", "(I)V", (void *)JNI_VP_EnableSensor },
// Add 2011/03/29 Z01ONOZAWA End   <--
// Add 2011/02/17 Z01ONOZAWA Start --> MMログ出力対応
	{"JNI_VP_SetMMLogFilename",  "(Ljava/lang/String;)J", (void *)JNI_VP_SetMMLogFilename},//MMログファイル名称設定
// Add 2011/02/17 Z01ONOZAWA End   <--
//Add 2011/06/21 Z01thedoanh Start -->
	{"JNI_VP_SetVPSettingFilename",  "(Ljava/lang/String;)J", (void *)JNI_VP_SetVPSettingFilename},//VPSetting.ini名称設定
//Add 2011/06/21 Z01thedoanh End <--
// Debug用 by Z01ONOZAWA
	{"JNI_VP_GetCurrentSpeed", "(Lnet/zmap/android/pnd/v2/data/JNIInt;)J", (void *)JNI_VP_GetCurrentSpeed },
    {"JNI_NE_Navi_EnableShowMagMap", "(I)V", (void *)JNI_NE_Navi_EnableShowMagMap },
    {"JNI_NE_Navi_SetMovingSpeed", "(I)J", (void *)JNI_NE_Navi_SetMovingSpeed },
    {"JNI_NE_VICS_GetRequestParam", "(JJJLnet/zmap/android/pnd/v2/data/JNIString;Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_VICS_GetRequestParam },
//Add 2011/10/01 Z01yamaguchi - VICS案内実装  Start -->
    {"JNI_NE_VICS_ReloadVicsFile", "()J", (void *)JNI_NE_VICS_ReloadVicsFile },
//Add 2011/10/01 Z01yamaguchi - VICS案内実装  End <--
    {"JNI_NE_VICS_SetDisplayFlag", "(Z)J", (void *)JNI_NE_VICS_SetDisplayFlag },
//Add 2012/04/10 Z01hirama Start --> #4223
   {"JNI_ct_uic_DelBackupRoute",   "()V",  (void *)JNI_ct_uic_DelBackupRoute },
//Add 2012/04/10 Z01hirama End <-- #4223

//Add 2011/05/10 yinrongji Start
    {"JNI_NE_POIHistory_SetDistance", "(J)J",  (void *)JNI_NE_POIHistory_SetDistance },
    {"JNI_NE_POIHistory_GetAroundList", "(JJ)J",  (void *)JNI_NE_POIHistory_GetAroundList },
    {"JNI_NE_POIHistory_GetAroundRecCount", "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIHistory_GetAroundRecCount },
    {"JNI_NE_POIHistory_GetAroundListItem", "(JJ[Lnet/zmap/android/pnd/v2/data/POI_UserFile_AroundRec;Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIHistory_GetAroundListItem },

    {"JNI_NE_POIHistory_SetRouteParam",  "(Lnet/zmap/android/pnd/v2/data/ZPOI_Route_Param;)J", (void *)JNI_NE_POIHistory_SetRouteParam},
    {"JNI_NE_POIHistory_SetRoute",  "(I)J", (void *)JNI_NE_POIHistory_SetRoute},
    {"JNI_NE_POIHistory_GetRouteList",  "()J", (void *)JNI_NE_POIHistory_GetRouteList},
    {"JNI_NE_POIHistory_GetRouteRecCount", "(Lnet/zmap/android/pnd/v2/data/JNILong;)J",  (void *)JNI_NE_POIHistory_GetRouteRecCount },
    {"JNI_NE_POIHistory_GetRouteListItem",   "(JJ[Lnet/zmap/android/pnd/v2/data/POI_UserFile_RouteRec;Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_POIHistory_GetRouteListItem },
//Add 2011/05/10 yinrongji End
   {"JNI_NE_BackupMainRoute",	 "()V",  (void *)JNI_NE_BackupMainRoute },
//Add 2011/07/01 Z01thedoanh Start -->
   {"JNI_CT_setBIsExistsDetailMapData",	 "(Z)V",  (void *)JNI_CT_setBIsExistsDetailMapData },
//Add 2011/07/01 Z01thedoanh End <--
//Add 2011/09/06 Z01thedoanh Start -->
   {"JNI_MP_DC_GetDisplayArea",   "(Lnet/zmap/android/pnd/v2/data/JNITwoLong;Lnet/zmap/android/pnd/v2/data/JNITwoLong;)J",  (void *)JNI_MP_DC_GetDisplayArea },
   {"JNI_MP_DC_SetPOIInfoData",   "([Lnet/zmap/android/pnd/v2/data/DC_POIInfoData;)J",  (void *)JNI_MP_DC_SetPOIInfoData },
   {"JNI_MP_DC_SetPOIIconSize",   "(I)J",  (void *)JNI_MP_DC_SetPOIIconSize  },
//Add 2011/09/06 Z01thedoanh End <--
//	{"JNI_VP_GPSNMEA_String",  "(Ljava/lang/String;)J", (void *)JNI_VP_GPSNMEA_String},
//	{"JNI_VP_SetEventTcnt",   "(II)J",  (void *)JNI_VP_SetEventTcnt },
//	{"JNI_VP_GetDRMessageString",   "(Lnet/zmap/android/pnd/v2/data/JNIString;)J",  (void *)JNI_VP_GetDRMessageString  },
//	{"JNI_VP_clear_MMData",  "()J", (void *)JNI_VP_clear_MMData},	//生ログ走行時用 生ログ先頭に戻った場合、クリアする
// AgentHMI -->
    {"JNI_NE_AddUserLayer", "(Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_AddUserLayer },
    {"JNI_NE_DeleteUserLayer", "(J)J", (void *)JNI_NE_DeleteUserLayer },
    {"JNI_NE_IsExistUserLayer", "(JLnet/zmap/android/pnd/v2/data/JNIInt;)J", (void *)JNI_NE_IsExistUserLayer },
    {"JNI_NE_AddUserFigure_Point", "(JJJLjava/lang/String;Ljava/lang/String;Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_AddUserFigure_Point },
    {"JNI_NE_AddUserFigure_Line", "(J[Lnet/zmap/android/pnd/v2/data/JNITwoLong;Ljava/lang/String;Lnet/zmap/android/pnd/v2/api/overlay/LineDrawParam;Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_NE_AddUserFigure_Line },
    {"JNI_NE_DeleteUserFigure", "(JJ)J", (void *)JNI_NE_DeleteUserFigure },
    {"JNI_NE_DeleteAllUserFigure", "()J", (void *)JNI_NE_DeleteAllUserFigure },
    {"JNI_Java_GetMapCenterInfo_UserFig", "(Lnet/zmap/android/pnd/v2/data/JNIInt;Lnet/zmap/android/pnd/v2/data/JNILong;)J", (void *)JNI_Java_GetMapCenterInfo_UserFig},
// <-- AgentHMI
// ADD.2013.07.23 N.Sasao 高解像度切り替え対応 START
    {"JNI_NE_SetGraphicModeScale", "(F)V", (void *)JNI_NE_SetGraphicModeScale },
// ADD.2013.07.23 N.Sasao 高解像度切り替え対応  END
	// V2.5
	{"JNI_VP_GPSNMEA_String",  "(Ljava/lang/String;)J", (void *)JNI_VP_GPSNMEA_String},
	{"JNI_VP_SetEventTcnt",   "(II)J",  (void *)JNI_VP_SetEventTcnt },
	{"JNI_VP_GetDRMessageString",   "(Lnet/zmap/android/pnd/v2/data/JNIString;)J",  (void *)JNI_VP_GetDRMessageString  },
	{"JNI_VP_clear_MMData",  "()J", (void *)JNI_VP_clear_MMData},	//生ログ走行時用 生ログ先頭に戻った場合、クリアする
	{"JNI_VP_DRStopGo_Status",  "()J", (void *)JNI_VP_DRStopGo_Status},	//DR(ACC)学習完了で自車の停止状態を戻す
	{"JNI_VP_DR_Status",  "()J", (void *)JNI_VP_DR_Status},	//DR動作状態を戻す
	{"JNI_VP_SetDRprofileFilename",  "(Ljava/lang/String;)J", (void *)JNI_VP_SetDRprofileFilename},
	{"JNI_VP_SetDRprofilePath",  "(Ljava/lang/String;)J", (void *)JNI_VP_SetDRprofilePath},
	{"JNI_VP_Exconfig_Call",  "()J", (void *)JNI_VP_Exconfig_Call},
	{"JNI_VP_ProFile_Back",  "()J", (void *)JNI_VP_ProFile_Back},
	{"JNI_VP_GetAngleString",   "(Lnet/zmap/android/pnd/v2/data/JNIString;)J",  (void *)JNI_VP_GetAngleString  },
	{"JNI_VP_GetMoveString",   "(Lnet/zmap/android/pnd/v2/data/JNIString;)J",  (void *)JNI_VP_GetMoveString  },
	{"JNI_VP_GetCurveString",   "(Lnet/zmap/android/pnd/v2/data/JNIString;)J",  (void *)JNI_VP_GetCurveString  },
// ADD 2013.08.08 M.Honma 走行規制 細街路表示制御 Start -->
    {"JNI_NE_SetDrivingRegulation",	 "(Z)V",  (void *)JNI_NE_SetDrivingRegulation },
// ADD 2013.08.08 M.Honma 走行規制 細街路表示制御 End -->
// ADD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応�@ START
    {"JNI_CT_BG_GetVehiclePosition",   "(Lnet/zmap/android/pnd/v2/data/JNITwoLong;Lnet/zmap/android/pnd/v2/data/JNILong;Lnet/zmap/android/pnd/v2/data/JNIShort;)J",  (void *)JNI_CT_BG_GetVehiclePosition },
// ADD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応�@  END
// Add by CPJsunagawa '15-06-04 Start
	{"JNI_NE_setIsGuide", "(I)V",  (void *)JNI_NE_setIsGuide },
	{"JNI_NE_getIsGuide", "()I", (void *)JNI_NE_getIsGuide }
// Add by CPJsunagawa '15-06-04 End
};

//static const char* const kClassPathName = "com/Navigation/NaviRun";
static const char* const kClassPathName = "net/zmap/android/pnd/v2/data/NaviRun";
jint registerNativeMethods(JNIEnv* env, const char* className, const JNINativeMethod* methods, jint methodCount)
{
	jclass clazz;

//	LOGD("[DEBUG] registerNativeMethods CALL");

	clazz = env->FindClass(className);
	if (clazz == NULL) {
		LOGD("[DEBUG] NavigationjniLoad Can't find class for registration : %s", className);
		return -1;
	}

	if (env->RegisterNatives(clazz, methods, methodCount) < 0) {
		LOGD("[DEBUG] NavigationjniLoad Failed to RegisterNatives : %s", className);
		return -1;
	}
	return 0;
}

static int register_android_navigation(JNIEnv *env)
{
	jclass clazz;


	clazz = env->FindClass(kClassPathName);

	if ( clazz == NULL )
	{
		LOGEp("[DEBUG] register_android_navigation Not NaviRun FindClass!");
		return -1;
	}

    	fields.context = env->GetFieldID(clazz, "mNativeContext", "I");

	if ( fields.context == NULL )
    	{
		LOGEp("[DEBUG] register_android_navigation context NULL!");
	        return -1;
	}

	fields.post_event = env->GetStaticMethodID(clazz, "postEventFromNative", "(Ljava/lang/Object;III[B)V");
	if(!fields.post_event)
	{
		LOGEp("[DEBUG] register_android_navigation: failed to get post method");
		return -1;
	}

// Add by qilj for bug 851 begin
	fields.draw_mag_name = env->GetStaticMethodID(clazz, "draw_mag_name", "(Ljava/lang/Object;I)V");
	if(!fields.draw_mag_name)
	{
		LOGEp("[DEBUG] register_android_navigation: failed to get draw mag name method");
		return -1;
	}
// Add by qilj for bug 851 end

//	int iret = AndroidRuntime::registerNativeMethods(env, kClassPathName, gMethods, NELEM(gMethods));
	int iret = registerNativeMethods(env, kClassPathName, gMethods, NELEM(gMethods));
	LOGD("[DEBUG] register_android_navigation Succesful!!!!!!!![%d]", iret);

	return iret;
}

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
	JNIEnv* env = NULL;
	jint result = -1;

	gJavaVM = vm;

	if (vm->GetEnv((void**) &env, JNI_VERSION_1_4) != JNI_OK)
    	{
		return result;
    	}

    	assert(env != NULL);

    	if (register_android_navigation(env) < 0)
    	{
		return result;
    	}

    	/* success -- return valid version number */
    	result = JNI_VERSION_1_4;

    	return result;
}
// Add by qilj for bug 851 begin
int UI_DrawMagName(int bShow)
{
	int status;
	bool isAttached = false;
	JNIEnv *env = NULL;

	if(!iInitialed)
		return -1;
	pthread_mutex_lock( &g_postMutex);

	status = gJavaVM->GetEnv((void **) &env, JNI_VERSION_1_4);
	if(status < 0) {
	    status = gJavaVM->AttachCurrentThread(&env, NULL);
	    if(status < 0) {
		//LOGDp("UI_DrawMagName: failed to attach current thread");
		pthread_mutex_unlock( &g_postMutex);
		return -1;
	    }
	    isAttached = true;
	}

	env->CallStaticVoidMethod(gClazz, fields.draw_mag_name, gRefObject, bShow);

	if(isAttached)
		gJavaVM->DetachCurrentThread();

	pthread_mutex_unlock( &g_postMutex);

	return 0;
}
// Add by qilj for bug 851 end


int SendMessageToJavaUI(int msg, int ext1, int ext2, const char* extb, int extbsize)
{
	int status;

	bool isAttached = false;
	JNIEnv *env = NULL;

	if(!iInitialed)
		return -1;
	pthread_mutex_lock( &g_postMutex);

	status = gJavaVM->GetEnv((void **) &env, JNI_VERSION_1_4);
	if(status < 0) {
	    //LOGE("SendMessageToJavaUI: failed to get JNI environment, assuming native thread");
	    status = gJavaVM->AttachCurrentThread(&env, NULL);
	    if(status < 0) {
	    	LOGEp("SendMessageToJavaUI: failed to attach current thread");
		pthread_mutex_unlock( &g_postMutex);
		return -1;
	    }
	    isAttached = true;
	}

	jbyteArray jbextb = ((NULL!=extb) && (0<extbsize)) ? env->NewByteArray(extbsize) : NULL;

	if ( jbextb )
	{
		env->SetByteArrayRegion(jbextb, 0, extbsize, (jbyte*)extb);
	}

	env->CallStaticVoidMethod(gClazz, fields.post_event, gRefObject, msg, ext1, ext2, jbextb);

	if(isAttached)
		gJavaVM->DetachCurrentThread();

	pthread_mutex_unlock( &g_postMutex);

	return 0;
}


char* ConvUtf8ToSjis ( const char* extb)
{
	int status;
	bool isAttached = false;

	JNIEnv *env = NULL;
	status = gJavaVM->GetEnv((void **) &env, JNI_VERSION_1_4);
	if(status < 0) {
	    status = gJavaVM->AttachCurrentThread(&env, NULL);
	    if(status < 0) {
	    	LOGEp("ConvUtf8ToSjis: failed to attach current thread");
	    	pthread_mutex_unlock( &g_postMutex);
	    	return NULL;
	    }
	    isAttached = true;
	}
	jclass interfaceClass = env->GetObjectClass(gRefObject);
	if(!interfaceClass) {
		LOGEp("ConvUtf8ToSjis: failed to get class reference");
	    if(isAttached) gJavaVM->DetachCurrentThread();
	    pthread_mutex_unlock( &g_postMutex);
	    return NULL;
	}

	char* rtn = NULL;
	jstring jstr = env->NewStringUTF(extb);
	jclass clsstring = env->FindClass("java/lang/String");
	jstring strencode = env->NewStringUTF("MS932");
	jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
	jbyteArray barr= (jbyteArray)env->CallObjectMethod(jstr, mid, strencode);
	jsize alen = env->GetArrayLength(barr);
	jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);

	if (alen > 0)
	{
		rtn = new char[alen+1];
		memcpy(rtn, ba, alen);
		rtn[alen] = 0;
	}

	//LOGDp("DBG_UPD", ("Local:Shift-JIS %02x %02x"), rtn[0], rtn[1]);

	env->ReleaseByteArrayElements(barr, ba, 0);
	if(isAttached)
		gJavaVM->DetachCurrentThread();

	pthread_mutex_unlock( &g_postMutex);
	return rtn;
}


//#define min(a, b) ((a) < (b) ? (a) : (b))

int ShiftJisToUtf8(const char* shiftjis, int ilenjis, char*Utf8, int ilenutf8)
{
/*
	int status;

	bool isAttached = false;
	JNIEnv *env = NULL;

	if(!iInitialed)
		return -1;

	status = gJavaVM->GetEnv((void **) &env, JNI_VERSION_1_4);
	if(status < 0) {
	    //LOGE("SendMessageToJavaUI: failed to get JNI environment, assuming native thread");
	    status = gJavaVM->AttachCurrentThread(&env, NULL);
	    if(status < 0) {
		LOGE("ShiftJisToUtf8: failed to attach current thread");
		return -1;
	    }
	    isAttached = true;
	}

	jmethodID ctorID_Str;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID_Str = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}else
	{
	    if(isAttached) gJavaVM->DetachCurrentThread();
		LOGDp("[DEBUG] ShiftJisToUtf8 for 7-error strClass NULL");
		return -1;
	}

	int lenjis = strlen((char*)shiftjis);
	jbyteArray bytes = env->NewByteArray(lenjis);
	if(bytes != NULL)
	{
		env->SetByteArrayRegion(bytes, 0, lenjis, (jbyte*)shiftjis);

	}else{
		if(isAttached) gJavaVM->DetachCurrentThread();
		LOGDp("[DEBUG] ShiftJisToUtf8  error bytes==NULL");
		return -1;
	}
	jstring encoding = env->NewStringUTF("ShiftJIS");
	jstring  jsName;
	if(encoding)
	{
	  jsName = (jstring)env->NewObject(strClass, ctorID_Str, bytes, encoding);
	}


       jstring strencode = env->NewStringUTF("utf-8");
       jmethodID mid = env->GetMethodID(strClass, "getBytes", "(Ljava/lang/String;)[B");
       jbyteArray barr= (jbyteArray)env->CallObjectMethod(jsName, mid, strencode);
       jsize alen = env->GetArrayLength(barr);
       jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);
       if (alen > 0)
 	 {
		       int ilencopy = min(alen,ilenutf8);
                 memcpy(Utf8, ba, ilencopy);
                 Utf8[ilencopy] = 0;
        }
       env->ReleaseByteArrayElements(barr, ba, 0);

	if(isAttached)
		gJavaVM->DetachCurrentThread();
*/

	int len = Lib_JISStrToUTF8Str((char*)shiftjis, Utf8, ilenjis,ilenutf8);
	return len;
}

// ADD.2013.05.10 N.Sasao エンジン共通化対応 START
char* GetDrawTextSizeInfo( const char* text,float fontSize, int bold,int writing_mode )
{
	int status;
	JNIEnv *env = NULL;

	if(!iInitialed){
		return NULL;
	}
	pthread_mutex_lock( &g_postMutex);

	status = gJavaVM->GetEnv((void **) &env, JNI_VERSION_1_4);
	if(status < 0) {
	    status = gJavaVM->AttachCurrentThread(&env, NULL);
	    if(status < 0) {
			//LOGDp("UI_DrawMagName: failed to attach current thread");
			pthread_mutex_unlock( &g_postMutex);
			LOGE("GetDrawTextSizeInfo: AttachCurrentThread error");
			return NULL;
	    }
	}

	jclass clsj = env->FindClass("net/zmap/android/pnd/v2/engine/PlatformWrapper");
	if (clsj==NULL){
		pthread_mutex_unlock( &g_postMutex);
		LOGE("GetDrawTextSizeInfo: clsj is null");
		return NULL;
	}
	jmethodID calcTextSizeFunc = env->GetStaticMethodID(clsj, "CalcDrawTextSize", "(Ljava/lang/String;FZI)Lnet/zmap/android/pnd/v2/engine/PlatformWrapper$DrawTextSizeInfo;");
	if (calcTextSizeFunc==NULL){
		pthread_mutex_unlock( &g_postMutex);
		LOGE("GetDrawTextSizeInfo: calcTextSizeFunc is null");
		return NULL;
	}

	jstring jstr = env->NewStringUTF(text);

	jobject jDrawSize = env->CallStaticObjectMethod(clsj, calcTextSizeFunc,jstr,fontSize,(bool)bold,writing_mode);

	jclass jPointClass = env->GetObjectClass(jDrawSize);
	jfieldID jXField = env->GetFieldID(jPointClass, "width", "I");
	if (jXField==NULL){
		pthread_mutex_unlock( &g_postMutex);
		LOGE("GetDrawTextSizeInfo: jXField is null");
		return NULL;
	}
	jfieldID jYField = env->GetFieldID(jPointClass, "height", "I");
	if (jYField==NULL){
		pthread_mutex_unlock( &g_postMutex);
		LOGE("GetDrawTextSizeInfo: jYField is null");
		return NULL;
	}
	jfieldID jbaselineField = env->GetFieldID(jPointClass, "baselineHeight", "I");
	if (jbaselineField==NULL){
		pthread_mutex_unlock( &g_postMutex);
		LOGE("GetDrawTextSizeInfo: jbaselineField is null");
		return NULL;
	}

	jint width = env->GetIntField(jDrawSize, jXField);
	jint height = env->GetIntField(jDrawSize, jYField);
	jint baseline_height = env->GetIntField(jDrawSize, jbaselineField);

	env->DeleteLocalRef(clsj);
	env->DeleteLocalRef(jstr);
	env->DeleteLocalRef(jDrawSize);
	env->DeleteLocalRef(jPointClass);

	memset( &stDrawTextSizeInfo, 0, sizeof(ZGRL_DRAW_TEXT_SIZE_INFO) );

	stDrawTextSizeInfo.ulWidth = width;
	stDrawTextSizeInfo.ulHeight = height;
	stDrawTextSizeInfo.ulBaseline_height = baseline_height;

	pthread_mutex_unlock( &g_postMutex);

	return (char*)&stDrawTextSizeInfo;
}
char* GetFontTextureInfo(const char* pInputData)
{
	ZGRL_CREATE_FONT_INFO* pstInputData;
	int status;
	JNIEnv *env = NULL;

	pstInputData = (ZGRL_CREATE_FONT_INFO*)pInputData;

	if( pstInputData == NULL ){
		return NULL;
	}

	if( pstInputData->pszDrawName == NULL ){
		return NULL;
	}

	if(!iInitialed){
		return NULL;
	}

	pthread_mutex_lock( &g_postMutex);

	status = gJavaVM->GetEnv((void **) &env, JNI_VERSION_1_4);
	if(status < 0) {
	    status = gJavaVM->AttachCurrentThread(&env, NULL);
	    if(status < 0) {
			//LOGDp("UI_DrawMagName: failed to attach current thread");
			pthread_mutex_unlock( &g_postMutex);
			LOGE("GetFontTextureInfo: AttachCurrentThread error");
			return NULL;
	    }
	}

	jclass clsj = env->FindClass("net/zmap/android/pnd/v2/engine/PlatformWrapper");
	if (clsj==NULL){
		pthread_mutex_unlock( &g_postMutex);
		LOGE("GetFontTextureInfo: clsj is NULL");
		return NULL;
	}
	jmethodID createTextureFunc = env->GetStaticMethodID(clsj, "CreateFontTexture", "(Ljava/lang/String;IIFZZ[IIIFFI)Lnet/zmap/android/pnd/v2/engine/PlatformWrapper$GLTextureInfo;");
	if (createTextureFunc==NULL){
		pthread_mutex_unlock( &g_postMutex);
		LOGE("GetFontTextureInfo: createTextureFunc is NULL");
		return NULL;
	}

	jboolean boldText = (jboolean)pstInputData->ulBoldText;
	jboolean fillRect = (jboolean)pstInputData->ulFillRect;

#ifdef __THAI__
	if( DAL_GetLanguageType() == DAL_LANG_THAI)
	{//タイ語の場合は大きめに描画するのでboldなし
		boldText=false;
	}
#endif

	int backImageWidth = 0;
	int backImageHeight = 0;
	float backImageScale = 1;

	jintArray backImagePixels = 0;

    if(pstInputData->ulDrawType == PCache::MapDrawName::DRAW_SIZE_SHIELD_ICON)
    {
        ZRoadNumberFrame_t roadNumberFrame={0};
#ifdef __THAI__
		DAL_GetRoadNumberFrameAsIndex(pstInputData->ulKindCode-1,strlen(pstInputData->pszDrawName),&roadNumberFrame);
#else
		DAL_GetRoadNumberFrame(pstInputData->ulKindCode,pstInputData->ulNameSize,&roadNumberFrame);
#endif

        backImageWidth = (float)roadNumberFrame.zstLandmarkData.ucWidth;
		backImageHeight =(float)roadNumberFrame.zstLandmarkData.ucHeight;

		float iconSize = pstInputData->ulUseIconSize;
		backImageScale = (float)(iconSize / backImageWidth) * 1.5f;

		backImagePixels = env->NewIntArray(backImageWidth * backImageHeight);
		jint* backImagePixelsData = env->GetIntArrayElements(backImagePixels, NULL);

		int* sourcePixels = (int*)roadNumberFrame.zstLandmarkData.pucData;
		int pixelSize = backImageWidth * backImageHeight;
		for(int i=0;i<pixelSize;i++)
		{
			//ARGB -> ABGR(RGBA)
			PCache::Color color;
			color.setRGBA(sourcePixels[i]);
			backImagePixelsData[i] =color.getARGB();
		}
		env->ReleaseIntArrayElements(backImagePixels,backImagePixelsData, 0);

		boldText = true;
    }


	float edge_size = 0;
	if(pstInputData->ulAlpha != 0 && !pstInputData->ulFillRect)
	{
		edge_size = pstInputData->dFontSize * 0.1;
	}
	jstring jstr = env->NewStringUTF(pstInputData->pszDrawName);

	jobject jTextureInfo = env->CallStaticObjectMethod(clsj, createTextureFunc,jstr,pstInputData->ulForeColorCode,pstInputData->ulHemmingColorCode,pstInputData->dFontSize,boldText,fillRect,backImagePixels,backImageWidth,backImageHeight,backImageScale,edge_size,pstInputData->ulDirection);

	jclass jTextureInfoClass = env->GetObjectClass(jTextureInfo);
	jfieldID jTextureID = env->GetFieldID(jTextureInfoClass, "textureID", "I");
	jfieldID jWidth = env->GetFieldID(jTextureInfoClass, "width", "I");
	jfieldID jHeight = env->GetFieldID(jTextureInfoClass, "height", "I");

	if (jWidth==NULL || jHeight==NULL || jTextureID==NULL){
		env->DeleteLocalRef(clsj);
		env->DeleteLocalRef(jstr);
		env->DeleteLocalRef(jTextureInfo);
		env->DeleteLocalRef(jTextureInfoClass);

		pthread_mutex_unlock( &g_postMutex);
		LOGE("GetFontTextureInfo: jWidth or jHeight or jTextureID is NULL");
		return NULL;
	}
	jint textureID = env->GetIntField(jTextureInfo, jTextureID);
	jint width = env->GetIntField(jTextureInfo, jWidth);
	jint height = env->GetIntField(jTextureInfo, jHeight);

	env->DeleteLocalRef(clsj);
	env->DeleteLocalRef(jstr);
	env->DeleteLocalRef(jTextureInfo);
	env->DeleteLocalRef(jTextureInfoClass);

	memset( &stFontTextureInfo, 0, sizeof(ZGRL_FONT_TEXTURE_INFO) );

	stFontTextureInfo.ulWidth = width;
	stFontTextureInfo.ulHeight = height;
	stFontTextureInfo.ulTextureID = textureID;

	pthread_mutex_unlock( &g_postMutex);

	return (char*)&stFontTextureInfo;
}

char* GetAssetFileInfo( const char* pszFileName )
{
	int status;
	JNIEnv *env = NULL;

	if(!iInitialed){
		return NULL;
	}

	clAssetFileInfo.clear();

	if( pszFileName == NULL ){
		LOGE("GetAssetFileInfo: pszFileName is null");
		return (char*)&clAssetFileInfo;
	}

	pthread_mutex_lock( &g_postMutex);

	status = gJavaVM->GetEnv((void **) &env, JNI_VERSION_1_4);
	if(status < 0) {
	    status = gJavaVM->AttachCurrentThread(&env, NULL);
	    if(status < 0) {
			//LOGDp("UI_DrawMagName: failed to attach current thread");
			pthread_mutex_unlock( &g_postMutex);
			LOGE("GetAssetFileInfo: AttachCurrentThread error");
			return NULL;
	    }
	}

	jclass clsj = env->FindClass("net/zmap/android/pnd/v2/engine/PlatformWrapper");
	if (clsj==NULL){
		pthread_mutex_unlock( &g_postMutex);
		LOGE("GetAssetFileInfo: clsj is null");
		return (char*)&clAssetFileInfo;
	}
	jmethodID getAssetFileDataFunc = env->GetStaticMethodID(clsj, "GetAssetFileData","(Ljava/lang/String;)[B");
	if (getAssetFileDataFunc==NULL){
		pthread_mutex_unlock( &g_postMutex);
		LOGE("GetAssetFileInfo: getAssetFileDataFunc is null");
		return (char*)&clAssetFileInfo;
	}

	jstring jstr = env->NewStringUTF(pszFileName);
	jobject jmvdata = env->CallStaticObjectMethod(clsj, getAssetFileDataFunc,jstr);
	jbyteArray jdata = *reinterpret_cast<jbyteArray*>(&jmvdata);

	if(jdata)
	{
		jbyte* filedata = env->GetByteArrayElements(jdata, NULL);
		clAssetFileInfo.assign(filedata,filedata+env->GetArrayLength(jdata));
		env->ReleaseByteArrayElements(jdata, filedata, 0);
	}
	env->DeleteLocalRef(clsj);
	env->DeleteLocalRef(jstr);
	env->DeleteLocalRef(jmvdata);

	pthread_mutex_unlock( &g_postMutex);

	return (char*)&clAssetFileInfo;
}
// ADD.2013.05.10 N.Sasao エンジン共通化対応  END
// ////for Test message
#ifdef TEST_POST_MESSAGE
static pthread_t g_pThreadHandle = 0;

void* ThreadMessageProcFunc(void * param)
{
	LOGDp( "[DEBUG]ThreadMessageProcFunc begin" );

	int status;
	bool isAttached = false;
	JNIEnv *env = NULL;

	status = gJavaVM->GetEnv((void **) &env, JNI_VERSION_1_4);
	if(status < 0) {
	    LOGE("ThreadMessageProcFunc: failed to get JNI environment, "
		 "assuming native thread");
	    status = gJavaVM->AttachCurrentThread(&env, NULL);
	    if(status < 0) {
		LOGE("ThreadMessageProcFunc: failed to attach "
		     "current thread");
		return 0;
	    }
	    isAttached = true;
	}
	LOGDp("[DEBUG]ThreadMessageProcFunc proc : env = %d", env);

	jclass interfaceClass = env->GetObjectClass(gRefObject);
	if(!interfaceClass) {
	    LOGE("ThreadMessageProcFunc: failed to get class reference");
	    if(isAttached) gJavaVM->DetachCurrentThread();
	    return 0;
	}
	LOGDp("ThreadMessageProcFunc: classref:%d method:%d",interfaceClass,fields.post_event);

	while(1)
	{
		LOGDp( "[DEBUG]ThreadProcFunc sendmessage begin" );
		env->CallStaticVoidMethod(interfaceClass, fields.post_event, gRefObject, -1, -1, -1, 0);
		LOGDp( "[DEBUG]ThreadProcFunc sendmessage end" );
		usleep(10*1000);
	}

	if(isAttached) gJavaVM->DetachCurrentThread();
	LOGDp( "[DEBUG]ThreadProcFunc end" );
}

void StartSendMessageThread()
{

	int iRet = -1;

	iRet = pthread_create( &g_pThreadHandle, NULL, ThreadMessageProcFunc, NULL );

	if( iRet < 0 )
	{
		LOGE( "[DEBUG]NaviMainThreadProc create thread fail" );
	}

	LOGDp( "[DEBUG]StartSendMessageThread end" );
}
#endif
