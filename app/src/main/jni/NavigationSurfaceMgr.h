// NavigationSurfaceMgr.h

#ifndef _NAVIGATION_SURFACE_MGR_H_
#define _NAVIGATION_SURFACE_MGR_H_

#include <sys/prctl.h>
#include <sys/resource.h>


#ifdef	__cplusplus
extern "C" {
#endif//__cplusplus

void initNaviSurfaceMgr();
void eixtNaviSurfaceMgr();
int setDisplay(int width, int height);

void setMouseEventPos(int mode, int x, int y);

#ifdef	__cplusplus
}
#endif//__cplusplus

#endif // _NAVIGATION_SURFACE_MGR_H_
