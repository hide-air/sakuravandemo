/********************************************************************************
* File Name	:	 JNIMap_Communication.cpp
* Created	:	2009/11/21
* Author	:	xiayx
* Model		:
* Description	:
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "JNIMap_Communication.h"
//#include "NaviEngineIF.h"
#include "NaviEngineCtrl.h"
#include "CTUICommand.h"
#include "NETypes.h"
#include "windows.h"
#include "NaviEngine/POI/POIStruct.h"
#include "CTCarGuide.h"
#include "datum.h"
#include "jni/JNI_Common.h"
#include "CTICU.h"
#include "NaviUI.h"
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "NaviEngine/GRL_GDI/GraphicsLibrary.h"
#include "NaviEngine/MP/DrawMap.h"
#include "NaviEngine/MP/MPICON/MPOutSidePOIIcon.h"
#include "CTMapDisplay.h"

// ExternalAPI -->
#include "JNI_Error.h"
#include "JNI_CommonData.h"
// <-- ExternalAPI

#define LOG_TAG "NavigationJNI_NaviRun"
#define JNI_DEBUG

#include "debugPrint.h"

typedef unsigned char byte;

/*********************************************************************
*	Function Name	: JNI_NE_SetDefaultMapScale        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetDefaultMapScale
  (JNIEnv *env, jclass cls, jbyte bAppLv, jdouble dScale)
{
	return (jlong)NE_SetDefaultMapScale((byte)bAppLv,(double)dScale);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetMapDParaMode       	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMapDParaMode
  (JNIEnv *env, jclass cls,jobject object)
{
	ENE_MapDParaMode MapParaMode;

	jlong lRet = (jlong)NE_GetMapDParaMode(&MapParaMode);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID MapMode = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(MapMode)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,MapMode,(jint)MapParaMode);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_SetDefaultMapScale GetFieldID MapMode Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_SetMapDParaMode       	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetMapDParaMode
    (JNIEnv *env, jclass cls,jint MapMode)
{
	return (jlong)NE_SetMapDParaMode((ENE_MapDParaMode)MapMode);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetMapIconSize       	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMapIconSize
  (JNIEnv *env, jclass cls,jobject object)
{
	ENE_MapIconSize IconSize;

	jlong lRet = (jlong)NE_GetMapIconSize(&IconSize);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID MapIconSize = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(MapIconSize)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,MapIconSize,(jint)IconSize);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetMapDParaMode GetFieldID MapIconSize Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_SetMapIconSize       	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetMapIconSize
    (JNIEnv *env, jclass cls,jint MapIconSize)
{
	return (jlong)NE_SetMapIconSize((ENE_MapIconSize)MapIconSize);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetMapWordSize       	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMapWordSize
  (JNIEnv *env, jclass cls,jobject object)
{
	ENE_MapWordSize WordSize;

	jlong lRet = (jlong)NE_GetMapWordSize(&WordSize);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID MapWordSize = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(MapWordSize)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,MapWordSize,(jint)WordSize);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetMapWordSize GetFieldID MapWordSize Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_SetMapWordSize       	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetMapWordSize
    (JNIEnv *env, jclass cls,jint MapWordSize)
{
	return (jlong)NE_SetMapWordSize((ENE_MapWordSize)MapWordSize);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetDispMapOneway       	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetDispMapOneway
  (JNIEnv *env, jclass cls,jobject object)
{
	int bDisp;

	jlong lRet = (jlong)NE_GetDispMapOneway(&bDisp);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID IsDisp = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(IsDisp)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,IsDisp,(jint)bDisp);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetDispMapOneway GetFieldID IsDisp Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_SetDispMapOneway       	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetDispMapOneway
    (JNIEnv *env, jclass cls,jint bDisp)
{
	return (jlong)NE_SetDispMapOneway(bDisp);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetMapHeadingup       	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMapHeadingup
  (JNIEnv *env, jclass cls,jobject object)
{
	int bOn;

	jlong lRet = (jlong)NE_GetMapHeadingup(&bOn);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID IsOn = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(IsOn)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,IsOn,(jint)bOn);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetMapHeadingup GetFieldID IsOn Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_SetMapHeadingup      	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetMapHeadingup
    (JNIEnv *env, jclass cls,jint bOn)
{
	return (jlong)NE_SetMapHeadingup(bOn);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetMapFrontWide      	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMapFrontWide
  (JNIEnv *env, jclass cls,jobject object)
{
	int bOn;

	jlong lRet = (jlong)NE_GetMapFrontWide(&bOn);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID IsOn = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(IsOn)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,IsOn,(jint)bOn);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetMapFrontWide GetFieldID IsOn Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_SetMapFrontWide      	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetMapFrontWide
    (JNIEnv *env, jclass cls,jint bOn)
{
	return (jlong)NE_SetMapFrontWide(bOn);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetMapCenter      	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMapCenter
  (JNIEnv *env, jclass cls,jobject object)
{
	long Longitude,Latitude;

	jlong lRet = (jlong)NE_GetMapCenter(&Longitude,&Latitude);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNITwoLong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jLongitude = (*env)->GetFieldID(env,clazz,"m_lLong","J");
		if(jLongitude)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env,object,jLongitude,Longitude);
			jfieldID jLatitude = (*env)->GetFieldID(env,clazz,"m_lLat","J");
			if(jLatitude)
			{
				(*env)->SetLongField(env,object,jLatitude,Latitude);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_GetMapCenter GetFieldID jLatitude Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetMapCenter GetFieldID jLongitude Failed");
		}
	}

	return lRet;
}


/*********************************************************************
*	Function Name	: JNI_CT_GetMapPosition      	     *
*	Description	:                                            *
*	Date		: 11/11/30                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_GetMapPosition
  (JNIEnv *env, jclass cls,jobject mapInfo)
{
	ZCT_ICU_CtrlInfo mapPosition;

	jclass mapInfoClazz;
	jfieldID mAppLevelFID, mScaleFID;
	jfieldID mMapPointFID, mMyPositionFID, mMapAngleFID, mMyPositionAngleFID;
	jfieldID mVehicleStatusID;

	jclass coordinateClazz;
	jfieldID mLongitudeFID, mLatitudeFID;
	jmethodID coordinateInitMID;

	jobject mapPoint, myPosition;

	jlong lRet = (jlong)CT_GetMapPosition(&mapPosition);

	mapInfoClazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/MapInfo");
	mAppLevelFID = (*env)->GetFieldID(env, mapInfoClazz, "mAppLevel", "B");
	mScaleFID = (*env)->GetFieldID(env, mapInfoClazz, "mScale", "D");
	mMapPointFID = (*env)->GetFieldID(env, mapInfoClazz, "mMapPoint", "Lnet/zmap/android/pnd/v2/data/Coordinate;");
	mMyPositionFID = (*env)->GetFieldID(env, mapInfoClazz, "mMyPosition", "Lnet/zmap/android/pnd/v2/data/Coordinate;");
	mMapAngleFID = (*env)->GetFieldID(env, mapInfoClazz, "mMapAngle", "F");
	mMyPositionAngleFID = (*env)->GetFieldID(env, mapInfoClazz, "mMyPositionAngle", "F");
	mVehicleStatusID = (*env)->GetFieldID(env, mapInfoClazz, "mVehicleStatus", "B");

	coordinateClazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/Coordinate");
	coordinateInitMID = (*env)->GetMethodID(env, coordinateClazz, "<init>", "()V");
	mLongitudeFID = (*env)->GetFieldID(env, coordinateClazz, "mLongitude", "J");
	mLatitudeFID = (*env)->GetFieldID(env, coordinateClazz, "mLatitude", "J");

	if (mapInfoClazz && mAppLevelFID && mScaleFID
		&& mMapPointFID && mMyPositionFID && mMapAngleFID && mMyPositionAngleFID && mVehicleStatusID
		&& coordinateClazz && mLongitudeFID && mLatitudeFID) {
		(*env)->SetByteField(env, mapInfo, mAppLevelFID, mapPosition.byAppLv);
		(*env)->SetDoubleField(env, mapInfo, mScaleFID, mapPosition.dScale);

		mapPoint = (*env)->NewObject(env, coordinateClazz, coordinateInitMID);
		(*env)->SetLongField(env, mapPoint, mLongitudeFID, mapPosition.stMapPt.lnLongitude);
		(*env)->SetLongField(env, mapPoint, mLatitudeFID, mapPosition.stMapPt.lnLatitude);
		(*env)->SetObjectField(env, mapInfo, mMapPointFID, mapPoint);

		myPosition = (*env)->NewObject(env, coordinateClazz, coordinateInitMID);
		(*env)->SetLongField(env, myPosition, mLongitudeFID, mapPosition.stMyPosiPt.lnLongitude);
		(*env)->SetLongField(env, myPosition, mLatitudeFID, mapPosition.stMyPosiPt.lnLatitude);
		(*env)->SetObjectField(env, mapInfo, mMyPositionFID, myPosition);

		(*env)->SetFloatField(env, mapInfo, mMapAngleFID, mapPosition.nMapAngle);
		(*env)->SetFloatField(env, mapInfo, mMyPositionAngleFID, mapPosition.nMyPosiAngle);
		(*env)->SetByteField(env, mapInfo, mVehicleStatusID, mapPosition.bVehicleStatus);
	} else {
		LOGD("[DEBUG] JNI_CT_GetMapPosition FindClass or GetFieldID Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_CT_GetMapPosition      	     *
*	Description	:                                            *
*	Date		: 11/11/30                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_GetReqMapPosition
  (JNIEnv *env, jclass cls,jobject mapInfo)
{
	ZCT_ICU_CtrlInfo mapPosition;

	jclass mapInfoClazz;
	jfieldID mAppLevelFID, mScaleFID;
	jfieldID mMapPointFID, mMyPositionFID, mMapAngleFID, mMyPositionAngleFID;
	jfieldID mVehicleStatusID;

	jclass coordinateClazz;
	jfieldID mLongitudeFID, mLatitudeFID;
	jmethodID coordinateInitMID;

	jobject mapPoint, myPosition;

	jlong lRet = (jlong)CT_GetReqMapPosition(&mapPosition);

	mapInfoClazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/MapInfo");
	mAppLevelFID = (*env)->GetFieldID(env, mapInfoClazz, "mAppLevel", "B");
	mScaleFID = (*env)->GetFieldID(env, mapInfoClazz, "mScale", "D");
	mMapPointFID = (*env)->GetFieldID(env, mapInfoClazz, "mMapPoint", "Lnet/zmap/android/pnd/v2/data/Coordinate;");
	mMyPositionFID = (*env)->GetFieldID(env, mapInfoClazz, "mMyPosition", "Lnet/zmap/android/pnd/v2/data/Coordinate;");
	mMapAngleFID = (*env)->GetFieldID(env, mapInfoClazz, "mMapAngle", "F");
	mMyPositionAngleFID = (*env)->GetFieldID(env, mapInfoClazz, "mMyPositionAngle", "F");
	mVehicleStatusID = (*env)->GetFieldID(env, mapInfoClazz, "mVehicleStatus", "B");

	coordinateClazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/Coordinate");
	coordinateInitMID = (*env)->GetMethodID(env, coordinateClazz, "<init>", "()V");
	mLongitudeFID = (*env)->GetFieldID(env, coordinateClazz, "mLongitude", "J");
	mLatitudeFID = (*env)->GetFieldID(env, coordinateClazz, "mLatitude", "J");

	if (mapInfoClazz && mAppLevelFID && mScaleFID
		&& mMapPointFID && mMyPositionFID && mMapAngleFID && mMyPositionAngleFID && mVehicleStatusID
		&& coordinateClazz && mLongitudeFID && mLatitudeFID) {
		(*env)->SetByteField(env, mapInfo, mAppLevelFID, mapPosition.byAppLv);
		(*env)->SetDoubleField(env, mapInfo, mScaleFID, mapPosition.dScale);

		mapPoint = (*env)->NewObject(env, coordinateClazz, coordinateInitMID);
		(*env)->SetLongField(env, mapPoint, mLongitudeFID, mapPosition.stMapPt.lnLongitude);
		(*env)->SetLongField(env, mapPoint, mLatitudeFID, mapPosition.stMapPt.lnLatitude);
		(*env)->SetObjectField(env, mapInfo, mMapPointFID, mapPoint);

		myPosition = (*env)->NewObject(env, coordinateClazz, coordinateInitMID);
		(*env)->SetLongField(env, myPosition, mLongitudeFID, mapPosition.stMyPosiPt.lnLongitude);
		(*env)->SetLongField(env, myPosition, mLatitudeFID, mapPosition.stMyPosiPt.lnLatitude);
		(*env)->SetObjectField(env, mapInfo, mMyPositionFID, myPosition);

		(*env)->SetFloatField(env, mapInfo, mMapAngleFID, mapPosition.nMapAngle);
		(*env)->SetFloatField(env, mapInfo, mMyPositionAngleFID, mapPosition.nMyPosiAngle);
		(*env)->SetByteField(env, mapInfo, mVehicleStatusID, mapPosition.bVehicleStatus);
	} else {
		LOGD("[DEBUG] JNI_CT_GetMapPosition FindClass or GetFieldID Failed");
	}

	return lRet;
}


/*********************************************************************
*	Function Name	: JNI_NE_GetMapLevel      	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMapLevel
  (JNIEnv *env, jclass cls,jobject object)
{
	byte byAppLv;

	jlong lRet = (jlong)NE_GetMapLevel(&byAppLv);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIByte");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jApplv = (*env)->GetFieldID(env,clazz,"m_bScale","B");
		if(jApplv)
		{
			//Set the variable's value  to Java
			(*env)->SetByteField(env,object,jApplv,byAppLv);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetMapLevel GetFieldID jApplv Failed");
		}
	}

	return lRet;
}


/*********************************************************************
*	Function Name	: JNI_NE_GetMapLevelScale      	     *
*	Description	:                                            *
*	Date		: 11/11/27                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMapLevelScale
  (JNIEnv *env, jclass cls, jobject level, jobject scale)
{
	byte byAppLv;
	double dScale;

	jlong lRet = (jlong)NE_GetMapLevelScale(&byAppLv, &dScale);

	jclass byteClazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIByte");
	jclass doubleClazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIDouble");

	jfieldID jApplvID = (*env)->GetFieldID(env,byteClazz,"m_bScale","B");
	jfieldID jScaleID = (*env)->GetFieldID(env,doubleClazz,"m_dCommon","D");

	if(byteClazz && doubleClazz)
	{
		//Get variable's ID in JNI
		if(jApplvID && jScaleID)
		{
			//Set the variable's value  to Java
			(*env)->SetByteField(env,level,jApplvID,byAppLv);
			(*env)->SetDoubleField(env,scale,jScaleID,dScale);
		}
		else
		{
			LOGD("[DEBUG] JNI_NE_GetMapLevelScale Failed");
		}

	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_CalcAppLevelScale      	     *
*	Description	:                                            *
*	Date		: 11/11/27                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_CalcAppLevelScale
  (JNIEnv *env, jclass cls, jbyte oldLevel, jdouble oldScale, jobject newLevel, jobject newScale)
{
	char cOldAppLv =oldLevel;
	char cNewAppLv;
	double dOldScale =oldScale;
	double dNewScale;

	jclass byteClazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIByte");
	jclass doubleClazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIDouble");
	jfieldID jApplvID = (*env)->GetFieldID(env,byteClazz,"m_bScale","B");
	jfieldID jScaleID = (*env)->GetFieldID(env,doubleClazz,"m_dCommon","D");

	jlong lRet = (jlong)NE_CalcAppLevelScale(cOldAppLv, dOldScale, &cNewAppLv, &dNewScale);

	if(byteClazz && doubleClazz)
	{
		//Get variable's ID in JNI
		if(jApplvID && jScaleID)
		{
			//Set the variable's value  to Java
			(*env)->SetByteField(env,newLevel,jApplvID,cNewAppLv);
			(*env)->SetDoubleField(env,newScale,jScaleID,dNewScale);
		}
		else
		{
			LOGD("[DEBUG] JNI_NE_CalcAppLevelScale Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_CalcScale      	     *
*	Description	:                                            *
*	Date		: 12/01/20                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_CalcScale
  (JNIEnv *env, jclass cls,jbyte oldLevel, jdouble oldScale, jbyte newLevel, jobject newScale)
{
	char cOldAppLv =oldLevel;
	char cNewAppLv = newLevel;
	double dOldScale =oldScale;
	double dNewScale;

	jclass doubleClazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIDouble");
	jfieldID jScaleID = (*env)->GetFieldID(env,doubleClazz,"m_dCommon","D");


	jlong lRet = (jlong)NE_CalcScale(cOldAppLv, dOldScale, cNewAppLv, &dNewScale);

	if(doubleClazz)
	{
		//Get variable's ID in JNI
		if(jScaleID)
		{
			//Set the variable's value  to Java
			(*env)->SetDoubleField(env,newScale,jScaleID,dNewScale);
		}
		else
		{
			LOGD("[DEBUG] JNI_NE_CalcScale Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNICALL JNI_NE_ExposeMap      	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_ExposeMap
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_ExposeMap();
}

/*********************************************************************
*	Function Name	: JNI_NE_DrawSampleMap     	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_DrawSampleMap
   (JNIEnv *env, jclass cls,jint MapDParaMode,jlong Longitude,jlong Latitude,jbyte byAppLv,jobject object)
{
	SIZE DrawSize;

	jlong lRet = (jlong)NE_DrawSampleMap((ENE_MapDParaMode)MapDParaMode,(long)Longitude,(long)Latitude,(byte)byAppLv,&DrawSize);

	jclass clazz = (*env)->FindClass(env,"com/zdc/android/navi/JNISize");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID DrawSize_X = (*env)->GetFieldID(env,clazz,"m_SizeX","I");
		if(DrawSize_X)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,DrawSize_X,DrawSize.cx);
			jfieldID DrawSize_Y = (*env)->GetFieldID(env,clazz,"m_SizeY","I");
			if (DrawSize_Y)
			{
				(*env)->SetIntField(env,object,DrawSize_Y,DrawSize.cy);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_DrawSampleMap GetFieldID DrawSize_Y Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_DrawSampleMap GetFieldID DrawSize_X Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_CopyMap   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_CopyMap
  (JNIEnv *env, jclass cls,jobject object)
{
	RECT Rect;
	jlong lRet = (jlong)NE_CopyMap(0, &Rect);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIRect");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jleft = (*env)->GetFieldID(env,clazz,"m_lLeft","J");
		if(jleft)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env,object,jleft,(jlong)Rect.left);
			jfieldID jtop = (*env)->GetFieldID(env,clazz,"m_lTop","J");
			if (jtop)
			{
				(*env)->SetLongField(env,object,jtop,(jlong)Rect.top);
				jfieldID jright = (*env)->GetFieldID(env,clazz,"m_lRight","J");
				if (jright)
				{
					(*env)->SetLongField(env,object,jright,(jlong)Rect.right);
					jfieldID jbottom = (*env)->GetFieldID(env,clazz,"m_LBottom","J");
					if (jbottom)
					{
						(*env)->SetLongField(env,object,jright,(jlong)Rect.bottom);
					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_CopyMap GetFieldID jbottom Failed");
					}
				}
				else
				{
						LOGDp("[DEBUG] JNI_NE_CopyMap GetFieldID jright Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_DrawSampleMap GetFieldID jtop Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_DrawSampleMap GetFieldID jleft Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_GetAddressString   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetAddressString
  (JNIEnv *env, jclass cls,jlong Longitude,jlong Latitude,jobject object)
{
	char* AddressString  = NULL;

	if(0 == Longitude && 0 == Latitude){
		return CT_ERR_PARAM;
	}
	jlong lRet = (jlong)NE_GetAddressString((long)Longitude,(long)Latitude,&AddressString);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIString");
	if (clazz)
	{

		if(AddressString != NULL)
		{
			jfieldID jStrField = (*env)->GetFieldID(env,clazz,"m_StrAddressString","Ljava/lang/String;");
			jclass strClass = (*env)->FindClass(env, "java/lang/String");
			jmethodID ctorIDstr = (*env)->GetMethodID(env, strClass,  "<init>", "([BLjava/lang/String;)V");
			jbyteArray bytes = (*env)->NewByteArray(env, strlen(AddressString));
			if(bytes != NULL)
			{
				(*env)->SetByteArrayRegion(env, bytes, 0, strlen(AddressString), (jbyte*)AddressString);
			}
			jstring encoding = (*env)->NewStringUTF(env, "ShiftJIS");
			jstring  jstrszName = (jstring)(*env)->NewObject(env, strClass, ctorIDstr, bytes, encoding);
			//LOGDp("[DEBUG] JNI_NE_GetAddressString  NewStringUTF success @@@@@@@@@@@@@@@@@@@@");

			if(jstrszName)
			{
				//Set the variable's value  to Java
				(*env)->SetObjectField(env, object,jStrField,jstrszName);
			}

			if(NULL != bytes)
			{
				(*env)->DeleteLocalRef(env, bytes);
				bytes = NULL;
			}
			if(NULL != encoding)
			{
				(*env)->DeleteLocalRef(env, encoding);
				encoding = NULL;
			}
			if(NULL != jstrszName)
			{
				(*env)->DeleteLocalRef( env,jstrszName);
				jstrszName = NULL;
			}

			else
			{
				LOGDp("[DEBUG] JNI_NE_GetAddressString NewObject jstrszName Failed");
			}
		}
		else
		{
			//LOGDp("[DEBUG] JNI_NE_GetAddressString AddressString is NULL");
		}

		/*if(str1)
		{
			(*env)->SetObjectField(env,object,str1,(*env)->NewString(env,(jchar*)AddressString,strlen(AddressString)));
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetAddressString GetFieldID str1 Failed");
		}*/
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_GetAddressString FindClass JNIString Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_GetAddressCode   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetAddressCode
  (JNIEnv *env, jclass cls,jlong Longitude,jlong Latitude,jobject object)
{
	unsigned short WideCode = 0;
	unsigned short MiddleCode = 0;
	unsigned short NarrowCode = 0;

	//LOGDp("[DEBUG] JNI_NE_GetAddressCode  begin!!!!!!!!!!!!");
	jlong lRet = (jlong)NE_GetAddressCode((long)Longitude,(long)Latitude,&WideCode,&MiddleCode,&NarrowCode);
	//LOGDp("[DEBUG] JNI_NE_GetAddressCode WideCode:%d,MiddleCode:%d,NarrowCode:%d!!!!!!!!!!!!",WideCode,MiddleCode,NarrowCode);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIThreeShort");

	if(clazz)
	{
		//Get variable's ID in JNI
		//LOGDp("[DEBUG] JNI_NE_GetAddressCode jWideCode begin!!!!!!!!!!!!");
		jfieldID jWideCode = (*env)->GetFieldID(env,clazz,"m_sWideCode","S");
		if(jWideCode)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env,object,jWideCode,WideCode);
			//LOGDp("[DEBUG] JNI_NE_GetAddressCode jWideCode Sucessful!!!!!!!!!!!!");
			jfieldID jMiddleCode = (*env)->GetFieldID(env,clazz,"m_sMiddleCode","S");
			if (jMiddleCode)
			{
				(*env)->SetShortField(env,object,jMiddleCode,MiddleCode);
				jfieldID jNarrowCode = (*env)->GetFieldID(env,clazz,"m_sNarrowCode","S");
				if (jNarrowCode)
				{
					(*env)->SetShortField(env,object,jNarrowCode,NarrowCode);
				}
				else
				{
						LOGDp("[DEBUG] JNI_NE_GetAddressCode GetFieldID jNarrowCode Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_GetAddressCode GetFieldID jMiddleCode Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetAddressCode GetFieldID jWideCode Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_GetMyPosi   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMyPosi
  (JNIEnv *env, jclass cls,jobject object)
{
	long Longitude,Latitude;

	jlong lRet = (jlong)NE_GetMyPosi(&Longitude,&Latitude);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNITwoLong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jLongitude = (*env)->GetFieldID(env,clazz,"m_lLong","J");
		if(jLongitude)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env,object,jLongitude,Longitude);
			jfieldID jLatitude = (*env)->GetFieldID(env,clazz,"m_lLat","J");
			if(jLatitude)
			{
				(*env)->SetLongField(env,object,jLatitude,Latitude);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_GetMyPosi GetFieldID jLatitude Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetMyPosi GetFieldID jLongitude Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_GetMyPosiEx   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMyPosiEx
  (JNIEnv *env, jclass cls,jobject object,jobject object2)
{
	long Longitude,Latitude;
	short angle;

	jlong lRet = (jlong)NE_GetMyPosiEx(&Longitude,&Latitude,&angle);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNITwoLong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jLongitude = (*env)->GetFieldID(env,clazz,"m_lLong","J");
		if(jLongitude)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env,object,jLongitude,Longitude);
			jfieldID jLatitude = (*env)->GetFieldID(env,clazz,"m_lLat","J");
			if(jLatitude)
			{
				(*env)->SetLongField(env,object,jLatitude,Latitude);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_GetMyPosiEx GetFieldID jLatitude Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetMyPosiEx GetFieldID jLongitude Failed");
		}
	}

	jclass clazz2 = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIShort");

	if(clazz2)
	{
		//Get variable's ID in JNI
		jfieldID Count = (*env)->GetFieldID(env, clazz2,"m_sJumpRecCount","S");
		if(Count)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object2,Count,angle);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetMyPosiEx SetShortField Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_GetMyPosiEx FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_AjustMyPosi   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_AjustMyPosi
  (JNIEnv *env, jclass cls,jlong Longitude,jlong Latitude)
{
	return (jlong)NE_AjustMyPosi((long)Longitude,(long)Latitude);
}

/*********************************************************************
*	Function Name	: JNI_NE_AjustMyPosiEx   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_AjustMyPosiEx
  (JNIEnv *env, jclass cls,jlong Longitude,jlong Latitude,jshort angle, jboolean bRefresh)
{
	return (jlong)NE_AjustMyPosiEx((long)Longitude,(long)Latitude,(short)angle, bRefresh);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetNaviMode   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetNaviMode
  (JNIEnv *env, jclass cls,jobject object)
{
	ENE_NaviMode NaviMode;
	jlong lRet = (jlong)NE_GetNaviMode(&NaviMode);
	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	//LOGDp("[DEBUG] JNI_NE_GetNaviMode Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jNaviMode = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(jNaviMode)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,jNaviMode,(jint)NaviMode);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetNaviMode GetFieldID jNaviMode Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_GetNaviMode FindClass  Failed");
	}

	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_NE_GetNaviCarMode   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetNaviCarMode
  (JNIEnv *env, jclass cls,jobject object)
{
	ENE_NaviMode NaviMode;
	jlong lRet = (jlong)NE_GetNaviCarMode(&NaviMode);
	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	//LOGDp("[DEBUG] JNI_NE_GetNaviCarMode Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jNaviMode = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(jNaviMode)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,jNaviMode,(jint)NaviMode);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetNaviCarMode GetFieldID jNaviMode Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_GetNaviCarMode FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_SetNaviMode   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetNaviMode
  (JNIEnv *env, jclass cls,jint NaviMode)
{
	return (jlong)NE_SetNaviMode((ENE_NaviMode)NaviMode);
}

/*********************************************************************
*	Function Name	: JNI_NE_ActivateControl   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_ActivateControl
(JNIEnv *env, jclass cls,jint ActiveType,jint TransType,jbyte byAppLv,jdouble dScale,jlong Longitude,jlong Latitude,jfloat nAngle)
{
	//LOGD("[DEBUG] JNI_NE_ActivateControl Start!!!!!!!!!!!!!!!!");
	return (jlong)NE_ActivateControl((ENE_NEActiveType)ActiveType,(ENE_NETransitionType)TransType,0,(byte)byAppLv,(double)dScale,(long)Longitude,(long)Latitude, nAngle);
}

/*JNIEXPORT jlong JNICALL JNI_ct_UserTapUp
  (JNIEnv *env, jclass cls,jlong TapDwX,jlong TapDwY,jlong TapUpX,jlong TapUpY,jobject object)
{

	unsigned long SyncCtrl = 0;
	int Transition = 0;
	unsigned int AheadWndID = 0;
	unsigned int OriginWndID = 0;

	jlong lRet = (jlong)CT_UIC_TapUp((long)TapDwX,(long)TapDwY,(long)TapUpX,(long)TapUpY,&SyncCtrl,&Transition,&AheadWndID,&OriginWndID);

	jclass clazz = env->FindClass("com/zdc/android/navi/JNIUserTapData");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jSyncCtrl = env->GetFieldID(clazz,"SyncCtrl","J");
		if(jSyncCtrl)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,jSyncCtrl,SyncCtrl);
			jfieldID jTransition = env->GetFieldID(clazz,"Transition","I");
			if (jTransition)
			{
				env->SetIntField(object,jTransition,Transition);
				jfieldID jAheadWndID = env->GetFieldID(clazz,"AheadWndID","I");
				if (jAheadWndID)
				{
					env->SetIntField(object,jAheadWndID,AheadWndID);
					jfieldID jOriginWndID = env->GetFieldID(clazz,"OriginWndID","I");
					if(jOriginWndID)
					{
						env->SetIntField(object,jOriginWndID,OriginWndID);
					}
					else
					{
						LOGDp("[DEBUG] JNICALL JNI_ct_UserTapUp GetFieldID jOriginWndID Failed");
					}
				}
				else
				{
						LOGDp("[DEBUG] JJNICALL JNI_ct_UserTapUp GetFieldID jAheadWndID Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNICALL JNI_ct_UserTapUp GetFieldID jTransition Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNICALL JNI_ct_UserTapUp GetFieldID jSyncCtrl Failed");
		}
	}

	return lRet;
}*/

/*********************************************************************
*	Function Name	: JNI_ct_uic_UserScroll   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_UserScroll
  (JNIEnv *env, jclass cls,jlong TapDwX,jlong TapDwY,jlong TapUpX,jlong TapUpY)
{
	return (jlong)CT_UIC_UserScroll((long)TapDwX,(long)TapDwY,(long)TapUpX,(long)TapUpY);
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_MoveMapMyPosi   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_MoveMapMyPosi
  (JNIEnv *env, jclass cls)
{
	return (jlong)CT_UIC_MoveMapMyPosi();
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_SetMapLevel   	     *
*	Description	:                                            *
*	Date		: 12/12/18                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_SetMapLevel
  (JNIEnv *env, jclass cls, jbyte level)
{
	return (jlong)CT_UIC_MAP_SetLevel((byte)level);
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_MapLevelUp   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_MapLevelUp
  (JNIEnv *env, jclass cls)
{
	return (jlong)CT_UIC_MapLevelUp();
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_MapLevelDown   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_MapLevelDown
  (JNIEnv *env, jclass cls)
{
	return (jlong)CT_UIC_MapLevelDown();
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_SetMapPosition   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_SetMapPosition
  (JNIEnv *env,jclass cls,jbyte AppLevel,jdouble dScale,jlong Longitude,jlong Latitude,jshort Angle,jint ForceDraw,jint UpdateMapInfo)
{
	return (jlong)CT_UIC_SetMapPosition((byte)AppLevel,(double)dScale,(long)Longitude,(long)Latitude,(short)Angle,(int)ForceDraw,(int)UpdateMapInfo);
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_RestructScreenComposition   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_RestructScreenComposition
  (JNIEnv *env,jclass cls,jint ItsvMode)
{
	return (jlong)CT_UIC_RestructScreenComposition((ECT_ICU_IntensiveMode)ItsvMode);
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_UpdateGuideList   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_UpdateGuideList
  (JNIEnv *env, jclass cls)
{
	return (jlong)CT_UIC_UpdateGuideList();
}

/*********************************************************************
*	Function Name	: JNI_ct_SwitchNaviMode   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_SwitchNaviMode
  (JNIEnv *env, jclass cls)
{
	return (jlong)CT_SwitchNaviMode();
}

/*********************************************************************
*	Function Name	: JNI_CT_UIC_SetNUIModeBeforeSmoothScroll   	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_UIC_SetNUIModeBeforeSmoothScroll
  (JNIEnv *env, jclass cls,jint NUIMode)
{
	return (jlong)CT_UIC_SetNUIModeBeforeSmoothScroll((ENUI_MODE_SET_TYPE)NUIMode);
}

/*********************************************************************
*	Function Name	: JNI_CT_UIC_SmoothScrollMap  	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_UIC_SmoothScrollMap
  (JNIEnv *env, jclass cls)
{
	return (jlong)CT_UIC_SmoothScrollMap();
}

/*********************************************************************
*	Function Name	: JNI_CT_UIC_StopSmoothScroll  	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_UIC_StopSmoothScroll
  (JNIEnv *env, jclass cls)
{
	return (jlong)CT_UIC_StopSmoothScroll();
}

/*********************************************************************
*	Function Name	: JNI_NE_ForceRedraw  	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_ForceRedraw
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_ForceRedraw();
}

/*********************************************************************
*	Function Name	: JNI_ct_OnMPCommand  	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_OnMPCommand
  (JNIEnv *env, jclass cls,jlong EventID,jlong ResultCode)
{

	CT_OnMPCommand((unsigned long)EventID,(unsigned long)ResultCode);

	return (jlong)0;
}

/*********************************************************************
*	Function Name	: JNI_ct_OnNUICommand  	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_OnNUICommand
  (JNIEnv *env, jclass cls,jlong lEvent1, jlong lEVent2)
{
	unsigned long lEve1,lEve2;
	lEve1 = (long)lEvent1;
	lEve2 = (long)lEVent2;
	//LOGDp("JNI_ct_OnNUICommand proc 1 lEve1:%d,lEve2:%d",lEve1,lEve2);
	CT_OnNUICommand(lEve1, lEve2);
	//LOGDp("JNI_ct_OnNUICommand proc 2");
	return (jlong)0;
}

/*********************************************************************
*	Function Name	: JNI_CT_ReqRedraw  	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_ReqRedraw
  (JNIEnv *env, jclass cls)
{
	long ret = CT_ReqRedraw();
	return ret;
}

/*********************************************************************
*	Function Name	: JNI_ct_OnDGCommand  	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_OnDGCommand
  (JNIEnv *env, jclass cls,jlong ulnEventID,jlong ulnGPIndex)
{
	CT_OnDGCommand((unsigned long)ulnEventID,(unsigned long)ulnGPIndex);
	return 0;
}

/*********************************************************************
*	Function Name	: JNI_ct_OnWGCommand  	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_OnWGCommand
  (JNIEnv *env, jclass cls,jlong ulnEventID,jlong ulnRetCode)
{
	CT_OnWGCommand((unsigned long) ulnEventID, (unsigned long) ulnRetCode);
	return 0;
}
/*********************************************************************
*	Function Name	: JNI_ct_OnBGCommand  	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_OnBGCommand
  (JNIEnv *env, jclass cls,jlong ulnEventID,jlong ulnRetCode)
{
	CT_OnBGCommand((unsigned long) ulnEventID, (unsigned long) ulnRetCode);
	return 0;
}
/*********************************************************************
*	Function Name	: JNI_CT_SetDrawSurface  	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
long JNI_CT_SetDrawSurface(void* pDrawingTarget)
{
	long ret = 0;
	ret = CT_SetDrawSurface(pDrawingTarget);
	//ret = CT_ReqRedraw();
//Add 2011/09/16 Z01yoneya Start -->
	if(ret != CT_SUCCESS){
		return 0;
	}
//Add 2011/09/16 Z01yoneya End <--

	if(pDrawingTarget != NULL)
	{
	   NE_ExposeMap();
	}
	return 0;
}

/*
JNIEXPORT jlong JNICALL JNI_ct_uic_StartGuide
  (JNIEnv *env, jclass clz)
{
        int  ModeType = 1;
	return (jlong)ct_uic_StartGuide((ENUI_MODE_SET_TYPE)ModeType);
}
JNIEXPORT jlong JNICALL JNI_ct_uic_RefreshHighwayGuide
  (JNIEnv *env, jclass clz)
{
	int EventInfo = 1;
	return (jlong)ct_uic_RefreshHighwayGuide((ZNUI_EVENT_INFO)EventInfo);
}*/

/*********************************************************************
*	Function Name	: JNI_CT_UIC_SetScreenCoordinate  	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_UIC_SetScreenCoordinate
  (JNIEnv *env,jclass clz,jlong jTapX,jlong jTapY)
{
	long TapX = jTapX;
	long TapY = jTapY;
	jlong Ret = CT_UIC_SetScreenCoordinate(TapX,TapY);

	return Ret;
}

// Wangxp add start
/*********************************************************************
*	Function Name	: JNI_NE_SetMapScale  	     *
*	Description	:                                            *
*	Date		: 09/12/18                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetMapScale(JNIEnv *env, jclass cls,jint iMapScale)
{
	jlong Ret = 0;

	NE_SetMapScale((int)iMapScale);
	return Ret;
}
/*********************************************************************
*	Function Name	: JNI_DG_startDemoDrive  	     *
*	Description	:                                            *
*	Date		: 09/12/18                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_DG_startDemoDrive(JNIEnv *env, jclass cls,jint iDemoDrive)
{
	jlong Ret = 0;

	//DG_startDemoDrive((int)iDemoDrive);
	CT_Drv_SetGuidanceSts((bool)iDemoDrive);
	return Ret;
}
/*********************************************************************
*	Function Name	: JNI_DG_endDemoDrive  	     *
*	Description	:                                            *
*	Date		: 09/12/18                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_DG_endDemoDrive(JNIEnv *env, jclass cls,jint iDemoDrive)
{
	jlong Ret = 0;

	//DG_endDemoDrive((int)iDemoDrive);
	CT_Drv_SetGuidanceSts((bool)iDemoDrive);
	return Ret;
}
/*********************************************************************
*	Function Name	: JNI_NE_GetMapScale  	     *
*	Description	:                                            *
*	Date		: 09/12/18                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMapScale(JNIEnv *env, jclass cls, jobject object)
{
	jlong Ret = 0;
	int iMapScale = 0;

	iMapScale = NE_GetMapScale();

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jiMapScale = (*env)->GetFieldID(env, clazz,"m_iCommon","I");
		if(jiMapScale)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jiMapScale,(jint)iMapScale);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetMapScale SetIntField jiMapScale Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_GetMapScale FindClass  Failed!!!!!!!!!!!!!!!");
	}
	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_SetMapScale  	     *
*	Description	:                                            *
*	Date		: 10/04/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetMPMode(JNIEnv *env, jclass cls,jlong lMapMode)
{
	jlong Ret = 0;

	NE_SetMPMode((long)lMapMode);
	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_GetMPMode  	     *
*	Description	:                                            *
*	Date		: 10/04/21                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMPMode(JNIEnv *env, jclass cls, jobject object)
{
	jlong Ret = 0;
	long lMapMode = 0;

	lMapMode = NE_GetMPMode();

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jlMapMode = (*env)->GetFieldID(env, clazz,"lcount","J");
		if(jlMapMode)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlMapMode,(jlong)lMapMode);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetMPMode SetLongField jiMapScale Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_GetMPMode FindClass  Failed!!!!!!!!!!!!!!!");
	}
	return Ret;
}
/*********************************************************************
*	Function Name	: JNI_CT_ICU_ReqRefleshUI  	     *
*	Description	:                                            *
*	Date		: 2010/01/11                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_ICU_ReqRefleshUI(JNIEnv *env, jclass cls, jint iCtrlType)
{
	long Ret = 0;

	Ret = (jlong)CT_ICU_ReqRefleshUI( (ECT_ICU_CtrlType)iCtrlType );

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_CT_ICU_ReqRefleshMapUI  	     *
*	Description	:                                            *
*	Date		: 2010/01/11                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_ICU_ReqRefleshMapUI(JNIEnv *env, jclass cls, jint iCtrlType)
{
	long Ret = 0;

	Ret = (jlong)CT_ICU_ReqRefleshMapUI( (ECT_ICU_CtrlType)iCtrlType );

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetScaleDistance			*
*	Description	:                                            *
*	Date		: 2010/1/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetScaleDistance(JNIEnv *env, jclass clz, jobject object)
{
	long	lnDistance = 0;
	jlong	lRet = 0;

	Java_GetScaleDistance(&lnDistance);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jfldlnDistance = (*env)->GetFieldID(env, clazz,"lcount","J");
		if(jfldlnDistance)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jfldlnDistance,(jlong)lnDistance);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetScaleDistance SetLongField jfldlnDistance Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetScaleDistance FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}
// Wangxp add end

/*********************************************************************
*	Function Name	: JNI_LIB_dtmConvByDegree			*
*	Description	:                                            *
*	Date		: 2010/1/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_LIB_dtmConvByDegree
  (JNIEnv *env, jclass cls,jint a,jint b,jlong Lon,jlong Lat,jobject object)
{
	long arrayWgsCoor[3] = {0};
	long arrayCoor[3] = {0};
	arrayWgsCoor[0] = (long)Lat;
	arrayWgsCoor[1] = (long)Lon;


	jlong lRet = (jlong)LIB_dtmConvByDegree((int)a,(int)b,arrayWgsCoor,arrayCoor);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNITwoLong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jLongitude = (*env)->GetFieldID(env,clazz,"m_lLong","J");
		if(jLongitude)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env,object,jLongitude,arrayCoor[1]);
			jfieldID jLatitude = (*env)->GetFieldID(env,clazz,"m_lLat","J");
			if(jLatitude)
			{
				(*env)->SetLongField(env,object,jLatitude,arrayCoor[0]);
			}
			else
			{
				LOGDp("[DEBUG] JNI_JNI_LIB_dtmConvByDegree GetFieldID jLatitude Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_JNI_LIB_dtmConvByDegree GetFieldID jLongitude Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_ct_ExposeMap       	     *
*	Description	:                                            *
*	Date		: 2010/1/13                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_ExposeMap
 (JNIEnv *env, jclass cls)
{
	unsigned long ct_ExposeMap();
	return (jlong)ct_ExposeMap();
}
JNIEXPORT jlong JNICALL JNI_CT_ExposeMap
 (JNIEnv *env, jclass cls)
{
	return (jlong)CT_ExposeMap();
}
#ifdef DRAW_POILOG
JNIEXPORT jlong JNICALL JNI_MP_DM_SetCSVInfo(JNIEnv *env, jclass cls, jlong lFlag, jlong lLayer)
{
	long Ret = 0;

	Ret = (jlong)MP_DM_SetCSVInfo(lFlag, lLayer);

	return Ret;
}
#endif

/*********************************************************************
*	Function Name	: JNI_CT_MD_ConvWinPos2LonLat       	     *
*	Description	:                                            *
*	Date		: 2010/05/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_MD_ConvWinPos2LonLat
  (JNIEnv *env, jclass cls,jlong X,jlong Y,jobject object1 ,jobject object2,jbyte flag)
{
	long lRet = 0;

	long lnLongitude = 0;
	long lnLatitude = 0;

	lRet = CT_MD_ConvWinPos2LonLat((long)X,(long)Y,&lnLongitude,&lnLatitude,(BYTE)flag);

	jclass clazzLon = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");
	jclass clazzLat = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazzLon)
	{
		//Get variable's ID in JNI
		jfieldID jlnLon = (*env)->GetFieldID(env, clazzLon,"lcount","J");
		if(jlnLon)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object1,jlnLon,(jlong)lnLongitude);
		}
		else
		{
			LOGDp("[DEBUG] JNI_CT_MD_ConvWinPos2LonLat GetFieldID jlnLon Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_CT_MD_ConvWinPos2LonLat FindClass  Failed!!!!!!!!!!!!!!!");
	}

	if(clazzLat)
	{
		//Get variable's ID in JNI
		jfieldID jlnLat = (*env)->GetFieldID(env, clazzLat,"lcount","J");
		if(jlnLat)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object2,jlnLat,(jlong)lnLatitude);
		}
		else
		{
			LOGDp("[DEBUG] JNI_CT_MD_ConvWinPos2LonLat GetFieldID jlnLat Failed");
		}
	}

	return (jlong)lRet;
}


/*********************************************************************
*	Function Name	: JNI_CT_MD_ConvLonLat2WinPos       	     *
*	Description	:                                            *
*	Date		: 2011/01/18                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_MD_ConvLonLat2WinPos
  (JNIEnv *env, jclass cls,jlong lon,jlong lat,jobject object1 ,jobject object2,jbyte flag)
{
	long lRet = 0;

	long lnX = 0;
	long lnY = 0;

	lRet = CT_MD_ConvLonLat2WinPos((long)lon,(long)lat,&lnX,&lnY,(BYTE)flag);

	jclass clazzX = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");
	jclass clazzY = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazzX)
	{
		//Get variable's ID in JNI
		jfieldID jlnX = (*env)->GetFieldID(env, clazzX,"lcount","J");
		if(jlnX)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object1,jlnX,(jlong)lnX);
		}
		else
		{
			LOGD("[DEBUG] JNI_CT_MD_ConvLonLat2WinPos GetFieldID jlnX Failed");
		}
	}
	else
	{
		LOGD("[DEBUG] JNI_CT_MD_ConvLonLat2WinPos FindClass  Failed!!!!!!!!!!!!!!!");
	}

	if(clazzY)
	{
		//Get variable's ID in JNI
		jfieldID jlnY = (*env)->GetFieldID(env, clazzY,"lcount","J");
		if(jlnY)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object2,jlnY,(jlong)lnY);
		}
		else
		{
			LOGD("[DEBUG] JNI_CT_MD_ConvLonLat2WinPos GetFieldID jlnY Failed");
		}
	}

	return (jlong)lRet;
}

JNIEXPORT jlong JNICALL JNI_CT_DrawMap(JNIEnv *env, jclass cls,jintArray jarrayviewport,jfloat birdViewAngle,jboolean touching,jboolean animationEnable)
{

    jint* viewport = (*env)->GetIntArrayElements(env,jarrayviewport, NULL);

	long result = CT_DrawMap(viewport,birdViewAngle,touching,animationEnable);

	(*env)->ReleaseIntArrayElements(env,jarrayviewport, viewport, 0);

	return result;
}


JNIEXPORT jlong JNICALL JNI_CT_DrawMagMap(JNIEnv *env, jclass cls,jintArray jarrayviewport)
{

    jint* viewport = (*env)->GetIntArrayElements(env,jarrayviewport, NULL);

	long result = CT_DrawMagMap(viewport);

	(*env)->ReleaseIntArrayElements(env,jarrayviewport, viewport, 0);

	return result;
}

JNIEXPORT jfloat JNICALL JNI_CT_GetMapBirdviewAngle(JNIEnv *env, jclass cls)
{
	return CT_GetMapBirdviewAngle();
}
JNIEXPORT jboolean JNICALL JNI_CT_MapAnimating(JNIEnv *env, jclass cls)
{
	return CT_MapAnimating();
}
JNIEXPORT jlong JNICALL JNI_CT_GLResourceClear(JNIEnv *env, jclass cls)
{
	return CT_GLResourceClear();
}

/*********************************************************************
*	Function Name	: JNI_ct_SwitchVicsMode         	             *
*	Description	:                                                    *
*	Date		: 09/10/12                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_SwitchVicsMode
  (JNIEnv *env, jclass a)
{
	//LOGDp("[DEBUG] JNICALL JNI_ct_SwitchVicsMode begin!!!!!!!!!");
	return 0;
	// return (jlong)CT_SwitchVicsMode();
}
/*********************************************************************
*	Function Name	: JNI_NE_MP_SetGuideLineIsDisplayed              *
*	Description	:                                                    *
*	Date		: 2010/10/16                                         *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_MP_SetGuideLineIsDisplayed
  (JNIEnv *env, jclass clz,jlong bGuideLineIsDisplayed,jlong byDrawSurface)
{
	return (jlong)NE_MP_SetGuideLineIsDisplayed((BOOL)bGuideLineIsDisplayed,(BYTE)byDrawSurface);
}

/*********************************************************************
*	Function Name	: JNI_NE_MP_DestinationPoint                     *
*	Description	:                                                    *
*	Date		: 2010/10/16                                         *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_MP_DestinationPoint
  (JNIEnv *env, jclass clz,jlong lnLongitude,jlong lnLatitude)
{
	return (jlong)NE_MP_DestinationPoint((long)lnLongitude,(long)lnLatitude);
}

/*********************************************************************
*	Function Name	: JNI_NE_MP_SetDirectLineIsDisplayed              *
*	Description	:                                                    *
*	Date		: 2010/10/16                                         *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_MP_SetDirectLineIsDisplayed
  (JNIEnv *env, jclass clz,jlong bGuideLineIsDisplayed,jlong byDrawSurface)
{
	return (jlong)NE_MP_SetDirectLineIsDisplayed((BOOL)bGuideLineIsDisplayed,(BYTE)byDrawSurface);
}

/*********************************************************************
*	Function Name	: JNI_NE_MP_PointContainInVehicle              *
*	Description	:                                                    *
*	Date		: 2011/02/11                                         *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: wangmeng	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_MP_PointContainInVehicle
(JNIEnv *env, jclass clz,jlong ScreenCenterX,jlong ScreenCenterY,jobject object)
{
	long lnInVehicleFlag = 0;

	jlong lRet = (jlong)NE_MP_PointContainInVehicle(ScreenCenterX,ScreenCenterY,&lnInVehicleFlag);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID InVehicleFlag = (*env)->GetFieldID(env,clazz,"lcount","J");

		if(InVehicleFlag)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env,object,InVehicleFlag,(jlong)lnInVehicleFlag);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_MP_PointContainInVehicle GetFieldID InVehicleFlag Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_MP_PointContainInVehicle FindClass  Failed");
	}

	return lRet;
}

/**

*/

JNIEXPORT void JNICALL JNI_NE_MP_SetOffRoute(JNIEnv *env, jclass clz,jint jiOffRoute)
{
	NE__MP_SetOffRoute((int)jiOffRoute);
	LOGDp("JNI_NE_MP_SetOffRoute = : %d",(int)jiOffRoute);
	return ;
}

/**

*/

// Add 2011/04/08 add by libin for Changing Vehicle Status when Click Vehicle Start -->
JNIEXPORT void JNICALL JNI_NE_MP_SetClickVehicle(JNIEnv *env, jclass clz,jint jiClickVehicle)
{
	NE__MP_SetClickVehicle((int)jiClickVehicle);
	LOGDp("JNI_NE_MP_SetClickVehicle = : %d",(int)jiClickVehicle);
	return ;
}
// Add 2011/04/08 add by libin for Changing Vehicle Status when Click Vehicle End <--


//Add 2011/08/16 Z01thedoanh Start -->
JNIEXPORT jlong JNICALL JNI_MP_OutSidePOIIcon_GetIconData(JNIEnv *env, jclass clz, jint iKindCode,jint eIconSizeType,jobject object)
{
	jlong				lRet = 0;
	int					iPixArrLen = 0;
	ZMP_BITMAP_DATA  	stImage;
	jmethodID	mid;
	int* PixArray = NULL;
	unsigned short usKindCode = (unsigned short)iKindCode;
	memset(&stImage, 0, sizeof(ZMP_BITMAP_DATA));

	MP_OutSidePOIIcon_GetIconData(usKindCode,(ENE_MapIconSize)eIconSizeType,&stImage );
	//DAL_GetLandmarkData(usKindCode,&stImage );
         // Liugang DAL end
	short unWidth = (short)stImage.ucWidth;
	short unHeight = (short)stImage.ucHeight;
	short ucBitCountParPixel = (short)stImage.ucBitCountParPixel;
	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZLandmarkData_t");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jsWidth = (*env)->GetFieldID(env, clazz,"sWidth","S");
		if(jsWidth)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,jsWidth,(jshort)unWidth);
		}
		else
		{
			LOGDp("[DEBUG] JNI_MP_OutSidePOIIcon_GetIconData Getfield jsWidth Failed");
		}

		jfieldID jsHeight = (*env)->GetFieldID(env, clazz,"sHeight","S");
		if(jsHeight)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,jsHeight,(jshort)unHeight);
		}
		else
		{
			LOGDp("[DEBUG] JNI_MP_OutSidePOIIcon_GetIconData Getfield jsHeight Failed");
		}

		jfieldID jBitCountParPixel = (*env)->GetFieldID(env, clazz,"ucBitCountParPixel","S");
		if(jBitCountParPixel)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,jBitCountParPixel,(jshort)ucBitCountParPixel);
		}
		else
		{
			LOGDp("[DEBUG] JNI_MP_OutSidePOIIcon_GetIconData Getfield jsHeight Failed");
		}

		iPixArrLen = (unHeight) * (unWidth);
		PixArray = (int*)malloc(iPixArrLen*sizeof(int));
		memset(PixArray, 0, iPixArrLen*sizeof(int) );

		if( NULL != PixArray && NULL != stImage.pucData)
		{
			int pix;
			for(pix=0;pix<iPixArrLen;++pix)
			{
				int bgra = ((int*)stImage.pucData)[pix];
				bgra = (((bgra      ) & 0xFF000000) |
						((bgra << 16) & 0x00FF0000) |
						((bgra      ) & 0x0000FF00) |
						((bgra >> 16) & 0x000000FF));
				PixArray[pix] =bgra;
			}
		}

		mid = (*env)->GetMethodID(env,clazz,"initSPixRGBA32","(I)V");
		if(mid && iPixArrLen > 0)
		{
			//LOGDp("[DEBUG] JNI_DAL_GetLandmarkData  before CallObjectMethod  ");
			(*env)->CallVoidMethod(env, object, mid, (jint)iPixArrLen);
			//LOGDp("[DEBUG] JNI_DAL_GetLandmarkData  after CallObjectMethod  ");
		}
		else
		{
			LOGDp("[DEBUG] JNI_MP_OutSidePOIIcon_GetIconData  GetMethodID mid  failed");
		}

		jfieldID jfldSPix = (*env)->GetFieldID(env, clazz,"SPixRGBA32","[I");
		if(jfldSPix)
		{
			jintArray iArr = (*env)->NewIntArray(env, (jint)iPixArrLen);
			if(iArr)
			{
				(*env)->SetIntArrayRegion(env, iArr, 0, (jint)iPixArrLen, (jint*)PixArray);
				(*env)->SetObjectField(env, object,jfldSPix,iArr);

			}
			else
			{
				LOGDp("[DEBUG] JNI_MP_OutSidePOIIcon_GetIconData  NewIntArray  Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_MP_OutSidePOIIcon_GetIconData GetFieldID jfldSPix Failed");
		}

		if(PixArray)
		{
			free(PixArray);
			PixArray = NULL;
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetCompass FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;
}

//Add 2011/08/16 Z01thedoanh End <--


// ExternalAPI -->
JNIEXPORT jlong JNICALL JNI_NE_AddUserLayer(JNIEnv *env, jclass clz, jobject layerId )
{
	unsigned long ulRet = JNI_ERR_OK;

	unsigned long ulLayerId = 0;
	ulRet = NE_AddUserLayer( &ulLayerId );
	if( ulRet != NE_SUCCESS ){
		ulRet = JNI_ERR_ILLEGAL_STATE;
		goto _FUNC_END;
	}

	jclass jcJNILong = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");
	if (jcJNILong) {
		jfieldID jfldlCount = (*env)->GetFieldID(env, jcJNILong, "lcount", "J");
		if (jfldlCount) {
			(*env)->SetLongField(env, layerId, jfldlCount, (jlong) ulLayerId);
		}
	}
_FUNC_END:
	return ulRet;
}

JNIEXPORT jlong JNICALL JNI_NE_DeleteUserLayer(JNIEnv *env, jclass clz, jlong layerId )
{
	unsigned long ulRet = JNI_ERR_OK;

	ulRet = NE_DeleteUserLayer( layerId );
	if( ulRet != NE_SUCCESS ){
		ulRet = JNI_ERR_ILLEGAL_STATE;
		goto _FUNC_END;
	}

_FUNC_END:
	return ulRet;
}

JNIEXPORT jlong JNICALL JNI_NE_IsExistUserLayer(JNIEnv *env, jclass clz, jlong layerId, jobject existFlag )
{
	unsigned long ulRet = JNI_ERR_OK;

	bool bExist = NE_IsExistUserLayer(layerId );

	int nExistFlag = NE_TRUE;
	if (bExist) {
		nExistFlag = NE_TRUE;
	} else {
		nExistFlag = NE_FALSE;
	}

	jclass jcJNIInt = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIInt");
	if (jcJNIInt) {
		jfieldID jfldiCommon = (*env)->GetFieldID(env, jcJNIInt, "m_iCommon", "I");
		if (jfldiCommon) {
			(*env)->SetIntField(env, existFlag, jfldiCommon, (jint)nExistFlag );
		}
	}

_FUNC_END:
	return ulRet;
}

JNIEXPORT jlong JNICALL JNI_NE_AddUserFigure_Point( JNIEnv *env, jclass clz, jlong layerId, jlong lon, jlong lat, jstring name, jstring imagePath, jobject figureId )
{
	unsigned long ulRet = JNI_ERR_OK;
	char* pszName = NULL;
	int iNameLen = 0;
	char* pszImagePath = NULL;
	int iImagePathLen = 0;

	ulRet = JNI_CMN_GetJStringCharString( env, name, &pszName, &iNameLen  );
	if( ulRet != JNI_ERR_OK ) {
		goto _FUNC_END;
	}

	ulRet = JNI_CMN_GetJStringCharString( env, imagePath, &pszImagePath, &iImagePathLen );
	if( ulRet != JNI_ERR_OK ) {
		goto _FUNC_END;
	}

	ZNE_GeoPoint stPoint = {0};
	stPoint.lnLongitude = lon;
	stPoint.lnLatitude = lat;

	unsigned long ulFigureId = 0;
	long lRet = NE_AddUserFigure_Point( layerId, &stPoint, pszName, pszImagePath, &ulFigureId );
	if( lRet != NE_SUCCESS ){
		ulRet = JNI_ERR_ILLEGAL_STATE;
		goto _FUNC_END;
	}

// Add 2011/10/14 r.itoh Start -->
	jclass jcJNILong = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");
	if (jcJNILong) {
		jfieldID jfldlCount = (*env)->GetFieldID(env, jcJNILong, "lcount", "J");
		if (jfldlCount) {
			(*env)->SetLongField(env, figureId, jfldlCount, (jlong) ulFigureId);
		}
	}
// Add 2011/10/14 r.itoh End <--

_FUNC_END:
	JNI_CMN_FreeJStringCharString( &pszName );
	JNI_CMN_FreeJStringCharString( &pszImagePath );

	return ulRet;
}

JNIEXPORT jlong JNICALL JNI_NE_AddUserFigure_Line( JNIEnv *env, jclass clz, jlong layerId, jobjectArray locationArray, jstring name, jobject drawParam, jobject figureId )
{
	unsigned long ulRet = JNI_ERR_OK;

	jint i=0;
	ZNE_GeoPointArray stPointArray = {0};
	char* pszName = NULL;
	int iNameLen = 0;
	ZNE_UserPolyDrawParam stPolyDrawParam = {0};


	ulRet = JNI_CMN_GetUserPolyDrawParam(env, drawParam, &stPolyDrawParam );
	if( ulRet != JNI_ERR_OK ) {
		goto _FUNC_END;
	}

	ulRet = JNI_CMN_GetJStringCharString( env, name, &pszName, &iNameLen );
	if( ulRet != JNI_ERR_OK ) {
		goto _FUNC_END;
	}

	ulRet = JNI_CMN_GetGeoPointArray(env, locationArray, &stPointArray);
	if( ulRet != JNI_ERR_OK ) {
		goto _FUNC_END;
	}

	unsigned long ulFigureId = 0;
	long lRetNE = NE_AddUserFigure_Line( layerId, &stPointArray, pszName, &stPolyDrawParam, &ulFigureId );
	if( lRetNE != NE_SUCCESS ){
		ulRet = JNI_ERR_ILLEGAL_STATE;
		goto _FUNC_END;
	}

	jclass jcJNILong = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");
	if (jcJNILong) {
		jfieldID jfldlCount = (*env)->GetFieldID(env, jcJNILong, "lcount", "J");
		if (jfldlCount) {
			(*env)->SetLongField(env, figureId, jfldlCount, (jlong) ulFigureId);
		}
	}

_FUNC_END:
	JNI_CMN_FreeJStringCharString( &pszName );
	JNI_CMN_FreeGeoPointArray( &stPointArray );

	return ulRet;
}

JNIEXPORT jlong JNICALL JNI_NE_DeleteUserFigure( JNIEnv *env, jclass clz, jlong layerId, jlong figureId )
{
	return NE_DeleteUserFigure( layerId, figureId );
}

JNIEXPORT jlong JNICALL JNI_NE_DeleteAllUserFigure( JNIEnv *env, jclass clz )
{
	return NE_DeleteAllUserFigure();
}

JNIEXPORT jlong JNICALL JNI_Java_GetMapCenterInfo_UserFig(JNIEnv *env, jclass clz, jobject state, jobject figureId )
{
	long lnState = 0;
	unsigned long ulFigureId = 0;
	Java_GetMapCenterInfo_UserFig( &lnState, &ulFigureId );

	jclass jcJNIInt = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIInt");
	if (jcJNIInt) {
		jfieldID jfldiCommon = (*env)->GetFieldID(env, jcJNIInt, "m_iCommon", "I");
		if (jfldiCommon) {
			(*env)->SetIntField(env, state, jfldiCommon, (jint) lnState);
		}
	}

	jclass jcJNILong = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");
	if (jcJNILong) {
		jfieldID jfldlCount = (*env)->GetFieldID(env, jcJNILong, "lcount", "J");
		if (jfldlCount) {
			(*env)->SetLongField(env, figureId, jfldlCount, (jlong) ulFigureId);
		}
	}
}
/*********************************************************************
*	Function Name	: JNI_NE_SetGraphicModeScale                     *
*	Description	:                                                    *
*	Date		: 13/07/23                                           *
*	Parameter	: jfloat                                             *
*	Return Code	: none                                               *
*	Author		: N.Sasao                                            *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT void JNICALL JNI_NE_SetGraphicModeScale
  (JNIEnv *env, jclass cls, float fGraphicScale)
{
	NE_SetScaleDensity(fGraphicScale);
	return ;
}

// <-- ExternalAPI
