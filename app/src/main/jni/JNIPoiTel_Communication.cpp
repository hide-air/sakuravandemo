/********************************************************************************
* File Name	:	JNIPoiTel_Communication.cpp
* Created	:	2009/10/22
* Author	:	xiayx
* Model		:
* Description	:
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "JNIPoiTel_Communication.h"
#include "NaviEngineIF.h"
#include "NaviEngineCtrl.h"
#include "NaviEngine/CT/CTUICommand.h"
#include "NETypes.h"
#include "windows.h"
#include "NaviEngine/POI/POIStruct.h"
#include "jni/JNI_Common.h"
#include "CTICU.h"
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define LOG_TAG "NavigationJNI_NaviRun"
#define JNI_DEBUG

#include "debugPrint.h"

/*********************************************************************
*	Function Name	: JNI_NE_POITel_Clear         	             *
*	Description	:                                            *
*	Date		: 09/10/22                                   *
*	Parameter	:                                            *
*	Return Code	: void                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POITel_Clear
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POITel_Clear();
}

/*********************************************************************
*	Function Name	: JNI_NE_POITel_SearchList         	     *
*	Description	:                                            *
*	Date		: 09/10/22                                   *
*	Parameter	:                                            *
*	Return Code	: void                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POITel_SearchList
  (JNIEnv *env, jclass cls,jstring jstr)
{
	//LOGDp("[DEBUG]JNI_NE_POITel_SearchList Begin!!!!!!!!!!!");
       char* rtn = NULL;

       jclass clsstring = env->FindClass("java/lang/String");
       jstring strencode = env->NewStringUTF("utf-8");
       jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
       jbyteArray barr= (jbyteArray)env->CallObjectMethod(jstr, mid, strencode);
       jsize alen = env->GetArrayLength(barr);
       jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);
       if (alen > 0)
       {
                 rtn = new char[alen+1];
                 memcpy(rtn, ba, alen);
                 rtn[alen] = 0;
       }

       jlong Ret = NE_POITel_SearchList(rtn);
      //LOGDp("[DEBUG] JNI_NE_POITel_GetRecCount rtn:%s!!!!!!!!!!!!",rtn);
      env->ReleaseByteArrayElements(barr, ba, 0);
      env->DeleteLocalRef(strencode);
      if(rtn)
	{
		delete[] rtn;
		rtn = NULL;
	 }

	//LOGDp("[DEBUG]JNI_NE_POITel_SearchList End!!!!!!!!!!!");

      return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POITel_GetRecCount         	     *
*	Description	:                                            *
*	Date		: 09/10/22                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POITel_GetRecCount
  (JNIEnv *env, jclass cls,jobject object)
{

	unsigned long Listconut = 0;

	jlong lRet = (jlong)NE_POITel_GetRecCount(&Listconut);

	//LOGDp("[DEBUG] JNI_NE_POITel_GetRecCount Listconut:%d,lRet:%d!!!!!!!!!!!!",Listconut,lRet);

	jlong listconut = Listconut;
	//LOGE("[DEBUG]listconut is : listconut = %d", listconut);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POITel_GetRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POITel_GetRecCount SetLongField Count Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POITel_GetRecCount FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POITel_GetRecList         	     *
*	Description	:                                            *
*	Date		: 09/10/22                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POITel_GetRecList
  (JNIEnv *env, jclass a, jlong RecIndex, jlong RecCount, jobjectArray args, jobject object)
  {
	//LOGDp("[DEBUG] JNI_NE_POITel_GetRecList begin");

	long Count;
	long Index = RecIndex;
	long RecNum = RecCount;
	ZPOI_Tel_ListItem* pstRecs;

	jlong Ret = (jlong)NE_POITel_GetRecList(Index,RecNum,&pstRecs, &Count);

	jlong listconut = Count;
	//Fing Java's Class
	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	jmethodID ctorID = 0;

	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
	    ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}

	//LOGDp("[DEBUG] JNI_NE_POITel_GetRecList Sucessful");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POITel_GetRecList SetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POITel_GetRecList FindClass  Failed");
	}

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Tel_ListItem");


	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < Count; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POITel_GetRecList loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			jfieldID TelSize = env->GetFieldID(clazz,"m_lTelSize","J");
			if(TelSize)
			{
				env->SetLongField(obj,TelSize,pstRecs[i].lnTelSize);
				jfieldID Tel = env->GetFieldID(clazz,"m_Tel","Ljava/lang/String;");
				if(Tel)
				{
					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].pszTel));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].pszTel), (jbyte*)pstRecs[i].pszTel);
					}
					jstring encoding = env->NewStringUTF("ShiftJIS");

					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						//LOGDp("[DEBUG] JNI_NE_POITel_GetRecList jsName begin");
						env->SetObjectField(obj,Tel,jsName);
						//LOGDp("[DEBUG] JNI_NE_POITel_GetRecList jsName end");
					}
					if(NULL != bytes)
					{
						env->DeleteLocalRef( bytes);
						bytes = NULL;
					}
					if(NULL != encoding)
					{
						env->DeleteLocalRef( encoding);
						encoding = NULL;
					}
					if(NULL != jsName)
					{
						env->DeleteLocalRef( jsName);
						jsName = NULL;
					}
					//LOGDp("[DEBUG] JNI_NE_POITel_GetRecList m_lFullTelSize begin");
					//env->SetObjectField(obj,Tel,env->NewStringUTF((char *)pstRecs[i].pszTel));
					jfieldID FullTelSize = env->GetFieldID(clazz,"m_lFullTelSize","J");
					//LOGDp("[DEBUG] JNI_NE_POITel_GetRecList m_lFullTelSize End");
					if(FullTelSize)
					{
						//LOGDp("[DEBUG] JNI_NE_POITel_GetRecList SetLongField m_lFullTelSize begin");
						env->SetLongField(obj,FullTelSize,pstRecs[i].lnFullTelSize);
						jfieldID FullTel = env->GetFieldID(clazz,"m_FullTel","Ljava/lang/String;");
						if(FullTel)
						{
							jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].pszFullTel));
							if(bytes != NULL)
							{
								env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].pszFullTel), (jbyte*)pstRecs[i].pszFullTel);
							}
							jstring encoding = env->NewStringUTF("ShiftJIS");

							jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

							if(jsName)
							{
								env->SetObjectField(obj,FullTel,jsName);
							}
							if(NULL != bytes)
							{
								env->DeleteLocalRef( bytes);
								bytes = NULL;
							}
							if(NULL != encoding)
							{
								env->DeleteLocalRef( encoding);
								encoding = NULL;
							}
							if(NULL != jsName)
							{
								env->DeleteLocalRef( jsName);
								jsName = NULL;
							}

							jfieldID NameSize = env->GetFieldID(clazz,"m_lNameSize","J");
							if(NameSize)
							{
								env->SetLongField(obj,NameSize,pstRecs[i].lnNameSize);
								jfieldID Name = env->GetFieldID(clazz,"m_Name","Ljava/lang/String;");
								if(Name)
								{
									jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].pszName));
									if(bytes != NULL)
									{
										env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].pszName), (jbyte*)pstRecs[i].pszName);
									}
									jstring encoding = env->NewStringUTF("ShiftJIS");

									jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

									if(jsName)
									{
										env->SetObjectField(obj,Name,jsName);
									}
									if(NULL != bytes)
									{
										env->DeleteLocalRef( bytes);
										bytes = NULL;
									}
									if(NULL != encoding)
									{
										env->DeleteLocalRef( encoding);
										encoding = NULL;
									}
									if(NULL != jsName)
									{
										env->DeleteLocalRef( jsName);
										jsName = NULL;
									}
									//env->SetObjectField(obj,FullTel,env->NewStringUTF((char *)pstRecs[i].pszName));
									jfieldID Lon = env->GetFieldID(clazz,"m_lLon","J");
									if (Lon)
									{
										env->SetLongField(obj,Lon,pstRecs[i].lnLon);
										jfieldID Lat = env->GetFieldID(clazz,"m_lLat","J");
										if (Lat)
										{
											env->SetLongField(obj,Lat,pstRecs[i].lnLat);
										}
										else
										{
											LOGDp("[DEBUG] JNICALL JNI_NE_POITel_GetRecList GetFieldID Lat Failed");
										}
									}
									else
									{
										LOGDp("[DEBUG] JNICALL JNI_NE_POITel_GetRecList GetFieldID Lon Failed");
									}
								}
								else
								{
									LOGDp("[DEBUG] JNICALL JNI_NE_POITel_GetRecList GetFieldID Name Failed");
								}
							}
							else
							{
								LOGDp("[DEBUG] JNICALL JNI_NE_POITel_GetRecList GetFieldID NameSize Failed");
							}
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POITel_GetRecList GetFieldID FullTel Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POITel_GetRecList GetFieldID FullTelSize Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POITel_GetRecList GetFieldID Tel Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POITel_GetRecList GetFieldID TelSize Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POITel_GetRecList FindClass Failed!");
		}

		if(NULL != obj)
		{
			env->DeleteLocalRef( obj);
			obj = NULL;
		}
	}

	return Ret;
}

/*JNIEXPORT jlong JNICALL JNI_NE_POITel_GetProgressRate
  (JNIEnv *env, jclass cls,jobject object)
{
	long lRate = 0;
	jlong Ret = (jlong)NE_POITel_GetProgressRate(&lRate);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Rate = env->GetFieldID(clazz,"lcount","J");
		if(Rate)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Rate,lRate);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POITel_GetProgressRate GetLongField Rate Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POITel_GetProgressRate FindClass  Failed");
	}

	LOGDp("[DEBUG] JNI_NE_POITel_GetProgressRate Sucessful");

	return Ret;
}*/

/*********************************************************************
*	Function Name	: JNI_NE_POITel_Cancel         	             *
*	Description	:                                            *
*	Date		: 09/10/22                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POITel_Cancel
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POITel_Cancel();
}
