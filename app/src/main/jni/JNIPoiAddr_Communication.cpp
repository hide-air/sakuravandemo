/********************************************************************************
* File Name	:	 JNIPoiAddr_Communication.cpp
* Created	:	2009/10/12
* Author	:	xiayx
* Model		:
* Description	:
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "JNIPoiAddr_Communication.h"
#include "NaviEngineIF.h"
#include "NaviEngineCtrl.h"
#include "NaviEngine/CT/CTUICommand.h"
#include "NETypes.h"
#include "windows.h"
#include "NaviEngine/POI/POIStruct.h"
#include "jni/JNI_Common.h"
#include "CTICU.h"
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define LOG_TAG "NavigationJNI_NaviRun"
#define JNI_DEBUG

#include "debugPrint.h"

/*********************************************************************
*	Function Name	: JNI_NE_POIAddrKey_Clear         	     *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddrKey_Clear
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POIAddrKey_Clear();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_Clear           	     *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_Clear
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POIAddr_Clear();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_GetJumpList           	     *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_GetJumpList
  (JNIEnv *env, jclass clz,jobjectArray args,jobject obj)
{
	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList Begin!!!!!!!!!!!!!!");
	ZPOI_JUMPKEY *pstJumpkey;
	unsigned short JumpRecCount;
	jlong Ret = (jlong)NE_POIAddr_GetJumpList(&pstJumpkey,&JumpRecCount);

	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList JumpRecCount:%d!!!!!!!!!!!!",JumpRecCount);
	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList Begin_1!!!!!!!!!!!!!!");

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIJumpKey");

	jclass strClass = env->FindClass("java/lang/String");
	jmethodID ctorID = 0;
	if(strClass != NULL)
	{
	      ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}
#if 0
	if(clazz)
	{
		//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList FindClass Sucessful!!!!!!!!!!!!!!");
	}
#endif

	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < JumpRecCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList loop begin");

		jobject object = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		jfieldID NameSize = env->GetFieldID(clazz,"NameSize","J");
		if(NameSize)
		{
			//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList GetFieldID NameSize start!!!!!!!!!!!!!!");
			env->SetLongField(object,NameSize,pstJumpkey[i].lnNameSize);
			//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList SetLongField NameSize start!!!!!!!!!!!!!!");
			jfieldID Pucname = env->GetFieldID(clazz,"pucName","Ljava/lang/String;");
			if(Pucname)
			{
				//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList GetFieldID Pucname start!!!!!!!!!!!!!!");
// Chg 2011/05/23 katsuta Start -->
//				jbyteArray bytes = env->NewByteArray(strlen((char *)pstJumpkey[i].pcName));
				jbyteArray bytes = env->NewByteArray(strlen((char *)pstJumpkey[i].pucName));
// Chg 2011/05/23 katsuta End <--
				if(bytes != NULL)
				{
// Chg 2011/05/23 katsuta Start -->
//					env->SetByteArrayRegion(bytes, 0, strlen((char *)pstJumpkey[i].pcName), (jbyte*)pstJumpkey[i].pcName);
					env->SetByteArrayRegion(bytes, 0, strlen((char *)pstJumpkey[i].pucName), (jbyte*)pstJumpkey[i].pucName);
// Chg 2011/05/23 katsuta End <--
				}
				jstring encoding = env->NewStringUTF("ShiftJIS");

				jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

				if(jsName)
				{
					env->SetObjectField(object,Pucname,jsName);
				}

				jfieldID Offset = env->GetFieldID(clazz,"Offset","J");
				if(Offset)
				{
					env->SetLongField(object,Offset,pstJumpkey[i].lnNameSize);
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList GetFieldID Offset Failed");
				}

				if(NULL != bytes)
				{
					env->DeleteLocalRef( bytes);
					bytes = NULL;
				}
				if(NULL != encoding)
				{
					env->DeleteLocalRef( encoding);
					encoding = NULL;
				}
				if(NULL != jsName)
				{
					env->DeleteLocalRef( jsName);
					jsName = NULL;
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList GetFieldID Pucname Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG]JNI_NE_POIAddr_GetJumpList GetFieldID NameSize Failed!");
		}

		if(NULL != object)
		{
			env->DeleteLocalRef(object);
			object = NULL;
		}
	}

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNIShort");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"m_sJumpRecCount","S");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetShortField(obj,Count,JumpRecCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIAddr_GetJump GetLongField JumpRecCount Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIAddr_GetJump FindClass  Failed");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_GetJumpList2RecordIndex      *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_GetJumpList2RecordIndex
  (JNIEnv *env, jclass cls, jlong NameSize,jstring jstr,jobject object)
{
       //LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex NameSize1:%d!!!!!!!!!!!!",(long)NameSize);

       long RecIndex =0;
       unsigned char* rtn = NULL;
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応 START
	jsize alen = 0;
	jbyte* ba = NULL;

	jclass clsstring = env->FindClass(CLASS_CommonLib);
	jmethodID mid = env->GetStaticMethodID(clsstring, "getToSJIS", "(Ljava/lang/String;)[B");
	jbyteArray barr= (jbyteArray)env->CallStaticObjectMethod(clsstring, mid, jstr);
	if( barr != NULL ){
		alen = env->GetArrayLength(barr);
		ba = env->GetByteArrayElements(barr, JNI_FALSE);
       	if (alen > 0)
       	{
                 rtn = new unsigned char[alen+1];
                 memcpy(rtn, ba, alen);
                 rtn[alen] = 0;
       	}
	}
	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex rtn:%s!!!!!!!!!!!!",rtn);
	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex NameSize2:%d!!!!!!!!!!!!",(long)NameSize);
	jlong Ret = (jlong)NE_POIAddr_GetJumpList2RecordIndex((long)NameSize,rtn,&RecIndex);
	if( barr != NULL ){
	 	env->ReleaseByteArrayElements(barr, ba, 0);
	}
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応  END
	if(rtn)
	{
		delete[] rtn;
		rtn = NULL;
	 }
	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex RecIndex:%d!!!!!!!!!!!!",RecIndex);
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex GetFieldID  Index Begin");
		jfieldID Index = env->GetFieldID(clazz,"lcount","J");
		if(Index)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Index,(jlong)RecIndex);
			//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex SetLongField  Index End");
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex SetLongField Index Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex FindClass  Failed");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_GetRecCount      *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_GetRecCount
  (JNIEnv * env, jclass a, jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecCount begin!!!!!!!!!!!!");

	long Listconut = 0;

	jlong lRet = (jlong)NE_POIAddr_GetRecCount(&Listconut);

	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecCount Listconut:%d,lRet:%d!!!!!!!!!!!!",Listconut,lRet);

	jlong listconut = Listconut;
	//LOGE("[DEBUG]listconut is : listconut = %d", listconut);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecCount GetLongField Count Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecCount FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_GetRecList                  *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_GetRecList
  (JNIEnv *env, jclass a, jlong RecIndex, jlong RecCount, jobjectArray args, jobject object)
  {
	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecList begin");
	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecList RecIndex:%d,RecCount:%d!!!!!!!!!!!!",RecIndex,RecCount);
	long Count;
	//long Index = RecIndex;
	//long RecNum = RecCount;
	ZPOI_Address_ListItem* pstRecs;

	jlong Ret = (jlong)NE_POIAddr_GetRecList(RecIndex,RecCount,&pstRecs, &Count);

	jlong listconut = Count;
	//Fing Java's Class
	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}

	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecList Sucessful");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecList GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecList FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Address_ListItem");

	if(clazz)
	{
		//LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecList FindClass Sucessful!!!!!!!!!!!!!!");
	}

	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < Count; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecList loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			jfieldID AreaNumber = env->GetFieldID(clazz,"m_lAreaNumber","J");
			if(AreaNumber)
			{
				env->SetLongField(obj,AreaNumber,pstRecs[i].lnAreaNumber);
				jfieldID NameSize = env->GetFieldID(clazz,"m_lNameSize","J");
				if(NameSize)
				{
					env->SetLongField(obj,NameSize,pstRecs[i].lnNameSize);
					jfieldID Pucname = env->GetFieldID(clazz,"m_Name","Ljava/lang/String;");
					if(Pucname)
					{
						jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].pcName));
						if(bytes != NULL)
						{
							env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].pcName), (jbyte*)pstRecs[i].pcName);

						}
						jstring encoding = env->NewStringUTF("ShiftJIS");


						jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

						if(jsName)
						{
							env->SetObjectField(obj,Pucname,jsName);
						}

						if(NULL != bytes)
						{
							env->DeleteLocalRef( bytes);
							bytes = NULL;
						}
						if(NULL != encoding)
						{
							env->DeleteLocalRef( encoding);
							encoding = NULL;
						}
						if(NULL != jsName)
						{
							env->DeleteLocalRef( jsName);
							jsName = NULL;
						}
						//env->SetObjectField(obj,Pucname,env->NewStringUTF((char *)pstRecs[i].pcName));
						jfieldID Longitude = env->GetFieldID(clazz,"m_lLongitude","J");
						if(Longitude)
						{
							env->SetLongField(obj,Longitude,pstRecs[i].lnLongitude);
							jfieldID Latitude = env->GetFieldID(clazz,"m_lLatitude","J");
							if(Latitude)
							{
								env->SetLongField(obj,Latitude,pstRecs[i].lnLatitude);
								jfieldID Existenceflg = env->GetFieldID(clazz,"m_iNextCategoryFlag","I");
								if(Existenceflg)
								{
									env->SetIntField(obj,Existenceflg,pstRecs[i].bNextCategoryFlag);
								}
								else
								{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIAddr_GetRecList GetFieldID NextCategoryFlag Failed");
								}
							}
							else
							{
								LOGDp("[DEBUG] JNICALL JNI_NE_POIAddr_GetRecList GetFieldID Latitude Failed");
							}
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POIAddr_GetRecList GetFieldID Longitude Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecList GetFieldID pcName Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecList GetFieldID NameSize Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIAddr_GetRecList GetFieldID AreaNumber Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POIAddr_GetRecList FindClass Failed!");
		}

		if(NULL != obj)
		{
			env->DeleteLocalRef( obj);
			obj = NULL;
		}
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_Abort                       *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_Abort
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POIAddr_Abort();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_GetProgressRate             *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_GetProgressRate
  (JNIEnv *env, jclass cls,jobject object)
{
	long lRate = 0;
	jlong Ret = (jlong)NE_POIAddr_GetProgressRate(&lRate);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Rate = env->GetFieldID(clazz,"lcount","J");
		if(Rate)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Rate,lRate);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIAddr_GetProgressRate GetLongField Rate Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIAddr_GetProgressRate FindClass  Failed");
	}

	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetProgressRate Sucessful");

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_SetAddrKey                  *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_SetAddrKey
  (JNIEnv *env, jclass cls, jshort WideCode,jshort MiddleCode,jshort NarrowCode)
{
	return (jlong)NE_POIAddr_SetAddrKey((unsigned short)WideCode,(unsigned short)MiddleCode,(unsigned short)NarrowCode);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_AddrIndex                   *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_AddrIndex
  (JNIEnv *env, jclass cls,jobject object)
{
	long lIndex = 0;
	jlong Ret = (jlong)NE_POIAddr_AddrIndex(&lIndex);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Index = env->GetFieldID(clazz,"lcount","J");
		if(Index)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Index,lIndex);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIAddr_AddrIndex GetLongField Index Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIAddr_AddrIndex FindClass  Failed");
	}

	//LOGDp("[DEBUG] JNI_NE_POIAddr_AddrIndex Sucessful");

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_SearchNextList              *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_SearchNextList
  (JNIEnv *env, jclass cls,jlong RecIndex)
{
	return (jlong)NE_POIAddr_SearchNextList((long)RecIndex);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_SearchPrevList              *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_SearchPrevList
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POIAddr_SearchPrevList();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_GetNextTreeIndex             *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_GetNextTreeIndex
  (JNIEnv *env, jclass cls,jlong RecIndex,jobject object)
{
	unsigned long TreeIndex = 0;
	jlong Ret = (jlong)NE_POIAddr_GetNextTreeIndex((long)RecIndex,&TreeIndex);

	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetNextTreeIndex TreeIndex:%d!!!!!!!!!!!!",TreeIndex);
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		//LOGDp("[DEBUG] JNI_NE_POIAddr_GetNextTreeIndex GetFieldID Begiin");
		jfieldID jTreeIndex = env->GetFieldID(clazz,"lcount","J");
		if(jTreeIndex)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,jTreeIndex,(jlong)TreeIndex);
			//LOGDp("[DEBUG] JNI_NE_POIAddr_GetNextTreeIndex SetLongField End");
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIAddr_GetNextTreeIndex GetLongField jTreeIndex Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIAddr_GetNextTreeIndex FindClass  Failed");
	}

	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetNextTreeIndex Sucessful");

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIAddr_GetPrevTreeIndex             *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIAddr_GetPrevTreeIndex
  (JNIEnv *env, jclass cls,jobject object)
{
	unsigned long TreeIndex = 0;
	jlong Ret = (jlong)NE_POIAddr_GetPrevTreeIndex(&TreeIndex);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jTreeIndex = env->GetFieldID(clazz,"lcount","J");
		if(TreeIndex)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,jTreeIndex,(jlong)TreeIndex);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIAddr_GetNextTreeIndex GetLongField jTreeIndex Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIAddr_GetNextTreeIndex FindClass  Failed");
	}

	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetNextTreeIndex Sucessful");

	return Ret;
}
