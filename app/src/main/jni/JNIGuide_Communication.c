/********************************************************************************
* File Name	:	 JNIGuide_Communication.cpp
* Created	:	2009/11/21
* Author	:	xiayx
* Model		:
* Description	:
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "JNIGuide_Communication.h"
#include "NaviEngineIF.h"
#include "NaviEngineCtrl.h"
#include "CTUICommand.h"
#include "NETypes.h"
#include "windows.h"
#include "NaviEngine/POI/POIStruct.h"
#include "NaviEngine/DG/DGArray.h"
#include "jni/JNI_Common.h"
#include "CTICU.h"
#include "NaviUI.h"
// Liugang DAL start
#include "CTCarGuide.h"
#include "CTManGuide.h"
#include "WGIF.h"
#include "DGIF.h"
//#include "HMAPDAL.h"
#include "DALTYPE.h"
#include "DALIF.h"
// Liugang DAL end
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "debugPrint.h"

#ifdef LOG_TAG
#undef LOG_TAG
#define LOG_TAG "NavigationJNI_NaviRun"
#endif

#define JNI_DEBUG

// Wangxp add start
static ZDGHighwayGuideInfo	 g_stHighwayGuideInfostInfo;

// Wangxp add end

/*********************************************************************
*	Function Name	: JNI_ct_uic_StartGuide        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_StartGuide
  (JNIEnv *env, jclass clz,jlong bStartVoice)
{
        int  ModeType = 1;
	return (jlong)CT_UIC_StartGuide((ENUI_MODE_SET_TYPE)ModeType,(bool)bStartVoice);
}
/*********************************************************************
*	Function Name	: JNI_ct_uic_SuspendGuide        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_SuspendGuide
  (JNIEnv *env, jclass clz)
{
	return (jlong)CT_UIC_SuspendGuide();
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_ResumeGuide        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_ResumeGuide
  (JNIEnv *env, jclass clz)
{
	return (jlong)CT_UIC_ResumeGuide();
}

/*********************************************************************
*	Function Name	: JNI_NE_GetGuideStat        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetGuideStat
  (JNIEnv *env, jclass clz,jobject object)
{
	int Stat = 0;
	ENE_GuideStat GuideStat = (ENE_GuideStat)Stat;

	jlong lRet = (jlong)NE_GetGuideStat(&GuideStat);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_GetNaviMode Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jMode = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(jMode)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,jMode,(jint)GuideStat);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetGuideStat GetLongField jMode Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_GetGuideStat FindClass Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_StopGuidance        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_StopGuidance
  (JNIEnv *env, jclass clz)
{
	return (jlong)NE_StopGuidance();
}
/*********************************************************************
*	Function Name	: JNI_ct_uic_ShowRestoreRoute        	     *
*	Description	:                                            *
*	Date		: 10/12/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: wangmeng	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_ShowRestoreRoute
  (JNIEnv *env, jclass clz)
{
	return (jlong)CT_UIC_ShowRestoreRoute();
}
/*********************************************************************
*	Function Name	: JNI_ct_uic_StopGuide        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_StopGuide
  (JNIEnv *env, jclass clz)
{
	unsigned int AheadWndID;
	unsigned int OriginWndID;
        int TransitionType;

	int SetType = 1;

	return (jlong)CT_UIC_StopGuide((ENUI_MODE_SET_TYPE)SetType,&AheadWndID,&OriginWndID,&TransitionType);
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_ChangeNaviMode        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_ChangeNaviMode
  (JNIEnv *env, jclass clz)
{
	return (jlong)CT_UIC_ChangeNaviMode();
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_ChangeNaviMode        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_RefreshHighwayGuide
  (JNIEnv *env, jclass clz)
{
	int EventInfo = 1;
	return (jlong)CT_UIC_RefreshHighwayGuide((ZNUI_EVENT_INFO)EventInfo);
}

// Wangxp add start

/*********************************************************************
*	Function Name	: JNI_Java_GetDestination      *
*	Description	:                                            *
*	Date		: 09/12/01                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_Java_GetDestination
  (JNIEnv *env, jclass clz,jobject object)
{
	ZNUI_DESTINATION_DATA_JAVA		stDestInfo;
	jmethodID ctorID;
	jobject  obj_ClassTime = 0;
	jlong Ret = 0;

	memset( &stDestInfo, 0, sizeof( stDestInfo ) );

	Ret = (jlong)Java_GetDestinationInfo(&stDestInfo);

	jclass ClassDestinationData = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNUI_DESTINATION_DATA");
	jclass ClassTime = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNUI_TIME");

	jfieldID jfield_ZNUI_TIME = (*env)->GetFieldID(env, ClassDestinationData,"stTime","Lnet/zmap/android/pnd/v2/data/ZNUI_TIME;");

	if(ClassTime != NULL)
	{
		ctorID = (*env)->GetMethodID(env, ClassTime, "<init>", "()V");

		obj_ClassTime = (jobject)(*env)->NewObject(env, ClassTime, ctorID, NULL);

		if(NULL == obj_ClassTime)
		{
			LOGDp("[DEBUG] JNI_Java_GetDestination  NewObject obj_ClassTime Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		jfieldID jHouer = (*env)->GetFieldID(env, ClassTime,"nHouer","I");

		if(jHouer)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, obj_ClassTime,jHouer,(stDestInfo.stTime).nHouer);
			//LOGDp("[DEBUG] JNI_Java_GetDestination  SetIntField  jHouer @@@@@@@@@@@@@@@@@@@@");
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetDestination GetFieldID jHouer Failed");
		}

		jfieldID jMinute = (*env)->GetFieldID(env, ClassTime,"nMinute","I");

		if(jMinute)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, obj_ClassTime,jMinute,(stDestInfo.stTime).nMinute);
			//LOGDp("[DEBUG] JNI_Java_GetDestination  setIntField  jMinute @@@@@@@@@@@@@@@@@@@@");
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetDestination GetFieldID jMinute Failed");
		}

	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetDestination  Find  class ZNUI_TIME Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	if(ClassDestinationData != NULL)
	{

		(*env)->SetObjectField(env, object, jfield_ZNUI_TIME, obj_ClassTime);

		jfieldID jlnDistance = (*env)->GetFieldID(env, ClassDestinationData,"lnDistance","J");
		if(jlnDistance)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnDistance,stDestInfo.lnDistance);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetDestination GetFieldID jlnDistance Failed");
		}

		jfieldID jlnToll = (*env)->GetFieldID(env, ClassDestinationData,"lnToll","J");
		if(jlnToll)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnToll,stDestInfo.lnToll);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetDestination GetFieldID jlnToll Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetDestination  Find class ZNUI_DESTINATION_DATA Failed!!!@@@@@@@@@@@@@@@@@@@@");

	}

	if(NULL != obj_ClassTime)
	{
		(*env)->DeleteLocalRef(env, obj_ClassTime);
		obj_ClassTime = NULL;
	}
	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_DG_GetNextHighwayGuideInfoIdx      *
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_DG_GetNextHighwayGuideInfoIdx(JNIEnv *env, jclass clz,jobject object)
{
	long	lnGroupIdx = 0;
	long	lnListIdx = 0;
	jlong	lRet = 0;

	lRet = DG_GetNextHighwayGuideInfoIdx(&lnGroupIdx, &lnListIdx);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoLong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jlnGroupIdx = (*env)->GetFieldID(env, clazz,"m_lLong","J");
		if(jlnGroupIdx)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnGroupIdx,(jlong)lnGroupIdx);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetNextHighwayGuideInfoIdx SetLongField jlnGroupIdx Failed");
		}

		jfieldID jlnListIdx = (*env)->GetFieldID(env, clazz,"m_lLat","J");
		if(jlnListIdx)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnListIdx,(jlong)lnListIdx);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetNextHighwayGuideInfoIdx SetLongField jlnListIdx Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetNextHighwayGuideInfoIdx FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_DG_GetHighwayGuideListNum			*
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_DG_GetHighwayGuideListNum(JNIEnv *env, jclass clz, jlong lnGroupIdx, jobject object)
{
	long	lnListNum = 0;
	jlong	lRet = 0;

	//LOGDp("[DEBUG] JNI_DG_GetHighwayGuideListNum  before call DG_GetHighwayGuideListNum");
	DG_GetHighwayGuideListNum((long)lnGroupIdx, &lnListNum);
	//LOGDp("[DEBUG] JNI_DG_GetHighwayGuideListNum  after call DG_GetHighwayGuideListNum");

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jlnListNum = (*env)->GetFieldID(env, clazz,"lcount","J");
		if(jlnListNum)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnListNum,(jlong)lnListNum);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetHighwayGuideListNum SetLongField jlnListNum Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetHighwayGuideListNum FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_DG_HighwayGuideInfo_new			*
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_DG_HighwayGuideInfo_new
(JNIEnv *env, jclass clz, jlong lnGroupIdx, jlong lnListIdx,jobject object)
{
	int *temp = NULL;
	jlong	lRet = 0;
	jmethodID	mid;
	ZDGHighwayGuideInfo	 stInfo;
	memset(&stInfo, 0, sizeof(ZDGHighwayGuideInfo));
	memset( &g_stHighwayGuideInfostInfo, 0, sizeof(g_stHighwayGuideInfostInfo) );

	lRet = DG_HighwayGuideInfo_new( (long)lnGroupIdx, (long)lnListIdx, &stInfo);

	if(lRet!=0)
	{
		LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new   call DG_HighwayGuideInfo_new  Failure!!!");
	}
	else
	{
		//LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new   call DG_HighwayGuideInfo_new  success");
	}


	if(stInfo.unSAPAServiceCodeNum > 0)
	{
		temp = (int*)malloc(stInfo.unSAPAServiceCodeNum * sizeof(int));
	}

	int i = 0;
	if(temp != NULL)
	{
		for(i=0; i < stInfo.unSAPAServiceCodeNum; i++)
		{
				temp[i] = (int)stInfo.punSAPAServiceCode[i];
		}
	}

	memcpy(&g_stHighwayGuideInfostInfo, &stInfo, sizeof(stInfo));

	jclass ClassHighwayGuideInfo = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZDGHighwayGuideInfo");

	if(ClassHighwayGuideInfo != NULL)
	{
		if(stInfo.szName != NULL)
		{
			jfieldID jStrField= (*env)->GetFieldID(env, ClassHighwayGuideInfo,"szName","Ljava/lang/String;");
			jclass strClass = (*env)->FindClass(env, "java/lang/String");
			jmethodID ctorIDstr = (*env)->GetMethodID(env, strClass,  "<init>", "([BLjava/lang/String;)V");
			jbyteArray bytes = (*env)->NewByteArray(env, strlen((char*)stInfo.szName));
			if(bytes != NULL)
			{
				(*env)->SetByteArrayRegion(env, bytes, 0, strlen(stInfo.szName), (jbyte*)stInfo.szName);
			}
			jstring encoding = (*env)->NewStringUTF(env, "ShiftJIS");
			jstring  jstrszName = (jstring)(*env)->NewObject(env, strClass, ctorIDstr, bytes, encoding);

			if(jstrszName)
			{
				//Set the variable's value  to Java
				(*env)->SetObjectField(env, object,jStrField,jstrszName);
			}
			else
			{
				LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new NewObject jstrszName Failed");
			}

			if(NULL != bytes)
			{
				(*env)->DeleteLocalRef(env, bytes);
				bytes = NULL;
			}
			if(NULL != encoding)
			{
				(*env)->DeleteLocalRef( env,encoding);
				encoding = NULL;
			}
			if(NULL != jstrszName)
			{
				(*env)->DeleteLocalRef(env, jstrszName);
				jstrszName = NULL;
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new stInfo.szName is NULL");
		}

		jfieldID jiKindCode = (*env)->GetFieldID(env, ClassHighwayGuideInfo,"iKindCode","I");
		if(jiKindCode)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jiKindCode,(jint)stInfo.eKindCode);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new GetFieldID jiKindCode Failed");
		}

		jfieldID junSAPAServiceCodeNum = (*env)->GetFieldID(env, ClassHighwayGuideInfo,"unSAPAServiceCodeNum","S");
		if(junSAPAServiceCodeNum)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,junSAPAServiceCodeNum,(jshort)stInfo.unSAPAServiceCodeNum);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new GetFieldID junSAPAServiceCodeNum Failed");
		}

		mid = (*env)->GetMethodID(env,ClassHighwayGuideInfo,"initUnSAPAServiceCode","(I)V");
		if(mid)
		{
			(*env)->CallVoidMethod(env, object, mid, (jshort)stInfo.unSAPAServiceCodeNum);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new  GetMethodID mid  failed");
		}

		jfieldID junSAPAServiceCode = (*env)->GetFieldID(env, ClassHighwayGuideInfo,"unSAPAServiceCode","[I");
		if(junSAPAServiceCode)
		{
			jintArray sArr = (*env)->NewIntArray(env, (jshort)stInfo.unSAPAServiceCodeNum);
			if(sArr)
			{
				(*env)->SetIntArrayRegion(env, sArr, 0, (jshort)stInfo.unSAPAServiceCodeNum, (jint *)temp);
				(*env)->SetObjectField(env, object,junSAPAServiceCode,sArr);
			}
			else
			{
				LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new  NewShortArray  Failed");
			}

			if(NULL != sArr)
			{
				(*env)->DeleteLocalRef( env,sArr);
				sArr = NULL;
			}
			//(*env)->SetShortField(env, object,junSAPAServiceCode,*(stInfo.punSAPAServiceCode));
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new GetFieldID junSAPAServiceCode Failed");
		}

		/*jfieldID junSAPATextSizeByte = (*env)->GetFieldID(env, ClassHighwayGuideInfo,"unSAPATextSizeByte","S");
		if(junSAPATextSizeByte)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,junSAPATextSizeByte,(jshort)stInfo.unSAPATextSizeByte);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new GetFieldID junSAPATextSizeByte Failed");
		}*/

		/*if(stInfo.pszSAPAText != NULL)
		{
			jfieldID jSAPATextField= (*env)->GetFieldID(env, ClassHighwayGuideInfo,"pszSAPAText","Ljava/lang/String;");
			jclass strSAPATextClass = (*env)->FindClass(env, "java/lang/String");
			jmethodID ctorIDSAPAText = (*env)->GetMethodID(env, strSAPATextClass,  "<init>", "([BLjava/lang/String;)V");
			jbyteArray bytes2 = (*env)->NewByteArray(env, strlen((char*)stInfo.pszSAPAText));
			if(bytes2 != NULL)
			{
				(*env)->SetByteArrayRegion(env, bytes2, 0, strlen((char*)stInfo.pszSAPAText), (jbyte*)stInfo.pszSAPAText);
			}
			jstring encodingText = (*env)->NewStringUTF(env, "ShiftJIS");
			jstring  jstrSAPAText = (jstring)(*env)->NewObject(env, strSAPATextClass, ctorIDSAPAText, bytes2, encodingText);

			if(jstrSAPAText)
			{
				//Set the variable's value  to Java
				(*env)->SetObjectField(env, object,jSAPATextField,jstrSAPAText);
			}
			else
			{
				LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new NewObject jstrSAPAText Failed");
			}

			if(NULL != bytes2)
			{
				(*env)->DeleteLocalRef( env,bytes2);
				bytes2 = NULL;
			}
			if(NULL != encodingText)
			{
				(*env)->DeleteLocalRef( env,encodingText);
				encodingText = NULL;
			}
			if(NULL != jstrSAPAText)
			{
				(*env)->DeleteLocalRef( env,jstrSAPAText);
				jstrSAPAText = NULL;
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new stInfo.pszSAPAText  is NULL");
		}*/

		jfieldID julnDistMeter = (*env)->GetFieldID(env, ClassHighwayGuideInfo,"ulnDistMeter","J");
		if(julnDistMeter)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,julnDistMeter,(jlong)stInfo.ulnDistMeter);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new GetFieldID julnDistMeter Failed");
		}

		jfieldID julnTollChargeYenr = (*env)->GetFieldID(env, ClassHighwayGuideInfo,"ulnTollChargeYen","J");
		if(julnTollChargeYenr)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,julnTollChargeYenr,(jlong)stInfo.ulnTollChargeYen);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new GetFieldID julnTollChargeYenr Failed");
		}

		jfieldID julnRestTimeMinute = (*env)->GetFieldID(env, ClassHighwayGuideInfo,"ulnRestTimeMinute","J");
		if(julnRestTimeMinute)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,julnRestTimeMinute,(jlong)stInfo.ulnRestTimeMinute);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_HighwayGuideInfo_new GetFieldID julnRestTimeMinute Failed");
		}
//Del 2011/10/24 Z01h_yamada Start --> CID:10780 リソースリーク
//		if(temp != NULL)
//		{
//			free(temp);
//			temp = NULL;
//		}
//Del 2011/10/24 Z01h_yamada End <--

	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetDestination  Find class ZDGHighwayGuideInfo Failed!!!@@@@@@@@@@@@@@@@@@@@");

	}
//Add 2011/10/24 Z01h_yamada Start --> CID:10780 リソースリーク
	if(temp != NULL)
	{
		free(temp);
		temp = NULL;
	}
//Add 2011/10/24 Z01h_yamada End <--
	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_DG_HighwayGuideInfo_free		*
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_DG_HighwayGuideInfo_free
(JNIEnv *env, jclass clz, jlong lnGroupIdx, jlong lnListIdx,jobject object)
{
	jlong	lRet = 0;
	lRet = DG_HighwayGuideInfo_free((long)lnGroupIdx, (long)lnListIdx, &g_stHighwayGuideInfostInfo);
	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetGuideSts		*
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetGuideSts
(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	long    lnSts = 0;
	//LOGDp("[DEBUG] JNI_Java_GetGuideSts  before call Java_GetGuideSts");
	Java_GetGuideSts(&lnSts);
	//LOGDp("[DEBUG] JNI_Java_GetGuideSts  after call Java_GetGuideSts");

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jlnSts = (*env)->GetFieldID(env, clazz,"lcount","J");
		if(jlnSts)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnSts,(jlong)lnSts);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuideSts SetLongField jlnSts Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetGuideSts FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_Java_GetSimpleGuideAt      *
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetSimpleGuideAt(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	ZNE_EventInfo    stEventInfo;
	memset(&stEventInfo, 0, sizeof(ZNE_EventInfo));

	//LOGDp("[DEBUG] JNI_Java_GetSimpleGuideAt  before call Java_GetSimpleGuideAt");
	Java_GetSimpleGuideAt( &stEventInfo );
	//LOGDp("[DEBUG] JNI_Java_GetSimpleGuideAt  after call Java_GetSimpleGuideAt");

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIThreeLong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jlnInAbsLinkID = (*env)->GetFieldID(env, clazz,"lnNum1","J");
		if(jlnInAbsLinkID)
		{
#ifdef _NEW_HDAL_
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnInAbsLinkID,(jlong)stEventInfo.stInLinkID.pclLinkId);
#else
			(*env)->SetLongField(env, object,jlnInAbsLinkID,(jlong)stEventInfo.ulnInAbsLinkID);
#endif
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetSimpleGuideAt SetLongField jlnInAbsLinkID Failed");
		}

		jfieldID jlnOutAbsLinkID = (*env)->GetFieldID(env, clazz,"lnNum2","J");
		if(jlnOutAbsLinkID)
		{
#ifdef _NEW_HDAL_
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnOutAbsLinkID,(jlong)stEventInfo.stOutLinkID.pclLinkId);
#else
			(*env)->SetLongField(env, object,jlnOutAbsLinkID,(jlong)stEventInfo.ulnOutAbsLinkID);
#endif
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetSimpleGuideAt SetLongField jlnOutAbsLinkID Failed");
		}

		jfieldID jlnDistMeterFromSt = (*env)->GetFieldID(env, clazz,"lnNum3","J");
		if(jlnDistMeterFromSt)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnDistMeterFromSt,(jlong)stEventInfo.ulnDistMeterFromSt);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetSimpleGuideAt SetLongField jlnOutAbsLinkID Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetSimpleGuideAt FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_DG_GetNextGuidancePointInfo      *
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_DG_GetNextGuidancePointInfo(JNIEnv *env, jclass clz,jobject object1, jobject object2)
{
	jlong	lRet = 0;
	ZNE_EventInfo    stEventInfo;
	DG_GuidancePointInto   stInfo;
	memset(&stEventInfo, 0, sizeof(ZNE_EventInfo));
	memset(&stInfo, 0, sizeof(DG_GuidancePointInto));

	//LOGDp("[DEBUG] JNI_DG_GetNextGuidancePointInfo  before call DG_GetNextGuidancePointInfo");
	lRet = DG_GetNextGuidancePointInfo( &stEventInfo, &stInfo);
	//LOGDp("[DEBUG] JNI_DG_GetNextGuidancePointInfo  after call DG_GetNextGuidancePointInfo");

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIThreeLong");
	jclass clazz2 = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jlnInAbsLinkID = (*env)->GetFieldID(env, clazz,"lnNum1","J");
		if(jlnInAbsLinkID)
		{
#ifdef _NEW_HDAL_
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object1,jlnInAbsLinkID,(jlong)stEventInfo.stInLinkID.pclLinkId);
#else

#endif
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetNextGuidancePointInfo SetLongField jlnInAbsLinkID Failed");
		}

		jfieldID jlnOutAbsLinkID = (*env)->GetFieldID(env, clazz,"lnNum2","J");
		if(jlnOutAbsLinkID)
		{
#ifdef _NEW_HDAL_

			//Set the variable's value  to Java
			(*env)->SetLongField(env, object1,jlnOutAbsLinkID,(jlong)stEventInfo.stOutLinkID.pclLinkId);
#endif
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetNextGuidancePointInfo SetLongField jlnOutAbsLinkID Failed");
		}

		jfieldID jlnDistMeterFromSt = (*env)->GetFieldID(env, clazz,"lnNum3","J");
		if(jlnDistMeterFromSt)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object1,jlnDistMeterFromSt,(jlong)stEventInfo.ulnDistMeterFromSt);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetNextGuidancePointInfo SetLongField jlnOutAbsLinkID Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetNextGuidancePointInfo FindClass  Failed!!!!!!!!!!!!!!!");
	}

	if(clazz2)
	{
		//Get variable's ID in JNI
		jfieldID jlnDistMeter = (*env)->GetFieldID(env, clazz2,"lcount","J");
		if(jlnDistMeter)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object2,jlnDistMeter,(jlong)stInfo.lnDistMeter);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetNextGuidancePointInfo SetLongField jlnDistMeter Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetNextGuidancePointInfo FindClass clazz2  Failed!!!!!!!!!!!!!!!");

	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_DG_IsHighwayGuideReady      *
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_DG_IsHighwayGuideReady(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	int    isReady = 0;

	//LOGDp("[DEBUG] JNI_DG_IsHighwayGuideReady  before call DG_IsHighwayGuideReady");
	lRet = DG_IsHighwayGuideReady(&isReady);
	//LOGDp("[DEBUG] JNI_DG_IsHighwayGuideReady  after call DG_IsHighwayGuideReady");

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jisReady = (*env)->GetFieldID(env, clazz,"m_iCommon","I");
		if(jisReady)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jisReady,(jint)isReady);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_IsHighwayGuideReady SetIntField jisReady Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_IsHighwayGuideReady FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_DG_IsVehicleOnHighway      *
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_DG_IsVehicleOnHighway(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	int     isOnHighway = 0;

	//LOGDp("[DEBUG] JNI_DG_IsVehicleOnHighway  before call DG_IsVehicleOnHighway");
	lRet = DG_IsVehicleOnHighway(&isOnHighway);
	//LOGDp("[DEBUG] JNI_DG_IsVehicleOnHighway  after call DG_IsVehicleOnHighway");

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jisOnHighway = (*env)->GetFieldID(env, clazz,"m_iCommon","I");
		if(jisOnHighway)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jisOnHighway,(jint)isOnHighway);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_IsVehicleOnHighway SetIntField jisOnHighway Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_IsVehicleOnHighway FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetGuide      *
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetGuide(JNIEnv *env, jclass clz,jobject object)
{
	jlong Ret = 0;
	Java_Guide	stGuide;
	jmethodID ctorID;
	jmethodID ctorID2;
//	jmethodID ctorID3;
	jobject  obj_ZNE_EventInfo;
	jobject  obj_JNIlong;
	jobjectArray  objArray_JNIlong;
//	jobject  obj_ZLinkID_t;

#if 1 // Modified By FR For Debug DAL-RDB TEMP.
	return 0;
#endif

	memset(&stGuide, 0, sizeof(Java_Guide));
	Java_GetGuide( &stGuide );

	jclass Class_Java_Guide = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/Java_Guide");
	jclass Class_ZNE_EventInfo = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNE_EventInfo");
//	//Liugang add start  *JNI_DG_GetGuidePointInfo*
//	jclass Class_ZLinkID = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZLinkID_t");
//#ifdef _NEW_HDAL_
//	jjfieldID jfld_InLinkID = (*env)->GetFieldID(env, Class_ZNE_EventInfo,"stInLinkID","Lnet/zmap/android/pnd/v2/data/ZLinkID_t;");
//	jjfieldID jfld_OutLinkID = (*env)->GetFieldID(env, Class_ZNE_EventInfo,"stOutLinkID","Lnet/zmap/android/pnd/v2/data/ZLinkID_t;");
//#endif
//	//Liugang add end
	jclass Class_JNILong = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	jfieldID jfield_ZNE_EventInfo = (*env)->GetFieldID(env, Class_Java_Guide,"stEventInfo","Lnet/zmap/android/pnd/v2/data/ZNE_EventInfo;");
//	//Liugang add start
//	if(Class_ZLinkID != NULL)
//	{
//		ctorID = (*env)->GetMethodID(env, Class_ZLinkID, "<init>", "()V");
//
//		obj_ZLinkID_t = (jobject)(*env)->NewObject(env, Class_ZLinkID, ctorID3);
//
//		if(NULL == obj_ZLinkID_t)
//		{
//			LOGDp("[DEBUG] JNI_Java_GetGuide  NewObject obj_ZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
//		}
//
//		jfieldID jlparcelNo = (*env)->GetFieldID(env, Class_ZLinkID,"lparcelNo","J");
//		if(jlparcelNo)
//		{
//			//Set the variable's value  to Java
//			(*env)->SetLongField(env, obj_ZLinkID_t,jlparcelNo, (jlong)stGuide.stEventInfo.stInLinkID.parcelNo);
//		}
//		else
//		{
//			LOGDp("[DEBUG] JNI_Java_GetGuide GetFieldID parcelNo Failed");
//		}
//
//		jfieldID jspclLinkId = (*env)->GetFieldID(env, Class_ZLinkID,"spclLinkId","S");
//		if(jspclLinkId)
//		{
//			//Set the variable's value  to Java
//			(*env)->SetLongField(env, obj_ZLinkID_t,jspclLinkId, (jshort)stGuide.stEventInfo.stInLinkID.pclLinkId);
//		}
//		else
//		{
//			LOGDp("[DEBUG] JNI_Java_GetGuide GetFieldID pclLinkId Failed");
//		}
//		jfieldID jsdivNo = (*env)->GetFieldID(env, Class_ZLinkID,"sdivNo","S");
//		if(jsdivNo)
//		{
//			//Set the variable's value  to Java
//			(*env)->SetLongField(env, obj_ZLinkID_t,jsdivNo, (jshort)stGuide.stEventInfo.stInLinkID.divNo);
//		}
//		else
//		{
//			LOGDp("[DEBUG] JNI_Java_GetGuide GetFieldID divNo Failed");
//		}
//
//
//	}
//	else
//	{
//		LOGDp("[DEBUG] JNI_Java_GetGuide  Find  class ClassZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
//	}
//	//Liugang add end
	if(Class_ZNE_EventInfo != NULL)
	{
		ctorID = (*env)->GetMethodID(env, Class_ZNE_EventInfo, "<init>", "()V");

		obj_ZNE_EventInfo = (jobject)(*env)->NewObject(env, Class_ZNE_EventInfo, ctorID);

		if(NULL == obj_ZNE_EventInfo)
		{
			LOGDp("[DEBUG] JNI_Java_GetGuide  NewObject obj_ZNE_EventInfo Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}
		else
		{
#ifdef _NEW_HDAL_

			(*env)->SetObjectField(env, obj_ZNE_EventInfo, jfld_InLinkID, obj_ZLinkID_t);

			(*env)->SetObjectField(env, obj_ZNE_EventInfo, jfld_OutLinkID, obj_ZLinkID_t);
#else
			jfieldID jlnInAbsLinkID = (*env)->GetFieldID(env, Class_ZNE_EventInfo,"ulnInAbsLinkID","J");
			if(jlnInAbsLinkID)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_ZNE_EventInfo, jlnInAbsLinkID, (jlong)stGuide.stEventInfo.ulnInAbsLinkID);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetGuide GetFieldID jlnInAbsLinkID Failed");
			}

			jfieldID jlnOutAbsLinkID = (*env)->GetFieldID(env, Class_ZNE_EventInfo,"ulnOutAbsLinkID","J");
			if(jlnOutAbsLinkID)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_ZNE_EventInfo, jlnOutAbsLinkID, (jlong)stGuide.stEventInfo.ulnOutAbsLinkID);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetGuide GetFieldID jlnOutAbsLinkID Failed");
			}
#endif
			jfieldID jlnDistMeterFromSt = (*env)->GetFieldID(env, Class_ZNE_EventInfo,"ulnDistMeterFromSt","J");
			if(jlnDistMeterFromSt)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_ZNE_EventInfo, jlnDistMeterFromSt, (jlong)stGuide.stEventInfo.ulnDistMeterFromSt);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetGuide GetFieldID jlnDistMeterFromSt Failed");
			}
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetGuide  Find  class Class_ZNE_EventInfo Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	if(Class_Java_Guide != NULL)
	{
		if(jfield_ZNE_EventInfo != NULL)
		{
			(*env)->SetObjectField(env, object, jfield_ZNE_EventInfo, obj_ZNE_EventInfo);
		}

		jfieldID jiGuideType = (*env)->GetFieldID(env, Class_Java_Guide,"iGuideType","I");
		if(jiGuideType)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jiGuideType, (jint)stGuide.eGuideType);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuide GetFieldID jiGuideType Failed");
		}

		jfieldID jlnArrDistance = (*env)->GetFieldID(env, Class_Java_Guide,"lnArrDistance","[Lnet/zmap/android/pnd/v2/data/JNILong;");
		if(Class_JNILong != NULL)
		{
			objArray_JNIlong = (*env)->NewObjectArray(env, 9, Class_JNILong, NULL);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuide  Find Class_JNILong Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}
		int i = 0;
		for(i=0; i<9; i++)
		{
			//##########
			if(Class_JNILong != NULL)
			{
				ctorID2= (*env)->GetMethodID(env, Class_JNILong, "<init>", "()V");

				obj_JNIlong = (jobject)(*env)->NewObject(env, Class_JNILong, ctorID2);

				if(NULL == obj_JNIlong)
				{
					LOGDp("[DEBUG] JNI_Java_GetGuide  NewObject obj_JNIlong Failed!!!@@@@@@@@@@@@@@@@@@@@");
				}
				else
				{
					jfieldID jlndistace = (*env)->GetFieldID(env, Class_JNILong,"lcount","J");
					if(jlndistace)
					{
						//Set the variable's value  to Java
						(*env)->SetLongField(env, obj_JNIlong,jlndistace,(jlong)stGuide.lnDistance[i]);
						(*env)->SetObjectArrayElement(env, objArray_JNIlong, i, obj_JNIlong);
					}
					else
					{
						LOGDp("[DEBUG] JNI_Java_GetGuide SetLongField jlndistace Failed");
					}
				}
			}
			if(NULL != obj_JNIlong)
			{
				(*env)->DeleteLocalRef( env,obj_JNIlong);
				obj_JNIlong = NULL;
			}
		}
		(*env)->SetObjectField(env, object, jlnArrDistance, objArray_JNIlong);

		jfieldID jiStatus = (*env)->GetFieldID(env, Class_Java_Guide,"iStatus","I");
		if(jiStatus)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jiStatus, (jint)stGuide.eStatus);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuide GetFieldID jiStatus Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetGuide  Find Class_Java_Guide Failed!!!");

	}

	if(NULL != objArray_JNIlong)
	{
		(*env)->DeleteLocalRef( env,objArray_JNIlong);
		objArray_JNIlong = NULL;
	}

	if(NULL != obj_ZNE_EventInfo)
	{
		(*env)->DeleteLocalRef( env,obj_ZNE_EventInfo);
		obj_ZNE_EventInfo = NULL;
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetJCTDirectionInfo      *
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetJCTDirectionInfo(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	ZDG_SimpleGuideInfo  simpleGuideInfo;

	memset(&simpleGuideInfo, 0, sizeof(ZDG_SimpleGuideInfo));

	Java_GetJCTDirectionInfo(&simpleGuideInfo);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jeGuideDirKind = (*env)->GetFieldID(env, clazz,"m_iSizeX","I");
		if(jeGuideDirKind)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jeGuideDirKind, (jint)simpleGuideInfo.eGuideDirKind);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetJCTDirectionInfo  Getfield jeGuideDirKind  Failed");
		}

		//Get variable's ID in JNI
		jfieldID jeGuidePointKind = (*env)->GetFieldID(env, clazz,"m_iSizeY","I");
		if(jeGuidePointKind)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jeGuidePointKind, (jint)simpleGuideInfo.eGuidePointKind);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetJCTDirectionInfo  Getfield jeGuidePointKind  Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetJCTDirectionInfo FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_Java_GetRestDistance			*
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetRestDistance(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	long	lnRestDistance = 0;

	Java_GetRestDistance( &lnRestDistance );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jlnRestDistance = (*env)->GetFieldID(env, clazz,"lcount","J");
		if(jlnRestDistance)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnRestDistance,(jlong)lnRestDistance);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetRestDistance Getfield jlnRestDistance Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetRestDistance FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_Java_GetJCTDirectionInfo      *
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetShowGuide(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	Java_ShowGuide  showGuide;

	memset(&showGuide, 0, sizeof(Java_ShowGuide));
	Java_GetShowGuide(&showGuide);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jeGuideType = (*env)->GetFieldID(env, clazz,"m_iSizeX","I");
		if(jeGuideType)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jeGuideType, (jint)showGuide.eGuideType);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetShowGuide  Getfield jeGuideType  Failed");
		}

		//Get variable's ID in JNI
		jfieldID jeStatus = (*env)->GetFieldID(env, clazz,"m_iSizeY","I");
		if(jeStatus)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jeStatus, (jint)showGuide.eStatus);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetShowGuide  Getfield jeStatus  Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetShowGuide FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_Java_GetLaneGuideInfo			*
*	Description	:                                            *
*	Date		: 09/12/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetLaneGuideInfo(JNIEnv *env, jclass clz, jobject object)
{
	jlong		lRet = 0;
	jmethodID	mid;
	ZNE_LaneGuideInfo  stLaneGuideInfo;
	memset(&stLaneGuideInfo, 0, sizeof(ZNE_LaneGuideInfo));

	Java_GetLaneGuideInfo( &stLaneGuideInfo );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNE_LaneGuideInfo");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jiValid = (*env)->GetFieldID(env, clazz,"iValid","I");
		if(jiValid)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jiValid,(jint)stLaneGuideInfo.bValid);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo Getfield jiValid Failed");
		}

		jfieldID jcLeftAddLaneNum = (*env)->GetFieldID(env, clazz,"cLeftAddLaneNum","I");
		if(jcLeftAddLaneNum)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jcLeftAddLaneNum,(jint)stLaneGuideInfo.ucLeftAddLaneNum);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo Getfield jcLeftAddLaneNum Failed");
		}

		jfieldID jcRightAddLaneNum = (*env)->GetFieldID(env, clazz,"cRightAddLaneNum","I");
		if(jcRightAddLaneNum)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jcRightAddLaneNum,(jint)stLaneGuideInfo.ucRightAddLaneNum);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo Getfield jcRightAddLaneNum Failed");
		}

		jfieldID jcLeftReduceLaneNum = (*env)->GetFieldID(env, clazz,"cLeftReduceLaneNum","I");
		if(jcLeftReduceLaneNum)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jcLeftReduceLaneNum,(jint)stLaneGuideInfo.ucLeftReduceLaneNum);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo Getfield jcLeftReduceLaneNum Failed");
		}

		jfieldID jcRightReduceLaneNum = (*env)->GetFieldID(env, clazz,"cRightReduceLaneNum","I");
		if(jcRightReduceLaneNum)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jcRightReduceLaneNum,(jint)stLaneGuideInfo.ucRightReduceLaneNum);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo Getfield jcRightReduceLaneNum Failed");
		}

		//LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo stLaneGuideInfo.ucEnterLaneNum= %d",stLaneGuideInfo.ucEnterLaneNum);

		jfieldID jcEnterLaneNum = (*env)->GetFieldID(env, clazz,"cEnterLaneNum","I");
		if(jcEnterLaneNum)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jcEnterLaneNum,(jint)stLaneGuideInfo.ucEnterLaneNum);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo Getfield jcEnterLaneNum Failed");
		}

		jfieldID jcExitGuideDir = (*env)->GetFieldID(env, clazz,"cExitGuideDir","I");
		if(jcExitGuideDir)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jcExitGuideDir,(jint)stLaneGuideInfo.ucExitGuideDir);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo Getfield jcExitGuideDir Failed");
		}

		jfieldID jsExitLaneFlag = (*env)->GetFieldID(env, clazz,"sExitLaneFlag","I");
		if(jsExitLaneFlag)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jsExitLaneFlag,(jint)stLaneGuideInfo.unExitLaneFlag);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo Getfield jsExitLaneFlag Failed");
		}

		//LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo aucArrowFlag= %s",stLaneGuideInfo.aucArrowFlag);

		mid = (*env)->GetMethodID(env,clazz,"initAucArrowFlag","(I)V");
		if(mid && (strlen((char*)stLaneGuideInfo.aucArrowFlag) > 0))
		{
			(*env)->CallVoidMethod(env, object, mid, (jint)strlen((char*)stLaneGuideInfo.aucArrowFlag));
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo  GetMethodID mid  failed");
		}

		int i = 0;
		int* tmpArrow = NULL;
		if(strlen((char*)stLaneGuideInfo.aucArrowFlag) > 0)
		{
			tmpArrow = (int*)malloc(strlen((char*)stLaneGuideInfo.aucArrowFlag)*sizeof(int));
		}

		if(tmpArrow != NULL)
		{
			for(i=0; i < strlen((char*)stLaneGuideInfo.aucArrowFlag); i++)
			{
				tmpArrow[i] = stLaneGuideInfo.aucArrowFlag[i];
			}
		}

		jfieldID jfldArrowFlag = (*env)->GetFieldID(env, clazz,"aucArrowFlag","[I");
		if(jfldArrowFlag && (strlen((char*)stLaneGuideInfo.aucArrowFlag) > 0))
		{
			jintArray iArr = (*env)->NewIntArray(env, (jint)strlen((char*)stLaneGuideInfo.aucArrowFlag));
			if(iArr)
			{
				(*env)->SetIntArrayRegion(env, iArr, 0, (jint)strlen((char*)stLaneGuideInfo.aucArrowFlag), (jint*)tmpArrow);
				(*env)->SetObjectField(env, object,jfldArrowFlag,iArr);

			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetGuidePicture  NewIntArray  Failed");
			}

			if(NULL != iArr)
			{
				(*env)->DeleteLocalRef( env,iArr);
				iArr = NULL;
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo GetFieldID jfldArrowFlag Failed");
		}

		if(tmpArrow != NULL)
		{
			free(tmpArrow);
			tmpArrow = NULL;
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetLaneGuideInfo FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_Java_GetEventInfo      *
*	Description	:                                            *
*	Date		: 09/12/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetEventInfo(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	ZNE_EventInfo    stEventInfo;
	memset(&stEventInfo, 0, sizeof(ZNE_EventInfo));

	Java_GetEventInfo( &stEventInfo );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIThreeLong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jlnInAbsLinkID = (*env)->GetFieldID(env, clazz,"lnNum1","J");
		if(jlnInAbsLinkID)
		{
#ifdef _NEW_HDAL_
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnInAbsLinkID,(jlong)stEventInfo.stInLinkID.pclLinkId);
#else
			(*env)->SetLongField(env, object,jlnInAbsLinkID,(jlong)stEventInfo.ulnInAbsLinkID);
#endif
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetEventInfo SetLongField jlnInAbsLinkID Failed");
		}

		jfieldID jlnOutAbsLinkID = (*env)->GetFieldID(env, clazz,"lnNum2","J");
		if(jlnOutAbsLinkID)
		{
#ifdef _NEW_HDAL_
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnOutAbsLinkID,(jlong)stEventInfo.stOutLinkID.pclLinkId);
#else
			(*env)->SetLongField(env, object,jlnOutAbsLinkID,(jlong)stEventInfo.ulnOutAbsLinkID);
#endif
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetEventInfo SetLongField jlnOutAbsLinkID Failed");
		}

		jfieldID jlnDistMeterFromSt = (*env)->GetFieldID(env, clazz,"lnNum3","J");
		if(jlnDistMeterFromSt)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnDistMeterFromSt,(jlong)stEventInfo.ulnDistMeterFromSt);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetEventInfo SetLongField jlnOutAbsLinkID Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetEventInfo FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_Java_GetCompass			*
*	Description	:                                            *
*	Date		: 09/12/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetCompass(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	Java_IconInfo  stIconInfo;
	memset(&stIconInfo, 0, sizeof(Java_IconInfo));

	Java_GetCompass( &stIconInfo );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/Java_IconInfo");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jsCalcAngle = (*env)->GetFieldID(env, clazz,"sCalcAngle","S");
		if(jsCalcAngle)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,jsCalcAngle,(jshort)stIconInfo.nCalcAngle);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetCompass Getfield jsCalcAngle Failed");
		}

		jfieldID jcAngleNo = (*env)->GetFieldID(env, clazz,"cAngleNo","I");
		if(jcAngleNo)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jcAngleNo,(jint)stIconInfo.byAngleNo);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetCompass Getfield jcAngleNo Failed");
		}

		jfieldID jsIconInfo = (*env)->GetFieldID(env, clazz,"sIconInfo","S");
		if(jsIconInfo)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,jsIconInfo,(jshort)stIconInfo.nIconInfo);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetCompass Getfield jsIconInfo Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetCompass FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_Java_GetMagMapCompass			*
*	Description	:                                            *
*	Date		: 09/12/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetMagMapCompass(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	Java_IconInfo  stIconInfo;
	memset(&stIconInfo, 0, sizeof(Java_IconInfo));

	Java_GetMagMapCompass( &stIconInfo );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/Java_IconInfo");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jsCalcAngle = (*env)->GetFieldID(env, clazz,"sCalcAngle","S");
		if(jsCalcAngle)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,jsCalcAngle,(jshort)stIconInfo.nCalcAngle);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetCompass Getfield jsCalcAngle Failed");
		}

		jfieldID jcAngleNo = (*env)->GetFieldID(env, clazz,"cAngleNo","I");
		if(jcAngleNo)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jcAngleNo,(jint)stIconInfo.byAngleNo);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetCompass Getfield jcAngleNo Failed");
		}

		jfieldID jsIconInfo = (*env)->GetFieldID(env, clazz,"sIconInfo","S");
		if(jsIconInfo)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,jsIconInfo,(jshort)stIconInfo.nIconInfo);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetCompass Getfield jsIconInfo Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetCompass FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_Java_GetLaneRestDist			*
*	Description	:                                            *
*	Date		: 09/12/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetLaneRestDist(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	Java_Lane  pLaneRestDist;
	memset(&pLaneRestDist, 0, sizeof(Java_Lane));

	Java_GetLaneRestDist( &pLaneRestDist );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/Java_Lane");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jlnLaneRestDist = (*env)->GetFieldID(env, clazz,"lnLaneRestDist","J");
		if(jlnLaneRestDist)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnLaneRestDist,(jlong)pLaneRestDist.alnLaneRestDist);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneRestDist Getfield jlnLaneRestDist Failed");
		}

		jfieldID jiExistLaneData = (*env)->GetFieldID(env, clazz,"iExistLaneData","I");
		if(jiExistLaneData)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jiExistLaneData,(jint)pLaneRestDist.abExistLaneData);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneRestDist Getfield jiExistLaneData Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetLaneRestDist FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_Java_GetGuidePicture			*
*	Description	:                                            *
*	Date		: 09/12/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetGuidePicture(JNIEnv *env, jclass clz, jobject object)
{
	jlong		lRet = 0;
	int			iPixArrLen = 0;
	ZNUI_IMAGE  stImage;
	jmethodID	mid;
	int* PixArray = NULL;
	memset(&stImage, 0, sizeof(ZNUI_IMAGE));

	Java_GetGuidePicture( &stImage );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNUI_IMAGE");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jsWidth = (*env)->GetFieldID(env, clazz,"sWidth","S");
		if(jsWidth)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,jsWidth,(jshort)stImage.unWidth);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuidePicture Getfield jsWidth Failed");
		}

		jfieldID jsHeight = (*env)->GetFieldID(env, clazz,"sHeight","S");
		if(jsHeight)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,jsHeight,(jshort)stImage.unHeight);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuidePicture Getfield jsHeight Failed");
		}

		iPixArrLen = (stImage.unHeight) * (stImage.unWidth);
		PixArray = (int*)malloc(iPixArrLen*sizeof(int));
		memset(PixArray, 0, iPixArrLen*sizeof(int) );

		if( (NULL != PixArray) && (NULL != stImage.pPixRGBA32) )
		{
			memcpy( PixArray, stImage.pPixRGBA32, iPixArrLen*sizeof(int) );
		}

		mid = (*env)->GetMethodID(env,clazz,"initSPixRGBA32","(I)V");
		if(mid && iPixArrLen > 0)
		{
			//LOGDp("[DEBUG] JNI_Java_GetGuidePicture  before CallObjectMethod  ");
			(*env)->CallVoidMethod(env, object, mid, (jint)iPixArrLen);
			//LOGDp("[DEBUG] JNI_Java_GetGuidePicture  after CallObjectMethod  ");
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuidePicture  GetMethodID mid  failed");
		}

		jfieldID jfldSPix = (*env)->GetFieldID(env, clazz,"SPixRGBA32","[I");
		if(jfldSPix)
		{
			jintArray iArr = (*env)->NewIntArray(env, (jint)iPixArrLen);
			if(iArr)
			{
				(*env)->SetIntArrayRegion(env, iArr, 0, (jint)iPixArrLen, (jint*)PixArray);
				(*env)->SetObjectField(env, object,jfldSPix,iArr);

			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetGuidePicture  NewIntArray  Failed");
			}

			if(NULL != iArr)
			{
				(*env)->DeleteLocalRef( env,iArr);
				iArr = NULL;
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuidePicture GetFieldID jfldSPix Failed");
		}

		if(PixArray)
		{
			free(PixArray);
			PixArray = NULL;
		}

	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetCompass FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_Java_GetKindInfo			*
*	Description	:                                            *
*	Date		: 09/12/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetKindInfo(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	Java_KINDDATA  stImage;
	memset(&stImage, 0, sizeof(Java_KINDDATA));

	Java_GetKindInfo( &stImage );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/Java_KINDDATA");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jiGuideType = (*env)->GetFieldID(env, clazz,"iGuideType","I");
		if(jiGuideType)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jiGuideType,(jint)stImage.eGuideType);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetKindInfo Getfield jiGuideType Failed");
		}

		jfieldID jiKindType = (*env)->GetFieldID(env, clazz,"iKindType","I");
		if(jiKindType)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jiKindType,(jint)stImage.eKindType);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetKindInfo Getfield jiKindType Failed");
		}

		if(strlen(stImage.cKindName) > 0)
		{
			jfieldID jStrField= (*env)->GetFieldID(env, clazz,"SKindName","Ljava/lang/String;");
			jclass strClass = (*env)->FindClass(env, "java/lang/String");
			jmethodID ctorIDstr = (*env)->GetMethodID(env, strClass,  "<init>", "([BLjava/lang/String;)V");
			jbyteArray bytes = (*env)->NewByteArray(env, strlen(stImage.cKindName));
			if(bytes != NULL)
			{
				(*env)->SetByteArrayRegion(env, bytes, 0, strlen(stImage.cKindName), (jbyte*)stImage.cKindName);
			}
			jstring encoding = (*env)->NewStringUTF(env, "ShiftJIS");
			jstring  jstrszName = (jstring)(*env)->NewObject(env, strClass, ctorIDstr, bytes, encoding);
			//LOGDp("[DEBUG] JNI_Java_GetKindInfo  NewStringUTF success @@@@@@@@@@@@@@@@@@@@");

			if(jstrszName)
			{
				//Set the variable's value  to Java
				(*env)->SetObjectField(env, object,jStrField,jstrszName);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetKindInfo NewObject jstrszName Failed");
			}

			if(NULL != bytes)
			{
				(*env)->DeleteLocalRef(env, bytes);
				bytes = NULL;
			}
			if(NULL != encoding)
			{
				(*env)->DeleteLocalRef( env,encoding);
				encoding = NULL;
			}
			if(NULL != jstrszName)
			{
				(*env)->DeleteLocalRef(env, jstrszName);
				jstrszName = NULL;
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetKindInfo   stImage.cKindName is NULL");
		}

		if(strlen(stImage.cDirectionName) > 0)
		{
			jfieldID jStrField= (*env)->GetFieldID(env, clazz,"SDirectionName","Ljava/lang/String;");
			jclass strClass = (*env)->FindClass(env, "java/lang/String");
			jmethodID ctorIDstr = (*env)->GetMethodID(env, strClass,  "<init>", "([BLjava/lang/String;)V");
			jbyteArray bytes = (*env)->NewByteArray(env, strlen(stImage.cDirectionName));
			if(bytes != NULL)
			{
				(*env)->SetByteArrayRegion(env, bytes, 0, strlen(stImage.cDirectionName), (jbyte*)stImage.cDirectionName);
			}
			jstring encoding = (*env)->NewStringUTF(env, "ShiftJIS");
			jstring  jstrszName = (jstring)(*env)->NewObject(env, strClass, ctorIDstr, bytes, encoding);
			//LOGDp("[DEBUG] JNI_Java_GetKindInfo  NewStringUTF success @@@@@@@@@@@@@@@@@@@@");

			if(jstrszName)
			{
				//Set the variable's value  to Java
				(*env)->SetObjectField(env, object,jStrField,jstrszName);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetKindInfo NewObject jstrszName Failed");
			}

			if(NULL != bytes)
			{
				(*env)->DeleteLocalRef(env, bytes);
				bytes = NULL;
			}
			if(NULL != encoding)
			{
				(*env)->DeleteLocalRef( env,encoding);
				encoding = NULL;
			}
			if(NULL != jstrszName)
			{
				(*env)->DeleteLocalRef(env, jstrszName);
				jstrszName = NULL;
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetKindInfo   stImage.cDirectionName is NULL");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetKindInfo FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_Java_ct_SwitchNaviMode			*
*	Description	:                                            *
*	Date		: 09/12/25                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_ct_SwitchNaviMode(JNIEnv *env, jclass clz)
{
	jlong	lRet = 0;
	CT_SwitchNaviMode();
	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_ct_ChangeNaviMode			*
*	Description	:                                            *
*	Date		: 10/12/28                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: wangmeng	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_ct_ChangeNaviMode(JNIEnv *env, jclass clz,jint iSettingMode)
{
	jlong	lRet = 0;
	LOGDp("------JNI_Java_ct_ChangeNaviMode------iSettingMode=[%d}",iSettingMode);
	CT_ChangeNaviMode( (ENE_NaviMode)iSettingMode);
	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_ChangeSettingMode			*
*	Description	:                                            *
*	Date		: 09/12/25                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_ChangeSettingMode(JNIEnv *env, jclass clz, jint iDelSettingMode, jint iAddSettingMode)
{
	jlong	lRet = 0;
	Java_ChangeSettingMode((ENUI_SETTING_MODE)iDelSettingMode, (ENUI_SETTING_MODE)iAddSettingMode);
	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetGuideState			*
*	Description	:                                            *
*	Date		: 09/12/25                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetGuideState(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	ENUI_GUIDE_STATE  eState = NUI_STATE_STOP;
	Java_GetGuideState(&eState);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jGuideState = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(jGuideState)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,jGuideState,(jint)eState);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuideState GetFieldID jGuideState Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_Java_GetGuideState FindClass Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetNaviMode			*
*	Description	:                                            *
*	Date		: 09/12/25                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetNaviMode(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	ENUI_GUIDE_TARGET  eGuideTarget = NUI_GUIDE_CAR;
	Java_GetNaviMode(&eGuideTarget );

	//Find Java's Class
	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jGuideTarget = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(jGuideTarget)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,jGuideTarget,(jint)eGuideTarget);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetNaviMode GetFieldID jGuideTarget Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_Java_GetNaviMode FindClass Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetShowRoute			*
*	Description	:                                            *
*	Date		: 09/12/25                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetShowRoute(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	int iShowRoute;
	Java_GetShowRoute(&iShowRoute);

	//Find Java's Class
	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jShowRoute = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(jShowRoute)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,jShowRoute,(jint)iShowRoute);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetShowRoute GetFieldID jShowRoute Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_Java_GetShowRoute FindClass Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetVicsBtnSts			*
*	Description	:                                            *
*	Date		: 09/12/25                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetVicsBtnSts(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	int iVicsBtnSts;
	Java_GetVicsBtnSts(&iVicsBtnSts);

	//Find Java's Class
	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jShowRoute = (*env)->GetFieldID(env,clazz,"m_iCommon","I");
		if(jShowRoute)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,object,jShowRoute,(jint)iVicsBtnSts);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetVicsBtnSts GetFieldID jShowRoute Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_Java_GetVicsBtnSts FindClass Failed");
	}

	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_DG_getVehiclePosInfo      *
*	Description	:                                            *
*	Date		: 2010/01/05                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_DG_getVehiclePosInfo
  (JNIEnv *env, jclass clz,jobject object)
{
	ZNE_VehicleInfo_t		vehicleInfo;
	jmethodID ctorID;
	jmethodID ctorID2;
	jobject  obj_ZNE_Location_t = 0;
	jobject  obj_ZNE_RoadType_t = 0;
	jlong Ret = 0;

	memset( &vehicleInfo, 0, sizeof( vehicleInfo ) );

	Ret = (jlong)DG_getVehiclePosInfo(&vehicleInfo);

	jclass ClassZNE_VehicleInfo_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNE_VehicleInfo_t");
	jclass ClassZNE_Location_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoLong");
	jclass ClassZNE_RoadType_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoInt");

	jfieldID jfld_ZNE_Location_t = (*env)->GetFieldID(env, ClassZNE_VehicleInfo_t,"stPos","Lnet/zmap/android/pnd/v2/data/JNITwoLong;");
	jfieldID jfld_ZNE_RoadType_t = (*env)->GetFieldID(env, ClassZNE_VehicleInfo_t,"stRoadType","Lnet/zmap/android/pnd/v2/data/JNITwoInt;");

	if(ClassZNE_Location_t != NULL)
	{
		ctorID = (*env)->GetMethodID(env, ClassZNE_Location_t, "<init>", "()V");

		obj_ZNE_Location_t = (jobject)(*env)->NewObject(env, ClassZNE_Location_t, ctorID);

		if(NULL == obj_ZNE_Location_t)
		{
			LOGDp("[DEBUG] JNI_DG_getVehiclePosInfo  NewObject obj_ZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		jfieldID jlnLonMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLong","J");
		if(jlnLonMillSec)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, obj_ZNE_Location_t,jlnLonMillSec, (jlong)vehicleInfo.stPos.lnLonMillSec);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_getVehiclePosInfo GetFieldID jlnLonMillSec Failed");
		}

		jfieldID jlnLatMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLat","J");
		if(jlnLatMillSec)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, obj_ZNE_Location_t,jlnLatMillSec, (jlong)vehicleInfo.stPos.lnLatMillSec);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_getVehiclePosInfo GetFieldID jlnLatMillSec Failed");
		}

	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_getVehiclePosInfo  Find  class ClassZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	if(ClassZNE_RoadType_t != NULL)
	{
		ctorID2 = (*env)->GetMethodID(env, ClassZNE_RoadType_t, "<init>", "()V");

		obj_ZNE_RoadType_t = (jobject)(*env)->NewObject(env, ClassZNE_RoadType_t, ctorID2);

		if(NULL == obj_ZNE_RoadType_t)
		{
			LOGDp("[DEBUG] JNI_DG_getVehiclePosInfo  NewObject obj_ZNE_RoadType_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		jfieldID jiPreRouteType = (*env)->GetFieldID(env, ClassZNE_RoadType_t,"m_iSizeX","I");
		if(jiPreRouteType)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, obj_ZNE_RoadType_t,jiPreRouteType, (jint)vehicleInfo.stRoadType.iPreRouteType);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_getVehiclePosInfo GetFieldID jiPreRouteType Failed");
		}

		jfieldID jiCurRouteType = (*env)->GetFieldID(env, ClassZNE_RoadType_t,"m_iSizeY","I");
		if(jiCurRouteType)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, obj_ZNE_RoadType_t,jiCurRouteType, (jint)vehicleInfo.stRoadType.iCurRouteType);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_getVehiclePosInfo GetFieldID jiCurRouteType Failed");
		}

	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_getVehiclePosInfo  Find  class ClassZNE_RoadType_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	if(ClassZNE_VehicleInfo_t != NULL)
	{

		(*env)->SetObjectField(env, object, jfld_ZNE_Location_t, obj_ZNE_Location_t);
		(*env)->SetObjectField(env, object, jfld_ZNE_RoadType_t, obj_ZNE_RoadType_t);

		jfieldID jiValid = (*env)->GetFieldID(env, ClassZNE_VehicleInfo_t,"iValid","I");
		if(jiValid)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jiValid,(jint)vehicleInfo.bValid);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_getVehiclePosInfo GetFieldID jiValid Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_getVehiclePosInfo  Find class ClassZNE_VehicleInfo_t Failed!!!@@@@@@@@@@@@@@@@@@@@");

	}
	if(NULL != obj_ZNE_Location_t)
	{
		(*env)->DeleteLocalRef( env,obj_ZNE_Location_t);
		obj_ZNE_Location_t = NULL;
	}
	if(NULL != obj_ZNE_RoadType_t)
	{
		(*env)->DeleteLocalRef( env,obj_ZNE_RoadType_t);
		obj_ZNE_RoadType_t = NULL;
	}

	return Ret;
}
/*********************************************************************
*	Function Name	: JNI_Java_GetGuideRoutePoint      *
*	Description	:                                            *
*	Date		: 2010/01/05                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_Java_GetGuideRoutePoint
  (JNIEnv *env, jclass clz,jobject object)
{
	 ZNE_GuideRoutePoint 		stRoutePoint;			// ・ｽ・ｽ・ｽ[・ｽg・ｽ・ｽ・ｽ
	//jmethodID 					ctorID;
	//jmethodID 					ctorID2;
	jobjectArray  				objArray_RouteInfoGuidePoint_t = 0;		// ・ｽo・ｽR・ｽH・ｽﾌ擾ｿｽ・ｽ
	jlong Ret = 0;

	memset( &stRoutePoint, 0, sizeof( stRoutePoint ) );

	// 案内情報を取得する
	Ret = (jlong)Java_GetGuideRoutePoint( &stRoutePoint );

	jclass ClassZNE_RouteInfo_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNE_GuideRoutePoint");
	jclass ClassZNE_RouteInfoGuidePoint_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNE_RoutePoint");
	jclass ClassZNE_Location_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoLong");

	jfieldID jfld_stGuidePoint =
	(*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"stGuidePoint","[Lnet/zmap/android/pnd/v2/data/ZNE_RoutePoint;");
	//jfieldID jfld_ZNE_Location_t =
	(*env)->GetFieldID(env, ClassZNE_RouteInfoGuidePoint_t,"stPos","Lnet/zmap/android/pnd/v2/data/JNITwoLong;");


	if(ClassZNE_RouteInfoGuidePoint_t != NULL)
	{
		objArray_RouteInfoGuidePoint_t = (*env)->NewObjectArray(env, 9, ClassZNE_RouteInfoGuidePoint_t, NULL);
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint  Find ClassZNE_RouteInfoGuidePoint_t Failed!!!@@@@@@@");
	}
	int i;
	for( i=0; i < 9; i++)
	{
		jmethodID  ctorID3;
		jmethodID  ctorID4;
		jobject    obj_RouteInfoGuidePoint_t = 0;
		jobject    obj_subZNE_Location_t;
		if(ClassZNE_RouteInfoGuidePoint_t != NULL)
		{
			ctorID3 = (*env)->GetMethodID(env, ClassZNE_RouteInfoGuidePoint_t, "<init>", "()V");
			obj_RouteInfoGuidePoint_t = (jobject)(*env)->NewObject(env, ClassZNE_RouteInfoGuidePoint_t, ctorID3);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint  Find ClassZNE_RouteInfoGuidePoint_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		if(strlen(stRoutePoint.stRoutePoint[i].szPointName) > 0)
		{
			jfieldID jszPointName = (*env)->GetFieldID(env, ClassZNE_RouteInfoGuidePoint_t,"szPointName","Ljava/lang/String;");
			jclass strClass = (*env)->FindClass(env, "java/lang/String");
			jmethodID ctorIDstr = (*env)->GetMethodID(env, strClass,  "<init>", "([BLjava/lang/String;)V");
			jbyteArray bytes = (*env)->NewByteArray(env, strlen((char*)stRoutePoint.stRoutePoint[i].szPointName));
			if(bytes != NULL)
			{
				(*env)->SetByteArrayRegion(env, bytes, 0, strlen((char*)stRoutePoint.stRoutePoint[i].szPointName), (jbyte*)stRoutePoint.stRoutePoint[i].szPointName);
			}
			jstring encoding = (*env)->NewStringUTF(env, "ShiftJIS");
			jstring  jstrszName = (jstring)(*env)->NewObject(env, strClass, ctorIDstr, bytes, encoding);

			if(jstrszName)
			{
				//Set the variable's value  to Java
				(*env)->SetObjectField(env, obj_RouteInfoGuidePoint_t, jszPointName, jstrszName);

			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint NewObject jstrszName Failed");
			}
		}
		if(ClassZNE_RouteInfoGuidePoint_t != NULL)
		{
			jfieldID jiViaValidCount = (*env)->GetFieldID(env, ClassZNE_RouteInfoGuidePoint_t,"iPointType","I");
			if(jiViaValidCount)
			{
				//Set the variable's value  to Java
				(*env)->SetIntField(env, obj_RouteInfoGuidePoint_t, jiViaValidCount, (jint)stRoutePoint.stRoutePoint[i].ePointType);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint GetFieldID jiViaValidCount Failed");
			}

			jfieldID jiRouteSearchType = (*env)->GetFieldID(env, ClassZNE_RouteInfoGuidePoint_t,"iPassed","I");
			if(jiRouteSearchType)
			{
				//Set the variable's value  to Java
				(*env)->SetIntField(env, obj_RouteInfoGuidePoint_t, jiRouteSearchType, (jint)stRoutePoint.stRoutePoint[i].bPassed);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint GetFieldID jiRouteSearchType Failed");
			}

		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint  Find  class ClassZNE_RouteInfoGuidePoint_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}
		if(ClassZNE_Location_t != NULL)
		{
			jfieldID jsubstPos = (*env)->GetFieldID(env, ClassZNE_RouteInfoGuidePoint_t,"stPos","Lnet/zmap/android/pnd/v2/data/JNITwoLong;");
			ctorID4 = (*env)->GetMethodID(env, ClassZNE_Location_t, "<init>", "()V");

			obj_subZNE_Location_t = (jobject)(*env)->NewObject(env, ClassZNE_Location_t, ctorID4);

			if(NULL == obj_subZNE_Location_t)
			{
				LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint  NewObject obj_subZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
			}

			jfieldID jlnLonMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLong","J");
			if(jlnLonMillSec)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_subZNE_Location_t,jlnLonMillSec, (jlong)stRoutePoint.stRoutePoint[i].stPoint.lnLongitude);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint GetFieldID jlnLonMillSec Failed");
			}

			jfieldID jlnLatMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLat","J");
			if(jlnLatMillSec)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_subZNE_Location_t,jlnLatMillSec, (jlong)stRoutePoint.stRoutePoint[i].stPoint.lnLatitude);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint GetFieldID jlnLatMillSec Failed");
			}

			(*env)->SetObjectField(env, obj_RouteInfoGuidePoint_t, jsubstPos, obj_subZNE_Location_t);

		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint  Find  class ClassZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		(*env)->SetObjectArrayElement(env, objArray_RouteInfoGuidePoint_t, i, obj_RouteInfoGuidePoint_t);
	}// for end

	if(jfld_stGuidePoint != NULL)
	{
		(*env)->SetObjectField(env, object, jfld_stGuidePoint, objArray_RouteInfoGuidePoint_t);
	}

	if(ClassZNE_RouteInfo_t != NULL)
	{
		jfieldID jiViaValidCount = (*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"iSearchType","I");
		if(jiViaValidCount)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jiViaValidCount, (jint)stRoutePoint.iSearchType);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint GetFieldID jiViaValidCount Failed");
		}

		jfieldID jiRouteSearchType = (*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"iCarManMode","I");
		if(jiRouteSearchType)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jiRouteSearchType, (jint)stRoutePoint.iCarManMode);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint GetFieldID jiRouteSearchType Failed");
		}

	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetGuideRoutePoint  Find  class ClassZNE_RouteInfo_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_DG_GetRouteInfo      *
*	Description	:                                            *
*	Date		: 2010/01/05                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_DG_GetRouteInfo
  (JNIEnv *env, jclass clz,jobject object)
{
	ZNE_RouteInfo_t		RouteInfo;
	jmethodID ctorID;
	//jmethodID ctorID2;
	jobject  obj_ZNE_Location_t;
	jobjectArray  objArray_RouteInfoGuidePoint_t = 0;
	jlong Ret = 0;

	memset( &RouteInfo, 0, sizeof( RouteInfo ) );

	Ret = (jlong)DG_GetRouteInfo(&RouteInfo);

	jclass ClassZNE_RouteInfo_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNE_RouteInfo_t");
	jclass ClassZNE_Location_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoLong");
	jclass ClassZNE_RouteInfoGuidePoint_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNE_RouteInfoGuidePoint_t");

	jfieldID jfld_ZNE_Location_t = (*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"stPos","Lnet/zmap/android/pnd/v2/data/JNITwoLong;");
	// jfieldID jfld_ZNE_RouteInfoGuidePoint_t = (*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"stGuidePoint","[Lnet/zmap/android/pnd/v2/data/ZNE_RouteInfoGuidePoint_t;");

	if(ClassZNE_Location_t != NULL)
	{
		ctorID = (*env)->GetMethodID(env, ClassZNE_Location_t, "<init>", "()V");

		obj_ZNE_Location_t = (jobject)(*env)->NewObject(env, ClassZNE_Location_t, ctorID);

		if(NULL == obj_ZNE_Location_t)
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo  NewObject obj_ZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		jfieldID jlnLonMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLong","J");
		if(jlnLonMillSec)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, obj_ZNE_Location_t,jlnLonMillSec, (jlong)RouteInfo.stPos.lnLonMillSec);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo GetFieldID jlnLonMillSec Failed");
		}

		jfieldID jlnLatMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLat","J");
		if(jlnLatMillSec)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, obj_ZNE_Location_t,jlnLatMillSec, (jlong)RouteInfo.stPos.lnLatMillSec);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo GetFieldID jlnLatMillSec Failed");
		}

		if(jfld_ZNE_Location_t != NULL)
		{
			(*env)->SetObjectField(env, object, jfld_ZNE_Location_t, obj_ZNE_Location_t);
		}

	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetRouteInfo  Find  class ClassZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	jfieldID jfld_stGuidePoint = (*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"stGuidePoint","[Lnet/zmap/android/pnd/v2/data/ZNE_RouteInfoGuidePoint_t;");

	if(ClassZNE_RouteInfoGuidePoint_t != NULL)
	{
		objArray_RouteInfoGuidePoint_t = (*env)->NewObjectArray(env, DEF_POINT_TYPE_VIA_COUNT, ClassZNE_RouteInfoGuidePoint_t, NULL);
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetRouteInfo  Find ClassZNE_RouteInfoGuidePoint_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	int i = 0;
	for(i=0; i<DEF_POINT_TYPE_VIA_COUNT; i++)
	{
		jmethodID  ctorID3;
		jmethodID  ctorID4;
		jobject    obj_RouteInfoGuidePoint_t = NULL;
		// CID#10988 未初期化ポインタ指摘への対応
		jobject    obj_subZNE_Location_t = NULL;
		if(ClassZNE_RouteInfoGuidePoint_t != NULL)
		{
			ctorID3 = (*env)->GetMethodID(env, ClassZNE_RouteInfoGuidePoint_t, "<init>", "()V");
			obj_RouteInfoGuidePoint_t = (jobject)(*env)->NewObject(env, ClassZNE_RouteInfoGuidePoint_t, ctorID3);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo  Find ClassZNE_RouteInfoGuidePoint_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		if(strlen(RouteInfo.stGuidePoint[i].szPointName) > 0)
		{
			jfieldID jszPointName = (*env)->GetFieldID(env, ClassZNE_RouteInfoGuidePoint_t,"szPointName","Ljava/lang/String;");
			jclass strClass = (*env)->FindClass(env, "java/lang/String");
			jmethodID ctorIDstr = (*env)->GetMethodID(env, strClass,  "<init>", "([BLjava/lang/String;)V");
			jbyteArray bytes = (*env)->NewByteArray(env, strlen((char*)RouteInfo.stGuidePoint[i].szPointName));
			if(bytes != NULL)
			{
				(*env)->SetByteArrayRegion(env, bytes, 0, strlen((char*)RouteInfo.stGuidePoint[i].szPointName), (jbyte*)RouteInfo.stGuidePoint[i].szPointName);
			}
			jstring encoding = (*env)->NewStringUTF(env, "ShiftJIS");
			jstring  jstrszName = (jstring)(*env)->NewObject(env, strClass, ctorIDstr, bytes, encoding);

			if(jstrszName)
			{
				//Set the variable's value  to Java
				(*env)->SetObjectField(env, obj_RouteInfoGuidePoint_t, jszPointName, jstrszName);

			}
			else
			{
				LOGDp("[DEBUG] JNI_DG_GetRouteInfo NewObject jstrszName Failed");
			}

			if(NULL != bytes)
			{
				(*env)->DeleteLocalRef( env,bytes);
				bytes = NULL;
			}
			if(NULL != encoding)
			{
				(*env)->DeleteLocalRef( env,encoding);
				encoding = NULL;
			}
			if(NULL != jstrszName)
			{
				(*env)->DeleteLocalRef( env,jstrszName);
				jstrszName = NULL;
			}
		}

		if(ClassZNE_Location_t != NULL)
		{
			jfieldID jsubstPos = (*env)->GetFieldID(env, ClassZNE_RouteInfoGuidePoint_t,"stPos","Lnet/zmap/android/pnd/v2/data/JNITwoLong;");
			ctorID4 = (*env)->GetMethodID(env, ClassZNE_Location_t, "<init>", "()V");

			obj_subZNE_Location_t = (jobject)(*env)->NewObject(env, ClassZNE_Location_t, ctorID4);

			if(NULL == obj_subZNE_Location_t)
			{
				LOGDp("[DEBUG] JNI_DG_GetRouteInfo  NewObject obj_subZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
			}

			jfieldID jlnLonMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLong","J");
			if(jlnLonMillSec)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_subZNE_Location_t,jlnLonMillSec, (jlong)RouteInfo.stGuidePoint[i].stPos.lnLonMillSec);
			}
			else
			{
				LOGDp("[DEBUG] JNI_DG_GetRouteInfo GetFieldID jlnLonMillSec Failed");
			}

			jfieldID jlnLatMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLat","J");
			if(jlnLatMillSec)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_subZNE_Location_t,jlnLatMillSec, (jlong)RouteInfo.stGuidePoint[i].stPos.lnLatMillSec);
			}
			else
			{
				LOGDp("[DEBUG] JNI_DG_GetRouteInfo GetFieldID jlnLatMillSec Failed");
			}

			(*env)->SetObjectField(env, obj_RouteInfoGuidePoint_t, jsubstPos, obj_subZNE_Location_t);

		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo  Find  class ClassZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		(*env)->SetObjectArrayElement(env, objArray_RouteInfoGuidePoint_t, i, obj_RouteInfoGuidePoint_t);

		if(NULL != obj_subZNE_Location_t)
		{
			(*env)->DeleteLocalRef( env,obj_subZNE_Location_t);
			obj_subZNE_Location_t = NULL;
		}
		if(NULL != obj_RouteInfoGuidePoint_t)
		{
			(*env)->DeleteLocalRef( env,obj_RouteInfoGuidePoint_t);
			obj_RouteInfoGuidePoint_t = NULL;
		}
	}

	if(jfld_stGuidePoint != NULL)
	{
		(*env)->SetObjectField(env, object, jfld_stGuidePoint, objArray_RouteInfoGuidePoint_t);
	}

	if(ClassZNE_RouteInfo_t != NULL)
	{
		jfieldID jiViaValidCount = (*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"iViaValidCount","I");
		if(jiViaValidCount)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jiViaValidCount, (jint)RouteInfo.iViaValidCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo GetFieldID jiViaValidCount Failed");
		}

		jfieldID jiRouteSearchType = (*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"iRouteSearchType","I");
		if(jiRouteSearchType)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jiRouteSearchType, (jint)RouteInfo.iRouteSearchType);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo GetFieldID jiRouteSearchType Failed");
		}

	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetRouteInfo  Find  class ClassZNE_RouteInfo_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	if(NULL != objArray_RouteInfoGuidePoint_t)
	{
		(*env)->DeleteLocalRef( env,objArray_RouteInfoGuidePoint_t);
		objArray_RouteInfoGuidePoint_t = NULL;
	}
	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_CT_Man_WG_getRouteInfo      *
*	Description	:                                            *
*	Date		: 2010/01/05                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_Man_WG_getRouteInfo
  (JNIEnv *env, jclass clz,jobject object)
{
	ZNE_RouteInfo_t		RouteInfo;
	jmethodID ctorID;
	//jmethodID ctorID2;
	jobject  obj_ZNE_Location_t = NULL;
	jobjectArray  objArray_RouteInfoGuidePoint_t = 0;
	jlong Ret = 0;

	memset( &RouteInfo, 0, sizeof( RouteInfo ) );

	Ret = (jlong)CT_Man_WG_getRouteInfo(&RouteInfo);

	jclass ClassZNE_RouteInfo_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNE_RouteInfo_t");
	jclass ClassZNE_Location_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoLong");
	jclass ClassZNE_RouteInfoGuidePoint_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNE_RouteInfoGuidePoint_t");

	jfieldID jfld_ZNE_Location_t = (*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"stPos","Lnet/zmap/android/pnd/v2/data/JNITwoLong;");
	// jfieldID jfld_ZNE_RouteInfoGuidePoint_t = (*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"stGuidePoint","[Lnet/zmap/android/pnd/v2/data/ZNE_RouteInfoGuidePoint_t;");

	if(ClassZNE_Location_t != NULL)
	{
		ctorID = (*env)->GetMethodID(env, ClassZNE_Location_t, "<init>", "()V");

		obj_ZNE_Location_t = (jobject)(*env)->NewObject(env, ClassZNE_Location_t, ctorID);

		if(NULL == obj_ZNE_Location_t)
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo  NewObject obj_ZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		jfieldID jlnLonMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLong","J");
		if(jlnLonMillSec)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, obj_ZNE_Location_t,jlnLonMillSec, (jlong)RouteInfo.stPos.lnLonMillSec);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo GetFieldID jlnLonMillSec Failed");
		}

		jfieldID jlnLatMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLat","J");
		if(jlnLatMillSec)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, obj_ZNE_Location_t,jlnLatMillSec, (jlong)RouteInfo.stPos.lnLatMillSec);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo GetFieldID jlnLatMillSec Failed");
		}

		if(jfld_ZNE_Location_t != NULL)
		{
			(*env)->SetObjectField(env, object, jfld_ZNE_Location_t, obj_ZNE_Location_t);
		}

		if(NULL != obj_ZNE_Location_t)
		{
			(*env)->DeleteLocalRef( env,obj_ZNE_Location_t);
			obj_ZNE_Location_t = NULL;
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetRouteInfo  Find  class ClassZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	jfieldID jfld_stGuidePoint = (*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"stGuidePoint","[Lnet/zmap/android/pnd/v2/data/ZNE_RouteInfoGuidePoint_t;");

	if(ClassZNE_RouteInfoGuidePoint_t != NULL)
	{
		objArray_RouteInfoGuidePoint_t = (*env)->NewObjectArray(env, DEF_POINT_TYPE_VIA_COUNT, ClassZNE_RouteInfoGuidePoint_t, NULL);
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetRouteInfo  Find ClassZNE_RouteInfoGuidePoint_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	int i = 0;
	for(i=0; i<DEF_POINT_TYPE_VIA_COUNT; i++)
	{
		jmethodID  ctorID3;
		jmethodID  ctorID4;
		jobject    obj_RouteInfoGuidePoint_t = NULL;
		jobject    obj_subZNE_Location_t = NULL;
		if(ClassZNE_RouteInfoGuidePoint_t != NULL)
		{
			ctorID3 = (*env)->GetMethodID(env, ClassZNE_RouteInfoGuidePoint_t, "<init>", "()V");
			obj_RouteInfoGuidePoint_t = (jobject)(*env)->NewObject(env, ClassZNE_RouteInfoGuidePoint_t, ctorID3);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo  Find ClassZNE_RouteInfoGuidePoint_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		if(strlen(RouteInfo.stGuidePoint[i].szPointName) > 0)
		{
			jfieldID jszPointName = (*env)->GetFieldID(env, ClassZNE_RouteInfoGuidePoint_t,"szPointName","Ljava/lang/String;");
			jclass strClass = (*env)->FindClass(env, "java/lang/String");
			jmethodID ctorIDstr = (*env)->GetMethodID(env, strClass,  "<init>", "([BLjava/lang/String;)V");
			jbyteArray bytes = (*env)->NewByteArray(env, strlen((char*)RouteInfo.stGuidePoint[i].szPointName));
			if(bytes != NULL)
			{
				(*env)->SetByteArrayRegion(env, bytes, 0, strlen((char*)RouteInfo.stGuidePoint[i].szPointName), (jbyte*)RouteInfo.stGuidePoint[i].szPointName);
			}
			jstring encoding = (*env)->NewStringUTF(env, "ShiftJIS");
			jstring  jstrszName = (jstring)(*env)->NewObject(env, strClass, ctorIDstr, bytes, encoding);

			if(jstrszName)
			{
				//Set the variable's value  to Java
				(*env)->SetObjectField(env, obj_RouteInfoGuidePoint_t, jszPointName, jstrszName);

			}
			else
			{
				LOGDp("[DEBUG] JNI_DG_GetRouteInfo NewObject jstrszName Failed");
			}

			if(NULL != bytes)
			{
				(*env)->DeleteLocalRef( env,bytes);
				bytes = NULL;
			}
			if(NULL != encoding)
			{
				(*env)->DeleteLocalRef( env,encoding);
				encoding = NULL;
			}
			if(NULL != jstrszName)
			{
				(*env)->DeleteLocalRef(env, jstrszName);
				jstrszName = NULL;
			}
		}

		if(ClassZNE_Location_t != NULL)
		{
			jfieldID jsubstPos = (*env)->GetFieldID(env, ClassZNE_RouteInfoGuidePoint_t,"stPos","Lnet/zmap/android/pnd/v2/data/JNITwoLong;");
			ctorID4 = (*env)->GetMethodID(env, ClassZNE_Location_t, "<init>", "()V");

			obj_subZNE_Location_t = (jobject)(*env)->NewObject(env, ClassZNE_Location_t, ctorID4);

			if(NULL == obj_subZNE_Location_t)
			{
				LOGDp("[DEBUG] JNI_DG_GetRouteInfo  NewObject obj_subZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
			}

			jfieldID jlnLonMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLong","J");
			if(jlnLonMillSec)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_subZNE_Location_t,jlnLonMillSec, (jlong)RouteInfo.stGuidePoint[i].stPos.lnLonMillSec);
			}
			else
			{
				LOGDp("[DEBUG] JNI_DG_GetRouteInfo GetFieldID jlnLonMillSec Failed");
			}

			jfieldID jlnLatMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLat","J");
			if(jlnLatMillSec)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_subZNE_Location_t,jlnLatMillSec, (jlong)RouteInfo.stGuidePoint[i].stPos.lnLatMillSec);
			}
			else
			{
				LOGDp("[DEBUG] JNI_DG_GetRouteInfo GetFieldID jlnLatMillSec Failed");
			}

			(*env)->SetObjectField(env, obj_RouteInfoGuidePoint_t, jsubstPos, obj_subZNE_Location_t);

		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo  Find  class ClassZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		(*env)->SetObjectArrayElement(env, objArray_RouteInfoGuidePoint_t, i, obj_RouteInfoGuidePoint_t);
		if(NULL != obj_subZNE_Location_t)
		{
			(*env)->DeleteLocalRef( env,obj_subZNE_Location_t);
			obj_subZNE_Location_t = NULL;
		}
		if(NULL != obj_RouteInfoGuidePoint_t)
		{
			(*env)->DeleteLocalRef(env, obj_RouteInfoGuidePoint_t);
			obj_RouteInfoGuidePoint_t = NULL;
		}
	}

	if(jfld_stGuidePoint != NULL)
	{
		(*env)->SetObjectField(env, object, jfld_stGuidePoint, objArray_RouteInfoGuidePoint_t);
	}

	if(ClassZNE_RouteInfo_t != NULL)
	{
		jfieldID jiViaValidCount = (*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"iViaValidCount","I");
		if(jiViaValidCount)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jiViaValidCount, (jint)RouteInfo.iViaValidCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo GetFieldID jiViaValidCount Failed");
		}

		jfieldID jiRouteSearchType = (*env)->GetFieldID(env, ClassZNE_RouteInfo_t,"iRouteSearchType","I");
		if(jiRouteSearchType)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jiRouteSearchType, (jint)RouteInfo.iRouteSearchType);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetRouteInfo GetFieldID jiRouteSearchType Failed");
		}

	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetRouteInfo  Find  class ClassZNE_RouteInfo_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	if(NULL != objArray_RouteInfoGuidePoint_t)
	{
		(*env)->DeleteLocalRef(env, objArray_RouteInfoGuidePoint_t);
		objArray_RouteInfoGuidePoint_t = NULL;
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_DG_GetGuidePointInfo      *
*	Description	:                                            *
*	Date		: 2010/01/05                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_DG_GetGuidePointInfo
  (JNIEnv *env, jclass clz,jobject object)
{
	ZNE_RouteGuidance_t		GuidePoint;
	jmethodID ctorID;
	jmethodID ctorID2;
	jobject  obj_ZNE_Location_t = 0;
	jobject  obj_ZNE_RouteInfoGuidePoint_t = 0;
	jlong Ret = 0;

	memset( &GuidePoint, 0, sizeof( GuidePoint ) );

	Ret = (jlong)DG_GetGuidePointInfo(&GuidePoint);

	jclass ClassZNE_RouteGuidance_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNE_RouteGuidance_t");
	jclass ClassZNE_Location_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoLong");
	jclass ClassZNE_RouteInfoGuidePoint_t = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNE_RouteInfoGuidePoint_t");

	jfieldID jfld_ZNE_Location_t = (*env)->GetFieldID(env, ClassZNE_RouteInfoGuidePoint_t,"stPos","Lnet/zmap/android/pnd/v2/data/JNITwoLong;");
	jfieldID jfld_ZNE_RouteInfoGuidePoint_t = (*env)->GetFieldID(env, ClassZNE_RouteGuidance_t,"stGuidePoint","Lnet/zmap/android/pnd/v2/data/ZNE_RouteInfoGuidePoint_t;");

	if(ClassZNE_Location_t != NULL)
	{
		ctorID = (*env)->GetMethodID(env, ClassZNE_Location_t, "<init>", "()V");

		obj_ZNE_Location_t = (jobject)(*env)->NewObject(env, ClassZNE_Location_t, ctorID);

		if(NULL == obj_ZNE_Location_t)
		{
			LOGDp("[DEBUG] JNI_DG_GetGuidePointInfo  NewObject obj_ZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		jfieldID jlnLonMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLong","J");
		if(jlnLonMillSec)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, obj_ZNE_Location_t,jlnLonMillSec, (jlong)GuidePoint.stGuidePoint.stPos.lnLonMillSec);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetGuidePointInfo GetFieldID jlnLonMillSec Failed");
		}

		jfieldID jlnLatMillSec = (*env)->GetFieldID(env, ClassZNE_Location_t,"m_lLat","J");
		if(jlnLatMillSec)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, obj_ZNE_Location_t,jlnLatMillSec, (jlong)GuidePoint.stGuidePoint.stPos.lnLatMillSec);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetGuidePointInfo GetFieldID jlnLatMillSec Failed");
		}

	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetGuidePointInfo  Find  class ClassZNE_Location_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	if(ClassZNE_RouteInfoGuidePoint_t != NULL)
	{
		ctorID2 = (*env)->GetMethodID(env, ClassZNE_RouteInfoGuidePoint_t, "<init>", "()V");

		obj_ZNE_RouteInfoGuidePoint_t = (jobject)(*env)->NewObject(env, ClassZNE_RouteInfoGuidePoint_t, ctorID2);

		if(NULL != obj_ZNE_RouteInfoGuidePoint_t)
		{
			(*env)->SetObjectField(env, obj_ZNE_RouteInfoGuidePoint_t, jfld_ZNE_Location_t, obj_ZNE_Location_t);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetGuidePointInfo  NewObject obj_ZNE_RouteInfoGuidePoint_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		if(strlen(GuidePoint.stGuidePoint.szPointName) > 0)
		{
			jfieldID jszPointName = (*env)->GetFieldID(env, ClassZNE_RouteInfoGuidePoint_t,"szPointName","Ljava/lang/String;");
			jclass strClass = (*env)->FindClass(env, "java/lang/String");
			jmethodID ctorIDstr = (*env)->GetMethodID(env, strClass,  "<init>", "([BLjava/lang/String;)V");
			jbyteArray bytes = (*env)->NewByteArray(env, strlen((char*)GuidePoint.stGuidePoint.szPointName));
			if(bytes != NULL)
			{
				(*env)->SetByteArrayRegion(env, bytes, 0, strlen((char*)GuidePoint.stGuidePoint.szPointName), (jbyte*)GuidePoint.stGuidePoint.szPointName);
			}
			jstring encoding = (*env)->NewStringUTF(env, "ShiftJIS");
			jstring  jstrszName = (jstring)(*env)->NewObject(env, strClass, ctorIDstr, bytes, encoding);

			if(jstrszName)
			{
				//Set the variable's value  to Java
				(*env)->SetObjectField(env, obj_ZNE_RouteInfoGuidePoint_t, jszPointName, jstrszName);
			}
			else
			{
				LOGDp("[DEBUG] JNI_DG_GetGuidePointInfo NewObject jstrszName Failed");
			}

			if(NULL != bytes)
			{
				(*env)->DeleteLocalRef( env,bytes);
				bytes = NULL;
			}
			if(NULL != encoding)
			{
				(*env)->DeleteLocalRef( env,encoding);
				encoding = NULL;
			}
			if(NULL != jstrszName)
			{
				(*env)->DeleteLocalRef(env, jstrszName);
				jstrszName = NULL;
			}
		}

		(*env)->SetObjectField(env, object, jfld_ZNE_RouteInfoGuidePoint_t, obj_ZNE_RouteInfoGuidePoint_t);

		if(NULL != obj_ZNE_Location_t)
		{
			(*env)->DeleteLocalRef( env,obj_ZNE_Location_t);
			obj_ZNE_Location_t = NULL;
		}

		if(NULL != obj_ZNE_RouteInfoGuidePoint_t)
		{
			(*env)->DeleteLocalRef( env,obj_ZNE_RouteInfoGuidePoint_t);
			obj_ZNE_RouteInfoGuidePoint_t = NULL;
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetGuidePointInfo  Find  class ClassZNE_RouteInfoGuidePoint_t  Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	if(ClassZNE_RouteGuidance_t != NULL)
	{
		jfieldID jiEventCode = (*env)->GetFieldID(env, ClassZNE_RouteGuidance_t,"iEventCode","I");
		if(jiEventCode)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jiEventCode, (jint)GuidePoint.iEventCode);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetGuidePointInfo GetFieldID jiEventCode Failed");
		}

		jfieldID jiEventDetailCode = (*env)->GetFieldID(env, ClassZNE_RouteGuidance_t,"iEventDetailCode","I");
		if(jiEventDetailCode)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jiEventDetailCode, (jint)GuidePoint.iEventDetailCode);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetGuidePointInfo GetFieldID jiEventDetailCode Failed");
		}

		jfieldID jfDist = (*env)->GetFieldID(env, ClassZNE_RouteGuidance_t,"iDist","F");
		if(jfDist)
		{
			//Set the variable's value  to Java
			(*env)->SetFloatField(env, object, jfDist, (jfloat)GuidePoint.iDist);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetGuidePointInfo GetFieldID jfDist Failed");
		}

		jfieldID jiTargetDirection = (*env)->GetFieldID(env, ClassZNE_RouteGuidance_t,"iTargetDirection","I");
		if(jiTargetDirection)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object, jiTargetDirection, (jint)GuidePoint.iTargetDirection);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetGuidePointInfo GetFieldID jiTargetDirection Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetGuidePointInfo  Find  class ClassZNE_RouteGuidance_t Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: Java_GetDestinationGuide      *
*	Description	:                                            *
*	Date		: 2010/01/07                                 *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_Java_GetDestinationGuide
  (JNIEnv *env, jclass clz,jobject object)
{
	ZNUI_UI_MARK_DATA		markData;
	jmethodID ctorID;
	jmethodID ctorID2;
	jobject  obj_TwoLong;
	jobject  obj_ZNUI_ANGLE_DATA;
	jlong Ret = 0;

	memset( &markData, 0, sizeof( markData ) );
	Java_GetDestinationGuide(&markData);

	jclass ClassZNUI_UI_MARK_DATA = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNUI_UI_MARK_DATA");
	jclass ClassTwoLong = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoLong");
	jclass ClassZNUI_ANGLE_DATA = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNUI_ANGLE_DATA");

	jfieldID jfld_stPixInfo = (*env)->GetFieldID(env, ClassZNUI_UI_MARK_DATA,"stPixInfo","Lnet/zmap/android/pnd/v2/data/JNITwoLong;");
	jfieldID jfld_stAngle = (*env)->GetFieldID(env, ClassZNUI_UI_MARK_DATA,"stAngle","Lnet/zmap/android/pnd/v2/data/ZNUI_ANGLE_DATA;");

	if(ClassTwoLong != NULL)
	{
		ctorID = (*env)->GetMethodID(env, ClassTwoLong, "<init>", "()V");
		obj_TwoLong = (jobject)(*env)->NewObject(env, ClassTwoLong, ctorID);
		if(NULL != obj_TwoLong)
		{

			jfieldID jlnPixX = (*env)->GetFieldID(env, ClassTwoLong,"m_lLong","J");
			if(jlnPixX)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_TwoLong,jlnPixX, (jlong)markData.stPixInfo.lnPixX);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetDestinationGuide GetFieldID jlnPixX Failed");
			}

			jfieldID jlnPixY = (*env)->GetFieldID(env, ClassTwoLong,"m_lLat","J");
			if(jlnPixY)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_TwoLong,jlnPixY, (jlong)markData.stPixInfo.lnPixY);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetDestinationGuide GetFieldID jlnPixY Failed");
			}

			(*env)->SetObjectField(env, object, jfld_stPixInfo, obj_TwoLong);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetDestinationGuide  NewObject obj_TwoLong Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}
		if(NULL != obj_TwoLong)
		{
			(*env)->DeleteLocalRef( env,obj_TwoLong);
			obj_TwoLong = NULL;
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetDestinationGuide  Find  class ClassTwoLong Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	if(ClassZNUI_ANGLE_DATA != NULL)
	{
		ctorID2 = (*env)->GetMethodID(env, ClassZNUI_ANGLE_DATA, "<init>", "()V");
		obj_ZNUI_ANGLE_DATA = (jobject)(*env)->NewObject(env, ClassZNUI_ANGLE_DATA, ctorID2);

		if(NULL != obj_ZNUI_ANGLE_DATA)
		{

			jfieldID jsAngle = (*env)->GetFieldID(env, ClassZNUI_ANGLE_DATA,"sAngle","S");
			if(jsAngle)
			{
				//Set the variable's value  to Java
				(*env)->SetShortField(env, obj_ZNUI_ANGLE_DATA, jsAngle, (jshort)markData.stAngle.nAngle);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetDestinationGuide GetFieldID jsAngle Failed");
			}

			jfieldID jiAngleNo = (*env)->GetFieldID(env, ClassZNUI_ANGLE_DATA,"iAngleNo","I");
			if(jiAngleNo)
			{
				//Set the variable's value  to Java
				(*env)->SetIntField(env, obj_ZNUI_ANGLE_DATA, jiAngleNo, (jint)markData.stAngle.byAngleNo);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetDestinationGuide GetFieldID jiAngleNo Failed");
			}

			jfieldID jiReserved = (*env)->GetFieldID(env, ClassZNUI_ANGLE_DATA,"iReserved","I");
			if(jiReserved)
			{
				//Set the variable's value  to Java
				(*env)->SetIntField(env, obj_ZNUI_ANGLE_DATA, jiReserved, (jint)markData.stAngle.byReserved);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetDestinationGuide GetFieldID jiReserved Failed");
			}

			(*env)->SetObjectField(env, object, jfld_stAngle, obj_ZNUI_ANGLE_DATA);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetDestinationGuide  NewObject obj_ZNUI_ANGLE_DATA Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		if(NULL != obj_ZNUI_ANGLE_DATA)
		{
			(*env)->DeleteLocalRef( env,obj_ZNUI_ANGLE_DATA);
			obj_ZNUI_ANGLE_DATA = NULL;
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetDestinationGuide  Find  class ClassZNUI_ANGLE_DATA Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetSetDestinationGuide      *
*	Description	:                                            *
*	Date		: 2010/01/07                                 *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_Java_GetSetDestinationGuide
  (JNIEnv *env, jclass clz,jobject object)
{
	ZNUI_UI_MARK_DATA		markData;
	jmethodID ctorID;
	jmethodID ctorID2;
	jobject  obj_TwoLong;
	jobject  obj_ZNUI_ANGLE_DATA;
	jlong Ret = 0;

	memset( &markData, 0, sizeof( markData ) );

	Java_GetSetDestinationGuide(&markData);

	jclass ClassZNUI_UI_MARK_DATA = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNUI_UI_MARK_DATA");
	jclass ClassTwoLong = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoLong");
	jclass ClassZNUI_ANGLE_DATA = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNUI_ANGLE_DATA");

	jfieldID jfld_stPixInfo = (*env)->GetFieldID(env, ClassZNUI_UI_MARK_DATA,"stPixInfo","Lnet/zmap/android/pnd/v2/data/JNITwoLong;");
	jfieldID jfld_stAngle = (*env)->GetFieldID(env, ClassZNUI_UI_MARK_DATA,"stAngle","Lnet/zmap/android/pnd/v2/data/ZNUI_ANGLE_DATA;");

	if(ClassTwoLong != NULL)
	{
		ctorID = (*env)->GetMethodID(env, ClassTwoLong, "<init>", "()V");

		obj_TwoLong = (jobject)(*env)->NewObject(env, ClassTwoLong, ctorID);

		if(NULL != obj_TwoLong)
		{

			jfieldID jlnPixX = (*env)->GetFieldID(env, ClassTwoLong,"m_lLong","J");
			if(jlnPixX)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_TwoLong,jlnPixX, (jlong)markData.stPixInfo.lnPixX);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetSetDestinationGuide GetFieldID jlnPixX Failed");
			}

			jfieldID jlnPixY = (*env)->GetFieldID(env, ClassTwoLong,"m_lLat","J");
			if(jlnPixY)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env, obj_TwoLong,jlnPixY, (jlong)markData.stPixInfo.lnPixY);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetSetDestinationGuide GetFieldID jlnPixY Failed");
			}

			(*env)->SetObjectField(env, object, jfld_stPixInfo, obj_TwoLong);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetSetDestinationGuide  NewObject obj_TwoLong Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		if(NULL != obj_TwoLong)
		{
			(*env)->DeleteLocalRef( env,obj_TwoLong);
			obj_TwoLong = NULL;
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetSetDestinationGuide  Find  class ClassTwoLong Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	if(ClassZNUI_ANGLE_DATA != NULL)
	{
		ctorID2 = (*env)->GetMethodID(env, ClassZNUI_ANGLE_DATA, "<init>", "()V");
		obj_ZNUI_ANGLE_DATA = (jobject)(*env)->NewObject(env, ClassZNUI_ANGLE_DATA, ctorID2);

		if(NULL != obj_ZNUI_ANGLE_DATA)
		{

			jfieldID jsAngle = (*env)->GetFieldID(env, ClassZNUI_ANGLE_DATA,"sAngle","S");
			if(jsAngle)
			{
				//Set the variable's value  to Java
				(*env)->SetShortField(env, obj_ZNUI_ANGLE_DATA, jsAngle, (jshort)markData.stAngle.nAngle);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetSetDestinationGuide GetFieldID jsAngle Failed");
			}

			jfieldID jiAngleNo = (*env)->GetFieldID(env, ClassZNUI_ANGLE_DATA,"iAngleNo","I");
			if(jiAngleNo)
			{
				//Set the variable's value  to Java
				(*env)->SetIntField(env, obj_ZNUI_ANGLE_DATA, jiAngleNo, (jint)markData.stAngle.byAngleNo);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetSetDestinationGuide GetFieldID jiAngleNo Failed");
			}

			jfieldID jiReserved = (*env)->GetFieldID(env, ClassZNUI_ANGLE_DATA,"iReserved","I");
			if(jiReserved)
			{
				//Set the variable's value  to Java
				(*env)->SetIntField(env, obj_ZNUI_ANGLE_DATA, jiReserved, (jint)markData.stAngle.byReserved);
			}
			else
			{
				LOGDp("[DEBUG] JNI_Java_GetSetDestinationGuide GetFieldID jiReserved Failed");
			}

			(*env)->SetObjectField(env, object, jfld_stAngle, obj_ZNUI_ANGLE_DATA);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetSetDestinationGuide  NewObject obj_ZNUI_ANGLE_DATA Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		if(NULL != obj_ZNUI_ANGLE_DATA)
		{
			(*env)->DeleteLocalRef( env,obj_ZNUI_ANGLE_DATA);
			obj_ZNUI_ANGLE_DATA = NULL;
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetSetDestinationGuide  Find  class ClassZNUI_ANGLE_DATA Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetShowDestinationGuide      *
*	Description	:                                            *
*	Date		: 2010/1/07                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetShowDestinationGuide
(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	int     iState = 0;

	Java_GetShowDestinationGuide((ENUI_STATE_TYPE*)&iState);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jiState = (*env)->GetFieldID(env, clazz,"m_iCommon","I");
		if(jiState)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jiState,(jint)iState);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetShowDestinationGuide SetIntField jiState Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetShowDestinationGuide FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetShowInformation      *
*	Description	:                                            *
*	Date		: 2010/1/07                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetShowInformation
(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	int     iState = 0;

	Java_GetShowInformation((ENUI_STATE_TYPE*)&iState);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jiState = (*env)->GetFieldID(env, clazz,"m_iCommon","I");
		if(jiState)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jiState,(jint)iState);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetShowInformation SetIntField jiState Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetShowInformation FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetInformationInfo      *
*	Description	:                                            *
*	Date		: 2010/1/07                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetInformationInfo
(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	char	pszStr[128];
	jmethodID  ctorID = 0;
	jfieldID   Name = 0;

	Java_GetInformationInfo(pszStr);

	jclass cls = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIString");
	if(cls)
	{
		Name = (*env)->GetFieldID(env,cls,"m_StrAddressString","Ljava/lang/String;");
	}

	if(strlen(pszStr) > 0)
	{
		//Fing Java's Class
		jclass strClass = (*env)->FindClass(env,"java/lang/String");
		if(strClass != NULL)
		{
			ctorID = (*env)->GetMethodID(env,strClass, "<init>", "([BLjava/lang/String;)V");
		}
		jbyteArray bytes = (*env)->NewByteArray(env,strlen(pszStr));
		if(bytes != NULL)
		{
			(*env)->SetByteArrayRegion(env,bytes, 0, strlen(pszStr), (jbyte*)pszStr);

		}
		jstring encoding = (*env)->NewStringUTF(env,"ShiftJIS");
		jstring  jsName = (jstring)(*env)->NewObject(env,strClass, ctorID, bytes, encoding);

		if(jsName)
		{
			(*env)->SetObjectField(env,object,Name,jsName);
		}

		if(NULL != bytes)
		{
			(*env)->DeleteLocalRef(env, bytes);
			bytes = NULL;
		}
		if(NULL != encoding)
		{
			(*env)->DeleteLocalRef(env, encoding);
			encoding = NULL;
		}
		if(NULL != jsName)
		{
			(*env)->DeleteLocalRef(env, jsName);
			jsName = NULL;
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetName      *
*	Description	:                                            *
*	Date		: 2010/1/07                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetName
(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	char	pName[68];
	jmethodID  ctorID = 0;
	jfieldID   Name = 0;

	Java_GetName( pName );

	jclass cls = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIString");
	if(cls)
	{
		Name = (*env)->GetFieldID(env,cls,"m_StrAddressString","Ljava/lang/String;");
	}

	if(strlen(pName) > 0)
	{
		//Fing Java's Class
		jclass strClass = (*env)->FindClass(env,"java/lang/String");
		if(strClass != NULL)
		{
			ctorID = (*env)->GetMethodID(env,strClass, "<init>", "([BLjava/lang/String;)V");
		}
		jbyteArray bytes = (*env)->NewByteArray(env,strlen(pName));
		if(bytes != NULL)
		{
			(*env)->SetByteArrayRegion(env,bytes, 0, strlen(pName), (jbyte*)pName);

		}
		jstring encoding = (*env)->NewStringUTF(env,"ShiftJIS");
		jstring  jsName = (jstring)(*env)->NewObject(env,strClass, ctorID, bytes, encoding);

		if(jsName)
		{
			(*env)->SetObjectField(env,object,Name,jsName);
		}

		if(NULL != bytes)
		{
			(*env)->DeleteLocalRef(env, bytes);
			bytes = NULL;
		}
		if(NULL != encoding)
		{
			(*env)->DeleteLocalRef(env, encoding);
			encoding = NULL;
		}
		if(NULL != jsName)
		{
			(*env)->DeleteLocalRef(env, jsName);
			jsName = NULL;
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_GetLaneRestDistance			*
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetLaneRestDistance(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	long	plnLaneRestDistance = 0;

	Java_GetLaneRestDistance( &plnLaneRestDistance );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jplnLaneRestDistance = (*env)->GetFieldID(env, clazz,"lcount","J");
		if(jplnLaneRestDistance)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jplnLaneRestDistance,(jlong)plnLaneRestDistance);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetLaneRestDistance Getfield jplnLaneRestDistance Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetLaneRestDistance FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_Java_GetMagMapRestDistance			*
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetMagMapRestDistance(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	long	plnMagMapRestDistance = 0;

	Java_GetMagMapRestDistance( &plnMagMapRestDistance );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jplnMagMapRestDistance = (*env)->GetFieldID(env, clazz,"lcount","J");
		if(jplnMagMapRestDistance)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jplnMagMapRestDistance,(jlong)plnMagMapRestDistance);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetMagMapRestDistance Getfield jplnLaneRestDistance Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetMagMapRestDistance FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}
// Wangxp add end

/*********************************************************************
*	Function Name	: JNI_Java_GetMagMapRestDistance			*
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_Java_GetJCTImagRestDistance(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	long	plnMagMapRestDistance = 0;

	Java_GetJCTImagRestDistance( &plnMagMapRestDistance );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jplnMagMapRestDistance = (*env)->GetFieldID(env, clazz,"lcount","J");
		if(jplnMagMapRestDistance)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jplnMagMapRestDistance,(jlong)plnMagMapRestDistance);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetJCTImagRestDistance Getfield jplnLaneRestDistance Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetJCTImagRestDistance FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

JNIEXPORT jlong JNICALL JNI_Java_GetShowPOIIconInformation(JNIEnv *env, jclass clz, jobject object)
{
	jlong 	lRet = 0;
	long		lnFlag = 0;
	long 	lnLon = 0;
	long 	lnLat = 0;
	long 	lnLayerID = -1;

	Java_GetShowPOIIconInformation( &lnFlag, &lnLon, &lnLat, &lnLayerID);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/PoiIconInfo");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jplnFlag = (*env)->GetFieldID(env, clazz,"lFlag","J");
		if(jplnFlag)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jplnFlag,(jlong)lnFlag);
		}
		else
		{
			LOGDp("[DEBUG]Failed");
		}
		jfieldID jplnLon = (*env)->GetFieldID(env, clazz,"lLon","J");
		if(jplnLon)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jplnLon,(jlong)lnLon);
		}
		else
		{
			LOGDp("[DEBUG]Failed");
		}
		jfieldID jplnLat = (*env)->GetFieldID(env, clazz,"lLat","J");
		if(jplnLat)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jplnLat,(jlong)lnLat);
		}
		else
		{
			LOGDp("[DEBUG]Failed");
		}
		jfieldID jplnLayerID = (*env)->GetFieldID(env, clazz,"lLayerID","J");
		if(jplnLayerID)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jplnLayerID,(jlong)lnLayerID);
		}
		else
		{
			LOGDp("[DEBUG]Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetShowPOIIconInformation FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

//Add 2011/09/19 Z01yamaguchi Start -->
JNIEXPORT jlong JNICALL JNI_JAVA_GetDriConPOIIconInformationState(JNIEnv *env, jclass clz, jobject state, jobject count)
{
	jlong lRet = 0;
	long lnState = 0;
	int  nDataCnt = 0;

	JAVA_GetDriConPOIIconInformationState( &lnState, &nDataCnt );

	jclass jclazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIInt");

	if(jclazz)
	{
		// ドライブコンテンツ有無, コンテンツ件数
		jfieldID jiData = (*env)->GetFieldID(env, jclazz, "m_iCommon", "I");
		if(jiData)
		{
			(*env)->SetIntField(env, state, jiData, (jint)lnState);
			(*env)->SetIntField(env, count, jiData, (jint)nDataCnt);
		}
		else
		{
			LOGDp("[DEBUG] JNI_JAVA_GetDriConPOIIconInformationState GetFieldID m_iCommon Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_JAVA_GetDriConPOIIconInformationState FindClass JNIInt Failed!!!!!!!!!!!!!!!");
	}

	return lRet;
}
JNIEXPORT jlong JNICALL JNI_JAVA_GetDriConPOIIconInformation(JNIEnv *env, jclass clz, jint areaCount, jobjectArray poiInfoArray)
{
	jlong lRet = 0;
	ZNUI_POI_SHOW_INFORMATION* pstPOIShowInfo = NULL;

	if( areaCount <= 0 ){
		return CT_NONPROC;
	}

	pstPOIShowInfo = malloc( areaCount * sizeof(ZNUI_POI_SHOW_INFORMATION) );
	if( pstPOIShowInfo == NULL ){
		return CT_ERR_MALLOC;
	}

	JAVA_GetDriConPOIIconInformation( areaCount, pstPOIShowInfo );

	//Fing Java's Class
	jclass jclazzPoiInfo = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/DC_POIInfoShowList");

	if(jclazzPoiInfo)
	{
		int i;

		// POI名称設定準備
		jfieldID jplnPOIName = (*env)->GetFieldID(env, jclazzPoiInfo, "m_sPOIName", "Ljava/lang/String;");
		jclass strClass = (*env)->FindClass(env, "java/lang/String");
		jmethodID ctorIDstr = (*env)->GetMethodID(env, strClass,  "<init>", "([BLjava/lang/String;)V");

		for( i = 0; i < areaCount; i++ )
		{
			jobject obj = (jobject)(*env)->GetObjectArrayElement(env, (jobjectArray)poiInfoArray, i);

			if(obj)
			{

				// POI名称
				jbyteArray bytes = (*env)->NewByteArray(env, strlen((char*)pstPOIShowInfo[i].szDriConPOIName));
				if(bytes != NULL)
				{
						(*env)->SetByteArrayRegion(env, bytes, 0, strlen(pstPOIShowInfo[i].szDriConPOIName), (jbyte*)pstPOIShowInfo[i].szDriConPOIName);
				}
				jstring encoding = (*env)->NewStringUTF(env, "utf-8");
				jstring jstrszName = (jstring)(*env)->NewObject(env, strClass, ctorIDstr, bytes, encoding);
		
				if(jplnPOIName)
				{
					//Set the variable's value  to Java
					(*env)->SetObjectField(env, obj, jplnPOIName, jstrszName);
				}
				else
				{
					LOGE("[DEBUG] JNI_JAVA_GetDriConPOIIconInformation GetFieldID m_POIName Failed");
				}

				// POI ID
				jfieldID jplnPOIId = (*env)->GetFieldID(env, jclazzPoiInfo, "m_POIId", "J");
				if(jplnPOIId)
				{
					//Set the variable's value  to Java
					(*env)->SetLongField(env, obj, jplnPOIId, (jlong)pstPOIShowInfo[i].llDriConPOIId);
				}
				else
				{
					LOGE("[DEBUG] JNI_JAVA_GetDriConPOIIconInformation GetFieldID m_POIId Failed");
				}
				// 解放
				if(NULL != bytes)
				{
					(*env)->DeleteLocalRef(env, bytes);
					bytes = NULL;
				}
				if(NULL != encoding)
				{
					(*env)->DeleteLocalRef(env, encoding);
					encoding = NULL;
				}

				(*env)->DeleteLocalRef(env, jstrszName);
				(*env)->DeleteLocalRef(env, obj);
			}
			else{
				LOGE("[DEBUG] GetObjectArrayElement obj DC_POIInfoShowList Failed!!!!!!!!!!!!!!!");
			}
		}
		(*env)->DeleteLocalRef(env, strClass);
		(*env)->DeleteLocalRef(env, jclazzPoiInfo);
	}
	else
	{
		LOGE("[DEBUG] JNI_JAVA_GetDriConPOIIconInformation FindClass DC_POIInfoShowList Failed!!!!!!!!!!!!!!!");
	}

	if( pstPOIShowInfo != NULL ){
		free( pstPOIShowInfo );
	}

	return lRet;

}
// ADD.2013.07.02 N.Sasao POI情報リスト表示負荷低減3 START
JNIEXPORT jboolean JNICALL JNI_JAVA_GetIsDriConShow(JNIEnv *env, jclass clz)
{
	return JAVA_GetIsDriConShow();
}
// ADD.2013.07.02 N.Sasao POI情報リスト表示負荷低減3  END

//Add 2011/09/19 Z01yamaguchi End <--
// MDD.2013.06.12 N.Sasao POI情報リスト表示対応  END

JNIEXPORT jlong JNICALL JNI_CT_UIC_RouteFailerProc
  (JNIEnv *env, jclass clz)
{
	return (jlong)CT_UIC_RouteFailerProc();
}
// Wangxp add end

/*********************************************************************
*	Function Name	: JNI_CT_Drv_GetVehiclePosition   	     *
*	Description	:                                            *
*	Date		: 10/07/01                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: 	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_Drv_GetVehiclePosition
  (JNIEnv *env, jclass cls,jobject object)
{
	long Longitude,Latitude;

	jlong lRet = (jlong)CT_Drv_GetVehiclePosition(&Longitude,&Latitude);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNITwoLong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jLongitude = (*env)->GetFieldID(env,clazz,"m_lLong","J");
		if(jLongitude)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env,object,jLongitude,Longitude);
			jfieldID jLatitude = (*env)->GetFieldID(env,clazz,"m_lLat","J");
			if(jLatitude)
			{
				(*env)->SetLongField(env,object,jLatitude,Latitude);
			}
			else
			{
				LOGDp("[DEBUG] JNI_CT_Drv_GetVehiclePosition GetFieldID jLatitude Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_CT_Drv_GetVehiclePosition GetFieldID jLongitude Failed");
		}
	}

	return lRet;
}
// Add 2011/07/12 sawada [AgentHMI] Start -->
/*********************************************************************
* Function Name	: JNI_CT_Drv_GetVehiclePosition2   	     			 *
* Description	:                                            		 *
* Date			: 10/07/01                                 			 *
* Parameter		:                                          		 	 *
* Return Code	: jlong                                       		 *
* Author		: 	                                     			 *
* ------------------------------------------------------------------ *
* Revision History	                                             	 *
* No	Date		Revised by		Description          			 *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_Drv_GetVehiclePosition2
  (JNIEnv *env, jclass cls, jobject object, jobject speed, jobject direction)
{
	long lnLon, lnLat;
	ulong ulnSpeed;
	short nDirection;

	jlong lRet = (jlong)CT_Drv_GetVehiclePosition2(&lnLon, &lnLat, &ulnSpeed, &nDirection);

	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoLong");
	if (clazz)
	{
		// 経度
		jfieldID jLongitude = (*env)->GetFieldID(env, clazz, "m_lLong", "J");
		if (jLongitude)
		{
			(*env)->SetLongField(env, object, jLongitude, lnLon);
		}
		else
		{
			LOGDp("[DEBUG] JNI_CT_Drv_GetVehiclePosition GetFieldID jLongitude Failed");
		}
		// 緯度
		jfieldID jLatitude = (*env)->GetFieldID(env, clazz, "m_lLat", "J");
		if (jLatitude)
		{
			(*env)->SetLongField(env, object, jLatitude, lnLat);
		}
		else
		{
			LOGDp("[DEBUG] JNI_CT_Drv_GetVehiclePosition GetFieldID jLatitude Failed");
		}
	}

	jclass jLongCls = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNILong");
	if (jLongCls)
	{
		// 速度
		jfieldID jSpeed = (*env)->GetFieldID(env, jLongCls, "lcount", "J");
		if (jSpeed)
		{
			(*env)->SetLongField(env, speed, jSpeed, ulnSpeed);
		}
		else
		{
			LOGDp("[DEBUG] JNI_CT_Drv_GetVehiclePosition GetFieldID jSpeed Failed");
		}
	}

	jclass jShortCls = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIShort");
	if (jShortCls)
	{
		// 方向
		jfieldID jDirection = (*env)->GetFieldID(env, jShortCls, "m_sJumpRecCount", "S");
		if (jDirection)
		{
			(*env)->SetShortField(env, direction, jDirection, nDirection);
		}
		else
		{
			LOGDp("[DEBUG] JNI_CT_Drv_GetVehiclePosition GetFieldID jDirection Failed");
		}
	}

	return lRet;
}
// Add 2011/07/12 sawada [AgentHMI] End   <--
/*********************************************************************
*	Function Name	: JNI_DG_RepeatVoice                             *
*	Description	:                                                    *
*	Date		: 10/09/25                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: 	                                                 *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_DG_RepeatVoice
  (JNIEnv *env, jclass clz)
{
	return (jlong)DG_RepeatVoice();
}
/*********************************************************************
*	Function Name	: JNI_DAL_GetLandmarkData			*
*	Description	:                                            *
*	Date		: 10/09/25                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
//JNIEXPORT jlong JNICALL JNI_DAL_GetLandmarkData(JNIEnv *env, jclass clz, short usKindCode,jint eIconSizeType,jobject object)
JNIEXPORT jlong JNICALL JNI_DAL_GetLandmarkData(JNIEnv *env, jclass clz, jint iKindCode,jint eIconSizeType,jobject object)
{
	jlong		lRet = 0;
	int			iPixArrLen = 0;
	ZLandmarkData_t  stImage;
	jmethodID	mid;
	int* PixArray = NULL;
	unsigned short usKindCode = (unsigned short)iKindCode;
	memset(&stImage, 0, sizeof(ZNUI_IMAGE));

         // Liugang DAL start
	DAL_GetLandmarkData(usKindCode,(EDAL_DrawParam_IconSizeType)eIconSizeType,&stImage );
	//DAL_GetLandmarkData(usKindCode,&stImage );
         // Liugang DAL end
	short unWidth = (short)stImage.ucWidth;
	short unHeight = (short)stImage.ucHeight;
	short ucBitCountParPixel = (short)stImage.ucBitCountParPixel;
	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZLandmarkData_t");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jsWidth = (*env)->GetFieldID(env, clazz,"sWidth","S");
		if(jsWidth)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,jsWidth,(jshort)unWidth);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DAL_GetLandmarkData Getfield jsWidth Failed");
		}

		jfieldID jsHeight = (*env)->GetFieldID(env, clazz,"sHeight","S");
		if(jsHeight)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,jsHeight,(jshort)unHeight);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DAL_GetLandmarkData Getfield jsHeight Failed");
		}

		jfieldID jBitCountParPixel = (*env)->GetFieldID(env, clazz,"ucBitCountParPixel","S");
		if(jBitCountParPixel)
		{
			//Set the variable's value  to Java
			(*env)->SetShortField(env, object,jBitCountParPixel,(jshort)ucBitCountParPixel);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DAL_GetLandmarkData Getfield jsHeight Failed");
		}

		iPixArrLen = (unHeight) * (unWidth);
		PixArray = (int*)malloc(iPixArrLen*sizeof(int));
		memset(PixArray, 0, iPixArrLen*sizeof(int) );

		if( NULL != PixArray && NULL != stImage.pucData)
		{
			int pix;
			for(pix=0;pix<iPixArrLen;++pix)
			{
				int bgra = ((int*)stImage.pucData)[pix];
				bgra = (((bgra      ) & 0xFF000000) | 
						((bgra << 16) & 0x00FF0000) | 
						((bgra      ) & 0x0000FF00) | 
						((bgra >> 16) & 0x000000FF)); 
				PixArray[pix] =bgra;
			}
		}

		mid = (*env)->GetMethodID(env,clazz,"initSPixRGBA32","(I)V");
		if(mid && iPixArrLen > 0)
		{
			//LOGDp("[DEBUG] JNI_DAL_GetLandmarkData  before CallObjectMethod  ");
			(*env)->CallVoidMethod(env, object, mid, (jint)iPixArrLen);
			//LOGDp("[DEBUG] JNI_DAL_GetLandmarkData  after CallObjectMethod  ");
		}
		else
		{
			LOGDp("[DEBUG] JNI_DAL_GetLandmarkData  GetMethodID mid  failed");
		}

		jfieldID jfldSPix = (*env)->GetFieldID(env, clazz,"SPixRGBA32","[I");
		if(jfldSPix)
		{
			jintArray iArr = (*env)->NewIntArray(env, (jint)iPixArrLen);
			if(iArr)
			{
				(*env)->SetIntArrayRegion(env, iArr, 0, (jint)iPixArrLen, (jint*)PixArray);
				(*env)->SetObjectField(env, object,jfldSPix,iArr);

			}
			else
			{
				LOGDp("[DEBUG] JNI_DAL_GetLandmarkData  NewIntArray  Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_DAL_GetLandmarkData GetFieldID jfldSPix Failed");
		}

		if(PixArray)
		{
			free(PixArray);
			PixArray = NULL;
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetCompass FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_DG_DecideRoute                             *
*	Description	:                                                    *
*	Date		: 09/11/21                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_DG_DecideRoute
  (JNIEnv *env, jclass clz,jint Type)
{
	return (jlong)DG_DecideRoute((DG_RouteSearchType)Type);
}

/*********************************************************************
*	Function Name	: JNI_Java_SetDriveMode                                               *
*	Description	:                                                                                        *
*	Date		: 09/12/25                                                                          *
*	Parameter	:                                                                                        *
*	Return Code	: jlong                                                                                 *
*	Author		: Wangxp	                                                                           *
*  -------------------------------------------------------------------*
* Revision History	                                                                                         *
* No	Date		Revised by		Description                                               *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_Java_SetDriveMode
  (JNIEnv *env, jclass a,jint i)
{
	jlong	lRet = 0;
	Java_SetDriveMode((int)i);
	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_Java_GetDriveMode                                               *
*	Description	:                                                                                        *
*	Date		: 09/12/25                                                                          *
*	Parameter	:                                                                                        *
*	Return Code	: jlong                                                                                 *
*	Author		: Wangxp	                                                                            *
*  -------------------------------------------------------------------*
* Revision History	                                                                                         *
* No	Date		Revised by		Description                                              *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_Java_GetDriveMode
(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	int     iDriveMode = 0;

	Java_GetDriveMode(&iDriveMode);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jiState = (*env)->GetFieldID(env, clazz,"m_iCommon","I");
		if(jiState)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jiState,(jint)iDriveMode);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetDriveMode SetIntField jiState Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetDriveMode FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_Java_SetSettingInfo                        *
*	Description	:                                                    *
*	Date		: 10/12/14                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: wangmeng	                                         *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_Java_SetSettingInfo
  (JNIEnv *env, jclass a,jint i)
{
	jlong	lRet = 0;
	Java_SetSettingInfo((int)i);
	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_Java_GetSettingInfo                        *
*	Description	:                                                    *
*	Date		: 10/12/14                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: wangmeng	                                         *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_Java_GetSettingInfo
(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	int     iCycleFrequency = 0;

	Java_GetSettingInfo(&iCycleFrequency);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jiState = (*env)->GetFieldID(env, clazz,"m_iCommon","I");
		if(jiState)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env, object,jiState,(jint)iCycleFrequency);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Java_GetSettingInfo SetIntField jiState Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_Java_GetSettingInfo FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_Java_SetAppLevel                                                 *
*	Description	:                                                                                        *
*	Date		: 10/12/14                                                                          *
*	Parameter	:                                                                                        *
*	Return Code	: jlong                                                                                 *
*	Author		: wangmeng	                                                                    *
*  ------------------------------------------------------------------*
* Revision History	                                                                                        *
* No	Date		Revised by		Description                                              *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_Java_SetAppLevel
  (JNIEnv *env, jclass a,jint i)
{
	jlong	lRet = 0;
	Java_SetAppLevel((int)i);
	return lRet;
}

// Add 2011/01/15 sawada Start -->

JNIEXPORT jlong JNICALL
JNI_WG_IsOnRoute(JNIEnv* env, jclass clz, jobject object)
{
	jlong jlnRet = 0;
	int nIsOnRoute = 0;

	WG_JNI_IsOnRoute(&nIsOnRoute);

	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNIInt");
	if (clazz) {
		jfieldID jiState = (*env)->GetFieldID(env, clazz, "m_iCommon", "I");
		if (jiState) {
			(*env)->SetIntField(env, object, jiState, (jint)nIsOnRoute);
		}
	}

	return jlnRet;
}

JNIEXPORT jlong JNICALL
JNI_WG_GetJctDirectionInfo(JNIEnv* env, jclass clz, jobject object)
{
	jlong jlRet = 0;
	long lnDirType = -1;
	long lnWrapType = 0;

	WG_JNI_GetJctDirectionInfo(&lnDirType, &lnWrapType);

	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoInt");
	if (clazz) {
		jfieldID jDirType = (*env)->GetFieldID(env, clazz, "m_iSizeX", "I");
		if (jDirType) {
			(*env)->SetIntField(env, object, jDirType, (jint)lnDirType);
		}
		jfieldID jLinkType = (*env)->GetFieldID(env, clazz, "m_iSizeY", "I");
		if (jLinkType) {
			(*env)->SetIntField(env, object, jLinkType, (jint)lnWrapType);
		}
	}

	return jlRet;
}

JNIEXPORT jlong JNICALL
JNI_WG_GetRestDistance(JNIEnv* env, jclass clz, jobject object)
{
	jlong jlnRet = 0;
	long lnDist = 0;

	WG_JNI_GetRestDistance(&lnDist);

	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");
	if (clazz) {
		jfieldID jlnRestDistance = (*env)->GetFieldID(env, clazz, "lcount", "J");
		if (jlnRestDistance) {
			(*env)->SetLongField(env, object, jlnRestDistance, (jlong)lnDist);
		}
	}

	return jlnRet;
}

JNIEXPORT jlong JNICALL
JNI_WG_GetDestination(JNIEnv* env, jclass clz, jobject object)
{
	jlong jlnRet = 0;
	jmethodID ctorID;
	jobject jobjTime = 0;
	int nHour = 0;
	int nMin = 0;
	long lnTotalDist = 0;

	WG_JNI_GetDestination(&nHour, &nMin, &lnTotalDist);

	jclass jclDestinationData = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNUI_DESTINATION_DATA");
	jclass jclTime = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ZNUI_TIME");
	jfieldID jstTime = (*env)->GetFieldID(env, jclDestinationData, "stTime", "Lnet/zmap/android/pnd/v2/data/ZNUI_TIME;");

	if (jclTime) {
		ctorID = (*env)->GetMethodID(env, jclTime, "<init>", "()V");

		jobjTime = (jobject)(*env)->NewObject(env, jclTime, ctorID, NULL);

		jfieldID jHour = (*env)->GetFieldID(env, jclTime, "nHouer", "I");
		if (jHour) {
			(*env)->SetIntField(env, jobjTime, jHour, nHour);
		}

		jfieldID jMinute = (*env)->GetFieldID(env, jclTime, "nMinute", "I");
		if(jMinute) {
			(*env)->SetIntField(env, jobjTime, jMinute, nMin);
		}
	}

	if (jclDestinationData) {
		(*env)->SetObjectField(env, object, jstTime, jobjTime);

		jfieldID jlnDistance = (*env)->GetFieldID(env, jclDestinationData, "lnDistance", "J");
		if (jlnDistance) {
			(*env)->SetLongField(env, object, jlnDistance, lnTotalDist);
		}

		jfieldID jlnToll = (*env)->GetFieldID(env, jclDestinationData, "lnToll", "J");
		if (jlnToll) {
			(*env)->SetLongField(env, object, jlnToll, 0);
		}
	}

	return jlnRet;
}

JNIEXPORT jlong JNICALL
JNI_WG_RepeatVoice(JNIEnv* env, jclass clz)
{
	jlong jlnRet = 0;

	WG_JNI_RepeatVoice();

	return jlnRet;
}

JNIEXPORT void JNICALL
JNI_NE_Navi_EnableShowMagMap(JNIEnv* env, jclass clz, jint bIsShow){

	NE_Navi_EnableShowMagMap((BOOL)bIsShow);
	return;
}
// Add 2011/03/22 sawada Start -->
JNIEXPORT jlong JNICALL
JNI_WG_SetCompassInfo(JNIEnv *env, jclass clz, jlong jlnEnabled, jlong jlnDirection)
{
	WG_JNI_SetCompassInfo((long)jlnEnabled, (long)jlnDirection);

	return 0;
}
// Add 2011/03/22 sawada End   <--
JNIEXPORT jlong JNICALL
JNI_NE_Navi_SetMovingSpeed(JNIEnv* env, jclass clz, jint iMovingSpeed){

	if(NE_SUCCESS == NE_Navi_SetMovingSpeed((int)iMovingSpeed)){

	}
	return 0;
}

JNIEXPORT jlong JNICALL JNI_NE_VICS_GetRequestParam(JNIEnv* env, jclass clz, jlong ulnLongitude,  jlong ulnLatitude,  jlong  usPathBytesMax,  jobject joFilePath, jobject joFileSize){

	long lFileSize = 0;

	char* pcaFilePath = (char*)malloc(usPathBytesMax+ 1);

	if(pcaFilePath){
		memset(pcaFilePath, 0, usPathBytesMax+ 1);
		if(NE_SUCCESS == NE_VICS_GetRequestParam ( ulnLongitude,  ulnLatitude,   usPathBytesMax,   pcaFilePath, &lFileSize)){

			jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");
			if (clazz) {
				jfieldID jlongFiedID = (*env)->GetFieldID(env, clazz, "lcount", "J");
				if (jlongFiedID) {
					(*env)->SetLongField(env, joFileSize, jlongFiedID, (jlong)lFileSize);
				}
			}

			jclass jStrCls = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIString");

			if(jStrCls)	{
				jfieldID jstrStrID = (*env)->GetFieldID(env, jStrCls, "m_StrAddressString","Ljava/lang/String;");

				if(strlen(pcaFilePath) > 0)
				{
					jclass strClass = (*env)->FindClass(env,"java/lang/String");

					jmethodID ctorID = (*env)->GetMethodID(env,strClass, "<init>", "([BLjava/lang/String;)V");

					jbyteArray bytes = (*env)->NewByteArray(env,strlen(pcaFilePath));

					if(bytes != NULL)					{
						(*env)->SetByteArrayRegion(env,bytes, 0, strlen(pcaFilePath), (jbyte*)pcaFilePath);

					}

					jstring encoding = (*env)->NewStringUTF(env,"ShiftJIS");
					jstring  jsName = (jstring)(*env)->NewObject(env,strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						(*env)->SetObjectField(env,joFilePath,jstrStrID,jsName);
					}

					if(NULL != bytes)
					{
						(*env)->DeleteLocalRef(env, bytes);
						bytes = NULL;
					}
					if(NULL != encoding)
					{
						(*env)->DeleteLocalRef(env, encoding);
						encoding = NULL;
					}
					if(NULL != jsName)
					{
						(*env)->DeleteLocalRef(env, jsName);
						jsName = NULL;
					}
				}
			}

		}
		free(pcaFilePath);
	}

	return 0;
}

//Add 2011/10/01 Z01yamaguchi - VICS案内実装  Start -->
JNIEXPORT jlong JNICALL
JNI_NE_VICS_ReloadVicsFile(JNIEnv* env, jclass clz)
{
	return (jlong)NE_VICS_ReloadVicsFile();
}
//Add 2011/10/01 Z01yamaguchi - VICS案内実装  End <--

JNIEXPORT jlong JNICALL
JNI_NE_VICS_SetDisplayFlag(JNIEnv* env, jclass clz , jboolean bFlag)
{
	return (jlong)NE_VICS_SetDisplayFlag(bFlag);
}

//Add 2012/04/10 Z01hirama Start --> #4223
JNIEXPORT void JNICALL
JNI_ct_uic_DelBackupRoute(JNIEnv *env, jclass clz)
{
	CT_UIC_DelBackupRoute();
	return;
}
//Add 2012/04/10 Z01hirama End <-- #4223

// ADD 2013.08.08 M.Honma 走行規制 細街路表示制御 Start -->
JNIEXPORT void JNICALL
JNI_NE_SetDrivingRegulation(JNIEnv* env, jclass clz, jboolean bOn)
{
	NE_SetDrivingRegulation(bOn);
	return;
}
// ADD 2013.08.08 M.Honma 走行規制 細街路表示制御 End -->
// ADD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応�@ START
/*********************************************************************
* Function Name	: JNI_CT_BG_GetVehiclePosition   	     			 *
* Description	:                                            		 *
* Date			: 14/02/028                               			 *
* Parameter		:                                          		 	 *
* Return Code	: N.Sasao                                      		 *
* Author		: 	                                     			 *
* ------------------------------------------------------------------ *
* Revision History	                                             	 *
* No	Date		Revised by		Description          			 *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_BG_GetVehiclePosition
  (JNIEnv *env, jclass cls, jobject object, jobject speed, jobject direction)
{
	long lnLon, lnLat;
	ulong ulnSpeed;
	short nDirection;

	jlong lRet = (jlong)CT_BG_GetVehiclePosition(&lnLon, &lnLat, &ulnSpeed, &nDirection);

	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNITwoLong");
	if (clazz)
	{
		// 経度
		jfieldID jLongitude = (*env)->GetFieldID(env, clazz, "m_lLong", "J");
		if (jLongitude)
		{
			(*env)->SetLongField(env, object, jLongitude, lnLon);
		}
		else
		{
			LOGDp("[DEBUG] JNI_CT_BG_GetVehiclePosition GetFieldID jLongitude Failed");
		}
		// 緯度
		jfieldID jLatitude = (*env)->GetFieldID(env, clazz, "m_lLat", "J");
		if (jLatitude)
		{
			(*env)->SetLongField(env, object, jLatitude, lnLat);
		}
		else
		{
			LOGDp("[DEBUG] JNI_CT_BG_GetVehiclePosition GetFieldID jLatitude Failed");
		}
	}

	jclass jLongCls = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNILong");
	if (jLongCls)
	{
		// 速度
		jfieldID jSpeed = (*env)->GetFieldID(env, jLongCls, "lcount", "J");
		if (jSpeed)
		{
			(*env)->SetLongField(env, speed, jSpeed, ulnSpeed);
		}
		else
		{
			LOGDp("[DEBUG] JNI_CT_BG_GetVehiclePosition GetFieldID jSpeed Failed");
		}
	}

	jclass jShortCls = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIShort");
	if (jShortCls)
	{
		// 方向
		jfieldID jDirection = (*env)->GetFieldID(env, jShortCls, "m_sJumpRecCount", "S");
		if (jDirection)
		{
			(*env)->SetShortField(env, direction, jDirection, nDirection);
		}
		else
		{
			LOGDp("[DEBUG] JNI_CT_BG_GetVehiclePosition GetFieldID jDirection Failed");
		}
	}

	return lRet;
}
// ADD.2014.02.28 N.Sasao アローモード時の自車位置更新イベント対応�@  END