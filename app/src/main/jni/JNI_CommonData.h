/********************************************************************************
* File Name	:	JNI_CommonData.h
* Created	:	2011/10/18
* Author	:	ryousuke itoh itoh@cslab.co.jp
* Description
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include <jni.h>
#include "NETypes.h"

#ifndef _JUNI_COMMON_DATA_H_
#define _JUNI_COMMON_DATA_H_

#define SIGNATURE(_class)			"L" _class ";"

#ifdef __cplusplus
extern "C" {
#endif

unsigned long JNI_CMN_GetJStringCharString(JNIEnv *env, jstring jstrSrc, char** ppszString, int* piLength);

void JNI_CMN_FreeJStringCharString( char** ppszString );

unsigned long JNI_CMN_GetUserPolyDrawParam(JNIEnv *env, jobject joDrawParam, ZNE_UserPolyDrawParam* pstPolyDrawParam );
//unsigned long JNI_CMN_GetUserRouteDrawParam(JNIEnv *env, jobject joDrawParam, ZNE_UserRouteDrawParam* pstRouteDrawParam );

unsigned long JNI_CMN_GetGeoPointArray(JNIEnv *env, jarray jaJNITwoLongArray, ZNE_GeoPointArray* pstPointArray );

unsigned long JNI_CMN_GetGeoPoint(JNIEnv *env, jobject joJNITwoLong, ZNE_GeoPoint* pstPoint );

void JNI_CMN_FreeGeoPointArray( ZNE_GeoPointArray* pstPointArray );


#ifdef __cplusplus
}
#endif

#endif //_JUNI_COMMON_DATA_H_
