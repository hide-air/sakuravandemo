LOCAL_PATH:= $(call my-dir)/..
include $(CLEAR_VARS)

LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/DrawMap.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MapEngine.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPCommon.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MP_ReturnCode.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPThread.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPUserItem.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/ParcelCacheCreate.cpp
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/ParcelCacheTrafficCreate.cpp
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/OverlayMeshCreate.cpp
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPICON/MPICONCommon.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPICON/MPOutSidePOIIcon.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPICON/MPDriverContents.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPDG/MPDG_IF.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPDG/MPDG_RouteData.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPWG/MPWGDataLoad.c
#LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPUserDrawRoute.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MP_UserFigure/MP_UserFigure.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MP_UserFigure/MP_UserFigureDraw.c
#LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPVICS/MPVICS_cache.c
#LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPVICS/MPVICS_link.c
#LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPVICS/MPVICS_VLink.c
#LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/MP/MPVICS/MPVICS_if.c

LOCAL_CFLAGS += $(BUILD_CFLAGS)
LOCAL_CFLAGS += -I/.
LOCAL_CFLAGS += -I/..
LOCAL_CFLAGS += -D_EMONE_BUILD_=1
LOCAL_CFLAGS += -D_NESW_FOR_GDI_=1
LOCAL_CFLAGS += -DNDEBUG=1
#LOCAL_CFLAGS += -D_WIN32_WCE=$(CEVER)
LOCAL_CFLAGS += -DUNDER_CE=1
LOCAL_CFLAGS += -DWINCE=1
LOCAL_CFLAGS += -DPOCKETPC2003_UI_MODEL=1
LOCAL_CFLAGS += -D_USE_TEST_TIME=1

LOCAL_C_INCLUDES += $(BUILD_C_INCLUDES)
LOCAL_C_INCLUDES += jni

LOCAL_ARM_MODE := arm
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE := libMP
include $(BUILD_STATIC_LIBRARY)