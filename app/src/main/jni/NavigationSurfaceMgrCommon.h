// NavigationSurfaceMgrCommon.h

#ifndef _NAVIGATION_SURFACE_MGR_COMMON_H_
#define _NAVIGATION_SURFACE_MGR_COMMON_H_

////////////////////////////////////////////////////////////////////////////////
// Debug Config
////////////////////////////////////////////////////////////////////////////////
//#define NAVISURFACEMGR_DEBUG

////////////////////////////////////////////////////////////////////////////////
// Video Config
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Audio Config
////////////////////////////////////////////////////////////////////////////////

void navisurfacemgr_dmpmen(const void* dat, int len);

#ifdef NAVISURFACEMGR_DEBUG
#define NAVISURFACEMGR_ENTER(fmt, arg...)            LOGE("Enter> %s|%s[%d] "fmt, __FILE__, __FUNCTION__, __LINE__, ##arg)
#define NAVISURFACEMGR_DEBUG(fmt, arg...)            LOGE("Debug: %s|%s[%d] "fmt, __FILE__, __FUNCTION__, __LINE__, ##arg)
#define NAVISURFACEMGR_MDUMP(ptr, len, fmt, arg...)  LOGE("Mdump: %s|%s[%d] (address:%p, size:%d) "fmt, __FILE__, __FUNCTION__, __LINE__, ptr, len, ##arg); \
                                         navisurfacemgr_dmpmen(ptr, len);
#define NAVISURFACEMGR_ERROR(fmt, arg...)            LOGE("Error* %s|%s[%d] "fmt, __FILE__, __FUNCTION__, __LINE__, ##arg)
#define NAVISURFACEMGR_LEAVE(fmt, arg...)            LOGE("Leave< %s|%s[%d] "fmt, __FILE__, __FUNCTION__, __LINE__, ##arg)
#else
#define navisurfacemgr_ENTER(fmt, arg...)
#define navisurfacemgr_DEBUG(fmt, arg...)
#define navisurfacemgr_MDUMP(ptr, len, fmt, arg...)
#define navisurfacemgr_ERROR(fmt, arg...)
#define navisurfacemgr_LEAVE(fmt, arg...)
#endif

#endif /* _NAVIGATION_SURFACE_MGR_COMMON_H_ */
