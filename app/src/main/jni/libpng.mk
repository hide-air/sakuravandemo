LOCAL_PATH:= $(call my-dir)/..
include $(CLEAR_VARS)

LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/png.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngerror.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pnggccrd.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngget.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngmem.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngpread.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngread.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngrio.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngrtran.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngrutil.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngset.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngtrans.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngvcrd.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngwio.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngwrite.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngwtran.c
LOCAL_SRC_FILES += $(EXT_SOURCE_PATH)/libpng/pngwutil.c

LOCAL_CFLAGS += $(BUILD_CFLAGS)
LOCAL_CFLAGS += -I/.
LOCAL_CFLAGS += -I/..
LOCAL_CFLAGS += -fvisibility=hidden ## -fomit-frame-pointer

LOCAL_C_INCLUDES += $(BUILD_C_INCLUDES)

LOCAL_ARM_MODE := arm
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE := libpng
include $(BUILD_STATIC_LIBRARY)