LOCAL_PATH:= $(call my-dir)/..
include $(CLEAR_VARS)

LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/Emulator/Source/EmuLibrary.c
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/Emulator/Source/ConfigOP.cpp 
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/Emulator/Source/RegOperation.cpp 
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/Emulator/Source/datum.c 
LOCAL_SRC_FILES += $(NAVIENGINE_PATH)/Emulator/Source/IniFile.cpp

LOCAL_CFLAGS += $(BUILD_CFLAGS)
LOCAL_CFLAGS += -I/.
LOCAL_CFLAGS += -I/..

LOCAL_C_INCLUDES += $(BUILD_C_INCLUDES)

LOCAL_ARM_MODE := arm
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE := libEmulator
include $(BUILD_STATIC_LIBRARY)