// NavigationSurfaceMgr.cpp

//#define LOG_NDEBUG 0
#define LOG_TAG "NaviSurfaceMgr"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/prctl.h>
#include <sys/resource.h>


//#include "NavigationMouseEventCommon.h"
#include "NavigationSurfaceMgrCommon.h"
#include "NavigationSurfaceMgr.h"

#include "jni.h"
#include "NaviEngineCtrl.h"
#include "JNIMap_Communication.h"

#include "debugPrint.h"

static int          g_DisplayWidth = 0;
static int          g_DisplayHeight = 0;


#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

int getDisplayWidth();
int getDisplayHeight();
#if defined(__cplusplus)
}
#endif /* __cplusplus */



int getDisplayWidth()
{
	return g_DisplayWidth;
}

int getDisplayHeight()
{
	return g_DisplayHeight;
}

int
setDisplay(int width, int height)
{
	g_DisplayWidth  = width;
	g_DisplayHeight = height;

	NE_ExposeMap();
	
	return 0;
}







void 
initNaviSurfaceMgr()
{
}

void 
eixtNaviSurfaceMgr()
{

}
