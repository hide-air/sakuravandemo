/********************************************************************************
* File Name	:	 JNIPoiZip_Communication.cpp
* Created	:	2009/10/18
* Author	:	xiayx
* Model		:
* Description	:
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "JNIPoiZip_Communication.h"
#include "NaviEngineIF.h"
#include "NaviEngineCtrl.h"
#include "NaviEngine/CT/CTUICommand.h"
#include "NETypes.h"
#include "windows.h"
#include "NaviEngine/POI/POIStruct.h"
#include "jni/JNI_Common.h"
#include "CTICU.h"
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define LOG_TAG "NavigationJNI_NaviRun"
#define JNI_DEBUG

#include "debugPrint.h"

/*********************************************************************
*	Function Name	: JNI_NE_POIZip_Clear        	             *
*	Description	:                                            *
*	Date		: 09/10/18                                   *
*	Parameter	:                                            *
*	Return Code	: void                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_NE_POIZip_Clear
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POIZip_Clear();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIZip_SearchList        	     *
*	Description	:                                            *
*	Date		: 09/10/18                                   *
*	Parameter	:                                            *
*	Return Code	: void                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIZip_SearchList
  (JNIEnv *env, jclass cls,jstring jstr)
{
       char* rtn = NULL;

       jclass clsstring = env->FindClass("java/lang/String");
       jstring strencode = env->NewStringUTF("utf-8");
       jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
       jbyteArray barr= (jbyteArray)env->CallObjectMethod(jstr, mid, strencode);
       jsize alen = env->GetArrayLength(barr);
       jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);
       if (alen > 0)
       {
                 rtn = new char[alen+1];
                 memcpy(rtn, ba, alen);
                 rtn[alen] = 0;
       }

       jlong Ret = NE_POIZip_SearchList(rtn);

       env->ReleaseByteArrayElements(barr, ba, 0);
	env->DeleteLocalRef(strencode);
	 if(rtn)
	{
		delete[] rtn;
		rtn = NULL;
	 }

       return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIZip_GetRecCount        	     *
*	Description	:                                            *
*	Date		: 09/10/18                                   *
*	Parameter	:                                            *
*	Return Code	: void                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIZip_GetRecCount
  (JNIEnv *env, jclass cls,jobject object)
{

	unsigned long Listconut = 0;

	jlong lRet = (jlong)NE_POIZip_GetRecCount(&Listconut);

	//LOGDp("[DEBUG] JNI_NE_POIZip_GetRecCount Listconut:%d,lRet:%d!!!!!!!!!!!!",Listconut,lRet);

	jlong listconut = Listconut;
	//LOGE("[DEBUG]listconut is : listconut = %d", listconut);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POIZip_GetRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIZip_GetRecCount SetLongField Count Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIZip_GetRecCount FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIZip_GetRecList       	     *
*	Description	:                                            *
*	Date		: 09/10/18                                   *
*	Parameter	:                                            *
*	Return Code	: void                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIZip_GetRecList
  (JNIEnv *env, jclass a, jlong RecIndex, jlong RecCount, jobjectArray args, jobject object)
  {
	//LOGDp("[DEBUG] JNI_NE_POIZip_GetRecList begin");

	long Count;

	ZPOI_Zipcode_ListItem* pstRecs;

	jlong Ret = (jlong)NE_POIZip_GetRecList((long)RecIndex,(long)RecCount,&pstRecs, &Count);

	//LOGDp("[DEBUG] JNI_NE_POIZip_GetRecList Count:%d,Ret,%d!!!!!!!!!!!!",Count,Ret);

	jlong listconut = Count;
	//Fing Java's Class
	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");
	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}

	//LOGDp("[DEBUG] JNI_NE_POIZip_GetRecList Sucessful");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIZip_GetRecList SetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIZip_GetRecList FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_ZipCode_ListItem");


	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < Count; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIZip_GetRecList loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			jfieldID ZipCodeSize = env->GetFieldID(clazz,"m_lZipCodeSize","J");
			if(ZipCodeSize)
			{
				env->SetLongField(obj,ZipCodeSize,pstRecs[i].lnZipCodeSize);
				jfieldID ZipCode = env->GetFieldID(clazz,"m_ZipCode","Ljava/lang/String;");
				if(ZipCode)
				{
					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].pszZipCode));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].pszZipCode), (jbyte*)pstRecs[i].pszZipCode);
					}
					jstring encoding = env->NewStringUTF("ShiftJIS");

					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						env->SetObjectField(obj,ZipCode,jsName);
					}
					if(NULL != bytes)
					{
						env->DeleteLocalRef( bytes);
						bytes = NULL;
					}
					if(NULL != encoding)
					{
						env->DeleteLocalRef( encoding);
						encoding = NULL;
					}
					if(NULL != jsName)
					{
						env->DeleteLocalRef( jsName);
						jsName = NULL;
					}
					//env->SetObjectField(obj,ZipCode,env->NewStringUTF((char *)pstRecs[i].pszZipCode));
					jfieldID FullZipCodeSize = env->GetFieldID(clazz,"m_lFullZipCodeSize","J");
					if(FullZipCodeSize)
					{
						env->SetLongField(obj,FullZipCodeSize,pstRecs[i].lnFullZipCodeSize);
						jfieldID FullZipCode = env->GetFieldID(clazz,"m_FullZipCode","Ljava/lang/String;");
						if(FullZipCode)
						{
							jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].pszFullZipCode));
							if(bytes != NULL)
							{
								env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].pszFullZipCode), (jbyte*)pstRecs[i].pszFullZipCode);
							}
							jstring encoding = env->NewStringUTF("ShiftJIS");

							jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

							if(jsName)
							{
								env->SetObjectField(obj,FullZipCode,jsName);
							}

							if(NULL != bytes)
							{
								env->DeleteLocalRef( bytes);
								bytes = NULL;
							}
							if(NULL != encoding)
							{
								env->DeleteLocalRef( encoding);
								encoding = NULL;
							}
							if(NULL != jsName)
							{
								env->DeleteLocalRef( jsName);
								jsName = NULL;
							}
							jfieldID NameSize = env->GetFieldID(clazz,"m_lNameSize","J");
							if(NameSize)
							{
								env->SetLongField(obj,NameSize,pstRecs[i].lnNameSize);
								jfieldID Name = env->GetFieldID(clazz,"m_Name","Ljava/lang/String;");
								if(Name)
								{
									jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].pszName));
									if(bytes != NULL)
									{
										env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].pszName), (jbyte*)pstRecs[i].pszName);
									}
									jstring encoding = env->NewStringUTF("ShiftJIS");

									jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

									if(jsName)
									{
										env->SetObjectField(obj,Name,jsName);
									}

									if(NULL != bytes)
									{
										env->DeleteLocalRef( bytes);
										bytes = NULL;
									}
									if(NULL != encoding)
									{
										env->DeleteLocalRef( encoding);
										encoding = NULL;
									}
									if(NULL != jsName)
									{
										env->DeleteLocalRef( jsName);
										jsName = NULL;
									}
									jfieldID Lon = env->GetFieldID(clazz,"m_lLon","J");
									if (Lon)
									{
										env->SetLongField(obj,Lon,pstRecs[i].lnLon);
										jfieldID Lat = env->GetFieldID(clazz,"m_lLat","J");
										if (Lat)
										{
											env->SetLongField(obj,Lat,pstRecs[i].lnLat);
										}
										else
										{
											LOGDp("[DEBUG] JNICALL JNI_NE_POIZip_GetRecList GetFieldID Lat Failed");
										}
									}
									else
									{
										LOGDp("[DEBUG] JNICALL JNI_NE_POIZip_GetRecList GetFieldID Lon Failed");
									}
								}
								else
								{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIZip_GetRecList GetFieldID Name Failed");
								}
							}
							else
							{
								LOGDp("[DEBUG] JNICALL JNI_NE_POIZip_GetRecList GetFieldID NameSize Failed");
							}
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POIZip_GetRecList GetFieldID FullZipCode Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POIZip_GetRecList GetFieldID FullZipCodeSize Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIZip_GetRecList GetFieldID ZipCode Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIZip_GetRecList GetFieldID ZipCodeSize Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POIZip_GetRecList FindClass Failed!");
		}

		if(NULL != obj)
		{
			env->DeleteLocalRef( obj);
			obj = NULL;
		}
	}

	return Ret;
}

/*JNIEXPORT jlong JNICALL JNI_NE_POITel_GetProgressRate
  (JNIEnv *env, jclass cls,jobject object)
{
	long lRate = 0;
	jlong Ret = (jlong)NE_POITel_GetProgressRate(&lRate);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Rate = env->GetFieldID(clazz,"lcount","J");
		if(Rate)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Rate,lRate);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POITel_GetProgressRate GetLongField Rate Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POITel_GetProgressRate FindClass  Failed");
	}

	LOGDp("[DEBUG] JNI_NE_POITel_GetProgressRate Sucessful");

	return Ret;
}*/

/*********************************************************************
*	Function Name	: JNI_NE_POIZip_Cancel      	     	     *
*	Description	:                                            *
*	Date		: 09/10/18                                   *
*	Parameter	:                                            *
*	Return Code	: void                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIZip_Cancel
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POITel_Cancel();
}
