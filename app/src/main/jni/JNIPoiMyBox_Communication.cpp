/********************************************************************************
* File Name	:	 JNIPoiArnd_Communication.cpp
* Created	:	2009/11/15
* Author	:	xiayx
* Model		:
* Description	:
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "JNIPoiMyBox_Communication.h"
#include "NaviEngineIF.h"
#include "NaviEngineCtrl.h"
#include "NaviEngine/CT/CTUICommand.h"
#include "NETypes.h"
#include "windows.h"
#include "NaviEngine/POI/POIStruct.h"
#include "jni/JNI_Common.h"
#include "CTICU.h"
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define LOG_TAG "NavigationJNI_NaviRun"
#define JNI_DEBUG

#include "debugPrint.h"

typedef unsigned char byte;

/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_Reset         	     *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_Reset
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POIMybox_Reset();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_Clear          	     *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_Clear
  (JNIEnv *env, jclass cls)
{
	ZPOI_Mybox_FilePath Path;

	jlong Ret = (jlong)NE_POIMybox_Clear(&Path);

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_GetGenreCount              *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_GetGenreCount
  (JNIEnv *env, jclass cls,jobject object)
{
	long Listconut = 0;

	jlong lRet = (jlong)NE_POIMybox_GetGenreCount(&Listconut);

	//LOGDp("[DEBUG] JNI_NE_POIMybox_GetGenreCount Listconut:%d,lRet:%d!!!!!!!!!!!!",Listconut,lRet);

	jlong listconut = Listconut;
	//LOGE("[DEBUG]Listconut is : Listconut = %d", Listconut);
	//LOGE("[DEBUG]listconut is : listconut = %d", listconut);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POIMybox_GetGenreCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIMybox_GetGenreCount GetLongField Count Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIMybox_GetGenreCount FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_GetRecName              *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_GetRecName
  (JNIEnv *env, jclass cls,jlong GenreIndex,jobject object)
{
	ZPOI_Mybox_ListGenreName Genre_Name;

	jlong Ret = (jlong)NE_POIMybox_GetRecName((long)GenreIndex,&Genre_Name);
	//LOGDp("[DEBUG] NE_POIMybox_GetRecName GenreIndex:%d",(long)GenreIndex);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Mybox_ListGenreName");

	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JJNI_NE_POIMybox_GetRecName Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID GenreCode = env->GetFieldID(clazz,"m_lGenreCode","J");
		if(GenreCode)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,GenreCode,Genre_Name.dwGenreCode);
			jfieldID GenreName = env->GetFieldID(clazz,"m_GenreName","Ljava/lang/String;");
			if (GenreName)
			{
				jbyteArray bytes = env->NewByteArray(strlen(Genre_Name.szGenreName));
				if(bytes != NULL)
				{
					env->SetByteArrayRegion(bytes, 0, strlen(Genre_Name.szGenreName), (jbyte*)Genre_Name.szGenreName);

				}
				jstring encoding = env->NewStringUTF("ShiftJIS");


				jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

				if(jsName)
				{
					env->SetObjectField(object,GenreName,jsName);
				}

				if(NULL != bytes)
				{
					env->DeleteLocalRef( bytes);
					bytes = NULL;
				}
				if(NULL != encoding)
				{
					env->DeleteLocalRef( encoding);
					encoding = NULL;
				}
				if(NULL != jsName)
				{
					env->DeleteLocalRef( jsName);
					jsName = NULL;
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIMybox_GetGenreCount GetLongField GenreName Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIMybox_GetGenreCount GetLongField GenreCode Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIMybox_GetGenreCount FindClass  Failed");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_GetRecCount               *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_GetRecCount
  (JNIEnv *env, jclass cls,jlong GenreIndex,jobject object)
{
	ZPOI_Mybox_ListGenreCount RecCount;

	jlong lRet = (jlong)NE_POIMybox_GetRecCount((long)GenreIndex,&RecCount);

	//LOGDp("[DEBUG] JNI_NE_POIMybox_GetGenreCount Listconut:%d,lRet:%d!!!!!!!!!!!!",RecCount,lRet);


	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Mybox_ListGenreCount");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID GenreCode = env->GetFieldID(clazz,"m_lGenreCode","J");
		if(GenreCode)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,GenreCode,RecCount.dwGenreCode);
			jfieldID RecordCount = env->GetFieldID(clazz,"m_sRecordCount","S");
			if (RecordCount)
			{
				env->SetShortField(object,RecordCount,(jshort)RecCount.wRecordCount);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecCount GetLongField RecordCount Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecCount GetShortField GenreCode Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecCount FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_RemoveRec               *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_RemoveRec
  (JNIEnv *env, jclass cls,jlong GenreIndex,jlong RecordIndex)
{
	return (jlong)NE_POIMybox_RemoveRec((long)GenreIndex,(long)RecordIndex);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_RemoveGenreAllRec           *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_RemoveGenreAllRec
  (JNIEnv *env, jclass cls,jlong GenreIndex)
{
	return (jlong)NE_POIMybox_RemoveGenreAllRec((long)GenreIndex);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_GetRecList                *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_GetRecList
  (JNIEnv *env, jclass clz,jlong GenreIndex,jlong RecIndex,jlong RecCount,jobjectArray args,jobject object)
{
	ZPOI_UserFile_RecordIF *pstRecs;
	long Rec_Count;
	jmethodID ctorID = 0;

	jlong Ret = (jlong)NE_POIMybox_GetRecList((long)GenreIndex,(long)RecIndex,(long)RecCount,&pstRecs,&Rec_Count);

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)Rec_Count);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecList GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecList FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_UserFile_RecordIF");
#if 0
	if(clazz)
	{
		LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecList FindClass Sucessful!!!!!!!!!!!!!!");
	}
#endif
	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < Rec_Count; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecList loop begin####RecCount:%d",RecCount);
		//LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecList loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			jfieldID IconCode = env->GetFieldID(clazz,"m_lIconCode","J");
			if(IconCode)
			{
				env->SetLongField(obj,IconCode,pstRecs[i].dwIconCode);

				jclass strClass = env->FindClass("java/lang/String");
				if(strClass != NULL)
				{
					ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
				}
				jfieldID DispName = env->GetFieldID(clazz,"m_DispName","Ljava/lang/String;");
				if(DispName)
				{

					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].szDispName));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].szDispName), (jbyte*)pstRecs[i].szDispName);

					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						env->SetObjectField(obj,DispName,jsName);
					}

					if(NULL != bytes)
					{
						env->DeleteLocalRef( bytes);
						bytes = NULL;
					}
					if(NULL != encoding)
					{
						env->DeleteLocalRef( encoding);
						encoding = NULL;
					}
					if(NULL != jsName)
					{
						env->DeleteLocalRef( jsName);
						jsName = NULL;
					}
					jfieldID Date = env->GetFieldID(clazz,"m_lDate","J");
					if(Date)
					{
						env->SetLongField(obj,Date,pstRecs[i].dwDate);
						jfieldID Lon = env->GetFieldID(clazz,"m_lLon","J");
						if(Lon)
						{
							env->SetLongField(obj,Lon,pstRecs[i].dwLon);
							jfieldID Lat = env->GetFieldID(clazz,"m_lLat","J");
							if(Lat)
							{
								env->SetLongField(obj,Lat,pstRecs[i].dwLat);
								jfieldID UserFlag = env->GetFieldID(clazz,"m_lUserFlag","J");
								if(UserFlag)
								{
									env->SetLongField(obj,UserFlag,pstRecs[i].wUserFlag);
									jfieldID Import = env->GetFieldID(clazz,"m_lImport","J");
									if (Import)
									{
										env->SetLongField(obj,Import,pstRecs[i].bImport);
									}
									else
									{
										LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetRecList GetFieldID Import Failed");
									}
								}
								else
								{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetRecList GetFieldID UserFlag Failed");
								}
							}
							else
							{
								LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetRecList GetFieldID Lat Failed");
							}
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetRecList GetFieldID Lon Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecList GetFieldID Date Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecList GetFieldID DispName Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecList GetFieldID IconCode Failed");
			}

			env->SetObjectArrayElement( args, i, obj);
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POIMybox_GetRecList FindClass Failed!");
		}

		if(NULL != obj)
		{
			env->DeleteLocalRef( obj);
			obj = NULL;
		}
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_GetAroundList               *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_GetAroundList
  (JNIEnv *env, jclass cls,jlong Longitude,jlong Latitude)
{
	return (jlong)NE_POIMybox_GetAroundList((long)Longitude,(long)Latitude);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_GetAroundRecCount           *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_GetAroundRecCount
  (JNIEnv *env, jclass cls,jint GenreIndex,jobject object)
{
	ZPOI_Mybox_ListGenreCount RecCount;

	jlong Ret = (jlong)NE_POIMybox_GetAroundRecCount((long)GenreIndex,&RecCount);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Mybox_ListGenreCount");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID GenreCode = env->GetFieldID(clazz,"m_lGenreCode","J");
		if(GenreCode)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,GenreCode,(jlong)RecCount.dwGenreCode);
			jfieldID RecordCount = env->GetFieldID(clazz,"m_sRecordCount","S");
			if (RecordCount)
			{
				env->SetShortField(object,RecordCount,(jshort)RecCount.wRecordCount);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundRecCount GetLongField GenreName Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundRecCount GetLongField GenreCode Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundRecCount FindClass  Failed");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_GetAroundListItem           *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_GetAroundListItem
  (JNIEnv *env, jclass clz,jlong GenreIndex,jlong RecIndex,jlong RecCount,jobjectArray args,jobject object)
{
	ZPOI_UserFile_AroundRec *pstRecs;
	long GetRecCount = 0;

	jlong Ret = (jlong)NE_POIMybox_GetAroundListItem((long)GenreIndex,(long)RecIndex,(long)RecCount,&pstRecs,&GetRecCount);

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}

	//LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundListItem Sucessful");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)GetRecCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundListItem GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundListItem FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_UserFile_AroundRec");



	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < GetRecCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundListItem loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			jfieldID IconCode = env->GetFieldID(clazz,"m_lIconCode","J");
			if(IconCode)
			{
				env->SetLongField(obj,IconCode,pstRecs[i].dwIconCode);
				jfieldID DispName = env->GetFieldID(clazz,"m_DispName","Ljava/lang/String;");
				if(DispName)
				{

					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].szDispName));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].szDispName), (jbyte*)pstRecs[i].szDispName);

					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						env->SetObjectField(obj,DispName,jsName);
					}

					if(NULL != bytes)
					{
						env->DeleteLocalRef( bytes);
						bytes = NULL;
					}
					if(NULL != encoding)
					{
						env->DeleteLocalRef( encoding);
						encoding = NULL;
					}
					if(NULL != jsName)
					{
						env->DeleteLocalRef( jsName);
						jsName = NULL;
					}

					jfieldID Date = env->GetFieldID(clazz,"m_lDate","J");
					if(Date)
					{
						env->SetLongField(obj,Date,pstRecs[i].dwDate);
						jfieldID Lon = env->GetFieldID(clazz,"m_lLon","J");
						if(Lon)
						{
							env->SetLongField(obj,Lon,pstRecs[i].dwLon);
							jfieldID Lat = env->GetFieldID(clazz,"m_lLat","J");
							if(Lat)
							{
								env->SetLongField(obj,Lat,pstRecs[i].dwLat);
								jfieldID UserFlag = env->GetFieldID(clazz,"m_lUserFlag","J");
								if(UserFlag)
								{
									env->SetLongField(obj,UserFlag,pstRecs[i].wUserFlag);
									jfieldID Import = env->GetFieldID(clazz,"m_lImport","J");
									if (Import)
									{
										env->SetLongField(obj,Import,pstRecs[i].bImport);
										jfieldID Time = env->GetFieldID(clazz,"m_sTime","S");
										if (Time)
										{
											env->SetShortField(obj,Time,pstRecs[i].wTime);
											jfieldID RecIndex = env->GetFieldID(clazz,"m_lRecIndex","J");
											if (RecIndex)
											{
												env->SetLongField(obj,RecIndex,pstRecs[i].dwRecIndex);
												jfieldID Distance = env->GetFieldID(clazz,"m_lDistance","J");
												if (Distance)
												{
													env->SetLongField(obj,Distance,pstRecs[i].lnDistance);
													jfieldID GenreIndex = env->GetFieldID(clazz,"m_lGenreIndex","J");
													if (GenreIndex)
													{
														env->SetLongField(obj,GenreIndex,pstRecs[i].lnGenreIndex);
													}
													else
													{
														LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetAroundListItem GetFieldID GenreIndex Failed");
													}
												}
												else
												{
													LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetAroundListItem GetFieldID Distance Failed");
												}
											}
											else
											{
												LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetAroundListItem GetFieldID RecIndex Failed");
											}
										}
										else
										{
											LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetAroundListItem GetFieldID Time Failed");
										}
									}
									else
									{
										LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetAroundListItem GetFieldID Import Failed");
									}
								}
								else
								{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetAroundListItem GetFieldID UserFlag Failed");
								}
							}
							else
							{
								LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetAroundListItem GetFieldID Lat Failed");
							}
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetAroundListItem GetFieldID Lon Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundListItem GetFieldID Date Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundListItem GetFieldID DispName Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundListItem GetFieldID IconCode Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POIMybox_GetAroundListItem FindClass Failed!");
		}

		if(NULL != obj)
		{
			env->DeleteLocalRef( obj);
			obj = NULL;
		}
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_SetRec                         *
*	Description	:                                                    *
*	Date		: 09/11/15                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_SetRec
  (JNIEnv *env, jclass cls,jlong GenreIndex,jobject object,jlong DataCount,jobject obj)
{
	char* rtn = NULL;
	ZPOI_UserFile_RecordIF Record;
	ZPOI_UserFile_DataIF Data;
	jclass jcls = env->GetObjectClass(object);
	jclass jclz = env->GetObjectClass(obj);

	if (jcls)
	{
		jfieldID jfid1 = env->GetFieldID( jcls, "m_lIconCode", "J" );
		if (jfid1)
		{
			jlong value1 = env->GetLongField(object,jfid1);
			Record.dwIconCode = (long)value1;
		}

		jfieldID jfid2 = env->GetFieldID( jcls, "m_DispName", "Ljava/lang/String;");
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応 START
		if (jfid2)
		{
		   	jstring jstr = (jstring)env->GetObjectField(object,jfid2);
			jsize alen = 0;
			jbyte* ba = NULL;

		    jclass clsstring = env->FindClass(CLASS_CommonLib);
			jmethodID mid = env->GetStaticMethodID(clsstring, "getToSJIS", "(Ljava/lang/String;)[B");
			jbyteArray barr= (jbyteArray)env->CallStaticObjectMethod(clsstring, mid, jstr);
			if( barr != NULL ){
				alen = env->GetArrayLength(barr);
				ba = env->GetByteArrayElements(barr, JNI_FALSE);

				if( alen > 0 ){
		    		// CID#10261  無意味な一時領域の確保を取りやめ直接目的領域に文字列を転写する
		    		memcpy(Record.szDispName,ba, sizeof(char)*(alen+1));
		    		Record.szDispName[alen] = '\0';	// jniの仕様がわからないので念のため
				}
	        	env->ReleaseByteArrayElements(barr, ba, 0);
	 		}
		}
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応  END

		jfieldID jfid3 = env->GetFieldID( jcls, "m_lDate", "J" );
		if (jfid3)
		{
			jlong value3 = env->GetLongField(object,jfid3);
			Record.dwDate = (long)value3;
		}

		jfieldID jfid4 = env->GetFieldID( jcls, "m_lLon", "J" );
		if (jfid4)
		{
			jlong value4 = env->GetLongField(object,jfid4);
			Record.dwLon = (long)value4;
		}

		jfieldID jfid5 = env->GetFieldID( jcls, "m_lLat", "J" );
		if (jfid5)
		{
			jlong value5 = env->GetLongField(object,jfid5);
			Record.dwLat = (long)value5;
		}

		jfieldID jfid6 = env->GetFieldID( jcls, "m_lUserFlag", "J" );
		if (jfid6)
		{
			jlong value6 = env->GetLongField(object,jfid6);
			Record.wUserFlag = (long)value6;
		}

		jfieldID jfid7 = env->GetFieldID( jcls, "m_lUserFlag", "J" );
		if (jfid7)
		{
			jlong value7 = env->GetLongField(object,jfid7);
			Record.bImport = (long)value7;
		}
	}

	else
	{

		LOGDp("[DEBUG] JNI_NE_POIMybox_SetRec FindClass  Failed");
	}

	if (jclz)
	{
		jfieldID jfid1 = env->GetFieldID( jclz, "m_sDataIndex", "S" );
		if (jfid1)
		{
			jlong value1 = env->GetShortField(obj,jfid1);
			Data.wDataIndex = (long)value1;
		}

		jfieldID jfid2 = env->GetFieldID( jclz, "m_sDataSize", "S" );
		if (jfid2)
		{
			jlong value2 = env->GetShortField(obj,jfid2);
			Data.wDataSize = (long)value2;
		}

		jfieldID jfid3 = env->GetFieldID( jclz, "m_Data", "Ljava/lang/String;");
		if (jfid3)
		{
			jstring jstr = (jstring)env->GetObjectField(obj,jfid3);

		    jclass clsstring = env->FindClass("java/lang/String");
		    jstring strencode = env->NewStringUTF("ShiftJIS");
		    jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
		    jbyteArray barr= (jbyteArray)env->CallObjectMethod(jstr, mid, strencode);
		    jsize alen = env->GetArrayLength(barr);
		    jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);
		    if (alen > 0)
			{
				 rtn = new char[alen+1];
				 memset(rtn, 0, sizeof(char)*(alen+1));
				 memcpy(rtn, ba, sizeof(char)*(alen+1));
				 rtn[alen] = '\0';
			}
			Data.pcData = rtn;
			Data.wDataSize = alen;
			env->ReleaseByteArrayElements(barr, ba, 0);
			env->DeleteLocalRef(strencode);
			 if(rtn)
			{
				delete[] rtn;
				rtn = NULL;
			 }
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIMybox_SetRec FindClass  Failed");
	}

	jlong Ret = (jlong)NE_POIMybox_SetRec((long)GenreIndex,&Record,(int)DataCount,&Data);

	return Ret;
}
/*********************************************************************
*	Function Name	: JNI_NE_POI_Mybox_ChangeData                    *
*	Description	:                                                    *
*	Date		: 09/11/15                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Mybox_ChangeData
  (JNIEnv *env, jclass clz,jlong lnGenreIndex,jlong lnRecordIndex,jstring jstr, jint i)
{
       char* rtn = NULL;
//Add 2011/12/15 Z01_h_yamada Start -->
       jlong Ret = 0;
//Add 2011/12/15 Z01_h_yamada End <--
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応 START
	jsize alen = 0;
	jbyte* ba = NULL;

	jclass clsstring = env->FindClass(CLASS_CommonLib);
	jmethodID mid = env->GetStaticMethodID(clsstring, "getToSJIS", "(Ljava/lang/String;)[B");
	jbyteArray barr= (jbyteArray)env->CallStaticObjectMethod(clsstring, mid, jstr);
	if( barr != NULL ){
		alen = env->GetArrayLength(barr);
		ba = env->GetByteArrayElements(barr, JNI_FALSE);
       	if (alen > 0)
       	{
        	rtn = new char[alen+1];
           	memcpy(rtn, ba, alen);
           	rtn[alen] = 0;

			//Chg 2011/12/15 Z01_h_yamada Start -->
			//           NE_POI_Mybox_ChangeData(lnGenreIndex, lnRecordIndex, rtn, i);
			//--------------------------------------------
           	Ret = NE_POI_Mybox_ChangeData(lnGenreIndex, lnRecordIndex, rtn, i);
			//Chg 2011/12/15 Z01_h_yamada End <--
	   		delete [] rtn;
	    	rtn = NULL;
       	}

       	env->ReleaseByteArrayElements(barr, ba, 0);
	}
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応  END

       //LOGDp("[DEBUG]JNI_NE_UpdateGPSPortNo End!!!!!!!!!!!");
//Chg 2011/12/15 Z01_h_yamada Start -->
//       return 0;
//--------------------------------------------
       return Ret;
//Chg 2011/12/15 Z01_h_yamada End <--
}
/*********************************************************************
*	Function Name	: JNI_NE_POI_Mybox_ChangeData                    *
*	Description	:                                                    *
*	Date		: 09/11/15                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_GetRouteList
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POIMybox_GetRouteList();
}
/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_GetRecCount               *
*	Description	:                                            *
*	Date		: 09/11/15                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_GetRouteRecCount
  (JNIEnv *env, jclass cls,jlong GenreIndex,jobject object)
{
	ZPOI_Mybox_ListGenreCount RecCount;

	jlong lRet = (jlong)NE_POIMybox_GetRouteRecCount((long)GenreIndex,&RecCount);

	//LOGDp("[DEBUG] JNI_NE_POIMybox_GetGenreCount Listconut:%d,lRet:%d!!!!!!!!!!!!",RecCount,lRet);


	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Mybox_ListGenreCount");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID GenreCode = env->GetFieldID(clazz,"m_lGenreCode","J");
		if(GenreCode)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,GenreCode,RecCount.dwGenreCode);
			jfieldID RecordCount = env->GetFieldID(clazz,"m_sRecordCount","S");
			if (RecordCount)
			{
				env->SetShortField(object,RecordCount,(jshort)RecCount.wRecordCount);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecCount GetLongField RecordCount Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecCount GetShortField GenreCode Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIMybox_GetRecCount FindClass  Failed");
	}

	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_NE_POIMybox_GetRouteListItem               *
*	Description	:                                                    *
*	Date		: 09/11/15                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIMybox_GetRouteListItem
  (JNIEnv *env, jclass clz,jlong GenreIndex,jlong RecIndex,jlong RecCount,jobjectArray args,jobject object)
{
	ZPOI_UserFile_RouteRec *pstRecs;
	long GetRecCount = 0;

	jlong Ret = (jlong)NE_POIMybox_GetRouteListItem((long)GenreIndex,(long)RecIndex,(long)RecCount,&pstRecs,&GetRecCount);

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}

	//LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundListItem Sucessful");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)GetRecCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIMybox_GetRouteListItem GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIMybox_GetRouteListItem FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_UserFile_RouteRec");



	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < GetRecCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundListItem loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			jfieldID IconCode = env->GetFieldID(clazz,"m_lIconCode","J");
			if(IconCode)
			{
				env->SetLongField(obj,IconCode,pstRecs[i].dwIconCode);
				jfieldID DispName = env->GetFieldID(clazz,"m_DispName","Ljava/lang/String;");
				if(DispName)
				{

					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].szDispName));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].szDispName), (jbyte*)pstRecs[i].szDispName);

					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						env->SetObjectField(obj,DispName,jsName);
					}

					jfieldID Date = env->GetFieldID(clazz,"m_lDate","J");
					if(Date)
					{
						env->SetLongField(obj,Date,pstRecs[i].dwDate);
						jfieldID Lon = env->GetFieldID(clazz,"m_lLon","J");
						if(Lon)
						{
							env->SetLongField(obj,Lon,pstRecs[i].dwLon);
							jfieldID Lat = env->GetFieldID(clazz,"m_lLat","J");
							if(Lat)
							{
								env->SetLongField(obj,Lat,pstRecs[i].dwLat);
								jfieldID UserFlag = env->GetFieldID(clazz,"m_lUserFlag","J");
								if(UserFlag)
								{
									env->SetLongField(obj,UserFlag,pstRecs[i].wUserFlag);
									jfieldID Import = env->GetFieldID(clazz,"m_lImport","J");
									if (Import)
									{
										env->SetLongField(obj,Import,pstRecs[i].bImport);
										jfieldID Time = env->GetFieldID(clazz,"m_sTime","S");
										if (Time)
										{
											env->SetShortField(obj,Time,pstRecs[i].wTime);
											jfieldID RecIndex = env->GetFieldID(clazz,"m_lRecIndex","J");
											if (RecIndex)
											{
												env->SetLongField(obj,RecIndex,pstRecs[i].dwRecIndex);
												jfieldID Distance = env->GetFieldID(clazz,"m_lDistance","J");
												if (Distance)
												{
													env->SetLongField(obj,Distance,pstRecs[i].lnDistance);
													jfieldID GenreIndex = env->GetFieldID(clazz,"m_lGenreIndex","J");
													if (GenreIndex)
													{
														env->SetLongField(obj,GenreIndex,pstRecs[i].lnGenreIndex);
													}
													else
													{
														LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetRouteListItem GetFieldID GenreIndex Failed");
													}
												}
												else
												{
													LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetRouteListItem GetFieldID Distance Failed");
												}
											}
											else
											{
												LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetRouteListItem GetFieldID RecIndex Failed");
											}
										}
										else
										{
											LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetRouteListItem GetFieldID Time Failed");
										}
									}
									else
									{
										LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetRouteListItem GetFieldID Import Failed");
									}
								}
								else
								{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetRouteListItem GetFieldID UserFlag Failed");
								}
							}
							else
							{
								LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetRouteListItem GetFieldID Lat Failed");
							}
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POIMybox_GetRouteListItem GetFieldID Lon Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POIMybox_GetRouteListItem GetFieldID Date Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIMybox_GetRouteListItem GetFieldID DispName Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIMybox_GetAroundListItem GetFieldID IconCode Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POIMybox_GetRouteListItem FindClass Failed!");
		}
	}

	return Ret;
}

