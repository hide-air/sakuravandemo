/********************************************************************************
* File Name	:	JNI_CommonData.c
* Created	:	2011/10/18
* Author	:	ryousuke itoh itoh@cslab.co.jp
* Description
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "jni/JNI_Common.h"
#include "debugPrint.h"
#include "windowsTypeDef.h"
#include "JNI_CommonData.h"
#include "windows.h"
#include "JNI_Error.h"


#define CLASS_DrawParam				"net/zmap/android/pnd/v2/api/overlay/LineDrawParam"
#define SIGNATURE_DrawParam			SIGNATURE(CLASS_DrawParam)
//#define CLASS_DrawRouteParam		"net/zmap/android/pnd/v2/api/common/data/route/DrawRouteParam"
//#define SIGNATURE_DrawRouteParam	SIGNATURE(CLASS_DrawRouteParam)
#define CLASS_JNITwoLong			"net/zmap/android/pnd/v2/data/JNITwoLong"
#define SIGNATURE_JNITwoLong		SIGNATURE(CLASS_JNITwoLong)

/**
 *  JStringより文字列取得
 *
 *  @param	jstrSrc : [jstring] : (in) : JString文字列
 *  @param	ppszString : [char**] : (out) : 取得文字列
 *  @param	piLength : [int*] : (out) : 取得文字列数
 *
 *  @retval	 リターンコード
 */
unsigned long JNI_CMN_GetJStringCharString(JNIEnv *env, jstring jstrSrc, char** ppszString, int* piLength)
{
	unsigned long ulRet = JNI_ERR_OK;

	(*ppszString) = NULL;
	(*piLength) = 0;

	if (jstrSrc == NULL) {
		return JNI_ERR_ILLEGAL_STATE;
	}
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応 START
	jclass jclsString = NULL;
	jmethodID midGetBytes = NULL;
	jbyteArray jarStrData = NULL;
	jbyte* jbyteDatas = NULL;
	jsize nStrDataLen = 0;

	jclsString = (*env)->FindClass(env, CLASS_CommonLib);
	if (jclsString == NULL) {
		ulRet = JNI_ERR_ILLEGAL_STATE;
		goto _FUNC_END;
	}

	midGetBytes = (*env)->GetStaticMethodID(env, jclsString, "getToSJIS", "(Ljava/lang/String;)[B");
	if (midGetBytes == NULL) {
		ulRet = JNI_ERR_ILLEGAL_STATE;
		goto _FUNC_END;
	}

	jarStrData = (jbyteArray) (*env)->CallStaticObjectMethod(env, jclsString, midGetBytes, jstrSrc);
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応  END
	if (jarStrData == NULL) {
		ulRet = JNI_ERR_ILLEGAL_STATE;
		goto _FUNC_END;
	}
	nStrDataLen = (*env)->GetArrayLength(env, jarStrData);

	jbyteDatas = (*env)->GetByteArrayElements(env, jarStrData, JNI_FALSE);
	if (nStrDataLen > 0) {
		(*ppszString) = (char*) malloc(sizeof(char) * (nStrDataLen + 1));
		memcpy((*ppszString), jbyteDatas, nStrDataLen);
		(*ppszString)[nStrDataLen] = 0;
		(*piLength) = nStrDataLen;
	}

_FUNC_END: ;
// Mod by CPJsunagawa '2015-07-22 Start
//	(*env)->ReleaseByteArrayElements(env, jarStrData, jbyteDatas, 0);
 	if (jbyteDatas != NULL) {
 		(*env)->ReleaseByteArrayElements(env, jarStrData, jbyteDatas, 0);
 	}
// Mod by CPJsunagawa '2015-07-22 End
	(*env)->DeleteLocalRef(env, jclsString);
	(*env)->DeleteLocalRef(env, jarStrData);

	return JNI_ERR_OK;
}

/**
 *  JString文字列解放処理
 *
 *  @param	ppszString : [xxxxx] : () : JString文字列
 *
 *  @retval	 リターンコード
 */
void JNI_CMN_FreeJStringCharString( char** ppszString )
{
	if( (*ppszString) ){
		free( (*ppszString) );
		(*ppszString) = NULL;
	}
}

unsigned long
JNI_CMN_GetUserPolyDrawParam(JNIEnv *env, jobject joDrawParam, ZNE_UserPolyDrawParam* pstPolyDrawParam )
{
	jclass jcDrawParam = (*env)->FindClass(env, CLASS_DrawParam );
	if ( !jcDrawParam ) {
		return JNI_ERR_ILLEGAL_STATE;
	}

	jmethodID jmGetLineWidh = (*env)->GetMethodID( env, jcDrawParam, "getLineWidth", "()I");
	if( !jmGetLineWidh ){
		return JNI_ERR_ILLEGAL_STATE;
	}
	jmethodID jmGetLineColor = (*env)->GetMethodID( env, jcDrawParam, "getLineColor", "()I");
	if( !jmGetLineColor ){
		return JNI_ERR_ILLEGAL_STATE;
	}
	jmethodID jmGetLineFrameWidh = (*env)->GetMethodID( env, jcDrawParam, "getLineFrameWidth", "()I");
	if( !jmGetLineFrameWidh ){
		return JNI_ERR_ILLEGAL_STATE;
	}
	jmethodID jmGetLineFrameColor = (*env)->GetMethodID( env, jcDrawParam, "getLineFrameColor", "()I");
	if( !jmGetLineFrameColor ){
		return JNI_ERR_ILLEGAL_STATE;
	}
	jmethodID jmGetDotSize = (*env)->GetMethodID( env, jcDrawParam, "getDotSize", "()I");
	if( !jmGetDotSize ){
		return JNI_ERR_ILLEGAL_STATE;
	}
	jmethodID jmGetDotColor = (*env)->GetMethodID( env, jcDrawParam, "getDotColor", "()I");
	if( !jmGetDotColor ){
		return JNI_ERR_ILLEGAL_STATE;
	}

	pstPolyDrawParam->iLineWidth = (*env)->CallIntMethod(env, joDrawParam, jmGetLineWidh );
	pstPolyDrawParam->ulLineColor = (unsigned long)(*env)->CallIntMethod(env, joDrawParam, jmGetLineColor );
	pstPolyDrawParam->iLineFrameWidth = (*env)->CallIntMethod(env, joDrawParam, jmGetLineFrameWidh );
	pstPolyDrawParam->ulLineFrameColor = (unsigned long)(*env)->CallIntMethod(env, joDrawParam, jmGetLineFrameColor );
	pstPolyDrawParam->iDotSize = (*env)->CallIntMethod(env, joDrawParam, jmGetDotSize );
	pstPolyDrawParam->ulDotColor = (unsigned long)(*env)->CallIntMethod(env, joDrawParam, jmGetDotColor );

	return JNI_ERR_OK;
}


//unsigned long
//JNI_CMN_GetUserRouteDrawParam(JNIEnv *env, jobject joDrawRouteParam, ZNE_UserRouteDrawParam* pstRouteDrawParam )
//{
//	unsigned long ulRet = JNI_ERR_OK;
//	jclass jcDrawRouteParam = (*env)->FindClass(env, CLASS_DrawRouteParam );
//	if (!jcDrawRouteParam ) {
//		return JNI_ERR_ILLEGAL_STATE;
//	}
//
//	jmethodID midGetGeneralLine = (*env)->GetMethodID( env, jcDrawRouteParam, "getGeneralLine", "()" SIGNATURE_DrawParam);
//	if( !midGetGeneralLine ){
//		return JNI_ERR_ILLEGAL_STATE;
//	}
//	jmethodID midGetHighwayLine = (*env)->GetMethodID( env, jcDrawRouteParam, "getHighwayLine", "()" SIGNATURE_DrawParam);
//	if( !midGetHighwayLine ){
//		return JNI_ERR_ILLEGAL_STATE;
//	}
//
//
//	{
//		ZNE_UserPolyDrawParam* pstLineParam = &pstRouteDrawParam->stGeneral;
//		jobject joDrawParam = (*env)->CallObjectMethod(env, joDrawRouteParam, midGetGeneralLine  );
//		if( !joDrawParam ){
//			return JNI_ERR_ILLEGAL_ARGUMENT;
//		}
//
//		ulRet = JNI_CMN_GetUserPolyDrawParam(env, joDrawParam, pstLineParam );
//		if( ulRet != JNI_ERR_OK ){
//			return ulRet;
//		}
//	}
//	{
//		ZNE_UserPolyDrawParam* pstLineParam = &pstRouteDrawParam->stHighway;
//		jobject joDrawParam = (*env)->CallObjectMethod(env, joDrawRouteParam, midGetHighwayLine  );
//		if( !joDrawParam ){
//			return JNI_ERR_ILLEGAL_ARGUMENT;
//		}
//		ulRet = JNI_CMN_GetUserPolyDrawParam(env, joDrawParam, pstLineParam );
//		if( ulRet != JNI_ERR_OK ){
//			return ulRet;
//		}
//	}
//
//	return ulRet;
//}


unsigned long
JNI_CMN_GetGeoPoint(JNIEnv *env, jobject joJNITwoLong, ZNE_GeoPoint* pstPoint )
{
	jclass jcJNITwoLong = (*env)->FindClass(env, CLASS_JNITwoLong );
	if (!jcJNITwoLong) {
		return JNI_ERR_ILLEGAL_STATE;
	}

	jfieldID jfLongitude = (*env)->GetFieldID(env, jcJNITwoLong, "m_lLong", "J");
	jfieldID jfLatitude = (*env)->GetFieldID(env, jcJNITwoLong, "m_lLat", "J");
	if (!jfLongitude || !jfLatitude) {
		return JNI_ERR_ILLEGAL_STATE;
	}

	pstPoint->lnLongitude = (*env)->GetLongField(env,joJNITwoLong,jfLongitude);
	pstPoint->lnLatitude = (*env)->GetLongField(env,joJNITwoLong,jfLatitude);

	return JNI_ERR_OK;
}

unsigned long
JNI_CMN_GetGeoPointArray(JNIEnv *env, jobjectArray jaJNITwoLongArray, ZNE_GeoPointArray* pstPointArray )
{
	unsigned long ulRet = JNI_ERR_OK;

	jint jiLocationNum = (*env)->GetArrayLength(env, jaJNITwoLongArray);

	pstPointArray->nPointNum = jiLocationNum;

	int nAllocSize = pstPointArray->nPointNum * sizeof(ZNE_GeoPoint);
	pstPointArray->pstPoint = (ZNE_GeoPoint*)malloc(nAllocSize);
	memset(pstPointArray->pstPoint, 0x00, nAllocSize);

	int i = 0;
	for (i = 0; i < pstPointArray->nPointNum; i++) {

		jobject location = (*env)->GetObjectArrayElement(env, jaJNITwoLongArray, i);
		if ((*env)->ExceptionOccurred(env)) {
			return JNI_ERR_ILLEGAL_ARGUMENT;
		}
		ulRet = JNI_CMN_GetGeoPoint(env, location, &pstPointArray->pstPoint[i] );
		if( ulRet != JNI_ERR_OK ) {
			goto _FUNC_END;
		}
	}

_FUNC_END:;
	if( ulRet != JNI_ERR_OK ){
		JNI_CMN_FreeGeoPointArray(pstPointArray);
	}

	return ulRet;
}

void
JNI_CMN_FreeGeoPointArray( ZNE_GeoPointArray* pstPointArray )
{
	if(pstPointArray->pstPoint){
		free(pstPointArray->pstPoint);
		pstPointArray->pstPoint = NULL;
	}
	pstPointArray->nPointNum = 0;

}
