#ifndef JNI_COMMON_H
#define JNI_COMMON_H

////////////////////////////////////////////////////////////////////////////////
// Debug Config
////////////////////////////////////////////////////////////////////////////////


#ifdef JNI_DEBUG
#define NAVI_ENTER(fmt)            LOGDp(fmt)
#define NAVI_DEBUG(fmt, arg...)            LOGE("Debug: %s|%s[%d] "fmt, __FILE__, __FUNCTION__, __LINE__, ##arg)
#define NAVI_ERROR(fmt, arg...)            LOGE("Error* %s|%s[%d] "fmt, __FILE__, __FUNCTION__, __LINE__, ##arg)
#define NAVI_LEAVE(fmt, arg...)            LOGE("Leave< %s|%s[%d] "fmt, __FILE__, __FUNCTION__, __LINE__, ##arg)
#else
#define NAVI_ENTER(fmt, arg...)
#define NAVI_DEBUG(fmt, arg...)
#define NAVI_ERROR(fmt, arg...)
#define NAVI_LEAVE(fmt, arg...)
#endif

// ADD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応 START
#define CLASS_CommonLib				"net/zmap/android/pnd/v2/common/CommonLib"
// ADD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応  END

#ifdef __cplusplus
extern "C" {
#endif

int SendMessageToJavaUI(int msg, int ext1, int ext2, const char* extb, int extbsize);

// Add by qilj for bug 851 begin
int UI_DrawMagName(int bShow);
// Add by qilj for bug 851 end

//Add 2011/03/08 thedoanh Start -->
char* ConvUtf8ToSjis ( const char* extb);
//Add 2011/03/08 thedoanh End <--

int ShiftJisToUtf8(const char* shiftjis, int ilenjis, char*Utf8, int ilenutf8);

// ADD.2013.05.10 N.Sasao エンジン共通化対応 START
char* GetDrawTextSizeInfo( const char* text,float fontSize, int bold,int writing_mode );
char* GetFontTextureInfo( const char* pInputData );
char* GetAssetFileInfo( const char* pszFileName );
// ADD.2013.05.10 N.Sasao エンジン共通化対応  END

#ifdef __cplusplus
}
#endif

#endif /* NMTV_COMMON_H */
