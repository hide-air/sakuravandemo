/********************************************************************************
* File Name	:	 JNISet_Communication.cpp
* Created	:	2009/11/21
* Author	:	xiayx
* Model		:
* Description	:
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "JNISet_Communication.h"
#include "NaviEngineIF.h"

#ifdef __cplusplus
extern "C" {
#endif
#include "NaviEngineCtrl.h"
#ifdef __cplusplus
}
#endif

#include "windows.h"
#include "NaviEngine/POI/POIStruct.h"
#include "jni/JNI_Common.h"
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define LOG_TAG "NavigationJNI_NaviRun"
#define JNI_DEBUG

#include "debugPrint.h"
#include <pthread.h>
#include "semaphore.h"
#include "ZDCPND_Dev.h"
#include "JNI_Common.h"
#include <wchar.h>
#include "VPExternTypes.h"
#include "VPNMEAParser.h"
#include "VPIF.h"
#include "VPSensorData.h"

//#include "MPFriendCarItem.h"

/*********************************************************************
*	Function Name	: JNI_NE_Drv_GetCarType        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_Drv_GetCarType
  (JNIEnv *env, jclass cls,jobject object)
{
	ENE_CarType CarType;

	jlong Ret = (jlong)NE_Drv_GetCarType(&CarType);

	jclass claz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(claz)
	{
		//Get variable's ID in JNI
		jfieldID jCarType = env->GetFieldID(claz,"m_iCommon","I");
		if(jCarType)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,jCarType,(jint)CarType);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_Drv_GetCarType GetLongField jCarType Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_Drv_GetCarType FindClass claz Failed");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_Drv_SetCarType        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_NE_Drv_SetCarType
  (JNIEnv *env, jclass clz,jint CarType)
{
	return (jlong)NE_Drv_SetCarType((ENE_CarType)CarType);
}

/*********************************************************************
*	Function Name	: JNI_NE_Drv_GetDefaultSearchProperty        *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_Drv_GetDefaultSearchProperty
  (JNIEnv *env, jclass cls,jobject object)
{
	ENE_CarNaviSearchProperty Property;

	jlong Ret = (jlong)NE_Drv_GetDefaultSearchProperty(&Property);

	jclass claz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(claz)
	{
		//Get variable's ID in JNI
		jfieldID jProperty = env->GetFieldID(claz,"m_iCommon","I");
		if(jProperty)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,jProperty,(jint)Property);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_Drv_GetDefaultSearchProperty GetLongField jProperty Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_Drv_GetDefaultSearchProperty FindClass claz Failed");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_Drv_SetDefaultSearchProperty       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_Drv_SetDefaultSearchProperty
  (JNIEnv *env, jclass clz,jint Property)
{
	return (jlong)NE_Drv_SetDefaultSearchProperty((ENE_CarNaviSearchProperty)Property);
}

/*********************************************************************
*	Function Name	: JNI_NE_Drv_GetSectionSearchProperty       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_Drv_GetSectionSearchProperty
  (JNIEnv *env, jclass clz,jint PointSt,jint PointEd,jobject object)
{
	ENE_CarNaviSearchProperty Property;

	//jlong Ret =
	(jlong)NE_Drv_GetSectionSearchProperty((ENE_RoutePointType)PointSt,(ENE_RoutePointType)PointEd,&Property);

	jclass claz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(claz)
	{
		//Get variable's ID in JNI
		jfieldID jProperty = env->GetFieldID(claz,"m_iCommon","I");
		if(jProperty)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,jProperty,(jint)Property);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_Drv_GetDefaultSearchProperty GetLongField jProperty Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_Drv_GetDefaultSearchProperty FindClass claz Failed");
	}
	return 0;
}

/*********************************************************************
*	Function Name	: JNI_NE_Drv_SetSectionSearchProperty       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_Drv_SetSectionSearchProperty
  (JNIEnv *env, jclass clz,jint PointSt,jint PointEd,jint Property)
{
	return (jlong)NE_Drv_SetSectionSearchProperty((ENE_RoutePointType)PointSt,(ENE_RoutePointType)PointEd,(ENE_CarNaviSearchProperty)Property);
}

/*********************************************************************
*	Function Name	: JNI_NE_Drv_GetComplexProperty             *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_Drv_GetComplexProperty
  (JNIEnv *env, jclass cls,jobject object)
{
	ENE_CarNaviComplexProperty Property;

	jlong Ret = (jlong)NE_Drv_GetComplexProperty(&Property);

	jclass claz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(claz)
	{
		//Get variable's ID in JNI
		jfieldID jProperty = env->GetFieldID(claz,"m_iCommon","I");
		if(jProperty)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,jProperty,(jint)Property);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_Drv_GetComplexProperty GetLongField jProperty Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_Drv_GetComplexProperty FindClass claz Failed");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_Drv_SetComplexProperty             *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_Drv_SetComplexProperty
  (JNIEnv *env, jclass clz,jint Property)
{
	return (jlong)NE_Drv_SetComplexProperty((ENE_CarNaviComplexProperty)Property);
}


//Add 2011/09/30 Z01yamaguchi Start -->
/*********************************************************************
*	Function Name	: JNI_NE_Drv_GetVicsUpdateInterval             *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetVicsInfo
  (JNIEnv *env, jclass cls,jobject object)
{
	ENE_VicsInfo eVicsInfo;

#ifndef PND_NOVICS
	jlong Ret = (jlong)NE_Vics_GetVicsInfo(&eVicsInfo);
#else
	iVicsUpdateInterval = 0;
#endif

	jclass claz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(claz)
	{
		//Get variable's ID in JNI
		jfieldID jInterval = env->GetFieldID(claz,"m_iCommon","I");
		if(jInterval)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,jInterval,(jint)eVicsInfo);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_Drv_GetVicsUpdateInterval GetLongField jInterval Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_Drv_GetVicsUpdateInterval FindClass claz Failed");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_Drv_SetVicsUpdateInterval             *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetVicsInfo
  (JNIEnv *env, jclass clz,jint iVicsInfo)
{
#ifndef PND_NOVICS
	return (jlong)NE_Vics_SetVicsInfo((ENE_VicsInfo)iVicsInfo);
#else
	return CT_SUCCESS;
#endif
}
//Add 2011/09/30 Z01yamaguchi End <--


/*********************************************************************
*	Function Name	: JNI_NE_Man_GetDefaultSearchProperty        *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_Man_GetDefaultSearchProperty
  (JNIEnv *env, jclass cls,jobject object)
{
	ENE_ManNaviSearchProperty Property;

	jlong Ret = (jlong)NE_Man_GetDefaultSearchProperty(&Property);

	jclass claz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(claz)
	{
		//Get variable's ID in JNI
		jfieldID jProperty = env->GetFieldID(claz,"m_iCommon","I");
		if(jProperty)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,jProperty,(jint)Property);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_Man_GetDefaultSearchProperty GetLongField jProperty Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_Man_GetDefaultSearchProperty FindClass claz Failed");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_Man_SetDefaultSearchProperty       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_Man_SetDefaultSearchProperty
  (JNIEnv *env, jclass clz,jint Property)
{
	return (jlong)NE_Man_SetDefaultSearchProperty((ENE_ManNaviSearchProperty)Property);
}

/*********************************************************************
*	Function Name	: JNI_NE_Man_GetPointSearchProperty       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_Man_GetPointSearchProperty
  (JNIEnv *env, jclass clz,jint PointType,jobject object)
{
	ENE_ManNaviSearchProperty Property;

	//jlong Ret =
	(jlong)NE_Man_GetPointSearchProperty((ENE_RoutePointType)PointType,&Property);

	jclass claz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(claz)
	{
		//Get variable's ID in JNI
		jfieldID jProperty = env->GetFieldID(claz,"m_iCommon","I");
		if(jProperty)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,jProperty,(jint)Property);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_Drv_GetDefaultSearchProperty GetLongField jProperty Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_Drv_GetDefaultSearchProperty FindClass claz Failed");
	}
	return 0;
}

/*********************************************************************
*	Function Name	: JNI_NE_Man_SetPointSearchProperty       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_Man_SetPointSearchProperty
  (JNIEnv *env, jclass clz,jint Type,jint Property)
{
	return (jlong)NE_Man_SetPointSearchProperty((ENE_RoutePointType)Type,(ENE_ManNaviSearchProperty)Property);
}

/*********************************************************************
*	Function Name	: JNI_NE_SetOPSound       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetOPSound
  (JNIEnv *env, jclass clz,jint OPsound)
{
	return (jlong)NE_SetOPSound(OPsound);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetOPSound       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetOPSound
  (JNIEnv *env, jclass clz,jobject object)
{
	int bOn;

	jlong lRet = (jlong)NE_GetOPSound(&bOn);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID IsOn = env->GetFieldID(clazz,"m_iCommon","I");
		if(IsOn)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,IsOn,bOn);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetOPSound GetFieldID IsOn Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_SetGuideVoice       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetGuideVoice
  (JNIEnv *env, jclass clz,jint OPsound)
{
	return (jlong)NE_SetGuideVoice(OPsound);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetGuideVoice       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetGuideVoice
  (JNIEnv *env, jclass clz,jobject object)
{
	int bOn;

	jlong lRet = (jlong)NE_GetGuideVoice(&bOn);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID IsOn = env->GetFieldID(clazz,"m_iCommon","I");
		if(IsOn)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,IsOn,bOn);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetOPSound GetFieldID IsOn Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_IsExternalGPS       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_IsExternalGPS
  (JNIEnv *env, jclass clz,jobject object)
{
	int bOn;

	jlong lRet = (jlong)NE_IsExternalGPS(&bOn);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID IsOn = env->GetFieldID(clazz,"m_iCommon","I");
		if(IsOn)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,IsOn,bOn);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_IsExternalGPS GetFieldID IsOn Failed");
		}
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_GetGPSPortNo       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetGPSPortNo
  (JNIEnv *env, jclass clz,jobject object)
{
	const char* pGPSPortNo;
	jlong lRet = (jlong)NE_GetGPSPortNo(&pGPSPortNo);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIString");


	jfieldID str1 = env->GetFieldID(clazz,"Str","Ljava/lang/String;");
	//LOGE("[DEBUG]String is : str = [%s]\n", str);


	env->SetObjectField(object,str1,env->NewStringUTF(pGPSPortNo));

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_UpdateGPSPortNo       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_UpdateGPSPortNo
  (JNIEnv *env, jclass clz,jstring jstr)
{
	   //LOGDp("[DEBUG]JNI_NE_UpdateGPSPortNo Begin!!!!!!!!!!!");
       char* rtn = NULL;

       jclass clsstring = env->FindClass("java/lang/String");
       jstring strencode = env->NewStringUTF("utf-8");
       jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
       jbyteArray barr= (jbyteArray)env->CallObjectMethod(jstr, mid, strencode);
       jsize alen = env->GetArrayLength(barr);
       jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);
       if (alen > 0)
       {
                 rtn = new char[alen+1];
                 memcpy(rtn, ba, alen);
                 rtn[alen] = 0;
       }

       jlong Ret = NE_UpdateGPSPortNo(rtn);
       //LOGDp("[DEBUG] JNI_NE_UpdateGPSPortNo rtn:%s!!!!!!!!!!!!",rtn);
       env->ReleaseByteArrayElements(barr, ba, 0);

       // CID#10782 変数rtnのメモリーリーク対応
       if (rtn)
       {
    	   delete [] rtn;
    	   rtn = NULL;
       }

       //LOGDp("[DEBUG]JNI_NE_UpdateGPSPortNo End!!!!!!!!!!!");

       return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_GetDBDiskTitle       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetDBDiskTitle
  (JNIEnv *env, jclass clz,jobject object)
{
	char* pDBDiskTitle = 0;
    jfieldID str1 = 0;
	jmethodID ctorID = 0;
	jlong lRet = (jlong)NE_GetDBDiskTitle(&pDBDiskTitle);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIString");
	if (clazz)
	{
		str1 = env->GetFieldID(clazz,"m_StrAddressString","Ljava/lang/String;");
	}
	else
        {
		LOGDp("[DEBUG]wwwwwwwwwwwwwwww JNI_NE_GetDBDiskTitle FindClass JNIString Failed");
	}
	//LOGE("[DEBUG]String is : str = [%s]\n", str);
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}else
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList for 7-error strClass NULL");
	}

	if (str1)
	{
		//env->SetObjectField(object,str1,env->NewStringUTF(pDBDiskTitle));

		int lenjis = strlen((char*)pDBDiskTitle);
		jbyteArray bytes = env->NewByteArray(lenjis);
		if(bytes != NULL)
		{
			env->SetByteArrayRegion(bytes, 0, lenjis, (jbyte*)pDBDiskTitle);

		}else{
			LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList for 8 error bytes==NULL");
		}
		jstring encoding = env->NewStringUTF("ShiftJIS");


		jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

		if(jsName)
		{
			env->SetObjectField(object,str1,jsName);
		}else
		{
			LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList error888 jsName== NULL ");
		}

		if(NULL != bytes)
		{
			env->DeleteLocalRef( bytes);
			bytes = NULL;
		}
		if(NULL != encoding)
		{
			env->DeleteLocalRef( encoding);
			encoding = NULL;
		}
		if(NULL != jsName)
		{
			env->DeleteLocalRef( jsName);
			jsName = NULL;
		}
	}
	else
        {
		LOGDp("[DEBUG] JNI_NE_GetDBDiskTitle GetFieldID str1 Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_SetExternalGPS       *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetExternalGPS
  (JNIEnv *env, jclass clz,jint ExternalGPS)
{
	return (jlong)NE_SetExternalGPS((int)ExternalGPS);
}

// MOD.2013.08.22 N.Sasao エンジン共通化対応2 START
//Chg 2011/07/25 Z01thedoanh (自由解像度対応: Copy or Modify from MarketV1) Start -->
JNIEXPORT jlong JNICALL JNI_InitInstance
//(JNIEnv *env, jclass clz)
//@@MOD-START BB-0003 2012/10/29 Y.Hayashida
// MOD.2013.05.10 N.Sasao エンジン共通化対応 START
(JNIEnv *env, jclass clz, jint ileft, jint itop, jint iright, jint ibottom, jint iGuideMapsize, jint iFrontWide,
		jint dispBrdLeft, jint dispBrdTop, jint dispBrdRight, jint dispBrdBottom, jint isSmallMode
		, jint routeCalcMapLeft, jint routeCalcMapTop, jint routeCalcMapRight, jint routeCalcMapBottom, jstring sdpath, jstring apppath
		, jstring drawParamPath, jstring drawParamFilename, jstring drawIconFilename, jstring extDrawParamPath, jstring extDrawParamPrefixName
		, jboolean simulationLoopMode)
// MOD.2013.05.10 N.Sasao エンジン共通化対応  END
//(JNIEnv *env, jclass clz, jint ileft, jint itop, jint iright, jint ibottom, jint iGuideMapsize, jint iFrontWide,
//		jint dispBrdLeft, jint dispBrdTop, jint dispBrdRight, jint dispBrdBottom, jint isSmallMode
//		, jint routeCalcMapLeft, jint routeCalcMapTop, jint routeCalcMapRight, jint routeCalcMapBottom)
//@@MOD-END BB-0003 2012/10/29 Y.Hayashida
// MOD.2013.08.22 N.Sasao エンジン共通化対応2  END
{
//@@MOD-START BB-0003 2012/10/29 Y.Hayashida
	const char* sdcardpath = env->GetStringUTFChars(sdpath, NULL);
//@@MOD-END BB-0003 2012/10/29 Y.Hayashida
// ADD.2013.05.10 N.Sasao エンジン共通化対応 START
	const char* appDataPath = env->GetStringUTFChars(apppath, NULL);
// ADD.2013.05.10 N.Sasao エンジン共通化対応  END

// MOD.2013.08.22 N.Sasao エンジン共通化対応2 START
	const char* drawParameterPath = env->GetStringUTFChars(drawParamPath, NULL);
	const char* drawParameterFilename = env->GetStringUTFChars(drawParamFilename, NULL);
	const char* drawParameterIconFilename = env->GetStringUTFChars(drawIconFilename, NULL);
	const char* extDrawParameterPath = env->GetStringUTFChars(extDrawParamPath, NULL);
	const char* extDrawParameterPrefixName = env->GetStringUTFChars(extDrawParamPrefixName, NULL);
// MOD.2013.08.22 N.Sasao エンジン共通化対応2  END
	//	InitInstance();
	RECT rect = {ileft, itop, iright, ibottom};
	// 案内情報RECT
	RECT dispBoardRect = {dispBrdLeft, dispBrdTop, dispBrdRight, dispBrdBottom};
	// 探索結果地図RECT
	RECT routeCalcMapRect = {routeCalcMapLeft, routeCalcMapTop, routeCalcMapRight, routeCalcMapBottom};
// MOD.2013.08.22 N.Sasao エンジン共通化対応2 START
//@@MOD-START BB-0003 2012/10/29 Y.Hayashida
// MOD.2013.05.10 N.Sasao エンジン共通化対応 START
	InitInstance(rect, iGuideMapsize, iFrontWide, dispBoardRect, isSmallMode, routeCalcMapRect, sdcardpath, appDataPath, drawParameterPath, 
		drawParameterFilename, drawParameterIconFilename, extDrawParameterPath, extDrawParameterPrefixName
		, simulationLoopMode);
	env->ReleaseStringUTFChars(sdpath, sdcardpath);
	env->ReleaseStringUTFChars(apppath, appDataPath);
	env->ReleaseStringUTFChars(drawParamPath, drawParameterPath);
	env->ReleaseStringUTFChars(drawParamFilename, drawParameterFilename);
	env->ReleaseStringUTFChars(drawIconFilename, drawParameterIconFilename);
	env->ReleaseStringUTFChars(extDrawParamPath, extDrawParameterPath);
	env->ReleaseStringUTFChars(extDrawParamPrefixName, extDrawParameterPrefixName);
	
// MOD.2013.05.10 N.Sasao エンジン共通化対応  END
//	InitInstance(rect, iGuideMapsize, iFrontWide, dispBoardRect, isSmallMode, routeCalcMapRect);
//@@MOD-END BB-0003 2012/10/29 Y.Hayashida
// MOD.2013.08.22 N.Sasao エンジン共通化対応2  END
	return 0;
}
//Chg 2011/07/25 Z01thedoanh (自由解像度対応: Copy or Modify from MarketV1) End <--

JNIEXPORT jlong JNICALL JNI_NE_Finalize
  (JNIEnv *env, jclass clz)
{
	NE_Finalize();

	return 0;
}

JNIEXPORT jlong JNICALL JNI_saveConfig
  (JNIEnv *env, jclass clz)
{
	saveConfig();

	return 0;
}

/*********************************************************************
*	Function Name	: JNI_NE_GetSatelliteInfo      *
*	Description	:                                            *
*	Date		: 09/12/30                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetSatelliteInfo
  (JNIEnv *env, jclass clz,jobject object)
{
	ZNE_SatelliteInfo		SatelliteInfo;
	jmethodID ctorID;
	jobject  obj_JNITwoLong = 0;
	jobjectArray objArray_ZNE_Satellite = 0;
	jlong Ret = 0;

	memset( &SatelliteInfo, 0, sizeof( SatelliteInfo ) );
	Ret = (jlong)NE_GetSatelliteInfo(&SatelliteInfo);

	jclass ClassZNE_SatelliteInfo = env->FindClass("net/zmap/android/pnd/v2/data/ZNE_SatelliteInfo");
	jclass ClassZNE_Satellite = env->FindClass("net/zmap/android/pnd/v2/data/ZNE_Satellite");
	jclass ClassJNITwoLong = env->FindClass("net/zmap/android/pnd/v2/data/JNITwoLong");

	jfieldID jfield_Point = env->GetFieldID( ClassZNE_SatelliteInfo,"Point","Lnet/zmap/android/pnd/v2/data/JNITwoLong;");

	if(ClassJNITwoLong != NULL)
	{
		ctorID = env->GetMethodID( ClassJNITwoLong, "<init>", "()V");

		obj_JNITwoLong = (jobject)env->NewObject(ClassJNITwoLong, ctorID);

		if(NULL == obj_JNITwoLong)
		{
			LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo  NewObject obj_JNITwoLong Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		jfieldID jlnLatitude = env->GetFieldID( ClassJNITwoLong,"m_lLat","J");
		if(jlnLatitude)
		{
			//Set the variable's value  to Java
			env->SetLongField(obj_JNITwoLong,jlnLatitude,(jlong)SatelliteInfo.stPoint.lnLatitude);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo GetFieldID jlnLatitude Failed");
		}

		jfieldID jlnLongitude = env->GetFieldID( ClassJNITwoLong,"m_lLong","J");
		if(jlnLongitude)
		{
			//Set the variable's value  to Java
			env->SetLongField(obj_JNITwoLong,jlnLongitude,(jlong)SatelliteInfo.stPoint.lnLongitude);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo GetFieldID jlnLongitude Failed");
		}

	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo  Find  class ClassJNITwoLong Failed!!!@@@@@@@@@@@@@@@@@@@@");
	}

	if(ClassZNE_SatelliteInfo != NULL)
	{

		env->SetObjectField(object, jfield_Point, obj_JNITwoLong);

		jfieldID jlnTotalSatsInView = env->GetFieldID( ClassZNE_SatelliteInfo,"lnTotalSatsInView","J");
		if(jlnTotalSatsInView)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,jlnTotalSatsInView,(jlong)SatelliteInfo.dwTotalSatsInView);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo GetFieldID jlnTotalSatsInView Failed");
		}

		jfieldID jdAltitude = env->GetFieldID(ClassZNE_SatelliteInfo,"dAltitude","D");
		if(jdAltitude)
		{
			//Set the variable's value  to Java
			env->SetDoubleField(object,jdAltitude,(jdouble)SatelliteInfo.dAltitude);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo GetFieldID jdAltitude Failed");
		}

		jfieldID jfld_Satellite = env->GetFieldID(ClassZNE_SatelliteInfo,"Satellite","[Lnet/zmap/android/pnd/v2/data/ZNE_Satellite;");

		if(ClassZNE_Satellite != NULL)
		{
			objArray_ZNE_Satellite = env->NewObjectArray((jlong)SatelliteInfo.dwTotalSatsInView, ClassZNE_Satellite, NULL);
		}
		else
		{
				LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo  Find ClassZNE_Satellite Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}

		int iTotalSatsInView = (int)SatelliteInfo.dwTotalSatsInView;
		for(int i=0; i<iTotalSatsInView; i++)
		{
			jmethodID  ctorID2;
			jobject    obj_ZNE_Satellite = 0;
			if(ClassZNE_Satellite != NULL)
			{
				ctorID2 = env->GetMethodID(ClassZNE_Satellite, "<init>", "()V");
				obj_ZNE_Satellite = (jobject)env->NewObject(ClassZNE_Satellite, ctorID2);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo  Find ClassZNE_Satellite Failed!!!@@@@@@@@@@@@@@@@@@@@");
			}

			jfieldID jsSatID = env->GetFieldID(ClassZNE_Satellite,"sSatID","S");
			if(jsSatID)
			{
				//Set the variable's value  to Java
				env->SetShortField(obj_ZNE_Satellite,jsSatID,(jshort)SatelliteInfo.stSatellite[i].wSatID);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo GetFieldID jsSatID Failed");
			}

			jfieldID jsAzimuth = env->GetFieldID(ClassZNE_Satellite,"sAzimuth","S");
			if(jsAzimuth)
			{
				//Set the variable's value  to Java
				env->SetShortField(obj_ZNE_Satellite,jsAzimuth,(jshort)SatelliteInfo.stSatellite[i].wAzimuth);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo GetFieldID jsAzimuth Failed");
			}

			jfieldID jsElevation = env->GetFieldID(ClassZNE_Satellite,"sElevation","S");
			if(jsElevation)
			{
				//Set the variable's value  to Java
				env->SetShortField(obj_ZNE_Satellite,jsElevation,(jshort)SatelliteInfo.stSatellite[i].wElevation);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo GetFieldID jsElevation Failed");
			}

			jfieldID jsSNRate = env->GetFieldID(ClassZNE_Satellite,"sSNRate","S");
			if(jsSNRate)
			{
				//Set the variable's value  to Java
				env->SetShortField(obj_ZNE_Satellite,jsSNRate,(jshort)SatelliteInfo.stSatellite[i].wSNRate);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo GetFieldID jsSNRate Failed");
			}

			jfieldID jiEnable = env->GetFieldID(ClassZNE_Satellite,"iEnable","I");
			if(jiEnable)
			{
				//Set the variable's value  to Java
				env->SetIntField(obj_ZNE_Satellite,jiEnable,(jint)SatelliteInfo.stSatellite[i].bEnable);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_GetSatelliteInfo GetFieldID jiEnable Failed");
			}
			env->SetObjectArrayElement(objArray_ZNE_Satellite, i, obj_ZNE_Satellite);
		}
		if(jfld_Satellite != NULL)
		{
			env->SetObjectField(object, jfld_Satellite, objArray_ZNE_Satellite);
		}
	}
	else
	{
		LOGDp("[DEBUG] ClassZNE_SatelliteInfo  Find class ClassZNE_SatelliteInfo Failed!!!@@@@@@@@@@@@@@@@@@@@");

	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_CMN_cos       *
*	Description	:                                            *
*	Date		: 09/12/30                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CMN_cos
  (JNIEnv *env, jclass clz,jshort Azimuth, jobject object)
{
	float fcos = 0.0f;
	jlong lRet = 0;

	fcos = CMN_cos((short)Azimuth);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIFloat");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jfCommon = env->GetFieldID(clazz,"m_fCommon","F");
		if(jfCommon)
		{
			//Set the variable's value  to Java
			env->SetFloatField(object,jfCommon, (jfloat)fcos);
		}
		else
		{
			LOGDp("[DEBUG] JNI_CMN_cos GetFieldID jfCommon Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_CMN_cos Find  clazz Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_CMN_sin       *
*	Description	:                                            *
*	Date		: 09/12/30                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CMN_sin
  (JNIEnv *env, jclass clz,jshort Azimuth, jobject object)
{
	float fcos = 0.0f;
	jlong lRet = 0;

	fcos = CMN_sin((short)Azimuth);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIFloat");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jfCommon = env->GetFieldID(clazz,"m_fCommon","F");
		if(jfCommon)
		{
			//Set the variable's value  to Java
			env->SetFloatField(object,jfCommon, (jfloat)fcos);
		}
		else
		{
			LOGDp("[DEBUG] JNI_CMN_cos GetFieldID jfCommon Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_CMN_cos Find  clazz Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_VP_NMEASetGPSStat       *
*	Description	:                                            *
*	Date		: 10/03/01                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNI_VP_NMEASetGPSStat
  (JNIEnv *env, jclass clz,jint gpsStat)
{
	//LOGDp("[DEBUG] JNI_VP_NMEASetGPSStat gpsStat:%d",gpsStat);
	jlong ret = (jlong)VP_NMEASetGPSStat((ENE_GPSStatus)gpsStat);

	return ret;
}

/*********************************************************************
*	Function Name	: JNI_VP_NMEASetGPSSensorData       *
*	Description	:                                            *
*	Date		: 10/03/01                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNI_VP_NMEASetGPSSensorData
  (JNIEnv *env, jclass clz,jobject object)
{
	ZVP_GPSSensorData_t gpsSensorData;

	jclass jcls = env->GetObjectClass(object);

	if (jcls)
	{
		jfieldID jfid1 = env->GetFieldID( jcls, "longitude1000", "J" );
		if (jfid1)
		{
			jlong value1 = env->GetLongField(object,jfid1);
			gpsSensorData.lnLongitude1000= (long)value1;
		}

		jfieldID jfid2 = env->GetFieldID( jcls, "latitude1000", "J" );
		if (jfid2)
		{
			jlong value2 = env->GetLongField(object,jfid2);
			gpsSensorData.lnLatitude1000= (long)value2;
		}

		jfieldID jfid3 = env->GetFieldID( jcls, "nDirDegCW", "S" );
		if (jfid3)
		{
// Chg 2012/02/08 Z01_h_yamada Start -->
//			jlong value3 = env->GetLongField(object,jfid3);
//--------------------------------------
			jlong value3 = env->GetShortField(object,jfid3);
// Chg 2012/02/08 Z01_h_yamada End <--
			gpsSensorData.nDirDegCW= (short)value3;
		}

// Chg 2011/01/20 Z01ONOZAWA Start --> 速度の精度を維持するためfloatに変更
//		jfieldID jfid4 = env->GetFieldID( jcls, "speed", "J" );
		jfieldID jfid4 = env->GetFieldID( jcls, "fSpeedMPS", "F" );
		if (jfid4)
		{
//			jlong value4 = env->GetLongField(object,jfid4);
			jfloat value4 = env->GetFloatField(object,jfid4);
// Chg 2010/11/11 Z01ONOZAWA Start --> ｍ単位の精度が欲しいので、m/sec * 1000 を格納している
//			gpsSensorData.ulnSpeed = (long)value4;
//			gpsSensorData.ulnSpeed = (long)((double)value4 * 3600.0 / 1000000.0);//@@ 取り敢えずkm/hに変換
// Chg 2010/11/11 Z01ONOZAWA End   <--
			gpsSensorData.ulnSpeedKPH = (long)(value4 * 3600.0 / 1000.0);// m/s -> km/hに変換
		}
// Chg 2011/01/20 Z01ONOZAWA End   <--

// Add 2010/12/20 Z01ONOZAWA Start -->
		jfieldID jfid5 = env->GetFieldID( jcls, "timestamp", "J" );
		if (jfid5)
		{
			jlong value5 = env->GetLongField(object,jfid5);
			gpsSensorData.lnTimestamp = (long)value5;
		}
// Add 2010/12/20 Z01ONOZAWA End   <--
// 		if (gpsSensorData.ulnSpeed > 0) {
// 			LOGDp("GPS Sensor JNI Push [%d:%d] Dir:%d Speed:%d Km/h",
// 								gpsSensorData.lnLatitude1000, gpsSensorData.lnLongitude1000,
// 								gpsSensorData.nDirDegCW, gpsSensorData.ulnSpeedKPH);
// 		}

		//
		// V2.5
		//
		jfieldID jfid6 = env->GetFieldID( jcls, "fAltitude", "F" );
		if (jfid6)
		{
			jfloat value6 = env->GetFloatField(object,jfid6);
			gpsSensorData.fAltitude = (float)(value6);
		}
		jfieldID jfid7 = env->GetFieldID( jcls, "fAccuracy", "F" );
		if (jfid7)
		{
			jfloat value7 = env->GetFloatField(object,jfid7);
			gpsSensorData.fAccuracy = (float)(value7);
		}
		jfieldID jfid8 = env->GetFieldID( jcls, "fsnrAverageUsedInFix", "F" );
		if (jfid8)
		{
			jfloat value8 = env->GetFloatField(object,jfid8);
			gpsSensorData.fsnrAverageUsedInFix = (float)(value8);
		}
		jfieldID jfid9 = env->GetFieldID( jcls, "iFixCount", "I" );
		if (jfid9)
		{
			jfloat value9 = env->GetIntField(object,jfid9);
			gpsSensorData.iFixCount = (int)(value9);
		}
		jfieldID jfid10 = env->GetFieldID( jcls, "d_wlon", "D" );
		if (jfid10)
		{
			jfloat value10 = env->GetDoubleField(object,jfid10);
			gpsSensorData.d_wlon = (double)(value10);
		}
		jfieldID jfid11 = env->GetFieldID( jcls, "d_wlat", "D" );
		if (jfid11)
		{
			jfloat value11 = env->GetDoubleField(object,jfid11);
			gpsSensorData.d_wlat = (double)(value11);
		}
	}

	else
	{

		LOGDp("[DEBUG] JNI_VP_NMEASetGPSSensorData FindClass  Failed");
	}

	jlong ret = (jlong)VP_NMEASetGPSSensorData(gpsSensorData);

	return ret;
}

/*********************************************************************
*	Function Name	: JNI_VP_NMEASetSatellitesInfo       *
*	Description	:                                            *
*	Date		: 10/03/01                                  *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNI_VP_NMEASetSatellitesInfo
  (JNIEnv *env, jclass clz,jobject object,jobjectArray args,jint satelliteNum)
{
	// CID#10989 未初期化変数数宣言指摘への対応
	ZVP_SatInfoList_t satellitesInfo = {0};

	//LOGDp("[DEBUG] JNI_VP_NMEASetSatellitesInfo Begin!!!!!!!!!!!");
	jclass jcls = env->GetObjectClass(object);
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/ZVP_SatInfo_t");

	if (jcls)
	{
		jfieldID jfid1 = env->GetFieldID( jcls, "wTotalSatsInView", "J" );
		if (jfid1)
		{
			jlong value1 = env->GetLongField(object,jfid1);
			satellitesInfo.wTotalSatsInView= (long)value1;
			//LOGDp("[DEBUG] JNI_VP_NMEASetSatellitesInfo satellitesInfo.wTotalSatsInView:%d!!!!!!!!!!!",satellitesInfo.wTotalSatsInView);
		}

		jfieldID jfid2 = env->GetFieldID( jcls, "lnLongitude1000", "J" );
		if (jfid2)
		{
			jlong value2 = env->GetLongField(object,jfid2);
			satellitesInfo.lnLongitude1000= (long)value2;
			//LOGDp("[DEBUG] JNI_VP_NMEASetSatellitesInfo satellitesInfo.lnLongitude1000:%d!!!!!!!!!!!",satellitesInfo.lnLongitude1000);
		}

		jfieldID jfid3 = env->GetFieldID( jcls, "lnLatitude1000", "J" );
		if (jfid3)
		{
			jlong value3 = env->GetLongField(object,jfid3);
			satellitesInfo.lnLatitude1000= (short)value3;
			//LOGDp("[DEBUG] JNI_VP_NMEASetSatellitesInfo satellitesInfo.lnLatitude1000:%d!!!!!!!!!!!",satellitesInfo.lnLatitude1000);
		}

		jfieldID jfid4 = env->GetFieldID( jcls, "dAltitude", "D" );
		if (jfid4)
		{
			jlong value4 = env->GetLongField(object,jfid4);
			satellitesInfo.dAltitude= (long)value4;
			//LOGDp("[DEBUG] JNI_VP_NMEASetSatellitesInfo satellitesInfo.dAltitude:%d!!!!!!!!!!!",satellitesInfo.dAltitude);
		}

		//LOGDp("[DEBUG] JNI_VP_NMEASetSatellitesInfo satelliteNum:%d",satelliteNum);
		for(int i = 0; i < (int)satelliteNum; i++)
		{
			jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);
			jfieldID jwSatID = env->GetFieldID(clazz, "wSatID", "I" );
			if(jwSatID)
			{
				jint SatID = env->GetIntField(obj,jwSatID);
				satellitesInfo.stVPSatInfo[satelliteNum].wSatID = (int)SatID;
			}
			jfieldID jwAzimuth = env->GetFieldID(clazz, "wAzimuth", "F" );
			if(jwAzimuth)
			{
				jfloat Azimuth = env->GetFloatField(obj,jwAzimuth);
				satellitesInfo.stVPSatInfo[satelliteNum].wAzimuth = (float)Azimuth;
			}
			jfieldID jwElevation = env->GetFieldID(clazz, "wElevation", "F" );
			if(jwElevation)
			{
				jfloat Elevation = env->GetFloatField(obj,jwElevation);
				satellitesInfo.stVPSatInfo[satelliteNum].wElevation = (float)Elevation;
			}
			jfieldID jwSNRate = env->GetFieldID(clazz, "wSNRate", "F" );
			if(jwSNRate)
			{
				jfloat SNRate = env->GetFloatField(obj,jwSNRate);
				satellitesInfo.stVPSatInfo[satelliteNum].wSNRate = (float)SNRate;
			}
			jfieldID jbEnable = env->GetFieldID(clazz, "bEnable", "Z" );
			if(jbEnable)
			{
				jboolean Enable = env->GetBooleanField(obj,jbEnable);

				satellitesInfo.stVPSatInfo[satelliteNum].bEnable = (bool)Enable;
			}
		}
	}

	else
	{

		LOGDp("[DEBUG] JNI_VP_NMEASetSatellitesInfo FindClass  Failed");
	}

	// CID#10412 168バイト構造体実体での引き渡しから参照に変更
	jlong ret = (jlong)VP_NMEASetSatellitesInfo(&satellitesInfo);

	return ret;

}

// Add 2010/10/12 ONOZAWA Start -->
/** 加速度センサー通知
 *
 */
JNIEXPORT jlong JNI_VP_SetAcceleroSensorData
  (JNIEnv *env, jclass clz, jobject object)
{
	// CID#11593 未初期化変数数宣言指摘への対応
	ZVP_AccSensorData_t accSensorData = {0};

	jclass jcls = env->GetObjectClass(object);

	if (jcls)
	{
		jfieldID jfid;
		jfid = env->GetFieldID(jcls, "accuracy", "I" );//long:J/float:F/double:D/boolean:Z/byte:B/char:C/
		if (jfid)
		{
			jint value = env->GetIntField(object, jfid);
			accSensorData.iAccuracy = (int)value;
		}

		jfid = env->GetFieldID(jcls, "timestamp", "J" );
		if (jfid)
		{
			jlong value = env->GetLongField(object, jfid);
			accSensorData.lnTimestamp = (long)value;
		}

		jfid = env->GetFieldID(jcls, "acceleroX", "F" );
		if (jfid)
		{
			jfloat value = env->GetFloatField(object, jfid);
			accSensorData.fAcceleroX = (float)value;
		}

		jfid = env->GetFieldID(jcls, "acceleroY", "F" );
		if (jfid)
		{
			jfloat value = env->GetFloatField(object, jfid);
			accSensorData.fAcceleroY = (float)value;
		}

		jfid = env->GetFieldID(jcls, "acceleroZ", "F" );
		if (jfid)
		{
			jfloat value = env->GetFloatField(object, jfid);
			accSensorData.fAcceleroZ = (float)value;
		}

// Add 2011/02/28 sawada Start -->
		jfid = env->GetFieldID(jcls, "status", "J" );
		if (jfid)
		{
			jlong value = env->GetLongField(object, jfid);
			accSensorData.lnSensorStatus = (long)value;
		}
// Add 2011/02/28 sawada End   <--
//Add 2011/04/20 Z01yoneya Start -->
		jfid = env->GetFieldID(jcls, "pitch", "F" );
		if (jfid)
		{
			jfloat value = env->GetFloatField(object, jfid);
			accSensorData.fOriPitchDeg = (float)value;
		}
		jfid = env->GetFieldID(jcls, "roll", "F" );
		if (jfid)
		{
			jfloat value = env->GetFloatField(object, jfid);
			accSensorData.fOriRollDeg = (float)value;
		}
		jfid = env->GetFieldID(jcls, "yaw", "F" );
		if (jfid)
		{
			jfloat value = env->GetFloatField(object, jfid);
			accSensorData.fOriYawDeg = (float)value;
		}
//Add 2011/04/20 Z01yoneya End <--

	}
	else
	{
		LOGDp("[DEBUG] JNI_VP_SetAcceleroSensorData FindClass  Failed");
		return ERROR;
	}

	jlong ret = (jlong)VP_SetAccSensorData(&accSensorData);

	return ret;
}

/** ジャイロセンサー通知
 *
 */
JNIEXPORT jlong JNI_VP_SetGyroSensorData
  (JNIEnv *env, jclass clz, jobject object)
{
	// CID#11594 未初期化変数数宣言指摘への対応
	ZVP_GyroSensorData_t gyroSensorData = {0};

	jclass jcls = env->GetObjectClass(object);

	if (jcls)
	{
		jfieldID jfid;
		jfid = env->GetFieldID(jcls, "accuracy", "I" );
		if (jfid)
		{
			jint value = env->GetIntField(object, jfid);
			gyroSensorData.iAccuracy = (int)value;
		}

		jfid = env->GetFieldID(jcls, "timestamp", "J" );
		if (jfid)
		{
			jlong value = env->GetLongField(object, jfid);
			gyroSensorData.lnTimestamp = (long)value;
		}

		jfid = env->GetFieldID(jcls, "gyroAccelX", "F" );
		if (jfid)
		{
			jfloat value = env->GetFloatField(object, jfid);
			gyroSensorData.fGyroAccelX = (float)value;
		}

		jfid = env->GetFieldID(jcls, "gyroAccelY", "F" );
		if (jfid)
		{
			jfloat value = env->GetFloatField(object, jfid);
			gyroSensorData.fGyroAccelY = (float)value;
		}

		jfid = env->GetFieldID(jcls, "gyroAccelZ", "F" );
		if (jfid)
		{
			jfloat value = env->GetFloatField(object, jfid);
			gyroSensorData.fGyroAccelZ = (float)value;
		}

// Add 2011/02/28 sawada Start -->
		jfid = env->GetFieldID(jcls, "status", "J" );
		if (jfid)
		{
			jlong value = env->GetLongField(object, jfid);
			gyroSensorData.lnSensorStatus = (long)value;
		}
// Add 2011/02/28 sawada End   <--
	}
	else
	{
		LOGDp("[DEBUG] JNI_VP_SetGyroSensorData FindClass  Failed");
		return ERROR;
	}

	jlong ret = (jlong)VP_SetGyroSensorData(&gyroSensorData);

	return ret;
}

/** 傾きセンサー通知
 *
 */
JNIEXPORT jlong JNI_VP_SetOrientSensorData
  (JNIEnv *env, jclass clz, jobject object)
{
	// CID#11595 未初期化変数数宣言指摘への対応
	ZVP_OriSensorData_t oriSensorData = {0};

	jclass jcls = env->GetObjectClass(object);

	if (jcls)
	{
		jfieldID jfid;
		jfid = env->GetFieldID(jcls, "accuracy", "I" );
		if (jfid)
		{
			jint value = env->GetIntField(object, jfid);
			oriSensorData.iAccuracy = (int)value;
		}

		jfid = env->GetFieldID(jcls, "timestamp", "J" );
		if (jfid)
		{
			jlong value = env->GetLongField(object, jfid);
			oriSensorData.lnTimestamp = (long)value;
		}

		jfid = env->GetFieldID(jcls, "azimuth", "F" );
		if (jfid)
		{
			jfloat value = env->GetFloatField(object, jfid);
			oriSensorData.fAzimuth = (float)value;
		}

		jfid = env->GetFieldID(jcls, "pitch", "F" );
		if (jfid)
		{
			jfloat value = env->GetFloatField(object, jfid);
			oriSensorData.fPitch = (float)value;
		}

		jfid = env->GetFieldID(jcls, "roll", "F" );
		if (jfid)
		{
			jfloat value = env->GetFloatField(object, jfid);
			oriSensorData.fRoll = (float)value;
		}

// Add 2011/02/28 sawada Start -->
		jfid = env->GetFieldID(jcls, "status", "J" );
		if (jfid)
		{
			jlong value = env->GetLongField(object, jfid);
			oriSensorData.lnSensorStatus = (long)value;
		}
// Add 2011/02/28 sawada End   <--
	}
	else
	{
		LOGDp("[DEBUG] JNI_VP_SetOrientSensorData FindClass  Failed");
		return ERROR;
	}

	jlong ret = (jlong)VP_SetOriSensorData(&oriSensorData);

	return ret;
}
// Add 2010/10/12 ONOZAWA End   <--
/*********************************************************************
*	Function Name	: JNI_VP_GetLoadMode      *
*	Description	:                                            *
*	Date		: 2010/01/07                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_VP_GetLoadMode
(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	EVP_LoadMode  iState;

	lRet = VP_GetLoadMode(&iState);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jiState = env->GetFieldID(clazz,"m_iCommon","I");
		if(jiState)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,jiState,(jint)iState);
		}
		else
		{
			LOGDp("[DEBUG] JNI_VP_GetLoadMode SetIntField jiState Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_VP_GetLoadMode FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_VP_IsOutputLog      *
*	Description	:                                            *
*	Date		: 2010/04/27                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_VP_IsOutputLog
(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;
	BOOL  bIsOutputLog;

	lRet = VP_IsOutputLog(&bIsOutputLog);

	int iTemp = (int)bIsOutputLog;
	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jiState = env->GetFieldID(clazz,"m_iCommon","I");
		if(jiState)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,jiState,(jint)iTemp);
		}
		else
		{
			LOGDp("[DEBUG] JNI_VP_IsOutputLog SetIntField jiState Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_VP_IsOutputLog FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;
}
extern int OutPut_UsedTime_Ctrl ;

// Add 2011/02/23 sawada Start -->
JNIEXPORT jlong JNICALL JNI_VP_SetCarGoingStatus
  (JNIEnv *env, jclass a, jlong status)
{
	return (jlong)VP_SetCarGoingStatus((long)status);
}
// Add 2011/02/23 sawada End   <--
//Add 2011/09/08 Z01yoneya Start -->
JNIEXPORT jlong JNI_VP_SetEnableSensorNaviFlag
  (JNIEnv *env, jclass clz, jint sensorNaviFlag)
{
	return (jlong)VP_SetEnableSensorNaviFlag((int)sensorNaviFlag);
}
JNIEXPORT jint JNI_VP_GetEnableSensorNaviFlag
  (JNIEnv *env, jclass clz)
{
	return (jint)VP_GetEnableSensorNaviFlag();
}
//Add 2011/09/08 Z01yoneya End <--

JNIEXPORT jlong JNICALL JNI_Set_OutPutUsedtime_Ctrl
  (JNIEnv *env, jclass a, jlong jlctrl)
{
	OutPut_UsedTime_Ctrl  = jlctrl;
	return (jlong)0;
}
JNIEXPORT jlong JNICALL JNI_SetFilePathByType
  (JNIEnv *env, jclass clz,jlong jtype,jstring jstr)
{
       char* rtn = NULL;

       jclass clsstring = env->FindClass("java/lang/String");
       jstring strencode = env->NewStringUTF("utf-8");
       jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
       jbyteArray barr= (jbyteArray)env->CallObjectMethod(jstr, mid, strencode);
       jsize alen = env->GetArrayLength(barr);
       jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);
       if (alen > 0)
       {
                 rtn = new char[alen+1];
                 memcpy(rtn, ba, alen);
                 rtn[alen] = 0;
		 CT_SetFilePathByType(jtype, rtn);
       }

       env->ReleaseByteArrayElements(barr, ba, 0);

	env->DeleteLocalRef(strencode);
	 if(rtn)
	{
	        delete [] rtn;
		rtn = NULL;
	 }
       //LOGDp("[DEBUG]JNI_NE_UpdateGPSPortNo End!!!!!!!!!!!");

       return 0;
}
#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */
void MapRotate(BOOL b);
long DG_VS_SetVolume(float flvolume,float frvolume);
long DG_VS_SetVolumeScale(long lvolscale);
long DG_VS_GetVolumeScale();
long DG_VS_SetVolumeCtrl(long max, long min);
unsigned long	DG_ClearOrbisData();
unsigned long	DG_UpdateOrbisData(ulong ulnCount, ZDG_ORBIS_INFO* pastOrbisArray);
unsigned long	DG_SetOrbisSetting(ZDG_OrbisSettingInfo* pstDGOrbisSettingInfo);
int	Lib_calcDistancePointToPoint( ulong *Distance, GeoLocation_t *LocationA, GeoLocation_t *LocationB );
//int OPM_VICS_SendVICSData( char* pcAddr, unsigned long ulSize );
//unsigned long MP_DM_SetFriendCarInfo(ZMP_FRIEN_CAR_INFO* pstFriendCarInfo, int iCarCount);
#if defined(__cplusplus)
}
#endif /* __cplusplus */
void NE_SetDefaultVolumeScale(long lVolumeScale);

JNIEXPORT void JNICALL JNI_MapRotate(JNIEnv *env, jclass clz,jlong b)
{

	MapRotate((BOOL)b);
	return ;
}
JNIEXPORT void JNICALL JNI_SetVolume(JNIEnv *env, jclass clz,jlong lvolume,jlong rvolume)
{
	DG_VS_SetVolume( (float)lvolume, (float)rvolume);
	LOGDp("JNI_SetVolume lvolume:%d,rvolume:%d",(long)lvolume,(long)rvolume);
	return ;
}
JNIEXPORT void JNICALL JNI_SetVolumeScale(JNIEnv *env, jclass clz,jlong lvolscale)
{
	DG_VS_SetVolumeScale(lvolscale);
	LOGDp("JNI_SetVolume lvolscale:%d",lvolscale);
	return ;
}
JNIEXPORT void JNICALL JNI_GetVolumeScale(JNIEnv *env, jclass clz,jobject object)
{
	long lscale = DG_VS_GetVolumeScale();
	LOGDp("JNI_GetVolume lscale:%d",lscale);
	jclass claz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(claz)
	{
		//Get variable's ID in JNI
		jfieldID jvolumeScale = env->GetFieldID(claz,"lcount","J");
		if(jvolumeScale)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,jvolumeScale,(jlong)lscale);
		}
		else
		{
			LOGDp("[DEBUG] JNI_GetVolumeScale GetLongField jCarType Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_GetVolumeScale FindClass claz Failed");
	}
	return ;
}
JNIEXPORT void JNICALL JNI_SetVolumeCtrl(JNIEnv *env, jclass clz,jlong ldefault,jlong lmax,jlong lmin)
{
	NE_SetDefaultVolumeScale((long)ldefault);
	DG_VS_SetVolumeCtrl((long)lmax,(long)lmin);
//LOGDp("JNI_SetVolumeCtrl ldefault:%d,lmax:%d,lmin",(long)ldefault,(long)lmax,(long)lmin);
	return ;
}

JNIEXPORT jlong JNICALL JNI_NE_SetShowVehicleTrackFlag
  (JNIEnv *env, jclass clz,jlong showFlag)
{
	return (jlong)NE_SetShowVehicleTrackFlag((long)showFlag);
}

JNIEXPORT jlong JNICALL JNI_NE_ClearVehicleTrack
  (JNIEnv *env, jclass clz)
{
	return (jlong)NE_ClearVehicleTrack();
}

// Add 20151204 Start
JNIEXPORT jlong JNICALL JNI_NE_SaveVehicleTrack
  (JNIEnv *env, jclass clz)
{
	return (jlong)NE_SaveVehicleTrack();
}

JNIEXPORT jlong JNICALL JNI_NE_AgainOpenVehicleTrack
  (JNIEnv *env, jclass clz)
{
	return (jlong)NE_AgainOpenVehicleTrack();
}

JNIEXPORT jlong JNICALL JNI_NE_RestartOpenVehicleTrack(JNIEnv *env, jclass clz,jstring jstr)
{
	LOGDp("[DEBUG]JNI_NE_RestartOpenVehicleTrack Begin!!!!!!!!!!!");

	char	*pFilePath = NULL;

	jclass clsstring = env->FindClass("java/lang/String");
	jstring encode = env->NewStringUTF("utf-8");
	jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
	jbyteArray array = (jbyteArray)env->CallObjectMethod(jstr, mid, encode);
	jsize length = env->GetArrayLength(array);
	jbyte* buffer = env->GetByteArrayElements(array, JNI_FALSE);

	if (length > 0)
	{
		pFilePath = new char[length + 1];
		memcpy(pFilePath, buffer, length);
		pFilePath[length] = '\0';
		NE_RestartOpenVehicleTrack(pFilePath);
	}

	env->ReleaseByteArrayElements(array, buffer, 0);
	delete [] pFilePath;
	pFilePath = NULL;

	return 0;
}
// Add 20151204 End

// Add 20151224 Start
JNIEXPORT jlong JNICALL JNI_NE_OptionalVTFinalize
  (JNIEnv *env, jclass clz)
{
	return (jlong)NE_OptionalVTFinalize();
}


JNIEXPORT jlong JNICALL JNI_NE_VT_Initialize_Cust(JNIEnv *env, jclass clz, jstring jstr)
{
	LOGDp("[DEBUG]JNI_NE_VT_Initialize_Cust Begin!!!!!!!!!!!");

	char	*pFileName = NULL;

	jclass clsstring = env->FindClass("java/lang/String");
	jstring encode = env->NewStringUTF("utf-8");
	jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
	jbyteArray array = (jbyteArray)env->CallObjectMethod(jstr, mid, encode);
	jsize length = env->GetArrayLength(array);
	jbyte* buffer = env->GetByteArrayElements(array, JNI_FALSE);

	if (length > 0)
	{
		pFileName = new char[length + 1];
		memcpy(pFileName, buffer, length);
		pFileName[length] = '\0';
		NE_VT_Initialize_Cust(pFileName);
	}

	env->ReleaseByteArrayElements(array, buffer, 0);
	delete [] pFileName;
	pFileName = NULL;

	return 0;
}
// Add 20151224 End


JNIEXPORT jlong JNICALL JNI_NE_SetVehicleTrackParam
  (JNIEnv *env, jclass clz,jlong pixelFilter,jlong jumpNumber)
{
	return (jlong)NE_SetVehicleTrackParam((long)pixelFilter,(long)jumpNumber);
}

// Add by CPJsunagawa '2015-07-26 Start
JNIEXPORT jlong JNICALL JNI_NE_SetVehicleTrackCourse(JNIEnv *env, jclass clz,jstring jstr)
{
	LOGDp("[DEBUG]JNI_NE_SetVehicleTrackCourse Begin!!!!!!!!!!!");
// 	return (jlong)NE_SetVehicleTrackCourse((long)pixelFilter,(long)jumpNumber);

	char	*pCoursename = NULL;

	jclass clsstring = env->FindClass("java/lang/String");
	jstring encode = env->NewStringUTF("utf-8");
	jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
	jbyteArray array = (jbyteArray)env->CallObjectMethod(jstr, mid, encode);
	jsize length = env->GetArrayLength(array);
	jbyte* buffer = env->GetByteArrayElements(array, JNI_FALSE);

	if (length > 0)
	{
		pCoursename = new char[length + 1];
		memcpy(pCoursename, buffer, length);
		pCoursename[length] = '\0';
		NE_SetVehicleTrackCourse(pCoursename);
	}

	env->ReleaseByteArrayElements(array, buffer, 0);
	delete [] pCoursename;
	pCoursename = NULL;

	return 0;
}
// Add by CPJsunagawa '2015-07-26 End

// Add by CPJsunagawa '2015-08-03 Start
JNIEXPORT jlong JNICALL JNI_NE_SetShowOnlyVehicleTrackFlag
  (JNIEnv *env, jclass clz,jlong showFlag)
{
	return (jlong)NE_SetShowOnlyVehicleTrackFlag((long)showFlag);
}

JNIEXPORT jlong JNICALL JNI_NE_GetShowOnlyVehicleTrackFlag(JNIEnv *env, jclass clz,jobject object)
{
	long showFlag = 0;
	unsigned long ret = NE_GetShowOnlyVehicleTrackFlag(&showFlag);
	jclass claz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(claz)
	{
		//Get variable's ID in JNI
		jfieldID jvolumeScale = env->GetFieldID(claz,"lcount","J");
		if(jvolumeScale)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,jvolumeScale,(jlong)showFlag);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetShowOnlyVehicleTrackFlag GetLongField jCarType Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_GetShowOnlyVehicleTrackFlag FindClass claz Failed");
	}
	return (jlong)ret;
}
// Add by CPJsunagawa '2015-08-03 End

JNIEXPORT jlong JNICALL JNI_NE_GetShowVehicleTrackFlag(JNIEnv *env, jclass clz,jobject object)
{
	long showFlag = 0;
	unsigned long ret = NE_GetShowVehicleTrackFlag(&showFlag);
	jclass claz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(claz)
	{
		//Get variable's ID in JNI
		jfieldID jvolumeScale = env->GetFieldID(claz,"lcount","J");
		if(jvolumeScale)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,jvolumeScale,(jlong)showFlag);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetShowVehicleTrackFlag GetLongField jCarType Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_GetShowVehicleTrackFlag FindClass claz Failed");
	}
	return (jlong)ret;
}
JNIEXPORT jlong JNICALL JNI_VP_SwitchMapMatchingRoad
  (JNIEnv *env, jclass a)
{
	return (jlong)VP_SwitchMapMatchingRoad();
}
/*********************************************************************
*	Function Name	: JNI_VP_GetMovingStatus                         *
*	Description	:                                                    *
*	Date		: 09/12/25                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: Wangxp	                                         *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_VP_GetMovingStatus
(JNIEnv *env, jclass clz,jobject object)
{
	jlong	lRet = 0;

	ENE_MovingStatus eMovingStatus;
	VP_GetMovingStatus(&eMovingStatus);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jiState = env->GetFieldID(clazz,"m_iCommon","I");
		if(jiState)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,jiState,(jint)eMovingStatus);
		}
		else
		{
			LOGDp("[DEBUG] JNI_VP_GetMovingStatus SetIntField jiState Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_VP_GetMovingStatus FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_NE_SetMinorRoad       	                                          *
*	Description	:                                                                                        *
*	Date		: 09/11/21                                                                          *
*	Parameter	:                                                                                        *
*	Return Code	: jlong                                                                                 *
*	Author		: xiayx	                                                                            *
*  -------------------------------------------------------------------*
* Revision History	                                                                                         *
* No	Date		Revised by		Description                                               *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetMinorRoad
  (JNIEnv *env, jclass clz,jint OPsound)
{
	return (jlong)NE_SetMinorRoad(OPsound);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetMinorRoad                                                 *
*	Description	:                                                                                        *
*	Date		: 09/11/21                                                                          *
*	Parameter	:                                                                                        *
*	Return Code	: jlong                                                                                 *
*	Author		: xiayx	                                                                           *
*  ------------------------------------------------------------------*
* Revision History	                                                                                       *
* No	Date		Revised by		Description                                             *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetMinorRoad
  (JNIEnv *env, jclass clz,jobject object)
{
	int bOn;

	jlong lRet = (jlong)NE_GetMinorRoad(&bOn);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID IsOn = env->GetFieldID(clazz,"m_iCommon","I");
		if(IsOn)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,IsOn,bOn);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetOPSound GetFieldID IsOn Failed");
		}
	}

	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_NE_SetMinorRoad       	                                          *
*	Description	:                                                                                        *
*	Date		: 09/11/21                                                                          *
*	Parameter	:                                                                                        *
*	Return Code	: jlong                                                                                 *
*	Author		: xiayx	                                                                            *
*  -------------------------------------------------------------------*
* Revision History	                                                                                         *
* No	Date		Revised by		Description                                               *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetTollGuide
  (JNIEnv *env, jclass clz,jint OPsound)
{
	return (jlong)NE_SetTollGuide(OPsound);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetMinorRoad                                                 *
*	Description	:                                                                                        *
*	Date		: 09/11/21                                                                          *
*	Parameter	:                                                                                        *
*	Return Code	: jlong                                                                                 *
*	Author		: xiayx	                                                                           *
*  ------------------------------------------------------------------*
* Revision History	                                                                                       *
* No	Date		Revised by		Description                                             *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetTollGuide
  (JNIEnv *env, jclass clz,jobject object)
{
	int bOn;

	jlong lRet = (jlong)NE_GetTollGuide(&bOn);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID IsOn = env->GetFieldID(clazz,"m_iCommon","I");
		if(IsOn)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,IsOn,bOn);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetOPSound GetFieldID IsOn Failed");
		}
	}

	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_NE_SetMinorRoad       	                                          *
*	Description	:                                                                                        *
*	Date		: 09/11/21                                                                          *
*	Parameter	:                                                                                        *
*	Return Code	: jlong                                                                                 *
*	Author		: xiayx	                                                                            *
*  -------------------------------------------------------------------*
* Revision History	                                                                                         *
* No	Date		Revised by		Description                                               *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetOrbisGuide
  (JNIEnv *env, jclass clz,jint OPsound)
{
	return (jlong)NE_SetOrbisGuide(OPsound);
}

/*********************************************************************
*	Function Name	: JNI_NE_GetMinorRoad                                                 *
*	Description	:                                                                                        *
*	Date		: 09/11/21                                                                          *
*	Parameter	:                                                                                        *
*	Return Code	: jlong                                                                                 *
*	Author		: xiayx	                                                                           *
*  ------------------------------------------------------------------*
* Revision History	                                                                                       *
* No	Date		Revised by		Description                                             *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetOrbisGuide
  (JNIEnv *env, jclass clz,jobject object)
{
	int bOn;

	jlong lRet = (jlong)NE_GetOrbisGuide(&bOn);

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID IsOn = env->GetFieldID(clazz,"m_iCommon","I");
		if(IsOn)
		{
			//Set the variable's value  to Java
			env->SetIntField(object,IsOn,bOn);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetOPSound GetFieldID IsOn Failed");
		}
	}

	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_DG_ClearOrbisData                          *
*	Description	:                                                    *
*	Date		: 09/11/21                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_DG_ClearOrbisData
  (JNIEnv *env, jclass clz)
{
	return (jlong)DG_ClearOrbisData();
}
/*********************************************************************
*	Function Name	: JNI_DG_SetOrbisSetting                          *
*	Description	:                                                    *
*	Date		: 09/11/21                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_DG_SetOrbisSetting
  (JNIEnv *env, jclass clz,jobject object)
{
	ZDG_OrbisSettingInfo Record ={0};
	jclass jcls = env->GetObjectClass(object);
	if (jcls)
	{
		jfieldID jfid1 = env->GetFieldID( jcls, "unGeneralOrbisGuideDist", "S" );
		if (jfid1)
		{
			jlong value1 = env->GetLongField(object,jfid1);
			Record.unGeneralOrbisGuideDist = (long)value1;
		}
		jfieldID jfid2 = env->GetFieldID( jcls, "unHighwayOrbisGuideDist", "S" );
		if (jfid2)
		{
			jlong value2 = env->GetLongField(object,jfid2);
			Record.unHighwayOrbisGuideDist = (long)value2;
		}
		jfieldID jfid3 = env->GetFieldID( jcls, "unGeneralOrbisGuideAngle", "S" );
		if (jfid3)
		{
			jlong value3 = env->GetLongField(object,jfid3);
			Record.unGeneralOrbisGuideAngle = (long)value3;
		}
		jfieldID jfid4 = env->GetFieldID( jcls, "unHighwayOrbisGuideAngle", "S" );
		if (jfid4)
		{
			jlong value4 = env->GetLongField(object,jfid4);
			Record.unHighwayOrbisGuideAngle = (long)value4;
		}
	}else{
		LOGDp("[DEBUG]JNI_DG_SetOrbisSetting Error");
	}

	DG_SetOrbisSetting(&Record);

       //LOGDp("[DEBUG]JNI_NE_UpdateGPSPortNo End!!!!!!!!!!!");
       return 0;
}
/*********************************************************************
*	Function Name	: JNI_DG_SetOrbisSetting                          *
*	Description	:                                                    *
*	Date		: 09/11/21                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_DG_UpdateOrbisData
  (JNIEnv *env, jclass clz,jlong jl,jobjectArray args)
{
	ZDG_ORBIS_INFO *Record = NULL;
	 jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/ZDG_ORBIS_INFO");

	Record = (ZDG_ORBIS_INFO *)malloc(sizeof(ZDG_ORBIS_INFO)*jl);
	memset(Record, 0, sizeof(ZDG_ORBIS_INFO)*jl);

	 LOGDp("JNI_DG_UpdateOrbisData jl=[%d]", (int)jl);

	if(Record == NULL)
	{
	    LOGDp("JNI_DG_UpdateOrbisData malloc error!");
		return 0;
	}

	for(int i = 0; i < (int)jl; i++)
	{
		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);
	  	jfieldID jpcID = env->GetFieldID(clazz,"ulnID","Ljava/lang/String;");

		if(jpcID)
		{
			jstring jstr = (jstring)env->GetObjectField(obj,jpcID);
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応 START
			jsize alen = 0;
			jbyte* ba = NULL;
			
	       	jclass clsstring = env->FindClass(CLASS_CommonLib);
	       	jmethodID mid = env->GetStaticMethodID(clsstring, "getToSJIS", "(Ljava/lang/String;)[B");
		    jbyteArray barr= (jbyteArray)env->CallStaticObjectMethod(clsstring, mid, jstr);
			if( barr != NULL ){
			    alen = env->GetArrayLength(barr);
	       		ba = env->GetByteArrayElements(barr, JNI_FALSE);
				if (alen > 0){
		   			memcpy((Record+i)->szID, ba, min(sizeof((Record+i)->szID), (unsigned int)alen));
				}
       			env->ReleaseByteArrayElements(barr, ba, 0);
			}
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応  END
		}

		jfieldID jlnPosLatMilliSec = env->GetFieldID(clazz, "lnPosLatMilliSec", "J" );
		if(jlnPosLatMilliSec)
		{
			jlong jPosLatMilliSec = env->GetIntField(obj, jlnPosLatMilliSec);
			(Record+i)->lnPosLatMilliSec = (long)jPosLatMilliSec;
		}

		jfieldID jlnPosLonMilliSec = env->GetFieldID(clazz, "lnPosLonMilliSec", "J" );
		if(jlnPosLonMilliSec)
		{
			jlong jPosLonMilliSec = env->GetIntField(obj, jlnPosLonMilliSec);
			(Record+i)->lnPosLonMilliSec = (long)jPosLonMilliSec;
		}

//Add 2011/09/19 Z01sakamoto Start -->
		jfieldID jbOnHighway = env->GetFieldID(clazz, "bHighway", "Z");
		if(jbOnHighway) {
			jboolean jOnHighway = env->GetBooleanField(obj, jbOnHighway);
			(Record+i)->bOnHighway = (BOOL)jOnHighway;
		}
//Add 2011/09/19 Z01sakamoto End <--
	}

       DG_UpdateOrbisData((long)jl, Record);

      if( Record != NULL){
	  	free(Record);
		Record = NULL;
      	}

       return 0;
}
/*********************************************************************
*	Function Name	: JNI_Lib_calcDistancePointToPoint               *
*	Description	:                                                    *
*	Date		: 09/11/21                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_Lib_calcDistancePointToPoint
(JNIEnv *env, jclass clz,jlong jlon1,jlong jlat1,jlong jlon2,jlong jlat2,jobject object)
{
	unsigned long lDistance = 0;
	GeoLocation_t a;
	GeoLocation_t b;

	a.Longitude = (long)jlon1;
	a.Latitude = (long)jlat1;

	LOGDp("[DEBUG] @@@start Lon = %d", a.Longitude );
	LOGDp("[DEBUG] @@@start Lat = %d", a.Latitude );

	b.Longitude = (long)jlon2;
	b.Latitude = (long)jlat2;
	LOGDp("[DEBUG] @@@End Lon = %d", b.Longitude );
	LOGDp("[DEBUG] @@@End Lat = %d", b.Latitude );

	jlong lRet = (jlong)Lib_calcDistancePointToPoint(&lDistance, &a, &b);

	LOGDp("[DEBUG] @@@lDistance = %d", lDistance );

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID IsOn = env->GetFieldID(clazz,"lcount","J");
		if(IsOn)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,IsOn,(jlong)lDistance);
		}
		else
		{
			LOGDp("[DEBUG] JNI_Lib_calcDistancePointToPoint GetFieldID IsOn Failed");
		}
	}

	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_CT_SCM_ChangeSettingInfo                   *
*	Description	:                                                    *
*	Date		: 09/11/21                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_SCM_ChangeSettingInfo
  (JNIEnv *env, jclass clz, jlong jl, jobjectArray args)
{
	ZGRL_SETTING_INFORMATION Record;

	memset(&Record, 0, sizeof(Record));
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/ZGRL_SIZE");

	for(int i = 0; i < (int)jl; i++)
	{
		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);
		switch(i){
			case 0:
				{
				jfieldID junWidth = env->GetFieldID(clazz, "unWidth", "S" );
				if(junWidth)
				{
					jshort lnID = env->GetShortField(obj,junWidth);
					Record.stTopPlaneSize.unWidth = lnID;
				} else {
					LOGDp("[DEBUG]junWidth 0");
				}

				jfieldID junHigth = env->GetFieldID(clazz, "unHigth", "S" );
				if(junHigth)
				{
					jshort jPosLatMilliSec = env->GetShortField(obj, junHigth);
					Record.stTopPlaneSize.unHigth= jPosLatMilliSec;
				} else {
					LOGDp("[DEBUG]junHigth 0");
				}
				break;
				}
			case 1:
				{
				jfieldID junWidth = env->GetFieldID(clazz, "unWidth", "S" );
				if(junWidth)
				{
					jshort lnID = env->GetShortField(obj,junWidth);
					Record.stGuidePlaneSize.unWidth = lnID;
				}else {
					LOGDp("[DEBUG]junWidth 1");
				}

				jfieldID junHigth = env->GetFieldID(clazz, "unHigth", "S" );
				if(junHigth)
				{
					jshort jPosLatMilliSec = env->GetShortField(obj, junHigth);
					Record.stGuidePlaneSize.unHigth= jPosLatMilliSec;
				}else {
					LOGDp("[DEBUG]junHigth 1");
				}
				break;
				}
			case 2:
				{
				jfieldID junWidth = env->GetFieldID(clazz, "unWidth", "S" );
				if(junWidth)
				{
					jshort lnID = env->GetShortField(obj,junWidth);
					Record.stMyPlaneSize.unWidth = lnID;
				}else {
					LOGDp("[DEBUG]junWidth 2");
				}

				jfieldID junHigth = env->GetFieldID(clazz, "unHigth", "S" );
				if(junHigth)
				{
					jshort jPosLatMilliSec = env->GetShortField(obj, junHigth);
					Record.stMyPlaneSize.unHigth= jPosLatMilliSec;
				}else {
					LOGDp("[DEBUG]junHigth 2");
				}
				break;
				}
			case 3:
				{
				jfieldID junWidth = env->GetFieldID(clazz, "unWidth", "S" );
				if(junWidth)
				{
					jshort lnID = env->GetShortField(obj,junWidth);
					Record.stMap1PlaneSize.unWidth = lnID;
				}else {
					LOGDp("[DEBUG]junWidth 3");
				}

				jfieldID junHigth = env->GetFieldID(clazz, "unHigth", "S" );
				if(junHigth)
				{
					jshort jPosLatMilliSec = env->GetShortField(obj, junHigth);
					Record.stMap1PlaneSize.unHigth= jPosLatMilliSec;
				}else {
					LOGDp("[DEBUG]junHigth 3");
				}
				break;
				}
		 	case 4:
				{
				jfieldID junWidth = env->GetFieldID(clazz, "unWidth", "S" );
				if(junWidth)
				{
					jshort lnID = env->GetShortField(obj,junWidth);
					Record.stMap2PlaneSize.unWidth = lnID;
				}else {
					LOGDp("[DEBUG]junWidth 4");
				}

				jfieldID junHigth = env->GetFieldID(clazz, "unHigth", "S" );
				if(junHigth)
				{
					jshort jPosLatMilliSec = env->GetShortField(obj, junHigth);
					Record.stMap2PlaneSize.unHigth= jPosLatMilliSec;
				}else {
					LOGDp("[DEBUG]junHigth 4");
				}
				break;
		 		}
			default:
				break;
		}
	}
	LOGDp("[DEBUG] Record.stMap2PlaneSize.unWidth = %d", Record.stTopPlaneSize.unWidth);
	LOGDp("[DEBUG] Record.stMap2PlaneSize.unHigth = %d", Record.stTopPlaneSize.unHigth);

	LOGDp("[DEBUG] Record.stMap2PlaneSize.unWidth = %d", Record.stGuidePlaneSize.unWidth);
	LOGDp("[DEBUG] Record.stMap2PlaneSize.unHigth = %d", Record.stGuidePlaneSize.unHigth);

	LOGDp("[DEBUG] Record.stMap2PlaneSize.unWidth = %d", Record.stMyPlaneSize.unWidth);
	LOGDp("[DEBUG] Record.stMap2PlaneSize.unHigth = %d", Record.stMyPlaneSize.unHigth);

	LOGDp("[DEBUG] Record.stMap2PlaneSize.unWidth = %d", Record.stMap1PlaneSize.unWidth);
	LOGDp("[DEBUG] Record.stMap2PlaneSize.unHigth = %d", Record.stMap1PlaneSize.unHigth);

	LOGDp("[DEBUG] Record.stMap2PlaneSize.unWidth = %d", Record.stMap2PlaneSize.unWidth);
	LOGDp("[DEBUG] Record.stMap2PlaneSize.unHigth = %d", Record.stMap2PlaneSize.unHigth);

       CT_SCM_ChangeSettingInfo(Record);

       return 0;
}

// Add 2011/03/29 Z01ONOZAWA Start -->
JNIEXPORT void JNICALL JNI_VP_EnableSensor(JNIEnv *env, jclass clz,jint jenableSensor)
{
	VP_EnableSensor(jenableSensor == 1 ? TRUE : FALSE);
	return ;
}
// Add 2011/03/29 Z01ONOZAWA End   <--

// Add 2011/02/17 Z01ONOZAWA Start --> MMログ出力対応
// MMログファイル名称設定
JNIEXPORT jlong JNICALL JNI_VP_SetMMLogFilename(JNIEnv *env, jclass clz, jstring jstr)
{
	char	*pFilename = NULL;

	jclass clsstring = env->FindClass("java/lang/String");
	jstring encode = env->NewStringUTF("utf-8");
	jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
	jbyteArray array = (jbyteArray)env->CallObjectMethod(jstr, mid, encode);
	jsize length = env->GetArrayLength(array);
	jbyte* buffer = env->GetByteArrayElements(array, JNI_FALSE);

	if (length > 0)
	{
		pFilename = new char[length + 1];
		memcpy(pFilename, buffer, length);
		pFilename[length] = '\0';
		VP_SetMMLogFilename(pFilename);
	}

	env->ReleaseByteArrayElements(array, buffer, 0);
	delete [] pFilename;
	pFilename = NULL;

	return 0;
}
// Add 2011/02/17 Z01ONOZAWA End   <--

//Add 2011/06/21 Z01thedoanh Start -->
// VPSetting名称設定
JNIEXPORT jlong JNICALL JNI_VP_SetVPSettingFilename(JNIEnv *env, jclass clz, jstring jstr)
{
	char	*pFilename = NULL;

	jclass clsstring = env->FindClass("java/lang/String");
	jstring encode = env->NewStringUTF("utf-8");
	jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
	jbyteArray array = (jbyteArray)env->CallObjectMethod(jstr, mid, encode);
	jsize length = env->GetArrayLength(array);
	jbyte* buffer = env->GetByteArrayElements(array, JNI_FALSE);

	if (length > 0)
	{
		pFilename = new char[length + 1];
		memcpy(pFilename, buffer, length);
		pFilename[length] = '\0';
		VP_SetVPSettingFilename(pFilename);
	}

	env->ReleaseByteArrayElements(array, buffer, 0);
	delete [] pFilename;
	pFilename = NULL;

	return 0;
}

//Add 2011/06/21 Z01thedoanh End <--
//Add 2011/07/01 Z01thedoanh Start -->
JNIEXPORT void JNICALL JNI_CT_setBIsExistsDetailMapData(JNIEnv *env, jclass clz, jboolean jflag)
{
	CT_setBIsExistsDetailMapData(jflag);
}
//Add 2011/07/01 Z01thedoanh End <--
// Debug用 by Z01ONOZAWA
#ifdef __cplusplus
extern "C" {
#endif
#include "VP/VPResource.h"
#ifdef __cplusplus
}
#endif

JNIEXPORT jlong JNICALL JNI_VP_GetCurrentSpeed(JNIEnv *env, jclass clz, jobject object)
{
	ZVP_Position_t		stLastPosition;

	memset(&stLastPosition, 0, sizeof (ZVP_Position_t));
	if (VP_R_GetLastPosition(VP_Position_LAST, &stLastPosition) == VP_SUCCESS)
	{
		int iKind = (int)stLastPosition.eKind;
		jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIInt");

		if (clazz)
		{
			//Get variable's ID in JNI
			jfieldID jiKind = env->GetFieldID(clazz,"m_iCommon","I");
			if (jiKind)
			{
				env->SetIntField(object, jiKind, (jint)iKind);
			}
		}
// Chg 2011/04/01 Z01ONOZAWA Start -->
//		return (jlong)stLastPosition.ulnSpeed;
		return (jlong)stLastPosition.fSpeedKPH;
// Chg 2011/04/01 Z01ONOZAWA End   <--
	} else
	{
		return (-1);
	}
}
/* ●V2.5対応 2013/ 4/22 IT-Wata */
/*********************************************************************
*	Function Name	: JNI_VP_GPSNMEA_String
*	Description	: 1行にまとめた Time+NMEAをDRに送る
*                 MyNmeaListener.java(GPS) --> Native(DR)
*	Date		: 2013.04.22
*	Parameter	:
*	Return Code	: jlong
*	Author		: 
*  ------------------------------------------------------------------*
* Revision History
* No	Date		Revised by		Description
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_VP_GPSNMEA_String
  (JNIEnv *env, jclass clz, jstring nmea_f)
{
	jlong Ret = 0;
	char *pNMEAStr = NULL;

	jclass clsstring = env->FindClass("java/lang/String");
	jstring encode = env->NewStringUTF("utf-8");
	jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
	jbyteArray array= (jbyteArray)env->CallObjectMethod(nmea_f, mid, encode);
	jsize length = env->GetArrayLength(array);
	jbyte* buffer = env->GetByteArrayElements(array, JNI_FALSE);

	if (length > 0)
	{
		pNMEAStr = new char[length+1];
		memcpy(pNMEAStr, buffer, length);
		pNMEAStr[length] = 0;

		/* Time+GPSNMEA文字を分解し、評価結果をテーブルにまとめる */
		Ret = VP_GPSNMEA_String(pNMEAStr);
	}

	env->ReleaseByteArrayElements(array, buffer, 0);
	delete [] pNMEAStr;
	pNMEAStr = NULL;

	return Ret;
}
/*********************************************************************
*	Function Name	: JNI_VP_SetEventTcnt
*	Description	: 1行にまとめた Time+NMEAをDRに送る
*                 MyNmeaListener.java(GPS) --> Native(DR)
*	Date		: 2013.04.22
*	Parameter	:
*	Return Code	: jlong
*	Author		: 
*  ------------------------------------------------------------------*
* Revision History
* No	Date		Revised by		Description
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_VP_SetEventTcnt
  (JNIEnv *env, jclass clz,jint acctcnt,jint gyrotcnt)
{
	VP_SetEventTcnt((int)acctcnt,(int)gyrotcnt);
	return 0;
}
/*********************************************************************
*	Function Name	: JNI_VP_GetMessgeString
*	Description	: DR情報を文字列として、出力
*                 Native(DR) --> 地図画面の住所表示
*	Date		: 2012.01.19
*	Parameter	: DistanceKm : アスキー文字

*	Return Code	: jlong
*	Author		:
*   Ret         : VP_SUCCESS 設定完了
*  ------------------------------------------------------------------*
* Revision History
* No	Date		Revised by		Description
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_VP_GetDRMessageString
(JNIEnv *env, jclass clz,jobject object)
{
	char* pDRMessage = 0;
	const char test[] = "DR Wait..";
	jfieldID str1 = 0;
	jlong lRet = (jlong)VP_GetDRMessageString(&pDRMessage);

	if(pDRMessage == 0)
	{
		/* DRスレッド起動までここを通る */
		LOGDp("[DEBUG] JNI_VP_GetDRMessageString ERROR --NULL--");
		pDRMessage = (char *)test;
	}
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIString");
	if (clazz)
	{
		/* ＡｄｄｒｅｓｓＳｔｒｉｎｇ　で送ります */
		str1 = env->GetFieldID(clazz,"m_StrAddressString","Ljava/lang/String;");
	}
	if (str1)
	{
		env->SetObjectField(object,str1,env->NewStringUTF(pDRMessage));
	}
	return lRet;
}
/*********************************************************************
*	Function Name	: JNI_VP_clear_MMData                            *
*	Description	:                                                    *
*	Date		: 2013/05/09                                         *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_VP_clear_MMData
  (JNIEnv *env, jclass clz)
{
	return (jlong)VP_clear_MMData();
}

/*********************************************************************
*	Function Name	: JNI_VP_DRStopGo_Status                         *
*	Description	: DR(ACC)学習完了で自車の停止状態を戻す              *
*	Date		: 2013/05/21                                         *
*	Parameter	:                                                    *
*	Return Code	: jlong  !=0 : Stop ,, = 0 Go or 未判定              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_VP_DRStopGo_Status
  (JNIEnv *env, jclass clz)
{
	return (jlong)VP_DRStopGo_Status();
}
/*********************************************************************
*	Function Name	: JNI_VP_DR_Status                               *
*	Description	: DR動作状態を戻す                                   *
*	Date		: 2013/06/12                                         *
*	Parameter	:                                                    *
*	Return Code	: jlong  0:未動作 1:動作可能(学習完了) 2:動作中(DR走行)  *
*	Author		: 動作可能は、端末異常、手持ちなどを含む状態         *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_VP_DR_Status
  (JNIEnv *env, jclass clz)
{
	return (jlong)VP_DR_Status();
}

// Exconfig.ini 指定のProFile名称設定
JNIEXPORT jlong JNICALL JNI_VP_SetDRprofileFilename(JNIEnv *env, jclass clz, jstring jstr)
{
	char	*pFilename = NULL;

	jclass clsstring = env->FindClass("java/lang/String");
	jstring encode = env->NewStringUTF("utf-8");
	jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
	jbyteArray array = (jbyteArray)env->CallObjectMethod(jstr, mid, encode);
	jsize length = env->GetArrayLength(array);
	jbyte* buffer = env->GetByteArrayElements(array, JNI_FALSE);

	if (length > 0)
	{
		pFilename = new char[length + 1];
		memcpy(pFilename, buffer, length);
		pFilename[length] = '\0';
		VP_SetDRprofileFilename(pFilename);
	}

	env->ReleaseByteArrayElements(array, buffer, 0);
	delete [] pFilename;
	pFilename = NULL;

	return 0;
}
// Exconfig.ini 指定のProFileパス設定
JNIEXPORT jlong JNICALL JNI_VP_SetDRprofilePath(JNIEnv *env, jclass clz, jstring jstr)
{
	char	*pFilepath = NULL;

	jclass clsstring = env->FindClass("java/lang/String");
	jstring encode = env->NewStringUTF("utf-8");
	jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
	jbyteArray array = (jbyteArray)env->CallObjectMethod(jstr, mid, encode);
	jsize length = env->GetArrayLength(array);
	jbyte* buffer = env->GetByteArrayElements(array, JNI_FALSE);

	if (length > 0)
	{
		pFilepath = new char[length + 1];
		memcpy(pFilepath, buffer, length);
		pFilepath[length] = '\0';
		VP_SetDRprofilePath(pFilepath);
	}

	env->ReleaseByteArrayElements(array, buffer, 0);
	delete [] pFilepath;
	pFilepath = NULL;

	return 0;
}

// Exconfig 取得完了を DRへ通知する
JNIEXPORT jlong JNICALL JNI_VP_Exconfig_Call
  (JNIEnv *env, jclass clz)
{
	return (jlong)VP_Exconfig_Call();
}

// ProFile 設定完了を Java通知する(戻り値)
JNIEXPORT jlong JNICALL JNI_VP_ProFile_Back
  (JNIEnv *env, jclass clz)
{
	return (jlong)VP_ProFile_Back();
}

// DR情報(道路勾配情報)を文字列として、出力
JNIEXPORT jlong JNICALL JNI_VP_GetAngleString
(JNIEnv *env, jclass clz,jobject object)
{
	char* pDRMessage = 0;
	const char test[] = "---";
	jfieldID str1 = 0;
	jlong lRet = (jlong)VP_GetAngleString(&pDRMessage);

	if(pDRMessage == 0)
	{
		pDRMessage = (char *)test;
	}
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIString");
	if (clazz)
	{
		/* Stringで送ります */
		str1 = env->GetFieldID(clazz,"m_StrAddressString","Ljava/lang/String;");
	}
	if (str1)
	{
		env->SetObjectField(object,str1,env->NewStringUTF(pDRMessage));
	}
	return lRet;
}

// DR情報(自車停発車情報)を文字列として、出力
JNIEXPORT jlong JNICALL JNI_VP_GetMoveString
(JNIEnv *env, jclass clz,jobject object)
{
	char* pDRMessage = 0;
	const char test[] = "---";
	jfieldID str1 = 0;
	jlong lRet = (jlong)VP_GetMoveString(&pDRMessage);

	if(pDRMessage == 0)
	{
		pDRMessage = (char *)test;
	}
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIString");
	if (clazz)
	{
		/* Stringで送ります */
		str1 = env->GetFieldID(clazz,"m_StrAddressString","Ljava/lang/String;");
	}
	if (str1)
	{
		env->SetObjectField(object,str1,env->NewStringUTF(pDRMessage));
	}
	return lRet;
}

// DR情報(右左折情報)を文字列として、出力
JNIEXPORT jlong JNICALL JNI_VP_GetCurveString
(JNIEnv *env, jclass clz,jobject object)
{
	char* pDRMessage = 0;
	const char test[] = "---";
	jfieldID str1 = 0;
	jlong lRet = (jlong)VP_GetCurveString(&pDRMessage);

	if(pDRMessage == 0)
	{
		pDRMessage = (char *)test;
	}
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIString");
	if (clazz)
	{
		/* Stringで送ります */
		str1 = env->GetFieldID(clazz,"m_StrAddressString","Ljava/lang/String;");
	}
	if (str1)
	{
		env->SetObjectField(object,str1,env->NewStringUTF(pDRMessage));
	}
	return lRet;
}

// 案内板表示、非表示制御
JNIEXPORT void JNICALL JNI_NE_setIsGuide(JNIEnv *env, jclass clz, jint bIsGuide)
{
	NE_setIsGuide((BOOL)bIsGuide);
}

JNIEXPORT jint JNICALL JNI_NE_getIsGuide(JNIEnv *env, jclass clz)
{
	return (jint)NE_getIsGuide();
}
