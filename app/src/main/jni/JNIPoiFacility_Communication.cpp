/********************************************************************************
* File Name	:	 JNIPoiFacility_Communication.cpp
* Created	:	2009/10/26
* Author	:	xiayx
* Model		:
* Description	:
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "JNIPoiFacility_Communication.h"
#include "NaviEngineIF.h"
#include "NaviEngineCtrl.h"
#include "NaviEngine/CT/CTUICommand.h"
#include "NETypes.h"
#include "windows.h"
#include "NaviEngine/POI/POIStruct.h"
#include "jni/JNI_Common.h"
#include "CTICU.h"
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define LOG_TAG "NavigationJNI_NaviRun"
#define JNI_DEBUG

#include "debugPrint.h"
/*********************************************************************
*	Function Name	: JNI_NE_POI_Facility_SearchRefine         	     *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_SearchRefine
  (JNIEnv *env, jclass cls, jlong jlnCount)
{
	return (jlong)NE_POI_Facility_SearchRefine((long)jlnCount);
}
/*********************************************************************
*	Function Name	: JNI_NE_POIFacility_GetRecCount              *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_GetPrefuctureRecListCount
  (JNIEnv *env, jclass clz,jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefuctureRecListCount begin!!!!!!!!!!!!");

	long Listconut = 0;

	jlong lRet = (jlong)NE_POI_Facility_GetPrefuctureRecListCount(&Listconut);

	LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefuctureRecListCount Listconut:%d,lRet:%d!!!!!!!!!!!!",Listconut,lRet);

	jlong listconut = Listconut;
	LOGDp("[DEBUG]listconut is : listconut = %d", listconut);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefuctureRecListCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefuctureRecListCount GetLongField Count Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefuctureRecListCount FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POI_Facility_GetPrefectureRecList              *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_GetPrefectureRecList
  (JNIEnv *env, jclass clz,jlong RecIndex,jlong RecCount,jobjectArray args,jobject object)
{
	ZPOI_FACILITY_LISTITEM *pstRecs;
	long GetCount = 0;

	jlong Ret = (jlong)NE_POI_Facility_GetPrefectureRecList(RecIndex,RecCount,&pstRecs,&GetCount);

	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount Ret:%d!!!!!!!!!!!!",Ret);
	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount GetCount:%d!!!!!!!!!!!!",GetCount);

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
		//LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList GetMethodID Sucessful");
	}

	//LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList Sucessful");

	if(cls)
	{
		//LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList SetLongField cls Enter");
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList SetLongField Count Begin");
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)GetCount);
			//LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList SetLongField Count Sucessful");
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Facility_ListItem");
#if 0
	if(clazz)
	{
		LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList FindClass Sucessful!!!!!!!!!!!!!!");
	}
#endif
	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < GetCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			//LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList NameSize:%d!!!!!!!!!!!!",pstRecs[i].lnNameSize);
			jfieldID NameSize = env->GetFieldID(clazz,"m_lNameSize","J");
			if(NameSize)
			{
				env->SetLongField(obj,NameSize,pstRecs[i].lnNameSize);
				jfieldID Name = env->GetFieldID(clazz,"m_Name","Ljava/lang/String;");
				if(Name)
				{
					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].pszName));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].pszName), (jbyte*)pstRecs[i].pszName);

					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						env->SetObjectField(obj,Name,jsName);
					}

					jfieldID Latitude = env->GetFieldID(clazz,"m_lLatitude","J");

					if(Latitude)
					{
						env->SetLongField(obj,Latitude,pstRecs[i].ulnLatitude);
						jfieldID Longitude = env->GetFieldID(clazz,"m_lLongitude","J");
						if(Longitude)
						{
							env->SetLongField(obj,Longitude,pstRecs[i].ulnLongitude);
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POI_Facility_GetPrefectureRecList GetFieldID Longitude Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList GetFieldID Latitude Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList GetFieldID Name Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefectureRecList GetFieldID NameSize Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POI_Facility_GetPrefectureRecList FindClass Failed!");
		}
	}

	return Ret;
}


/*********************************************************************
*	Function Name	: JNI_NE_POIFacility_Clear         	     *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIFacility_Clear
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POIFacility_Clear();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIFacility_SearchList              *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIFacility_SearchList
  (JNIEnv *env, jclass clz,jstring jstr)
{
      //LOGDp("[DEBUG]JNI_NE_POIFacility_SearchList Begin!!!!!!!!!!!");
       char* rtn = NULL;
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応 START
    jsize alen = 0;
	jbyte* ba = NULL;
	jclass clsstring = env->FindClass(CLASS_CommonLib);
	jmethodID mid = env->GetStaticMethodID(clsstring, "getToSJIS", "(Ljava/lang/String;)[B");
	jbyteArray barr= (jbyteArray)env->CallStaticObjectMethod(clsstring, mid, jstr);
	if( barr != NULL ){
		alen = env->GetArrayLength(barr);
		ba = env->GetByteArrayElements(barr, JNI_FALSE);
       	if (alen > 0)
       	{
                 rtn = new char[alen+1];
                 memcpy(rtn, ba, alen);
                 rtn[alen] = 0;
       	}
	}
       //LOGDp("[DEBUG] JNI_NE_POIFacility_SearchList rtn[0]:%x!!!!!!!!!!!!",rtn[0]);
       //LOGDp("[DEBUG] JNI_NE_POIFacility_SearchList rtn[1]:%x!!!!!!!!!!!!",rtn[1]);
       //LOGDp("[DEBUG] JNI_NE_POIFacility_SearchList rtn[2]:%x!!!!!!!!!!!!",rtn[2]);
      //LOGDp("[DEBUG] JNI_NE_POIFacility_SearchList rtn[3]:%x!!!!!!!!!!!!",rtn[3]);
       	jlong Ret = NE_POIFacility_SearchList(rtn);
       //LOGDp("[DEBUG] JNI_NE_POIFacility_SearchList rtn:%s!!!!!!!!!!!!",rtn);
	if( barr != NULL ){
       	env->ReleaseByteArrayElements(barr, ba, 0);
	}
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応  END
	if(rtn)
	{
		delete[] rtn;
		rtn = NULL;
	}

       //LOGDp("[DEBUG]JNI_NE_POIFacility_SearchList End!!!!!!!!!!!");

      return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIFacility_GetRecCount         *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                         *
* No	Date		Revised by		Description              *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIFacility_GetRecCount
  (JNIEnv *env, jclass clz,jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount begin!!!!!!!!!!!!");

	unsigned long Listconut = 0;

	jlong lRet = (jlong)NE_POIFacility_GetRecCount(&Listconut);

	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount Listconut:%d,lRet:%d!!!!!!!!!!!!",Listconut,lRet);

	jlong listconut = Listconut;
	//LOGE("[DEBUG]listconut is : listconut = %d", listconut);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount GetLongField Count Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIFacility_GetMatchCount       *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                         *
* No	Date		Revised by		Description              *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIFacility_GetMatchCount
  (JNIEnv *env, jclass clz,jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetMatchCount begin!!!!!!!!!!!!");

	unsigned long Listconut = 0;

	jlong lRet = (jlong)NE_POIFacility_GetMatchCount(&Listconut);

	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetMatchCount Listconut:%d,lRet:%d!!!!!!!!!!!!",Listconut,lRet);

	jlong listconut = Listconut;
	//LOGE("[DEBUG]listconut is : listconut = %d", listconut);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetMatchCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIFacility_GetMatchCount GetLongField Count Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIFacility_GetMatchCount FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIFacility_GetRecList          *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                         *
* No	Date		Revised by		Description              *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIFacility_GetRecList
  (JNIEnv *env, jclass clz,jlong RecIndex,jlong RecCount,jobjectArray args,jobject object)
{
	ZPOI_FACILITY_LISTITEM *pstRecs;
	long GetCount = 0;

	jlong Ret = (jlong)NE_POIFacility_GetRecList(RecIndex,RecCount,&pstRecs,&GetCount);

	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount Ret:%d!!!!!!!!!!!!",Ret);
	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount GetCount:%d!!!!!!!!!!!!",GetCount);

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
		//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList GetMethodID Sucessful");
	}

	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList Sucessful");

	if(cls)
	{
		//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList SetLongField cls Enter");
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList SetLongField Count Begin");
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)GetCount);
			//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList SetLongField Count Sucessful");
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Facility_ListItem");
#if 0
	if(clazz)
	{
		LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList FindClass Sucessful!!!!!!!!!!!!!!");
	}
#endif
	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < GetCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList NameSize:%d!!!!!!!!!!!!",pstRecs[i].lnNameSize);
			jfieldID NameSize = env->GetFieldID(clazz,"m_lNameSize","J");
			if(NameSize)
			{
				env->SetLongField(obj,NameSize,pstRecs[i].lnNameSize);
				jfieldID Name = env->GetFieldID(clazz,"m_Name","Ljava/lang/String;");
				if(Name)
				{
					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].pszName));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].pszName), (jbyte*)pstRecs[i].pszName);

					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						env->SetObjectField(obj,Name,jsName);
					}

					if(NULL != bytes)
					{
						env->DeleteLocalRef( bytes);
						bytes = NULL;
					}
					if(NULL != encoding)
					{
						env->DeleteLocalRef( encoding);
						encoding = NULL;
					}
					if(NULL != jsName)
					{
						env->DeleteLocalRef( jsName);
						jsName = NULL;
					}

					jfieldID Latitude = env->GetFieldID(clazz,"m_lLatitude","J");

					if(Latitude)
					{
						env->SetLongField(obj,Latitude,pstRecs[i].ulnLatitude);
						jfieldID Longitude = env->GetFieldID(clazz,"m_lLongitude","J");
						if(Longitude)
						{
							env->SetLongField(obj,Longitude,pstRecs[i].ulnLongitude);
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POIFacility_GetRecList GetFieldID Longitude Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList GetFieldID Latitude Failed");
					}
					//
					jfieldID jDistance = env->GetFieldID(clazz,"m_lDistance","J");
					if(jDistance)
					{
						env->SetLongField(obj,jDistance,pstRecs[i].ulnDistance);
					}
					else
					{
						LOGDp("[DEBUG] JNICALL JNI_NE_POIFacility_GetRecList GetFieldID m_lDistance Failed");
					}
					//
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList GetFieldID Name Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList GetFieldID NameSize Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POIFacility_GetRecList FindClass Failed!");
		}
		if(NULL != obj)
		{
			env->DeleteLocalRef( obj);
			obj = NULL;
		}
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIFacility_Cancel                  *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIFacility_Cancel
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POIFacility_Cancel();
}

/*********************************************************************
*	Function Name	: JNI_NE_POI_Facility_GetMypositon              *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_GetMypositon
  (JNIEnv *env, jclass clz,jlong lnHierarchyRelative,jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount begin!!!!!!!!!!!!");

	long Listconut = 0;

	jlong lRet = (jlong)NE_POI_Facility_GetMypositon((long)lnHierarchyRelative,&Listconut);

	LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount Listconut:%d,lRet:%d!!!!!!!!!!!!",Listconut,lRet);

	jlong listconut = Listconut;
	//LOGE("[DEBUG]listconut is : listconut = %d", listconut);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POI_Facility_GetMypositon GetLongField Count Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POI_Facility_GetMypositon FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POI_Facility_SetAddressKeyCode                  *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_SetAddressKeyCode
  (JNIEnv *env, jclass cls,jshort unWideCode,jshort unMiddleCode,jshort unNarrowCode)
{
	return (jlong)NE_POI_Facility_SetAddressKeyCode((unsigned short)unWideCode, (unsigned short)unMiddleCode, (unsigned short)unNarrowCode);
}

/*********************************************************************
*	Function Name	: JNI_NE_POI_Facility_GetPointList                  *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_GetPointList
  (JNIEnv *env, jclass cls,jlong lnRecIndex,jlong lnHierarchyRelative)
{
    return (jlong)NE_POI_Facility_GetPointList((long)lnRecIndex,(long)lnHierarchyRelative);
}

/*********************************************************************
*	Function Name	: JNI_POI_Facility_GetPointListCount             *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                         *
* No	Date		Revised by		Description              *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_GetPointListCount
  (JNIEnv *env, jclass a, jlong lnHierarchyRelative, jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetPrevTreeIndex begin");
	long lnRecCount = 0;

	//jlong lRet =

	(jlong)NE_POI_Facility_GetPointListCount(lnHierarchyRelative, &lnRecCount);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID TreeIndex = env->GetFieldID(clazz,"lcount","J");
		if(TreeIndex)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,TreeIndex,lnRecCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_POI_Facility_GetPointListCount SetLongField TreeIndex Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_POI_Facility_GetPointListCount FindClass  Failed");
	}

	return 0;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIFacility_GetRecList          *
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                         *
* No	Date		Revised by		Description              *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_GetPointRecList
  (JNIEnv *env, jclass clz,jlong RecIndex,jlong RecCount,jlong lnHierarchyRelative,jobjectArray args,jobject object)
{
	ZPOI_FACILITY_LISTITEM *pstRecs;
	long GetCount = 0;

	jlong Ret = 0;

	Ret = (jlong)NE_POI_Facility_GetPointRecList(RecIndex,RecCount,lnHierarchyRelative,&pstRecs,&GetCount);

	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount Ret:%d!!!!!!!!!!!!",Ret);
	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecCount GetCount:%d!!!!!!!!!!!!",GetCount);

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	jmethodID ctorID =0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
		//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList GetMethodID Sucessful");
	}

	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList Sucessful");

	if(cls)
	{
		//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList SetLongField cls Enter");
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList SetLongField Count Begin");
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)GetCount);
			//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList SetLongField Count Sucessful");
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPointRecList GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPointRecList FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Facility_ListItem");
#if 0
	if(clazz)
	{
		LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList FindClass Sucessful!!!!!!!!!!!!!!");
	}
#endif
	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < GetCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList NameSize:%d!!!!!!!!!!!!",pstRecs[i].lnNameSize);
			jfieldID NameSize = env->GetFieldID(clazz,"m_lNameSize","J");
			if(NameSize)
			{
				env->SetLongField(obj,NameSize,pstRecs[i].lnNameSize);
				jfieldID Name = env->GetFieldID(clazz,"m_Name","Ljava/lang/String;");
				if(Name)
				{
					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].pszName));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].pszName), (jbyte*)pstRecs[i].pszName);

					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						env->SetObjectField(obj,Name,jsName);
					}

					jfieldID Latitude = env->GetFieldID(clazz,"m_lLatitude","J");

					if(Latitude)
					{
						env->SetLongField(obj,Latitude,pstRecs[i].ulnLatitude);
						jfieldID Longitude = env->GetFieldID(clazz,"m_lLongitude","J");
						if(Longitude)
						{
							env->SetLongField(obj,Longitude,pstRecs[i].ulnLongitude);
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POI_Facility_GetPointRecList GetFieldID Longitude Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPointRecList GetFieldID Latitude Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPointRecList GetFieldID Name Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPointRecList GetFieldID NameSize Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPointRecList FindClass Failed!");
		}
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIFacility_GetMatchCount               *
*	Description	:                                                    *
*	Date		: 09/10/26                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_GetMunicipalityRecListCount
  (JNIEnv * env, jclass a, jlong jlnprefectureIndex, jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount begin!!!!!!!!!!!!");

	long Listconut = 0;
	jlong lRet = (jlong)NE_POI_Facility_GetMunicipalityRecListCount( (long)jlnprefectureIndex,&Listconut);

	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount Listconut:%d,lRet:%d!!!!!!!!!!!!",Listconut,lRet);
	jlong listconut = Listconut;
	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount SetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIFacility_GetRecList                  *
*	Description	:                                                    *
*	Date		: 09/10/26                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_GetMunicipalityRecList
  (JNIEnv *env, jclass clz,jlong RecIndex,jlong RecCount,jobjectArray args,jobject object)
{
	ZPOI_FACILITY_LISTITEM *pstRecs;
	long GetCount = 0;

	jlong Ret = 0;

	Ret = (jlong)NE_POI_Facility_GetMunicipalityRecList(RecIndex,RecCount,&pstRecs,&GetCount);

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
		//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList GetMethodID Sucessful");
	}

	//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList Sucessful");

	if(cls)
	{
		//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList SetLongField cls Enter");
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList SetLongField Count Begin");
			//Set the variable's value  to Java
			env->SetLongField(object,Count,(jlong)GetCount);
			//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList SetLongField Count Sucessful");
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POI_Facility_GetMunicipalityRecList GetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POI_Facility_GetMunicipalityRecList FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/POI_Facility_ListItem");
#if 0
	if(clazz)
	{
		LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList FindClass Sucessful!!!!!!!!!!!!!!");
	}
#endif
	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < GetCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList loop begin");

		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		if(obj)
		{
			//LOGDp("[DEBUG] JNI_NE_POIFacility_GetRecList NameSize:%d!!!!!!!!!!!!",pstRecs[i].lnNameSize);
			jfieldID NameSize = env->GetFieldID(clazz,"m_lNameSize","J");
			if(NameSize)
			{
				env->SetLongField(obj,NameSize,pstRecs[i].lnNameSize);
				jfieldID Name = env->GetFieldID(clazz,"m_Name","Ljava/lang/String;");
				if(Name)
				{
					jbyteArray bytes = env->NewByteArray(strlen(pstRecs[i].pszName));
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, strlen(pstRecs[i].pszName), (jbyte*)pstRecs[i].pszName);

					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
						env->SetObjectField(obj,Name,jsName);
					}

					jfieldID Latitude = env->GetFieldID(clazz,"m_lLatitude","J");

					if(Latitude)
					{
						env->SetLongField(obj,Latitude,pstRecs[i].ulnLatitude);
						jfieldID Longitude = env->GetFieldID(clazz,"m_lLongitude","J");
						if(Longitude)
						{
							env->SetLongField(obj,Longitude,pstRecs[i].ulnLongitude);
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POI_Facility_GetMunicipalityRecList GetFieldID Longitude Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POI_Facility_GetMunicipalityRecList GetFieldID Latitude Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POI_Facility_GetMunicipalityRecList GetFieldID Name Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POI_Facility_GetMunicipalityRecList GetFieldID NameSize Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POI_Facility_GetMunicipalityRecList FindClass Failed!");
		}
	}

	return Ret;
}
/*********************************************************************
*	Function Name	: JNI_NE_POI_Facility_Refine                                           *
*	Description	:                                                                                       *
*	Date		: 09/10/26                                                                         *
*	Parameter	:                                                                                        *
*	Return Code	: jlong                                                                                 *
*	Author		: xiayx	                                                                           *
*  ------------------------------------------------------------------ *
* Revision History	                                                                                        *
* No	Date		Revised by		Description                                              *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_Refine
  (JNIEnv *env, jclass cls, jlong jprefectureIndex, jlong jmunicipalityIndex)
{
	return (jlong)NE_POI_Facility_Refine((long)jprefectureIndex, (long)jmunicipalityIndex);
}
/*********************************************************************
*	Function Name	: JNI_NE_POI_Facility_SyllabaricSearch
*	Description	:                                            *
*	Date		: 09/10/26                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                      *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                         *
* No	Date		Revised by		Description              *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_SyllabaricSearch
  (JNIEnv *env, jclass cls)
{
	return (jlong)NE_POI_Facility_SyllabaricSearch();
}
/*********************************************************************
*	Function Name	: JNI_NE_POI_Facility_Refine                                           *
*	Description	:                                                                                       *
*	Date		: 09/10/26                                                                         *
*	Parameter	:                                                                                        *
*	Return Code	: jlong                                                                                 *
*	Author		: xiayx	                                                                           *
*  ------------------------------------------------------------------ *
* Revision History	                                                                                        *
* No	Date		Revised by		Description                                              *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_AroundSearch
  (JNIEnv *env, jclass cls, jlong jlnLatitude, jlong jlnLongitude, jlong jlnDistance)
{
	return (jlong)NE_POI_Facility_AroundSearch((long)jlnLatitude, (long)jlnLongitude, (long)jlnDistance);
}
/*********************************************************************
*	Function Name	: JNI_NE_POI_Facility_GetJumpList                *
*	Description	:                                                    *
*	Date		: 09/11/15                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  -------------------------------------------------------------------*
* Revision History	                                                  *
* No	Date		Revised by		Description                       *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_GetJumpList
  (JNIEnv *env, jclass clz,jobjectArray args,jobject obj)
{
	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList Begin!!!!!!!!!!!!!!");
	ZPOI_JUMPKEY *pstJumpkey;
	unsigned short JumpRecCount;
	jlong Ret = (jlong)NE_POI_Facility_GetJumpList(&pstJumpkey,&JumpRecCount);

	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList JumpRecCount:%d!!!!!!!!!!!!",JumpRecCount);
	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList Begin_1!!!!!!!!!!!!!!");

	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNIJumpKey");

	jclass strClass = env->FindClass("java/lang/String");
	jmethodID ctorID = 0;
	if(strClass != NULL)
	{
	      ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}
#if 0
	if(clazz)
	{
		//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList FindClass Sucessful!!!!!!!!!!!!!!");
	}
#endif

	jsize len = 0;
	len = env->GetArrayLength(args);

	int i;
	for( i = 0; i < JumpRecCount; i++ )
	{
		//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList loop begin");

		jobject object = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);

		jfieldID NameSize = env->GetFieldID(clazz,"NameSize","J");
		if(NameSize)
		{
			//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList GetFieldID NameSize start!!!!!!!!!!!!!!");
			env->SetLongField(object,NameSize,pstJumpkey[i].lnNameSize);
			//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList SetLongField NameSize start!!!!!!!!!!!!!!");
			jfieldID Pucname = env->GetFieldID(clazz,"pucName","Ljava/lang/String;");
			if(Pucname)
			{
				//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList GetFieldID Pucname start!!!!!!!!!!!!!!");
// Chg 2011/05/23 katsuta Start -->
//				jbyteArray bytes = env->NewByteArray(strlen((char *)pstJumpkey[i].pcName));
				jbyteArray bytes = env->NewByteArray(strlen((char *)pstJumpkey[i].pucName));
// Chg 2011/05/23 katsuta End <--
				if(bytes != NULL)
				{
// Chg 2011/05/23 katsuta Start -->
//					env->SetByteArrayRegion(bytes, 0, strlen((char *)pstJumpkey[i].pcName), (jbyte*)pstJumpkey[i].pcName);
					env->SetByteArrayRegion(bytes, 0, strlen((char *)pstJumpkey[i].pucName), (jbyte*)pstJumpkey[i].pucName);
// Chg 2011/05/23 katsuta End <--
				}
				jstring encoding = env->NewStringUTF("ShiftJIS");

				jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

				if(jsName)
				{
					env->SetObjectField(object,Pucname,jsName);
				}

				jfieldID Offset = env->GetFieldID(clazz,"Offset","J");
				if(Offset)
				{
					env->SetLongField(object,Offset,pstJumpkey[i].lnNameSize);
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList GetFieldID Offset Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList GetFieldID Pucname Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG]JNI_NE_POIAddr_GetJumpList GetFieldID NameSize Failed!");
		}
	}

	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNIShort");

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"m_sJumpRecCount","S");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetShortField(obj,Count,JumpRecCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIAddr_GetJump GetLongField JumpRecCount Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIAddr_GetJump FindClass  Failed");
	}

	return Ret;
}
/*********************************************************************
*	Function Name	: JNI_NE_POI_Facility_GetJumpKey2RecordNumber    *
*	Description	:                                                    *
*	Date		: 09/11/15                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_GetJumpKey2RecordNumber
  (JNIEnv *env, jclass cls, jlong NameSize,jstring jstr,jobject object)
{
       //LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex NameSize1:%d!!!!!!!!!!!!",(long)NameSize);

       long RecIndex =0;
       unsigned char* rtn = NULL;
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応 START
	jsize alen = 0;
	jbyte* ba = NULL;
	jclass clsstring = env->FindClass(CLASS_CommonLib);
	jmethodID mid = env->GetStaticMethodID(clsstring, "getToSJIS", "(Ljava/lang/String;)[B");
	jbyteArray barr= (jbyteArray)env->CallStaticObjectMethod(clsstring, mid, jstr);
	if( barr != NULL ){
		alen = env->GetArrayLength(barr);
		ba = env->GetByteArrayElements(barr, JNI_FALSE);
       	if (alen > 0)
       	{
                 rtn = new unsigned char[alen+1];
                 memcpy(rtn, ba, alen);
                 rtn[alen] = 0;
       	}
	}
	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex rtn:%s!!!!!!!!!!!!",rtn);
	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex NameSize2:%d!!!!!!!!!!!!",(long)NameSize);
	jlong Ret = (jlong)NE_POI_Facility_GetJumpKey2RecordNumber((long)NameSize,rtn,&RecIndex);
	if( barr != NULL ){
	 	env->ReleaseByteArrayElements(barr, ba, 0);
	}
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応  END

	//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex RecIndex:%d!!!!!!!!!!!!",RecIndex);
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//jlong lCount = (*env)->GetLongField(env,object,Count);

	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex GetFieldID  Index Begin");
		jfieldID Index = env->GetFieldID(clazz,"lcount","J");
		if(Index)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Index,(jlong)RecIndex);
			//LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex SetLongField  Index End");
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex SetLongField Index Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIAddr_GetJumpList2RecordIndex FindClass  Failed");
	}


//Add 2011/10/24 Z01h_yamada Start --> CID:10781 リソースリーク
	if (rtn != NULL) {
		delete[] rtn;
	}
//Add 2011/10/24 Z01h_yamada End <--

	return Ret;
}
/*********************************************************************
*	Function Name	: JNI_NE_POI_Facility_GetJumpKey2RecordNumber    *
*	Description	:                                                    *
*	Date		: 09/11/15                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: xiayx	                                             *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POI_Facility_GetPrefucturePointCount
  (JNIEnv *env, jclass cls, jlong jlnprefectureIndex,jobject object)
{
	long Listconut = 0;
	jlong lRet = (jlong)NE_POI_Facility_GetPrefucturePointCount( (long)jlnprefectureIndex,&Listconut);

	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount Listconut:%d,lRet:%d!!!!!!!!!!!!",Listconut,lRet);
	jlong listconut = Listconut;
	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefucturePointCount SetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POI_Facility_GetPrefucturePointCount FindClass  Failed");
	}

	return lRet;
}
