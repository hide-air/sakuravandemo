LOCAL_PATH:= $(call my-dir)/..
include $(CLEAR_VARS)

LOCAL_SRC_FILES += jni/NavigationSurfaceMgr.cpp
LOCAL_SRC_FILES += jni/NavigationjniLoad.cpp
LOCAL_SRC_FILES += jni/JNIMap_Communication.c
LOCAL_SRC_FILES += jni/JNIPoiAddr_Communication.cpp
LOCAL_SRC_FILES += jni/JNIPoiFacility_Communication.cpp
LOCAL_SRC_FILES += jni/JNIPoiGnr_Communication.cpp
LOCAL_SRC_FILES += jni/JNIPoiHist_Communication.cpp
LOCAL_SRC_FILES += jni/JNIPoiMyBox_Communication.cpp
LOCAL_SRC_FILES += jni/JNIPoiTel_Communication.cpp
LOCAL_SRC_FILES += jni/JNIPoiZip_Communication.cpp
LOCAL_SRC_FILES += jni/JNIPoiArnd_Communication.cpp
LOCAL_SRC_FILES += jni/JNIPoiRoute_Communication.cpp
LOCAL_SRC_FILES += jni/JNIRoute_Communication.c
LOCAL_SRC_FILES += jni/JNISet_Communication.cpp
LOCAL_SRC_FILES += jni/JNIGuide_Communication.c
LOCAL_SRC_FILES += jni/JNIMap_DriCon_Communication.c
LOCAL_SRC_FILES += jni/JNI_CommonData.c
LOCAL_SRC_FILES += jni/unicode/loccode.cpp

LOCAL_C_INCLUDES += $(BUILD_C_INCLUDES)
LOCAL_C_INCLUDES += jni/unicode

LOCAL_CFLAGS += $(BUILD_CFLAGS)
LOCAL_CFLAGS += -I/.
LOCAL_CFLAGS += -I/..

LOCAL_STATIC_LIBRARIES += libz
##LOCAL_STATIC_LIBRARIES += GeoUtils
LOCAL_STATIC_LIBRARIES += libpng
LOCAL_STATIC_LIBRARIES += libEmulator
LOCAL_STATIC_LIBRARIES += libFrame
LOCAL_STATIC_LIBRARIES += libMML
LOCAL_STATIC_LIBRARIES += libDAL
LOCAL_STATIC_LIBRARIES += libDG
LOCAL_STATIC_LIBRARIES += libRP
LOCAL_STATIC_LIBRARIES += libCMN
LOCAL_STATIC_LIBRARIES += libVP
LOCAL_STATIC_LIBRARIES += libMP
LOCAL_STATIC_LIBRARIES += libWG
LOCAL_STATIC_LIBRARIES += libPOI
LOCAL_STATIC_LIBRARIES += libCT
LOCAL_STATIC_LIBRARIES += libMN
LOCAL_STATIC_LIBRARIES += libNUI
LOCAL_STATIC_LIBRARIES += libGRL
LOCAL_STATIC_LIBRARIES += libBG
LOCAL_STATIC_LIBRARIES += libVICS
LOCAL_STATIC_LIBRARIES += libVoice
LOCAL_STATIC_LIBRARIES += libDR

LOCAL_LDLIBS := -ldl -L${SYSROOT}/usr/lib -llog
LOCAL_LDLIBS  += -Lobj/local/$(TARGET_ARCH_ABI) -lEmulator -lMML -lDG -lRP -lCMN -lVP -lMP -lWG -lPOI -lCT -lMN -lNUI -lGRL -lFrame -lBG -lVICS -lVoice
LOCAL_LDLIBS  += -lDAL

LOCAL_LDLIBS += -lpng -lz

LOCAL_LDLIBS += -lGLESv2
LOCAL_LDLIBS += -lstdc++

LOCAL_ARM_MODE := arm

LOCAL_PRELINK_MODULE := false

LOCAL_MODULE := Navi

include $(BUILD_SHARED_LIBRARY)
