/********************************************************************************
* File Name	:	 JNIMap_DriCon_Communication.h
* Created	:	2011/09/07
* Author	:	TheDoanh
* Description
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include <jni.h>

#ifndef _JNIMAP_DRICON_COMMUNICATION_H
#define _JNIMAP_DRICON_COMMUNICATION_H
#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jlong JNICALL JNI_MP_DC_SetPOIIconSize
  (JNIEnv *env, jclass clz, jint iconSize);

JNIEXPORT jlong JNI_MP_DC_GetDisplayArea
  (JNIEnv *, jclass,jobject, jobject);

JNIEXPORT jlong JNICALL JNI_MP_DC_SetPOIInfoData
  (JNIEnv *, jclass , jobjectArray );
#ifdef __cplusplus
}
#endif
#endif
