/********************************************************************************
* File Name	:	 JNIPoiFacility_Communication.cpp
* Created	:	2009/10/26
* Author	:	xiayx
* Model		:
* Description	:
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "JNIPoiRoute_Communication.h"
#include "NaviEngineIF.h"
#include "NaviEngineCtrl.h"
#include "NaviEngine/CT/CTUICommand.h"
#include "NETypes.h"
#include "windows.h"
#include "NaviEngine/POI/POIStruct.h"
#include "jni/JNI_Common.h"
#include "CTICU.h"
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define LOG_TAG "NavigationJNI_NaviRun"
#define JNI_DEBUG

#include "debugPrint.h"

/*********************************************************************
*	Function Name	: JNI_NE_POIRoute_Cancel         	     *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIRoute_Cancel
  (JNIEnv *env, jclass a)
{
	//LOGDp("[DEBUG] JNICALL JNI_NE_POIArnd_Cancel begin!!!!!!!!!");

	return (jlong)NE_POIRoute_Cancel();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIRoute_SetRoute         	     *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIRoute_SetRoute
  (JNIEnv *env, jclass a)
{
	//LOGDp("[DEBUG] JNICALL JNI_NE_POIArnd_Cancel begin!!!!!!!!!");

	return (jlong)NE_POIRoute_SetRoute();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIRouteMybox_SetRoute         	     *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIRouteMybox_SetRoute
  (JNIEnv *env, jclass a,jint iForward)
{
	//LOGDp("[DEBUG] JNICALL JNI_NE_POIArnd_Cancel begin!!!!!!!!!");

	return (jlong)NE_POIRouteMybox_SetRoute((BOOL)iForward);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIRoute_SetParam         	     *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIRoute_SetParam
  (JNIEnv *env, jclass a,jobject object)
{
	//LOGDp("[DEBUG] JNICALL JNI_NE_POIArnd_Cancel begin!!!!!!!!!");
	ZPOI_Route_Param pstRoutData;

	LOGDp("[DEBUG] JNI_NE_POIRoute_SetParam Begin!!!!!!!!!!!");
	jclass jcls = env->GetObjectClass(object);

	if (jcls)
	{

		jfieldID jfid1 = env->GetFieldID( jcls, "eside", "I" );
		if (jfid1)
		{
			jint value1 = env->GetIntField(object,jfid1);
			pstRoutData.eSide= (EPOI_ROUTE_SIDE)value1;
		}

		jfieldID jfid2 = env->GetFieldID( jcls, "lnCount", "J" );
		if (jfid2)
		{
			jlong value2 = env->GetLongField(object,jfid2);
			pstRoutData.lnCount = (long)value2;
		}

		jfieldID jfid3 = env->GetFieldID( jcls, "ulnRange", "J" );
		if (jfid3)
		{
			jlong value3 = env->GetLongField(object,jfid3);
			pstRoutData.ulnRange = (unsigned long)value3;

		}

	}
	return (jlong)NE_POIRoute_SetParam(&pstRoutData);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIRoute_SetParam         	     *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIRouteMybox_SetParam
  (JNIEnv *env, jclass a,jobject object)
{
	//LOGDp("[DEBUG] JNICALL JNI_NE_POIArnd_Cancel begin!!!!!!!!!");
	ZPOI_Route_Param stRoutData;

	//LOGDp("[DEBUG] JNI_VP_NMEASetSatellitesInfo Begin!!!!!!!!!!!");
	jclass jcls = env->GetObjectClass(object);
	//jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/ZPOI_Route_Param");

	if (jcls)
	{
		jfieldID jfid1 = env->GetFieldID( jcls, "eside", "I" );
		if (jfid1)
		{
			jint value1 = env->GetIntField(object,jfid1);
			stRoutData.eSide= (EPOI_ROUTE_SIDE)value1;
		}


		jfieldID jfid2 = env->GetFieldID( jcls, "lnCount", "J" );
		if (jfid2)
		{
			jlong value2 = env->GetLongField(object,jfid2);
			stRoutData.lnCount = (long)value2;
		}

		jfieldID jfid3 = env->GetFieldID( jcls, "ulnRange", "J" );
		if (jfid3)
		{
			jlong value3 = env->GetLongField(object,jfid3);
			stRoutData.ulnRange = (unsigned long)value3;
		}

	}
	return (jlong)NE_POIRouteMybox_SetParam(&stRoutData);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIRoute_SearchList         	     *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIRoute_SearchList
  (JNIEnv *env, jclass a)
{
	//LOGDp("[DEBUG] JNICALL JNI_NE_POIArnd_SearchList begin!!!!!!!!!!");
	return (jlong)NE_POIRoute_SearchList();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIRoute_GetRecCount        	     *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIRoute_GetRecCount
  (JNIEnv * env, jclass a, jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount begin!!!!!!!!!!!!");

	long Listconut = 0;
	jlong lRet = (jlong)NE_POIRoute_GetRecCount(&Listconut);

	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount Listconut:%d,lRet:%d!!!!!!!!!!!!",Listconut,lRet);
	jlong listconut = Listconut;
	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount Sucessful");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(clazz,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount SetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecCount FindClass  Failed");
	}

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIRoute_GetNextTreeIndex            *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIRoute_GetNextTreeIndex
  (JNIEnv * env, jclass a, jlong RecIndex, jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetNextTreeIndex begin!!!!!!!!!!!!!!!!!!!!!");
	unsigned long Treeindex = 0;

	jlong lRet = (jlong)NE_POIRoute_GetNextTreeIndex(RecIndex,&Treeindex);

	jlong treeindex = Treeindex;
	LOGDp("[DEBUG]treeindex is : treeindex = %d", treeindex);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID TreeIndex = env->GetFieldID(clazz,"lcount","J");
		if(TreeIndex)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,TreeIndex,treeindex);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIArnd_GetNextTreeIndex SetLongField TreeIndex Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetNextTreeIndex FindClass  Failed");
	}

	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetNextTreeIndex Sucessful");

	return lRet;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIRoute_SearchNextList              *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIRoute_SearchNextList
  (JNIEnv *env, jclass a, jlong RecIndex)
{
	unsigned long Recindex = RecIndex;
	//LOGDp("[DEBUG] JNI_NE_POIArnd_SearchNextList begin");
	return (jlong)NE_POIRoute_SearchNextList(Recindex);
}

/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_GetPrevTreeIndex             *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIRoute_GetPrevTreeIndex
  (JNIEnv *env, jclass a, jobject object)
{
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetPrevTreeIndex begin");
	unsigned long treeindex = 0;

	//jlong lRet =
	(jlong)NE_POIRoute_GetPrevTreeIndex(&treeindex);
	LOGDp("[DEBUG]treeindex is : treeindex = %d", treeindex);

	//Fing Java's Class
	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID TreeIndex = env->GetFieldID(clazz,"lcount","J");
		if(TreeIndex)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,TreeIndex,treeindex);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIArnd_GetPrevTreeIndex SetLongField TreeIndex Failed");
		}
	}

	else
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetPrevTreeIndex FindClass  Failed");
	}
	//LOGDp("[DEBUG] JNI_NE_POIArnd_GetPrevTreeIndex Sucessful");

	return 0;
}

/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_SearchPrevList              *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIRoute_SearchPrevList
  (JNIEnv *env, jclass a)
{
	return (jlong)NE_POIRoute_SearchPrevList();
}

/*********************************************************************
*	Function Name	: JNI_NE_POIArnd_GetRecList                  *
*	Description	:                                            *
*	Date		: 09/10/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_POIRoute_GetRecList
  (JNIEnv *env, jclass a, jlong RecIndex, jlong RecCount, jobjectArray args, jobject object)
  {
	//LOGDp("[DEBUG] JNI_NE_POIRoute_GetRecList begin");
	//LOGDp("[DEBUG] JNI_NE_POIRoute_GetRecList RecIndex:%d,RecCount:%d!!!!!!!!!!!!",RecIndex,RecCount);
	long Count;
	long Index = RecIndex;
	long RecNum = RecCount;
	ZPOI_Route_ListItem* pstRecs;

	//jlong Ret =
	(jlong)NE_POIRoute_GetRecList(Index,RecNum,&pstRecs, &Count);

	jlong listconut = Count;
	//Fing Java's Class
	jclass cls = env->FindClass("net/zmap/android/pnd/v2/data/JNILong");

	//LOGDp("[DEBUG] JNI_NE_POIRoute_GetRecList Sucessful%d",Count);

	if(cls)
	{
		//Get variable's ID in JNI
		jfieldID Count = env->GetFieldID(cls,"lcount","J");
		if(Count)
		{
			//Set the variable's value  to Java
			env->SetLongField(object,Count,listconut);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_POIRoute_GetRecList SetLongField Count Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_POIRoute_GetRecList FindClass  Failed");
	}


	jclass clazz = env->FindClass("net/zmap/android/pnd/v2/data/ZPOI_Route_ListItem");

#if 0
	if(clazz)
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList FindClass Sucessful!!!!!!!!!!!!!!");
	}
#endif
	int i;
	//char recName[512] = {0};
	jmethodID ctorID = 0;
	jclass strClass = env->FindClass("java/lang/String");
	if(strClass != NULL)
	{
		ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	}else
	{
		LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList for 7-error strClass NULL");
	}

	for( i = 0; i < Count; i++ )
	{
		jobject obj = (jobject)env->GetObjectArrayElement((jobjectArray)args, i);
		if(obj)
		{
			jfieldID NameSize = env->GetFieldID(clazz,"NameSize","J");
			if(NameSize)
			{
				env->SetLongField(obj,NameSize,pstRecs[i].lnNameSize);
				jfieldID Pucname = env->GetFieldID(clazz,"pucName","Ljava/lang/String;");
				if(Pucname)
				{
					int lenjis = strlen((char*)pstRecs[i].pucName);
					jbyteArray bytes = env->NewByteArray(lenjis);
					if(bytes != NULL)
					{
						env->SetByteArrayRegion(bytes, 0, lenjis, (jbyte*)pstRecs[i].pucName);
					}else{
						LOGDp("[DEBUG] JNI_NE_POIArnd_GetRecList for 8 error bytes==NULL");
					}
					jstring encoding = env->NewStringUTF("ShiftJIS");


					jstring  jsName = (jstring)env->NewObject(strClass, ctorID, bytes, encoding);

					if(jsName)
					{
								env->SetObjectField(obj,Pucname,jsName);
					}else
					{
						LOGDp("[DEBUG] JNI_NE_POIRoute_GetRecList error888 jsName== NULL ");
					}

					//delete []jname;
					jfieldID Distance = env->GetFieldID(clazz,"Distance","J");
					if(Distance)
					{
						env->SetLongField(obj,Distance,pstRecs[i].lnDistance);
						jfieldID Latitude = env->GetFieldID(clazz,"Latitude","J");
						if(Latitude)
						{
							env->SetLongField(obj,Latitude,pstRecs[i].ulnLatitude);
							jfieldID Longitude = env->GetFieldID(clazz,"Longitude","J");
							if(Longitude)
							{
								env->SetLongField(obj,Longitude,pstRecs[i].ulnLongitude);
								jfieldID Existenceflg = env->GetFieldID(clazz,"ExistenceFlg","J");
								if(Existenceflg)
								{
									env->SetLongField(obj,Existenceflg,pstRecs[i].lnExistenceFlg);
									jfieldID jside = env->GetFieldID(clazz,"eside","I");
									if(jside)
									{
										//env->SetIntField(obj,jside,pstRecs[i].eSide);
									}

								}
								else
								{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIRoute_GetRecList GetFieldID Existenceflg Failed");
								}
								jfieldID jKindCode = env->GetFieldID(clazz,"kindeCode","J");
								if(jKindCode){
									env->SetLongField(obj, jKindCode, pstRecs[i].lnKindCode);
								}
								else{
									LOGDp("[DEBUG] JNICALL JNI_NE_POIRoute_GetRecList GetFieldID kindCode Failed");
								}
							}
							else
							{
								LOGDp("[DEBUG] JNICALL JNI_NE_POIRoute_GetRecList GetFieldID Longitude Failed");
							}
						}
						else
						{
							LOGDp("[DEBUG] JNICALL JNI_NE_POIRoute_GetRecList GetFieldID Latitude Failed");
						}

					}
					else
					{
						LOGDp("[DEBUG] JNI_NE_POIRoute_GetRecList GetFieldID Distance Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_NE_POIRoute_GetRecList GetFieldID Pucname Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_POIRoute_GetRecList GetFieldID NameSize Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNITestLoad JNI_NE_POIRoute_GetRecList FindClass Failed!");
		}
	}
	//LOGDp("[DEBUG] JNI_NE_POIRoute_GetRecList end");

	return 0;
}

JNIEXPORT jlong JNICALL JNI_NE_POIRoute_MiddleList_Search
  (JNIEnv *env, jclass cls, jlong jlKey, jlong jPointKey)
{
	return (jlong)NE_POI_Route_MiddleList_Search((unsigned short)jlKey, (unsigned short)jPointKey);
}

JNIEXPORT jlong JNICALL JNI_NE_POIRoute_Clear
  (JNIEnv *env, jclass a)
{
	return (jlong)NE_POI_Route_Clear();
}
