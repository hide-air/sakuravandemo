/********************************************************************************
* File Name	:	 JNIMap_DriCon_Communication.c
* Created	:	2011/09/07
* Author	:	TheDoanh
* Description
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/

#include "jni/JNIMap_DriCon_Communication.h"
#include "jni/JNI_Common.h"
#include "debugPrint.h"
#include "windowsTypeDef.h"
#include "NaviEngine/MP/MPICON/MPDriverContents.h"

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */


#if defined(__cplusplus)
}
#endif /* __cplusplus */

#define JNI_DC_NOERR					0
#define JNI_DC_ERR_REALLOC_ALL_DATA		1
#define JNI_DC_ERR_REALLOC_INFO_DATA	2
#define JNI_DC_ERR_MALLOC				3

JNIEXPORT jlong JNICALL JNI_MP_DC_GetDisplayArea
  (JNIEnv *env, jclass cls,jobject object1, jobject object2)
{
	jlong lRet;
	RECT rect;
	lRet = (jlong)MP_DC_GetDisplayArea(&rect);

	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNITwoLong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jLongitude1 = (*env)->GetFieldID(env,clazz,"m_lLong","J");
		if(jLongitude1)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object1,jLongitude1,rect.left);
			jfieldID jLatitude1 = (*env)->GetFieldID(env,clazz,"m_lLat","J");
			if(jLatitude1)
			{
				(*env)->SetLongField(env, object1,jLatitude1,rect.bottom);
				jfieldID jLongitude2 = (*env)->GetFieldID(env,clazz,"m_lLong","J");
				if(jLongitude2)
				{
					(*env)->SetLongField(env, object2,jLongitude2,rect.right);
					jfieldID jLatitude2 = (*env)->GetFieldID(env,clazz,"m_lLat","J");
					if(jLatitude2)
					{
						(*env)->SetLongField(env, object2,jLatitude2,rect.top);
					}
					else
					{
						LOGDp("[DEBUG] JNI_MP_DC_GetDisplayArea GetFieldID jLatitude2 Failed");
					}
				}
				else
				{
					LOGDp("[DEBUG] JNI_MP_DC_GetDisplayArea GetFieldID jLongitude2 Failed");
				}
			}
			else
			{
				LOGDp("[DEBUG] JNI_MP_DC_GetDisplayArea GetFieldID jLatitude1 Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_MP_DC_GetDisplayArea GetFieldID jLongitude1 Failed");
		}
	}

	return lRet;
}

JNIEXPORT jlong JNICALL JNI_MP_DC_SetPOIIconSize
  (JNIEnv *env, jclass clz, jint iconSize)
{
	return (jlong)MP_DC_SetPOIIconSize(iconSize);
}

JNIEXPORT jlong JNICALL JNI_MP_DC_SetPOIInfoData
  (JNIEnv *env, jclass clz, jobjectArray objDataList)
{
	long						recFunc = MP_SUCCESS;
	int 						i = 0;
	ZMP_DC_POI_DATA_INFO* 		pstPOIDataListInfo = NULL;
	ZMP_DC_POI_DATA_INFO* 		pstOldPOIDataListInfo = NULL;
	ZMP_DC_POI_INFO*			pstOldPOIInfo = NULL;
	long						lnDataCnt;
	long						lnPreMeshNum = -1;
	long						lnListSize = 0;
	long						lnErrCode = JNI_DC_NOERR;
	jobject 					obj = NULL;
	lnDataCnt = (long) (*env)->GetArrayLength(env, objDataList);
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/DC_POIInfoData");
	if(clazz){
		for(i = 0; i < lnDataCnt; i++)
		{
			obj = (jobject) (*env)->GetObjectArrayElement(env, (jobjectArray)objDataList, i);
			if(obj){
				jfieldID jflnMeshNum = (*env)->GetFieldID(env, clazz, "m_lMeshNum", "J" );
				if(jflnMeshNum)
				{
					jlong jlnMeshNum = (*env)->GetLongField(env, obj, jflnMeshNum);
					if(jlnMeshNum){
						if(jlnMeshNum > lnPreMeshNum){
							lnPreMeshNum = jlnMeshNum;
							lnListSize++;
							pstOldPOIDataListInfo = (ZMP_DC_POI_DATA_INFO*)MP_C_Realloc(pstPOIDataListInfo, sizeof(ZMP_DC_POI_DATA_INFO)*lnListSize);
							if(pstOldPOIDataListInfo == NULL) {
								lnErrCode = JNI_DC_ERR_REALLOC_ALL_DATA;
								break;
							}
							pstPOIDataListInfo = pstOldPOIDataListInfo;
							memset(&pstPOIDataListInfo[lnListSize-1], 0, sizeof( ZMP_DC_POI_DATA_INFO ));
							pstPOIDataListInfo[lnListSize - 1].lnMeshNum = (long)jlnMeshNum;
						}
						pstPOIDataListInfo[lnListSize - 1].lnCount++;
						pstOldPOIInfo = (ZMP_DC_POI_INFO*)MP_C_Realloc(pstPOIDataListInfo[lnListSize - 1].pstPOIInfo, sizeof(ZMP_DC_POI_INFO)*pstPOIDataListInfo[lnListSize - 1].lnCount);
						if(pstOldPOIInfo == NULL) {
							lnErrCode = JNI_DC_ERR_REALLOC_INFO_DATA;
							break;
						}
						pstPOIDataListInfo[lnListSize - 1].pstPOIInfo = pstOldPOIInfo;
						int nIndex = pstPOIDataListInfo[lnListSize - 1].lnCount - 1;
						memset(&pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex], 0, sizeof(ZMP_DC_POI_INFO));
						pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].nIconID =DEF_UNKNOWN_ICON_ID;
						jfieldID jflnLat = (*env)->GetFieldID(env, clazz, "m_lnLatitude1000", "J" );
						if(jflnLat)
						{
							jlong jlnLat = (*env)->GetLongField(env, obj,jflnLat);
							if(jlnLat){
								pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].lnLatitude1000 = (long)jlnLat;
							}
						} else {
							LOGDp("[DEBUG]jflnLat error");
						}
						jfieldID jflnLon = (*env)->GetFieldID(env, clazz, "m_lnLongitude1000", "J" );
						if(jflnLon)
						{
							jlong jlnLon = (*env)->GetLongField(env, obj, jflnLon);
							if(jlnLon){
								pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].lnLongitude1000= (long)jlnLon;
							}
						} else {
							LOGDp("[DEBUG]jflnLon error");
						}
						jfieldID jflnPPOIId = (*env)->GetFieldID(env, clazz, "m_POIId", "J" );
						if(jflnPPOIId)
						{
							jlong jlnPPOIId = (*env)->GetLongField(env, obj, jflnPPOIId);
							if(jlnPPOIId){
								pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].llPOIId= (long long)jlnPPOIId;
							}
						} else {
							LOGDp("[DEBUG]jfiPPOIId error");
						}
						jfieldID jfiX = (*env)->GetFieldID(env, clazz, "m_nX", "I" );
						if(jfiX)
						{
							jint jiX = (*env)->GetIntField(env, obj, jfiX);
							if(jiX){
								pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].nX= (int)jiX;
							}
						} else {
							LOGDp("[DEBUG]jfiX error");
						}
						jfieldID jfiY = (*env)->GetFieldID(env, clazz, "m_nY", "I" );
						if(jfiY)
						{
							jint jiY = (*env)->GetIntField(env, obj, jfiY);
							if(jiY){
								pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].nY= (int)jiY;
							}
						} else {
							LOGDp("[DEBUG]jiY error");
						}
						jfieldID jfIconPath = (*env)->GetFieldID(env, clazz, "m_sIconPath", "Ljava/lang/String;" );
						if(jfIconPath)
						{
							jstring jIconPath = (jstring)(*env)->GetObjectField(env, obj,jfIconPath);
							if(jIconPath){
								const char* pszTmp = (*env)->GetStringUTFChars(env, jIconPath, NULL);
								if(!pszTmp){
									pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].pszIconPath = NULL;
								}else{
									int len = strlen(pszTmp);
									pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].pszIconPath = (char*)MP_C_Malloc(sizeof(char) * len+1);
									if(pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].pszIconPath == NULL) {
										(*env)->ReleaseStringUTFChars(env, jIconPath, pszTmp);
										(*env)->DeleteLocalRef(env , jIconPath);
										lnErrCode = JNI_DC_ERR_MALLOC;
										break;
									}
									strcpy(pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].pszIconPath, pszTmp);
									pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].pszIconPath[len] = '\0';
								}
								(*env)->ReleaseStringUTFChars(env, jIconPath, pszTmp);
								(*env)->DeleteLocalRef(env , jIconPath);
							}
						}else{
							LOGDp("[DEBUG]jfIconPath error");
						}
						jfieldID jfPOIName = (*env)->GetFieldID(env, clazz, "m_sPOIName", "Ljava/lang/String;" );
						if(jfPOIName)
						{
							jstring jPOIName = (jstring)(*env)->GetObjectField(env, obj,jfPOIName);
							if(jPOIName){
								const char* pszTmp = (*env)->GetStringUTFChars(env, jPOIName, NULL);
								if(!pszTmp){
									pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].pszPOIName = NULL;
								}else{
									int len = strlen(pszTmp);
									pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].pszPOIName = (char*)MP_C_Malloc(sizeof(char) * len + 1);
									if(pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].pszPOIName == NULL) {
										(*env)->ReleaseStringUTFChars(env, jPOIName, pszTmp);
										(*env)->DeleteLocalRef(env , jPOIName);
										lnErrCode = JNI_DC_ERR_MALLOC;
										break;
									}
									strcpy(pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].pszPOIName, pszTmp);
									pstPOIDataListInfo[lnListSize - 1].pstPOIInfo[nIndex].pszPOIName[len] = '\0';
								}
								(*env)->ReleaseStringUTFChars(env, jPOIName, pszTmp);
								(*env)->DeleteLocalRef(env , jPOIName);
							}
						}else{
							LOGDp("[DEBUG]jfPOIName error");
						}
					}
				} else {
					LOGDp("[DEBUG]m_lMeshNum error");
				}
				(*env)->DeleteLocalRef(env , obj);
			}
		}
	}

	switch(lnErrCode){
	case JNI_DC_NOERR:
		recFunc = MP_DC_SetDrawDCData(pstPOIDataListInfo, lnListSize);
		break;
	case JNI_DC_ERR_REALLOC_ALL_DATA:
		MP_DC_Free(pstPOIDataListInfo, lnListSize - 1);
		recFunc = MP_ERR_MALLOC;
		break;
	case JNI_DC_ERR_REALLOC_INFO_DATA:
		MP_DC_Free(pstPOIDataListInfo, lnListSize - 1);
		recFunc = MP_ERR_MALLOC;
		break;
	case JNI_DC_ERR_MALLOC:
		MP_DC_Free(pstPOIDataListInfo, lnListSize);
		recFunc = MP_ERR_MALLOC;
		break;
	default:
		break;
	}

	return (jlong)recFunc;
}
#if defined(__cplusplus)
}
#endif /* __cplusplus */
