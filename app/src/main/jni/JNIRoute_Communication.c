/********************************************************************************
* File Name	:	 JNIRoute_Communication.cpp
* Created	:	2009/11/21
* Author	:	xiayx
* Model		:
* Description	:
* -----------------------------------------------------------------------------
* Revision History
* No	Date		Revised by		Description
* --	----		----------		-----------
*******************************************************************************/
#include "JNIRoute_Communication.h"
#include "NaviEngineIF.h"
#include "NaviEngineCtrl.h"
#include "NaviEngine/CT/CTUICommand.h"
#include "NETypes.h"
#include "windows.h"
#include "NaviEngine/POI/POIStruct.h"
#include "jni/JNI_Common.h"
#include "CTICU.h"
#include "DGIF.h"
#include "CTUICommand.h"
#include "NaviUI.h"
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define LOG_TAG "NavigationJNI_NaviRun"
#define JNI_DEBUG

#include "debugPrint.h"

/*********************************************************************
*	Function Name	: JNI_NE_GetRoutePoint        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_GetRoutePoint
  (JNIEnv *env, jclass clz,jint Type,jobject object,jobject jstr,jobject obj2)
{
	long Longitude = 0;
	long Latitude = 0;
	char *pszPointName= NULL;
	int bPassed = 0;

	jlong Ret = (jlong)NE_GetRoutePoint((ENE_RoutePointType)Type,&Longitude,&Latitude,&pszPointName,&bPassed);

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNITwoLong");


	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jLon = (*env)->GetFieldID(env,clazz,"m_lLong","J");
		if(jLon)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env,object,jLon,(jlong)Longitude);
			jfieldID jLat = (*env)->GetFieldID(env,clazz,"m_lLat","J");
			if (jLat)
			{
				//Set the variable's value  to Java
				(*env)->SetLongField(env,object,jLat,(jlong)Latitude);
			}
			else
			{
				LOGDp("[DEBUG] JNI_NE_GetRoutePoint GetLongField jLat Failed");
			}
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetRoutePoint GetLongField jLon Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_GetRoutePoint FindClass Failed");
	}

	jmethodID ctorID = 0;
	jclass strClass = (*env)->FindClass(env,"java/lang/String");
	if(strClass != NULL)
	{
		ctorID = (*env)->GetMethodID(env,strClass, "<init>", "([BLjava/lang/String;)V");
	}

	jclass cls = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIString");

	jfieldID Name = 0;

	if (cls)
	{
		Name = (*env)->GetFieldID(env,cls,"m_StrAddressString","Ljava/lang/String;");
	}

	if(pszPointName != NULL)
	{
		jbyteArray bytes = (*env)->NewByteArray(env,strlen(pszPointName));
		if(bytes != NULL)
		{
			(*env)->SetByteArrayRegion(env, bytes, 0, strlen(pszPointName), (jbyte*)pszPointName);

		}
		jstring encoding = (*env)->NewStringUTF(env,"ShiftJIS");
		jstring  jsName = (jstring)(*env)->NewObject(env, strClass, ctorID, bytes, encoding);

		if(jsName)
		{
			(*env)->SetObjectField(env,jstr,Name,jsName);
		}

		if(NULL != bytes)
		{
			(*env)->DeleteLocalRef(env,bytes);
			bytes = NULL;
		}
		if(NULL != encoding)
		{
			(*env)->DeleteLocalRef(env,encoding);
			encoding = NULL;
		}
		if(NULL != jsName)
		{
			(*env)->DeleteLocalRef(env,jsName);
			jsName = NULL;
		}
	}

	jclass claz = (*env)->FindClass(env,"net/zmap/android/pnd/v2/data/JNIInt");
	if(claz)
	{
		//Get variable's ID in JNI
		jfieldID jPass = (*env)->GetFieldID(env,claz,"m_iCommon","I");
		if(jPass)
		{
			//Set the variable's value  to Java
			(*env)->SetIntField(env,obj2,jPass,(jint)bPassed);
		}
		else
		{
			LOGDp("[DEBUG] JNI_NE_GetRoutePoint GetLongField bPassed Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_NE_GetRoutePoint FindClass claz Failed");
	}

	return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_SetRoutePoint        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_SetRoutePoint
  (JNIEnv *env, jclass clz, jint Type,jlong Longitude,jlong Latitude,jstring jstr,jint bPassed)
{
	jlong Ret = 0;
   char* rtn = NULL;
   jbyte* ba = NULL;
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応 START
   jclass clsstring = (*env)->FindClass(env, CLASS_CommonLib);
   jmethodID mid = (*env)->GetStaticMethodID(env, clsstring, "getToSJIS", "(Ljava/lang/String;)[B");
   jbyteArray barr= (jbyteArray)(*env)->CallStaticObjectMethod(env, clsstring, mid, jstr);
// MOD.2013.12.05 N.Sasao OS4.4 ShiftJIS誤変換対応  END
   if(barr != NULL)
   {
	   jsize alen = (*env)->GetArrayLength(env,barr);
	   ba = (*env)->GetByteArrayElements(env,barr, JNI_FALSE);
	   if (alen > 0)
	   {
		   rtn = (char *)malloc(sizeof(char)*(alen+1));
		   memcpy(rtn, ba, sizeof(char)*alen);
		   rtn[alen] = '\0';
	   }
   }

   Ret = (jlong)NE_SetRoutePoint((ENE_RoutePointType)Type,(long)Longitude,(long)Latitude,rtn,(int)bPassed);

   if(barr != NULL)
   {
       (*env)->ReleaseByteArrayElements(env,barr, ba, 0);
   }
   if(rtn != NULL)
   {
	   free(rtn);
	   rtn = NULL;
   }

   return Ret;
}

/*********************************************************************
*	Function Name	: JNI_NE_DeleteRoutePoint        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_DeleteRoutePoint
  (JNIEnv *env, jclass clz,jint Type)
{
	return (jlong)NE_DeleteRoutePoint((ENE_RoutePointType)Type);
}

/*********************************************************************
*	Function Name	: JNI_NE_CheckRouteCalculate        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_CheckRouteCalculate
  (JNIEnv *env, jclass clz,jint NaviMode,jobject object,jobject obj)
{
	ZNE_GeoPoint Point1,Point2;
	jclass jcls = (*env)->GetObjectClass(env,object);
	jclass jclz = (*env)->GetObjectClass(env,obj);

	if (jcls)
	{
		jfieldID jfid1 = (*env)->GetFieldID(env,jcls, "m_lLongitude", "J" );
		if (jfid1)
		{
			jlong value1 = (*env)->GetLongField(env,object,jfid1);
			Point1.lnLongitude = (long)value1;
		}

		jfieldID jfid2 = (*env)->GetFieldID(env,jcls, "m_lLatitude", "J" );
		if (jfid2)
		{
			jlong value2 = (*env)->GetLongField(env,object,jfid2);
			Point1.lnLatitude = (long)value2;
		}
	}

	if (jclz)
	{
		jfieldID jfid1 = (*env)->GetFieldID(env,jclz, "m_lLongitude", "J" );
		if (jfid1)
		{
			jlong value1 = (*env)->GetLongField(env,obj,jfid1);
			Point2.lnLongitude = (long)value1;
		}

		jfieldID jfid2 = (*env)->GetFieldID(env,jclz, "m_lLatitude", "J" );
		if (jfid2)
		{
			jlong value2 = (*env)->GetLongField(env,obj,jfid2);
			Point2.lnLatitude = (long)value2;
		}
	}

	return (jlong)NE_CheckRouteCalculate((ENE_NaviMode)NaviMode,&Point1,&Point2);
}

/*********************************************************************
*	Function Name	: JNI_NE_ClearCalculate        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_NE_ClearCalculate
  (JNIEnv *env, jclass clz)
{
	return (jlong)NE_ClearCalculate();
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_RouteCalculate        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_RouteCalculate
// Chg 2012/03/13 Z01_sawada(No.334) Start --> ref 2697
//(JNIEnv *env, jclass clz,jint ModeType)
  (JNIEnv *env, jclass clz, jint ModeType, jstring jstr)
// Chg 2012/03/13 Z01_sawada(No.334) End <-- ref 2697
{
// Chg 2012/03/13 Z01_sawada(No.334) Start --> ref 2697
//	return (jlong)CT_UIC_RouteCalculate((ENUI_MODE_SET_TYPE)ModeType, DEF_NUI_HITTEST_EVENTNON);

	jlong		lRet = 0;
	char*		rtn = NULL;
	jbyte*		ba = NULL;
	jclass		clsstring = (*env)->FindClass(env,"java/lang/String");
	jstring 	strencode = (*env)->NewStringUTF(env,"ShiftJIS");
	jmethodID	mid = (*env)->GetMethodID(env,clsstring, "getBytes", "(Ljava/lang/String;)[B");
	jbyteArray 	barr= (jbyteArray)(*env)->CallObjectMethod(env, jstr, mid, strencode);

	if (barr != NULL) {
		jsize alen = (*env)->GetArrayLength(env,barr);
		ba = (*env)->GetByteArrayElements(env,barr, JNI_FALSE);
		if (alen > 0) {
		   rtn = (char *)malloc(sizeof(char) * (alen + 1));
		   memcpy(rtn, ba, sizeof(char) * alen);
		   rtn[alen] = '\0';
		}
	}

	lRet = (jlong)CT_UIC_RouteCalculate((ENUI_MODE_SET_TYPE)ModeType, DEF_NUI_HITTEST_EVENTNON, rtn);

	if (barr != NULL) {
	   (*env)->ReleaseByteArrayElements(env, barr, ba, 0);
	}
	(*env)->DeleteLocalRef(env, strencode);
	if(rtn != NULL) {
	   free(rtn);
	   rtn = NULL;
	}

	return lRet;

// Chg 2012/03/13 Z01_sawada(No.334) End <-- ref 2697
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_5RouteCalculate        	         *
*	Description	:                                                    *
*	Date		: 09/11/21                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: LiuGang	                                         *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_5RouteCalculate
  (JNIEnv *env, jclass clz)
{
	return (jlong)CT_UIC_5RouteCalculate(0);
}
/*********************************************************************
*	Function Name	: JNI_ct_uic_5RouteCalculate        	         *
*	Description	:                                                    *
*	Date		: 09/11/21                                           *
*	Parameter	:                                                    *
*	Return Code	: jlong                                              *
*	Author		: LiuGang	                                         *
*  ------------------------------------------------------------------*
* Revision History	                                                 *
* No	Date		Revised by		Description                      *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_RouteView
  (JNIEnv *env, jclass clz,jint iRouteType)
{
	return (jlong)CT_UIC_RouteView((ZNUI_EVENT_INFO)iRouteType);
}
/*********************************************************************
*	Function Name	: JNI_ct_uic_RouteCalcCancel        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_RouteCalcCancel
  (JNIEnv *env, jclass clz)
{
	return (jlong)CT_UIC_RouteCalcCancel();
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_RoutePtAdd        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_RoutePtAdd
  (JNIEnv *env, jclass clz)
{
	unsigned int AheadWndID;
	unsigned int OriginWndID;
	int TransitionType;

	return (jlong)CT_UIC_RoutePtAdd(&AheadWndID,&OriginWndID,&TransitionType);
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_RoutePtChange        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_RoutePtChange
  (JNIEnv *env, jclass clz)
{
	unsigned int AheadWndID;
	unsigned int OriginWndID;
	int TransitionType;

	return (jlong)CT_UIC_RoutePtChange(&AheadWndID,&OriginWndID,&TransitionType);
}

/*********************************************************************
*	Function Name	: JNI_CT_IsNEActiveType        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jint JNICALL JNI_CT_IsNEActiveType
  (JNIEnv *env, jclass clz,jint ActiveType)
{
	//return (jint)CT_IsNEActiveType((ENE_NEActiveType)ActiveType);
	return 0;
}

/*********************************************************************
*	Function Name	: JNI_ct_uic_RouteCalcAbort        	     *
*	Description	:                                            *
*	Date		: 09/11/21                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_ct_uic_RouteCalcAbort
  (JNIEnv *env, jclass clz,jint ModeType)
{
	unsigned int AheadWndID;
	unsigned int OriginWndID;
        int TransitionType;

	return (jlong)CT_UIC_RouteCalcAbort((ENUI_MODE_SET_TYPE)ModeType,&AheadWndID,&OriginWndID,&TransitionType);
}

/*********************************************************************
*	Function Name	: JNI_CT_ICU_SetDistination        	     *
*	Description	:                                            *
*	Date		: 10/01/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: xiayx	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_ICU_SetDistination
  (JNIEnv *env, jclass clz,jlong Lon,jlong Lat)
{
	return (jlong)CT_ICU_SetDistination((long)Lon,(long)Lat);
}

/*********************************************************************
*	Function Name	: JNI_CT_Navi_StartCalculate        	     *
*	Description	:                                            *
*	Date		: 10/01/12                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/
JNIEXPORT jlong JNICALL JNI_CT_Navi_StartCalculate
  (JNIEnv *env, jclass clz, jint calcType)
{
	return (jlong)CT_UIC_RouteCalculateWithPrevPoint((ENUI_MODE_SET_TYPE)calcType, DEF_NUI_HITTEST_SEARCHSTART);
}

/*********************************************************************
*	Function Name	: JNI_java_GetRouteCalcDistance			*
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_java_GetRouteCalcDistance(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	long	lnRouteCount = 0;
	long    alnDistance[6];
	jmethodID	mid = 0;
	jmethodID   ctorID2;
	jobject  obj_JNIlong = 0;
	jobjectArray  objArray_JNIlong = {0};

	lRet = (jlong)java_GetRouteCalcDistance( &lnRouteCount,alnDistance );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/ROUTE_DISTANCE");
	jclass Class_JNILong = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		mid = (*env)->GetMethodID(env,clazz,"initalnDistance","(I)V");
	}
	if(mid && lnRouteCount > 0)
	{
		(*env)->CallVoidMethod(env, object, mid, (jint)lnRouteCount);

		jfieldID jfldalnDistance = (*env)->GetFieldID(env, clazz,"alnDistance","[Lnet/zmap/android/pnd/v2/data/JNILong;");
		if(Class_JNILong != NULL)
		{
			objArray_JNIlong = (*env)->NewObjectArray(env, (jint)lnRouteCount, Class_JNILong, NULL);
		}
		else
		{
			LOGDp("[DEBUG] JNI_java_GetRouteCalcDistance  Find Class_JNILong Failed!!!@@@@@@@@@@@@@@@@@@@@");
		}
		int i = 0;
		for(i=0; i<lnRouteCount; i++)
		{
			//##########
			if(Class_JNILong != NULL)
			{
				ctorID2= (*env)->GetMethodID(env, Class_JNILong, "<init>", "()V");

				obj_JNIlong = (jobject)(*env)->NewObject(env, Class_JNILong, ctorID2);

				if(NULL == obj_JNIlong)
				{
					LOGDp("[DEBUG] JNI_java_GetRouteCalcDistance  NewObject obj_JNIlong Failed!!!@@@@@@@@@@@@@@@@@@@@");
				}
				else
				{
					jfieldID jlndistace = (*env)->GetFieldID(env, Class_JNILong,"lcount","J");
					if(jlndistace)
					{
						//Set the variable's value  to Java
						(*env)->SetLongField(env, obj_JNIlong,jlndistace,(jlong)alnDistance[i]);
						(*env)->SetObjectArrayElement(env, objArray_JNIlong, i, obj_JNIlong);
					}
					else
					{
						LOGDp("[DEBUG] JNI_java_GetRouteCalcDistance SetLongField jlndistace Failed");
					}
				}

				if(NULL != obj_JNIlong)
				{
					(*env)->DeleteLocalRef(env,obj_JNIlong);
					obj_JNIlong = NULL;
				}
			}
		}

		(*env)->SetObjectField(env, object, jfldalnDistance, objArray_JNIlong);

		if(NULL != objArray_JNIlong)
		{
			(*env)->DeleteLocalRef(env,objArray_JNIlong);
			objArray_JNIlong = NULL;
		}
	}

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jlnRouteCount = (*env)->GetFieldID(env, clazz,"lnRouteCount","J");
		if(jlnRouteCount)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnRouteCount,(jlong)lnRouteCount);
		}
		else
		{
			LOGDp("[DEBUG] JNI_java_GetRouteCalcDistance Getfield jlnRouteCount Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_java_GetRouteCalcDistance FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_DG_GetCalculateStat			*
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_DG_GetCalculateStat(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	long	plnCalcStat = 0;

	lRet = (jlong)DG_GetCalculateStat( &plnCalcStat );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jplnCalcStat = (*env)->GetFieldID(env, clazz,"lcount","J");
		if(jplnCalcStat)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jplnCalcStat,(jlong)plnCalcStat);
		}
		else
		{
			LOGDp("[DEBUG] JNI_DG_GetCalculateStat Getfield jplnCalcStat Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_DG_GetCalculateStat FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/*********************************************************************
*	Function Name	: JNI_java_GetShowDetailedMessage			*
*	Description	:                                            *
*	Date		: 09/12/08                                   *
*	Parameter	:                                            *
*	Return Code	: jlong                                       *
*	Author		: Wangxp	                                     *
*  ------------------------------------------------------------------*
* Revision History	                                             *
* No	Date		Revised by		Description          *
**********************************************************************/

JNIEXPORT jlong JNICALL JNI_java_GetShowDetailedMessage(JNIEnv *env, jclass clz, jobject object)
{
	jlong	lRet = 0;
	long	lnShowMessage = 0;

	lRet = (jlong)java_GetShowDetailedMessage( &lnShowMessage );

	//Fing Java's Class
	jclass clazz = (*env)->FindClass(env, "net/zmap/android/pnd/v2/data/JNILong");

	if(clazz)
	{
		//Get variable's ID in JNI
		jfieldID jlnShowMessage = (*env)->GetFieldID(env, clazz,"lcount","J");
		if(jlnShowMessage)
		{
			//Set the variable's value  to Java
			(*env)->SetLongField(env, object,jlnShowMessage,(jlong)lnShowMessage);
		}
		else
		{
			LOGDp("[DEBUG] JNI_java_GetShowDetailedMessage Getfield jlnShowMessage Failed");
		}
	}
	else
	{
		LOGDp("[DEBUG] JNI_java_GetShowDetailedMessage FindClass  Failed!!!!!!!!!!!!!!!");
	}

	return lRet;

}

/**
 * @brief
 * @param[in]
 * @return
 */
JNIEXPORT void JNICALL JNI_NE_Drv_PlayChangeVolume(JNIEnv *env, jclass clz)
{
	NE_Drv_PlayChangeVolume();
	return;
}
/**
 * @brief
 * @param[in]
 * @return
 */
JNIEXPORT void JNICALL JNI_NE_BackupMainRoute(JNIEnv *env, jclass clz)
{
	NE_BackupMainRoute();
	return;
}

//Add 2011/08/03 Z01thedoanh (自由解像度対応) Start -->
JNIEXPORT void JNICALL JNI_NE_SetAreaShowCalcRoute
  (JNIEnv *env, jclass clz, jint routeLeft,jint routeTop, jint routeRight, jint routeBottom,
		  jint wndLeft,jint wndTop, jint wndRight, jint wndBottom)
{
	// 探索結果地図RECTを設定
	RECT recRoute = {routeLeft, routeTop, routeRight, routeBottom};
	// ナビエンジンウィンドウのRectを設定
	RECT recWnd = {wndLeft, wndTop, wndRight, wndBottom};
	NE_SetAreaShowCalcRoute(recRoute, recWnd);
}
//Add 2011/08/03 Z01thedoanh (自由解像度対応) End <--
