/*
 * JNI_Error.h
 *
 *  Created on: 2011/08/31
 */

#ifndef JNI_ERROR_H_
#define JNI_ERROR_H_

#define JNI_ERR_OK					(0)
#define JNI_ERR_NG					(1)

/**
 * なるべく例外名と一致させる
 */
#define JNI_ERR_ILLEGAL_ARGUMENT	(1001)
#define JNI_ERR_ILLEGAL_STATE		(1002)
#define JNI_ERR_OUT_OF_MEMORY		(1003)
#define JNI_ERR_NULL_POINTER		(1004)
#define JNI_ERR_OUT_OF_RANGE		(1005)
#define JNI_ERR_INVALID_VALUE		(1006)
#define JNI_ERR_NOT_FOUND			(1007)
#define JNI_ERR_REQUEST_FAILURE		(1008)
#define JNI_ERR_REMOTE_ERROR 		(1009)
#define JNI_ERR_IO_ERROR	 		(1010)
#define JNI_ERR_NAVI_ENGINE_ERROR	(1011)
#define JNI_ERR_SERVICE_ERROR		(1012)
#define JNI_ERR_UNKNOWN_ERROR		(1013)

#endif /* JNI_ERROR_H_ */
