// JETFONT rasterizer for BMW (Ver. 1.18)
//
// (c) RICOH Co.,Ltd.
//
// Cache Enable
//
#ifndef JET_GLYPH_H
#define JET_GLYPH_H

// Mult Typeface Enable
#define	JETENCODE_RecNo                  0
#define	JETENCODE_Japan                932

//	signed char
//typedef signed char	JINT8, *PJINT8;

int	Lib_JISStrToUTF8Str(char* pcGBKText, char* pcUtf8, int iLenGBK,int iLenUtf8);
#endif
